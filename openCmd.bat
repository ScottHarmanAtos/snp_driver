echo off
echo *********************************************************
echo * This batch file assumes that you've put the           *
echo * TNS system under your normal CC_ROOT path (%CC_ROOT%) *
echo *********************************************************
echo * This app only changes the CC_SYSTEM, PATH and         *
echo * CC_WORKSTATION envinronment variables in this session *
echo * and these values are not changed on disk or persisted *
echo * anywhere. You can run the masterSystem without having *
echo * specifically set your system up to do so              *
echo *********************************************************
echo * This script just opens a command window with the      *
echo * environment set up for this system                    *
echo *********************************************************
pause

set CC_ROOT=c:\bncs
set CC_SERVER_ROOT=c:\bncs
set CC_SYSTEM=discovery
set CC_WORKSTATION=4
set path=%CC_ROOT%\%CC_SYSTEM%\windows\lib;%CC_ROOT%\%CC_SYSTEM%\windows\bin;%path%
start cmd.exe
