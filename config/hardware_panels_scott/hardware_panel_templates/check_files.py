# Author Scott Harman
# 2020
# Small python utility to check the number of elements in each json options leaf to ensure it's exactly divisible by 16
# will output a sensible response in the console window when executed

import json
import os

root = os.environ.get('CC_ROOT')
system = os.environ.get('CC_SYSTEM')

print(root)
print(system)

jsonpath = os.path.join(root,system,'config\\hardware_panels\\hardware_panel_templates')
print(jsonpath)
def json_print(data):
    for v in data:
        if isinstance(v, dict):
            if 'options' in v:
                if (len(v['options'])%16): 
                    print(v['label'].ljust(24, ' '),"Num entries is: \t", len(v['options']), " - Sub-level menu is NOT divisible by 16")
                else:
                    print(v['label'].ljust(24, ' '),"Num entries is: \t", len(v['options']), " - Sub-level menu is divisible by 16")
                #print(len(v['options']))
                json_print(v['options'])
            if 'functions' in v:
                if (len(v['functions'])%16): 
                    print(v['location'].ljust(24, ' '),"Num entries is: \t", len(v['functions']), " - Sub-level menu is NOT divisible by 16")
                else:
                    print(v['location'].ljust(24, ' '),"Num entries is: \t", len(v['functions']), " - Sub-level menu is divisible by 16")
                #print(len(v['options']))
                try:
                    json_print(v['options'])
                except:
                    print("An exception occurred")
                try:
                    json_print(v['functions'])
                except:
                    print("An exception occurred")
                
        else:
            # Nothing to do here
            continue

for file in os.listdir(jsonpath):
    if file.endswith(".json"):
        with open(os.path.join(jsonpath,file)) as json_file:
            print('\n\n------------------------------------------------')
            print('++++++++++++++++++++++++++++++++++++++++++++++++')
            print('------------------------------------------------')
            print(file)
            print('________________________________________________')

            data = json.load(json_file)
            if (len(data)%16): 
                print(file.ljust(24,' '),"Num entries is: \t",len(data)," - Top level is NOT divisible by 16")
            else:
                print(file.ljust(24,' '),"Num entries is: \t",len(data)," - Top level is divisible by 16")
            json_print(data)



