<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h1>
      <xsl:for-each select="device_description">
       <td><xsl:value-of select="@name"/></td>
      	</xsl:for-each>
      	</h1>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Name</th>
        <th>Class/Style</th>
        <th>Access</th>
        <th>Slot</th>
        <th>Detail</th>
      </tr>
      <xsl:for-each select="device_description/param">
      <tr>
        <td><xsl:value-of select="@name"/></td>
        <td><xsl:value-of select="@class"/> / <xsl:value-of select="@style"/></td>
        <td><xsl:value-of select="@access"/></td>
        <td><xsl:value-of select="@slot"/></td>
	<td>
	<xsl:for-each select="state">
	        <xsl:value-of select="@value"/> = <xsl:value-of select="@caption"/> <br />
      	</xsl:for-each>
	<xsl:for-each select="values">
	        min = <xsl:value-of select="@min"/><br />
	        max = <xsl:value-of select="@max"/><br />
	        step = <xsl:value-of select="@step"/><br />
	        bigstep = <xsl:value-of select="@bigstep"/><br />
	        default = <xsl:value-of select="@defaultvalue"/><br />
      	</xsl:for-each>
	<xsl:for-each select="display">
	
	<br />Display as:<br />
	        (<i>value</i> + <xsl:value-of select="@inoffset"/>) * <xsl:value-of select="@multiplier"/> + <xsl:value-of select="@outoffset"/> <b><xsl:value-of select="@units"/></b><br />
      	</xsl:for-each>
      </td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>

