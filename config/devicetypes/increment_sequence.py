import re
import itertools as it

#from functools import reduce # only in Python 3

import os

dir_path = os.path.dirname(os.path.realpath(__file__))
for file in os.listdir(dir_path):
     filename = os.fsdecode(file)
     if filename.endswith(".xml"): #or filename.endswith(".py"):
        print('Processing %1',os.path.join(dir_path, filename))
        cnt = it.count(1)


        with open(filename,'r') as mib_file:
            params = mib_file.readlines()

        for line in range(len(params)):
            #print (params[line])
            params[line] = re.sub(r'slot="[1-9]\d*"', lambda x: 'slot="{}"'.format(next(cnt)), params[line])
            #print (params[line])


        with open(filename,'w') as mib_file:
            mib_file.writelines(params)

        continue
     else:
        print ('No XML files found to process')
        continue


# regex:  
# (<bncs_nms.*)(/>\n<values)
# $1 syntax="integer"$2
