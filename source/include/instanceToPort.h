#if !defined(AFX_INSTANCETOPORT1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_)
#define AFX_INSTANCETOPORT1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#ifdef DO_INSTANCETOPORT_EXPORT
	#define INSTANCETOPORT_EXPORT __declspec(dllexport) 
#else
	#define INSTANCETOPORT_EXPORT __declspec(dllimport) 
#endif

#include <windows.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <map>

#include <ccClient.h>

using namespace std;

#pragma warning( disable: 4251 )
#pragma warning( disable: 4786 )

class INSTANCETOPORT_EXPORT portEntry
{
public:
	portEntry( const bncs_string & instance, const bncs_string & name, const int & device, int index )
	{
		m_instance = instance;
		m_name = name;
		m_device = device;
		m_index = index;
	};
	portEntry(void)
	{
		m_instance = "";
		m_name = "";
		m_device = 0;
		m_index = 0;
	};
	bncs_string m_instance;
	bncs_string m_name;
	int m_device;
	int m_index;
};

class INSTANCETOPORT_EXPORT instanceToPort : private IccClientCallback
{
	// give access to our internals to our helpers
	friend BOOL APIENTRY DllMain( HANDLE , DWORD  , LPVOID );
	friend class ccClientHelper;

public:
	bool getStatus(bncs_string & s_errMsg);
	instanceToPort();
	virtual ~instanceToPort();

	// singleton accessor (loading is taken care of in the DLL load/unload functions
	static instanceToPort * getinstanceToPortObject( void );
	bool getValue( const bncs_string & instance, const bncs_string & name, portEntry & ret);
	bool getValue( const bncs_string & instance, const bncs_string & name, portEntry & ret , const bncs_string & routerInstance);
	bool getValue(const bncs_string &instance, const bncs_string & name, portEntry & ret, const bool & checkInstancesWithinComposite);
	bool getValue(const bncs_string &instance, const bncs_string & name, portEntry & ret, const bncs_string & routerInstance, const bool & checkInstancesWithinComposite);
private:
	inline bool getMapValue(const multimap< bncs_string, portEntry > & m_map, const bncs_string & instance, const bncs_string & name, portEntry & ret);
	inline bool getMapValue_with_device_id(const multimap< bncs_string, portEntry > & m_map, const bncs_string & instance, const bncs_string & name, portEntry & ret, const bncs_string & routerInstance);

	int getDeviceID(bncs_config & c_instanceID);
	bool b_validObjectSetting;
	void load( void );

	static instanceToPort *m_instanceToPortObj;

	multimap< bncs_string, portEntry > m_values_in_composite;
	
	multimap< bncs_string, portEntry > m_values;
	
	void setValue( int device, int index, int database, const bncs_string & name );
	
	bncs_dll_client_shim *m_shim;
};

#endif // !defined(AFX_INSTANCETOPORT1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_)
