#pragma once

using namespace std;
#include <Windows.h>
#define ODS OutputDebugStringA

#include <map>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <bncs_config.h>

#define CONFIG_GROUP_OUTPUTS				"outputs"
#define CONFIG_GROUP_PREFIX_OUTPUTS			"output_"
#define CONFIG_GROUP_INPUTS					"inputs"
#define CONFIG_GROUP_PREFIX_INPUTS			"input_"
#define CONFIG_GROUP_DRIVER					"driver"
#define CONFIG_SETTING_DRIVER_ADDRESS		"driver_address"
#define CONFIG_SETTING_DRIVER_PORT			"driver_port"
#define CONFIG_TAG_INPUT_MAPPING			"tag_input_mapping"
#define CONFIG_TAG_INPUT_MAPPING_GROUP		"tag_input_mapping"
#define CONFIG_TAG_INPUT_MAPPING_DEST_PKG	"dest_pkg"
#define CONFIG_TAG_OUTPUT_GEOMETRY_PARAM	"geometry_version"
#define CONFIG_TAG_OUTPUT_LAYOUT_ID_PARAM	"layout_id"
#define	BNCS_TAGMV_ERROR				-1

class bncs_tagmv
{
public:
	bncs_tagmv();
	~bncs_tagmv();

	bncs_tagmv(bncs_string cInstance, int head);

	bncs_string instance();
	bncs_string altId();

	int head();

	bool isValid();

	bncs_string requestURI();
	bncs_string layoutsURI();
	bncs_string layoutURI(bncs_string layout_id);
	bncs_string headInstance();
	bncs_string headId();
	bncs_string headAltId();

	int inputDestPkg(int input);

	int headDevice();
	//int geometryDevice();
	int geometryIndex();
	int layoutIdIndex();

	friend bool operator==(const bncs_tagmv &lhs, const bncs_tagmv &rhs)
	{
		// we are equal if
		// both sides are using the same cinstance
		// and
		// both sides are using the same output head

		bool result = (lhs.m_cinstance == rhs.m_cinstance && lhs.m_head == rhs.m_head);
		ODS(bncs_string("bncs_tagmv::compare %1-%2 with %3-%4, MVs are %5").arg(lhs.m_cinstance).arg(lhs.m_head).arg(rhs.m_cinstance).arg(rhs.m_head).arg(result ? "equal" : "NOT equal"));

		return result;
	}

	friend bool bncs_tagmv::operator!=(const bncs_tagmv &lhs, const bncs_tagmv &rhs)
	{

		bool result = !(lhs == rhs);

		ODS(bncs_string("bncs_tagmv::NOT compare %1-%2 with %3-%4, MVs are %5").arg(lhs.m_cinstance).arg(lhs.m_head).arg(rhs.m_cinstance).arg(rhs.m_head).arg(result ? "NOT equal" : "equal"));
		return result;
	}

private:
	struct tagOutputInstance
	{
		bncs_string group;		// group name within the composite
		bncs_string instance;	// instance name of the output
		bncs_string altid;		// instance altid of the output
		int device;				// device number of the output
		int offset;				// offset of the output
		bncs_string id;			// uuid of the output
		bncs_string type;		// devicetype
		int geometry;			// slot for the geometry
		int layout_id;			// slot for the layout id

		tagOutputInstance()
		{
			group = "";
			instance = "";
			altid = "";
			device = 0;
			offset = 0;
			id = "";
			type = "";
			geometry = 0;
			layout_id = 0;
		}

		bool isValid()
		{
			return (instance.length() && device > 0 && id.length() && geometry && layout_id);
		}

		int index()
		{
			return (isValid() ? group.firstInt() : BNCS_TAGMV_ERROR);
		}
	};

	struct tagInputInstance
	{
		bncs_string group;
		bncs_string instance;
		int device;
		int offset;
		int dest_pkg;

		tagInputInstance()
		{
			group = "";
			instance = "";
			device = 0;
			offset = 0;
			dest_pkg = BNCS_TAGMV_ERROR;
		}

		bool isValid()
		{
			return (instance.length() && device > 0 && dest_pkg > 0);
		}

		int index()
		{
			return (isValid() ? group.firstInt() : BNCS_TAGMV_ERROR);
		}
	};

	bncs_string m_cinstance;		// composite instance for the tag
	bncs_string m_caltid;			// composite alt_id for the tag
	bncs_string m_driver_instance;	// instance for driver device
	bncs_string m_input_cinstance;	// composite instance holding all the inputs
	bncs_string m_output_cinstance;	// composite instance holding all the head outputs
	map<int, tagOutputInstance> m_output_instance_map;
	map<int, tagInputInstance> m_input_instance_map;
	int m_head;
	bncs_string m_driver_address;
	bncs_string m_driver_port;
	bool m_isValid;

	void initValues();
};

