#if !defined(AFX_CCMCONFIG1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_)
#define AFX_CCMCONFIG1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#ifdef DO_CCMCONFIG_EXPORT
	#define CCMCONFIG_EXPORT __declspec(dllexport) 
#else
	#define CCMCONFIG_EXPORT __declspec(dllimport) 
#endif

#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "sqlite3.h"
using namespace std;



class CCMCONFIG_EXPORT local_cache  
{
public:
	local_cache();
	virtual ~local_cache();

	static local_cache * getLocalCache( void );

	bool setValue(bncs_string key, bncs_string value);
	bool getValue(bncs_string key, bncs_string &value);
	bool deleteValue(bncs_string key, bncs_string &value);

private:
	static local_cache *local_cacheObj;
	friend BOOL APIENTRY DllMain( HANDLE , DWORD  , LPVOID );

	void load( void );

	sqlite3 *m_db;
	sqlite3_stmt *m_setStmt;
	sqlite3_stmt *m_getStmt;
	sqlite3_stmt *m_deleteStmt;

	// these are samples - replace with your own

};

#endif // !defined(AFX_CCMCONFIG1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_)
