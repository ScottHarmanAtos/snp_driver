#pragma once
//Common Packager Definitions - used by the following components:
//	panels/packaging/dest_package_editor
//TODO
//  _components/routing/source_package
//	_components/routing/dest_package
//	panels/packaging/source_package_editor

//Packager databases
#define DATABASE_SOURCE_NAME	0
#define DATABASE_DEST_NAME		1
#define DATABASE_SOURCE_TITLE	2
#define	DATABASE_DEST_TITLE		3
#define DATABASE_SOURCE_AP		6
#define	DATABASE_DEST_LEVELS	7
#define DATABASE_MCR_UMD		9
#define DATABASE_PROD_UMD		10
#define DATABASE_SOURCE_VIDEO	11

#define DATABASE_DEST_PACKAGE_AUDIO_PROC_USAGE	13

#define DATABASE_DEST_AUDIO		15	
#define DATABASE_DEST_LANG_TYPE	16
#define DATABASE_DEST_AUDIO_1	17	//dest:		audio  1-16 16x audio#lang-tag:type-tag,
#define DATABASE_DEST_AUDIO_2	18	//dest:		audio 17-32 16x audio#lang-tag:type-tag,
#define DATABASE_DEST_REV_AUDIO 19	//dest:		rev audio 1,2   a1=audio#lang-tag:type-tag,a2=audio#lang-tag:type-tag

#define DATABASE_SOURCE_ID		14	//Temp - will be 24 exported from DN01/101
#define DATABASE_DEST_ID		15	//Temp - will be 25 exported from DN01/101

#define DATABASE_SPEV_ID		31
#define DATABASE_RECORD_ID		32

#define DATABASE_MCR_NOTES		37

#define DATABASE_VIDEO_HUB_TAG	41

#define DATABASE_LANG_TAG		43
#define DATABASE_TYPE_TAG		44

#define DATABASE_AP_NAME		60
#define DATABASE_AP_LONG_NAME	61
#define DATABASE_AP_ID_OWNER	62
#define DATABASE_AP_TAGS		63
#define DATABASE_AP_PAIRS		64
#define DATABASE_AP_RETURN		65
#define DATABASE_AP_VIDEO_LINK	66
#define DATABASE_AP_CF_LABELS	67
#define DATABASE_AP_CONF_LABELS	68
#define DATABASE_AP_AUDIO_PROC	69

#define DATABASE_PRESET_MAPPING	70
#define DATABASE_PRESET_NAME	71
#define DATABASE_PRESET_PRIMARY	72
#define DATABASE_PRESET_SUB1	73
#define DATABASE_PRESET_SUB2	74

#define DATABASE_SPEV_01_08		131
#define DATABASE_SPEV_09_16		132
#define DATABASE_SPEV_17_24		133
#define DATABASE_SPEV_25_30		134

#define	INSTANCE_PACKAGE_ROUTER	"packager_router_main"

#define AP_LEVEL_COUNT			32

