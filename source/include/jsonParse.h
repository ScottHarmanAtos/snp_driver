#pragma once

#include <bncs_string.h>
#include "json-forwards.h"
#include "json.h"

using namespace std;

#pragma warning(disable : 4275)

class JsonParse
{
public:
	JsonParse();

	~JsonParse();

	Json::Value Parse(bncs_string& value);

	bncs_string Parse(Json::Value& value);

	bncs_string String;
	Json::Value Json;

	bool isParseValid();

protected:
	void virtual GetValues(Json::Value& v) = 0;
	bncs_string JsonToString(Json::Value v);

private:
	bool _isParseValid;
};

