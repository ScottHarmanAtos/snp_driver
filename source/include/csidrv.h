#if !defined(AFX_CSIDRV_H__A9DB83DB_A9FD_11D0_BFD1_444553545200__INCLUDED_)
#define AFX_CSIDRV_H__A9DB83DB_A9FD_11D0_BFD1_444553545200__INCLUDED_

#include <bncsdef.h>

#ifndef GTHUNKS_EXPORTS
#undef GTHUNKS_API
#define GTHUNKS_API
#endif

#define __EXTDRIVER_VERSION__ "4.5"


class GTHUNKS_API extdriver
{
public:
	extdriver();
	~extdriver();

	UINT connect(UINT iDevice, HINSTANCE hInstApp=NULL, UINT iType=DRVTYPE_NONE);
	UINT connectx(UINT iDevice);
	void notify(LRESULT(*)(extdriver*,LPCSTR));

	void setcounters(PULONG txcounter, PULONG rxcounter);
	void incrx(void);

	UINT getstate(void);
	UINT setstate(UINT stateflags);

	LRESULT txrevmsg(LPSTR netmessage,UINT iCallingWS, BOOL fSendNow=FALSE);
	LRESULT txquerymsg(LPSTR netmessage,UINT iCallingWS, BOOL lastflag=FALSE);
	LRESULT txrevqueue();

	void getdbname(WORD device, WORD database, WORD index, LPSTR namebuffer, int iMaxSize=32);
	int getdbindex(WORD device, WORD database, LPCSTR name);

	UINT gettype(void);

	LRESULT setifmode(WORD mode);
	WORD getifmode(void);
	
	UINT tokenize(void);

	LRESULT (*func)(extdriver*,LPCSTR);

	UINT iDevice;
	LPSTR netmsg;
	UINT paramcount;
	LPSTR param[16];

private:
	HWND hWndSpawn;
	PULONG txcount;
	PULONG rxcount;
	LPSTR netmsgtok;
	UINT iStatus;
	UINT iDrvType;
	ULONG deftx,defrx;

	static HINSTANCE hInstEx;
};

#endif