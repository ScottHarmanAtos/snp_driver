#pragma once

using namespace std;

#include <map>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <bncs_config.h>

#define CONFIG_PACKAGER_CONFIG			"packager_config"
#define CONFIG_PACKAGER_AUTO			"packager_auto"
#define CONFIG_PACKAGER_DIVISOR			"packager_divisor"
#define CONFIG_NUMBER_OF_PACKAGES		"number_of_packages"
#define CONFIG_PACKAGER_FIRST_VIRTUAL	"first_virtual_package"
#define CONFIG_GROUP_ROUTER				"router"
#define BNCS_PACKAGER_ERROR_STRING		""
#define BNCS_PACKAGER_ERROR				-1
#define BNCS_PACKAGER_NAMES_DEVICE		1
#define BNCS_PACKAGER_BASE_DEVICE		1

class bncs_packager
{
	/*****************************************
			bncs_packager helper class
	******************************************

	Extracts config for a BNCS packager
	This is particularly useful where the number of destinations is > 4096
	as it is then necessary to use multiple infodrivers to accommodate the packager

	Useage:
	
		You need to add bncs_packager.lib into the dependencies for your project
		Properties > Configuration Properties > Linker > Input > Additional Dependencies
		Ensure you have the correct Configuration selected (normally "Release")
	
		Create a new instance of the class with the packager composite instance

			m_packager = bncs_packager("c_packager");

		By default this looks up the "router" group in the overall composite
		use a second parameter to specify a different group that operates with the same divisor principles

			m_packager_status = bncs_packager("c_packager","dest_status");

		This relies on the overall composite group name being part of the group names in the array devices
		For example the "dest_status" group points to a composite instance which has group names of the form "packager_dest_status_x"
			
		Check isValid() to ensure the packager config is valid

			debug("m_packager config is %1", m_packager.isValid() ? "valid" : "NOT valid");

		Use the destDev() and destSlot() methods to resolve a real destination index into a device number and slot

		Where the first infodriver group device number is 1001 and the block size is 4000

			int myInfodriverDevice = m_packager.destDev(17595)	// returns 1005 (the infodriver device for destination 17595)
			int myInfodriverSlot = m_packager.destSlot(17595)	// returns 1595 (the infodriver slot for destination 17595)

		Use deviceCount(), packageDivisor() and device() to register for all slots on the packager
		Note that the device() array is 1 based and corresponds to the integer withing the composite group name

			if (m_packager.isValid())
			{
				for (int i = 0; i < m_packager.deviceCount(); i++)
				{
					infoRegister(m_packager.device(i+1), 1, m_packager.packagerDivisor());
				}
			}

		Use toDest() with a device number and slot to convert back the other way for example within a revertive message
			
			int dest = m_packager(r->device(), r->index());
	*/

public:
	bncs_packager();
	~bncs_packager();

	bncs_packager(bncs_string pInstance, bncs_string pGroup=CONFIG_GROUP_ROUTER);	// Create a new bncs_packager instance using the packager composite instance

	bool isValid();									// Returns true if the packager initialised correctly
	bncs_string destInstance(int dest);				// Returns the group instance for the real destination 'dest'
	int destDev(int dest);							// Returns the group device number for the real destination 'deat'
	int destSlot(int dest);							// Returns the slot number for the real destination 'deat'
	int deviceCount();								// Returns the count of the group devices
	int numberOfPackages();							// Returns the total number of packages in the packager
	int packagerDivisor();							// Returns the block size of each group
	int firstVirtual();								// Returns the first virtual destination in the packager
	bncs_string instance(void);						// Returns the composite instance name for group 'router'
	bncs_string instance(int router);				// Returns the group instance name for group 'router' - 1 based index
	int device(int router);							// Returns the group instance device number for group 'router' - 1 based index
	int device(void);								// Returns the group instance for the base device of the group
	int deviceNames(int database = 0);				// Returns the device number for names - nominally the root device
	int toDest(int dev, int slot);					// Returns the real destination number for a device/index pair
	bool isDevice(int dev);							// Returns true if dev is a device within the packager device group
	bncs_string config();							// Returns the name of the config section used within packager_config.xml
	bncs_string getConfigItem(bncs_string pItem);	// Returns config item pItem from the resolved config section

private:
	struct pgInstance
	{
		bncs_string group;
		bncs_string instance;
		int device;

		pgInstance()
		{
			group = "";		//	group id within the composite instance
			instance = "";	//	instance of that group
			device = 0;		//	device number of that instance
		}

		bool isValid()
		{
			return (instance.length() && device > 0);
		}

		int index()
		{
			return (isValid() ? group.firstInt() : BNCS_PACKAGER_ERROR);
		}
	};

	bncs_string m_config_packager_auto;	// path to the config
	bncs_string m_group_instance;		// resolved instance from the selected group withing the composite
	map<int, pgInstance> m_packager_instance_map;
	int m_numberOfPackages;
	int m_packagerDivisor;
	int m_firstVirtualPackage;
	int m_deviceCount;
	bool m_isValid;

	void initValues();
	bncs_string getConfigItem(bncs_string pConfig, bncs_string pAuto, bncs_string pItem);
};

