#if !defined(AFX_riedelHelper1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_)
#define AFX_riedelHelper1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#ifdef DO_riedelHelper_EXPORT
	#define riedelHelper_EXPORT __declspec(dllexport) 
#else
	#define riedelHelper_EXPORT __declspec(dllimport) 
#endif

#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <bncs_config.h>
#include <map>
#include <list>

#include <ccClient.h>
#include "databasemgr.h"
#include "instanceLookup.h"

using namespace std;

#pragma warning( disable: 4251 )
#pragma warning( disable: 4786 )
#pragma warning (disable: 4275 )

//From RiedelCommon
enum
{
	TL_MODE_TALKLISTEN,
	TL_MODE_TALK,
	TL_MODE_LISTEN
};

enum
{
	KEY_MODE_MOMDIM,
	KEY_MODE_AUTODIM,
	KEY_MODE_LATCHNODIM
};


enum MASTER_CHANNEL_MODE
{
	MODE_UNKNOWN,
	MODE_ALL_CH1,
	MODE_ALL_CH2,
	MODE_MIXED
};

#define KEY_MODE_LIST  "KMD1,KAD1,KLD0"
#define TALKLISTEN_MODE_LIST   "TL,T,L"
#define DEFAULT_TALK_LISTEN_MODE "TL"
#define DEFAULT_KEYMODE "KMD1"
#define DEFAULT_DIM_MODE  "D1"
#define RIEDEL_PORT_TYPE_STRINGS    "undefined,4-w input,4-w output,4-w split,4-wire,panel, panel(dual),pool port,codec"


#define MSG_PORT_IS_PANEL  "Port is a|PANEL|Autolisten|OFF"

//End of stuff from RiedelCommon

struct riedelHelper_EXPORT riedelHelpers
{
	struct riedelHelper_EXPORT databases
	{
		static const int Input_Port_Button_Name = 0;
		static const int Output_Port_Button_Name = 1;
		static const int Input_Long_Name = 2;
		static const int Output_Long_Name = 3;
		static const int Port_Details = 4;
		static const int Assignable_Keys = 5;
		static const int Port_Address = 6;
		static const int Upstream_Details = 7;
		static const int TB_Assign_Groups = 11;
		static const int Subtitle_Input = 12;
		static const int Subtitle_Output = 13;
		static const int Input_8_Char = 20;
		static const int Output_8_Char = 21;
	
		static const int Conference_Name = 0;
		static const int Logic_Name = 0;
		static const int IFB_Name = 0;

		static const int Monitor_Channel = 0;
		static const int Mapping = 8;
		static const int Port_Type = 9;

		static const int Monitor_Lookup = 9;
		static const int Monitor_Column = 11;
		static const int Mixer_Nickname = 0;

		//Functions on Monitor Device
		static const int Function_Name = 0;
		static const int Functions = 4;


		//PTI Device
		static const int Take_Type = 9;
	};

	struct riedelHelper_EXPORT max
	{
		static const int Conferences = 500;
		static const int IFBs = 1500;
		static const int Keys = 32;
	};

	struct riedelHelper_EXPORT offset
	{
		static const int LISTEN_PTI = 	2000;
		static const int CONF_LABEL = 1000;
		static const int CONTRIBUTING_SOURCE_INFO = 2000;
		static const int CONF_PANEL_MEMBERS = 2000;

		static const int MIXERS = 500;

		static const int OUTPUT_GAIN = 2000;

		static const int ALIAS_DEST = 2000;

		static const int EXTRAS_PROBE_PORT = 301;
		static const int EXTRAS_PROBE_COMMAND_COUNT = 302;
		static const int EXTRAS_PROBE_COMMAND_FIRST = 306;
		static const int EXTRAS_PROBE_COMMAND_LAST = 325;

		//Start and end of the extra probe slots
		static const int EXTRAS_PROBE_START = EXTRAS_PROBE_PORT;
		static const int EXTRAS_PROBE_END = EXTRAS_PROBE_COMMAND_LAST;
	};

	struct riedelHelper_EXPORT icons
	{
		static const bncs_string CONF;
		static const bncs_string IFB;
		static const bncs_string LOGIC;
		static const bncs_string _ERROR; //Putting underscore in front and a MACRO is defined with this name, I could ignore the macro, but every where this is used you would have to ignore it. 
		static const bncs_string PANEL;
		static const bncs_string _4WIRE;
		static const bncs_string _4WIRE_SPLIT;
		static const bncs_string DUAL;
		static const bncs_string LOCAL_CH2;
		static const bncs_string REMOTE_CH2;
		static const bncs_string NONE;

		static const bncs_string PIXMAP_LOCATION_CONF;
		static const bncs_string PIXMAP_LOCATION_IFB;

		static const bncs_string PIXMAP_CONF;
		static const bncs_string PIXMAP_IFB;
	};

	struct riedelHelper_EXPORT defaults
	{
		static const bncs_string KEY_MODE;
		static const bncs_string TALK_LISTEN;
		static const bncs_string DIM_MODE;
	};
	
};

class riedelHelper_EXPORT ringmaster_devices
{
public: 
	int Main;
	int Ifbs;
	int Conferences;
	map<int,int> Ports;
	int Ifb_pti;
	bncs_string Composite;
	ringmaster_devices()
	{
		Main = 0;
		Ifbs = 0;
		Conferences = 0;
		Ports.clear();
		Ifb_pti = 0;;
	}
};


//Riedel port types
class riedelHelper_EXPORT Devices
{
public:
	int Grd;
	int Monitor;
	int Pti;
	int Alias;
	int Gpio;
	int Gain;
	int Conference;
	int Ifb;
	int Extras;
	int Lock;
	bncs_string Grd_Instance;
	bncs_string Monitor_Instance;
	bncs_string Pti_Instance;
	bncs_string Alias_Instance;
	bncs_string GPIO_Instance;
	bncs_string Gain_Instance;
	bncs_string Conference_Instance;
	bncs_string Ifb_Instance;
	bncs_string Extra_Instance;
	bncs_string Lock_Instance;

	Devices()
	{
		Grd = 0;
		Monitor = 0;
		Pti = 0;
		Alias = 0;
		Gpio = 0;
		Gain = 0;
		Conference = 0;
		Ifb = 0;
		Extras = 0;
		Lock = 0;
	}
};
;


//This enum holds differnt views that are possible.
enum ENUM_PORT_VIEWS
{
	VIEW_PORT_TYPE_NONE,
	VIEW_PORT_TYPE_INPUT,
	VIEW_PORT_TYPE_OUTPUT,
	VIEW_PORT_TYPE_SPLIT,
	VIEW_PORT_TYPE_4WIRE,
	VIEW_PORT_TYPE_PANEL,
	VIEW_PORT_TYPE_PANEL_DUAL_CHANNEL,
	VIEW_PORT_TYPE_CODEC,
	VIEW_PORT_TYPE_POOL_PORT,
	VIEW_PORT_MAPPING_INPUT,
	VIEW_PORT_MAPPING_OUTPUT,
	VIEW_PORT_FUNCTION
};

//These are all of the port types that exist
enum ENUM_RIEDEL_PORT_TYPES
{
	PORT_TYPE_NONE,
	PORT_TYPE_INPUT,
	PORT_TYPE_OUTPUT,
	PORT_TYPE_SPLIT,
	PORT_TYPE_4WIRE,
	PORT_TYPE_PANEL,
	PORT_TYPE_PANEL_DUAL_CHANNEL,
	PORT_TYPE_CODEC,
	PORT_TYPE_POOL_PORT
};


class riedelHelper_EXPORT getPortType
{
private:
	static const bncs_string Input;
	static const bncs_string Output;
	static const bncs_string Split;
	static const bncs_string Wire4;
	static const bncs_string Panel;
	static const bncs_string Dual;
	static const bncs_string Codec;
	static const bncs_string PoolPort;
	static const bncs_string Mapping_Input;
	static const bncs_string Mapping_Output;
	static const bncs_string None;

	static const bncs_string Long_NA;
	static const bncs_string Long_Split;
	static const bncs_string Long_4Wire;
	static const bncs_string Long_Panel;
	static const bncs_string Long_Dual_Panel;
	static const bncs_string Long_Not_used;		 
	static const bncs_string Long_Codec;
	static const bncs_string Long_PoolPort;

	static const bncs_string Short_NA;
	static const bncs_string Short_Split;
	static const bncs_string Short_4Wire;
	static const bncs_string Short_Panel;
	static const bncs_string Short_Dual_Panel;
	static const bncs_string Short_Not_used;
	static const bncs_string Short_Codec;
	static const bncs_string Short_PoolPort;


public:

	static bool IsValid(int portType);

	static const list<int> ValidPorts();

	static const ENUM_PORT_VIEWS view_port_type(const bncs_string& s);

	static const bncs_string& view_port_type(ENUM_PORT_VIEWS i);

	static const ENUM_RIEDEL_PORT_TYPES port_type(const bncs_string& s);

	static const bncs_string& port_type(ENUM_RIEDEL_PORT_TYPES portType);

	static const bncs_string& Name_Long(ENUM_RIEDEL_PORT_TYPES portType);

	static const bncs_string& Name_Type(ENUM_RIEDEL_PORT_TYPES portType);
};




class comms_rings
{
public:
	int ring_id;
	bncs_string ID;
	bncs_string label;
	bncs_string site;
	bncs_string instance;
	Devices devices;
	bool valid;

	comms_rings()
	{
		this->valid = false;
	}

	static comms_rings Empty;

	comms_rings(int ring_id)
	{
		this->ring_id = ring_id;
		this->valid = true;
	}

	~comms_rings()
	{
	}

	const bncs_string toString()
	{
		return bncs_string("riedelHelper comms_rings ring_id:%1 ID:%2 label:%3 site:%4 instance:%5").arg(ring_id).arg(ID).arg(label).arg(site).arg(instance);
	}

	void print()
	{
		OutputDebugString(toString());
	}

};


class comms_ringmaster_port
{
public:
	bncs_string ring_port;
	bncs_string ring_port_leading_zero;
	int ring_id;
	int port;
	bncs_string instance;
	int slot_index;

	bncs_string sourceName_short;
	bncs_string sourceName_long;
	bncs_string destName_short;
	bncs_string destName_long;
	ENUM_RIEDEL_PORT_TYPES portType;
	bncs_string rrcs_address;
	bncs_string source_8_char;
	bncs_string dest_8_char;

	bool valid;

	Devices devices;

	comms_ringmaster_port()
	{
		ring_id = 0;
		port = 0;
		slot_index = 0;
		portType = ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_NONE;
		valid = false;
	}

	bool operator==(const comms_ringmaster_port& other) {
		return this->ring_port == other.ring_port;
	}

	bool operator!=(const comms_ringmaster_port& other) {
		return !(*this == other);
	}

	comms_ringmaster_port(int ring_id, int port, const bncs_string& instance, int slot_index, const Devices& devices)
	{
		bncs_string s = "%1.%2";
		s.replace("%1", ring_id);
		s.replace("%2", port);
		this->ring_port = s;
		this->ring_port_leading_zero = bncs_string("%1.%2").arg(ring_id, '\00',2U).arg(port, '\0000', 4U);
		this->ring_id = ring_id;
		this->port = port;
		this->instance = instance;
		this->slot_index = slot_index;
		this->devices = devices;
		this->valid = true;
	}

	~comms_ringmaster_port()
	{
	}


	bncs_string toString()
	{
		return bncs_string("riedelHelper comms_ringmaster_port ring_id:%1 port:%2 instance:%3 slot_index:%4").arg(ring_id).arg(port).arg(instance).arg(slot_index);
	}




	void print()
	{
		OutputDebugString(toString());
	}
};

class comms_ringmaster_output
{
public:
	comms_ringmaster_port port;
	bncs_string instance;
	int slot_index;
	int device;
	bool valid;

	comms_ringmaster_output()
	{
		device = 0;
		slot_index = 0;
		valid = false;
	}

	bool operator==(const comms_ringmaster_output& other) {
		return this->port.ring_port == other.port.ring_port;
	}

	bool operator!=(const comms_ringmaster_output& other) {
		return !(*this == other);
	}

	comms_ringmaster_output(const comms_ringmaster_port &port, const bncs_string& instance, int device, int slot_index)
	{
		this->port = port;
		this->instance = instance;
		this->slot_index = slot_index;
		this->device = device;
		this->valid = true;
	}

	~comms_ringmaster_output()
	{
	}


	bncs_string toString()
	{
		return bncs_string("riedelHelper comms_ringmaster_output ring_port:%1 instance:%2 device:%3 slot_index:%4").arg(port.ring_port).arg(instance).arg(device).arg(slot_index);
	}

	void print()
	{
		OutputDebugString(toString());
	}
};

struct RingPort
{
public:
	int Ring;
	int Port;

	RingPort(int ring, int port)
	{
		Ring = ring;
		Port = port;
	}

	bool operator==(const RingPort& r) const
	{
		return Ring == r.Ring && Port == r.Port;
	}

	bool operator<(const RingPort& r) const
	{
		return Ring < r.Ring ||  (Ring == r.Ring && Port < r.Port);
	}
};

struct PanelInfo
{
public:
	int Ring;
	int Port;
	int Expansion;
	bncs_string Uid;
	bncs_string Type;
	comms_ringmaster_port* CommsPort;
};

typedef map<int, comms_rings> COMMS_RINGS_LOOPKUP;
typedef map<bncs_string, comms_ringmaster_output> COMMS_RINGMASTER_OUTPUT_LOOKUP;
typedef map<bncs_string, comms_ringmaster_port> COMMS_RINGMASTER_PORT_LOOKUP;

class riedelHelper_EXPORT riedelHelper : private IccClientCallback
{
	// give access to our internals to our helpers
	friend BOOL APIENTRY DllMain( HANDLE , DWORD  , LPVOID );
	friend class ccClientHelper;

public:
	riedelHelper();
	virtual ~riedelHelper();

	// singleton accessor (loading is taken care of in the DLL load/unload functions
	static riedelHelper * getriedelHelperObject( void );

	const COMMS_RINGMASTER_PORT_LOOKUP& AllPorts();
	const COMMS_RINGMASTER_PORT_LOOKUP& SourcePorts();
	const COMMS_RINGMASTER_PORT_LOOKUP& OutputPorts();
	const COMMS_RINGS_LOOPKUP& GetRings();
	const comms_ringmaster_port& GetPort(int ring, int port);
	const comms_ringmaster_port& GetPort(const bncs_string& ringport);
	const comms_ringmaster_output& GetOutputPort(int ring, int port);
	const comms_ringmaster_output& GetOutputPort(const bncs_string& ringport);
	const ringmaster_devices& GetRingmasterDevices();
	const comms_rings& GetRing(int ring);
	const comms_rings& GetRing(bncs_string compositeInstance);

	//RRCS Address format is 1.2.0 <- Port address found in DB6
	const comms_ringmaster_port& GetPortFromRRCSAddress(int ring, bncs_string rrcsAddress);

	//Ring and RRCS Address format is ring#rrcs address 10#1.2.0
	const comms_ringmaster_port& GetPortFromRRCSAddress( bncs_string ringAndRRCSAddress);

	//Get the assign groups related to a port
	const vector<comms_ringmaster_port*>& GetAssignGroup(int ring, int port);
	const vector<comms_ringmaster_port*>& GetAssignGroup(bncs_string assignGroupName);
	const bool CanAccessPanel(int ring, int port);
	const bool CanAccessAssignGroup(int ring, int port);
	const comms_ringmaster_port& GetLocalPanel();
	const bncs_string& Ops_Position();
	const vector<PanelInfo> GetLocalPorts(int ring, int port);
	const vector<ringmaster_devices>& GetOtherRingmasterDevices();

private:

	COMMS_RINGS_LOOPKUP comms_rings_lookup;
	COMMS_RINGMASTER_OUTPUT_LOOKUP comms_ringmaster_output_lookup;
	COMMS_RINGMASTER_PORT_LOOKUP comms_ringmaster_all_port_lookup;
	COMMS_RINGMASTER_PORT_LOOKUP comms_ringmaster_output_port_lookup;
	COMMS_RINGMASTER_PORT_LOOKUP comms_ringmaster_rrcs_lookup;
	map<bncs_string, vector<comms_ringmaster_port*>> assign_group_to_port_lookup;
	map<RingPort, bncs_string> port_to_assign_group_lookup;
	map<bncs_string, vector<PanelInfo>> ops_position_to_panel_info;
	map<RingPort, bncs_string> port_to_ops_postion_lookup;
	const bool CanAccessLocalPanels(int ring, int port);

	void load( void );

	COMMS_RINGMASTER_OUTPUT_LOOKUP load_comms_ringmaster_output(const COMMS_RINGMASTER_PORT_LOOKUP& ports);
	COMMS_RINGS_LOOPKUP load_comms_rings(void);
	void load_comms_ports(const COMMS_RINGS_LOOPKUP&);
	COMMS_RINGMASTER_PORT_LOOKUP set_output_port_lookup(const COMMS_RINGMASTER_OUTPUT_LOOKUP&);
	ringmaster_devices load_ringmaster_devices(bncs_string ringmasterComposite);

	static riedelHelper *m_riedelHelperObj;

	vector<ringmaster_devices> m_other_ringmasters;
	ringmaster_devices m_ringmaster_devices;
	//bncs_dll_client_shim *m_shim;
	databaseMgr dbm;

	comms_ringmaster_port m_EMPTY_PORT;
	comms_ringmaster_output m_EMPTY_OUTPUT_PORT;

	// Helper functions
	int GetDevice(const bncs_string& instance);
	Devices GetDeviceComposite(const bncs_string& instance);

	void print(const bncs_string& s);

	bncs_string sanitize(const bncs_string& s);

	bncs_string combineRingPort(int ring, int port);
	pair<int,int> splitRingPort(const bncs_string& ringport);

	bncs_stringlist m_tb_assign_access;
	comms_ringmaster_port m_local_port;
	bncs_string m_ops_position;


};

#endif // !defined(AFX_riedelHelper1_H__D0DE1248_4051_4F19_BEF8_FE4E0593E4EB__INCLUDED_)
