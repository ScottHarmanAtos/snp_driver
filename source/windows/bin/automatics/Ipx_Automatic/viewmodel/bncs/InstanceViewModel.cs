﻿using driver_template.helpers;
using driver_template.model.bncs;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace driver_template.viewmodel
{
    public class InstanceViewModel<T> : INotifyPropertyChanged where T : Slot
    {
        private RelayCommand editParameterCommand;
        private RelayCommand applyValueChangesCommand;
        private ControlledInstances<T> instances;
        private bool isEditing;
        private InstanceItemViewModel selectedInstanceItemViewModel;

        public InstanceViewModel(ControlledInstances<T> controlledInstances)
        {
            instances = controlledInstances;
            instances.OnInstanceAdd += Instances_OnInstanceAdd;

            foreach (var v in instances.InstanceSlots)
            {
                foreach (var ps in v.Slots)
                {
                    InstanceItems.Add(new InstanceItemViewModel(v.Instance, ps.Param, ps.Index, ps.Slot, typeof(T) == typeof(Slot)));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<InstanceItemViewModel> InstanceItems { get; private set; } = new ObservableCollection<InstanceItemViewModel>();

        public ICommand EditParameterCommand
        {
            get
            {
                if (editParameterCommand == null)
                    editParameterCommand = new RelayCommand(param => EditParameter(param));

                return editParameterCommand;
            }
        }

        public ICommand ApplyValueChangesCommand
        {
            get
            {
                if (applyValueChangesCommand == null)
                    applyValueChangesCommand = new RelayCommand(param => ApplyValueChanges());

                return applyValueChangesCommand;
            }
        }

        public InstanceItemViewModel SelectedInstanceItemViewModel
        {
            get { return selectedInstanceItemViewModel; }
            set
            {
                if (selectedInstanceItemViewModel != value)
                {
                    selectedInstanceItemViewModel = value;
                    OnPropertyChanged();
                }
            }
        }

        public bool IsEditing
        {
            get { return isEditing; }
            private set
            {
                if (isEditing != value)
                {
                    isEditing = value;
                    OnPropertyChanged();
                }
            }
        }

        private void Instances_OnInstanceAdd(object sender, EventInstanceAdded arg)
        {
            var i = instances.InstanceSlots.Where(x => x.Instance.Id == arg.Instance.Id).First();

            foreach (var ps in i.Slots)
            {
                InstanceItems.Add(new InstanceItemViewModel(arg.Instance, ps.Param, ps.Index, ps.Slot, typeof(T) == typeof(Slot)));
            }
        }
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void EditParameter(object parameter)
        {
            IsEditing = bool.Parse(parameter.ToString());

            if (!IsEditing)
                foreach (var instanceItem in InstanceItems)
                    instanceItem.TempValue = instanceItem.Value;
        }

        private void ApplyValueChanges()
        {
            foreach (var instanceItem in InstanceItems)
                instanceItem.Value = instanceItem.TempValue;
        }
    }
}
