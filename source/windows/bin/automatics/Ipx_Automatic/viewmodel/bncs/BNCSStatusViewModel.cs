﻿using driver_template.helpers;
using NLog;
using System;
using System.ComponentModel;
using driver_template.model.bncs;

namespace driver_template.viewmodel
{

    public class BNCSStatusViewModel : INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private BNCSStatusModel Model;

        public BNCSStatusViewModel(BNCSStatusModel model)
        {
            Model = model;
            Model.RXCountChanged += (sender, arg) => { OnPropertyChanged("RXCount"); };
            Model.TXCountChanged += (sender, arg) => { OnPropertyChanged("TXCount"); };
            Model.CommsStateChanged += (sender, arg) => { OnPropertyChanged("CommsStatus"); CommsStateChanged?.Invoke(this, new EventArgs()); };
        }

        public long TXCount => Model.TXCount;
        public long RXCount => Model.RXCount;

        public BncsState CommsStatus { get { return Model.CommsStatus; } }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void OkToBeInControl(bool InControl)
        {
            Model.OkToBeInControl(InControl);
        }

        public void ForceTxRx() => Model.ForceTxRx();

        public void ForceRxOnly() => Model.ForceRxOnly();

        public event EventHandler CommsStateChanged;
    }
}
