﻿using driver_template.helpers;
using driver_template.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace driver_template.viewmodel
{
    public class RouterViewModel : IDisposable, INotifyPropertyChanged
    {

        private IPXVirtualRouter model;
        private ObservableCollection<IPXVirtualSource> sources;
        private ObservableCollection<IPXVirtualDestination> destinations;

        public RouterViewModel(IPXVirtualRouter model)
        {

            this.model = model;

            Sources = new ObservableCollection<IPXVirtualSource>(model.Sources.Values);
            Destinations = new ObservableCollection<IPXVirtualDestination>(model.Destinations.Values);

            model.LoadingFinished += Model_LoadingFinished;

        }

        private void Model_LoadingFinished(object sender, bool e)
        {
            Sources = new ObservableCollection<IPXVirtualSource>(model.Sources.Values);
            OnPropertyChanged("Sources");
            Destinations = new ObservableCollection<IPXVirtualDestination>(model.Destinations.Values);
            OnPropertyChanged("Destinations");
            Loading = false;
        }

        private bool loading = true;
        public bool Loading
        {
            get { return loading; }
            set
            {
                if (loading != value)
                {
                    loading = value;
                    OnPropertyChanged("Loading");
                }
            }
        }

        public ObservableCollection<IPXVirtualSource> Sources
        {
            get
            {
                return sources;
            }
            set
            {
                if (sources == value)
                {
                    return;
                }

                sources = value;
            }
        }

        public ObservableCollection<IPXVirtualDestination> Destinations
        {
            get
            {
                return destinations;
            }
            set
            {
                if (destinations == value)
                {
                    return;
                }

                destinations = value;
            }
        }

        IPXVirtualSource selectedSource;
        public IPXVirtualSource SelectedSource
        {
            get
            {
                return selectedSource;
            }
            set
            {
                if (selectedSource == value)
                {
                    return;
                }

                selectedSource = value;
                OnPropertyChanged(nameof(SelectedSource));
            }
        }

        IPXVirtualDestination selectedDestination;
        public IPXVirtualDestination SelectedDestination
        {
            get
            {
                return selectedDestination;
            }
            set
            {
                if (selectedDestination == value)
                {
                    return;
                }

                selectedDestination = value;
                OnPropertyChanged(nameof(SelectedDestination));
            }
        }

        public ICommand Take
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    if (SelectedSource != null && SelectedDestination != null)
                    {
                        SelectedDestination.TakeSource(SelectedSource);
                    }
                });
                return command;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void Dispose()
        {
        }

    }
}
