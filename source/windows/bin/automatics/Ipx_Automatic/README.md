#     IPX Automatic  #
## Overview ##
This automatic is designed to implement BNCS Router Combiner functionality as required for the Discovery/EuroSport Digital Channel Playout project described in document WP001.  

## 1. Prerequisites

The driver is a .Net application and requires Windows 10 or newer and .Net Framework 4.8 installed.

## 2.  Instance Configuration ##

The Automatic is designed to operate with main & backup routers.  Following is a typical configuration from the instances file.

     	<instance composite="no" id="rtr_ipx_ldc_combiner" type="Router" location="TBA" ref="device=3401,offset=0" alt_id="IPX Combiner">
    		<setting id="main_router" value="rtr_ipx_ldc_paris_colo" />
    		<setting id="backup_router" value="rtr_ipx_ldc_london_colo" />
    		<setting id="failure_delay" value="4000" />
    		<setting id="sync_delay" value="1000" />
    	</instance>
    	
    	<instance composite="no" id="rtr_ipx_ldc_paris_colo" type="Router" location="TBA" ref="device=3403,offset=0" alt_id="IPX Router Paris Colo" />
    	<instance composite="no" id="rtr_ipx_ldc_london_colo" type="Router" location="TBA" ref="device=3405,offset=0" alt_id="IPX Rouuter London Colo" />

The following user settings are provided:

Parameter | Description
-|-
main_router|This must match the Device ID of the main router within the system 
backup_router|This must match the Device ID of the backup router within the system 
failure_delay|This is the delay (in ms) that the automatic will wait after sending route requests for both routers to respond and a valid source to be confirmed.  If this delay is exceeded, a mis-route tally (-1) will be set.  In practice, unless neither of the routers respond, it is more likely that the sync_delay will determine the time before a mis-route is detected. 
sync_delay|This is the delay (in ms) that the automatic will wait after receiving a revertive from one router driver that creates a mis-route condition before the mis-route tally is set.  This is to account for the asynchronous nature of the two required revertives in a real system.

## 3.  Database Usage ##

The automatic makes use of the following instance database files:

DB_ID|Description
-|-
0|Standard BNCS router database source button mapping 
1|Standard BNCS router database destination button mapping 
4|Level mapping database.  Each row having the form  **0001=main_hp=1,main_lp=57,backup_hp=1,backup_lp=57** specifying the physical source index of the HP & LP levels for each source on both the main & backup routers
5 & 7|Mapping of the destination indexes to physical indexes on both the main (db 5) and backup (db7) routers
15 & 17|Required source type for each destination in main (db 15) and backup (db 17) routers.  Allowed values are either HP or LP

All databases are monitored for dynamic changes via RM commands.  A change in configuration that results in a mis-route condition will be indicated by a -1 tally.  The automatic will NOT re-assert invalid routes in the event of a configuration change. 

## 4.  InfoDriver Slot Usage ##

Infodriver slots relating to the IPX Combiner Automatic will have the following general form:
 
*index=\<source_index>,buindex=\<backup_index>,status=\<status_enum>*
 
Field | Meaning
-|-
**source_index**|the source routed on the main (Paris) IPX router OR -1 where the main router source is invalid (not a known source or known source of the wrong type)
**backup_index**|the source routed on the backup (London) IPX router OR -1 where the backup router source is invalid (not a known source or known source of the wrong type)
**status_enum**|1 or 2 where 1 indicates that the backup (London) source matches that expected given the current main source and 2 indicates that it does not match
 
BNCS Panels will display a source tally based on the source_index but will indicate an error state when the status_enum=2
 
GV API will return either:
a)	Tally equal to the source_index when the status_enum=1
b)	Tally equal to -1 when the status_enum=2
 
Route requests will be initiated by writing *index=\<source_index>* to the appropriate infodriver slot


## 5.  Version Control ##
Version|Date|Author|Comments
-|-|-|-
1.0|02/06/2020|Kenny Munro|Original
1.1|03/06/2020|Kenny Munro|Revised requirements for InfoDriver slot contents
1.2|04/06/2020|Kenny Munro|Added *buindex* to InfoDriver specification