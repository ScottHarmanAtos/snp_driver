﻿using driver_template.helpers;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using driver_template.model.bncs;
using driver_template.model.interfaces;

namespace driver_template.model
{
    //Driver Template Version:0.0.14.1

    public sealed class MainModel : IDisposable
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private IPXVirtualRouter router;

        public MainModel()
        {
            //Here is an example of a place you the program may differ if you are running in simulation
            if (BNCSConfig.StartupArguments.Simulation)
            {
                //DO Simulation stuff
            }
            else
            {
                //DO Normal stuff

                //Use the config loaded to create the DeviceModels and add these to the Devices list.

            }

            //create instance of the IPXVirtualRouter
            router = new IPXVirtualRouter();

            /* Example of how to register client slots (so that they appear in the grid view)
			 *
			 *
			 *
            foreach (var instance in BNCSConfig.ClientInstances.Instances.Values)
            {
                logger.Info($"Found instance {instance.Id}");

                foreach (var param in instance.DeviceType.Parameters)
                {
                    // Get the slot by Instance and Parameter
                    var (success, slot) = BNCSConfig.ClientInstances.GetSlot(instance, param);

                    if (success)
                        logger.Info($"Found slot {slot.Index}.");
                    else
                        continue;

                    //slot.OnChange += (sender, e) => { logger.Warn($"Slot {slot.Index} changed to {slot.Value}."); };
                }
            }

			-------------------

            Example of how to get Slots
             * 
             * 
             * Get an index to send a RC to.
                //Get Instance
                if (instances_and_devicetypes.Instance.TryGetInstance("rtr_ip_facility", out var i))
                {
                    //Get a clientInstance slot by index
                    var (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(i, 1, 1);
                    if (Success)
                    {
                        //Cast the slot to a SlotRouter <- as we know it is one. You can use Type to check
                        var r = Slots[0].Slot as SlotRouter;

                        //Set the Source to 2
                        r.Set(2);

                        //Set a callback to get notifed when it changes
                        Slots[0].Slot.OnChange += Slot_OnChange;
                    }
                }


                //The callback
                private void Slot_OnChange(object sender, EventSlotChange e)
                {
                    logger.Error($"RR {e.Value}");
                }

                -------------------

                Get a database to send an RM to

                Get the Instance
                if (instances_and_devicetypes.Instance.TryGetInstance("rtr_sdi_fcut_x", out var i2))
                {
                    //Get the Database by index
                    var (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(i2, 1, 1, 0);
                    if (SuccessDb)
                    {
                        //Send the RM
                        Database[0].DatabaseSlot.Set("lkjsflasjdf");

                        //Set a callback to get notifed when it changes.
                        Database[0].DatabaseSlot.OnChange += DatabaseSlot_OnChange;
                    }
                }

                //Database callback come here        
                private void DatabaseSlot_OnChange(object sender, EventSlotChange e)
                {
                    logger.Error($"RM {e.Value}");
                }

                --------------------------------
                Set a Value in a managed Slot

                //Get the parameter for an instance
                BNCSConfig.MainInstance.DeviceType.TryGetParameter("Comms", out var p);

                // Get the slot by Instance and Parameter
                var (SuccessManaged, SlotsManaged) = BNCSConfig.ManagedInstances.GetSlot(BNCSConfig.MainInstance, p.Parameter);
                if (SuccessManaged)
                {
                    //Set the Slot
                    SlotsManaged.Set("lkjsflasjdf");

                    //Register for IW to the slot
                    SlotsManaged.OnChange += (s, e) => { Slot_OnChange1(s, e, p); };
                }

                //IW to the slot come here
                private void Slot_OnChange1(object sender, EventSlotChange e)
                {
                    logger.Error($"Managed: {e.Value}");

                    //This sets the slot on the callback, so it acts like an echo slot
                    e.Slot.Set(e.Value);
                }

                ----------------------------------

                //Get all the slot for an Instance

                var (SuccessManagedAll, SlotsManagedAll) = BNCSConfig.ManagedInstances.GetSlots(BNCSConfig.MainInstance);
                if (SuccessManagedAll)
                {
                    //Register for all of them
                    foreach (var param in SlotsManagedAll)
                    {
                        //Get the ParameterValue for each, this hold info of the instance and parameter
                        BNCSConfig.MainInstance.TryGetParameterValue(param.Param, out var pv);

                        //Add a closure to the callback so, you get the additional instance and parameter information in the callback
                        param.Slot.OnChange += (s, e) => { Slot_OnChange1(s, e, pv); };
                    }
                }

                //Callback with the additional ParameterValue info
                private void Slot_OnChange1(object sender, EventSlotChange e, ParameterValue pv)
                {
                    logger.Error($"Managed: {pv.Instance.Id}:{pv.Parameter.Name} = {e.Value}");
                    e.Slot.Set(e.Value);
                }

             */
        }

        public IPXVirtualRouter Router
        {
            get
            {
                return router;
            }
        }

        public void Dispose()
        {
            router.Dispose();
        }

    }
}
