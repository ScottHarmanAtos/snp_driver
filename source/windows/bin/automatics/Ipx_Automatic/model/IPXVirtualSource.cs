﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace driver_template.model
{
    public class IPXVirtualSource : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly uint index;

        public IPXVirtualSource(uint index)
        {
            this.index = index;
        }

        public uint Index { get => index; }

        string buttonName;
        public string ButtonName
        {
            get
            {
                return buttonName;
            }
            set
            {
                if (buttonName == value)
                {
                    return;
                }

                buttonName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ButtonName)));
            }
        }

        string levels;
        public string Levels
        {
            get
            {
                return levels;
            }
            set
            {
                if (levels == value)
                {
                    return;
                }

                levels = value;

                //decode the mapping levels
                Regex regexObj = new Regex(@"(main_hp=(?<main_hp>\d*)),(main_lp=(?<main_lp>\d*)),(backup_hp=(?<backup_hp>\d*)),(backup_lp=(?<backup_lp>\d*))", RegexOptions.ExplicitCapture);
                uint.TryParse(regexObj.Match(value).Groups["main_hp"].Value, out var intMainHP); 
                uint.TryParse(regexObj.Match(value).Groups["main_lp"].Value, out var intMainLP);
                uint.TryParse(regexObj.Match(value).Groups["backup_hp"].Value, out var intBackupHP);
                uint.TryParse(regexObj.Match(value).Groups["backup_lp"].Value, out var intBackupLP);

                IndexHpMain = intMainHP;
                IndexHpBackup = intBackupHP;
                IndexLpMain = intMainLP;
                IndexLpBackup = intBackupLP;

            }
        }

        uint indexHpMain;
        public uint IndexHpMain
        {
            get
            {
                return indexHpMain;
            }
            private set
            {
                if (indexHpMain == value)
                {
                    return;
                }

                indexHpMain = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IndexHpMain)));
            }
        }

        uint indexHpBackup;
        public uint IndexHpBackup
        {
            get
            {
                return indexHpBackup;
            }
            private set
            {
                if (indexHpBackup == value)
                {
                    return;
                }

                indexHpBackup = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IndexHpBackup)));
            }
        }

        uint indexLpMain;
        public uint IndexLpMain
        {
            get
            {
                return indexLpMain;
            }
            private set
            {
                if (indexLpMain == value)
                {
                    return;
                }

                indexLpMain = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IndexLpMain)));
            }
        }

        uint indexLpBackup;
        public uint IndexLpBackup
        {
            get
            {
                return indexLpBackup;
            }
            private set
            {
                if (indexLpBackup == value)
                {
                    return;
                }

                indexLpBackup = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IndexLpBackup)));
            }
        }

    }
}
