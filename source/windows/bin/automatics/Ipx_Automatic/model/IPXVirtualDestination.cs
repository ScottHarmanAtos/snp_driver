﻿using driver_template.model.bncs;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace driver_template.model
{
    public class IPXVirtualDestination : IDisposable, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        static int intRouteFailDelayMs = 5000;
        public static int IntRouteFailDelayMs
        {
            get
            {
                return intRouteFailDelayMs;
            }
            set
            {
                if (intRouteFailDelayMs == value)
                {
                    return;
                }

                intRouteFailDelayMs = value;
                logger.Info($"IntRouteFailDelayMs = {value}");
            }
        }

        static int intRouteSyncDelayMs = 2000;
        public static int IntRouteSyncDelayMs
        {
            get
            {
                return intRouteSyncDelayMs;
            }
            set
            {
                if (intRouteSyncDelayMs == value)
                {
                    return;
                }

                intRouteSyncDelayMs = value;
                logger.Info($"IntRouteSyncDelayMs = {value}");
            }
        }

        public enum DestinationTypes
        {
            Undefined = 0,
            HP = 1,
            LP = 2
        }

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly uint index;
        private long changeNumber = 0;
        private Slot slot;

        int sourceIndex = 0;
        public int SourceIndex
        {
            get
            {
                return sourceIndex;
            }
            private set
            {
                if (sourceIndex == value)
                {
                    return;
                }

                sourceIndex = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SourceIndex)));
            }
        }

        int backupIndex;
        public int BackupIndex
        {
            get
            {
                return backupIndex;
            }
            set
            {
                if (backupIndex == value)
                {
                    return;
                }

                backupIndex = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(BackupIndex)));
            }
        }

        string sourceName;
        public string SourceName
        {
            get
            {
                return sourceName;
            }
            private set
            {
                if (sourceName == value)
                {
                    return;
                }

                sourceName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SourceName)));
            }
        }

        int tallyIndex = 0;
        public int TallyIndex
        {
            get
            {
                return tallyIndex;
            }
            private set
            {
                if (tallyIndex == value)
                {
                    return;
                }

                tallyIndex = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TallyIndex)));
            }
        }

        bool tallyMatch;
        public bool TallyMatch
        {
            get
            {
                return tallyMatch;
            }
            set
            {
                if (tallyMatch == value)
                {
                    return;
                }

                tallyMatch = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TallyMatch)));
            }
        }

        uint sourceMain;
        public uint SourceMain
        {
            get
            {
                return sourceMain;
            }
            set
            {
                if (sourceMain == value)
                {
                    return;
                }

                sourceMain = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SourceMain)));
            }
        }

        uint sourceBackup;
        public uint SourceBackup
        {
            get
            {
                return sourceBackup;
            }
            set
            {
                if (sourceBackup == value)
                {
                    return;
                }

                sourceBackup = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SourceBackup)));
            }
        }

        public IPXVirtualDestination(uint index)
        {
            this.index = index;

            //setup the infodriver slot
            var result = BNCSConfig.ManagedInstances.GetSlots(BNCSConfig.MainInstance, index);
            if (result.Success)
            {
                slot = result.Slots.First().Slot;
                slot.OnChange += Slot_OnChange;
            }

        }

        private void Slot_OnChange(object sender, EventSlotChange e)
        {

            try
            {
                //slot has been updated - parse it
                var strIndex = Regex.Match(e.Value, @"(?i)(index=(?<index>\d*))", RegexOptions.ExplicitCapture).Groups["index"].Value;

                if (int.TryParse(strIndex, out var intIndex))
                {

                    //slot contents resolve to an integer
                    if (TallyIndex == intIndex && TallyMatch)  //if the requested source matches current source and the Tallies Match, don't reassert
                    {
                        logger.Warn($"DEST: {Index} - Ignoring source reassert {e.Value}");
                        return; //value is already matching so dont do anything else
                    }

                    if (intIndex < 0) return; //can't set the slot to a negative value

                    logger.Info($"DEST: {Index} - Route requested SRC: {e.Value}");

                    var source = LookupSource((uint)intIndex);

                    if (source == null)
                    {
                        logger.Warn($"DEST: {Index} - No source found at index {e.Value}");
                    }
                    else
                    {
                        TakeSource(source);
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"DEST: {Index} - Unexpected error in Slot_OnChange");
            }

        }

        public void TakeSource(IPXVirtualSource source)
        {

            if (source==null) 
            {
                //park everything & return
                logger.Warn($"DEST: {Index} - Cannot route null source");
                return;
            }
            else
            {

                changeNumber += 1;
                SourceIndex = (int)source.Index;

                logger.Info($"DEST: {Index} - Routing source {source.Index}:{source.ButtonName}");

                //make the 
                switch (MainType)
                {
                    case DestinationTypes.Undefined:
                        break;
                    case DestinationTypes.HP:
                        if (source.IndexHpMain > 0)
                        {
                            logger.Info($"DEST: {Index} - {source.Index}:{source.ButtonName} taking HP Variant on MAIN IDX: {source.IndexHpMain}");
                            PhysicalDestinationMain.SetSource(source.IndexHpMain);
                        }
                        else logger.Warn($"DEST: {Index} - {source.Index}:{source.ButtonName} cannot take source on MAIN - HP index is 0");
                        break;
                    case DestinationTypes.LP:
                        if (source.IndexLpMain > 0)
                        {
                            logger.Info($"DEST: {Index} - {source.Index}:{source.ButtonName} taking LP Variant on MAIN IDX: {source.IndexLpMain}");
                            PhysicalDestinationMain.SetSource(source.IndexLpMain);
                        }
                        else logger.Warn($"DEST: {Index} - {source.Index}:{source.ButtonName} cannot take source on MAIN - LP index is 0");
                        break;
                    default:
                        break;
                }

                switch (BackupType)
                {
                    case DestinationTypes.Undefined:
                        break;
                    case DestinationTypes.HP:
                        if (source.IndexHpBackup > 0)
                        {
                            logger.Info($"DEST: {Index} - {source.Index}:{source.ButtonName} taking HP Variant on BACKUP IDX: {source.IndexHpBackup}");
                            PhysicalDestinationBackup.SetSource(source.IndexHpBackup);
                        }
                        else logger.Warn($"DEST: {Index} - {source.Index}:{source.ButtonName} cannot take source on BACKUP - HP index is 0");
                        break;
                    case DestinationTypes.LP:
                        if (source.IndexLpBackup > 0)
                        {
                            logger.Info($"DEST: {Index} - {source.Index}:{source.ButtonName} taking LP Variant on BACKUP IDX: {source.IndexLpBackup}");
                            PhysicalDestinationBackup.SetSource(source.IndexLpBackup);
                        }
                        else logger.Warn($"DEST: {Index} - {source.Index}:{source.ButtonName} cannot take source on BACKUP - LP index is 0");
                        break;
                    default:
                        break;
                }

                //start a source check timer
                Task.Run(async () =>
                {

                    long myChange = changeNumber;

                    await Task.Delay(IntRouteFailDelayMs);

                    //check that there hasn't been a subsequent change - if there has then we should no longer do our checks
                    if (changeNumber != myChange) return;

                    //By this stage the sourceIndex & tallyIndex should match, if not we have a misroute
                    if (SourceIndex != TallyIndex)
                    {
                        //recheck the source - this will also check for a partial match
                        IsSourceValid();
                    }

                });

            }
        }

        public uint Index { get => index; }

        string buttonName;
        public string ButtonName
        {
            get
            {
                return buttonName;
            }
            set
            {
                if (buttonName == value)
                {
                    return;
                }

                buttonName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ButtonName)));
            }
        }

        DestinationTypes mainType;
        public DestinationTypes MainType
        {
            get
            {
                return mainType;
            }
            set
            {
                if (mainType == value)
                {
                    return;
                }

                mainType = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MainType)));
            }
        }

        DestinationTypes backupType;
        public DestinationTypes BackupType
        {
            get
            {
                return backupType;
            }
            set
            {
                if (backupType == value)
                {
                    return;
                }

                backupType = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(BackupType)));
            }
        }

        IPXPhysicalDestination physicalDestinationMain;
        public IPXPhysicalDestination PhysicalDestinationMain
        {
            get
            {
                return physicalDestinationMain;
            }
            set
            {
                physicalDestinationMain = value;
                physicalDestinationMain.OnChange += PhysicalDestination_OnChange;
                physicalDestinationMain.InitSlot();
            }
        }

        IPXPhysicalDestination physicalDestinationBackup;

        public IPXPhysicalDestination PhysicalDestinationBackup
        {
            get
            {
                return physicalDestinationBackup;
            }
            set
            {
                physicalDestinationBackup = value;
                physicalDestinationBackup.OnChange += PhysicalDestination_OnChange;
                physicalDestinationBackup.InitSlot();
            }
        }

        private void PhysicalDestination_OnChange(object sender, uint tally)
        {

            try
            {

                if (sender == PhysicalDestinationMain) SourceMain = tally;
                if (sender == PhysicalDestinationBackup) SourceBackup = tally;

                //check that both the Main & Backup destinations exist to prevent null errors on startup
                if (PhysicalDestinationMain == null || PhysicalDestinationBackup == null || ResolveSource == null) return;

                //one of the physical destinations has reported a change - we need to see if the endpoints now resolve to a source or not
                var source = ResolveSource(PhysicalDestinationMain.Tally, MainType, PhysicalDestinationBackup.Tally, BackupType);

                //if source resolves then we need to set the tally to that source - otherwise trigger a sync wait
                if (source != null)
                {
                    logger.Info($"DEST: {Index} - {source.Index}:{source.ButtonName} Source route completed");
                    SourceIndex = (int)source.Index;
                    BackupIndex = SourceIndex;
                    TallyIndex = (int)source.Index;
                    TallyMatch = true;
                    SourceName = source.ButtonName;
                    SetSlotTally();
                }
                else
                {
                    //schedule a sync delay
                    Task.Run(async () =>
                    {

                        long myChange = changeNumber;

                        await Task.Delay(IntRouteSyncDelayMs);

                        //check that there hasn't been a subsequent change - if there has then we should no longer do our checks
                        if (changeNumber != myChange) return;

                        //recheck the source - this will also check for a partial match
                        IsSourceValid();

                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unexpected error in PhysicalDestination_OnChange");
            }

        }

        public bool IsSourceValid()
        {

            if (PhysicalDestinationMain == null || PhysicalDestinationBackup == null || ResolveSource == null) return false;

            SourceMain = PhysicalDestinationMain.Tally;
            SourceBackup = PhysicalDestinationBackup.Tally;

            //one of the physical destinations has reported a change - we need to see if the endpoints now resolve to a source or not
            var source = ResolveSource(PhysicalDestinationMain.Tally, MainType, PhysicalDestinationBackup.Tally, BackupType);
            var mainSource = ResolveSource(PhysicalDestinationMain.Tally, MainType, 0, DestinationTypes.Undefined);
            var backupSource = ResolveSource(0, DestinationTypes.Undefined, PhysicalDestinationBackup.Tally, BackupType);

            //if source resolves then we need to set the tally to that source - otherwise trigger a sync wait
            if (source != null)
            {
                //source matches both MAIN & BACKUP routers
                SourceIndex = (int)source.Index;
                BackupIndex = SourceIndex;
                TallyIndex = (int)source.Index;
                TallyMatch = true;
                SourceName = source.ButtonName;
                SetSlotTally();
                return true;
            }
            else if (mainSource != null)
            {
                SourceIndex = (int)mainSource.Index;
                TallyIndex = (int)mainSource.Index;
                TallyMatch = false;
                SourceName = mainSource.ButtonName;

                if (backupSource == null) BackupIndex = -1;
                else BackupIndex = (int)backupSource.Index;

                SetSlotTally();
                return false;
            }
            else
            {
                SourceIndex = -2;
                TallyIndex = -1;
                TallyMatch = false;
                SourceName = "Error";

                if (backupSource == null) BackupIndex = -1;
                else BackupIndex = (int)backupSource.Index;

                SetSlotTally();
                return false;
            }

        }
       
        public void SetSlotTally()
        {
            int status = 1;
            if (!TallyMatch) status = 2;
            slot.Set($"index={TallyIndex},buindex={BackupIndex},status={status}");
        }

        public void Dispose()
        {
            slot?.Dispose();
            PhysicalDestinationMain?.Dispose();
            PhysicalDestinationBackup?.Dispose();
        }

        //function template - call with the index of a source and returns either the source or null
        public Func<uint, IPXVirtualSource> LookupSource { get; set; }

        //function template to resolve a source based on tallys - structure is 
        // public IPXVirtualSource function(uint mainTally, DestinationTypes mainType, uint backupTally, DestinationTypes backupType)
        public Func<uint, DestinationTypes, uint, DestinationTypes, IPXVirtualSource> ResolveSource { get; set; }

    }

}
