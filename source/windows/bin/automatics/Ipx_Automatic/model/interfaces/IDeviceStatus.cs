﻿using driver_template.helpers;
using System;

namespace driver_template.model.interfaces
{
    public interface IDeviceStatus
    {
        DeviceState CommsStatus { get; }
        event EventHandler CommsStatusChange;
        event EventHandler TXIncrement;
        event EventHandler RXIncrement;
    }
}
