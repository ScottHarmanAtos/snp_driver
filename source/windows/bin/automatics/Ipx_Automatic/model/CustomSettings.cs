﻿using driver_template.model.bncs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.model
{
    /// <summary>
    /// Settings holds settings that are needed for the driver.
    /// The settings are stored in drivers_config.xml
    /// Add any setting that is required by the program that does not make sense to add to instances
    /// 
    /// The class is updated from the DriverSettings Class
    /// </summary>
    public class CustomSettings : Settings
    {
    }
}
