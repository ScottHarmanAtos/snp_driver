﻿using System;

namespace driver_template.model.bncs
{
    public class SlotRouter : SlotClient
    {
        public static (SlotRouter Slot, Action<string> Update) Create(uint device, uint index, int initalValue, Action<int> set, Action updateTxCount, Action updateRxCount, Action poll)
        {
            var r = new SlotRouter(device, index, initalValue.ToString(),
                (s) =>
                {
                    if (Int32.TryParse(s, out var i))
                    {
                        set(int.Parse(s));
                    }
                    else
                    {
                        logger.Error($"RouterSlot: Message not sent as it is not an integer: {s}");
                    }

                }, updateTxCount, updateRxCount, poll);
            return (r, (i) => { r.Changed(i); });
        }

        public void Set(int source, bool ifChanged = true, bool useNagle = false, int nagleTimeMs = 10, bool nagleSendFirstStraightAway = true, bool ignoreTXRXState = false)
        {
            Set(source.ToString(), ifChanged, useNagle, nagleTimeMs, nagleSendFirstStraightAway, ignoreTXRXState);
        }

        protected SlotRouter(uint device, uint index, string initialValue, Action<string> set, Action updateTxCount, Action updateRxCount, Action poll) : base(device, index, initialValue, set, updateTxCount, updateRxCount, poll)
        {
            Type = SlotType.RouterClient;
        }

        private int _value;

        public new int Value
        {
            get { return _value; }
            private set
            {
                if (_value != value)
                {
                    _value = value;
                    base.Set(_value.ToString());
                }
            }
        }
    }
}
