﻿using instances_and_devicetypes;
using NLog;
using System;
using System.Linq;
using System.Collections.Generic;

namespace driver_template.model.bncs
{
    public abstract class ControlledInstances<T> where T : Slot
    {
        protected static Logger logger = LogManager.GetCurrentClassLogger();
        public Dictionary<String, Instance> Instances { get; private set; } = new Dictionary<string, Instance>();
        private Dictionary<(uint Device, uint Index), (T Slot, Action<String> Update)> Slots { get; set; } = new Dictionary<(uint Device, uint Index), (T Slot, Action<string> Update)>();
        private Dictionary<uint, Dictionary<String, (Instance Instance, List<(Parameter Param, uint Index, T Slot)> Slots)>> SlotAgainstInstance { get; set; } = new Dictionary<uint, Dictionary<String, (Instance Instance, List<(Parameter Param, uint Index, T Slot)> Slots)>>();

        public IEnumerable<(Instance Instance, List<(Parameter Param, uint Index, T Slot)> Slots)> InstanceSlots
        {
            get
            {
                foreach (var v in SlotAgainstInstance.Values)
                {
                    foreach (var w in v.Values)
                        yield return w;
                }
            }
        }

        protected abstract string Name { get; }
        protected virtual bool CanAddInstanceAtRuntime { get; } = false;

        /// <summary>
        /// Add a managed instance
        /// </summary>
        /// <param name="instance"></param>
        protected abstract bool _Add(Instance instance);

        public bool Add(Instance Instance)
        {
            //Check we don't already have it
            if (Instances.ContainsKey(Instance.Id))
                return true;

            var b = _Add(Instance);
            if (b)
            {
                OnInstanceAdd?.Invoke(this, new EventInstanceAdded(Instance));
            }
            return b;
        }

        public event EventHandler<EventInstanceAdded> OnInstanceAdd;

        private bool InstanceCheck(Instance instance)
        {
            if (instance == null)
                return false;

            //Check if we are managing the instance
            if (!Instances.ContainsKey(instance.Id))
            {
                if (CanAddInstanceAtRuntime)
                {
                    Instances.Add(instance.Id, instance);
                }
                else
                {
                    logger.Error($"Failed to get Instance:{instance.Id}, this is not a {Name} Instance.");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Get all the parameters for an instance
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
		public (bool Success, List<(Parameter Param, T Slot)> Slots) GetSlots(Instance instance)
        {
            //Check if we are managing the instance
            if (!InstanceCheck(instance))
            {
                return (false, null);
            }

            //Check that the DeviceType is Good
            if (!instance.DeviceType.IsGood)
            {
                //logger.Error($"Failed to Get Slots for Instance:{instance.Id}, the DeviceType:{instance.DeviceType} is not valid.");
                return (false, null);
            }

            var l = new List<(Parameter, T)>();

            //Get all the slots
            foreach (var p in instance.DeviceType.Parameters.Where(x => x.Slot > 0))
            {
                var r = GetSlotNoChecks(instance, p);
                if (r.Success == false)
                {
                    logger.Error($"Failed to Get Slots for Instance:{instance.Id}, the DeviceType:{instance.DeviceType}, Param:{p.Name} caused an error.");
                    return (false, null);
                }
                l.Add((p, r.Slot));
            }
            return (true, l);
        }

        /// <summary>
        /// Get a single param from an instance
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public (bool Success, T Slot) GetSlot(Instance instance, Parameter parameter)
        {
            //Check if we are managing the instance
            if (!InstanceCheck(instance))
            {
                return (false, null);
            }

            //Check param has had a slot allocated
            if (parameter.Slot == 0)
            {
                logger.Error($"Failed to get Instance:{instance.Id}, Param:{parameter.Name}, a param with slot 0 cannot be controlled.");
                return (false, null);
            }

            return GetSlotNoChecks(instance, parameter);
        }

        /// <summary>
        /// Get a range of Indexes for this Instance
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public (bool Success, List<(uint Index, T Slot)> Slots) GetSlots(Instance instance, uint startIndex, uint length = 1)
        {
            //Check if we are managing the instance
            if (!InstanceCheck(instance))
            {
                return (false, null);
            }

            //Check if the DeviceType is Good
            if (startIndex == 0)
            {
                logger.Error($"Failed to Get Slots for Instance:{instance.Id}, StartIndex cannot be 0.");
            }
            if (length == 0)
            {
                logger.Error($"Failed to Get Slots for Instance:{instance.Id}, Length cannot be 0.");
            }

            var l = new List<(uint, T)>();

            //Get all the slots
            for (uint i = startIndex; i < startIndex + length; ++i)
            {
                var r = GetSlotNoChecks(instance, i);
                if (r.Success == false)
                {
                    logger.Error($"Failed to Get Slots for Instance:{instance.Id}, Slot:{i} caused an error.");
                    return (false, null);
                }
                l.Add((i, r.Slot));
            }
            return (true, l);
        }

        /// <summary>
        /// GetSlot No Checks get the slot without doing check on Instance and Param supplied, checks should be done before this is called.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private (bool Success, T Slot) GetSlotNoChecks(Instance instance, Parameter parameter)
        {
            if (parameter == null)
                return (false, null);
            return GetSlotNoChecks(instance, (uint)(parameter.Slot), parameter);
        }

        /// <summary>
        /// GetSlot No Checks get the index without doing check on Instance supplied, checks should be done before this is called.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private (bool Success, T Slot) GetSlotNoChecks(Instance instance, uint index, Parameter parameter = null)
        {
            uint IndexWithOffset = index + instance.Offset;
            //Check if we already have the slot loaded.
            if (Slots.TryGetValue(((uint)instance.Device, IndexWithOffset), out var s))
                return (true, s.Slot);

            var (Success, SlotAndUpdate) = LoadSlot(instance, index, parameter);
            if (Success)
            {
                Slots.Add((instance.Device, IndexWithOffset), SlotAndUpdate);

                //Store the Slots against the Instance, this will make it easier to use the data later
                if (!SlotAgainstInstance.TryGetValue(instance.Device, out var iSs))
                {
                    iSs = new Dictionary<String, (Instance Instance, List<(Parameter Param, uint Index, T Slot)> Slots)>();
                    SlotAgainstInstance.Add(instance.Device, iSs);
                }
                if (!iSs.TryGetValue(instance.Id, out var lParams))
                {
                    lParams = (instance, new List<(Parameter Param, uint Index, T Slot)>());
                    iSs.Add(instance.Id, lParams);

                }
                lParams.Slots.Add((parameter, index, SlotAndUpdate.Slot));

                return (true, SlotAndUpdate.Slot);
            }
            return (false, null);
        }

        protected bool Update(uint device, uint index, string value)
        {
            if (Slots.TryGetValue((device, index), out var i))
            {
                i.Update(value);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Load the Slot
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="slot">The Device Slot, without the offset added</param>
        /// <returns></returns>
        protected abstract (bool Success, (T Slot, Action<String> Update) SlotAndUpdate) LoadSlot(Instance instance, uint slot, Parameter parameter);
    }

    public class EventInstanceAdded : EventArgs
    {
        public Instance Instance { get; }
        public EventInstanceAdded(Instance instance)
        {
            Instance = instance;
        }
    }
}