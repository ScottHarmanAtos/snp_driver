﻿using BncsClientLibrary;
using instances_and_devicetypes;
using System;
using System.Collections.Generic;

namespace driver_template.model.bncs
{
    /// <summary>
    /// This class hold all the config that has been loaded for the driver.
    /// It is a static class so can be called from anywhere
    /// </summary>
    public static class BNCSConfig
    {
        public static StartupArguments StartupArguments { get; set; }
        public static Instance MainInstance { get; private set; }
        public static BncsClient BncsClient { get; private set; }
        public static ManagedInstances ManagedInstances { get; private set; }
        public static ClientInstances ClientInstances { get; private set; }
        public static Settings Settings { get; set; }

        public static Databases Database { get; private set; }

        public static void Set(
            Instance mainInstance,
            BncsClient bncsClient,
            ManagedInstances managedInstances,
            ClientInstances clientInstances,
            Databases databases
            )
        {

            MainInstance = mainInstance;
            BncsClient = bncsClient;
            ManagedInstances = managedInstances;
            ClientInstances = clientInstances;
            Database = databases;
        }

        private static bool Disposing = false;

        public static void Dispose()
        {
            if (Disposing)
                return;
            Disposing = true;

            if (BncsClient != null)
            {
                if (BncsClient.isConnected())
                {
                    BncsClient.writeEnable(false);
                    BncsClient.disconnect();
                }
                BncsClient.Dispose();
            }
        }
    }
}
