﻿using driver_template;
using driver_template.helpers;
using NLog;
using System;
using System.Linq;

namespace driver_template.model.bncs
{
    /// <summary>
    /// This is a singleton class which holds bncs status.
    /// This class holds the status for the infodrivers the driver controls
    /// Status on command numbers sent to and from the driver to the BNCS network
    /// </summary>
    public class BNCSStatusModel
    {
        private static Lazy<BNCSStatusModel> instance = new Lazy<BNCSStatusModel>(() => new BNCSStatusModel());
        public static BNCSStatusModel Instance
        {
            get { return instance.Value; }
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        private BNCSStatusModel()
        {
            Infodrivers.All.ToList().ForEach(
                x =>
                {
                    x.Value.DisconnectedEvent += Value_DisconnectedEvent;
                }
                );
            Infodrivers.InfodriverStatusChanged += Infodrivers_InfodriverStatusChanged;
            Infodrivers.InfodriverAdded += Infodrivers_InfodriverAdded;

            CommsStatus = Infodrivers.Status;
        }

        private void Infodrivers_InfodriverAdded(object sender, EventInfodriverAdded state)
        {
            state.Infodriver.DisconnectedEvent += Value_DisconnectedEvent;
        }

        private void Value_DisconnectedEvent(uint device)
        {
            App.CriticalError($"Lost connection to an infodriver:{device}", "Lost Infodriver Connection", logger, ErrorCode.InfodriverClosed);
        }

        private void Infodrivers_InfodriverStatusChanged(object sender, EventInfodriverStatusChange status)
        {
            logger.Info("Status Changed: {0}", status.State.ToString());
            CommsStatus = status.State;

            //If Changed to TXRX resend values.
            if (status.State == BncsState.TXRX)
            {
                foreach (var i in BNCSConfig.ManagedInstances.InstanceSlots)
                {
                    foreach (var s in i.Slots)
                    {
                        //Send again
                        s.Slot.Set(s.Slot.Value, false);
                    }
                }
            }

            CommsStateChanged?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// This is incremented whenever command is sent to a managed infodriver or a database
        /// </summary>
        public void TXCountIncrement()
        {
            TXCount++;
            TXCountChanged?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// This is incrented whenever a RX is received in a managed infodriver or a database
        /// </summary>
        public void RXCountIncrement()
        {
            RXCount++;
            RXCountChanged?.Invoke(this, new EventArgs());
        }

        public long TXCount { get; private set; } = 0;

        public long RXCount { get; private set; } = 0;

        public event EventHandler TXCountChanged;
        public event EventHandler RXCountChanged;
        public event EventHandler CommsStateChanged;

        public BncsState CommsStatus { get; private set; } = BncsState.Error;

        public void OkToBeInControl(bool InControl)
        {
            if (InControl)
                Infodrivers.SetDriverState(BncsInfodriverLibrary.eDriverState.ok, true, false);
            else
                Infodrivers.SetDriverState(BncsInfodriverLibrary.eDriverState.fail, true, true); //Might be bad forcing this, there needs to be 3 states
        }

        public void ForceTxRx()
        {
            Infodrivers.ForceTXRX();
        }

        public void ForceRxOnly()
        {
            Infodrivers.ForceRxOnly();
        }

    }
}
