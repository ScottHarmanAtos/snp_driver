﻿using instances_and_devicetypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.model
{
    // NB: This class can be deleted if only default behaviour is required and no custom changes have been made
    public static class Helpers
    {
        /// <summary>
        /// The Method returns the MangedInstance and Client instances, to be loaded by the driver.
        /// The function is called once on startup. It can be edited to change which instances are loaded as managed instances are which instance are loaded as client instances.
        /// 
        /// Passed in are all the composites groups that exists in the original composite passed in.
        /// Also passed in is a flattened list of every instance in the composite with a group name as key. The group name is dot(.) delimited if the instance is muiltiple composites deep.
        ///
        /// It then returns a list of instances which are managed, the driver will connect to the infodrivers for these instances
        /// It also returns a list of client instances, these are instances the driver is interested in but will only connect as client too.
        /// 
        /// @@CHANGE@@
        /// This function should be changed to work with the instances you are expecting, or the class should be deleted completely if only default behaviour is required.
        /// Below is the default behaviour which will likely work in most cases.
        /// This loads in all the instances supplied in the composite/instance as managed instances.
        /// </summary>
        public static (Dictionary<string, Instance> ManagedInstances, Dictionary<string, Instance> ClientInstances) GetManagedAndClientInstances(List<(string Id, Instance Instance)> composites, List<(string Group, Instance Instance)> instances)
        {
            Dictionary<string, Instance> managedInstances = new Dictionary<string, Instance>();
            Dictionary<string, Instance> clientInstances = new Dictionary<string, Instance>();

            //At this point here we can figure out if the instances are to be managed or are just client instances.
            //The default is to allocate all instances as managed instances, the driver will attempt to connect to an infodriver for each device number provided.
            managedInstances = instances.ToDictionary(x => x.Instance.Id, y => y.Instance);


            //If a composite instance of c_packager is passed to the driver with the below config
            /*
             * 	<instance composite="yes" id="c_packager">
                    <group id="router" instance="packager_router" />
                    <group id="mm_stack" instance="packager_mm_stack" />
                    <group id="pti" instance="packager_pti" />
                    <group id="dest_comms" instance="packager_dest_comms" />
                    <group id="lock" instance="packager_lock" />
                    <group id="ifbpti" instance="packager_ifbpti" />
                    <group id="lineup" instance="packager_lineup" />
                    <group id="miscdata" instance="packager_misc_data" />
                    <group id="4wpti" instance="packager_4wtbpti" />
                    <group id="video_router" instance="rtr_ip_facility" />
                </instance>

                composites would be empty as these are not nested.
                instances would contain
                    - router, instance
                    - mm_stack, instance 
                    ect.
                
                So if pti and dest_comms are managed and the rest not you could do this

                //Add only pti and dest_comms to managed Instances
                managedInstances = instances.Where(x=>x.Group == "pti" || x.Group == "dest_comms").ToDictionary(x => x.Instance.Id, y => y.Instance);

                //Add all but pti and dest_comms to clientInstances
                clientInstances = instances.Where(x => x.Group != "pti" || x.Group != "dest_comms").ToDictionary(x => x.Instance.Id, y => y.Instance);;
             */

            return (managedInstances, clientInstances);
        }
    }
}
