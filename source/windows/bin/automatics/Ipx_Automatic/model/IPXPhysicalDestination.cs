﻿using driver_template.model.bncs;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Runtime.Remoting.Channels;
using System.Security.RightsManagement;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Interop;

namespace driver_template.model
{
    public class IPXPhysicalDestination : IDisposable
    {

        public delegate void OnChangeDelegate(object sender, uint tally);
        public event OnChangeDelegate OnChange;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private string routerDevice;
        private uint routerIndex;
        private uint tallySource;
        private SlotClient slot;

        public IPXPhysicalDestination(string routerDevice, uint routerIndex)
        {
            this.routerDevice = routerDevice;
            this.routerIndex = routerIndex;
        }

        public void InitSlot()
        {
            //setup the slot
            if (instances_and_devicetypes.Instance.TryGetInstance(routerDevice, out var i))
            {
                //Get a clientInstance slot by index
                var result = BNCSConfig.ClientInstances.GetSlots(i, routerIndex, 1);
                if (result.Success)
                {
                    slot = result.Slots.First().Slot;
                    slot.OnChange += Slot_OnChange;

                    //if slot is already populated (due to being recreated) this will cause the update event to fire
                    if (uint.TryParse(slot.Value, out var tally))
                    {
                        tallySource = tally;
                        OnChange?.Invoke(this, tally);
                    }

                }
            }
        }

        private void Slot_OnChange(object sender, EventSlotChange e)
        {

            if (uint.TryParse(e.Value, out var tally))
            {
                tallySource = tally;
                OnChange?.Invoke(this, tally);
            }

        }

        public void Dispose()
        {
            slot?.Dispose();
        }

        public void SetSource(uint source)
        {
            slot.Set(source.ToString());
        }

        public uint Tally { get => tallySource; }

    }
}
