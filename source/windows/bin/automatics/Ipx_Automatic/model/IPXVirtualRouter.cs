﻿using driver_template.model.bncs;
using IniParser;
using IniParser.Model;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel.Configuration;
using System.Threading.Tasks;

namespace driver_template.model
{
    public class IPXVirtualRouter : IDisposable
    {

        public event EventHandler<bool> LoadingFinished;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private FileIniDataParser iniParser;
        private Instance instance;
        private string mainRouterDevice;
        private string backupRouterDevice;

        private List<(uint Index, SlotDatabase DatabaseSlot)> database_0;
        private List<(uint Index, SlotDatabase DatabaseSlot)> database_1;
        private List<(uint Index, SlotDatabase DatabaseSlot)> database_4;
        private List<(uint Index, SlotDatabase DatabaseSlot)> database_5;
        private List<(uint Index, SlotDatabase DatabaseSlot)> database_7;
        private List<(uint Index, SlotDatabase DatabaseSlot)> database_15;
        private List<(uint Index, SlotDatabase DatabaseSlot)> database_17;

        private ConcurrentDictionary<uint, IPXVirtualSource> sources;
        private ConcurrentDictionary<uint, IPXVirtualDestination> destinations;

        public ConcurrentDictionary<uint, IPXVirtualSource> Sources
        {
            get
            {
                return sources;
            }
        }

        public ConcurrentDictionary<uint, IPXVirtualDestination> Destinations
        {
            get
            {
                return destinations;
            }
        }

        public IPXVirtualRouter()
        {

            iniParser = new FileIniDataParser();
            instance = BNCSConfig.MainInstance;

            sources = new ConcurrentDictionary<uint, IPXVirtualSource>();
            destinations = new ConcurrentDictionary<uint, IPXVirtualDestination>();

            BNCSConfig.MainInstance.TryGetSetting("main_router", out mainRouterDevice);
            BNCSConfig.MainInstance.TryGetSetting("backup_router", out backupRouterDevice);

            //set the sync & fail delay statics
            BNCSConfig.MainInstance.TryGetSetting("failure_delay", out var strIntRouteFailDelayMs);
            BNCSConfig.MainInstance.TryGetSetting("sync_delay", out var strIntRouteSyncDelayMs);
            if (int.TryParse(strIntRouteFailDelayMs, out var intRouteFailDelayMs)) IPXVirtualDestination.IntRouteFailDelayMs = intRouteFailDelayMs;
            if (int.TryParse(strIntRouteSyncDelayMs, out var intRouteSyncDelayMs)) IPXVirtualDestination.IntRouteSyncDelayMs = intRouteSyncDelayMs;
            
            Task.Run(async () =>
            {

                try
                {
                    await InitDb();
                    await InitSources();
                    await InitDestinations();
                    await SetupDatabaseCallbacks();

                    LoadingFinished?.Invoke(this, true);
                    logger.Info("Router init complete");
                }
                catch (Exception ex)
                {
                    logger.Error(ex, $"Unexpected error while creating router");
                }

            });

        }

        private async Task<bool> InitDb()
        {

            //load sources
            await Task.Run(() =>
            {
                var (SuccessDb, db) = BNCSConfig.Database.GetDatabases(BNCSConfig.MainInstance, 1, BNCSConfig.Database.MaxIndexDatabase, 0);
                if (SuccessDb)
                {
                    database_0 = db;
                    InitialiseDb(database_0, 0);
                }
            });

            //load destinations
            await Task.Run(() =>
            {
                var (SuccessDb, db) = BNCSConfig.Database.GetDatabases(BNCSConfig.MainInstance, 1, BNCSConfig.Database.MaxIndexDatabase, 1);
                if (SuccessDb)
                {
                    database_1 = db;
                    InitialiseDb(database_1, 1);
                }
            });

            await Task.Run(() =>
            {
                var (SuccessDb, db) = BNCSConfig.Database.GetDatabases(BNCSConfig.MainInstance, 1, BNCSConfig.Database.MaxIndexDatabase, 4);
                if (SuccessDb)
                {
                    database_4 = db;
                    InitialiseDb(database_4, 4);
                }
            });

            await Task.Run(() =>
            {
                var (SuccessDb, db) = BNCSConfig.Database.GetDatabases(BNCSConfig.MainInstance, 1, BNCSConfig.Database.MaxIndexDatabase, 5);
                if (SuccessDb)
                {
                    database_5 = db;
                    InitialiseDb(database_5, 5);
                }
            });

            await Task.Run(() =>
            {
                var (SuccessDb, db) = BNCSConfig.Database.GetDatabases(BNCSConfig.MainInstance, 1, BNCSConfig.Database.MaxIndexDatabase, 7);
                if (SuccessDb)
                {
                    database_7 = db;
                    InitialiseDb(database_7, 7);
                }
            });

            await Task.Run(() =>
            {
                var (SuccessDb, db) = BNCSConfig.Database.GetDatabases(BNCSConfig.MainInstance, 1, BNCSConfig.Database.MaxIndexDatabase, 15);
                if (SuccessDb)
                {
                    database_15 = db;
                    InitialiseDb(database_15, 15);
                }
            });

            await Task.Run(() =>
            {
                var (SuccessDb, db) = BNCSConfig.Database.GetDatabases(BNCSConfig.MainInstance, 1, BNCSConfig.Database.MaxIndexDatabase, 17);
                if (SuccessDb)
                {
                    database_17 = db;
                    InitialiseDb(database_17, 17);
                }
            });

            return true;

        }

        private async Task<bool> InitSources()
        {

            await Task.Run(() =>
            {

                foreach (var item in database_0)
                {
                    CreateSource(item);
                }

            });

            return true;

        }

        private async Task<bool> InitDestinations()
        {

            await Task.Run(() =>
            {

                foreach (var item in database_1)
                {
                    CreateDestination(item);
                }

            });

            return true;

        }

        private void CreateSource((uint Index, SlotDatabase DatabaseSlot) item)
        {
            try
            {
                if (item.DatabaseSlot.Value != null && !item.DatabaseSlot.Value.Equals("---"))
                {

                    var newSource = new IPXVirtualSource(item.Index);
                    sources.TryAdd(item.Index, newSource);
                    newSource.ButtonName = item.DatabaseSlot.Value;

                    var db4slot = (from a in database_4
                                   where a.Index == item.Index
                                   select a.DatabaseSlot).FirstOrDefault();

                    if (db4slot != null) newSource.Levels = db4slot.Value;

                    //setup slot change updates
                    item.DatabaseSlot.OnChange += SourceSlotOnChange;
                    if (db4slot != null) db4slot.OnChange += SourceSlotOnChange;

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"Error in CreateSource IDX: {item.Index}");
            }
        }

        private async Task<bool> SetupDatabaseCallbacks()
        {

            foreach (var slot in database_0) slot.DatabaseSlot.OnChange += SourceSlotOnChange;
            foreach (var slot in database_1) slot.DatabaseSlot.OnChange += DestinationSlotOnChange;
            foreach (var slot in database_4) slot.DatabaseSlot.OnChange += SourceSlotOnChange;
            foreach (var slot in database_5) slot.DatabaseSlot.OnChange += DestinationSlotOnChange;
            foreach (var slot in database_7) slot.DatabaseSlot.OnChange += DestinationSlotOnChange;
            foreach (var slot in database_15) slot.DatabaseSlot.OnChange += DestinationSlotOnChange;
            foreach (var slot in database_17) slot.DatabaseSlot.OnChange += DestinationSlotOnChange;

            return true;

        }

        private void SourceSlotOnChange(object sender, EventSlotChange e)
        {

            logger.Info($"Source slot change detected for IDX: {e.Slot.Index}");

            uint updatedIndex = e.Slot.Index;

            //if a source exists at that index then remove it
            if (Sources.ContainsKey(updatedIndex))
            {
                Sources.TryRemove(updatedIndex, out var deletedSource);
            }

            //attempt to create a source at the index - need to lookup db0 for this index
            var db0slot = (from a in database_0
                           where a.Index == updatedIndex
                           select a.DatabaseSlot).FirstOrDefault();

            if (db0slot != null)
            {
                CreateSource((updatedIndex, db0slot));
            }

            //fire the loading finished event - this will cause the grids to reload sources & destinations
            ResyncRouter();
            LoadingFinished?.Invoke(this, true);

        }

        private void DestinationSlotOnChange(object sender, EventSlotChange e)
        {

            logger.Info($"Destination slot change detected for IDX: {e.Slot.Index}");

            uint updatedIndex = e.Slot.Index;

            //if a destination exists at that index then remove it
            if (Destinations.ContainsKey(updatedIndex))
            {
                Destinations.TryRemove(updatedIndex, out var deletedDestination);
                deletedDestination.Dispose();
            }

            //attempt to create a destinartion at the index - need to lookup db1 for this index
            var db1slot = (from a in database_1
                           where a.Index == updatedIndex
                           select a.DatabaseSlot).FirstOrDefault();

            if (db1slot != null)
            {
                CreateDestination((updatedIndex, db1slot));
            }

            //fire the loading finished event - this will cause the grids to reload sources & destinations
            ResyncRouter();
            LoadingFinished?.Invoke(this, true);

        }

        public void ResyncRouter()
        {

            Task.Run(() =>
            {
                foreach (var destination in Destinations.Values) destination.IsSourceValid();
            });

        }

        private void CreateDestination((uint Index, SlotDatabase DatabaseSlot) item)
        {
            try
            {
                if (item.DatabaseSlot.Value != null && !item.DatabaseSlot.Value.Equals("---"))
                {

                    //database slot is set so create the destination
                    var newDest = new IPXVirtualDestination(item.Index);
                    destinations.TryAdd(item.Index, newDest);
                    newDest.ButtonName = item.DatabaseSlot.Value;

                    newDest.LookupSource = LookupSource;
                    newDest.ResolveSource = ResolveSource;

                    //lookup the destination mappings in each colo
                    var db5slot = (from a in database_5
                                   where a.Index == item.Index
                                   select a.DatabaseSlot).FirstOrDefault();

                    if (db5slot != null)
                    {
                        uint destIndex = uint.Parse(db5slot.Value);
                        newDest.PhysicalDestinationMain = new IPXPhysicalDestination(mainRouterDevice, destIndex);
                    }

                    var db7slot = (from a in database_7
                                   where a.Index == item.Index
                                   select a.DatabaseSlot).FirstOrDefault();

                    if (db7slot != null)
                    {
                        uint destIndex = uint.Parse(db7slot.Value);
                        newDest.PhysicalDestinationBackup = new IPXPhysicalDestination(backupRouterDevice, destIndex);
                    }

                    var db15slot = (from a in database_15
                                    where a.Index == item.Index
                                    select a.DatabaseSlot).FirstOrDefault();

                    if (db15slot != null) newDest.MainType = (IPXVirtualDestination.DestinationTypes)Enum.Parse(typeof(IPXVirtualDestination.DestinationTypes), db15slot.Value);

                    var db17slot = (from a in database_17
                                    where a.Index == item.Index
                                    select a.DatabaseSlot).FirstOrDefault();

                    if (db17slot != null) newDest.BackupType = (IPXVirtualDestination.DestinationTypes)Enum.Parse(typeof(IPXVirtualDestination.DestinationTypes), db17slot.Value);

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, $"Error in CreateDestination IDX: {item.Index}");
            }

        }

        private void InitialiseDb(List<(uint Index, SlotDatabase DatabaseSlot)> db, uint dbIndex)
        {

            //db file name extensions might or might not be padded!
            string dbName = $"{instances_and_devicetypes.Helper.ConfigPath}/system/dev_{instance.Device}.db{dbIndex}";
            string dbAltName = $"{instances_and_devicetypes.Helper.ConfigPath}/system/dev_{instance.Device}.db{dbIndex.ToString("D4")}";

            IniData data = null;

            if (System.IO.File.Exists(dbName))
            {
                data = iniParser.ReadFile(dbName);
            }
            else if (System.IO.File.Exists(dbAltName))
            {
                data = iniParser.ReadFile(dbAltName);
            }
            else
            {
                logger.Error($"Unable to find database file for index {dbIndex}");
                return;  //can't continue
            }

            foreach (var item in db)
            {
                item.DatabaseSlot.SetInitialValue(data[$"Database_{dbIndex}"][item.Index.ToString("D4")]);
            }

        }

        private IPXVirtualSource LookupSource(uint index)
        {
            if (sources.ContainsKey(index)) return sources[index];
            else return null;
        }

        private IPXVirtualSource ResolveSource(uint mainTally, IPXVirtualDestination.DestinationTypes mainType, uint backupTally, IPXVirtualDestination.DestinationTypes backupType)
        {

            var source = (from a in sources
                          select a.Value);

            //check the special case where we have an undefined types then we need to make sure we don't have a 0 tally
            if (mainType == IPXVirtualDestination.DestinationTypes.Undefined || backupType == IPXVirtualDestination.DestinationTypes.Undefined)
            {
                if (mainTally == 0 && (mainType == IPXVirtualDestination.DestinationTypes.HP || mainType == IPXVirtualDestination.DestinationTypes.LP)) return null;
                if (backupTally == 0 && (backupType == IPXVirtualDestination.DestinationTypes.HP || backupType == IPXVirtualDestination.DestinationTypes.LP)) return null;
            }

            //special case - don't match anything when both tallies are 0
            if (mainTally == 0 && backupTally == 0) return null;

            switch (mainType)
            {
                case IPXVirtualDestination.DestinationTypes.Undefined:
                    break;
                case IPXVirtualDestination.DestinationTypes.HP:
                    source = source.Where(a => a.IndexHpMain == mainTally);
                    break;
                case IPXVirtualDestination.DestinationTypes.LP:
                    source = source.Where(a => a.IndexLpMain == mainTally);
                    break;
                default:
                    break;
            }

            switch (backupType)
            {
                case IPXVirtualDestination.DestinationTypes.Undefined:
                    break;
                case IPXVirtualDestination.DestinationTypes.HP:
                    source = source.Where(a => a.IndexHpBackup == backupTally);
                    break;
                case IPXVirtualDestination.DestinationTypes.LP:
                    source = source.Where(a => a.IndexLpBackup == backupTally);
                    break;
                default:
                    break;
            }

            return source.FirstOrDefault();

        }

        public void Dispose()
        {

            foreach (var destination in Destinations.Values)
            {
                destination.Dispose();
            }

            Destinations.Clear();
            Sources.Clear();

        }

    }
}
