﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.helpers
{
    public enum DeviceState
    {
        Disconnected,
        Connected
    }

    public enum MultipleDeviceState
    {
        Disconnected,
        PartiallyConnected,
        Connected
    }

    public enum BncsState
    {
        Error,
        RXOnly,
        Mixed,
        TXRX
    }
}
