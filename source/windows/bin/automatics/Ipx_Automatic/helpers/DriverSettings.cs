﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Threading;
using driver_template.model;
using driver_template.model.bncs;

namespace driver_template.helpers
{
    public class Setting<T> : Setting, INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private T value;

        public T Value
        {
            get
            {
                return value;
            }
            set
            {
                if (!this.value.Equals(value))
                {
                    this.value = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Value"));
                    Changed?.Invoke(this, new EventSettingChange<T>(value));
                }
            }
        }

        //Implicit to make it easy to 
        public static implicit operator T(Setting<T> v) { return v.Value; }

        public TryParseMethod<T> TryParse;

        public Setting(T Default, String ValidSettingsExplaination, bool CannotBeDefault = false, TryParseMethod<T> TryParse = null, bool ChangeableAtRunTime = true)
            : this(Default, CannotBeDefault, TryParse, ChangeableAtRunTime)
        {
            this.ValidSettingsExplaination = ValidSettingsExplaination;
        }

        public T Default { get; }

        public override void SetDefault()
        {
            Value = Default;
        }

        public override bool IsDefault()
        {
            return this.value.Equals(Default);
        }

        public override bool IsValid(String s)
        {
            return this.TryParse(s, out T v);
        }

        public override bool IsDifferent(String s)
        {
            return s != this.ToString();
        }

        public Setting(T Default, bool CannotBeDefault = false, TryParseMethod<T> TryParse = null, bool ChangeableAtRunTime = false)
        {
            this.ChangeableAtRunTime = ChangeableAtRunTime;
            this.CannotBeDefault = CannotBeDefault;
            this.Default = Default;
            this.value = Default;
            if (TryParse == null)
            {
                //Look for the default parsers
                ITryParser i = StringToObject.GetParser(typeof(T));
                if (i == null)
                {
                    throw new Exception(String.Format(
@"Error whilst trying to create a Setting of type:{0}
The type:{0} does not have a generic TryParse.
Either supply a TryParse Function to the settings constructor,
If the type already has a TryParse function:
Example:
YourSetting = new Setting<{0}>(DefualtValue, {0}.TryParse);

OtherWise:
Example Setting<{0}>(DefaultValue, (string input, out {0} value) => 
{{ //Do some checking here to see if the string matches your value
value = somthing; return true; 
}})

or add the TryParse Function to the StringToObject class
", typeof(T).Name));


                }
                else
                {
                    this.TryParse = (string input, out T value) =>
                    {
                        if (i.TryParse(input, out object o))
                        {
                            value = (T)o;
                            return true;
                        }
                        value = default(T);
                        return false;
                    };
                }
            }
            else
            {
                this.TryParse = TryParse;
            }
        }

        public override (bool Success, bool Changed) SetFromString(string s)
        {
            if (this.TryParse(s, out T v))
            {
                if (!Value.Equals(v))
                {
                    Value = v;
                    return (Success: true, Changed: true);
                }
                else
                    return (Success: true, Changed: false);

            }
            return (Success: false, Changed: false);
        }


        public override string ToString()
        {
            return value.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<EventSettingChange<T>> Changed;
    }

    public class EventSettingChange<T> : EventArgs
    {
        public T Setting;
        public EventSettingChange(T setting)
        {
            Setting = setting;
        }
    }


    public abstract class Setting
    {
        public abstract (bool Success, bool Changed) SetFromString(string s);
        public string ValidSettingsExplaination = null;
        public bool CannotBeDefault { get; protected set; }
        public bool ChangeableAtRunTime { get; protected set; }

        public event EventHandler OnSave;

        public void Save()
        {
            OnSave?.Invoke(this, new EventArgs());
        }

        public abstract void SetDefault();

        public abstract bool IsDefault();

        public abstract bool IsValid(String s);

        public abstract bool IsDifferent(String s);
    }

    public static class StringToObject
    {
        private static Dictionary<Type, ITryParser> Parsers;

        static StringToObject()
        {
            Parsers = new Dictionary<Type, ITryParser>();
            AddParser<DateTime>(DateTime.TryParse);
            AddParser<int>(Int32.TryParse);
            AddParser<double>(Double.TryParse);
            AddParser<decimal>(Decimal.TryParse);
            AddParser<bool>(Boolean.TryParse);
            AddParser<string>((string input, out string value) => { value = input; return true; });
            AddParser<uint>(uint.TryParse);
            AddParser<long>(long.TryParse);
            AddParser<ulong>(ulong.TryParse);
        }

        public static void AddParser<T>(TryParseMethod<T> parseMethod)
        {
            Parsers.Add(typeof(T), new TryParser<T>(parseMethod));
        }

        public static ITryParser GetParser(Type type)
        {
            Parsers.TryGetValue(type, out ITryParser i);
            return i;
        }
    }

    public delegate bool TryParseMethod<T>(string input, out T value);

    public interface ITryParser
    {
        bool TryParse(string input, out object value);
    }

    public class TryParser<T> : ITryParser
    {
        private readonly TryParseMethod<T> ParsingMethod;

        public TryParser(TryParseMethod<T> parsingMethod)
        {
            this.ParsingMethod = parsingMethod;
        }

        public bool TryParse(string input, out object value)
        {
            bool success = ParsingMethod(input, out T parsedOutput);
            value = parsedOutput;
            return success;
        }
    }


    public static class DriverSettings
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static String Instance = String.Empty;
        private static Dictionary<string, Setting> AllSingleSettings = new Dictionary<string, Setting>();

        static DriverSettings()
        {
            //Setup the private members
            var t = BNCSConfig.Settings.GetType();
            var w = t.GetProperties().Where(x => typeof(Setting).IsAssignableFrom(x.PropertyType));

            //Find all Setting, that we have been given
            AllSingleSettings = w.ToDictionary(x => x.Name, x => x.GetValue(BNCSConfig.Settings) as Setting);

            foreach (var s in AllSingleSettings)
            {
                s.Value.OnSave += (sender, e) => { Save(s.Key); };
            }
        }

        private static readonly String XmlPath = Path.Combine(instances_and_devicetypes.Helper.ConfigPath, "driver_config.xml");

        static readonly object lockObject = new object();

        //Watches the file system for changes
        private static void Watch()
        {
            FileSystemWatcher watcher = new FileSystemWatcher
            {
                Path = Path.GetDirectoryName(XmlPath),
                NotifyFilter = NotifyFilters.LastWrite,
                Filter = Path.GetFileName(XmlPath)
            };
            watcher.Changed += new FileSystemEventHandler((s, e) =>
            {
                lock (lockObject)
                {
                    //This needs to be done, to stop double callbacks for change events
                    watcher.EnableRaisingEvents = false;

                    //Call in timer, as the file might not be finished writing too yet.
                    Timer timer = null;
                    timer = new Timer(new TimerCallback(y =>
                    {
                        try
                        {
                            logger.Info("Loading Settings - File Changed");
                            var (State, Error, Document) = Load();
                            if (Error != null)
                            {
                                logger.Error("Loading Settings, {0}", Error);
                            }
                            // here i want to dispose this timer
                            timer.Dispose();
                        }
                        catch
                        {
                        }
                        finally
                        {
                            watcher.EnableRaisingEvents = true;
                        }
                    }));
                    timer.Change(10, Timeout.Infinite);
                }
            });

            watcher.EnableRaisingEvents = true;
        }

        static public void LoadSettings(string Instance)
        {
            DriverSettings.Instance = Instance;
            var file = Load(true, true);
            Save(null, false, file);
            Watch();
            BNCSConfig.Settings.RunAdditionalChecks();
        }

        enum LoadState
        {
            FileMissing,
            IOError,
            DocException,
            MissingInstanceSegment,
            SettingMissing,
            NoError
        }

        static (LoadState State, String Error, XDocument Document) Load(bool StoreValuesFromFile = true, bool FirstLoad = false)
        {
            XDocument xDoc = null;
            try
            {
                xDoc = XDocument.Load(XmlPath);
            }
            catch (FileNotFoundException)
            {
                return (LoadState.FileMissing, $"Failed to find the settings file:{XmlPath}", null);
            }
            catch (IOException)
            {
                return (LoadState.IOError, $"Failed open the settings file:{XmlPath}", null);
            }
            catch (Exception e)
            {
                return (LoadState.DocException, $"Error loading settings file:{XmlPath} Error:{e.Message}", null);
            }

            //Look for our instance
            var elms = xDoc.Root.Elements("driver").Where(x => x.Attribute("id") != null && x.Attribute("id").Value == Instance).FirstOrDefault();

            if (elms == null)
                return (LoadState.MissingInstanceSegment, $"Failed to find the instance:{Instance} in settings file:{XmlPath}", xDoc);

            List<string> ValuesFound = new List<string>();

            List<string> MissingValues = new List<string>();
            //Loop through the settings, looping so we can catch errors are report them
            foreach (var n in elms.Elements())
            {
                if (n.Name != "setting")
                {
                    logger.Warn("Loading Settings, Tag incorrect:{0}, should be \"setting\".", n.ToString());
                    continue;
                }

                if (n.Attribute("id") == null)
                {
                    logger.Warn("Loading Settings, \"id\" attribute missing.", n.ToString());
                    continue;
                }

                string id = n.Attribute("id").Value;

                //Check if it is a value we are interested in
                if (!AllSingleSettings.TryGetValue(id, out var value))
                {
                    logger.Warn("Loading Settings, id:\"{0}\" not expected by driver, ignoring", id);
                    continue;
                }

                if (n.Attribute("value") == null)
                {
                    logger.Warn("Loading Settings, id:\"{0}\" missing \"value\" attribute. Expecting  <setting id=\"id\" value=\"thing\"/>", id);
                    continue;
                }

                if (StoreValuesFromFile)
                {

                    //Check if it is changeable at runtime
                    if (FirstLoad == false && value.ChangeableAtRunTime == false)
                    {
                        if (value.ToString() != n.Attribute("value").Value)
                            logger.Error("Loading Settings, id:\"{0}\" setting cannot be changed at runtime currently is:\"{1}\" attempt to change to \"{2}\"", id, value.ToString(), n.Attribute("value").Value);
                    }
                    else
                    {
                        string oldVal = value.ToString();
                        //If we are here it is a single setting
                        (var success, var changed) = value.SetFromString(n.Attribute("value").Value);

                        if (changed && !FirstLoad)
                        {
                            logger.Info("Loading Settings, id:\"{0}\" setting changed from:\"{1}\" to \"{2}\"", id, oldVal, value.ToString());
                        }
                        else if (!success)
                        {
                            value.SetDefault();
                            logger.Error("Loading Settings, id:\"{0}\", Error invalid value:\"{1}\", using default \"{2}\"", id, n.Attribute("value").Value, value.ToString());
                            MissingValues.Add(id);
                        }
                    }
                }
                else
                {
                    if (!value.IsValid(n.Attribute("value").Value))
                    {
                        MissingValues.Add(id);
                    }
                }

                ValuesFound.Add(id);
            }

            //Finished looping through, see if we have missed anything we need
            var ErrorIfTheseAreMissing = AllSingleSettings.Where(x => x.Value.CannotBeDefault && ValuesFound.Contains(x.Key));
            if (ErrorIfTheseAreMissing.Count() > 0)
            {
                //If not first load use current settings
                foreach (var v in ErrorIfTheseAreMissing)
                {
                    if (StoreValuesFromFile)
                    {
                        logger.Error("Loading Settings, id:\"{0}\" not found in settings but required. Using:\"{1}\".", v.Key, v.Value.ToString());
                    }
                    MissingValues.Add(v.Key);
                }
            }

            var ErrorIfTheseAreDefault = AllSingleSettings.Where(x => x.Value.CannotBeDefault && x.Value.IsDefault());
            if (ErrorIfTheseAreDefault.Count() > 0)
            {
                //If not first load use current settings
                foreach (var v in ErrorIfTheseAreDefault)
                {
                    if (StoreValuesFromFile)
                    {
                        logger.Error("Loading Settings, id:\"{0}\" is found in settings but is Default.:\"{1}\".", v.Key, v.Value.ToString());
                    }
                }
            }

            //All missing values that can be default
            var UsingDefault = AllSingleSettings.Where(x => !ValuesFound.Contains(x.Key) && x.Value.CannotBeDefault == false);
            foreach (var v in UsingDefault)
            {
                if (StoreValuesFromFile)
                {
                    //If the value is allowed to be changed
                    if (FirstLoad == true || v.Value.ChangeableAtRunTime == true)
                    {
                        v.Value.SetDefault();
                        logger.Warn("Loading Settings, id:\"{0}\" not found in settings using default:\"{1}\"", v.Key, v.Value.ToString());
                    }
                    else
                    {
                        logger.Error("Loading Settings, id:\"{0}\" not found in settings and cannot be changed at runtime, current value:\"{1}\"", v.Key, v.Value.ToString());
                    }
                }
                MissingValues.Add(v.Key);
            }

            if (MissingValues.Count > 0)
                return (LoadState.SettingMissing, "Missing Values", xDoc);

            else
                return (LoadState.NoError, null, xDoc);
        }

        public static void Save(string SingleItemSave = null, bool ClearBadXMLValues = false)
        {
            var file = Load(false);
            Save(SingleItemSave, ClearBadXMLValues, file);
        }

        static void Save(string SingleItemSave, bool ClearBadXMLValues, (LoadState State, String Error, XDocument Document) file)
        {
            //Not sure what to do on IO error, maybe just try to access it again?

            XDocument d = file.Document;

            if (ClearBadXMLValues && file.State >= LoadState.SettingMissing)
            {
                //Delete it and readd
                var all = d.Root.Elements("driver").Where(x => x.Attribute("id") != null && x.Attribute("id").Value == Instance);

                //This is more complex than it needs to be to ensure I maintain position in the xml

                //If we have duplicate remove all but the first
                if (all.Count() > 1)
                {
                    all.Skip(1).Remove();
                }
                else if (all.Count() == 0)
                {
                    //If this is empty the Instance Segment is missing, we shouldn't be here in theory, as the setting passed should have told us this
                    file.State = LoadState.MissingInstanceSegment;
                }
                else
                {
                    //Clear all attributes we don't need
                    var node = all.Take(1);
                    node.Attributes().Where(x => x.Name != "id").Remove();
                    node.Nodes().Remove();//Clear child nodes
                    file.State = LoadState.SettingMissing;
                }
            }

            switch (file.State)
            {
                case LoadState.FileMissing:
                    logger.Info("Creating driver config file as it does not exist: {0}", XmlPath);
                    d = new XDocument(
                        new XElement("driver_config")
                        );
                    //File.Create(XmlPath());
                    goto case LoadState.MissingInstanceSegment;

                case LoadState.IOError:
                    logger.Error("IO Error whilst trying to open XML: {0}", XmlPath);
                    break;
                case LoadState.DocException:
                    logger.Error("There is an error in the current XML, please fix this: {0}, Error: {1}", XmlPath, file.Error);
                    break;

                case LoadState.MissingInstanceSegment:
                    logger.Info("Creating instance: {0} in driver config file", Instance);
                    d.Root.Add(new XElement("driver", new XAttribute("id", Instance)));
                    goto case LoadState.SettingMissing;

                case LoadState.SettingMissing:
                case LoadState.NoError:

                    var elms = d.Root.Elements("driver").Where(x => x.Attribute("id") != null && x.Attribute("id").Value == Instance).FirstOrDefault();

                    if (SaveSettings(elms, SingleItemSave))
                    {
                        d.Save(XmlPath);
                        logger.Info("Saved config to {0}", XmlPath);
                    }
                    break;
            }
        }

        private static bool SaveSettings(XElement elms, string singleItemSave)
        {
            bool change = false;
            foreach (var s in AllSingleSettings.Where(x => singleItemSave == null || x.Key == singleItemSave))
            {
                var e = elms.Elements().Where(x => x.Attribute("id") != null && x.Attribute("id").Value == s.Key).FirstOrDefault();

                //If the item is missing add it
                if (e == null)
                {
                    elms.Add(new XElement("setting", new XAttribute("id", s.Key), new XAttribute("value", s.Value.ToString())));
                    logger.Info("Saved setting id=\"{0}\" value=\"{1}\"", s.Key, s.Value.ToString());
                    change = true;
                }
                else
                {
                    //Write the value if, the attribute is missing or
                    //The current value is invalid or
                    //The current stored value is different
                    if (e.Attribute("value") == null || !s.Value.IsValid(e.Attribute("value").Value) || s.Value.IsDifferent(e.Attribute("value").Value))
                    {
                        string old = String.Empty;
                        if (e.Attribute("value") != null)
                            old = e.Attribute("value").Value;

                        e.SetAttributeValue("value", s.Value.ToString());
                        change = true;

                        logger.Info("Saved setting id=\"{0}\" old=\"{1}\" new value=\"{2}\"", s.Key, old, s.Value.ToString());
                    }
                }
            }
            return change;
        }
    }
}
