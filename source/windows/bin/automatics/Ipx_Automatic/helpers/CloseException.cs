﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace driver_template.helpers
{
    [Serializable]
    public class CloseException : Exception
    {
        public string Title;
        public ILogger Logger;
        public ErrorCode ErrorCode;

        public CloseException(string message, string title, ILogger logger, ErrorCode errorCode = helpers.ErrorCode.Exception)
            : base(message)
        {
            Title = title;
            Logger = logger;
            ErrorCode = errorCode;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("Title", Title);
            info.AddValue("ErrorCode", ErrorCode);
        }
    }
}
