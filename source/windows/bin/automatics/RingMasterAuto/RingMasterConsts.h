
/* max size of buffer for data from string resource */
#define	MAX_LOADSTRING	100

/* max number of items in debug listbox */
#define	MAX_LB_ITEMS	1500

/* buffer constant for strings */
#define	MAX_AUTOBFFR_STRING 270 // was MAX_AUTOBFFR_STRING
#define	UNKNOWNVAL -1

#define	APP_WIDTH	1030
#define	APP_HEIGHT	940

#define	ROUTERCOMMAND 1
#define	INFODRVCOMMAND 2
#define	GPIOCOMMAND 3
#define	DATABASECOMMAND 4
#define	AUTOINFOREVERTIVE 5

#define	BNCS_COMMAND_RATE  10           // number commands sent out per iteration of command timer

// defined colours used in app
#define COL_BLACK  RGB(0,0,0)
#define COL_WHITE RGB(255,255,255)

#define COL_DK_RED RGB(127,0,0)
#define COL_DK_GRN RGB(0,127,0)
#define COL_DK_BLUE RGB(0,0,80)
#define COL_DK_ORANGE RGB(128,64,0)

#define COL_LT_RED RGB(255,0,0)
#define COL_LT_GRN RGB(0,255,0)
#define COL_LT_BLUE RGB(100,100,255)
#define COL_LT_ORANGE RGB(255,128,0)

#define COL_YELLOW RGB(255,255,0)
#define COL_TURQ RGB(210,210,250)


#define ONE_SECOND_TIMER_ID		1	 // could be used to relook for comms port / infodrivers -- for more intelligent driver
#define COMMAND_TIMER_ID		  2							// 
#define STARTUP_TIMER_ID	    	3							// 
#define DATABASE_TIMER_ID	       4							// 
#define TIKTOK_ALIVE_TIMER_ID  5
#define FORCETXRX_TIMER_ID       7					// 
#define SHAREDRESOURCE_TIMER_ID  8

#define ONE_SECOND_TIMER_INTERVAL	 1000 // 1.5 sec timer used for housekeeping - mainly for re connects to info driver, comport connection failures, device interrogation 
#define COMMAND_TIMER_INTERVAL			100 // 0.1 sec interval on commands being sent from the fifobuffer 
#define STARTUP_TIMER_INTERVAL			1000 // 
#define TENSECONDS						10  
#define SIXTYSECONDS					20  

#define STRINGSEQUAL 0

#define REV_ROUTER   1
#define REV_INFODRV  2
#define REV_GPISTAT  3
#define REV_DBASERM  4

#define MCR_CLONE_COMMANDS_INFO_START 1000    // slots to take MCR / STUDIO mon commands - for clone port monitoring destinations - defined in ring_master_config.xml
#define MAX_MCR_CLONE_PORTS  200                                // slots 1000 to 1200 for clone port monitoring

#define HIGHWAYS_INFO_ALARM_TRIGGER 1799  // slot to trigger top strap to get user to go to highways panel
#define HIGHWAYS_INFO_TOTALUSAGE  1800   // slot 1800 for totals (std and mon) used - specific ring highway usage then from 1801..1849  -- max 49 rings
#define HIGHWAYS_INFO_CLONE_USAGE 1850   // - specific ring monitoring highway usage then from 1851..1899  -- max 49 rings

#define HIGHWAYS_INFO_TRACE_DATA  2000   // slot 2000 + for further trace details of highways -- allows for 1900 defined highways (std and monitoring)-- for panel use
													//  before running over other defined slots below
#define RIEDEL_TRUNK_INFO_TOTALUSAGE   3900    // slots 3901 .. 3950  - trunk nav totals for each ring ( by trunk address )
#define RINGTRUNKS_ERROR_MSGS		   4000    // 4001..4050 - warning and  error messages linked to trunk / ring, slot 4000 for general warning / error messages
#define COMM_STATUS_SLOT			   4091

// slot usage for ifb infodriver  - now multiple infodrivers - one per ring to handle 1500 ifbs per ring
#define IFB_SLOT_INDEX_START 0  // slots 1..1500 for 1500 ifbs and the panels assoc to them - calculated from Monitor Revs
#define IFB_MON_PANEL_MEMBERSHIP 1500  // slots 1501..3000 for 1500 ifbs and the panels assoc to them - calculated from Monitor Revs

// mixer and logic data are stored in IFB infodrivers now - on a per ring basis
#define RMSTR_MIXERS_INFO_START   3000        // 500 mixers in riedel driver per ring -- linked to Extras infodriver of riedel drivers 501-1000
#define RMSTR_LOGIC_INFO_START     3500        // 500 remote logics in riedel driver per ring-- Extras infodriver of riedel drivers 1001-1500

#define RIEDELDRIVER_MIXERS_INFO_START   500        // 500 mixers in riedel driver per ring -- linked to Extras infodriver of riedel drivers 501-1000
#define RIEDELDRIVER_LOGIC_INFO_START     1000        // 500 remote logics in riedel driver per ring-- Extras infodriver of riedel drivers 1001-1500

// slot usage for conf infodriver   943 -- ring master conf revs are 1..500, 1001..1500, 2001..2500 - in same vein as riedel driver, plus :
#define CONF_MON_PANEL_MEMBERSHIP 3000  // slots 3001..3500 for 500 conf and the panels assoc to them - calculated from Monitor revs

// infodriver types
#define AUTO_RINGMASTER_INFO  1         // info on rings, trunks, highways,  etc
#define AUTO_CONFERENCES_INFO 2       // confs 1..500, 1001..1500 2001..2500 -- just like riedel driver
#define AUTO_RING_IFBS_INFOS      3      //  ifbs now
#define AUTO_RING_PORTS_INFOS  4       // ports
#define AUTO_PORTS_PTI_LOGIC_INFO        5        // pti for ports etc

#define MAX_RIEDEL_RINGS      50
#define MAX_MIXERS_LOGIC    500   				   // allows for up to  500 mixers in riedel
#define MAX_CONFERENCES      500   				   // allows for up to  500 conf in riedel
#define MAX_IFBS                       1500   				   // allows for up to  1500 ifbs  PER RING NOW - more infodrivers used
#define MAX_RIEDEL_SLOTS    4000                      //   1..4000  -- monitor ports per ring
#define MAX_DRIVER_TRUNKS     99                      // XXX -- needs to change in driver - as 99 slots in Riedel driver Extras infodriver for trunk nav info not enough
#define MAX_RIEDEL_PTI         2000                     // pti for conf / ifb / mixer  ports 1..2000 
#define IFB_HIGHWAY_BASE     10000               // special base index used as a trip in finding highways for ifb input/output use : calc: (10000 X trunk address) + port 

//Riedel databases
#define DATABASE_RIEDEL_4WNAME	1
#define DATABASE_RIEDEL_PORT_TYPE	9

#define TAB_CHAR 0x09

// riedel port types
#define PORT_TYPE_NONE      0
#define PORT_TYPE_INPUT     1
#define PORT_TYPE_OUTPUT    2
#define PORT_TYPE_SPLIT     3
#define PORT_TYPE_4WIRE     4
#define PORT_TYPE_PANEL     5
#define PORT_TYPE_PANELEXPN 6

// these numbers are also index into data check list box
#define RIEDEL_RING_TYPE 1
#define RIEDEL_GRD_TYPE 2
#define RIEDEL_MONITOR_TYPE 3
#define RIEDEL_PTI_PORTS 4
#define RIEDEL_CONFERENCE_TYPE 5
#define RIEDEL_IFB_TYPE 6
#define RIEDEL_EXTRAS_MIXERS 7
#define RIEDEL_EXTRAS_LOGIC 8
#define RIEDEL_EXTRAS_TRUNKS 9

#define AUTOS_CONTROL_INFO 11

#define CONF_MBMR 1
#define CONF_LABEL  2
#define CONF_PANEL  3

#define PTI_OVERFLOW_INDICATOR '$'

#define IDENT_OFF 0
#define IDENT_ON 1

#define MAX_TRACE 500

#define HIGHWAY_FREE           0    // parked
#define HIGHWAY_CLEARING 1    // clearing highway - to be ignored in ifb / grd revs as being cleared 
#define HIGHWAY_PENDING  2    // newly allocated - pending rtr revs
#define HIGHWAY_INUSE        3    // in use - - has users ( to-src is routed to other dests )  - so not free to be reused
#define HIGHWAY_IFBMXRIN 4    // in use - - special case when used in IFB / Mixer so not free to be reused  
#define HIGHWAY_IFBMXROUT 5    // in use - - special case when used in IFB / Mixer so not free to be reused  
#define HIGHWAY_CLONEMON 6    // in use - - special case when used in mcr clone monitoring - either src or dest -- exclusive use of highway?
#define HIGHWAY_REMOTE_USE 777    // highway in use by other ringamster - leave well alone

#define MODE_IFBINPUT      1    // in use - - special case when used in IFB  inputs / mix minus - so not free to be reused ( as highway has no users )
#define MODE_IFBOUTPUT  2    // in use - - special case when used in IFB  outputs - so not free to be reused  ( as highway has no real source )
#define MODE_MIXERIN       3    // in use - - special case when used in  Mixer inputs  - so not free to be reused ( as highway has no users )
#define MODE_MIXEROUT    4    // in use - - special case when used in Mixer outputs - so not free to be reused  ( as highway has no real source )
#define MODE_CLONESRCE    5    // in use - - special case when used in clone monitoring as source
#define MODE_CLONEDEST    6    // in use - - special case when used in clone monitoring as dest 

#define PARK_SRCE 0

#define DEST_UNLOCKED 0
#define DEST_LOCKED   1

#define SOURCE_TYPE 0
#define DESTINATION_TYPE 1

#define ALARM_CLEAR 0
#define ALARM_WARNING 1
#define ALARM_ERROR_STATE 2
#define ALARM_SEVERE_STATE 3

#define OTHER_RMSTR_ASSIGNED 777
#define OTHER_RMSTR_USAGE       77

// consts for slot use in automatics control infodriver
// all listed for completeness

// Locality area riedel ring types -- currently two multi sites supported by Ringmaster auto
#define AREA1_LOCALE_RING      1    // nominally ldc == london
#define AREA2_LOCALE_RING      2   // nominally nhn == hilversum

// Ringmaster base offsets
#define CTRL_INFO_RMSTR_AREA1_BASE 1010
#define CTRL_INFO_RMSTR_AREA2_BASE 1020

// ctrl info common slot offsets
#define CTRL_INFO_TXRX_TIKTOK 1
#define CTRL_INFO_RXONLY_TIKTOK 2

// basic riedel pool data for confs and ifbs
// offsets for shared riedel pool data for confs and ifbs
#define CTRL_INFO_CONFPOOL  4            // txrx auto - rxonly may use next slot
#define CTRL_INFO_IFBPOOL       6
#define CTRL_INFO_HIGHWAYS   8

#define CTRL_INFO_RMSTR_HIGHWAYS 1100    // base slot for data on all highways

