#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class to hold REVERTIVE data from riedel drivers 
class CRiedel_Monitors
{
private:
	int m_iRecordIndex;
	int m_iRevDevice;
	int m_iRevSlot;
	char m_splitChar;
	int m_iAssoc_TrunkAddr;  // can be used to index into ring class for this revertive record for infodriver device
	int m_iAssoc_MonHighway;  // this dev and slot is outgoing monitoring highway index
	int m_iAssoc_MonPortRecord;  // this dev and slot is linked to a final monitoring destination  // McrMonPortRecord index

	bncs_stringlist m_ssl_Data; // raw data split by char

	bncs_string m_ss1_MonFunction;  // first of up to 6 parts of revertives for riedel infodrv revs
	bncs_string m_ss1_PrevFunction;  // prev of 4 revertives for riedel infodrv revs   // for ease of processing
	bncs_string m_ss2_MonMarker;     // 2nd of 6 revertives for riedel infodrv revs
	bncs_string m_ss3_MonLabel;         // 3rd of 6 revertives for riedel infodrv revs
	bncs_string m_ss4_MonKeyProp;   // 4th of 6 revertives for riedel infodrv revs -- only from monitoring and IFB revs
	bncs_string m_ss5_MonKeySubTitle;   // 5th of 6 revertives for riedel infodrv revs -- only from monitoring and IFB revs 
	bncs_string m_ss6_MonKeyTextColour;   // 6th of 6 revertives for riedel infodrv revs -- only from monitoring and IFB revs

public:
	CRiedel_Monitors(int iRecordIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk);
	~CRiedel_Monitors();

	void clearRiedelData(void);

	void storeRiedelData(bncs_string ss);

	int getRecordIndex(void);
	int getDevice(void);
	int getSlot(void);
	int getTrunkAddress(void);

	bncs_string getRawRiedelRevertive(void);
	bncs_string getMonitoring_Function(void);
	bncs_string getPrevious_Function(void);
	bncs_string getMonitoring_Marker(void);
	bncs_string getMonitoring_Label(void);
	bncs_string getMonitoring_KeyProperty(void);
	bncs_string getMonitoring_SubTitle(void);
	bncs_string getMonitoring_TextColour(void);

	void setAssociatedMonHighway(int iHighIndex);
	int getAssociatedMonHighway(void);

	void setAssociatedMonitoringRecord(int iRecIndex);
	int getAssociatedMonitoringRecord(void);

};

