// HighwayRecord.cpp: implementation of the CHighwayRecord class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "HighwayRecord.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHighwayRecord::CHighwayRecord(int iKey, int iFromTrunk, int iFromRtr, int iFromDest, int iToTrunk, int iToRtr, int iToSrc, BOOL bMon)
{
	m_iRecordIndex = iKey;

	m_iFromTrunk = iFromTrunk;
	m_iFromRouter=iFromRtr;
	m_iFromDestination=iFromDest;

	m_iToTrunk = iToTrunk;
	m_iToRouter = iToRtr;
	m_iToSource=iToSrc;

	m_bMonitorHighway = bMon;
	m_iMonitorGivenFromDest = iFromDest;
	m_iMonitorGivenToSource = iToSrc;

	m_iPrimaryTracedRing = 0;
	m_iPrimaryTracedRouter = 0;
	m_iPrimaryTracedPort = 0;

	m_iHighwayState = 0;
	m_iPendingCount = 0;
	m_iAllocated_Index = PARK_SRCE;
	m_iHighwayAreaInUse = 0;

	m_ssl_WorkingMode = bncs_stringlist("", '|');

}

CHighwayRecord::~CHighwayRecord()
{
	// 
}


int CHighwayRecord::getRecordIndex(void)
{
	return m_iRecordIndex;
}


int CHighwayRecord::getFromRouter( void )
{
	return m_iFromRouter;
}

int CHighwayRecord::getFromTrunkAddr(void)
{
	return m_iFromTrunk;
}

int CHighwayRecord::getFromDestination( void )
{
	return m_iFromDestination;
}

int CHighwayRecord::getToTrunkAddr(void)
{
	return m_iToTrunk;
}

int CHighwayRecord::getToRouter( void )
{
	return m_iToRouter;
}

int CHighwayRecord::getToSource( void )
{
	return m_iToSource;
}


void CHighwayRecord::setTracedRouterAndPort(int iRing, int iGrd, int iPort)
{
	m_iPrimaryTracedRing = iRing;
	m_iPrimaryTracedRouter = iGrd;
	m_iPrimaryTracedPort = iPort;
}


void CHighwayRecord::getTracedRouterAndPort(int* iRing, int* iRtr, int* iPort)
{
	*iRing = m_iPrimaryTracedRing;
	*iRtr = m_iPrimaryTracedRouter;
	*iPort = m_iPrimaryTracedPort;
}


BOOL CHighwayRecord::isMonitoringHighway(void)
{
	return m_bMonitorHighway;
}

void CHighwayRecord::resetMonitorFromDestination(int iGivenPort, int iTranslatedPort)
{
	m_iMonitorGivenFromDest = iGivenPort;
	m_iFromDestination = iTranslatedPort;
}

void CHighwayRecord::resetMonitorToSource(int iGivenPort, int iTranslatedPort)
{
	m_iMonitorGivenToSource = iGivenPort;
	m_iToSource = iTranslatedPort;   
}

int CHighwayRecord::getMonitorGivenFromDest(void)
{
	return m_iMonitorGivenFromDest;
}

int CHighwayRecord::getMonitorGivenToSource(void)
{
	return m_iMonitorGivenToSource;
}


void CHighwayRecord::setHighwayState(int iNewState, int iAllocIndex, int iPending)
{
	m_iHighwayState = iNewState;
	if (iAllocIndex>UNKNOWNVAL)	m_iAllocated_Index = iAllocIndex; // does not overwrite if -1 passed as src - way of changing state without altering already assigned src
	if (iPending > UNKNOWNVAL)	m_iPendingCount = iPending; // does not overwrite if -1 passed as src - way of changing state without altering already pending counter value
	if ((iNewState <= HIGHWAY_PENDING) && (iAllocIndex == 0)) {
		m_ssl_WorkingMode.clear();
	}
}

int CHighwayRecord::getHighwayState(void)
{
	return m_iHighwayState;
}

int CHighwayRecord::getHighwayAllocatedIndex(void)
{
	return m_iAllocated_Index;
}

void CHighwayRecord::setHighwayAreaUse(int iAreaUse)
{
	m_iHighwayAreaInUse = iAreaUse;
}

int CHighwayRecord::getHighwayAreaUse()
{
	return m_iHighwayAreaInUse;
}



/////////////////////////////////////////////////////////////

void CHighwayRecord::decrimentHighwayPendingCount(void)
{
	if (m_iPendingCount>0) m_iPendingCount--;
}


int CHighwayRecord::getHighwayPendingCount(void)
{
	return m_iPendingCount;
}


void CHighwayRecord::addWorkingMode(int iMode, int iUserIndex)
{
	bncs_string sstr = bncs_string("%1,%2").arg(iMode).arg(iUserIndex);
	if (m_ssl_WorkingMode.find(sstr)<0) m_ssl_WorkingMode.append(sstr);
}

void CHighwayRecord::clearWorkingModes()
{
	m_ssl_WorkingMode.clear();
}

void CHighwayRecord::removeWorkingMode(int iMode, int iUserIndex)
{
	bncs_string sstr = bncs_string("%1,%2").arg(iMode).arg(iUserIndex);
	int iPos = m_ssl_WorkingMode.find(sstr);
	if ((iPos >= 0) && (iPos < m_ssl_WorkingMode.count())) {
		m_ssl_WorkingMode.deleteItem(iPos);
	}
}

int CHighwayRecord::isHighwayWorkingMode(int iMode, int iUserIndex)
{
	bncs_string sstr = bncs_string("%1,%2").arg(iMode).arg(iUserIndex);
	int iPos = m_ssl_WorkingMode.find(sstr);
	if ((iPos >= 0) && (iPos < m_ssl_WorkingMode.count())) {
		return iPos;
	}
	return UNKNOWNVAL;
}

BOOL CHighwayRecord::getWorkingMode(int iEntry, int* iMode, int* iUserIndex)
{
	*iMode = 0;
	*iUserIndex = 0;
	if ((iEntry >= 0) && (iEntry < m_ssl_WorkingMode.count())) {
		bncs_stringlist ssl_str = bncs_stringlist(m_ssl_WorkingMode[iEntry], ',');
		if (ssl_str.count() > 0) *iMode = ssl_str[0].toInt();
		if (ssl_str.count() > 1) *iUserIndex = ssl_str[1].toInt();
		return TRUE;
	}
	return FALSE;
}

int CHighwayRecord::getNumberWorkingModeEntries()
{
	return m_ssl_WorkingMode.count();
}

bncs_string CHighwayRecord::getAllWorkingModeEntries(void)
{
	return m_ssl_WorkingMode.toString('|');
}
