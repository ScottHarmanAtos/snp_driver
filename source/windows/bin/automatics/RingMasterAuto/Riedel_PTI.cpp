#include "Riedel_PTI.h"


CRiedel_PTI::CRiedel_PTI(int iRecordIndex, int iDevNum, int iRevSlot, int iAssocTrunk)
{
	m_iRecordIndex = iRecordIndex;
	m_iRevDevice = iDevNum;
	m_iRevSlot = iRevSlot;
	m_iAssoc_TrunkAddr = iAssocTrunk;  // index into ring class

	m_ss_Data = ""; // raw data 

	m_element1 = bncs_string("%1.0").arg(iAssocTrunk);
	m_element2 = bncs_string("%1.0").arg(iAssocTrunk);
	m_element3 = bncs_string("%1.0").arg(iAssocTrunk);
	m_element4 = bncs_string("%1.0").arg(iAssocTrunk);
	m_element5 = bncs_string("%1.0").arg(iAssocTrunk);

}


CRiedel_PTI::~CRiedel_PTI()
{
}

void CRiedel_PTI::storeRiedelData(bncs_string ss)
{
	m_ss_Data = ss;
	m_element1 = bncs_string("%1.0").arg(m_iAssoc_TrunkAddr);
	m_element2 = bncs_string("%1.0").arg(m_iAssoc_TrunkAddr);
	m_element3 = bncs_string("%1.0").arg(m_iAssoc_TrunkAddr);
	m_element4 = bncs_string("%1.0").arg(m_iAssoc_TrunkAddr);
	m_element5 = bncs_string("%1.0").arg(m_iAssoc_TrunkAddr);
	bncs_stringlist ssl1 = bncs_stringlist(ss, '|');
	// add in trunk addr into data elements
	for (int ii = 0; ii < ssl1.count(); ii++) {
		bncs_stringlist ssl2 = bncs_stringlist(ssl1[ii], ',');
		bncs_string sstr = "";
		for (int ic2 = 0; ic2 < ssl2.count(); ic2++) {
			if (ic2>0) 
				sstr.append(bncs_string(",%1.%2").arg(m_iAssoc_TrunkAddr).arg(ssl2[ic2]));
			else
				sstr = bncs_string("%1.%2").arg(m_iAssoc_TrunkAddr).arg(ssl2[ic2]);
		}
		if (sstr.length() == 0) sstr = bncs_string("%1.0").arg(m_iAssoc_TrunkAddr);
		if (ii == 0) m_element1 = sstr;
		if (ii == 1) m_element2 = sstr;
		if (ii == 2) m_element3 = sstr;
		if (ii == 3) m_element4 = sstr;
		if (ii == 4) m_element5 = sstr;
	}
}


int CRiedel_PTI::getRecordIndex(void)
{
	return m_iRecordIndex;
}


int CRiedel_PTI::getDevice(void)
{
	return m_iRevDevice;
}

int CRiedel_PTI::getSlot(void)
{
	return m_iRevSlot;
}

int CRiedel_PTI::getTrunkAddress(void)
{
	return m_iAssoc_TrunkAddr;
}

bncs_string CRiedel_PTI::getRawRiedelRevertive(void)
{
	return m_ss_Data;
}

bncs_string CRiedel_PTI::getRevertiveElement(int iElement)
{
	switch (iElement) {
		case 1: return m_element1; break;
		case 2: return m_element2; break;
		case 3: return m_element3; break;
		case 4: return m_element4; break;
		case 5: return m_element5; break;
	}
	// else
	return bncs_string("0");
}
