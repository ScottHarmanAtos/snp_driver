// RouterData.cpp: implementation of the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RouterDestData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRouterDestData::CRouterDestData( int iIndx )
{
	m_iMapIndex=iIndx;
	m_iRoutedIndex=0;

	m_iTracedGrd = 0;
	m_iTracedRing = 0;
	m_iTracedSource = 0;

	m_isDestHighway = 0;
	m_iPendingSource = 0;

	m_iLockState = 0;
	m_ss_ReasonTimeStamp = "";

	m_iWhichPortsInfodriver = 0;
	m_iWhichPortsSlot = 0;

	m_iUsedasOutputinIFB = 0;
	m_iUsedasOutputinMIXER = 0;

}

CRouterDestData::~CRouterDestData()
{
	// fade into distance
}
