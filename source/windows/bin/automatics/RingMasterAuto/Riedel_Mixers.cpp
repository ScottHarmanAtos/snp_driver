#include "Riedel_Mixers.h"


CRiedel_Mixers::CRiedel_Mixers(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk)
{
	m_iRecordIndex = iRecIndex;
	m_iRevDevice = iDevNum;
	m_iRevSlot = iRevSlot;
	m_splitChar = sc;
	m_iRevertive_TrunkAddr = iAssocTrunk;  // index into ring class

	bncs_stringlist m_ssl_Data = bncs_stringlist("", sc); // raw revertive data split by char

	m_ss_Inputs = "";
	m_ss_Outputs = "";
	m_ss_PreviousInputs = "";
	m_ss_PreviousOutputs = "";
}


CRiedel_Mixers::~CRiedel_Mixers()
{
}

void CRiedel_Mixers::storeRiedelData(bncs_string ss)
{
	m_ssl_Data.clear();
	m_ssl_Data = bncs_stringlist(ss, m_splitChar);
	int iCount = m_ssl_Data.count();
	if (iCount == 0) {
		m_ss_PreviousInputs = m_ss_Inputs;
		m_ss_Inputs = "";
		m_ss_PreviousOutputs = m_ss_Outputs;
		m_ss_Outputs = "";
		return;
	}
	if (iCount > 0) {
		m_ss_PreviousInputs = m_ss_Inputs;
		m_ss_Inputs = m_ssl_Data[0];
	}
	if (iCount > 1) {
		m_ss_PreviousOutputs = m_ss_Outputs;
		m_ss_Outputs = m_ssl_Data[1];
	}
	
}

int CRiedel_Mixers::getRecordIndex(void)
{
	return m_iRecordIndex;
}

int CRiedel_Mixers::getDevice(void)
{
	return m_iRevDevice;
}

int CRiedel_Mixers::getSlot(void)
{
	return m_iRevSlot;
}

int CRiedel_Mixers::getTrunkAddress(void)
{
	return m_iRevertive_TrunkAddr;
}

bncs_string CRiedel_Mixers::getRawRiedelRevertive(void)
{
	return m_ssl_Data.toString(m_splitChar);
}

bncs_string CRiedel_Mixers::getMixer_Inputs(void)
{
	return m_ss_Inputs;
}

bncs_string CRiedel_Mixers::getMixer_Outputs(void)
{
	return m_ss_Outputs;
}

bncs_string CRiedel_Mixers::getMixer_PreviousRev(int iWhichType)
{
	if (iWhichType == 1) return m_ss_PreviousInputs;
	if (iWhichType == 2) return m_ss_PreviousOutputs;
	return "";
}

BOOL CRiedel_Mixers::AreChangesinRevertive(int iWhichType)
{
	if (iWhichType == 1) {
		if (m_ss_PreviousInputs.length() != m_ss_Inputs.length()) {
			return TRUE;
		}
		else if (m_ss_PreviousInputs != m_ss_Inputs)
			return TRUE;
	}
	else if (iWhichType == 2) {
		if (m_ss_PreviousOutputs.length() != m_ss_Outputs.length()) {
			return TRUE;
		}
		else if (m_ss_PreviousOutputs != m_ss_Outputs)
			return TRUE;
	}
	return FALSE; 
}



