#include "Riedel_Rings.h"


CRiedel_Rings::CRiedel_Rings(int iKeyIndex, int iTrunkAddr, bncs_string ssName )
{
	m_iRecordIndex = iKeyIndex;
	m_iTrunkAddress = iTrunkAddr;   
	ssRingName = ssName;

	ssRing_Instance_GRD = "";
	ssRing_Instance_Monitor = "";
	ssRing_Instance_Conference = "";
	ssRing_Instance_IFB = "";
	ssRing_Instance_Extras = "";
	ssAProcessString = "";

	iRing_Device_GRD=0;
	iRing_Device_Monitor=0;
	iRing_Device_Conference=0;
	iRing_Device_IFB = 0;
	iRing_Device_PTI = 0;
	iRing_Device_Extras=0;

	iRing_Ports_Talk = 0;
	iRing_Ports_Listen = 0;
	iRing_RevsRecord_Monitors = 0;
	iRing_RevsRecord_IFBs = 0;
	iRing_RevsRecord_Conferences = 0;
	iRing_RevsRecord_Trunks = 0;
	iRing_RevsRecord_Mixers = 0;
	iRing_RevsRecord_Logic = 0;
	iRing_RevsRecord_PortsPTI = 0;

	iCurrentTrunksInUse = 0;
	iDefinedTrunksAvailable = 0;

	iRingAlarmWarningState = 0;
	ssRing_Threshold_Message = "";
	ssRing_Error_Message = "";

	ssl_HighwaysFromDestsGoingToRing = bncs_stringlist("", '|');   // each element will be a comma delim list of highway indicies from each ring
	ssl_HighwaysIntoSourcesFromRing = bncs_stringlist("", '|');    
	for (int ii = 0; ii < MAX_RIEDEL_RINGS; ii++) {
		iOutgoingHighways[ii] = 0;
		iIncomingHighways[ii] = 0;
		ssl_HighwaysFromDestsGoingToRing.append("0");
		ssl_HighwaysIntoSourcesFromRing.append("0");
		//
		iOutgoingMonitoring[ii] = 0;
		iIncomingMonitoring[ii] = 0;
		ssl_MonitoringFromDestsGoingToRing.append("0");
		ssl_MonitoringIntoSourcesFromRing.append("0");
	}

}


CRiedel_Rings::~CRiedel_Rings()
{
	// fade to black
}

// loaded up at init time
void CRiedel_Rings::addHighwayFromDestIntoRing(int iToRing, int iHighwayIndx)
{
	if ((iToRing > 0) && (iToRing < MAX_RIEDEL_RINGS)) {
		iOutgoingHighways[iToRing]++;
		if (iToRing <= ssl_HighwaysFromDestsGoingToRing.count()) { // not zero based by the way here
			bncs_string sstr = ssl_HighwaysFromDestsGoingToRing[iToRing];
			if (sstr == "0") {
				sstr = ""; // remove 0 as first addition
				sstr.append(bncs_string(iHighwayIndx));
			}
			else {
				// later additions
				sstr.append(",");
				sstr.append(bncs_string(iHighwayIndx)); // build comma delim list
			}
			ssl_HighwaysFromDestsGoingToRing[iToRing] = sstr;
		}
	}
}

void CRiedel_Rings::addHighwayIntoSourceFromRing(int iFromRing, int iHighwayIndx)
{
	if ((iFromRing > 0) && (iFromRing < MAX_RIEDEL_RINGS)) {
		iIncomingHighways[iFromRing]++;
		if (iFromRing <= ssl_HighwaysIntoSourcesFromRing.count()) { // not zero based by the way here
			bncs_string sstr = ssl_HighwaysIntoSourcesFromRing[iFromRing];
			if (sstr == "0") {
				sstr = ""; // remove 0 as first addition
				sstr.append(bncs_string(iHighwayIndx));
			}
			else {
				// later additions
				sstr.append(",");
				sstr.append(bncs_string(iHighwayIndx)); // build comma delim list
			}
			ssl_HighwaysIntoSourcesFromRing[iFromRing] = sstr;
		}
	}
}


// loaded up at init time
void CRiedel_Rings::addMonitoringFromDestIntoRing(int iToRing, int iRestHighwayIndx)
{
	if ((iToRing > 0) && (iToRing < MAX_RIEDEL_RINGS)) {
		iOutgoingMonitoring[iToRing]++;
		if (iToRing <= ssl_MonitoringFromDestsGoingToRing.count()) { // not zero based by the way here
			bncs_string sstr = ssl_MonitoringFromDestsGoingToRing[iToRing];
			if (sstr == "0") {
				sstr = ""; // remove 0 as first addition
				sstr.append(bncs_string(iRestHighwayIndx));
			}
			else {
				// later additions
				sstr.append(",");
				sstr.append(bncs_string(iRestHighwayIndx)); // build comma delim list
			}
			ssl_MonitoringFromDestsGoingToRing[iToRing] = sstr;
		}
	}
}

void CRiedel_Rings::addMonitoringIntoSourceFromRing(int iFromRing, int iRestHighwayIndx)
{
	if ((iFromRing > 0) && (iFromRing < MAX_RIEDEL_RINGS)) {
		iIncomingHighways[iFromRing]++;
		if (iFromRing <= ssl_MonitoringIntoSourcesFromRing.count()) { // not zero based by the way here
			bncs_string sstr = ssl_MonitoringIntoSourcesFromRing[iFromRing];
			if (sstr == "0") {
				sstr = ""; // remove 0 as first addition
				sstr.append(bncs_string(iRestHighwayIndx));
			}
			else {
				// later additions
				sstr.append(",");
				sstr.append(bncs_string(iRestHighwayIndx)); // build comma delim list
			}
			ssl_MonitoringIntoSourcesFromRing[iFromRing] = sstr;
		}
	}
}


// used during run time

bncs_string CRiedel_Rings::getOutgoingHighways(int iToWhichRing)
{
	// NOTE - works on TRUNK ADDRESS as index
	if ((iToWhichRing > 0) && (iToWhichRing <= ssl_HighwaysFromDestsGoingToRing.count())) { // not zero based by the way here
		bncs_string ssret = ssl_HighwaysFromDestsGoingToRing[iToWhichRing];
		if (ssret == "0") ssret = "";   // to resolve where no highways have been assigned
		return ssret ;
	}
	return "";
}

bncs_string CRiedel_Rings::getAllIncomingHighways(void)
{
	bncs_string sstr = ssl_HighwaysIntoSourcesFromRing.toString(',');
	return sstr;
}


bncs_string CRiedel_Rings::getIncomingHighways(int iFromWhichRing)
{
	// NOTE - works on TRUNK ADDRESS as index
	if ((iFromWhichRing > 0) && (iFromWhichRing <= ssl_HighwaysIntoSourcesFromRing.count())) { // not zero based by the way here
		return ssl_HighwaysIntoSourcesFromRing[iFromWhichRing];
	}
	return "";
}

int CRiedel_Rings::getNumberOutgoingHighways(int iToWhichRing)
{
	// NOTE - works on TRUNK ADDRESS as index
	if ((iToWhichRing > 0) && (iToWhichRing < MAX_RIEDEL_RINGS)) {
		return iOutgoingHighways[iToWhichRing];
	}
	else
		return 0;
}

int CRiedel_Rings::getNumberIncomingHighways(int iFromWhichRing)
{
	// NOTE - works on TRUNK ADDRESS as index
	if ((iFromWhichRing > 0) && (iFromWhichRing < MAX_RIEDEL_RINGS)) {
		return iIncomingHighways[iFromWhichRing];
	}
	else
		return 0;
}


// restircted highways from / into this ring 

bncs_string CRiedel_Rings::getOutgoingMonitoring(int iToWhichRing)
{
	// NOTE - works on TRUNK ADDRESS as index
	if ((iToWhichRing > 0) && (iToWhichRing <= ssl_MonitoringFromDestsGoingToRing.count())) { // not zero based by the way here
		bncs_string ssret = ssl_MonitoringFromDestsGoingToRing[iToWhichRing];
		if (ssret == "0") ssret = "";   // to resolve where no Monitoring have been assigned
		return ssret;
	}
	return "";
}

bncs_string CRiedel_Rings::getIncomingMonitoring(int iFromWhichRing)
{
	// NOTE - works on TRUNK ADDRESS as index
	if ((iFromWhichRing > 0) && (iFromWhichRing <= ssl_MonitoringIntoSourcesFromRing.count())) { // not zero based by the way here
		return ssl_MonitoringIntoSourcesFromRing[iFromWhichRing];
	}
	return "";
}

bncs_string CRiedel_Rings::getAllIncomingMonitoring(void)
{
	bncs_string sstr = ssl_MonitoringIntoSourcesFromRing.toString(',');
	return sstr;
}

int CRiedel_Rings::getNumberOutgoingMonitoring(int iToWhichRing)
{
	// NOTE - works on TRUNK ADDRESS as index
	if ((iToWhichRing > 0) && (iToWhichRing < MAX_RIEDEL_RINGS)) {
		return iOutgoingMonitoring[iToWhichRing];
	}
	else
		return 0;
}

int CRiedel_Rings::getNumberIncomingMonitoring(int iFromWhichRing)
{
	// NOTE - works on TRUNK ADDRESS as index
	if ((iFromWhichRing > 0) && (iFromWhichRing < MAX_RIEDEL_RINGS)) {
		return iIncomingMonitoring[iFromWhichRing];
	}
	else
		return 0;
}
