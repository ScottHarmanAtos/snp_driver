#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class to hold REVERTIVE data from riedel drivers 
class CRiedel_Conferences
{
private:
	int m_iRecordIndex;
	int m_iRevDevice;
	int m_iRevConfSlot;   // records created from 1..500 slot index for confs -- 1000+ 2000+ use this same record
	char m_splitChar;
	int m_iAssoc_TrunkAddr;  // can be used to index into ring class for this revertive record for infodriver device

	bncs_stringlist m_ssl_Data; // raw data split by char

	bncs_string m_ss1_ConfMem;   // first of 4 revertives for riedel infodrv revs
	bncs_string m_ss2_ConfName;   // 2nd of 4 revertives for riedel infodrv revs
	bncs_string m_ss3_ConfPanelMem;   // 3rd of 4 revertives for riedel infodrv revs

public:
	CRiedel_Conferences(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk);
	~CRiedel_Conferences();

	void clearRiedelData(void);

	void storeRiedelData(bncs_string ss);
	void storeConfName(bncs_string ss);       // special to store conf name ( slots 1001-1350 )
	void storeConfPanelMem(bncs_string ss);  // special to store conf panel members ( slots 2001-2350 )

	int getRecordIndex(void);
	int getDevice(void);
	int getConfSlot(void);
	int getTrunkAddress(void);
	
	bncs_string getRawRiedelRevertive(void);
	bncs_string getConference_Members(void);
	bncs_string getConference_Name(void);
	bncs_string getConference_PanelMembers(void);

};

