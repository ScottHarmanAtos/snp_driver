#include "RingMaster_CONF.h"


CRingMaster_CONF::CRingMaster_CONF( int iIndex )
{
	m_iIndexNumber = iIndex;
	m_ss_RMstr_ConfMembers = "";
	m_ss_RMstr_PanelMembers = "";
	m_ss_RMstr_Label = "";
	m_iAssociatedTrunkAddress = 0;
	m_ssl_RMstr_FromMonitorMembers = bncs_stringlist("", ',');
	for (int ii = 0; ii < MAX_RIEDEL_RINGS; ii++) m_iRevRecordIndex[ii] = 0;
}


CRingMaster_CONF::~CRingMaster_CONF()
{
}

void CRingMaster_CONF::addRiedelRevIndex(int iTrunkAddr, int iRecordKey)
{
	if ((iTrunkAddr>0) && (iTrunkAddr < MAX_RIEDEL_RINGS)) {
		m_iRevRecordIndex[iTrunkAddr] = iRecordKey;
	}
}

int CRingMaster_CONF::getRiedelRevIndex(int iTrunkAddr)
{
	if ((iTrunkAddr>0) && (iTrunkAddr < MAX_RIEDEL_RINGS)) {
		return m_iRevRecordIndex[iTrunkAddr];
	}
	return 0;
}

bncs_string  CRingMaster_CONF::getAllRiedelRevIndicies(void)
{
	bncs_stringlist ssl = bncs_stringlist("", ',');
	for (int ii = 0; ii < MAX_RIEDEL_RINGS; ii++) ssl.append(m_iRevRecordIndex[ii]);
	return ssl.toString(',');
}


void CRingMaster_CONF::storeRingMasterConfMembers(bncs_string ssRev)
{
	m_ss_RMstr_ConfMembers = ssRev;
}

bncs_string CRingMaster_CONF::getRingMasterConfMembers()
{
	return m_ss_RMstr_ConfMembers;
}

void CRingMaster_CONF::storeRingMasterPanelMembers(bncs_string ssRev)
{
	m_ss_RMstr_PanelMembers = ssRev;
}

bncs_string CRingMaster_CONF::getRingMasterPanelMembers()
{
	return m_ss_RMstr_PanelMembers;
}

void CRingMaster_CONF::storeRingMasterLabel(bncs_string ssRev)
{
	m_ss_RMstr_Label = ssRev;
}

bncs_string CRingMaster_CONF::getRingMasterLabel()
{
	return m_ss_RMstr_Label;
}

void CRingMaster_CONF::addFromMonitorMember(bncs_string ssMem)
{
	int iPos = m_ssl_RMstr_FromMonitorMembers.find(ssMem);
	if (iPos < 0) {
		m_ssl_RMstr_FromMonitorMembers.append(ssMem);
	}
}

void CRingMaster_CONF::removeFromMonitorMember(bncs_string ssMem)
{
	int iPos = m_ssl_RMstr_FromMonitorMembers.find(ssMem);
	if (iPos>=0) {
		m_ssl_RMstr_FromMonitorMembers.deleteItem(iPos);
	}
}

bncs_string CRingMaster_CONF::getFromMonitorMembers(void)
{
	return m_ssl_RMstr_FromMonitorMembers.toString(',');
}


void CRingMaster_CONF::setAssociatedTrunkAddress(int iTrunkAddr)
{
	m_iAssociatedTrunkAddress = iTrunkAddr;
}

int CRingMaster_CONF::getAssociatedTrunkAddress(void)
{
	return m_iAssociatedTrunkAddress;
}



