#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

#include <map>
using namespace std;

#include "RouterDestData.h"
#include "RouterSrceData.h"
//typedef map<int, CRouterDestData*> MAP_RouterDestData;
//typedef map<int, CRouterSrceData*> MAP_RouterSrceData;

// class to hold REVERTIVE data from riedel drivers 
class CRiedel_GRDs
{
private:

	int m_iGRDDeviceNumber;     // riedel grd dev ini number
	int m_iMaxSources;                  // db0 size
	int m_iMaxDestinations;            // db1 size
	int m_iAssoc_Trunk;   // index into ring class  -- by trunk address

	int m_iRevertivesReceived;   // useful for first pass 

	map<int, CRouterDestData*> cl_DestRouterData;   // map for Destination data
	map<int, CRouterSrceData*> cl_SrcRouterData;    // map for source data

	//data maps are not seen outside class 
	CRouterSrceData* GetSourceData(int iSource);
	CRouterDestData* GetDestinationData(int iDestination);

public:
	CRiedel_GRDs(int iDevNum, int iMaxSrces, int iMaxDests, int iAssocTrunk);
	~CRiedel_GRDs();

	int getDevice(void);
	int getRingTrunk(void);
	int getMaximumDestinations(void);
	int getMaximumSources(void);

	void storeRiedelRevertive(int iDestination, int iSource);
	int getRoutedSourceForDestination(int iDestination);  // return revertive source as got from grd rev
	bncs_string getAllDestinationsForSource(int iSource);
	int getNumberDestinationsUsingSource(int iSource);

	void storeTracedRingSource(int iDestination, int iRing, int iGrdDev, int iSource);
	int getTracedGrdForDestination(int iDestination);       // if from another ring via BNCS controlled highways
	int getTracedRingForDestination(int iDestination);       // if from another ring via BNCS controlled highways
	int getTracedSourceForDestination(int iDestination); // if from another ring via BNCS controlled highways

	void storeAssocDestHighway(int iDestination, int iHigh);
	void storeAssocSrceHighway(int iSource, int iHigh);
	int getAssocDestHighway(int iDestination);
	int getAssocSrceHighway(int iSource);

	void changeLockStatus(int iDestination, int iState, bncs_string ssReason);
	void unlockDestination(int iDestination);
	int getLockStatus(int iDestination);
	bncs_string getLockReasonTimeStamp(int iDestination);
	int getNumberRevertivesReceived();

	void setDestinationMapping(int iDestination, int iInfodriver, int iSlotIndex);
	void getDestinationMapping(int iDestination, int* iInfo, int* iSlot);
	int getDestinationMappingInfoDrv(int iDestination);
	int getDestinationMappingInfoSlot(int iDestination);

	void setHighwayDestPendingSource(int iDestination, int iSource);
	int getHighwayDestPendingSource(int iDestination);
};

