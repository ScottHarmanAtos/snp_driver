#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// holds ring master explicit info for each IFB
// pointers into various IFB records for each of the rings

class CRingMaster_IFB
{
private:
	int m_RecordIndex;
	// keys into relevant revertive records
	int m_iRevRecordIndex;   // most likely 1:1 now 

	// IFBs have defualt / associated ring/trunk to which they belong
	int m_iAssociatedTrunkAddress;

	// associated indices when ifb is disabled -- also tied to the associated ring for ifb
	int m_iAssociatedListenToPort;                        // input port
	int m_iAssociatedMixMinusPort;
	int m_iAssociatedSpeakToPort;                       // output port
	int m_iAssociatedRemoteLogic;

	BOOL m_bCurrentProcessingOnThisIFB;         // flag to check if currently undergoing processing for this IFB ( assign or deassign )

	// any other data required for IFBs at ringmaster level rather than just ifb revertive 
	bncs_string m_ss_RMstr_Revertive;

	bncs_stringlist m_ssl_RMstr_IFBMonitorMembers;   // collated list of users of ifb as determined from monitor revs - same idea as conf

	bncs_stringlist m_ssl_RMstr_ListenToPortMonitorMembers;   // list of users of listen 2 Port from monitor revs for assoc listen to Port linked to IFB


public:
	CRingMaster_IFB(int iRecordIndex, int iTrunkAddress);
	~CRingMaster_IFB();

	void addRiedelRevIndex( int iRevRecIndex);
	int getRiedelRevIndex(void);

	void storeRingMasterRevertive(bncs_string ssRev);
	bncs_string getRingMasterRevertive(void);

	int getAssociatedTrunkAddress(void);

	void setAssociatedListenToPort(int iPort);
	int getAssociatedListenToPort(void);
	void setAssociatedMixMinusPort(int iPort);
	int getAssociatedMixMinusPort(void);
	void setAssociatedSpeakToPort(int iPort);
	int getAssociatedSpeakToPort(void);
	void setAssociatedRemoteLogic(int iLogic);
	int getAssociatedRemoteLogic(void);

	void addIFBMonitorMember(bncs_string ssMem);
	void removeIFBMonitorMember(bncs_string ssMem);
	bncs_string getIFBMonitorMembers(void);

	void addListen2PortMonitorMember(bncs_string ssMem);
	void removeListen2PortMonitorMember(bncs_string ssMem);
	bncs_string getListen2PortMonitorMembers(void);

	void setIFBProcessingFlag(BOOL bState);
	BOOL getIFBProcessingFlag(void);

};

