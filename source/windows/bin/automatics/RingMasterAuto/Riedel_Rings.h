#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class too hold info on ring info and connection to trunk navigator etc
// can be thought of as master record holding key info pertaining to ring for Automatic

class CRiedel_Rings
{
private:
	int iOutgoingHighways[MAX_RIEDEL_RINGS];
	bncs_stringlist ssl_HighwaysFromDestsGoingToRing;   // STANDARD highway record index leaving ring - all highways leaving this ring - but as list of lists on a TO ring basis 
	int iIncomingHighways[MAX_RIEDEL_RINGS];
	bncs_stringlist ssl_HighwaysIntoSourcesFromRing; // STANDARD highway index landing into this ring -- but is list of highways per From Ring lists 

	// monitoring highways 
	int iOutgoingMonitoring[MAX_RIEDEL_RINGS];
	bncs_stringlist ssl_MonitoringFromDestsGoingToRing;   // MONITORING highway record index leaving ring - all highways leaving this ring - but as list of lists on a TO ring basis 
	int iIncomingMonitoring[MAX_RIEDEL_RINGS];
	bncs_stringlist ssl_MonitoringIntoSourcesFromRing; // MONITORING highway index landing into this ring -- but is list of highways per From Ring lists 


public:
	CRiedel_Rings( int iKeyIndex, int iTrunkAddr, bncs_string ssName );
	~CRiedel_Rings();

	int m_iRecordIndex;
	int m_iTrunkAddress;
	bncs_string ssRingName;
	BOOL m_bLocalRing;    // ring in local hub 

	bncs_string ssRing_Instance_GRD;
	int iRing_Device_GRD;
	bncs_string ssRing_Instance_Monitor;
	int iRing_Device_Monitor;
	bncs_string ssRing_Instance_Conference;
	int iRing_Device_Conference;
	bncs_string ssRing_Instance_IFB;
	int iRing_Device_IFB;
	bncs_string ssRing_Instance_PTI;
	int iRing_Device_PTI;
	bncs_string ssRing_Instance_Extras;  // within extras infodriver for trunk usage data
	int iRing_Device_Extras;

	int iRing_Ports_Talk, iRing_Ports_Listen;  // src, dest v -- grd size

	int iRing_RevsRecord_Monitors;   // start riedel revs record index w.r.t. this ring
	int iRing_RevsRecord_IFBs;
	int iRing_RevsRecord_Conferences;
	int iRing_RevsRecord_Trunks;  // start riedel revs record index w.r.t. this ring
	int iRing_RevsRecord_Mixers;
	int iRing_RevsRecord_Logic;
	int iRing_RevsRecord_PortsPTI;

	int iCurrentTrunksInUse, iDefinedTrunksAvailable;

	int iRingAlarmWarningState;
	bncs_string ssRing_Threshold_Message;   // calc during housekeeping 
	bncs_string ssRing_Error_Message;

	bncs_string ssAProcessString;                     // a string to store temp data for a given ring - public for ease of use

	// loaded up at init time
	void addHighwayFromDestIntoRing(int iIntoRing, int iHighwayIndx);
	void addHighwayIntoSourceFromRing(int iFromRing, int iHighwayIndx);
	void addMonitoringFromDestIntoRing(int iIntoRing, int iRestHighwayIndx);
	void addMonitoringIntoSourceFromRing(int iFromRing, int iRestHighwayIndx);

	// used during run time
	bncs_string getOutgoingHighways(int iToWhichRing);
	int getNumberOutgoingHighways(int iToWhichRing);

	bncs_string getAllIncomingHighways(void);
	bncs_string getIncomingHighways(int iFromWhichRing);
	int getNumberIncomingHighways(int iFromWhichRing);

	// Monitoring
	bncs_string getOutgoingMonitoring(int iToWhichRing);
	int getNumberOutgoingMonitoring(int iToWhichRing);

	bncs_string getAllIncomingMonitoring(void);
	bncs_string getIncomingMonitoring(int iFromWhichRing);
	int getNumberIncomingMonitoring(int iFromWhichRing);

};

