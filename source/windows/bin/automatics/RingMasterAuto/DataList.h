
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786)
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"
#include "RingMasterConsts.h"

// generic structure to hold some data - created at startup for global use -- but initially for ifb commands - 

class CDataList
{
private:
	int iRecordIndexRing;
	int iRecordTrunk;

	// data storage
	int iDataInteger;
	bncs_string ss_datastring;
	bncs_stringlist ssl_datalist1, ssl_datalist2, ssl_datalist3 ;

public:
	CDataList( int iIndex, int iTrunk );
	~CDataList();

	void clearAllData();
	void setDataInt(int iValue);
	void setDataString(bncs_string ssValue);
	void addToDataList(int iWhichlist, bncs_string ssAddThis);

	int getRecordTrunk(void);
	int getDataInt(void);
	bncs_string getDataString(void);
	bncs_string getDataList(int iWhichList);

};

