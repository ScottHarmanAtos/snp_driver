#include "Riedel_Logic.h"


CRiedel_Logic::CRiedel_Logic(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk)
{
	m_iRecordIndex = iRecIndex;
	m_iRevDevice = iDevNum;
	m_iRevSlot = iRevSlot;
	m_splitChar = sc;
	m_iRevertive_TrunkAddr = iAssocTrunk;  // index into ring class
	m_iLogicState = 0; // raw revertive data 
}


CRiedel_Logic::~CRiedel_Logic()
{
}


void CRiedel_Logic::storeRiedelData(bncs_string ss)
{
	if (ss.length()>0) 
		m_iLogicState = ss.toInt();
	else
		m_iLogicState = 0; 
}

int CRiedel_Logic::getRecordIndex(void)
{
	return m_iRecordIndex;
}

int CRiedel_Logic::getDevice(void)
{
	return m_iRevDevice;
}

int CRiedel_Logic::getSlot(void)
{
	return m_iRevSlot;
}

int CRiedel_Logic::getTrunkAddress(void)
{
	return m_iRevertive_TrunkAddr;
}

int CRiedel_Logic::getLogicState(void)
{
	return m_iLogicState;
}




