#include "Riedel_IFBs.h"


CRiedel_IFBs::CRiedel_IFBs(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk)
{
	m_iRecordIndex = iRecIndex;
	m_iRevDevice = iDevNum;
	m_iRevSlot = iRevSlot;
	m_splitChar = sc;
	m_iRevertive_TrunkAddr = iAssocTrunk;  // index into ring class

	m_ssl_Data = bncs_stringlist("", sc); // raw data split by char

	m_ss1_IFBInput="";   
	m_ss2_IFBMixMinus="";   
	m_ss3_IFBOutput = "";
	m_ss1_PreviousInputs = "";
	m_ss2_PreviousMixMinus = "";
	m_ss3_PreviousOutputs = "";
	m_ss4_IFBLabel="";   
}

CRiedel_IFBs::~CRiedel_IFBs()
{
	// go ahead punk, make my day
}

void CRiedel_IFBs::clearRiedelData(void)
{
	m_ss1_IFBInput = "";
	m_ss2_IFBMixMinus = "";
	m_ss3_IFBOutput = "";
	m_ss4_IFBLabel = "";
}

void CRiedel_IFBs::storeRiedelData(bncs_string ss)
{
	m_ssl_Data.clear();
	m_ssl_Data = bncs_stringlist(ss, m_splitChar);
	int iCount = m_ssl_Data.count();
	if (iCount == 0) {
		clearRiedelData();
		return;
	}
	if (iCount > 0) {
		m_ss1_PreviousInputs = m_ss1_IFBInput;
		m_ss1_IFBInput = m_ssl_Data[0];
	}
	if (iCount > 1) {
		m_ss2_PreviousMixMinus = m_ss2_IFBMixMinus;
		m_ss2_IFBMixMinus = m_ssl_Data[1];
	}
	if (iCount > 2) {
		m_ss3_PreviousOutputs = m_ss3_IFBOutput;
		m_ss3_IFBOutput = m_ssl_Data[2];
	}
	if (iCount>3) m_ss4_IFBLabel = m_ssl_Data[3];
}

int CRiedel_IFBs::getRecordIndex(void)
{
	return m_iRecordIndex;
}

int CRiedel_IFBs::getDevice(void)
{
	return m_iRevDevice;
}

int CRiedel_IFBs::getSlot(void)
{
	return m_iRevSlot;
}

int CRiedel_IFBs::getTrunkAddress(void)
{
	return m_iRevertive_TrunkAddr;
}

bncs_string CRiedel_IFBs::getRawRiedelRevertive(void)
{
	return m_ssl_Data.toString(m_splitChar);
}

bncs_string CRiedel_IFBs::getIFB_Inputs(void)
{
	return m_ss1_IFBInput;
}

bncs_string CRiedel_IFBs::getIFB_MixMinus(void)
{
	return m_ss2_IFBMixMinus;
}

bncs_string CRiedel_IFBs::getIFB_Outputs(void)
{
	return m_ss3_IFBOutput;
}

bncs_string CRiedel_IFBs::getIFB_Label(void)
{
	return m_ss4_IFBLabel;
}

bncs_string CRiedel_IFBs::getIFB_PreviousRev(int iWhichType)
{
	if (iWhichType == 1) return m_ss1_PreviousInputs;
	if (iWhichType == 2) return m_ss2_PreviousMixMinus;
	if (iWhichType == 3) return m_ss3_PreviousOutputs;
	return "";
}
