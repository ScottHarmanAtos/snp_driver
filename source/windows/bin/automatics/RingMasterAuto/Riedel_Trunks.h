#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class to hold REVERTIVE data from riedel drivers 
class CRiedel_Trunks
{
private:
	int m_iRecordIndex;
	int m_iRevDevice;
	int m_iRevSlot;
	char m_splitChar;
	int m_iAssoc_Ring;  // index into ring class
	bncs_string m_ssraw_rev;

public:
	CRiedel_Trunks(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocRing);
	~CRiedel_Trunks();

	int getRecordIndex(void);
	int getDevice(void);
	int getSlot(void);
	int getRing(void);
	int getTrunkAddress(void);

	void storeRiedelData(bncs_string ss);
	bncs_string getRawRiedelRevertive(void);

};

