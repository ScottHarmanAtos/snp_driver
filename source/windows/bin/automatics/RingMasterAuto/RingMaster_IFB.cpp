#include "RingMaster_IFB.h"


CRingMaster_IFB::CRingMaster_IFB(int iIndex, int iTrunkAddress)
{
	m_RecordIndex = iIndex;
	m_iRevRecordIndex = iIndex;   // happens to be now in V2
	m_ss_RMstr_Revertive = "0|0|0|";
	m_iAssociatedTrunkAddress = iTrunkAddress;
	m_ssl_RMstr_IFBMonitorMembers = bncs_stringlist("", ',');
	m_ssl_RMstr_ListenToPortMonitorMembers = bncs_stringlist("", ',');

	m_iAssociatedListenToPort = 0;
	m_iAssociatedMixMinusPort = 0;
	m_iAssociatedSpeakToPort = 0;
	m_iAssociatedRemoteLogic = 0;
	m_bCurrentProcessingOnThisIFB = FALSE;

}


CRingMaster_IFB::~CRingMaster_IFB()
{
}


void CRingMaster_IFB::addRiedelRevIndex( int iRevRecIndex)
{
	m_iRevRecordIndex= iRevRecIndex;
}

int CRingMaster_IFB::getRiedelRevIndex(void)
{
	return m_iRevRecordIndex;
}

void CRingMaster_IFB::storeRingMasterRevertive(bncs_string ssRev)
{
	m_ss_RMstr_Revertive = ssRev;
}

bncs_string CRingMaster_IFB::getRingMasterRevertive()
{
	return m_ss_RMstr_Revertive;
}


int CRingMaster_IFB::getAssociatedTrunkAddress(void)
{
	return m_iAssociatedTrunkAddress;
}

void CRingMaster_IFB::setAssociatedListenToPort(int iPortAddr)
{
	m_iAssociatedListenToPort = iPortAddr;
}

int CRingMaster_IFB::getAssociatedListenToPort(void)
{
	return m_iAssociatedListenToPort;
}

void CRingMaster_IFB::setAssociatedMixMinusPort(int iPortAddr)
{
	m_iAssociatedMixMinusPort = iPortAddr;
}

int CRingMaster_IFB::getAssociatedMixMinusPort(void)
{
	return m_iAssociatedMixMinusPort;
}

void CRingMaster_IFB::setAssociatedSpeakToPort(int iPortAddr)
{
	m_iAssociatedSpeakToPort = iPortAddr;
}

int CRingMaster_IFB::getAssociatedSpeakToPort(void)
{
	return m_iAssociatedSpeakToPort;
}

void CRingMaster_IFB::setAssociatedRemoteLogic(int iLogic)
{
	m_iAssociatedRemoteLogic = iLogic;
}

int CRingMaster_IFB::getAssociatedRemoteLogic(void)
{
	return m_iAssociatedRemoteLogic;
}


void CRingMaster_IFB::setIFBProcessingFlag(BOOL bState)
{
	m_bCurrentProcessingOnThisIFB = bState;
}

BOOL CRingMaster_IFB::getIFBProcessingFlag(void)
{
	return m_bCurrentProcessingOnThisIFB;
}


void CRingMaster_IFB::addIFBMonitorMember(bncs_string ssMem)
{
	int iPos = m_ssl_RMstr_IFBMonitorMembers.find(ssMem);
	if (iPos < 0) {
		m_ssl_RMstr_IFBMonitorMembers.append(ssMem);
	}
}

void CRingMaster_IFB::removeIFBMonitorMember(bncs_string ssMem)
{
	int iPos = m_ssl_RMstr_IFBMonitorMembers.find(ssMem);
	if (iPos >= 0) {
		m_ssl_RMstr_IFBMonitorMembers.deleteItem(iPos);
	}
}

bncs_string CRingMaster_IFB::getIFBMonitorMembers(void)
{
	return m_ssl_RMstr_IFBMonitorMembers.toString(',');
}


void CRingMaster_IFB::addListen2PortMonitorMember(bncs_string ssMem)
{
	int iPos = m_ssl_RMstr_ListenToPortMonitorMembers.find(ssMem);
	if (iPos < 0) {
		m_ssl_RMstr_ListenToPortMonitorMembers.append(ssMem);
	}
}

void CRingMaster_IFB::removeListen2PortMonitorMember(bncs_string ssMem)
{
	int iPos = m_ssl_RMstr_ListenToPortMonitorMembers.find(ssMem);
	if (iPos >= 0) {
		m_ssl_RMstr_ListenToPortMonitorMembers.deleteItem(iPos);
	}
}

bncs_string CRingMaster_IFB::getListen2PortMonitorMembers(void)
{
	return m_ssl_RMstr_ListenToPortMonitorMembers.toString(',');
}

