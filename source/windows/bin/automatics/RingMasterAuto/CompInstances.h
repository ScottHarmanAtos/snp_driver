#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

#include "RingMasterConsts.h"

class CCompInstances
{
public:
	CCompInstances(bncs_string ssInst);
	~CCompInstances();
	// composites
	bncs_string ssOverallComposite;

	// mains instances - first infodriver from each of composite groups - Rmstr init uses to connect to all infodrivers
	bncs_string ssRmstr_Main_Info;
	bncs_string ssRmstr_Confs_Info;
	bncs_string ssRmstr_IFB_Info[MAX_RIEDEL_RINGS];
	bncs_string ssRmstr_Ports_Info[MAX_RIEDEL_RINGS];
	bncs_string ssRmstr_PortsPTI;

	int i_dev_Rmstr_Main;
	int i_dev_Rmstr_Confs;
	int i_dev_Rmstr_IFBs[MAX_RIEDEL_RINGS];
	int i_dev_Rmstr_Ports[MAX_RIEDEL_RINGS];
	int i_dev_Rmstr_PortsPTI;

};

