#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// holds ring master conference specific information
class CRingMaster_CONF
{
private:
	int m_iIndexNumber;
	int m_iRevRecordIndex[MAX_RIEDEL_RINGS];
	
	// assign defualt / associated ring/trunk to which they belong - or most used with - for backward compatability
	int m_iAssociatedTrunkAddress;

	// any other fields exclusive for conferences that revs from each ring cannot hold
	bncs_string m_ss_RMstr_ConfMembers;
	bncs_string m_ss_RMstr_PanelMembers;                         // not really used anywhere - monitor members are more interesting
	bncs_stringlist m_ssl_RMstr_FromMonitorMembers;   // collated list of users of conf as determined from monitor revs
	bncs_string m_ss_RMstr_Label;

public:
	CRingMaster_CONF( int iIndex );
	~CRingMaster_CONF();

	void addRiedelRevIndex(int iTrunkAddr, int iRecordKey);
	int getRiedelRevIndex(int iTrunkAddr);
	bncs_string  getAllRiedelRevIndicies(void);

	void storeRingMasterConfMembers(bncs_string ssRev);
	void storeRingMasterPanelMembers(bncs_string ssRev);
	void storeRingMasterLabel(bncs_string ssRev);

	bncs_string getRingMasterConfMembers(void);
	bncs_string getRingMasterPanelMembers(void);
	bncs_string getRingMasterLabel(void);

	void addFromMonitorMember(bncs_string ssMem);
	void removeFromMonitorMember(bncs_string ssMem);
	bncs_string getFromMonitorMembers(void);

	void setAssociatedTrunkAddress(int iTrunk);
	int getAssociatedTrunkAddress(void);

};

