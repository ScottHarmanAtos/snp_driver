#include "MCRMonPortRecord.h"


CMCRMonPortRecord::CMCRMonPortRecord(int iIndx, int iTrunk, int iGivenPort, int iSlotIndx, int iTranslatedPort)
{
	m_iRecordIndex = iIndx;
	m_iMonTrunkAddr = iTrunk;
	m_iConfigMonPort = iGivenPort;
	m_iMonPortTranslated = iTranslatedPort;
	m_iAssoc_RingMaster_Slot = iSlotIndx;

	m_ssCommandRevertive = "";
	m_iUsingHighwayRecord = 0;
	m_iWorkingModeForHighway = 0;
}


CMCRMonPortRecord::~CMCRMonPortRecord()
{
	//erase
}
