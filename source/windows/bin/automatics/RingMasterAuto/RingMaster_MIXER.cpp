#include "RingMaster_MIXER.h"


CRingMaster_MIXER::CRingMaster_MIXER(int iIndex, int iTrunkAddr)
{
	m_RecordIndex = iIndex;
	m_iRevRecordIndex = iIndex; 
	m_ss_RMstr_Revertive = "0:MUTE|0";
	m_iAssociatedTrunkAddress = iTrunkAddr;
}

CRingMaster_MIXER::~CRingMaster_MIXER()
{
}

void CRingMaster_MIXER::addRiedelRevIndex(int iRevRecIndex)
{
	m_iRevRecordIndex = iRevRecIndex;
}

int CRingMaster_MIXER::getRiedelRevIndex(void)
{
	return m_iRevRecordIndex;
}

void CRingMaster_MIXER::storeRingMasterRevertive(bncs_string ssRev)
{
	m_ss_RMstr_Revertive = ssRev;
}

bncs_string CRingMaster_MIXER::getRingMasterRevertive()
{
	return m_ss_RMstr_Revertive;
}


int CRingMaster_MIXER::getAssociatedTrunkAddress(void)
{
	return m_iAssociatedTrunkAddress;
}

