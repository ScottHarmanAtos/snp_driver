//////////////////////////////////////
// header file for RingMasterAuto project //
//////////////////////////////////////

#if !defined(AFX_RingMasterAuto_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
#define AFX_RingMasterAuto_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996 4244)

#include "resource.h"
#include "RingMasterConsts.h"

/* main.cpp */
/* prototypes for main functionality */
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT				InitApp(HWND);
void				CloseApp(HWND);

void				LoadIni(void);
char *				getBNCS_File_Path( void );    // new for v4.2 of wizard - assigns the global szBNCS_File_Path
char*				r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite);
void				w_p(char* file, char* section, char* entry, char* setting);
int					getWS(void);

void				Debug(LPCSTR szFmt, ...);
void				Log(LPCSTR szDate);

int				SplitString(char *string, char *delim, UINT count, char **outarray );
void          InitStatusListboxDialog( void );  // helper function to initialise listbox on app dialog

BOOL SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight);

/* RingMasterAuto.cpp /*

/* prototypes for the notification functions */
void UpdateCounters(void);
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot);
// LRESULT CSIDriverNotify(extdriver* ped,LPCSTR szMsg);
LRESULT CSIClientNotify(extclient* pec,LPCSTR szMsg);

void ClearCommandQueue(void);
void AddCommandToQue(LPCSTR szCmd, int iCmdType, int iDev, int iSlot, int iDB);
void ProcessNextCommand(int iRateRevs);

//void ClearRevertiveQueue(void);
//void AddRevertiveToQue(LPCSTR szCmd, int iCmdType, int iDev, int iSlot, int iDB);
//void ProcessNextRevertive(int iRateRevs);

BOOL LoadAllInitialisationData( void );

BOOL ConnectAllInfodrivers( void );
void ProcessFirstPass_Logic( void );
void ProcessHighwayManagement(void);
void CalculateHighwayStatistics();
void ParkHighway(int iGivenHighway);
void ForceParkHighway(int iGivenHighway);
void ForceParkMcrMonHighway(int iHighway);

void PostWarningMessage(int iFromTrunk, int iHighway, bncs_string ssMessage);
void PostAutomaticErrorMessage(int iFromTrunk, bncs_string ssMessage);

BOOL AutomaticRegistrations(void);
void PollAutomaticDrivers( int iCounter );
void VersionInfo();
void CheckAndSetInfoResilience( void );
void ForceAutoIntoRXOnlyMode( void );
void ForceAutoIntoTXRXMode( void );
void Restart_CSI_NI_NO_Messages(void);

void ClearAutomaticMaps();
CRiedel_Rings* GetRiedel_Rings_Record(int iIndex);

void DisplayDestPortData();
void DisplayHighwayData();
void DisplayRingInformation();
void DisplayMonitorPortData();
void DisplayMcrMonHighwayData();

void DisplayIFBData();
void DisplayLogicData();
void DisplayMixerData();
void DisplayTrunkingData();
void DisplayConferenceData();

/*function to create nice new paint brushes*/
void CreateBrushes();
/*function to clean paint brushes and put them away*/
void ReleaseBrushes();

void SendOutClosingMessage();

#ifndef EXT
#define EXT extern
#endif

/////////////////////////////////////////////////////////////////////
// Global Variables for the RingMasterAuto project
/////////////////////////////////////////////////////////////////////

EXT HINSTANCE hInst;						// current instance
EXT TCHAR szTitle[MAX_LOADSTRING];			// The title bar text
EXT TCHAR szWindowClass[MAX_LOADSTRING];	// The title bar text
EXT char szBNCS_File_Path[MAX_AUTOBFFR_STRING]; // global for Colledia system file path - assigned in getBNCS_File_Path, used in r_p and w_p
EXT char szBNCS_Log_File_Path[MAX_AUTOBFFR_STRING];  // global for crash log file path - assigned in getBNCS_File_Path
EXT char szAppName[ MAX_LOADSTRING];                     // application name used in crash log messages

EXT char szClientCmd[MAX_AUTOBFFR_STRING];						// global for commands via ecclient
EXT char szResult[MAX_AUTOBFFR_STRING];						// global for profile reads and infodriver updates
EXT char szRMData[512];														// global for RM handling and r_p database reads
EXT char szDebug[1000];						// global string data for debug msgs
EXT char szLogDbg[1100];

EXT BOOL fLog;								// log enable flag
EXT int iDebugFlag;							   // debug enable flag
EXT BOOL fDebugHide;						// debug visible flag
EXT BOOL fDbgView;							   // debug view outputstring enable flag
EXT HWND hWndMain,hWndList;					// window handles, main and debug
EXT HWND hWndDlg;							// window handle for child dialog
EXT int iChildHeight;						// height of child dialog
EXT RECT rc;
EXT int iDevice;							// command line parameter = base info device number from wich all 4 infos increment
EXT int iWorkstation;						// workstation number from CSI.INI

EXT ULONG lTXID;							// ID tx count
EXT ULONG lRXID;							// ID rx count

EXT extclient *ecCSIClient;				// a client connection class

// - V 2 can use increasing number based on number of rings defined 
// all will be kept in ststus sync with base info device number -- all TXRX or all RXONLY

EXT bncs_string ssCompositeInstance;   // parameter passed in on startup line
EXT CCompInstances* cl_KeyInstances;

EXT int i_info_RingMaster;           // same as iDevice -- ie parameter passed in on startup Master / Base infodriver
EXT int i_info_Conferences;         // same as iDevice    V1 = + 2 - ;   V2 = +1  --- see config xml as it gets infodriver number from config/instances
EXT int i_info_IFBs_Start, i_info_IFBs_End;             // V2 gets START infodriver number from config / instances  uses ONE INFO per ring now for IFBS ;;  V1 used only 1 INFO for just 1500 ifbs
EXT int i_info_Ports_Start, i_info_Ports_End;          // V2 gets START infodriver number from config / instances  uses ONE INFO per ring now   ;;                V1 used 3 INFOs for ports mapping 
EXT int i_info_Ports_PTI;              // 

EXT int iNumberOfInfodriversRequired;     // 3 + (number rings)*2

EXT extinfo *eiRingMasterInfoId;    	// external infodriver for driver  == base device number
EXT extinfo *eiConferencesInfoId;    	
EXT extinfo *eiPortsPTIInfoId;    	
EXT extinfo *eiIFBsInfoIds[MAX_RIEDEL_RINGS];         // Sky have 6 rings; wales 3, each Discovery region have 2
EXT extinfo *eiPortsInfoIds[MAX_RIEDEL_RINGS];       // actual numbers used is determined from the number of rings defined

//
//   MULTI HUB/Site variables
//
EXT BOOL bAutoLocationInUse;                // flag to indicate that RingMaster Multi Hub/Site operational
EXT int iAutomaticLocation;                        // LDC or NHN or ???? other multi sites ???
EXT int iAutomatics_CtrlInfo_Local;         // used for tic-tok alive notifications between sites and passing shared resources data
EXT int iAutomatics_CtrlInfo_Remote;    // used for tic-tok alive notifications between sites and passing shared resources data
EXT int iPrevCtrlStatus, iControlStatus, iCtrlStatusCalcCounter;      // overall status from cntrol infodriver reports - colours status field
EXT int iAREA1_TXRX_TicTocStatus, iAREA1_RXONLY_TicTocStatus;
EXT int iAREA2_TXRX_TicTocStatus, iAREA2_RXONLY_TicTocStatus;

EXT bncs_stringlist ssl_area_names;
EXT bncs_string ssAREA1_TX_Revertive, ssAREA1_RX_Revertive;
EXT bncs_string ssAREA2_TX_Revertive, ssAREA2_RX_Revertive;

EXT int iOtherSite_Ringmaster_main;        // base infodriver for "other" ringamaster  

EXT BOOL bSendingOutHighwaysData;
EXT bncs_stringlist sslSendingOutHighwaysData;


// data and revertive storage classes 

EXT bncs_stringlist ssl_AllKnownRings;
EXT int iNumberRiedelRings;
EXT map<int, CRiedel_Rings*>*  cl_Riedel_Rings;

EXT int iNumberRiedelGRDRecords;
EXT map<int, CRiedel_GRDs*>* cl_Riedel_GrdRecords;    // int key is bncs device number of riedel grd in question

EXT int iNumberRiedelMonitorPortRecords;
EXT map<int,  CRiedel_Monitors*>*  cl_Riedel_Revs_Monitor;  // key is rev slot number index

EXT int iNumberRiedelIFBRecords;
EXT map<int, CRiedel_IFBs*>*  cl_Riedel_Revs_IFB;

EXT int iNumberRiedelConferenceRecords;    // these are overall totals of records across all rings (eg conf = 500 * number rings )
EXT map<int, CRiedel_Conferences*>* cl_Riedel_Revs_Conf;

EXT int iNumberRiedelMixerRecords;
EXT map<int, CRiedel_Mixers*>*  cl_Riedel_Revs_Mixers;

EXT int iNumberRiedelLogicRecords;
EXT map<int, CRiedel_Logic*>*  cl_Riedel_Revs_Logic;

EXT int iNumberRiedelTrunkRecords;
EXT map<int, CRiedel_Trunks*>*  cl_Riedel_Trunks;

EXT int iNumberRiedelPTIPortRecords;
EXT map<int, CRiedel_PTI*>*  cl_Riedel_Revs_PTI;

////////////////////////////////////

EXT int iNumberRingMasterConfRecords;
EXT map<int, CRingMaster_CONF*>*  cl_RingMaster_Conferences;  // key is conf number index

EXT int iNumberRingMasterIFBRecords;
EXT map<int, CRingMaster_IFB*>*  cl_RingMaster_IFBs;  // key is ifb number index -- 1500* per ring (1-1500, ring 1 // 1501-3000 ring 2 // 3001-4500 ring 3 etc etc etc 

EXT int iNumberRingMasterMixerRecords;
EXT map<int, CRingMaster_MIXER*>*  cl_RingMaster_Mixers;  // key is mixer number index

EXT int iNumberRingMasterLogicRecords;
EXT map<int, CRingMaster_LOGIC*>*  cl_RingMaster_Logic;  // key is logic source index

EXT int iNumberBNCSHighways;   // ALL highways under BNCS control -- Standard and Monitoring
EXT int iNumberMCRMonHighways;   // monitoring - subset of all 
EXT map<int, CHighwayRecord*>* cl_HighwayRecords;    // int index key, there are functions to find specific BNCS Highways

EXT int iNumberRingPortsDefnRecords;
EXT map<int, CRingPortsDefn*>* cl_RingPortsMapping;   // 4096 defined ports per ring permitted

EXT int iNumberMCRMonDestRecords;
EXT map<int, CMCRMonPortRecord*>* cl_MCRMonPorts;

EXT int iNumberDataListRecords;
EXT map<int, CDataList*>* cl_DataStruct;

EXT queue<CCommand*> qCommands;           // queue of commands to go out to bncs drivers, routers, other devices 

///////////////////////////////////////////
EXT int iOverallTXRXModeStatus;			// overall mode -- based on base infodriver i.e gpi input driver
EXT BOOL bConnectedToInfoDriverOK;	// connected to the infodriver or not
EXT BOOL bValidCollediaStatetoContinue;
EXT BOOL bAutomaticStarting;
EXT BOOL bShowAllDebugMessages;
EXT BOOL bPollTrunkNavigatorData;
EXT int iConfigCounterTrunkNavData;
EXT int iNextPollTrunkNavigatorData;
EXT int iOverallAutoAlarmState;

EXT int iOneSecondPollTimerId;
EXT BOOL bOneSecondPollTimerRunning;
EXT int iNextResilienceProcessing;        // count down to zero -- check info tx/rx status 
EXT int iOverallStateJustChanged;
EXT int iOverallModeChangedOver;
EXT int iNextManagementProcessing;
EXT int iUserDefinedMngmntProc;       // from ini file - default is 5 seconds
EXT int iNextCommandTimerId;
EXT BOOL bNextCommandTimerRunning;
EXT BOOL bRMDatabaseTimerRunning;

EXT int iLabelAutoStatus; // used to distinguish diff cols for label
EXT int iStartupCounter;
EXT int iConfigMonitorErrors;

EXT int iChosenRing;
EXT int iChosenGRDDest;
EXT int iChosenHighway;
EXT int iChosenMonitorPort;
EXT int iChosenMixer;
EXT int iChosenConference;
EXT int iChosenIFB;
EXT int iChosenLogic;

EXT databaseMgr dbmPkgs;

// rm cmds - store db,index for src / dest RM changes
EXT bncs_stringlist ssl_RMChanges;

EXT bncs_string ssKeyClearCmd;

EXT int iTraceCount;                             // used to limit number of times a recurrsive function called - to stop infinite loops
EXT bncs_stringlist ssl_Calc_Trace; // used in recursive functions when tracing forward thru routers and calc trace elements;
EXT bncs_stringlist ssl_PotentialRouteCommands;     // global used to hold list of route commands until route is complete - then execute
EXT bncs_stringlist ssl_CalculatedRouteHops;     // global used to hold calc list of route hop sequences from one given ring to another
EXT bncs_stringlist ssl_hops;     // global used to hold single list of route hop sequence from one ring to another

///////////////////////////////////////////////////////////////////

EXT	HBRUSH hbrWindowsGrey;
EXT	HBRUSH hbrBlack;
EXT	HBRUSH hbrWhite;

EXT	HBRUSH hbrDarkRed;
EXT	HBRUSH hbrDarkGreen;
EXT	HBRUSH hbrDarkBlue;
EXT	HBRUSH hbrDarkOrange;

EXT	HBRUSH hbrYellow;
EXT	HBRUSH hbrTurq;

EXT	HBRUSH hbrLightRed;
EXT	HBRUSH hbrLightGreen;
EXT	HBRUSH hbrLightOrange;
EXT	HBRUSH hbrLightBlue;

EXT HBRUSH HBr, HBrOld;

EXT COLORREF iCurrentTextColour;
EXT COLORREF iCurrentBackgroundColour;
EXT HBRUSH iCurrentBrush;



#endif // !defined(AFX_RingMasterAuto_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
