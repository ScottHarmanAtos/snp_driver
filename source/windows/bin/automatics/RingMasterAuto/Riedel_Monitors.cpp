#include "Riedel_Monitors.h"


CRiedel_Monitors::CRiedel_Monitors(int iRecordIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk)
{
	m_iRecordIndex = iRecordIndex;
	m_iRevDevice = iDevNum;
	m_iRevSlot = iRevSlot;
	m_splitChar = sc;
	m_iAssoc_TrunkAddr = iAssocTrunk;  // index into ring class
	m_iAssoc_MonHighway = 0;
	m_iAssoc_MonPortRecord = 0;

	m_ssl_Data = bncs_stringlist("", sc); // raw data split by char

	m_ss1_MonFunction = "";
	m_ss1_PrevFunction = "";
	m_ss2_MonMarker = "";
	m_ss3_MonLabel = "";
	m_ss4_MonKeyProp = "";
	m_ss5_MonKeySubTitle = "";
	m_ss6_MonKeyTextColour = "";
}


CRiedel_Monitors::~CRiedel_Monitors()
{
	// 
}

void CRiedel_Monitors::clearRiedelData(void)
{
	m_ss1_MonFunction = "";
	if ((m_iAssoc_MonHighway>0) || (m_iAssoc_MonPortRecord>0)) m_ss1_MonFunction = "0";
	m_ss2_MonMarker = "";
	m_ss3_MonLabel = "";
	m_ss4_MonKeyProp = "";
	m_ss5_MonKeySubTitle = "";
	m_ss6_MonKeyTextColour = "";
}

void CRiedel_Monitors::storeRiedelData(bncs_string ss)
{
	m_ss1_PrevFunction = m_ss1_MonFunction;
	m_ssl_Data.clear();
	m_ssl_Data = bncs_stringlist(ss, m_splitChar);
	int iCount = m_ssl_Data.count();
	clearRiedelData();
	if ((iCount>0) && (m_ssl_Data[0].length()>0)) m_ss1_MonFunction = m_ssl_Data[0];
	if (iCount>1) m_ss2_MonMarker = m_ssl_Data[1];
	if (iCount>2) m_ss3_MonLabel = m_ssl_Data[2];
	if (iCount>3) m_ss4_MonKeyProp = m_ssl_Data[3];
	if (iCount>4) m_ss5_MonKeySubTitle = m_ssl_Data[4];
	if (iCount>5) m_ss6_MonKeyTextColour = m_ssl_Data[5];
}

int CRiedel_Monitors::getRecordIndex(void)
{
	return m_iRecordIndex;
}


int CRiedel_Monitors::getDevice(void)
{
	return m_iRevDevice;
}

int CRiedel_Monitors::getSlot(void)
{
	return m_iRevSlot;
}

int CRiedel_Monitors::getTrunkAddress(void)
{
	return m_iAssoc_TrunkAddr;
}

bncs_string CRiedel_Monitors::getRawRiedelRevertive(void)
{
	return m_ssl_Data.toString(m_splitChar);
}

bncs_string CRiedel_Monitors::getMonitoring_Function(void)
{
	return m_ss1_MonFunction;
}

bncs_string CRiedel_Monitors::getPrevious_Function(void)
{
	return m_ss1_PrevFunction;
}

bncs_string CRiedel_Monitors::getMonitoring_Marker(void)
{
	return m_ss2_MonMarker;
}

bncs_string CRiedel_Monitors::getMonitoring_Label(void)
{
	return m_ss3_MonLabel;
}

bncs_string CRiedel_Monitors::getMonitoring_KeyProperty(void)
{
	return m_ss4_MonKeyProp;
}


bncs_string CRiedel_Monitors::getMonitoring_SubTitle(void)
{
	return m_ss5_MonKeySubTitle;
}


bncs_string CRiedel_Monitors::getMonitoring_TextColour(void)
{
	return m_ss6_MonKeyTextColour;
}


void CRiedel_Monitors::setAssociatedMonHighway(int iHighIndex)
{
	// set at startup only
	m_iAssoc_MonHighway = iHighIndex;
	m_ss1_MonFunction = "0";
}

int CRiedel_Monitors::getAssociatedMonHighway(void)
{
	return m_iAssoc_MonHighway;

}

void CRiedel_Monitors::setAssociatedMonitoringRecord(int iRecIndex)
{
	// set at startup only
	m_iAssoc_MonPortRecord = iRecIndex;
	m_ss1_MonFunction = "0";
}

int CRiedel_Monitors::getAssociatedMonitoringRecord(void)
{
	return m_iAssoc_MonPortRecord;
}



