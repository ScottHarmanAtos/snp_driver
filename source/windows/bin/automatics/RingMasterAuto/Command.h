// Command.h: interface for the CCommand class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMMAND_H__8F140286_08D2_461C_A9FF_9ADF33FD9514__INCLUDED_)
#define AFX_COMMAND_H__8F140286_08D2_461C_A9FF_9ADF33FD9514__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)

	#include <windows.h>

#define		UNKNOWNVAL -1

class CCommand  
{
public:
	CCommand();
	virtual ~CCommand();

	// variables used for bncs message queue to control going in / out - all public - direct access
	char szCommand[270];    // also infodriver revertive
	int iTypeCommand;            // also revertive type
	int iWhichDevice;
	int iWhichSlotDest;
	int iWhichDatabase;
};

#endif // !defined(AFX_COMMAND_H__8F140286_08D2_461C_A9FF_9ADF33FD9514__INCLUDED_)
