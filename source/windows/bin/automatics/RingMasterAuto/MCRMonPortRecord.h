
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786)
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include "bncs_auto_helper.h"

// note -- not just mcr ring any more -- any monitoring ports defined for any ring 

class CMCRMonPortRecord
{
public:
	CMCRMonPortRecord(int iIndx, int iTrunk, int iGivenPort, int iSlotIndx, int iTranslatedPort);
	~CMCRMonPortRecord();

	int m_iRecordIndex;

	int m_iMonTrunkAddr;
	int m_iConfigMonPort; 
	int m_iMonPortTranslated;   // translated  port - from dev_xx2.db9 -- reverse lookup file at start to convert m_iConfigMonPort to port used in riedel cmds / revs

	int m_iAssoc_RingMaster_Slot;
	bncs_string m_ssCommandRevertive;

	int m_iUsingHighwayRecord;
	int m_iWorkingModeForHighway;

};

