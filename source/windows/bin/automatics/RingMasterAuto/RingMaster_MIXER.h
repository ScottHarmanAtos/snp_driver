#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// holds ring master explicit info for each MIXER

class CRingMaster_MIXER
{
private:
	int m_RecordIndex;
	// keys into relevant revertive records
	int m_iRevRecordIndex;   // array entry is based on trunk address not ring number by the way

	// Like IFBs, Mixers have default / associated ring/trunk to which they work with -- as highways are involved
	int m_iAssociatedTrunkAddress;

	// any other data required for IFBs at ringmaster level rather than just ifb revertive 
	bncs_string m_ss_RMstr_Revertive;

public:
	CRingMaster_MIXER(int iIndex, int iTrunkAddr);
	~CRingMaster_MIXER();

	void addRiedelRevIndex(int iRevRecIndex);
	int getRiedelRevIndex(void);

	void storeRingMasterRevertive(bncs_string ssRev);
	bncs_string getRingMasterRevertive(void);

	int getAssociatedTrunkAddress(void);

};

