#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"

class CRingPortsDefn
{
public:
	CRingPortsDefn();
	~CRingPortsDefn();

	int iTableIndex;
	int iRingTrunk;
	int iRingPort;
	int iPortsInfodrv;
	int iPortsInfodrvSlot;
};

