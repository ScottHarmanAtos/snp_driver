#include "DataList.h"


CDataList::CDataList( int iIndex, int iTrunk)
{
	iRecordIndexRing = iIndex;
	iRecordTrunk = iTrunk;
	iDataInteger = 0;
	ss_datastring = "";
	ssl_datalist1 = bncs_stringlist("", ',');
	ssl_datalist2 = bncs_stringlist("", ',');
	ssl_datalist3 = bncs_stringlist("", ',');
}


CDataList::~CDataList()
{
}

void CDataList::clearAllData()
{
	iDataInteger = 0;
	ss_datastring = "";
	ssl_datalist1.clear();
	ssl_datalist2.clear();
	ssl_datalist3.clear();
}

void CDataList::setDataInt(int iValue)
{
	iDataInteger = iValue;
}


void CDataList::setDataString(bncs_string ssValue)
{
	ss_datastring = ssValue;
}


void CDataList::addToDataList(int iWhichlist, bncs_string ssAddThis)
{
	switch (iWhichlist) {
		case 2:	ssl_datalist2.append(ssAddThis); break;
		case 3:	ssl_datalist3.append(ssAddThis); break;
	default:
		ssl_datalist1.append(ssAddThis);
	}
}


int CDataList::getRecordTrunk(void)
{
	return iRecordTrunk;
}

int CDataList::getDataInt(void)
{
	return iDataInteger;
}

bncs_string CDataList::getDataString(void)
{
	return ss_datastring;
}

bncs_string CDataList::getDataList(int iWhichList)
{
	if (iWhichList == 2) return ssl_datalist2.toString(',');
	else if (iWhichList == 3) return ssl_datalist3.toString(',');
	else return ssl_datalist1.toString(',');
}
