#include "CompInstances.h"


CCompInstances::CCompInstances( bncs_string ssInst )
{
	ssOverallComposite=ssInst;
	// mains instances - first infodriver from each of composite groups - Rmstr init uses to connect to all infodrivers

	ssRmstr_Main_Info="";
	ssRmstr_Confs_Info="";
	ssRmstr_PortsPTI="";
	i_dev_Rmstr_Main=0;
	i_dev_Rmstr_Confs = 0;
	i_dev_Rmstr_PortsPTI = 0;
	for (int ii = 0; ii < MAX_RIEDEL_RINGS; ii++) {
		ssRmstr_IFB_Info[ii]="";
		ssRmstr_Ports_Info[ii]="";
		i_dev_Rmstr_IFBs[ii]=0;
		i_dev_Rmstr_Ports[ii]=0;
	}

}


CCompInstances::~CCompInstances()
{
}
