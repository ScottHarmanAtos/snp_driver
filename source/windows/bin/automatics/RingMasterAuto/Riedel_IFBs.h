#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class to hold REVERTIVE data from riedel drivers 
class CRiedel_IFBs
{
private:
	int m_iRecordIndex;
	int m_iRevDevice;
	int m_iRevSlot;
	char m_splitChar;
	int m_iRevertive_TrunkAddr;  // can be used to index into ring class for this revertive record for infodriver device

	bncs_stringlist m_ssl_Data; // raw data split by char

	bncs_string m_ss1_IFBInput, m_ss1_PreviousInputs;   // first of 4 revertives for riedel infodrv revs
	bncs_string m_ss2_IFBMixMinus, m_ss2_PreviousMixMinus;   // 2nd of 4 revertives for riedel infodrv revs
	bncs_string m_ss3_IFBOutput, m_ss3_PreviousOutputs;   // 3rd of 4 infodrv revs - need to know prev if outputs change to process diffs -> highways etc
	bncs_string m_ss4_IFBLabel;   // 4th of 4 revertives for riedel infodrv revs -- only from monitoring and IFB revs

public:
	CRiedel_IFBs(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk);
	~CRiedel_IFBs();

	void clearRiedelData(void);

	void storeRiedelData(bncs_string ss);

	int getRecordIndex(void);
	int getDevice(void);
	int getSlot(void);
	int getTrunkAddress(void);

	bncs_string getRawRiedelRevertive(void);
	bncs_string getIFB_Inputs(void);
	bncs_string getIFB_MixMinus(void);
	bncs_string getIFB_Outputs(void);
	bncs_string getIFB_PreviousRev(int iWhichType );
	bncs_string getIFB_Label(void);


};

