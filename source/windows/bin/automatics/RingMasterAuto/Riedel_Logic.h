#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class to store riedel revertives for mixers

class CRiedel_Logic
{
private:
	int m_iRecordIndex;
	int m_iRevDevice;
	int m_iRevSlot;
	char m_splitChar;
	int m_iRevertive_TrunkAddr;  // can be used to index into ring class for this revertive record for infodriver device
	int m_iLogicState;   

public:
	CRiedel_Logic(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk);
	~CRiedel_Logic();

	void storeRiedelData(bncs_string ss);

	int getRecordIndex(void);
	int getDevice(void);
	int getSlot(void);
	int getTrunkAddress(void);
	int getLogicState(void);

};


