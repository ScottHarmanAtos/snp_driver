/* RingMasterAuto.cpp - custom program file for project RingMasterAuto */

#include "stdafx.h"
#include "RingMasterAuto.h"

/* TODO: place references to your functions and global variables in the RingMasterAuto.h file */

// local function prototypes
// this function is called when a valid slotchange notification is extracted from the InfoNotify
BOOL SlotChange(int iWhichInfoDriver, int iInfoDevice, int iInfoSlot, LPCSTR szSlot);
char* ExtractSrcNamefromDevIniFile( int iDev, int iDbFile, int iEntry );

void AdjustRiedel_PortKeyData(string & szData);

CRiedel_Conferences* GetConferenceRecord( int iIndex );

void ClearAllConferenceKeyMembers(int iRiedelDevice, CRiedel_Conferences* pConf);
void ClearConferencePortMembers( int iRiedelDevice, int iConfIndex );

int ValidInfodriverDevice(int iInfoDev, int iInfoSlot, int *iAssocRing);
int StoreRawRiedelRevertive(int iInfoType, int iAssocRing, int iInfoDev, int iInfoSlot, bncs_string ssState, BOOL *bChanged);

int ValidRiedelGRDDevice(int iDev);
BOOL StoreProcessRiedelGRDRevertive(int iThisRing, int iThisGrdDev, int iThisDest, int iNewSrce);

void ProcessRiedelConferenceRev(int iRevRecordIndex, int iSlotNum);
void ProcessRiedelIFBRev(int iRevRecordIndex, int iWhichRevRing, int iSlotNum);
void ProcessRiedelMIXERRev(int iRevRecordIndex, int iWhichRevRing, int iSlotNum);
void ProcessRiedelMonitorRev(int iRecordIndex, int iWhichRevRing, int iSlotNum);
void ProcessRiedelLOGICRev(int iRevRecordIndex, int iWhichRevRing, int iSlotNum);
void ProcessRiedelPTIRev(int iRevRecordIndex, int iWhichRevRing, int iSlotNum);

void MakeRiedelGRDRoute(int iRiedelGrd, int iSource, int iDest);
void UpdateGRDRevertive(CRiedel_GRDs* pGrd, int iThisDest);
void UpdateRingMasterPortPti(int iPort);

void GetTrunkAndPortFromString(bncs_string ssParams, int *iTrunk, int *iPort, BOOL *bSecondChannel);
bncs_string GetPortAnddBFromString(bncs_string ssParams, int *iPort, BOOL *bSecondChannel);
bncs_string GetTrunkAndPortAnddBFromString(bncs_string ssParams, int* iTrunk, int* iPort, BOOL *bSecondChannel);

void CalculateTrunkNavigatorUseForRing(CRiedel_Rings* pRing);

BOOL TraceThePrimarySource(int *iTracedRing, int *iTracedRtr, int *iTracedSrc);


//////////////////////////////////////////////////////////////////////////
// 
//  COMMAND QUEUE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


void ClearCommandQueue(void)
{
	while (!qCommands.empty()) {
		CCommand* pCmd;
		pCmd = qCommands.front();
		qCommands.pop();
		delete pCmd;
	}
	SetDlgItemInt(hWndDlg, IDC_QUEUE, qCommands.size(), FALSE);
}

void AddCommandToQue(LPCSTR szCmd, int iCmdType, int iDev, int iSlot, int iDB)
{
	CCommand* pCmd = new CCommand;
	if (pCmd) {
		char szData[260] = "";
		strcpy(szData, szCmd);
		if (strlen(szData) > 255) szData[255] = NULL;
		strcpy(pCmd->szCommand, szData);
		pCmd->iTypeCommand = iCmdType;
		pCmd->iWhichDevice = iDev;
		pCmd->iWhichSlotDest = iSlot;
		pCmd->iWhichDatabase = iDB;
		qCommands.push(pCmd);
	}
	else
		Debug("AddCommand2Q - cmd record not created");

}

void ProcessNextCommand(int iRateRevs) {

	BOOL bContinue = TRUE;
	do {
		if (qCommands.size()>0) {
			strcpy(szClientCmd, "");
			iRateRevs--;
			// something in queue so process it
			BOOL bNow = FALSE;
			if ((iRateRevs == 0) || (qCommands.size() == 1)) bNow = TRUE;
			CCommand* pCommand = qCommands.front();
			qCommands.pop();
			if (pCommand&&ecCSIClient) {
				// ONLY send cmd if main driver only or in starting mode for rx driver to register and poll initially
				if ((iOverallTXRXModeStatus == IFMODE_TXRX) || (bAutomaticStarting)) {
					strcpy(szClientCmd, pCommand->szCommand);
					if (strlen(szClientCmd) > 255) szClientCmd[255] = NULL;
					if ((bShowAllDebugMessages) && (!bAutomaticStarting)) Debug("ProcessCommand - TX> %s ", szClientCmd);
					switch (pCommand->iTypeCommand){
					case ROUTERCOMMAND:	ecCSIClient->txrtrcmd(szClientCmd, bNow);	break;
					case INFODRVCOMMAND: ecCSIClient->txinfocmd(szClientCmd, bNow); break;
					case GPIOCOMMAND:	ecCSIClient->txgpicmd(szClientCmd); break;
					case DATABASECOMMAND:
					case AUTOINFOREVERTIVE:	eiRingMasterInfoId->updateslot(pCommand->iWhichSlotDest, szClientCmd);  break;
						break;
					default:
						Debug("ProcessNextCmd - unknown type from buffer - %d %s", pCommand->iTypeCommand, szClientCmd);
					} // switch
				}
				else {
					// as in RXonly mode - Clear the buffer - no need to go round loop
					ClearCommandQueue();
					bContinue = FALSE;
					iRateRevs = 0;
				}
				delete pCommand; // delete record as now finished 
			}
			else
				Debug("ProcessNextCommand -ERROR- invalid command class");
		}
		else {
			// nothing else in queue
			bContinue = FALSE;
			iRateRevs = 0;
		}
	} while ((bContinue) && (iRateRevs>0));

	// if more commands still in queue and timer not running then start timer, else perhaps kill it
	if (qCommands.size()>0) {
		if (!bNextCommandTimerRunning) {
			SetTimer(hWndMain, COMMAND_TIMER_ID, 100, NULL); // timer for start poll
			bNextCommandTimerRunning = TRUE;
		}
	}
	else {
		if (bNextCommandTimerRunning) {
			KillTimer(hWndMain, COMMAND_TIMER_ID);
			bNextCommandTimerRunning = FALSE;
		}
	}

}

void SendRiedelCommandNow(LPCSTR szCmd, int iDev, int iSlot )
{
	if ((iDev>0) && (iSlot>0) && (iSlot <= 4096) && (ecCSIClient) && (iOverallTXRXModeStatus == IFMODE_TXRX)) {
		ecCSIClient->txinfocmd(szCmd, NOW);
	}
}


//////////////////////////////////////////////////////////////////////////
// 
//  MAP PACKAGE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////

void ClearAutomaticMaps()
{
	Debug("ClearAutomaticMaps 1");
	if (cl_HighwayRecords) {
		while (cl_HighwayRecords->begin() != cl_HighwayRecords->end()) {
			map<int, CHighwayRecord*>::iterator it = cl_HighwayRecords->begin();
			if (it->second) delete it->second;
			cl_HighwayRecords->erase(it);
		}
		delete cl_HighwayRecords;
	}
	
	Debug("ClearAutomaticMaps 2");
	if (cl_Riedel_GrdRecords) {
		while (cl_Riedel_GrdRecords->begin() != cl_Riedel_GrdRecords->end()) {
			map<int, CRiedel_GRDs*>::iterator it = cl_Riedel_GrdRecords->begin();
			if (it->second) delete it->second;
			cl_Riedel_GrdRecords->erase(it);
		}
		delete cl_Riedel_GrdRecords;
	}

	Debug("ClearAutomaticMaps 3");
	if (cl_Riedel_Revs_Monitor) {
		while (cl_Riedel_Revs_Monitor->begin() != cl_Riedel_Revs_Monitor->end()) {
			map<int, CRiedel_Monitors*>::iterator it = cl_Riedel_Revs_Monitor->begin();
			if (it->second) delete it->second;
			cl_Riedel_Revs_Monitor->erase(it);
		}
		delete cl_Riedel_Revs_Monitor;
	}

	Debug("ClearAutomaticMaps 4");
	if (cl_Riedel_Revs_IFB) {
		while (cl_Riedel_Revs_IFB->begin() != cl_Riedel_Revs_IFB->end()) {
			map<int, CRiedel_IFBs*>::iterator it = cl_Riedel_Revs_IFB->begin();
			if (it->second) delete it->second;
			cl_Riedel_Revs_IFB->erase(it);
		}
		delete cl_Riedel_Revs_IFB;
	}
	Debug("ClearAutomaticMaps 4a");
	if (cl_Riedel_Revs_Mixers) {
		while (cl_Riedel_Revs_Mixers->begin() != cl_Riedel_Revs_Mixers->end()) {
			map<int, CRiedel_Mixers*>::iterator it = cl_Riedel_Revs_Mixers->begin();
			if (it->second) delete it->second;
			cl_Riedel_Revs_Mixers->erase(it);
		}
		delete cl_Riedel_Revs_Mixers;
	}

	if (cl_Riedel_Revs_Logic) {
		while (cl_Riedel_Revs_Logic->begin() != cl_Riedel_Revs_Logic->end()) {
			map<int, CRiedel_Logic*>::iterator it = cl_Riedel_Revs_Logic->begin();
			if (it->second) delete it->second;
			cl_Riedel_Revs_Logic->erase(it);
		}
		delete cl_Riedel_Revs_Logic;
	}

	if (cl_Riedel_Revs_PTI) {
		while (cl_Riedel_Revs_PTI->begin() != cl_Riedel_Revs_PTI->end()) {
			map<int, CRiedel_PTI*>::iterator it = cl_Riedel_Revs_PTI->begin();
			if (it->second) delete it->second;
			cl_Riedel_Revs_PTI->erase(it);
		}
		delete cl_Riedel_Revs_PTI;
	}

	Debug("ClearAutomaticMaps 5");
	if (cl_Riedel_Revs_Conf) {
		while (cl_Riedel_Revs_Conf->begin() != cl_Riedel_Revs_Conf->end()) {
			map<int, CRiedel_Conferences*>::iterator it = cl_Riedel_Revs_Conf->begin();
			if (it->second) delete it->second;
			cl_Riedel_Revs_Conf->erase(it);
		}
		delete cl_Riedel_Revs_Conf;
	}

	Debug("ClearAutomaticMaps 6");
	if (cl_Riedel_Rings) {
		while (cl_Riedel_Rings->begin() != cl_Riedel_Rings->end()) {
			map<int, CRiedel_Rings*>::iterator it = cl_Riedel_Rings->begin();
			if (it->second) delete it->second;
			cl_Riedel_Rings->erase(it);
		}
		delete cl_Riedel_Rings;
	}

	Debug("ClearAutomaticMaps 7");
	if (cl_Riedel_Trunks) {
		while (cl_Riedel_Trunks->begin() != cl_Riedel_Trunks->end()) {
			map<int, CRiedel_Trunks*>::iterator it = cl_Riedel_Trunks->begin();
			if (it->second) delete it->second;
			cl_Riedel_Trunks->erase(it);
		}
		delete cl_Riedel_Trunks;
	}

	Debug("ClearAutomaticMaps 8");
	if (cl_RingMaster_Conferences) {
		while (cl_RingMaster_Conferences->begin() != cl_RingMaster_Conferences->end()) {
			map<int, CRingMaster_CONF*>::iterator it = cl_RingMaster_Conferences->begin();
			if (it->second) delete it->second;
			cl_RingMaster_Conferences->erase(it);
		}
		delete cl_RingMaster_Conferences;
	}

	Debug("ClearAutomaticMaps 9");
	if (cl_RingMaster_IFBs) {
		while (cl_RingMaster_IFBs->begin() != cl_RingMaster_IFBs->end()) {
			map<int, CRingMaster_IFB*>::iterator it = cl_RingMaster_IFBs->begin();
			if (it->second) delete it->second;
			cl_RingMaster_IFBs->erase(it);
		}
		delete cl_RingMaster_IFBs;
	}

	if (cl_RingMaster_Mixers) {
		while (cl_RingMaster_Mixers->begin() != cl_RingMaster_Mixers->end()) {
			map<int, CRingMaster_MIXER*>::iterator it = cl_RingMaster_Mixers->begin();
			if (it->second) delete it->second;
			cl_RingMaster_Mixers->erase(it);
		}
		delete cl_RingMaster_Mixers;
	}

	if (cl_RingMaster_Logic) {
		while (cl_RingMaster_Logic->begin() != cl_RingMaster_Logic->end()) {
			map<int, CRingMaster_LOGIC*>::iterator it = cl_RingMaster_Logic->begin();
			if (it->second) delete it->second;
			cl_RingMaster_Logic->erase(it);
		}
		delete cl_RingMaster_Logic;
	}

	Debug("ClearAutomaticMaps 10");
	if (cl_RingPortsMapping) {
		while (cl_RingPortsMapping->begin() != cl_RingPortsMapping->end()) {
			map<int, CRingPortsDefn*>::iterator it = cl_RingPortsMapping->begin();
			if (it->second) delete it->second;
			cl_RingPortsMapping->erase(it);
		}
		delete cl_RingPortsMapping;
	}
	if (cl_MCRMonPorts) {
		while (cl_MCRMonPorts->begin() != cl_MCRMonPorts->end()) {
			map<int, CMCRMonPortRecord*>::iterator it = cl_MCRMonPorts->begin();
			if (it->second) delete it->second;
			cl_MCRMonPorts->erase(it);
		}
		delete cl_MCRMonPorts;
	}

	Debug("ClearAutomaticMaps out");

}



////////////////////////////////////////////////////////////////////////////////////////
//
// get records from maps
//

CMCRMonPortRecord* GetMCRMonPortRecord(int iIndex)
{
	if ((iIndex>0) && (cl_MCRMonPorts)) {
		map<int, CMCRMonPortRecord*>::iterator itp;
		itp = cl_MCRMonPorts->find(iIndex);
		if (itp != cl_MCRMonPorts->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CMCRMonPortRecord* GetMCRMonPortByRingTransDest(int iTrunk, int iTransDest)
{
	// NOTE -- translated port really used by Riedel driver is not same as config given port 
	if (cl_MCRMonPorts) {
		// go thru map reg and compare new source and router
		for (int ii = 1; ii <= iNumberMCRMonDestRecords; ii++) {
			CMCRMonPortRecord* pRec = GetMCRMonPortRecord(ii);
			if (pRec) {
				if ((pRec->m_iMonTrunkAddr == iTrunk) && (pRec->m_iMonPortTranslated == iTransDest)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}

CMCRMonPortRecord* GetMCRMonPortByRingConfigDest(int iTrunk, int iGivenDest)
{
	// NOTE -- given config port is not same as translated port really used by Riedel driver
	if (cl_MCRMonPorts) {
		// go thru map reg and compare new source and router
		for (int ii = 1; ii <= iNumberMCRMonDestRecords; ii++) {
			CMCRMonPortRecord* pRec = GetMCRMonPortRecord(ii);
			if (pRec) {
				if ((pRec->m_iMonTrunkAddr == iTrunk) && (pRec->m_iConfigMonPort == iGivenDest)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}

CMCRMonPortRecord* GetMCRMonPortByInfodriverSlot(int iSlot)
{
	if (cl_MCRMonPorts) {
		// go thru map reg and compare new source and router
		for (int ii = 1; ii <= iNumberMCRMonDestRecords; ii++) {
			CMCRMonPortRecord* pRec = GetMCRMonPortRecord(ii);
			if (pRec) {
				if (pRec->m_iAssoc_RingMaster_Slot == iSlot) {
					return pRec;
				}
			}
		}
	}	
	return NULL;
}

////////////////////////////////////////////////

CRingPortsDefn* GetRingPortsDefintion(int iIndex)
{
	if ((iIndex>0) && (cl_RingPortsMapping)) {
		map<int, CRingPortsDefn*>::iterator itp;
		itp = cl_RingPortsMapping->find(iIndex);
		if (itp != cl_RingPortsMapping->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRingPortsDefn* GetRingPortsDefintionByRingDest(int iFromRing, int iFromDest)
{
	if (cl_RingPortsMapping) {
		for (int ii = 1; ii <= iNumberRingPortsDefnRecords; ii++) {
			CRingPortsDefn* pRec = GetRingPortsDefintion(ii);
			if (pRec) {
				if ((pRec->iRingTrunk == iFromRing) && (pRec->iRingPort == iFromDest)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}

CRingPortsDefn* GetRingPortsDefintionByInfodriverSlot(int iInfodriver, int iSlot)
{
	if (cl_RingPortsMapping) {
		for (int ii = 1; ii <= iNumberRingPortsDefnRecords; ii++) {
			CRingPortsDefn* pRec = GetRingPortsDefintion(ii);
			if (pRec) {
				if ((pRec->iPortsInfodrv == iInfodriver) && (pRec->iPortsInfodrvSlot == iSlot)) {
					return pRec;
				}
			}
		}
	}	
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////

CHighwayRecord* GetHighwayRecord(int iKey)
{
	if ((iKey>0) && (cl_HighwayRecords)) {
		map<int, CHighwayRecord*>::iterator itp;
		itp = cl_HighwayRecords->find(iKey);
		if (itp != cl_HighwayRecords->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CHighwayRecord* GetHighwayRecordByFromRtrDest(int iFromRtr, int iFromDest)
{
	// go thru map reg and compare new source and router
	if (cl_HighwayRecords) {
		for (int ii = 1; ii < iNumberBNCSHighways; ii++) {
			CHighwayRecord* pHigh = GetHighwayRecord(ii);
			if (pHigh) {
				if ((pHigh->getFromRouter() == iFromRtr) && (pHigh->getFromDestination() == iFromDest)) {
					return pHigh;
				}
			}
 		}
	}
	return NULL;
}

CHighwayRecord* GetHighwayRecordByFromTrunkAndDest(int iFromTrunk, int iFromDest, BOOL bMonHighway)
{
	// go thru map reg and compare new source and router
	if (cl_HighwayRecords) {
		for (int ii = 1; ii < iNumberBNCSHighways; ii++) {
			CHighwayRecord* pHigh = GetHighwayRecord(ii);
			if (pHigh) {
				if (bMonHighway ) {
					if ((pHigh->getFromTrunkAddr() == iFromTrunk) && (pHigh->getMonitorGivenFromDest() == iFromDest)) {
						return pHigh;
					}
				}
				else {
					if ((pHigh->getFromTrunkAddr() == iFromTrunk) && (pHigh->getFromDestination() == iFromDest)) {
						return pHigh;
					}
				}
			}
		}
	}
	return NULL;
}

CHighwayRecord* GetHighwayRecordByToTrunkAndSrce( int iToTrunk, int iToSrce, BOOL bMonHighway)
{
	// go thru map reg and compare new source and router
	if (cl_HighwayRecords) {
		for (int ii = 1; ii < iNumberBNCSHighways; ii++) {
			CHighwayRecord* pHigh = GetHighwayRecord(ii);
			if (pHigh) {
				if (bMonHighway ) {
					if ((pHigh->getToTrunkAddr() == iToTrunk) && (pHigh->getMonitorGivenToSource() == iToSrce)) {
						return pHigh;
					}
				}
				else {
					if ((pHigh->getToTrunkAddr() == iToTrunk) && (pHigh->getToSource() == iToSrce)) {
						return pHigh;
					}
				}
			}
		}
	}
	return NULL;
}

CHighwayRecord* GetHighwayRecordByToRtrSrce(int iToRtr, int iToSrce)
{
	// go thru map reg and compare new source and router
	if (cl_HighwayRecords) {
		for (int ii = 1; ii < iNumberBNCSHighways; ii++) {
			CHighwayRecord* pHigh = GetHighwayRecord(ii);
			if (pHigh) {
				if ((pHigh->getToRouter() == iToRtr) && (pHigh->getToSource() == iToSrce)) {
					return pHigh;
				}
			}
		}
	}
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

CHighwayRecord* FindIFBMixerOutputHighwayFromTrunkAndDest(int iWhichIFB, int iMode, int iFinalTrunk, int iFinalPort)
{
	if (cl_HighwayRecords) {
		int iCalcIfbdest = (iFinalTrunk*IFB_HIGHWAY_BASE) + iFinalPort;  // unique number for trunk + port used in ifb highways
		for (int ii = 1; ii < iNumberBNCSHighways; ii++) {
			CHighwayRecord* pHigh = GetHighwayRecord(ii);
			if (pHigh) {
				if ((pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) &&
					(pHigh->getHighwayAllocatedIndex() == iCalcIfbdest) &&
					(pHigh->isHighwayWorkingMode(iMode, iWhichIFB) > UNKNOWNVAL)) {
					return pHigh;
				}
			}
		}
	}
	return NULL;
}


//////////////////////////////////////////////////////////////////////////////

CRiedel_Rings* GetRiedel_Rings_Record(int iIndex)
{
	if (cl_Riedel_Rings) {
		map<int, CRiedel_Rings*>::iterator itp;
		itp = cl_Riedel_Rings->find(iIndex);
		if (itp != cl_Riedel_Rings->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRiedel_Rings* GetRiedel_Rings_By_TrunkAddress(int iAddress)
{
	if (cl_Riedel_Rings) {
		for (int ii = 1; ii <= iNumberRiedelRings; ii++) {
			CRiedel_Rings* pRec = GetRiedel_Rings_Record(ii);
			if (pRec) {
				if (pRec->m_iTrunkAddress == iAddress) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}


CRiedel_Rings* GetRiedel_Rings_By_GRD_Device(int iGRDDevice)
{
	if (cl_Riedel_Rings) {
		for (int ii = 1; ii <= iNumberRiedelRings; ii++) {
			CRiedel_Rings* pRec = GetRiedel_Rings_Record(ii);
			if (pRec) {
				if (pRec->iRing_Device_GRD == iGRDDevice) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}


CRiedel_GRDs* GetRiedel_GRD_Record(int iGrdIndex)
{
	if (cl_Riedel_GrdRecords) {
		map<int, CRiedel_GRDs*>::iterator itp;
		itp = cl_Riedel_GrdRecords->find(iGrdIndex);
		if (itp != cl_Riedel_GrdRecords->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CRiedel_GRDs* GetRiedel_GRD_By_TrunkAddr(int iTrunk)
{
	if (cl_Riedel_GrdRecords&&cl_Riedel_Rings) {
		// go thru ring records - find grd dev 
		for (int ii = 1; ii <= iNumberRiedelRings; ii++) {
			CRiedel_Rings* pRec = GetRiedel_Rings_Record(ii);
			if (pRec) {
				if (pRec->m_iTrunkAddress == iTrunk) {
					int iGRD = pRec->iRing_Device_GRD;
					CRiedel_GRDs* pGrd = GetRiedel_GRD_Record(iGRD);
					if (pGrd) {
						if (pGrd->getRingTrunk() == iTrunk) return pGrd;
					}
				}
			}
		}
	}
	return NULL;
}


CRiedel_Monitors* GetRiedel_Monitors_Record(int iIndex)
{
	if (cl_Riedel_Revs_Monitor) {
		map<int, CRiedel_Monitors*>::iterator itp;
		itp = cl_Riedel_Revs_Monitor->find(iIndex);
		if (itp != cl_Riedel_Revs_Monitor->end()) {
			if (itp->second) {
				return itp->second;
			}
		}
	}
	return NULL;
}

CRiedel_Monitors* GetRiedel_Monitors_ByDeviceSlot(int iInfoDev, int iInfoSlot)
{
	if (cl_Riedel_Revs_Monitor) {
		for (int ii = 1; ii <= iNumberRiedelMonitorPortRecords; ii++) {
			CRiedel_Monitors* pRec = GetRiedel_Monitors_Record(ii);
			if (pRec) {
				if ((pRec->getDevice() == iInfoDev) && (pRec->getSlot() == iInfoSlot)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}

CRiedel_Monitors* GetRiedel_Monitors_ByTrunkAddressAndSlot(int iTrunkAddr, int iInfoSlot)
{
	if (cl_Riedel_Revs_Monitor) {
		for (int ii = 1; ii <= iNumberRiedelMonitorPortRecords; ii++) {
			CRiedel_Monitors* pRec = GetRiedel_Monitors_Record(ii);
			if (pRec) {
				if ((pRec->getTrunkAddress() == iTrunkAddr) && (pRec->getSlot() == iInfoSlot)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}


CRiedel_Conferences* GetRiedel_Conference_Record(int iIndex)
{
	if (cl_Riedel_Revs_Conf) {
		map<int, CRiedel_Conferences*>::iterator itp;
		itp = cl_Riedel_Revs_Conf->find(iIndex);
		if (itp != cl_Riedel_Revs_Conf->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRiedel_Conferences* GetRiedel_Conferences_ByDeviceSlot(int iInfoDev, int iInfoSlot)
{
	int iConf = iInfoSlot;
	if ((iInfoSlot>1000) && (iInfoSlot<2000)) iConf = iInfoSlot - 1000;
	else if ((iInfoSlot>2000) && (iInfoSlot<3000)) iConf = iInfoSlot - 2000;
	//
	if (cl_Riedel_Revs_Conf) {
		for (int ii = 1; ii <= iNumberRiedelConferenceRecords; ii++) {
			CRiedel_Conferences* pConf = GetRiedel_Conference_Record(ii);
			if (pConf) {
				if ((pConf->getDevice() == iInfoDev) && (pConf->getConfSlot() == iConf)) {
					return pConf;
				}
			}
		}
	}
	return NULL;
}


CRiedel_IFBs* GetRiedel_IFBs_Record(int iIndex)
{
	if (cl_Riedel_Revs_IFB) {
		map<int, CRiedel_IFBs*>::iterator itp;
		itp = cl_Riedel_Revs_IFB->find(iIndex);
		if (itp != cl_Riedel_Revs_IFB->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRiedel_IFBs* GetRiedel_IFBs_ByDeviceSlot(int iInfoDev, int iInfoSlot)
{
	if (cl_Riedel_Revs_IFB) {
		for (int ii = 1; ii <= iNumberRiedelIFBRecords; ii++) {
			CRiedel_IFBs* pIfb = GetRiedel_IFBs_Record(ii);
			if (pIfb) {
				if ((pIfb->getDevice() == iInfoDev) && (pIfb->getSlot() == iInfoSlot)) {
					return pIfb;
				}
			}
		}
	}
	return NULL;
}


CRiedel_Mixers* GetRiedel_Mixers_Record(int iIndex)
{
	if (cl_Riedel_Revs_Mixers) {
		map<int, CRiedel_Mixers*>::iterator itp;
		itp = cl_Riedel_Revs_Mixers->find(iIndex);
		if (itp != cl_Riedel_Revs_Mixers->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRiedel_Mixers* GetRiedel_Mixers_ByDeviceSlot(int iInfoDev, int iInfoSlot)
{
	if (cl_Riedel_Revs_Mixers) {
		for (int ii = 1; ii <= iNumberRiedelMixerRecords; ii++) {
			CRiedel_Mixers* pRec = GetRiedel_Mixers_Record(ii);
			if (pRec) {
				if ((pRec->getDevice() == iInfoDev) && (pRec->getSlot() == iInfoSlot)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}


CRiedel_Logic* GetRiedel_Logic_Record(int iIndex)
{
	if (cl_Riedel_Revs_Logic) {
		map<int, CRiedel_Logic*>::iterator itp;
		itp = cl_Riedel_Revs_Logic->find(iIndex);
		if (itp != cl_Riedel_Revs_Logic->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRiedel_Logic* GetRiedel_Logic_ByDeviceSlot(int iInfoDev, int iInfoSlot)
{
	if (cl_Riedel_Revs_Logic) {
		for (int ii = 1; ii <= iNumberRiedelLogicRecords; ii++) {
			CRiedel_Logic* pRec = GetRiedel_Logic_Record(ii);
			if (pRec) {
				if ((pRec->getDevice() == iInfoDev) && (pRec->getSlot() == iInfoSlot)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}


CRiedel_PTI* GetRiedel_PTI_Record(int iIndex)
{
	if (cl_Riedel_Revs_PTI) {
		map<int, CRiedel_PTI*>::iterator itp;
		itp = cl_Riedel_Revs_PTI->find(iIndex);
		if (itp != cl_Riedel_Revs_PTI->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRiedel_PTI* GetRiedel_PTI_ByDeviceSlot(int iInfoDev, int iInfoSlot)
{
	if (cl_Riedel_Revs_PTI) {
		for (int ii = 1; ii <= iNumberRiedelPTIPortRecords; ii++) {
			CRiedel_PTI* pRec = GetRiedel_PTI_Record(ii);
			if (pRec) {
				if ((pRec->getDevice() == iInfoDev) && (pRec->getSlot() == iInfoSlot)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}


CRiedel_Trunks* GetRiedel_Trunks_Record(int iIndex)
{
	if (cl_Riedel_Trunks) {
		map<int, CRiedel_Trunks*>::iterator itp;
		itp = cl_Riedel_Trunks->find(iIndex);
		if (itp != cl_Riedel_Trunks->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRiedel_Trunks* GetRiedel_Trunks_ByDeviceSlot(int iInfoDev, int iInfoSlot)
{
	if (cl_Riedel_Trunks) {
		for (int ii = 1; ii <= iNumberRiedelTrunkRecords; ii++) {
			CRiedel_Trunks* pRec = GetRiedel_Trunks_Record(ii);
			if (pRec) {
				if ((pRec->getDevice() == iInfoDev) && (pRec->getSlot() == iInfoSlot)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}


CRingMaster_IFB* GetRingMaster_IFB_Record(int iIFB)
{
	if (cl_RingMaster_IFBs) {
		map<int, CRingMaster_IFB*>::iterator itp;
		itp = cl_RingMaster_IFBs->find(iIFB);
		if (itp != cl_RingMaster_IFBs->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CRingMaster_IFB* GetRingMaster_IFB_Record_FromL2P(int iTrunk, int iListen2Port)
{
	if (cl_RingMaster_IFBs) {
		for (int iifb = 1; iifb <= MAX_IFBS; iifb++) {
			CRingMaster_IFB* pRec = GetRingMaster_IFB_Record(iifb);
			if (pRec) {
				if ((pRec->getAssociatedTrunkAddress() == iTrunk) && (pRec->getAssociatedListenToPort() == iListen2Port)) {
					return pRec;
				}
			}
		}
	}
	return NULL;
}


CRingMaster_CONF* GetRingMaster_CONF_Record(int iCONF)
{
	if (cl_RingMaster_Conferences) {
		map<int, CRingMaster_CONF*>::iterator itp;
		itp = cl_RingMaster_Conferences->find(iCONF);
		if (itp != cl_RingMaster_Conferences->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRingMaster_MIXER* GetRingMaster_MIXER_Record(int iMixer)
{
	if (cl_RingMaster_Mixers) {
		map<int, CRingMaster_MIXER*>::iterator itp;
		itp = cl_RingMaster_Mixers->find(iMixer);
		if (itp != cl_RingMaster_Mixers->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRingMaster_LOGIC* GetRingMaster_LOGIC_Record(int iLogic)
{
	if (cl_RingMaster_Logic) {
		map<int, CRingMaster_LOGIC*>::iterator itp;
		itp = cl_RingMaster_Logic->find(iLogic);
		if (itp != cl_RingMaster_Logic->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}



CDataList* GetDataList_By_Index(int iRing)
{
	if (cl_DataStruct) {
		map<int, CDataList*>::iterator itp;
		itp = cl_DataStruct->find(iRing);
		if (itp != cl_DataStruct->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CDataList* GetDataList_By_Trunk(int iTrunk)
{
	if (cl_DataStruct) {
		for (int ii = 1; ii <= iNumberDataListRecords; ii++) {
			CDataList* pRec = GetDataList_By_Index(ii);
			if (pRec) {
				if (pRec->getRecordTrunk() == iTrunk) return pRec;
			}
		}
	}
	return NULL;
}

void ClearDataLists()
{
	if (cl_DataStruct) {
		for (int ii = 1; ii <= iNumberDataListRecords; ii++) {
			CDataList* pRec = GetDataList_By_Index(ii);
			if (pRec) {
				if (pRec) pRec->clearAllData();
			}
		}
	}
}

void AddToDataLists_Stringlist(int iWhichTrunk, int iWhichList, bncs_string ssData)
{
	CDataList* pData = GetDataList_By_Trunk(iWhichTrunk);
	if (pData) pData->addToDataList(iWhichList, ssData);
}


//////////////////////////////////////////////////////////////////////////
// 
BOOL AreBncsStringsEqual( bncs_string ss1, bncs_string ss2 )
{
	if (ss1.length()==ss2.length()) {
		if (ss1.length()==0)
			return TRUE;
		if (ss1.find(ss2)>=0)
			return TRUE;
	}
	return FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////
//
//   Error messages via infodriver slots 
//

void UpdateAutoAlarmStateSlot()
{
	// go thru the rings and determine overall alarm state
	iOverallAutoAlarmState = ALARM_CLEAR;
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRing);
		if (pRing) {
			if (iOverallAutoAlarmState < pRing->iRingAlarmWarningState) iOverallAutoAlarmState = pRing->iRingAlarmWarningState;
		}
	}
	switch (iOverallAutoAlarmState) {
		case 0: eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_ALARM_TRIGGER, "0"); break;  // clear
		case 1: eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_ALARM_TRIGGER, "1"); break;  // warning
		case 2: eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_ALARM_TRIGGER, "2"); break;  // general alarm
		case 3: eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_ALARM_TRIGGER, "3"); break;  // severe alarm
	}
}


void PostAutomaticErrorMessage(int iFromTrunk, bncs_string ssMessage)
{
	// get date and time  -- post messages starting at 4001 .. 4050 for 50 trunks, if fromtrunk==0, general error message to slot 4000.
	// if ssMessage is empty - can be used to clear previous message
	// add time to message
	char szMessage[MAX_AUTOBFFR_STRING] = "";
	char tBuffer[9];
	char szDate[16];
	struct tm *newtime;
	time_t long_time;
	time(&long_time);                // Get time as long integer. 
	newtime = localtime(&long_time); // Convert to local time. 
	_strtime(tBuffer);
	wsprintf(szDate, "%02d:%02d:%02d", newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
	if (iFromTrunk>0) {
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iFromTrunk);
		if (pRing) {
			if (pRing->ssRing_Error_Message.length() > 2) {
				pRing->iRingAlarmWarningState = ALARM_ERROR_STATE;
				wsprintf(szMessage, "%s - %s", szDate, LPCSTR(pRing->ssRing_Error_Message));
			}
			else if (pRing->ssRing_Threshold_Message.length() > 2) {
				if (pRing->iRingAlarmWarningState<ALARM_WARNING) pRing->iRingAlarmWarningState = ALARM_WARNING;
				wsprintf(szMessage, "%s", LPCSTR(pRing->ssRing_Threshold_Message));
			}
			else {
				// both strings empty - post empty string to clear alarm for ring
				pRing->iRingAlarmWarningState = ALARM_CLEAR;	
			}
		}
	}
	else {
		// general message
		if (ssMessage.length()>2) wsprintf(szMessage, "%s - %s", szDate, LPCSTR(ssMessage));
	}
	if (eiRingMasterInfoId) {
		eiRingMasterInfoId->updateslot(RINGTRUNKS_ERROR_MSGS + iFromTrunk, szMessage);
		UpdateAutoAlarmStateSlot();
	}
}

void SetAlarmThresholdMessage(int iFromRing, int iToRing, int iPercentUsed)
{
	bncs_string ss_message = "";
	if (iFromRing > 0) {
		// specific message for a ring threshold
		CRiedel_Rings* pFromRing = GetRiedel_Rings_By_TrunkAddress(iFromRing);
		if (pFromRing) {
			pFromRing->iRingAlarmWarningState = ALARM_CLEAR;
			if (iToRing > 0) {
				CRiedel_Rings* pToRing = GetRiedel_Rings_By_TrunkAddress(iToRing);
				if (pToRing) {
					if (iPercentUsed >= 95) {
						ss_message = bncs_string("WARNING - HIGHWAY USE  from %1 to %2 NOW %3 percent ").arg(pFromRing->ssRingName).arg(pToRing->ssRingName).arg(iPercentUsed);
						pFromRing->iRingAlarmWarningState = ALARM_ERROR_STATE;
					}
					else if (iPercentUsed > 89) {
						ss_message = bncs_string("Warning - Highway use from %1 to %2 NOW %3 percent ").arg(pFromRing->ssRingName).arg(pToRing->ssRingName).arg(iPercentUsed);
						pFromRing->iRingAlarmWarningState = ALARM_WARNING;
					}
					else if (iPercentUsed > 79) {
						ss_message = bncs_string("Info - Highway use from %1 to %2 is at %3 percent ").arg(pFromRing->ssRingName).arg(pToRing->ssRingName).arg(iPercentUsed);
						pFromRing->iRingAlarmWarningState = ALARM_WARNING;
					}
				}
			}
			else {
				if (iPercentUsed >= 95) {
					ss_message = bncs_string("SEVERE WARNING - ALL HIGHWAY USE from the %1 ring NOW AT %2 percent ").arg(pFromRing->ssRingName).arg(iPercentUsed);
					pFromRing->iRingAlarmWarningState = ALARM_ERROR_STATE;
				}
				else if (iPercentUsed > 89) {
					ss_message = bncs_string("Warning - All Highway use from the %1 ring now at %2 percent ").arg(pFromRing->ssRingName).arg(iPercentUsed);
					pFromRing->iRingAlarmWarningState = ALARM_WARNING;
				}
				else if (iPercentUsed > 79) {
					ss_message = bncs_string("Info - All Highway use from the %1 ring is %2 percent ").arg(pFromRing->ssRingName).arg(iPercentUsed);
					pFromRing->iRingAlarmWarningState = ALARM_WARNING;
				}
			}
			pFromRing->ssRing_Threshold_Message = ss_message;
		}
	}
	else {
		// generic message -- probably because total useage is VERY high
		if (iPercentUsed > 89) {
			ss_message = bncs_string("SEVERE WARNING- TOTAL HIGHWAY USAGE IS AT %1 PERCENT - check highways").arg(iPercentUsed);
			iOverallAutoAlarmState = ALARM_ERROR_STATE;
		}
		else if (iPercentUsed >79) {
			ss_message = bncs_string("Warning as Total Highway usage is at %1 percent - check highways").arg(iPercentUsed);
			if (iOverallAutoAlarmState<ALARM_WARNING) iOverallAutoAlarmState = ALARM_WARNING;
		}
		else
			iOverallAutoAlarmState = ALARM_CLEAR;
	}
	PostAutomaticErrorMessage(iFromRing, ss_message);
}

void SetAlarmErrorMessage(int iTrunkAddress, bncs_string ssErrMessage)
{
	if (iTrunkAddress > 0) {
		// specific message for a ring threshold
		CRiedel_Rings* pFromRing = GetRiedel_Rings_By_TrunkAddress(iTrunkAddress);
		if (pFromRing) {
			pFromRing->ssRing_Error_Message = ssErrMessage;
		}
	}
	PostAutomaticErrorMessage(iTrunkAddress, ssErrMessage);
}


void ClearRingAlarmMessage(int iTrunkAddress)
{
	CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunkAddress);
	if (pRing) {
		// check alarm first
		if (pRing->ssRing_Error_Message.length() > 0) {
			pRing->ssRing_Error_Message = "";
			if (pRing->iRingAlarmWarningState > ALARM_WARNING)  pRing->iRingAlarmWarningState = ALARM_WARNING; // downgrade
		}
		// then threshold
		if (pRing->ssRing_Threshold_Message.length() > 0) {
			pRing->ssRing_Threshold_Message = "";
			if (pRing->iRingAlarmWarningState > ALARM_CLEAR)  pRing->iRingAlarmWarningState = ALARM_CLEAR; // downgrade
		}
		// if both messages empty - no alarm for this ring
		if ((pRing->ssRing_Error_Message.length()<2) && (pRing->ssRing_Threshold_Message.length()<2)) pRing->iRingAlarmWarningState = ALARM_CLEAR;
	}
	// set overall alarm state 
	PostAutomaticErrorMessage(iTrunkAddress, "");
}


//////////////////////////////////////////////////////////////////////////
// 
//  AUTOMATIC GUI FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////

void DisplayRingInformation()
{
	iChosenRing = SendDlgItemMessage(hWndDlg, IDC_RINGS, LB_GETCURSEL, 0, 0) + 1;
	//clear fields
	SetDlgItemText(hWndDlg, IDC_RING_NAME, "");
	SetDlgItemText(hWndDlg, IDC_RING_INFOS, "");
	SetDlgItemText(hWndDlg, IDC_RING_STATUS, "");
	SetDlgItemText(hWndDlg, IDC_RING_SRC, "");
	SetDlgItemText(hWndDlg, IDC_RING_DEST, "");
	SetDlgItemText(hWndDlg, IDC_RING_HIGH1, "");
	SetDlgItemText(hWndDlg, IDC_RING_HIGH2, "");
	SetDlgItemText(hWndDlg, IDC_RING_INFOREVS, "");
	SetDlgItemText(hWndDlg, IDC_RING_TRUNKUSE, "");
	// get chosen ring and display
	CRiedel_Rings* pRing = GetRiedel_Rings_Record(iChosenRing);
	if (pRing) {
		SetDlgItemText(hWndDlg, IDC_RING_NAME, LPCSTR(pRing->ssRingName));
		bncs_string sstr = bncs_string("%1 %2 %3 %4 %5 %6").arg(pRing->iRing_Device_GRD)
			.arg(pRing->iRing_Device_Monitor).arg(pRing->iRing_Device_Conference).arg(pRing->iRing_Device_IFB).arg(pRing->iRing_Device_Extras).arg(pRing->iRing_Device_PTI);
		SetDlgItemText(hWndDlg, IDC_RING_INFOS, LPCSTR(sstr));
		SetDlgItemInt(hWndDlg, IDC_RING_SRC, pRing->iRing_Ports_Talk, TRUE);
		SetDlgItemInt(hWndDlg, IDC_RING_DEST, pRing->iRing_Ports_Listen, TRUE);
		SetDlgItemInt(hWndDlg, IDC_DEST_RECORDS, pRing->iRing_Ports_Listen, TRUE);
		if ((pRing->iRing_Device_GRD < 1) || (pRing->iRing_Device_Monitor < 1) || (pRing->iRing_Device_Conference < 1) || (pRing->iRing_Device_IFB < 1) || (pRing->iRing_Device_Extras<1)) {
			SetDlgItemText(hWndDlg, IDC_RING_STATUS, "** ERROR - one/more INVALID riedel dev numbers - fix instances**");
		}
		bncs_string ssOut = "", ssIn = "";
		for (int ii = 1; ii < 9; ii++) {   // only show numbers for first 8 rings at present
			ssIn.append(bncs_string(pRing->getNumberIncomingHighways(ii) + pRing->getNumberIncomingMonitoring(ii))); ssIn.append(',');
			ssOut.append(bncs_string(pRing->getNumberOutgoingHighways(ii)+pRing->getNumberOutgoingMonitoring(ii))); ssOut.append(',');
		}
		SetDlgItemText(hWndDlg, IDC_RING_HIGH1, LPCSTR(ssIn));
		SetDlgItemText(hWndDlg, IDC_RING_HIGH2, LPCSTR(ssOut));
		//
		char szData[MAX_AUTOBFFR_STRING] = "";
		eiRingMasterInfoId->getslot(HIGHWAYS_INFO_TOTALUSAGE + pRing->m_iTrunkAddress, szData);
		SetDlgItemText(hWndDlg, IDC_RING_HIGH4, szData);
		//
		wsprintf(szData, "def=%d,in_use=%d", pRing->iDefinedTrunksAvailable, pRing->iCurrentTrunksInUse);
		SetDlgItemText(hWndDlg, IDC_RING_TRUNKUSE, szData);
		//
		DisplayDestPortData();
	}
 }


void DisplayHighwayData()
{
	iChosenHighway = SendDlgItemMessage(hWndDlg, IDC_HIGHWAYS, LB_GETCURSEL, 0, 0) + 1;
	//clear fields
	SetDlgItemText(hWndDlg, IDC_HWAY_INDEX, "");
	SetDlgItemText(hWndDlg, IDC_HWAY_TRACE, "");
	SetDlgItemText(hWndDlg, IDC_HWAY_USERS, "");
	SetDlgItemText(hWndDlg, IDC_HWAY_FROM, "");
	SetDlgItemText(hWndDlg, IDC_HWAY_INTO, "");
	SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "");
	SetDlgItemText(hWndDlg, IDC_HWAY_AREAUSER, "");
	//
	CHighwayRecord* pHigh = GetHighwayRecord(iChosenHighway);
	if (pHigh) {
		bncs_string sstr = "";
		SetDlgItemInt(hWndDlg, IDC_HWAY_INDEX, pHigh->getRecordIndex(), TRUE);
		if (!pHigh->isMonitoringHighway()) {
			SetDlgItemText(hWndDlg, IDC_HWAY_FROM, LPCSTR(bncs_string("%1 : %2").arg(pHigh->getFromTrunkAddr()).arg(pHigh->getFromDestination())));
			SetDlgItemText(hWndDlg, IDC_HWAY_INTO, LPCSTR(bncs_string("%1 : %2").arg(pHigh->getToTrunkAddr()).arg(pHigh->getToSource())));
			iTraceCount = 0;
			ssl_Calc_Trace = bncs_stringlist("", '|');
			int iTracedTrunk = pHigh->getToTrunkAddr();
			int iTracedGrd = pHigh->getToRouter();
			int iTracedSrc = pHigh->getToSource();
			TraceThePrimarySource(&iTracedTrunk, &iTracedGrd, &iTracedSrc);
			bncs_string sstr = bncs_string("%1").arg(ssl_Calc_Trace.toString(' '));
			SetDlgItemText(hWndDlg, IDC_HWAY_TRACE, LPCSTR(sstr));
		}
		else {
			// mon hway - trace is done differently 
			SetDlgItemText(hWndDlg, IDC_HWAY_FROM, LPCSTR(bncs_string("%1 : %2 (g:%3)").arg(pHigh->getFromTrunkAddr()).arg(pHigh->getFromDestination()).arg(pHigh->getMonitorGivenFromDest())));
			SetDlgItemText(hWndDlg, IDC_HWAY_INTO, LPCSTR(bncs_string("%1 : %2 (g:%3)").arg(pHigh->getToTrunkAddr()).arg(pHigh->getToSource()).arg(pHigh->getMonitorGivenToSource())));
			CRiedel_Monitors* pFromMon = GetRiedel_Monitors_ByTrunkAddressAndSlot(pHigh->getFromTrunkAddr(), pHigh->getFromDestination());
			if (pFromMon) {
				// get port from this rev -- true source 
				bncs_string sstr = bncs_string("%1.%2").arg(pFromMon->getTrunkAddress()).arg(pFromMon->getMonitoring_Function());
				SetDlgItemText(hWndDlg, IDC_HWAY_TRACE, LPCSTR(sstr));
			}
		}

		switch (pHigh->getHighwayState()) {
			case HIGHWAY_FREE: 	SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "free"); break;
			case HIGHWAY_CLEARING: 	SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "clearing"); break;
			case HIGHWAY_PENDING: 	SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "pending"); break;
			case HIGHWAY_INUSE: 	SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "in-use"); break;
			case HIGHWAY_IFBMXRIN: SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "ifbmxrin"); break;
			case HIGHWAY_IFBMXROUT: SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "ifbmxrout"); break;
			case HIGHWAY_CLONEMON: SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "clonemon"); break;
			case HIGHWAY_REMOTE_USE: SetDlgItemText(hWndDlg, IDC_HWAY_STATE, "*remote*"); break;
		}

		int iValue = pHigh->getHighwayAreaUse();
		if (iValue == AREA1_LOCALE_RING) SetDlgItemText(hWndDlg, IDC_HWAY_AREAUSER, "LD");
		if (iValue == AREA2_LOCALE_RING) SetDlgItemText(hWndDlg, IDC_HWAY_AREAUSER, "NH");
		else SetDlgItemText(hWndDlg, IDC_HWAY_AREAUSER, "--");

		if (!pHigh->isMonitoringHighway()) {
			sstr = "dests:  ";
			CRiedel_GRDs* pToGrd = GetRiedel_GRD_Record(pHigh->getToRouter());
			if (pToGrd) {
				bncs_stringlist ssl = bncs_stringlist(pToGrd->getAllDestinationsForSource(pHigh->getToSource()));
				for (int ii = 0; ii < ssl.count(); ii++) {
					sstr.append(ssl[ii]);
					if (pToGrd->getAssocDestHighway(ssl[ii].toInt())>0)
						sstr.append(bncs_string("(%1) ").arg(pToGrd->getAssocDestHighway(ssl[ii].toInt())));
					else
						sstr.append(" ");
				}
				switch (pHigh->getHighwayState()) {
				case HIGHWAY_IFBMXRIN:
					sstr.append(" mode: ");
					sstr.append(pHigh->getAllWorkingModeEntries());
					break;
				case HIGHWAY_IFBMXROUT:
					sstr.append(" mode: ");
					sstr.append(pHigh->getAllWorkingModeEntries());
					break;
				}
			}
		}
		else {
			// mon highway
			for (int iE = 0; iE < pHigh->getNumberWorkingModeEntries(); iE++) {
				int iMode = 0, iUser = 0;
				pHigh->getWorkingMode(iE, &iMode, &iUser);
				CMCRMonPortRecord* pMon = GetMCRMonPortRecord(iUser);  
				if (pMon) {
					sstr.append(pMon->m_iConfigMonPort);  
					sstr.append(",");
				}
			}
			if (pHigh->getHighwayState() == HIGHWAY_CLONEMON) {
				sstr.append(" mode: ");
				sstr.append(pHigh->getAllWorkingModeEntries());
			}
		}
		SetDlgItemText(hWndDlg, IDC_HWAY_USERS, LPCSTR(sstr));
	}
}


void DisplayDestPortData()
{
	//clear fields
	SetDlgItemText(hWndDlg, IDC_DEST_INDX2, "");
	SetDlgItemText(hWndDlg, IDC_DEST_INFOREVS2, "");
	SetDlgItemText(hWndDlg, IDC_SRCE_HIGHWAY, "");
	SetDlgItemText(hWndDlg, IDC_DEST_HIGHWAY, "");
	SetDlgItemText(hWndDlg, IDC_DEST_INDX3, "");
	SetDlgItemText(hWndDlg, IDC_DEST_INDX4, "");
	SetDlgItemText(hWndDlg, IDC_DEST_INDX5, "");
	SetDlgItemText(hWndDlg, IDC_DEST_INDX6, "");
	SetDlgItemText(hWndDlg, IDC_DEST_INDX7, "");
	SetDlgItemText(hWndDlg, IDC_DEST_LOCKS, "");
	SetDlgItemText(hWndDlg, IDC_DEST_LOCKS2, "");
	SetDlgItemText(hWndDlg, IDC_DEST_TRACE, "");
	//
	CRiedel_Rings* pRing = GetRiedel_Rings_Record(iChosenRing);
	if (pRing) {
		int iGrdDev = pRing->iRing_Device_GRD;
		CRiedel_GRDs* pGrd = GetRiedel_GRD_Record(iGrdDev);
		if ((pGrd)&&(iChosenGRDDest>0)) {
			// port name
			SetDlgItemText(hWndDlg, IDC_DEST_INDX2, ExtractSrcNamefromDevIniFile(iGrdDev, 1, iChosenGRDDest));
			SetDlgItemText(hWndDlg, IDC_DEST_INFOREVS2, 
				LPCSTR(bncs_string("%1 %2").arg(pGrd->getDestinationMappingInfoDrv(iChosenGRDDest)).arg(pGrd->getDestinationMappingInfoSlot(iChosenGRDDest))));
			// highway ?
			SetDlgItemInt(hWndDlg, IDC_DEST_HIGHWAY, pGrd->getAssocDestHighway(iChosenGRDDest), TRUE);
			if (pGrd->getAssocDestHighway(iChosenGRDDest) == 0) {
				// is mon highway dest ??
				CRiedel_Monitors* pFromMon = GetRiedel_Monitors_ByTrunkAddressAndSlot(pRing->m_iTrunkAddress, iChosenGRDDest);
				if (pFromMon) {
					int iMonHigh = pFromMon->getAssociatedMonHighway();
					if (iMonHigh > 0) SetDlgItemInt(hWndDlg, IDC_DEST_HIGHWAY, iMonHigh, TRUE);
				}
			}
			// lock status
			if (pGrd->getLockStatus(iChosenGRDDest) > 0) {
				SetDlgItemText(hWndDlg, IDC_DEST_LOCKS, "XX");
				SetDlgItemText(hWndDlg, IDC_DEST_LOCKS2, LPCSTR(pGrd->getLockReasonTimeStamp(iChosenGRDDest)));
			}
			// routed source and name
			SetDlgItemInt(hWndDlg, IDC_DEST_INDX3, pGrd->getRoutedSourceForDestination(iChosenGRDDest), TRUE);
			SetDlgItemInt(hWndDlg, IDC_SRCE_HIGHWAY, pGrd->getAssocSrceHighway(pGrd->getRoutedSourceForDestination(iChosenGRDDest)), TRUE);
			SetDlgItemText(hWndDlg, IDC_DEST_INDX4, ExtractSrcNamefromDevIniFile(iGrdDev, 0, pGrd->getRoutedSourceForDestination(iChosenGRDDest)));

			int iSrc = pGrd->getRoutedSourceForDestination(iChosenGRDDest);
			Debug("ShowDest - %d - src %d users of src %d : %s", iChosenGRDDest, iSrc, pGrd->getNumberDestinationsUsingSource(iSrc), 
				LPCSTR(pGrd->getAllDestinationsForSource(iSrc)) );

			// is dest a mon port
			CMCRMonPortRecord* pMon = GetMCRMonPortByRingConfigDest(pRing->m_iTrunkAddress, iChosenGRDDest);
			if (pMon) {
				bncs_string sstr = bncs_string("see monitor rev index : %1").arg(pMon->m_iMonPortTranslated);
				SetDlgItemText(hWndDlg, IDC_DEST_TRACE, LPCSTR(sstr));
			}

			// traced ring srce and name
			SetDlgItemInt(hWndDlg, IDC_DEST_INDX5, pGrd->getTracedRingForDestination(iChosenGRDDest), TRUE);
			SetDlgItemInt(hWndDlg, IDC_DEST_INDX7, pGrd->getTracedSourceForDestination(iChosenGRDDest), TRUE);
			if ((pGrd->getTracedRingForDestination(iChosenGRDDest) > 0) && (pGrd->getTracedSourceForDestination(iChosenGRDDest) > 0)) {
				CRiedel_Rings* pTracRing = GetRiedel_Rings_By_TrunkAddress(pGrd->getTracedRingForDestination(iChosenGRDDest));
				if (pTracRing) {
					SetDlgItemText(hWndDlg, IDC_DEST_INDX6, ExtractSrcNamefromDevIniFile(pTracRing->iRing_Device_GRD, 0, pGrd->getTracedSourceForDestination(iChosenGRDDest)));
					CHighwayRecord* pHigh = GetHighwayRecord(pGrd->getAssocSrceHighway(pGrd->getRoutedSourceForDestination(iChosenGRDDest)));
					if (pHigh) {
						// get trace str
						iTraceCount = 0;
						ssl_Calc_Trace = bncs_stringlist("", '|');  // clear global var - populated in TraceThePrimarySource
						int iTracedRing = pGrd->getRingTrunk();
						int iTracedGrd = pGrd->getDevice();
						int iTracedSrc = pGrd->getRoutedSourceForDestination(iChosenGRDDest);
						TraceThePrimarySource(&iTracedRing, &iTracedGrd, &iTracedSrc);
						ssl_Calc_Trace.append(bncs_string("%1.%2").arg(pGrd->getRingTrunk()).arg(pGrd->getRoutedSourceForDestination(iChosenGRDDest)));
						SetDlgItemText(hWndDlg, IDC_DEST_TRACE, LPCSTR(ssl_Calc_Trace.toString(' ')));
					}
				}
			}
		}
	}
}

void DisplayMonitorPortData()
{
	int iChosenTrunkAddr = 0;
	//clear fields
	SendDlgItemMessage(hWndDlg, IDC_RIEDEL_MONREVS, LB_RESETCONTENT, 0, 0);
	SetDlgItemText(hWndDlg, IDC_DEST_INDX8, "");
	SetDlgItemText(hWndDlg, IDC_MONPORT_RECORD, "");
	char szData[MAX_AUTOBFFR_STRING] = "";
	if ((iChosenMonitorPort > 0)&&(iChosenMonitorPort<=MAX_RIEDEL_SLOTS)) {
		for (int iRs = 1; iRs <= iNumberRiedelRings;iRs++) {
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRs);
			if (pRing) {

				if (iRs == iChosenRing)  { // get name for mon port from first ring
					strcpy(szData, ExtractSrcNamefromDevIniFile(pRing->iRing_Device_Monitor, 0, iChosenMonitorPort));
					SetDlgItemText(hWndDlg, IDC_DEST_INDX8, szData);
					iChosenTrunkAddr = pRing->m_iTrunkAddress;
				}

				// get rev record for mon port for  ring
				int iRevRec = pRing->iRing_RevsRecord_Monitors + iChosenMonitorPort;
				CRiedel_Monitors* pMon = GetRiedel_Monitors_Record(iRevRec);
				if (pMon) {
					wsprintf(szData, "%d : %s", pRing->m_iTrunkAddress, LPCSTR(pMon->getRawRiedelRevertive()));
					if (pMon->getRawRiedelRevertive().find(",S") >= 0) {
						bncs_string sstr = pMon->getRawRiedelRevertive().left(pMon->getRawRiedelRevertive().length() - 2);
						CHighwayRecord* pHigh = GetHighwayRecordByToTrunkAndSrce(pRing->m_iTrunkAddress, sstr.toInt(), TRUE);
						if (pHigh) {
							char szExtra[32] = "";
							wsprintf( szExtra, " - high indx %d", pHigh->getRecordIndex());
							strcat(szData, szExtra);
						}
					}
					SendDlgItemMessage(hWndDlg, IDC_RIEDEL_MONREVS, LB_INSERTSTRING, -1, (LPARAM)szData);
				}
				else
					Debug("DisplayMon - no rev rec class %d for ring %d monitor %d", iRevRec, iChosenRing, iChosenMonitorPort);
			}
		} // for int iRs
		// is this dest assoc to a monitor highway ?
		if (iChosenTrunkAddr > 0) {
			CHighwayRecord* pHigh = GetHighwayRecordByFromTrunkAndDest(iChosenTrunkAddr, iChosenMonitorPort, FALSE);  // after rev index == mon port
			if (pHigh) {
				SetDlgItemText(hWndDlg, IDC_MONPORT_RECORD, LPCSTR(bncs_string("high, idx:%1 trk:%2").arg(pHigh->getRecordIndex()).arg(pHigh->getFromTrunkAddr())));
			}
			// is this dest linked to a monitor port record ?
			CMCRMonPortRecord* pMonRec = GetMCRMonPortByRingTransDest(iChosenTrunkAddr, iChosenMonitorPort);
			if (pMonRec) {
				SetDlgItemText(hWndDlg, IDC_MONPORT_RECORD, LPCSTR(bncs_string("mon:%1 %2 %3").arg(pMonRec->m_iRecordIndex).arg(pMonRec->m_iUsingHighwayRecord).arg(pMonRec->m_ssCommandRevertive)));
			}
		}
	}
}

void DisplayConferenceData()
{
	SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_RESETCONTENT, 0, 0);
	SetDlgItemText(hWndDlg, IDC_CONIFB_LBL, ""); SetDlgItemText(hWndDlg, IDC_CONF_IFB_REV, "");
	SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "");
	SetDlgItemText(hWndDlg, IDC_CONIFB_LBL2, "");
	SetDlgItemText(hWndDlg, IDC_CONF_IFB_MONS, "");
	if ((iChosenConference > 0) && (iChosenConference <= MAX_CONFERENCES)) {
		CRingMaster_CONF* pRMConf = GetRingMaster_CONF_Record(iChosenConference);
		if (pRMConf) {
			SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "CONF");
			char szStr[MAX_AUTOBFFR_STRING] = "";
			// get the revs records for rings 
			for (int ic = 1; ic < MAX_RIEDEL_RINGS; ic++) {
				if (pRMConf->getRiedelRevIndex(ic)>0) {
					CRiedel_Conferences* pConf = GetRiedel_Conference_Record(pRMConf->getRiedelRevIndex(ic));
					if (pConf) {
						wsprintf(szStr, "%d : %s*%s*%s", ic, LPCSTR(pConf->getConference_Members()),
							LPCSTR(pConf->getConference_PanelMembers()), LPCSTR(pConf->getConference_Name()));
						SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
					}
				}
			} // for ic
			//
			SetDlgItemText(hWndDlg, IDC_CONIFB_LBL, LPCSTR(pRMConf->getRingMasterLabel()));
			wsprintf(szStr, "%s*%s*%s", LPCSTR(pRMConf->getRingMasterConfMembers()),
				LPCSTR(pRMConf->getRingMasterPanelMembers()), LPCSTR(pRMConf->getRingMasterLabel()));
			SetDlgItemText(hWndDlg, IDC_CONF_IFB_REV, szStr);
			// conf used by these monitors
			SetDlgItemText(hWndDlg, IDC_CONF_IFB_MONS, LPCSTR(pRMConf->getFromMonitorMembers()));
		}
	}
}

void DisplayMixerData()
{
	SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_RESETCONTENT, 0, 0);
	SetDlgItemText(hWndDlg, IDC_CONIFB_LBL, ""); 
	SetDlgItemText(hWndDlg, IDC_CONF_IFB_REV, "");
	SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "");
	SetDlgItemText(hWndDlg, IDC_CONIFB_LBL2, "");
	SetDlgItemText(hWndDlg, IDC_CONF_IFB_MONS, "");
	if ((iChosenMixer > 0) && (iChosenMixer <= MAX_MIXERS_LOGIC)) {
		// get the revs records for all rings for chosen mixer
		for (int ic = 1; ic <=iNumberRiedelRings; ic++) {
			//
			CRingMaster_MIXER* pRMMixer = GetRingMaster_MIXER_Record( ((ic-1)*MAX_MIXERS_LOGIC)+iChosenMixer);
			CRiedel_Mixers* pmxr = GetRiedel_Mixers_Record( ((ic - 1)*MAX_MIXERS_LOGIC) + iChosenMixer );
			if (pRMMixer&&pmxr) {
				SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "MXR");
				char szStr[MAX_AUTOBFFR_STRING] = "";
				wsprintf(szStr, "t %d: %s|%s", pRMMixer->getAssociatedTrunkAddress(), LPCSTR(pmxr->getMixer_Inputs()), LPCSTR(pmxr->getMixer_Outputs()));
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
				wsprintf(szStr, "rmr: %s", LPCSTR(pRMMixer->getRingMasterRevertive()));
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
				strcpy(szStr, "-----------");
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
			}
		} // for ic
	}
}

void DisplayIFBData()
{
	SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_RESETCONTENT, 0, 0);
	SetDlgItemText(hWndDlg, IDC_CONIFB_LBL, ""); SetDlgItemText(hWndDlg, IDC_CONF_IFB_REV, "");
	SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "");
	SetDlgItemText(hWndDlg, IDC_CONIFB_LBL2, "");
	SetDlgItemText(hWndDlg, IDC_CONF_IFB_MONS, "");
	if ((iChosenIFB > 0)&&(iChosenIFB<=MAX_IFBS)) {
		//		
		SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "IFB");
		for (int ic = 1; ic <= iNumberRiedelRings; ic++) {
			CRingMaster_IFB* pRMIFB = GetRingMaster_IFB_Record(((ic - 1)*MAX_IFBS) + iChosenIFB);
			CRiedel_IFBs* pIFB = GetRiedel_IFBs_Record(((ic - 1)*MAX_IFBS) + iChosenIFB);
			if (pRMIFB&&pIFB) {
				char szStr[MAX_AUTOBFFR_STRING] = "";
				wsprintf(szStr, "t %d: %s", pRMIFB->getAssociatedTrunkAddress(), LPCSTR(pIFB->getRawRiedelRevertive()));
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
				wsprintf(szStr, "rmr: %s", LPCSTR(pRMIFB->getRingMasterRevertive()));
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
				// conf used by these monitors
				wsprintf(szStr, "mon: %s", LPCSTR(pRMIFB->getIFBMonitorMembers()));
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
				strcpy(szStr, "-----------");
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
			}
		}
	}
}


void DisplayLogicData()
{
	SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_RESETCONTENT, 0, 0);
	SetDlgItemText(hWndDlg, IDC_CONIFB_LBL, ""); SetDlgItemText(hWndDlg, IDC_CONF_IFB_REV, "");
	SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "");
	SetDlgItemText(hWndDlg, IDC_CONIFB_LBL2, "");
	SetDlgItemText(hWndDlg, IDC_CONF_IFB_MONS, "");

	if ((iChosenLogic > 0) && (iChosenLogic <= MAX_MIXERS_LOGIC)) {
		// get the revs records for all rings for chosen mixer
		/**********************  xxxx to re do
		for (int ic = 1; ic <= iNumberRiedelRings; ic++) {
			//
			CRingMaster_LOGIC* pRMLogic = GetRingMaster_LOGIC_Record(((ic - 1)*MAX_MIXERS_LOGIC) + iChosenMixer);
			CRiedel_Mixers* pmxr = GetRiedel_Mixers_Record(((ic - 1)*MAX_MIXERS_LOGIC) + iChosenMixer);
			if (pRMMixer&&pmxr) {
				SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "MXR");
				SetDlgItemText(hWndDlg, IDC_CONIFB_LBL2, LPCSTR(bncs_string("ring %1").arg(pRMMixer->getAssociatedTrunkAddress())));
				char szStr[MAX_AUTOBFFR_STRING] = "";
				wsprintf(szStr, "%d : %s|%s", pRMMixer->getAssociatedTrunkAddress(), LPCSTR(pmxr->getMixer_Inputs()), LPCSTR(pmxr->getMixer_Outputs()));
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
				wsprintf(szStr, "%d : %s", pRMMixer->getAssociatedTrunkAddress(), LPCSTR(pRMMixer->getRingMasterRevertive()));
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
				strcpy(szStr, "-----------");
				SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
			}
		} // for ic
	}
		CRingMaster_LOGIC* pRMLogic = GetRingMaster_LOGIC_Record(iChosenLogic);
		if (pRMLogic) {
			int iAssocIFB = pRMLogic->getAssociatedIFB();
			//
			CRingMaster_IFB* pRMIFB = GetRingMaster_IFB_Record(iAssocIFB);
			if (pRMIFB) {
				char szStr[MAX_AUTOBFFR_STRING] = "";
				SetDlgItemText(hWndDlg, IDC_CON_OR_IFB, "LOG");
				SetDlgItemText(hWndDlg, IDC_CONIFB_LBL2, LPCSTR(bncs_string("ring %1").arg(pRMLogic->getAssociatedTrunkAddress())));
				char sztxt[128] = "";
				wsprintf(sztxt, "ifb:%d port:%d", pRMLogic->getAssociatedIFB(), pRMLogic->getAssociatedListenToPort());
				SetDlgItemText(hWndDlg, IDC_CONIFB_LBL, sztxt);
				// get the 6 revs records for first 6 rings 
				for (int ic = 1; ic < MAX_RIEDEL_RINGS; ic++) {
					if (pRMLogic->getRiedelRevIndex(ic)>0) {
						CRiedel_Logic* plog = GetRiedel_Logic_Record(pRMLogic->getRiedelRevIndex(ic));
						if (plog) {
							wsprintf(szStr, "%d : %d", ic, LPCSTR(plog->getLogicState()));
							SendDlgItemMessage(hWndDlg, IDC_RIEDEL_REVS, LB_INSERTSTRING, -1, (LPARAM)szStr);
						}
					}
				} // for ic
				//
				wsprintf(szStr, "%s", LPCSTR(pRMLogic->getRingMasterRevertive()));
				SetDlgItemText(hWndDlg, IDC_CONF_IFB_REV, szStr);
				// conf used by these monitors
				SetDlgItemText(hWndDlg, IDC_CONF_IFB_MONS, LPCSTR(pRMIFB->getListen2PortMonitorMembers()));
			}
		}
		****************************/
	}
}


//////////////////////////////////////////////////////////////////////////
// 
//  AUTOMATIC STARTUP FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


BOOL GetRouterDeviceDBSizes( bncs_string ssName, int *iRtrDevice, int *iDB0Size, int *iDB1Size ) 
{
	char szFileName[64]="", szEntry[12]="";
	int iNewDevice = 0;
	*iDB0Size=0; *iDB1Size=0;
	iNewDevice=getInstanceDevice( ssName );
	strcpy( szFileName, ssName );
	if (iNewDevice>0) {
		*iRtrDevice = iNewDevice;
		// get database sizes db0 and db1
		wsprintf(szFileName, "dev_%03d.ini", iNewDevice );
		*iDB0Size = atoi( r_p(szFileName, "Database", "DatabaseSize_0", "", false) );
		*iDB1Size = atoi( r_p(szFileName, "Database", "DatabaseSize_1", "", false) );
		return TRUE;
	}
	else 
		Debug( "GetRouterDeviceDBSizes - dev %s has ret 0", LPCSTR(ssName));
	return FALSE;
}


//
//  FUNCTION: ExtractSrcNamefromDevIniFile( )
//
//  PURPOSE: Extracts the src name from specified Database dev ini file and returns string 
//
char* ExtractSrcNamefromDevIniFile( int iDev, int iDbFile, int iEntry )
{
	char szFileName[MAX_AUTOBFFR_STRING], szEntry[32], szDatabase[32];
	if ((iDev>0)&&(iEntry>0)) {	
		sprintf(szFileName, "dev_%03d.db%d", iDev, iDbFile);
		sprintf(szEntry, "%04d", iEntry );
		sprintf(szDatabase, "Database_%d", iDbFile );
		return r_p( szFileName, szDatabase, szEntry,"",FALSE);
	}
	return "";
}


char* GetParameterString( bncs_string sstr )
{
	strcpy( szResult, "");
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		wsprintf( szResult, "%s", LPCSTR(ssl1[1]) );
	return szResult;

}

int GetParameterInt( bncs_string sstr  )
{
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		return ssl1[1].toInt();
	else
		return UNKNOWNVAL;
}			

void GetTwinParameters( bncs_string sstr, int *i1, int *i2 )
{
	*i1=0;
	*i2=0;
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '/' );
	if (ssl1.count()>0) *i1 = ssl1[0].toInt();
	if (ssl1.count()>1) *i2 = ssl1[1].toInt();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: ConnectAllInfodrivers()
//
//  PURPOSE:connects to 4 infodrivers needed for app to run
//  if it fails then no point in continuing to run - so flags major error
//			

BOOL ConnectAllInfodrivers( )
{

	if (cl_KeyInstances) {

		bValidCollediaStatetoContinue = TRUE;

		// this ringmaster auto
		// get the info device numbers from config xml file pointing to intstances
		i_info_RingMaster = cl_KeyInstances->i_dev_Rmstr_Main;
		i_info_Conferences = cl_KeyInstances->i_dev_Rmstr_Confs;
		i_info_Ports_PTI = cl_KeyInstances->i_dev_Rmstr_PortsPTI;
		if ((iNumberRiedelRings > 0) && (iNumberRiedelRings < MAX_RIEDEL_RINGS)) {
			i_info_IFBs_Start = cl_KeyInstances->i_dev_Rmstr_IFBs[1];
			i_info_IFBs_End = cl_KeyInstances->i_dev_Rmstr_IFBs[iNumberRiedelRings];
			//
			i_info_Ports_Start = cl_KeyInstances->i_dev_Rmstr_Ports[1];
			i_info_Ports_End = cl_KeyInstances->i_dev_Rmstr_Ports[iNumberRiedelRings];
		}

		// check and verify infodriver values / ranges - no overlaps etc -- determine V1 or V2 running style etc
		// check for assigned values
		if (i_info_RingMaster < 1) {
			Debug("ConnectAllInfodrivers - ERROR - NO MAIN AUTO INFODRIVER assigned - FIX CONFIG - auto STOPPING");
			bValidCollediaStatetoContinue = FALSE;
		}
		if (i_info_Conferences < 1) {
			Debug("ConnectAllInfodrivers - ERROR - NO CONFERENCESINFODRIVER assigned - FIX CONFIG - auto STOPPING");
			bValidCollediaStatetoContinue = FALSE;
		}
		if (i_info_IFBs_Start < 1) {
			Debug("ConnectAllInfodrivers - ERROR - NO IFBS START INFODRIVER config defintion - FIX CONFIG - auto STOPPING");
			bValidCollediaStatetoContinue = FALSE;
		}
		if (i_info_Ports_Start < 1) {
			Debug("ConnectAllInfodrivers - ERROR - NO PORTS START INFODRIVER config defintion - FIX CONFIG - auto STOPPING");
			bValidCollediaStatetoContinue = FALSE;
		}
		if (i_info_Ports_PTI < 1) {
			Debug("ConnectAllInfodrivers - ERROR - NO PORTS PTI INFODRIVER config defintion - FIX CONFIG - auto STOPPING");
			bValidCollediaStatetoContinue = FALSE;
		}

		for (int iiff = i_info_IFBs_Start; iiff <= i_info_IFBs_End; iiff++) {
			if (iiff == i_info_Conferences)  {
				Debug("ConnectAllInfodrivers - ERROR - IFB INFO %d clash with CONFERNCES ", iiff);
				bValidCollediaStatetoContinue = FALSE;
			}
			if (((iiff >= i_info_Ports_Start) && (iiff <= i_info_Ports_End)) || (iiff == i_info_Ports_PTI)) {
				Debug("ConnectAllInfodrivers - ERROR - IFB INFO %d clash with PORTS INFOS ", iiff);
				bValidCollediaStatetoContinue = FALSE;
			}
		}

		SetDlgItemInt(hWndDlg, IDC_AUTO_INFNUM1, i_info_RingMaster, TRUE);
		SetDlgItemInt(hWndDlg, IDC_AUTO_INFNUM2, i_info_Conferences, TRUE);
		SetDlgItemInt(hWndDlg, IDC_AUTO_INFNUM7, i_info_Ports_PTI, TRUE);
		char szData[32] = "";
		wsprintf(szData, "%d-%d", i_info_Ports_Start, i_info_Ports_End);
		SetDlgItemText(hWndDlg, IDC_AUTO_INFNUM4, szData);
		wsprintf(szData, "%d-%d", i_info_IFBs_Start, i_info_IFBs_End);
		SetDlgItemText(hWndDlg, IDC_AUTO_INFNUM3, szData);

		Debug("ConnectAllInfodrivers - COMPOSITE config - RMstr is %d / confs %d/  ifbs %d to %d / ports %d to %d, pti %d",
			i_info_RingMaster, i_info_Conferences, i_info_IFBs_Start, i_info_IFBs_End, i_info_Ports_Start, i_info_Ports_End, i_info_Ports_PTI);

		if (bValidCollediaStatetoContinue) {

			// connect to External Infodrivers
			eiRingMasterInfoId = new extinfo;
			eiRingMasterInfoId->notify(InfoNotify);
			switch (eiRingMasterInfoId->connect(i_info_RingMaster)) { // connect to infodriver
			case CONNECTED:
				if (ecCSIClient) ecCSIClient->regtallyrange(eiRingMasterInfoId->iDevice, 4096, 4096, INSERT);
				eiRingMasterInfoId->setRedundancyMaster(i_info_RingMaster);
				Debug("Connected OK to main ringmaster infodriver %d", eiRingMasterInfoId->iDevice);
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "Connected OK");
				break;
			default:
				Debug("ERROR ERROR - Connect failed to main infodriver, code %d", eiRingMasterInfoId->connect(i_info_RingMaster));
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "Info connect FAILED");
				bValidCollediaStatetoContinue = FALSE;
			}

			eiConferencesInfoId = new extinfo;
			eiConferencesInfoId->notify(InfoNotify);
			switch (eiConferencesInfoId->connect(i_info_Conferences)) { // connect to infodriver
			case CONNECTED:
				if (ecCSIClient) ecCSIClient->regtallyrange(eiConferencesInfoId->iDevice, 4096, 4096, INSERT);
				eiConferencesInfoId->setRedundancyMaster(i_info_RingMaster);
				Debug("Connected OK to conference infodriver %d", i_info_Conferences);
				break;
			default:
				Debug("ERROR ERROR - Connect failed to infodriver 3, code %d", eiConferencesInfoId->connect(i_info_Conferences));
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "Conf Info error");
				bValidCollediaStatetoContinue = FALSE;
			}

			eiPortsPTIInfoId = new extinfo;
			eiPortsPTIInfoId->notify(InfoNotify);
			switch (eiPortsPTIInfoId->connect(i_info_Ports_PTI)) { // connect to infodriver
			case CONNECTED:
				if (ecCSIClient) ecCSIClient->regtallyrange(eiPortsPTIInfoId->iDevice, 4096, 4096, INSERT);
				eiPortsPTIInfoId->setRedundancyMaster(i_info_RingMaster);
				Debug("Connected OK to ports pti infodriver %d", eiPortsPTIInfoId->iDevice);
				break;
			default:
				Debug("ERROR ERROR - Connect failed to infodriver 5, code %d", eiPortsPTIInfoId->connect(i_info_Ports_PTI));
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "Port pti error");
				bValidCollediaStatetoContinue = FALSE;
			}

			for (int iirr = 1; iirr <= iNumberRiedelRings; iirr++) {
				// ifb
				int iIFBInfo = cl_KeyInstances->i_dev_Rmstr_IFBs[iirr];
				if (iIFBInfo > 0) {
					eiIFBsInfoIds[iirr] = new extinfo;
					if (eiIFBsInfoIds[iirr]) {
						eiIFBsInfoIds[iirr]->notify(InfoNotify);
						switch (eiIFBsInfoIds[iirr]->connect(iIFBInfo)) { // connect to infodriver
						case CONNECTED:
							if (ecCSIClient) ecCSIClient->regtallyrange(eiIFBsInfoIds[iirr]->iDevice, 4096, 4096, INSERT);
							eiIFBsInfoIds[iirr]->setRedundancyMaster(i_info_RingMaster);
							Debug("Connected OK to ifb infodriver %d", eiIFBsInfoIds[iirr]->iDevice);
							break;
						default:
							Debug("ERROR ERROR - Connect failed to IFB-%d infodriver %d, code %d", iirr, iIFBInfo, eiIFBsInfoIds[iirr]->connect(iirr));
							SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "ifb connect error");
							bValidCollediaStatetoContinue = FALSE;
						}
					}
					else {
						Debug("ERROR ERROR - CREATE INFO failed to IFB-%d infodriver %d", iirr, iIFBInfo);
						SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "ifb class fail");
						bValidCollediaStatetoContinue = FALSE;
					}
				}
				// ports
				int iPortInfo = cl_KeyInstances->i_dev_Rmstr_Ports[iirr];
				if (iPortInfo > 0) {
					eiPortsInfoIds[iirr] = new extinfo;
					if (eiPortsInfoIds[iirr]) {
						eiPortsInfoIds[iirr]->notify(InfoNotify);
						switch (eiPortsInfoIds[iirr]->connect(iPortInfo)) { // connect to infodriver
						case CONNECTED:
							if (ecCSIClient) ecCSIClient->regtallyrange(eiPortsInfoIds[iirr]->iDevice, 4096, 4096, INSERT);
							eiPortsInfoIds[iirr]->setRedundancyMaster(i_info_RingMaster);
							Debug("Connected OK to Ports infodriver %d", eiPortsInfoIds[iirr]->iDevice);
							break;
						default:
							Debug("ERROR ERROR - Connect failed to PORTS-%d infodriver %d, code %d", iirr, iPortInfo, eiPortsInfoIds[iirr]->connect(iPortInfo));
							SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "port connect error");
							bValidCollediaStatetoContinue = FALSE;
						}
					}
					else {
						Debug("ERROR ERROR - CREATE INFO failed to PORTS-%d infodriver %d", iirr, iPortInfo);
						SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "port class fail");
						bValidCollediaStatetoContinue = FALSE;
					}
				}
			} // for iirr

			if (bValidCollediaStatetoContinue) {
				// check on infodrivers status -- really ought to be either all TX/RX or all RXOnly -- a mix is not good !!
				// when determining overall status -- main infodriver ( iDevice ) is very important
				iOverallTXRXModeStatus = 0; // unknown first time
				// continue on to initialise vars, get object settings, dev registrations and poll for revs 
				eiRingMasterInfoId->updateslot(COMM_STATUS_SLOT, "1");	 // ok	
				return TRUE;
			}
		}
	}
	else
		Debug("error - no key instances class");

	// something has failed so flag it
	iLabelAutoStatus=2;
	SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
	return FALSE;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: LoadRiedelDBData()
//
//  COMMENTS:  All this riedel data is required for IFBs and CONFs
//			
void CreateRiedel_AllPortTypes( )
{
	/*
	int iIndx=1;
	for (iIndx=1;iIndx<=MAX_RIEDEL_SLOTS;iIndx++)	{
		// init rec
		iAllIFBPortTypes[iIndx]=0;
		iAllTBPortTypes[iIndx]=0;
		// get data 
		iAllTBPortTypes[iIndx] = atoi( ExtractSrcNamefromDevIniFile(i_rtr_Riedel_TB_GRD, DATABASE_RIEDEL_PORT_TYPE, iIndx ) );
		iAllIFBPortTypes[iIndx] = atoi( ExtractSrcNamefromDevIniFile(i_rtr_Riedel_IFB_GRD, DATABASE_RIEDEL_PORT_TYPE, iIndx ) );
	}
	*/
}

// Riedel Database info
int CreateRiedel_TBPortKeys( int iWhichDb)
{
	int iEntries=0;
	/*
	int iIndx=1;
	BOOL bCont=TRUE;
	string szData;
	char szEntry[MAX_AUTOBFFR_STRING];
	do {
		szData =  ExtractSrcNamefromDevIniFile(i_rtr_Riedel_TB_Monitor, iWhichDb, iIndx );
		
		if (szData.length()>0)
		{
			//Start Mantis 1166
			AdjustRiedel_PortKeyData(szData);
			//End Mantis 1166

			iEntries++;
			CRiedel_LookUp* cData = new CRiedel_LookUp( iIndx, szData, TRUE );
			if (cData) {
				// add to map
				mapOfAllTBPortKeys->insert(pair<int, CRiedel_LookUp*>(iIndx, cData));
				// add to TB keys list box
				wsprintf( szEntry, "%04d: %s  ", iIndx, szData.c_str()); 
				SendDlgItemMessage(hWndDlg, IDC_LIST5, LB_INSERTSTRING, -1, (LPARAM)szEntry);
			}
			else
				Debug( "CreateRiedel_TBPortKeys - data rec failed %d ", iIndx );
		}
		else
			bCont=FALSE;
		iIndx++;
	}
	while ((bCont)&&(iIndx<=MAX_RIEDEL_SLOTS));
	*/
	return iEntries;
}


void AdjustRiedel_PortKeyData(string & szData)
{

//Start Mantis 1166
	size_t iCount=0;
	size_t iPos=0;

	// Check for "NULL" at start of string.
	// If found ignore and return

	if (szData.find("NULL", 0) > 0)
	{

		// Check to see if we are reading old or new config IFB data
		// Old = "Port"
		// OR Old = "Port, Key"
		// New = "Port,Panel,Shift,Key"

		iCount = (std::count(szData.begin(), szData.end(), '.'));

		if(0 == iCount)
		{
			// Append to the string the values '0.1.0' to update it to the latest config setting.
			// Should now be Port/Panel/Shift/Key we are setting it to "x.0.1.0"

			szData.append(".0.1.0");
		}
		else if(1 == iCount)
		{	
			// Insert in the string the values '0.1' to update it to the latest config setting.
			// Should now be Port/Panel/Shift/Key we are setting it to "x.0.1.x"
	
			iPos = (szData.find('.', 0));
			szData.insert(iPos,".0.1");
		}
	}
//End Mantis 1166
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    Housekeeping functions run at regular interval
//

void CalculateTrunkNavigatorUseForRing(CRiedel_Rings* pRing)
{
	// get totals from riedel trunk records for given ring - update main infodriver slots
	if (pRing) {
		int iTrunksKnown = 0, iTrunksInUse = 0;
		for (int iRec = 1; iRec <= MAX_DRIVER_TRUNKS; iRec++) {
			//
			CRiedel_Trunks* pTr = GetRiedel_Trunks_Record(pRing->iRing_RevsRecord_Trunks + iRec);
			if (pTr) {
				bncs_stringlist ssl = bncs_stringlist(pTr->getRawRiedelRevertive(), '*');
				if (ssl.count() > 0) {
					bncs_stringlist ssl2 = bncs_stringlist(ssl[0], '|');
					if (ssl2.count() > 2) {
						int iDestAddr = ssl2[0].toInt();
						int iDestPort = ssl2[1].toInt();
						int iInUse = ssl[2].toInt();
						if ((iDestAddr > 0) && (iDestPort>0)) {
							iTrunksKnown++;
							if (iInUse > 0) iTrunksInUse++;
						}
					}
				}
			}
			else
				Debug("CalcTrunkNavUse - no trunk rec %d for ring %d starting at %d", iRec, pRing->m_iTrunkAddress, pRing->iRing_RevsRecord_Trunks);
		} // for irec
		// update ring and infodrv
		pRing->iDefinedTrunksAvailable = iTrunksKnown;
		pRing->iCurrentTrunksInUse = iTrunksInUse;
		//
		char szData[MAX_AUTOBFFR_STRING] = "";
		wsprintf(szData, "ring=%d,trunk_lines=%d,in_use=%d", pRing->m_iTrunkAddress, pRing->iDefinedTrunksAvailable, pRing->iCurrentTrunksInUse);
		eiRingMasterInfoId->updateslot(RIEDEL_TRUNK_INFO_TOTALUSAGE + pRing->m_iTrunkAddress, szData);
	}
}


void CalculateHighwayStatistics()
{
	// calc stats to post for STD and MON highways info / rings info on OUTGOING usage basis
	int iTotalStdHighwaysInUse = 0;
	int iTotalMonHighwaysInUse = 0;
	char szStdData[MAX_AUTOBFFR_STRING] = "";
	char szMonData[MAX_AUTOBFFR_STRING] = "";
	int iToRingsStdCounter[MAX_RIEDEL_RINGS];
	int iToRingsMonCounter[MAX_RIEDEL_RINGS];
	//
	for (int iFromRing = 1; iFromRing <= iNumberRiedelRings; iFromRing++) {
		CRiedel_Rings* pFromRing = GetRiedel_Rings_Record(iFromRing);
		if (pFromRing) {
			int iStdInUseFromRing = 0;
			int iStdHighsOutFromRing = 0;
			int iMonInUseFromRing = 0;
			int iMonHighsOutFromRing = 0;
			for (int ii = 0; ii < MAX_RIEDEL_RINGS; ii++) {
				iToRingsStdCounter[ii] = 0;
				iToRingsMonCounter[ii] = 0;
			}
			for (int iToRing = 1; iToRing <= iNumberRiedelRings; iToRing++) {
				if (iToRing != iFromRing) { // ignore yourself
					CRiedel_Rings* pToRing = GetRiedel_Rings_Record(iToRing);
					if (pToRing) {
						// std highways
						bncs_string sstr = pFromRing->getOutgoingHighways(pToRing->m_iTrunkAddress);
						// add any mon highways
						if (pFromRing->getNumberOutgoingMonitoring(pToRing->m_iTrunkAddress) > 0) {
							sstr.append(",");
							sstr.append(pFromRing->getOutgoingMonitoring(pToRing->m_iTrunkAddress));
						}
						bncs_stringlist ssl_highways = bncs_stringlist(sstr, ',');
						for (int iH = 0; iH < ssl_highways.count(); iH++) {
							CHighwayRecord* pHigh = GetHighwayRecord(ssl_highways[iH].toInt());
							if (pHigh) {
								if (pHigh->isMonitoringHighway() ) {
									iMonHighsOutFromRing++;
									if (pHigh->getHighwayState()>HIGHWAY_FREE) {  // ie in use
										iToRingsMonCounter[iToRing]++;
										iTotalMonHighwaysInUse++;
										iMonInUseFromRing++;
									}
								}
								else {  // standard highway
									iStdHighsOutFromRing++;
									if (pHigh->getHighwayState() > HIGHWAY_FREE) {  // ie in use
										iToRingsStdCounter[iToRing]++;
										iTotalStdHighwaysInUse++;
										iStdInUseFromRing++;
									}
								}
							}
						} // for iH
					} 
					//Debug("CalcStats - from ring %d to ring %d highways out %d %s total %d ", iFromRing, iToRing, ssl_highways.count(), LPCSTR(ssl_highways.toString(',')), iTotalHighsOutFromRing);
				}
			} // for iToRIng
			// update totals from ring
			strcpy(szStdData, "");
			strcpy(szMonData, "");
			int iHighestPercent = 0, iHighestToRing = 0;
			for (int iR = 1; iR <= iNumberRiedelRings; iR++) {
				CRiedel_Rings* pRing = GetRiedel_Rings_Record(iR);
				if (pRing) {
					char szdatum[16] = "";
					char szdatumon[16] = "";
					wsprintf(szdatum, "%d,%d", iToRingsStdCounter[iR], pFromRing->getNumberOutgoingHighways(pRing->m_iTrunkAddress));
					wsprintf(szdatumon, "%d,%d", iToRingsMonCounter[iR], pFromRing->getNumberOutgoingMonitoring(pRing->m_iTrunkAddress));
					if (iR > 1) {
						strcat(szStdData, " | ");
						strcat(szMonData, " | ");
					}
					strcat(szStdData, szdatum);
					strcat(szMonData, szdatumon);
					// check for over 90% usage of standard highways from that ring-- if so set alarm flag
					int iPercent = 0;
					if ((iToRingsStdCounter[iR] > 0) && (pFromRing->getNumberOutgoingHighways(pRing->m_iTrunkAddress) > 0)) {
						iPercent = int(float(float(iToRingsStdCounter[iR]) / float(pFromRing->getNumberOutgoingHighways(pRing->m_iTrunkAddress))) * 100);
						if (iPercent >= iHighestPercent) {
							iHighestPercent = iPercent;
							iHighestToRing = pRing->m_iTrunkAddress;
						}
					}
				}
			} // for int iR
			//
			char szdatum[16] = "";
			char szdatumon[16] = "";
			wsprintf(szdatum, " | %d,%d", iStdInUseFromRing, iStdHighsOutFromRing);
			wsprintf(szdatumon, " | %d,%d", iMonInUseFromRing, iMonHighsOutFromRing);
			strcat(szStdData, szdatum);
			strcat(szMonData, szdatumon);
			// check for over 90% overall usage of standard highways from that ring-- if so set alarm flag
			int iPercent = 0;
			if ((iStdInUseFromRing > 0) && (iStdHighsOutFromRing > 0)) {
				iPercent = int(float(float(iStdInUseFromRing) / float(iStdHighsOutFromRing)) * 100);
				if (iPercent >= iHighestPercent) {
					iHighestPercent = iPercent;
					iHighestToRing = 0;
				}
			}
			SetAlarmThresholdMessage(pFromRing->m_iTrunkAddress, iHighestToRing, iHighestPercent);
			// update gui
			switch (iFromRing) {
			case 1:
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_RING1, iStdInUseFromRing, TRUE);
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_MON1, iMonInUseFromRing, TRUE);
				break;
			case 2:
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_RING2, iStdInUseFromRing, TRUE);
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_MON2, iMonInUseFromRing, TRUE);
				break;
			case 3:
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_RING3, iStdInUseFromRing, TRUE);
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_MON3, iMonInUseFromRing, TRUE);
				break;
			case 4:
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_RING4, iStdInUseFromRing, TRUE);
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_MON4, iMonInUseFromRing, TRUE);
				break;
			case 5:
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_RING5, iStdInUseFromRing, TRUE);
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_MON5, iMonInUseFromRing, TRUE);
				break;
			case 6:
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_RING6, iStdInUseFromRing, TRUE);
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_MON6, iMonInUseFromRing, TRUE);
				break;
			}
		}
		if (strlen(szStdData) > 255) szStdData[255] = NULL;
		strcpy(szClientCmd, szStdData);
		eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_TOTALUSAGE + pFromRing->m_iTrunkAddress, szClientCmd);
		if (strlen(szMonData) > 255) szMonData[255] = NULL;
		strcpy(szClientCmd, szMonData);
		eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_CLONE_USAGE + pFromRing->m_iTrunkAddress, szClientCmd);
	} // for iFromRing
	// totals 
	wsprintf(szClientCmd, "%d,%d", iTotalStdHighwaysInUse, (iNumberBNCSHighways - iNumberMCRMonHighways));
	eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_TOTALUSAGE, szClientCmd);
	SetDlgItemText(hWndDlg, IDC_AUTO_HIGHWAYS, szClientCmd);
	// check for over 90% usage of standard highways-- if so set alarm flag
	int iPercent = 0;
	if ((iTotalStdHighwaysInUse > 0) && ((iNumberBNCSHighways - iNumberMCRMonHighways) > 0)) {
		iPercent = int( float(float(iTotalStdHighwaysInUse) / float(iNumberBNCSHighways - iNumberMCRMonHighways)) * 100);
		SetAlarmThresholdMessage(0, 0, iPercent);  // generic overall message
	}
	//
	wsprintf(szClientCmd, "%d,%d", iTotalMonHighwaysInUse, iNumberMCRMonHighways);
	eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_CLONE_USAGE, szClientCmd);
	SetDlgItemText(hWndDlg, IDC_AUTO_HIGHWAYS2, szClientCmd);
}


void CalculateUpdateHighwayDetails()
{
	for (int iHigh = 1; iHigh <=iNumberBNCSHighways; iHigh++)  {
		// get highway 
		CHighwayRecord* pHigh = GetHighwayRecord(iHigh);
		if (pHigh) {
			bncs_string ssData = bncs_string("%1.%2|%3.%4|%5|").arg(pHigh->getFromTrunkAddr()).arg(pHigh->getFromDestination())
				.arg(pHigh->getToTrunkAddr()).arg(pHigh->getToSource()).arg(pHigh->getHighwayState());
			if (!pHigh->isMonitoringHighway()) {
				CRiedel_GRDs* pFromGrd = GetRiedel_GRD_By_TrunkAddr(pHigh->getFromTrunkAddr());
				CRiedel_GRDs* pToGrd = GetRiedel_GRD_By_TrunkAddr(pHigh->getToTrunkAddr());
				if (pFromGrd&&pToGrd) {
					// get traced source
					iTraceCount = 0;
					ssl_Calc_Trace = bncs_stringlist("", ',');
					int iTracedTrunk = pHigh->getToTrunkAddr();
					int iTracedGrd = pHigh->getToRouter();
					int iTracedSrc = pHigh->getToSource();
					if (TraceThePrimarySource(&iTracedTrunk, &iTracedGrd, &iTracedSrc)) {
						ssData.append(bncs_string("%1").arg(ssl_Calc_Trace.toString(',')));
						// get number users of to src
						int iToUsers = pToGrd->getNumberDestinationsUsingSource(pHigh->getToSource());
						bncs_stringlist ssl_users = bncs_stringlist(pToGrd->getAllDestinationsForSource(pHigh->getToSource()), ',');
						bncs_string ssusers = bncs_string("");
						if (ssl_users.count() == 0) ssusers = bncs_string("0");
						for (int ii = 0; ii < ssl_users.count(); ii++) {
							if (ii>0) ssusers.append(",");
							bncs_string ssus = bncs_string("%1.%2").arg(pHigh->getToTrunkAddr()).arg(ssl_users[ii]);
							ssusers.append(ssus);
						}
						ssData.append(bncs_string("|%1|%2|%3|%4").arg(iToUsers).arg(ssusers).arg(pHigh->getNumberWorkingModeEntries()).arg(pHigh->getAllWorkingModeEntries()));
					}
					else {
						Debug("CalculateUpdateHighwayDetails -TRACE-ERROR-Parking Highway %d", iHigh);
						ParkHighway(iHigh);
					}
				}
				else {
					Debug("CalculateUpdateHighwayDetails-ERROR-Getting GRD from Trunk Address %d or %d ", pHigh->getFromTrunkAddr(), pHigh->getToTrunkAddr());
				}
			}
			else {
				// monitoring highway details -- use given ports rather than  translated
				ssData = bncs_string("%1.%2|%3.%4|%5|").arg(pHigh->getFromTrunkAddr()).arg(pHigh->getMonitorGivenFromDest())
					.arg(pHigh->getToTrunkAddr()).arg(pHigh->getMonitorGivenToSource()).arg(pHigh->getHighwayState());
				// get traced source
				iTraceCount = 0;
				ssl_Calc_Trace = bncs_stringlist("", ',');
				// trace for direct monitoring :   mon rev for the from dest of highway
				CRiedel_Monitors* pMonRec = GetRiedel_Monitors_ByTrunkAddressAndSlot(pHigh->getFromTrunkAddr(), pHigh->getFromDestination());
				int iTracedTrunk = pHigh->getFromTrunkAddr();
				bncs_string ssRev = "0";
				if (pMonRec) ssRev = pMonRec->getMonitoring_Function();
				ssData.append(bncs_string("%1.%2").arg(iTracedTrunk).arg(ssRev));
				// get number users of to src
				int iToUsers = pHigh->getNumberWorkingModeEntries();
				// get from Working mode entries the monitor dest in monPortRecords
				bncs_string ssusers = "";
				for (int iU = 0; iU < iToUsers; iU++) {
					int iMode = 0, iUserRec = 0;
					pHigh->getWorkingMode(iU, &iMode, &iUserRec);
					CMCRMonPortRecord* pMonPort = GetMCRMonPortRecord(iUserRec);  
					if (pMonPort) {
						if (iU>0) ssusers.append(",");
						ssusers.append(bncs_string("%1.%2").arg(pMonPort->m_iMonTrunkAddr).arg(pMonPort->m_iConfigMonPort)); 
					}
				}
				if (ssusers.length() == 0) ssusers = bncs_string("0");
				ssData.append(bncs_string("|%1|%2|%3|%4").arg(iToUsers).arg(ssusers).arg(pHigh->getNumberWorkingModeEntries()).arg(pHigh->getAllWorkingModeEntries()));
			}
			// update slot in ringmaster infodriver - from 2000+
			strcpy(szClientCmd, LPCSTR(ssData));
			eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_TRACE_DATA + iHigh, szClientCmd);
		}
	}
}


void ValidateWorkingModeOUT(CHighwayRecord* pHigh, int iMode, int iIFBMxr)
{
	if (pHigh) {
		int iFinalTrunk = pHigh->getHighwayAllocatedIndex() / IFB_HIGHWAY_BASE;
		int iFinalDest = pHigh->getHighwayAllocatedIndex() % IFB_HIGHWAY_BASE;
		if (iMode == MODE_IFBOUTPUT) {
			// check highway into source is still routed to dest
			int iNumberUsers = 0;
			CRiedel_GRDs* pToGrd = GetRiedel_GRD_Record(pHigh->getToRouter());
			if (pToGrd) { // check numbers users of highway - exception to this is for ifb inputs / mix minus- and marked as inuse or ifb 
				iNumberUsers = pToGrd->getNumberDestinationsUsingSource(pHigh->getToSource());
			}
			CRingMaster_IFB* pRmIFB = GetRingMaster_IFB_Record(iIFBMxr);
			if (pRmIFB) {
				if ((iFinalTrunk > 0) && (iFinalDest > 0)&&(iNumberUsers>0)) {
					bncs_string sstr = bncs_string("%1.%2").arg(iFinalTrunk).arg(iFinalDest);
					if (pRmIFB->getRingMasterRevertive().find(sstr, 0, false) < 0) {
						pHigh->removeWorkingMode(iMode, iIFBMxr);  // as not found in rev at startup - so remove
						Debug("ValidateWorking IFB OUT -WARN- final dest %s not found in %s ifb %d for highway %d %d %d", LPCSTR(sstr), LPCSTR(pRmIFB->getRingMasterRevertive()),
							iIFBMxr, pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
					}
				}
				else {
					pHigh->removeWorkingMode(iMode, iIFBMxr);
					Debug("ValidateWorking IFB OUT -WARN2- invalid allocated index OR NO USERS - ifb %d for highway %d %d %d",  iIFBMxr, pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
				}
			}
			else {
				Debug("ValidateWorking IFB OUT -ERROR- no ringmaster ifb output highway rec %d %d %d", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
				pHigh->removeWorkingMode(iMode, iIFBMxr);
			}
		}
		else if (iMode == MODE_MIXEROUT) {
			CRingMaster_MIXER* pRec = GetRingMaster_MIXER_Record(iIFBMxr);
			if (pRec) {
				if ((iFinalTrunk > 0) && (iFinalDest > 0)) {
					bncs_string sstr = bncs_string("%1.%2").arg(iFinalTrunk).arg(iFinalDest);
					if (pRec->getRingMasterRevertive().find(sstr, 0, false) < 0) {
						pHigh->removeWorkingMode(iMode, iIFBMxr); // as not found in rev at startup - so remove
						Debug("ValidateWorking MIXER OUT -WARN- final dest %s not found in %s mixer %d for highway %d %d %d", LPCSTR(sstr), LPCSTR(pRec->getRingMasterRevertive()),
							iIFBMxr, pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
					}
				}
				else {
					pHigh->removeWorkingMode(iMode, iIFBMxr);
					Debug("ValidateWorking MIXER OUT -WARN2- invalid allocated index - mixer %d for highway %d %d %d", iIFBMxr, pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
				}
			}
			else {
				Debug("ProcessHighwayMgmt -ERROR- no ringmaster mixer output highway rec %d %d %d", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
				pHigh->removeWorkingMode(iMode, iIFBMxr);
			}
		}
	}
}

void ValidateWorkingModeIN(CHighwayRecord* pHigh, int iMode, int iIFBMxr)
{
	if (pHigh) {

		// check final dest - from allocated source is in ringmaster rev - if not implies something a miss
		int iStartTrunk = pHigh->getHighwayAllocatedIndex() / IFB_HIGHWAY_BASE;
		int iStartSrce = pHigh->getHighwayAllocatedIndex() % IFB_HIGHWAY_BASE;
		if (iMode == MODE_IFBINPUT) {
			CRingMaster_IFB* pRmIFB = GetRingMaster_IFB_Record(iIFBMxr);
			if (pRmIFB) {
				if ((iStartTrunk > 0) && (iStartSrce > 0)) {
					bncs_string sstr = bncs_string("%1.%2").arg(iStartTrunk).arg(iStartSrce);
					if (pRmIFB->getRingMasterRevertive().find(sstr, 0, false) < 0) {
						pHigh->removeWorkingMode(iMode, iIFBMxr);   // as not found in rev at startup - so remove
						Debug("ValidateWorking IFB IN -WARN- start srce %s not found in %s ifb %d for highway %d %d %d", LPCSTR(sstr), LPCSTR(pRmIFB->getRingMasterRevertive()),
							iIFBMxr, pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
					}
				}
				else {
					pHigh->removeWorkingMode(iMode, iIFBMxr);
					Debug("ValidateWorking IFB IN  -WARN2- invalid allocated index - ifb %d for highway %d %d %d", iIFBMxr, pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
				}
			}
			else {
				pHigh->removeWorkingMode(iMode, iIFBMxr);
				Debug("ProcessHighwayMgmt -ERROR- no ringmaster ifb input rec highway %d %d %d", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
			}
		}
		else if (iMode == MODE_MIXERIN) {
			CRingMaster_MIXER* pRec = GetRingMaster_MIXER_Record(iIFBMxr);
			if (pRec) {
				if ((iStartTrunk > 0) && (iStartSrce > 0)) {
					bncs_string sstr = bncs_string("%1.%2").arg(iStartTrunk).arg(iStartSrce);
					if (pRec->getRingMasterRevertive().find(sstr,0,false) < 0) {
						pHigh->removeWorkingMode(iMode, iIFBMxr);   // as not found in rev at startup - so remove
						Debug("ValidateWorking MIXER IN -WARN- start srce %s not found in %s mixer %d for highway %d %d %d", LPCSTR(sstr), LPCSTR(pRec->getRingMasterRevertive()),
							iIFBMxr, pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
					}
				}
				else {
					pHigh->removeWorkingMode(iMode, iIFBMxr);
					Debug("ValidateWorking MIXER IN  -WARN2- invalid allocated index - mixer %d for highway %d %d %d", iIFBMxr, pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
				}
			}
			else {
				Debug("ProcessHighwayMgmt -ERROR- no ringmaster mixer input highway rec %d %d %d", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
				pHigh->removeWorkingMode(iMode, iIFBMxr);
			}
		}
	}
}


void ValidateWorkingModeMonitoring(CHighwayRecord* pHigh, int iMode, int iMonRec)
{
	if (pHigh) {
		//
		if ((iMode > 0) && (iMonRec > 0)) {
			CMCRMonPortRecord* pMonRec = GetMCRMonPortRecord(iMonRec);
			if (pMonRec) {
				// check rec 
				bncs_stringlist ssl = bncs_stringlist(pMonRec->m_ssCommandRevertive, ',');
				BOOL bSndChnl = FALSE;
				int iTrunkToMonitor = 0, iPortToMonitor = 0, iMonType = 0;
				if (ssl.count() > 1) {
					GetTrunkAndPortFromString(ssl[0], &iTrunkToMonitor, &iPortToMonitor, &bSndChnl);
					if (ssl[1].upper() == bncs_string("S")) iMonType = MODE_CLONESRCE;
					else if (ssl[1].upper() == bncs_string("D")) iMonType = MODE_CLONEDEST;
					// check monitor rev for highway dest against ringmaster rev
					CRiedel_Monitors* pRev = GetRiedel_Monitors_ByTrunkAddressAndSlot(pHigh->getFromTrunkAddr(), pHigh->getFromDestination());
					if (pRev) {
						if (pRev->getMonitoring_Function().find(bncs_string("%1,%2").arg(iPortToMonitor).arg(ssl[1]).upper(), 0, false) < 0) {
							Debug("ProcessHighwayMgmt -ERROR- mon rev %s not matching %s mode highway rec %d %d %d", LPCSTR(pRev->getMonitoring_Function()), LPCSTR(pMonRec->m_ssCommandRevertive),
								pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
							pHigh->removeWorkingMode(iMode, iMonRec);
						}
					}
				}
				else {
					// deemed 0 - so not in use -- remove working mode from highway as not consistant
					Debug("ProcessHighwayMgmt -ERROR- inconsistant mon mode highway rec %d %d %d", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
					pHigh->removeWorkingMode(iMode, iMonRec);
				}
			}
			else {
				Debug("ProcessHighwayMgmt -ERROR- no known mon mode highway rec %d %d %d", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
				pHigh->removeWorkingMode(iMode, iMonRec);
			}
		}
	}
}


void ProcessHighwayManagement(void)
{
	// go thru all highways and see if users - if none then that highway can be deemed as "free" - thus park the from dest to src 0 - unless locked
	// also check for source of 0/park into highway and marked free and still has users - if so then park users
	for (int iHighway = 1; iHighway <= iNumberBNCSHighways; iHighway++) {
		//
		//  if location / multi hub in operation and link lost ? still check/clear/process those highways going to the other side  ???
		// if (bAutoLocationInUse) {}
		//
		CHighwayRecord* pHigh = GetHighwayRecord(iHighway);
		if (pHigh) {
			if (pHigh->getHighwayPendingCount() > 0) pHigh->decrimentHighwayPendingCount();

			// check number of users of "toSrc" 
			// 1. if highway maked as PENDING and pending counter now 0 - has been pending for 15 secs - implies no revertive from grd  
			if (((pHigh->getHighwayState() == HIGHWAY_PENDING) || (pHigh->getHighwayState() == HIGHWAY_CLEARING)) && (pHigh->getHighwayPendingCount() == 0)) {
				Debug("ProcessHighwayMgmt -1- setting FREE from PENDING/clearing - high %d  from grd %d %d  to grd %d %d", pHigh->getRecordIndex(),
					pHigh->getFromRouter(), pHigh->getFromDestination(), pHigh->getToRouter(), pHigh->getToSource());
				ForceParkHighway(pHigh->getRecordIndex());
			}

			// 2. check if used for ifb and that it is ifb use is still valid 
			if ((pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) && (pHigh->getHighwayPendingCount() == 0)) {
				if ((pHigh->getNumberWorkingModeEntries() > 0) && (pHigh->getHighwayAllocatedIndex() > 0)) {
					// check thru all entries - is allocated source in ringmaster rev - if not implies something a miss
					for (int iE = 0; iE < pHigh->getNumberWorkingModeEntries(); iE++) {
						int iMode = 0, iIFBMxr = 0;
						pHigh->getWorkingMode(iE, &iMode, &iIFBMxr);
						ValidateWorkingModeOUT(pHigh, iMode, iIFBMxr);
					} // for
				}
				else { // why flagged as ifb use then - set for release
					Debug("ProcessHighwayMgmt -2a- ifb output highway %d %d %d - no ifb working ent %d allocated index %d",
						pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination(), pHigh->getNumberWorkingModeEntries(), pHigh->getHighwayAllocatedIndex());
					pHigh->setHighwayState(HIGHWAY_CLEARING, 0, 1);
				}
			}
			else if (pHigh->getHighwayState() == HIGHWAY_IFBMXRIN) {
				// note there will no users of final highway feeding ifb input / mix minus  -- but should have a valid source
				if ((pHigh->getNumberWorkingModeEntries() > 0) && (pHigh->getHighwayAllocatedIndex() > 0)) {
					// check thru all entries - is allocated source in ringmaster rev - if not implies something a miss
					for (int iE = 0; iE < pHigh->getNumberWorkingModeEntries(); iE++) {
						int iMode = 0, iIFBMxr = 0;
						pHigh->getWorkingMode(iE, &iMode, &iIFBMxr);
						ValidateWorkingModeIN(pHigh, iMode, iIFBMxr);
					} // for
				}
				else { // why flagged as ifb use then - set for release
					Debug("ProcessHighwayMgmt -2b- ifb input highway %d %d %d - no ifb / allocated dest ", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
					pHigh->setHighwayState(HIGHWAY_CLEARING, 0, 1);
				}
			}
			else if (pHigh->getHighwayState() == HIGHWAY_CLONEMON) {
				// no GRD source or GRD dest users -- as data in monitor revs
				if ((pHigh->getNumberWorkingModeEntries() > 0) && (pHigh->getHighwayAllocatedIndex() > 0)) {
					// check thru all entries - is allocated source in ringmaster rev - if not implies something a miss
					for (int iE = 0; iE < pHigh->getNumberWorkingModeEntries(); iE++) {
						int iMode = 0, iMonRec = 0;
						pHigh->getWorkingMode(iE, &iMode, &iMonRec);
						ValidateWorkingModeMonitoring(pHigh, iMode, iMonRec);
					} // for
				}
				else { // why flagged as in use then - set for release
					Debug("ProcessHighwayMgmt -2c- Clone highway %d %d %d - no working modes ", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
					pHigh->setHighwayState(HIGHWAY_CLEARING, 0, 1);
				}
			}

			if (!pHigh->isMonitoringHighway()) {
				// the following rules apply for standard highways only - not monitoring ones
				CRiedel_GRDs* pFromGrd = GetRiedel_GRD_Record(pHigh->getFromRouter());
				CRiedel_GRDs* pToGrd = GetRiedel_GRD_Record(pHigh->getToRouter());
				if (pFromGrd&&pToGrd) {
					// 3. check if no users of highway - exception to this is for ifb inputs / mix minus- and marked as inuse or ifb 
					if (pHigh->getHighwayState() != HIGHWAY_IFBMXRIN) {
						if (pToGrd->getNumberDestinationsUsingSource(pHigh->getToSource()) == 0) {
							int iActSrce = pFromGrd->getRoutedSourceForDestination(pHigh->getFromDestination());
							// was if ((iActSrce > PARK_SRCE) && (pHigh->getHighwayState() > HIGHWAY_PENDING)) {
							if ((pHigh->getHighwayState() > HIGHWAY_PENDING)) {
								Debug("ProcessHighwayMgmt -3- releasing highway %d %d %d - no users", pHigh->getRecordIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
								ParkHighway(pHigh->getRecordIndex());
							}
						}
					}

					// 4. there are users of highway - is it marked free -- why are they still using it then ?? - park those dests 
					if (pToGrd->getNumberDestinationsUsingSource(pHigh->getToSource()) > 0) {
						int iActSrce = pFromGrd->getRoutedSourceForDestination(pHigh->getFromDestination());
						if ((iActSrce == PARK_SRCE) && (pHigh->getHighwayState() == HIGHWAY_FREE)) {
							// park users
							CRiedel_GRDs* pRtr = GetRiedel_GRD_Record(pHigh->getToRouter());
							if (pRtr) {
								if (pRtr->getNumberDestinationsUsingSource(pHigh->getToSource()) > 0) {
									bncs_stringlist ssl = bncs_stringlist(pRtr->getAllDestinationsForSource(pHigh->getToSource()), ',');
									for (int iIndx = 0; iIndx < ssl.count(); iIndx++) {
										Debug("ProcessHighwayMgmt -4- Highway %d parking user rtr %d dest %d", pHigh->getRecordIndex(), pHigh->getToRouter(), ssl[iIndx].toInt());
										MakeRiedelGRDRoute(pHigh->getToRouter(), PARK_SRCE, ssl[iIndx].toInt());
									} // for indx
								}
							} // if prtr
						}
					}
				} // if pFromgrd && pToGrd
			} // if != clonemon
			// else --- add in here any other required processing for monitoring highways

			ProcessNextCommand(BNCS_COMMAND_RATE);
		}
	}  // for iHigh

	// usage stats per ring
	CalculateHighwayStatistics();

	// update highway details to infodriver
	CalculateUpdateHighwayDetails();

	// trunk navigator calculations -- only do if revs have been received from trunk slots since last housekeeping ?
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRing);
		if (pRing) {
			CalculateTrunkNavigatorUseForRing(pRing);
		}
	}

}

//////////////////////////////////////////////////////////////////////////////////////////////

int TranslateGivenMonPort(int iTrunkAddr, int iGivenPort)
{
	int iDBRetIndex = 0;
	if ((iGivenPort > 0) && (iGivenPort < MAX_RIEDEL_SLOTS)) {
		// take given port and reverse lookup to get index for use with riedel driver -- from  dev_xx2.db9 file for riedel 
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunkAddr);
		if (pRing) {
			int iMonDev = pRing->iRing_Device_Monitor;
			// 
			bncs_string ssPort = bncs_string(iGivenPort);
			iDBRetIndex = dbmPkgs.getIndex(iMonDev, 9, LPCSTR(ssPort));
			if (iDBRetIndex >0) {
				Debug("TranslateGivenMonPort - Monitor trunk  %d  dev %d  port %d translates to index %d", iTrunkAddr, iMonDev, iGivenPort, iDBRetIndex);
				return iDBRetIndex;
			}
			else {
				iConfigMonitorErrors++;
				Debug("TranslateGivenMonPort - Monitor trunk %d dev %d port %d - MAJOR ERROR - NO translation index found ", iTrunkAddr, iMonDev, iGivenPort);
			}
		}
	}
	return 0;  // error condition
}

////////////////////////////////////////////////////////////////////////////////////////////////
//
//  LOAD / init functions 

bncs_string GetCompositeInstance(bncs_string ssRequiredInstance)
{
	Debug("GetCompositeInstance - Given instance %s", LPCSTR(ssRequiredInstance));
	bncs_stringlist sslist = bncs_stringlist("", ',');
	// load in key groups composite
	bncs_string ssItem = bncs_string("instances.%1").arg(ssRequiredInstance);
	bncs_config cfg(ssItem);
	while (cfg.isChildValid())  {
		// get id and instance from cfg child - add to list as id=instance
		bncs_string ssGrpId = cfg.childAttr("id");
		bncs_string ssValue = cfg.childAttr("value");
		bncs_string ssInstance = cfg.childAttr("instance");
		if (ssInstance.length() > 0) {
			sslist.append(bncs_string("%1=%2").arg(ssGrpId).arg(ssInstance));
			Debug("--- composite child - id=%s yields instance=%s ", LPCSTR(ssGrpId), LPCSTR(ssInstance));
		}
		else if (ssValue.length() > 0) {
			sslist.append(bncs_string("%1=%2").arg(ssGrpId).arg(ssValue));
			Debug("--- composite child - id=%s yields value=%s ", LPCSTR(ssGrpId), LPCSTR(ssValue));
		}
		else Debug(" --- ERROR ---  child %d  has both instance AND value missing / null strings ", LPCSTR(ssGrpId));
		cfg.nextChild();				 // go on to the next sibling (if there is one)
	}
	//
	Debug("GetCompositeInstance - Required instance %s returned %d entries ", LPCSTR(ssRequiredInstance), sslist.count());
	return sslist.toString(',');
}


BOOL LoadRingMasterCompositeInstances()
{

	cl_KeyInstances = new CCompInstances(ssCompositeInstance);
	if (cl_KeyInstances) {
		// load in key groups composite instance
		bncs_stringlist sslcomp = bncs_stringlist(GetCompositeInstance(ssCompositeInstance), ',');
		if (sslcomp.count() > 4) {
			cl_KeyInstances->ssRmstr_Main_Info = sslcomp.getNamedParam("main");
			cl_KeyInstances->ssRmstr_Confs_Info = sslcomp.getNamedParam("conferences");
			cl_KeyInstances->ssRmstr_PortsPTI = sslcomp.getNamedParam("ports_pti");
			for (int iirr = 1; iirr <= iNumberRiedelRings; iirr++) {
				if (iirr < MAX_RIEDEL_RINGS) {
					cl_KeyInstances->ssRmstr_Ports_Info[iirr] = sslcomp.getNamedParam(bncs_string("ports_%1").arg(iirr));
					cl_KeyInstances->ssRmstr_IFB_Info[iirr] = sslcomp.getNamedParam(bncs_string("ifbs_%1").arg(iirr));
				}
			}
		}
		else
			Debug("LoadComposites - FAIL for packager composites - Ringmaster suspended - list had only %d items ", sslcomp.count());

		// load in the mains for each key group just obtained
		if (cl_KeyInstances->ssRmstr_Main_Info.length() > 0) {
			cl_KeyInstances->i_dev_Rmstr_Main = getInstanceDevice(cl_KeyInstances->ssRmstr_Main_Info);
			Debug("LoadComposites  main = %s and %d", LPCSTR(cl_KeyInstances->ssRmstr_Main_Info), cl_KeyInstances->i_dev_Rmstr_Main);
		}
		else
			Debug("LoadComposites - FAIL for key ringmaster main");
		//
		if (cl_KeyInstances->ssRmstr_Confs_Info.length() > 0) {
			cl_KeyInstances->i_dev_Rmstr_Confs = getInstanceDevice(cl_KeyInstances->ssRmstr_Confs_Info);
			Debug("LoadComposites -confs = %s and %d", LPCSTR(cl_KeyInstances->ssRmstr_Confs_Info), cl_KeyInstances->i_dev_Rmstr_Confs);
		}
		else
			Debug("LoadComposites - FAIL for key conferences composite");
		//
		if (cl_KeyInstances->ssRmstr_PortsPTI.length() > 0) {
			cl_KeyInstances->i_dev_Rmstr_PortsPTI = getInstanceDevice(cl_KeyInstances->ssRmstr_PortsPTI);
			Debug("LoadComposites- ports pti = %s and %d", LPCSTR(cl_KeyInstances->ssRmstr_PortsPTI), cl_KeyInstances->i_dev_Rmstr_PortsPTI);
		}
		else
			Debug("LoadComposites - FAIL for key AP composite");
		//
		for (int iirr = 1; iirr <= iNumberRiedelRings; iirr++) {
			if (iirr <MAX_RIEDEL_RINGS) {
				if (cl_KeyInstances->ssRmstr_IFB_Info[iirr].length() > 0) {
					cl_KeyInstances->i_dev_Rmstr_IFBs[iirr] = getInstanceDevice(cl_KeyInstances->ssRmstr_IFB_Info[iirr]);
					Debug("LoadComposites -IFB info %d = %s and %d", iirr, LPCSTR(cl_KeyInstances->ssRmstr_IFB_Info[iirr]), cl_KeyInstances->i_dev_Rmstr_IFBs[iirr]);
				}
				else
					Debug("LoadComposites - FAIL for key IFB info %d composite", iirr);
				//
				if (cl_KeyInstances->ssRmstr_Ports_Info[iirr].length() > 0) {
					cl_KeyInstances->i_dev_Rmstr_Ports[iirr] = getInstanceDevice(cl_KeyInstances->ssRmstr_Ports_Info[iirr]);
					Debug("LoadComposites -PORTS info %d = %s and %d", iirr, LPCSTR(cl_KeyInstances->ssRmstr_Ports_Info[iirr]), cl_KeyInstances->i_dev_Rmstr_Ports[iirr]);
				}
				else
					Debug("LoadComposites - FAIL for key PORTS info %d composite", iirr);
			}
		}
		//
		// test for all devs > 0 - then continue
		if ((cl_KeyInstances->i_dev_Rmstr_Main > 0) && (cl_KeyInstances->i_dev_Rmstr_Confs > 0) &&
			(cl_KeyInstances->i_dev_Rmstr_PortsPTI > 0) && (cl_KeyInstances->i_dev_Rmstr_IFBs[1] > 0) &&
			(cl_KeyInstances->i_dev_Rmstr_Ports[1] > 0) ) {
			Debug("LoadComposites -loaded ");
			return TRUE;
		}
		else
			Debug("LoadComposites -ONE or more composite devices is 0 ");
	}
	return FALSE;
}



int LoadRiedelRings()
{
	// load all defined rings from config xnl file
	bncs_string ssXmlConfigFile = "ring_master_config";
	int iEntry = 0;
	BOOL bCont = TRUE;
	do {
		iEntry++;
		bncs_string ssEntry = bncs_string("Riedel_%1").arg(iEntry);
		bncs_stringlist sslData = bncs_stringlist(getXMLItemValue(ssXmlConfigFile, "RIEDEL_SYSTEMS", ssEntry), ',');
		if (sslData.count()>1) {
			int iTrunkAddress = sslData.getNamedParam("trunk_address").toInt();
			bncs_string ssRingId = sslData.getNamedParam("name");
			bncs_string ssBaseId = sslData.getNamedParam("base_instance");
			Debug("LoadRiedelRing base inst %s name %s trunk address %d", LPCSTR(ssBaseId), LPCSTR(ssRingId), iTrunkAddress);
			if (ssBaseId.length() > 0) {

				CRiedel_Rings* pRing = new CRiedel_Rings(iEntry, iTrunkAddress, ssRingId);
				if (pRing) {
					// load from composite 
					bncs_stringlist sslcomp = bncs_stringlist(GetCompositeInstance(ssBaseId), ',');
					//
					pRing->ssRing_Instance_GRD = sslcomp.getNamedParam("grd");
					pRing->ssRing_Instance_Monitor = sslcomp.getNamedParam("monitors");
					pRing->ssRing_Instance_PTI = sslcomp.getNamedParam("pti");
					pRing->ssRing_Instance_Conference = sslcomp.getNamedParam("conference");
					pRing->ssRing_Instance_IFB = sslcomp.getNamedParam("ifbs");
					pRing->ssRing_Instance_Extras = sslcomp.getNamedParam("extras");
					//
					pRing->iRing_Device_GRD = getInstanceDevice(pRing->ssRing_Instance_GRD);
					pRing->iRing_Device_Monitor = getInstanceDevice(pRing->ssRing_Instance_Monitor);
					pRing->iRing_Device_PTI = getInstanceDevice(pRing->ssRing_Instance_PTI);
					pRing->iRing_Device_Conference = getInstanceDevice(pRing->ssRing_Instance_Conference);
					pRing->iRing_Device_IFB = getInstanceDevice(pRing->ssRing_Instance_IFB);
					pRing->iRing_Device_Extras = getInstanceDevice(pRing->ssRing_Instance_Extras);
					//
					int iDev = 0, iSrc = 0, iDest = 0;
					GetRouterDeviceDBSizes(pRing->ssRing_Instance_GRD, &iDev, &iSrc, &iDest);
					pRing->iRing_Ports_Talk = iSrc;
					pRing->iRing_Ports_Listen = iDest;
					// add to map
					cl_Riedel_Rings->insert(pair<int, CRiedel_Rings*>(iEntry, pRing));
					ssl_AllKnownRings.append(ssRingId);
					//
					char szData[100] = "";
					wsprintf(szData, "%02d - %s", iTrunkAddress, LPCSTR(ssRingId));
					SendDlgItemMessage(hWndDlg, IDC_RINGS, LB_INSERTSTRING, -1, (LPARAM)szData);
					//
					Debug("LoadRiedelRing - %d %d %s - grd %d src %d dest %d", iEntry, iTrunkAddress, LPCSTR(ssRingId), pRing->iRing_Device_GRD, pRing->iRing_Ports_Talk, pRing->iRing_Ports_Listen);
					Debug("LoadRiedelRing - %d %d %s - mon %d conf %d ifb %d", iEntry, iTrunkAddress, LPCSTR(ssRingId), pRing->iRing_Device_Monitor, pRing->iRing_Device_Conference, pRing->iRing_Device_IFB);

				}
			}
			else
				bCont = FALSE;
		}
		else
			bCont = FALSE;
	} while (bCont);
	iEntry--;
	return iEntry;

}

void LoadSetUpRiedelRevRmstrRecords()
{
	// for each ring - go thru and set up riedel rev records and links into RMSTR records 
	for (int iEntry = 1; iEntry <= iNumberRiedelRings; iEntry++) {

		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iEntry);
		if (pRing) {

			int iTrunkAddress = pRing->m_iTrunkAddress;

			// set up GRD class etc
			if (pRing->iRing_Device_GRD > 0) {
				// 
				CRiedel_GRDs* pGRD = new CRiedel_GRDs(pRing->iRing_Device_GRD, pRing->iRing_Ports_Talk, pRing->iRing_Ports_Listen, iTrunkAddress);
				if (pGRD) {
					cl_Riedel_GrdRecords->insert(pair<int, CRiedel_GRDs*>(pRing->iRing_Device_GRD, pGRD));
				}
				else
					Debug("LoadRiedelRing -ERROR- GRD class failure for dev %d ", pRing->iRing_Device_GRD);
			}
			else {
				Debug("LoadRiedelRing -ERROR- GRD zero for ring %d trunk %d ", iEntry, iTrunkAddress);
				SetDlgItemText(hWndDlg, IDC_RING_STATUS, "** ERROR - INVALID GRD riedel dev numbers - fix instances**");
			}

			// set up monitors
			if (pRing->iRing_Device_Monitor > 0) {
				for (int iMon = 1; iMon <= MAX_RIEDEL_SLOTS; iMon++) {
					if (iMon == 1) pRing->iRing_RevsRecord_Monitors = iNumberRiedelMonitorPortRecords; // base revs record used as starting index if required - add index  to this
					iNumberRiedelMonitorPortRecords++;
					CRiedel_Monitors* pRec = new CRiedel_Monitors(iNumberRiedelMonitorPortRecords, pRing->iRing_Device_Monitor, iMon, '*', pRing->m_iTrunkAddress);
					if (pRec&&cl_Riedel_Revs_Monitor) {
						cl_Riedel_Revs_Monitor->insert(pair<int, CRiedel_Monitors*>(iNumberRiedelMonitorPortRecords, pRec));
					}
					else
						Debug("LoadRiedelRing -ERROR- No MON PORT %d class for ring %d trun %d ", iNumberRiedelMonitorPortRecords, iEntry, iTrunkAddress);
				}
			}
			else {
				Debug("LoadRiedelRing -ERROR- MONITOR dev zero for ring %d trunk %d ", iEntry, iTrunkAddress);
				SetDlgItemText(hWndDlg, IDC_RING_STATUS, "** ERROR - INVALID MON riedel dev numbers - fix instances**");
			}

			// pti records
			if (pRing->iRing_Device_PTI > 0) {
				for (int irec = 1; irec <= MAX_RIEDEL_SLOTS; irec++) {
					if (irec == 1) pRing->iRing_RevsRecord_PortsPTI = iNumberRiedelPTIPortRecords; // base revs record used as starting index if required - add index  to this
					iNumberRiedelPTIPortRecords++;
					CRiedel_PTI* pRec = new CRiedel_PTI(iNumberRiedelPTIPortRecords, pRing->iRing_Device_PTI, irec, pRing->m_iTrunkAddress);
					if (pRec&&cl_Riedel_Revs_PTI) {
						cl_Riedel_Revs_PTI->insert(pair<int, CRiedel_PTI*>(iNumberRiedelPTIPortRecords, pRec));
					}
					else
						Debug("LoadRiedelRing -ERROR- No PTI PORT %d class for ring %d trun %d ", iNumberRiedelPTIPortRecords, iEntry, iTrunkAddress);
				}
			}
			else {
				Debug("LoadRiedelRing -ERROR- PTI dev zero for ring %d trunk %d ", iEntry, iTrunkAddress);
				SetDlgItemText(hWndDlg, IDC_RING_STATUS, "** ERROR - INVALID PTI riedel dev numbers - fix instances**");
			}

			// set up trunk nav records and Mixer records
			if (pRing->iRing_Device_Extras > 0) {
				// trunks
				for (int iTruk = 1; iTruk <= MAX_DRIVER_TRUNKS; iTruk++) {
					if (iTruk == 1) pRing->iRing_RevsRecord_Trunks = iNumberRiedelTrunkRecords; // base revs record used as starting index if required - add index  to this
					iNumberRiedelTrunkRecords++;
					CRiedel_Trunks* pRec = new CRiedel_Trunks(iNumberRiedelTrunkRecords, pRing->iRing_Device_Extras, 400 + iTruk, '*', pRing->m_iTrunkAddress);
					if (pRec&&cl_Riedel_Trunks) {
						cl_Riedel_Trunks->insert(pair<int, CRiedel_Trunks*>(iNumberRiedelTrunkRecords, pRec));
					}
					else
						Debug("LoadRiedelRing -ERROR- No TRUNK INFO %d class for ring %d trunk %d ", iNumberRiedelTrunkRecords, iEntry, iTrunkAddress);
				}

				// mixers 
				for (int iMxr = 1; iMxr <= MAX_MIXERS_LOGIC; iMxr++) {
					if (iMxr == 1) pRing->iRing_RevsRecord_Mixers = iNumberRiedelMixerRecords; // base revs record used as starting index if required - add index  to this
					iNumberRiedelMixerRecords++;
					CRiedel_Mixers* pRec = new CRiedel_Mixers(iNumberRiedelMixerRecords, pRing->iRing_Device_Extras, 500 + iMxr, '|', pRing->m_iTrunkAddress);
					if (pRec&&cl_Riedel_Revs_Mixers) {
						cl_Riedel_Revs_Mixers->insert(pair<int, CRiedel_Mixers*>(iNumberRiedelMixerRecords, pRec));
					}
					else
						Debug("LoadRiedelRing -ERROR- No MIXER INFO %d class for ring %d trunk %d ", iNumberRiedelMixerRecords, iEntry, iTrunkAddress);
				}

				//remote LOGIC sources 
				for (int iLogic = 1; iLogic <= MAX_MIXERS_LOGIC; iLogic++) {
					if (iLogic == 1) pRing->iRing_RevsRecord_Logic = iNumberRiedelLogicRecords; // base revs record used as starting index if required - add index  to this
					iNumberRiedelLogicRecords++;
					CRiedel_Logic* pRec = new CRiedel_Logic(iNumberRiedelLogicRecords, pRing->iRing_Device_Extras, 1000 + iLogic, '|', pRing->m_iTrunkAddress);
					if (pRec&&cl_Riedel_Revs_Logic) {
						cl_Riedel_Revs_Logic->insert(pair<int, CRiedel_Logic*>(iNumberRiedelLogicRecords, pRec));
					}
					else
						Debug("LoadRiedelRing -ERROR- No LOGIC INFO %d class for ring %d trunk %d ", iNumberRiedelLogicRecords, iEntry, iTrunkAddress);
				}

			}
			else {
				Debug("LoadRiedelRing -ERROR- EXTRAS dev zero for ring %d trunk %d ", iEntry, iTrunkAddress);
				SetDlgItemText(hWndDlg, IDC_RING_STATUS, "** ERROR - INVALID XTRA riedel dev numbers - fix instances**");
			}

			// set up conferences
			if (pRing->iRing_Device_Conference > 0) {
				for (int iConf = 1; iConf <= MAX_CONFERENCES; iConf++) {
					if (iConf == 1) pRing->iRing_RevsRecord_Conferences = iNumberRiedelConferenceRecords; // base revs record used as starting index if required - add index  to this
					iNumberRiedelConferenceRecords++;
					CRiedel_Conferences* pConf = new CRiedel_Conferences(iNumberRiedelConferenceRecords, pRing->iRing_Device_Conference, iConf, ',', pRing->m_iTrunkAddress);
					if (pConf&&cl_Riedel_Revs_Conf) {
						cl_Riedel_Revs_Conf->insert(pair<int, CRiedel_Conferences*>(iNumberRiedelConferenceRecords, pConf));
						// add entry to ringmaster confs 
						CRingMaster_CONF* pRmstrConf = GetRingMaster_CONF_Record(iConf);
						if (pRmstrConf) pRmstrConf->addRiedelRevIndex(pRing->m_iTrunkAddress, iNumberRiedelConferenceRecords);
					}
					else
						Debug("LoadRiedelRing -ERROR- No CONF %d class for ring %d trun %d ", iNumberRiedelConferenceRecords, iEntry, iTrunkAddress);
				}
			}
			else {
				Debug("LoadRiedelRing -ERROR- CONF dev zero for ring %d trunk %d ", iEntry, iTrunkAddress);
				SetDlgItemText(hWndDlg, IDC_RING_STATUS, "** ERROR - INVALID CONF riedel dev numbers - fix instances**");
			}

			// set up ifbs - all ifbs are created on all rings - in prep for future use
			if (pRing->iRing_Device_IFB > 0) {
				for (int iIfb = 1; iIfb <= MAX_IFBS; iIfb++) {
					if (iIfb == 1) pRing->iRing_RevsRecord_IFBs = iNumberRiedelIFBRecords; // base revs record used as starting index if required - add index  to this
					iNumberRiedelIFBRecords++;
					CRiedel_IFBs* pRec = new CRiedel_IFBs(iNumberRiedelIFBRecords, pRing->iRing_Device_IFB, iIfb, '|', pRing->m_iTrunkAddress);
					if (pRec&&cl_Riedel_Revs_IFB) {
						cl_Riedel_Revs_IFB->insert(pair<int, CRiedel_IFBs*>(iNumberRiedelIFBRecords, pRec));
					}
					else
						Debug("LoadRiedelRing -ERROR- No IFB %d class for ring %d trun %d" , iNumberRiedelIFBRecords, iEntry, iTrunkAddress);
				}
			}
			else {
				Debug("LoadRiedelRing -ERROR- IFB dev zero for ring %d trunk %d ", iEntry, iTrunkAddress);
				SetDlgItemText(hWndDlg, IDC_RING_STATUS, "** ERROR - INVALID IFB riedel dev numbers - fix instances**");
			}

			// general data
			if (cl_DataStruct) {
				iNumberDataListRecords = iEntry;
				CDataList* pdata = new CDataList(iEntry, iTrunkAddress);
				if (pdata) cl_DataStruct->insert(pair<int, CDataList*>(iEntry, pdata));
			}

		}
	} // for iring
}


int LoadAllBNCSHighways()
{
	// load all defined highways from comms_highways xml file
	bncs_string ssXmlConfigFile = "ring_master_config";
	int iLoadedHighways = 0;
	iNumberMCRMonHighways = 0;
	iConfigMonitorErrors = 0;

	//for (int iiarea = AREA1_LOCALE_RING; iiarea <= AREA2_LOCALE_RING; iiarea++) {
		// loads ALL highways for both locations now
		bncs_string ssarea = "highways";
		if ((bAutoLocationInUse)&&(ssl_area_names.count()>1)) {
			ssarea.append("_");
			if (iAutomaticLocation == AREA1_LOCALE_RING) ssarea.append(ssl_area_names[AREA1_LOCALE_RING - 1]);
			else if (iAutomaticLocation == AREA2_LOCALE_RING) ssarea.append(ssl_area_names[AREA2_LOCALE_RING - 1]);
		}
		Debug("LoadAllBNCSHighways - loading from table name :  %s", LPCSTR(ssarea));

		bncs_string ssFileName = bncs_string("%1.%2").arg("comms_highways").arg(ssarea);
		bncs_config cfg(ssFileName);
		Debug("LoadAllBNCSHighways - file.table = %s", LPCSTR(ssFileName));
		while (cfg.isChildValid())  {

			int iFromRing = cfg.childAttr("from_ring").toInt();
			int iFromDest = cfg.childAttr("from_port").toInt();
			int iToRing = cfg.childAttr("to_ring").toInt();
			int iToSrce = cfg.childAttr("to_port").toInt();
			bncs_string ssMCRMonSpecial = cfg.childAttr("monitor_out").upper();
			BOOL bMonitoringHW = FALSE;
			if (ssMCRMonSpecial.find("TRUE") >= 0) bMonitoringHW = TRUE;

			// get from and to rings from trunks 
			CRiedel_Rings* pFromRing = GetRiedel_Rings_By_TrunkAddress(iFromRing);
			CRiedel_Rings* pIntoRing = GetRiedel_Rings_By_TrunkAddress(iToRing);

			if (pFromRing&&pIntoRing) {
				int iFromGrd = pFromRing->iRing_Device_GRD;
				int iToGrd = pIntoRing->iRing_Device_GRD;
				if (bMonitoringHW) {
					iFromGrd = pFromRing->iRing_Device_Monitor;
					iToGrd = pIntoRing->iRing_Device_Monitor;
					Debug("LoadHighway - Monitoring - from %d %d to %d %d", iFromRing, iFromDest, iToRing, iToSrce);
				}
				iLoadedHighways++;
				CHighwayRecord* pHigh = new CHighwayRecord(iLoadedHighways, iFromRing, iFromGrd, iFromDest, iToRing, iToGrd, iToSrce, bMonitoringHW);
				if (pHigh) {
					// update the relevant rings and grd records
					if (bMonitoringHW) {
						pFromRing->addMonitoringFromDestIntoRing(pIntoRing->m_iTrunkAddress, iLoadedHighways);
						pIntoRing->addMonitoringIntoSourceFromRing(pFromRing->m_iTrunkAddress, iLoadedHighways);
						// mons
						CRiedel_Monitors* pFromMon = GetRiedel_Monitors_ByTrunkAddressAndSlot(pFromRing->m_iTrunkAddress, iFromDest);
						if (pFromMon) pFromMon->setAssociatedMonHighway(iLoadedHighways);
						// update fromdest/tosrce with translated indices
						int iTransDest = TranslateGivenMonPort(iFromRing, iFromDest);
						int iTransSrce = TranslateGivenMonPort(iToRing, iToSrce);
						pHigh->resetMonitorFromDestination(iFromDest, iTransDest);
						pHigh->resetMonitorToSource(iToSrce, iTransSrce);
					}
					else {
						pFromRing->addHighwayFromDestIntoRing(pIntoRing->m_iTrunkAddress, iLoadedHighways);
						pIntoRing->addHighwayIntoSourceFromRing(pFromRing->m_iTrunkAddress, iLoadedHighways);
						// grds
						CRiedel_GRDs* pFromGrd = GetRiedel_GRD_Record(iFromGrd);
						if (pFromGrd) pFromGrd->storeAssocDestHighway(iFromDest, iLoadedHighways);
						CRiedel_GRDs* pIntoGrd = GetRiedel_GRD_Record(iToGrd);
						if (pIntoGrd) pIntoGrd->storeAssocSrceHighway(iToSrce, iLoadedHighways);
					}
					// add to map
					cl_HighwayRecords->insert(pair<int, CHighwayRecord*>(iLoadedHighways, pHigh));
					// add to list box
					char szData[100] = "";
					wsprintf(szData, "%03d - %s", iLoadedHighways, LPCSTR(bncs_string("r %1 d %2").arg(pFromRing->m_iTrunkAddress).arg(iFromDest)));
					if (bMonitoringHW) {
						strcat(szData, " *mon");
						iNumberMCRMonHighways++;
					}
					SendDlgItemMessage(hWndDlg, IDC_HIGHWAYS, LB_INSERTSTRING, -1, (LPARAM)szData);
				}
				else
					Debug("LoadAllBNCSHighways - ERROR - in creating class for index %d", iLoadedHighways);
			}
			else
				Debug("LoadAllBNCSHighways - ERROR -missing rings for trunks %d or %d", iFromRing, iToRing);
			//
			cfg.nextChild();				 // go on to the next sibling (if there is one)
			//
		} // while
	//}  // for 

	SetDlgItemInt(hWndDlg, IDC_AUTO_HIGHWAYS2, iNumberMCRMonHighways, TRUE);
	Debug("LoadAllBNCSHighways - %d highways of which  %d are monitoring", iLoadedHighways, iNumberMCRMonHighways);
	if (iConfigMonitorErrors > 0) {
		Debug("LoadAllBNCSHighways - ERRORS for %d monitoring highways found", iConfigMonitorErrors);
		char szData[100] = "";
		wsprintf(szData, "** %d MONITOR PORT ERRORS - check riedel db9s **", iConfigMonitorErrors);
		SetDlgItemText(hWndDlg, IDC_RING_STATUS, szData);
	}

	SetDlgItemInt(hWndDlg, IDC_HWAY_TOTAL, iLoadedHighways, TRUE);
	return iLoadedHighways;

}


void SortRingsGrdPortsInfodrivers( int iNumberPortsTableEntries)
{
	Debug("into SortRingsGRDPorts");
	if (iNumberPortsTableEntries > 0) {
		// using new table entries for ports to auto infodrv slots mapping
		for (int iP = 1; iP <= iNumberPortsTableEntries; iP++) {
			CRingPortsDefn* pDefn = GetRingPortsDefintion(iP);
			if (pDefn) {
				CRiedel_GRDs* pGrd = GetRiedel_GRD_By_TrunkAddr(pDefn->iRingTrunk);
				if (pGrd) {
					pGrd->setDestinationMapping(pDefn->iRingPort, pDefn->iPortsInfodrv, pDefn->iPortsInfodrvSlot);
				}
				else {
					Debug("SortRingsGrdPortsInfodrivers-ERROR-Getting GRD from Trunk Address %d ", pDefn->iRingTrunk);
				}
			}
		} // for ip
	}
	Debug("outof SortRingsGRDPorts");
}


int LoadInRingPortsTable()
{
	Debug("into LoadRingPortsTable");
	bncs_string ssFileName = bncs_string("%1.%2").arg("comms_ringmaster_outputs").arg("outputs");
	int iNdx = 0;
	bncs_config cfg(ssFileName);
	Debug("getXMLTable - file : %s", LPCSTR(ssFileName));
	while (cfg.isChildValid())  {
		iNdx++;
		CRingPortsDefn* pDefn = new CRingPortsDefn();
		if (pDefn) {
			pDefn->iRingTrunk = cfg.childAttr("ring").toInt();
			pDefn->iRingPort = cfg.childAttr("port").toInt();
			pDefn->iPortsInfodrv = getInstanceDevice(cfg.childAttr("instance"));
			pDefn->iPortsInfodrvSlot = cfg.childAttr("slot_index").toInt();
			iNumberRingPortsDefnRecords = iNdx;
			cl_RingPortsMapping->insert(pair<int, CRingPortsDefn*>(iNdx, pDefn));
			//Debug("LoadInRingPortsTable - index %d item found : r %d p %d i %d s %d", iNdx, pDefn->iRingTrunk, pDefn->iRingPort, pDefn->iPortsInfodrv, pDefn->iPortsInfodrvSlot);
		}
		else
			Debug("LoadInRingPortsTable - ERROR - in creating class for index %d", iNdx);
		cfg.nextChild();				 // go on to the next sibling (if there is one)
	}
	Debug("LoadInRingPortsTable - %d items from table", iNdx);
	return iNdx;
}


void LoadAllMonitorPorts()
{
	// load monitor ports from config for MCR and other rings too
	// get table of defined rings for IFB
	int iIndx = 0;
	iNumberMCRMonDestRecords = 0;
	bncs_string ssFileName = bncs_string("%1.%2").arg("ring_master_config").arg("mcr_monitor_ports");
	bncs_config cfg(ssFileName);
	Debug("LoadAllMonitorPorts - file : %s", LPCSTR(ssFileName));
	while (cfg.isChildValid())  {
		int iMonTrunk = cfg.childAttr("ring").toInt();
		int iGivenMonPort = cfg.childAttr("port").toInt();
		bncs_string ssRMInst = cfg.childAttr("instance").toInt();
		int iSlotIndx = cfg.childAttr("slot_index").toInt();
		int iTransMonPort = TranslateGivenMonPort(iMonTrunk, iGivenMonPort);  
		if ((iMonTrunk > 0) && (iGivenMonPort > 0) && (iTransMonPort > 0) && (iSlotIndx > 0) && (cl_MCRMonPorts)) {
			iIndx++;
			CMCRMonPortRecord* pMon = new CMCRMonPortRecord(iIndx, iMonTrunk, iGivenMonPort, iSlotIndx, iTransMonPort);
			if (pMon) {
				iNumberMCRMonDestRecords++;
				cl_MCRMonPorts->insert(pair<int, CMCRMonPortRecord*>(iIndx, pMon));
				// link to riedel monitor revertive record
				CRiedel_Monitors* pRiedelMonRec = GetRiedel_Monitors_ByTrunkAddressAndSlot(iMonTrunk, iTransMonPort);
				if (pRiedelMonRec) pRiedelMonRec->setAssociatedMonitoringRecord(iIndx);
			}
		}
		else
			Debug("LoadMCRMonPorts - some error in config - trunk %d port %d trans %d slot %d", iMonTrunk, iGivenMonPort, iTransMonPort, iSlotIndx);
		cfg.nextChild();				 // go on to the next sibling (if there is one)
	}
	Debug("LoadMCRMonPorts - %d loaded items from table", iNumberMCRMonDestRecords);
}


void 	LoadLogicToIFBAssociation()
{
	// XXXXXX to write - load from ringmaster config file now if required
}


BOOL DetermineMultiHubWorking()
{
	// read in from config xml if Multihub working required or not
	// read in certain optional fields - if all ok then return TRUE; else false
	bncs_string ssXmlConfigFile = "ring_master_config";
	Debug("DetermineMultHub - file : %s  composite %s", LPCSTR(ssXmlConfigFile), LPCSTR(ssCompositeInstance));

	iControlStatus = 0;
	iPrevCtrlStatus = -1;
	ssAREA1_TX_Revertive = "x";
	ssAREA1_RX_Revertive = "x";
	ssAREA2_TX_Revertive = "x";
	ssAREA2_RX_Revertive = "x";
	iCtrlStatusCalcCounter = 33;
	iAREA1_TXRX_TicTocStatus = 0;
	iAREA1_RXONLY_TicTocStatus = 0;
	iAREA2_TXRX_TicTocStatus = 0;
	iAREA2_RXONLY_TicTocStatus = 0;
	iAutomatics_CtrlInfo_Local = 0;
	iAutomatics_CtrlInfo_Remote = 0;
	iOtherSite_Ringmaster_main = 0;
																																																						  
	ssl_area_names = bncs_stringlist(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "multi_site_working"), ',');
	if (ssl_area_names.count() > 1) {

		Debug("DetermineMultHub - list of areas: %s", LPCSTR(ssl_area_names.toString(',')));
		bncs_string ss1 = ssl_area_names[0].upper();
		bncs_string ss2 = ssl_area_names[1].upper();
		bncs_string ssComp = ssCompositeInstance.upper();
		bncs_string ssdbg = "";

		if (ssComp.find(ss1) >= 0) {
			iAutomaticLocation = AREA1_LOCALE_RING;
			SetDlgItemText(hWndDlg, IDC_AUTO_AREA, LPCSTR(ss1));
			ssdbg = ss1;
		}
		else if (ssComp.find(ss2) >= 0) {
			iAutomaticLocation = AREA2_LOCALE_RING;
			SetDlgItemText(hWndDlg, IDC_AUTO_AREA, LPCSTR(ss2));
			ssdbg = ss2;
		}
		// get control infodrivers
		iAutomatics_CtrlInfo_Local = getInstanceDevice(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "automatics_control_local"));
		iAutomatics_CtrlInfo_Remote = getInstanceDevice(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "automatics_control_remote"));
		SetDlgItemInt(hWndDlg, IDC_AUTO_CTRLINFO, iAutomatics_CtrlInfo_Local, TRUE);
		SetDlgItemInt(hWndDlg, IDC_AUTO_CTRLINFO2, iAutomatics_CtrlInfo_Remote, TRUE);
		// get other side ringmaster
		iOtherSite_Ringmaster_main = getInstanceDevice(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "other_site_rmstr"));

		if ((iAutomaticLocation > 0) && (iAutomatics_CtrlInfo_Local > 0) && (iAutomatics_CtrlInfo_Remote > 0) && (iOtherSite_Ringmaster_main > 0)) {
			// valid parameters
			Debug("LoadAllInit - DetermineMultiHub location %s %d - ctrl Infos %d %d ", LPCSTR(ssdbg), iAutomaticLocation, iAutomatics_CtrlInfo_Local, iAutomatics_CtrlInfo_Remote);
			Debug("LoadAllInit - DetermineMultiHub other instance is %s - device %d", LPCSTR(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "other_site_rmstr")), iOtherSite_Ringmaster_main);
			Debug("LoadAllInit - DetermineMultiHubWorking ENABLED ");
			return TRUE;
		}
		else
			Debug("LoadAllInit - invalid multihub data - autoloc %d   ctrl-infos %d %d  othersite %d", 
			iAutomaticLocation, iAutomatics_CtrlInfo_Local, iAutomatics_CtrlInfo_Remote, iOtherSite_Ringmaster_main);
	}
	else
		Debug("LoadAllInit - missing multihub areas string - so single ended rmstr");

	SetDlgItemText(hWndDlg, IDC_AUTO_AREA, "single");
	Debug("LoadAllInit - DetermineMultiHubWorking NOT enabled ");
	return FALSE;
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: LoadAllInitialisationData()
//
//  PURPOSE: Function called on startup to load data from ini files / object settings etc relevant for driver
//                 creates new memory instances/
//
BOOL LoadAllInitialisationData( void ) 
{

	BOOL bContinue=TRUE;
	int iMaxSrcs = 0, iMaxDests = 0;
	bncs_string strStart= "RingMaster_Automatic", strValue = "", strIdName = "";
	bncs_string ssXmlConfigFile = "ring_master_config";
	ssl_area_names = bncs_stringlist("", ',');

	Debug( "LoadAllInitData  - loading all data" );

	bAutoLocationInUse = DetermineMultiHubWorking();

	// create maps 
	cl_Riedel_Rings = new map<int, CRiedel_Rings*>;

	cl_Riedel_GrdRecords = new map<int, CRiedel_GRDs*>;  
	cl_Riedel_Revs_Monitor = new map<int, CRiedel_Monitors*>;
	cl_Riedel_Revs_Conf = new map<int, CRiedel_Conferences*>;
	cl_Riedel_Revs_IFB = new map<int, CRiedel_IFBs*>;
	cl_Riedel_Revs_PTI = new map<int, CRiedel_PTI*>;
	cl_Riedel_Trunks = new map<int, CRiedel_Trunks*>;
	cl_Riedel_Revs_Logic = new map<int, CRiedel_Logic*>;
	cl_Riedel_Revs_Mixers = new map<int, CRiedel_Mixers*>;
	
	cl_HighwayRecords = new map<int, CHighwayRecord*>;

	cl_RingMaster_Conferences = new map<int, CRingMaster_CONF*>;
	cl_RingMaster_IFBs = new map<int, CRingMaster_IFB*>;
	cl_RingMaster_Mixers = new map<int, CRingMaster_MIXER*>;
	cl_RingMaster_Logic = new map<int, CRingMaster_LOGIC*>;
	cl_RingPortsMapping = new map<int, CRingPortsDefn*>;
	cl_MCRMonPorts = new map<int, CMCRMonPortRecord*>;
	cl_DataStruct = new map<int, CDataList*>;

	ssl_hops = bncs_stringlist("", ',');
	ssl_AllKnownRings = bncs_stringlist("", ',');
	ssl_CalculatedRouteHops = bncs_stringlist("", '|');
	ssl_PotentialRouteCommands = bncs_stringlist("", '|');

	iNumberRiedelIFBRecords = 0; 
	iNumberRiedelLogicRecords = 0;
	iNumberRiedelMixerRecords = 0;
	iNumberRiedelTrunkRecords = 0;
	iNumberRiedelPTIPortRecords = 0;
	iNumberRiedelConferenceRecords = 0;
	iNumberRiedelMonitorPortRecords = 0;

	iNumberRiedelRings = 0; 
	iNumberBNCSHighways = 0;     // ALL highways under BNCS control -- Standard and Monitoring
	iNumberMCRMonHighways = 0;
	iNumberMCRMonDestRecords = 0;

	iUserDefinedMngmntProc = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "Highway_Management").toInt();
	Debug("LoadAllInit - Highway Management Interval read in as %d seconds", iUserDefinedMngmntProc);
	if ((iUserDefinedMngmntProc<=0) || (iUserDefinedMngmntProc>60)) {
		iUserDefinedMngmntProc = 7;
		Debug("LoadAllInit - Highway Management defintion at %d seconds - deemed invalid, setting to 7", iUserDefinedMngmntProc);
	}

	// FIRST --- load rings / riedel systems from ring_master_config.xml file
	if (cl_Riedel_Rings&&cl_Riedel_GrdRecords) {
		iNumberRiedelRings = LoadRiedelRings();
		SetDlgItemInt(hWndDlg, IDC_AUTO_RINGS, iNumberRiedelRings, TRUE);
		Debug("LoadAllInit - number rings %d - names : %s", iNumberRiedelRings, LPCSTR(ssl_AllKnownRings.toString(',')));
		// grd routers - load from ring_master_config.xml
	}
	else {
		Debug("LoadAllInit -ERROR- no Riedel Ring or GRD class created - exiting init");
		return FALSE;
	}

	if (iNumberRiedelRings > 0) 
	{
		// load composites 
		if (!LoadRingMasterCompositeInstances()) {
			Debug("LoadAllInit -ERROR- Fail to Load required COMPOSITE RINGMASTER instances - exiting init");
			return FALSE;
		}

		// set up some base records
		//
		int iNumberTableEntries = LoadInRingPortsTable();
		//
		if (cl_RingMaster_Conferences) {
			int iDefTrunk = 0;
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(1);
			if (pRing) iDefTrunk = pRing->m_iTrunkAddress;
			for (int iConf = 1; iConf <= MAX_CONFERENCES; iConf++) {
				CRingMaster_CONF* pRmstrConf = new CRingMaster_CONF(iConf);
				if (pRmstrConf) {
					pRmstrConf->setAssociatedTrunkAddress(iDefTrunk);
					cl_RingMaster_Conferences->insert(pair<int, CRingMaster_CONF*>(iConf, pRmstrConf));
				}
				else
					Debug("LoadAllInit -ERROR- ringmstr conf rec for conf %d not created", iConf);
			}
		}
		else {
			Debug("LoadAllInit -ERROR- no Ring Master Conferences class created - exiting init");
			return FALSE;
		}

		if (cl_RingMaster_IFBs) {
			int iNumberIFBs = 0;
			for (int iiRing = 1; iiRing <= iNumberRiedelRings; iiRing++) {
				CRiedel_Rings* pRing = GetRiedel_Rings_Record(iiRing);
				if (pRing) {
					int iDefTrunk = pRing->m_iTrunkAddress;
					for (int iifb = 1; iifb <= MAX_IFBS; iifb++) {
						iNumberIFBs++;
						CRingMaster_IFB* pRmstrIfb = new CRingMaster_IFB(iNumberIFBs, iDefTrunk);
						if (pRmstrIfb) {
							cl_RingMaster_IFBs->insert(pair<int, CRingMaster_IFB*>(iNumberIFBs, pRmstrIfb));
						}
						else
							Debug("LoadAllInit -ERROR- ringmstr ifb rec %d / %d not created", iifb, iNumberIFBs);
					}
				}
			}
		}
		else {
			Debug("LoadAllInit -ERROR- no Ring Master IFBS class created - exiting init");
			return FALSE;
		}

		if (cl_RingMaster_Mixers) {
			int iNumberMxrs = 0;
			for (int iiRing = 1; iiRing <= iNumberRiedelRings; iiRing++) {
				CRiedel_Rings* pRing = GetRiedel_Rings_Record(iiRing);
				if (pRing) {
					int iDefTrunk = pRing->m_iTrunkAddress;
					for (int iMxr = 1; iMxr <= MAX_MIXERS_LOGIC; iMxr++) {
						iNumberMxrs++;
						CRingMaster_MIXER* pRmstrMxr = new CRingMaster_MIXER(iNumberMxrs, iDefTrunk);
						if (pRmstrMxr) {
							cl_RingMaster_Mixers->insert(pair<int, CRingMaster_MIXER*>(iNumberMxrs, pRmstrMxr));
						}
						else
							Debug("LoadAllInit -ERROR- ringmstr Mixer rec %d / %d not created", iMxr, iNumberMxrs);
					}
				}
			}
		}
		else {
			Debug("LoadAllInit -ERROR- no Ring Master MIXER class created - exiting init");
			return FALSE;
		}

		if (cl_RingMaster_Logic) {
			int iNumberLogs = 0;
			for (int iiRing = 1; iiRing <= iNumberRiedelRings; iiRing++) {
				CRiedel_Rings* pRing = GetRiedel_Rings_Record(iiRing);
				if (pRing) {
					int iDefTrunk = pRing->m_iTrunkAddress;
					for (int iLogic = 1; iLogic <= MAX_MIXERS_LOGIC; iLogic++) {
						iNumberLogs++;
						CRingMaster_LOGIC* pRmstrLogic = new CRingMaster_LOGIC(iNumberLogs, iDefTrunk);
						if (pRmstrLogic) {
							cl_RingMaster_Logic->insert(pair<int, CRingMaster_LOGIC*>(iNumberLogs, pRmstrLogic));
						}
						else
							Debug("LoadAllInit -ERROR- ringmstr Logic rec %d / %d not created", iLogic, iNumberLogs);
					}
				}
			}
		}
		else {
			Debug("LoadAllInit -ERROR- no Ring Master LOGIC class created - exiting init");
			return FALSE;
		}

		if (ConnectAllInfodrivers()) {

			LoadSetUpRiedelRevRmstrRecords();

			SortRingsGrdPortsInfodrivers(iNumberTableEntries);

			// load highways for each grd defined 
			if (cl_HighwayRecords) {
				iNumberBNCSHighways = LoadAllBNCSHighways();
				if (iNumberBNCSHighways == 0) {
					Debug("LOADINIT -ERROR- NO HIGHWAYS LOADED check config");
					char szmsg[128] = "** ERROR- NO HIGHWAYS LOADED check config **";
					SetDlgItemText(hWndDlg, IDC_RING_STATUS, szmsg);
				}
				SetDlgItemInt(hWndDlg, IDC_AUTO_HIGHWAYS, iNumberBNCSHighways, TRUE);
			}
			else {
				Debug("LoadAllInit -ERROR- no highways class created - exiting init");
				return FALSE;
			}

			// load config dats for ifbs and confs  
			Debug("LoadAllInit LoadLogicToIFBAssociation");
			LoadLogicToIFBAssociation();
			Debug("LoadAllInit LoadAllMonitorPorts"); // xxx load all or just area associated ???
			LoadAllMonitorPorts();

			// trunk nav poll
			bPollTrunkNavigatorData = FALSE;
			iConfigCounterTrunkNavData = 30; //   can be enabled on GUI if really required
			iNextPollTrunkNavigatorData = 0;

			Debug("LoadAllInitData - finished init data ");
			return TRUE;

		}
		else {
			Debug("LoadAllInit -ERROR- Connect to Infodrivers FAILED - exiting init");
			return FALSE;
		}

	}
	else {
		Debug("LoadAllInit -ERROR- no Riedel Rings defined -fix configuration - no point in continuing");
		return FALSE;
	}

}



/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: AutomaticRegistrations()
//
//  PURPOSE:register with CSI for gory details
//
//  COMMENTS: 
//			
BOOL AutomaticRegistrations(void)
{
	// go thru all rings and register with devices - need to know sizes for registration
	Debug( "into AutoRegistrations " );

	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRing);
		if (pRing) {
			// get GRD and register
			if (pRing->iRing_Device_GRD > 0)  {
				ecCSIClient->regtallyrange(pRing->iRing_Device_GRD, 1, pRing->iRing_Ports_Listen, INSERT);
				Debug("AutoReg - ring %d grd %d dests 1 to %d", iRing, pRing->iRing_Device_GRD, pRing->iRing_Ports_Listen);
			}
			else
				Debug("AutoReg - no GRD defined for ring %d", iRing);
			// monitor infodriver
			if (pRing->iRing_Device_Monitor>0)  ecCSIClient->regtallyrange(pRing->iRing_Device_Monitor, 1, MAX_RIEDEL_SLOTS, INSERT);
			// conf
			if (pRing->iRing_Device_Conference > 0)  {
				// conf infodriver has 3 slot ranges required
				ecCSIClient->regtallyrange(pRing->iRing_Device_Conference, 1, MAX_CONFERENCES, INSERT);   // fixed across riedel
				ecCSIClient->regtallyrange(pRing->iRing_Device_Conference, 1001, 1000 + MAX_CONFERENCES, INSERT);  // labels
				ecCSIClient->regtallyrange(pRing->iRing_Device_Conference, 2001, 2000 + MAX_CONFERENCES, INSERT);  // panel members
			}
			// ifb
			if (pRing->iRing_Device_IFB>0)  ecCSIClient->regtallyrange(pRing->iRing_Device_IFB, 1, MAX_IFBS, INSERT);
	
			// 99 trunk usage and 500 Mixers  AND also 500 Logic sources
			if (pRing->iRing_Device_Extras>0)  ecCSIClient->regtallyrange(pRing->iRing_Device_Extras, 401, 1500, INSERT);  // slots 401-1500 in extras infodriver

			if (pRing->iRing_Device_PTI>0)  ecCSIClient->regtallyrange(pRing->iRing_Device_PTI, 1, 4000, INSERT);  // input and output ports

		}
	} // for iRing

	// automatics control infodriver   if auto location enabled
	if ((bAutoLocationInUse) && (iAutomatics_CtrlInfo_Local > 0) && (iAutomatics_CtrlInfo_Remote>0)) {

		int iLocalBase = 0, iOtherBase = 0;
		if (iAutomaticLocation == AREA1_LOCALE_RING) {
			iLocalBase = CTRL_INFO_RMSTR_AREA1_BASE;
			iOtherBase = CTRL_INFO_RMSTR_AREA2_BASE;
		}
		else if (iAutomaticLocation == AREA2_LOCALE_RING) {
			iLocalBase = CTRL_INFO_RMSTR_AREA2_BASE;
			iOtherBase = CTRL_INFO_RMSTR_AREA1_BASE;
		}
		// all packagers register for revs from ALL tic-toc slots to determine running status of packagers
		ecCSIClient->regtallyrange(iAutomatics_CtrlInfo_Local, iLocalBase + CTRL_INFO_TXRX_TIKTOK, iLocalBase + CTRL_INFO_RXONLY_TIKTOK, INSERT);   // LDC
		ecCSIClient->regtallyrange(iAutomatics_CtrlInfo_Remote, iOtherBase + CTRL_INFO_TXRX_TIKTOK, iOtherBase + CTRL_INFO_HIGHWAYS, INSERT);   // NHN

		// register for slots 1101 to 1100 + iNumberHighways  ( approx 1499 - 96 highways from one ring to the 3 other rings i.e. 32 hways from each ring to one other )
		// get / check usage of highways
		ecCSIClient->regtallyrange(iAutomatics_CtrlInfo_Local, CTRL_INFO_RMSTR_HIGHWAYS, CTRL_INFO_RMSTR_HIGHWAYS + iNumberBNCSHighways + 1, INSERT);
		ecCSIClient->regtallyrange(iAutomatics_CtrlInfo_Remote, CTRL_INFO_RMSTR_HIGHWAYS, CTRL_INFO_RMSTR_HIGHWAYS + iNumberBNCSHighways + 1, INSERT);

	}

	//if (iOtherSite_Ringmaster_main > 0) {
	//	// register with the other packager main infodriver for RMs about shared resources confs and ifbs 
	//	if (ecCSIClient) ecCSIClient->regtallyrange(iOtherSite_Ringmaster_main, 4094, 4094, INSERT);
	//}

	Debug( "out of AutoRegistrations " );
	return TRUE;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//
// FUNCTION PollAutomaticDrivers();
//
// to query all drivers to get this automatic up to speed - at startup
void PollAutomaticDrivers( int iCounter )
{

	// during startup this function is called on one second intervals to spread poll out and process revs in ordered manner 
	char szCommand[MAX_AUTOBFFR_STRING]="";
	int iWhichRing = iCounter / 6;
	int iWhichElement = iCounter % 6;
	Debug("into PollAutomaticDrivers  counter=%d Ring %d element %d", iCounter, iWhichRing, iWhichElement);
	CRiedel_Rings* pRing = GetRiedel_Rings_Record(iWhichRing);
	if (pRing) {
		if (iWhichElement == 0) 	{  // grd
			if (pRing->iRing_Device_GRD > 0) {
				wsprintf(szCommand, "RP %d 1 %d", pRing->iRing_Device_GRD, pRing->iRing_Ports_Listen);
				ecCSIClient->txrtrcmd(szCommand);
				Debug("AutoPolling - ring %d grd %d polling 1 to %d", iWhichRing, pRing->iRing_Device_GRD, pRing->iRing_Ports_Listen);
			}
		}
		else if (iWhichElement == 1) { // monitors
			if (pRing->iRing_Device_Monitor > 0) {
				wsprintf(szCommand, "IP %d 1 %d", pRing->iRing_Device_Monitor, MAX_RIEDEL_SLOTS); // 
				ecCSIClient->txinfocmd(szCommand);
			}
		}
		else if (iWhichElement == 2) { // pti
			if (pRing->iRing_Device_PTI > 0) {
				wsprintf(szCommand, "IP %d 1 %d", pRing->iRing_Device_PTI, MAX_RIEDEL_SLOTS); // 
				ecCSIClient->txinfocmd(szCommand);
			}
		}
		else if (iWhichElement == 3) { // conf
			if (pRing->iRing_Device_Conference > 0) {
				wsprintf(szCommand, "IP %d 1 %d", pRing->iRing_Device_Conference, MAX_CONFERENCES); // 
				ecCSIClient->txinfocmd(szCommand);
				wsprintf(szCommand, "IP %d 1001 %d", pRing->iRing_Device_Conference, 1000+MAX_CONFERENCES); // 
				ecCSIClient->txinfocmd(szCommand);
				wsprintf(szCommand, "IP %d 2001 %d", pRing->iRing_Device_Conference, 2000+MAX_CONFERENCES); // 
				ecCSIClient->txinfocmd(szCommand);
			}
		}
		else if (iWhichElement == 4) { // ifbs
			if (pRing->iRing_Device_IFB > 0) {
				wsprintf(szCommand, "IP %d 1 %d", pRing->iRing_Device_IFB, MAX_IFBS); // 
				ecCSIClient->txinfocmd(szCommand);
			}
		}
		else if (iWhichElement == 5) { // trunk and MIXERS info
			if (pRing->iRing_Device_Extras > 0) {  // to finish when known
				wsprintf(szCommand, "IP %d 401 499", pRing->iRing_Device_Extras); // poll for trunk info 
				ecCSIClient->txinfocmd(szCommand);
				wsprintf(szCommand, "IP %d 501 1000", pRing->iRing_Device_Extras); // poll for MIXERS info 
				ecCSIClient->txinfocmd(szCommand);
				wsprintf(szCommand, "IP %d 1001 1500", pRing->iRing_Device_Extras); // poll for LOGIC info 
				ecCSIClient->txinfocmd(szCommand);
			}
		}
	} // if pRing

}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    RECURRSIVE / TRACE FUNCTIONS
//
//

BOOL TraceThePrimarySource(int *iTracedTrunk, int *iTracedRtr, int *iTracedSrc)
{
	// trace from given source,  back via highways to primary source, also fills in highways trace as it goes along
	int iThisTrunk = *iTracedTrunk;
	int iThisRtr = *iTracedRtr;
	int iThisSource = *iTracedSrc;

	// check counter
	iTraceCount++;
	if (iTraceCount > MAX_TRACE) {
		Debug("TraceThePrimarySource - MAX TRACE for traced trunk %d grd %d src %d", iThisTrunk, iThisRtr, iThisSource);
		return FALSE;
	}

	CHighwayRecord* pHigh = GetHighwayRecordByToRtrSrce(iThisRtr, iThisSource);
	if (pHigh) {
		// get grd for from dest
		CRiedel_GRDs* pFromGrd = GetRiedel_GRD_Record(pHigh->getFromRouter());
		if (pFromGrd) {
			// get source routed to the from dest
			iThisTrunk = pFromGrd->getRingTrunk();
			iThisRtr = pFromGrd->getDevice();
			iThisSource = pFromGrd->getRoutedSourceForDestination(pHigh->getFromDestination());
			// prepend into calc string 
			bncs_string ssTrace = bncs_string("%1.%2").arg(iThisTrunk).arg(iThisSource);
			// check to see if this trace already within calc trace string -- high probability of  loop - so stop and exit recurrsive loop ???
			if (ssl_Calc_Trace.find(ssTrace)<0) ssl_Calc_Trace.prepend(ssTrace);
			if (pFromGrd->getAssocSrceHighway(iThisSource) > 0) {
				// prepend data 
				if (!TraceThePrimarySource(&iThisTrunk, &iThisRtr, &iThisSource)) return FALSE;
			}
		}
	}

	// srce is not a highway - so recurrsion terminates
	// check to see if this trace already within calc trace string -- high probability of  loop - so stop and exit recurrsive loop ???
	bncs_string ssEndTrace = bncs_string("%1.%2").arg(iThisTrunk).arg(iThisSource);
	if (ssl_Calc_Trace.find(ssEndTrace)<0) ssl_Calc_Trace.prepend(bncs_string("%1.%2").arg(iThisTrunk).arg(iThisSource));
	*iTracedTrunk = iThisTrunk;
	*iTracedRtr = iThisRtr;
	*iTracedSrc = iThisSource; // primay src
	if (iTraceCount < MAX_TRACE)
		return TRUE;
	else
		return FALSE;

}


BOOL TracePrimarySourceForward(int iStartGrd, int iStartSrc, int iTracedRing, int iTracedGrd, int iTracedSrc)
{
	// check counter
	iTraceCount++; 
	if (iTraceCount > MAX_TRACE) {
		Debug("TracePrimarySrcFwd - MAX TRACE hit for traced trunk %d grd %d src %d", iTracedRing, iTracedGrd, iTracedSrc);
		return FALSE;
	}
	// get dests routed to src and set 
	CRiedel_GRDs* pGrd = GetRiedel_GRD_Record(iStartGrd);
	if (pGrd) {
		bncs_stringlist ssl = pGrd->getAllDestinationsForSource(iStartSrc);
		//if (!bAutomaticStarting) Debug("TraceForward- traced : g %d s%d  ds:(%d)(%s)", iStartGrd, iStartSrc, ssl.count(), LPCSTR(ssl.toString(',')));
		for (int iNdx = 0; iNdx < ssl.count(); iNdx++) {
			int iDest = ssl[iNdx].toInt();
			int iCurrGrd = pGrd->getTracedGrdForDestination(iDest);
			int iCurrSrc = pGrd->getTracedSourceForDestination(iDest);
			if ((iCurrGrd!=iTracedGrd) || (iCurrSrc!=iTracedSrc)) {
				pGrd->storeTracedRingSource(iDest, iTracedRing, iTracedGrd, iTracedSrc);
				UpdateGRDRevertive(pGrd, iDest);
			}
			// further processing if dest is a highway
			if (pGrd->getAssocDestHighway(iDest) > 0) {
				//if (!bAutomaticStarting) Debug("TraceForward- traced : g %d d %d  highway:%d", iStartGrd, iDest, pGrd->getAssocDestHighway(iDest) );
				CHighwayRecord* pHigh = GetHighwayRecord(pGrd->getAssocDestHighway(iDest));
				if (pHigh) {
					// update highway with traced src etc
					pHigh->setTracedRouterAndPort(iTracedRing, iTracedGrd, iTracedSrc);
					if (!TracePrimarySourceForward(pHigh->getToRouter(), pHigh->getToSource(), iTracedRing, iTracedGrd, iTracedSrc)) {
						// abort dest loop too
						iNdx = ssl.count() + 1;
						return FALSE;
					}
				}
			}
		} // for
	}
	if (iTraceCount < MAX_TRACE)
		return TRUE;
	else
		return FALSE;
}

BOOL TraceIFBMixerFinalDestination(int iRecType, int iIFBMixer, int *iUserTrunk, int *iUserGrd, int *iUserDest)
{
	int iTrunk = *iUserTrunk;
	int iGrd = *iUserGrd;
	int iDest = *iUserDest;
	// check counter
	iTraceCount++;
	if (iTraceCount > MAX_TRACE) {
		Debug("TraceIFBMixerFinalDest - MAX TRACE hit for traced ring %d grd %d dest %d", iTrunk, iGrd, iDest);
		return FALSE;
	}
	// get dests routed to src and set 
	CHighwayRecord* pHigh = GetHighwayRecordByFromRtrDest(iGrd, iDest);
	if (pHigh) {
		CRiedel_GRDs* pGrd = GetRiedel_GRD_Record(pHigh->getToRouter());
		if (pGrd) {
			bncs_stringlist ssl = pGrd->getAllDestinationsForSource(pHigh->getToSource());
			if (ssl.count() == 0) {
				Debug("TraceIFBMixerFinalDest- ZERO Sources for : g %d s%d - TRACE FAILS", pGrd->getDevice(), pHigh->getToSource());
				return FALSE;
			}
			if (ssl.count()>1) Debug("TraceIFBMixerFinalDest- Multiple Sources for : g %d s%d  ds:(%s)", pGrd->getDevice(), pHigh->getToSource(), LPCSTR(ssl.toString(',')));
			for (int iNdx = 0; iNdx < ssl.count(); iNdx++) {
				iDest = ssl[iNdx].toInt();
				iGrd = pGrd->getDevice();
				iTrunk = pGrd->getRingTrunk();
				// further processing only if dest is a highway
				if (pGrd->getAssocDestHighway(ssl[iNdx].toInt()) > 0) {
					TraceIFBMixerFinalDestination(iRecType, iIFBMixer, &iTrunk, &iGrd, &iDest);
				}
				else {
					// check dest is in ifb rev from this ring -- else report inconsistency found
					// get riedel rev record for this trunk and ifb / mixer 
					bncs_stringlist sslOuts = bncs_stringlist("", ',');
					if (iRecType == MODE_IFBOUTPUT) {
						CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iIFBMixer);
						if (pIFB) sslOuts = bncs_stringlist(pIFB->getRingMasterRevertive(), '|');
						Debug("TraceIFBMixerFinalDest looking for %d %d in list outs : %s", iTrunk, iDest, LPCSTR(pIFB->getRingMasterRevertive()));
					}
					else if (iRecType == MODE_MIXEROUT) {
						CRingMaster_MIXER* pMXR = GetRingMaster_MIXER_Record(iIFBMixer);
						if (pMXR) sslOuts = bncs_stringlist(pMXR->getRingMasterRevertive(), '|');
						Debug("TraceIFBMixerFinalDest looking for %d %d in list outs : %s", iTrunk, iDest, LPCSTR(pMXR->getRingMasterRevertive()));
					}
					if (sslOuts.find(bncs_string("%1.%2").arg(iTrunk).arg(iDest)) < 0) {
						bncs_string ssmsg = bncs_string("TraceIFBMixerFinalDest -note- Final Trunk %1 Dest %2 not in IFB/MXR %3 revertive - possible Inconsistency").arg(iTrunk).arg(iDest).arg(iIFBMixer);
						Debug(LPCSTR(ssmsg));
						//PostAutomaticErrorMessage(iTrunk, ssmsg);
					}
				}
				*iUserGrd = iGrd;
				*iUserDest = iDest;
				*iUserTrunk = iTrunk;
				return TRUE;
			} // for
		}
	}
	//  else fallout on most recent settings
	*iUserGrd = iGrd;
	*iUserDest = iDest;
	*iUserTrunk = iTrunk;
	return TRUE;
}


void PreProcessIFBHighways(int iIFB)
{
	// get ringmaster ifb record -- for defined ifb records for each known trunk, see if =any outputs are highway dests
	CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iIFB);
	if (pIFB) {
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pIFB->getAssociatedTrunkAddress());
		CRiedel_IFBs* pIFBRev = GetRiedel_IFBs_Record(iIFB);
		if (pRing&&pIFBRev) {
			// inputs + mix minus
			bncs_stringlist sslinns = bncs_stringlist(bncs_string("%1,%2").arg(pIFBRev->getIFB_Inputs()).arg(pIFBRev->getIFB_MixMinus()), ',');
			for (int iInndx = 0; iInndx < sslinns.count(); iInndx++) {
				int iInPort = sslinns[iInndx].toInt();
				if (iInPort > 0) {
					CHighwayRecord* pHigh = GetHighwayRecordByToRtrSrce(pRing->iRing_Device_GRD, iInPort);
					if (pHigh) {
						// this highway srce is listed as an input in IFB -- therefore set highway state as HIGHWAY_IFBMXRIN and determine real traced srce
						Debug("PreProcessIFB -input- IFB %d has input %d %d %d - as highway", iIFB, pHigh->getToTrunkAddr(), pHigh->getToRouter(), pHigh->getToSource());
						int iTracedTrunk = 0, iTracedGrd = 0, iTracedSrce = 0;
						pHigh->getTracedRouterAndPort(&iTracedTrunk, &iTracedGrd, &iTracedSrce);
						int iCalcSrce = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedSrce;
						if ((iTracedTrunk > 0) && (iTracedSrce > 0)) {
							Debug("PreProcessIFB -input- traced User srce as %d from %d %d ", iCalcSrce, iTracedTrunk, iTracedSrce);
						}
						else {
							bncs_string ssmsg = bncs_string("PreProcessIFB - ifb %1 - could NOT trace IFB Srce - setting user as %2 from %3 %4 ").arg(iIFB).arg(iCalcSrce).arg(iTracedTrunk).arg(iTracedSrce);
							Debug("PreProcessIFB -WARNING- could NOT trace IFB %d Srce - setting user as %d from %d %d ", iIFB, iCalcSrce, iTracedTrunk, iTracedSrce);
							//PostAutomaticErrorMessage(iTracedTrunk, ssmsg);
						}
						pHigh->setHighwayState(HIGHWAY_IFBMXRIN, iCalcSrce, 0);
						pHigh->addWorkingMode(MODE_IFBINPUT, iIFB);
					}
				}
			} // for inndx
			//
			// outputs
			bncs_stringlist sslOuts = bncs_stringlist(pIFBRev->getIFB_Outputs(), ',');
			for (int iIndx = 0; iIndx < sslOuts.count(); iIndx++) {
				CHighwayRecord* pFromHigh = GetHighwayRecordByFromRtrDest(pRing->iRing_Device_GRD, sslOuts[iIndx].toInt());
				if (pFromHigh) {
					// this highway dest is listed as an output in IFB -- therefore set highway state as HIGHWAY_IFBMXROUT and determine final dest
					Debug("PreProcessIFB - IFB %d has output %d %d %d - as highway", iIFB, pFromHigh->getFromTrunkAddr(), pFromHigh->getFromRouter(), pFromHigh->getFromDestination());
					int iUserTrunk = pFromHigh->getFromTrunkAddr();
					int iUserDest = pFromHigh->getFromDestination();
					int iUserGrd = pFromHigh->getFromRouter();
					int iCalcUser = (iUserTrunk*IFB_HIGHWAY_BASE) + iUserDest;
					iTraceCount = 0;
					if (TraceIFBMixerFinalDestination(MODE_IFBOUTPUT, iIFB, &iUserTrunk, &iUserGrd, &iUserDest) ) {
						iCalcUser = (iUserTrunk*IFB_HIGHWAY_BASE) + iUserDest;
						Debug("PreProcessIFB - traced Final User dest - setting user as %d from %d %d ", iCalcUser, iUserTrunk, iUserDest);
					}
					else {
						bncs_string ssmsg = bncs_string("PreProcessIFB - ifb %1 - could NOT trace Final Dest - setting user as %2 from %3 %4 ").arg(iIFB).arg(iCalcUser).arg(iUserTrunk).arg(iUserDest);
						Debug("PreProcessIFB -WARNING- ifb %d could NOT trace Final User dest - setting user as %d from %d %d ", iIFB, iCalcUser, iUserTrunk, iUserDest);
						//PostAutomaticErrorMessage(iUserTrunk, ssmsg);
					}
					pFromHigh->setHighwayState(HIGHWAY_IFBMXROUT, iCalcUser, 0);
					pFromHigh->addWorkingMode(MODE_IFBOUTPUT, iIFB);
				}
			} // for indx
		}
	}
}

//pre process MIXER Highway usage
void PreProcessMIXERHighways(int iMXR)
{
	// get ringmaster ifb record -- for defined ifb records for each known trunk, see if =any outputs are highway dests
	CRingMaster_MIXER* pMixer = GetRingMaster_MIXER_Record(iMXR);
	if (pMixer) {
		int iAssocTrunk = pMixer->getAssociatedTrunkAddress();
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iAssocTrunk);
		CRiedel_Mixers* pMxrRev = GetRiedel_Mixers_Record(iMXR);
		if (pRing&&pMxrRev) {
			// inputs
			bncs_stringlist sslinns = bncs_stringlist(bncs_string("%1,%2").arg(pMxrRev->getMixer_Inputs()), ',');
			for (int iInndx = 0; iInndx < sslinns.count(); iInndx++) {
				BOOL bSndChnl = FALSE;
				int iInPort = 0;
				bncs_string ssDb = GetPortAnddBFromString(sslinns[iInndx], &iInPort, &bSndChnl);
				if (iInPort>0) {
					CHighwayRecord* pHigh = GetHighwayRecordByToRtrSrce(pRing->iRing_Device_GRD, iInPort);
					if (pHigh) {
						// this highway srce is listed as an input in IFB -- therefore set highway state as HIGHWAY_IFBMXRIN and determine real traced srce
						Debug("PreProcessMIXER -input- mixer %d has input %d %d %d - as highway", iMXR, pHigh->getToTrunkAddr(), pHigh->getToRouter(), pHigh->getToSource());
						int iTracedTrunk = 0, iTracedGrd = 0, iTracedSrce = 0;
						pHigh->getTracedRouterAndPort(&iTracedTrunk, &iTracedGrd, &iTracedSrce);
						int iCalcSrce = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedSrce;
						if ((iTracedTrunk>0) && (iTracedSrce>0)) {
							Debug("PreProcessMIXER -input- traced User srce as %d from %d %d ", iCalcSrce, iTracedTrunk, iTracedSrce);
						}
						else {
							bncs_string ssmsg = bncs_string("PreProcessMIXER - mixer %1 - could NOT trace IFB Srce - setting user as %2 from %3 %4 ").arg(iMXR).arg(iCalcSrce).arg(iTracedTrunk).arg(iTracedSrce);
							Debug("PreProcessMixer -WARNING- could NOT trace Mixer %d Srce - setting user as %d from %d %d ", iMXR, iCalcSrce, iTracedTrunk, iTracedSrce);
							//PostAutomaticErrorMessage(iTracedTrunk, ssmsg);
						}
						pHigh->setHighwayState(HIGHWAY_IFBMXRIN, iCalcSrce, 0);
						pHigh->addWorkingMode(MODE_MIXERIN, iMXR);
					}
				}
			} // for inndx
			//
			// outputs
			bncs_stringlist sslOuts = bncs_stringlist(pMxrRev->getMixer_Outputs(), ',');
			for (int iIndx = 0; iIndx < sslOuts.count(); iIndx++) {
				CHighwayRecord* pFromHigh = GetHighwayRecordByFromRtrDest(pRing->iRing_Device_GRD, sslOuts[iIndx].toInt());
				if (pFromHigh) {
					// this highway dest is listed as an output in mixer -- therefore set highway state as HIGHWAY_IFBMXROUT and determine final dest
					Debug("PreProcessMixer - Mixer %d has output %d %d %d - as highway", iMXR, pFromHigh->getFromTrunkAddr(), pFromHigh->getFromRouter(), pFromHigh->getFromDestination());
					int iUserTrunk = pFromHigh->getFromTrunkAddr();
					int iUserDest = pFromHigh->getFromDestination();
					int iUserGrd = pFromHigh->getFromRouter();
					int iCalcUser = (iUserTrunk*IFB_HIGHWAY_BASE) + iUserDest;
					int iTraceCount = 0;
					if (TraceIFBMixerFinalDestination(MODE_MIXEROUT, iMXR, &iUserTrunk, &iUserGrd, &iUserDest) ) {
						iCalcUser = (iUserTrunk*IFB_HIGHWAY_BASE) + iUserDest;
						Debug("PreProcessMixer - traced Final User dest - setting user as %d from %d %d ", iCalcUser, iUserTrunk, iUserDest);
					}
					else {
						bncs_string ssmsg = bncs_string("PreProcessMIXER  %1 - could NOT trace Final Dest - setting user as %2 from %3 %4 ").arg(iMXR).arg(iCalcUser).arg(iUserTrunk).arg(iUserDest);
						Debug("PreProcessMIXER -WARNING- mixer %d could NOT trace Final User dest - setting user as %d from %d %d ", iMXR, iCalcUser, iUserTrunk, iUserDest);
						//PostAutomaticErrorMessage(iUserTrunk, ssmsg);
					}
					pFromHigh->setHighwayState(HIGHWAY_IFBMXROUT, iCalcUser, 0);
					pFromHigh->addWorkingMode(MODE_MIXEROUT, iMXR);
				}
			} // for indx
		}
	}
}

void PreProcessAllMonitorHighways()
{
	// go thru all highways marked as monitor ones and trace source and if used in monitoring records and set up links etc
	// on a ring by ring basis
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		// get mon highways landing on ring - trace source, find which mon records use it on landing ring 
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRing);
		if (pRing) {
			bncs_stringlist ssl = bncs_stringlist(pRing->getAllIncomingMonitoring(), ',');
			for (int iMH = 0; iMH < ssl.count(); iMH++) {
				
			}
		}
	}
}



////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INITIAL PROCESSING TO GET AUTO INTO CORRECT STATE
//
void ProcessFirstPass_Logic( void )
{
	char szData[MAX_AUTOBFFR_STRING]="";
	char szBlank[MAX_AUTOBFFR_STRING]="";
	char szReply[64]="";				
	int iExpSrc=0;

	Debug("Firstpass-1");
	// clear all error message slots
	for (int i = 0; i <= MAX_RIEDEL_RINGS; i++) {
		if (eiRingMasterInfoId) {
			eiRingMasterInfoId->updateslot(RINGTRUNKS_ERROR_MSGS + i, " ");
			eiRingMasterInfoId->updateslot(HIGHWAYS_INFO_ALARM_TRIGGER, "0");
		}
	}

	Debug("Firstpass-2");
	// go thru all grds and report on number of revs received -- as a safety check
	for (int iRings = 1; iRings <= iNumberRiedelRings; iRings++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRings);
		if (pRing) {
			CRiedel_GRDs* pRtr = GetRiedel_GRD_Record(pRing->iRing_Device_GRD);
			if (pRtr) {
				if (pRtr->getNumberRevertivesReceived() < (pRtr->getMaximumDestinations()-8)) {
					bncs_string ssmsg = bncs_string("RingMaster - ProcessFirstPass - Trunk %1 GRD %2 - TOO FEW RTR REVS %3 out of %4").arg(pRing->m_iTrunkAddress).arg(pRing->iRing_Device_GRD).arg(pRtr->getNumberRevertivesReceived()).arg(pRtr->getMaximumDestinations());
					SetAlarmErrorMessage(pRing->m_iTrunkAddress, ssmsg);
					Debug(LPCSTR(ssmsg));
				}
				else 
					Debug("ProcessFirstPass - Trunk %d GRD %d -- enough revertives received %d : expected %d ", pRing->m_iTrunkAddress, 
					pRing->iRing_Device_GRD, pRtr->getNumberRevertivesReceived(), pRtr->getMaximumDestinations());
			}
			else
				Debug("ProcFirstPass -ERROR- no GRD class for %d ", pRing->iRing_Device_GRD);
		}
	}
	
	Debug("Firstpass-3");
	// highways processing
	for (int iHighway = 1; iHighway <= iNumberBNCSHighways; iHighway++) {
		//
		CHighwayRecord* pHigh = GetHighwayRecord(iHighway);
		if (pHigh) {
			if (!pHigh->isMonitoringHighway()) {
				// standard highways
				Debug("Firstpass-3 std highway %d ", iHighway);
				CRiedel_GRDs* pFromGrd = GetRiedel_GRD_Record(pHigh->getFromRouter());
				if (pFromGrd) {
					int iNewSrce = pFromGrd->getRoutedSourceForDestination(pHigh->getFromDestination());
					// if new src is a highway in -- then determine and store traced source
					iTraceCount = 0;  // reset
					ssl_Calc_Trace = bncs_stringlist("", '|');
					int iTracedTrunk = pHigh->getFromTrunkAddr();
					int iTracedGrd = pHigh->getFromRouter();
					int iTracedSrc = iNewSrce;
					// get traced ring, grd and source
					if (pFromGrd->getAssocSrceHighway(iNewSrce) > 0) {
						if (TraceThePrimarySource(&iTracedTrunk, &iTracedGrd, &iTracedSrc) ) {
							//
							pFromGrd->storeTracedRingSource(pHigh->getFromDestination(), iTracedTrunk, iTracedGrd, iTracedSrc);
							if (pFromGrd->getAssocDestHighway(pHigh->getFromDestination()) > 0) {
								pHigh->setTracedRouterAndPort(iTracedTrunk, iTracedGrd, iTracedSrc);
								if ((iNewSrce > 0) && (iNewSrce != PARK_SRCE))
									pHigh->setHighwayState(HIGHWAY_INUSE, iNewSrce, 0);
								else
									pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);  // for now - further checks below for IFB highway use
							}
							// update this grd rev
							UpdateGRDRevertive(pFromGrd, pHigh->getFromDestination());
							// use traced ring/src and go forward to update all dests that have this src routed to it
							// trace forward - updating all relevant grd + dest records with revised traced ring + src - update infodriver too
							iTraceCount = 0;  // reset
							Debug("ProcessFirstPass_Logic- traced : r %d g %d s%d ", iTracedTrunk, iTracedGrd, iTracedSrc);
							if (!TracePrimarySourceForward(iTracedGrd, iTracedSrc, iTracedTrunk, iTracedGrd, iTracedSrc)) {
								Debug("ProcessFirstPass-TRACE-FORWARD-ERROR-possible LOOP detected- parking highway %d", iHighway);
								ParkHighway(iHighway);
							}
						}
						else {
							Debug("ProcessFirstPass-HWAY-TRACE-ERROR-probable LOOP detected- parking highway %d", iHighway);
							ParkHighway(iHighway);
						}
					}
				}
			}
			else {
				// monitoring highway -- resolve from grd rev or from monitor rev 
				Debug("Firstpass-3 mon highway %d ", iHighway);
				CRiedel_Monitors* pFromMon = GetRiedel_Monitors_ByDeviceSlot(pHigh->getFromRouter(), pHigh->getFromDestination());
				if (pFromMon) {
					int iNewPort = 0, iNewType = 0;
					bncs_stringlist ssl = bncs_stringlist(pFromMon->getMonitoring_Function(), ',');
					if (ssl.count() == 2) {
						// verify mcr highways etc to generate Ringamster Rev equivalent -- trace of port being monitored
						iNewPort = ssl[0].toInt();
						if (ssl[1].upper() == bncs_string("S")) iNewType = MODE_CLONESRCE;
						else if (ssl[1].upper() == bncs_string("D")) iNewType = MODE_CLONEDEST;
					}
					// only direct mon highways at present
					int iTracedTrunk = pHigh->getFromTrunkAddr();
					int iTracedGrd = pHigh->getFromRouter();
					int iTracedSrc = iNewPort;
					pHigh->setTracedRouterAndPort(iTracedTrunk, iTracedGrd, iTracedSrc);
					if ((iNewPort > 0) && (iNewType > 0)) {
						pHigh->setHighwayState(HIGHWAY_CLONEMON, iNewPort, 0);
						// find any users and create working mode entries -- go thru all mon port records for this trunk
						for (int iRec = 1; iRec <= iNumberRiedelMonitorPortRecords; iRec++) {
							CMCRMonPortRecord* pMonRec = GetMCRMonPortRecord(iRec);
							if (pMonRec) {
								// need to get rev linked to the dest in -- is it the source of the highway in question 
								CRiedel_Monitors* pRevMon = GetRiedel_Monitors_ByTrunkAddressAndSlot(pMonRec->m_iMonTrunkAddr, pMonRec->m_iMonPortTranslated);
								if (pRevMon) {
									bncs_stringlist ssl_Rev = bncs_stringlist(pRevMon->getMonitoring_Function(), ',');
									int iPort = ssl_Rev[0].toInt();
									if (iPort == pHigh->getToSource()) {
										pHigh->addWorkingMode(iNewType, pMonRec->m_iRecordIndex);
										pMonRec->m_iUsingHighwayRecord = pHigh->getRecordIndex();
										pMonRec->m_iWorkingModeForHighway = iNewType;
									}
								}
							}
						} // for
					}
					else {
						pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
					}
				}

			}
		}
	}

	Debug("Firstpass-4");
	// go thru all grds and update port revertives based on highways etc
	for (int iRings = 1; iRings <= iNumberRiedelRings; iRings++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRings);
		if (pRing) {
			// for each ring process grd revs
			CRiedel_GRDs* pRtr = GetRiedel_GRD_Record(pRing->iRing_Device_GRD);
			if (pRtr) {
				for (int iDest = 1; iDest <= pRtr->getMaximumDestinations(); iDest++) {
					// trace and update all revs for ports
					UpdateGRDRevertive(pRtr, iDest);
				}
			}
			else
				Debug("ProcFirstPass - no GRD class for %d ", pRing->iRing_Device_GRD);
			// calc and post trunk nav totals per ring
			Debug("FirstPass -4a- CalcTrunkNav");
			CalculateTrunkNavigatorUseForRing(pRing);
		}
	}

	// go thru all conference revs and generate r.mstr conf revs
	Debug("FirstPass -5- Process Conf revs");
	for (int iConf = 1; iConf <= MAX_CONFERENCES; iConf++) {
		CRingMaster_CONF* pRMstrConf = GetRingMaster_CONF_Record(iConf);
		if (pRMstrConf) {
			// mbrs
			ProcessRiedelConferenceRev( 0, iConf);
			// labels -- need only do for "associated" ring -- but should all be the same across all rings
			int iAssocTrunk = pRMstrConf->getAssociatedTrunkAddress();
			ProcessRiedelConferenceRev(pRMstrConf->getRiedelRevIndex(iAssocTrunk),  iConf + 1000);
			// panels
			ProcessRiedelConferenceRev(0, iConf + 2000);
		}
	}

	Debug("FirstPass -6- Process IFB revs");
	//go thru all ifb revs and generate r.mstr ifb revs
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		for (int iIFB = 1; iIFB <= MAX_IFBS; iIFB++) {
			int iCalcIFB = ((iRing - 1)*MAX_IFBS) + iIFB;
			PreProcessIFBHighways(iCalcIFB);
			// calculate Ringmaster mixer rev from the asociated trunk mixer rev 
			CRingMaster_IFB* pRMstrIfb = GetRingMaster_IFB_Record(iCalcIFB);
			if (pRMstrIfb) {
				CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRing);
				if (pRing) ProcessRiedelIFBRev(iCalcIFB, iRing, iIFB);
			}
		}
	}

	// preprocess mixers and highways and update mixer slots in main ringmaster infodriver  501-1000
	//go thru all mixer revs and generate r.mstr ifb revs
	Debug("FirstPass -7- Process Mixer revs");
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		for (int iMXR = 1; iMXR <= MAX_MIXERS_LOGIC; iMXR++) {
			int iCalcMxr = ((iRing - 1)*MAX_MIXERS_LOGIC) + iMXR;
			PreProcessMIXERHighways(iCalcMxr);
			// calculate Ringmaster mixer rev from the asociated trunk mixer rev 
			CRingMaster_MIXER* pRMstrMxr = GetRingMaster_MIXER_Record(iCalcMxr);
			if (pRMstrMxr) {
				int iAssocTrunk = pRMstrMxr->getAssociatedTrunkAddress();
				CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iAssocTrunk);
				if (pRing) ProcessRiedelMIXERRev(iCalcMxr, pRing->m_iRecordIndex, iMXR + RIEDELDRIVER_MIXERS_INFO_START);
			}
		}
	}

	// process first pass on remote logics 
	Debug("FirstPass -8- Process Remote Logic revs");
	//go thru all ifb revs and generate r.mstr ifb revs
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		for (int iLogic = 1; iLogic <= MAX_MIXERS_LOGIC; iLogic++) {
			int iCalcLogic = ((iRing - 1)*MAX_MIXERS_LOGIC) + iLogic;
			CRingMaster_LOGIC* pRMstrLogic = GetRingMaster_LOGIC_Record(iCalcLogic);
			if (pRMstrLogic) {
				int iAssocTrunk = pRMstrLogic->getAssociatedTrunkAddress();
				CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iAssocTrunk);
				if (pRing) ProcessRiedelLOGICRev(iCalcLogic, pRing->m_iRecordIndex, iLogic + RIEDELDRIVER_LOGIC_INFO_START);
			}
		}
	}

	Debug("FirstPass -9- Process Monitor revs");
	// go thru and process conf / ifb / ListenToPort  panel membership from Monitor revs
	for (int iRings = 1; iRings <= iNumberRiedelRings; iRings++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRings);
		if (pRing) {
			// for each ring process all monitor panel revs - setting up conf / ifb panel memberlists
			for (int iPanel = 1; iPanel < MAX_RIEDEL_SLOTS; iPanel++) {
				ProcessRiedelMonitorRev(pRing->iRing_RevsRecord_Monitors + iPanel, pRing->m_iRecordIndex, iPanel);
			}
		}
	}

	Debug("FirstPass -10- Process port pti revs");
	//go thru all port revs and generate r.mstr port pti revs
	for (int iiPort = 1; iiPort <= MAX_RIEDEL_PTI; iiPort++) {
		UpdateRingMasterPortPti(iiPort);
	}

	Debug("FirstPass - finished");
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
void ClearAllConferenceKeyMembers( int iRiedelDevice, CRiedel_Conferences* pConf )
{
	/*
	// go thru and clear all monitors keys for tb or Ifb conferences
	if (pConf) {
		char szCommand[MAX_AUTOBFFR_STRING]="";
		bncs_stringlist ssl = bncs_stringlist( pConf->GetAllMonitorMembers(), ' ' );
		for ( int iM=0;iM<ssl.count();iM++) {
			// was wsprintf( szCommand, "IW %d '*0*  ....  *' %d", iRiedelDevice,  ssl[iM].toInt() );
			wsprintf( szCommand, "IW %d '%s' %d", iRiedelDevice,  LPCSTR(ssKeyClearCmd), ssl[iM].toInt() );
			AddCommandToQue( szCommand, INFODRVCOMMAND, iRiedelDevice, 0,0 );
		}
	}
	*/
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
void ClearTBConferencePortMembers( int iRiedelDevice, int iConfIndex )
{
	// go thru and clear members for tb conferences  -- IFB conf handled elsewhere
	if ((iConfIndex>0)&&(iConfIndex<=MAX_CONFERENCES)) {
		char szCommand[MAX_AUTOBFFR_STRING]="";
		// clear the label
		wsprintf( szCommand, "IW %d ' ' %d", iRiedelDevice,  iConfIndex+1000 );  
		AddCommandToQue( szCommand, INFODRVCOMMAND, iRiedelDevice, 0,0 );
		// clear all port members by sending null string
		wsprintf( szCommand, "IW %d ' ' %d", iRiedelDevice,  iConfIndex );
		AddCommandToQue( szCommand, INFODRVCOMMAND, iRiedelDevice, 0,0 );
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
void MakeRiedelGRDRoute( int iRiedelGrd, int iSource, int iDest ) 
{
	char szCommand[MAX_AUTOBFFR_STRING]="";
	wsprintf(szCommand, "RC %d %d %d ", iRiedelGrd, iSource, iDest);
	AddCommandToQue(szCommand, ROUTERCOMMAND, iRiedelGrd, 0, 0);
}


////////////////////////////////////////////////////////////////////////////////////////////////

void ParkHighway(int iGivenHighway)
{
	char szCommand[MAX_AUTOBFFR_STRING] = "";
	CHighwayRecord* pHigh = GetHighwayRecord(iGivenHighway);
	if (pHigh) {
		//
		if (pHigh->isMonitoringHighway() ) {
			// send 0 to monitoring from dest on relevant riedel
			CRiedel_Rings* pFromRing = GetRiedel_Rings_By_TrunkAddress(pHigh->getFromTrunkAddr());
			if (pFromRing) {
				wsprintf(szCommand, "IW %d '0' %d", pFromRing->iRing_Device_Monitor, pHigh->getFromDestination());
				AddCommandToQue(szCommand, INFODRVCOMMAND, pFromRing->iRing_Device_Monitor, pHigh->getFromDestination(), 0);
				// go thru any working modes - mon users and clear their clone mon too
				for (int iE = 0; iE < pHigh->getNumberWorkingModeEntries(); iE++) {
					int iMode = 0, iMonRec = 0;
					pHigh->getWorkingMode(iE, &iMode, &iMonRec);
					CMCRMonPortRecord* pMonRec = GetMCRMonPortRecord(iMonRec);
					if (pMonRec) {
						CRiedel_Rings* pMonRing = GetRiedel_Rings_By_TrunkAddress(pMonRec->m_iMonTrunkAddr);
						if (pMonRing) {
							wsprintf(szCommand, "IW %d '0' %d", pMonRing->iRing_Device_Monitor, pMonRec->m_iMonPortTranslated);
							AddCommandToQue(szCommand, INFODRVCOMMAND, pMonRing->iRing_Device_Monitor, pMonRec->m_iMonPortTranslated, 0);
						}
					}
				}
			}
		}
		else {
			// standard highway
			// route park to all users of said highway
			CRiedel_GRDs* pToRtr = GetRiedel_GRD_Record(pHigh->getToRouter());
			if (pToRtr) {
				if (pToRtr->getNumberDestinationsUsingSource(pHigh->getToSource()) > 0) {
					bncs_stringlist ssl = bncs_stringlist(pToRtr->getAllDestinationsForSource(pHigh->getToSource()), ',');
					for (int iIndx = 0; iIndx < ssl.count(); iIndx++) {
						Debug("ParkHighway of rtr %d dest %d", pHigh->getToRouter(), ssl[iIndx].toInt());
						MakeRiedelGRDRoute(pHigh->getToRouter(), PARK_SRCE, ssl[iIndx].toInt());
					}
				}
			}
			// check if highway is not already parked to prevent sending multiple rcs to park it - or if highway for ifb use
			CRiedel_GRDs* pFromRtr = GetRiedel_GRD_Record(pHigh->getFromRouter());
			if (pFromRtr) {
				// route 0 / park to highway
				MakeRiedelGRDRoute(pHigh->getFromRouter(), PARK_SRCE, pHigh->getFromDestination());
				// linked to ifb or mixer ? then remove -- check ifb rev for correct port - if present then remove 
				if ((pHigh->getHighwayState() >= HIGHWAY_IFBMXRIN) && (pHigh->getNumberWorkingModeEntries()>0))  {
					// go thru all entries
					for (int iE = 0; iE < pHigh->getNumberWorkingModeEntries(); iE++) {
						int iMode = 0, iIFBMxr = 0;
						pHigh->getWorkingMode(iE, &iMode, &iIFBMxr);  
						if (iMode == MODE_IFBINPUT) {
							int iRealIFB = ((iIFBMxr-1)%MAX_IFBS)+1;
							wsprintf(szCommand, "IW %d '&INPUTREMOVE=%d' %d", pFromRtr->getDevice() + 7, pHigh->getToSource(), iRealIFB);
							AddCommandToQue(szCommand, INFODRVCOMMAND, pFromRtr->getDevice() + 7, iRealIFB, 0);
						}
						else if (iMode == MODE_MIXERIN) {
							int iRealMixer = ((iIFBMxr - 1) % MAX_MIXERS_LOGIC) + 1;
							wsprintf(szCommand, "IW %d '&REMOVEIN=%d' %d", pFromRtr->getDevice() + 8, pHigh->getToSource(), iRealMixer + RIEDELDRIVER_MIXERS_INFO_START);
							AddCommandToQue(szCommand, INFODRVCOMMAND, pFromRtr->getDevice() + 8, iRealMixer, 0);
						}
						else if (iMode == MODE_IFBOUTPUT) {
							int iRealIFB = ((iIFBMxr - 1) % MAX_IFBS) + 1;
							wsprintf(szCommand, "IW %d '&OUTPUTREMOVE=%d' %d", pFromRtr->getDevice() + 7, pHigh->getFromDestination(), iRealIFB);
							AddCommandToQue(szCommand, INFODRVCOMMAND, pFromRtr->getDevice() + 7, iRealIFB, 0);
						}
						else if (iMode == MODE_MIXEROUT) {
							int iRealMixer = ((iIFBMxr - 1) % MAX_MIXERS_LOGIC) + 1;
							wsprintf(szCommand, "IW %d '&REMOVEOUT=%d' %d", pFromRtr->getDevice() + 8, pHigh->getFromDestination(), iRealMixer + RIEDELDRIVER_MIXERS_INFO_START);
							AddCommandToQue(szCommand, INFODRVCOMMAND, pFromRtr->getDevice() + 8, iRealMixer, 0);
						}
					}
				}
			}
		}
		if (pHigh->getHighwayState()>HIGHWAY_CLEARING) pHigh->setHighwayState(HIGHWAY_CLEARING, 0, 1);
		ProcessNextCommand(BNCS_COMMAND_RATE);
	}
}


void ForceParkHighway(int iGivenHighway)
{
	CHighwayRecord* pHigh = GetHighwayRecord(iGivenHighway);
	if (pHigh) {
		ParkHighway(iGivenHighway);
		// force parking - frees highway
		pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
		ProcessNextCommand(BNCS_COMMAND_RATE);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
//
void SortCalculatedRouteHops()
{
	// sorts generated routes list into number of hops order
	if (ssl_CalculatedRouteHops.count() > 0) {
			bncs_stringlist ssl_sorted = bncs_stringlist("", '|');
		for (int iLength = 2; iLength <= iNumberRiedelRings; iLength++) {
			for (int iE = 0; iE < ssl_CalculatedRouteHops.count(); iE++) {
				bncs_stringlist ssltr = bncs_stringlist( ssl_CalculatedRouteHops[iE], ',');
				if (ssltr.count() == iLength) { // ie entry we are looking for
					ssl_sorted.append(ssltr.toString(','));
				}
			} // for iE
		} // for iLength
		// copy sorted list back
		ssl_CalculatedRouteHops.clear();
		for (int ii = 0; ii < ssl_sorted.count(); ii++) ssl_CalculatedRouteHops.append(ssl_sorted[ii]);
	}
}

void FindHopsFromOutgoing(int iHopRing, int iStartRing, int iEndRing)
{
	CRiedel_Rings* pHopRing = GetRiedel_Rings_By_TrunkAddress(iHopRing);
	if (pHopRing) {
		ssl_hops.append(iHopRing);
		bncs_stringlist ssl_keep1 = ssl_hops;
		for (int ii = 1; ii < MAX_RIEDEL_RINGS; ii++) {
			if (pHopRing->getNumberOutgoingHighways(ii)>0) {
				ssl_hops.clear();
				ssl_hops = ssl_keep1;
				if (ii == iEndRing) {
					bncs_stringlist ssl_keep2 = ssl_hops;
					// destination found - so can add to calc list
					ssl_hops.append(iEndRing);
					ssl_CalculatedRouteHops.append(ssl_hops.toString(','));
					ssl_hops.clear(); // reset back to keep list
					ssl_hops = ssl_keep2;
				}
				else {  // continue search
					if (ssl_hops.find(bncs_string(ii)) < 0) {
						// cannot include a ring already in hops list - would indicate loop or unneccessay hops 
						//ssl_hops.append(ii);
						if (ssl_hops.count()>iNumberRiedelRings) Debug("Find_Hops -1- longer than num rings - %d %s", ssl_hops.count(), LPCSTR(ssl_hops));
						FindHopsFromOutgoing(ii, iStartRing, iEndRing);
					}
				}
			} 
		} // for ii
	}
	return;
}


int CalculateHighwayRouteHops(int iFromRing, int iToRing)
{
	// 
	if ((iFromRing > 0) && (iFromRing<MAX_RIEDEL_RINGS) && (iToRing>0) && (iToRing < MAX_RIEDEL_RINGS)) {
		//
		CRiedel_Rings* pFromRing = GetRiedel_Rings_By_TrunkAddress(iFromRing);
		if (pFromRing) {
			for (int ii = 1; ii < MAX_RIEDEL_RINGS; ii++) {
				if (ii != iFromRing) { // do not include yourself
					if (pFromRing->getNumberOutgoingHighways(ii)>0) {
						ssl_hops.clear();
						ssl_hops.append(iFromRing);
						if (ii == iToRing) {
							// destination found - so can add to calc list -- direct links ie 1 hop
							ssl_hops.append(iToRing);
							ssl_CalculatedRouteHops.append(ssl_hops.toString(','));
							ssl_hops.clear(); // reset
						}
						else { // start recurrsive search
							FindHopsFromOutgoing(ii, iFromRing, iToRing);
						}
					}
				}
			} // for int ii
		}
	}
	//Debug("CalcHighwayHops from ring %d to ring %d - hops found : %d", iFromRing, iToRing, ssl_CalculatedRouteHops.count());
	//for (int il = 0; il < ssl_CalculatedRouteHops.count(); il++) 	Debug("CalcHighwayHops - entry %d - %s", il, LPCSTR(ssl_CalculatedRouteHops[il]));
	SortCalculatedRouteHops();
	//if (bShowAllDebugMessages) {
	//	Debug("CalcHighwayHops from ring %d to ring %d - sorted hops : %d", iFromRing, iToRing, ssl_CalculatedRouteHops.count());
	//	for (int il = 0; il < ssl_CalculatedRouteHops.count(); il++) 	Debug("CalcHighwayHops - entry %d - %s", il, LPCSTR(ssl_CalculatedRouteHops[il]));
	//}
	return ssl_CalculatedRouteHops.count();
}

////////////////////////////////////////////////////////////////////////////////////////////

// 1, is source already routed all the way thru to the dest ring ( ie to other dests on that final ring ) -- special case
int FindSourceAlreadyRoutedToFinalRouter(int iPrimaryRing, int iPrimarySource, int iFinalRing, int iFinalDest)
{
	// get all incoming highways for the final ring - is the primary ring,src on any one of these ( will find direct or multiple hop routes )
	bncs_stringlist ssl_Highways = bncs_stringlist("", ',');
	CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iFinalRing);
	if (pRing) {
		bncs_stringlist ssl_Incoming = pRing->getAllIncomingHighways();
		for (int iHigh = 0; iHigh < ssl_Incoming.count(); iHigh++) {
			CHighwayRecord* pHigh = GetHighwayRecord(ssl_Incoming[iHigh].toInt());
			if (pHigh) {
				int iTracedRing = 0, iTracedGrd = 0, iTracedSrc = 0;
				pHigh->getTracedRouterAndPort(&iTracedRing, &iTracedGrd, &iTracedSrc);
				Debug("FindSrceAlreadyRouted - high %d trace ring %d src %d, Primary is %d %d", pHigh->getRecordIndex(),  iTracedRing, iTracedSrc, iPrimaryRing, iPrimarySource);
				if ((iTracedRing == iPrimaryRing) && (iTracedSrc == iPrimarySource)) {
					// as this incoming Highway has required ring and source ( do not care how many hops and even if there is now a quicker route available )
					if ((iFinalDest>0) && (iFinalDest<IFB_HIGHWAY_BASE)) {
						// only make final route for standard highway use - not needed for IFB/ Mixer input use
						bncs_string ssRoute = bncs_string("RC %1 %2 %3").arg(pRing->iRing_Device_GRD).arg(pHigh->getToSource()).arg(iFinalDest);
						ssl_PotentialRouteCommands.append(ssRoute);
					}
					if (bShowAllDebugMessages) Debug("FindSourceAlreadyRoutedToFinalRouter highway %d ", pHigh->getRecordIndex());
					return pHigh->getRecordIndex();
				}
			}
		} // for iHigh
	}
	else
		Debug("FindSrceAlreadyRouted No Ring for trunk %d", iFinalRing);
	return UNKNOWNVAL;
}


// 2. is dest using a direct highway and solo use ?? ( if solo - reuse highway ) -- special case
BOOL FindDestinationAlreadyUsesSoloHighway(int iPrimarySource, int iFinalDest, bncs_string ss_Rings)
{
	BOOL bRouteFound = TRUE;
	// get final dest - is it using highway from required ring - if so and solo user - reuse highway 
	int iReqDest = iFinalDest;
	// go backwards from final ring to first ring in list and see if just one user along its length - then it can be reused
	bncs_stringlist ssl_route = bncs_stringlist(ss_Rings, ',');
	int iNumHops = ssl_route.count() - 1;
	//Debug("FindDestAlreadySolo - src %d dest %d hops %d %s", iPrimarySource, iReqDest, iNumHops, LPCSTR(ssl_route.toString(',')));
	if (iNumHops > 0) {
		int iHop = iNumHops;
		while ((iHop>0)&&(bRouteFound)) {
			CRiedel_Rings* pHopInRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iHop].toInt());
			CRiedel_Rings* pHopOutRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iHop - 1].toInt());
			if (pHopOutRing&&pHopInRing) {
				CRiedel_GRDs* pToGrd = GetRiedel_GRD_Record(pHopInRing->iRing_Device_GRD);
				if (pToGrd) {
					int iGrdSource = pToGrd->getRoutedSourceForDestination(iReqDest);
					if (iGrdSource > 0) {
						int iNumberUsers = pToGrd->getNumberDestinationsUsingSource(iGrdSource);
						CHighwayRecord* pHigh = GetHighwayRecordByToRtrSrce(pHopInRing->iRing_Device_GRD, iGrdSource);
						if ((pHigh) && (iNumberUsers == 1)) {
							//  need to check no current IFB/mixer users too 
							if ((pHigh->getFromTrunkAddr() == pHopOutRing->m_iTrunkAddress) && (pHigh->getHighwayState() > HIGHWAY_PENDING) && (pHigh->getNumberWorkingModeEntries()==0)) {
								// success - conditions met - highway found - add commands into PotentialRoutes  -- if pending means something is in process of changing - so dont use.
								bncs_string ssRoute = bncs_string("RC %1 %2 %3").arg(pHopInRing->iRing_Device_GRD).arg(iGrdSource).arg(iReqDest);
								ssl_PotentialRouteCommands.append(ssRoute);
								if (bShowAllDebugMessages) Debug("FindDestSoloHighway highway route %s", LPCSTR(ssRoute));
								// set reqDest to that of dest feeding into toHigh
								iReqDest = pHigh->getFromDestination();
							}
							else bRouteFound = FALSE;
						}
						else bRouteFound = FALSE;
					}
					else bRouteFound = FALSE;
				}
				else bRouteFound = FALSE;
			}
			else bRouteFound = FALSE;
			iHop--;
		} // while  iHop
	}
	else bRouteFound = FALSE;
	//
	if ((bRouteFound) && (ssl_PotentialRouteCommands.count()>0) && (ssl_PotentialRouteCommands.count() == iNumHops)) {
		// test to see number of potential routes is expected number -- add in route for primary source to first highway - first ring in route list
		CRiedel_Rings* pFirstRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[0]);
		if (pFirstRing) {
			bncs_string ssRoute = bncs_string("RC %1 %2 %3").arg(pFirstRing->iRing_Device_GRD).arg(iPrimarySource).arg(iReqDest);
			ssl_PotentialRouteCommands.append(ssRoute);  // all done now
			if (bShowAllDebugMessages) Debug("FindDestSoloHighway final route %s", LPCSTR(ssRoute));
		}
		else { // fails at this point due to invalid ring !
			Debug("FindDestinationAlreadyUsesSoloHighway -- Ring Route %s failed FIRST ring no class ", LPCSTR(ss_Rings) );
			bRouteFound = FALSE;   // exit
		}
	}
	else 
		bRouteFound = FALSE;   // exit
	//
	return bRouteFound;
}

// 3. see if src already on a direct highway from one ring to another within the current sequence of hops- and reuse that one
// generic function
int FindSourceAlreadyOnHighway(int* iReqSrc, CRiedel_Rings* pFromRing, CRiedel_Rings* pToRing)
{
	// check the known outgoing highways fromRing - is src already routed to any ?
	int iCurrSource = *iReqSrc;
	if ((pFromRing&&pToRing) && (iCurrSource>0)) {
		// get list of direct highways from-ring to the given to-ring explicitly  
		bncs_stringlist ssl_DirectHways = bncs_stringlist(pFromRing->getOutgoingHighways(pToRing->m_iTrunkAddress), ',');
		for (int iH = 0; iH < ssl_DirectHways.count(); iH++) {
			CHighwayRecord* pFromHigh = GetHighwayRecord(ssl_DirectHways[iH].toInt());
			if (pFromHigh) {
				CRiedel_GRDs* pFromGrd = GetRiedel_GRD_Record(pFromHigh->getFromRouter());
				if (pFromGrd) {
					if (pFromGrd->getHighwayDestPendingSource(pFromHigh->getFromDestination()) == iCurrSource) {
						// check highway state and allocated source 
						if ((pFromHigh->getHighwayState() >= HIGHWAY_PENDING) && (pFromHigh->getHighwayAllocatedIndex() == iCurrSource)) {
							// success - highway found - add commands into PotentialRoutes ( reasserts route )
							Debug("FindSourceAlready on highway indx %d from %d -> %d then next src is %d ", pFromHigh->getRecordIndex(), iCurrSource, pFromHigh->getFromDestination(), pFromHigh->getToSource());
							// set highway to pending - so as not to possibly reuse before cmds sent and returned
							pFromHigh->setHighwayState(HIGHWAY_PENDING, iCurrSource, 3);
							pFromGrd->setHighwayDestPendingSource(pFromHigh->getFromDestination(), iCurrSource);
							bncs_string ssRoute1 = bncs_string("RC %1 %2 %3").arg(pFromGrd->getDevice()).arg(iCurrSource).arg(pFromHigh->getFromDestination());
							ssl_PotentialRouteCommands.append(ssRoute1);
							// set the source to that of src on the inRing
							*iReqSrc = pFromHigh->getToSource();
							return pFromHigh->getRecordIndex();
						}
					}
				}
			}
		}
	}
	return 0;
}

// 4. get next free highway from specified from and to rings - use highway if not in use  - generic function
int FindNextFreeHighway(int* iReqSrc, CRiedel_Rings* pFromRing, CRiedel_Rings* pToRing, int iAssocIFBMxrIndx, int iAssocIFBMxrMode, int iIFBAllocIndx )
{
	int iCurrSource = *iReqSrc;
	// get the list of defined highways from ring into to ring - go thru until first free one - if none free flag error
	if ((pFromRing&&pToRing)&&((iCurrSource>0)||(iIFBAllocIndx>IFB_HIGHWAY_BASE))) {
		//
		// xxx what to do for multi hub - if other site not there ? - but is that due to other RMSTR not running or to SLINK down ?
		//  if location / multi hub in operation and link lost ? still check/find those highways going to the other side  ???
		// if (bAutoLocationInUse) {}
		//
		bncs_stringlist ssl_DirectHways = bncs_stringlist(pFromRing->getOutgoingHighways(pToRing->m_iTrunkAddress), ',');
		for (int iH = 0; iH < ssl_DirectHways.count(); iH++) {
			CHighwayRecord* pHigh = GetHighwayRecord(ssl_DirectHways[iH].toInt());
			if (pHigh) {
				if ((pHigh->getHighwayState() == HIGHWAY_FREE) && ((pHigh->getHighwayAllocatedIndex() == PARK_SRCE)|| (pHigh->getHighwayAllocatedIndex() == 0)) ) {
					CRiedel_GRDs* pFromGrd = GetRiedel_GRD_Record(pHigh->getFromRouter());
					if (pFromGrd) {
						int iRoutedsrc = pFromGrd->getRoutedSourceForDestination(pHigh->getFromDestination());   
						if ( (iRoutedsrc == PARK_SRCE)||(iRoutedsrc==0) ) {  
							// is this test really required ???
							// if not the park srce -- why not ??? as highway is marked free ??? -- marked free but rev from riedel grd not returned yet ??? 
							// success - highway found - add commands into PotentialRoutes
							Debug("FindNextFreeHighway indx %d from %d -> %d then next src is %d ", pHigh->getRecordIndex(), iCurrSource, pHigh->getFromDestination(), pHigh->getToSource());
							// was if ((iAssocIFBMxrIndx > 0) && (iCurrSource > IFB_HIGHWAY_BASE)) {
							// set highway to at least pending - so as not to possibly get reused before cmds sent have returned
							// for standard highway routing
							pHigh->setHighwayState(HIGHWAY_PENDING, iCurrSource, 3);
							pFromGrd->setHighwayDestPendingSource(pHigh->getFromDestination(), iCurrSource);
							// tweak if being used for IFB-MIXER in/out
							if ((iAssocIFBMxrIndx > 0) && (iAssocIFBMxrMode>0)) {
								if ((iAssocIFBMxrMode == MODE_IFBINPUT) || (iAssocIFBMxrMode == MODE_MIXERIN)) {
									pHigh->setHighwayState(HIGHWAY_IFBMXRIN, iIFBAllocIndx, 3);
									pHigh->addWorkingMode(iAssocIFBMxrMode, iAssocIFBMxrIndx);
								}
								else if ((iAssocIFBMxrMode == MODE_IFBOUTPUT) || (iAssocIFBMxrMode == MODE_MIXEROUT)) {
									pHigh->setHighwayState(HIGHWAY_IFBMXROUT, iIFBAllocIndx, 3);
									pHigh->addWorkingMode(iAssocIFBMxrMode, iAssocIFBMxrIndx);
								}
							}
							bncs_string ssRoute1 = bncs_string("RC %1 %2 %3").arg(pFromGrd->getDevice()).arg(iCurrSource).arg(pHigh->getFromDestination());
							ssl_PotentialRouteCommands.append(ssRoute1);
							// set the source to that of src on the inRing
							*iReqSrc = pHigh->getToSource();
							return pHigh->getRecordIndex();
						}
					}
				}
			}
		}
	}
	return 0;
}


BOOL FindSuccessfulRoute(int iStartRing, int iStartSrc, int iFinalRing, int iFinalDest, bncs_string ss_Rings)
{
	int iSrcToRoute = iStartSrc;
	ssl_PotentialRouteCommands.clear();
	bncs_stringlist ssl_route = bncs_stringlist(ss_Rings, ',');
	int iNumHops = ssl_route.count() - 1;
	// 2. special test - is final dest using a highway route back to start ring and solo in its use in all hops?? ( if solo - reuse highway )
	if (!FindDestinationAlreadyUsesSoloHighway(iSrcToRoute, iFinalDest, ss_Rings)) {
		// Debug("FindSucessfulRute thru 2");
		// reset potential commands
		ssl_PotentialRouteCommands.clear();
		for (int iHop = 0; iHop < (ssl_route.count() - 1); iHop++) {
			CRiedel_Rings* pHopOutRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iHop]);
			CRiedel_Rings* pHopInRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iHop + 1]);
			if (pHopOutRing&&pHopInRing) {
				// 3. is source already on a highway to next ring in sequence- then use that highway
				if (FindSourceAlreadyOnHighway(&iSrcToRoute, pHopOutRing, pHopInRing) <= 0) {
					// Debug("FindSucessfulRute thru 3 fail");
					// 4. else see if a free highway from those not in use
					if (FindNextFreeHighway(&iSrcToRoute, pHopOutRing, pHopInRing, 0, 0, 0) <= 0) {
						Debug("FindSuccessfulRoute -- no Ring Route %s available- from r:%d s:%d to r:%d %d", LPCSTR(ss_Rings), ssl_route[0].toInt(), iStartSrc, ssl_route[iNumHops].toInt(), iFinalDest);
						return FALSE;   // exit as no route on this sequence of hops
					}
					else Debug("FindSucessfulRute thru 4 - next free highway found");
				}
				else Debug("FindSucessfulRute thru 3 - srce already on highway");
			}
			else {
				Debug("FindSuccessfulRute - invalid classes");
				return FALSE;   // exit immediately - error in classes
			}
		} // for iHop
		// successful to this point - so add in final grd rc to complete routing
		if ((ssl_PotentialRouteCommands.count() > 0) && (iNumHops>0) && (iNumHops<ssl_route.count())) {
			// get last ring in ssRoute
			CRiedel_Rings* pFinalRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iNumHops]);
			if (pFinalRing) {
				bncs_string ssRoute = bncs_string("RC %1 %2 %3").arg(pFinalRing->iRing_Device_GRD).arg(iSrcToRoute).arg(iFinalDest);
				ssl_PotentialRouteCommands.append(ssRoute);  // all done now
				if (bShowAllDebugMessages) Debug("FindSuccessfulRoute %s", LPCSTR(ssRoute));
			}
			else { // fails at last point due to invalid ring !
				Debug("FindSuccessfulRoute -- Ring Route %s failed FINAL ring no class - from r:%d s:%d to r:%d %d", LPCSTR(ss_Rings), ssl_route[0].toInt(), iStartSrc, ssl_route[iNumHops].toInt(), iFinalDest);
				return FALSE;   // exit
			}
		}
		else { // fails at last point due to invalid hops !
			Debug("FindSuccessfulRoute -- Ring Route %s failed invalid hops %d", LPCSTR(ss_Rings), iNumHops);
			return FALSE;   // exit 
		}
	}
	//
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////
//
// MCR highway acquisation

int CanExistingHighwayBeUsed(CMCRMonPortRecord*  pMonPortRec, int iTrunkToMonitor, int iNewMonMode, BOOL* bUsingNewHighway)
{
	// determine if current highway can be used - or existing needs clearing as no longer required
	if (pMonPortRec) {
		if (pMonPortRec->m_iUsingHighwayRecord > 0) {
			CHighwayRecord* pHigh = GetHighwayRecord(pMonPortRec->m_iUsingHighwayRecord);
			if (pHigh) {
				if ((pHigh->getHighwayState() == HIGHWAY_CLONEMON) && (pHigh->getFromTrunkAddr() == iTrunkToMonitor) && (pHigh->getNumberWorkingModeEntries() == 1)) {
					Debug("FindHighwayMCRMon - mon trunk %d found on highway %d ", iTrunkToMonitor, pHigh->getRecordIndex());
					// delete existing working entry for this mcr mon port
					pHigh->clearWorkingModes();
					pHigh->addWorkingMode(iNewMonMode, pMonPortRec->m_iRecordIndex);
					*bUsingNewHighway = FALSE;   // re-using existing highway
					return pHigh->getRecordIndex();
				}
				else {
					// existing highway not applicable -- if multiple users --just remove workingMode - let others users keep highway, else park it
					pHigh->removeWorkingMode(pMonPortRec->m_iWorkingModeForHighway, pMonPortRec->m_iRecordIndex);
					pMonPortRec->m_iUsingHighwayRecord = 0;
					pMonPortRec->m_iWorkingModeForHighway = 0;
					if (pHigh->getNumberWorkingModeEntries() ==0) ParkHighway(pMonPortRec->m_iUsingHighwayRecord);
				}
			}
			else {
				pMonPortRec->m_iUsingHighwayRecord = 0;
				pMonPortRec->m_iWorkingModeForHighway = 0;
			}
		}
	}
	return 0;
}

int FindHighwayForMCRMonitoring(CMCRMonPortRecord*  pMonPortRec, int iTrunkToMonitor, int iPortToMonitor, int iMonStyle )
{
	if (pMonPortRec) {
		// get from list of mon highways incoming from trunk into mcr ring and see if requied port to monitor is already using a highway
		CRiedel_Rings* pMonRing = GetRiedel_Rings_By_TrunkAddress(pMonPortRec->m_iMonTrunkAddr);
		if (pMonRing) {
			bncs_stringlist ssl_highs = bncs_stringlist(pMonRing->getIncomingMonitoring(iTrunkToMonitor), ',');
			Debug("FingHigh4Mon - highs from %d - into %d - %s", iTrunkToMonitor, pMonRing->m_iTrunkAddress, LPCSTR(pMonRing->getIncomingMonitoring(iTrunkToMonitor)));
			for (int iH = 0; iH < ssl_highs.count(); iH++) {
				CHighwayRecord* pHigh = GetHighwayRecord(ssl_highs[iH]);
				if (pHigh) {
					if ((pHigh->getHighwayState() == HIGHWAY_CLONEMON) && (pHigh->getHighwayAllocatedIndex() == iPortToMonitor)) {
						// get working mode -- is it same as given mon style -- ie source or dest mon
						if (pHigh->getAllWorkingModeEntries().find(bncs_string("%1,").arg(iMonStyle)) >= 0) {
							Debug("FindHighwayMCRMon -1- style %d mon index %d.%d found on highway %d ", iMonStyle, iTrunkToMonitor, iPortToMonitor, pHigh->getRecordIndex());
							// so add additional user working mode
							pHigh->addWorkingMode(iMonStyle, pMonPortRec->m_iRecordIndex);
							return pHigh->getRecordIndex();
						}
					}
				}
			} // for iH
			// else get the next free mon highway from list from ring into MCR ring -- limited to single hop at present --- direct defined MON highways only ....
			for (int iNH = 0; iNH < ssl_highs.count(); iNH++) {
				CHighwayRecord* pHigh = GetHighwayRecord(ssl_highs[iNH]);
				if (pHigh) {
					if (pHigh->getHighwayState() == HIGHWAY_FREE)  {
						// found a free monitoring highway -- so allocate 
						pHigh->setHighwayState(HIGHWAY_CLONEMON, iPortToMonitor, 3);
						pHigh->addWorkingMode(iMonStyle, pMonPortRec->m_iRecordIndex);
						Debug("FindHighwayMCRMon -2- mon set %d.%d for highway %d ", iTrunkToMonitor, iPortToMonitor, pHigh->getRecordIndex());
						return pHigh->getRecordIndex();
					}
				}
			} // for iNH
		}
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////

void UnlockGRDDestination(CRiedel_Rings* pRing, int iGrdDevice, int iGrdDest)
{
	// to be called for standard dests only - highways are to be handled differently
	CRiedel_GRDs* pGrd = GetRiedel_GRD_Record(iGrdDevice);
	if (pRing&&pGrd) {
		// unlock the dest in records, unlock highways too if required 
		pGrd->unlockDestination(iGrdDest);
		UpdateGRDRevertive(pGrd, iGrdDest);
		// go thru highways - trace back -- update grd rev if mapped
	}
}


void LockGRDDestination(CRiedel_Rings* pRing, int iGrdDevice, int iGrdDest, bncs_string ssReason )
{
	// to be called for standard dests only - highways are to be handled differently
	CRiedel_GRDs* pGrd = GetRiedel_GRD_Record(iGrdDevice);
	if (pRing&&pGrd) {
		// unlock the dest in records, unlock highways too if required 
		pGrd->changeLockStatus(iGrdDest, DEST_LOCKED, ssReason );
		UpdateGRDRevertive(pGrd, iGrdDest);
		// go thru highways - trace back and locking accordingly and updating grd rev ?? -- if mapped 
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: DetermineWhichInfoDriver() is notification from a valid infodriver
//

int DetermineWhichInfoDriverType( int iDeviceNumber )
{
	if (iDeviceNumber==i_info_RingMaster) return AUTO_RINGMASTER_INFO;
	else if (iDeviceNumber==i_info_Conferences) return AUTO_CONFERENCES_INFO;
	else if (iDeviceNumber == i_info_Ports_PTI) return AUTO_PORTS_PTI_LOGIC_INFO;
	else if ((iDeviceNumber >= i_info_IFBs_Start) && (iDeviceNumber <= i_info_IFBs_End)) return AUTO_RING_IFBS_INFOS;
	else if ((iDeviceNumber >= i_info_Ports_Start) && (iDeviceNumber <= i_info_Ports_End)) return AUTO_RING_PORTS_INFOS;
	//     - ie auto will not respond to any writes / slot changes to these infodrivers
	return UNKNOWNVAL;
}


BOOL ProcessIncomingRouteCommand(int iInfoDevice, int iInfoSlot, bncs_string ssSlot)
{
	ssl_PotentialRouteCommands.clear();
	if (iNextManagementProcessing < 2) iNextManagementProcessing++; // give time for revs to come back from cmds issued
	// determine dest for info dev and slot
	int iGrdDest = 0, iGrdDevice = 0;  // final dest, grd 
	// get ring for dev and slot
	CRingPortsDefn* pPortDefn = GetRingPortsDefintionByInfodriverSlot(iInfoDevice, iInfoSlot);
	if (pPortDefn) {
		//
		CRiedel_Rings* pToRing = GetRiedel_Rings_By_TrunkAddress(pPortDefn->iRingTrunk);
		if (pToRing) {
			iGrdDevice = pToRing->iRing_Device_GRD;
			iGrdDest = pPortDefn->iRingPort;
			CRiedel_GRDs* pFinalGrd = GetRiedel_GRD_Record(iGrdDevice);
			if ((pFinalGrd) && (ssSlot.length()>0)) {
				if ((iGrdDest > 0) && (iGrdDest <= pFinalGrd->getMaximumDestinations())) {
					// check for PARK or LOCK or UNLOCK keywords
					if (ssSlot == "PARK") {
						// park even if locked -- as it is one way of clearing a lock
						Debug("ProcIncomingRoute - PARK command for %d dest %d", iGrdDevice, iGrdDest);
						MakeRiedelGRDRoute(iGrdDevice, PARK_SRCE, iGrdDest);
						return TRUE;
					}
					else if (ssSlot == "UNLOCK") {
						Debug("ProcIncomingRoute - UNLOCK command for d:%d", iGrdDevice, iGrdDest);
						UnlockGRDDestination(pToRing, iGrdDevice, iGrdDest);
						return TRUE; // ??? what if route and unlock come together ?
					}
					else if (ssSlot.find("LOCK") >= 0) {
						Debug("ProcIncomingRoute - LOCK command for d:%d", iGrdDevice, iGrdDest);
						// find reason ???
						bncs_string ssReason = "";
						LockGRDDestination(pToRing, iGrdDevice, iGrdDest, ssReason);
						return TRUE; // ??? what if route and lock come together ?
					}
					// numeric input of ring,source to route
					// get source required - either single integer - for source on same device or dev,src from 
					//bncs_stringlist ssl_src = bncs_stringlist(ssSlot, '.');
					BOOL bSndChnl = FALSE;
					int iSrcTrunk = 0, iSrcGrd = 0, iSrcSrc = 0;
					GetTrunkAndPortFromString(ssSlot, &iSrcTrunk, &iSrcSrc, &bSndChnl);
					if ((iSrcTrunk>0) && (iSrcSrc>0) && (iSrcSrc != PARK_SRCE)) {
						CRiedel_Rings* pFromRing = GetRiedel_Rings_By_TrunkAddress(iSrcTrunk);
						if (pFromRing) {
							iSrcGrd = pFromRing->iRing_Device_GRD;
							if (iSrcGrd != iGrdDevice) {
								if (pFinalGrd->getLockStatus(iGrdDest) == DEST_UNLOCKED) {
									// different grds == different rings requires highway
									// 1. is src already routed to another dest on same final rtr -- ie route already exists
									BOOL bFoundRoute = FALSE;
									if (FindSourceAlreadyRoutedToFinalRouter(iSrcTrunk, iSrcSrc, pToRing->m_iTrunkAddress, iGrdDest) > 0) {
										// Debug("FindSucessfulRute thru 1");
										// process thru list making actual grd routes and execute rc commands
										for (int iC = 0; iC < ssl_PotentialRouteCommands.count(); iC++) 
											AddCommandToQue(LPCSTR(ssl_PotentialRouteCommands[iC]), ROUTERCOMMAND, 0, 0, 0);
										ProcessNextCommand(iNumberRiedelRings);
										ssl_PotentialRouteCommands.clear(); // finished with for now
										bFoundRoute = TRUE;
										return TRUE;
									}
									else {
										//Debug("ProcIncomingRute gettting possible routes");
										ssl_CalculatedRouteHops.clear();  // reset global for possible routes listing
										int iPossibleRoutes = CalculateHighwayRouteHops(pFromRing->m_iTrunkAddress, pToRing->m_iTrunkAddress);
										Debug("ProcIncomingRute hops from ring %d to ring %d - possible routes : %d", pFromRing->m_iTrunkAddress, pToRing->m_iTrunkAddress, iPossibleRoutes);
										if (iPossibleRoutes > 0) {
											int iRoute = 0;
											do {
												ssl_PotentialRouteCommands.clear();
												// process thru ssl_route - which is a list of ring hops - if successful make rcs then exit method
												if (FindSuccessfulRoute(iSrcTrunk, iSrcSrc, pToRing->m_iTrunkAddress, iGrdDest, ssl_CalculatedRouteHops[iRoute])) {
													// process thru list making actual grd routes and execute rc commands
													for (int iC = 0; iC < ssl_PotentialRouteCommands.count(); iC++) AddCommandToQue(LPCSTR(ssl_PotentialRouteCommands[iC]), ROUTERCOMMAND, 0, 0, 0);
													ProcessNextCommand(iNumberRiedelRings);
													ssl_PotentialRouteCommands.clear(); // finished with for now
													bFoundRoute = TRUE;
													return TRUE;
												}
												iRoute++;
											} while ((!bFoundRoute) && (iRoute < iPossibleRoutes));
										}
									}
									// ALL possible routes have failed if this point reached
									SetAlarmErrorMessage(iSrcTrunk, bncs_string("ProcessRoute src %1.%2 to dest %3.%4 - FAILED - no highways").arg(iSrcTrunk).arg(iSrcSrc).arg(pToRing->m_iTrunkAddress).arg(iGrdDest));
									Debug("ProcIncomingRoute -ERROR- NO ROUTES FOUND source grd %d src %d to grd %d dest %d ", iSrcGrd, iSrcSrc, iGrdDevice, iGrdDest);
								}
								else {
									SetAlarmErrorMessage(pToRing->m_iTrunkAddress, bncs_string("RingMaster-ProcessRoute LOCKED dest %1.%2 ").arg(pToRing->m_iTrunkAddress).arg(iGrdDest));
									Debug("ProcIncomingRoute -1- DEST LOCKED trunk %d grd %d dest %d", pToRing->m_iTrunkAddress, iGrdDevice, iGrdDest);
								}
								return FALSE;
							}
							else {
								// on same ring - simple route
								if (((iSrcSrc > 0) && (iSrcSrc != PARK_SRCE)) && (iSrcSrc <= pFinalGrd->getMaximumSources())) {
									if (pFinalGrd->getLockStatus(iGrdDest) == DEST_UNLOCKED) {
										Debug("ProcIncomingRoute - same ring %d s:%d d:%d", iSrcGrd, iSrcSrc, iGrdDest);
										MakeRiedelGRDRoute(iSrcGrd, iSrcSrc, iGrdDest);
										return TRUE;
									}
									else {
										SetAlarmErrorMessage(pToRing->m_iTrunkAddress, bncs_string("RingMaster-ProcessRoute LOCKED dest %1.%2 ").arg(pToRing->m_iTrunkAddress).arg(iGrdDest));
										Debug("ProcIncomingRoute -2- DEST LOCKED trunk %d grd %d dest %d", pToRing->m_iTrunkAddress, iGrdDevice, iGrdDest);
									}
									return FALSE;
								}
								else if (iSrcSrc == PARK_SRCE) {
									// park even if locked -- as it is one way of clearing a lock
									pFinalGrd->unlockDestination(iGrdDest);
									Debug("ProcIncomingRoute - single PARK srce to grd %d dest %d", iSrcGrd, iGrdDest);
									MakeRiedelGRDRoute(iSrcGrd, iSrcSrc, iGrdDest);
									return TRUE;
								}
							}
						}
						else
							Debug("ProcIncomingRoute - no FROM ring class %d", iSrcTrunk);
					}
					else if (iSrcSrc == PARK_SRCE) {
						// park even if locked -- as it is one way of clearing a lock
						pFinalGrd->unlockDestination(iGrdDest);
						Debug("ProcIncomingRoute - single PARK srce to grd %d dest %d", iSrcGrd, iGrdDest);
						MakeRiedelGRDRoute(iGrdDevice, PARK_SRCE, iGrdDest);
						return TRUE;
					}
					else
						Debug("ProcIncomingRoute - invalid source %d %d from string %s", iSrcGrd, iSrcSrc, LPCSTR(ssSlot));
				}
				else
					Debug("ProcIncomingRoute -dest %d outside valid range 1..%d for grd %d", iGrdDest, pFinalGrd->getMaximumDestinations(), iGrdDevice);
			}
			else
				Debug("ProcIncomingRoute - issue with grd %d  or slot data :%s: ", iGrdDest, LPCSTR(ssSlot));
		}
		else
			Debug("ProcIncomingRoute - no ring for dev %d slot %d", iInfoDevice, iInfoSlot);
	}
	else
		Debug("ProcIncomingRoute - no Ring Port Definition for dev %d slot %d", iInfoDevice, iInfoSlot);
	return FALSE;
}


/////////////////////////////////////////////////////////////////////////////////////////
//
//   IFB processing

void QueueCmdForRiedelDriver(int iRiedelInfo, int iRiedelSlot, bncs_string ssCmd)
{
	char szCommand[MAX_AUTOBFFR_STRING] = "";
	wsprintf(szCommand, "IW %d '%s' %d", iRiedelInfo, LPCSTR(ssCmd), iRiedelSlot);
	AddCommandToQue(szCommand, INFODRVCOMMAND, iRiedelInfo, iRiedelSlot, 0);
}


void GetTrunkAndPortFromString(bncs_string ssParams, int* iTrunk, int* iPort, BOOL* bSecondChannel)
{
	// ideally it should be <trunk>.<port> -- if not treated as just a port if only 1 parameter
	// was *iTrunk =0;   NOTE -- THIS VAR MUST BE ASSIGNED BEFORE CALLING THIS FUNCTION e.g to 0 or the DEFAULT TRUNK value
	*iPort = 0;
	*bSecondChannel = FALSE;
	if ((ssParams.find("s") >= 0) || (ssParams.find("S") >= 0)) *bSecondChannel = TRUE;
	bncs_stringlist ssl = bncs_stringlist(ssParams, '.');
	if (ssl.count() > 1) {
		*iTrunk = ssl[0].toInt();
		*iPort = ssl[1].toInt();
	}
	else if (ssl.count() == 1) {
		//  note if there is an 's' character, the .toInt will still return the number part of the given string
		*iPort = ssl[0].toInt();
	}
}

bncs_string GetTrunkAndPortAnddBFromString(bncs_string ssParams, int* iTrunk, int* iPort, BOOL* bSecondChannel)
{  // ideally it should be <trunk.port>:<db>  where dB may be MUTE or 0.0 -12.5 etc
	*iTrunk = 0;
	*iPort = 0;
	*bSecondChannel = FALSE;
	if ((ssParams.find("s") >= 0) || (ssParams.find("S") >= 0)) *bSecondChannel = TRUE;
	bncs_string ssdB = "0.0";
	bncs_stringlist ssl = bncs_stringlist(ssParams, ':');
	if (ssl.count() > 0) {
		bncs_stringlist ssldata = bncs_stringlist(ssl[0], '.');
		if (ssldata.count() > 1) {
			*iTrunk = ssldata[0].toInt();
			*iPort = ssldata[1].toInt();
		}
		else if (ssldata.count() == 1) {
			*iPort = ssldata[0].toInt();
		}
	}
	if (ssl.count() > 1) ssdB = ssl[1];
	return ssdB;
}

bncs_string GetPortAnddBFromString(bncs_string ssParams, int* iPort, BOOL*bSecondChannel)
{  // ideally it should be <port>:<db>
	*iPort = 0;
	*bSecondChannel = FALSE;
	if ((ssParams.find("s") >= 0) || (ssParams.find("S") >= 0)) *bSecondChannel = TRUE;
	bncs_string ssdB = "0.0";
	bncs_stringlist ssl = bncs_stringlist(ssParams, ':');
	if (ssl.count() > 0) *iPort = ssl[0].toInt();
	if (ssl.count() > 1) ssdB = ssl[1];
	return ssdB;
}

int GetReidel_IFB_Device_FromTrunk(int iGivenTrunk)
{
	if ((iGivenTrunk > 0) && (iGivenTrunk < MAX_RIEDEL_RINGS)) {
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iGivenTrunk);
		if (pRing) {
			return pRing->iRing_Device_IFB;
		}
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  MCR / STUDIO MON COMMANDS
//
void ProcessMCRMonCommand(int iInfoSlot, bncs_string ssSlot)
{
	char szCommand[MAX_AUTOBFFR_STRING] = "";
	if (iNextManagementProcessing < 3) iNextManagementProcessing++; // give time for revs to come back from cmds issued
	// get mcr dest record - is route different to that already, clear existing highway, make new routing
	CMCRMonPortRecord* pMonPortRec = GetMCRMonPortByInfodriverSlot(iInfoSlot);
	if (pMonPortRec) {
		int iWhichMonRecord = pMonPortRec->m_iRecordIndex;
		if (pMonPortRec->m_ssCommandRevertive != ssSlot) {
			//
			CRiedel_Rings* pMonRing = GetRiedel_Rings_By_TrunkAddress(pMonPortRec->m_iMonTrunkAddr);
			if (pMonRing) {
				// get new data from slot
				BOOL bSndChnl = FALSE;
				int iTrunkToMonitor = 0, iPortToMonitor = 0;
				bncs_stringlist ssl = bncs_stringlist(ssSlot, ',');
				GetTrunkAndPortFromString(ssl[0], &iTrunkToMonitor, &iPortToMonitor, &bSndChnl);
				if (ssl.count() > 1) {
					int iMonType = UNKNOWNVAL;
					if (ssl[1].upper() == bncs_string("S")) iMonType = MODE_CLONESRCE;
					else if (ssl[1].upper() == bncs_string("D")) iMonType = MODE_CLONEDEST;
					if ((iTrunkToMonitor > 0) && (iPortToMonitor > 0) && ((iMonType == MODE_CLONESRCE) || (iMonType == MODE_CLONEDEST))) {
						// same ring or different
						if (iTrunkToMonitor == pMonPortRec->m_iMonTrunkAddr) {
							// is mcr mon dest currently monitoring dest using a highway ? - if so clear as new mon is on same trunk
							if (pMonPortRec->m_iUsingHighwayRecord > 0) {
								// check highway is really used here - if more than 1 user - just remove working record
								CHighwayRecord* pOldHigh = GetHighwayRecord(pMonPortRec->m_iUsingHighwayRecord);
								if (pOldHigh) {
									pOldHigh->removeWorkingMode(pMonPortRec->m_iWorkingModeForHighway, iWhichMonRecord);
									if (pOldHigh->getNumberWorkingModeEntries()==0) ParkHighway(pMonPortRec->m_iUsingHighwayRecord);
								}
							}
							pMonPortRec->m_iUsingHighwayRecord = 0;
							pMonPortRec->m_iWorkingModeForHighway = 0;
							// as same ring - just monitor local port -- IW to Mon dev 
							wsprintf(szCommand, "IW %d '%d,%s' %d", pMonRing->iRing_Device_Monitor, iPortToMonitor, LPCSTR(ssl[1]), pMonPortRec->m_iMonPortTranslated);
							AddCommandToQue(szCommand, INFODRVCOMMAND, pMonRing->iRing_Device_Monitor, pMonPortRec->m_iMonPortTranslated, 0);
						}
						else {
							// on different ring -- find direct MON highway into mcr ring and route local incoming src to mon port
							BOOL bUsingNewHighway = TRUE;
							// check to see if existing highway in use and can be resused or needs clearing
							int iMonHigh = CanExistingHighwayBeUsed(pMonPortRec, iTrunkToMonitor, iMonType, &bUsingNewHighway);
							// so if not reused - get new highway
							if (iMonHigh<=0) iMonHigh = FindHighwayForMCRMonitoring(pMonPortRec, iTrunkToMonitor, iPortToMonitor, iMonType);
							// need to clear old highway / links - if there was one and not reusing it
							if ((pMonPortRec->m_iUsingHighwayRecord > 0)&&(pMonPortRec->m_iUsingHighwayRecord!=iMonHigh)) {
								CHighwayRecord* pOldHigh = GetHighwayRecord(pMonPortRec->m_iUsingHighwayRecord);
								if (pOldHigh) {
									pOldHigh->removeWorkingMode(pMonPortRec->m_iWorkingModeForHighway, iWhichMonRecord);
									if (pOldHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pMonPortRec->m_iUsingHighwayRecord);
									pMonPortRec->m_iUsingHighwayRecord = 0;
									pMonPortRec->m_iWorkingModeForHighway = 0;
								}
							}
							// now process new highway if acquired
							CHighwayRecord* pHigh = GetHighwayRecord(iMonHigh);
							CRiedel_Rings* pFromRing = GetRiedel_Rings_By_TrunkAddress(iTrunkToMonitor);
							if (pHigh&&pFromRing) {
								pMonPortRec->m_iUsingHighwayRecord = iMonHigh;
								pMonPortRec->m_iWorkingModeForHighway = iMonType;
								// command to clone requested port into outgoing mon highway
								wsprintf(szCommand, "IW %d '%d,%s' %d", pFromRing->iRing_Device_Monitor, iPortToMonitor, LPCSTR(ssl[1]), pHigh->getFromDestination());
								AddCommandToQue(szCommand, INFODRVCOMMAND, pFromRing->iRing_Device_Monitor, pHigh->getFromDestination(), 0);
								if (bUsingNewHighway ) {
									// if reusing existing highway then no need to send command into highway - as already set up 
									// just send command to clone incoming highway source to this final monitoring dest- 
									wsprintf(szCommand, "IW %d '%d,S' %d", pMonRing->iRing_Device_Monitor, pHigh->getMonitorGivenToSource(), pMonPortRec->m_iMonPortTranslated);
									AddCommandToQue(szCommand, INFODRVCOMMAND, pMonRing->iRing_Device_Monitor, pMonPortRec->m_iMonPortTranslated, 0);
								}
							}
							else {
								// error no available highway
								SetAlarmErrorMessage(pMonPortRec->m_iMonTrunkAddr, bncs_string("Process MCR Monitor -  NO HIGHWAYS FOR mon dest %1.%2 ").arg(pMonPortRec->m_iMonTrunkAddr).arg(pMonPortRec->m_iConfigMonPort));
								Debug("ProcessMCRMon -error- NO HIGHWAYS FOR mon dest - %d.%d", pMonPortRec->m_iMonTrunkAddr, pMonPortRec->m_iConfigMonPort);
								wsprintf(szCommand, "IW %d '0' %d", pMonRing->iRing_Device_Monitor, pMonPortRec->m_iMonPortTranslated);
								AddCommandToQue(szCommand, INFODRVCOMMAND, pMonRing->iRing_Device_Monitor, pMonPortRec->m_iMonPortTranslated, 0);
								ssSlot = "0";
							}
						}
						// update slot 
						ProcessNextCommand(10);
						pMonPortRec->m_ssCommandRevertive = ssSlot;
						eiRingMasterInfoId->updateslot(iInfoSlot, LPCSTR(ssSlot));
					}
				}
				else {
					// zero or PARK - to clear current routing  
					if ((ssSlot.find("PARK") >= 0) || (ssSlot.toInt() == 0) ||(iPortToMonitor==0)) {
						if (pMonPortRec->m_iUsingHighwayRecord > 0) {
							// park used highway if sole user
							CHighwayRecord* pOldHigh = GetHighwayRecord(pMonPortRec->m_iUsingHighwayRecord);
							if (pOldHigh) {
								pOldHigh->removeWorkingMode(pMonPortRec->m_iWorkingModeForHighway, iWhichMonRecord);
								if (pOldHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pMonPortRec->m_iUsingHighwayRecord);
							}
							pMonPortRec->m_iUsingHighwayRecord = 0;
							pMonPortRec->m_iWorkingModeForHighway = 0;
						}
						// send park command to mcr final dest
						wsprintf(szCommand, "IW %d '0' %d", pMonRing->iRing_Device_Monitor, pMonPortRec->m_iMonPortTranslated);
						AddCommandToQue(szCommand, INFODRVCOMMAND, pMonRing->iRing_Device_Monitor, pMonPortRec->m_iMonPortTranslated, 0);
						// update slot if successful
						ProcessNextCommand(10);
						pMonPortRec->m_ssCommandRevertive = "0";
						eiRingMasterInfoId->updateslot(iInfoSlot, "0");
					}
					else
						Debug("ProcMcrMonCmd -2- invalid params from slot %d contents %s", iInfoSlot, LPCSTR(ssSlot));
				}
			}
		} // rev same - so no need to do any processing
	}
	else
		Debug("ProcMcrMonCmd - invalid record from slot %d contents %s", iInfoSlot, LPCSTR(ssSlot));
}


///////////////////////////////////////////////////////////////////////////////////////////
//
//  Highways for OUPUT mixers and ifbs
//

// is final dest already using a highway for iFB/Mixer use - then reuse highway for additional 
bncs_string FindHighwayAlreadyRoutedToIFBMixerDestination(int iFinalTrunk, int iFinalDest, int iStartTrunk)
{
	// get final dest - is it using highway from required ring - if so and for ifbs / mixer outs - use highway 
	// go backwards from final ring to first ring in list and see if just one user along its length - then it can be reused
	int iCounter = 0;
	int iDest = iFinalDest;
	int iTrunk = iFinalTrunk;
	BOOL bKeepLooking = TRUE;
	bncs_stringlist ssl_highways = bncs_stringlist("", ',');
	do {
		iCounter++;   // to prevent loop if circular routing exists
		CRiedel_GRDs* pToGrd = GetRiedel_GRD_By_TrunkAddr(iTrunk);
		if (pToGrd) {
			int iGrdSource = pToGrd->getRoutedSourceForDestination(iDest);
			CHighwayRecord* pHigh = GetHighwayRecordByToRtrSrce(pToGrd->getDevice(), iGrdSource);
			if (pHigh) {
				if (pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) { // can only reuse if highway currently used for ifb output 
					ssl_highways.append(pHigh->getRecordIndex());
					iTrunk = pHigh->getFromTrunkAddr();
					iDest = pHigh->getFromDestination();
					if (iTrunk == iStartTrunk) {
						// got back to starting trunk so route is in existance
						bKeepLooking = FALSE;
						iCounter = 99;
					}
				}
			}
			else
				bKeepLooking = FALSE;
		}
		else {
			Debug("FindHighwayAlreadyRoutedToIFB-ERROR-Getting GRD from Trunk Address %d ", iTrunk);
		}
	} while ((bKeepLooking) && (iCounter<20));  //  - should never reach 20 unless loop

	if ((ssl_highways.count() > 0)&&(iCounter==99)) {
		// should be a valid list of highways from final dest to start trunk
		int iRec = ssl_highways.count() - 1;
		CHighwayRecord* pHigh = GetHighwayRecord(ssl_highways[iRec].toInt());
		if (pHigh) {
			if (pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) return ssl_highways.toString(',');
		}
	}
	//
	return ssl_highways.toString(',');
}


int FindIFBMixerOutputHighwayRoute(int iWhichIndex, int iMode, int iFinalRing, int iFinalDest, bncs_string ss_Rings)
{
	int iSrcToRoute = 0;   // intial source for IFB out or mixer out will be 0 - special case
	int iAllocIndex = (IFB_HIGHWAY_BASE*iFinalRing) + iFinalDest;   // sets up a Allocated index that is stored in highway 
	int iFirstHighwayForIFBOutput = UNKNOWNVAL;
	ssl_PotentialRouteCommands.clear();
	bncs_stringlist ssl_route = bncs_stringlist(ss_Rings, ',');
	int iNumHops = ssl_route.count() - 1;
	for (int iHop = 0; iHop < (ssl_route.count() - 1); iHop++) {
		CRiedel_Rings* pHopOutRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iHop]);
		CRiedel_Rings* pHopInRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iHop + 1]);
		if (pHopOutRing&&pHopInRing) {
			int iWhichHigh = 0;
			//  is source already on a highway to next ring in sequence- then use that highway
			iWhichHigh = FindSourceAlreadyOnHighway(&iSrcToRoute, pHopOutRing, pHopInRing);
			if (iWhichHigh <= 0) {
				// else see if a free highway from those not in use - is start ring and srce already on highway to this ring
				iWhichHigh = FindNextFreeHighway(&iSrcToRoute, pHopOutRing, pHopInRing, iWhichIndex, iMode, iAllocIndex);
				if (iWhichHigh <= 0) {
					Debug("FindIFBMixerInputHighwayRoute -- no Route %s available- from r:%d %d ifb:%d to r:%d", LPCSTR(ss_Rings), ssl_route[0].toInt(), iSrcToRoute, iWhichIndex, ssl_route[iNumHops].toInt());
					return UNKNOWNVAL;   // exit as no route on this sequence of hops
				}
				if (iHop == 0) iFirstHighwayForIFBOutput = iWhichHigh; // assign var here to send back - MUST know the first outgoing highway index
			}
			else {
				Debug("FindSucessfulRute thru 3 - srce already on highway");
				if (iHop == 0) iFirstHighwayForIFBOutput = iWhichHigh; // assign var here to send back - MUST know the first outgoing highway index
			}
		}
		else {
			return UNKNOWNVAL;   // exit immediately - error in classes
		}
	} // for iHop

	// successful to this point - so add in final grd rc to complete routing
	if ((ssl_PotentialRouteCommands.count() > 0) && (iNumHops>0) && (iNumHops<ssl_route.count())) {
		// get last ring in ssRoute
		CRiedel_Rings* pFinalRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iNumHops]);
		if (pFinalRing) {
			bncs_string ssRoute = bncs_string("RC %1 %2 %3").arg(pFinalRing->iRing_Device_GRD).arg(iSrcToRoute).arg(iFinalDest);
			ssl_PotentialRouteCommands.append(ssRoute);  // all done now
			if (bShowAllDebugMessages) Debug("FindIFBMxrOutputHighwayRoute %s", LPCSTR(ssRoute));
		}
		else { // fails at last point due to invalid ring !
			Debug("FindIFBMxrOutputHighwayRoute -- Ring Route %s failed FINAL ring no class - from r:%d to r:%d %d", LPCSTR(ss_Rings), ssl_route[0].toInt(), ssl_route[iNumHops].toInt(), iFinalDest);
			return UNKNOWNVAL;   // exit
		}
	}
	else { // fails at last point due to invalid hops !
		Debug("FindIFBMxrOutputHighwayRoute -- Ring Route %s failed invalid hops %d", LPCSTR(ss_Rings), iNumHops);
		return UNKNOWNVAL;   // exit 
	}
	//
	return iFirstHighwayForIFBOutput;
}


int FindHighwayForIFBMixerOutputUse(int iWhichIFBMixer, int iWhichMode, int iStartTrunk, int iFinalTrunk, int iFinalDest)
{
	//Debug("ProcIncomingRute gettting possible routes");
	int iRetHighwayIndex = UNKNOWNVAL;
	int iCalcVal = (iFinalTrunk*IFB_HIGHWAY_BASE) + iFinalDest;
	// 1. does final dest already have highway into it - and does this trace back to start ring - and in use for ifb / mixer - ie route already exists
	bncs_stringlist ssl_retHighways = bncs_stringlist(FindHighwayAlreadyRoutedToIFBMixerDestination(iFinalTrunk, iFinalDest, iStartTrunk ), ',');
	if (ssl_retHighways.count() > 0)  {
		Debug("FindHighwayForIFBMixerOutputUse - dest %d %d uses highways %s already has route ", iFinalTrunk, iFinalDest, LPCSTR(ssl_retHighways.toString(',')));
		// check working entries -- add entry if not in list already down chain of highways
		int iH = 0;
		for (iH = 0; iH < ssl_retHighways.count(); iH++) {
			CHighwayRecord* pHigh = GetHighwayRecord(ssl_retHighways[iH].toInt());
			if (pHigh) pHigh->addWorkingMode(iWhichMode, iWhichIFBMixer);
		}
		iH = ssl_retHighways.count()-1;
		return ssl_retHighways[iH].toInt();	
	}

	// else dest does not have valid highway route - so establish
	ssl_CalculatedRouteHops.clear();  // reset global for possible routes listing
	int iPossibleRoutes = CalculateHighwayRouteHops(iStartTrunk, iFinalTrunk);
	BOOL bFoundRoute = FALSE;
	if (iPossibleRoutes > 0) {
		int iRoute = 0;
		do {
			ssl_PotentialRouteCommands.clear();
			// process thru ssl_route - which is a list of ring hops - if successful make rcs then exit method
			iRetHighwayIndex = FindIFBMixerOutputHighwayRoute(iWhichIFBMixer, iWhichMode, iFinalTrunk, iFinalDest, ssl_CalculatedRouteHops[iRoute]);
			if (iRetHighwayIndex>0) {
				// process thru list making actual grd routes and execute rc commands
				for (int iC = 0; iC < ssl_PotentialRouteCommands.count(); iC++) AddCommandToQue(LPCSTR(ssl_PotentialRouteCommands[iC]), ROUTERCOMMAND, 0, 0, 0);
				ProcessNextCommand(iNumberRiedelRings);
				bFoundRoute = TRUE;
				return iRetHighwayIndex;
			}
			iRoute++;
		} while ((!bFoundRoute) && (iRoute < iPossibleRoutes));
	}
	// ALL possible routes have failed if this point reached
	Debug("FindHwayForIFBOutput -WARNING- NO ROUTES FOUND for ifb %d  to %d dest %d ", iWhichIFBMixer, iFinalTrunk, iFinalDest);
	return iRetHighwayIndex;
}


int DetermineInputHighwayForIFBMixerGivenRingPort(int iWhichIFBMixer, int iWhichMode, int iSourceTrunk, int iSourcePort, int *iPortonIFBRing)
{
	*iPortonIFBRing = 0;
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iWhichIFBMixer);
	CRiedel_IFBs* pRevsIFB = GetRiedel_IFBs_Record(iWhichIFBMixer);
	if (pRMstrIFB&&pRevsIFB) {
		// get inputports - are any highways -- does this highway lead back to src trunk+port
		bncs_stringlist ssl = bncs_stringlist(pRevsIFB->getIFB_Inputs(), ',');
		for (int ii = 0; ii < ssl.count(); ii++) {
			CHighwayRecord* pHigh = GetHighwayRecordByToTrunkAndSrce(pRMstrIFB->getAssociatedTrunkAddress(), ssl[ii].toInt(), FALSE);
			if (pHigh) {
				// is this highway linked to this ifb and trace back to src trunk and port ?
				if ((pHigh->getHighwayState() == HIGHWAY_IFBMXRIN) && (pHigh->getNumberWorkingModeEntries()>0)) {
					// now go thru the entries until a match 
					//  the "allocated index should match the given src  ???
					iTraceCount = 0;
					ssl_Calc_Trace = bncs_stringlist("", '|');  // clear global var - populated in TraceThePrimarySource
					int iTracedTrunk = pHigh->getToTrunkAddr();
					int iTracedGrd = pHigh->getToRouter();
					int iTracedSrc = pHigh->getToSource();
					TraceThePrimarySource(&iTracedTrunk, &iTracedGrd, &iTracedSrc);
					if ((iTracedTrunk == iSourceTrunk) && (iTracedSrc == iSourcePort)) {
						*iPortonIFBRing = ssl[ii].toInt();
						return pHigh->getRecordIndex();
					}
				}
			}
		}
	}
	// no known highway
	return UNKNOWNVAL;
}


int FindIFBMixerInputHighwayRoute(int iWhichIFBMixer, int iWhichMode, int iStartRing, int iStartSrce, int iFinalTrunk, bncs_string ss_Rings)
{
	int iSrcToRoute = iStartSrce;
	int iAllocIndex = (IFB_HIGHWAY_BASE*iStartRing) + iStartSrce;   // sets up a Allocated index that is stored in highway 
	int iFinalHighwayForIFBInput = UNKNOWNVAL;
	ssl_PotentialRouteCommands.clear();
	bncs_stringlist ssl_route = bncs_stringlist(ss_Rings, ',');
	int iNumHops = ssl_route.count() - 1;
	for (int iHop = 0; iHop < (ssl_route.count() - 1); iHop++) {
		CRiedel_Rings* pHopOutRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iHop]);
		CRiedel_Rings* pHopInRing = GetRiedel_Rings_By_TrunkAddress(ssl_route[iHop + 1]);
		if (pHopOutRing&&pHopInRing) {
			int iWhichHigh = 0;
			// 3. is source already on a highway to next ring in sequence- then use that highway
			iWhichHigh = FindSourceAlreadyOnHighway(&iSrcToRoute, pHopOutRing, pHopInRing);
			if (iWhichHigh <= 0) {
				// else see if a free highway from those not in use - is start ring and srce already on highway to this ring
				iWhichHigh = FindNextFreeHighway(&iSrcToRoute, pHopOutRing, pHopInRing, iWhichIFBMixer, iWhichMode, iAllocIndex);
				if (iWhichHigh <= 0) {
					Debug("FindIFBMixerInputHighwayRoute -- no Route %s available- from r:%d %d ifb:%d to r:%d", LPCSTR(ss_Rings), ssl_route[0].toInt(), iSrcToRoute, iWhichIFBMixer, ssl_route[iNumHops].toInt());
					return UNKNOWNVAL;   // exit as no route on this sequence of hops
				}
				iFinalHighwayForIFBInput = iWhichHigh; // assign var here to send back - MUST know the LAST highway index to get final source for input into ifb
			}
			else {
				Debug("FindSucessfulRute thru 3 - srce already on highway");
				iFinalHighwayForIFBInput = iWhichHigh; // assign var here to send back - MUST know the LAST highway index to get final source for input into ifb
			}
		}
		else {
			return UNKNOWNVAL;   // exit immediately - error in classes
		}
	} // for iHop

	// successful to this point - no final grd rc to complete routing in this case - so return highway index
	//
	return iFinalHighwayForIFBInput;
}

void AddWorkingModeForIFBMixers(int iHighwayIndex, int iWhichMode, int iWhichIFBMixer)
{
	int iHigh = iHighwayIndex;
	BOOL bContinue = TRUE;
	int iLoopCount = 20;
	// add working entries along discovered highway chain
	while ((bContinue) && (iLoopCount>0)) {
		iLoopCount--;
		CHighwayRecord* pHigh = GetHighwayRecord(iHigh);
		if (pHigh) {
			pHigh->addWorkingMode(iWhichMode, iWhichIFBMixer);
			// get routed source to this highway from dest - is that a highway
			CRiedel_GRDs* pGrd = GetRiedel_GRD_By_TrunkAddr(pHigh->getFromTrunkAddr());
			if (pGrd) {
				int iSrce = pGrd->getRoutedSourceForDestination(pHigh->getFromDestination());
				CHighwayRecord* pNext = GetHighwayRecordByToRtrSrce(pHigh->getFromRouter(), iSrce);
				if (pNext) {
					iHigh = pNext->getRecordIndex();
				}
				else bContinue = FALSE;
			}
			else bContinue = FALSE;
		}
	} // while bCont

}


int FindHighwayForIFBMixerInputUse(int iWhichIFBMixer, int iWhichMode, int iFinalTrunk, int iStartTrunk, int iStartSource)
{
	//Debug("ProcIncomingRute gettting possible routes");
	ssl_CalculatedRouteHops.clear();  // reset global for possible routes listing
	int iPossibleRoutes = CalculateHighwayRouteHops(iStartTrunk, iFinalTrunk);
	BOOL bFoundRoute = FALSE;
	if (iPossibleRoutes > 0) {
		int iRoute = 0;
		do {
			int iRetHighwayIndex = UNKNOWNVAL;
			ssl_PotentialRouteCommands.clear();
			// process thru ssl_route - which is a list of ring hops - if successful make rcs then exit method
			// 1. is src already routed to another dest on same final rtr -- ie route already exists
			iRetHighwayIndex = FindSourceAlreadyRoutedToFinalRouter(iStartTrunk, iStartSource, iFinalTrunk, IFB_HIGHWAY_BASE);
			if ((iRetHighwayIndex > 0) && (iRetHighwayIndex <= iNumberBNCSHighways)) {
				// Debug("FindHighwayForIFBMixerInputUse - highway %d already has route ", iRetHighwayIndex);#
				AddWorkingModeForIFBMixers(iRetHighwayIndex, iWhichMode, iWhichIFBMixer);
				bFoundRoute = TRUE;
				return iRetHighwayIndex;
			}
			else {
				// not routed already
				iRetHighwayIndex = FindIFBMixerInputHighwayRoute(iWhichIFBMixer, iWhichMode, iStartTrunk, iStartSource, iFinalTrunk, ssl_CalculatedRouteHops[iRoute]);
				if ((iRetHighwayIndex > 0) && (iRetHighwayIndex <= iNumberBNCSHighways)) {
					// process thru list making actual grd routes and execute rc commands
					AddWorkingModeForIFBMixers(iRetHighwayIndex, iWhichMode, iWhichIFBMixer);
					for (int iC = 0; iC < ssl_PotentialRouteCommands.count(); iC++)
						AddCommandToQue(LPCSTR(ssl_PotentialRouteCommands[iC]), ROUTERCOMMAND, 0, 0, 0);
					ProcessNextCommand(iNumberRiedelRings);
					bFoundRoute = TRUE;
					return iRetHighwayIndex;
				}
			}
			iRoute++;
		} while ((!bFoundRoute) && (iRoute < iPossibleRoutes));
	}
	// ALL possible routes have failed if this point reached
	Debug("FindHighwayForIFBInputUse -WARNING- NO ROUTES for ifb/mxr %d  from %d src %d to ifb ring %d", iWhichIFBMixer, iStartTrunk, iStartSource, iFinalTrunk);
	return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////


CHighwayRecord* DetermineHighwayInIFBMixerOutputRev(bncs_string ssOutputsRev, int iWhichIFBMixer, int iIFBTrunkAddress, int iDestTrunk, int iDestPort)
{
	// 
	bncs_stringlist ssl = bncs_stringlist(ssOutputsRev, ',');
	for (int indx = 0; indx < ssl.count(); indx++) {
		CHighwayRecord* pHigh = GetHighwayRecordByFromTrunkAndDest(iIFBTrunkAddress, ssl[indx].toInt(), FALSE);
		if (pHigh) {
			int iCalcVal = (iDestTrunk*IFB_HIGHWAY_BASE) + iDestPort;
			if ((pHigh->getNumberWorkingModeEntries()>0) && (pHigh->getHighwayAllocatedIndex() == iCalcVal)) {
				// must / should be this one 
				return pHigh;
			}
		}
	}
	return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////
//
//    MIXER command processing functions
//

int GetIndexForMixerInputRingPort(int ssFindStr, bncs_string ssListStr)
{
	int iIndex = UNKNOWNVAL;
	bncs_stringlist ssl = bncs_stringlist(ssListStr, ',');
	// added complication of ":db" in each list string entry
	for (int iI = 0; iI < ssl.count(); iI++) {
		bncs_stringlist ssll = bncs_stringlist(ssl[iI], ':');
		if (ssll[0] == ssFindStr) {
			return iI;   // ring/port entry found in string
		}
	}
	return iIndex;
}

int GetInputMixerPortFromIndex(int iIndex, bncs_string ssMixerRevs)
{
	// iIndex is zero based
	int iPort = 0;
	bncs_stringlist ssl = bncs_stringlist(ssMixerRevs, ',');
	// added complication of ":db" in each list string entry
	if (iIndex < ssl.count()) {
		bncs_stringlist ssll = bncs_stringlist(ssl[iIndex], ':');
		iPort = ssll[0].toInt();   // port entry found in string
	}
	return iPort;
}

BOOL IsPortForMixerAlreadyInUse( bncs_string ssCurrRMstrRev, bncs_string ssNewParams, int iPortVal, int iInOrOut )
{
	// is ring.port already in input or output rev - if so return true else false
	// zero port could already be there for other ports - but not applicable in this function - so false
	if (iPortVal == 0) return FALSE;
	// get appropriate side of curr rev 
	bncs_stringlist sslrev = bncs_stringlist(ssCurrRMstrRev, '|');
	if ((sslrev.count() > 1)&&((iInOrOut==0)||(iInOrOut==1))) {
		if (sslrev[iInOrOut].find(ssNewParams) >= 0) {
			return TRUE;
		}
	}
	return FALSE;
}

void ProcessMIXERInputCommand(int iMixerTrunkAddress, int iWhichMixer, bncs_string ssCmd, bncs_string ssParams)
{
	CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iMixerTrunkAddress);
	if (pRing) {
		//
		int iWhichMxrInput = bncs_string(ssCmd.right(1)).toInt();    // less than 10 inputs defined for mixers
		Debug("ProcMixerINput command  input chosen %d", iWhichMxrInput);
		int iMixerRecIndex = ((pRing->m_iRecordIndex - 1)*MAX_MIXERS_LOGIC) + iWhichMixer;
		CRingMaster_MIXER* pRMstrMIX = GetRingMaster_MIXER_Record(iMixerRecIndex);
		if (pRMstrMIX) {

			// check for vol:xx.xx -- no port changes involved 
			if (ssParams.find("vol:") >= 0) {
				// just pass command straight thru to appropriate driver
				QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(ssParams));
				return;    // as complete now
			}

			// standard input port params 
			BOOL bSndChnl = FALSE;
			int iTrunk = 0, iPort = 0;
			bncs_string ssDb = GetTrunkAndPortAnddBFromString(ssParams, &iTrunk, &iPort, &bSndChnl);
			if (iTrunk == 0) iTrunk = iMixerTrunkAddress;
			if ((iWhichMxrInput > 0) && (iTrunk > 0) && (iPort >= 0)) {
				// check for change in port else no point in resending existing port and messing with highways unneccessarily ???
				if (!IsPortForMixerAlreadyInUse(pRMstrMIX->getRingMasterRevertive(), bncs_string("%1.%2:").arg(iTrunk).arg(iPort), iPort, 0)) {
					CRiedel_Mixers* pRevMix = GetRiedel_Mixers_Record(iMixerRecIndex);
					if (pRevMix) {
						// check CURRENT port -- is it a highway -- then clear that one, then process NEW port 
						bncs_stringlist ssl_Inns = pRevMix->getMixer_Inputs();
						if (iWhichMxrInput <= ssl_Inns.count()) {
							BOOL bSndChnl = FALSE;
							int iCurrPort = 0;
							bncs_string ssDb = GetPortAnddBFromString(ssl_Inns[iWhichMxrInput - 1], &iCurrPort, &bSndChnl);
							// is this port on trunk for mixer a highway out for this ring
							CHighwayRecord* pHigh = GetHighwayRecordByFromTrunkAndDest(iMixerTrunkAddress, iCurrPort, FALSE);
							if (pHigh) {
								if ((pHigh->getHighwayState() == HIGHWAY_IFBMXRIN) && (pHigh->getHighwayAllocatedIndex() > IFB_HIGHWAY_BASE)) {
									pHigh->removeWorkingMode(MODE_MIXERIN, iMixerRecIndex);
									if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
								}
								else
									Debug("ProcMixerInputCmd - highway from %d %d not in IFB/MIXER use as expected", iMixerTrunkAddress, iCurrPort);   //  clear anyway - or too much ???
							}
						}
					}
					// now NEW PORT -- same trunk or from different one - thus requiring highway
					if (iTrunk == iMixerTrunkAddress) {
						// same trunk - just add to output
						QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2:%3").arg(ssCmd).arg(iPort).arg(ssDb));
					}
					else {
						// requires highway -- 
						int iFirstHighwayIndex = FindHighwayForIFBMixerInputUse(iMixerRecIndex, MODE_MIXERIN, iMixerTrunkAddress, iTrunk, iPort);
						CHighwayRecord* pHigh = GetHighwayRecord(iFirstHighwayIndex);
						if (pHigh)
							QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2:%3").arg(ssCmd).arg(pHigh->getToSource()).arg(ssDb));
						else
							Debug("ProcessMIXERInputCommand -ERROR-ADD- NO ROUTE/Highway FOR MIXER %d output %d %d ", iMixerRecIndex, iTrunk, iPort);
					}
				}
				else {
					// no diff in trunk.port -- but could be a change in dB value --
					if (!IsPortForMixerAlreadyInUse(pRMstrMIX->getRingMasterRevertive(), ssParams, iPort, 0)) {
						// we know port has not changed so must be db change - just send vol change 
						if (pRing) QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=vol:%2").arg(ssCmd).arg(ssDb));
					}
				}
			}
		}
		else
			Debug("ProcMixerInputCmd - no ringmaster rec %d for mixer %d trunk %d from  %s", iMixerRecIndex, iWhichMixer, iMixerTrunkAddress, LPCSTR(ssParams));
	}
	else
		Debug("ProcMixerINput no ring record ");
}


void ProcessMIXERRemoveCommand(int iMixerTrunkAddress, int iWhichMixer, bncs_string ssCmd, bncs_string ssParams, int iLoopCheck)
{

	// determine type of removal
	CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iMixerTrunkAddress);
	if (pRing) {

		BOOL bSndChnl = FALSE;
		int iTrunk = 0, iPort = 0;
		GetTrunkAndPortFromString(ssParams, &iTrunk, &iPort, &bSndChnl);
		if (iTrunk == 0) iTrunk = iMixerTrunkAddress;
		int iMixerRecIndex = ((pRing->m_iRecordIndex - 1)*MAX_MIXERS_LOGIC) + iWhichMixer;
		CRingMaster_MIXER* pRMstrMIX = GetRingMaster_MIXER_Record(iMixerRecIndex);
		CRiedel_Mixers* pRevMix = GetRiedel_Mixers_Record(iMixerRecIndex);

		if ((pRMstrMIX&&pRevMix) && (iTrunk > 0) && (iPort > 0)) {
				
			// get extras infodriver for trunk ring
			if (ssCmd.find("IN") >= 0) {
				if (iTrunk == iMixerTrunkAddress) {  // local port just remove
					QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(iPort));
				}
				else { // check for highway
					// find which input from ringmaster rev
					bncs_stringlist ssl_RmstrRev = bncs_stringlist(pRMstrMIX->getRingMasterRevertive(), '|');
					// needs special parsing to get ring.port from inputs as they include a db value too
					int iWhichMxrInput = GetIndexForMixerInputRingPort(ssParams, pRMstrMIX->getRingMasterRevertive());
					if (iWhichMxrInput >= 0) {
						//  special parsing due to db vol part
						int iCurrPort = GetInputMixerPortFromIndex(iWhichMxrInput, pRevMix->getMixer_Inputs());
						// is this port a highway out for this trunk, sole user ? - then clear
						CHighwayRecord* pHigh = GetHighwayRecordByToTrunkAndSrce(iTrunk, iCurrPort, FALSE);
						if (pHigh) {
							if ((pHigh->getHighwayState() == HIGHWAY_IFBMXRIN) && (pHigh->getHighwayAllocatedIndex() > IFB_HIGHWAY_BASE)) {
								if (pHigh->getNumberWorkingModeEntries() == 1) {
									// sole user - thus clear --- but could check for mixer mode and this mixer ?
									QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(iCurrPort));
								}
								else {
									Debug("ProcMixerRemoveCmd -info- highway from %d %d IFB/MIXER - not sole user tidying users", iTrunk, iCurrPort);
									pHigh->removeWorkingMode(MODE_MIXERIN, iMixerRecIndex);
								}
							}
							else {
								Debug("ProcMixerRemoveCmd - highway from %d %d not in IFB/MIXER use as expected", iTrunk, iCurrPort);   //  clear anyway - or too much ???
								QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(iCurrPort));
							}
						}
					}
					else
						Debug("ProcMixerRemoveCmd -error- no input port for index %d from mixer %d trunk %d revs %s ", iWhichMxrInput, iMixerRecIndex, iTrunk, LPCSTR(pRMstrMIX->getRingMasterRevertive()));
				}
			}
			else if (ssCmd.find("OUT") >= 0) {
				//
				if (iTrunk == iMixerTrunkAddress) {  // local port just remove
					bncs_string sstr = bncs_string("%1").arg(iPort);
					if (bSndChnl) sstr.append("s");
					QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(sstr));
				}
				else { // check for highway
					// find which output from ringmaster rev
					int iWhichMxrOutput = UNKNOWNVAL;
					bncs_stringlist ssl_RmstrRev = bncs_stringlist(pRMstrMIX->getRingMasterRevertive(), '|');
					if (ssl_RmstrRev.count() > 1) iWhichMxrOutput = ssl_RmstrRev[1].find(ssParams);
					if (iWhichMxrOutput >= 0) {
						bncs_stringlist ssl_Outs = pRevMix->getMixer_Outputs();
						int iCurrPort = ssl_Outs[iWhichMxrOutput].toInt();
						// is this port a highway out for this trunk, sole user ? - then clear
						CHighwayRecord* pHigh = GetHighwayRecordByFromTrunkAndDest(iTrunk, iCurrPort, FALSE);
						if (pHigh) {
							if ((pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) && (pHigh->getHighwayAllocatedIndex() > IFB_HIGHWAY_BASE)) {
								pHigh->removeWorkingMode(MODE_MIXEROUT, iMixerRecIndex);
								if (pHigh->getNumberWorkingModeEntries() == 0) {
									// was sole user - thus clear --- but could check for mixer mode and this mixer ?
									ParkHighway(pHigh->getRecordIndex());
									QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(iCurrPort));
								}
							}
							else {
								Debug("ProcMixerRemoveCmd - highway from %d %d not in IFB/MIXER use as expected", iTrunk, iCurrPort);   //  clear anyway - or too much ???
								QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(iCurrPort));
							}
						}
						else {
							// not a highway for some reason ( error ?? ) - but clear anyway
							QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(iCurrPort));
							Debug("ProcMixerRemoveCmd -warn- unexpected output port for index %d from mixer %d trunk %d revs %s ", iWhichMxrOutput, iMixerRecIndex, iTrunk, LPCSTR(pRevMix->getMixer_Outputs()));
						}
					}
					else
						Debug("ProcMixerRemoveCmd -error- no output port for index %d from mixer %d trunk %d revs %s ", iWhichMxrOutput, iMixerRecIndex, iTrunk, LPCSTR(pRMstrMIX->getRingMasterRevertive()));
				}
			}
			else {
				// just remove whereever found
				if (iTrunk == iMixerTrunkAddress) {  // local port just remove
					bncs_string sstr = bncs_string("%1").arg(iPort);
					if (bSndChnl) sstr.append("s");
					QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(sstr));
				}
				else { // check as remove from both ins and outs could involve highways on both sides
					if (iLoopCheck == 0) { // should not go thru here again if loop check >0
						// call this function but redefine the command from just REMOVE to REMOVEIN and REMOVEOUT
						ProcessMIXERRemoveCommand(iMixerTrunkAddress, iWhichMixer, bncs_string("&REMOVEIN"), ssParams, 1);
						ProcessMIXERRemoveCommand(iMixerTrunkAddress, iWhichMixer, bncs_string("&REMOVEOUT"), ssParams, 2);
					}
					else
						Debug("ProcMixerRemoveCmd -ERROR-calling REMOVE with loop check>0, value passed was %d ", iLoopCheck);
				}
			}
		}
		else
			Debug("ProcMixerRemoveCmd - no Mixer Rev record for trunk %d for mixer %d or port %d ", iTrunk, iMixerRecIndex, iPort);
	}
	else
		Debug("ProcMixerRemoveCmd - no ring or Mixer %d record for trunk %d from  %s", iWhichMixer, iMixerTrunkAddress, LPCSTR(ssParams));
}


void ProcessMIXEROutputCommand(int iMixerTrunkAddress, int iWhichMixer, bncs_string ssCmd, bncs_string ssParams)
{
	CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iMixerTrunkAddress);
	if (pRing) {

		BOOL bSndChnl = FALSE;
		int iTrunk = 0, iPort = 0;
		int iWhichMxrOutput = bncs_string(ssCmd.right(1)).toInt();    // less than 10 outputs defined for mixers
		Debug("ProcMixerOUTput command  output chosen %d from %s", iWhichMxrOutput, LPCSTR(ssCmd));
		GetTrunkAndPortFromString(ssParams, &iTrunk, &iPort, &bSndChnl);
		if (iTrunk == 0) iTrunk = iMixerTrunkAddress;

		int iMixerRecIndex = ((pRing->m_iRecordIndex - 1)*MAX_MIXERS_LOGIC) + iWhichMixer;
		CRingMaster_MIXER* pRMstrMIX = GetRingMaster_MIXER_Record(iMixerRecIndex);
		CRiedel_Mixers* pRevMix = GetRiedel_Mixers_Record(iMixerRecIndex);
			
		if ((pRMstrMIX&&pRevMix) && (iWhichMxrOutput > 0) && (iTrunk > 0) && (iPort >= 0)) {

			// check for change else no point in resending existing port and messing with highways unneccessarily ???
			if (!IsPortForMixerAlreadyInUse(pRMstrMIX->getRingMasterRevertive(), ssParams, iPort, 1)) {
				// check CURRENT port -- is it a highway -- then clear that one, then process NEW port 
				bncs_stringlist ssl_Outs = pRevMix->getMixer_Outputs();
				if (iWhichMxrOutput <= ssl_Outs.count()) {
					int iCurrPort = ssl_Outs[iWhichMxrOutput - 1].toInt();
					// is this port on trunk for mixer a highway out for this ring
					CHighwayRecord* pHigh = GetHighwayRecordByFromTrunkAndDest(iMixerTrunkAddress, iCurrPort, FALSE);
					if (pHigh) {
						if ((pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) && (pHigh->getHighwayAllocatedIndex() > IFB_HIGHWAY_BASE)) {
							pHigh->removeWorkingMode(MODE_MIXEROUT, iMixerRecIndex);
							if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
						}
						else
							Debug("ProcMixerOutputCmd - highway from %d %d not in MIXER use as expected", iMixerTrunkAddress, iCurrPort);   //  clear anyway - or too much ???
					}
				}

				// now NEW PORT -- same trunk or from different one - thus requiring highway
				if (iTrunk == iMixerTrunkAddress) {
					// same trunk - just add to output
					bncs_string sstr = bncs_string("%1").arg(iPort);
					if (bSndChnl) sstr.append("s");
					QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(sstr));
				}
				else {
					// requires highway -- 
					int iFirstHighwayIndex = FindHighwayForIFBMixerOutputUse(iWhichMixer, MODE_MIXEROUT, iMixerTrunkAddress, iTrunk, iPort);
					CHighwayRecord* pHigh = GetHighwayRecord(iFirstHighwayIndex);
					if (pHigh)
						QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, bncs_string("%1=%2").arg(ssCmd).arg(pHigh->getFromDestination()));
					else
						Debug("ProcessMIXEROutputCommand -ERROR-ADD- NO ROUTE/Highway FOR MIXER %d output %d %d ", iMixerRecIndex, iTrunk, iPort);
				}

			}
			else
				Debug("ProcMixerOutputCmd - trunk %d port %d already in use for mixer %d from  %s", iTrunk, iPort, iMixerRecIndex, LPCSTR(ssParams));
		}
		else
			Debug("ProcMixerOutputCmd INVALID output in command so ignored");
	}
	else
		Debug("ProcMixerOutputCmd - no ring rec for trunk %d from  %s", iMixerTrunkAddress, ssParams);
}


void ProcessMIXERCommand(int iWhichInfodrv, int iWhichSlot, bncs_string ssSlot)
{
	// process shortcut commands or full mixer command from packager -- may require highways if ports are from rings other than ifb ring
	// search for &<cmd> -- for shortcut command or   '|' to indicate a full command
	// which ifb infodriver will determine ring index  --- which ring determines, trunk address; ifb index into maps
	//
	int iRingIndex = iWhichInfodrv - i_info_IFBs_Start + 1;
	if ((iRingIndex>0) && (iRingIndex <= iNumberRiedelRings) && (iRingIndex < MAX_RIEDEL_RINGS)) {
		//
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRingIndex);
		if (pRing) {
			if (iNextManagementProcessing < 3) iNextManagementProcessing++; // give time for revs to come back from cmds issued
			int iWhichMixer = iWhichSlot - RIEDELDRIVER_MIXERS_INFO_START;
			int iMixerRecordIndex = ((iRingIndex - 1)*MAX_MIXERS_LOGIC) + iWhichMixer;
			int iMixerTrunkAddress = pRing->m_iTrunkAddress;
			//
			CRiedel_Mixers* pRevsMixer = GetRiedel_Mixers_Record(iMixerRecordIndex);
			CRingMaster_MIXER* pRMstrMIX = GetRingMaster_MIXER_Record(iMixerRecordIndex);
			if (pRMstrMIX&&pRevsMixer) {
				//
				Debug("ProcessMIXERCommand Using Trunk %d for CMD to be processed %s", iMixerTrunkAddress, LPCSTR(ssSlot));
				//
				bncs_string ssleft = "", ssCmd = "", ssParams = "";
				ssSlot.split('=', ssleft, ssParams);
				ssCmd = ssleft.upper();
				if ((ssSlot.find("&") == 0) && (ssSlot.find("=") > 0)) {
					// shortcut command - extract parameters
					if (ssCmd.find("INPUT") >= 0) {
						ProcessMIXERInputCommand(iMixerTrunkAddress, iWhichMixer, ssCmd, ssParams);
					}
					else if (ssCmd.find("OUTPUT") >= 0) { // output command 
						ProcessMIXEROutputCommand(iMixerTrunkAddress, iWhichMixer, ssCmd, ssParams);
					}
					else if (ssCmd.find("REMOVE") >= 0) { // remove port command ( either in, out or wherever found )
						ProcessMIXERRemoveCommand(iMixerTrunkAddress, iWhichMixer, ssCmd, ssParams, 0);
					}
				}
				else if (ssSlot.find("|") >= 0) {
					// full command - split into inputs and outputs - see what is different ( watch for just be a change in dB level for an input )

					// split up full ifb command
					if (ssSlot == "|") ssSlot = "0.0:0.0|0";
					bncs_stringlist ssl_NewCmd_list = bncs_stringlist(ssSlot, '|');
					if ((pRing&&pRMstrMIX&&pRevsMixer) && (ssl_NewCmd_list.count() > 1)) {

						bncs_stringlist ssl_NewInputs = bncs_stringlist(ssl_NewCmd_list[0], ',');
						bncs_stringlist ssl_NewOutputs = bncs_stringlist(ssl_NewCmd_list[1], ',');
						// clear command lists - process inputs, process outputs - then build final command for mixer command to Riedel

						// get current revertives for inputs and outputs
						bncs_stringlist ssl_CurrInns = bncs_stringlist("", ',');
						bncs_stringlist ssl_CurrOuts = bncs_stringlist("", ',');
						bncs_stringlist ssl_MIXER_Rev = bncs_stringlist(pRMstrMIX->getRingMasterRevertive(), '|');
						if (ssl_MIXER_Rev.count() > 0) ssl_CurrInns = bncs_stringlist(ssl_MIXER_Rev[0], ',');
						if (ssl_MIXER_Rev.count() > 1) ssl_CurrOuts = bncs_stringlist(ssl_MIXER_Rev[1], ',');

						// also get curr riedel rev for inputs and outputs for mixer
						bncs_stringlist ssl_RevInns = bncs_stringlist(pRevsMixer->getMixer_Inputs(), ',');
						bncs_stringlist ssl_RevOuts = bncs_stringlist(pRevsMixer->getMixer_Outputs(), ',');

						ClearDataLists();
						// INPUTS - check curr against new - to find differences - of port, of dB 
						for (int iInput = 0; iInput < ssl_CurrInns.count(); iInput++) {
							// check each trunk.port and dB value in curr to see if no longer in new list -- remove highways if no longer required for new inputs
							BOOL bCurrSndChnl = FALSE;
							int iCurrTrunk = 0, iCurrPort = 0;
							bncs_string ssCurrDb = GetTrunkAndPortAnddBFromString(ssl_CurrInns[iInput], &iCurrTrunk, &iCurrPort, &bCurrSndChnl);
							BOOL bNewSndChnl = FALSE;
							int iNewTrunk = 0, iNewPort = 0;
							bncs_string ssNewDb = "";
							if (iInput<ssl_NewInputs.count()) ssNewDb = GetTrunkAndPortAnddBFromString(ssl_NewInputs[iInput], &iNewTrunk, &iNewPort, &bNewSndChnl);
							if ((iCurrTrunk != iNewTrunk) || (iCurrPort != iNewPort)) {
								//some change in trunk and or port for this specific mixer input- so is port in Riedel rev a highway - if so clear for this mixer use
								// get relevant port in 
								BOOL bRevSndChnl = FALSE;
								int iRevPort = 0;
								bncs_string ssRevdB = "";
								if (iInput < ssl_RevInns.count()) ssRevdB = GetPortAnddBFromString(ssl_RevInns[iInput], &iRevPort, &bRevSndChnl);
								CHighwayRecord* pHigh = GetHighwayRecordByToTrunkAndSrce(iMixerTrunkAddress, iRevPort, FALSE);
								if (pHigh) {
									if ((pHigh->getHighwayState() == HIGHWAY_IFBMXRIN) && (pHigh->getHighwayAllocatedIndex() > IFB_HIGHWAY_BASE)) {
										pHigh->removeWorkingMode(MODE_MIXERIN, iMixerRecordIndex);
										if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
									}
									else
										Debug("ProcMixerCommand - input highway from %d %d not in IFB/MIXER use as expected", iMixerTrunkAddress, iRevPort);   //  clear anyway - or too much ???
								}
							}
						} // for iInput

						// now go thru NEW inputs and find changes for ports and or dB values -- adding inputs and  datalist as we go
						for (int iInputNew = 0; iInputNew < ssl_NewInputs.count(); iInputNew++) {
							// check each trunk.port and dB value in new to see if highway required and or change dB
							BOOL bNewSndChnl = FALSE;
							int iNewTrunk = 0, iNewPort = 0;
							bncs_string ssNewDb = GetTrunkAndPortAnddBFromString(ssl_NewInputs[iInputNew], &iNewTrunk, &iNewPort, &bNewSndChnl);
							BOOL bCurrSndChnl = FALSE;
							int iCurrTrunk = 0, iCurrPort = 0;
							bncs_string ssCurrDb = "";
							if (iInputNew<ssl_CurrInns.count()) ssCurrDb = GetTrunkAndPortAnddBFromString(ssl_CurrInns[iInputNew], &iCurrTrunk, &iCurrPort, &bCurrSndChnl);
							// set up default data string for input 
							bncs_string ssData = bncs_string("%1:%2").arg(iNewPort).arg(ssNewDb);
							//
							if ((iNewTrunk>0) && (iNewPort>0)) {
								if (iNewTrunk != iMixerTrunkAddress) {
									// if new different from current - highway may be required - if same as current then 
									if ((iNewTrunk == iCurrTrunk) && (iNewPort == iCurrPort)) {
										// same as current -- so leave port as that of existing rev - handles case of retained existing highway or just local port
										BOOL bRevSndChnl = FALSE;
										int iRevPort = 0;
										bncs_string ssRevdB = "";
										if (iInputNew < ssl_RevInns.count()) ssRevdB = GetPortAnddBFromString(ssl_RevInns[iInputNew], &iRevPort, &bRevSndChnl);
										ssData = bncs_string("%1:%2").arg(iRevPort).arg(ssNewDb);
									}
									else {
										// some difference in trunk and or port for New input - and distant trunk - so requies a highway
										int iFirstHighwayIndex = FindHighwayForIFBMixerInputUse(iMixerRecordIndex, MODE_MIXERIN, iMixerTrunkAddress, iNewTrunk, iNewPort);
										CHighwayRecord* pHigh = GetHighwayRecord(iFirstHighwayIndex);
										if (pHigh)
											ssData = bncs_string("%1:%2").arg(pHigh->getToSource()).arg(ssNewDb);
										else {
											Debug("ProcessMIXERCommand -ERROR-INPUT- NO ROUTE/Highway FOR MIXER %d output %d %d ", iMixerRecordIndex, iNewTrunk, iNewPort);
											ssData = bncs_string("0:0.0");
										}
									}
								}
							}
							AddToDataLists_Stringlist(iMixerTrunkAddress, 1, ssData);
						}// for iInputNew

						// OUTPUTS - - check curr against new - to find differences - of port
						for (int iOutput = 0; iOutput < ssl_CurrOuts.count(); iOutput++) {
							// check each trunk.port value in curr to see if no longer in new list -- remove highways if no longer required for new outputs
							BOOL bCurrSndChnl = FALSE;
							int iCurrTrunk = 0, iCurrPort = 0;
							GetTrunkAndPortFromString(ssl_CurrOuts[iOutput], &iCurrTrunk, &iCurrPort, &bCurrSndChnl);
							BOOL bNewSndChnl = FALSE;
							int iNewTrunk = 0, iNewPort = 0;
							if (iOutput<ssl_NewOutputs.count()) GetTrunkAndPortFromString(ssl_NewOutputs[iOutput], &iNewTrunk, &iNewPort, &bNewSndChnl);
							if ((iCurrTrunk != iNewTrunk) || (iCurrPort != iNewPort)) {
								//some change in trunk and or port for this specific mixer input- so is port in Riedel rev a highway - if so clear for this mixer use
								// get relevant port in 
								int iRevPort = 0;
								if (iOutput < ssl_RevOuts.count()) iRevPort = ssl_RevOuts[iOutput].toInt();
								CHighwayRecord* pHigh = GetHighwayRecordByFromTrunkAndDest(iMixerTrunkAddress, iRevPort, FALSE);
								if (pHigh) {
									if ((pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) && (pHigh->getHighwayAllocatedIndex() > IFB_HIGHWAY_BASE)) {
										pHigh->removeWorkingMode(MODE_MIXEROUT, iMixerRecordIndex);
										if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
									}
									else
										Debug("ProcMixerCommand - output highway from %d %d not in IFB/MIXER use as expected", iMixerTrunkAddress, iRevPort);   //  clear anyway - or too much ???
								}
							}
						} // for iOutput

						// now go thru NEW outputs and find changes for ports -- adding outputs to  datalist as we go
						for (int iOutputNew = 0; iOutputNew < ssl_NewOutputs.count(); iOutputNew++) {
							// check each trunk.port and dB value in new to see if highway required and or change dB
							BOOL bNewSndChnl = FALSE;
							int iNewTrunk = 0, iNewPort = 0;
							GetTrunkAndPortFromString(ssl_NewOutputs[iOutputNew], &iNewTrunk, &iNewPort, &bNewSndChnl);
							BOOL bCurrSndChnl = FALSE;
							int iCurrTrunk = 0, iCurrPort = 0;
							if (iOutputNew<ssl_CurrInns.count()) GetTrunkAndPortFromString(ssl_CurrInns[iOutputNew], &iCurrTrunk, &iCurrPort, &bCurrSndChnl);
							// set up default data string for input 
							bncs_string ssData = bncs_string("%1").arg(iNewPort);
							if (bNewSndChnl) ssData.append("s");
							//
							if ((iNewTrunk>0) && (iNewPort>0)) {
								if (iNewTrunk != iMixerTrunkAddress) {
									// if new different from current - highway may be required - if same as current then 
									if ((iNewTrunk == iCurrTrunk) && (iCurrPort == iNewPort)) {
										// same as current -- so leave port as that of existing rev - handles both case of retained existing highway or just local port
										int iRevPort = 0;
										if (iOutputNew < ssl_RevOuts.count()) iRevPort = ssl_RevOuts[iOutputNew].toInt();
										ssData = bncs_string("%1").arg(iRevPort);
									}
									else {
										// some difference in trunk and or port for New input - and distant trunk - so requies a highway
										int iFirstHighwayIndex = FindHighwayForIFBMixerOutputUse(iMixerRecordIndex, MODE_MIXEROUT, iMixerTrunkAddress, iNewTrunk, iNewPort);
										CHighwayRecord* pHigh = GetHighwayRecord(iFirstHighwayIndex);
										if (pHigh)
											ssData = bncs_string("%1").arg(pHigh->getFromDestination());
										else {
											Debug("ProcessMIXERCommand -ERROR-OUTPUT- NO ROUTE/Highway FOR MIXER %d output %d %d ", iMixerRecordIndex, iNewTrunk, iNewPort);
											ssData = bncs_string("0");
										}
									}
								}
							}
							AddToDataLists_Stringlist(iMixerTrunkAddress, 2, ssData);
						}// for iOutputNew

						// build and send command to Riedel
						CDataList* pData = GetDataList_By_Trunk(iMixerTrunkAddress);
						if (pData&&pRing) {
							// build a complete command for each ring -- and send a command, even |||, to each rings ifb driver / slot  
							bncs_string ssCmd = "";
							if (pData->getDataList(1).length() > 0) ssCmd.append(pData->getDataList(1));
							ssCmd.append("|");
							if (pData->getDataList(2).length() > 0) ssCmd.append(pData->getDataList(2));
							Debug("ProcessMixerCommand - full cmd out : %s", LPCSTR(ssCmd));
							// only send if different from last rececived revertive 
							bncs_string ssRev = "";
							if (pRevsMixer) ssRev = pRevsMixer->getRawRiedelRevertive();
							Debug("ProcessMixerCommand - rev cmd was : %s", LPCSTR(ssRev));
							if (ssRev != ssCmd) QueueCmdForRiedelDriver(pRing->iRing_Device_Extras, iWhichMixer + RIEDELDRIVER_MIXERS_INFO_START, ssCmd);
						}
						else
							Debug("ProcessMixerCommand - no ring or Datalist class from %d", iMixerTrunkAddress);
						//
					}
					else
						Debug("ProcessMixerCommand -error in classes or short command");
				}
				ProcessNextCommand(50);

			}
		else
			Debug("ProcessMIXERCmd - invalid mixer or revs class from mixer record index %d", iMixerRecordIndex );
		}
		else
			Debug("ProcessMIXERCmd - invalid ring class from ringIndex %d", iRingIndex);
	}
	else 
		Debug("ProcessMIXERCmd - invalid ring index from infodriver %d", iWhichInfodrv);

}


///////////////////////////////////////////////////////////////////////////////////////////////
//
//  Remote logic commands
//
void Process_REMOTE_LOGIC_Command(int iWhichInfodrv, int iWhichLogic, bncs_string ssSlot)
{
	bncs_stringlist ssl = bncs_stringlist(ssSlot, ':');
	if (ssl.count() == 2) {
		int iTrunkAddr = ssl[0].toInt();
		int iNewState = ssl[1].toInt();
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunkAddr);
		if (pRing) {
			char szCommand[MAX_AUTOBFFR_STRING] = "";
			if (iNewState > 0) {
				// activate logic function on ring
				wsprintf(szCommand, "IW %d '1' %d", pRing->iRing_Device_Extras, iWhichLogic);
				Debug("Process_REMOTE_LOGIC_Command enabled for logic %d on ring %d ", iWhichLogic, iTrunkAddr);
			}
			else {
				// deactivate logic function on ring
				wsprintf(szCommand, "IW %d '0' %d", pRing->iRing_Device_Extras, iWhichLogic);
				Debug("Process_REMOTE_LOGIC_Command disabled for logic %d on ring %d ", iWhichLogic, iTrunkAddr);
			}
			if ((ecCSIClient) && (iOverallTXRXModeStatus == IFMODE_TXRX)) ecCSIClient->txinfocmd(szCommand);
		}
		else
			Debug("Process_REMOTE_LOGIC_Command error - no ring from data %s", LPCSTR(ssSlot));
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////
//
//   IFB command processing functions
//

void ProcessIFBInputCommand(int iIFBRecordIndex, int iWhichIFB, bncs_string ssCmd, bncs_string ssParameters)
{
	int iIFBTrunkAddress = 0;
	CRiedel_IFBs* pRevsIFB = GetRiedel_IFBs_Record(iIFBRecordIndex);
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
	if ((pRMstrIFB) && (pRevsIFB)) {
		iIFBTrunkAddress = pRMstrIFB->getAssociatedTrunkAddress();
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iIFBTrunkAddress);
		if (pRing)  {
			// test for type of input cmd -- may be some tidying up to do of highways first 
			if (ssCmd.find("INPUTADD") >= 0) {
				// just adding -- is it on same trunk - then no highway required
				BOOL bSndChnl = FALSE;
				int iSourceTrunk = iIFBTrunkAddress, iSourcePort = 0;
				if (ssParameters.length() == 0) ssParameters = bncs_string("0");
				GetTrunkAndPortFromString(ssParameters, &iSourceTrunk, &iSourcePort, &bSndChnl);
				int iIFBDevice = GetReidel_IFB_Device_FromTrunk(iIFBTrunkAddress);  // ifb assoc to specific ring
				if ((iIFBDevice > 0) && (iSourcePort > 0)) {
					if (iSourceTrunk == iIFBTrunkAddress) {
						bncs_string ssData = bncs_string("%1").arg(iSourcePort);
						if (bSndChnl) ssData.append("s");
						QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&INPUTADD=%1").arg(ssData));
					}
					else {
						// source port is on another trunk -- requires a highway to be put into ifb as highway INPUT and routed into ring for IFB
						int iInHighwayIndex = FindHighwayForIFBMixerInputUse(iIFBRecordIndex, MODE_IFBINPUT, iIFBTrunkAddress, iSourceTrunk, iSourcePort);
						CHighwayRecord* pHigh = GetHighwayRecord(iInHighwayIndex);
						if (pHigh)
							QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&INPUTADD=%1").arg(pHigh->getToSource()));
						else {
							SetAlarmErrorMessage(iIFBTrunkAddress, bncs_string("ProcessIFB - INPUT NO HIGHWAY found for ifb %1 cmd %2 ").arg(iIFBRecordIndex).arg(ssCmd));
							Debug("ProcessIFBInputCommand -ERROR-ADD- NO ROUTE/Highway FOR IFB %d cmd %s ", iIFBRecordIndex, LPCSTR(ssCmd));
						}
					}
				}
			}
			else if (ssCmd.find("INPUTREMOVE") >= 0) {
				// just removing a port -- is on same trunk or does a highway need clearing  -- is source port a highway in...
				BOOL bSndChnl = FALSE;
				int iSourceTrunk = iIFBTrunkAddress, iSourcePort = 0;
				if (ssParameters.length() == 0) ssParameters = bncs_string("0");
				GetTrunkAndPortFromString(ssParameters, &iSourceTrunk, &iSourcePort, &bSndChnl);
				int iIFBDevice = GetReidel_IFB_Device_FromTrunk(iIFBTrunkAddress);
				// if using highway then remove the ToSource of highway feeding input + clear highway ??? / mm - else just remove local port
				if (iSourceTrunk == iIFBTrunkAddress) {
					if ((iIFBDevice > 0) && (iSourcePort > 0)) {
						bncs_string ssData = bncs_string("%1").arg(iSourcePort);
						if (bSndChnl) ssData.append("s");
						QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&INPUTREMOVE=%1").arg(ssData));
					}
				}
				else {
					// there should be a highway into ifb -- find it and get final source as this will be the port to remove from ifb
					// get current revertive for this ifb on its ring - check each input - if highway traced back to given ultimate src is that specified - checking number users; if =1 then clear down highway
					int iLocalPort = 0;
					int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iSourceTrunk, iSourcePort, &iLocalPort);
					if ((iHighwayUsed > 0) && (iLocalPort>0)) {
						CHighwayRecord* pHigh = GetHighwayRecord(iHighwayUsed);
						if (pHigh) {
							pHigh->removeWorkingMode(MODE_IFBINPUT, iIFBRecordIndex);
							if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
						}
						QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&INPUTREMOVE=%1").arg(iLocalPort));
					}
				}
			}
			else {
				// list of inputs command - need to determine differences !! - release highway if no longer required, acquire highways for new - but leave existing if they are still required
				// first clear existing ports and any highways but only if still NOT in new command
				bncs_stringlist ssl_params = bncs_stringlist(ssParameters, ',');
				bncs_stringlist ssl_tosend = bncs_stringlist("", ',');
				bncs_stringlist ssll = bncs_stringlist(pRMstrIFB->getRingMasterRevertive(), '|');
				bncs_string ssCurrInns = "";
				if (ssll.count() > 0) ssCurrInns = ssll[0];
				Debug("ProcIFBjustInputs - curr inns : %s  new params %s ", LPCSTR(ssCurrInns), LPCSTR(ssParameters));
				if (ssParameters != ssCurrInns) {
					// some diff -- so processing required
					//  determine diffs - as to what is now old, what is new, and what stays the same -- in bid to keep existing highways if still required
					bncs_stringlist ssl_CurrInns = bncs_stringlist(ssCurrInns, ',');
					for (int iOld = 0; iOld < ssl_CurrInns.count(); iOld++) {
						bncs_string ssOldement = ssl_CurrInns[iOld];
						if (ssl_params.find(ssOldement) < 0) {
							BOOL bSndChnl = FALSE;
							int iTrunk = iIFBTrunkAddress;
							int iPort = 0;
							GetTrunkAndPortFromString(ssOldement, &iTrunk, &iPort, &bSndChnl);
							// so old element is not included in new list of outputs - so will be dropped - if a highway - release
							if (iTrunk != iIFBTrunkAddress) {
								int iLocalPort = 0;
								int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iTrunk, iPort, &iLocalPort);
								if ((iHighwayUsed > 0) && (iLocalPort > 0)) {
									CHighwayRecord* pHigh = GetHighwayRecord(iHighwayUsed);
									if (pHigh) {
										pHigh->removeWorkingMode(MODE_IFBINPUT, iIFBRecordIndex);
										if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
									}
									else
										Debug("ProcessIFBInputCommand -error- ifb %d no OLD Highway for trunk %d srce %d from old inns %s", iIFBRecordIndex, iTrunk, iPort, LPCSTR(ssCurrInns));
								}
							}
						}
					}// for iold	

					// now run thru list other way round and find what is to be kept an what is new
					for (int indx = 0; indx < ssl_params.count(); indx++) {
						// get element - is element in curr ringmaster rev - then keep by adding to  data list
						bncs_string ssElement = ssl_params[indx];
						BOOL bSndChnl = FALSE;
						int iTrunk = iIFBTrunkAddress;
						int iPort = 0;
						GetTrunkAndPortFromString(ssElement, &iTrunk, &iPort, &bSndChnl);
						if (ssl_CurrInns.find(ssElement) >= 0) {
							// already an input in ifb - if same trunk - just add dest to datalist - else need to determine highway dest port used in ifb
							if (iTrunk == iIFBTrunkAddress) {
								bncs_string ssData = bncs_string("%1").arg(iPort);
								if (bSndChnl) ssData.append("s");
								ssl_tosend.append(ssData);
							}
							else {
								// on diff trunk - has to be using a highway - find it, find outgoing dest on ifb trunk - check this dest is in current REV -add that to data list
								int iLocalPort = 0;
								int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iTrunk, iPort, &iLocalPort);
								if ((iHighwayUsed > 0) && (iLocalPort > 0))
									ssl_tosend.append(iLocalPort);
								else
									Debug("ProcessIFBInputCommand -error- ifb %d no Highway for trunk %d srce %d from curr inns %s", iIFBRecordIndex, iTrunk, iPort, LPCSTR(ssCurrInns));
							}
						}
						else {
							// element is not in curr ringmstr rev - thus NEW - 
							//  if same trunk - add  dest to datalist , else get highway and add its dest to datalist
							if (iTrunk == iIFBTrunkAddress) {
								bncs_string ssData = bncs_string("%1").arg(iPort);
								if (bSndChnl) ssData.append("s");
								ssl_tosend.append(ssData);
							}
							else {
								// source port is on another trunk -- requires a highway to be put into ifb as highway INPUT and routed into ring for IFB
								int iInHighwayIndex = FindHighwayForIFBMixerInputUse(iIFBRecordIndex, MODE_IFBINPUT, iIFBTrunkAddress, iTrunk, iPort);
								CHighwayRecord* pHigh = GetHighwayRecord(iInHighwayIndex);
								if (pHigh)
									ssl_tosend.append(pHigh->getToSource());
								else {
									SetAlarmErrorMessage(iIFBTrunkAddress, bncs_string("ProcessIFB -2- INPUT NO HIGHWAY found for ifb %1 cmd %2 ").arg(iIFBRecordIndex).arg(ssCmd));
									Debug("ProcessIFBInputCommand -ERROR2-ADD- NO ROUTE/Highway FOR IFB %d cmd %s ", iIFBRecordIndex, LPCSTR(ssCmd));
								}
							}
						}
					} // for indx
					// send new list of inputs to Riedel for this ifb
					QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, bncs_string("&INPUT=%1").arg(ssl_tosend.toString(',')));
				}
				else {
					Debug("ProcessIFBInputCommand - IFB %d same outputs  %s as current %s", iIFBRecordIndex, LPCSTR(ssParameters), LPCSTR(ssCurrInns));
					// thus nothing to do ???
				}
			}
			ProcessNextCommand(50);

		}
	}
	else
		Debug("ProcessIFBInputCmd - invalid IFB input command %d %s or invalid classes", iIFBRecordIndex, LPCSTR(ssParameters));
}


void ProcessIFBMixMinusCommand(int iIFBRecordIndex, int iWhichIFB, bncs_string ssCmd, bncs_string ssParameters)
{
	int iIFBTrunkAddress = 0;
	CRiedel_IFBs* pRevsIFB = GetRiedel_IFBs_Record(iIFBRecordIndex);
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
	if ((pRMstrIFB) && (pRevsIFB)) {
		iIFBTrunkAddress = pRMstrIFB->getAssociatedTrunkAddress();
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iIFBTrunkAddress);
		if (pRing) {
			// test for type of input cmd -- may be some tidying up to do of highways first 
			if (ssCmd.find("MIXMINUSADD") >= 0) {
				// just adding -- is it on same trunk - then no highway required
				int iSourcePort = 0;
				BOOL bSndChnl = FALSE;
				int iSourceTrunk = iIFBTrunkAddress;
				if (ssParameters.length() == 0) ssParameters = bncs_string("0");
				GetTrunkAndPortFromString(ssParameters, &iSourceTrunk, &iSourcePort, &bSndChnl);
				int iIFBDevice = GetReidel_IFB_Device_FromTrunk(iSourceTrunk);
				if ((iIFBDevice > 0) && (iSourcePort > 0)) {
					if (iSourceTrunk == iIFBTrunkAddress) {
						bncs_string ssData = bncs_string("%1").arg(iSourcePort);
						if (bSndChnl) ssData.append("s");
						QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&MIXMINUSADD=%1").arg(ssData));
					}
					else {
						// source port is on another trunk -- requires a highway to be put into ifb as highway INPUT and routed into ring for IFB
						int iLastHighwayIndex = FindHighwayForIFBMixerInputUse(iIFBRecordIndex, MODE_IFBINPUT, iIFBTrunkAddress, iSourceTrunk, iSourcePort);
						CHighwayRecord* pHigh = GetHighwayRecord(iLastHighwayIndex);
						if (pHigh)
							QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&MIXMINUSADD=%1").arg(pHigh->getToSource()));
						else {
							SetAlarmErrorMessage(iIFBTrunkAddress, bncs_string("ProcessIFB - MIXMINUS NO HIGHWAY found for ifb %1 cmd %2 ").arg(iIFBRecordIndex).arg(ssCmd));
							Debug("ProcessIFBMixMinusCommand -ERROR-ADD- NO ROUTE/Highway FOR IFB %d cmd %s ", iIFBRecordIndex, LPCSTR(ssCmd));
						}
					}
				}
			}
			else if (ssCmd.find("MIXMINUSREMOVE") >= 0) {
				// just removing a port -- is on same trunk or does a highway need clearing  -- is source port a highway in...
				BOOL bSndChnl = FALSE;
				int iSourceTrunk = iIFBTrunkAddress, iSourcePort = 0;
				if (ssParameters.length() == 0) ssParameters = bncs_string("0");
				GetTrunkAndPortFromString(ssParameters, &iSourceTrunk, &iSourcePort, &bSndChnl);
				int iIFBDevice = GetReidel_IFB_Device_FromTrunk(iIFBTrunkAddress);
				// if using highway then remove the ToSource of highway feeding input + clear highway ??? / mm - else just remove local port
				if (iSourceTrunk == iIFBTrunkAddress) {
					if ((iIFBDevice > 0) && (iSourcePort > 0)) {
						bncs_string ssData = bncs_string("%1").arg(iSourcePort);
						if (bSndChnl) ssData.append("s");
						QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&MIXMINUSREMOVE=%1").arg(ssData));
					}
				}
				else {
					// there should be a highway into ifb -- find it and get final source as this will be the port to remove from ifb
					// get current revertive for this ifb on its ring - check each input - if highway traced back to given ultimate src is that specified - checking number users; if =1 then clear down highway
					int iLocalPort = 0;
					int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iSourceTrunk, iSourcePort, &iLocalPort);
					if ((iHighwayUsed > 0) && (iLocalPort>0)) {
						CHighwayRecord* pHigh = GetHighwayRecord(iHighwayUsed);
						if (pHigh) {
							pHigh->removeWorkingMode(MODE_IFBINPUT, iIFBRecordIndex);
							if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
						}
						QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&MIXMINUSREMOVE=%1").arg(iLocalPort));
					}
				}
			}
			else {
				// list of inputs command - need to determine differences !! - release highway if no longer required, acquire highways for new - but leave existing if they are still required
				// first clear existing ports and any highways but only if still NOT in new command
				bncs_stringlist ssl_params = bncs_stringlist(ssParameters, ',');
				bncs_stringlist ssl_tosend = bncs_stringlist("", ',');
				bncs_stringlist ssll = bncs_stringlist(pRMstrIFB->getRingMasterRevertive(), '|');
				bncs_string ssCurrMixMs = "";
				if (ssll.count() > 0) ssCurrMixMs = ssll[1];
				Debug("ProcIFBjustInputs - curr mmins : %s  new params %s ", LPCSTR(ssCurrMixMs), LPCSTR(ssParameters));
				if (ssParameters != ssCurrMixMs) {
					// some diff -- so processing required
					//  determine diffs - as to what is now old, what is new, and what stays the same -- in bid to keep existing highways if still required
					bncs_stringlist ssl_CurrMxMs = bncs_stringlist(ssCurrMixMs, ',');
					for (int iOld = 0; iOld < ssl_CurrMxMs.count(); iOld++) {
						bncs_string ssOldement = ssl_CurrMxMs[iOld];
						if (ssl_params.find(ssOldement) < 0) {
							BOOL bSndChnl = FALSE;
							int iTrunk = iIFBTrunkAddress;
							int iPort = 0;
							GetTrunkAndPortFromString(ssOldement, &iTrunk, &iPort, &bSndChnl);
							// so old element is not included in new list of outputs - so will be dropped - if a highway - release
							if (iTrunk != iIFBTrunkAddress) {
								int iLocalPort = 0;
								int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iTrunk, iPort, &iLocalPort);
								if ((iHighwayUsed > 0) && (iLocalPort > 0)) {
									CHighwayRecord* pHigh = GetHighwayRecord(iHighwayUsed);
									if (pHigh) {
										pHigh->removeWorkingMode(MODE_IFBINPUT, iIFBRecordIndex);
										if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
									}
									else
										Debug("ProcessIFBMixMinusCommand -error- ifb %d no OLD Highway for trunk %d srce %d from old inns %s", iIFBRecordIndex, iTrunk, iPort, LPCSTR(ssCurrMixMs));
								}
							}
						}
					}// for iold	

					// now run thru list other way round and find what is to be kept an what is new
					for (int indx = 0; indx < ssl_params.count(); indx++) {
						// get element - is element in curr ringmaster rev - then keep by adding to  data list
						bncs_string ssElement = ssl_params[indx];
						BOOL bSndChnl = FALSE;
						int iTrunk = iIFBTrunkAddress;
						int iPort = 0;
						GetTrunkAndPortFromString(ssElement, &iTrunk, &iPort, &bSndChnl);
						if (ssl_CurrMxMs.find(ssElement) >= 0) {
							// already an output in ifb - if same trunk - just add dest to datalist - else need to determine highway dest port used in ifb
							if (iTrunk == iIFBTrunkAddress) {
								bncs_string ssData = bncs_string("%1").arg(iPort);
								if (bSndChnl) ssData.append("s");
								ssl_tosend.append(ssData);
							}
							else {
								// on diff trunk - has to be using a highway - find it, find outgoing dest on ifb trunk - check this dest is in current REV -add that to data list
								int iLocalPort = 0;
								int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iTrunk, iPort, &iLocalPort);
								if ((iHighwayUsed > 0) && (iLocalPort > 0))
									ssl_tosend.append(iLocalPort);
								else
									Debug("ProcessIFBMixMinusCommand -error- ifb %d no Highway for trunk %d srce %d from curr mm %s", iIFBRecordIndex, iTrunk, iPort, LPCSTR(ssCurrMixMs));
							}
						}
						else {
							// element is not in curr ringmstr rev - thus NEW - 
							//  if same trunk - add  dest to datalist , else get highway and add its dest to datalist
							if (iTrunk == iIFBTrunkAddress) {
								bncs_string ssData = bncs_string("%1").arg(iPort);
								if (bSndChnl) ssData.append("s");
								ssl_tosend.append(ssData);
							}
							else {
								// source port is on another trunk -- requires a highway to be put into ifb as highway INPUT and routed into ring for IFB
								int iInHighwayIndex = FindHighwayForIFBMixerInputUse(iIFBRecordIndex, MODE_IFBINPUT, iIFBTrunkAddress, iTrunk, iPort);
								CHighwayRecord* pHigh = GetHighwayRecord(iInHighwayIndex);
								if (pHigh)
									ssl_tosend.append(pHigh->getToSource());
								else {
									SetAlarmErrorMessage(iIFBTrunkAddress, bncs_string("ProcessIFB -2- MIXMINUS NO HIGHWAY found for ifb %1 cmd %2 ").arg(iIFBRecordIndex).arg(ssCmd));
									Debug("ProcessIFBMixMinusCommand -ERROR-ADD- MIXMINUS NO HIGHWAY /ROUTE FOR IFB %d cmd %s ", iIFBRecordIndex, LPCSTR(ssCmd));
								}
							}
						}
					} // for indx
					// send new list of inputs to Riedel for this ifb
					QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, bncs_string("&MIXMINUS=%1").arg(ssl_tosend.toString(',')));
				}
				else {
					Debug("ProcessIFBMixMinusCommand - IFB %d same outputs  %s as current %s", iIFBRecordIndex, LPCSTR(ssParameters), LPCSTR(ssCurrMixMs));
					// thus nothing to do ???
				}
			}
			ProcessNextCommand(50);
		}
	}
	else
		Debug("ProcessIFBMixMinusCmd - invalid IFB mix minus command %d %s ", iIFBRecordIndex, LPCSTR(ssParameters));
}

void ProcessIFBOutputCommand(int iIFBRecordIndex, int iWhichIFB, bncs_string ssCmd, bncs_string ssParameters)
{

	int iIFBTrunkAddress = 0;
	CRiedel_IFBs* pRevsIFB = GetRiedel_IFBs_Record(iIFBRecordIndex);
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
	if ((pRMstrIFB) && (pRevsIFB)) {
		iIFBTrunkAddress = pRMstrIFB->getAssociatedTrunkAddress();
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iIFBTrunkAddress);
		if (pRing) {
			// test for type of output -- may be some tidying up to do of highways first 
			if (ssCmd.find("OUTPUTADD") >= 0) {
				// just adding -- is it on same trunk - then no highway required
				BOOL bSndChnl = FALSE;
				int iDestinationPort = 0;
				int iDestinationTrunk = iIFBTrunkAddress;
				if (ssParameters.length() == 0) ssParameters = bncs_string("0");
				GetTrunkAndPortFromString(ssParameters, &iDestinationTrunk, &iDestinationPort, &bSndChnl);
				if (iDestinationTrunk == iIFBTrunkAddress) {
					bncs_string ssData = bncs_string("%1").arg(iDestinationPort);
					if (bSndChnl) ssData.append("s");
					QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, bncs_string("&OUTPUTADD=%1").arg(ssData));
				}
				else {
					// port is on another trunk -- requires a highway to be put into ifb as highway dest and routed to ring specified
					int iFirstHighwayIndex = FindHighwayForIFBMixerOutputUse(iIFBRecordIndex, MODE_IFBOUTPUT, iIFBTrunkAddress, iDestinationTrunk, iDestinationPort);
					CHighwayRecord* pHigh = GetHighwayRecord(iFirstHighwayIndex);
					if (pHigh)
						QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, bncs_string("&OUTPUTADD=%1").arg(pHigh->getFromDestination()));
					else {
						SetAlarmErrorMessage(iIFBTrunkAddress, bncs_string("ProcessIFBOutput NO HIGHWAY found for ifb %1 cmd %2 ").arg(iIFBRecordIndex).arg(ssCmd));
						Debug("ProcessIFBOutputCommand -ERROR-ADD- NO ROUTE/Highway FOR IFB %d cmd %s ", iIFBRecordIndex, LPCSTR(ssCmd));
					}
				}
			}
			else if (ssCmd.find("OUTPUTREMOVE") >= 0) {
				// could be : ordinary port on ifb ring, highway dest port or final trunk.dest on far ring using highway to another ring.....
				BOOL bSndChnl = FALSE;
				int iDestinationPort = 0;
				int iDestinationTrunk = iIFBTrunkAddress;
				if (ssParameters.length() == 0) ssParameters = bncs_string("0");
				GetTrunkAndPortFromString(ssParameters, &iDestinationTrunk, &iDestinationPort, &bSndChnl);
				if (iDestinationTrunk == iIFBTrunkAddress) {
					// check if this dest happens to be highway dest on this ring - if so is it set for this ifb's use - if so park highway
					CHighwayRecord* pFromHigh = GetHighwayRecordByFromTrunkAndDest(iIFBTrunkAddress, iDestinationPort, FALSE);
					if (pFromHigh) {
						pFromHigh->removeWorkingMode(MODE_IFBOUTPUT, iIFBRecordIndex);
						if (pFromHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pFromHigh->getRecordIndex());
						QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, bncs_string("&OUTPUTREMOVE=%1").arg(pFromHigh->getFromDestination())); // may not need this line
					}
					else {
						if (iDestinationPort > 0) {
							bncs_string ssData = bncs_string("%1").arg(iDestinationPort);
							if (bSndChnl) ssData.append("s");
							QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, bncs_string("&OUTPUTREMOVE=%1").arg(ssData));
						}
					}
				}
				else {
					// from distant trunk/port need to know highway being used 
					CHighwayRecord* pHigh = FindIFBMixerOutputHighwayFromTrunkAndDest(iIFBRecordIndex, MODE_IFBOUTPUT, iDestinationTrunk, iDestinationPort);
					if (pHigh) {
						pHigh->removeWorkingMode(MODE_IFBOUTPUT, iIFBRecordIndex);
						if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
						QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, bncs_string("&OUTPUTREMOVE=%1").arg(pHigh->getFromDestination()));
					}
					else {
						int iIFBDevice = GetReidel_IFB_Device_FromTrunk(iDestinationTrunk);
						if ((iIFBDevice > 0) && (iDestinationPort > 0)) {
							bncs_string ssData = bncs_string("%1").arg(iDestinationPort);
							if (bSndChnl) ssData.append("s");
							QueueCmdForRiedelDriver(iIFBDevice, iWhichIFB, bncs_string("&OUTPUTREMOVE=%1").arg(ssData));
						}
					}
				}
			}
			else {
				// list of outputs command - need to determine differences !! - release highway if no longer required, acquire highways for new - but leave existing if they are still required
				// first clear existing ports and any highways -- only if still NOT in new command
				bncs_stringlist ssl_params = bncs_stringlist(ssParameters, ',');
				bncs_stringlist ssl_tosend = bncs_stringlist("", ',');
				bncs_stringlist ssll = bncs_stringlist(pRMstrIFB->getRingMasterRevertive(), '|');
				bncs_string ssCurrOuts = "";
				if (ssll.count() > 2) ssCurrOuts = ssll[2];
				Debug("ProcIFBjustOutputs - curr outs : %s  new params %s ", LPCSTR(ssCurrOuts), LPCSTR(ssParameters));
				if (ssParameters != ssCurrOuts) {
					// some diff -- so processing required
					//  determine diffs - as to what is now old, what is new, and what stays the same -- in bid to keep existing highways
					bncs_stringlist ssl_CurrOuts = bncs_stringlist(ssCurrOuts, ',');
					for (int iOld = 0; iOld < ssl_CurrOuts.count(); iOld++) {
						bncs_string ssOldement = ssl_CurrOuts[iOld];
						if (ssl_params.find(ssOldement) < 0) {
							BOOL bSndChnl = FALSE;
							int iDestTrunk = iIFBTrunkAddress;
							int iDestPort = 0;
							GetTrunkAndPortFromString(ssOldement, &iDestTrunk, &iDestPort, &bSndChnl);
							// so old element is not included in new list of outputs - so will be dropped - if a highway - release
							if (iDestTrunk != iIFBTrunkAddress) {
								// should be using a highway - so could release here 
								CHighwayRecord* pHighIfb = DetermineHighwayInIFBMixerOutputRev(pRevsIFB->getIFB_Outputs(), iIFBRecordIndex, iIFBTrunkAddress, iDestTrunk, iDestPort);
								if (pHighIfb) {
									Debug("ProcessIFBOutputCommand -ifb %d parking/removing OLD Highway %d for trunk %d dest %d", iIFBRecordIndex, pHighIfb->getRecordIndex(), iDestTrunk, iDestPort);
									pHighIfb->removeWorkingMode(MODE_IFBOUTPUT, iIFBRecordIndex);
									if (pHighIfb->getNumberWorkingModeEntries() == 0) ParkHighway(pHighIfb->getRecordIndex());
								}
								else
									Debug("ProcessIFBOutputCommand -error- ifb %d no OLD Highway for trunk %d dest %d from old outs %s", iIFBRecordIndex, iDestTrunk, iDestPort, LPCSTR(ssCurrOuts));
							}
						}
					}// for iold	

					// now run thru list other way round and find what is to be kept an what is new
					for (int indx = 0; indx < ssl_params.count(); indx++) {
						// get element - is element in curr  ringmaster rev - then keep by adding to  data list
						bncs_string ssElement = ssl_params[indx];
						BOOL bSndChnl = FALSE;
						int iDestTrunk = iIFBTrunkAddress;
						int iDestPort = 0;
						GetTrunkAndPortFromString(ssElement, &iDestTrunk, &iDestPort, &bSndChnl);
						if (ssl_CurrOuts.find(ssElement) >= 0) {
							// already an output in ifb - if same trunk - just add dest to datalist - else need to determine highway dest port used in ifb
							if (iDestTrunk == iIFBTrunkAddress) {
								bncs_string ssData = bncs_string("%1").arg(iDestPort);
								if (bSndChnl) ssData.append("s");
								ssl_tosend.append(ssData);
							}
							else {
								// on diff trunk - has to be using a highway - find it, find outgoing dest on ifb trunk - check this dest is in current REV -add that to data list
								CHighwayRecord* pHighIfb = DetermineHighwayInIFBMixerOutputRev(pRevsIFB->getIFB_Outputs(), iIFBRecordIndex, iIFBTrunkAddress, iDestTrunk, iDestPort);
								if (pHighIfb)
									ssl_tosend.append(pHighIfb->getFromDestination());
								else
									Debug("ProcessIFBOutputCommand -error- ifb %d no Highway for trunk %d dest %d from curr outs %s", iIFBRecordIndex, iDestTrunk, iDestPort, LPCSTR(ssCurrOuts));
							}
						}
						else {
							// element is not in curr ringmstr rev - thus NEW - 
							//  if same trunk - add  dest to datalist , else get highway and add its dest to datalist
							if (iDestTrunk == iIFBTrunkAddress) {
								bncs_string ssData = bncs_string("%1").arg(iDestPort);
								if (bSndChnl) ssData.append("s");
								ssl_tosend.append(ssData);
							}
							else {
								// port is on another trunk -- requires a highway to be put into ifb as highway dest and routed to ring specified
								int iFirstHighwayIndex = FindHighwayForIFBMixerOutputUse(iIFBRecordIndex, MODE_IFBOUTPUT, iIFBTrunkAddress, iDestTrunk, iDestPort);
								CHighwayRecord* pHigh = GetHighwayRecord(iFirstHighwayIndex);
								if (pHigh)
									ssl_tosend.append(pHigh->getFromDestination());
								else
									Debug("ProcessIFBOutputCommand -ERROR-ADD- NO ROUTE/Highway FOR IFB %d output %d %d ", iIFBRecordIndex, iDestTrunk, iDestPort);
							}
						}
					} // for indx
					// send new list of outputs to Riedel for this ifb
					QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, bncs_string("&OUTPUT=%1").arg(ssl_tosend.toString(',')));
				}
				else {
					Debug("ProcessIFBOutputCommand - IFB %d same outputs  %s as current %s", iIFBRecordIndex, LPCSTR(ssParameters), LPCSTR(ssCurrOuts));
					// nothing to do ???
				}
			}
			ProcessNextCommand(BNCS_COMMAND_RATE);
		}
	}
	else
		Debug("ProcessIFBOutputCmd - invalid IFB %d or trunk address %d or classes", iIFBRecordIndex, iIFBTrunkAddress);
}


void ProcessIFBLabelCommand(int iIFBRecordIndex, int iWhichIFB, bncs_string ssParameters)
{
	// Labels are now distinct per ring 
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
	if (pRMstrIFB) {
		int iIFBRevsRecord = 0;
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pRMstrIFB->getAssociatedTrunkAddress());
		if (pRing) {
			// sending  label fbs for this ring only now
			char szCommand[MAX_AUTOBFFR_STRING] = "";
			wsprintf(szCommand, "IW %d '&LABEL=%s' %d", pRing->iRing_Device_IFB, LPCSTR(ssParameters), iWhichIFB);
			AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_IFB, iWhichIFB, 0);
			ProcessNextCommand(5);
		}
	}
	else
		Debug("ProcessIFB..Cmd - invalid IFB %d record %d ", iWhichIFB, iIFBRecordIndex);

}


void ProcessIFBRemoveCommand(int iIFBRecordIndex, int iWhichIFB, bncs_string ssCmd, bncs_string ssParameters)
{

	int iIFBTrunkAddress = 0;
	CRiedel_IFBs* pRevsIFB = GetRiedel_IFBs_Record(iIFBRecordIndex);
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
	if ((pRMstrIFB) && (pRevsIFB)) {
		iIFBTrunkAddress = pRMstrIFB->getAssociatedTrunkAddress();
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iIFBTrunkAddress);
		if ((pRing) && (ssParameters.length() > 0)) {
			//
			if (ssParameters.find("EVERYTHING") >= 0) {
				// send to the Riedel rings defined for this ifb
				char szCommand[MAX_AUTOBFFR_STRING] = "";
				wsprintf(szCommand, "IW %d '&REMOVE=EVERYTHING' %d", pRing->iRing_Device_IFB, iWhichIFB);
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_IFB, iWhichIFB, 0);
				//
				int iP = 0;
				// removing everything -- go thru ports -- are any ports highways in / out - then clear
				bncs_stringlist sslinns = bncs_stringlist(bncs_string("%1,%2").arg(pRevsIFB->getIFB_Inputs()).arg(pRevsIFB->getIFB_MixMinus()), ',');
				for (int iiP = 0; iiP < sslinns.count(); iiP++) {
					CHighwayRecord* pInHigh = GetHighwayRecordByToRtrSrce(pRing->iRing_Device_GRD, sslinns[iiP].toInt());
					if (pInHigh) {
						pInHigh->removeWorkingMode(MODE_IFBINPUT, iIFBRecordIndex);
						if (pInHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pInHigh->getRecordIndex());
					}
				}
				// out ports
				bncs_stringlist sslOut = bncs_stringlist(pRevsIFB->getIFB_Outputs(), ',');
				for (int iP = 0; iP < sslOut.count(); iP++) {
					CHighwayRecord* pOutHigh = GetHighwayRecordByFromRtrDest(pRing->iRing_Device_GRD, sslOut[iP].toInt());
					if (pOutHigh) {
						pOutHigh->removeWorkingMode(MODE_IFBOUTPUT, iIFBRecordIndex);
						if (pOutHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pOutHigh->getRecordIndex());
					}
				}
			}
			else {
				// REVOVE=<port>  removing a specified port -- is this port listed as a dest output   note : REMOVE=<ring.port> ???
				// just removing a port -- is on same trunk does a highway need clearing 
				BOOL bSndChnl = FALSE;
				int iGivenTrunk = iIFBTrunkAddress;
				int iGivenPort = 0;
				GetTrunkAndPortFromString(ssParameters, &iGivenTrunk, &iGivenPort, &bSndChnl);
				// check it is a highway and listed in ifb ports
				bncs_stringlist sslinns = bncs_stringlist(bncs_string("%1,%2").arg(pRevsIFB->getIFB_Inputs()).arg(pRevsIFB->getIFB_MixMinus()), ',');
				if (sslinns.find(bncs_string(iGivenPort)) >= 0) {
					CHighwayRecord* pInHigh = GetHighwayRecordByToRtrSrce(pRing->iRing_Device_GRD, iGivenPort);
					if (pInHigh) {
						pInHigh->removeWorkingMode(MODE_IFBINPUT, iIFBRecordIndex);
						if (pInHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pInHigh->getRecordIndex());
					}
				}
				bncs_stringlist sslOut = bncs_stringlist(pRevsIFB->getIFB_Outputs(), ',');
				if (sslOut.find(bncs_string(iGivenPort)) >= 0) {
					CHighwayRecord* pOutHigh = GetHighwayRecordByFromRtrDest(pRing->iRing_Device_GRD, iGivenPort);
					if (pOutHigh) {
						pOutHigh->removeWorkingMode(MODE_IFBOUTPUT, iIFBRecordIndex);
						if (pOutHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pOutHigh->getRecordIndex());
					}
				}
				// now clear from ifb
				bncs_string ssData = bncs_string("%1").arg(iGivenPort);
				if (bSndChnl) ssData.append("s");
				char szCommand[MAX_AUTOBFFR_STRING] = "";
				wsprintf(szCommand, "IW %d '&REMOVE=%s' %d", pRing->iRing_Device_IFB, LPCSTR(ssData), iWhichIFB);
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_IFB, iWhichIFB, 0);
			}
			ProcessNextCommand(BNCS_COMMAND_RATE);
		}
	}
	else
		Debug("ProcessIFB..Cmd - invalid IFB %d or record address %d ", iWhichIFB, iIFBRecordIndex);
}



void ProcessIncomingIFBCommand(int iIFBInfoDevice, int iIFBSlotIndx, bncs_string ssSlot)
{
	// process shortcut commands or full ifb command from packager -- may require highways if ports are from rings other than ifb ring
	// search for &<cmd> -- for shortcut command or   '|' to indicate a full command
	//
	// as 1500 ifbs per ring -- determine which IFB infodriver and hence which IFB index into maps and hence default ring  etc etc etc 
	// iwhcihifb is the IFB 1-1500;    iIFBrecordIndex is the map entry - multiple of 1500 per number riedel rings defined
	int iWhichIFB = iIFBSlotIndx;
	int iIFBRecordIndex = 0;
	if ((iIFBInfoDevice >= i_info_IFBs_Start) && (iIFBInfoDevice <= i_info_IFBs_End)) {
		int iArrayIndx = iIFBInfoDevice - i_info_IFBs_Start + 1;
		if ((iArrayIndx > 0) && (iArrayIndx <= iNumberRiedelRings) && (iArrayIndx < MAX_RIEDEL_RINGS)) {
			iIFBRecordIndex = ((iArrayIndx - 1)*MAX_IFBS) + iIFBSlotIndx;
		}
	}
	else {
		Debug("ProcessIFBCommand- invalid incoming ifb infodriver %d - cmd ignored ", iIFBInfoDevice);
		return;
	}

	if (iNextManagementProcessing < 3) iNextManagementProcessing++; // give time for revs to come back from cmds issued
	int iIFBTrunkAddress = 0;
	CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
	if (pIFB) {
		iIFBTrunkAddress = pIFB->getAssociatedTrunkAddress();
		if (pIFB->getIFBProcessingFlag())
			Debug("ProcessIFBCommand --ALREADY PROCESSING ifb %d -- Using Trunk %d for CMD to be processed %s", iWhichIFB, iIFBTrunkAddress, LPCSTR(ssSlot));
		else
			Debug("ProcessIFBCommand --Setting flag TRUE ifb %d -- Using Trunk %d for CMD to be processed %s", iWhichIFB, iIFBTrunkAddress, LPCSTR(ssSlot));
		pIFB->setIFBProcessingFlag(TRUE);
	}

	//
	if ((ssSlot.find("&") == 0) && (ssSlot.find("=") > 0)) {
		// determine shortcut command
		bncs_string ssleft = "", ssCmd = "", ssParams = "";
		ssSlot.split('=', ssleft, ssParams);
		ssCmd = ssleft.upper();
		if (ssCmd.find("&INPUT") >= 0) {
			ProcessIFBInputCommand(iIFBRecordIndex, iWhichIFB, ssCmd, ssParams);
		}
		else if (ssCmd.find("&MIXMINUS") >= 0) {
			ProcessIFBMixMinusCommand(iIFBRecordIndex, iWhichIFB, ssCmd, ssParams);
		}
		else if (ssCmd.find("&LABEL") >= 0) {
			ProcessIFBLabelCommand(iIFBRecordIndex, iWhichIFB, ssParams);
		}
		else if (ssCmd.find("&OUTPUT") >= 0) {
			ProcessIFBOutputCommand(iIFBRecordIndex, iWhichIFB, ssCmd, ssParams);
		}
		else if (ssCmd.find("&REMOVE") >= 0) {
			ProcessIFBRemoveCommand(iIFBRecordIndex, iWhichIFB, ssCmd, ssParams);
		}
		else
			Debug("ProcessIFBCommand - ifb %d record %d - invalid shortcut cmd : %s", iWhichIFB, iIFBRecordIndex, LPCSTR(ssSlot));
		//
	}
	else if (ssSlot.find("|") >= 0) {

		CRiedel_IFBs* pRevsIFB = GetRiedel_IFBs_Record(iIFBRecordIndex);
		CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iIFBTrunkAddress);

		// split up full ifb command
		if (ssSlot == "|||") ssSlot = "0|0|0|";
		bncs_stringlist ssl_NewCmd_list = bncs_stringlist(ssSlot, '|');
		if ((pRing&&pRMstrIFB&&pRevsIFB) && (ssl_NewCmd_list.count() > 2)) {

			// needs to be at least 3 items - inputs, mm, outputs; label is optional
			bncs_stringlist ssl_NewInputs = bncs_stringlist(ssl_NewCmd_list[0], ',');
			bncs_stringlist ssl_NewMMinus = bncs_stringlist(ssl_NewCmd_list[1], ',');
			bncs_stringlist ssl_NewOutputs = bncs_stringlist(ssl_NewCmd_list[2], ',');
			bncs_string ssLabel = "";
			if (ssl_NewCmd_list.count() > 3) ssLabel = ssl_NewCmd_list[3];

			// get current revertives for inputs, mm and outputs
			bncs_stringlist ssl_CurrInns = bncs_stringlist("", ',');
			bncs_stringlist ssl_CurrMixMs = bncs_stringlist("", ',');
			bncs_stringlist ssl_CurrOuts = bncs_stringlist("", ',');
			bncs_stringlist ssl_IFB_Rev = bncs_stringlist(pRMstrIFB->getRingMasterRevertive(), '|');
			if (ssl_IFB_Rev.count() > 0) ssl_CurrInns = bncs_stringlist(ssl_IFB_Rev[0], ',');
			if (ssl_IFB_Rev.count() > 1) ssl_CurrMixMs = bncs_stringlist(ssl_IFB_Rev[1], ',');
			if (ssl_IFB_Rev.count() > 2) ssl_CurrOuts = bncs_stringlist(ssl_IFB_Rev[2], ',');

			// INPUTS
			// check where ports are from -- on IFB ring or are bncs highways required ??? -- if no highways - pass cmd as is onto Riedel ifb 
			// find the differences and process - if none then nothing to process
			ClearDataLists();
			// process thru 3 lists and build appropriate lists for ifb assoc ring 
			// go thru curr list and see what is NOT in New list - ie being removed -- if on distant ring, then highway needs releasing
			for (int iOld = 0; iOld < ssl_CurrInns.count(); iOld++) {
				bncs_string ssOldement = ssl_CurrInns[iOld];
				if (ssl_NewInputs.find(ssOldement) < 0) {
					BOOL bSndChnl = FALSE;
					int iTrunk = iIFBTrunkAddress;
					int iPort = 0;
					GetTrunkAndPortFromString(ssOldement, &iTrunk, &iPort,&bSndChnl);
					// so old element is not included in new list of outputs - so will be dropped - if a highway - release
					if (iTrunk != iIFBTrunkAddress) {
						int iLocalPort = 0;
						int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iTrunk, iPort, &iLocalPort);
						if ((iHighwayUsed > 0) && (iLocalPort > 0)) {
							CHighwayRecord* pHighIfb = GetHighwayRecord(iHighwayUsed);
							if (pHighIfb) {
								// id sole user then park
								Debug("ProcessIFBCommand -Inp ifb %d parking OLD Highway %d for trunk %d dest %d", iIFBRecordIndex, pHighIfb->getRecordIndex(), iTrunk, iPort);
								pHighIfb->removeWorkingMode(MODE_IFBINPUT, iIFBRecordIndex);
								if (pHighIfb->getNumberWorkingModeEntries() == 0) ParkHighway(pHighIfb->getRecordIndex());
							}
							else
								Debug("ProcessIFBCommand -inp error- ifb %d no OLD Highway for trunk %d srce %d from old inns %s", iIFBRecordIndex, iTrunk, iPort, LPCSTR(ssl_CurrInns.toString(',')));
						}
					}
				}
			}// for iold	

			// now run thru list other way round and find what is to be kept an what is new
			for (int indx = 0; indx < ssl_NewInputs.count(); indx++) {
				// get element - is element in curr  ringmaster rev - then keep by adding to  data list
				bncs_string ssElement = ssl_NewInputs[indx];
				BOOL bSndChnl = FALSE;
				int iTrunk = iIFBTrunkAddress;
				int iPort = 0;
				GetTrunkAndPortFromString(ssElement, &iTrunk, &iPort, &bSndChnl);
				if (ssl_CurrInns.find(ssElement) >= 0) {
					// already an input in ifb - if same trunk - just add dest to datalist - else need to determine highway dest port used in ifb
					if (iTrunk == iIFBTrunkAddress) {
						bncs_string ssData = bncs_string("%1").arg(iPort);
						if (bSndChnl) ssData.append("s");
						AddToDataLists_Stringlist(iIFBTrunkAddress, 1, ssData);
					}
					else {
						// on diff trunk - has to be using a highway - find it, find outgoing dest on ifb trunk - check this dest is in current REV -add that to data list
						int iLocalPort = 0;
						int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iTrunk, iPort, &iLocalPort);
						if ((iHighwayUsed > 0) && (iLocalPort > 0))  //  more checks pls  -- and what to do it if is not on a highway ???
							AddToDataLists_Stringlist(iIFBTrunkAddress, 1, iLocalPort);
						else
							Debug("ProcessIFBCommand -Inp error- ifb %d no Highway for trunk %d srce %d from curr inns %s", iIFBRecordIndex, iTrunk, iPort, LPCSTR(ssl_CurrInns.toString(',')));
					}
				}
				else {
					// element is not in curr ringmstr rev - thus NEW - 
					//  if same trunk - add  dest to datalist , else get highway and add its dest to datalist
					if (iTrunk == iIFBTrunkAddress) {
						bncs_string ssData = bncs_string("%1").arg(iPort);
						if (bSndChnl) ssData.append("s");
						AddToDataLists_Stringlist(iIFBTrunkAddress, 1, ssData);
					}
					else {
						// source port is on another trunk -- requires a highway to be put into ifb as highway INPUT and routed into ring for IFB
						int iInHighwayIndex = FindHighwayForIFBMixerInputUse(iIFBRecordIndex, MODE_IFBINPUT, iIFBTrunkAddress, iTrunk, iPort);
						CHighwayRecord* pHigh = GetHighwayRecord(iInHighwayIndex);
						if (pHigh)
							AddToDataLists_Stringlist(iIFBTrunkAddress, 1, pHigh->getToSource());
						else {
							SetAlarmErrorMessage(iIFBTrunkAddress, bncs_string("ProcessIFB-INPUT -2- NO HIGHWAY found for ifb %1 cmd %2 ").arg(iIFBRecordIndex).arg(ssl_NewInputs.toString(',')));
							Debug("ProcessIFBCommand -Inp-ERROR-ADD- NO ROUTE/Highway FOR IFB %d cmd %s ", iIFBRecordIndex, LPCSTR(ssl_NewInputs.toString(',')));
						}
					}
				}
			} // for indx

			// MIX MINUS
			// go thru new list - if on same ring - just add to new command, if on distant ring - and in curr, get existing highway, else get new highway for ring.port
			// go thru curr list and see what is NOT in New list - ie being removed -- if on distant ring, then highway needs releasing
			for (int iOld = 0; iOld < ssl_CurrMixMs.count(); iOld++) {
				bncs_string ssOldement = ssl_CurrMixMs[iOld];
				if (ssl_NewMMinus.find(ssOldement) < 0) {
					BOOL bSndChnl = FALSE;
					int iTrunk = iIFBTrunkAddress;
					int iPort = 0;
					GetTrunkAndPortFromString(ssOldement, &iTrunk, &iPort, &bSndChnl);
					// so old element is not included in new list of outputs - so will be dropped - if a highway - release
					if (iTrunk != iIFBTrunkAddress) {
						int iLocalPort = 0;
						int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iTrunk, iPort, &iLocalPort);
						if ((iHighwayUsed > 0) && (iLocalPort > 0)) {
							CHighwayRecord* pHighIfb = GetHighwayRecord(iHighwayUsed);
							if (pHighIfb) {
								// is sole user - then park
								Debug("ProcessIFBCommand MM-ifb %d parking OLD Highway %d for trunk %d dest %d", iIFBRecordIndex, pHighIfb->getRecordIndex(), iTrunk, iPort);
								pHighIfb->removeWorkingMode(MODE_IFBINPUT, iIFBRecordIndex);
								if (pHighIfb->getNumberWorkingModeEntries() == 0) ParkHighway(pHighIfb->getRecordIndex());
							}
							else
								Debug("ProcessIFBCommand -MM error- ifb %d no OLD Highway for trunk %d srce %d from old mxms %s", iIFBRecordIndex, iTrunk, iPort, LPCSTR(ssl_CurrOuts.toString(',')));
						}
					}
				}
			}// for iold	

			// now run thru list other way round and find what is to be kept an what is new
			for (int indx = 0; indx < ssl_NewMMinus.count(); indx++) {
				// get element - is element in curr  ringmaster rev - then keep by adding to  data list
				bncs_string ssElement = ssl_NewMMinus[indx];
				BOOL bSndChnl = FALSE;
				int iTrunk = iIFBTrunkAddress;
				int iPort = 0;
				GetTrunkAndPortFromString(ssElement, &iTrunk, &iPort,&bSndChnl);
				if (ssl_CurrMixMs.find(ssElement) >= 0) {
					// already an input in ifb - if same trunk - just add dest to datalist - else need to determine highway dest port used in ifb
					if (iTrunk == iIFBTrunkAddress) {
						bncs_string ssData = bncs_string("%1").arg(iPort);
						if (bSndChnl) ssData.append("s");
						AddToDataLists_Stringlist(iIFBTrunkAddress, 2, ssData);
					}
					else {
						// on diff trunk - has to be using a highway - find it, find outgoing dest on ifb trunk - check this dest is in current REV -add that to data list
						int iLocalPort = 0;
						int iHighwayUsed = DetermineInputHighwayForIFBMixerGivenRingPort(iIFBRecordIndex, MODE_IFBINPUT, iTrunk, iPort, &iLocalPort);
						if ((iHighwayUsed > 0) && (iLocalPort > 0))  // more checks pls  -- and what to do it if is not on a highway ???
							AddToDataLists_Stringlist(iIFBTrunkAddress, 2, iLocalPort);
						else
							Debug("ProcessIFBCommand -MxMs error- ifb %d no Highway for trunk %d srce %d from curr mxms %s", iIFBRecordIndex, iTrunk, iPort, LPCSTR(ssl_CurrMixMs.toString(',')));
					}
				}
				else {
					// element is not in curr ringmstr rev - thus NEW - 
					//  if same trunk - add  dest to datalist , else get highway and add its dest to datalist
					if (iTrunk == iIFBTrunkAddress) {
						bncs_string ssData = bncs_string("%1").arg(iPort);
						if (bSndChnl) ssData.append("s");
						AddToDataLists_Stringlist(iIFBTrunkAddress, 2, ssData);
					}
					else {
						// source port is on another trunk -- requires a highway to be put into ifb as highway INPUT and routed into ring for IFB
						int iInHighwayIndex = FindHighwayForIFBMixerInputUse(iIFBRecordIndex, MODE_IFBINPUT, iIFBTrunkAddress, iTrunk, iPort);
						CHighwayRecord* pHigh = GetHighwayRecord(iInHighwayIndex);
						if (pHigh)
							AddToDataLists_Stringlist(iIFBTrunkAddress, 2, pHigh->getToSource());
						else {
							SetAlarmErrorMessage(iIFBTrunkAddress, bncs_string("RingMaster-ProcessIFB-MIXMINUS -2- NO HIGHWAY found for ifb %1 cmd %2 ").arg(iIFBRecordIndex).arg(ssl_NewMMinus.toString(',')));
							Debug("ProcessIFBCommand -MXMs-ERROR-ADD- NO ROUTE/Highway FOR IFB %d cmd %s ", iIFBRecordIndex, LPCSTR(ssl_NewMMinus.toString(',')));
						}
					}
				}
			} // for indx


			//OUTPUTS
			// need to determine differences !! - release highway if no longer required, acquire highways for new - but leave existing if they are still required
			// first clear existing ports and any highways and then assign / acquire for new list

			Debug("ProcIFBOutputs - curr outs : %s  new outs %s ", LPCSTR(ssl_CurrOuts.toString(',')), LPCSTR(ssl_NewOutputs.toString(',')));
			if (ssl_CurrOuts.toString(',') == ssl_NewOutputs.toString(',')) {
				// no changes in outputs - just send curr list back at Riedel driver for given trunk address
				bncs_stringlist ssl_CurrRev = bncs_stringlist(pRevsIFB->getIFB_Outputs(), ',');
				for (int indx = 0; indx < ssl_CurrRev.count(); indx++) {
					AddToDataLists_Stringlist(iIFBTrunkAddress, 3, ssl_CurrRev[indx]);
				}
			}
			else {
				// some diff -- so processing required
				//  determine diffs - as to what is now old, what is new, and what stays the same -- in bid to keep existing highways
				for (int iOld = 0; iOld < ssl_CurrOuts.count(); iOld++) {
					bncs_string ssOldement = ssl_CurrOuts[iOld];
					if (ssl_NewOutputs.find(ssOldement) < 0) {
						BOOL bSndChnl = FALSE;
						int iDestTrunk = 0, iDestPort = 0;
						GetTrunkAndPortFromString(ssOldement, &iDestTrunk, &iDestPort, &bSndChnl);
						if (iDestTrunk == 0) iDestTrunk = iIFBTrunkAddress;
						// so old element is not included in new list of outputs - so will be dropped - if a highway - release
						if (iDestTrunk != iIFBTrunkAddress) {
							// should be using a highway - if sole user release here 
							CHighwayRecord* pHighIfb = DetermineHighwayInIFBMixerOutputRev(pRevsIFB->getIFB_Outputs(), iIFBRecordIndex, iIFBTrunkAddress, iDestTrunk, iDestPort);
							if (pHighIfb) {
								Debug("ProcessIFBCommand -ifb %d parking OLD Highway %d for trunk %d dest %d", iIFBRecordIndex, pHighIfb->getRecordIndex(), iDestTrunk, iDestPort);
								pHighIfb->removeWorkingMode(MODE_IFBOUTPUT, iIFBRecordIndex);
								if (pHighIfb->getNumberWorkingModeEntries() == 0) ParkHighway(pHighIfb->getRecordIndex());
							}
							else
								Debug("ProcessIFBCommand -error- ifb %d no OLD Highway for trunk %d dest %d from old outs %s", iIFBRecordIndex, iDestTrunk, iDestPort, LPCSTR(ssl_CurrOuts.toString(',')));
						}
					}
				}// for iold	

				// now run thru list other way round and find what is to be kept an what is new
				for (int indx = 0; indx < ssl_NewOutputs.count(); indx++) {
					// get element - is element in curr  ringmaster rev - then keep by adding to  data list
					bncs_string ssElement = ssl_NewOutputs[indx];
					BOOL bSndChnl = FALSE;
					int iDestTrunk = 0, iDestPort = 0;
					GetTrunkAndPortFromString(ssElement, &iDestTrunk, &iDestPort,&bSndChnl);
					if (iDestTrunk == 0) iDestTrunk = iIFBTrunkAddress;
					if (ssl_CurrOuts.find(ssElement) >= 0) {
						// already an output in ifb - if same trunk - just add dest to datalist - else need to determine highway dest port used in ifb
						if (iDestTrunk == iIFBTrunkAddress) {
							bncs_string ssData = bncs_string("%1").arg(iDestPort);
							if (bSndChnl) ssData.append("s");
							AddToDataLists_Stringlist(iIFBTrunkAddress, 3, ssData);
						}
						else {
							// on diff trunk - has to be using a highway - find it, find outgoing dest on ifb trunk - check this dest is in current REV -add that to data list
							CHighwayRecord* pHighIfb = DetermineHighwayInIFBMixerOutputRev(pRevsIFB->getIFB_Outputs(), iIFBRecordIndex, iIFBTrunkAddress, iDestTrunk, iDestPort);
							if (pHighIfb)
								AddToDataLists_Stringlist(iIFBTrunkAddress, 3, pHighIfb->getFromDestination());
							else
								Debug("ProcessIFBCommand -error- ifb %d no Highway for trunk %d dest %d from curr outs %s", iIFBRecordIndex, iDestTrunk, iDestPort, LPCSTR(ssl_CurrOuts.toString(',')));
						}
					}
					else {
						// element is not in curr ringmstr rev - thus NEW - 
						//  if same trunk - add  dest to datalist , else get highway and add its dest to datalist
						if (iDestTrunk == iIFBTrunkAddress) {
							bncs_string ssData = bncs_string("%1").arg(iDestPort);
							if (bSndChnl) ssData.append("s");
							AddToDataLists_Stringlist(iIFBTrunkAddress, 3, ssData);
						}
						else {
							// port is on another trunk -- requires a highway to be put into ifb as highway dest and routed to ring specified
							int iFirstHighwayIndex = FindHighwayForIFBMixerOutputUse(iIFBRecordIndex, MODE_IFBOUTPUT, iIFBTrunkAddress, iDestTrunk, iDestPort);
							CHighwayRecord* pHigh = GetHighwayRecord(iFirstHighwayIndex);
							if (pHigh)
								AddToDataLists_Stringlist(iIFBTrunkAddress, 3, pHigh->getFromDestination());
							else
								Debug("ProcessIFBOutputCommand -ERROR-ADD- NO ROUTE/Highway FOR IFB %d output %d %d ", iIFBRecordIndex, iDestTrunk, iDestPort);
						}
					}
				} // for indx
			}

			// only send data to relevant ring now 
			CDataList* pData = GetDataList_By_Trunk(iIFBTrunkAddress);
			if (pData&&pRing&&pRMstrIFB) {
				// build a complete command for each ring -- and send a command, even |||, to each rings ifb driver / slot  
				bncs_string ssCmd = "";
				if (pData->getDataList(1).length() > 0) ssCmd.append(pData->getDataList(1));
				ssCmd.append("|");
				if (pData->getDataList(2).length() > 0) ssCmd.append(pData->getDataList(2));
				ssCmd.append("|");
				if (pData->getDataList(3).length() > 0) ssCmd.append(pData->getDataList(3));
				ssCmd.append("|");
				if ((ssLabel.length()>0) && (ssCmd != "|||")) ssCmd.append(ssLabel);
				// only send if different from last rececived revertive 
				bncs_string ssRev = pRevsIFB->getRawRiedelRevertive();
				if ((ssRev == "") || (ssRev == "0|0|0")) ssRev = "|||";
				// test for difference ???
				QueueCmdForRiedelDriver(pRing->iRing_Device_IFB, iWhichIFB, ssCmd);
			}
			//
			ProcessNextCommand(50);
		}
		else
			Debug("ProcessIFBCommand - IFB %d invalid full cmd %s", iIFBRecordIndex, LPCSTR(ssSlot));
	}
	else 
		Debug("ProcessIFBCommand - IFB %d invalid/unexpected cmd %s", iIFBRecordIndex, LPCSTR(ssSlot));

}



////////////////////////////////////////////////////////////////////////////////////////
//
// CONFERENCE processing

void ProcessConfMembersCommand(int iWhichConf, bncs_string ssSlot)
{
	if (iNextManagementProcessing < 3) iNextManagementProcessing++; // give time for revs to come back from commands
	// parse thru given members and send commands to appropriate rings
	// process shortcut commands else long form 
	// members should have trunk.port properties in their lists
	// shortcut command
	if (ssSlot.find("&RESEND") >= 0) {
		for (int iRings = 1; iRings <= iNumberRiedelRings; iRings++) {
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRings);
			if (pRing) {
				char szCommand[MAX_AUTOBFFR_STRING] = "";
				wsprintf(szCommand, "IW %d '&RESEND' %d", pRing->iRing_Device_Conference, iWhichConf);
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Conference, 0, 0);
			}
		} // for
		ProcessNextCommand(iNumberRiedelRings);
	}
	else if (ssSlot.find("&REMOVE") >= 0) {
		if (ssSlot.find("EVERYTHING") >= 0) {
			for (int iRings = 1; iRings <= iNumberRiedelRings; iRings++) {
				CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRings);
				if (pRing) {
					char szCommand[MAX_AUTOBFFR_STRING] = "";
					wsprintf(szCommand, "IW %d '&REMOVE=EVERYTHING' %d", pRing->iRing_Device_Conference, iWhichConf);
					AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Conference, 0, 0);
				}
			} // for
			ProcessNextCommand(iNumberRiedelRings);
		}
		else {
			// remove just remove a trunk.port 
			bncs_string ssCmd = "", ssParams = "";
			ssSlot.split('=', ssCmd, ssParams);
			BOOL bSndChnl = FALSE;
			int iTrunk = 0, iPort = 0;
			GetTrunkAndPortFromString(ssParams, &iTrunk, &iPort, &bSndChnl);
			CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
			if (pRing) {
				char szCommand[MAX_AUTOBFFR_STRING] = "";
				wsprintf(szCommand, "IW %d '&REMOVE=%d' %d", pRing->iRing_Device_Conference, iPort, iWhichConf);
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Conference, 0, 0);
				ProcessNextCommand(1);
			}
			else
				Debug("ProcessConfMembers - RemovePort - invalid trunk from conf %d - %s", iWhichConf, LPCSTR(ssSlot));
		}
	}
	else if (ssSlot.find("&ADD") >= 0) {
		// just a trunk.port 
		bncs_string ssCmd = "", ssParams = "";
		ssSlot.split('=', ssCmd, ssParams);
		BOOL bSndChnl = FALSE;
		int iTrunk = 0, iPort = 0;
		GetTrunkAndPortFromString(ssParams, &iTrunk, &iPort, &bSndChnl);
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
		if (pRing) {
			char szCommand[MAX_AUTOBFFR_STRING] = "";
			wsprintf(szCommand, "IW %d '&ADD=%d' %d", pRing->iRing_Device_Conference, iPort, iWhichConf);
			AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Conference, 0, 0);
			ProcessNextCommand(1);
		}
		else
			Debug("ProcessConfMembers - AddPort - invalid trunk from conf %d - %s", iWhichConf, LPCSTR(ssSlot));
	}
	else {
		// completely new port list for conf  // trunk.port|TLV|prot, in form
		// add new list of members
		//bncs_stringlist sslTrunkToProcess = bncs_stringlist("", ',');
		bncs_stringlist sslData = bncs_stringlist(ssSlot, ',');
		for (int ielement = 0; ielement < sslData.count(); ielement++) {
			// for each data element string by |, so <trunk>.<port> should be first then priv then prot 
			bncs_stringlist sslElement = bncs_stringlist(sslData[ielement], '|');
			if (sslElement.count()>2) {
				// should be 3 entries in this list
				BOOL bSndChnl = FALSE;
				int iTrunk = 0, iPort = 0;
				GetTrunkAndPortFromString(sslElement[0], &iTrunk, &iPort, &bSndChnl);
				CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
				if ((pRing)&&(iPort>0)) {
					bncs_string sstr = bncs_string("%1|%2|%3").arg(iPort).arg(sslElement[1]).arg(sslElement[2]);
					if (pRing->ssAProcessString.length() > 0) pRing->ssAProcessString.append(","); 
					pRing->ssAProcessString.append(sstr);
					//if (sslTrunkToProcess.find(iTrunk) < 0) sslTrunkToProcess.append(iTrunk);
				}
				else
					Debug("ProcessConfMembers - NEW PORT LIST - invalid trunk from conf %d - %s", iWhichConf, LPCSTR(sslElement.toString('|')) );
			}
			else
				Debug("ProcessConfMembers - NEW PORT LIST - invalid params from conf %d - %s", iWhichConf, LPCSTR(sslElement.toString('|')));
		} // for ielement
		// now go thru ALL the rings and send out ALL the ProcessStrings - will thus clear any that should no longer be there
		// was for (int iProc = 0; iProc <sslTrunkToProcess.count(); iProc++) {
		for (int iRings = 1; iRings <= iNumberRiedelRings; iRings++) {
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRings);
			if (pRing) {
				char szCommand[MAX_AUTOBFFR_STRING] = "";
				if (pRing->ssAProcessString.length()>0) 
					wsprintf(szCommand, "IW %d '%s' %d", pRing->iRing_Device_Conference, LPCSTR(pRing->ssAProcessString), iWhichConf); // member(s) to add
				else 
					wsprintf(szCommand, "IW %d ' ' %d", pRing->iRing_Device_Conference, iWhichConf); // empty string - send ' '  even if no members in current rev
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Conference, 0, 0);
				pRing->ssAProcessString = "";  // must clear this string after use - thats the rule
			}
		}  // for iproc
		ProcessNextCommand(iNumberRiedelRings);
	}
}


void ProcessConfLabelCommand(int iWhichConf, bncs_string ssSlot)
{
	// store label 
	CRingMaster_CONF* pConf = GetRingMaster_CONF_Record(iWhichConf);
	if (pConf) {
		pConf->storeRingMasterLabel(ssSlot);
	}
	// send this label to all confs on all rings
	int iRings = 1;
	char szCommand[MAX_AUTOBFFR_STRING] = "";
	for (iRings = 1; iRings <= iNumberRiedelRings; iRings++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRings);
		if (pRing) {
			wsprintf(szCommand, "IW %d '%s' %d", pRing->iRing_Device_Conference, LPCSTR(pConf->getRingMasterLabel()), iWhichConf+1000);
			if ((ecCSIClient) && (iOverallTXRXModeStatus == IFMODE_TXRX)) ecCSIClient->txinfocmd(szCommand, NOW);
		}
	}
	// -------------- packager does not resend - so required from here
	for (iRings = 1; iRings <= iNumberRiedelRings; iRings++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRings);
		if (pRing) {
			wsprintf(szCommand, "IW %d '&RESEND' %d", pRing->iRing_Device_Conference, iWhichConf);
			AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Conference, 0, 0);
		}
	}
	// labels first, then tramit resend in next cmd timer loop
	SetTimer(hWndMain, COMMAND_TIMER_ID, 100, NULL); // timer for command Q
	bNextCommandTimerRunning = TRUE;

}


void ProcessConfPanel(int iWhichConf, bncs_string ssSlot)
{
	//  driver docs says slots are read only 
	//  so just need to process revs and store appropriately
}


void ProcessParkGenericHighway(bncs_string ssSlot)
{
	// command to slot 2000 - in form of PARK_HIGHWAY=index  or PARK_DESTINATION=ring.port
	bncs_stringlist ssl_cmd = bncs_stringlist(ssSlot, '=');
	if (ssl_cmd.count() == 2) {
		if (ssSlot.find("HIGHWAY") >= 0) {
			int iWhichHighway = ssl_cmd[1].toInt();
			if ((iWhichHighway > 0) && (iWhichHighway <= iNumberBNCSHighways)) {
				Debug("ProcessGenericPark  -(1)- Highway %d parked command %s", iWhichHighway, LPCSTR(ssSlot));
				ParkHighway(iWhichHighway);
			}
		}
		else if (ssSlot.find("DESTINATION") >= 0) {
			BOOL bSndChnl = FALSE;
			int iRingTrunk = 0, iDestPort = 0;
			GetTrunkAndPortFromString(ssl_cmd[1], &iRingTrunk, &iDestPort, &bSndChnl);
			if ((iRingTrunk > 0) && (iDestPort > 0)) {
				// find highway with given dest
				CHighwayRecord* pHigh = GetHighwayRecordByFromTrunkAndDest(iRingTrunk, iDestPort, FALSE);
				if (pHigh) {
					Debug("ProcessGenericPark  -(2)- Highway %d parked dest command %s", pHigh->getRecordIndex(), LPCSTR(ssSlot));
					ParkHighway(pHigh->getRecordIndex());
				}
				else
					Debug("ProcessGenericPark - no highway found from command %s", LPCSTR(ssSlot));
			}
			else
				Debug("ProcessGenericPark - no highway found from command %s", LPCSTR(ssSlot));
		}
	}
	else
		Debug("ProcessGenericPark - invalid command %s", LPCSTR(ssSlot));
}


///////////////////////////////////////////////////////////////////////////////////////
//
// monitor port processing ???


////////////////////////////////////////////////////////////////////////////////////////

int CalculateRandomOffset()
{
	// used to create a time offset before calling TXRX function - to stop multiInfodriver tx clash
	int iRand1 = 0;
	srand(time(0)); // generate random seed point
	iRand1 = (rand() % 2007);
	return (iRand1);
}


//
//  FUNCTION: InfoNotify()
//
//  PURPOSE: Callback function from an infodriver, notifying event
//
//  COMMENTS: the pointer pex references the infodriver which is notifying
//			  iSlot and szSlot are provided for convenience
//			
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot)
{

	if (pex&&szSlot) {
		int iWhichInfoDriverType = DetermineWhichInfoDriverType(pex->iDevice);
		if (iWhichInfoDriverType>0) {
			switch (pex->iStatus) {
			case CONNECTED: // this is the "normal" situation
				if (bShowAllDebugMessages) Debug("InfoNotify -INFOtype %d  device %d slot %d changed to %s", iWhichInfoDriverType, pex->iDevice, iSlot, szSlot);
				if (!SlotChange(iWhichInfoDriverType, pex->iDevice, iSlot, szSlot)) pex->setslot(iSlot, szSlot);
				UpdateCounters();
				break;

			case DISCONNECTED:
				Debug("Infodriver %d has disconnected-RingMasterAuto CLOSING DOWN", pex->iDevice);
				if (eiRingMasterInfoId) eiRingMasterInfoId->updateslot(COMM_STATUS_SLOT, "0");	 // error	
				bValidCollediaStatetoContinue = FALSE;
				PostMessage(hWndMain, WM_CLOSE, 0, 0);
				return;
				break;

			case TO_RXONLY:
				if (bShowAllDebugMessages) Debug("Infodriver %d received request to go RX Only", pex->iDevice);
				if ((pex->iDevice == i_info_RingMaster) && (iOverallStateJustChanged == 0) && (iOverallTXRXModeStatus == IFMODE_TXRX)) {
					Debug("Infodriver %d received request to go RX Only -- checking state ", pex->iDevice);
					if (eiRingMasterInfoId) {
						eiRingMasterInfoId->setmode(IFMODE_TXRX);
						eiRingMasterInfoId->requestmode = TO_TXRX;
					}
					iOverallModeChangedOver = 4;
				}
				break;

			case TO_TXRX:
				if ((pex->iDevice == i_info_RingMaster) && (iOverallStateJustChanged == 0) && (iOverallTXRXModeStatus == IFMODE_RXONLY)) {
					iOverallStateJustChanged = 1;
					int iTimerOffset = CalculateRandomOffset();
					SetTimer(hWndMain, FORCETXRX_TIMER_ID, iTimerOffset, NULL);
					SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "Req to Tx/Rx to main ");
					if (bShowAllDebugMessages) Debug("InfoNotify - force go TXRX - in %d ms", iTimerOffset);
				}
				break;

			case QUERY_TXRX:
			case 2190:
				if (iLabelAutoStatus == 3) Debug("Infodriver %d received Query_TXRX", pex->iDevice);
				// ignore this state -- means all left as it was 
				break;

			default:
				Debug("iStatus=%d", pex->iStatus);
			} // switch
		}
		else {
			Debug("InfoNotify - unknown infodriver %d slot %d - input ignored", pex->iDevice, iSlot);
		}
	}
	else
		Debug("InfoNotify -error- invalid pex class or szslot ");

}


//
//  FUNCTION: SlotChange()
//
//  PURPOSE: Function called when slot change is notified
//
//  COMMENTS: All we need here are the driver number, the slot, and the new contents
//		
//	RETURNS: TRUE for processed (don't update the slot),
//			 FALSE for ignored (so calling function can just update the slot 
//								like a normal infodriver)
//			

BOOL SlotChange(int iWhichInfoDriver, int iInfoDevice, int iInfoSlot, LPCSTR szSlot)
{
	// slots 1..1999 are for destination package routing cmds and responses
	bncs_string ssSlot = bncs_string(szSlot);
	switch (iWhichInfoDriver) {
		case AUTO_RINGMASTER_INFO: 
		{
	 
			if ((iInfoSlot>MCR_CLONE_COMMANDS_INFO_START) && (iInfoSlot <= (MCR_CLONE_COMMANDS_INFO_START + MAX_MCR_CLONE_PORTS))) {
				ProcessMCRMonCommand(iInfoSlot, ssSlot);
			}
			else if (iInfoSlot == HIGHWAYS_INFO_TRACE_DATA) {
				ProcessParkGenericHighway(ssSlot);
			}
			else if ((iInfoSlot>HIGHWAYS_INFO_TRACE_DATA) && (iInfoSlot < RIEDEL_TRUNK_INFO_TOTALUSAGE)) {
				int iWhichHighway = iInfoSlot - HIGHWAYS_INFO_TRACE_DATA;
				if ((iWhichHighway>0) && (iWhichHighway <= iNumberBNCSHighways) && (ssSlot.find("PARK") >= 0)) {
					ParkHighway(iWhichHighway);
				}
				Debug("ProcessParkDefHighway - invalid command %s or highway index %d ", LPCSTR(ssSlot), iWhichHighway);
			}
			else if (iInfoSlot == RINGTRUNKS_ERROR_MSGS) {
				PostAutomaticErrorMessage(0, " ");
			}
			else if ((iInfoSlot > RINGTRUNKS_ERROR_MSGS) && (iInfoSlot < (RINGTRUNKS_ERROR_MSGS + MAX_RIEDEL_RINGS))) {
				// ack - to clear slot, else display what is given -- way of sending messages for a ring ???
				int iWhichRing = iInfoSlot - RINGTRUNKS_ERROR_MSGS;
				ClearRingAlarmMessage(iWhichRing);
			}
			// else nothing to do here at present -- write only by auto for Highways (2000...) and error messages
		}
		break;

		case AUTO_CONFERENCES_INFO:
		{
			if ((iInfoSlot>0) && (iInfoSlot <= MAX_CONFERENCES)) {
				// members
				ProcessConfMembersCommand(iInfoSlot, ssSlot.upper());
			}
			else if ((iInfoSlot>1000) && (iInfoSlot <= (1000 + MAX_CONFERENCES))) {
				// label
				int iWhichConf = iInfoSlot - 1000;
				ProcessConfLabelCommand(iWhichConf, ssSlot);
			}
			else if ((iInfoSlot>2000) && (iInfoSlot <= (2000 + MAX_CONFERENCES))) {
				// panel members
				int iWhichConf = iInfoSlot - 2000;
				// slots are read only as per driver docs -- as member list generated by monitor commands
				// ProcessConfPanelCommand(iWhichConf, ssSlot);
			}
			else
				Debug("Slotchange CONF function OUT OF RANGE - slot %d has NOT changed to '%s'", iInfoSlot, szSlot);
		}
		break;

		case AUTO_RING_IFBS_INFOS:
		{
			// 1..1500 for ifbs revs etc 
			if ((iInfoSlot>0) && (iInfoSlot <= MAX_IFBS)) {
				ProcessIncomingIFBCommand( iInfoDevice, iInfoSlot, ssSlot);
			}
			else if ((iInfoSlot>RMSTR_MIXERS_INFO_START) && (iInfoSlot <= (RMSTR_MIXERS_INFO_START + MAX_MIXERS_LOGIC))) {
				// 3001..3500 are for ringmaster mixer commands per ring
				ProcessMIXERCommand(iInfoDevice, iInfoSlot, ssSlot);
			}
			else if ((iInfoSlot>RMSTR_LOGIC_INFO_START) && (iInfoSlot <= (RMSTR_LOGIC_INFO_START + MAX_MIXERS_LOGIC))) {
				// 3501..4000 are for remote logic states - enabled or disabled
				Process_REMOTE_LOGIC_Command(iInfoDevice, iInfoSlot, ssSlot);
			}
		}
		break;
		
		
		case AUTO_RING_PORTS_INFOS:
		{
			// only execute if txrx auto - rx will catch up from routing revertives
			ssSlot = bncs_string(szSlot).upper();
			if (iOverallTXRXModeStatus == IFMODE_TXRX) {
				ProcessIncomingRouteCommand( iInfoDevice, iInfoSlot, ssSlot);
				ProcessNextCommand(BNCS_COMMAND_RATE);
			}
		}
		break;

		case AUTO_PORTS_PTI_LOGIC_INFO:
			// NOTE -- this info is read only now - used as PTI and remote logic reporting
		break;
	}

	// all return true as under automatic control - no free user slots 
	return TRUE;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
//   CSI CLIENT FUNCTIONS FOR INFODRIVER REVERTIVES MESSAGES 
//
//   GRD revertives
//
int ValidRiedelGRDDevice(int iDev)
{
	// returns associated ring if a valid/known grd
	if (cl_Riedel_Rings) {
		for (int ii = 1; ii <= iNumberRiedelRings; ii++) {
			CRiedel_Rings* pRec = GetRiedel_Rings_Record(ii);
			if (pRec) {
				if (pRec->iRing_Device_GRD == iDev) {
					return pRec->m_iTrunkAddress;
				}
			}
		}
	}
	return UNKNOWNVAL;
}


void UpdateGRDRevertive(CRiedel_GRDs* pGrd, int iThisDest)
{
	if (pGrd) {
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pGrd->getRingTrunk());
		if (pRing) {
			// get ringPortDefn record
			//CRingPortsDefn* pDefn = GetRingPortsDefintionByRingDest(pGrd->getRingTrunk(), iThisDest);
			//if (pDefn) {
			//
			char szCommand[MAX_AUTOBFFR_STRING] = "";
			iTraceCount = 0;
			ssl_Calc_Trace = bncs_stringlist("", '|');  // clear global var - populated in TraceThePrimarySource
			CHighwayRecord* pHigh = GetHighwayRecord(pGrd->getAssocSrceHighway(pGrd->getRoutedSourceForDestination(iThisDest)));
			if (pHigh) {
				// get trace str
				int iTracedRing = pGrd->getRingTrunk();
				int iTracedGrd = pGrd->getDevice();
				int iTracedSrc = pGrd->getRoutedSourceForDestination(iThisDest);
				TraceThePrimarySource(&iTracedRing, &iTracedGrd, &iTracedSrc);
			}
			ssl_Calc_Trace.append(bncs_string("%1.%2").arg(pGrd->getRingTrunk()).arg(pGrd->getRoutedSourceForDestination(iThisDest)));
			wsprintf(szCommand, "%s", LPCSTR(ssl_Calc_Trace.toString(',')));
			if (pGrd->getLockStatus(iThisDest) > 0) {
				char szLockStr[128] = "";
				wsprintf(szLockStr, "|LOCKED|%s", LPCSTR(pGrd->getLockReasonTimeStamp(iThisDest)));
				strcat(szCommand, szLockStr);
			}
			int iCalcRevInfo = pGrd->getDestinationMappingInfoDrv(iThisDest);
			int iCalcRevSlot = pGrd->getDestinationMappingInfoSlot(iThisDest);
			//
			if ((iCalcRevInfo >= i_info_Ports_Start) && (iCalcRevInfo <= i_info_Ports_End)) {
				int infoIndx = iCalcRevInfo - i_info_Ports_Start +1;
				if ((infoIndx > 0) && (infoIndx <= iNumberRiedelRings) && (infoIndx < MAX_RIEDEL_RINGS)) {
					if (eiPortsInfoIds[infoIndx]) {
						BOOL bUpdated = eiPortsInfoIds[infoIndx]->updateslot(iCalcRevSlot, szCommand);
						if (bUpdated) Debug("UpdateGRDRev - info %d slot %d data %s", iCalcRevInfo, iCalcRevSlot, szCommand);
					}
				}
			}
		}
		else
			Debug("UpdateGRDRev - no ring for grd %d", pGrd->getDevice());
	}
	else
		Debug("UpdateGRDRev - no grd class");
}


BOOL StoreProcessRiedelGRDRevertive(int iThisRing, int iThisGrdDev, int iThisDest, int iNewSrce )
{
	BOOL bTraceError = FALSE;
	// get ring
	CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iThisRing);
	if (pRing) {
		CRiedel_GRDs* pGrd = GetRiedel_GRD_Record(iThisGrdDev);
		if (pGrd) {
			int iOldSrce = pGrd->getRoutedSourceForDestination(iThisDest);
			// store rev 
			pGrd->storeRiedelRevertive(iThisDest, iNewSrce);
			//pGrd->storeTracedRingSource(iThisDest, iThisRing, iThisGrdDev, iNewSrce);
			// note - proccessing carried out in "first pass" once all revs received for first time
			if ((!bAutomaticStarting) && (iNewSrce != iOldSrce)) {
				// clear assoc from old rev - any processing if a highway -- if old srce is highway in and now and no users -- then park it   
				if ((iOldSrce > 0)&&((iNewSrce==PARK_SRCE)||(iNewSrce==0))) {
					if ((pGrd->getAssocSrceHighway(iOldSrce) > 0) && (pGrd->getNumberDestinationsUsingSource(iOldSrce) == 0)) {
						Debug("StoreProcRiedelRev - Parking Highway %d rtr %d into src %d", pGrd->getAssocSrceHighway(iOldSrce), pGrd->getDevice(), iOldSrce);
						ParkHighway(pGrd->getAssocSrceHighway(iOldSrce));
						ProcessNextCommand(5);
					}
					UnlockGRDDestination(pRing, iThisGrdDev, iThisDest);
				}

				// if new src is a highway in -- then determine and store traced source
				iTraceCount = 0;  // reset
				ssl_Calc_Trace = bncs_stringlist("", '|');
				int iTracedRing = iThisRing;
				int iTracedGrd = iThisGrdDev;
				int iTracedSrc = iNewSrce;
				// get traced ring, grd and source
				if (pGrd->getAssocSrceHighway(iNewSrce) > 0) {
					if (!TraceThePrimarySource(&iTracedRing, &iTracedGrd, &iTracedSrc)) {
						Debug("StoreRiedelGRDRev TRACE-PRIMARY-SOURCE-ERROR-detected-possible routing loop");
						bTraceError = TRUE;
					}
				}
				pGrd->storeTracedRingSource(iThisDest, iTracedRing, iTracedGrd, iTracedSrc);
				if (pGrd->getAssocDestHighway(iThisDest) > 0) {
					CHighwayRecord* pHigh = GetHighwayRecord(pGrd->getAssocDestHighway(iThisDest));
					if (pHigh) {
						pHigh->setTracedRouterAndPort(iTracedRing, iTracedGrd, iTracedSrc);
						// note -  highways in use for IFBS / mixer outputs --- will have a real source of 0
						// note - incoming highways into IFBs / mixers will / may not be routed to any dest
						if ((iNewSrce == 0) || (iNewSrce == PARK_SRCE)) {
							// only free highway with src 0 if not in use for ifb / mixer output, as clearing these ifb / mixer will park highway 
							if (pHigh->getHighwayState()<HIGHWAY_IFBMXROUT) pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
						}
						else { // srce other than park so highway in use - check working modes count if none remove ifbmixin state
							if (pHigh->getHighwayState()<HIGHWAY_IFBMXRIN) pHigh->setHighwayState(HIGHWAY_INUSE, iNewSrce, 0);
						}
						if ((iChosenHighway>0)&&(pHigh->getRecordIndex() == iChosenHighway)) DisplayHighwayData();
					}
				}
				// update this grd rev
				UpdateGRDRevertive(pGrd, iThisDest);
				// use traced ring/src and go forward to update all dests that have this src routed to it
				// trace forward - updating all relevant grd + dest records with revised traced ring + src - update infodriver too
				iTraceCount = 0;  // reset
				//Debug("StoreRiedelGrd- traced : r %d g %d s%d ", iTracedRing, iTracedGrd, iTracedSrc);
				if (!TracePrimarySourceForward(iTracedGrd, iTracedSrc, iTracedRing, iTracedGrd, iTracedSrc)) {
					Debug("StoreRiedelGRDRev TRACE-FORWARD-ERROR-detected-possible-loop ???");
					if (bTraceError ) {
						// double error - is a loop - so park highway :
						Debug("StoreRiedelGRDRev DOUBLE-ERROR-detected-so parking highway %d for dest %d %d", pGrd->getAssocDestHighway(iThisDest), iThisGrdDev, iThisDest );
						ParkHighway(pGrd->getAssocDestHighway(iThisDest));
					}
				}
				// update GUI if chosen ring and dest
				if ((iChosenRing == iThisRing) && (iChosenGRDDest == iThisDest)) DisplayDestPortData();
			}
			return TRUE;
		}
		else
			Debug("StoreRiedelGRDRev - no GRD record for dev %d", iThisGrdDev);
	}
	else 
		Debug("StoreRiedelGRDRev - no ring %d record for dev %d", iThisRing, iThisGrdDev);
	return FALSE;

}

//////////////////////////////////////////////////////////////////////////////
//
// Ringmaster port pti processing
//

void UpdateRingMasterPortPti(int iPort)
{
	// in ports slots 1-2000  5 entries in list;   out ports slots 2001 - 4000 3 elements in list
	bncs_stringlist ssl_rev = bncs_stringlist("0|0|0|0|0", '|');
	bncs_stringlist ssl_rev2 = bncs_stringlist("0|0|0", '|');

	// takes all records from each ring and builds complex revertive
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		int iRec1 = ((iRing - 1) * 4000) + iPort;
		int iRec2 = iRec1 + MAX_RIEDEL_PTI;
		// 4000 records per ring - 2000 in, 2000 out
		CRiedel_PTI* pPtiIn = GetRiedel_PTI_Record(iRec1);
		CRiedel_PTI* pPtiOut = GetRiedel_PTI_Record(iRec2);
		if (pPtiIn&&pPtiOut) {
			int iTrunk = pPtiIn->getTrunkAddress();
			// in 
			for (int ielement = 1; ielement <= 5; ielement++) {
				bncs_string sstr = pPtiIn->getRevertiveElement(ielement);
				if (iRing > 1)
					ssl_rev[ielement - 1].append(bncs_string(",%1").arg(sstr));
				else
					ssl_rev[ielement - 1] = sstr;
			}
			// out
			for (int iout = 1; iout <= 3; iout++) {
				bncs_string sstr = pPtiOut->getRevertiveElement(iout);
				if (iRing > 1)
					ssl_rev2[iout - 1].append(bncs_string(",%1").arg(sstr));
				else
					ssl_rev2[iout - 1] = sstr;
			}
		}
		else
			Debug("UpdateRingMPortPTI  - missing rec for ring %d port %d - recs %d %d ", iRing, iPort, iRec1, iRec2);
	}

	bncs_string ssIn = ssl_rev.toString('|');
	if (ssIn.length() > 255) ssIn.truncate(255);
	strcpy(szClientCmd, LPCSTR(ssIn));
	eiPortsPTIInfoId->updateslot(iPort, szClientCmd);

	bncs_string ssOut = ssl_rev2.toString('|');
	if (ssOut.length() > 255) ssOut.truncate(255);
	strcpy(szClientCmd, LPCSTR(ssOut));
	eiPortsPTIInfoId->updateslot(iPort+2000, szClientCmd);

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  INFODRIVER REVERTIVES
//

int ValidInfodriverDevice(int iInfoDev, int iInfoSlot, int *iAssocRing )
{
	*iAssocRing = 0;
	//
	if (iInfoDev == iAutomatics_CtrlInfo_Local) return AUTOS_CONTROL_INFO;
	if (iInfoDev == iAutomatics_CtrlInfo_Remote) return AUTOS_CONTROL_INFO;

	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRing);
		if (pRing) {
			*iAssocRing = iRing;
			if (pRing->iRing_Device_Monitor == iInfoDev) {
				return RIEDEL_MONITOR_TYPE;	// monitors
			}
			if (pRing->iRing_Device_Conference == iInfoDev) {
				return RIEDEL_CONFERENCE_TYPE;			// conferences
			}
			if (pRing->iRing_Device_IFB == iInfoDev) {
				return RIEDEL_IFB_TYPE;			// ifbs
			}
			if (pRing->iRing_Device_PTI == iInfoDev) {
				return RIEDEL_PTI_PORTS;
			}
			if (pRing->iRing_Device_Extras == iInfoDev) {
				if ((iInfoSlot > 400) && (iInfoSlot < RIEDELDRIVER_MIXERS_INFO_START)) {
					return RIEDEL_EXTRAS_TRUNKS;
				}
				else if ((iInfoSlot > RIEDELDRIVER_MIXERS_INFO_START) && (iInfoSlot <= (RIEDELDRIVER_MIXERS_INFO_START + MAX_MIXERS_LOGIC))) {
					return RIEDEL_EXTRAS_MIXERS;
				}
				else if ((iInfoSlot > RIEDELDRIVER_LOGIC_INFO_START) && (iInfoSlot <= (RIEDELDRIVER_LOGIC_INFO_START + MAX_MIXERS_LOGIC))) {
					return RIEDEL_EXTRAS_LOGIC;
				}
			}
		}
		else
			Debug("ValidInforev - no ring class for ring %d", iRing);
	}
	// not found in any ring
	return UNKNOWNVAL;
}

//
// store riedel rev, return record index of rev type being processed
//
int StoreRawRiedelRevertive(int iInfoType, int iAssocRing, int iInfoDev, int iInfoSlot, bncs_string ssState, BOOL *bChanged)
{
	int iV1=0, iV2=0, iV3=0;
	*bChanged = FALSE;
	bncs_string ssOld="";

	// check for presence of "shortcut" commands in revertive - so ignore -- means Riedel driver not running - and infodriver just bouncing what is written to it
	// the label should not contain these two characters
	if ((ssState.find("&") >= 0) && (ssState.find("=") >= 0)) {
		Debug("StoreReidelRev -- FOUND SHORTHAND CMD - so ignoring REV from %d %d - %s", iInfoDev, iInfoSlot, LPCSTR(ssState));
		return 0;
	}

	//Debug("into StoreRaw type %d getmap %d %d ", iInfoType, iInfoDev, iInfoSlot);

	switch (iInfoType)	{

		case RIEDEL_MONITOR_TYPE:
		{
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(iAssocRing);
			if (pRing) {
				int iRecIndx = pRing->iRing_RevsRecord_Monitors + iInfoSlot;
				CRiedel_Monitors* pRec = GetRiedel_Monitors_Record(iRecIndx);
				if (pRec) {
					//Debug("StoreRaw - Mon check rec %d dev %d slot %d -- from %d %d", iRecIndx, pRec->getDevice(), pRec->getSlot(), iInfoDev, iInfoSlot);
					ssOld = pRec->getRawRiedelRevertive();
					if (ssOld != ssState) {
						pRec->storeRiedelData(ssState);
						*bChanged = TRUE;
						return pRec->getRecordIndex();
					}
				}
				else
					Debug("StoreRiedelRev - ERROR no MONITOR rec for %d slot %d found ", iInfoDev, iInfoSlot);
			}
			else
				Debug("StoreRiedelRev -mons- ERROR no RING rec for ring %d for dev %d slot %d", iAssocRing, iInfoDev, iInfoSlot);
		}
		break;

		case RIEDEL_IFB_TYPE:
		{
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(iAssocRing);
			if (pRing) {
				int iRecIndx = pRing->iRing_RevsRecord_IFBs + iInfoSlot;
				CRiedel_IFBs* pRec = GetRiedel_IFBs_Record(iRecIndx);
				if (pRec) {
					ssOld = pRec->getRawRiedelRevertive();
					if (ssOld != ssState) {
						Debug("StoreRaw - ifb raw rev %d -%s- from %d %d", iRecIndx, LPCSTR(ssState), iInfoDev, iInfoSlot);
						pRec->storeRiedelData(ssState);
						*bChanged = TRUE;
						return pRec->getRecordIndex();
					}
				}
				else
					Debug("StoreRiedelRev - ERROR no IFB rec for %d slot %d found ", iInfoDev, iInfoSlot);
			}
			else
				Debug("StoreRiedelRev -ifb- ERROR no RING rec for ring %d for dev %d slot %d", iAssocRing, iInfoDev, iInfoSlot);
		}
		break;

		case RIEDEL_CONFERENCE_TYPE:
		{					
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(iAssocRing);
			if (pRing) {
				int iConf = iInfoSlot;
				if ((iInfoSlot>1000) && (iInfoSlot<2000)) iConf = iInfoSlot - 1000;
				else if ((iInfoSlot>2000) && (iInfoSlot<3000)) iConf = iInfoSlot - 2000;
				int iRecIndx = pRing->iRing_RevsRecord_Conferences + iConf;
				CRiedel_Conferences* pRec = GetRiedel_Conference_Record(iRecIndx);
				if (pRec) {
					//Debug("StoreRaw - conf check rec %d dev %d conf %d -- from %d %d/%d", iRecIndx, pRec->getDevice(), pRec->getConfSlot(), iInfoDev, iConf, iInfoSlot);
					if ((iInfoSlot > 0) && (iInfoSlot < 1000)) {
						ssOld = pRec->getRawRiedelRevertive();
						if (ssOld != ssState) {
							pRec->storeRiedelData(ssState);
							*bChanged = TRUE;
							return pRec->getRecordIndex();
						}
					}
					else if ((iInfoSlot > 1000) && (iInfoSlot < 2000)) {
						ssOld = pRec->getConference_Name();
						if (ssOld != ssState) {
							pRec->storeConfName(ssState);
							*bChanged = TRUE;
							return pRec->getRecordIndex();  // nothing more to do at present
						}
					}
					else if ((iInfoSlot > 2000) && (iInfoSlot < 3000)) {
						ssOld = pRec->getConference_PanelMembers();
						if (ssOld != ssState) {
							pRec->storeConfPanelMem(ssState);
							*bChanged = TRUE;
							return pRec->getRecordIndex();  // nothing more to do at present
						}
					}
				}
				else
					Debug("StoreRiedelRev - ERROR no CONF rec for %d slot %d found ", iInfoDev, iInfoSlot);
			}
			else
				Debug("StoreRiedelRev -conf- ERROR no RING rec for ring %d for dev %d slot %d", iAssocRing, iInfoDev, iInfoSlot);
		}
		break;

		case RIEDEL_EXTRAS_TRUNKS:
		{
			CRiedel_Trunks* pRec = GetRiedel_Trunks_ByDeviceSlot(iInfoDev, iInfoSlot);
			if (pRec) {
				ssOld = pRec->getRawRiedelRevertive();
				if (ssOld != ssState) {
					pRec->storeRiedelData(ssState);
					*bChanged = FALSE;  // no need to flag changed - processing done by housekeeping
					return pRec->getRecordIndex();
				}
			}
			else
				Debug("StoreRiedelRev - ERROR no TRUNK rec for %d slot %d found ", iInfoDev, iInfoSlot);
		}
		break;

		case RIEDEL_EXTRAS_MIXERS:
		{
			CRiedel_Mixers* pRec = GetRiedel_Mixers_ByDeviceSlot(iInfoDev, iInfoSlot);
			if (pRec) {
				ssOld = pRec->getRawRiedelRevertive();
				pRec->storeRiedelData(ssState);
				if (ssOld != ssState) {
					*bChanged = TRUE;
					return pRec->getRecordIndex();
				}
			}
			else
				Debug("StoreRiedelRev - ERROR no MIXER rec for %d slot %d found ", iInfoDev, iInfoSlot);
		}
		break;

		case RIEDEL_EXTRAS_LOGIC:
		{
			CRiedel_Logic* pRec = GetRiedel_Logic_ByDeviceSlot(iInfoDev, iInfoSlot);
			if (pRec) {
				int ioldstate = pRec->getLogicState();
				int inewstate = ssState.toInt();
				pRec->storeRiedelData(ssState);
				if (ioldstate != inewstate) {
					*bChanged = TRUE;
					return pRec->getRecordIndex();
				}
			}
			else
				Debug("StoreRiedelRev - ERROR no LOGIC rec for %d slot %d found ", iInfoDev, iInfoSlot);
		}
		break;

		case RIEDEL_PTI_PORTS:
		{
			CRiedel_PTI* pRec = GetRiedel_PTI_ByDeviceSlot(iInfoDev, iInfoSlot);
			if (pRec) {
				ssOld = pRec->getRawRiedelRevertive();
				pRec->storeRiedelData(ssState);
				if (ssOld != ssState) {
					*bChanged = TRUE;
					return pRec->getRecordIndex();
				}
			}
			else
				Debug("StoreRiedelRev - ERROR no PTI port rec for dev %d slot %d found ", iInfoDev, iInfoSlot);
		}
		break;


	} // SWITCH

	return 0;
}


void ProcessRiedelMonitorRev(int iRecordIndex, int iWhichRevRing, int iSlotNum)
{
	//Debug("ProcMonRev - rev Record %d ring %d slot %d ", iRecordIndex, iWhichRing, iSlotNum);
	CRiedel_Monitors* pRevRec = GetRiedel_Monitors_Record(iRecordIndex);
	if (pRevRec) {
		// check / test for clone port style revertive -- processed differently to key panel members etc
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iWhichRevRing);
		if (pRing) {
			// is iSlotNum -- a. MON final dest    or    b. mon highway dest  -- each handled differently 
			// (a)
			CMCRMonPortRecord* pMonRec = GetMCRMonPortByRingTransDest(pRevRec->getTrunkAddress(), iSlotNum);
			if (pMonRec) {
				//Debug("ProcMonRev - MonPort Record %d from %d %d ", pMonRec->m_iRecordIndex, pRevRec->getTrunkAddress(), iSlotNum);
				// resolve previous 
				bncs_stringlist ssl = bncs_stringlist(pRevRec->getPrevious_Function(), ',');
				if (ssl.count() == 2) {
					// verify mcr highways etc to generate Ringamster Rev equivalent -- trace of port being monitored
					int iPRevPort = ssl[0].toInt();
					int iPRevType = 0;
					if (ssl[1].upper() == bncs_string("S")) iPRevType = MODE_CLONESRCE;
					else if (ssl[1].upper() == bncs_string("D"))  iPRevType = MODE_CLONEDEST;
					if ((iPRevPort > 0) && (iPRevType == MODE_CLONESRCE)) {
						// is port a highway srce -- does it now need clearing ??? as no longer in use ??
						CHighwayRecord* pToHigh = GetHighwayRecordByToTrunkAndSrce(pRevRec->getTrunkAddress(), iPRevPort, TRUE);    
						if (pToHigh) {
							if (pToHigh->getHighwayState() == HIGHWAY_CLONEMON) {
								pToHigh->removeWorkingMode(pMonRec->m_iWorkingModeForHighway, pMonRec->m_iRecordIndex);
								pMonRec->m_iUsingHighwayRecord = 0;
								pMonRec->m_iWorkingModeForHighway = 0;
								if (pToHigh->getNumberWorkingModeEntries()==0) {
									// as now no working entries-- park it 
									Debug("ProcRiedelMonRev - MON rec %d - parking highway %d from old rev %s", 
										pMonRec->m_iRecordIndex, pToHigh->getRecordIndex(), LPCSTR(pRevRec->getPrevious_Function()));
									ParkHighway(pToHigh->getRecordIndex());
								}
							}
						}
					}
					// else prevtype is dest -- nothing to do as old local clone dest monitoring clearing
				}
				// process new clone port revertive 
				bncs_string ssRMstrRev = bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(pRevRec->getMonitoring_Function());
				ssl = bncs_stringlist(pRevRec->getMonitoring_Function(), ',');
				// revs are 0 or  port,S   or port,D
				if (ssl.count() == 2) {
					// verify mcr highways etc to generate Ringamster Rev equivalent -- trace back to real port being monitored for ringmaster rev
					int iRevPort = ssl[0].toInt();
					int iRevType = 0;
					if (ssl[1].upper() == bncs_string("S")) iRevType = MODE_CLONESRCE;
					else if (ssl[1].upper() == bncs_string("D"))  iRevType = MODE_CLONEDEST;
					if ((iRevPort > 0) && (iRevType>0)) {
						if (iRevType == MODE_CLONESRCE) {
							// is it highway into ring  -- trace back to find starting source -- that becomes ringmaster rev -- note only one hop possible at present for clone highways

							CHighwayRecord* pToHigh = GetHighwayRecordByToTrunkAndSrce(pRevRec->getTrunkAddress(), iRevPort, TRUE);
							if (pToHigh) {
								//Debug("ProcRiedelMonRev - A highway %d found from %d %d ", pToHigh->getRecordIndex(), pRevRec->getTrunkAddress(), iRevPort);
								// get the traced source feeding highway - does it match value in highway record
								CRiedel_Monitors* pFromMon = GetRiedel_Monitors_ByTrunkAddressAndSlot(pToHigh->getFromTrunkAddr(), pToHigh->getFromDestination());
								if (pFromMon) {
									// get port from this rev -- true source 
									ssRMstrRev = bncs_string("%1.%2").arg(pFromMon->getTrunkAddress()).arg(pFromMon->getMonitoring_Function());
									//go thru all working mode entries for the ToHighway -- as could be routed/cloned to multiple dests
									for (int iM = 0; iM < pToHigh->getNumberWorkingModeEntries(); iM++) {
										int iMode = 0, iRec = 0;
										pToHigh->getWorkingMode(iM, &iMode, &iRec);
										if (iRec>0) {
											CMCRMonPortRecord* pUserRec = GetMCRMonPortRecord(iRec);
											if (pUserRec) {
												pUserRec->m_ssCommandRevertive = ssRMstrRev;
												Debug("ProcMonRev - 1- ssRMstrRev is %s", LPCSTR(ssRMstrRev));
												eiRingMasterInfoId->updateslot(pUserRec->m_iAssoc_RingMaster_Slot, LPCSTR(ssRMstrRev));
											}
										}
									} // for iM
								}
							}
							// else local source port monitoring -- just return prepared ringmaster rev above
						}
						else if (iRevType == MODE_CLONEDEST) {
							// has to be local dest port monitoring -- return prepared ringmaster rev with added trunk
							pMonRec->m_ssCommandRevertive = ssRMstrRev; // just to 
							Debug("ProcMonRev - 2- ssRMstrRev is %s", LPCSTR(ssRMstrRev));
							eiRingMasterInfoId->updateslot(pMonRec->m_iAssoc_RingMaster_Slot, LPCSTR(ssRMstrRev));
							return;
						}
					}
					// else zero or strange rev -- return prepared ringmaster rev with added trunk
				}
				//  else nothing to do - new rev is illegal or 0 -- return prepared ringmaster rev with added trunk
				pMonRec->m_ssCommandRevertive = ssRMstrRev;
				Debug("ProcMonRev - 3- ssRMstrRev is %s", LPCSTR(ssRMstrRev));
				eiRingMasterInfoId->updateslot(pMonRec->m_iAssoc_RingMaster_Slot, LPCSTR(ssRMstrRev));
				return;
			} // if pMonRec

			// (b) is new riedel mon port rev associated outgoing monitoring highway dest 
			CHighwayRecord* pMonHigh = GetHighwayRecordByFromTrunkAndDest(pRevRec->getTrunkAddress(), iSlotNum, FALSE);  // as dest is revertive slot index
			if (pMonHigh) {
				if (pMonHigh->isMonitoringHighway() ) {
					// trace forward to final users MonPortRecords - has to be based on working mode entries for highway -- update them with ringmastered revertive
					// NOTE - currently only one hop permitted for monitoring ports zzz
					//Debug("ProcMonRev - Mon Highway Record %d from %d %d working %d ", pMonHigh->getRecordIndex(),
					//	pRevRec->getTrunkAddress(), iSlotNum, pMonHigh->getNumberWorkingModeEntries());
					if (pMonHigh->getHighwayState() == HIGHWAY_CLONEMON) {
						bncs_string ssRMstrRev = bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(pRevRec->getMonitoring_Function());
						//go thru all working mode entries for the ToHighway -- as could be routed/cloned to multiple dests
						// XXX what if there are no working modes -- but rev coming back suggests otherwise ???
						for (int iM = 0; iM < pMonHigh->getNumberWorkingModeEntries(); iM++) {
							int iMode = 0, iRec = 0;
							pMonHigh->getWorkingMode(iM, &iMode, &iRec);
							if (iRec>0) {
								CMCRMonPortRecord* pUserRec = GetMCRMonPortRecord(iRec);
								if (pUserRec) {
									pUserRec->m_ssCommandRevertive = ssRMstrRev;
									Debug("ProcMonRev - 4- ssRMstrRev is %s", LPCSTR(ssRMstrRev));
									eiRingMasterInfoId->updateslot(pUserRec->m_iAssoc_RingMaster_Slot, LPCSTR(ssRMstrRev));
								}
							}
						} // for iM
					}
					return;
				} // if isMonitoringHighway
			} // if pMonHigh
		} // if pRing

		// extract CONF  IFB   TrunkIfb  LISTENTOPORT  membership and store with added ring - for use in packager
		// monitor revs are all local to specific ring and processed by packager and panels appropriately
		// currently no monitor infodriver within this auto - so conf members via conf infodrv, ifb members via ifb infodrv
		// get rev record, get prev - remove prev conf or ifb from CONF or IFB recs, Add current CONF or IFB to recs - update all CHANGED records
		bncs_stringlist ssl_confs = bncs_stringlist("", ',');
		bncs_stringlist ssl_ifbs = bncs_stringlist("", ',');
		bncs_stringlist ssl_LToP = bncs_stringlist("", ',');
		// sort out PREV conf/ifb members first
		bncs_stringlist sslPrev = bncs_stringlist(pRevRec->getPrevious_Function(), ',');
		for (int iC = 0; iC < sslPrev.count(); iC++) {
			bncs_string sstr = sslPrev[iC];
			//Debug("ProcessMonRev - prev ele %d str %s", iC, LPCSTR(sstr));
			bncs_stringlist ssl = bncs_stringlist(sstr, '|');
			if (sstr.find("Conference") >= 0) {
				if (ssl.count()>1) {
					int iConfIndx = ssl[1].toInt();
					CRingMaster_CONF* pRMstrConf = GetRingMaster_CONF_Record(iConfIndx);
					if (pRMstrConf) { // remove entry from list
						pRMstrConf->removeFromMonitorMember(bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(iSlotNum));
						ssl_confs.append(iConfIndx);
					}
				}
			}
			else if (sstr.find("TrunkIfb", 0, FALSE) >= 0) {
				if (ssl.count()>1) {
					bncs_stringlist ssl2 = bncs_stringlist(ssl[1], '.');
					if (ssl2.count() > 1) {
						int iIFBTrunk = ssl2[0].toInt();
						int iIFBIndx = ssl2[1].toInt();
						int iIFBRecordIndex = 0;
						if ((iIFBTrunk > 0) && (iIFBIndx > 0)) {
							CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iIFBTrunk);
							if (pRing) {
								iIFBRecordIndex = ((pRing->m_iRecordIndex - 1)*MAX_IFBS) + iIFBIndx;
								CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
								if (pRMstrIFB) { // remove entry from list
									pRMstrIFB->removeIFBMonitorMember(bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(iSlotNum));
									ssl_ifbs.append(iIFBRecordIndex);
								}
							}
						}
						else
							Debug("ProcessMonRev-1-TrunkIfb invalid trunk %d ifb %d values from %s", iIFBTrunk, iIFBIndx, LPCSTR(pRevRec->getPrevious_Function()));
					}
				}
			}
			else if (sstr.find("IFB") >= 0) {
				if (ssl.count()>1) {
					int iIFBIndx = ssl[1].toInt();
					if ((iWhichRevRing > 0) && (iWhichRevRing <= iNumberRiedelRings)) {
						int iIFBRecordIndex = ((pRing->m_iRecordIndex - 1)*MAX_IFBS) + iIFBIndx;
						CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
						if (pRMstrIFB) { // remove entry from list
							pRMstrIFB->removeIFBMonitorMember(bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(iSlotNum));
							ssl_ifbs.append(iIFBRecordIndex);
						}
					}
				}
			}
			else if ((sstr.find("ListenToPort") >= 0) || (sstr.find("LISTENTOPORT") >= 0)) {
				// should indicate the same assoc IFB - tie to this for list of keys to pass onto packager
				if (ssl.count()>1) {
					int iL2PIndx = ssl[1].toInt();
					CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record_FromL2P(pRevRec->getTrunkAddress(), iL2PIndx);
					if (pRMstrIFB) { // remove entry from list
						pRMstrIFB->removeListen2PortMonitorMember(bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(iSlotNum));
						ssl_LToP.append(iL2PIndx);
					}
				}
			}
			// xxx what of CallToPort ?? or TrunkCall ??

		} // for iC - prev

		// sort out curr key functions
		bncs_stringlist sslCurr = bncs_stringlist(pRevRec->getMonitoring_Function(), ',');
		for (int iCu = 0; iCu < sslCurr.count(); iCu++) {
			bncs_string sstr = sslCurr[iCu];
			bncs_stringlist ssl = bncs_stringlist(sstr, '|');
			//Debug("ProcessMonRev - curr ele %d str %s", iCu, LPCSTR(sstr));
			if (sstr.find("Conference") >= 0) {
				if (ssl.count()>1) {
					int iConfIndx = ssl[1].toInt();
					CRingMaster_CONF* pRMstrConf = GetRingMaster_CONF_Record(iConfIndx);
					if (pRMstrConf) { // add entry from list
						pRMstrConf->addFromMonitorMember(bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(iSlotNum));
						if (ssl_confs.find(iConfIndx)<0) ssl_confs.append(iConfIndx);
					}
				}
			}
			else if (sstr.find("TrunkIfb", 0, FALSE) >= 0) {
				if (ssl.count()>1) {
					bncs_stringlist ssl2 = bncs_stringlist(ssl[1], '.');
					if (ssl2.count() > 1) {
						int iIFBTrunk = ssl2[0].toInt();
						int iIFBIndx = ssl2[1].toInt();
						int iIFBRecordIndex = 0;
						if ((iIFBTrunk > 0) && (iIFBIndx > 0)) {
							CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iIFBTrunk);
							if (pRing) {
								iIFBRecordIndex = ((pRing->m_iRecordIndex - 1)*MAX_IFBS) + iIFBIndx;
								CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
								if (pRMstrIFB) { // remove entry from list
									pRMstrIFB->addIFBMonitorMember(bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(iSlotNum));
									if (ssl_ifbs.find(iIFBIndx)<0) ssl_ifbs.append(iIFBRecordIndex);
								}
							}
						}
						else
							Debug("ProcessMonRev-2-TrunkIfb invalid trunk %d ifb %d values from %s", iIFBTrunk, iIFBIndx, LPCSTR(pRevRec->getPrevious_Function()));
					}
				}
			}
			else if (sstr.find("IFB") >= 0) {
				if (ssl.count()>1) {
					int iIFBIndx = ssl[1].toInt();
					if ((iWhichRevRing > 0) && (iWhichRevRing <= iNumberRiedelRings)) {
						int iIFBRecordIndex = ((pRing->m_iRecordIndex - 1)*MAX_IFBS) + iIFBIndx;
						CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
						if (pRMstrIFB) { // add entry from list
							pRMstrIFB->addIFBMonitorMember(bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(iSlotNum));
							if (ssl_ifbs.find(iIFBIndx)<0) ssl_ifbs.append(iIFBRecordIndex);
						}
					}
				}
			}
			else if ((sstr.find("ListenToPort") >= 0)||(sstr.find("LISTENTOPORT") >= 0)) {
				// was LISTENTOPORT -- but revertive is "ListenToPort" 
				if (ssl.count()>1) {
					int iL2PIndx = ssl[1].toInt();
					CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record_FromL2P(pRevRec->getTrunkAddress(), iL2PIndx);
					if (pRMstrIFB) { // add entry from list
						pRMstrIFB->addListen2PortMonitorMember(bncs_string("%1.%2").arg(pRevRec->getTrunkAddress()).arg(iSlotNum));
						if (ssl_LToP.find(iL2PIndx)<0) ssl_LToP.append(iL2PIndx);
					}
				}
				// xxx what of CallToPort ?? or TrunkCall ?? or other parts of rev as seen in  CSQ???

			}
		} // for iCu - curr
		//
		// update conf revs
		for (int icc = 0; icc < ssl_confs.count(); icc++) {
			int iConff = ssl_confs[icc].toInt();
			if ((iConff>0) && (iConff < MAX_CONFERENCES)) {
				CRingMaster_CONF* pRec = GetRingMaster_CONF_Record(iConff);
				if (pRec) {
					bncs_string ssOut = pRec->getFromMonitorMembers();
					if (ssOut.length() > 255) ssOut.truncate(255);
					strcpy(szClientCmd, LPCSTR(ssOut));
					eiConferencesInfoId->updateslot(CONF_MON_PANEL_MEMBERSHIP + iConff, szClientCmd);
				}
			}
		}
		// update ifb rev
		for (int icf = 0; icf < ssl_ifbs.count(); icf++) {
			int iIfbrec = ssl_ifbs[icf].toInt();
			CRingMaster_IFB* pRec = GetRingMaster_IFB_Record(iIfbrec);
			if (pRec) {
				CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pRec->getAssociatedTrunkAddress());
				if (pRing) {
					bncs_string ssOut = pRec->getIFBMonitorMembers();
					if (ssOut.length() > 255) ssOut.truncate(255);
					strcpy(szClientCmd, LPCSTR(ssOut));
					if ((pRing->m_iRecordIndex > 0) && (pRing->m_iRecordIndex < MAX_RIEDEL_RINGS)) {
						if (eiIFBsInfoIds[pRing->m_iRecordIndex]) {
							int iactIfb = ((iIfbrec - 1) % MAX_IFBS) + 1;
							eiIFBsInfoIds[pRing->m_iRecordIndex]->updateslot(IFB_MON_PANEL_MEMBERSHIP + iactIfb, szClientCmd);
						}
					}
				}
			}
		}
		// update Listen2Port rev
		for (int icf = 0; icf < ssl_LToP.count(); icf++) {
			int iListenPort = ssl_LToP[icf].toInt();
			if ((iListenPort>0) && (iListenPort < MAX_RIEDEL_SLOTS)) {
				CRingMaster_IFB* pRec = GetRingMaster_IFB_Record_FromL2P(pRevRec->getTrunkAddress(), iListenPort);
				if (pRec) {
					if ((pRec->getAssociatedRemoteLogic() > 0) && (pRec->getAssociatedRemoteLogic() <= MAX_MIXERS_LOGIC)) {
						bncs_string ssOut = pRec->getListen2PortMonitorMembers();
						if (ssOut.length() > 255) ssOut.truncate(255);
						strcpy(szClientCmd, LPCSTR(ssOut));
						CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pRec->getAssociatedTrunkAddress());
						if (pRing) {
							if ((pRing->m_iRecordIndex > 0) && (pRing->m_iRecordIndex <= iNumberRiedelRings)) {
								if (eiIFBsInfoIds[pRing->m_iRecordIndex]) {
									eiIFBsInfoIds[pRing->m_iRecordIndex]->updateslot(RMSTR_LOGIC_INFO_START + pRec->getAssociatedRemoteLogic(), szClientCmd);
								}
							}
						}
					}
				}
			}
		}
		// xxx what of CallToPort ?? or TrunkCall ??
	}
}

void ProcessRiedelConferenceRev(int iRevRecordIndex, int iSlotNum)
{
	// go thru all conf records for rings and build ring master revertive + post that
	bncs_string ssMstrConfMembers="", ssMstrPanelMembers="", ssLabel="";
	int iConf = iSlotNum;
	int iRevtype = CONF_MBMR;
	if ((iSlotNum > 1000) && (iSlotNum < 2000)) {
		iConf = iSlotNum - 1000;    
		iRevtype = CONF_LABEL; // label
	}
	else if ((iSlotNum > 2000) && (iSlotNum < 3000)) {
		iConf = iSlotNum - 2000;   // panel members
		iRevtype = CONF_PANEL; // panel
	}
	CRingMaster_CONF* pRMstrConf = GetRingMaster_CONF_Record(iConf);
	if (pRMstrConf) {
		if (iRevtype == CONF_LABEL) {
			CRiedel_Conferences* pRevertive = GetRiedel_Conference_Record(iRevRecordIndex);
			if (pRevertive) pRMstrConf->storeRingMasterLabel(pRevertive->getConference_Name());
			// update conf slot with latest label revertive -- they are meant to be the same across all rings
			eiConferencesInfoId->updateslot(iSlotNum, LPCSTR(pRMstrConf->getRingMasterLabel()));
		}
		else {
			// for other rev types - collate all revs from all trunks to build r.mstr conference revertive
			bncs_stringlist ssl = bncs_stringlist(pRMstrConf->getAllRiedelRevIndicies(), ',');
			bncs_stringlist sslRev = bncs_stringlist("", ',');
			for (int iTrunk = 1; iTrunk < ssl.count(); iTrunk++) {
				if (ssl[iTrunk].toInt()>0) {
					// conf record entry - get it
					CRiedel_Conferences* pConf = GetRiedel_Conference_Record(ssl[iTrunk].toInt());
					if (pConf) {
						if (iRevtype == CONF_MBMR) {
							bncs_stringlist ssl1 = bncs_stringlist(pConf->getConference_Members(), ',');
							for (int iIndx = 0; iIndx < ssl1.count(); iIndx++) {
								if (ssl1[iIndx].toInt()>0) sslRev.append(bncs_string("%1.%2").arg(pConf->getTrunkAddress()).arg(ssl1[iIndx]));
							}
							pRMstrConf->storeRingMasterConfMembers(sslRev.toString(','));
						}
						else if (iRevtype == CONF_PANEL) {
							bncs_stringlist ssl2 = bncs_stringlist(pConf->getConference_PanelMembers(), ',');
							for (int iIndx = 0; iIndx < ssl2.count(); iIndx++) {
								if (ssl2[iIndx].toInt()>0) sslRev.append(bncs_string("%1.%2").arg(pConf->getTrunkAddress()).arg(ssl2[iIndx]));
							}
							pRMstrConf->storeRingMasterPanelMembers(sslRev.toString(','));
						}
					}
					else
						Debug("ProcessConfRev - no riedel conf rec for trunk %d, slot %d /conf %d", iTrunk, iSlotNum, iConf);
				}
			} // for itrunk
			// update relevant ring master conf infodriver slot
			bncs_string ssOut = sslRev.toString(',');
			if (ssOut.length() > 255) ssOut.truncate(255);
			strcpy(szClientCmd, LPCSTR(ssOut));
			eiConferencesInfoId->updateslot(iSlotNum, szClientCmd);
		}
	}
	else
		Debug("ProcessConfRev - no ring master conf record or REV record %d for slot %d /conf %d", iRevRecordIndex, iSlotNum, iConf);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void ProcessDifferencesIFBPorts(int iTrunk, int iWhichIFB, bncs_string ss_Previous, bncs_string ss_Current, int iInMMOrOut )
{
	bncs_stringlist ssl_prev = bncs_stringlist(ss_Previous, ',');
	bncs_stringlist ssl_curr = bncs_stringlist(ss_Current, ',');
	bncs_stringlist ssl_diffs = bncs_stringlist("", ',');
	//Debug("ProcDiffIFBOuts -comparing ports curr : %s   prev : %s", LPCSTR(ssl_curr.toString(',')), LPCSTR(ssl_prev.toString(',')));
	// find ports in prev that are now not in curr list of outputs
	for (int indx = 0; indx < ssl_prev.count(); indx++) {
		if (ssl_curr.find(ssl_prev[indx]) < 0) ssl_diffs.append(ssl_prev[indx]);
	}
	if (ssl_diffs.count()>0) {
		//Debug("ProcDiffIFBOuts - removed ports : %d : %s", ssl_diffs.count(), LPCSTR(ssl_diffs.toString(',')));
		// process diffs --- looking for dests no longer outputs that are highways - in order to clear them down
		for (int iDiff = 0; iDiff < ssl_diffs.count(); iDiff++) {
			CHighwayRecord* pHigh = NULL;
			if ((iInMMOrOut == 1) || (iInMMOrOut == 2)) GetHighwayRecordByToTrunkAndSrce(iTrunk, ssl_diffs[iDiff].toInt(), FALSE);
			else if (iInMMOrOut == 3) GetHighwayRecordByFromTrunkAndDest(iTrunk, ssl_diffs[iDiff].toInt(), FALSE);
			if (pHigh) { // clear down
				if ((pHigh->getHighwayState()>HIGHWAY_PENDING) && 
					(pHigh->getHighwayAllocatedIndex()>IFB_HIGHWAY_BASE)&&(pHigh->getNumberWorkingModeEntries()>0)) {
					//Debug("ProcDiffIFBOuts - highway %d - trunk %d dest %d - clearing from ifb %d", pHigh->getRecordIndex(), iTrunk, ssl_diffs[iDiff].toInt(), iWhichIFB);
					if ((iInMMOrOut == 1) || (iInMMOrOut == 2)) pHigh->removeWorkingMode(MODE_IFBINPUT, iWhichIFB);
					else if (iInMMOrOut == 3) pHigh->removeWorkingMode(MODE_IFBOUTPUT, iWhichIFB);
					if (pHigh->getNumberWorkingModeEntries()==0) ParkHighway(pHigh->getRecordIndex());
				}
				//else
				//	Debug("ProcDiffIFBPorts - highway %d state %d alloc %d - trunk %d dest %d - not in ifb use ?", pHigh->getRecordIndex(), 
				//			pHigh->getHighwayState(), pHigh->getHighwayAllocatedIndex(), iTrunk, ssl_diffs[iDiff].toInt());
			}
		} // for idiff
	}
}



void ProcessRiedelIFBRev(int iRevRecordIndex, int iWhichRevRing, int iSlotNum)
{
	// check that rev is for ring associated to ifb ??? -- why process for ifb for other ( non assoc ) ring revertives ??
	// check what has changed from previous revertive -- if new ports update rings  revertive, maybe need to clear down used highways etc
	//
	// info device will determine IFB record -- happens to be same as RevRecordIndex now
	//
	int iWhichIFB = iSlotNum;
	CRingMaster_IFB* pRMstrIfb = GetRingMaster_IFB_Record(iRevRecordIndex);   // rev record index is now same index into rmstr ifbs
	CRiedel_Rings* pRing = GetRiedel_Rings_Record(iWhichRevRing);
	if (pRMstrIfb&&pRing) {
		int iTrunkAddress = pRMstrIfb->getAssociatedTrunkAddress();
		//if (!bAutomaticStarting) Debug("ProcRiedelIFBRev rec %d ring %d trunk %d slot %d", iRevRecordIndex, iWhichRevRing, iTrunkAddress, iSlotNum);
		bncs_stringlist sslInputs = bncs_stringlist("", ',');
		bncs_stringlist sslMMinus = bncs_stringlist("", ',');
		bncs_stringlist sslOutputs = bncs_stringlist("", ',');
		bncs_string ssLabel = "";
		CRiedel_IFBs* pIfbrev = GetRiedel_IFBs_Record(iRevRecordIndex);
			// for inputs and mix minus -- could have highways now
		if (pIfbrev) {
			// 1. has there been a change in list of inputs
			if ((pIfbrev->getIFB_PreviousRev(1) != pIfbrev->getIFB_Inputs()) && (pIfbrev->getIFB_PreviousRev(1).length() > 0)) {  // some difference
				ProcessDifferencesIFBPorts(iTrunkAddress, iRevRecordIndex, pIfbrev->getIFB_PreviousRev(1), pIfbrev->getIFB_Inputs(), 1);
			}
			// now current Inputs
			bncs_stringlist ssl1 = bncs_stringlist(pIfbrev->getIFB_Inputs(), ',');
			for (int iIndx = 0; iIndx < ssl1.count(); iIndx++) {
				// need to check if source port is incoming highway 
				CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunkAddress);
				int iTrunkGrd = 0;
				if (pRing) iTrunkGrd = pRing->iRing_Device_GRD;
				CHighwayRecord* pHigh = GetHighwayRecordByToRtrSrce(iTrunkGrd, ssl1[iIndx].toInt());
				if (pHigh) {

					if ((pHigh->getHighwayState() == HIGHWAY_IFBMXRIN) && (pHigh->getHighwayAllocatedIndex()>IFB_HIGHWAY_BASE)) {
						int iPrimaryTrunk = pHigh->getHighwayAllocatedIndex() / IFB_HIGHWAY_BASE;
						int iiPrimarySrce = pHigh->getHighwayAllocatedIndex() % IFB_HIGHWAY_BASE;
						//Debug("ProcRiedelIFBRev - ifb %d highway input trunk %d %d alloc index %d -> %d %d", iRevRecordIndex, iTrunkAddress, ssl1[iIndx].toInt(),
						//	pHigh->getHighwayAllocatedIndex(), iPrimaryTrunk, iiPrimarySrce);
						if ((iPrimaryTrunk > 0) && (iiPrimarySrce > 0)) {
							// test to see if trunk.port already in ifb list
							if (sslInputs.find(bncs_string("%1.%2").arg(iPrimaryTrunk).arg(iiPrimarySrce)) < 0)
								sslInputs.append(bncs_string("%1.%2").arg(iPrimaryTrunk).arg(iiPrimarySrce));
						}
					}
					else {
						if (pHigh->getHighwayState() > HIGHWAY_CLEARING) {
							// repair in this instance -- trace back to primary trunk / srce 
							iTraceCount = 0;
							ssl_Calc_Trace = bncs_stringlist("", '|');  // clear global var 
							int iTracedTrunk = pHigh->getToTrunkAddr();
							int iTracedGrd = pHigh->getToRouter();
							int iTracedSrc = pHigh->getToSource();
							int iCalcUser = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedSrc;
							if (TraceThePrimarySource(&iTracedTrunk, &iTracedGrd, &iTracedSrc) ) {
								iCalcUser = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedSrc;
								Debug("ProcRiedelIFBRev -WARN- repair traced primary User srce - setting user as %d from %d %d ", iCalcUser, iTracedTrunk, iTracedSrc);
								if (sslInputs.find(bncs_string("%1.%2").arg(iTracedTrunk).arg(iTracedSrc)) < 0) sslInputs.append(bncs_string("%1.%2").arg(iTracedTrunk).arg(iTracedSrc));
								pHigh->setHighwayState(HIGHWAY_IFBMXRIN, iCalcUser, 0);
								pHigh->addWorkingMode(MODE_IFBINPUT, iRevRecordIndex);
							}
							else {
								if (sslInputs.find(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl1[iIndx])) < 0)
									sslInputs.append(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl1[iIndx]));
								Debug("PreProcessIFB -WARNING- could NOT trace Primary Srce - setting user as %d from %d %d ", iCalcUser, iTracedTrunk, iTracedSrc);
								bncs_string ssmsg = bncs_string("PreProcessIFB - ifb %1 - could NOT trace Primary Srce - setting user as %2 from %3 %4 ").arg(iRevRecordIndex).arg(iCalcUser).arg(iTracedTrunk).arg(iTracedSrc);
								// too severe ??? PostAutomaticErrorMessage(iUserTrunk, ssmsg);
								// park this highway then -- could still be pending on forthcoming revs ?
							}
						}
						else
							Debug("ProcRiedelIFBRev -WARN- ifb %d highway from %d %d -but NOT IFBUSE- state %d allocsrc %d", iRevRecordIndex, iTrunkAddress, ssl1[iIndx].toInt(), pHigh->getHighwayState(), pHigh->getHighwayAllocatedIndex());
					}
				}
				else {
					if (ssl1[iIndx].toInt() > 0) {
						if (sslInputs.find(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl1[iIndx])) < 0)
							sslInputs.append(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl1[iIndx]));
					}
				}
			} // for indx 


			// 2. has there been a change in list of mixminus
			if ((pIfbrev->getIFB_PreviousRev(2) != pIfbrev->getIFB_MixMinus()) && (pIfbrev->getIFB_PreviousRev(2).length() > 0)) {  // some difference
				ProcessDifferencesIFBPorts(iTrunkAddress, iRevRecordIndex, pIfbrev->getIFB_PreviousRev(2), pIfbrev->getIFB_MixMinus(), 2);
			}

			// now current mix minus
			bncs_stringlist ssl2 = bncs_stringlist(pIfbrev->getIFB_MixMinus(), ',');
			for (int iIndx = 0; iIndx < ssl2.count(); iIndx++) {
				// need to check if source port is incoming highway 
				CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunkAddress);
				int iTrunkGrd = 0;
				if (pRing) iTrunkGrd = pRing->iRing_Device_GRD;
				CHighwayRecord* pHigh = GetHighwayRecordByToRtrSrce(iTrunkGrd, ssl2[iIndx].toInt());
				if (pHigh) {
					if ((pHigh->getHighwayState() == HIGHWAY_IFBMXRIN) && (pHigh->getHighwayAllocatedIndex()>IFB_HIGHWAY_BASE)) {
						int iPrimaryTrunk = pHigh->getHighwayAllocatedIndex() / IFB_HIGHWAY_BASE;
						int iiPrimarySrce = pHigh->getHighwayAllocatedIndex() % IFB_HIGHWAY_BASE;
						//Debug("ProcRiedelIFBRev - ifb %d highway mixminus trunk %d %d alloc index %d -> %d %d", iRevRecordIndex, iTrunkAddress, ssl2[iIndx].toInt(),
						//	pHigh->getHighwayAllocatedIndex(), iPrimaryTrunk, iiPrimarySrce);
						if ((iPrimaryTrunk > 0) && (iiPrimarySrce > 0)) {
							// test to see if trunk.port already in ifb list
							if (sslMMinus.find(bncs_string("%1.%2").arg(iPrimaryTrunk).arg(iiPrimarySrce)) < 0)
								sslMMinus.append(bncs_string("%1.%2").arg(iPrimaryTrunk).arg(iiPrimarySrce));
						}
					}
					else {
						if (pHigh->getHighwayState() > HIGHWAY_CLEARING) {
							// repair in this instance -- trace back to primary trunk / srce 
							iTraceCount = 0;
							ssl_Calc_Trace = bncs_stringlist("", '|');  // clear global var 
							int iTracedTrunk = pHigh->getToTrunkAddr();
							int iTracedGrd = pHigh->getToRouter();
							int iTracedSrc = pHigh->getToSource();
							int iCalcUser = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedSrc;
							if (TraceThePrimarySource(&iTracedTrunk, &iTracedGrd, &iTracedSrc) ) {
								iCalcUser = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedSrc;
								Debug("ProcRiedelIFBRev -WARN- repair MM traced primary User srce - setting MM user as %d from %d %d ", iCalcUser, iTracedTrunk, iTracedSrc);
								if (sslMMinus.find(bncs_string("%1.%2").arg(iTracedTrunk).arg(iTracedSrc)) < 0) sslMMinus.append(bncs_string("%1.%2").arg(iTracedTrunk).arg(iTracedSrc));
								pHigh->setHighwayState(HIGHWAY_IFBMXRIN, iCalcUser, 0);
								pHigh->addWorkingMode(MODE_IFBINPUT, iRevRecordIndex);
							}
							else {
								if (sslMMinus.find(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl2[iIndx])) < 0)
									sslMMinus.append(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl2[iIndx]));
								Debug("PreProcessIFB -WARNING- MM could NOT trace Primary Srce - setting MM user as %d from %d %d ", iCalcUser, iTracedTrunk, iTracedSrc);
								bncs_string ssmsg = bncs_string("PreProcessIFB - ifb %1 - MM could NOT trace Primary Srce - setting MM user as %2 from %3 %4 ").arg(iRevRecordIndex).arg(iCalcUser).arg(iTracedTrunk).arg(iTracedSrc);
								// too severe ??? PostAutomaticErrorMessage(iUserTrunk, ssmsg);
								// park this highway then -- could still be pending on forthcoming revs ?
							}
						}
						else
							Debug("ProcRiedelIFBRev -WARN- ifb %d highway from %d %d -but NOT IFBUSE- state %d allocsrc %d", iRevRecordIndex, iTrunkAddress, ssl2[iIndx].toInt(), pHigh->getHighwayState(), pHigh->getHighwayAllocatedIndex());
					}
				}
				else {
					if (ssl2[iIndx].toInt() > 0) {
						if (sslMMinus.find(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl2[iIndx])) < 0)
							sslMMinus.append(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl2[iIndx]));
					}
				}
			} // for indx 

			// 3. has there been a change in list of outputs
			if ((pIfbrev->getIFB_PreviousRev(3) != pIfbrev->getIFB_Outputs()) && (pIfbrev->getIFB_PreviousRev(3).length() > 0)) {  // some difference
				ProcessDifferencesIFBPorts(iTrunkAddress, iRevRecordIndex, pIfbrev->getIFB_PreviousRev(3), pIfbrev->getIFB_Outputs(), 3);
			}
			// now current outputs 
			bncs_stringlist ssl3 = bncs_stringlist(pIfbrev->getIFB_Outputs(), ',');
			for (int iIndx = 0; iIndx < ssl3.count(); iIndx++) {
				// need to check if dest port is a highway 
				CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunkAddress);
				int iTrunkGrd = 0;
				if (pRing) iTrunkGrd = pRing->iRing_Device_GRD;
				CHighwayRecord* pHigh = GetHighwayRecordByFromRtrDest(iTrunkGrd, ssl3[iIndx].toInt());
				if (pHigh) {
					if ((pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) && (pHigh->getHighwayAllocatedIndex()>IFB_HIGHWAY_BASE)) {
						int iForwardTrunk = pHigh->getHighwayAllocatedIndex() / IFB_HIGHWAY_BASE;
						int iForwardDest = pHigh->getHighwayAllocatedIndex() % IFB_HIGHWAY_BASE;
						//Debug("ProcRiedelIFBRev - ifb %d highway output trunk %d %d allocsrc %d -> %d %d", iRevRecordIndex, iTrunkAddress, ssl3[iIndx].toInt(),
						//	pHigh->getHighwayAllocatedIndex(), iForwardTrunk, iForwardDest);
						if ((iForwardTrunk > 0) && (iForwardDest > 0)) {
							// test to see if trunk.port already in ifb list
							if (sslOutputs.find(bncs_string("%1.%2").arg(iForwardTrunk).arg(iForwardDest)) < 0)
								sslOutputs.append(bncs_string("%1.%2").arg(iForwardTrunk).arg(iForwardDest));
						}
					}
					else {
						if (pHigh->getHighwayState() > HIGHWAY_CLEARING) {
							// repair in this instance -- trace forward to final trunk /dest 
							int iUserTrunk = pHigh->getFromTrunkAddr();
							int iUserDest = pHigh->getFromDestination();
							int iUserGrd = pHigh->getFromRouter();
							int iCalcUser = (iUserTrunk*IFB_HIGHWAY_BASE) + iUserDest;
							iTraceCount = 0;
							if (TraceIFBMixerFinalDestination(MODE_IFBOUTPUT, iRevRecordIndex, &iUserTrunk, &iUserGrd, &iUserDest) ) {
								iCalcUser = (iUserTrunk*IFB_HIGHWAY_BASE) + iUserDest;
								Debug("ProcRiedelIFBRev -WARN- repair traced Final User dest - setting user as %d from %d %d ", iCalcUser, iUserTrunk, iUserDest);
								if (sslOutputs.find(bncs_string("%1.%2").arg(iUserTrunk).arg(iUserDest)) < 0) sslOutputs.append(bncs_string("%1.%2").arg(iUserTrunk).arg(iUserDest));
								pHigh->setHighwayState(HIGHWAY_IFBMXROUT, iCalcUser, 3);
								pHigh->addWorkingMode(MODE_IFBOUTPUT, iRevRecordIndex);
							}
							else {
								if (sslOutputs.find(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl3[iIndx])) < 0)
									sslOutputs.append(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl3[iIndx]));
								Debug("PreProcessIFB -WARNING- could NOT trace Final User dest - setting user as %d from %d %d ", iCalcUser, iUserTrunk, iUserDest);
								bncs_string ssmsg = bncs_string("PreProcessIFB - ifb %1 - could NOT trace Final Dest - setting user as %2 from %3 %4 ").arg(iRevRecordIndex).arg(iCalcUser).arg(iUserTrunk).arg(iUserDest);
								// too severe ??? PostAutomaticErrorMessage(iUserTrunk, ssmsg);
								// park this highway then -- could still be pending on forthcoming revs ?
							}
						}
						else
							Debug("ProcRiedelIFBRev -WARN- ifb %d highway from %d %d -but NOT IFBUSE- state %d allocsrc %d", iRevRecordIndex, iTrunkAddress, ssl3[iIndx].toInt(), pHigh->getHighwayState(), pHigh->getHighwayAllocatedIndex());
					}
				}
				else {
					if (ssl3[iIndx].toInt() > 0) {
						if (sslOutputs.find(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl3[iIndx])) < 0)
							sslOutputs.append(bncs_string("%1.%2").arg(pIfbrev->getTrunkAddress()).arg(ssl3[iIndx]));
					}
				}
			} // for iIndex
			// get label
			if (pIfbrev->getIFB_Label().length() > 0) ssLabel = pIfbrev->getIFB_Label();
		}
		// now update r mstr infodriver slot
		char szCommand[MAX_AUTOBFFR_STRING] = "";
		wsprintf(szCommand, "%s|%s|%s|%s", LPCSTR(sslInputs.toString(',')), LPCSTR(sslMMinus.toString(',')), LPCSTR(sslOutputs.toString(',')), LPCSTR(ssLabel));
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pRMstrIfb->getAssociatedTrunkAddress());
		if (pRing) {
			if ((pRing->m_iRecordIndex > 0) && (pRing->m_iRecordIndex < MAX_RIEDEL_RINGS)) {
				if (eiIFBsInfoIds[pRing->m_iRecordIndex]) {
					eiIFBsInfoIds[pRing->m_iRecordIndex]->updateslot(iWhichIFB, szCommand);
				}
			}
		}
		// store rmstr ifb slot contents in record
		pRMstrIfb->storeRingMasterRevertive(bncs_string(szCommand));
		pRMstrIfb->setIFBProcessingFlag(FALSE);
		//Debug("ProcessIFBRevertive -FINISHED for - ifb %d", iRevRecordIndex);

	}
	else
		Debug("ProcessIFBRev - no ring master ifb record or REV record for slot/ifb %d == ifb rec indx %d", iSlotNum, iRevRecordIndex);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void ProcessDifferencesMIXERPorts(int iTrunk, int iWhichMixer, bncs_string ss_Previous, bncs_string ss_Current, int iInOrOut)
{
	bncs_stringlist ssl_prev = bncs_stringlist(ss_Previous, ',');
	bncs_stringlist ssl_curr = bncs_stringlist(ss_Current, ',');
	bncs_stringlist ssl_diffs = bncs_stringlist("", ',');
	// find ports in prev that are now NOT in curr list
	for (int indx = 0; indx < ssl_prev.count(); indx++) {
		BOOL bSndChnl = FALSE;
		int iPrevPort = 0;
		bncs_string ssDb = "";
		GetPortAnddBFromString(ssl_prev[indx], &iPrevPort, &bSndChnl);
		if (iInOrOut == 1) {
			if (iPrevPort>0) {
				if (ssl_curr.find(bncs_string("%1:").arg(iPrevPort)) < 0) ssl_diffs.append(iPrevPort);
			}
		}
		else {
			iPrevPort = ssl_prev[indx].toInt();
			if (iPrevPort>0) {
				bncs_string sstr = bncs_string("%1").arg(iPrevPort);
				if (bSndChnl) sstr.append(bncs_string("s"));
				if (ssl_curr.find(sstr) < 0) ssl_diffs.append(sstr);
			}
		}
	}
	if (ssl_diffs.count()>0) {
		// process diffs --- looking for dests no longer outputs that are highways - in order to clear them down
		for (int iDiff = 0; iDiff < ssl_diffs.count(); iDiff++) {
			CHighwayRecord* pHigh = NULL;
			if (iInOrOut == 1)  GetHighwayRecordByToTrunkAndSrce(iTrunk, ssl_diffs[iDiff].toInt(), FALSE);
			else if (iInOrOut == 2) GetHighwayRecordByFromTrunkAndDest(iTrunk, ssl_diffs[iDiff].toInt(), FALSE);
			if (pHigh) { // clear down
				if ((pHigh->getHighwayState()>HIGHWAY_PENDING) &&
					(pHigh->getHighwayAllocatedIndex()>IFB_HIGHWAY_BASE) && (pHigh->getNumberWorkingModeEntries()>0)) {
					if (iInOrOut == 1) pHigh->removeWorkingMode(MODE_MIXERIN, iWhichMixer);
					else if (iInOrOut == 2) pHigh->removeWorkingMode(MODE_MIXEROUT, iWhichMixer);
					 if (pHigh->getNumberWorkingModeEntries() == 0) ParkHighway(pHigh->getRecordIndex());
				}
				else
					Debug("ProcDiffMIXERPorts - highway %d state %d alloc %d - trunk %d dest %d - not in ifb use ?", pHigh->getRecordIndex(),
							pHigh->getHighwayState(), pHigh->getHighwayAllocatedIndex(), iTrunk, ssl_diffs[iDiff].toInt());
			}
		} // for idiff
	}
}


void ProcessRiedelPTIRev(int iRevRecordIndex, int iWhichRevRing, int iSlotNum)
{
	// as update function does both in and out revs for pti -- iptiport needs to be in range 1-2000
	int iPTIPort = iSlotNum;
	if (iSlotNum > MAX_RIEDEL_PTI)  iPTIPort = iSlotNum - MAX_RIEDEL_PTI;
	UpdateRingMasterPortPti(iPTIPort);

}


void ProcessRiedelMIXERRev(int iRevRecordIndex, int iWhichRevRing, int iSlotNum)
{
	int iWhichMixer = iSlotNum - RIEDELDRIVER_MIXERS_INFO_START;
	// note revrecordIndex now matches the ringmaster mixer record too - as per ring
	CRingMaster_MIXER* pRMstrMxr = GetRingMaster_MIXER_Record(iRevRecordIndex);
	CRiedel_Mixers* pMxrRev = GetRiedel_Mixers_Record(iRevRecordIndex);
	CRiedel_Rings* pRing = GetRiedel_Rings_Record(iWhichRevRing);
	if (pRing&&pRMstrMxr&&pMxrRev) {
		if (pRMstrMxr->getAssociatedTrunkAddress() == pRing->m_iTrunkAddress) {  // is this really right stategy ???
			bncs_stringlist sslRMstrInputs = bncs_stringlist("", ',');    // to build up ring master string rev
			bncs_stringlist sslRMstrOutputs = bncs_stringlist("", ',');
			// go thru inputs - what has changed ports and or dB - verify highways where required 
			bncs_stringlist sslInputs = bncs_stringlist(pMxrRev->getMixer_Inputs(), ',');
			if (pMxrRev->AreChangesinRevertive(1) ) {
				bncs_stringlist sslPrevInputs = bncs_stringlist(pMxrRev->getMixer_PreviousRev(1), ',');
				// 1. has there been a change in list of outputs
				if (sslPrevInputs.count()>0) {  // some difference
					ProcessDifferencesMIXERPorts(pRing->m_iTrunkAddress, iRevRecordIndex, pMxrRev->getMixer_PreviousRev(1), pMxrRev->getMixer_Inputs(), 1);
				}
			}
			// now go thru inputs and check highways as appropriate
			for (int iIndx = 0; iIndx < sslInputs.count(); iIndx++) {
				// need to check if dest port is a highway 
				BOOL bSndChnl = FALSE;
				int iCurrentPort = 0;
				bncs_string ssDb = GetPortAnddBFromString(sslInputs[iIndx], &iCurrentPort, &bSndChnl);
				CHighwayRecord* pHigh = GetHighwayRecordByToRtrSrce(pRing->iRing_Device_GRD, iCurrentPort);
				if (pHigh) {
					if ((pHigh->getHighwayState() == HIGHWAY_IFBMXRIN) && (pHigh->getHighwayAllocatedIndex()>IFB_HIGHWAY_BASE)) {
						int iTracedTrunk = pHigh->getHighwayAllocatedIndex() / IFB_HIGHWAY_BASE;
						int iTracedPort = pHigh->getHighwayAllocatedIndex() % IFB_HIGHWAY_BASE;
						Debug("ProcRiedelMIXERRev - mixer %d highway input trunk %d %d allocsrc %d -> %d %d", iRevRecordIndex, pRing->m_iTrunkAddress, sslInputs[iIndx].toInt(),
							pHigh->getHighwayAllocatedIndex(), iTracedTrunk, iTracedPort);
						if ((iTracedTrunk > 0) && (iTracedPort > 0)) {
							// test to see if trunk.port already in ifb list
							if (sslRMstrInputs.find(bncs_string("%1.%2:").arg(iTracedTrunk).arg(iTracedPort))<0) 
								sslRMstrInputs.append(bncs_string("%1.%2:%3").arg(iTracedTrunk).arg(iTracedPort).arg(ssDb));
						}
					}
					else {
						if (pHigh->getHighwayState() > HIGHWAY_CLEARING) { // repair in this instance -- trace back to primary trunk /dest 
							int iTracedTrunk = pHigh->getToTrunkAddr();
							int iTracedGrd = pHigh->getToRouter();
							int iTracedPort = pHigh->getToSource();
							int iCalcUser = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedPort;
							iTraceCount = 0;
							ssl_Calc_Trace = bncs_stringlist("", '|');  // clear global var 
							if (TraceThePrimarySource(&iTracedTrunk, &iTracedGrd, &iTracedPort) ) {
								iCalcUser = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedPort;
								if (sslRMstrInputs.find(bncs_string("%1.%2:").arg(iTracedTrunk).arg(iTracedPort)) < 0)
									sslRMstrInputs.append(bncs_string("%1.%2:%3").arg(iTracedTrunk).arg(iTracedPort).arg(ssDb));
								pHigh->setHighwayState(HIGHWAY_IFBMXRIN, iCalcUser, 0);
								pHigh->addWorkingMode(MODE_MIXERIN, iRevRecordIndex);
								Debug("ProcRiedelMIXERRev -WARN- repair traced user as %d from %d %d ", iCalcUser, iTracedTrunk, iTracedPort);
							}
							else {
								if (sslRMstrInputs.find(bncs_string("%1.%2").arg(iTracedTrunk).arg(iTracedPort)) < 0)
									sslRMstrInputs.append(bncs_string("%1.%2:%3").arg(iTracedTrunk).arg(iTracedPort).arg(ssDb));
								Debug("ProcRiedelMIXERRev -WARNING- could NOT trace Final User dest - setting user as %d from %d %d ", iCalcUser, iTracedTrunk, iTracedPort);
								//bncs_string ssmsg = bncs_string("ProcRiedelMIXERRev - Mixer %1 - could NOT trace Final Dest - setting user as %2 from %3 %4 ").arg(iWhichMixer).arg(iCalcUser).arg(iTracedTrunk).arg(iTracedPort);
								// too severe ??? PostAutomaticErrorMessage(iUserTrunk, ssmsg);
								// park this highway then -- could still be pending on forthcoming revs ?
							}
						}
						else
							Debug("ProcRiedelMIXERRev -WARN- Mixer %d highway from %d %d -but NOT IFBUSE- state %d allocsrc %d", 
							iRevRecordIndex, pRing->m_iTrunkAddress, sslInputs[iIndx].toInt(), pHigh->getHighwayState(), pHigh->getHighwayAllocatedIndex());
					}
				}
				else {
					if (iCurrentPort>0) {
						if (sslRMstrInputs.find(bncs_string("%1.%2:").arg(pMxrRev->getTrunkAddress()).arg(iCurrentPort))<0)
							sslRMstrInputs.append(bncs_string("%1.%2:%3").arg(pMxrRev->getTrunkAddress()).arg(iCurrentPort).arg(ssDb));
					}
					else {
						sslRMstrInputs.append(bncs_string("0.0:MUTE"));
					}
				}
			} // for iIndex

			// go thru outputs - what has changed - verify highways where required
			bncs_stringlist sslOutputs = bncs_stringlist(pMxrRev->getMixer_Outputs(), ',');
			if (pMxrRev->AreChangesinRevertive(2) ) {
				bncs_stringlist sslPrevOutputs = bncs_stringlist(pMxrRev->getMixer_PreviousRev(2), ',');
				// 1. has there been a change in list of outputs
				if (sslPrevOutputs.count()>0) {  // some difference
					ProcessDifferencesMIXERPorts(pRing->m_iTrunkAddress, iRevRecordIndex, pMxrRev->getMixer_PreviousRev(2), pMxrRev->getMixer_Outputs(), 2);
				}
			}
			// now go thru outputs and check highways as appropriate
			for (int iIndx = 0; iIndx < sslOutputs.count(); iIndx++) {
				// need to check if dest port is a highway 
				BOOL bSecondChannel = FALSE;
				bncs_string sstr = sslOutputs[iIndx];
				if (sstr.find('s') >= 0) bSecondChannel = TRUE;
				int iCurrentPort = sslOutputs[iIndx].toInt();
				CHighwayRecord* pHigh = GetHighwayRecordByFromRtrDest(pRing->iRing_Device_GRD, iCurrentPort);
				if (pHigh) {
					if ((pHigh->getHighwayState() == HIGHWAY_IFBMXROUT) && (pHigh->getHighwayAllocatedIndex()>IFB_HIGHWAY_BASE)) {
						int iTracedTrunk = pHigh->getHighwayAllocatedIndex() / IFB_HIGHWAY_BASE;
						int iTracedPort = pHigh->getHighwayAllocatedIndex() % IFB_HIGHWAY_BASE;
						if ((iTracedTrunk > 0) && (iTracedPort > 0)) sslRMstrOutputs.append(bncs_string("%1.%2").arg(iTracedTrunk).arg(iTracedPort));
						Debug("ProcRiedelMIXERRev - mixer %d highway output trunk %d %d allocsrc %d -> %d %d", iRevRecordIndex, pRing->m_iTrunkAddress, sslOutputs[iIndx].toInt(),
							pHigh->getHighwayAllocatedIndex(), iTracedTrunk, iTracedPort);
					}
					else {
						if (pHigh->getHighwayState() > HIGHWAY_CLEARING) { // repair in this instance -- trace back to primary trunk /dest 
							int iTracedTrunk = pHigh->getFromTrunkAddr();
							int iTracedGrd = pHigh->getFromRouter();
							int iTracedPort = pHigh->getFromDestination();
							int iCalcUser = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedPort;
							iTraceCount = 0;
							if (TraceIFBMixerFinalDestination(MODE_MIXEROUT, iRevRecordIndex, &iTracedTrunk, &iTracedGrd, &iTracedPort) ) {
								iCalcUser = (iTracedTrunk*IFB_HIGHWAY_BASE) + iTracedPort;
								sslRMstrOutputs.append(bncs_string("%1.%2").arg(iTracedTrunk).arg(iTracedPort));
								pHigh->setHighwayState(HIGHWAY_IFBMXROUT, iCalcUser, 2);
								pHigh->addWorkingMode(MODE_MIXEROUT, iRevRecordIndex);
								Debug("ProcRiedelMIXERRev -WARN- repair traced user as %d from %d %d ", iCalcUser, iTracedTrunk, iTracedPort);
							}
							else {
								sslRMstrOutputs.append(bncs_string("%1.%2").arg(iTracedTrunk).arg(iTracedPort));
								Debug("ProcRiedelMIXERRev -WARNING- could NOT trace Final User dest - setting user as %d from %d %d ", iCalcUser, iTracedTrunk, iTracedPort);
								//bncs_string ssmsg = bncs_string("ProcRiedelMIXERRev - Mixer %1 - could NOT trace Final Dest - setting user as %2 from %3 %4 ").arg(iRevRecordIndex).arg(iCalcUser).arg(iTracedTrunk).arg(iTracedPort);
								// too severe ??? PostAutomaticErrorMessage(iUserTrunk, ssmsg);
								// park this highway then -- could still be pending on forthcoming revs ?
							}
						}
						else
							Debug("ProcRiedelMIXERRev -WARN- Mixer %d highway from %d %d -but NOT IFBUSE- state %d allocsrc %d",
							iRevRecordIndex, pRing->m_iTrunkAddress, sslOutputs[iIndx].toInt(), pHigh->getHighwayState(), pHigh->getHighwayAllocatedIndex());
					}
				}
				else {
					if (iCurrentPort > 0) {
						bncs_string sstr = bncs_string("%1.%2").arg(pMxrRev->getTrunkAddress()).arg(iCurrentPort);
						if (bSecondChannel) sstr.append("s");
						sslRMstrOutputs.append(sstr);
					}
					else {
						sslRMstrOutputs.append(bncs_string("0.0"));
					}
				}
			} // for iIndex

			// build revised Ringmaster Rev to broadcast
			char szRevertive[MAX_AUTOBFFR_STRING] = "";
			wsprintf(szRevertive, "%s|%s", LPCSTR(sslRMstrInputs.toString(',')), LPCSTR(sslRMstrOutputs.toString(',')));
			// store rev in ring master record
			pRMstrMxr->storeRingMasterRevertive(bncs_string(szRevertive));
			// update slot too
			if ((iWhichRevRing > 0) && (iWhichRevRing <= iNumberRiedelRings)) {
				if (eiIFBsInfoIds[iWhichRevRing]) {
					eiIFBsInfoIds[iWhichRevRing]->updateslot(RMSTR_MIXERS_INFO_START + iWhichMixer, szRevertive);
				}
			}

		}
		else
			Debug("ProcessRiedelMixerRev -warn- ignoring rev from NON-ASSOC ring %d for mixer %d (trunk %d )", iWhichRevRing, iWhichMixer, pRMstrMxr->getAssociatedTrunkAddress());
	}
	else
		Debug("ProcessRiedelMixerRev -error- no mixer or rev record for slot number %d - mixer %d rev record %d", iSlotNum, iWhichMixer, iRevRecordIndex);

}


void ProcessRiedelLOGICRev(int iRevRecordIndex, int iWhichRing, int iSlotNum)
{
	int iWhichLogic = iSlotNum - RIEDELDRIVER_LOGIC_INFO_START;
	CRingMaster_LOGIC* pRMstrLogic = GetRingMaster_LOGIC_Record(iRevRecordIndex);
	CRiedel_Logic* pLogicRev = GetRiedel_Logic_Record(iRevRecordIndex);
	CRiedel_Rings* pRing = GetRiedel_Rings_Record(iWhichRing);
	if (pRing&&pRMstrLogic&&pLogicRev) {
		// rev is a 1 or 0 for on or off ???
		if ((iWhichRing > 0) && (iWhichRing <= iNumberRiedelRings)) {
			pRMstrLogic->storeRingMasterRevertive(bncs_string(pLogicRev->getLogicState()));
			if (eiIFBsInfoIds[iWhichRing]) {
				eiIFBsInfoIds[iWhichRing]->updateslot(RMSTR_LOGIC_INFO_START + iWhichLogic, LPCSTR(pRMstrLogic->getRingMasterRevertive()));
			}
		}
	}
	else
		Debug("ProcessRiedelLogicRev -error- no logic or rev record for slot number %d - logic %d rev record %d", iSlotNum, iWhichLogic, iRevRecordIndex);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////// 
// start of processing control infodriver revs  
//
//
BOOL StoreControlInfoDrvRevertive(int iSlotDest, bncs_string ssRevertiveContent)
{

	switch (iSlotDest) {
	case CTRL_INFO_RMSTR_AREA1_BASE + CTRL_INFO_TXRX_TIKTOK:
		if (!AreBncsStringsEqual(ssRevertiveContent, ssAREA1_TX_Revertive)) {
			// rev will change if site up and tic-toking - else something is wrong
			ssAREA1_TX_Revertive = ssRevertiveContent;
			iAREA1_TXRX_TicTocStatus = ssAREA1_TX_Revertive.toInt();
		}
		break;

	case CTRL_INFO_RMSTR_AREA1_BASE + CTRL_INFO_RXONLY_TIKTOK:
		if (!AreBncsStringsEqual(ssRevertiveContent, ssAREA1_RX_Revertive)) {
			ssAREA1_RX_Revertive = ssRevertiveContent;
			iAREA1_RXONLY_TicTocStatus = ssAREA1_RX_Revertive.toInt();
		}
		break;

	case CTRL_INFO_RMSTR_AREA2_BASE + CTRL_INFO_TXRX_TIKTOK:
		if (!AreBncsStringsEqual(ssRevertiveContent, ssAREA2_TX_Revertive)) {
			ssAREA2_TX_Revertive = ssRevertiveContent;
			iAREA2_TXRX_TicTocStatus = ssAREA2_TX_Revertive.toInt();
		}
		break;

	case CTRL_INFO_RMSTR_AREA2_BASE + CTRL_INFO_RXONLY_TIKTOK:
		if (!AreBncsStringsEqual(ssRevertiveContent, ssAREA2_RX_Revertive)) {
			ssAREA2_RX_Revertive = ssRevertiveContent;
			iAREA2_RXONLY_TicTocStatus = ssAREA2_RX_Revertive.toInt();
		}
		break;

	}
	return TRUE;
}


// process changes based on new rev ( not if still the same )...
void ProcessControlInfoDrvRevertive(int iSlotDest)
{
	// nothing to process at this - wait for timer
}


void StoreAndProcessHighwayAreaUse(int iDevNum, int iSlotNum, bncs_string ssHWayData)
{
	if (iSlotNum > CTRL_INFO_RMSTR_HIGHWAYS) {
		int iHighway = iSlotNum - CTRL_INFO_RMSTR_HIGHWAYS;
		CHighwayRecord* pHigh = GetHighwayRecord(iHighway);
		if (pHigh) {
			int iValue = ssHWayData.toInt();
			pHigh->setHighwayAreaUse(iValue);
			if (iChosenHighway == iHighway) {
				if (iValue == AREA1_LOCALE_RING) SetDlgItemText(hWndDlg, IDC_HWAY_AREAUSER, "LD");
				if (iValue == AREA2_LOCALE_RING) SetDlgItemText(hWndDlg, IDC_HWAY_AREAUSER, "NH");
				else SetDlgItemText(hWndDlg, IDC_HWAY_AREAUSER, "--");
			}
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////



/// RM recent list manipulation
BOOL FindEntryInRecentRMList(int iDev, int iDB, int iIndex)
{
	bncs_string ssStr = bncs_string("%1,%2,%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.count()>0) {
		if (ssl_RMChanges.find(ssStr)>=0) {
			//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s found", LPCSTR(ssStr));
			return TRUE;
		}
	}
	return FALSE;
} 


void AddEntryToRecentRMList( int iDev, int iDB, int iIndex )
{
	bncs_string ssStr = bncs_string("%1,%2,%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.find(ssStr)<0) {
		//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s not found so adding", LPCSTR(ssStr));
		ssl_RMChanges.append( ssStr );		
		if (bRMDatabaseTimerRunning) KillTimer( hWndMain, DATABASE_TIMER_ID );
		// reset timer
		SetTimer( hWndMain, DATABASE_TIMER_ID, 350, NULL ); 
	}
} 


////////////////////////////////////////////////////////////////////////////////////////////
//
// strip quotes from info revertives 
//
void StripOutQuotes( LPSTR szStr ) 
{
	int iI, iLen = strlen( szStr );
	char szTmp[MAX_AUTOBFFR_STRING]="";

	wsprintf(szTmp,"",NULL);
	if (iLen > 2) {
		for ( iI=0; iI < (iLen-2); iI++)
			szTmp[iI] = szStr[iI+1];
		szTmp[iI] = NULL; // terminate string
		iLen = strlen( szTmp );
	}
	strcpy( szStr, szTmp );	
}

//
//  FUNCTION: CSIClientNotify()
//
//  PURPOSE: Callback function for incoming CSI messages for client class
//
//  COMMENTS: the message is supplied in szMsg
LRESULT CSIClientNotify(extclient* pec,LPCSTR szMsg)
{
		bncs_string ssIncomingContent = bncs_string("");

		if (pec&&szMsg) {
			UINT iParamCount = pec->getparamcount();
			if ((iParamCount < 0) || (iParamCount > 7)) Debug("CSIClient - rev param count unexpected : %d", iParamCount);

			BOOL bChanged = FALSE;
			int iWhichTypeIndex = 0, iContent = 0, iWhichRing = 0;
			int iDevNum = 0, iDBNum = 0, iRtrDest = 0, iRtrSource = 0, iWhichGpi = 0, iGPIState = 0;
			int iInfoRevType = UNKNOWNVAL, iSlotNum = 0, iIndexNum = 0, iPrevSource = 0, iPrevState = UNKNOWNVAL;

			LPSTR szParamRev = NULL;
			LPSTR szDevNum = NULL;
			LPSTR szSlot = NULL;
			LPSTR szIndex = NULL;
			LPSTR szContent = NULL;

			if (iParamCount > 0) szParamRev = pec->getparam(0);
			if (iParamCount > 1)  szDevNum = pec->getparam(1);
			if (iParamCount > 2)  szSlot = pec->getparam(2);
			if (iParamCount > 3)  szIndex = pec->getparam(3);
			if (iParamCount > 4)  szContent = pec->getparam(4);

			if (szDevNum) {
				iDevNum = atoi(szDevNum);
			}

			if (szSlot)  {
				iDBNum = atoi(szSlot);
				iSlotNum = atoi(szSlot);
				iRtrDest = atoi(szSlot);
				iWhichGpi = atoi(szSlot);
			}

			if (szIndex)  {
				iRtrSource = atoi(szIndex);
				iIndexNum = atoi(szIndex);
				iGPIState = atoi(szIndex);
			}

			//lRXID++;
			if (bValidCollediaStatetoContinue ) {

				switch (pec->getstate())
				{
				case REVTYPE_R:
					//if ((bShowAllDebugMessages)&&(!bAutomaticStarting)) Debug("CSIClient - Router Rev from dev %d, dest %d, src %d ", iDevNum, iRtrDest, iRtrSource);
					iWhichRing = ValidRiedelGRDDevice(iDevNum);
					if (iWhichRing > 0) {
						if (!bAutomaticStarting) Debug("CSIClient - Ring %d Router Rev from dev %d, dest %d, src %d ", iWhichRing, iDevNum, iRtrDest, iRtrSource);
						BOOL bChanged = StoreProcessRiedelGRDRevertive(iWhichRing, iDevNum, iRtrDest, iRtrSource);
						if ((bChanged ) && (!bAutomaticStarting)) {
							ProcessNextCommand(BNCS_COMMAND_RATE); // in event anything added to buffer 
						}
					}
					else
						Debug("CSIClient - Unexpected /No Ring for Riedel GRD revertive from device %d ", iDevNum);
				break;

				case REVTYPE_I:
					if ((iParamCount == 5) && (szContent)) {
						ssIncomingContent = bncs_string(szContent);
						// remove both quotes from start and end of revertive
						ssIncomingContent.replace(bncs_string("'"), bncs_string(""));
						ssIncomingContent.replace(bncs_string("'"), bncs_string(""));
						iContent = atoi(szContent);  // for revertives that are just numbers;
						iInfoRevType = ValidInfodriverDevice(iDevNum, iSlotNum, &iWhichRing);
						if ((iInfoRevType >= RIEDEL_MONITOR_TYPE) && (iInfoRevType <= RIEDEL_EXTRAS_LOGIC)){
							int iRecordIndex = StoreRawRiedelRevertive(iInfoRevType, iWhichRing, iDevNum, iSlotNum, ssIncomingContent, &bChanged);
							if ((iRecordIndex > 0) && (bChanged ) && (!bAutomaticStarting)) {
								switch (iInfoRevType) {
									case RIEDEL_MONITOR_TYPE: ProcessRiedelMonitorRev(iRecordIndex, iWhichRing, iSlotNum); break;
									case RIEDEL_CONFERENCE_TYPE: ProcessRiedelConferenceRev(iRecordIndex, iSlotNum); break;
									case RIEDEL_IFB_TYPE: ProcessRiedelIFBRev(iRecordIndex, iWhichRing, iSlotNum); break;
									case RIEDEL_EXTRAS_MIXERS: ProcessRiedelMIXERRev(iRecordIndex, iWhichRing, iSlotNum); break;
									case RIEDEL_EXTRAS_LOGIC: ProcessRiedelLOGICRev(iRecordIndex, iWhichRing, iSlotNum); break;
								}
								ProcessNextCommand(BNCS_COMMAND_RATE); // in event anything added to buffer 
							}
						}
						else if (iInfoRevType == AUTOS_CONTROL_INFO) {
							// registered for all 4 tic tok slots -> for 2 sites each with 2 packager intstances
							if ((iSlotNum == (CTRL_INFO_RMSTR_AREA1_BASE + CTRL_INFO_TXRX_TIKTOK)) || (iSlotNum == (CTRL_INFO_RMSTR_AREA1_BASE + CTRL_INFO_RXONLY_TIKTOK)) ||
								(iSlotNum == (CTRL_INFO_RMSTR_AREA2_BASE + CTRL_INFO_TXRX_TIKTOK)) || (iSlotNum == (CTRL_INFO_RMSTR_AREA2_BASE + CTRL_INFO_RXONLY_TIKTOK))) {
								bChanged = StoreControlInfoDrvRevertive(iSlotNum, ssIncomingContent);
								// process changes based on new rev ( not if still the same )...
								if (bChanged) ProcessControlInfoDrvRevertive(iSlotNum);
							}
							else if (iSlotNum == (CTRL_INFO_RMSTR_AREA1_BASE + CTRL_INFO_HIGHWAYS)) {
								// AS only registered for conf / ifb data from OTHER site - process incoming data - ignore REQUEST rev, begin action on START, integer stringlist data and FINISH revs- else ignore
								Debug("Ctrl Auto slot %d rev %s", iSlotNum, LPCSTR(ssIncomingContent));
								if (ssIncomingContent.find("REQUEST") >= 0) {
									// start sending out current dyn conf data
									//if ((!bSendingOutHighwayData) && (iOverallTXRXModeStatus == IFMODE_TXRX)) {
									//	GenerateHighwayUseDataStrings();
									//}
								}
								else {
									// receiving data START, lists of confs in use at other site
									//ProcessHighwayUseDataRevertive(ssRevertiveContent);
								}
							}
							else if ((iSlotNum > CTRL_INFO_RMSTR_HIGHWAYS) && (iSlotNum <= CTRL_INFO_RMSTR_HIGHWAYS+iNumberBNCSHighways)) {
								StoreAndProcessHighwayAreaUse(iDevNum, iSlotNum, ssIncomingContent);
							}
						}
					}
					else
						Debug("CSIClient - Incorrectly formed infodriver revertive from dev %d or null string", iDevNum);
				break;

				case DATABASECHANGE:
					// significant part of processing for setting up of packages
					// RM commands come in from src and dest package dbs and from 556 and 557
					// get first RM command 
					// because using internal database manager need to update internal record but not update actual file ( csi does that )
					if ((bShowAllDebugMessages) && (!bAutomaticStarting)) Debug("CSIClient - RM for dev %d, slot %d, state %s ", iDevNum, iSlotNum, szContent);
					if (iDevNum == i_info_RingMaster) {
						if (!FindEntryInRecentRMList(iDevNum, iDBNum, iIndexNum)) {
							// anything to do for RMs for ringmaster ??? // not as yet
							strcpy(szRMData, ExtractSrcNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
							if (bShowAllDebugMessages) Debug("CSIClient - Database change message - %s; device %d db %d indx %d content %s", szMsg, iDevNum, iDBNum, iIndexNum, szRMData);
							dbmPkgs.setName(iDevNum, iDBNum, iIndexNum, szRMData, false);
							AddEntryToRecentRMList(iDevNum, iDBNum, iIndexNum);
						}// if in recent list
					}
					break;

				case REVTYPE_G:
					Debug("CSIClient - IGNORED GPI Revertive from dev (%d), gpi (%d), state %d", iDevNum, iWhichGpi, iGPIState);
					break;

				case STATUS:
					Debug("CSIClient - Status message is %s", szMsg);
					break;

				case DISCONNECTED:
					Debug("CSIClient - CSI has closed down - driver in FAIL  or  ERROR  state");
					//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI connect FAILED");
					iLabelAutoStatus = 2;
					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
					eiRingMasterInfoId->updateslot(COMM_STATUS_SLOT, "0");	 // error	into
					bValidCollediaStatetoContinue = FALSE;
					PostMessage(hWndMain, WM_CLOSE, 0, 0);
					break;

				} // switch
			}
			else
				Debug("CSIClient - Automatic in INVALID or FAIL  State - info revertive ignored");
		}
		else
			Debug("CSIClient - invalid pec class or szMsg passed");

		/* always return TRUE so CSI doesn't delete your client registration */
	return TRUE;

}


///////////////////////////////////////////////////////////////////////////////////////////
//
//  Infodriver Resilience
//
///////////////////////////////////////////////////////////////////////////////////////////
// 
//  Check infodriver status for resilience
//
// 
void CheckAndSetInfoResilience( void )
{
	Debug("into CheckAndSetInfoResil");
	// having connected to driver now then getmode for redundancy
	// get state of infodriver
	char szNM[32]="", szCM[32]="";
	int iMode = eiRingMasterInfoId->getmode(); // main info determines the status of all others -- to prevent mix occuring
	iOverallStateJustChanged = 0;

	// determine string for driver dialog window
	if (iMode ==IFMODE_RXONLY) {
		strcpy(szNM, "RXONLY" );
		iLabelAutoStatus = 2;
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "RXonly - RESERVE");
	}
	if (iMode ==IFMODE_TXRX){
		strcpy(szNM, "TX-RX" );
		iLabelAutoStatus = 0;
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Tx/Rx - MAIN OK");
	}

	if (iMode!=iOverallTXRXModeStatus) {
		if (iOverallTXRXModeStatus ==IFMODE_RXONLY) strcpy(szCM, "RXONLY" );
		if (iOverallTXRXModeStatus ==IFMODE_TXRX) strcpy(szCM, "TX-RX" );
		Debug("CheckInfoMode- new mode %s(%d) different to current %s(%d) -- changing status", szNM, iMode, szCM, iOverallTXRXModeStatus);
		switch (iMode) {
			case IFMODE_RXONLY:
				iLabelAutoStatus = 2;
				ForceAutoIntoRXOnlyMode();
				iOverallTXRXModeStatus = IFMODE_RXONLY;
				Debug( "RingMaster Automatic running in  RXONLY  mode ");
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "RXonly RESERVE Mode");
			break;

			case IFMODE_TXRX:
				iLabelAutoStatus = 0;
				ForceAutoIntoTXRXMode();
				iOverallTXRXModeStatus = IFMODE_TXRX;
				Debug( "RingMaster Automatic running in  TX/RX  mode ");
				eiRingMasterInfoId->updateslot(COMM_STATUS_SLOT, "1"); // automatic in OK mode
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Tx/Rx - MAIN OK");	
			break;

			default:
				iLabelAutoStatus = 3;
				iOverallTXRXModeStatus = IFMODE_NONE;
				Debug( "RingMaster Automatic in an  UNKNOWN  infodriver mode ");
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "????");
		}
	}
	// go thru all infos and report status
	int iTXRXCount = 0, iRXOnlyCount = 0, iOthModeCount=0;
	if (eiRingMasterInfoId->getmode()==IFMODE_TXRX) { 
		iTXRXCount++; SetDlgItemText( hWndDlg, IDC_INFO_STATUS1, "  TX-RX ok"	);
	}
	else if (eiRingMasterInfoId->getmode()==IFMODE_RXONLY) {
		iRXOnlyCount++; SetDlgItemText( hWndDlg, IDC_INFO_STATUS1, "RX-ONLY"	);
	}
	else {
		iOthModeCount++; SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "??oth??");
	}
	if (eiConferencesInfoId->getmode()==IFMODE_TXRX) iTXRXCount++; 
	else if (eiConferencesInfoId->getmode()==IFMODE_RXONLY) iRXOnlyCount++; 
	else iOthModeCount++;

	if (eiPortsPTIInfoId->getmode() == IFMODE_TXRX) iTXRXCount++; 
	else if (eiPortsPTIInfoId->getmode() == IFMODE_RXONLY) iRXOnlyCount++; 
	else iOthModeCount++;

	for (int iirr = 1; iirr <= iNumberRiedelRings; iirr++) {
		if (eiIFBsInfoIds[iirr]) {
			if (eiIFBsInfoIds[iirr]->getmode() == IFMODE_TXRX) iTXRXCount++;
			else if (eiIFBsInfoIds[iirr]->getmode() == IFMODE_RXONLY) iRXOnlyCount++;
			else iOthModeCount++;
		}
		if (eiPortsInfoIds[iirr]) {
			if (eiPortsInfoIds[iirr]->getmode() == IFMODE_TXRX) iTXRXCount++;
			else if (eiPortsInfoIds[iirr]->getmode() == IFMODE_RXONLY) iRXOnlyCount++;
			else iOthModeCount++;
		}
	}

	iNextResilienceProcessing = 37; // reset counter 

	if (iOverallTXRXModeStatus==IFMODE_TXRX) {
		if (iTXRXCount<7) {
			Debug("ERROR -- Driver has mixed txrx states:: txrx %d  rxonly %d other %d - trying to correct", iTXRXCount, iRXOnlyCount, iOthModeCount);
			iLabelAutoStatus = 3;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Error: Mixed TX/rx States");
			// force all into txrx
			ForceAutoIntoTXRXMode();
			iOverallTXRXModeStatus = IFMODE_TXRX;
		}
	}
	else if (iOverallTXRXModeStatus==IFMODE_RXONLY) {
		if (iRXOnlyCount<7) {
			Debug("ERROR -- Driver has mixed rxonly states:: rxonly %d txrx %d other %d - trying to correct", iRXOnlyCount, iTXRXCount, iOthModeCount);
			iLabelAutoStatus = 3;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Error: Mixed RX/tx States...");
			// force all into rxonly
			ForceAutoIntoRXOnlyMode();
			iOverallTXRXModeStatus = IFMODE_RXONLY;
		}
	}
	Debug("outof CheckAndSetInfoResil");
}


void ForceAutoIntoRXOnlyMode( void )
{
	if (eiRingMasterInfoId) {
		eiRingMasterInfoId->setslot( COMM_STATUS_SLOT, "0" ); // currently automatic in lost fail mode
		eiRingMasterInfoId->setmode(IFMODE_RXONLY);					// force to rxonly -- so that resilient pair can take over
		eiRingMasterInfoId->requestmode = TO_RXONLY; // force other driver into tx immeadiately if running ??
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "FORCE RX ONLY" );
	}
	iNextResilienceProcessing = 3; // reset counter 
	iOverallModeChangedOver = 4;
	iOverallTXRXModeStatus = UNKNOWNVAL;
	// return now - as other infodrivers are slaved to master now
	return;

}


void ForceAutoIntoTXRXMode( void )
{
	if (eiRingMasterInfoId) {
		eiRingMasterInfoId->setmode(IFMODE_TXRX);
		eiRingMasterInfoId->requestmode = TO_TXRX;
		eiRingMasterInfoId->setslot( COMM_STATUS_SLOT, "1" ); 
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "Forcing to TX/RX");
	}
	iNextResilienceProcessing = 3;
	iOverallModeChangedOver = 4;
	iOverallTXRXModeStatus = UNKNOWNVAL;
	// return now - as other infodrivers are slaved to master now
	return;

}


void Restart_CSI_NI_NO_Messages(void)
{
	int iModetoset = IFMODE_TXRX;
	if (iOverallTXRXModeStatus != IFMODE_TXRX)  iModetoset = IFMODE_TXRXINQ;
	if (eiRingMasterInfoId) eiRingMasterInfoId->requestmode = iModetoset;
	//
	iOverallModeChangedOver = 0;
	return;

}

void SendOutClosingMessage()
{
	// set relevant autos control info slot to 0 if possible 
	// set relevant autos control info slot to 0 if possible 
	if (iAutomatics_CtrlInfo_Local > 0) {
		char szCommand[256] = "0";
		char szBase[12] = "";
		int iLocalBase = 0;
		if (iAutomaticLocation == AREA1_LOCALE_RING) {
			iLocalBase = CTRL_INFO_RMSTR_AREA1_BASE;
			strcpy(szBase, " LDC rmstr ");
		}
		else if (iAutomaticLocation == AREA2_LOCALE_RING) {
			iLocalBase = CTRL_INFO_RMSTR_AREA2_BASE;
			strcpy(szBase, " NHN rmstr ");
		}
		if (iOverallTXRXModeStatus == IFMODE_TXRX)
			wsprintf(szCommand, "IW %d '0 %s stopped ' %d", iAutomatics_CtrlInfo_Local, szBase, iLocalBase + CTRL_INFO_TXRX_TIKTOK);
		else
			wsprintf(szCommand, "IW %d '0 %s stopped' %d", iAutomatics_CtrlInfo_Local, szBase, iLocalBase + CTRL_INFO_RXONLY_TIKTOK);
		if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
	}
	// get WS - write to info 998 slot WS stating closing Packager - with system time
	char* szCCWS = getenv("CC_WORKSTATION");
	int iWS = atoi(szCCWS);

	char szMsg[255] = "";
	char tBuffer[32];
	char szDate[64];
	struct tm *newtime;
	time_t long_time;

	time(&long_time);                // Get time as long integer. 
	newtime = localtime(&long_time); // Convert to local time. 
	_strtime(tBuffer);
	wsprintf(szDate, "%02d:%02d:%02d", newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
	// truncate long messages and add date and time
	wsprintf(szMsg, "IW %d 'xxx WS %d - RINGMASTER CLOSING at %s' 2", iAutomatics_CtrlInfo_Remote, iWS, szDate);
	if ((ecCSIClient) && (iWS>0)) ecCSIClient->txinfocmd(szMsg, TRUE);
}


