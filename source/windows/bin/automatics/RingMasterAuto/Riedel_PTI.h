#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class to hold REVERTIVE data from riedel pti infodriver 
class CRiedel_PTI
{
private:
	int m_iRecordIndex;
	int m_iRevDevice;
	int m_iRevSlot;
	int m_iAssoc_TrunkAddr;  // can be used to index into ring class for this revertive record for infodriver device

	bncs_string m_ss_Data; // raw data ( pipe delimiter )

	// extracted elements from raw data -5 for forward route indication /  3 elements for contributing src indication
	// based on riedel docs
	bncs_string m_element1;
	bncs_string m_element2;
	bncs_string m_element3;
	bncs_string m_element4;
	bncs_string m_element5;

public:
	CRiedel_PTI(int iRecordIndex, int iDevNum, int iRevSlot, int iAssocTrunk);
	~CRiedel_PTI();

	void storeRiedelData(bncs_string ss);

	int getRecordIndex(void);
	int getDevice(void);
	int getSlot(void);
	int getTrunkAddress(void);

	bncs_string getRawRiedelRevertive(void);
	bncs_string getRevertiveElement(int iElement);

};

