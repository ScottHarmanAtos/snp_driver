// RevsRouterData.h: interface for the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
#define AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
	//
	#include <windows.h>
	#include "RingMasterConsts.h"
	#include <bncs_string.h>
	#include <bncs_stringlist.h>
	#include "bncs_auto_helper.h"

	#include "RouterData.h"	

typedef map<int, CRouterDestData*> MAP_RouterDestData;
typedef map<int, CRouterSrceData*> MAP_RouterSrceData;

class CRevsRouterData  
{
private:
	int m_iRouterDeviceNumber;     // dev ini number
	int m_iMaxSources;                  // db0 size
	int m_iMaxDestinations;            // db1 size

	MAP_RouterDestData cl_DestRouterData;   // map for Destination data
	MAP_RouterSrceData cl_SrceRouterData;    // map for source data

	//data maps are not seen outside class 
	CRouterSrceData* GetSourceData(int iSource);
	CRouterSrceData* GetDestinationData(int iDestination);

public:
	CRevsRouterData( int iDeviceNumber, int iDests, int iSrcs );
	virtual ~CRevsRouterData();

	// router revertives 

	BOOL storeGRDRevertive( int iDestination, int iSource );  // store given source from actual rev; 
	BOOL storeGRDLockState( int iDestination, int iState );

	void setDestinationReEnterantValue(int iDestination, int iReEntDev, int iReEntSource); // done at startup from reading obj settings for re-enterant vals
	void setSourceReEnterantValue(int iSource, int iReEntDev, int iReEntDest); // done at startup from reading obj settings for re-enterant vals
	void setDestDestPackageIndex(int iDestination, int iPackage);

	int getRouterNumber(void);
	int getMaximumDestinations(void);
	int getMaximumSources(void);
	int getGRDLockState(int iDestination);
	int getDestDestPackageIndex(int iDestination);

	int getRoutedSourceForDestination(int iDestination);  // return revertive source as got from grd rev
	int getCalculatedSourceForDestination(int iDestination); // only has a val if src/dest is re-enterant and is calculated on storing grd rev

	int getRouterLockDevice(void);
	int getDestinationLockState(int iDestination);
	void getRouterLockSlotRange(int *iStart, int *iEnd);

	void addPackageToSourceList(int iSource, int iPackage);
	void removePackageFromSourceList(int iSource, int iPackage);
	bncs_string getAllPackagesForSource(int iSource);
	int getNumberPackagesUsingSource(int iSource);

	void addPackageToDestinationList(int iDestination, int iPackage);
	void removePackageFromDestinationList(int iDestination, int iPackage);
	bncs_string getAllPackagesForDestination(int iDestination);
	int getNumberPackagesUsingDestination(int iDestination);

};

#endif // !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
