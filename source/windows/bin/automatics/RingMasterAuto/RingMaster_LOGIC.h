#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// holds ring master explicit info for each LOGIC
// pointers into various LOGIC records for each of the rings

class CRingMaster_LOGIC
{
private:
	int m_RecordIndex;
	// keys into relevant revertive records
	int m_iRevRecordIndex;   // single entry now in V2

	// LOGIC  have default / associated ring/trunk to which they belong -- same ring as associated ifb
	int m_iAssociatedTrunkAddress;

	// for ifb enable/disable
	int m_iAssociatedIFB;
	int m_iAssociatedListenToPort;

	// any other data required for LOGIC at ringmaster level rather than just logic revertive 
	bncs_string m_ss_RMstr_Revertive;

public:
	CRingMaster_LOGIC(int iIndex, int iTrunkAddr);
	~CRingMaster_LOGIC();

	void addRiedelRevIndex( int iRevRecIndex);
	int getRiedelRevIndex(void);

	void storeRingMasterRevertive(bncs_string ssRev);
	bncs_string getRingMasterRevertive(void);

	int getAssociatedTrunkAddress(void);

	void setAssociatedIFB(int iTrunk);
	int getAssociatedIFB(void);

	void setAssociatedListenToPort(int iTrunk);
	int getAssociatedListenToPort(void);


};

