// HighwayRecord.h: interface for the CHighwayRecord class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HIGHWAYRECORD_H__55C43CC0_6D2C_4375_9A7E_1835E30FC607__INCLUDED_)
#define AFX_HIGHWAYRECORD_H__55C43CC0_6D2C_4375_9A7E_1835E30FC607__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786)
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CHighwayRecord  
{
private:
	int m_iRecordIndex;

	int m_iFromTrunk;    // key assoc trunk addr
	int m_iFromRouter;  // which ring grd -***- note Monitoring Highways will have this set to Monitor infodriver number for riedel
	int m_iFromDestination;   // always real Riedel Port for riedel driver commands and revs

	int m_iToTrunk;
	int m_iToRouter;           // into grd device -***- note Monitoring Highways will have this set to Monitor infodriver number for riedel
	int m_iToSource;

	BOOL m_bMonitorHighway;             // special case for mcr monitoring highways
	int m_iMonitorGivenFromDest;       // original fromdest and toSource from which translated reverse lookup used to update fromDest and toSource for monitoring highways
	int m_iMonitorGivenToSource;

	// state
	int m_iHighwayState;                   // parked (free+available), pending (), allocated(in use), maintainence, bars, ....
	int m_iPendingCount;                  // count used by highway mgmt functions before clearing etc -- e.g. gives time get back revs from Riedels
	int m_iHighwayAreaInUse;        // 

	int m_iAllocated_Index;            // allocated value ( eg src or other integer to trip on ) for this highway when in pending state and awaiting real rtr rev.
															// also used to hold key index related to IFBs and mixers
	int m_iPrimaryTracedRing;         // traced srce ring and port all input uses; traced final dest ring and port for ifb / mixer outputs ;   
	int m_iPrimaryTracedRouter;
	int m_iPrimaryTracedPort;

	// in use with ifbs mixers or mcr monitoring
	 // if highway state is ifbUse, and allocated index > 100000 - this is ifbs/mixers using this highway
	// input/mix minus/mixer  or output ifb or mixer mode
	bncs_stringlist m_ssl_WorkingMode;    // pipe delimited format is :  <mode>,<ifb or mixer index or clone type>|<mode>,<ifb or mixer index>|... 

public:
	CHighwayRecord(int iKey, int iFromTrunk, int iFromRtr, int iFromDest, int iToTrunk, int iToRtr, int iToSrc, BOOL bMon );
	virtual ~CHighwayRecord();

	int getRecordIndex(void);

	int getFromRouter(void);
	int getFromTrunkAddr(void);
	int getFromDestination( void );

	int getToRouter(void);
	int getToTrunkAddr(void);
	int getToSource( void );

	BOOL isMonitoringHighway(void);
	void resetMonitorFromDestination(int iGivenPort, int iTranslatedPort);
	void resetMonitorToSource(int iGivenPort, int iTranslatedPort);
	int getMonitorGivenFromDest(void);
	int getMonitorGivenToSource(void);

	void setHighwayState(int iNewState, int iAllocIndex, int iPending );
	int getHighwayState(void);
	int getHighwayAllocatedIndex(void);

	void setHighwayAreaUse(int iAreaUse);
	int getHighwayAreaUse();

	void decrimentHighwayPendingCount();
	int getHighwayPendingCount(void);

	void setTracedRouterAndPort(int iRing, int iGrd, int iPort);                     // std highway + ifb / mixer inputs are sources;  ifb / mixer outs are dests
	void getTracedRouterAndPort(int *iRing, int* iRtr, int* iPort);


	// ifb / mixer specials
	void addWorkingMode(int iMode, int iUserIndex);
	void removeWorkingMode(int iMode, int iUserIndex);
	void clearWorkingModes(void);
	int isHighwayWorkingMode(int iMode, int iUserIndex);
	BOOL getWorkingMode(int iEntry, int* iMode, int* iUserIndex);
	bncs_string getAllWorkingModeEntries(void);
	int getNumberWorkingModeEntries();

};

#endif // !defined(AFX_HighwayRECORD_H__55C43CC0_6D2C_4375_9A7E_1835E30FC607__INCLUDED_)
