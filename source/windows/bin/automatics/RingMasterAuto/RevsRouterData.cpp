// RevsRouterData.cpp: implementation of the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RevsRouterData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRevsRouterData::CRevsRouterData( int iDeviceNumber, int iDests, int iSrcs )
{
	// initialise structure
	m_iRouterDeviceNumber=iDeviceNumber;
	m_iMaxDestinations=iDests;
	m_iMaxSources=iSrcs;

	// create entries in maps for dests and sources
	for (int iS = 1; iS <= m_iMaxSources; iS++) {
		CRouterData* pData = new CRouterData(iS);
		if (pData) {
			// add to map
			cl_SrcRouterData.insert(pair<int, CRouterData*>(iS, pData));
		}
	}
	// create entries in maps for dests and sources
	for (int iD = 1; iD <= m_iMaxDestinations; iD++) {
		CRouterData* pData = new CRouterData(iD);
		if (pData) {
			// add to map
			cl_DestRouterData.insert(pair<int, CRouterData*>(iD, pData));
		}
	}

}

CRevsRouterData::~CRevsRouterData()
{
	// seek and destroy
	while (cl_DestRouterData.begin() != cl_DestRouterData.end()) {
		map<int, CRouterData*>::iterator it = cl_DestRouterData.begin();
		if (it->second) delete it->second;
		cl_DestRouterData.erase(it);
	}
	while (cl_SrcRouterData.begin() != cl_SrcRouterData.end()) {
		map<int, CRouterData*>::iterator it = cl_SrcRouterData.begin();
		if (it->second) delete it->second;
		cl_SrcRouterData.erase(it);
	}
}



////////////////////////////////////////////////////
// private internal functions for src/dest maps 
CRouterData* CRevsRouterData::GetSourceData(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		map<int, CRouterData*>::iterator it = cl_SrcRouterData.find(iSource);
		if (it != cl_SrcRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

CRouterData* CRevsRouterData::GetDestinationData(int iDest)
{
	if ((iDest>0) && (iDest <= m_iMaxDestinations)) {
		map<int, CRouterData*>::iterator it = cl_DestRouterData.find(iDest);
		if (it != cl_DestRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

///////////////////////////////////////////////////////////////
BOOL CRevsRouterData::storeGRDRevertive( int iDestination, int iSource )
{
	// store given source from actual rev; any re-enterant src/dest calc then done  if required
	if (iSource >= -1) {
		if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
			// get from map
			CRouterData* pData = GetDestinationData(iDestination);
			if (pData) {
				pData->m_iRoutedIndex = iSource;
				// xxxx sort out routed to lists - prev and new
				return TRUE;
			}
			else
				OutputDebugString(LPCSTR(bncs_string("CRevsRouterData::storeGRDRevertive no data class for destination %1\n").arg(iDestination)));
		}
		else
			OutputDebugString(LPCSTR(bncs_string("CRevsRouterData::storeGRDRevertive negative destination or greater than maxUsed %1\n").arg(iDestination)));
	}
	else
		OutputDebugString("CRevsRouterData::storeGRDRevertive negative source \n");
	return FALSE;
}


BOOL CRevsRouterData::storeGRDLockState( int iDestination, int iState )
{
	// store given source from actual rev; any re-enterant src/dest calc then done  if required
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iLockStateIndex = iState;
			return TRUE;
		}
		else
			OutputDebugString("CRevsRouterData::storeGRDLockState no data class for destination \n");
	}
	return FALSE;
}



void CRevsRouterData::setDestinationReEnterantValue( int iDestination, int iReEntDev, int iReEntSource )
{
	// done at startup from reading obj settings for re-enterant vals
	if (iReEntSource>0) {
		if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
			// get from map
			CRouterData* pData = GetDestinationData(iDestination);
			if (pData) {
				pData->m_iReEnterant_Device = iReEntDev;
				pData->m_iReEnterant_Index = iReEntSource;
			}
		}
	}
}


void CRevsRouterData::addPackageToDestinationList(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) {
			// is package already in list
			if (pData->ssl_SDIIndxUsedInPackages.find(iPackage)<0) {
				pData->ssl_SDIIndxUsedInPackages.append(iPackage);
				pData->ssl_SDIIndxUsedInPackages.sort(true);
			}
		}
	}
}


void CRevsRouterData::removePackageFromDestinationList(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) {
			// is package already in list
			int iPos = pData->ssl_SDIIndxUsedInPackages.find(iPackage);
			if (iPos >= 0) {
				pData->ssl_SDIIndxUsedInPackages.deleteItem(iPos);
			}
		}
	}
}


bncs_string CRevsRouterData::getAllPackagesForDestination(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->ssl_SDIIndxUsedInPackages.toString(',');
		}
	}
	return "";
}


int CRevsRouterData::getNumberPackagesUsingDestination(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->ssl_SDIIndxUsedInPackages.count();
		}
	}
	return 0;
}



void CRevsRouterData::setDestDestPackageIndex(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iDestination_Package = iPackage;
		}
	}
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CRevsRouterData::getRouterNumber( void )
{
	return m_iRouterDeviceNumber;
}


int CRevsRouterData::getMaximumDestinations( void )
{
	return m_iMaxDestinations;
}


int CRevsRouterData::getMaximumSources( void )
{
	return m_iMaxSources;
}


int CRevsRouterData::getRoutedSourceForDestination( int iDestination )
{
	// return revertive source as  got from grd rev
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iRoutedIndex;
	}
	return 0;
}


int CRevsRouterData::getGRDLockState( int iDestination )
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iLockStateIndex;
	}
	return UNKNOWNVAL;
}






//////////////////////////////////////////////////////////////////////////
// source data routines
//////////////////////////////////////////////////////////////////////////

void CRevsRouterData::setSourceReEnterantValue(int iSource, int iReEntDev, int iReEntDest)
{
	// done at startup from reading obj settings for re-enterant vals
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterData* pData = GetSourceData(iSource);
		if (pData) {
			pData->m_iReEnterant_Device = iReEntDev;
			pData->m_iReEnterant_Index = iReEntDest;
		}
	}
}


void CRevsRouterData::addPackageToSourceList(int iSource, int iPackage)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterData* pData = GetSourceData(iSource);
		if (pData) {
			// is package already in list
			if (pData->ssl_SDIIndxUsedInPackages.find(iPackage)<0) {
				pData->ssl_SDIIndxUsedInPackages.append(iPackage);
				pData->ssl_SDIIndxUsedInPackages.sort(true);
			}
		}
	}
}


void CRevsRouterData::removePackageFromSourceList(int iSource, int iPackage)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterData* pData = GetSourceData(iSource);
		if (pData) {
			// is package already in list
			int iPos = pData->ssl_SDIIndxUsedInPackages.find(iPackage);
			if (iPos >= 0) {
				pData->ssl_SDIIndxUsedInPackages.deleteItem(iPos);
			}
		}
	}
}


bncs_string CRevsRouterData::getAllPackagesForSource(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterData* pData = GetSourceData(iSource);
		if (pData) {
			return pData->ssl_SDIIndxUsedInPackages.toString(',');
		}
	}
	return "";
}


int CRevsRouterData::getNumberPackagesUsingSource(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterData* pData = GetSourceData(iSource);
		if (pData) {
			return pData->ssl_SDIIndxUsedInPackages.count();
		}
	}
	return 0;
}



//////////////////////////////////////////////////////////////////////////
// lock data routines
//////////////////////////////////////////////////////////////////////////

void CRevsRouterData::setRouterLockDevice( int iLockDev, int iStartSlot )
{
	m_iRouterLockDevice = iLockDev;
	m_iStartLockInfoSlot = iStartSlot;
}

int CRevsRouterData::getRouterLockDevice( void )
{
	return m_iRouterLockDevice;
}

void CRevsRouterData::getRouterLockSlotRange( int *iStart, int *iEnd )
{
	*iStart = m_iStartLockInfoSlot;
	*iEnd = m_iStartLockInfoSlot+m_iMaxDestinations-1;
}

int CRevsRouterData::getDestinationLockState( int iDestination )
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_iLockState;
		}
		else
			OutputDebugString("CRevsRouterData::getDestinationLockState no data class for destination \n");
	}
	return -1;
}

