
#include "stdafx.h"
#include "Riedel_Conferences.h"

CRiedel_Conferences::CRiedel_Conferences(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk)
{
	m_iRecordIndex = iRecIndex;
	m_iRevDevice = iDevNum;
	m_iRevConfSlot = iRevSlot;
	m_splitChar = sc;
	m_iAssoc_TrunkAddr = iAssocTrunk;  

	m_ssl_Data = bncs_stringlist("", sc);

	m_ss1_ConfMem =  "";   // first of 4 revertives for riedel infodrv revs
	m_ss2_ConfName = "";   // 2nd of 4 revertives for riedel infodrv revs
	m_ss3_ConfPanelMem = "";   // 3rd of 4 revertives for riedel infodrv revs

}


CRiedel_Conferences::~CRiedel_Conferences()
{
	// burn baby burn
}


void CRiedel_Conferences::clearRiedelData(void)
{
	m_ss1_ConfMem = "";
	m_ss2_ConfName = "";
	m_ss3_ConfPanelMem = "";
}


///////////////////////////////////////////////////////////////

void CRiedel_Conferences::storeRiedelData(bncs_string ss)
{
	m_ssl_Data.clear();
	m_ssl_Data = bncs_stringlist(ss, m_splitChar);
	int iCount = m_ssl_Data.count();
	m_ss1_ConfMem = ss;

}

void CRiedel_Conferences::storeConfName(bncs_string ss)
{
	// special to store conf Name ( slots 1001-1500 )
	m_ss2_ConfName = ss;
}

void CRiedel_Conferences::storeConfPanelMem(bncs_string ss)
{
	// special to store conf panel members ( slots 2001-2500 )
	m_ss3_ConfPanelMem = ss;
}


/////////////////////////////////////////////////////////////////////////////////////////////
int CRiedel_Conferences::getRecordIndex(void)
{
	return m_iRecordIndex;
}

int CRiedel_Conferences::getDevice(void)
{
	return m_iRevDevice;
}

int CRiedel_Conferences::getConfSlot(void)
{
	return m_iRevConfSlot;
}

int CRiedel_Conferences::getTrunkAddress(void)
{
	return m_iAssoc_TrunkAddr;
}

bncs_string CRiedel_Conferences::getRawRiedelRevertive(void)
{
	return m_ssl_Data.toString(m_splitChar);
}

bncs_string CRiedel_Conferences::getConference_Members(void)
{
	return m_ss1_ConfMem;
}

bncs_string CRiedel_Conferences::getConference_Name(void)
{
	return m_ss2_ConfName;
}

bncs_string CRiedel_Conferences::getConference_PanelMembers(void)
{
	return m_ss3_ConfPanelMem;
}


