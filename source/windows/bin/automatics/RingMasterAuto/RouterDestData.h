// RouterData.h: interface for the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROUTERDESTDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
#define AFX_ROUTERDESTDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CRouterDestData  
{
public:
	CRouterDestData( int iIndx );
	virtual ~CRouterDestData();

	int m_iMapIndex;                      // for this grd dest
	int m_iRoutedIndex;					// processed revertive from grd -- for dest data only - primary source

	// from trace if on another ring - but if on same ring, no trace and will be same as primary source, ring will be 0
	int m_iTracedRing;
	int m_iTracedGrd;
	int m_iTracedSource;

	int m_isDestHighway;					// index of assoc outgoing highway record for dest
	int m_iPendingSource;                 // pending source for issued rc on highways only

	// dest lock stuff - not used here -- locks are at a bncs panel level via infodrivers
	int m_iLockState;
	bncs_string m_ss_ReasonTimeStamp;      // most recent text reason for this lock, and or timestamp for last action - lock or unlock

	// data read in from comms_ringmaster_outputs.xml as CG defines -- ties up with ringportsdefn record
	int m_iWhichPortsInfodriver;
	int m_iWhichPortsSlot;

	int m_iUsedasOutputinIFB; 
	int m_iUsedasOutputinMIXER;

};

#endif // !defined(AFX_ROUTERDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
