///////////////////////////////////////////////
//	main.cpp - core code for the application //
///////////////////////////////////////////////

/************************************
  REVISION LOG ENTRY
  Revision By: Chris Gil
  Revised on 23/07/2002 19:40:00
  Version: V
  Comments: Added SNMP 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 18/07/2001 11:29:51
  Version: V
  Comments: Modified extinfo, extclient, extdriver, to have connect() function so
			all constructors take no parameters
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 28/03/2001 14:42:29
  Version: V
  Comments: new comms class specifies the serial port number in the open() function
  now, rather than the constructor
 ************************************/


/************************************
  REVISION LOG ENTRY
  Revision By: PaulW
  Revised on 20/09/2006 12:12:12
  Version: V4.2 
  Comments: r_p and w_p -- will look first for 4.5 CC_ROOT and SYSTEM,   
                        then  ConfigPath defined for V3 systems 
						or as a last resort the default Windows/Winnt directory for dev ini files etc 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: RichardK
  Revised on 03/08/2007
  Version: V4.3
  Comments: Debug turns off redraw of list-box while updating it.  To stop flicker problems seen on
		systems accessed using Remote Desktop.
 ************************************/


#include "stdafx.h"
#define EXT
#include "RingMasterAuto.h"

//
//  FUNCTION: WinMain()
//
//  PURPOSE: 
//
//  COMMENTS: main entry point for BNCS Driver "RingMasterAuto"
//
//
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{

	// exception install
	structured_exception::install();
	try
	{


	MSG msg;
	HACCEL hAccelTable;
	char acTemp[MAX_AUTOBFFR_STRING] = "", szLeft[MAX_AUTOBFFR_STRING] = "", szRight[MAX_AUTOBFFR_STRING] = "";
	char szDebug[MAX_AUTOBFFR_STRING] = "";
	ssCompositeInstance = "";

	lstrcpyn(acTemp, lpCmdLine, 255);

	// deterime system type
	getBNCS_File_Path();

	// MUST pass in parameter - a composite packager instance id- 
	// this check is done as windows shortcuts sometimes pass in whole target path and parameters sometimes
	char *pdest = strstr(acTemp, " ");
	if (pdest != NULL) {
		// space present - as string may include full path to driver
		wsprintf(szDebug, " \n RINGMASTER -winmain- passed in=%s \n", acTemp);
		OutputDebugString(szDebug);
		if (SplitAString(acTemp, ' ', szLeft, szRight)) strcpy(acTemp, szRight);
		wsprintf(szDebug, " \n RINGMASTER -winmain- after split  instance=%s \n", acTemp);
		OutputDebugString(szDebug);
		// now call xml parser to extract device for instance name
		ssCompositeInstance = bncs_string(acTemp);
		wsprintf(szDebug, " \n RINGMASTER -winmain-2- composite instance=%s \n", acTemp);
		OutputDebugString(szDebug);
	}
	else {
		ssCompositeInstance = bncs_string(acTemp);
		wsprintf(szDebug, " \n RINGMASTER -winmain-1- composite instance=%s \n", acTemp);
		OutputDebugString(szDebug);
	}

	if (ssCompositeInstance.length() == 0) {
		OutputDebugString("RINGMASTER -winmain- -ERROR- MISSING COMPOSITE INSTANCE - RINGMASTER WILL NOT RUN \n");
	}


	// Initialize global strings
	wsprintf(szTitle, "BNCS V2 Ringmaster - %s", acTemp);
	//LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDS_SECTION, szAppName, MAX_LOADSTRING);
	LoadString(hInstance, IDC_APPCLASS, szWindowClass, MAX_LOADSTRING);


	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	BOOL bCont = TRUE;
	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_RingMasterAuto);

	// Main message loop:
	do {

			bCont = GetMessage(&msg, NULL, 0, 0);
			if (bCont) {
				if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
			else
				OutputDebugString("GetMessage returned -1 \n");

	} while (bCont);

	return msg.wParam;

	}
	catch (structured_exception const & e)
	{
		Debug("%s:WinMain() - thrown exception %x from %x", szAppName, e.what(), e.where());
		return 0;
		//exit(1);
	}
	catch (...)
	{
		Debug("%s:WinMain() - Unknown exception ", szAppName);
		return 0;
		//exit(1);
	}
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_REG);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCSTR)IDC_RingMasterAuto;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hWndMain = CreateWindow(szWindowClass, szTitle, WS_SYSMENU|WS_MINIMIZEBOX,
		CW_USEDEFAULT, 0, APP_WIDTH, APP_HEIGHT, NULL, NULL, hInstance, NULL);

	if (!hWndMain)
	{
		return FALSE;
	}

//#ifdef _DEBUG
	// always show auto window
	ShowWindow(hWndMain,nCmdShow);
//#else
//	ShowWindow(hWndMain, SW_MINIMIZE);
//#endif

	UpdateWindow(hWndMain);

	return TRUE;
}



///////////////////////////////////////////////////////////////////////////////
// generic method to split string into two halves around a specified character
//
BOOL SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight)
{
	int result = 0;
	char szInStr[MAX_AUTOBFFR_STRING] = "";
	strcpy(szInStr, szStr);
	strcpy(szLeft, "");
	strcpy(szRight, "");

	char *pdest = strchr(szInStr, cSplit);
	if (pdest != NULL) {
		result = pdest - szInStr + 1;
		if (result>0) {
			if (result == strlen(szInStr)) {
				strcpy(szLeft, szInStr);
			}
			else {
				strncpy(szLeft, szInStr, result - 1);
				szLeft[result - 1] = 0; // terminate
				strncpy(szRight, szInStr + result, strlen(szInStr) - result + 1);
			}
			return TRUE;
		}
		else {
			Debug("SplitAString pdest not null, BUT result 0 or less : %d", result);
		}
	}
	// error condition
	strcpy(szLeft, szInStr);
	return FALSE;
}




//
//  FUNCTION: AssertDebugSettings(HWND)
//
//  PURPOSE:  Sorts out window size and ini file settings for the two debug
//            controls - debug and hide.
//  
//	COMMENTS:
//	
void AssertDebugSettings(HWND hWnd)
{
	char szFileName[256];
	char szSection[MAX_LOADSTRING];
	LoadString(hInst, IDS_SECTION, szSection, MAX_LOADSTRING);
	sprintf(szFileName, "dev_%03d.ini", i_info_RingMaster);

	if (iDebugFlag>0)
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_ON, MF_CHECKED);
		//w_p(szFileName, szSection, "DebugMode", "1");
	}
	else
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_ON, MF_UNCHECKED);
		//w_p(szFileName, szSection, "DebugMode", "0");
	}

	RECT rctWindow;
	GetWindowRect(hWnd, &rctWindow);
	if (fDebugHide)
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, iChildHeight + 42, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_HIDE, MF_CHECKED);
	}
	else
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, APP_HEIGHT, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_HIDE, MF_UNCHECKED);
	}
}

void  SendUpdateForTrunkUseData()
{
	char szCommand[MAX_AUTOBFFR_STRING] = "";
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRing);
		if (pRing) {
			wsprintf(szCommand, "IW %d 'Update' 400", pRing->iRing_Device_Extras);
			if ((ecCSIClient) && (iOverallTXRXModeStatus == IFMODE_TXRX)) ecCSIClient->txinfocmd(szCommand);  // send now
		}
	}
}


void SendOutTicTocStatus()
{
	if ((bAutoLocationInUse) && (iAutomatics_CtrlInfo_Local > 0)) {
		char szCommand[MAX_BUFFER_STRING] = "";

		int iRegionBase = 0;
		bncs_string ssRegRevtx = "", ssRegRevrx = "";
		if (iAutomaticLocation == AREA1_LOCALE_RING) {
			iRegionBase = CTRL_INFO_RMSTR_AREA1_BASE;
			ssRegRevtx = ssAREA1_TX_Revertive;
			ssRegRevrx = ssAREA1_RX_Revertive;
		}
		else if (iAutomaticLocation == AREA2_LOCALE_RING) {
			iRegionBase = CTRL_INFO_RMSTR_AREA2_BASE;
			ssRegRevtx = ssAREA2_TX_Revertive;
			ssRegRevrx = ssAREA2_RX_Revertive;
		}

		if (iOverallTXRXModeStatus == IFMODE_TXRX) {
			if (ssRegRevtx.find("TIC") >= 0)
				wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, "2 TOC RMSTR TX", iRegionBase + CTRL_INFO_TXRX_TIKTOK);
			else
				wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, "1 TIC RMSTR TX", iRegionBase + CTRL_INFO_TXRX_TIKTOK);
		}
		else {
			if (ssRegRevrx.find("TIC") >= 0)
				wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, "2 TOC RMSTR RX", iRegionBase + CTRL_INFO_RXONLY_TIKTOK);
			else
				wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, "1 TIC RMSTR RX", iRegionBase + CTRL_INFO_RXONLY_TIKTOK);
		}
		if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
	}

}


void CalculateAndReportControlInfoStatus()
{
	if (bAutoLocationInUse) {
		// deterimine changes been received from  autos control infodriver for registered slots
		int iPrevCtrlStat = iControlStatus;
		iControlStatus = 0;
		// cumulative sum provides for unique parsing
		if (iAREA1_RXONLY_TicTocStatus > 0) iControlStatus = iControlStatus + 1;
		if (iAREA1_TXRX_TicTocStatus > 0) iControlStatus = iControlStatus + 2;
		if (iAREA2_RXONLY_TicTocStatus > 0) iControlStatus = iControlStatus + 4;
		if (iAREA2_TXRX_TicTocStatus > 0) iControlStatus = iControlStatus + 8;
		SetDlgItemInt(hWndDlg, IDC_AUTO_CTRLSTAT2, iControlStatus, TRUE);
		// report state of sum -- xxx use site names given in config
		switch (iControlStatus) {
		case 0: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "NO LDC -- NO NHN"); break;
		case 1: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC RX -- NO NHN"); break;
		case 2: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC TX -- NO NHN"); break;
		case 3: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC OK -- NO NHN"); break;
		case 4: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "NO LDC -- NHN RX"); break;
		case 5: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC RX -- NHN RX"); break;
		case 6: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC TX -- NHN RK"); break;
		case 7: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC OK -- NHN RX"); break;
		case 8: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "NO LDC -- NHN TX"); break;
		case 9: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC RX -- NHN TX"); break;
		case 10: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC TX -- NHN TX"); break;
		case 11: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC OK -- NHN TX"); break;
		case 12: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "NO LDC -- NHN OK"); break;
		case 13: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC RX -- NHN OK"); break;
		case 14: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC TX -- NHN OK"); break;
		case 15: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC OK -- NHN OK"); break;
		}
		// reset 4 site status vars - as they need to be set / change by the arriving revs
		iAREA1_TXRX_TicTocStatus = 0;
		iAREA2_TXRX_TicTocStatus = 0;
		iAREA1_RXONLY_TicTocStatus = 0;
		iAREA2_RXONLY_TicTocStatus = 0;

		// report on shared resources xxx
		//BOOL bWasDualSites = FALSE, bNowDualSites = FALSE;
		//if ((iPrevCtrlStat == 10) || (iPrevCtrlStat == 11) || (iPrevCtrlStat == 14) || (iPrevCtrlStat == 15)) bWasDualSites = TRUE;
		//if ((iControlStatus == 10) || (iControlStatus == 11) || (iControlStatus == 14) || (iControlStatus == 15)) bNowDualSites = TRUE;
		//if ((!bWasDualSites) && (bNowDualSites) && (iAutomatics_Control_Infodriver>0)) {
		// change in state of autos pairs to both being present
		// special poll for allocated dyn confs from pool at OTHER site xxx to write
		// write REQUEST to other's highways data slot ( 2101 or 3101 )
		//RequestOtherRingmasterHighwaysData();
		//}
	}
}




//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  
//	COMMENTS:
//	
//	WM_COMMAND	- process the application menu
//  WM_DESTROY	- post a quit message and return
//	
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	BOOL bContinue = TRUE;

	switch (message) 	{
		case WM_CREATE:
			return InitApp(hWnd);
			break;

		case WM_TIMER:

			switch (wParam) {
			case STARTUP_TIMER_ID: // go thru start up now

				if (iStartupCounter == 0) {
					// stop timer as it takes a while to load all data in
					KillTimer(hWnd, STARTUP_TIMER_ID);
					iLabelAutoStatus = 1;
					bAutomaticStarting = TRUE;
					if (fLog) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_CHECKED);
					if (fDbgView) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_CHECKED);
					if (bShowAllDebugMessages) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_CHECKED);
					Debug(bncs_string("RINGMASTER -- auto starting on WS %1 ").arg(iWorkstation));
					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, " +++ Loading Data...");

					if (LoadAllInitialisationData()) {

						CheckAndSetInfoResilience();
						// load all required data for automatic

						// register with router, panel infodrivers etc
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Registering...");
						AutomaticRegistrations();

						// restart start timer for polling devices
						iOneSecondPollTimerId = SetTimer(hWnd, STARTUP_TIMER_ID, 2000, NULL); // 2 sec timer at this point
						bOneSecondPollTimerRunning = TRUE;
						iStartupCounter = 5;
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, " -*-*- Auto Polling -*-*-");
						PostAutomaticErrorMessage(0, bncs_string(" "));
						bContinue = TRUE;
					}
					else {
						bContinue = FALSE;
					}

					if (!bContinue ) {
						// set Auto in NONRUNNING STATE
						Debug("**** RINGMASTER AUTO -- NOT RUNNING - INFODRIVER OR INIT ERRORS ****");
						Debug("**** RINGMASTER AUTO -- NOT RUNNING - INFODRIVER OR INIT ERRORS ****");
						iLabelAutoStatus = 2;
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "INIT ERRORS");
						iOverallTXRXModeStatus = IFMODE_RXONLY;
						ForceAutoIntoRXOnlyMode();
					}

				}
				else { // startupcounter >0

					// poll routers etc and get revertives - on a staggered poll 
					iLabelAutoStatus = 1;
					bShowAllDebugMessages = FALSE;
					Debug("Startup - Polling at %d", iStartupCounter);
					PollAutomaticDrivers(iStartupCounter);
					iStartupCounter++;
					UpdateCounters();

					if (iStartupCounter > (iNumberRiedelRings * 8)) {
						char szCommand[MAX_BUFFER_STRING] = "";
						wsprintf(szCommand, "IP %d %d %d", iAutomatics_CtrlInfo_Local, CTRL_INFO_RMSTR_HIGHWAYS, CTRL_INFO_RMSTR_HIGHWAYS + iNumberBNCSHighways);
						if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
						wsprintf(szCommand, "IP %d %d %d", iAutomatics_CtrlInfo_Remote, CTRL_INFO_RMSTR_HIGHWAYS, CTRL_INFO_RMSTR_HIGHWAYS + iNumberBNCSHighways);
						if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
					}

					if (iStartupCounter > ((iNumberRiedelRings * 8) + 10)) {
						// 
						KillTimer(hWnd, STARTUP_TIMER_ID);
						// should have processed all initial revertives by now
						//if (qRevertives.size() > 0) {
						//	Debug("Startup - REVERTIVES STILL TO BE PROCESSED before first pass %d ", qRevertives.size());
						//	ProcessNextRevertive(qRevertives.size());
						//}
						if (iLabelAutoStatus == 1) iLabelAutoStatus = 0; // ie no init errors / issues

						bShowAllDebugMessages = TRUE;
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "1st Processing");
						bAutomaticStarting = FALSE;
						if (bPollTrunkNavigatorData) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 2), ID_OPTIONS_POLLTRUNKUSEDATA, MF_CHECKED);

						// FIRST PASS AT PROCESSING 
						Debug("1st Processing pass for startup");
						// determine current state of packages etc
						ProcessFirstPass_Logic();
						// now deal with command que
						ProcessNextCommand(BNCS_COMMAND_RATE);
						iNextManagementProcessing = iUserDefinedMngmntProc;

						bShowAllDebugMessages = FALSE;   // turn off all messages now
						CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_UNCHECKED);

						iOneSecondPollTimerId = SetTimer(hWnd, ONE_SECOND_TIMER_ID, ONE_SECOND_TIMER_INTERVAL, NULL);
						Debug("One Second  timer start returned %d ", iOneSecondPollTimerId);

						if ((bAutoLocationInUse) && (iAutomatics_CtrlInfo_Local > 0) && (iAutomatics_CtrlInfo_Remote > 0)) {
							// only tic tok if location enabled
							// now poll for tic-tok status - and then as a consequence shared resource data from other auto ??? 
							char szCommand[MAX_BUFFER_STRING] = "";
							int iLocalBase = 0, iOtherBase = 0;
							if (iAutomaticLocation == AREA1_LOCALE_RING) {
								iLocalBase = CTRL_INFO_RMSTR_AREA1_BASE;
								iOtherBase = CTRL_INFO_RMSTR_AREA2_BASE;
							}
							else if (iAutomaticLocation == AREA2_LOCALE_RING) {
								iLocalBase = CTRL_INFO_RMSTR_AREA2_BASE;
								iOtherBase = CTRL_INFO_RMSTR_AREA1_BASE;
							}
							// start tictoc timer
							SetTimer(hWnd, TIKTOK_ALIVE_TIMER_ID, 15000, NULL);   // every 15 secs send out a heartbeat
							iCtrlStatusCalcCounter = 5;
							SendOutTicTocStatus();
							wsprintf(szCommand, "IP %d %d %d", iAutomatics_CtrlInfo_Local, iLocalBase + CTRL_INFO_TXRX_TIKTOK, iLocalBase + CTRL_INFO_RXONLY_TIKTOK);
							if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
							wsprintf(szCommand, "IP %d %d %d", iAutomatics_CtrlInfo_Remote, iOtherBase + CTRL_INFO_TXRX_TIKTOK, iOtherBase + CTRL_INFO_RXONLY_TIKTOK);
							if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
						}

						if (iLabelAutoStatus == 3)
							SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Running: but INIT Errors");
						else
							SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Running OK");
					}
				}
			break;

			case ONE_SECOND_TIMER_ID: // used for housekeeping and cyclic polling of hardware 

				// check on infodriver status
				if (iNextResilienceProcessing > 0) {
					iNextResilienceProcessing--;
					if (iNextResilienceProcessing <= 0) {
						CheckAndSetInfoResilience();
					}
				}

				// 2. check if recently failed over
				if (iOverallModeChangedOver > 0) {
					iOverallModeChangedOver--;
					if (iOverallModeChangedOver <= 0) {
						if (bShowAllDebugMessages) Debug("RestartCSI_NI-NO messages called");
						Restart_CSI_NI_NO_Messages();
					}
				}

				if (iCtrlStatusCalcCounter > 0) {
					iCtrlStatusCalcCounter--;
					if (iCtrlStatusCalcCounter <= 0) {
						CalculateAndReportControlInfoStatus();
						iCtrlStatusCalcCounter = 33;
					}
				}

				// 4.poll for trunk nav data - if enabled
				if (bPollTrunkNavigatorData) {
					iNextPollTrunkNavigatorData--;
					if (iNextPollTrunkNavigatorData <= 0) {
						SendUpdateForTrunkUseData();
						iNextPollTrunkNavigatorData = iConfigCounterTrunkNavData;
					}
				}

				// 5. Housekeeping - in quiet periods - check highways usage - if no users of a highway and not locked clear down - park highway
				iNextManagementProcessing--;
				if (iNextManagementProcessing <= 0) {
					ProcessHighwayManagement();
					iNextManagementProcessing = iUserDefinedMngmntProc;
				}

				// any other actions required during housekeeping timer -- process any pending commands if command timer not already running
				if (!bNextCommandTimerRunning) {
					ProcessNextCommand(BNCS_COMMAND_RATE);
				}
				UpdateCounters();

			break;

			case COMMAND_TIMER_ID:
				// process next command in command queue if any 
				ProcessNextCommand(BNCS_COMMAND_RATE);
				if (qCommands.size() == 0) { // queue empty so stop timer
					if (bNextCommandTimerRunning) KillTimer(hWndMain, COMMAND_TIMER_ID);
					bNextCommandTimerRunning = FALSE;
				}
			break;

			case DATABASE_TIMER_ID:
				// reload all panelsets and reassert panel devices
				KillTimer(hWndMain, DATABASE_TIMER_ID);
				bRMDatabaseTimerRunning = FALSE;
				ssl_RMChanges.clear();
				Debug(" RMtimer - clearing list ");
				break;

			case TIKTOK_ALIVE_TIMER_ID:
				// depending on location and if tx or rx - determines slot to update
				SendOutTicTocStatus();
				break;

			case SHAREDRESOURCE_TIMER_ID:
			{
				// timer to send out data on shared resources after a REQUEST command from other site
				if ((iAutomatics_CtrlInfo_Local > 0) && (ecCSIClient)) {
					// highways use data - if ever required

					if (sslSendingOutHighwaysData.count() == 0) KillTimer(hWndMain, SHAREDRESOURCE_TIMER_ID);
				}
			}
			break;

			case FORCETXRX_TIMER_ID:
				Debug("ForceTxrx timer");
				KillTimer(hWndMain, FORCETXRX_TIMER_ID);
				ForceAutoIntoTXRXMode();
				break;

			}  // switch(wparam) on timer types

		break; // wmtimer

		case WM_SIZE: // if the main window is resized, resize the listbox too
			//Don't resize window with child dialog
		break;

		case WM_COMMAND:
			wmId = LOWORD(wParam);
			wmEvent = HIWORD(wParam);
			// Parse the menu selections:
			switch (wmId)	{
			case IDM_ABOUT:
				DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				break;

			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;

			case ID_DEBUG_CLEAR:
				SendMessage(hWndList, LB_RESETCONTENT, 0, 0L);
				break;

			case ID_DEBUG_ON:
				if (iDebugFlag)
					iDebugFlag = 0;
				else	{
					iDebugFlag = 1;
					fDebugHide = FALSE;
				}
				AssertDebugSettings(hWnd);
				break;

			case ID_DEBUG_HIDE:
				if (fDebugHide)	{
					fDebugHide = FALSE;
					iDebugFlag = 1;
				}
				else	{
					fDebugHide = TRUE;
					iDebugFlag = 0;
				}
				AssertDebugSettings(hWnd);
				break;

			case  ID_DEBUG_SHOWALLMESSAGES:
				if (bShowAllDebugMessages ) {
					Debug("-- turning off most debug messages");
					bShowAllDebugMessages = FALSE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_UNCHECKED);
				}
				else {
					Debug("++ Turning on all debug messages");
					bShowAllDebugMessages = TRUE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_CHECKED);
				}
				break;

			case ID_DEBUG_LOGTOFILE:
			{
				if (fLog) {
					Debug("-- cease logging debug messages to file");
					fLog = FALSE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_UNCHECKED);
				}
				else {
					fLog = TRUE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_CHECKED);
					Debug("++ commence logging debug messages to file");
				}
			}
			break;

			case ID_DEBUG_LOGTODBGVIEW:
				if (fDbgView) {
					Debug("-- cease sending messages to dbgview app");
					fDbgView = FALSE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_UNCHECKED);
				}
				else {
					fDbgView = TRUE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_CHECKED);
					Debug("++ commence sending messages to dbgview app");
				}
				break;

			case ID_OPTIONS_FORCETXRX:
				Debug("Forcing Automatic into TXRX Mode");
				ForceAutoIntoTXRXMode();
				break;

			case ID_OPTIONS_FORCERXONLY:
				Debug(" Trying to force driver into RXONLY mode");
				Debug(" *** Now force the other Automatic into TXRX mode to complete operation ***");
				ForceAutoIntoRXOnlyMode();
				break;

			case ID_OPTIONS_POLLTRUNKUSEDATA:
				if (bPollTrunkNavigatorData) {
					bPollTrunkNavigatorData = FALSE;
					iNextPollTrunkNavigatorData = 0;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 2), ID_OPTIONS_POLLTRUNKUSEDATA, MF_UNCHECKED);
					Debug("-- Turning off polling for Trunk Nav data");
				}
				else {
					bPollTrunkNavigatorData = TRUE;
					iNextPollTrunkNavigatorData = iConfigCounterTrunkNavData;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 2), ID_OPTIONS_POLLTRUNKUSEDATA, MF_CHECKED);
					Debug("++ Turning ON polling for Trunk Nav data");
					SendUpdateForTrunkUseData();
				}
			break;

			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			} // switch (wmId)

		break; // case WMCommand

		case WM_DESTROY:
			CloseApp(hWnd);
			PostQuitMessage(0);
			break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ProcessRingChoice()
{
	// get index from list box 
	iChosenRing = (SendDlgItemMessage(hWndDlg, IDC_RINGS, LB_GETCURSEL, 0, 0)) + 1;
	DisplayRingInformation();
	// populate dest ports and highways for this ring

}

void ProcessGRDDestSelection()
{
	// get index from eneterd field, get grd data
	if ((iChosenRing > 0)&&(iChosenRing<=iNumberRiedelRings)) {
		char szAddr[MAX_AUTOBFFR_STRING] = "";
		int iLen = GetDlgItemText(hWndDlg, IDC_DESTINDEX, szAddr, 16);
		iChosenGRDDest = atoi(szAddr);
		if (iChosenGRDDest > 0) DisplayDestPortData();
	}
}

void ProcessConferenceSelection()
{
	char szAddr[MAX_AUTOBFFR_STRING] = "";
	int iLen = GetDlgItemText(hWndDlg, IDC_CONFINDEX, szAddr, 16);
	iChosenConference = atoi(szAddr);
	// get data for current ring
	if ((iChosenConference <1) || (iChosenConference> MAX_CONFERENCES)) {
		iChosenConference = 0; SetDlgItemText(hWndDlg, IDC_CONFINDEX, "");
	}
	DisplayConferenceData();
}

void ProcessIFBSelection()
{
	char szAddr[MAX_AUTOBFFR_STRING] = "";
	int iLen = GetDlgItemText(hWndDlg, IDC_IFBINDEX, szAddr, 16);
	iChosenIFB = atoi(szAddr);
	// get data for current ring
	if ((iChosenIFB <1) || (iChosenIFB> MAX_IFBS)) {
		iChosenIFB = 0; SetDlgItemText(hWndDlg, IDC_IFBINDEX, "");
	}
	DisplayIFBData();
}

void ProcessMonitorPortSelection()
{
	char szAddr[MAX_AUTOBFFR_STRING] = "";
	int iLen = GetDlgItemText(hWndDlg, IDC_MONINDEX, szAddr, 16);
	iChosenMonitorPort = atoi(szAddr);
	if ((iChosenMonitorPort <1) || (iChosenMonitorPort> MAX_RIEDEL_SLOTS)) {
		iChosenMonitorPort = 0; SetDlgItemText(hWndDlg, IDC_MONINDEX, "");
	}
	DisplayMonitorPortData();
}

void ProcessMixerSelection()
{
	char szAddr[MAX_AUTOBFFR_STRING] = "";
	int iLen = GetDlgItemText(hWndDlg, IDC_MIXERINDEX, szAddr, 16);
	iChosenMixer = atoi(szAddr);
	// get data for current ring
	if ((iChosenMixer <1) || (iChosenMixer> MAX_MIXERS_LOGIC)) {
		iChosenMixer = 0; SetDlgItemText(hWndDlg, IDC_MIXERINDEX, "");
	}
	DisplayMixerData();
}

void ProcessRemoteLogicSelection()
{
	char szAddr[MAX_AUTOBFFR_STRING] = "";
	int iLen = GetDlgItemText(hWndDlg, IDC_LOGICINDX, szAddr, 16);
	iChosenLogic = atoi(szAddr);
	// get data for current ring
	if ((iChosenLogic <1) || (iChosenLogic> MAX_IFBS)) {
		iChosenLogic = 0; SetDlgItemText(hWndDlg, IDC_LOGICINDX, "");
	}
	DisplayLogicData();
}


//
//  FUNCTION: DlgProcChild()
//
//  PURPOSE: Message handler for child dialog.
//
//  COMMENTS: Displays the interface status
//
// 
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	//if (hDlg&&wParam&&lParam) {

		int wmId = LOWORD(wParam);
		int wmEvent = HIWORD(wParam);
		int iLen = 0;
		char szAddr[MAX_AUTOBFFR_STRING] = "";

		HWND hButton = (HWND)lParam;
		LONG idButton = GetDlgCtrlID(hButton);

		//char szdata[MAX_AUTOBFFR_STRING];
		//wsprintf( szdata, "DlgChild message %d, BUTTON %d \n", message, idButton);
		//OutputDebugString( szdata );
		switch (message)	{

		case WM_COMMAND:
			switch (wmId)	{
			case IDC_RINGS:		// list of rings
				ProcessRingChoice();
				break;

			case IDC_HIGHWAYS:		// list of standard bncs highways
				DisplayHighwayData();
				break;

			case IDC_PARKHIGHWAY:
				// park selected highway  button
				if (iChosenHighway > 0) {
					Debug("GUI - force parking highway %d", iChosenHighway);
					ForceParkHighway(iChosenHighway);
				}
				break;

			case IDC_BUTTON4:
				// select grd dest from entered element
				ProcessGRDDestSelection();
				break;

			case IDC_BUTTON5:
				// select conference for display from list 
				ProcessConferenceSelection();
				break;

			case IDC_BUTTON6:
				// select ifb for display from list 
				ProcessIFBSelection();
				break;

			case IDC_BUTTON7:
				// select monitor rev for display 
				ProcessMonitorPortSelection();
				break;

			case IDC_BUTTON8:
				// select monitor rev for display 
				ProcessMixerSelection();
				break;

			case IDC_BUTTON9:
				// select monitor rev for display 
				ProcessRemoteLogicSelection();
				break;
			}
			break;

		case WM_INITDIALOG:
			//This will colour the background of the child dialog on startup to defined colour
			// brushes are defined in INIT_APP function below
			HBr = hbrTurq;
			return (LRESULT)HBr;
			//HBr = CreateSolidBrush( COL_TURQ );
			//return (LRESULT)HBr;
			break;

		case WM_CTLCOLORDLG:
			// colour all diag at startup
			SetTextColor((HDC)wParam, COL_DK_BLUE);
			SetBkColor((HDC)wParam, COL_TURQ);
			HBr = hbrTurq;
			return (LRESULT)HBr;
			break;

		case WM_CTLCOLORSTATIC:
			// to colour labels --  this will be called on startup of the dialog by windows
			// this section will be called every time you do a SetDlgItemText() command for a given label
			// by testing the value of global vars this can be used to colour labels for Comms OK/Fail, TXRX/Rxonly etc
			switch (idButton)	{
			case IDC_INFO_STATUS1:
			{
				if (eiRingMasterInfoId) {
					int iInfStat = eiRingMasterInfoId->getmode();
					if (iInfStat == IFMODE_TXRX) {
						SetTextColor((HDC)wParam, COL_WHITE);
						SetBkColor((HDC)wParam, COL_DK_GRN);
						HBr = hbrDarkGreen;
					}
					else if (iInfStat == IFMODE_RXONLY) {
						SetTextColor((HDC)wParam, COL_WHITE);
						SetBkColor((HDC)wParam, COL_LT_RED);
						HBr = hbrLightRed;
					}
					else {
						SetTextColor((HDC)wParam, COL_DK_ORANGE);
						SetBkColor((HDC)wParam, COL_LT_BLUE);
						HBr = hbrLightBlue;
					}
				}
			}
				break;

			case IDC_AUTO_STATUS:
				if (iLabelAutoStatus == 1) {
					SetTextColor((HDC)wParam, COL_DK_BLUE);
					SetBkColor((HDC)wParam, COL_YELLOW);
					HBr = hbrYellow;
				}
				else if (iLabelAutoStatus == 2) {
					SetTextColor((HDC)wParam, COL_WHITE);
					SetBkColor((HDC)wParam, COL_LT_RED);
					HBr = hbrLightRed;
				}
				else if (iLabelAutoStatus == 3) {
					SetTextColor((HDC)wParam, COL_BLACK);
					SetBkColor((HDC)wParam, COL_LT_ORANGE);
					HBr = hbrLightOrange;
				}
				else {
					SetTextColor((HDC)wParam, COL_WHITE);
					SetBkColor((HDC)wParam, COL_DK_GRN);
					HBr = hbrDarkGreen;
				}
				break;

			case IDC_CTRL_STATUS:
				if (bAutoLocationInUse) {
					if (iControlStatus == 0) {
						// no control data at all
						SetTextColor((HDC)wParam, COL_YELLOW);
						SetBkColor((HDC)wParam, COL_BLACK);
						HBr = hbrBlack;
					}
					else if ((iControlStatus == 1) || (iControlStatus == 4) || (iControlStatus == 5) || (iControlStatus == 7) ||
						(iControlStatus == 9) || (iControlStatus == 12) || (iControlStatus == 13)) {
						// some NO, some RX
						SetTextColor((HDC)wParam, COL_WHITE);
						SetBkColor((HDC)wParam, COL_LT_RED);
						HBr = hbrLightRed;
					}
					else if ((iControlStatus == 2) || (iControlStatus == 3) || (iControlStatus == 6) || (iControlStatus == 8)) {
						// ldc tx or both  and no nhn
						SetTextColor((HDC)wParam, COL_BLACK);
						SetBkColor((HDC)wParam, COL_LT_ORANGE);
						HBr = hbrLightOrange;
					}
					else if ((iControlStatus == 10) || (iControlStatus == 11) || (iControlStatus == 14)) {
						// ldc and nhn both tx - some rx too
						// states 10,11,14, and 15 all mean full resource sharing can be used - as both tx are running as a minimum
						SetTextColor((HDC)wParam, COL_DK_BLUE);
						SetBkColor((HDC)wParam, COL_YELLOW);
						HBr = hbrYellow;
					}
					else {
						// all valid control data from both sites -- icontrolstatus = 15 
						SetTextColor((HDC)wParam, COL_WHITE);
						SetBkColor((HDC)wParam, COL_DK_GRN);
						HBr = hbrDarkGreen;
					}
				}
				break;

			case IDC_VERSION: case IDC_IDRX: case IDC_IDTX: case IDC_QUEUE: case IDC_CONF_IFB_MONS: case IDC_AUTO_CTRLSTAT2:
			case IDC_AUTO_RINGMASTER2: case IDC_AUTO_RINGMASTER3: case IDC_RING_TRUNKUSE: case IDC_AUTO_RINGS:
			case IDC_AUTO_INFNUM1: case IDC_AUTO_INFNUM2: case IDC_AUTO_INFNUM3:  case IDC_AUTO_INFNUM4: case IDC_AUTO_INFNUM7:
			case IDC_AUTO_MAPPIFBS:  case IDC_DEST_TRACE: case IDC_AUTO_DESTCOLL2: case IDC_MONITOR_REV: case IDC_AUTO_CONFASS: 
			case IDC_AUTO_HIGHWAYS: case IDC_HIGHWAY_RING1: case IDC_HIGHWAY_RING2: case IDC_HIGHWAY_RING3: case IDC_HIGHWAY_RING4:
			case IDC_HIGHWAY_RING5: case IDC_HIGHWAY_RING6: case IDC_HWAY_INDEX: case IDC_HWAY_USERS: case IDC_AUTO_CTRLINFO:
			case IDC_HWAY_INTO:  case IDC_HWAY_TRACE:  case IDC_DEST_INDX8:  case IDC_DEST_RECORDS2:  case IDC_DEST_LOCKS:case IDC_DEST_LOCKS2:
			case IDC_RING_NAME: case IDC_RING_INFOS: case IDC_RING_INFOREVS: case IDC_RING_SRC: case IDC_RING_DEST: case IDC_AUTO_AREA:
			case IDC_RING_HIGH1: case IDC_RING_HIGH2: case IDC_RING_HIGH4: case IDC_DEST_RECORDS: case IDC_HWAY_FROM:
			case IDC_DEST_INDX2: case IDC_DEST_INDX3: case IDC_DEST_INDX4: case IDC_DEST_INDX5: case IDC_DEST_INDX6: case IDC_DEST_INDX7:
			case IDC_SRCE_HIGHWAY:  case IDC_DEST_HIGHWAY: case IDC_DEST_INFOREVS2:  case IDC_CONF_IFB_REV:

				SetTextColor((HDC)wParam, COL_DK_BLUE);
				SetBkColor((HDC)wParam, COL_WHITE);
				HBr = hbrWhite;

			break;

			case IDD_STATUS: case IDC_STATIC:
				SetTextColor((HDC)wParam, COL_DK_BLUE);
				SetBkColor((HDC)wParam, COL_TURQ);
				HBr = hbrTurq;
				break;

			default:
				SetTextColor((HDC)wParam, COL_DK_BLUE);
				SetBkColor((HDC)wParam, COL_TURQ);
				HBr = hbrTurq;
			}
			return (LRESULT)HBr;
		break;

		default:
			//Debug( "dlg child def");
			break;
		}
	//}
	//else {
	//	Debug("DlgProcChild - ERROR - invalid parameters");
	//}
    return FALSE;


}

//
//  FUNCTION: Version Info()
//
void VersionInfo() {
	LPBYTE   abData;
	DWORD  handle;
	DWORD  dwSize;
	LPBYTE lpBuffer;
	LPSTR szName, szModFileName, szBuf;
	char acBuf[16];
	#define	PATH_LENGTH	MAX_AUTOBFFR_STRING
			
	szName=(LPSTR)malloc(PATH_LENGTH);
	szModFileName=(LPSTR)malloc(PATH_LENGTH);
	szBuf=(LPSTR)malloc(PATH_LENGTH);

	/*get version info*/

	GetModuleFileName(hInst,szModFileName,PATH_LENGTH);

	dwSize = GetFileVersionInfoSize(szModFileName, &handle);
	abData=(LPBYTE)malloc(dwSize);
	GetFileVersionInfo(szModFileName, handle, dwSize, abData);
	if (dwSize) {
		/* get country translation */
		VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
		/* make country code */
		wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
		if (dwSize!=0) {
			/* get a versioninfo file version number */
			wsprintf(szName,"\\StringFileInfo\\%s\\PRODUCTVERSION",(LPSTR)acBuf);
			VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
		}
		/* copy version number from byte buffer to a string buffer */
		lstrcpyn(szModFileName,(LPSTR)lpBuffer,(int)dwSize);
		/* copy to the dialog static text box */
		wsprintf(szBuf,"%s",szModFileName);
		// replace any commas with full stops
		for (int c=0; c<int(strlen(szBuf)); c++)
			if (szBuf[c]==44 ) szBuf[c]=46; 
		SetDlgItemText(hWndDlg, IDC_VERSION, szBuf);
	}			
	free((PVOID)abData);
	free((PVOID)szName);
	free((PVOID)szModFileName);
	free((PVOID)szBuf);
}



//
//  FUNCTION: About()
//
//  PURPOSE: Message handler for about box.
//
//  COMMENTS: Displays the compile date info
//				and used to display version info from
//				a version resource
//
// 
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
	case WM_INITDIALOG:
		{
			LPBYTE   abData;
			DWORD  handle;
			DWORD  dwSize;
			LPBYTE lpBuffer;
			LPSTR szName, szModFileName, szBuf;
			char acBuf[16];
			#define	PATH_LENGTH	MAX_AUTOBFFR_STRING
			
			szName=(LPSTR)malloc(PATH_LENGTH);
			szModFileName=(LPSTR)malloc(PATH_LENGTH);
			szBuf=(LPSTR)malloc(PATH_LENGTH);
			
			/*get version info*/
			
			GetModuleFileName(hInst,szModFileName,PATH_LENGTH);
			
			dwSize = GetFileVersionInfoSize(szModFileName, &handle);
			
			abData=(LPBYTE)malloc(dwSize);
			
			GetFileVersionInfo(szModFileName, handle, dwSize, abData);
			
			if (dwSize)
			{
				LPSTR szFileVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szProductVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szComments=(LPSTR)alloca(PATH_LENGTH);

				/* get country translation */
				VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
				/* make country code */
				wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
				
				if (dwSize!=0)
				{
					/* get a versioninfo file version number */
					wsprintf(szName,"\\StringFileInfo\\%s\\FileVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szFileVersion,(LPSTR)lpBuffer,(int)dwSize);

				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\ProductVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szProductVersion,(LPSTR)lpBuffer,(int)dwSize);
				
				/* copy to the dialog static text box */
				wsprintf(szBuf,"Version %s  (%s)",szFileVersion, szProductVersion);
				SetDlgItemText (hDlg, IDC_VERSION, szBuf);

				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\Comments",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				/* copy to the dialog static text box */
				lstrcpyn(szComments,(LPSTR)lpBuffer,(int)dwSize);
				SetDlgItemText (hDlg, IDC_COMMENT, szComments);

			}
			
			wsprintf(szName,"%s - %s",(LPSTR)__DATE__,(LPSTR)__TIME__);
			SetDlgItemText(hDlg,IDC_COMPID,szName);
			
			free((PVOID)abData);
			free((PVOID)szName);
			free((PVOID)szModFileName);
			free((PVOID)szBuf);
		}
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
    return FALSE;

}

//
//  FUNCTION: InitApp()
//
//  PURPOSE: Things to do when main window is created
//
//  COMMENTS: Called from WM_CREATE message
//			 We create a list box to fill the main window to use as a
//			debugging tool, and configure the serial port and infodriver 
//			connections, as well as a circular fifo buffer
//
//
LRESULT InitApp(HWND hWnd)
{

	/* return 0 if OK, or -1 if an error occurs*/
		RECT rctWindow;
	
	OutputDebugString( " \n BNCS RINGMASTER AUTO starting up \n " );

	// define brushes and colours
	CreateBrushes();	
	Debug("brushes done");

	iLabelAutoStatus=1;
	iUserDefinedMngmntProc = 7;
	iNextManagementProcessing = 60; // after startup polling
	iOverallTXRXModeStatus = 0;
	iOverallAutoAlarmState = 0;
	iOverallModeChangedOver = 0;
	iOverallStateJustChanged = 0;
	iNextResilienceProcessing = 2;
	i_info_RingMaster = 0; 
	i_info_Conferences = 0;
	i_info_IFBs_Start = 0;
	i_info_Ports_Start = 0;
	i_info_IFBs_End = 0;
	i_info_Ports_End = 0;
	i_info_Ports_PTI = 0;
	bValidCollediaStatetoContinue=TRUE;
	bAutomaticStarting=TRUE;
	bRMDatabaseTimerRunning = FALSE;
	bNextCommandTimerRunning = FALSE;
	iNextPollTrunkNavigatorData = 0;
	iConfigCounterTrunkNavData = 0;  
	bPollTrunkNavigatorData = FALSE;
	ssl_RMChanges = bncs_stringlist( "", '|' );
	iChosenRing=0;
	iChosenGRDDest = 0;
	iChosenHighway = 0;
	iChosenMixer = 0;
	iChosenMonitorPort = 0;
	iChosenConference = 0;
	iChosenIFB = 0;
	ssl_Calc_Trace = bncs_stringlist("", '|');

	// Init Status Dialogs
	hWndDlg = CreateDialog(hInst, (LPCTSTR)IDD_STATUS, hWnd, (DLGPROC)DlgProcChild);
	GetClientRect(hWndDlg, &rctWindow);
	iChildHeight = rctWindow.bottom - rctWindow.top + 2;
		
	GetClientRect(hWnd,&rc);
	hWndList=CreateWindow("LISTBOX","",WS_CHILD | WS_VSCROLL | LBS_NOINTEGRALHEIGHT | LBS_USETABSTOPS,
						  0,iChildHeight,rc.right-rc.left,rc.bottom-rc.top-iChildHeight,
						  hWnd,(HMENU)IDW_LIST,hInst,0);
	ShowWindow(hWndList,SW_SHOW);

	LoadIni();
	VersionInfo();

	if (ssCompositeInstance.length() > 0) {

		AssertDebugSettings(hWnd);
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Initialising");
		OutputDebugString( " initialising - CSI Client notify \n " );
		
		// connect to CSI first
		ecCSIClient = new extclient;
		ecCSIClient->notify(CSIClientNotify);
		OutputDebugString(" initialising - CSI Client connect \n ");
		switch (ecCSIClient->connect()){
			case CONNECTED: 
				Debug("Connected OK to CSI");
				//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI Connected ok");
			break;
			case CANT_FIND_CSI:
				Debug("ERROR ERROR - Connect failed to CSI ");
				Debug("ERROR ERROR -  Automatic not functioning");
				//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI connect FAILED");
				iLabelAutoStatus=2;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
				bValidCollediaStatetoContinue	= FALSE;
			break;
			case BAD_WS:
				Debug("ERROR ERROR - Connect failed to CSI - Bas WS ?? WTF ");
				Debug("ERROR ERROR -  Automatic not functioning");
				//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI connect FAILED");
				iLabelAutoStatus=2;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "BAD WS - in FAIL state");
				bValidCollediaStatetoContinue	= FALSE;
			break;
		default:
			Debug("Other Error - code %d",ecCSIClient->getstate());
			Debug("ERROR ERROR - Connect failed to CSI,  code :%d",ecCSIClient->getstate() );
			Debug("ERROR ERROR -  Automatic not functioning");
			//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI connect FAILED");
			iLabelAutoStatus=2;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
			bValidCollediaStatetoContinue	= FALSE;
		} // switch ec
		ecCSIClient->setcounters(&lTXID, &lRXID);

		if (bValidCollediaStatetoContinue) {			
			OutputDebugString(" initialising - commencing full init \n ");
			iStartupCounter = 0;
			iOneSecondPollTimerId = SetTimer( hWnd, STARTUP_TIMER_ID, STARTUP_TIMER_INTERVAL , NULL );
			iLabelAutoStatus=1;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Starting Initialisaion ");
		}
		else {
			OutputDebugString(" initialising - ERROR fail \n ");
		}

	}
	else {
		Debug("ERROR ERROR - MISSING COMPOSITE INSTANCE");
		Debug("ERROR ERROR -  Automatic not functioning");
		iLabelAutoStatus = 2;
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "C-INSTANCE FAIL ");
		bValidCollediaStatetoContinue = FALSE;
	}

	// end of first part of init
	return 0;

}

//
//  FUNCTION: CloseApp()
//
//  PURPOSE: Things to do when main window is destroyed
//
//  COMMENTS: Called from WM_DESTROY message
//			The listbox debug window is a child of the main, so it will be 
//			destroyed automatically. 
//
void CloseApp(HWND hWnd)
{
	Debug( bncs_string("RINGMASTER -- txrx auto closing down on WS %1 ").arg(iWorkstation));

	SendOutClosingMessage();
	
	// kill any timers
	Debug("CloseApp 1");
	KillTimer( hWnd , COMMAND_TIMER_ID );
	KillTimer( hWnd , ONE_SECOND_TIMER_ID );
	

	Debug("CloseApp 2");
	ForceAutoIntoRXOnlyMode();

	Debug("CloseApp 3");
	ClearAutomaticMaps();
	Debug("CloseApp 3a");
	ClearCommandQueue();
	//ClearRevertiveQueue();
	if (cl_KeyInstances) delete cl_KeyInstances;

	// close all infodrivers ???
	if (eiRingMasterInfoId) eiRingMasterInfoId->closeInfodriver();
	if (eiConferencesInfoId) eiConferencesInfoId->closeInfodriver();
	if (eiPortsPTIInfoId) eiPortsPTIInfoId->closeInfodriver();
	for (int ii = 1; ii <= iNumberRiedelRings; ii++) {
		if ((ii >= 0) && (ii < MAX_RIEDEL_RINGS)) {
			if (eiIFBsInfoIds[ii]) eiIFBsInfoIds[ii]->closeInfodriver();
			if (eiPortsInfoIds[ii]) eiPortsInfoIds[ii]->closeInfodriver();
		}
	}

	Debug("CloseApp 4 - deleting info records");
	if (eiRingMasterInfoId) delete eiRingMasterInfoId;
	if (eiConferencesInfoId) delete eiConferencesInfoId;
	if (eiPortsPTIInfoId) delete eiPortsPTIInfoId;
	for (int ii = 1; ii <=iNumberRiedelRings; ii++) {
		if ((ii >= 0) && (ii < MAX_RIEDEL_RINGS)) {
			if (eiIFBsInfoIds[ii]) delete eiIFBsInfoIds[ii];
			if (eiPortsInfoIds[ii]) delete eiPortsInfoIds[ii];
		}
	}
		
	Debug("CloseApp 5");
	if (ecCSIClient)    delete ecCSIClient;


	Debug("CloseApp 5");
	// tidy up colours
	ReleaseBrushes();
	DeleteObject(HBr);
	Debug("CloseApp 7");

}



//
//  FUNCTION: LoadIni()
//
//  PURPOSE: Assign (global) variables with values from .ini file
//
//  COMMENTS: The [section] name is gotten from a stringtable resource
//			 which defines it. It is the same name as the application, defined by the wizard
//			
//			
void LoadIni(void)
{

	iWorkstation=getWS();
	ssKeyClearCmd = bncs_string("&CLEAR");
	
	fLog = FALSE;
	fDbgView = FALSE;
	iDebugFlag = 0;
	fDebugHide = TRUE;
	if (ssCompositeInstance.length() > 0) {
		bncs_string ssXmlConfigFile = "ring_master_config";
		int iVal = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "DebugMode").toInt();
		if (iVal > 0) {
			iDebugFlag = 1;
			fDebugHide = FALSE;
			if (iVal > 1) bShowAllDebugMessages = TRUE;
		}
		iVal = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "LogToFile").toInt();
		if (iVal == 1) fLog = TRUE;
		if (iVal == -1) fDbgView = TRUE;
	}

}

/*!
\returns int The workstation number

\brief Function to get the workstation number

First checks to see if %CC_WORKSTATION% is defined
otherwise returns the WS stored in %WINDIR%\csi.ini

NOTE: 
In BNCSWizard apps the following change should be made to LoadINI()
- Replace:
	iWorkstation=atoi(r_p("CSI.INI","Network","Workstation","0",FALSE));
- With:
	iWorkstation=getWS();
- Also add the following to "app_name.h":
int		getWS(void);
*/
int getWS(void)
{
	char* szWS = getenv( "CC_WORKSTATION" );
	
	if(szWS)
	{
		return atoi(szWS);
	}
	else
	{
		return atoi(r_p("CSI.INI","Network","Workstation","0",FALSE));
	}

}

// new function added to version 4.2 assigns the globals    szBNCS_File_Path and szBNCS_Log_File_Path
// called from r_p and w_p functions immediately below
// could easily be called once on first start up of app

char *	getBNCS_File_Path( void  )  
{
	char* szCCRoot = getenv( "CC_ROOT" );
	char* szCCSystem = getenv( "CC_SYSTEM" );
	char szV3Path[MAX_AUTOBFFR_STRING] = "";
	GetPrivateProfileString("Config", "ConfigPath", "", szV3Path, sizeof(szV3Path),"C:\\bncs_config.ini" );
	char szWinDir[MAX_AUTOBFFR_STRING] = "";
	GetWindowsDirectory(szWinDir, MAX_AUTOBFFR_STRING);

	// first check for 4.5 system settings
	if(szCCRoot && szCCSystem)	{
		sprintf(szBNCS_File_Path, "%s\\%s\\config\\system", szCCRoot, szCCSystem);
		sprintf(szBNCS_Log_File_Path, "%s\\%s\\logs", szCCRoot, szCCSystem);
	}
	else if (strlen(szV3Path)>0) {
		sprintf(szBNCS_File_Path,"%s",szV3Path);
		strcpy(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	else { 
		// all other BNCS / Colledia systems - get windows or winnt directory inc v3  that are not using bncs_config.ini
		sprintf(szBNCS_File_Path,"%s",szWinDir);
		strcpy(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	return szBNCS_File_Path;

}

/*!
\returns char* Pointer to data obtained
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* defval The default value to return if not found
\param BOOL fWrite Flag - write the default value to the file

\brief Function to read one item of user data from an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
char* r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite)
{
	char szFilePath[MAX_AUTOBFFR_STRING];	
	char szPathOnly[MAX_AUTOBFFR_STRING];

	strcpy( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );

	GetPrivateProfileString(section, entry, defval, szResult, sizeof(szResult), szFilePath);
	if (fWrite)
		w_p(file,section,entry,szResult);
	
	return szResult;

}

/*!
\returns void 
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* setting The value to write - =value

\brief Function to write one item of user data to an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
void w_p(char* file, char* section, char* entry, char* setting)
{

	char szFilePath[MAX_AUTOBFFR_STRING];	
	char szPathOnly[MAX_AUTOBFFR_STRING];

	strcpy( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );

	WritePrivateProfileString(section, entry, setting, szFilePath);

}


//
//  FUNCTION: UpdateCounters()
//
//  PURPOSE: Writes current counter values to child dialog
//
//  COMMENTS: 
//				
void UpdateCounters(void)
{
	char szBuf[32];
	//Update Infodriver External Message Count
	wsprintf(szBuf, "%011d", lTXID);
	SetDlgItemText(hWndDlg, IDC_IDTX, szBuf);
	wsprintf(szBuf, "%011d", lRXID);
	SetDlgItemText(hWndDlg, IDC_IDRX, szBuf);
	SetDlgItemInt(hWndDlg, IDC_QUEUE, qCommands.size(), FALSE);
	//SetDlgItemInt(hWndDlg, IDC_QUEUE2, qRevertives.size(), FALSE);
	if (lTXID > 100000000) lTXID = 0;
	if (lRXID > 100000000) lRXID = 0;
}

//
//  FUNCTION: Debug()
//
//  PURPOSE: Writes debug information to the listbox
//
//  COMMENTS: The function works the same way as printf
//				example: Debug("Number=%d",iVal);
//			
//			
void Debug(LPCSTR szFmt, ...)
{
	if ((iDebugFlag>0) || (fLog) || (fDbgView))
	{
		va_list argptr;

		va_start(argptr, szFmt);
		vsprintf(szDebug, szFmt, argptr);
		va_end(argptr);

		if (strlen(szDebug) > 820) szDebug[820] = NULL; // trim very long messages

		// log to dbview app
		if (fDbgView) {
			wsprintf(szLogDbg, "RingmasterAuto - %s \n", szDebug);
			OutputDebugString(szLogDbg);
		}

		// app debug window
		if ((iDebugFlag>0) || (fLog)) {

			char tBuffer[32];
			char szDate[64];
			char szTime[64];
			struct tm *newtime;
			time_t long_time;

			time(&long_time);                // Get time as long integer. 
			newtime = localtime(&long_time); // Convert to local time. 
			_strtime(tBuffer);
			sprintf(szDate, "%04d%02d%02d", newtime->tm_year + 1900, newtime->tm_mon + 1, newtime->tm_mday);
			wsprintf(szTime, "%02d:%02d:%02d", newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
			wsprintf(szLogDbg, "%s %s", szTime, szDebug);

			// log to bncs logs dir
			if (fLog) {
				Log(szDate);
			}

			if (iDebugFlag>0) {
				// truncate messages to fit app dbg window
				if (strlen(szLogDbg) > 220) szLogDbg[220] = NULL;
				//
				SendMessage(hWndList, WM_SETREDRAW, FALSE, 0);	//Stop redraws.
				SendMessage(hWndList, LB_ADDSTRING, 0, (LPARAM)szLogDbg);	//Add message
				long lCount = SendMessage(hWndList, LB_GETCOUNT, 0, 0);	//Get List Count
				if (lCount > MAX_LB_ITEMS)
				{
					SendMessage(hWndList, LB_DELETESTRING, 0, 0);	//Delete item
					lCount--;
				}
				SendMessage(hWndList, LB_SETTOPINDEX, (WPARAM)lCount - 1, 0);	//Set Top Index
				SendMessage(hWndList, WM_SETREDRAW, TRUE, 0);	//Restart redraws.
			}
		}
	}
}


//
// FUNCTION CreateLogDirectory
//
// new function Version 4.2 of the Wizard - to create log file directory according to Colledia system type
//
BOOL CreateLogFileDirectory(void)
{
	static bool bFirst = true;
	//create the log directory the first time
	if (bFirst) {
		_mkdir(szBNCS_Log_File_Path);
		bFirst = false;
		return TRUE;
	}
	return FALSE;
}

//
//  FUNCTION: Log()
//
//  PURPOSE: Writes information to the log file
//
//
void Log(LPCSTR szDate)
{
	// this function must only be called from Debug - as this dir sets up time and date
	// uses szLogDbg == global debug string for message logged ( set up in Debug function )

	char szLogFile[300];
	char   c = '\n';
	FILE *fp;

	wsprintf(szLogFile, "%s\\%s_RingMasterAuto.log", szBNCS_Log_File_Path, szDate);
	// create log file directory if it doesn't exist
	CreateLogFileDirectory();
	if (fp = fopen(szLogFile, "a"))
	{
		fprintf(fp, "%s%c", szLogDbg, c);
		fclose(fp);
	}

}


//
// Function name	: SplitString
// Description	   : Split a delimited string to an array of pointers to strings
// Return type		: int number of elements returned
//
//----------------------------------------------------------------------------//
// Takes a string and splits it up creating an array of pointers to the start //
// of the sections delimted by the array of specified char delimiters         //
// the delimter characters in "string" are overwritten with NULLs             //
// Usage:                                                                     //
// char szString[] = "1,2|3'4,5,6,7,";                                        //
// char delim[] = {',', '|', '\'', ',', ',', ',', ',',};                      //
// UINT count=7;                                                              //
// char *pElements[7];                                                        //
// int iLen;                                                                  //
//	iLen = SplitString( szString, delim, 7, pElements);                       //
//                                                                            //
// NOTE: This funcion works differently to strtok in that consecutive         //
// delimiters are not overlooked and so a NULL output string is possible      //
// i.e. a string                                                              //
//    hello,,dave                                                             //
// where ,,, is the delimiter will produce 3 output strings the 2nd of which  //
// will be NULL                                                               //
//----------------------------------------------------------------------------//
int SplitString(char *string, char *delim, UINT count, char **outarray )
{

	UINT x;
	UINT y;
	UINT len;					// length of the input string
	static char *szNull = "";	// generic NULL output string
	int delimlen;
	int dp;

	len = strlen( string );
	delimlen = strlen( delim );

	if(!len)					// if the input string is a NULL then set all the output strings to NULL
	{
		for (x = 0 ; x < count; x++ )
			outarray[x] = szNull;
		return 0;
	}

	outarray[0] = string;		// set the 1st output string to the beginning of the string

	for( x = 0,y = 1 ; (x < len) && (y < count); x++ )
	{
		if( delimlen < 2 )
			dp = 1;
		else
			dp = y;
		if(string[x] == delim[dp-1])
		{
			string[ x ] = '\0';
			if((x+1) < len)		// if there is another string following this one....
			{
				outarray[ y ] = &string[x+1];
				y++;			// increment the number of assigned buffer
			}
			else
			{
				outarray[ y ] = szNull;
			}
		}
	}

	x = y;
	while( x < count )
		outarray[x++] = szNull;
	return y;					// return the number of strings allocated
	
}




/////////////////////////////////////////////////////////////////////////
// BRUSHES FOR BUTTONS & LABELS
/////////////////////////////////////////////////////////////////////////

/*********************************************************************************************************/
/*** Function to Create Colour brushes ***/
void CreateBrushes()
{ 
	hbrWindowsGrey = CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
	hbrBlack = CreateSolidBrush(COL_BLACK);
	hbrWhite = CreateSolidBrush(COL_WHITE);

	hbrDarkRed = CreateSolidBrush(COL_DK_RED);
	hbrDarkGreen = CreateSolidBrush(COL_DK_GRN);
	hbrDarkOrange = CreateSolidBrush(COL_DK_ORANGE);
	hbrDarkBlue = CreateSolidBrush(COL_DK_BLUE);

	hbrYellow = CreateSolidBrush(COL_YELLOW);
	hbrTurq = CreateSolidBrush(COL_TURQ);

	hbrLightRed = CreateSolidBrush(COL_LT_RED);
	hbrLightGreen = CreateSolidBrush(COL_LT_GRN);
	hbrLightOrange = CreateSolidBrush(COL_LT_ORANGE);
	hbrLightBlue = CreateSolidBrush(COL_LT_BLUE);


}

 /********************************************************************************************************/
void ReleaseBrushes()
{
	if (hbrBlack) DeleteObject(hbrBlack);
	if (hbrWhite) DeleteObject(hbrWhite);
	if (hbrWindowsGrey) DeleteObject(hbrWindowsGrey);

	if (hbrDarkRed) DeleteObject(hbrDarkRed);
	if (hbrDarkGreen) DeleteObject(hbrDarkGreen);
	if (hbrDarkBlue) DeleteObject(hbrDarkBlue);
	if (hbrDarkOrange) DeleteObject(hbrDarkOrange);

	if (hbrYellow)	DeleteObject(hbrYellow);
	if (hbrTurq)	DeleteObject(hbrTurq);
	if (hbrLightRed) DeleteObject(hbrLightRed);
	if (hbrLightBlue) DeleteObject(hbrLightBlue);
	if (hbrLightGreen) DeleteObject(hbrLightGreen);
	if (hbrLightOrange) DeleteObject(hbrLightOrange);

}

/*********************************************************************************************************/

