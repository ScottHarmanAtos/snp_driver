
#include "stdafx.h"
#include "Riedel_GRDs.h"


CRiedel_GRDs::CRiedel_GRDs(int iDevNum, int iMaxSrces, int iMaxDests, int iAssocTrunk)
{
	m_iGRDDeviceNumber = iDevNum;     // riedel grd dev ini number
	m_iMaxSources = iMaxSrces;                  // db0 size
	m_iMaxDestinations = iMaxDests;            // db1 size
	m_iAssoc_Trunk = iAssocTrunk;  // trunk address into ring class
	m_iRevertivesReceived = 0;

	// create entries in maps for dests and sources
	for (int iS = 1; iS <= m_iMaxSources; iS++) {
		CRouterSrceData* pData = new CRouterSrceData(iS);
		if (pData) { // add to map
			cl_SrcRouterData.insert(pair<int, CRouterSrceData*>(iS, pData));
		}
	}
	// create entries in maps for dests and sources
	for (int iD = 1; iD <= m_iMaxDestinations; iD++) {
		CRouterDestData* pData = new CRouterDestData(iD);
		if (pData) { // add to map
			cl_DestRouterData.insert(pair<int, CRouterDestData*>(iD, pData));
		}
	}
}

CRiedel_GRDs::~CRiedel_GRDs()
{
	// seek and destroy
	while (cl_DestRouterData.begin() != cl_DestRouterData.end()) {
		map<int, CRouterDestData*>::iterator it = cl_DestRouterData.begin();
		if (it->second) delete it->second;
		cl_DestRouterData.erase(it);
	}
	while (cl_SrcRouterData.begin() != cl_SrcRouterData.end()) {
		map<int, CRouterSrceData*>::iterator it = cl_SrcRouterData.begin();
		if (it->second) delete it->second;
		cl_SrcRouterData.erase(it);
	}
}


////////////////////////////////////////////////////
// private internal functions for src/dest maps 
CRouterSrceData* CRiedel_GRDs::GetSourceData(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		map<int, CRouterSrceData*>::iterator it = cl_SrcRouterData.find(iSource);
		if (it != cl_SrcRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

CRouterDestData* CRiedel_GRDs::GetDestinationData(int iDest)
{
	if ((iDest>0) && (iDest <= m_iMaxDestinations)) {
		map<int, CRouterDestData*>::iterator it = cl_DestRouterData.find(iDest);
		if (it != cl_DestRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////

int CRiedel_GRDs::getDevice(void)
{
	return m_iGRDDeviceNumber;
}

int CRiedel_GRDs::getRingTrunk(void)
{
	return m_iAssoc_Trunk;
}

int CRiedel_GRDs::getMaximumDestinations(void)
{
	return m_iMaxDestinations;
}

int CRiedel_GRDs::getMaximumSources(void)
{
	return m_iMaxSources;
}


void CRiedel_GRDs::storeRiedelRevertive(int iDestination, int iSource)
{
	// store given source from actual rev; any re-enterant src/dest calc then done  if required
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			// sort out routed to lists - prev and new
			int iPrevSrc = pData->m_iRoutedIndex;
			CRouterSrceData* pPrevSrce = GetSourceData(iPrevSrc);
			if (pPrevSrce) {
				int iPos = pPrevSrce->m_ssl_SourceRoutedTo.find(bncs_string(iDestination));
				if (iPos >= 0) pPrevSrce->m_ssl_SourceRoutedTo.deleteItem(iPos);
			}
			// store new
			pData->m_iRoutedIndex = iSource;
			pData->m_iPendingSource = iSource;  // so it is inline with rev too
			pData->m_iTracedGrd = 0;
			pData->m_iTracedRing = 0;
			pData->m_iTracedSource = iSource; // until trace recalculated on full revertive process  - set to 0 ???
			m_iRevertivesReceived++;
			if (m_iRevertivesReceived > 1000000) m_iRevertivesReceived = m_iMaxDestinations;  // reset when too high
			//
			CRouterSrceData* pSrce = GetSourceData(pData->m_iRoutedIndex);
			if (pSrce) {
				if (pSrce->m_ssl_SourceRoutedTo.find(bncs_string(iDestination)) < 0) {
					pSrce->m_ssl_SourceRoutedTo.append(iDestination);
					pSrce->m_ssl_SourceRoutedTo.sort();
				}
			}
		} 
		else
			OutputDebugString(LPCSTR(bncs_string("CRiedel_GRDs::storeRiedelRevertive no data class for destination %1\n").arg(iDestination)));
	}
	else
		OutputDebugString(LPCSTR(bncs_string("CRiedel_GRDs::storeRiedelRevertive negative destination or greater than maxUsed %1\n").arg(iDestination)));
}

int CRiedel_GRDs::getRoutedSourceForDestination(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_iRoutedIndex;
		}
	}
	return 0;
}

void CRiedel_GRDs::storeTracedRingSource(int iDestination, int iRing, int iGrdDev, int iSource)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iTracedGrd = iGrdDev;
			pData->m_iTracedRing = iRing;
			pData->m_iTracedSource = iSource;
		}
	}
}

int CRiedel_GRDs::getTracedRingForDestination(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_iTracedRing;
		}
	}
	return 0;
}

int CRiedel_GRDs::getTracedGrdForDestination(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_iTracedGrd;
		}
	}
	return 0;
}

int CRiedel_GRDs::getTracedSourceForDestination(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_iTracedSource;
		}
	}
	return 0;
}


void CRiedel_GRDs::storeAssocDestHighway(int iDestination, int iHigh)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) pData->m_isDestHighway = iHigh;
	}
}

void CRiedel_GRDs::storeAssocSrceHighway(int iSource, int iHigh)
{
	if ((iSource > 0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) pData->m_isHighway = iHigh;
	}
}

int CRiedel_GRDs::getAssocDestHighway(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_isDestHighway;
	}
	return 0;
}

int CRiedel_GRDs::getAssocSrceHighway(int iSource)
{
	if ((iSource > 0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) return pData->m_isHighway;
	}
	return 0;
}

bncs_string CRiedel_GRDs::getAllDestinationsForSource(int iSource)
{
	if ((iSource > 0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) return pData->m_ssl_SourceRoutedTo.toString(',');
	}
	return "";
}

int CRiedel_GRDs::getNumberDestinationsUsingSource(int iSource)
{
	if ((iSource > 0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) return pData->m_ssl_SourceRoutedTo.count();
	}
	return 0;
}

///////////////////////// locks


void CRiedel_GRDs::changeLockStatus(int iDestination, int iState, bncs_string ssReason)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iLockState = iState;
			pData->m_ss_ReasonTimeStamp = ssReason;
		}
	}
}

void CRiedel_GRDs::unlockDestination(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iLockState = 0;
			// ??? get time on lock and unlock functions 
			pData->m_ss_ReasonTimeStamp = "";
		}
	}
}

int CRiedel_GRDs::getLockStatus(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iLockState;
	}
	return 0;
}

int CRiedel_GRDs::getNumberRevertivesReceived()
{
	return m_iRevertivesReceived;
}

bncs_string CRiedel_GRDs::getLockReasonTimeStamp(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_ss_ReasonTimeStamp;
	}
	return "";
}

/// auto ring ports mapping 

void CRiedel_GRDs::setDestinationMapping(int iDestination, int iInfodriver, int iSlotIndex)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iWhichPortsInfodriver = iInfodriver;
			pData->m_iWhichPortsSlot = iSlotIndex;
		}
	}
}

void CRiedel_GRDs::getDestinationMapping(int iDestination, int* iInfo, int* iSlot)
{
	*iInfo = 0;
	*iSlot = 0;
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			*iInfo = pData->m_iWhichPortsInfodriver;
			*iSlot = pData->m_iWhichPortsSlot;
		}
	}
}

int CRiedel_GRDs::getDestinationMappingInfoDrv(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iWhichPortsInfodriver;
	}
	return 0;
}

int CRiedel_GRDs::getDestinationMappingInfoSlot(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iWhichPortsSlot;
	}
	return 0;
}

void CRiedel_GRDs::setHighwayDestPendingSource(int iDestination, int iSource)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iPendingSource = iSource;
		}
	}
}

int CRiedel_GRDs::getHighwayDestPendingSource(int iDestination)
{
	if ((iDestination > 0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iPendingSource;
	}
	return 0;
}

