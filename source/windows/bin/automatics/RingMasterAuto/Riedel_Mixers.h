#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "RingMasterConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class to store riedel revertives for mixers

class CRiedel_Mixers
{
private:
	int m_iRecordIndex;
	int m_iRevDevice;
	int m_iRevSlot;
	char m_splitChar;
	int m_iRevertive_TrunkAddr;  // can be used to index into ring class for this revertive record for infodriver device

	bncs_stringlist m_ssl_Data; // raw data split by char

	bncs_string m_ss_Inputs, m_ss_PreviousInputs;         // first of 2 revertives for riedel infodrv revs
	bncs_string m_ss_Outputs, m_ss_PreviousOutputs;   // 2nd infodrv revs - need to know prev if outputs change 

public:
	CRiedel_Mixers(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocTrunk);
	~CRiedel_Mixers();

	void storeRiedelData(bncs_string ss);

	int getRecordIndex(void);
	int getDevice(void);
	int getSlot(void);
	int getTrunkAddress(void);

	bncs_string getRawRiedelRevertive(void);
	bncs_string getMixer_Inputs(void);
	bncs_string getMixer_Outputs(void);
	bncs_string getMixer_PreviousRev(int iWhichType);

	BOOL AreChangesinRevertive(int iWhichType);

};

