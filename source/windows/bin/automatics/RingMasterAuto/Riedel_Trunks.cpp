
#include "stdafx.h"
#include "Riedel_Trunks.h"


CRiedel_Trunks::CRiedel_Trunks(int iRecIndex, int iDevNum, int iRevSlot, char sc, int iAssocRing)
{
	m_iRecordIndex = iRecIndex;
	m_iRevDevice = iDevNum;
	m_iRevSlot = iRevSlot;
	m_splitChar = sc;
	m_iAssoc_Ring = iAssocRing;  // index into ring class
	m_ssraw_rev = "0";

}

CRiedel_Trunks::~CRiedel_Trunks()
{
}


void CRiedel_Trunks::storeRiedelData(bncs_string ss)
{
	m_ssraw_rev = ss;
}

int CRiedel_Trunks::getRecordIndex(void)
{
	return m_iRecordIndex;
}

int CRiedel_Trunks::getDevice(void)
{
	return m_iRevDevice;
}

int CRiedel_Trunks::getSlot(void)
{
	return m_iRevSlot;
}

int CRiedel_Trunks::getRing(void)
{
	return m_iAssoc_Ring;
}

bncs_string CRiedel_Trunks::getRawRiedelRevertive(void)
{
	return m_ssraw_rev;
}

