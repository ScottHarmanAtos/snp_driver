// RouterHighway.h: interface for the CRouerHighway class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROUTERHIGHWAY_H__D8705B7E_1572_4A75_8B28_0C46F8899C26__INCLUDED_)
#define AFX_ROUTERHIGHWAY_H__D8705B7E_1572_4A75_8B28_0C46F8899C26__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "tieline_manager_common.h"


class CRouterHighway  
{
private:
	
	int m_iHighwayIndex;         // INTERNAL highway number + key in map

	int m_iConfigTielineIndex;   // highway number from config entry
	// highway type
	int m_iHighwayType;          // pool (general use), or specific types - specific types routed to specific dests

	// highway data
	int m_iFromRouter;           // uses real bncs router indicies ( not virtual grd )
	int m_iFromDestination;       
	int m_iToRouter;
    int m_iToSource;

	bncs_stringlist ssl_LinkedHighways;    // list of contig highways routed at same time as this - for multiple audio routes etc when specified in mask of RC command. 

	// state
	int m_iHighwayState;                   // parked (free+available), pending (), allocated(in use), hanging, maintainence, bars, ....
	int m_iAllocated_Source;               // rtr src - ??? -- allocated to this highway when in pending state and awaiting real rtr rev.
	int m_iPendingCount;                   // count used by highway mgmt functions before clearing etc -- e.g. gives time get back revs from devices that may be slow
	int m_iParkitCount;                       // count used by highway mgmt - if park src and has users - when set to -2, then route local park to far users to clear highway

	bncs_string ss_UseDescription;         // description of why used ?? -- ultimate src perhaps ?? cbis booking ref ??
	bncs_string ss_TimeLastStateChange;    // date+time of last state change

	// other vars used by auto
	int m_iListBoxEntry;


public:
	CRouterHighway( int iIndex, int iConfigIndex, int iFromRtr, int iFromDest, int iToRtr, int iToSource, int iType );
	virtual ~CRouterHighway();
	
	void setHighwayState( int iNewState, int iAllocSrc, int iPending );
	void setHighwayDescription( bncs_string ssDesc );

	int getHighwayIndex( void );
	int getTielineConfigIndex(void);

	int getHighwayData( int *iFromRtr, int *iFromDest, int *iToRtr, int *iToSource );
	int getFromRouter();
	int getFromDestination();
	int getToRouter();
	int getToSource();

	int getHighwayType( void );
	int getHighwayState( void );

	bncs_string getHighwayDescription( void );
	bncs_string getHighwayTimeChanged( void );

	void setListBoxEntry( int iIndex );
	int getListBoxEntry( void );

	void addLinkedHighwayIndex( int iOtherHighway );
	void removeLinkedHighwayIndex( int iOtherHighway );
	void clearLinkedHighwaysList();

	int getNumberOfLinkedHighways();
	bncs_string getListOfLinkedHighways();

	void assignAllocatedHighwaySource( int iSrc );
	int getAllocatedHighwaySource( void );

	void decrimentHighwayPendingCount();
	int getHighwayPendingCount(void);

	void resetParkitCount(void);
	void decrimentHighwayParkitCount();
	int getHighwayParkitCount(void);

};

#endif // !defined(AFX_ROUTERHIGHWAY_H__D8705B7E_1572_4A75_8B28_0C46F8899C26__INCLUDED_)
