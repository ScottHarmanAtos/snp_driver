// RouterHighway.cpp: implementation of the CRouterHighway class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RouterHighway.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRouterHighway::CRouterHighway(int iIndex, int iConfigIndex, int iFromRtr, int iFromDest, int iToRtr, int iToSource, int iType)
{
	m_iHighwayIndex=0;
	m_iConfigTielineIndex = 0;
	m_iFromRouter=0;
	m_iFromDestination=0;
	m_iToRouter=0;
	m_iToSource=0;
	m_iHighwayType=0;
	ss_UseDescription="";
	ss_TimeLastStateChange="";	
	m_iListBoxEntry=UNKNOWNVAL;
	//
	m_iHighwayIndex=iIndex;
	m_iConfigTielineIndex = iConfigIndex;
	m_iFromRouter=iFromRtr;
	m_iFromDestination=iFromDest;
	m_iToRouter=iToRtr;
	m_iToSource=iToSource;
	m_iHighwayType = iType;
	m_iHighwayState = HIGHWAY_FREE; //  first pass will ascertain state
	if (m_iHighwayType==HIGHWAY_WRAP_TYPE)	{
		m_iHighwayState = HIGHWAY_INUSE; 
	}
	m_iAllocated_Source = 0;
	m_iPendingCount = 0;
	m_iParkitCount = 0;

	ssl_LinkedHighways=bncs_stringlist( "", '|');

}

CRouterHighway::~CRouterHighway()
{
	// fade to black
}


void CRouterHighway::setHighwayState( int iNewState, int iAllocSrc, int iPending )
{
	m_iHighwayState = iNewState;
	if (iAllocSrc>UNKNOWNVAL)	m_iAllocated_Source = iAllocSrc; // does not overwrite if -1 passed as src
	if (iPending > UNKNOWNVAL)	m_iPendingCount=iPending; // does not overwrite if -1 passed as pending - way of changing state without altering already pending counter value
	m_iParkitCount = 0;
}


void CRouterHighway::setHighwayDescription( bncs_string ssDesc )
{
	ss_UseDescription = ssDesc;
}


int CRouterHighway::getHighwayIndex(void)
{
	return m_iHighwayIndex;
}


int CRouterHighway::getTielineConfigIndex(void)
{
	return m_iConfigTielineIndex;
}


int CRouterHighway::getHighwayType( void )
{
	return m_iHighwayType;
}


int CRouterHighway::getHighwayState( void )
{
	return m_iHighwayState;
}


int CRouterHighway::getFromRouter()
{
	return m_iFromRouter;
}


int CRouterHighway::getFromDestination()
{
	return m_iFromDestination;
}


int CRouterHighway::getToRouter()
{
	return m_iToRouter;
}


int CRouterHighway::getToSource()
{
	return m_iToSource;
}


int CRouterHighway::getHighwayData( int *iFromRtr, int *iFromDest, int *iToRtr, int *iToSource )
{
	*iFromRtr = m_iFromRouter;
	*iFromDest = m_iFromDestination;
	*iToRtr = m_iToRouter;
	*iToSource = m_iToSource;
	return m_iHighwayState;
}


bncs_string CRouterHighway::getHighwayDescription( void )
{
	return ss_UseDescription;
}


bncs_string CRouterHighway::getHighwayTimeChanged( void )
{
	return ss_TimeLastStateChange;
}


void CRouterHighway::setListBoxEntry( int iIndex )
{
	m_iListBoxEntry = iIndex;
}


int CRouterHighway::getListBoxEntry( void )
{
	return m_iListBoxEntry;
}


//// highway linked list - for contig highways when routing stereo/multiple audio routes between routers

void CRouterHighway::addLinkedHighwayIndex( int iOtherHighway )
{
	if (ssl_LinkedHighways.find(iOtherHighway)<0)
		ssl_LinkedHighways.append(iOtherHighway);
}


void CRouterHighway::removeLinkedHighwayIndex( int iOtherHighway )
{
	int indx = ssl_LinkedHighways.find(iOtherHighway);
	if (indx>=0) ssl_LinkedHighways.deleteItem(indx);
}


void CRouterHighway::clearLinkedHighwaysList()
{
	ssl_LinkedHighways.clear();
}


int CRouterHighway::getNumberOfLinkedHighways()
{
	return ssl_LinkedHighways.count();
}


bncs_string CRouterHighway::getListOfLinkedHighways()
{
	return ssl_LinkedHighways.toString('|');
}


void CRouterHighway::assignAllocatedHighwaySource( int iSrc )
{
	m_iAllocated_Source = iSrc;
}

int CRouterHighway::getAllocatedHighwaySource( void )
{
	return m_iAllocated_Source;
}

void CRouterHighway::decrimentHighwayPendingCount(void)
{
	if (m_iPendingCount>0) m_iPendingCount--;
}


int CRouterHighway::getHighwayPendingCount(void)
{
	return m_iPendingCount;
}

void CRouterHighway::resetParkitCount(void)
{
	m_iParkitCount = 0;
}

void CRouterHighway::decrimentHighwayParkitCount()
{
	m_iParkitCount--;
}

int CRouterHighway::getHighwayParkitCount(void)
{
	return m_iParkitCount;
}


