/* tieline_manager.cpp - custom program file for project tieline_manager */

#include "stdafx.h"
#include "tieline_manager.h"

BOOL SlotChange( UINT iDevice,UINT iSlot, LPCSTR szSlot );
char* ExtractSrcNamefromDevIniFile( int iDev, int iDbFile, int iEntry );
BOOL StoreVirtualRouterRevertive( int iActualRtr, int iActualDest, int iVirtualDest );
BOOL ProcessPhysicalRouterRevertive(int iActualRtr, int iActualDest, int iNewSource);
CRouterHighway* GetNextPoolHighway(int iRtrSource, int iFromRouter, int iToRouter, int iFinalDest, int iIgnoreHighway);
CRouterHighway* GetRequiredHighwayOfType(int iRtrSource, int iFromRouter, int iToRouter, int iFinalDest, int iRequiredType);

////////////////////////////////////////////////////////////
//
// strip quotes from info revertives or rm names
//
void StripOutQuotes(LPSTR szStr, char iCharToStrip)
{
	int iLen = strlen(szStr);
	if (iLen>0) {
		int iI = 0, iJ = 0;
		char szTmp[MAX_BUFFER_STRING] = "";
		for (iI = 0; iI<iLen; iI++) {
			if (szStr[iI] != iCharToStrip) {
				szTmp[iJ] = szStr[iI];
				iJ++;
			}
		}
		szTmp[iJ] = NULL; // terminate modified str
		if (bShowAllDebugMessages) Debug("StripOutQuotes b:%s a:%s char:%c", szStr, szTmp, iCharToStrip);
		strcpy(szStr, szTmp);	 // copy back
	}
}

//////////////////////////////////////////////////////////////////////////////////////



CRevsRouterData* GetValidRouter(int iWhichRtr)
{
	if (mapOfAllRouters) {
		if (iWhichRtr > 0) {
			map<int, CRevsRouterData*>::iterator itp;
			itp = mapOfAllRouters->find(iWhichRtr);
			if (itp != mapOfAllRouters->end()) {
				return itp->second;
			}
		}
		else
			Debug("GetValidRouter -- zero router number passed ");
	}
	else
		Debug("GetValidRouter -- ERROR - no cl_allRouters class ");
	return NULL;
}




//////////////////////////////////////////////////////////////////////////////////////
//
//   Error messages via infodriver slots 
//

void UpdateAutoAlarmStateSlot()
{
	// go thru the routers and determine overall alarm state
	iOverallAutoAlarmState = ALARM_CLEAR;
	//
	// are any routers with error messages
	for (int iindx = 0; iindx<ssl_AllRoutersIndices.count(); iindx++) {
		CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iindx].toInt());
		if (pRtr->getRouterAlarmStatus() > iOverallAutoAlarmState) iOverallAutoAlarmState = pRtr->getRouterAlarmStatus();
	}
	if (pRouterVirtual->getRouterAlarmStatus() > iOverallAutoAlarmState) iOverallAutoAlarmState = pRouterVirtual->getRouterAlarmStatus();
	//
	switch (iOverallAutoAlarmState) {
		case ALARM_CLEAR: eiInfoGrd->updateslot(SLOT_OVERALL_ALARM, "0"); break;  // clear
		case ALARM_WARNING: eiInfoGrd->updateslot(SLOT_OVERALL_ALARM, "1"); break;  // warning
		case ALARM_ERROR_STATE: eiInfoGrd->updateslot(SLOT_OVERALL_ALARM, "2"); break;  // general alarm
		case ALARM_SEVERE_STATE: eiInfoGrd->updateslot(SLOT_OVERALL_ALARM, "3"); break;  // severe alarm
	}
}


void PostAutomaticErrorMessage(int iFromRouter, int iMessageType,  bncs_string ssMessage)
{
	// get date and time  -- post messages starting at 4011.. 4013 for routers, if fromtrouter==0, general error message to slot 4010.
	// if ssMessage is empty - can be used to clear previous message
	// add time to message
	char szMessage[MAX_BUFFER_STRING] = "";
	char tBuffer[9];
	char szDate[16];
	struct tm *newtime;
	time_t long_time;
	time(&long_time);                // Get time as long integer. 
	newtime = localtime(&long_time); // Convert to local time. 
	_strtime(tBuffer);
	wsprintf(szDate, "%02d:%02d:%02d", newtime->tm_hour, newtime->tm_min, newtime->tm_sec);

	if (pRouterVirtual->getRouterIndex() == iFromRouter) {
		// general / virtual rtr message
		if (iMessageType == ALARM_SET_ERROR_MESSAGE) {
			wsprintf(szMessage, "%s - %s", szDate, LPCSTR(ssMessage));
			pRouterVirtual->setRouterErrorMessage(bncs_string(szMessage));
			pRouterVirtual->setRouterAlarmStatus(ALARM_ERROR_STATE);
			eiInfoGrd->updateslot(SLOT_VIRTUAL_MESSAGE, szMessage);
		}
		else if (iMessageType == ALARM_SET_WARN_MESSAGE) {
			pRouterVirtual->setRouterThresholdMessage(ssMessage);
			if (pRouterVirtual->getRouterAlarmStatus() < ALARM_ERROR_STATE) {
				pRouterVirtual->setRouterAlarmStatus(ALARM_WARNING);
				wsprintf(szMessage, "%s", LPCSTR(ssMessage));
				eiInfoGrd->updateslot(SLOT_VIRTUAL_MESSAGE, szMessage);
			}
		}
		else if (iMessageType == ALARM_CLEAR_MESSAGE) {
			if (pRouterVirtual->getRouterAlarmStatus() == ALARM_ERROR_STATE) {
				pRouterVirtual->setRouterErrorMessage(" ");
				if (pRouterVirtual->getRouterThresholdMessage().length() > 2) {
					pRouterVirtual->setRouterAlarmStatus(ALARM_WARNING);
					wsprintf(szMessage, "%s", LPCSTR(pRouterVirtual->getRouterThresholdMessage()));
					eiInfoGrd->updateslot(SLOT_VIRTUAL_MESSAGE, szMessage);
				}
				else {
					pRouterVirtual->setRouterAlarmStatus(ALARM_CLEAR);
					eiInfoGrd->updateslot(SLOT_VIRTUAL_MESSAGE, "");
				}
			}
			else if (pRouterVirtual->getRouterAlarmStatus() == ALARM_WARNING) {
				pRouterVirtual->setRouterThresholdMessage(" ");
				pRouterVirtual->setRouterAlarmStatus(ALARM_CLEAR);
				eiInfoGrd->updateslot(SLOT_VIRTUAL_MESSAGE, "");
			}
		}
	}
	else {
		CRevsRouterData* pRtr = GetValidRouter(iFromRouter);
		if (pRtr) {
			if (iMessageType == ALARM_SET_ERROR_MESSAGE) {
				wsprintf(szMessage, "%s - %s", szDate, LPCSTR(ssMessage));
				pRtr->setRouterErrorMessage(bncs_string(szMessage));
				pRtr->setRouterAlarmStatus(ALARM_ERROR_STATE);
				eiInfoGrd->updateslot(pRtr->getSlotMessageIndex(), szMessage);
			}
			else if (iMessageType == ALARM_SET_WARN_MESSAGE) {
				pRtr->setRouterThresholdMessage(ssMessage);
				if (pRtr->getRouterAlarmStatus() < ALARM_ERROR_STATE) {
					pRtr->setRouterAlarmStatus(ALARM_WARNING);
					wsprintf(szMessage, "%s", LPCSTR(ssMessage));
					eiInfoGrd->updateslot(pRtr->getSlotMessageIndex(), szMessage);
				}
			}
			else if (iMessageType == ALARM_CLEAR_MESSAGE) {
				// request to clear alarm highest level message - downgrade to threshold if there is one
				if (pRtr->getRouterAlarmStatus() == ALARM_ERROR_STATE) {
					pRtr->setRouterErrorMessage(" ");
					if (pRtr->getRouterThresholdMessage().length() > 2) {
						pRtr->setRouterAlarmStatus(ALARM_WARNING);
						wsprintf(szMessage, "%s", LPCSTR(pRtr->getRouterThresholdMessage()));
						eiInfoGrd->updateslot(pRtr->getSlotMessageIndex(), szMessage);
					}
					else {
						pRtr->setRouterAlarmStatus(ALARM_CLEAR);
						eiInfoGrd->updateslot(pRtr->getSlotMessageIndex(), "");
					}
				}
				else if (pRtr->getRouterAlarmStatus() == ALARM_WARNING) {
					pRtr->setRouterThresholdMessage(" ");
					pRtr->setRouterAlarmStatus(ALARM_CLEAR);
					eiInfoGrd->updateslot(pRtr->getSlotMessageIndex(), "");
				}
			}
		}
	}
	UpdateAutoAlarmStateSlot();
}


void SetAlarmThresholdMessage(int iFromRouter,  int iPercentUsed)
{
	bncs_string ss_message = "";
	int iState = ALARM_CLEAR_MESSAGE;
	if (pRouterVirtual->getRouterIndex() == iFromRouter) {
		// generic / virtual message -- probably because total usage is VERY high
		if (iPercentUsed >79) {
			iState = ALARM_SET_WARN_MESSAGE;
			ss_message = bncs_string("Warning as Total Tieline usage is at %1 percent - check highways").arg(iPercentUsed);
			if (iPercentUsed > 89) ss_message = bncs_string("SEVERE WARNING- TOTAL TIELINE USAGE IS AT %1 PERCENT - check highways").arg(iPercentUsed);
		}
		pRouterVirtual->setRouterThresholdMessage(ss_message);
		if (pRouterVirtual->getRouterAlarmStatus()<ALARM_ERROR_STATE) 	PostAutomaticErrorMessage(iFromRouter, iState, ss_message);
	}
	else {
		CRevsRouterData* pRtr = GetValidRouter(iFromRouter);
		if (pRtr) {
			if (iPercentUsed > 79) {
				iState = ALARM_SET_WARN_MESSAGE;
				ss_message = bncs_string("Warning - Tieline use from Rtr %1 at %2 percent ").arg(iFromRouter).arg(iPercentUsed);
				if (iPercentUsed >= 94) ss_message = bncs_string("WARNING - TIELINE USE  from Rtr %1 at %2 percent ").arg(iFromRouter).arg(iPercentUsed);
			}
			pRtr->setRouterThresholdMessage(ss_message);
			if (pRtr->getRouterAlarmStatus()<ALARM_ERROR_STATE) 	PostAutomaticErrorMessage(iFromRouter, iState, ss_message);
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
// GUI FUNCTIONS


void LoadSourcesListbox()
{
	char szFileName[32];
	char szwhere [MAX_BUFFER_STRING], szIndex[MAX_LOADSTRING] ;
	char szData[MAX_BUFFER_STRING], szName[MAX_LOADSTRING];
	wsprintf(szFileName,"DEV_%03d.ini",iGRDDevice);	
	
	if (iLoadSrcListBoxEntry==1) SendDlgItemMessage(hWndDlg, IDC_LISTSRC, LB_RESETCONTENT, 0, 0); // at start - so clear first
	// section - need to determine if in ini file or in db0 file
	strcpy( szwhere, r_p(szFileName, "Database", "DatabaseFile_0", "", FALSE ));

	// get source numbers - for current block of 
	int iMaxthistime = iLoadSrcListBoxEntry+511;
	if (iMaxthistime>pRouterVirtual->getMaxSources()) iMaxthistime=pRouterVirtual->getMaxSources();
	// load block of names
	for (int iIndx=iLoadSrcListBoxEntry;iIndx<=iMaxthistime;iIndx++) {
		wsprintf( szIndex, "%04d", iIndx );
		strcpy( szName, r_p(szwhere, "Database_0", szIndex, "", FALSE ));
		wsprintf(szData, "%s %s", szIndex, szName );
		SendDlgItemMessage(hWndDlg, IDC_LISTSRC, LB_INSERTSTRING, -1, (LPARAM)szData);
	}
	//
	iLoadSrcListBoxEntry=iMaxthistime+1;
}


void LoadDestinationsListbox()
{
	char szFileName[32];
	char szwhere [MAX_BUFFER_STRING], szIndex[MAX_LOADSTRING] ;
	char szData[MAX_BUFFER_STRING], szName[MAX_LOADSTRING];
	wsprintf(szFileName,"DEV_%03d.ini",iGRDDevice);		

	if (iLoadDestListBoxEntry==1) SendDlgItemMessage(hWndDlg, IDC_LISTDEST, LB_RESETCONTENT, 0, 0); // at start - so clear first
	// section - need to determine if in ini file or in db0 file
	strcpy( szwhere, r_p(szFileName, "Database", "DatabaseFile_1", "", FALSE ));
	// get destination numbers
	int iMaxthistime = iLoadDestListBoxEntry+511;
	if (iMaxthistime>pRouterVirtual->getMaxDestinations()) iMaxthistime=pRouterVirtual->getMaxDestinations();
	// load block of names
	for (int iIndx=iLoadDestListBoxEntry;iIndx<=iMaxthistime;iIndx++) {
		wsprintf( szIndex, "%04d", iIndx);   
		strcpy( szName, r_p(szwhere, "Database_1", szIndex, "", FALSE ));
		wsprintf(szData, "%s %s", szIndex, szName );
		SendDlgItemMessage(hWndDlg, IDC_LISTDEST, LB_INSERTSTRING, -1, (LPARAM)szData);
	}
	//
	iLoadDestListBoxEntry=iMaxthistime+1;
}



void DisplayHighwayData( int iHighway )
{
	// clear fields
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_TYPE, " ");
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_FROMDEST, " ");
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_FROMRTR, " ");
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_TOSRC, " ");
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_TORTR, " ");
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_STATE, " ");
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_ACTSOURCE, " ");
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_USERS, " ");	
	SetDlgItemText( hWndDlg, IDC_HIGHWAY_USERS2, " ");	
	//
	if ((iHighway>0)&&(iHighway<=iTotalHighwaysAndWraps)) {
		CRouterHighway* pHigh = GetRouterHighway( iHighway );
		if (pHigh) {
			int iFromRtr, iFromDest, iToRtr, iToSrc;
			char szData[MAX_BUFFER_STRING];
			pHigh->getHighwayData(&iFromRtr, &iFromDest, &iToRtr, &iToSrc );
			SetDlgItemInt( hWndDlg, IDC_HIGHWAY_FROMRTR, iFromRtr, TRUE);
			SetDlgItemInt( hWndDlg, IDC_HIGHWAY_FROMDEST, iFromDest, TRUE);
			SetDlgItemInt( hWndDlg, IDC_HIGHWAY_TORTR, iToRtr, TRUE);
			SetDlgItemInt( hWndDlg, IDC_HIGHWAY_TOSRC, iToSrc, TRUE);
			//
			if (pHigh->getHighwayType() == HIGHWAY_TX_TYPE)  SetDlgItemText(hWndDlg, IDC_HIGHWAY_TYPE, "TX H-Way");
			else if (pHigh->getHighwayType() == HIGHWAY_LIVE_TYPE)  SetDlgItemText(hWndDlg, IDC_HIGHWAY_TYPE, "Live H-Way");
			else if (pHigh->getHighwayType() == HIGHWAY_ENG_TYPE)  SetDlgItemText(hWndDlg, IDC_HIGHWAY_TYPE, "Eng H-Way");
			else if (pHigh->getHighwayType() == HIGHWAY_SUPVR_TYPE)  SetDlgItemText(hWndDlg, IDC_HIGHWAY_TYPE, "Supvr H-Way");
			else if (pHigh->getHighwayType() == HIGHWAY_CMVP_TYPE)  SetDlgItemText(hWndDlg, IDC_HIGHWAY_TYPE, "C mvp H-Way");
			else if (pHigh->getHighwayType() == HIGHWAY_LMVP_TYPE)  SetDlgItemText(hWndDlg, IDC_HIGHWAY_TYPE, "L mvp H-Way");
			else if (pHigh->getHighwayType() == HIGHWAY_WRAP_TYPE)  SetDlgItemText(hWndDlg, IDC_HIGHWAY_TYPE, "Wrap H-Way");
			else if (pHigh->getHighwayType() == HIGHWAY_POOL_TYPE)  SetDlgItemText(hWndDlg, IDC_HIGHWAY_TYPE, "Pool H-Way");

			switch( pHigh->getHighwayState())
			{
				case HIGHWAY_UNKNOWN: SetDlgItemText( hWndDlg, IDC_HIGHWAY_STATE, "*unknown*"); break;
				case HIGHWAY_FREE: SetDlgItemText( hWndDlg, IDC_HIGHWAY_STATE,    "  FREE  "); break;
				case HIGHWAY_PENDING: SetDlgItemText( hWndDlg, IDC_HIGHWAY_STATE, "pending"); break;
				case HIGHWAY_INUSE: SetDlgItemText( hWndDlg, IDC_HIGHWAY_STATE,"IN USE"); break;
				case HIGHWAY_CLEARING: SetDlgItemText( hWndDlg, IDC_HIGHWAY_STATE, "clearing"); break;
			default:
				SetDlgItemText( hWndDlg, IDC_HIGHWAY_STATE, "????");
			}
			if (pHigh->getHighwayPendingCount()>0) 
				SetDlgItemInt(hWndDlg, IDC_HIGHWAY_PENDCOUNT, pHigh->getHighwayPendingCount(), TRUE);
			else
				SetDlgItemText(hWndDlg, IDC_HIGHWAY_PENDCOUNT, " ");
			//
			// get actual routed src for highway dest
			// get number users and list of users 
			int iActualSource=0, iVirSrc=0, iUsers=0;
			char szSrc[MAX_LOADSTRING]="";
			bncs_string ssUsers="";

			CRevsRouterData* pFromRtr = GetValidRouter(pHigh->getFromRouter());
			if (pFromRtr) {
				iActualSource = pFromRtr->getRoutedSourceForDestination(pHigh->getFromDestination());
				iVirSrc = iActualSource + pFromRtr->getRouterOffsetIntoVirtual();
				strcpy(szSrc, ExtractSrcNamefromDevIniFile(pFromRtr->getRouterIndex(), 0, iActualSource));
				if (pHigh->getHighwayType()==HIGHWAY_WRAP_TYPE) { // on same router
					iUsers = pFromRtr->getNumberOfDestsUsingSource(pHigh->getToSource());
					ssUsers = pFromRtr->getListOfDestsUsingSource(pHigh->getToSource());
				}
				else { // other highways on other router
					CRevsRouterData* pToRtr = GetValidRouter(pHigh->getToRouter());
					if (pToRtr) {
						iUsers = pToRtr->getNumberOfDestsUsingSource(pHigh->getToSource());
						ssUsers = pToRtr->getListOfDestsUsingSource(pHigh->getToSource());
					}
				}
			}
			wsprintf( szData, "%d %s", iActualSource, szSrc );
			SetDlgItemText( hWndDlg, IDC_HIGHWAY_ACTSOURCE, szData );
			SetDlgItemInt( hWndDlg, IDC_HIGHWAY_USERS, iUsers, TRUE );
			SetDlgItemText( hWndDlg, IDC_HIGHWAY_USERS2, LPCSTR(ssUsers.replace('|', ' ')));
		}
	}
}



//////////////////////////////////////////////////////////////////////////
// 
//  GET HIGHWAY FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////

CRouterHighway* GetRouterHighway( int iIndex )
{

	if ((iIndex>0)&&(iIndex<=iTotalHighwaysAndWraps)&&(mapOfAllHighways)) {
		map<int,CRouterHighway*>::iterator itp;
		itp = mapOfAllHighways->find(iIndex);
		if (itp!=mapOfAllHighways->end()) {
			if (itp->second) return itp->second;
		}
		Debug( "GetRouterHighway - failed to find highway %d in map", iIndex );
	}
	return NULL;
}


CRouterHighway* GetRouterHighwayFromDest( int iFromRtr, int iFromDest )
{
	if ((iFromRtr>0)&&(iFromDest>0)&&(mapOfAllHighways)) {
		map<int,CRouterHighway*>::iterator itp;
		itp = mapOfAllHighways->begin();
		while (itp!=mapOfAllHighways->end()) {
			if (itp->second) {
				if ((itp->second->getFromRouter()==iFromRtr)&&
					(itp->second->getFromDestination()==iFromDest))
					return itp->second;
			}
			itp++;
		} // while
		//Debug( "GetRouterHighwayFromDest - failed to find highway from rtr %d dest %d in map", iFromRtr, iFromDest );
	}
	return NULL;
}


CRouterHighway* GetRouterHighwayToSource( int iToRtr, int iToSrc )
{
	if ((iToRtr>0)&&(iToSrc>0)&&(mapOfAllHighways)) {
		map<int,CRouterHighway*>::iterator itp;
		itp = mapOfAllHighways->begin();
		while (itp!=mapOfAllHighways->end()) {
			if (itp->second) {
				if ((itp->second->getToRouter()==iToRtr)&&
					(itp->second->getToSource()==iToSrc))
					return itp->second;
			}
			itp++;
		} // while	
		//Debug( "GetRouterHighwayToSource - failed to find highway TO rtr %d SRC %d in map", iToRtr, iToSrc );
	}
	return NULL;
}


int IsAssociatedHighway( int iRtr, int iDest, int iSrc )
{
	for (int iH=1;iH<=iTotalHighwaysAndWraps;iH++) {
		CRouterHighway* pHigh = GetRouterHighway(iH);
		if (pHigh) {
			if ((pHigh->getFromRouter()==iRtr)&&(pHigh->getFromDestination()==iDest)) return iH;
			else if ((pHigh->getToRouter()==iRtr)&&(pHigh->getToSource()==iSrc)) return iH;
		}
	}
	// no associated highway
	return UNKNOWNVAL;
}


int GetHighwayRoutedSource( CRouterHighway* pHigh )
{
	if (pHigh) {
		CRevsRouterData* pRtr = GetValidRouter(pHigh->getFromRouter());
		if (pRtr) {
			return pRtr->getRoutedSourceForDestination(pHigh->getFromDestination());
		}
	}
	return 0;
}


CRouterHighway* GetHighwayWithAllocatedSource( int iFromRtr, int iAllocatedSrc, int iIgnoreHw, int iHighwayReqType ) 
{
	if ((iFromRtr>0)&&(iAllocatedSrc>0)&&(mapOfAllHighways)) {
		// reset the pointer -- need to use another pointer instance as reset did not work
		map<int,CRouterHighway*>::iterator itpP = mapOfAllHighways->begin();
		while (itpP!=mapOfAllHighways->end()) {
			if (itpP->second) {
				if (itpP->second->getHighwayIndex()!=iIgnoreHw) {
					// use allocated source -- as current routes may need to still complete
					// check for program highways
					//if (bShowAllDebugMessages) 
					//	Debug( "GetHighAlloc-Prog- indx %d alloc src %d",itpP->second->getHighwayIndex(), itpP->second->getAllocatedHighwaySource() );
					if ((itpP->second->getFromRouter()==iFromRtr)&&
						(itpP->second->getHighwayState()!=HIGHWAY_UNKNOWN)&&
						(itpP->second->getHighwayType() == iHighwayReqType) &&
						(itpP->second->getAllocatedHighwaySource()==iAllocatedSrc))
						return itpP->second;
				}
			}
			itpP++;
		} // while
		//if (bShowAllDebugMessages) Debug( "GetHighwayWithAllocSrc - no existing allocated highway from rtr %d with src %d", iFromRtr, iAllocatedSrc );
	}
	return NULL;
}


//////////////////////////////////////////////////////////////////////////
// 
//  GET INTERSITE HIGHWAY FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////

CRouterHighway* GetIntersiteHighway( int iIndex )
{
	if ((iIndex>0)&&(iIndex<=iTotalHighwaysAndWraps)&&(mapOfAllHighways)) {
		map<int,CRouterHighway*>::iterator itp;
		itp = mapOfAllHighways->find(iIndex);
		if (itp!=mapOfAllHighways->end()) {
			if (itp->second) return itp->second;
		}
		Debug( "GetIntersiteHighway - failed to find highway %d in map", iIndex );
	}
	return NULL;
}



//////////////////////////////////////////////////////////////////////////
// 
//  CALCULATE HIGHWAY FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////

int CalculateHighwayUsed( void )
{
	char szData[128]="";
	i_unknown_highways=0;
	int iTotalUsed=0, iProgramUsed=0, iMonitorUsed=0;

	for (int indx = 1; indx <= iTotalHighwaysAndWraps; indx++) {
		CRouterHighway* phigh = GetRouterHighway(indx);
		if (phigh) {
			if (phigh->getHighwayType() != HIGHWAY_WRAP_TYPE) {
				if (phigh->getHighwayState() > HIGHWAY_FREE) iTotalUsed++;
			}
		}
	} // for 
	// update gui
	wsprintf(szData, "%d / %d", iTotalUsed, iNumberLoadedHighways);
	SetDlgItemText(hWndDlg, IDC_CONF_HIGHWAYS2, szData);

	/************************************************** xxx rework if stats really required
	for (int indx=1;indx<=iTotalHighwaysAndWraps;indx++) {
		CRouterHighway* phigh = GetRouterHighway( indx );
		if (phigh) {
			if (phigh->getHighwayType()!=HIGHWAY_WRAP_TYPE) {
				if (phigh->getHighwayState()>HIGHWAY_FREE){
					iTotalUsed++;
					//
					if (phigh->getHighwayType()==HIGHWAY_MONITOR_TYPE) {
						iMonitorUsed++;
						if (phigh->getFromRouter()==pRouterOrange->getRouterIndex()) iMonUsedO2E++;
						else if (phigh->getFromRouter()==pRouterEmerald->getRouterIndex()) iMonUsedE2O++;
					}
					//
					if (phigh->getHighwayType()==HIGHWAY_POOL_TYPE) {
						iProgramUsed++;
						if (phigh->getFromRouter()==pRouterOrange->getRouterIndex()) iProgUsedO2E++;
						else if (phigh->getFromRouter()==pRouterEmerald->getRouterIndex()) iProgUsedE2O++;
					}
				}
				else {
					// calculate UNKNOWNS -- to do
					if (phigh->getHighwayState()==HIGHWAY_UNKNOWN) {
						iTotalUsed++;
						i_unknown_highways++;
						wsprintf( szData, "%d,%d|", phigh->getFromRouter(), phigh->getFromDestination() );
						if (strlen(szUnknownHighways)<235) strcat(szUnknownHighways, szData);
						else Debug("CalcHighway - warning(3) more UNKNOWN highways than can fit string %d", i_unknown_highways );
					}
				}
			}
		}
	}

	// update gui and slots
	if (eiInfoGrd) {
		wsprintf(szData, "%d / %d", iProgUsedO2E, iProgrammeHighwaysO2E);
		eiInfoGrd->updateslot(SLOT_HIGHWAY_PROG_USAGE_OE, szData);
		SetDlgItemText(hWndDlg, IDC_CONF_HIGHWAYS7, szData);
		wsprintf(szData, "%d / %d", iMonUsedO2E, iMonitoringHighwaysO2E);
		eiInfoGrd->updateslot(SLOT_HIGHWAY_MONT_USAGE_OE, szData);
		SetDlgItemText(hWndDlg, IDC_CONF_HIGHWAYS6, szData);

		int iPercent = 0;
		if ((iProgrammeHighwaysO2E>0) && (iProgUsedO2E>0))
			iPercent = int(float(float(iProgUsedO2E) / float(iProgrammeHighwaysO2E)) * 100);
		// was if ((iProgrammeHighwaysO2E + iMonitoringHighwaysO2E)>0)
		// was	 iPercent = int(float(float(iProgUsedO2E + iMonUsedO2E) / float(iProgrammeHighwaysO2E + iMonitoringHighwaysO2E)) * 100);
		SetAlarmThresholdMessage(pRouterOrange->getRouterIndex(), iPercent);

		wsprintf(szData, "%d / %d", iProgUsedE2O, iProgrammeHighwaysE2O);
		eiInfoGrd->updateslot(SLOT_HIGHWAY_PROG_USAGE_EO, szData);
		SetDlgItemText(hWndDlg, IDC_CONF_HIGHWAYS9, szData);
		wsprintf(szData, "%d / %d", iMonUsedE2O, iMonitoringHighwaysE2O);
		eiInfoGrd->updateslot(SLOT_HIGHWAY_MONT_USAGE_EO, szData);
		SetDlgItemText(hWndDlg, IDC_CONF_HIGHWAYS8, szData);

		iPercent = 0;
		if ((iProgrammeHighwaysE2O>0) && (iProgUsedE2O>0))
			iPercent = int(float(float(iProgUsedE2O) / float(iProgrammeHighwaysE2O)) * 100);
		// was if ((iProgrammeHighwaysE2O + iMonitoringHighwaysE2O)>0)
		// was 	iPercent = int(float(float(iProgUsedE2O + iMonUsedE2O) / float(iProgrammeHighwaysE2O + iMonitoringHighwaysE2O)) * 100);
		SetAlarmThresholdMessage(pRouterEmerald->getRouterIndex(), iPercent);

		// update gui
		wsprintf(szData, "%d / %d", iTotalUsed, iNumberLoadedHighways);
		iPercent = 0;
		if ((iTotalUsed > 0) && (iNumberLoadedHighways > 0)) iPercent = int(float(float(iTotalUsed) / float(iNumberLoadedHighways)) * 100);
		SetAlarmThresholdMessage(pRouterVirtual->getRouterIndex(), iPercent);
		SetDlgItemText( hWndDlg, IDC_CONF_HIGHWAYS2, szData );	

		SetDlgItemInt( hWndDlg, IDC_CONF_HIGHWAYS3, iDisabledOE, TRUE);
		SetDlgItemInt( hWndDlg, IDC_CONF_HIGHWAYS10, iDisabledEO, TRUE);
		
		wsprintf( szData, "%d", i_unknown_highways );
		eiInfoGrd->updateslot( SLOT_COUNT_UNKNOWN_HWAYS, szData );
		eiInfoGrd->updateslot( SLOT_LIST_UNKNOWN_HWAYS, szUnknownHighways );
		SetDlgItemText( hWndDlg, IDC_MESSAGE, szData );
		if (i_unknown_highways>0) {
			if (iNextDestinationCheck>10) iNextDestinationCheck = 10;
		}
		
		wsprintf( szData, "%d / %d", iMonitorUsed, iNumberMonitoringHighways );
		SetDlgItemText( hWndDlg, IDC_CONF_HIGHWAYS4, szData );
		
		wsprintf( szData, "%d / %d", iProgramUsed, iNumberProgrammeHighways );
		SetDlgItemText( hWndDlg, IDC_CONF_HIGHWAYS5, szData );
	}
	else
		Debug("ERROR - no infodriver to update CalculatedHighways");
	*****************************/

	return iTotalUsed;
}





//////////////////////////////////////////////////////////////////////////
// 
//  COMMAND QUEUE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


void ClearCommandQueue( void )
{
	while (!qCommands.empty()) {
		CCommand* pCmd;
		pCmd = qCommands.front();
		qCommands.pop();
		delete pCmd;
	}
	SetDlgItemInt( hWndDlg, IDC_QUEUE, qCommands.size(), FALSE );
}


void AddCommandToQue( LPCSTR szCmd, int iCmdType, int iDev, int iIndex, int iDB )
{
	CCommand* pCmd = new CCommand;
	if (pCmd) {
		wsprintf( pCmd->szCommand, szCmd );
		pCmd->iTypeCommand = iCmdType;
		pCmd->iWhichDevice = iDev;
		pCmd->iWhichSlotDest = iIndex;
		pCmd->iWhichDatabase = iDB;
		qCommands.push( pCmd );
		SetDlgItemInt( hWndDlg, IDC_QUEUE, qCommands.size(), FALSE );
	}
	else 
		Debug( "AddCommand2Q - cmd record not created");
}


void ProcessNextCommand( int iRateRevs )
{
	
	BOOL bContinue=TRUE;
	KillTimer( hWndMain, COMMAND_TIMER_ID );
	bNextCommandTimerRunning = FALSE;
	do {
		iRateRevs--;
		if (qCommands.size()>0) {
			// something in queue so process it
			CCommand* pCommand = qCommands.front();
			qCommands.pop();
			if (pCommand) {
				// ONLY send cmd if main driver only or in starting mode for rx driver to register and poll initially
				if ((iOverallTXRXModeStatus==INFO_TXRXMODE)||(bAutomaticStarting==TRUE)) {
					if ((bShowAllDebugMessages) && (!bAutomaticStarting)) Debug("ProcessCommand type %d - TX> %s ", pCommand->iTypeCommand, pCommand->szCommand);
					if (ecCSIClient) {
						switch(pCommand->iTypeCommand){
						case ROUTERCOMMAND:	ecCSIClient->txrtrcmd(pCommand->szCommand);	break;
						case INFODRVCOMMAND: ecCSIClient->txinfocmd(pCommand->szCommand); break;
						default:
							Debug( "ProcessNextCmd - unknown type from buffer - %d %s", pCommand->iTypeCommand, pCommand->szCommand );
						} // switch
					}
					else
						Debug("ERROR - no client class - auto very sick");
				}
				else {
					// as in RXonly mode - Clear the buffer - no need to go round loop
					if (bShowAllDebugMessages) Debug( "ProcNextCmd - auto in RXONLY - no cmds sent");
					ClearCommandQueue();
					bContinue=FALSE;
					iRateRevs=0;
				}
			}
			delete pCommand; // delete record as now finished 
		}
		else {
			// nothing else in queue
			bContinue=FALSE;
			iRateRevs=0;
		}
	} while ((bContinue==TRUE)&&(iRateRevs>0));
	
	SetDlgItemInt( hWndDlg, IDC_QUEUE, qCommands.size(), FALSE );
	UpdateCounters();
	
	// if more commands still in queue and timer not running then start timer, else perhaps kill it
	if (qCommands.size()>0) {
		if (bNextCommandTimerRunning==FALSE) SetTimer( hWndMain, COMMAND_TIMER_ID, 70, NULL ); // timer for next command - if no rev received
		bNextCommandTimerRunning = TRUE;
	}
	else {
		// command que is empty
		if (bNextCommandTimerRunning==TRUE) KillTimer( hWndMain, COMMAND_TIMER_ID ); 
		bNextCommandTimerRunning = FALSE;
	}

}


////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void ClearOutgoingQueue( void )
{
	while (!qOutgoing.empty()) {
		CCommand* pRev;
		pRev = qOutgoing.front();
		qOutgoing.pop();
		delete pRev;
	}
	SetDlgItemInt( hWndDlg, IDC_OUTGOING, qOutgoing.size(), FALSE );
}


void AddResponseToOutgoingQue( LPCSTR szCmd, int iCmdType, int iDev, int iIndex, int iFromWS )
{
	CCommand* pRev = new CCommand;
	if (pRev) {
		wsprintf( pRev->szCommand, szCmd );
		pRev->iTypeCommand = iCmdType;
		pRev->iWhichDevice = iDev;
		pRev->iWhichSlotDest = iIndex;
		pRev->iWhichDatabase = iFromWS;
		qOutgoing.push( pRev );
		SetDlgItemInt( hWndDlg, IDC_OUTGOING, qOutgoing.size(), FALSE );
	}
	else 
		Debug( "AddResponse2Q - cmd record not created");
}


void ProcessAnyMoreResponses( int iRateRevs )
{
	// send out any more responses in the Outgoing queue - only used for router polls to better ease large polls of dests

	BOOL bContinue=TRUE;
	KillTimer( hWndMain, RESPONSE_TIMER_ID );
	bOutgoingTimerRunning = FALSE;
	do {
		if (qOutgoing.size()>0) {
			iRateRevs--;
			// something in queue so process it
			CCommand* pResponse = qOutgoing.front();
			qOutgoing.pop();
			if (pResponse) {
				// ONLY send cmd if main driver only or in starting mode for rx driver to register and poll initially
				if ((iOverallTXRXModeStatus==INFO_TXRXMODE)||(bAutomaticStarting==TRUE)) {
					if (edMainGRD) {
						switch(pResponse->iTypeCommand){
							case ROUTERCOMMAND:	
								edMainGRD->txrevmsg(pResponse->szCommand, pResponse->iWhichDatabase, FALSE);
								//lTXID++;
							break;
						default:
							Debug( "ProcAnyMoreResponses - unknown type from buffer - %d %s", pResponse->iTypeCommand, pResponse->szCommand );
						} // switch
					}
					else
						Debug("ERROR - no driver class - auto very sick");
				}
				else {
					// as in RXonly mode - Clear the buffer - no need to go round loop
					if (bShowAllDebugMessages) Debug( "ProcAnymoreResponses - auto in RXONLY - no cmds sent");
					ClearOutgoingQueue();
					bContinue=FALSE;
					iRateRevs=0;
				}
			}
			delete pResponse; // delete record as now finished 
		}
		else {
			// nothing else in queue
			bContinue=FALSE;
			iRateRevs=0;
		}
	} while ((bContinue==TRUE)&&(iRateRevs>0));
	
	SetDlgItemInt( hWndDlg, IDC_OUTGOING, qOutgoing.size(), FALSE );
	UpdateCounters();
	
	// if more commands still in queue and timer not running then restart timer
	if (qOutgoing.size()>0) {
		if (bOutgoingTimerRunning==FALSE) SetTimer( hWndMain, RESPONSE_TIMER_ID, 100, NULL ); // timer for next response
		bOutgoingTimerRunning = TRUE;
	}
}


////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

void ClearIncomingQueue( void )
{
	while (!qIncoming.empty()) {
		CCommand* pCmd;
		pCmd = qIncoming.front();
		qIncoming.pop();
		delete pCmd;
	}
	SetDlgItemInt( hWndDlg, IDC_INCOMING, qIncoming.size(), FALSE );
}


void AddIncomingToQue( LPCSTR szCmd, int iCmdType, int iRtrDevice, int iSrc, int iDest, int iMask, LPCSTR szMsk )
{
	CCommand* pCmd = new CCommand;
	if (pCmd) {
		wsprintf( pCmd->szCommand, szCmd );
		wsprintf( pCmd->szMask, szMsk );
		pCmd->iTypeCommand = iCmdType;
		pCmd->iWhichDevice = iRtrDevice;
		pCmd->iWhichSource = iSrc;
		pCmd->iWhichSlotDest = iDest;
		pCmd->iMaskValue = iMask;
		qIncoming.push( pCmd );
		SetDlgItemInt( hWndDlg, IDC_INCOMING, qIncoming.size(), FALSE );
	}
	else 
		Debug( "AddIncoming2Q - ERROR INCOMING record not created");
}


void ProcessNextIncomingCommand()
{
	if (qIncoming.size()>0) {
		//bAbletoProcessIncomingCommand = FALSE;
		// get command and process
		CCommand* pCommand = qIncoming.front();
		qIncoming.pop();
		if (pCommand) {
			ProcessVirtualRouterCrosspoint(pCommand->iWhichSource, pCommand->iWhichSlotDest, pCommand->iMaskValue);
		}
		delete pCommand; // delete record as now finished 
		SetDlgItemInt( hWndDlg, IDC_INCOMING, qIncoming.size(), FALSE );
		UpdateCounters();
	}
	//else 
		bAbletoProcessIncomingCommand = TRUE;
}


//////////////////////////////////////////////////////////////////////////
// 
//  AUTOMATIC STARTUP FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


BOOL GetRouterDeviceDBSizes( bncs_string ssName, int *iRtrDevice, int *iDB0Size, int *iDB1Size ) 
{
	char szFileName[64]="";
	int iNewDevice = 0;
	*iDB0Size=0; *iDB1Size=0;
	iNewDevice=getInstanceDevice( ssName );
	strcpy( szFileName, ssName );
	if (iNewDevice>0) {
		*iRtrDevice = iNewDevice;
		// get database sizes db0 and db1
		wsprintf(szFileName, "dev_%03d.ini", iNewDevice );
		*iDB0Size = atoi( r_p(szFileName, "Database", "DatabaseSize_0", "", false) );
		*iDB1Size = atoi( r_p(szFileName, "Database", "DatabaseSize_1", "", false) );
		return TRUE;
	}
	else 
		Debug( "GetRouterDeviceDBSizes - dev %s has ret 0", LPCSTR(ssName));
	return FALSE;
}


//
//  FUNCTION: ExtractSrcNamefromDevIniFile( )
//
//  PURPOSE: Extracts the src name from specified Database dev ini file and returns string 
//
char* ExtractSrcNamefromDevIniFile( int iDev, int iDbFile, int iEntry )
{
	char szFileName[256], szEntry[32], szDatabase[32];
	if ((iDev>0)&&(iEntry>0)) {	
		sprintf(szFileName, "dev_%03d.db%d", iDev, iDbFile);
		sprintf(szEntry, "%04d", iEntry );
		sprintf(szDatabase, "Database_%d", iDbFile );
		return r_p( szFileName, szDatabase, szEntry,"",FALSE);
	}
	return "";
}


char* GetParameterString( bncs_string sstr )
{
	strcpy( szResult, "");
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		wsprintf( szResult, "%s", LPCSTR(ssl1[1]) );
	return szResult;
	
}

int GetParameterInt( bncs_string sstr  )
{
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		return ssl1[1].toInt();
	else
		return UNKNOWNVAL;
}			

void GetTwinParameters( bncs_string sstr, int *i1, int *i2 )
{
	*i1=0;
	*i2=0;
	bncs_stringlist ssl1 = bncs_stringlist( sstr, ',' );
	if (ssl1.count()>0) *i1 = ssl1[0].toInt();
	if (ssl1.count()>1) *i2 = ssl1[1].toInt();
}


void LoadTableOfAllHighwaysAndWraps()
{
	int iEntry = 0;
	char szEntry[MAX_BUFFER_STRING] = "", szData1[64] = "", szData2[64] = "";
	// load table from config file
	// get table of defined rings for IFB
	bncs_string ssFileName = bncs_string("%1.%2").arg("tieline_manager_auto").arg("tieline_highways");
	bncs_config cfg(ssFileName);
	Debug("LoadHighways - getXMLTable - file : %s", LPCSTR(ssFileName));
	while (cfg.isChildValid())  {

		bncs_string strIdIndx = cfg.childAttr("id");
		bncs_string strValue = cfg.childAttr("value");
		
		if ((strValue.length()>0)&&(strIdIndx.length()>0)) {
			//
			int iConfigIndex = strIdIndx.right(4).toInt();
			bncs_stringlist ssl = bncs_stringlist(strValue, ',');
			bncs_stringlist sslfrom = bncs_stringlist(ssl.getNamedParam("from"), '|');
			bncs_stringlist sslto = bncs_stringlist(ssl.getNamedParam("to"), '|');
			bncs_string ssType = ssl.getNamedParam("type").upper();
			int iType = 0; // 
			if (ssType.find("TX") >= 0) iType = HIGHWAY_TX_TYPE;
			else  if (ssType.find("LIVE") >= 0) iType = HIGHWAY_LIVE_TYPE;
			else  if (ssType.find("ENG") >= 0) iType = HIGHWAY_ENG_TYPE;
			else  if (ssType.find("SUPVR") >= 0) iType = HIGHWAY_SUPVR_TYPE;
			else  if (ssType.find("CMVP") >= 0) iType = HIGHWAY_CMVP_TYPE;
			else  if (ssType.find("LMVP") >= 0) iType = HIGHWAY_LMVP_TYPE;
			else  if (ssType.find("WRAP") >= 0) iType = HIGHWAY_WRAP_TYPE;
			else  if (ssType.find("POOL") >= 0) iType = HIGHWAY_POOL_TYPE;

			//Debug( "LoadAllinit- highway/wrap %s %s %d", LPCSTR(sslfrom.toString()),LPCSTR(sslto.toString()), iType );

			if ((sslfrom.count()>1) && (sslto.count()>1)) {

				int iFromRtr = 0, iToRtr = 0, iFromDest = 0, iToSrc = 0;
				// validate data 
				iFromRtr = getInstanceDevice(sslfrom[0]);
				iFromDest = sslfrom[1].toInt();
				iToRtr = getInstanceDevice(sslto[0]);
				iToSrc = sslto[1].toInt();
				BOOL bIncHigh = TRUE;
				if (strValue.find("inactive") >= 0) 	{
					Debug("LoadHighways IGNORED - index %d from %s %s ", iConfigIndex, LPCSTR(strIdIndx), LPCSTR(strValue));
					bIncHigh = FALSE;
				}

				if ((iFromRtr>0) && (iToRtr) && (iFromDest>0) && (iToSrc>0) && (bIncHigh==TRUE)) {
					// valid data so add to map etc
					iEntry++;
					Debug("LoadHighways entry %d index %d from %s %s ", iEntry, iConfigIndex, LPCSTR(strIdIndx), LPCSTR(strValue));

					// just check that given type is correct -- if two routers are the same - then force to wrap type
					if ((iFromRtr == iToRtr) && (iType != HIGHWAY_WRAP_TYPE)) {
						Debug("CONFIG ERROR - override highway to WRAP type for entry %d - data", iEntry);
						iType = HIGHWAY_WRAP_TYPE;
					}
					iTotalHighwaysAndWraps++;
					CRouterHighway* pHigh = NULL;
					pHigh = new CRouterHighway(iTotalHighwaysAndWraps, iConfigIndex, iFromRtr, iFromDest, iToRtr, iToSrc, iType); 
					if (iType == HIGHWAY_WRAP_TYPE) 
						iNumberLoadedWraps++;
					else 
						iNumberLoadedHighways++;

					if (pHigh) {
						// make links into revs rtr classes
						// from
						CRevsRouterData* pFromRtr = GetValidRouter(iFromRtr);
						if (pFromRtr) {
							pFromRtr->setHighwayAssociatedtoDest(iFromDest, iTotalHighwaysAndWraps);
							if (pRouterVirtual) pRouterVirtual->setHighwayAssociatedtoDest(iFromDest + pFromRtr->getRouterOffsetIntoVirtual(), iTotalHighwaysAndWraps);
							wsprintf(szData1, "%s.%d", LPCSTR(pFromRtr->getRouterLabel()), iFromDest);
						}
						//to
						CRevsRouterData* pToRtr = GetValidRouter(iToRtr);
						if (pToRtr) {
							pToRtr->setHighwayAssociatedtoSource(iToSrc, iTotalHighwaysAndWraps);
							if (pRouterVirtual) pRouterVirtual->setHighwayAssociatedtoSource(iToSrc + pToRtr->getRouterOffsetIntoVirtual(), iTotalHighwaysAndWraps);
							wsprintf(szData2, "%s.%d", LPCSTR(pToRtr->getRouterLabel()), iToSrc);
						}

						//
						// add to appropriate map
						if (mapOfAllHighways) mapOfAllHighways->insert(pair<int, CRouterHighway*>(iTotalHighwaysAndWraps, pHigh));
						// add to gui
						wsprintf(szEntry, "%04d %s -> %s  %s", iConfigIndex, szData1, szData2, LPCSTR(ssType.lower()));
						SendDlgItemMessage(hWndDlg, IDC_LISTHIGHWAYS, LB_INSERTSTRING, -1, (LPARAM)szEntry);

					}
					else
						Debug("loadAllInit - ERROR - failed to create highway/wrap class from %s - %s", LPCSTR(strIdIndx), LPCSTR(strValue));
				}
				else
					Debug("loadAllInit - invalid/inactive highway/wrap data - entry ignored from %s - %s", LPCSTR(strIdIndx), LPCSTR(strValue));
			}
			else
				Debug("loadAllInit - invalid highway/wrap data - entry ignored from %s - %s", LPCSTR(strIdIndx), LPCSTR(strValue));
		}

		cfg.nextChild();				 // go on to the next sibling (if there is one)
	}
	Debug("LoadTableOfAllHighwaysAndWraps - %d items from tieline higways table", iEntry);
}


BOOL LoadAllInitialisationData()
{
	// load router data
	// get the emerald and orange routers and the offsets
	char szEntry[256], szData1[64] = "", szData2[64] = "";
	bncs_string strStart = "tieline_manager_auto", strValue = "", strIdName = "", ss_Rtr_Orange = "", ss_Rtr_Emerald = "";
	int  iOrRtr1 = 0, iEmRtr2 = 0, iMaxSrcs = 0, iMaxDests = 0;

	i_zero_virtuals = 0;
	BOOL bContinue = TRUE;
	pRouterVirtual = NULL;
	ssl_CalcListOfDestinationsUsers = bncs_stringlist("", '|');
	ssl_CalcListOfChangedPtiSources = bncs_stringlist("", '|');

	mapOfAllRouters = new map<int, CRevsRouterData* >;
	if (mapOfAllRouters == NULL) {
		Debug("LoadInit -- ERROR - map of all routers failed to create ");
		return FALSE;
	}

	iUserDefinedMngmntProc = getXMLItemValue(strStart, ss_AutoId, "highway_management").toInt();
	Debug("LoadAllInit - Highway Management Interval read in as %d seconds", iUserDefinedMngmntProc);
	if ((iUserDefinedMngmntProc <= 5) || (iUserDefinedMngmntProc > 60)) {
		Debug("LoadAllInit - Highway Management definition was %d seconds - xml deemed invalid, so setting to 11", iUserDefinedMngmntProc);
		iUserDefinedMngmntProc = 11;
	}
	SetDlgItemInt(hWndDlg, IDC_CONF_MANAGEMENT, iUserDefinedMngmntProc, TRUE);

	ssInfodriverRoutePrefix = "";
	ssInfodriverRoutePrefix = getXMLItemValue(strStart, ss_AutoId, "infodriver_route_command", "value");
	Debug("LoadAllInitData - infodriver route prefix (%s)", LPCSTR(ssInfodriverRoutePrefix));

	// get VideoIpath pending delay
	iConfigurationPending = 3;  // current default prime number interval NOT seconds, it is number of Management Processing cycles
	//iConfigurationPending = atoi(getXMLItemValue(strStart, ss_AutoId, "highway_pending_setting", "value"));
	//if (iConfigurationPending<1) {
	//	iConfigurationPending = 7;   // deemed resonable interval -- NOT seconds, it is number of Management Processing cycles
	//	Debug("LoadAllInit - Config Pending Delay Setting reset to %d management processing cycles", iConfigurationPending);
	//}
	//else
	//	Debug("LoadAllInit - Pending Delay Setting set at %d highway management processing cycles", iConfigurationPending);
	SetDlgItemInt(hWndDlg, IDC_CONF_PENDING, iConfigurationPending, TRUE);

	Debug("LoadAllInitData  - loading router data");
	// routers - load from object settings
	// NOTE --- REMEMBER IN THIS AUTO --- Virtual Router is the BNCS "combined" presentation of all real routers to a max of 4000 src/dest

	bncs_string ssVirRtr = getXMLItemValue(strStart, ss_AutoId, "auto_grd_interface", "value");
	int iXMLgrd = getInstanceDevice(ssVirRtr);
	if (iXMLgrd != iGRDDevice) Debug("LoadAllInit - WARNING - grd parameter %d does not match xml instance %d ", iGRDDevice, iXMLgrd);
	//
	GetRouterDeviceDBSizes(ssVirRtr, &i_vir_rtr, &i_max_vir_rtr_Srcs, &i_max_vir_rtr_Dests);
	Debug("LoadAllInitData - virtual rtr %s is %d srcs %d dests %d ", LPCSTR(ssVirRtr), i_vir_rtr, i_max_vir_rtr_Srcs, i_max_vir_rtr_Dests);
	if (i_max_vir_rtr_Dests > 4000) i_max_vir_rtr_Dests = 4000;
	if (i_max_vir_rtr_Srcs > 4000) i_max_vir_rtr_Srcs = 4000;
	Debug("LoadAllInitData - virtual rtr %s is %d srcs %d dests %d ", LPCSTR(ssVirRtr), i_vir_rtr, i_max_vir_rtr_Srcs, i_max_vir_rtr_Dests);
	if (i_vir_rtr > 0) {
		pRouterVirtual = new CRevsRouterData(i_vir_rtr, i_max_vir_rtr_Srcs, i_max_vir_rtr_Dests, VIR_GRD_INFO_TYPE, 0, 0, 0, 0);
		SetDlgItemInt(hWndDlg, IDC_GRDDRV_INDEX, i_vir_rtr, TRUE);
		if (pRouterVirtual == NULL) {
			Debug("LoadAllInit - Rtr Virtual class failure - auto suspended");
			return FALSE;
		}
	}
	else {
		Debug("LoadAllInit - Virtual rtr has value of 0 - auto suspended");
		return FALSE;
	}

	// get  CMR and TX chain routers - ie those defined in xml file 
	bncs_string ssOff = "";
	int iiRtr = 0;
	for (int iiRtr = 1; iiRtr < 16; iiRtr++) {
		// load routers table - instance, offset, test and park srcs
		bncs_string ssArg = bncs_string("router_%1").arg(iiRtr);
		bncs_stringlist ssl_Data = bncs_stringlist(getXMLItemValue(strStart, "tieline_routers", ssArg, "value"), ',');
		bncs_string ssInst = ssl_Data.getNamedParam("instance");
		if (ssInst.length()>2) {
			int iOffset = ssl_Data.getNamedParam("virtual_offset").toInt();
			int iTXDivision = ssl_Data.getNamedParam("tx_division").toInt();
			int iSrctest = ssl_Data.getNamedParam("test_src").toInt();
			int iSrcpark = ssl_Data.getNamedParam("park_src").toInt();
			int iRtrIndx = 0, iMaxSrcs = 0, iMaxDests = 0;
			GetRouterDeviceDBSizes(ssInst, &iRtrIndx, &iMaxSrcs, &iMaxDests);
			if (iRtrIndx>0) {
				CRevsRouterData* pNewRtr = new CRevsRouterData(iRtrIndx, iMaxSrcs, iMaxDests, GRD_REVERTIVE_TYPE, iOffset, iSrcpark, iSrctest, iTXDivision);
				if (pNewRtr) {
					ssl_AllRoutersIndices.append(bncs_string(iRtrIndx));
					// add into routers map
					if (mapOfAllRouters) mapOfAllRouters->insert(pair<int, CRevsRouterData*>(iRtrIndx, pNewRtr));
					Debug("LoadAllInit -  loading %d tieline router %d srcs %d dests %d VIR offset %d ", iiRtr, iRtrIndx, iMaxSrcs, iMaxDests, iOffset);
					ssOff.append(bncs_string(" %1").arg(iOffset));
				}
				else {
					Debug("LoadAllInit - **** ERROR - loading %d tieline ROUTER %d FALIED ", iiRtr, iRtrIndx);
				}
			}
		}
	}

	// load up gui list boxes
	iLoadSrcListBoxEntry = 1;
	iLoadDestListBoxEntry = 1;
	SetTimer(hWndMain, LISTBOX_TIMER_ID, 1000, NULL);
	LoadSourcesListbox();
	LoadDestinationsListbox();

	SetDlgItemText(hWndDlg, IDC_CONF_RTRS, LPCSTR(ssl_AllRoutersIndices.toString(',')));
	SetDlgItemText(hWndDlg, IDC_CONF_OFFSETS, LPCSTR(ssOff));
	

	// load highways - by the way 0 highways is acceptable... ( ie this virtual grd then operates as a normal grd )
	iNumberLoadedWraps=0;
	iNumberLoadedHighways=0;
	iTotalHighwaysAndWraps=0;
	i_unknown_highways=0;
	i_disabled_highways = 0;
	
	mapOfAllHighways = new map<int,CRouterHighway*>;
	if (mapOfAllHighways==NULL) {
		PostAutomaticErrorMessage(0, ALARM_ERROR_STATE, bncs_string("map errors  - TIELINE MANAGER suspended"));
		Debug( "LoadAllInit - create MAP of Highways FAILED - auto stopped");
		return FALSE;
	}
	
	// loading highways -- from table rather than by index
	Debug( "LoadAllInitData - loading highway data " );
	LoadTableOfAllHighwaysAndWraps();
	Debug( "loadAllInit - total entries %d,  Highways (%d) / Wraps (%d) ", iTotalHighwaysAndWraps, iNumberLoadedHighways, iNumberLoadedWraps ); 	
	wsprintf( szEntry, "0 / %d", iNumberLoadedHighways );

	SetDlgItemText( hWndDlg, IDC_CONF_HIGHWAYS2, szEntry );
	SetDlgItemInt( hWndDlg, IDC_CONF_HIGHWAYS, iNumberLoadedHighways, TRUE );
	SetDlgItemInt( hWndDlg, IDC_CONF_WRAPS, iNumberLoadedWraps, TRUE );

	return TRUE;
}


void AutoRegistrations( void )
{
	// register for all defined routers
	for (int iiRtr = 0; iiRtr < ssl_AllRoutersIndices.count(); iiRtr++) {
		CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iiRtr]);
		if (pRtr && ecCSIClient) {
			ecCSIClient->regtallyrange(pRtr->getRouterIndex(), 1, pRtr->getMaxDestinations(), INSERT);
			Debug("AutoReg - router %d 1-%d ", pRtr->getRouterIndex(), pRtr->getMaxDestinations());
		}
	}
}


void PollPhysicalRouters( int iCounter )
{
	char szCommand[MAX_BUFFER_STRING]="";

	if ((iCounter>=0)&&(iCounter<ssl_AllRoutersIndices.count())) {
		// use counter as index into rtr list for polling
		CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iCounter]);
		if (pRtr && ecCSIClient) {
			if (pRtr->getRouterRevertiveType() == INFO_REVERTIVE_TYPE) {
				wsprintf(szCommand, "IP %d 1 %d", pRtr->getRouterIndex(), pRtr->getMaxDestinations());
				ecCSIClient->txinfocmd( szCommand );
				Debug("AutoPoll - INFO %d 1-%d ", pRtr->getRouterIndex(), pRtr->getMaxDestinations());
			}
			else {
				wsprintf(szCommand, "RP %d 1 %d", pRtr->getRouterIndex(), pRtr->getMaxDestinations());
				ecCSIClient->txrtrcmd( szCommand );
				Debug("AutoPoll - GRD %d 1-%d ", pRtr->getRouterIndex(), pRtr->getMaxDestinations());
			}
		}
	}
}


void CheckRevertivesReceivedOK( BOOL bRepollRouter )
{   
	if (qCommands.size()==0) {
		// only go thru this re poll if auto is quiet - and not in midst of repolling at this time
		char szResult[MAX_BUFFER_STRING]="";
		int iDest=0;
		int i_zero_CMRrevs = 0, i_zeroChains = 0;
		// go thru all physical revs and check all received ( stored route is not -1 or 0 ) - any not found - then re-poll
		// go thru orange
		for (int iiRtr = 0; iiRtr < ssl_AllRoutersIndices.count(); iiRtr++) {
			CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iiRtr]);
			if (pRtr) {
				for (iDest = 1; iDest <= pRtr->getMaxDestinations(); iDest++) {
					if (pRtr->getRoutedSourceForDestination(iDest) <= 0) {
						if (iiRtr == 0) i_zero_CMRrevs++; else i_zeroChains++;
						if (bRepollRouter == TRUE) {
							char szCommand[MAX_BUFFER_STRING] = "";
							if (pRtr->getRouterRevertiveType() == INFO_REVERTIVE_TYPE) {
								wsprintf(szCommand, "IP %d %d %d", pRtr->getRouterIndex(), iDest, iDest);
								AddCommandToQue(szCommand, INFODRVCOMMAND, pRtr->getRouterIndex(), 0, 0);
							}
							else {
								wsprintf(szCommand, "RP %d %d %d", pRtr->getRouterIndex(), iDest, iDest);
								AddCommandToQue(szCommand, ROUTERCOMMAND, pRtr->getRouterIndex(), 0, 0);
							}
						}
					}
				} // for idest
			}
		} // for irtr
		// send out repolls for all commands added 
		if (qCommands.size()>0) {
			Debug( "CheckRevfsOK - repolling for %d routes", qCommands.size());
			ProcessNextCommand(qCommands.size());
		}
		//
		if (eiInfoGrd) {
			wsprintf( szResult, "vir:%d|cmr:%d|oth:%d", i_zero_virtuals, i_zero_CMRrevs, i_zeroChains );
			eiInfoGrd->updateslot( SLOT_ROUTER_ZEROS, szResult );
			//
			char szData[MAX_BUFFER_STRING]="";
			if (i_zero_CMRrevs == 0)
				SetDlgItemText(hWndDlg, IDC_ZERO_ROUTERS, "all OK");
			else {
				wsprintf(szData, "%d zeros", i_zero_CMRrevs);
				SetDlgItemText(hWndDlg, IDC_ZERO_ROUTERS, szData);
			}
			if (i_zeroChains == 0)
				SetDlgItemText(hWndDlg, IDC_ZERO_EMERALD, "all OK");
			else {
				wsprintf(szData, "%d zeros", i_zeroChains);
				SetDlgItemText(hWndDlg, IDC_ZERO_EMERALD, szData);
			}
		}
	}
}


void CheckAndRecalculateVirtualDests( void )
{
	// check calculated virtual dests
	char szResult[MAX_BUFFER_STRING]="";
	int iDest=0;
	int iUnknowns=0;
	i_zero_virtuals=0;
	int i_zero_revs=0, i_zero_chains = 0;
	if (mapOfAllRouters) {
		for (int iiRtr = 0; iiRtr < ssl_AllRoutersIndices.count(); iiRtr++) {
			CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iiRtr]);
			if (pRtr) {
				for (iDest = 1; iDest <= pRtr->getMaxDestinations(); iDest++) {
					if (pRtr->getRoutedSourceForDestination(iDest) <=0) {
						if (iiRtr == 0) i_zero_revs++; else i_zero_chains++;
					}
					int iVirDest = iDest + pRtr->getRouterOffsetIntoVirtual();
					if (pRouterVirtual->getRoutedSourceForDestination(iVirDest)<0)	{
						i_zero_virtuals++;
					}
				}
			}
		}
	}
	if (eiInfoGrd) {
		wsprintf( szResult, "vir:%d|cmr:%d|oth:%d",  i_zero_virtuals, i_zero_revs, i_zero_chains );
		eiInfoGrd->updateslot( SLOT_ROUTER_ZEROS, szResult );
		//
		char szData[MAX_BUFFER_STRING]="";
		if (i_zero_virtuals==0) 
			SetDlgItemText(hWndDlg, IDC_ZERO_VIRTUAL, "all OK"); 
		else {
			wsprintf( szData, "%d zeros", i_zero_virtuals);
			SetDlgItemText(hWndDlg, IDC_ZERO_VIRTUAL, szData); 
		}
		if (i_zero_revs == 0)
			SetDlgItemText(hWndDlg, IDC_ZERO_ROUTERS, "all OK");
		else {
			wsprintf(szData, "%d zeros", i_zero_revs);
			SetDlgItemText(hWndDlg, IDC_ZERO_ROUTERS, szData);
		}
		if (i_zero_chains == 0)
			SetDlgItemText(hWndDlg, IDC_ZERO_EMERALD, "all OK");
		else {
			wsprintf(szData, "%d zeros", i_zero_chains);
			SetDlgItemText(hWndDlg, IDC_ZERO_EMERALD, szData);
		}
	}
	// further check on unknowns for sources into highway routes 
	if (i_zero_virtuals>0) {
		Debug("CheckReCalcVirtualDests -- %d UNKNOWN HIGHWAYS ", i_unknown_highways );
		Debug("CheckReCalcVirtualDests -- %d UNKNOWN routes -- re polling routers", i_zero_virtuals);
		// set the next check to be very soon -- for continued missing revs 
		//iNextDestinationCheck = 25;
		CheckRevertivesReceivedOK( TRUE );
	}
}


int IsRouterInfodriverRevertiveBased( int iGivenRtr )
{
	if (pRouterVirtual->getRouterIndex() == iGivenRtr) {
		return pRouterVirtual->getRouterRevertiveType();
	}
	else {
		CRevsRouterData* pRtr = GetValidRouter(iGivenRtr);
		if (pRtr) return pRtr->getRouterRevertiveType();
	}
	return UNKNOWNVAL;
}


int StoreThePendingSource( int iGivenRtr, int iPendingSource, int iDestination )
{
	int iCurrentSrce = 0;
	//
	if (pRouterVirtual->getRouterIndex() == iGivenRtr) {
		iCurrentSrce = pRouterVirtual->getRoutedSourceForDestination(iDestination);
		pRouterVirtual->storePendingSource(iDestination, iPendingSource);
	}
	else {
		CRevsRouterData* pRtr = GetValidRouter(iGivenRtr);
		if (pRtr) {
			iCurrentSrce = pRtr->getRoutedSourceForDestination(iDestination);
			pRtr->storePendingSource(iDestination, iPendingSource);
		}
	}
	return iCurrentSrce;
}


void SendRouterCrosspointImmediately( int iRtrDevice, int iGivenSource, int iFinalDestination, int iSDIMask )
{
	char szCommand[MAX_BUFFER_STRING]="";
	int iCurrSrce = StoreThePendingSource( iRtrDevice, iGivenSource, iFinalDestination );
	if (iOverallTXRXModeStatus == INFO_TXRXMODE) {
		if (IsRouterInfodriverRevertiveBased(iRtrDevice) == INFO_REVERTIVE_TYPE) {
			if (ssInfodriverRoutePrefix.length() > 0)
				wsprintf(szCommand, "IW %d '%s=%d' %d", iRtrDevice, LPCSTR(ssInfodriverRoutePrefix), iGivenSource, iFinalDestination);
			else
				wsprintf(szCommand, "IW %d '%d' %d", iRtrDevice, iGivenSource, iFinalDestination);
			if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
		}
		else {
			wsprintf(szCommand, "RC %d %d %d '%d'", iRtrDevice, iGivenSource, iFinalDestination, iSDIMask);
			if (ecCSIClient) ecCSIClient->txrtrcmd(szCommand);
		}
	}
	if (iCurrSrce == iGivenSource) bAbletoProcessIncomingCommand = TRUE; // carry on processing queued commands as routed srce will not be changing
}


void SendRouterCrosspointToCommandQueue( int iRtrDevice, int iGivenSource, int iFinalDestination, int iReqType )
{
	char szCommand[MAX_BUFFER_STRING]="";
	int iCurrSrce = StoreThePendingSource( iRtrDevice, iGivenSource, iFinalDestination );
	if (iOverallTXRXModeStatus == INFO_TXRXMODE) {
		if (IsRouterInfodriverRevertiveBased(iRtrDevice) == INFO_REVERTIVE_TYPE) {
			if (ssInfodriverRoutePrefix.length() > 0)
				wsprintf(szCommand, "IW %d '%s=%d' %d", iRtrDevice, LPCSTR(ssInfodriverRoutePrefix), iGivenSource, iFinalDestination);
			else
				wsprintf(szCommand, "IW %d '%d' %d", iRtrDevice, iGivenSource, iFinalDestination);
			AddCommandToQue(szCommand, INFODRVCOMMAND, iRtrDevice, iFinalDestination, 0);
		}
		else {
			wsprintf(szCommand, "RC %d %d %d '%d'", iRtrDevice, iGivenSource, iFinalDestination, iReqType);
			AddCommandToQue(szCommand, ROUTERCOMMAND, iRtrDevice, iFinalDestination, 0);
		}
	}
	if (iCurrSrce == iGivenSource) bAbletoProcessIncomingCommand = TRUE; // carry on processing queued commands as routed srce will not be changing
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Highway management
//
void ProcessHighwayManagement( void )
{
	// pass thru all highways -- see what is free, what is inuse - get stuff from ext infodriver etc etc
	// check for hung highways
	// check highways with test - and no users - disabled, ;; test but with users - in use
	// a highway with a src other than park or test with no users - hung - so route park ?? and free up
	//if (bShowAllDebugMessages) Debug( "entering process highway management" );

	for (int iH = 1; iH <= iTotalHighwaysAndWraps; iH++) {
		CRouterHighway* pHigh = GetRouterHighway(iH);
		if (pHigh) {
			//
			int iActualSource = UNKNOWNVAL;
			//int iAllocatedSource=pHigh->getAllocatedHighwaySource(); 
			int iSrcUsers = 0, iparksrc = UNKNOWNVAL, itestsrc = UNKNOWNVAL;
			//
			CRevsRouterData* pFromRtr = GetValidRouter(pHigh->getFromRouter());
			CRevsRouterData* pToRtr = GetValidRouter(pHigh->getToRouter());
			if (pFromRtr&&pToRtr) {
				//
				iActualSource = pFromRtr->getRoutedSourceForDestination(pHigh->getFromDestination());
				iSrcUsers = pToRtr->getNumberOfDestsUsingSource(pHigh->getToSource());
				iparksrc = pFromRtr->getParkSource();
				itestsrc = pFromRtr->getTestSource();
				//  decriment Pending state
				if (pHigh->getHighwayPendingCount() > 0) {
					Debug("ProcessHighwayMgmt -1-  high %d  pending count %d", pHigh->getHighwayIndex(), pHigh->getHighwayPendingCount());
					pHigh->decrimentHighwayPendingCount();
					// check for users of highway - if none then shortcircuit pending, if park routed into highway then mark as free ???
					// XXX if park is 0 -- 
					if ((iActualSource == iparksrc) && (iSrcUsers == 0) && (pHigh->getHighwayPendingCount() > 0)) {
						Debug("ProcessHighwayMgmt -1- short-circuit FREE from PendingCount - parked and no users - high %d  from grd %d %d  to grd %d %d", pHigh->getHighwayIndex(),
							pHigh->getFromRouter(), pHigh->getFromDestination(), pHigh->getToRouter(), pHigh->getToSource());
						pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
					}
				}
				// dont really have a concept of free as all highways are of specific type use
				// 1. if highway maked as PENDING and pending counter now 0 - has been pending for many many secs - implies no revertive from grd  / infodriver
				if (((pHigh->getHighwayState() == HIGHWAY_CLEARING) || (pHigh->getHighwayState() == HIGHWAY_PENDING)) && (pHigh->getHighwayPendingCount() == 0)) {
					Debug("ProcessHighwayMgmt -1- setting FREE from PENDING/clearing - high %d  from grd %d %d  to grd %d %d", pHigh->getHighwayIndex(),
						pHigh->getFromRouter(), pHigh->getFromDestination(), pHigh->getToRouter(), pHigh->getToSource());
					// route park to this highway
					RouteParkToHighway(pHigh, FALSE);
					pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
				}
				//
				// check for TEST
				if (iActualSource == itestsrc) {

					if (iSrcUsers > 0) {
						// stronger -- for cases of disabled highways -- if disabled then remain so
						if ((pHigh->getHighwayState()<HIGHWAY_INUSE)) {
							Debug("ProcHighMgmt- highway %d is test with %d USERS -> to INUSE(%d)", pHigh->getHighwayIndex(), iSrcUsers, pHigh->getHighwayState());
							pHigh->setHighwayState(HIGHWAY_INUSE, iActualSource, 0);
						}
					}
					else if (iSrcUsers == 0) {
						if ((pHigh->getHighwayState()>HIGHWAY_FREE) && (pHigh->getHighwayPendingCount() == 0)) {
							Debug("ProcHighMgmt- highway %d WITH TEST and is HUNG/CLEARING so -> to FREE(%d) and parking", pHigh->getHighwayIndex(), pHigh->getHighwayState());
							pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
							// route park to this highway
							SendRouterCrosspointImmediately(pHigh->getFromRouter(), iparksrc, pHigh->getFromDestination(), 0);
							//iActualSource = iparksrc;
						}
					}
				}
				// PARK  src
				else if (iActualSource == iparksrc) {
					if (iSrcUsers>0) {
						if (pHigh->getHighwayState() < HIGHWAY_INUSE) {
							if (bShowAllDebugMessages) Debug("ProcHighMgmt- highway %d is parked with %d users -> to INUSE(%d)", pHigh->getHighwayIndex(), iSrcUsers, pHigh->getHighwayState());
							pHigh->setHighwayState(HIGHWAY_INUSE, iActualSource, 0);
						}
						//if (bAutoParkHighways == TRUE) {
						//	//  - if highway has park routed into it and there are far users - then set count to route local park to far highway users - in order to clear it down 
						//	pHigh->decrimentHighwayParkitCount();
						//	if (pHigh->getHighwayParkitCount()<-2) {
						//		// park the highway as still has park after some time 
						//		Debug("ProcHighMgmt- highway %d is parked with %d users for some time - so AUTO PARKING NOW", pHigh->getHighwayIndex(), iSrcUsers);
						//		pHigh->resetParkitCount();
						//		RouteParkToHighway(pHigh, FALSE);
						//	}
						//}
					}
					else {
						// is highway parked with NO USERS and not set as free ?
						if ((pHigh->getHighwayState()>HIGHWAY_FREE) && (pHigh->getHighwayPendingCount() == 0)) {
							if (bShowAllDebugMessages) Debug("ProcHighMgmt- highway %d is PARKED/zero and no users and not disabled -> to FREE(%d)", pHigh->getHighwayIndex(), pHigh->getHighwayState());
							pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
						}
					}
				}
				// HUNG /CLEARING  Src ?? - is has a src and no users
				else if ((iActualSource>0) && (iSrcUsers == 0) && (iActualSource != iparksrc) && (pHigh->getHighwayPendingCount() == 0)) {
					// is highway inuse ?XXXX
					if (pHigh->getHighwayState()>HIGHWAY_FREE)  {
						Debug("ProcHighMgmt- highway %d is HUNG/CLEAR/PEND so -> to FREE(%d) and parking", pHigh->getHighwayIndex(), pHigh->getHighwayState());
						pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
						// route park to this highway
						SendRouterCrosspointImmediately(pHigh->getFromRouter(), iparksrc, pHigh->getFromDestination(), 0);
						//iActualSource = iparksrc;
					}
				}
				else if ((iActualSource>0) && (iSrcUsers > 0) && (iActualSource != iparksrc)) {
					// is highway inuse ?
					if (pHigh->getHighwayState()<HIGHWAY_INUSE) {
						// ie highway in use - but if disabled then it will remain so
						if (bShowAllDebugMessages) Debug("ProcHighMgmt- highway %d is INUSE ", pHigh->getHighwayIndex());
						pHigh->setHighwayState(HIGHWAY_INUSE, iActualSource, 0);
					}
				}
				else if ((iActualSource == 0) && (pHigh->getHighwayState()>HIGHWAY_FREE) && (pHigh->getHighwayPendingCount() == 0)) {
					Debug("ProcHighMgmt- highway %d has ZERO source with %d users -> to FREE State(%d)", pHigh->getHighwayIndex(), iSrcUsers, pHigh->getHighwayState());
					pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
					// route park to this highway
					RouteParkToHighway(pHigh, FALSE);
					pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
				}
				// unknown source  
				else if ((iActualSource < 0) && (pHigh->getHighwayPendingCount() == 0)) {
					Debug("ProcHighMgmt- highway %d is UNKNOWN with %d users -> to UNKNOWN(%d)", pHigh->getHighwayIndex(), iSrcUsers, pHigh->getHighwayState());
					pHigh->setHighwayState(HIGHWAY_UNKNOWN, 0, 0);
				}

			} // if pfromrtr
		} // if phigh
		// update gui 
		if (iChosenHighway == pHigh->getHighwayIndex()) DisplayHighwayData(iChosenHighway);
	} // for
	//
	CalculateHighwayUsed();
	//
	//if (bShowAllDebugMessages) Debug( "leaving process highway management" );
}



void ProcessVirtualRoutesOnStartup( void )
{
	int iRtrDest=0, iVirtualDest=0;
	// used at startup to go thru all routes a second time once all revs received
	if (pRouterVirtual&&mapOfAllRouters) {
		ssl_CalcListOfChangedPtiSources.clear();
		for (int iiRtr = 0; iiRtr < ssl_AllRoutersIndices.count(); iiRtr++) {
			CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iiRtr]);
			if (pRtr) {
				for (iRtrDest = 1; iRtrDest <= pRtr->getMaxDestinations(); iRtrDest++) {
					iVirtualDest = iRtrDest + pRtr->getRouterOffsetIntoVirtual();
					StoreVirtualRouterRevertive(pRtr->getRouterIndex(), iRtrDest, iVirtualDest);
				}
			}
		} // for 
	}
}



void DumpAllTallies( void )
{
	// used at start up after first calc of routes for highway manager
	if (pRouterVirtual&&mapOfAllRouters) {
		// dump current tallies for orange router
		for (int iiRtr = 0; iiRtr < ssl_AllRoutersIndices.count(); iiRtr++) {
			CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iiRtr]);
			if (pRtr) {
			Debug("DumpTallies - for rtr %d", pRtr->getRouterIndex());
				int iMax = pRtr->getMaxDestinations();
				int iOffset = pRtr->getRouterOffsetIntoVirtual();
				ProcessVirtualRouterPoll( iOffset+1, iOffset+iMax, 0 );
			}
		}
		Debug("DumpTallies - ProcessPTI");
		ssl_CalcListOfChangedPtiSources.clear();
		for (int itrace = 1; itrace <= pRouterVirtual->getMaxSources(); itrace++) ssl_CalcListOfChangedPtiSources.append(bncs_string(itrace));
		ProcessPtiUpdate();
	}
}




//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
//   HIGHWAY FUNCTIONS 
//
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
//
CRouterHighway* FindSourceAlreadyPoolRouted( int iFromRtr, int iReqSrc, int iIgnoreHighway )
{
	CRouterHighway* pHigh=NULL;
	// method 1 - is source already "allocated" to a pending / in use highway 
	int iHighwayTypeRequired = HIGHWAY_POOL_TYPE;
	pHigh = GetHighwayWithAllocatedSource( iFromRtr, iReqSrc, iIgnoreHighway, iHighwayTypeRequired );
	if (pHigh) {
		return pHigh;
	}

	// method 2 - based on users of router source
	bncs_stringlist ssl_Users = bncs_stringlist("", '|');
	CRevsRouterData* pFromRtr = GetValidRouter(iFromRtr);
	if (pFromRtr) ssl_Users = bncs_stringlist(pFromRtr->getListOfDestsUsingSource(iReqSrc), '|');
	//
	int iUsers=ssl_Users.count();
	//
	for (int iU=0;iU<iUsers;iU++) {
		int iDest = ssl_Users[iU].toInt();
		pHigh = GetRouterHighwayFromDest( iFromRtr, iDest );
		if (pHigh) {
			if ((pHigh->getHighwayType() == HIGHWAY_POOL_TYPE) &&
				(pHigh->getHighwayState() != HIGHWAY_UNKNOWN) &&
				(pHigh->getHighwayIndex() != iIgnoreHighway)) {
				// general pool highway found (ie mon highways excluded ) - so use this one ?? -- check other conditions that may preclude its use ??
				return pHigh;  // means mon dest will use program highway if src already routed 
			}
		}
	}
	//
	return NULL;
}


CRouterHighway* FindDestinationIsSoloOnPoolHighway( int iFinalDest, int iToRtr, int iIgnoreHighway )
{
	// is current source for final dest the landing point for highway
	int iCurrSrc=0;
	CRouterHighway* pHigh = NULL;
	CRevsRouterData* pToRtr = GetValidRouter(iToRtr);
	if (pToRtr) {
		iCurrSrc = pToRtr->getRoutedSourceForDestination(iFinalDest);
		pHigh = GetRouterHighwayToSource(iToRtr, iCurrSrc);

		if (pHigh) {
			if ((pHigh->getHighwayType()==HIGHWAY_POOL_TYPE) &&
				(pHigh->getHighwayState()!=HIGHWAY_UNKNOWN)&&
				(pHigh->getHighwayIndex()!=iIgnoreHighway)) {
				if (pToRtr->getNumberOfDestsUsingSource(iCurrSrc) == 1) {
					return pHigh;			
				}
			}
		}
	}
	// 
	return NULL;
}



//////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: MoveHighwayToNewOne()
//
BOOL MoveGivenHighwayToNewOne( CRouterHighway* pHigh, int iExemptDestination )
{
	char szCommand[MAX_BUFFER_STRING]="";
	
	// get current source, get new highway, route source, then route all current dests to new highway
	BOOL bMovedOK=TRUE;
	if (pHigh) {
		int iCurrentSource=0, iNumberUsers=0;
		bncs_stringlist ssl_Users = bncs_stringlist("", '|');
		//
		CRevsRouterData* pFromRtr = GetValidRouter(pHigh->getFromRouter());
		CRevsRouterData* pToRtr = GetValidRouter(pHigh->getToRouter());
		if (pFromRtr&&pToRtr) {

			iCurrentSource = pFromRtr->getRoutedSourceForDestination(pHigh->getFromDestination());
			Debug("curr src %d for dest %d", iCurrentSource, pHigh->getFromDestination());
			//if ((iCurrentSource==i_ParkSource_O)||(iCurrentSource==i_TestSource_O)) iCurrentSource=0; 
			// get number users of "toSrc" on other router
			iNumberUsers = pToRtr->getNumberOfDestsUsingSource(pHigh->getToSource());
			ssl_Users = bncs_stringlist(pToRtr->getListOfDestsUsingSource(pHigh->getToSource()), '|');
		}
		//
		if ((iCurrentSource>0)&&(iNumberUsers>0)) {
			CRouterHighway* pNewHigh = GetNextPoolHighway(iCurrentSource, pHigh->getFromRouter(), pHigh->getToRouter(), 0, pHigh->getHighwayIndex() );
			if (pNewHigh) {
				// route current source to new highway 
				SendRouterCrosspointToCommandQueue( pNewHigh->getFromRouter(), iCurrentSource, pNewHigh->getFromDestination(), 0 );
				// route landing point of new highway to all users to original highway
				for (int iU=0;iU<ssl_Users.count();iU++) {
					if (ssl_Users[iU].toInt() != iExemptDestination) {
						SendRouterCrosspointToCommandQueue( pNewHigh->getToRouter(), pNewHigh->getToSource(), ssl_Users[iU].toInt(),0 );
					}
					else {
						Debug("MoveHighway - ignoring exempt dest(%d) whilst moving highway", iExemptDestination );
					}
				}
				ProcessNextCommand(ssl_Users.count());
			}
			else {
				Debug( "MoveHighway -ERROR- NO FREE TIELINE TO MOVE TO - nothing moved or changed");
				bncs_string sstr = bncs_string("ERROR - NO FREE TIELINE FOR MOVE of tieline %1 ").arg(pHigh->getHighwayIndex());
				PostAutomaticErrorMessage(iGRDDevice, ALARM_SET_ERROR_MESSAGE, sstr );
				bMovedOK = FALSE;
			}
		}
		else
			Debug( "MoveHighway -info- no/park/test src or no users - so no move required on highway %d", pHigh->getHighwayIndex());
	}
	
	return bMovedOK;
}


BOOL CheckThruRouting( int iSrcRtr, int iInitSrc, int iDestRtr, int iFinalDest )
{
	// to check that initial source is not part of a highway and the destination is part of ( same or different ) highway / wrap
	// to prevent routing loops.....
	// src 
	CRouterHighway* pSrcHigh = GetRouterHighwayToSource(iSrcRtr, iInitSrc);
	// dest
	CRouterHighway* pDestHigh = GetRouterHighwayFromDest(iDestRtr, iFinalDest);
	//
	char szMessage[MAX_BUFFER_STRING]="";
	if ((pSrcHigh)&&(pDestHigh)) {
		if (pSrcHigh->getHighwayIndex()==pDestHigh->getHighwayIndex()) {
			wsprintf( szMessage, "Requested route part of SAME highway/wrap %d", pSrcHigh->getHighwayIndex() );
			PostAutomaticErrorMessage(iGRDDevice, ALARM_SET_WARN_MESSAGE, bncs_string(szMessage));
			Debug ("CheckRouting-ERROR-Requested route part of SAME highway/wrap %d ", pSrcHigh->getHighwayIndex() );
			return FALSE;
		}
		if ((pSrcHigh->getHighwayType()<HIGHWAY_WRAP_TYPE)&&(pDestHigh->getHighwayType()<HIGHWAY_WRAP_TYPE)) {
			// ie both program or monitor highways - cannot route a highway to another highway 
			wsprintf(szMessage, "Inhibiting the routing of highway %d to highway %d", pSrcHigh->getHighwayIndex(), pDestHigh->getHighwayIndex());
			PostAutomaticErrorMessage(iGRDDevice, ALARM_SET_WARN_MESSAGE, bncs_string(szMessage));
			Debug("CheckRouting-ERROR-inhibiting routing of highway %d to highway %d ", pSrcHigh->getHighwayIndex(), pDestHigh->getHighwayIndex());
			return FALSE;
		}
	}
	//
	// other tests on direct routing into highways from RC commands
	//
	if (pDestHigh) {
		if (pDestHigh->getHighwayType()<HIGHWAY_WRAP_TYPE) {
			wsprintf( szMessage, "Inhibiting direct routing into highway indx %d dest %d on other router", pDestHigh->getHighwayIndex(), iFinalDest );
			PostAutomaticErrorMessage(iGRDDevice, ALARM_SET_WARN_MESSAGE, bncs_string(szMessage));
			Debug("CheckRouting-ERROR-inhibiting direct routing into highway indx %d dest %d on other router", pDestHigh->getHighwayIndex(), iFinalDest);
			return FALSE;
		}
	}
	return TRUE;
}


//////////////////////////////////////////////////
CRouterHighway* GetRequiredHighwayOfType(int iRtrSource, int iFromRouter, int iToRouter, int iFinalDest, int iRequiredType)
{
	int iHigh = 0;
	CRouterHighway* pHigh = NULL;

	if (iFromRouter != iToRouter) { // only need highways between different routers
		iHigh = 0;
		do {
			iHigh++;
			pHigh = GetRouterHighway(iHigh);
			if (pHigh) {

				if ((pHigh->getFromRouter() == iFromRouter) && (pHigh->getToRouter() == iToRouter)) {
					if (pHigh->getHighwayType() == iRequiredType) {
						// check for router divisor - source must be greater than it - so must highway dest
						CRevsRouterData* pFrom = GetValidRouter(iFromRouter);
						if ( ( (iRtrSource > pFrom->getTXRouterDivision()) && (pHigh->getFromDestination()>pFrom->getTXRouterDivision()) ) ||
							  ( (iRtrSource <= pFrom->getTXRouterDivision()) && (pHigh->getFromDestination()<=pFrom->getTXRouterDivision()) )  ) {
							// if of required type and correct side of divisor then use it - all non blocking == last case wins
							pHigh->setHighwayState(HIGHWAY_PENDING, iRtrSource, iConfigurationPending);
							// update gui for usage and current highway
							if (iChosenHighway == iHigh) DisplayHighwayData(iHigh);
							return pHigh;
						}
					}
					// else any other conditions ??? 
				}
			}
		} while (iHigh<iTotalHighwaysAndWraps);

	}
	return NULL;
}



//////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: GetNextPoolHighway()
//
CRouterHighway* GetNextPoolHighway( int iRtrSource, int iFromRouter, int iToRouter, int iFinalDest, int iIgnoreHighway )
{
	int iHigh=0;
	CRouterHighway* pHigh = NULL;

	if (iFromRouter!=iToRouter) { // only need highways between different routers

		// step 1. - is src already routed on another highway for the "fromRouter"
		// if source is already on program highway then use that one - even if dest is dedicated mon dest
		// but DO NOT use existing MON HIGHWAY if src is already on that one
		pHigh = FindSourceAlreadyPoolRouted(iFromRouter, iRtrSource, iIgnoreHighway);
		if (pHigh!=NULL) {	// use highway found
			// update gui for usage and current highway
			if (iChosenHighway==pHigh->getHighwayIndex()) DisplayHighwayData(iHigh);
			return pHigh;
		}

		// step 2. if final dest is already using any other ( program ) highway and it is the only dest using that highway, then reuse the same highway
		pHigh = FindDestinationIsSoloOnPoolHighway(iFinalDest, iToRouter, 0);
		if (pHigh!=NULL) {
			pHigh->setHighwayState(HIGHWAY_PENDING, iRtrSource, iConfigurationPending);
			// update gui for usage and current highway
			if (iChosenHighway==iHigh) DisplayHighwayData(iHigh);
			return pHigh;
		}

		// step 4. find a free PROGRAM highway and set the new routing
		//         note monitoring highways will have been sorted out in 2. above
		iHigh=0;
		do {
			iHigh++;
			pHigh = GetRouterHighway(iHigh);
			if (pHigh) {
				if ((pHigh->getFromRouter()==iFromRouter)&&(pHigh->getToRouter()==iToRouter)) {
					if ((pHigh->getHighwayState()==HIGHWAY_FREE)&&(pHigh->getHighwayType()==HIGHWAY_POOL_TYPE)) {
						pHigh->setHighwayState(HIGHWAY_PENDING, iRtrSource, iConfigurationPending);
						
						// update gui for usage and current highway
						if (iChosenHighway==iHigh) DisplayHighwayData(iHigh);
						return pHigh;
					} 
					// else any other conditions ??? 
				}
			}
		} while (iHigh<iTotalHighwaysAndWraps);

		// ??? hanging highways ??? - ie those WITH PARK ROUTED and users or have either sources but no users of highway

	}
	// return NULL - no highway found
	Debug( "GetNextPoolHighway -ERROR- no free highway found, from %d to %d", iFromRouter, iToRouter );
	return NULL;
}


//////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: RouteParkToHighway()
//
void RouteParkToHighway(CRouterHighway* pHigh, BOOL bCalculate)
{
	char szCommand[MAX_BUFFER_STRING]="";
	
	// was if ((pHigh)&&(ecCSIClient)&&(iOverallTXRXModeStatus==INFO_TXRXMODE)&&(!bAutomaticStarting)) {
	if ((pHigh)&&(ecCSIClient)&&(!bAutomaticStarting)) {
		if (pHigh->getHighwayType()!=HIGHWAY_WRAP_TYPE) {
			CRevsRouterData* pFromRtr = GetValidRouter(pHigh->getFromRouter());
			CRevsRouterData* pToRtr = GetValidRouter(pHigh->getToRouter());
			if (pFromRtr&&pToRtr) {
				SendRouterCrosspointImmediately(pHigh->getFromRouter(), pFromRtr->getParkSource(), pHigh->getFromDestination(), 0);
				// need to route park on the other side now to user dests of highway
				bncs_stringlist ssl_Users = bncs_stringlist( pToRtr->getListOfDestsUsingSource( pHigh->getToSource()), '|' );
				for (int iU=0;iU<ssl_Users.count();iU++) {
					SendRouterCrosspointImmediately(pHigh->getToRouter(), pToRtr->getParkSource(), ssl_Users[iU].toInt(), 0);
					// as highway user is on emerald IP infodriver Router -- clear xpoint / store xpoint as 0, park so that number of users on that side becomes  0 -- handles force park scenario
					ProcessPhysicalRouterRevertive(pHigh->getToRouter(), ssl_Users[iU].toInt(), 0);
				}
			}
			//
			// was pHigh->setHighwayState(HIGHWAY_FREE, 0, 0);
			pHigh->setHighwayState(HIGHWAY_CLEARING, 0, iConfigurationPending);
			pHigh->resetParkitCount();
			if (pHigh->getHighwayIndex()==iChosenHighway) DisplayHighwayData(iChosenHighway);
			if (bCalculate==TRUE) CalculateHighwayUsed();
		}
		else
			Debug( "RoutePark - highway (%d) was a wrap type - so ignored", pHigh->getHighwayIndex() );
	}
	
}



//////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: RouteTestToHighway()
//
void RouteTestToHighway( CRouterHighway* pHigh )
{
	// routes test src, also "disables" highway from further use until parked
	// if highway in use it will move existing users first
	char szCommand[MAX_BUFFER_STRING]="";
	BOOL bContinue = TRUE;
	
	// was if ((pHigh)&&(ecCSIClient)&&(iOverallTXRXModeStatus==INFO_TXRXMODE)&&(!bAutomaticStarting)) {
	if ((pHigh)&&(ecCSIClient)&&(!bAutomaticStarting)) {
		if (pHigh->getHighwayType()!=HIGHWAY_WRAP_TYPE) {
			// need to move any current src and dests to another highway first
			if (pHigh->getHighwayState()>HIGHWAY_FREE) {
				bContinue =  MoveGivenHighwayToNewOne( pHigh, UNKNOWNVAL ); /// xxx remove
			}
			
			if (bContinue) {
				// if ok now route test
				CRevsRouterData* pFromRtr = GetValidRouter(pHigh->getFromRouter());
				if (pFromRtr) {
					pHigh->setHighwayState(HIGHWAY_INUSE,pFromRtr->getTestSource(), 0);
					SendRouterCrosspointToCommandQueue(pHigh->getFromRouter(), pFromRtr->getTestSource(), pHigh->getFromDestination(), 0);
				}
				ProcessNextCommand(1);
			}
			else {
				Debug( "RouteTest - ERROR - test signal not routed, nor Highway Disabled (%d) ", pHigh->getHighwayIndex() );
			}
			if (pHigh->getHighwayIndex()==iChosenHighway) DisplayHighwayData(iChosenHighway);
			CalculateHighwayUsed();
		}
		else
			Debug( "RoutePark - highway (%d) was a wrap type - so ignored", pHigh->getHighwayIndex() );
	}
}



//////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: TraceUltimateSource()
//  calculates the trace to the primary source -- both physical and virtual
bncs_string TraceUltimateSource(int iStartRtr, int iStartDest, int iStartSource, int *iTracedVirtualSrce, int *iHWType )
{
	int iToSrc = iStartSource;
	int iToRtr = iStartRtr;
	int iFromRtr=iStartRtr;
	int iFromDest=iStartDest;
	int iLoopCount=0;
	bncs_stringlist ssl_prevHighs = bncs_stringlist("", '|');
	bncs_stringlist ssl_trace = bncs_stringlist("", '|');
	CRevsRouterData* pStart = GetValidRouter(iStartRtr);
	int iVirSrc = iToSrc + pStart->getRouterOffsetIntoVirtual();
	if (pStart) ssl_trace.append(bncs_string("%1").arg(iVirSrc));
	BOOL bContinue=TRUE;
	do {
		// if src is linked to a highway - get highway and continue trace
		CRouterHighway* pHigh = GetRouterHighwayToSource( iToRtr, iToSrc );
		if (pHigh) {
			if (bShowAllDebugMessages) Debug("trace -> highway %d type %d", pHigh->getHighwayIndex(), pHigh->getHighwayType());
			// process highway or wrap
			iFromRtr = pHigh->getFromRouter();
			iFromDest = pHigh->getFromDestination();
			CRevsRouterData* pFromRtr = GetValidRouter(pHigh->getFromRouter());
			CRevsRouterData* pToRtr = GetValidRouter(pHigh->getToRouter());
			if (pFromRtr&&pToRtr) {
				// now reset vars
				iToSrc = pFromRtr->getRoutedSourceForDestination(iFromDest);
				iToRtr = pFromRtr->getRouterIndex();
				iVirSrc = iToSrc + pFromRtr->getRouterOffsetIntoVirtual();
				// test to break loop if it seems to be occurring
				// can occur when a highway routed into another highway or itself, or a wrap routed to itself
				ssl_prevHighs.append(bncs_string(pHigh->getHighwayIndex()));
				ssl_trace.prepend(bncs_string("%1").arg(iVirSrc));
				*iHWType = pHigh->getHighwayType();
				if (ssl_prevHighs.find(bncs_string(pHigh->getHighwayIndex()) >= 0)) iLoopCount++;
				if (bShowAllDebugMessages) Debug("trace loopcount %d list entries %d %s", iLoopCount, ssl_prevHighs.count(), LPCSTR(ssl_prevHighs.toString('|')));
				if ((iLoopCount > 5) || (ssl_prevHighs.count() > 10)) {
					// would a src be repeatedly wrapped and multiple highways 16 times and be a valid routing ??? -- very unlikely
					Debug("TraceUltSrc-ERROR-Excessive LOOPCOUNT, repeating highways %s", LPCSTR(ssl_prevHighs.toString('|')));
					bContinue = FALSE;
				}
			}
		}
		else {
			// src is not linked to any highway or wrap - so finished
			bContinue = FALSE;
		}

	} while (bContinue==TRUE);
	// 
	if (bShowAllDebugMessages) Debug( "TraceUltActSrc start src %d ends as %d", iStartSource, iToSrc);
	// returns the
	*iTracedVirtualSrce = iVirSrc;
	return ssl_trace.toString('|');
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// *** recurrsive function -- to calc list of users -- stored in global stringlist for processing
//
void DetermineListOfAffectedDestinations(int iStartRtr, int iStartDest)
{
	// put passed vars into list 
	ssl_CalcListOfDestinationsUsers.append(bncs_string("%1.%2").arg(iStartRtr).arg(iStartDest));
	// is dest a highway / wrap ??
	CRouterHighway* pHigh = GetRouterHighwayFromDest(iStartRtr, iStartDest);
	if (pHigh) {
		// get list of physical dests the "to-srce" is routed to
		bncs_stringlist sslMoreUsers = bncs_stringlist("", '|');
		CRevsRouterData* pToRtr = GetValidRouter(pHigh->getToRouter());
		if (pToRtr) {
			sslMoreUsers = bncs_stringlist(pToRtr->getListOfDestsUsingSource(pHigh->getToSource()), '|');
		}
		// now process thru all these 
		for (int iU = 0; iU<sslMoreUsers.count(); iU++) {
			bncs_string ssUser = bncs_string("%1.%2").arg(pHigh->getToRouter()).arg(sslMoreUsers[iU]);
			if (ssl_CalcListOfDestinationsUsers.find(ssUser) >= 0) iRecursiveCounter++;
			if (iRecursiveCounter>10) {
				Debug("DetermineListOfAffectedDestinationPackages-Recursive Counter exceeded limit - %s", LPCSTR(ssUser));
				return; // abort due to repeatly finding the same user already in list -- implies a routing loop -- which should not be
			}
			DetermineListOfAffectedDestinations(pHigh->getToRouter(), sslMoreUsers[iU].toInt());
		}
	}
	return;
}

///////////////////////////////////////////////////////////////////////////////////

int GetRouterDeviceFromVirtualIndex(int iVirIndx, int iSrceOrDest, int *iRtrIndx)
{
	*iRtrIndx = UNKNOWNVAL;
	for (int iiRtr = 0; iiRtr < ssl_AllRoutersIndices.count(); iiRtr++) {
		CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iiRtr]);
		if (pRtr) {
			int iStartPt = pRtr->getRouterOffsetIntoVirtual();
			int iEndPt = pRtr->getMaxSources() + iStartPt;
			if (iSrceOrDest>0) iEndPt = pRtr->getMaxDestinations() + iStartPt;
			if ((iVirIndx > iStartPt) && (iVirIndx <= iEndPt)) {
				*iRtrIndx = iVirIndx - iStartPt;
				return pRtr->getRouterIndex();
			}
		}
	}
	Debug("GetRtrDevFromVirIndx -ERROR- index %d - no router found", iVirIndx);
	return UNKNOWNVAL;
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: ProcessVirtualRouterCrosspoint()
//
//  main function to commence making routes - and initiate search for highway if required
//
//
void ProcessVirtualRouterCrosspoint( int iSource, int iDestination, int iRequiredHighwayType )
{
	BOOL bClearingDownHighway=FALSE;
	char szCommand[MAX_BUFFER_STRING]="";
	char szMessage[MAX_BUFFER_STRING]="";

	if ((pRouterVirtual) && (mapOfAllHighways) && (mapOfAllRouters)) {
		if (iNextManagementProcessing<4) iNextManagementProcessing = 7; // keep it well clear of commands and immediate revertives	
		// parse mask - to see if contig highways, monitoring route required #
		if (bShowAllDebugMessages) Debug("ProcVirRC - dest %d requests highway type %d", iDestination, iRequiredHighwayType);
		int iContigHighways = 1;
		// check to see if virtual crosspoint on same physical router  and check for dual presented source
		if ((iDestination>0) && (iSource>0)) {
			// determine which router destination is on
			// determine which router source is on
			// if same NO highway required - just route locally
			int iPhysicalSrc = iSource;
			int iPhysicalDest = iDestination;
			int iSrcRouter = GetRouterDeviceFromVirtualIndex(iSource, 0, &iPhysicalSrc);
			int iDestRouter = GetRouterDeviceFromVirtualIndex(iDestination, 1, &iPhysicalDest);
			// now continue
			if ((iPhysicalSrc > 0) && (iPhysicalDest > 0)) {

				// handle multiple routes for stereo, 5.1 etc esp in audio versions
				if (iSrcRouter == iDestRouter) {
					// test if src and dest are a known loop - if so then inhibit route - either in wrap or src/dest are part of highway definition
					CRevsRouterData* pSameRtr = GetValidRouter(iSrcRouter);
					CRouterHighway* pWrapHigh = GetRouterHighwayFromDest(iDestRouter, iPhysicalDest);
					if (pWrapHigh&&pSameRtr) {

						if (iPhysicalSrc <= 0)	{
							// as dest is linked to a highway - if src==0 then translate to highway park source
							if (iDestRouter == pSameRtr->getRouterIndex()) iPhysicalSrc = pSameRtr->getParkSource();
						}

						// first test not soley routing into a highway dest on local router
						if (pWrapHigh->getHighwayType() != HIGHWAY_WRAP_TYPE) {
							wsprintf(szMessage, "INHIBITED-routing of rtr %d src %d into a HIGHWAY dest %d", iSrcRouter, iPhysicalSrc, iPhysicalDest);
							PostAutomaticErrorMessage(iSrcRouter, ALARM_SET_ERROR_MESSAGE, bncs_string(szMessage));
							Debug("Proc-Vir_Xpoint-INHIBITED-routing of rtr %d src %d into a HIGHWAY dest %d", iSrcRouter, iPhysicalSrc, iPhysicalDest);
							// exit function - no routes to be made in this instance
							return;
						}

						if ((pWrapHigh->getToSource() == iPhysicalSrc) && (pWrapHigh->getFromDestination() == iPhysicalDest)) {
							wsprintf(szMessage, "INHIBITED-routing src of highway into itself rtr %d src %d dest %d", iSrcRouter, iPhysicalSrc, iPhysicalDest);
							PostAutomaticErrorMessage(iSrcRouter, ALARM_SET_ERROR_MESSAGE, bncs_string(szMessage));
							Debug("Proc-Vir_Xpoint-INHIBITED-routing src of wrap/highway into itself rtr %d src %d dest %d", iSrcRouter, iPhysicalSrc, iPhysicalDest);
							// exit function - no routes to be made in this instance
							return;
						}

						// make route for highway/wrap
						int iUsers = 0;
						if (pWrapHigh->getHighwayState() >= HIGHWAY_INUSE) {
							// also report warning if dest is an input to a highway - and highway is in use or disabled
							iUsers = pSameRtr->getNumberOfDestsUsingSource(pWrapHigh->getToSource());
							if (iUsers > 0) Debug("ProcVirXPoint - WARNING - routing to a highway(%d) in use/disabled", pWrapHigh->getHighwayIndex());
						}
					}

					// both on same router - make crosspoint as no highway required			
					SendRouterCrosspointToCommandQueue(iDestRouter, iPhysicalSrc, iPhysicalDest, iRequiredHighwayType);

				}
				else {
					// AS a highway is required
					// first check that not routing src to a highway output on other router
					CRevsRouterData* pFromRtr = GetValidRouter(iSrcRouter);
					CRevsRouterData* pToRtr = GetValidRouter(iDestRouter);
					if (pFromRtr&&pToRtr) {
						if (iPhysicalSrc <= 0)	{
							//  if src==0 then translate to highway park source
							iPhysicalSrc = pFromRtr->getParkSource();
						}

						if (CheckThruRouting(iSrcRouter, iPhysicalSrc, iDestRouter, iPhysicalDest) == TRUE) {
							// src and dest ok, so get next free one
							CRouterHighway* pHigh = NULL;
							if ((iRequiredHighwayType > 0) && (iRequiredHighwayType<HIGHWAY_POOL_TYPE)) {
								// get highway for a specified use type
								pHigh = GetRequiredHighwayOfType(iPhysicalSrc, iSrcRouter, iDestRouter, iPhysicalDest, iRequiredHighwayType);
							}
							else {
								// get pool highway
								pHigh = GetNextPoolHighway(iPhysicalSrc, iSrcRouter, iDestRouter, iPhysicalDest, 0);
							}
							if (pHigh) {
								// make routes on highways to connect src to dest
								int iFromRtr = 0, iToRtr = 0, iFromDest = 0, iToSrc = 0;
								pHigh->getHighwayData(&iFromRtr, &iFromDest, &iToRtr, &iToSrc);
								// put into que and execute singly with gap to enable revs to be processed first.....
								SendRouterCrosspointToCommandQueue(iFromRtr, iPhysicalSrc, iFromDest, iRequiredHighwayType);
								SendRouterCrosspointToCommandQueue(iToRtr, iToSrc, iPhysicalDest, iRequiredHighwayType);
							}
							else {
								wsprintf(szMessage, "ERROR NO VALID HIGHWAY from rtr %d to %d", iSrcRouter, iDestRouter);
								PostAutomaticErrorMessage(iSrcRouter, ALARM_SET_ERROR_MESSAGE, bncs_string(szMessage));
								Debug("ProcVirRtrXPoint ERROR NO VALID HIGHWAY from rtr %d to %d", iSrcRouter, iDestRouter);
								Debug("ProcVirRtrXPoint ERROR No Crosspoint(s) made from src %d to dest %d", iSource, iDestination);
							}
						}
						else {
							wsprintf(szMessage, "ERROR- cannot route directly into highways or one highway into another");
							PostAutomaticErrorMessage(iSrcRouter, ALARM_SET_ERROR_MESSAGE, bncs_string(szMessage));
							Debug("ProcVirXPoint -ERROR- cannot route directly into highways or one highway into another");
						}
					}

				}

				// kick off loop for commands
				ProcessNextCommand(1);

			}
			else 
				Debug("ProcVirXPoint invalid ACTUAL src %d or dest %d", iPhysicalSrc, iPhysicalDest);
		}
		else 
			Debug("ProcVirXPoint invalid src %d or dest %d", iSource, iDestination);
	}
	else
		Debug("ProcVirXPoint -auto starting- awaiting key classes - route ignored");
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: ProcessVirtualRouterPoll()
//
//  main function to commence making routes - and initiate search for highway if required
//
//
void ProcessVirtualRouterPoll( int iStartDestination, int iEndDestination, int iFromWS )
{
	if ((iOverallTXRXModeStatus==INFO_TXRXMODE)&&(bAutomaticStarting==FALSE)) {
		if ((pRouterVirtual)&&(edMainGRD)) {
			char szMessage[MAX_BUFFER_STRING];
			for (int iIndx=iStartDestination;iIndx<=iEndDestination;iIndx++) {
				int iRoutedSrc = pRouterVirtual->getRoutedSourceForDestination(iIndx);
				wsprintf( szMessage, "RR %d %d %d", pRouterVirtual->getRouterIndex(), iIndx, iRoutedSrc );
				if (bShowAllDebugMessages) Debug( "ProcVirPoll - virdest %d src %d", iIndx, iRoutedSrc );
				// use outgoing que if more than 200 dests requested in a poll at any one time
				if ((iEndDestination-iStartDestination)>199) {
					AddResponseToOutgoingQue(szMessage, ROUTERCOMMAND, pRouterVirtual->getRouterIndex(), iIndx, iFromWS );
				}
				else {
					edMainGRD->txrevmsg( szMessage, 0, FALSE );
					//lTXID++;
				}
				// update virtual infodriver slot == dest too 
				if ((iIndx > 0) && (iIndx <= pRouterVirtual->getMaxDestinations())) {
					if (eiInfoGrd) {
						char szSrc[MAX_BUFFER_STRING] = "";
						// was wsprintf(szSrc, "index=%d,trace=%s", iRoutedSrc, LPCSTR(pRouterVirtual->getDestinationTrace(iIndx)));
						wsprintf(szSrc, "%d, %d", iRoutedSrc, pRouterVirtual->getDestinationHWTypeUsed(iIndx)); 
						eiInfoGrd->updateslot(iIndx, szSrc);
					}
				}
			}
			if ((bOutgoingTimerRunning==FALSE)&&(qOutgoing.size()>0)) ProcessAnyMoreResponses( 200 );
		}
	}
	else {
		if (bShowAllDebugMessages) Debug( "ProcVirRouterPoll - RX-Only/Starting up- so no poll response(s) given");
	}
}



////////////////////////////////////////////////////////////////////////////////////////////
//
//

int CalculateRandomOffset()
{
	// used to create a time offset before calling TXRX function - to stop multiInfodriver tx clash
	int iRand1 = 0;
	//srand( time(0) ); // generate random seed point
	srand( iWorkstation*10000 ); // generate random seed point
	iRand1 = (rand()%41)*59;     // devised to create a spread of values so as force a gap between random values that come out close to each other
	return (iRand1);  
}


/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
//
//  CSI DRIVER  FUNCTIONS
//
///////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: CSIDriverNotify()
//
//  PURPOSE: Callback function for incoming CSI messages for driver class
//
//  COMMENTS: the message is supplied in szMsg
//			  
//	
LRESULT CSIDriverNotify(extdriver* ped, LPCSTR szMsg)
{
	if (ped) {
		// 		Debug("into CSIDriverNotify ped cmd %d msg %s", ped->getstate(), szMsg );
		// 	else 
		// 		Debug("into CSIDriverNotify - no ped class");

		UINT iParamCount = ped->paramcount;
		UINT iCmdState = ped->getstate();
		if (bShowAllDebugMessages) Debug("CSI Driver Notify -- ped message %s", szMsg);
		if (bShowAllDebugMessages) Debug("CSI Driver Notify -- ped message count %d ", iParamCount);
		if (bShowAllDebugMessages) Debug("CSI Driver Notify -- ped state %d ", iCmdState);

		if ((iParamCount > 1) && (iCmdState != STATUS)) {
			// -- there are STATUS messages which are invalid and less than 4 params
			//Debug("into switch");
			switch (ped->getstate())
			{
			case COMMAND:
			{
				//Debug("into commmand case");

				// only messages with suficient parameters are valid for processing here 
				int iGivenDevice = 0;
				int iIndex1 = 0;
				int iIndex2 = 0;
				int iStateMask = 0;
				int iFromWS = 0;
				if (iParamCount>1) iGivenDevice = atoi(ped->param[1]);
				if (iParamCount>2) iIndex1 = atoi(ped->param[2]);
				if (iParamCount>3) iIndex2 = atoi(ped->param[3]);
				if (iParamCount>4) iStateMask = atoi(ped->param[4]);
				if (iParamCount>4) iFromWS = atoi(ped->param[4]);

				char szCommand[MAX_BUFFER_STRING] = "";
				char szStateMask[MAX_BUFFER_STRING] = "";
				if (iParamCount >= 1) strcpy(szCommand, ped->param[0]);
				if (iParamCount>4) strcpy(szStateMask, ped->param[4]);

				if (bShowAllDebugMessages) Debug("CSIDrvNotify - fromWS is %d", iFromWS);

				if (strlen(szCommand)>1) {
					// parse command provided by driver notify
					if ((szCommand[0] == 'R') && (szCommand[1] == 'C')) {
						if (bShowAllDebugMessages) Debug("CsiDrv - RC command for %d source %d to dest %d mask %s", iGivenDevice, iIndex1, iIndex2, szStateMask);
						//if ((bAbletoProcessIncomingCommand==TRUE)&&(qIncoming.size()==0)) {
						//	bAbletoProcessIncomingCommand = FALSE;
						ProcessVirtualRouterCrosspoint(iIndex1, iIndex2, iStateMask);
						//}
						//else {
						//	AddIncomingToQue(szCommand, ROUTERCOMMAND, iGivenDevice, iIndex1, iIndex2, iStateMask, szStateMask);
						//}
						return TRUE;
					}

					if ((szCommand[0] == 'R') && (szCommand[1] == 'P')) {
						if (bShowAllDebugMessages) Debug("CsiDrv - RP command for %d range %d %d", iGivenDevice, iIndex1, iIndex2);
						ProcessVirtualRouterPoll(iIndex1, iIndex2, iFromWS);
						return TRUE;
					}

					if ((szCommand[0] == 'R') && (szCommand[1] == 'L')) {
						Debug("CsiDrv - IGNORED RL command for %d destination(s) %d %d lockmode %d", iGivenDevice, iIndex1, iIndex2, iStateMask);
						//SendRouterLockCommand( iGivenDevice, iIndex1, iIndex2, iState );
						return TRUE;
					}

					if ((szCommand[0] == 'R') && (szCommand[1] == 'M')) {
						if (bShowAllDebugMessages) Debug("CsiDrv - IGNORED RM command for %d database %d index %d", iGivenDevice, iIndex1, iIndex2);
						//ProcessRouterModifyCommand( iGivenDevice, iIndex1, iIndex2 );
						return TRUE;
					}

				}
				lRXID++;
				UpdateCounters();

			}
				break;

			case STATUS:
				// ignore these messages
				if (bShowAllDebugMessages) Debug("Status message ");
				break;

			case DISCONNECTED:
				if (bShowAllDebugMessages) Debug("DriverNotify -- DISCONNECTED message received - shutting app down");
				PostMessage(hWndMain, WM_CLOSE, 0, 0);
				break;

			case RXONLY:
				if (bShowAllDebugMessages) Debug("DriverNotify -- RXonly message - another ws txrx - %s", szMsg);
				if ((iOverallTXRXModeStatus == IFMODE_TXRX) && (iOverallStateJustChanged == 0) && (ped->iDevice == iGRDDevice)) {
					if (bShowAllDebugMessages) Debug("DriverNotify -- RXONLY message - another ws txrx  - %d ", ped->iDevice);
					// as another workstation is hosting this driver number
					iOverallStateJustChanged = 1;
					ForceAutoIntoRXONLY();
				}
				break;

			case TO_RXONLY:
				if (bShowAllDebugMessages) Debug("DriverNotify -- GOTO RXONLY message ignored ");
				return FALSE;
				break;

			case TO_TXRX:
				if (bShowAllDebugMessages) Debug("DriverNotify -- GOTO TXRX message received - %s", szMsg);
				//if in rxonly -- does it turn txrx ( or if recently was forced into txrx ?? )
				if ((iOverallTXRXModeStatus != INFO_TXRXMODE) && (iOverallStateJustChanged == 0) && (ped->iDevice == iGRDDevice) ) {
					iOverallStateJustChanged = 1;
					int iTimerOffset = CalculateRandomOffset();
					if (bShowAllDebugMessages) Debug("DriverNotify - force go TXRX - in %d ms", iTimerOffset);
					SetTimer(hWndMain, FORCETXRX_TIMER_ID, iTimerOffset, NULL);
				}
				break;

			case QUERY_TXRX:
				if (bShowAllDebugMessages) Debug("DriverNotify -- Query txrx message received");
				// ignore 
				break;

			default:
				if (bShowAllDebugMessages) Debug("DriverNotify -- Other State %s", szMsg);
			} // switch

		}
		else
			Debug("CSI Driver Notify -- Unexpected/Status message, param count %d,  msg %s ", iParamCount, szMsg);
	}
	else
		Debug("CSI Driver Notify -- Invalid PED class passed ");
		
	//Debug("out of CSIDriverNotify");
	return TRUE;
}





//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//
//  PROCESSING PHYSICAL ROUTER REVERTIVES
//
//
int ValidRouterRevertive( int iWhichRouter, int iGivenDest )
{
	int iCalcVirtual=UNKNOWNVAL;
	CRevsRouterData* pRtr = GetValidRouter(iWhichRouter);
	if (pRtr) {
		if ((iGivenDest>0) && (iGivenDest <= pRtr->getMaxDestinations())) {
			iCalcVirtual = iGivenDest +pRtr->getRouterOffsetIntoVirtual();
		}
	}
	// return virtual dest for given physical dest
	return iCalcVirtual;
}


BOOL StoreVirtualRouterRevertive( int iPhysicalRtr, int iPhysicalDest, int iVirtualDest )
{
	BOOL bStoredOK=FALSE;
	//
	CRevsRouterData* pRevRtr = GetValidRouter(iPhysicalRtr);
	if (pRouterVirtual&&pRevRtr) {
		int  iVirSource=0, iHWType=0;
		int iPhysicalSrce = pRevRtr->getRoutedSourceForDestination(iPhysicalDest);
		bncs_string ssTrace = TraceUltimateSource(iPhysicalRtr, iPhysicalDest, iPhysicalSrce, &iVirSource, &iHWType);
		bncs_string ssSrc = bncs_string( "%1").arg(iVirSource);
		// store calculated src
		if (bShowAllDebugMessages) Debug("StoreVirRev vir dest %d trace src %d from rtr %d dest %d physrc %d", iVirtualDest, iVirSource, iPhysicalRtr, iPhysicalDest, iPhysicalSrce);
		// these source's ptis will change 
		int iprevsrc = pRouterVirtual->getRoutedSourceForDestination(iVirtualDest);
		if ((pRouterVirtual->getRoutedSourceForDestination(iVirtualDest)>0) && (ssl_CalcListOfChangedPtiSources.find(iprevsrc)<0)) ssl_CalcListOfChangedPtiSources.append(iprevsrc);  // prev src
		if ((iVirSource>0) && (ssl_CalcListOfChangedPtiSources.find(iVirSource)<0)) ssl_CalcListOfChangedPtiSources.append(bncs_string(iVirSource));                                                                     // new src
		//
		pRouterVirtual->storeGRDRevertive(iVirtualDest, iVirSource, ssSrc);
		pRouterVirtual->setDestinationTrace(iVirtualDest, ssTrace, iHWType);
		bStoredOK = TRUE;
		// update gui if required
		if (iVirtualDest==iChosenDestination) {
			char szData[64]="";
			int iCurrSrc = pRouterVirtual->getRoutedSourceForDestination(iChosenDestination);
			wsprintf( szData, "%d (%d)", iChosenDestination, iCurrSrc);
			SetDlgItemText( hWndDlg, IDC_RTR_DEST, szData );	
			SetDlgItemText(hWndDlg, IDC_RTR_DEST3, LPCSTR(ssTrace));
		}
	}
	// 
	return bStoredOK;
}

BOOL ExtractUserDetails(bncs_string ssUser, int *iUserRtr, int *iUserDest)
{
	BOOL bRet = FALSE;
	*iUserRtr = 0;
	*iUserDest = 0;
	bncs_stringlist ssl = bncs_stringlist(ssUser, '.');
	if (ssl.count() == 2) {
		int iRtr = ssl[0].toInt();
		int iDest = ssl[1].toInt();
		if ((iRtr > 0) && (iDest > 0)) {
			*iUserRtr = iRtr;
			*iUserDest = iDest;
			bRet = TRUE;
		}
	}
	return bRet;
}


void TrimAndUpdatePti(int iSlotIndx, bncs_string ssPti)
{
	if ((iSlotIndx>0) && (iSlotIndx <= pRouterVirtual->getMaxSources())) { // xxx ???
		if (ssPti.length() > 250) {
			ssPti.truncate(250);
			int iPos = ssPti.rfind('|');
			if (iPos > 240) ssPti.truncate(iPos + 1); // should only be a few places back - to provide a cleaner pti string
			ssPti.append('~');
		}
		eiInfoTracePti->updateslot(iSlotIndx, LPCSTR(bncs_string("pti=%1").arg(ssPti)));
	}
}

void ProcessPtiUpdate()
{
	for (int iU = 0; iU<ssl_CalcListOfChangedPtiSources.count(); iU++) {
		// update trace
		int iVirSrce = ssl_CalcListOfChangedPtiSources[iU].toInt();
		if ((iVirSrce>0) && (iVirSrce <= pRouterVirtual->getMaxSources())) { // xxx ???
			bncs_string sstr = pRouterVirtual->getListOfDestsUsingSource(iVirSrce);
			TrimAndUpdatePti(iVirSrce, sstr);
		}
	}
	ssl_CalcListOfChangedPtiSources.clear();
}


BOOL ProcessPhysicalRouterRevertive( int iActualRtr, int iActualDest, int iNewSource )
{
	BOOL bChangedSrc=FALSE, bHighwaysInvolved=FALSE;
	int iOldPhySrc=0, iOldUsers=-1, iParkSrc=0, iTestSrc=0, iU=0;
	int iOldSrcHighwayIndx = 0, iNewSrcHighwayIndx = 0;
	int iOldSrcVirtualIndx = 0, iNewSrcVirtualIndx = 0;
	bncs_string ssOldHighUsers = "", ssNewHighUsers = "";
	int iTracedSource = 0, iActualOffset = 0;

	CRevsRouterData* pRevRtr = GetValidRouter(iActualRtr);
	if (pRevRtr) {
		iOldPhySrc = pRevRtr->getRoutedSourceForDestination(iActualDest);
		if ((pRevRtr->getRouterRevertiveType() == INFO_REVERTIVE_TYPE) && (iNewSource == 0)) iNewSource = pRevRtr->getParkSource();
		bncs_string ssSrc = bncs_string( "%1").arg(iNewSource);
		pRevRtr->storeGRDRevertive(iActualDest, iNewSource, ssSrc);
		iOldUsers = pRevRtr->getNumberOfDestsUsingSource(iOldPhySrc); // was 3 lines up -- now true number of current users
		ssOldHighUsers = pRevRtr->getListOfDestsUsingSource(iOldPhySrc);
		ssNewHighUsers = pRevRtr->getListOfDestsUsingSource(iNewSource);
		iOldSrcVirtualIndx = iOldPhySrc + pRevRtr->getRouterOffsetIntoVirtual();
		iNewSrcVirtualIndx = iNewSource + pRevRtr->getRouterOffsetIntoVirtual();
		iActualOffset = pRevRtr->getRouterOffsetIntoVirtual();
	}

	if (iOldPhySrc!=iNewSource)	bChangedSrc = TRUE;

	if ((bAutomaticStarting==FALSE)&&(bChangedSrc==TRUE)) {
		// check for highways linked to dest / src -- and make changes ( eg if parked etc )
		// is there a highway involved eith dest or src or previous src
		CRouterHighway* pHighSrc = NULL;
		CRouterHighway* pHighDest = NULL;
		char szCommand[MAX_BUFFER_STRING]="";
		ssl_CalcListOfChangedPtiSources.clear();

		// check old source
		pHighSrc = GetRouterHighwayToSource(iActualRtr, iOldPhySrc);
		if (pHighSrc) {
			bHighwaysInvolved = TRUE;
			iOldSrcHighwayIndx = pHighSrc->getHighwayIndex();
			// was if ((pHighSrc->getHighwayType() != HIGHWAY_WRAP_TYPE) && (pHighSrc->getHighwayState() == HIGHWAY_INUSE)) {
			if ((pHighSrc->getHighwayType() != HIGHWAY_WRAP_TYPE) && ((pHighSrc->getHighwayState() >= HIGHWAY_CLEARING )) ) {
				// check to see if this was last user of this incoming highway -- if so then route park into highway and mark as free ? 
				if (iOldUsers == 0) {
					if (pHighSrc->getHighwayState() > HIGHWAY_CLEARING) {
						if ((iOverallTXRXModeStatus == INFO_TXRXMODE) && (!bAutomaticStarting)) {
							CRevsRouterData* pFrom = GetValidRouter(pHighSrc->getFromRouter());
							if (pFrom) {
								iParkSrc = pFrom->getParkSource();
								if (iParkSrc>0) SendRouterCrosspointImmediately(pHighSrc->getFromRouter(), iParkSrc, pHighSrc->getFromDestination(), 0);
							}
						}
					}
					Debug("ProcPhyRoute - Old src part highway %d - has no users now - so parking and free", pHighSrc->getHighwayIndex());
					pHighSrc->setHighwayState(HIGHWAY_FREE, 0, 0);
					CalculateHighwayUsed();
					if (pHighSrc->getHighwayIndex() == iChosenHighway) DisplayHighwayData(iChosenHighway);
				}
			}
		}

		// new route src
		pHighSrc = GetRouterHighwayToSource(iActualRtr, iNewSource);
		if (pHighSrc) {
			bHighwaysInvolved = TRUE;
			iNewSrcHighwayIndx = pHighSrc->getHighwayIndex();
			// was if ((pHighSrc->getHighwayType() != HIGHWAY_WRAP_TYPE) && (pHighSrc->getHighwayState() <= HIGHWAY_INUSE) && (pHighSrc->getHighwayState() >= HIGHWAY_FREE)) {
			if ((pHighSrc->getHighwayState() <= HIGHWAY_INUSE) && (pHighSrc->getHighwayState() >= HIGHWAY_FREE)) {
				// as this is FAR side of highway -- do NOT update allocated src as it is not REAL src routed into highway
				pHighSrc->setHighwayState(HIGHWAY_INUSE,-1, 0 ); 
			}
			if (pHighSrc->getHighwayIndex()==iChosenHighway) DisplayHighwayData(iChosenHighway);
		}	

		// get number and list of all users on other end of highway
		iRecursiveCounter = 0;
		ssl_CalcListOfDestinationsUsers.clear();
		DetermineListOfAffectedDestinations(iActualRtr, iActualDest);  // in form rtr.index | rtr.index | etc == then process thru below
		// int iUsers = ssl_CalcListOfDestinationsUsers.count();

		// NOW proccess thru calculated list of all dests/users at other end of this highway and generate all required vir revs
		for (iU = 0; iU<ssl_CalcListOfDestinationsUsers.count(); iU++) {
			int iUserRtr = 0, iUserDest = 0;
			if (ExtractUserDetails(ssl_CalcListOfDestinationsUsers[iU], &iUserRtr, &iUserDest)) {
				int iVirtualDest = ValidRouterRevertive(iUserRtr, iUserDest);
				if (StoreVirtualRouterRevertive(iUserRtr, iUserDest, iVirtualDest) == TRUE) {
					ProcessVirtualRouterPoll(iVirtualDest, iVirtualDest, 0);
				}
				Debug("ProcPhyRev -USERS-  rtr %d dest %d newsrc %d user %d %d virdest %d", iActualRtr, iActualDest, iNewSource, iUserRtr, iUserDest, iVirtualDest);
			}
			else
				Debug("ProcPhyRev -Users- invalid user %s ", LPCSTR(ssl_CalcListOfDestinationsUsers[iU]));
		}
						
		// routed dest - is it assoc to highway or wrap
		pHighDest = GetRouterHighwayFromDest( iActualRtr, iActualDest );
		if (pHighDest) {
			bHighwaysInvolved = TRUE;
			int iHighUsers = 0;
			CRevsRouterData* pToRtr = GetValidRouter(pHighDest->getToRouter());
			if (pToRtr) {
				iHighUsers = pToRtr->getNumberOfDestsUsingSource(pHighDest->getToSource());
			}
			//
			if ((iNewSource == iParkSrc)||(iNewSource==0)) {
					if (iHighUsers>0) 
						pHighDest->setHighwayState(HIGHWAY_INUSE, iNewSource, 0);
					else 
						pHighDest->setHighwayState(HIGHWAY_FREE, 0, 0);
					pHighDest->assignAllocatedHighwaySource(iNewSource);
			}
			else if (iNewSource == iTestSrc) {
				// only change if not currently disabled
				if (iHighUsers>0) 
					pHighDest->setHighwayState(HIGHWAY_INUSE, iNewSource, 0);
				else 
					pHighDest->setHighwayState(HIGHWAY_PENDING, iNewSource, iConfigurationPending);
				pHighDest->assignAllocatedHighwaySource(iNewSource);
			}
			else if ((iNewSource>0) && (pHighDest->getFromDestination() == iActualDest)) {
				// only change if not currently disabled
				if (iHighUsers>0) 
					pHighDest->setHighwayState(HIGHWAY_INUSE, iNewSource, 0);
				else 
					pHighDest->setHighwayState(HIGHWAY_PENDING, iNewSource, iConfigurationPending);
				pHighDest->assignAllocatedHighwaySource(iNewSource);
			}
			else if ((iNewSource < 0) && (pHighDest->getFromDestination() == iActualDest)) {
				pHighDest->setHighwayState(HIGHWAY_UNKNOWN, 0, 0);
				pHighDest->assignAllocatedHighwaySource(iNewSource);
			}
			//
			if (pHighDest->getHighwayIndex()==iChosenHighway) DisplayHighwayData(iChosenHighway);
		}
		//
		if (bHighwaysInvolved == TRUE) {
			// calc virtual pti for source of highways affected above --note pti is just direct users of highway/wrap etc- not done elsewhere in this auto
			if ((iOldSrcHighwayIndx > 0) && (iOldSrcVirtualIndx > 0) && (iOldSrcVirtualIndx <= pRouterVirtual->getMaxSources())) {
				bncs_string ssOldPti = "";
				bncs_stringlist ssl = bncs_stringlist(ssOldHighUsers, '|');
				for (int iC = 0; iC<ssl.count(); iC++) {
					int iDest = ssl[iC].toInt();
					if (iDest>0) {
						if (iC>0) ssOldPti.append("|");
						ssOldPti.append(bncs_string(iDest + iActualOffset));
					}
				}
				// update pti slot
				pRouterVirtual->setSourcePti(iOldSrcVirtualIndx, ssOldPti);
				TrimAndUpdatePti(iOldSrcVirtualIndx, ssOldPti);
			}
			if ((iNewSrcHighwayIndx > 0) && (iNewSrcVirtualIndx > 0) && (iNewSrcVirtualIndx <= pRouterVirtual->getMaxSources())) {
				bncs_string ssNewPti = "";
				bncs_stringlist ssl = bncs_stringlist(ssNewHighUsers, '|');
				for (int iC = 0; iC<ssl.count(); iC++) {
					int iDest = ssl[iC].toInt();
					if (iDest>0) {
						if (iC>0) ssNewPti.append("|");
						ssNewPti.append(bncs_string(iDest + iActualOffset));
					}
				}
				// update pti slot
				pRouterVirtual->setSourcePti(iNewSrcVirtualIndx, ssNewPti);
				TrimAndUpdatePti(iNewSrcVirtualIndx, ssNewPti);
			}
			//
			CalculateHighwayUsed();
		}
		//
		ProcessPtiUpdate();
	}
	return bChangedSrc;
}


void ProcessRouterPendingStatus( int iActualRtr, int iActualDest )
{
   // anything to do -- set pending count  so that highways will not be cleared immediately if 0 is source or unknown from start-up
	CRevsRouterData* pRtr = GetValidRouter(iActualRtr);
	CRouterHighway* pHighDest = GetRouterHighwayFromDest( iActualRtr, iActualDest );
	if (pRtr&&pHighDest) {
		if ((pHighDest->getHighwayState() > HIGHWAY_CLEARING) &&
			((pHighDest->getAllocatedHighwaySource() != pRtr->getParkSource()) || (pHighDest->getAllocatedHighwaySource() != 0)) ) {
				pHighDest->setHighwayState(HIGHWAY_PENDING, -1, iConfigurationPending);
		}
	}
}


/// RM recent list manipulation
BOOL FindEntryInRecentRMList(int iDev, int iDB, int iIndex)
{
	bncs_string ssStr = bncs_string("%1.%2.%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.count()>0) {
		if (ssl_RMChanges.find(ssStr)>=0) {
			//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s found", LPCSTR(ssStr));
			return TRUE;
		}
	}
	return FALSE;
} 


void AddEntryToRecentRMList( int iDev, int iDB, int iIndex )
{
	bncs_string ssStr = bncs_string("%1.%2.%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.find(ssStr)<0) {
		//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s not found so adding", LPCSTR(ssStr));
		ssl_RMChanges.append( ssStr );		
		if (bRMDatabaseTimerRunning==TRUE) KillTimer( hWndMain, DATABASE_TIMER_ID );
		// reset timer
		SetTimer( hWndMain, DATABASE_TIMER_ID, 450, NULL ); 
	}
} 



////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: CSIClientNotify()
//
//  PURPOSE: Callback function for incoming CSI messages for client class
//
//  COMMENTS: the message is supplied in szMsg
LRESULT CSIClientNotify(extclient* pec,LPCSTR szMsg)
{
	if (!pec) {
		Debug("MAJOR ERROR - no pec client class passed to notify");
		return TRUE;
	}

	UINT iParamCount = pec->getparamcount();
	LPSTR szParamRev = pec->getparam(0);
	LPSTR szDevNum = pec->getparam(1);
	LPSTR szSlot = pec->getparam(2);
	LPSTR szIndex = pec->getparam(3);
	LPSTR szContent = pec->getparam(4);
	
	char szRMData[MAX_BUFFER_STRING]="";
	
	int iDevNum = atoi( szDevNum );
	int iDBNum = atoi( szSlot );
	
	// router revs
	int iRtrDest = atoi( szSlot );
	int iRtrSource = atoi( szIndex );
	int iVirtualDest = 0;


	// infodriver revs 
	int iInfoRevType=UNKNOWNVAL;
	int iSlotNum = atoi( szSlot );
	int iIndexNum = atoi( szIndex );
	int iContent =0;
	bncs_string ssContent="";     // for string router revs from infodriver routers eg "index=src"
	if ((iParamCount>4)&&(strlen(szContent)>0))
		iContent = atoi( szContent );  // for revertives that are just numbers;
	
	switch (pec->getstate())
	{
		
	case REVTYPE_R:
		if ((bShowAllDebugMessages)&&(!bAutomaticStarting)) Debug( "CSIClient - RTR Rev dev %d, dest %d has src %d ", iDevNum, iRtrDest, iRtrSource );
		iVirtualDest = ValidRouterRevertive( iDevNum, iRtrDest );
		if (pRouterVirtual) {
			if ((iVirtualDest>0)&&(iVirtualDest<=pRouterVirtual->getMaxDestinations())) {
				BOOL bChanged = ProcessPhysicalRouterRevertive( iDevNum, iRtrDest, iRtrSource );
				// process next command out - since revertive now received from previous command ( most likely )
				if (qCommands.size()>0)
					ProcessNextCommand(1);
				else if (qIncoming.size()>0) {
					ProcessNextIncomingCommand();
				}
			}
			else {
				Debug( "CSIClient -rtrRev- INVALID Virtual dest %d from rev dest %d", iVirtualDest, iRtrDest );
			}
		}
	break;

	case REVTYPE_I:
		if (iParamCount==5) {
			if (bShowAllDebugMessages) Debug( "CSIClient - Info Rev from dev %d, slot %d, state %s ", iDevNum, iSlotNum, szContent );
			StripOutQuotes(szContent, CHAR_SNGL_QUOTES);
			iContent = atoi(szContent);  // for revertives that are just numbers;
			ssContent = bncs_string(szContent);  // string based content
			// add processing of any infodriver revs applicable
			iVirtualDest = ValidRouterRevertive( iDevNum, iSlotNum );
			if ((pRouterVirtual)&&(iVirtualDest>0)&&(iVirtualDest<=pRouterVirtual->getMaxDestinations())) {

				// is revertive the Pending string -- flag in dest - so highway management does not clear down whilst route is being made
				if (ssContent.find("Pending")>=0) {
					ProcessRouterPendingStatus( iDevNum, iSlotNum );
					lRXID++;
					UpdateCounters();	
					return TRUE;
				}

				// extract from ssContent
				if ((ssContent.find(",") >= 0) ||(ssContent.find("=")>=0)) {
					bncs_stringlist ssl_params = bncs_stringlist(ssContent, ',');
					if (ssContent.find(ssInfodriverRoutePrefix) >= 0) {
						iContent = ssl_params.getNamedParam(ssInfodriverRoutePrefix).toInt();
					}
					else {
						if (ssl_params[0].find("=") >= 0) {
							bncs_string ssleft = "", ssInt = "";
							ssl_params[0].split('=', ssleft, ssInt);
							iContent = ssInt.toInt();
						}
					}
				}
				//
				if (bShowAllDebugMessages) Debug("CSINotify -inforev- src is %d from %s", iContent, LPCSTR(ssContent));
				//
				ProcessPhysicalRouterRevertive( iDevNum, iSlotNum, iContent );
				//
				// process next command out - since revertive now received from previous command ( most likely )
				if (qCommands.size()>0)
					ProcessNextCommand(1);
				else if (qIncoming.size()>0) {
					ProcessNextIncomingCommand();
				}
			}
		}
		else
			Debug( "CSIClient - Incorrectly formed infodriver revertive from dev %d ", iDevNum );
	break;
		
	case DATABASECHANGE:
		{
			if ( (FindEntryInRecentRMList(iDevNum, iDBNum, iIndexNum)==FALSE)&&(iOverallTXRXModeStatus==INFO_TXRXMODE) ) {
				// xxx check for dds csi rm messages etc
				char szCommand[MAX_BUFFER_STRING]="";
				strcpy( szRMData, ExtractSrcNamefromDevIniFile( iDevNum, iDBNum, iIndexNum ));
				if (bShowAllDebugMessages) Debug ("CSIClient - Database change message - %s; device %d db %d indx %d content %s", 
					szMsg, iDevNum, iDBNum, iIndexNum, szRMData );
				//
				if (iDevNum==pRouterVirtual->getRouterIndex()) {
					if (iDBNum==0) {		
						iLoadSrcListBoxEntry = 1;
						SetTimer(hWndMain, LISTBOX_TIMER_ID, 1000, NULL );
						LoadSourcesListbox();
					}
					else if (iDBNum==1) {
						iLoadDestListBoxEntry = 1;
						SetTimer(hWndMain, LISTBOX_TIMER_ID, 1000, NULL );
						LoadDestinationsListbox();
					}
				}
				else {
					CRevsRouterData* pRevRtr = GetValidRouter(iDevNum);
					if (pRevRtr) {
						if ((iDBNum >= 0) && (iDBNum <= 1) && (iOverallTXRXModeStatus == INFO_TXRXMODE)) {
							// change name for virtual router
							if (ecCSIClient&&pRouterVirtual) {
								ecCSIClient->setdbname(pRouterVirtual->getRouterIndex(), iDBNum, iIndexNum + pRevRtr->getRouterOffsetIntoVirtual(), szRMData);
							}
						}
					}
				}
				//add most recent data to list of changed indexes - to ignore the 2nd RM
				AddEntryToRecentRMList( iDevNum, iDBNum, iIndexNum );
			}
		}
	break;
		
	case REVTYPE_G:
		Debug ("CSIClient - IGNORED GPI Revertive from dev (%d), state %d",iDevNum );
	break;
		
	case STATUS:
		Debug("CSIClient - Status message is %s",szMsg);
	break;
		
	case DISCONNECTED:
		Debug("CSIClient - CSI has closed down - automatic in FAIL  or  ERROR  state");
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
		PostMessage( hWndMain, WM_CLOSE, 0, 0 );
	break;
		
	} // switch
	
	lRXID++;
	UpdateCounters();	
	/* always return TRUE so CSI doesn't delete your client registration */
	return TRUE;
		
}


void ProcessADeskCommand( int iSlot, bncs_string ssCmd, int iFromMonDesk )
{
	/// option - code to write if useful for commands linked to south monitoring desks coming via auto infodriver
	// format of command is : <rtr,source>|<rtr,dest>|<mask> eg for mask 0,MON or just MON or just 3 - meaning 3 consect routes 
	// response back thru slot is either FAIL - something could not be completed, or <rtr,dest>|<rtr,src> for highway used
	// slot to be used to inform panels of most recent cmd/response only
	bncs_stringlist ssl=bncs_stringlist(ssCmd,',');
	if (ssl.count()>=2) {
		// now parse command data given
		int iSource = ssl[0].toInt();
		int iFinalDest = ssl[1].toInt();
		int iHighwayType = ssl[2].toInt();
		if (bShowAllDebugMessages) Debug("ProcDeskCmd - route src %d to dest %d using highway %d",  iSource, iFinalDest, iHighwayType );
		ProcessVirtualRouterCrosspoint( iSource, iFinalDest, iHighwayType);		
	}
	else
		Debug( "ProcessDestCmd - short cmd string -%s- from slot %d", LPCSTR(ssCmd), iSlot );
}



void ProcessRoutingChangeCommand( int iSlotDest, bncs_string ssCmdData )
{
	// parse and pass on given data
	int iMask = 0;
	bncs_string ssMask = "0";
	int iFinalDest = iSlotDest;
	int iSource = ssCmdData.toInt();
	bncs_stringlist ssl_data = bncs_stringlist(ssCmdData, ',');  // if a mask value/string is given within command
	
	if (ssl_data.count() > 1) {
		iSource = ssl_data[0].toInt();
		ssMask = ssl_data[1];
		iMask = ssl_data[1].toInt();
	}
	//if ((bAbletoProcessIncomingCommand == TRUE) && (qIncoming.size() == 0)) {
	//	bAbletoProcessIncomingCommand = FALSE;
		ProcessVirtualRouterCrosspoint(iSource, iFinalDest, iMask);
	//}
	//else {
	//	AddIncomingToQue(ssCmdData, INFODRVCOMMAND, pRouterVirtual->getRouterIndex(), iSource, iFinalDest, iMask, LPCSTR(ssMask));
	//}
}



////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: InfoNotify()
//
//  PURPOSE: Callback function from an infodriver, notifying event
//
//  COMMENTS: the pointer pex references the infodriver which is notifying
//			  iSlot and szSlot are provided for convenience
//			
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot)
{
	if (pex) {
		
		switch (pex->iStatus)
		{
			case CONNECTED: // this is the "normal" situation
				if (bShowAllDebugMessages) 
					Debug( "InfoNotify - device %d slot %d changed to %s", pex->iDevice, iSlot, szSlot );
				UpdateCounters();
				if (SlotChange( pex->iDevice,iSlot,szSlot)==FALSE) 
					pex->setslot(iSlot,szSlot);
			break;
				
			case DISCONNECTED:
				Debug("Infodriver %d has disconnected- Rtr_highway auto CLOSING DOWN", pex->iDevice);
				PostMessage( hWndMain, WM_CLOSE, 0, 0 );
				return;
			break;
				
			case TO_RXONLY:
				if (bShowAllDebugMessages) Debug("Infodriver %d received request to go RX Only", pex->iDevice);
				if (iOverallTXRXModeStatus == INFO_TXRXMODE) {
					// need to bounce this request by reasserting this txrx 
					iOverallModeChangedOver = 2;
					if (pex->iDevice == iInfoDevice) {
						eiInfoGrd->setmode(INFO_TXRXMODE);
						eiInfoGrd->requestmode = TO_TXRX;
					}
					else if (pex->iDevice == iInfoTracePti) {
						eiInfoTracePti->setmode(INFO_TXRXMODE);
						eiInfoTracePti->requestmode = TO_TXRX;
					}
				}
				break;
				
			case TO_TXRX:
				if ((iOverallTXRXModeStatus == IFMODE_RXONLY) && (iOverallStateJustChanged == 0)) {
					Debug("Infodriver %d received request to go TXRX - checking state as it follows GRD state", pex->iDevice);
					CheckAutoResilienceState();
				}
			break;
				
			case QUERY_TXRX:
			case 2190:
				if (iLabelAutoStatus==3) Debug("Infodriver %d received Query_TXRX",pex->iDevice);
				// ignore this state -- means all left as it was 
			break;
				
		default:
			Debug("iStatus=%d",pex->iStatus);
		} // switch
	}
	else
		Debug("InfoNotify - no valid PEX class passed" );

}


//
//  FUNCTION: SlotChange()
//
//  PURPOSE: Function called when slot change is notified
//
//  COMMENTS: All we need here are the driver number, the slot, and the new contents
//		
//	RETURNS: TRUE for processed (don't update the slot),
//			 FALSE for ignored (so calling function can just update the slot 
//								like a normal infodriver)
//			

BOOL SlotChange(UINT iDevice,UINT iSlot, LPCSTR szSlot)
{
	// slots 1..1999 are for destination package routing cmds and responses
	bncs_stringlist ssl_Cmd = bncs_stringlist( bncs_string(szSlot).upper(),',' );
	int iGivenRtr=0, iGivenDest=0, iGivenSrc=0, iMonDesk=0;
			
	if ((iDevice == iInfoDevice) && (pRouterVirtual) && (mapOfAllHighways)&&(mapOfAllRouters)) {
		if (iSlot == SLOT_HIGHWAY_PARK_CMD) {
			// PARK Command in format <rtr num>,<dest_index> - will let auto find applicable highway using parameters
			if (ssl_Cmd.count() == 2) {
				// look for <highway_rtr>,<highway_dest>
				iGivenRtr = ssl_Cmd[0].toInt();
				iGivenDest = ssl_Cmd[1].toInt();
				if ((iGivenRtr>0) && (iGivenDest>0)) {
					// park the given highway
					CRouterHighway* pHigh = GetRouterHighwayFromDest(iGivenRtr, iGivenDest);
					if (pHigh)
						RouteParkToHighway(pHigh, TRUE);
					else
						Debug("SlotChange - INVALID PARK - no highway linked to %d - %d", iGivenRtr, iGivenDest);
				}
				else
					Debug("SlotChange - INVALID PARK parameters - %s ", szSlot);
			}
			else
				Debug("SlotChange - INVALID PARK command - %s ", szSlot);
		}
		else if (iSlot == SLOT_HIGHWAY_TEST_CMD) {
			// TEST Command in format<rtr num>,<dest_index> - will let auto find associated highway using these parameters
			// will disable a highway doing this
			if (ssl_Cmd.count() == 2) {
				// look for <highway_rtr>,<highway_dest>
				iGivenRtr = ssl_Cmd[0].toInt();
				iGivenDest = ssl_Cmd[1].toInt();
				if ((iGivenRtr>0) && (iGivenDest>0)) {
					// park the given highway
					CRouterHighway* pHigh = GetRouterHighwayFromDest(iGivenRtr, iGivenDest);
					if (pHigh)
						RouteTestToHighway(pHigh);
					else
						Debug("SlotChange - INVALID TEST - no highway linked to %d - %d", iGivenRtr, iGivenDest);
				}
				else
					Debug("SlotChange - INVALID TEST parameters - %s ", szSlot);
			}
			else
				Debug("SlotChange - INVALID TEST command - %s ", szSlot);
		}
		else if (iSlot == SLOT_VIRTUAL_MESSAGE) {
			eiInfoGrd->updateslot(iSlot, " ");
			pRouterVirtual->setRouterErrorMessage("");
			pRouterVirtual->setRouterAlarmStatus(ALARM_CLEAR);
			UpdateAutoAlarmStateSlot();
		}
		else if ((iSlot>0) && (iSlot <= pRouterVirtual->getMaxDestinations())) {
			ProcessRoutingChangeCommand(iSlot, szSlot);
		}
		// ELSE all other slots in range 4001-4096 are used to provide info on highways etc - and are not under user control, only by automatic
		
	}
	// ELSE trace infodriver -- all slots used by auto - no user slots

	// all return true as under automatic control - no free user slots 
	return TRUE;

}



