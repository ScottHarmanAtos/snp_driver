
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"
#include "tieline_manager_common.h"


class CRouterSrceData
{
public:
	CRouterSrceData( int iSource );
	~CRouterSrceData();

	int m_iMapIndex;
	int m_iAssociated_Type;                        // specific types that require specific highway types to only be routed
	int m_iAssociated_Highway;                 // highway, wrap etc - the "to src"
	bncs_stringlist ssl_RoutedToDests;      // pti of users 

};

