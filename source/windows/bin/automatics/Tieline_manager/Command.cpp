// Command.cpp: implementation of the CCommand class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Command.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCommand::CCommand()
{
	// init vars
	 strcpy( szCommand, "?" );
	 strcpy( szMask, "" );
	 iTypeCommand = UNKNOWNVAL;
	 iWhichDevice = UNKNOWNVAL;
	 iWhichSlotDest = UNKNOWNVAL;
	 iWhichDatabase = UNKNOWNVAL;
	 iWhichSource = UNKNOWNVAL;
	 iMaskValue = UNKNOWNVAL;
}

CCommand::~CCommand()
{
    // fade to black
}
