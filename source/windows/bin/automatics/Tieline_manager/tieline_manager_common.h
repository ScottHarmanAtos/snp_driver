// Include in all files to enable debug and status dialog updates

/* max size of buffer for data from string resource */
#define	MAX_LOADSTRING	100

/* buffer constant for strings */
#define	MAX_BUFFER_STRING 256
 

#define	SOFT_NAME_DB 2              // default values -- 
#define	SOURCE_NAME_DB 4

#define	ROUTERCOMMAND 1
#define	INFODRVCOMMAND 2
#define	GPIOCOMMAND 3
#define	DATABASECOMMAND 4
#define	AUTOINFOREVERTIVE 5

#define	UNKNOWNVAL -1

#define	BNCS_COMMAND_RATE  20 // 100 //30 //5 //10          // number commands sent out per iteration of command timer

// highway type

#define HIGHWAY_TX_TYPE 1		// tx use highways / fixed routes
#define HIGHWAY_LIVE_TYPE 2		// live use highways / fixed routes
#define HIGHWAY_ENG_TYPE 3		// eng use highways / fixed routes
#define HIGHWAY_SUPVR_TYPE 4		// reserved for supervisor dests / fixed routes
#define HIGHWAY_CMVP_TYPE 5		// reserved for monitoring dests / fixed routes
#define HIGHWAY_LMVP_TYPE 6		// reserved for monitoring dests / fixed routes
#define HIGHWAY_POOL_TYPE 7		// general pool highways - none actually used at Discovery
#define HIGHWAY_WRAP_TYPE 8	   // reserved for wraps on same router

#define SDI_STATUS_NONE  0
#define SDI_STATUS_EXPECTED  1
#define SDI_STATUS_UNEXPECTED   2

#define HIGHWAY_UNKNOWN  -1   // special case when rtr revs are missing / zero / -1 
#define HIGHWAY_FREE     0    // parked
#define HIGHWAY_CLEARING 1    // clearing or from hung state
#define HIGHWAY_PENDING  2    // allocated - pending rtr revs
#define HIGHWAY_INUSE    3    // in use - locked, dedicated etc

// status and error message slots 4001-4009
#define SLOTS_FOR_AUTOMATIC_USE       4000

#define SLOT_HIGHWAY_TEST_CMD           4048
#define SLOT_HIGHWAY_PARK_CMD          4049

#define SLOT_HIGHWAY_PROG_USAGE_CMR    4051  // highway usage for rtr 1 ( cmr )
#define SLOT_HIGHWAY_PROG_USAGE_CHAIN  4052  // highway usage for first chain router -- others use 4053 onwards to 4059 ( 8 chain routers )

#define SLOT_OVERALL_ALARM       4070  //  set to 1 for top strap button for alarm to look at
#define SLOT_VIRTUAL_MESSAGE     4071  // general error message slot from auto
#define SLOT_CMR_MESSAGE         4072  // cmr router / highway error message slot   
#define SLOT_CHAIN_MESSAGE       4073  // other chain router / highways error message slot   4073..4080 for 8 chain routers

#define SLOT_ROUTER_ZEROS         4091     // slot holds calc number of 0 / -1 revs for virtual and other routers
#define SLOT_COUNT_UNKNOWN_HWAYS  4092     // slot holds number of highways screwed up by invalid / missing rtr revs
#define SLOT_LIST_UNKNOWN_HWAYS   4093     // slot holds list of highways screwed up by invalid / missing rtr revs up to 220 chars

#define UNLOCKED 0
#define LOCKED 1

#define VIR_GRD_INFO_TYPE          0       // this virtual router -- returns both GRD and INFO revs via main info
#define GRD_REVERTIVE_TYPE         1       // standard GRD  router commands and revertives only
#define INFO_REVERTIVE_TYPE        2       // infodriver revertives for routing only
#define VIDEO_IPATH_REVERTIVE_TYPE 3       // special variant for this IP router driver - routes from slots 1..2000, secondary data from slots 2001+

#define CHAR_DBLE_QUOTES 0x22
#define CHAR_SNGL_QUOTES 0x27

#define ALARM_CLEAR 0
#define ALARM_WARNING 1
#define ALARM_ERROR_STATE 2
#define ALARM_SEVERE_STATE 3

#define ALARM_CLEAR_MESSAGE 1
#define ALARM_SET_WARN_MESSAGE 2
#define ALARM_SET_ERROR_MESSAGE 3
