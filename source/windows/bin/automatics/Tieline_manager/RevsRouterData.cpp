// RevsRouterData.cpp: implementation of the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RevsRouterData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRevsRouterData::CRevsRouterData(int iRouter, int iMaxSrcs, int iMaxDests, int iRtrRevType, int iVirOffset, int iParkSrc, int iTestSrc, int iTXDivision )
{
	m_iRouterIndex = iRouter;
	m_iMaximumDestinations=iMaxDests;
	m_iMaximumSources=iMaxSrcs;
	m_iRouterRevertiveType = iRtrRevType;
	m_iRouterOffsetIntoVirtualGrd = iVirOffset;
	m_iRouterAlarmStatus = ALARM_CLEAR;
	m_ssRouterThresholdMessage = "";
	m_ssRouterErrorMessage = "";
	m_iSlotMessageIndex = 0;
	m_ssRouterLabel = bncs_string(iRouter);
	m_iParkSource = iParkSrc;
	m_iTestSource = iTestSrc;
	m_iTXChain_Division = iTXDivision;

	// create entries in maps for dests and sources
	for (int iS = 0; iS <= m_iMaximumSources; iS++) {
		CRouterSrceData* pData = new CRouterSrceData(iS);
		if (pData) { // add to map
			cl_SrcRouterData.insert(pair<int, CRouterSrceData*>(iS, pData));
		}
	}
	// create entries in maps for dests and sources
	for (int iD = 1; iD <= m_iMaximumDestinations; iD++) {
		CRouterDestData* pData = new CRouterDestData(iD);
		if (pData) { // add to map
			cl_DestRouterData.insert(pair<int, CRouterDestData*>(iD, pData));
		}
	}

}

CRevsRouterData::~CRevsRouterData()
{
	// seek and destroy
	while (cl_DestRouterData.begin() != cl_DestRouterData.end()) {
		map<int, CRouterDestData*>::iterator it = cl_DestRouterData.begin();
		if (it->second) delete it->second;
		cl_DestRouterData.erase(it);
	}
	while (cl_SrcRouterData.begin() != cl_SrcRouterData.end()) {
		map<int, CRouterSrceData*>::iterator it = cl_SrcRouterData.begin();
		if (it->second) delete it->second;
		cl_SrcRouterData.erase(it);
	}
}



////////////////////////////////////////////////////
// private internal functions for src/dest maps 
CRouterSrceData* CRevsRouterData::GetSourceData(int iSource)
{
	if ((iSource>=0) && (iSource <= m_iMaximumSources)) {
		map<int, CRouterSrceData*>::iterator it = cl_SrcRouterData.find(iSource);
		if (it != cl_SrcRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

CRouterDestData* CRevsRouterData::GetDestinationData(int iDest)
{
	if ((iDest>0) && (iDest <= m_iMaximumDestinations)) {
		map<int, CRouterDestData*>::iterator it = cl_DestRouterData.find(iDest);
		if (it != cl_DestRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////

BOOL CRevsRouterData::storeGRDRevertive( int iDestination, int iSource, bncs_string ssSource )
{
  // store given source from actual hardware response; any re-enterant src/dest calc then done  if required
	if (iSource>=0) {
		if ((iDestination>0) && (iDestination<=m_iMaximumDestinations)) {
			// get from map
			CRouterDestData* pData = GetDestinationData(iDestination);
			if (pData) {
				// sort out old routed src - if different
				if (iSource != pData->m_iRoutedSource) {
					// tidy up where this old src users list, remove from prev src pti list
					pData->m_iPreviousRoutedSource = pData->m_iRoutedSource;
					pData->m_ssPreviousSource = pData->m_ssRoutedSource;
					//
					if ((pData->m_iPreviousRoutedSource>0) && (pData->m_iPreviousRoutedSource<=m_iMaximumSources)) {
						CRouterSrceData* pPrev = GetSourceData(pData->m_iPreviousRoutedSource);
						if (pPrev) {
							int indx = pPrev->ssl_RoutedToDests.find(iDestination);
							if (indx >= 0) pPrev->ssl_RoutedToDests.deleteItem(indx);
						}
					}
				}
				pData->m_iRoutedSource = iSource;
				pData->m_ssRoutedSource = ssSource;
				// add to new src pti
				if ((iSource>0) && (iSource <=m_iMaximumSources)) {
					CRouterSrceData* pSource = GetSourceData(iSource);
					if (pSource) {
						if (pSource->ssl_RoutedToDests.find(iDestination) < 0)
							pSource->ssl_RoutedToDests.append(iDestination);
					}
				}
			}
			return TRUE;
		}
		else {
			OutputDebugString( "tieline_manager::storeGRDRevertive negative destination or greater than maxUsed \n" );
			return FALSE;
		}
	}
	else {
		OutputDebugString( "tieline_manager::storeGRDRevertive negative or zero source \n" );
		return FALSE;
	}
}


void CRevsRouterData::storeAncillaryIPDataString(int iDestination, bncs_string ssData )
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_ssAncillaryData = ssData;
		}
	}
}


void CRevsRouterData::storePendingSource( int iDestination, int iPendSrce )
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iPendingSource = iPendSrce;
		}
	}
}


BOOL CRevsRouterData::storeGRDLockState( int iDestination, int iState )
{
    // store given source from actual rev; any re-enterant src/dest calc then done  if required
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iDestLockState = iState;
			return TRUE;
		}
	}
	return FALSE;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


int CRevsRouterData::getRoutedSourceForDestination( int iDestination )
{
  // return revertive source as  got from grd rev
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iRoutedSource;
	}
	return UNKNOWNVAL;
}


int CRevsRouterData::getPendingSource( int iDestination )
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) 	return pData->m_iPendingSource;
	}
	return 0;
}

int CRevsRouterData::getGRDLockState( int iDestination )
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iDestLockState;
	}
	return UNKNOWNVAL;
}


int CRevsRouterData::getRouterIndex()
{
	return m_iRouterIndex;
}

int CRevsRouterData::getMaxDestinations()
{
	return m_iMaximumDestinations;
}

int CRevsRouterData::getMaxSources()
{
	return m_iMaximumSources;
}

int CRevsRouterData::getRouterRevertiveType()
{
	return m_iRouterRevertiveType;
}

int CRevsRouterData::getRouterOffsetIntoVirtual()
{
	return m_iRouterOffsetIntoVirtualGrd;
}

int CRevsRouterData::getTXRouterDivision(void)
{
	return m_iTXChain_Division;
}

bncs_string CRevsRouterData::getAncillaryIPDataString(int iDestination )
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_ssAncillaryData;
	}
	return "";	
}

void CRevsRouterData::setHighwayAssociatedtoDest( int iDestination, int iHighwayIndex )
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) pData->m_iAssociated_Highway = iHighwayIndex;
	}
}


void CRevsRouterData::setHighwayAssociatedtoSource( int iSource, int iHighwayIndex )
{
	if ((iSource>0) && (iSource <= m_iMaximumSources)) {
		CRouterSrceData* pSrce = GetSourceData(iSource);
		if (pSrce) pSrce->m_iAssociated_Highway = iHighwayIndex;
	}
}


int CRevsRouterData::getHighwayAssociatedtoDest( int iDestination )
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iAssociated_Highway;
	}
	return UNKNOWNVAL;	
}


int CRevsRouterData::getHighwayAssociatedtoSource( int iSource )
{
	if ((iSource>0) && (iSource <= m_iMaximumSources)) {
		CRouterSrceData* pSrce = GetSourceData(iSource);
		if (pSrce) return pSrce->m_iAssociated_Highway;
	}
	return UNKNOWNVAL;
}


bncs_string CRevsRouterData::getListOfDestsUsingSource( int iSource )
{
	if ((iSource>0) && (iSource <= m_iMaximumSources)) {
		CRouterSrceData* pSrce = GetSourceData(iSource);
		if (pSrce) return pSrce->ssl_RoutedToDests.toString('|');
	}
	return bncs_string("");
}


int CRevsRouterData::getNumberOfDestsUsingSource( int iSource )
{
	if ((iSource>0) && (iSource <= m_iMaximumSources)) {
		CRouterSrceData* pSrce = GetSourceData(iSource);
		if (pSrce) return pSrce->ssl_RoutedToDests.count();
	}
	return UNKNOWNVAL;
}

void CRevsRouterData::setSourcePti(int iSource, bncs_string ssPti)
{
	// sets stringlist of ssl_RoutedToDests -- for highways sources in VIRTUAL GRD only as these not stored via normal way of revs
	// DO NOT use this for physical router records as it does routed dests list correctly from revs.
	if ((iSource>0) && (iSource<=m_iMaximumSources)) {
		CRouterSrceData* pSrce = GetSourceData(iSource);
		if (pSrce) pSrce->ssl_RoutedToDests = bncs_stringlist(ssPti, '|');
	}
}


void  CRevsRouterData::setDestinationTrace(int iDestination, bncs_string ssTrace, int iHWTypeUsed )
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->ssl_Trace = bncs_stringlist(ssTrace, '|');
			pData->m_HWTypeUsedInRoute = iHWTypeUsed;
		}
	}
}

bncs_string  CRevsRouterData::getDestinationTrace(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->ssl_Trace.toString('|');
	}
	return  bncs_string("");
}


int CRevsRouterData::getDestinationHWTypeUsed(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaximumDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_HWTypeUsedInRoute;
	}
	return  0;
}

void CRevsRouterData::setRouterErrorMessage(bncs_string ssMsg)
{
	m_ssRouterErrorMessage = ssMsg;
}

void CRevsRouterData::setRouterThresholdMessage(bncs_string ssMsg)
{
	m_ssRouterThresholdMessage = ssMsg;
}

void CRevsRouterData::setRouterAlarmStatus(int iState)
{
	m_iRouterAlarmStatus = iState;
}

int CRevsRouterData::getRouterAlarmStatus(void)
{
	return m_iRouterAlarmStatus;
}

bncs_string CRevsRouterData::getRouterErrorMessage(void)
{
	return m_ssRouterErrorMessage;
}

bncs_string CRevsRouterData::getRouterThresholdMessage(void)
{
	return m_ssRouterThresholdMessage;
}

void CRevsRouterData::setSlotMessageIndex(int iSlotToUse)
{
	m_iSlotMessageIndex = iSlotToUse;
}


int CRevsRouterData::getSlotMessageIndex()
{
	return m_iSlotMessageIndex;
}

void CRevsRouterData::setRouterLabel(bncs_string ssLabel)
{
	m_ssRouterLabel = ssLabel;
}

bncs_string CRevsRouterData::getRouterLabel()
{
	return m_ssRouterLabel;
}


int CRevsRouterData::getParkSource()
{
	return m_iParkSource;
}

int CRevsRouterData::getTestSource()
{
	return m_iTestSource;
}




