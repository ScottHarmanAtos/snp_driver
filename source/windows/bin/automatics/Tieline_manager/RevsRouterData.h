// RevsRouterData.h: interface for the CRevsRouterData class.
//
//   HARDWARE RESPONSES STORED HERE  for routes and lock state
//
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
#define AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "tieline_manager_common.h"

#include "RouterSrceData.h"	
#include "RouterDestData.h"	

class CRevsRouterData  
{
private:

	int m_iRouterIndex;
	int m_iMaximumSources;
	int m_iMaximumDestinations;         // size of router
	int m_iRouterOffsetIntoVirtualGrd;   // offset into vir router for start of this router

	int m_iRouterRevertiveType;         // 0=GRD, 1=Infodriver, 2=Video IPath special

	int m_iSlotMessageIndex;
	int m_iRouterAlarmStatus;
	bncs_string m_ssRouterErrorMessage;
	bncs_string m_ssRouterThresholdMessage;

	int m_iTXChain_Division;   // tx routers are 144 sq, two halves of 72 for two TX chain  eg  1-72 tx11,  73-144 tx12
	int m_iParkSource;
	int m_iTestSource;

	bncs_stringlist ssl_PendingRouteProcessing;
	int iPendingCounter;

	bncs_string m_ssRouterLabel;  // 3 or 4 letter mnenomic for gui use

	map<int, CRouterDestData*> cl_DestRouterData;   // map for Destination data
	map<int, CRouterSrceData*> cl_SrcRouterData;    // map for source data

	//router data maps are not seen outside class 
	CRouterSrceData* GetSourceData(int iSource);
	CRouterDestData* GetDestinationData(int iDestination);


public:
	CRevsRouterData( int iRouter, int iMaxSrcs, int iMaxDests, int iRtrRevType, int iVirOffset, int iParkSrc, int iTestSrc, int iTxDivider );
	virtual ~CRevsRouterData( void );

	// router revertives 

	BOOL storeGRDRevertive( int iDestination, int iSource, bncs_string ssSource );  // store source from bncs grd response and/or infodriver response; 
	void storeAncillaryIPDataString( int iDestination, bncs_string ssData );             // store ancillary data for IP etc routing   
	
	int getRoutedSourceForDestination( int iDestination );  
	bncs_string getStringSourceForDestination( int iDestination );
	bncs_string getAncillaryIPDataString( int iDestination );

	void storePendingSource( int iDestination, int iPendSrce );
	int getPendingSource( int iDestination );
	
	BOOL storeGRDLockState( int iDestination, int iState );   // store lock from bcns grd response;
	int getGRDLockState( int iDestination );                 

	int getRouterIndex();
	int getMaxDestinations();
	int getMaxSources();

	int getRouterRevertiveType( void );

	int getRouterOffsetIntoVirtual(void);

	int getTXRouterDivision(void);
	
	void setHighwayAssociatedtoDest( int iDestination, int iHighwayIndex );
	void setHighwayAssociatedtoSource( int iSource, int iHighwayIndex );

	int getHighwayAssociatedtoDest( int iDestination );
	int getHighwayAssociatedtoSource( int iSource );

	bncs_string getListOfDestsUsingSource( int iSource );
	int getNumberOfDestsUsingSource( int iSource );

	// sets stringlist of ssl_RoutedToDests -- for highways sources in VIRTUAL GRD only as these not stored via normal way of revs
	// DO NOT use this for physical router records as it does routed dests list correctly from revs.
	void setSourcePti(int iSource, bncs_string ssPti);  // only to be used to update virtual grd  highway/wrap sources - special case

	void setLinkedToSources( int iMasterSrc, bncs_string sstr );
	void setLinkedToDestinations( int iMasterDest, bncs_string sstr );
	
	bncs_string getLinkedToSources( int iSrc );
	int IsLinkedToSources( int iSrc );

	bncs_string getLinkedToDestinations( int iDest );
	int IsLinkedToDestinations( int iDest );
	
	void setDestinationTrace(int iDestination, bncs_string ssTrace, int iHWTypeUsed );
	bncs_string getDestinationTrace(int iDestination);
	int getDestinationHWTypeUsed(int iDestination);

	void setRouterAlarmStatus(int iState);
	void setRouterErrorMessage(bncs_string ssMsg);
	void setRouterThresholdMessage(bncs_string ssMsg);

	int getRouterAlarmStatus(void);
	bncs_string getRouterErrorMessage(void);
	bncs_string getRouterThresholdMessage(void);

	void setSlotMessageIndex(int iSlotToUse);
	int getSlotMessageIndex();

	void setRouterLabel(bncs_string ssLabel);
	bncs_string getRouterLabel();

	int getParkSource();
	int getTestSource();

};

#endif // !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
