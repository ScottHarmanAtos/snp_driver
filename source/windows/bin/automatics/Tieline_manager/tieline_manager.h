//////////////////////////////////////
// header file for tieline_manager project //
//////////////////////////////////////

#if !defined(AFX_RTR_HIGHWAY_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
#define AFX_RTR_HIGHWAY_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include "resource.h"
#include "tieline_manager_common.h"

// TODO:	Insert additional constants, function prototypes etc. in this file
/* max number of items in listbox */
#define		MAX_LB_ITEMS	2000

#define		APP_WIDTH	800
#define		APP_HEIGHT	720

#define		INFO_DISCONNECTED	   0		// infodriver not there or whatever
#define		INFO_RXMODE            1		// rx mode only - dual redundancy
#define		INFO_TXRXMODE          2		// connected, and in true rxtx mode

// defined colours used in app
#define  COL_BLACK  RGB(0,0,0)
#define  COL_WHITE RGB(255,255,255)

#define  COL_DK_RED RGB(127,0,0)
#define  COL_DK_GRN RGB(0,127,0)
#define  COL_DK_BLUE RGB(0,0,80)
#define  COL_DK_ORANGE RGB(128,64,0)

#define  COL_LT_RED RGB(255,0,0)
#define  COL_LT_GRN RGB(0,255,0)
#define  COL_LT_BLUE RGB(100,100,255)
#define  COL_LT_ORANGE RGB(255,128,0)

#define  COL_YELLOW RGB(255,255,0)
#define  COL_TURQ RGB(180,180,210)      // not really turquoise - but is the background colour for app


#define STARTUP_TIMER_ID	    1							// 
#define FIRSTPASS_TIMER_ID      2
#define ONE_SECOND_TIMER_ID		3	 // could be used to relook for comms port / infodrivers -- for more intelligent driver
#define COMMAND_TIMER_ID		4							// 
#define DATABASE_TIMER_ID	    5							// 
#define RESPONSE_TIMER_ID       7 
#define ROUTERPOLL_TIMER_ID     8
#define FORCETXRX_TIMER_ID      9					// 
#define LISTBOX_TIMER_ID       11

#define ONE_SECOND_TIMER_INTERVAL	 1000 // 1.0 sec timer used for housekeeping - mainly for re connects to info driver, comport connection failures, device interrogation 
#define COMMAND_TIMER_INTERVAL		  100 // 0.1 sec interval on commands being sent from the fifobuffer 
#define STARTUP_TIMER_INTERVAL		 1000 // 
#define TENSECONDS					   10  
#define SIXTYSECONDS				   60



/* main.cpp */
/* prototypes for main functionality */
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT				InitApp(HWND);
void				CloseApp(HWND);

void				LoadIni(void);
char *				getBNCS_File_Path( void );    // new for v4.2 of wizard - assigns the global szBNCS_File_Path
char*				r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite);
void				w_p(char* file, char* section, char* entry, char* setting);
int					getWS(void);

void				Debug(LPCSTR szFmt, ...);
void				Log(LPCSTR szFmt, ...);

int				SplitString(char *string, char *delim, UINT count, char **outarray );
void          InitStatusListboxDialog( void );  // helper function to initialise listbox on app dialog

/*function to create nice new paint brushes*/
void CreateBrushes();
/*function to clean paint brushes and put them away*/
void ReleaseBrushes();

void VersionInfo();

/* prototypes for the notification functions */
void UpdateCounters(void);
LRESULT CSIDriverNotify(extdriver* ped,LPCSTR szMsg);
LRESULT CSIClientNotify(extclient* pec,LPCSTR szMsg);
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot);

void ForceAutoIntoTXRX(void);
void ForceAutoIntoRXONLY(void);
void CheckAutoResilienceState(void);

// TODO: add prototypes for functions from tieline_manager_x.cpp

void ProcessNextCommand( int iRateRevs );
void ProcessAnyMoreResponses( int iRateRevs );
void ProcessNextIncomingCommand( void );
void ClearCommandQueue( void );
void AddCommandToQue( LPCSTR szCmd, int iCmdType, int iDev, int iIndex, int iDB );

BOOL LoadAllInitialisationData( void );
void AutoRegistrations( void );
void PollPhysicalRouters( int iCounter );
void CheckRevertivesReceivedOK( BOOL bRepollRouter );
void CheckAndRecalculateVirtualDests( void );

void ProcessVirtualRoutesOnStartup( void );
void ProcessHighwayManagement( void );
void DumpAllTallies( void );

void PostAutomaticErrorMessage(int iFromRouter, int iMessageType, bncs_string ssMessage);

BOOL SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight);

// routers
CRevsRouterData* GetValidRouter(int iIndex);


// highways
CRouterHighway* GetRouterHighway( int iIndex );
CRouterHighway* GetRouterHighwayFromDest( int iFromRtr, int iFromDest );
CRouterHighway* GetRouterHighwayToSource( int iToRtr, int iToSrc );

void LoadSourcesListbox();
void LoadDestinationsListbox();

void ProcessVirtualRouterCrosspoint( int iSource, int iDestination, int iHighwayType );
void ProcessVirtualRouterPoll( int iStartDestination, int iEndDestination, int iFromWS );

BOOL ProcessPhysicalRouterRevertive(int iActualRtr, int iActualDest, int iNewSource);

void DisplayHighwayData( int iHighway );
int CalculateHighwayUsed( void );
void TrimAndUpdatePti(int iSlotIndx, bncs_string ssPti);
void ProcessPtiUpdate();

void RouteParkToHighway(CRouterHighway* pHigh, BOOL bCalculate);
void RouteTestToHighway( CRouterHighway* pHigh );

void StripQuotes(LPSTR szStr);
void StripOutQuotes(LPSTR szStr, char iChar);


#ifndef EXT
#define EXT extern
#endif

/////////////////////////////////////////////////////////////////////
// Global Variables for the tieline_manager project
/////////////////////////////////////////////////////////////////////

EXT HINSTANCE hInst;						// current instance
EXT TCHAR szTitle[MAX_LOADSTRING];			// The title bar text
EXT TCHAR szWindowClass[MAX_LOADSTRING];	// The title bar text
EXT char szBNCS_File_Path[MAX_BUFFER_STRING]; // global for Colledia system file path - assigned in getBNCS_File_Path, used in r_p and w_p
EXT char szBNCS_Log_File_Path[MAX_BUFFER_STRING];  // global for crash log file path - assigned in getBNCS_File_Path
EXT char szAppName[ MAX_LOADSTRING];                     // application name used in crash log messages
EXT char szResult[MAX_BUFFER_STRING];						// global for profile reads
EXT BOOL fLog;								// log enable flag
EXT int iDebug;							// debug enable flag
EXT BOOL fDebugHide;						// debug visible flag
EXT HWND hWndMain,hWndList;					// window handles, main and debug
EXT HWND hWndDlg;							// window handle for child dialog
EXT int iChildHeight;						// height of child dialog
EXT RECT rc;
EXT int iGRDDevice;							// command line parameter =  virtual grd driver #
EXT int iInfoDevice;						// external infodriver # - read from dev ini file ( recommended to be iDevice+1 )
EXT int iInfoTracePti;						    // external infodriver # - read from dev ini file for trace infodiver ( recommended to be iDevice+2 )
EXT int iWorkstation;						// workstation number from CSI.INI
EXT ULONG lTXID;							// ID tx count
EXT ULONG lRXID;							// ID rx count


// CLASSES used by auto
EXT extdriver *edMainGRD;				// a driver connection class - will be virtual grd # == iGRDDevice
EXT extinfo   *eiInfoGrd;		    // an infodriver class --- used for src /dest information, or for GRD as "info" grd interface like hydra  - does trace too
EXT extinfo   *eiInfoTracePti;				// an infodriver class --- used for source pti--  trace in infogrd now

EXT extclient *ecCSIClient;				// a client connection class  -- for processing revertives from controlled actual router drivers

EXT queue<CCommand*> qCommands;                  // queue of outgoing commands to go to bncs drivers, routers, other devices  from auto
EXT queue<CCommand*> qIncoming;                  // queue of INCOMING commands for auto to respond to - as per spec / design
EXT queue<CCommand*> qOutgoing;                  // queue of outgoing revertives when auto responds to router polls

// NOTE --- REMEMBER IN THIS AUTO --- Virtual Router is the BNCS "combined" presentation of ALL physical routers

EXT CRevsRouterData* pRouterVirtual;               //  main virtual router  -- may one day be in map of all but separate for now to minimize changes

// all routers making up virtual grd
EXT bncs_stringlist ssl_AllRoutersIndices;        
EXT map<int, CRevsRouterData*>*  mapOfAllRouters;  // map of all the physical routers - rtr device index is key

// all highways to from all defined routers 
EXT map<int, CRouterHighway*>*  mapOfAllHighways;  // map of all the highways and wraps 

EXT bncs_string ss_AutoId;

EXT int iLabelAutoStatus; // used to distinguish diff cols for label
EXT int iLabelCSIStatus; // used to distinguish diff cols for label
EXT int iLabelInfoStatus;  // used to distinguish diff cols for label
EXT int iLabelTraceStatus;  // used to distinguish diff cols for label
EXT int iOverallTXRXModeStatus;			// overall mode -- based on base infodriver i.e gpi input driver
EXT int iOverallStateJustChanged, iStartupCounter;
EXT int iOverallModeChangedOver;
EXT int iOverallAutoAlarmState;

EXT BOOL bConnectedToInfoDriverOK;	// connected to the infodriver or not
EXT BOOL bAutomaticStarting;
EXT BOOL bForceUMDUpdate; 
EXT BOOL bShowAllDebugMessages;
EXT BOOL bShowingErrorMessage;
EXT BOOL bAbletoProcessIncomingCommand;   // flag to control in incoming command can be processed or needs to be queued

EXT int iOneSecondPollTimerId;
EXT BOOL bOneSecondPollTimerRunning;
EXT int iNextResilienceProcessing;  // count down to zero -- check info tx/rx status 
EXT int iNextManagementProcessing;  
EXT int iNextDestinationCheck;

EXT int iNextCommandTimerId;
EXT BOOL bNextCommandTimerRunning;
EXT BOOL bRMDatabaseTimerRunning;

EXT int iOutgoingResponseTimerId;
EXT BOOL bOutgoingTimerRunning;

// virtual grd gui
EXT int iChosenSource, iChosenDestination;
EXT int iChosenDest_Source; // src to chosen dest
EXT int iChosenHwayType;
EXT int iChosenHighway;
EXT int iTotalHighwaysAndWraps;

EXT int iNumberLoadedHighways;
EXT int iNumberLoadedWraps;

// this vir rtr
EXT int i_vir_rtr, i_max_vir_rtr_Srcs, i_max_vir_rtr_Dests, i_zero_virtuals;

EXT int i_disabled_highways;   // totals
EXT int i_unknown_highways;

// globals used in recurrsive functions
EXT int iRecursiveCounter; // used in recursive loops to (try) and limit recursion in event of routing loops
EXT bncs_stringlist ssl_CalcListOfDestinationsUsers;        // global list used in recurrsive processing of rtr revertives
EXT bncs_stringlist ssl_CalcListOfChangedPtiSources;        // global list used inprocessing of rtr revertives

EXT bncs_string ssInfodriverRoutePrefix;   // loaded from xml - optional e.g.  index= to be added to infodriver routing command string

EXT int iUserDefinedMngmntProc;
EXT int iConfigurationPending;      // config value for setting Highways Pending counter of Ipath routing

// rm cmds - store db,index for src / dest RM changes
EXT bncs_stringlist ssl_RMChanges;

// for v large routers - it can take long time to load list box
// which locks up driver for many seconds - which is unacceptable 
// so load src /dest list box in phases on timer, 
EXT int iLoadSrcListBoxEntry;    // current src loading at entry ( in blocks of 512 )
EXT int iLoadDestListBoxEntry;

EXT BOOL bAutoParkHighways;    // xxx not used in this version of auto

///////////////////////////////////////////////////////////////////

EXT	HBRUSH hbrWindowsGrey;
EXT	HBRUSH hbrBlack;
EXT	HBRUSH hbrWhite;

EXT	HBRUSH hbrDarkRed;
EXT	HBRUSH hbrDarkGreen;
EXT	HBRUSH hbrDarkBlue;
EXT	HBRUSH hbrDarkOrange;

EXT	HBRUSH hbrYellow;
EXT	HBRUSH hbrTurq;

EXT	HBRUSH hbrLightRed;
EXT	HBRUSH hbrLightGreen;
EXT	HBRUSH hbrLightOrange;
EXT	HBRUSH hbrLightBlue;

EXT HBRUSH HBr, HBrOld;

EXT COLORREF iCurrentTextColour;
EXT COLORREF iCurrentBackgroundColour;
EXT HBRUSH iCurrentBrush;


#endif // !defined(AFX_RTR_HIGHWAY_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
