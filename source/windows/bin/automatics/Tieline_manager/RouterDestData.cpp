#include "RouterDestData.h"


CRouterDestData::CRouterDestData( int iDest)
{
	m_iMapIndex = iDest;
	m_iRoutedSource = 0;
	m_iPendingSource = 0;
	m_iPreviousRoutedSource = 0;
	m_ssRoutedSource = bncs_string("0");
	m_ssPreviousSource = bncs_string("0");
	m_iDestLockState = UNLOCKED;
	m_iAssociated_Highway = 0;
	m_ssAncillaryData = "";
	ssl_Trace = bncs_stringlist("", '|');
	m_HWTypeUsedInRoute = 0;
}


CRouterDestData::~CRouterDestData()
{
}
