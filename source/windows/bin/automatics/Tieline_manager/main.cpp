///////////////////////////////////////////////
//	main.cpp - core code for the application //
///////////////////////////////////////////////

/************************************
  REVISION LOG ENTRY
  Revision By: Chris Gil
  Revised on 23/07/2002 19:40:00
  Version: V
  Comments: Added SNMP 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 18/07/2001 11:29:51
  Version: V
  Comments: Modified extinfo, extclient, extdriver, to have connect() function so
			all constructors take no parameters
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 28/03/2001 14:42:29
  Version: V
  Comments: new comms class specifies the serial port number in the open() function
  now, rather than the constructor
 ************************************/


/************************************
  REVISION LOG ENTRY
  Revision By: PaulW
  Revised on 20/09/2006 12:12:12
  Version: V4.2 
  Comments: r_p and w_p -- will look first for 4.5 CC_ROOT and SYSTEM,   
                        then  ConfigPath defined for V3 systems 
						or as a last resort the default Windows/Winnt directory for dev ini files etc 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: RichardK
  Revised on 03/08/2007
  Version: V4.3
  Comments: Debug turns off redraw of list-box while updating it.  To stop flicker problems seen on
		systems accessed using Remote Desktop.
 ************************************/


#include "stdafx.h"
#define EXT
#include "tieline_manager.h"

//
//  FUNCTION: WinMain()
//
//  PURPOSE: 
//
//  COMMENTS: main entry point for BNCS Driver "tieline_manager"
//
//
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;
	char acTemp[256] = "", szLeft[256] = "", szRight[256] = "";
	char szDebug[256] = "";

	lstrcpyn(acTemp, lpCmdLine, 255);

	// deterime system type
	getBNCS_File_Path();

	// was iDevice = atoi(acTemp);
	iGRDDevice = atoi(acTemp);
	if (iGRDDevice>0) {
		iInfoDevice = iGRDDevice + 1;
		iInfoTracePti = iGRDDevice + 2;
		wsprintf(szDebug, "TIELN_MGR -winmain- integer grd device %d \n", iGRDDevice);
		OutputDebugString(szDebug);
	}
	else {
		// is passed in parameter an instance name id- 
		// this check is done as windows shortcuts sometimes pass in whole target path and parameters sometimes
		char *pdest = strstr(acTemp, " ");
		if (pdest != NULL) {
			// space present - as string may include full path to driver
			if (SplitAString(acTemp, ' ', szLeft, szRight)) strcpy(acTemp, szRight);
		}
		// now call xml parser to extract device for instance name
		iGRDDevice = getInstanceDevice(acTemp);
		iInfoDevice = iGRDDevice + 1;
		iInfoTracePti = iGRDDevice + 2;
		wsprintf(szDebug, "TIELN_MGR -winmain- instance %s yielded grd device %d info %d pti %d \n", acTemp, iGRDDevice, iInfoDevice, iInfoTracePti );
		OutputDebugString(szDebug);
	}

	if (iGRDDevice < 1) {
		// if still 0 then get hardcoded instance name
		iGRDDevice = getInstanceDevice("tieline_manager_grd");
		iInfoDevice = iGRDDevice + 1;
		iInfoTracePti = iGRDDevice + 2;
		wsprintf(szDebug, "TIELN_MGR -winmain- instance %s yielded grd device %d info %d pti %d \n", acTemp, iGRDDevice, iInfoDevice, iInfoTracePti);
		OutputDebugString(szDebug);
	}

	// Initialize global strings
	LoadString(hInstance, IDS_SECTION, szAppName, MAX_LOADSTRING);
	LoadString(hInstance, IDS_APP_TITLE, acTemp, MAX_LOADSTRING);
	wsprintf(szTitle, acTemp, iGRDDevice);
	LoadString(hInstance, IDC_APPCLASS, szWindowClass, MAX_LOADSTRING);

	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

		hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_RTR_HIGHWAY);
		
		// Main message loop:
		while (GetMessage(&msg, NULL, 0, 0)) 
		{
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

	return msg.wParam;

}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_REG);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCSTR)IDC_RTR_HIGHWAY;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hWndMain = CreateWindow(szWindowClass, szTitle, WS_SYSMENU|WS_MINIMIZEBOX,
		CW_USEDEFAULT, 0, APP_WIDTH, APP_HEIGHT, NULL, NULL, hInstance, NULL);

	if (!hWndMain)
	{
		return FALSE;
	}

//#ifdef _DEBUG
	ShowWindow(hWndMain,nCmdShow);
//#else
//	ShowWindow(hWndMain, SW_MINIMIZE);
//#endif

	UpdateWindow(hWndMain);

	return TRUE;
}




///////////////////////////////////////////////////////////////////////////////
// generic method to split string into two halves around a specified character
//
BOOL SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight)
{
	int result = 0;
	char szInStr[MAX_BUFFER_STRING] = "";
	strcpy(szInStr, szStr);
	strcpy(szLeft, "");
	strcpy(szRight, "");

	char *pdest = strchr(szInStr, cSplit);
	if (pdest != NULL) {
		result = pdest - szInStr + 1;
		if (result>0) {
			if (result == strlen(szInStr)) {
				strcpy(szLeft, szInStr);
			}
			else {
				strncpy(szLeft, szInStr, result - 1);
				szLeft[result - 1] = 0; // terminate
				strncpy(szRight, szInStr + result, strlen(szInStr) - result + 1);
			}
			return TRUE;
		}
		else {
			Debug("SplitAString pdest not null, BUT result 0 or less : %d", result);
		}
	}
	// error condition
	strcpy(szLeft, szInStr);
	return FALSE;
}




//
//  FUNCTION: AssertDebugSettings(HWND)
//
//  PURPOSE:  Sorts out window size and ini file settings for the two debug
//            controls - debug and hide.
//  
//	COMMENTS:
//	
void AssertDebugSettings(HWND hWnd)
{
	char szFileName[256];
	char szSection [MAX_LOADSTRING];
	LoadString(hInst, IDS_SECTION, szSection, MAX_LOADSTRING);
	sprintf(szFileName, "dev_%03d.ini", iGRDDevice);

	if (iDebug)
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_ON,MF_CHECKED);
		//w_p(szFileName, szSection, "DebugMode", "1");
	} 
	else
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_ON,MF_UNCHECKED);
		//w_p(szFileName, szSection, "DebugMode", "0");
	}

	RECT rctWindow;
	GetWindowRect(hWnd, &rctWindow);
	if (fDebugHide)
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, iChildHeight + 42, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_HIDE,MF_CHECKED);
	} 
	else
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, APP_HEIGHT, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_HIDE,MF_UNCHECKED);
	}
}

//
// FUNCTION CreateLogDirectory
//
// new function Version 4.2 of the Wizard - to create log file directory according to Colledia system type
//
BOOL CreateDebugFileDirectory ( void )
{
	static bool bFirst = true;
	
	//create the log directory the first time
	if (bFirst) {
		_mkdir(szBNCS_Log_File_Path);	
		bFirst = false;
		return TRUE;
	}
	return FALSE;
	
}


void Restart_CSI_NI_NO_Messages( void )
{
	iOverallModeChangedOver=0;
	if (bShowAllDebugMessages) Debug( "RestartCSI_NI-NO messages called");
	if (iOverallTXRXModeStatus == IFMODE_TXRX) {
		if (eiInfoGrd) eiInfoGrd->requestmode = IFMODE_TXRX;
		if (eiInfoTracePti) eiInfoTracePti->requestmode = IFMODE_TXRX;
	}
	else {
		if (eiInfoGrd) eiInfoGrd->requestmode = IFMODE_TXRXINQ;
		if (eiInfoTracePti) eiInfoTracePti->requestmode = IFMODE_TXRXINQ;
	}
}


void ForceAutoIntoTXRX( )
{
	iNextResilienceProcessing = 3;
	iOverallModeChangedOver = 2;
	//
	SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Forcing TX/RX");
	//
	if (edMainGRD) {
		//edMainGRD->setstate(TO_TXRX);
		edMainGRD->setstate(QUERY_TXRX);
		edMainGRD->setifmode(INFO_TXRXMODE);
	}
	//
	if (eiInfoGrd) {
		eiInfoGrd->setmode(INFO_TXRXMODE);
		eiInfoGrd->requestmode = TO_TXRX;
		SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS, "Force TX/RX");
	}
	if (eiInfoTracePti) {
		eiInfoTracePti->setmode(INFO_TXRXMODE);
		eiInfoTracePti->requestmode = TO_TXRX;
		SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS2, "Force TX/RX");
	}
}



void ForceAutoIntoRXONLY( )
{
	iNextResilienceProcessing = 3;
	iOverallModeChangedOver = 2;
	SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Forcing RXONLY");
	//
	if (edMainGRD) {
		edMainGRD->setstate(TO_RXONLY);
		edMainGRD->setifmode(INFO_RXMODE);
	}
	//
	if (eiInfoGrd) {
		eiInfoGrd->setmode(INFO_RXMODE);
		eiInfoGrd->requestmode = TO_RXONLY;
		SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS, "Force RX");
	}
	if (eiInfoTracePti) {
		eiInfoTracePti->setmode(INFO_RXMODE);
		eiInfoTracePti->requestmode = TO_RXONLY;
		SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS2, "Force RX");
	}
}



void WorkThruAndProcessAnyPendingRoutes()
{
	// XXXX WorkThruAndProcessAnyPendingRoutes to do
	//if (iEmeraldPendingCounter > 0) {
	//	iEmeraldPendingCounter--;
	//	if (iEmeraldPendingCounter <= 0) {
	//		if (ssl_Emerald_PendingRouteProcessing.count() > 0) {
	//			for (int ic = 0; ic<ssl_Emerald_PendingRouteProcessing.count(); ic++) {
	//				ProcessPhysicalRouterRevertive(pRouterEmerald->getRouterIndex(), ssl_Emerald_PendingRouteProcessing[ic].toInt(), 0);
	//			}
	//			ssl_Emerald_PendingRouteProcessing.clear();
	//		}
	//		iEmeraldPendingCounter = 0;
	//	}
	//}

}



void ProcessFirstPass( void )
{
	// set allocated source for each highway based on physical revs
	int i_unknown_highways=0;
	for (int iH=1;iH<=iTotalHighwaysAndWraps;iH++) {
		CRouterHighway* pHigh = GetRouterHighway( iH );
		if (pHigh) {
			int iPhySrc=0, iVirSrc=0;
			// set the allocated source for highway from revs
			CRevsRouterData* pFromRtr = GetValidRouter(pHigh->getFromRouter());
			if (pFromRtr) {
				iPhySrc = pFromRtr->getRoutedSourceForDestination(pHigh->getFromDestination());
				pHigh->assignAllocatedHighwaySource(iPhySrc);
				if (iPhySrc < 0) {
					Debug("ProcessFirstPass -ERROR- 0 src for highway %d rtr %d dest %d -UNKNOWN STATE-",
						pHigh->getHighwayIndex(), pHigh->getFromRouter(), pHigh->getFromDestination());
					pHigh->setHighwayState(HIGHWAY_UNKNOWN, 0, 0);
					i_unknown_highways++;
				}

				// sort pti for virtual grd sources of highways as wraps xxx sort in first pass
				if (pHigh->getToSource() > 0) {
				//	int iOffset = iOrangeRouter_Offset;
				//	bncs_stringlist ssl = bncs_stringlist(pRouterOrange->getListOfDestsUsingSource(pHigh->getToSource()), '|');
				//	if (pHigh->getToRouter() == pRouterEmerald->getRouterIndex())  {
				//		iOffset = iEmeraldRouter_Offset;
				//		ssl = bncs_stringlist(pRouterEmerald->getListOfDestsUsingSource(pHigh->getToSource()), '|');
				//	}
				//	bncs_string ssPti = "";
				//	for (int iC = 0; iC<ssl.count(); iC++) {
				//		int iDest = ssl[iC].toInt();
				//		if (iDest>0) {
				//			if (iC>0) ssPti.append("|");
				//			ssPti.append(bncs_string(iDest + iOffset));
				//		}
				//	}
				//	// update pti slot
				//	pRouterVirtual->setSourcePti(pHigh->getToSource() + iOffset, ssPti);
				//	TrimAndUpdatePti(pHigh->getToSource() + iOffset, ssPti);
				}


			}
			else
				Debug("ProcessFirstPass -ERROR- no router %d in class", pHigh->getFromRouter());
		}
	}
	if (i_unknown_highways>0) {
		Debug( "ProcessFirstPass -UNKNOWN HIGHWAYS : %d", i_unknown_highways );
		char szMsg[128]="";
		wsprintf( szMsg, "%d", i_unknown_highways );
		SetDlgItemText( hWndDlg, IDC_MESSAGE, szMsg );
		eiInfoGrd->updateslot( SLOT_COUNT_UNKNOWN_HWAYS, szMsg );
	}
	//
}



void CheckAutoResilienceState() 
{

	iOverallStateJustChanged = 0;
	iNextResilienceProcessing = 57; // reset counter 

	if (edMainGRD) {
		int iMode = edMainGRD->getifmode();
		int iState = edMainGRD->getstate();
		char szNM[32] = "", szCM[32] = "", szStr[32] = "";
		//if (bShowAllDebugMessages) Debug("CheckAutoRes - overall %d state %d", iOverallTXRXModeStatus, iState);

		// determine string for driver dialog window
		if (iMode == INFO_RXMODE) {
			strcpy_s(szNM, "RXONLY");
			iLabelAutoStatus = 2;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "RXonly Resv");
		}
		if (iMode == INFO_TXRXMODE){
			strcpy_s(szNM, "TX-RX");
			iLabelAutoStatus = 0;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Tx/Rx Main");
		}

		if (iMode != iOverallTXRXModeStatus) {

			switch (iMode) {
			case INFO_RXMODE: case IFMODE_TXRXINQ:
				iOverallModeChangedOver = 5;
				iOverallTXRXModeStatus = INFO_RXMODE;
				if (eiInfoGrd) {
					if (eiInfoGrd->getmode() != TO_RXONLY)	{
						eiInfoGrd->setmode(TO_RXONLY);
						Debug("CheckAuto: Infodriver %d not in same state as GRD - trying to force rxonly", iInfoDevice);
					}
				}
				if (eiInfoTracePti) {
					if (eiInfoTracePti->getmode() != TO_RXONLY)	{
						eiInfoTracePti->setmode(TO_RXONLY);
						Debug("CheckAuto: Trace Infodriver %d not in same state as GRD - trying to force rxonly", iInfoTracePti);
					}
				}
				Debug("Tieline Manager Automatic running in  RXONLY  mode ");
			break;

			case INFO_TXRXMODE:
				iOverallModeChangedOver = 5;
				iOverallTXRXModeStatus = INFO_TXRXMODE;
				if (eiInfoGrd) {
					if (eiInfoGrd->getmode() != INFO_TXRXMODE)	{
						eiInfoGrd->setmode(INFO_TXRXMODE);
						eiInfoGrd->requestmode = TO_TXRX;
						Debug("CheckAuto: Infodriver %d not in same state as GRD - trying to force txrx", iInfoDevice);
					}
				}
				if (eiInfoTracePti) {
					if (eiInfoTracePti->getmode() != INFO_TXRXMODE)	{
						eiInfoTracePti->setmode(INFO_TXRXMODE);
						eiInfoTracePti->requestmode = TO_TXRX;
						Debug("CheckAuto: Trace Infodriver %d not in same state as GRD - trying to force txrx", iInfoTracePti);
					}
				}
				Debug("Tieline Manager Automatic running in  TX/RX  mode ");
			break;

			default:
				iLabelAutoStatus = 3;
				iOverallTXRXModeStatus = IFMODE_NONE;
				Debug("Packager Automatic in an  UNKNOWN  infodriver mode ");
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "??");
			}  // switch
		} 

		// go thru all infos and report status
		int iTXRXCount = 0, iRXOnlyCount = 0;
		if (eiInfoGrd->getmode() == INFO_TXRXMODE) {
			iTXRXCount++; 
			iLabelInfoStatus = 0;
			SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS, "TXRX");
		}
		else if (eiInfoGrd->getmode() == INFO_RXMODE) {
			iRXOnlyCount++; 
			iLabelInfoStatus = 2;
			SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS, "RXonly");
		}
		if (eiInfoTracePti->getmode() == INFO_TXRXMODE) {
			iLabelTraceStatus = 0;
			SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS2, "TXRX");
			iTXRXCount++;
		}
		else if (eiInfoTracePti->getmode() == INFO_RXMODE) {
			iLabelTraceStatus = 2;
			SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS2, "RXonly");
			iRXOnlyCount++;
		}

		if (iOverallTXRXModeStatus == INFO_TXRXMODE) {
			if (iTXRXCount<2) {
				Debug("ERROR -- Driver has mixed txrx states  - trying to correct");
				iLabelAutoStatus = 3;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, " Mixed TX States");
				// force all into txrx
				ForceAutoIntoTXRX();
				iOverallModeChangedOver = 5;
				iNextResilienceProcessing = 6; // reset counter 
			}
		}
		else if (iOverallTXRXModeStatus == INFO_RXMODE) {
			if (iRXOnlyCount<2) {
				Debug("ERROR -- Driver has mixed rxonly states  - trying to correct");
				iLabelAutoStatus = 3;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Mixed TXStates");
				// force all into rxonly
				ForceAutoIntoRXONLY();
				iOverallModeChangedOver = 5;
				iNextResilienceProcessing = 6; // reset counter 
			}
		}

	}
	else
		Debug( "CheckInfoState - no edMainGRD class " );

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  
//	COMMENTS:
//	
//	WM_COMMAND	- process the application menu
//  WM_DESTROY	- post a quit message and return
//	
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	int iState=0, iIFMode=0;
	
	switch (message) 
	{
		case WM_CREATE:
			return InitApp(hWnd);
		break;
		
		case WM_TIMER:
			switch ( wParam ) {
				case STARTUP_TIMER_ID : // go thru start up now
					Debug("First Startup timer");

					KillTimer( hWnd , STARTUP_TIMER_ID );
					// get required fields from instances and object settings
					iOverallModeChangedOver=0;
					iLabelAutoStatus=0;
					if (iDebug>0) 	CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_ON, MF_CHECKED);

					// all init ok so continue 
					if (LoadAllInitialisationData()==TRUE) 
					{
						Debug( "auto running now");
						//iNextInfoResilienceProcessing = 1;
						iNextDestinationCheck=30;
						// try and take control
						//ForceAutoIntoTXRX();
						CheckAutoResilienceState();
						// get grd state and mode
						if (edMainGRD) {
							iOverallTXRXModeStatus = edMainGRD->getifmode();
							iState = edMainGRD->getstate();
							Debug( "Auto-startup- overall %d state %d ", iOverallTXRXModeStatus, iState );
						}

						// register for revertives
						bAutomaticStarting = TRUE;
						AutoRegistrations();

						// poll for routes on physical routers
						CheckAutoResilienceState();
						iStartupCounter=0;
						PollPhysicalRouters(0);

						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Polling" );
						bShowAllDebugMessages = FALSE; 
						CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_UNCHECKED);
						//bShowAllDebugMessages = TRUE; 
						//CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_CHECKED);

						// poll routers seperately
						SetTimer( hWnd, ROUTERPOLL_TIMER_ID, 1000, NULL ); 
						// set timer for device management
						SetTimer( hWnd, FIRSTPASS_TIMER_ID, 20000, NULL ); 
						// wait for all revertives etc 
					}
					else 
					{
						Debug( "LoadInit - config errors - auto in error state");
						PostAutomaticErrorMessage(iGRDDevice, ALARM_SET_ERROR_MESSAGE, bncs_string("config errors - Router Tieline Manager STOPPED"));
						iLabelAutoStatus=2;
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "CONFIG ERRORS" );
						ForceAutoIntoRXONLY();
					}

				break;


				case ROUTERPOLL_TIMER_ID:
					iStartupCounter++;
					if (iStartupCounter<ssl_AllRoutersIndices.count())	{
						PollPhysicalRouters(iStartupCounter);
					}
					else if (iStartupCounter==12) {
						CheckAndRecalculateVirtualDests( );
					}
					else if (iStartupCounter >14 ){
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Pending First Pass");
						KillTimer(hWnd, ROUTERPOLL_TIMER_ID);
						CheckRevertivesReceivedOK(TRUE);
						CheckAutoResilienceState();
					}
				break;


				case FIRSTPASS_TIMER_ID:
					KillTimer( hWnd , FIRSTPASS_TIMER_ID );
					CheckAutoResilienceState();
					Debug("PFP- Processing first pass");
					//  set highway allocated src
					ProcessFirstPass();					
					// process all vir dests again
					Debug("PFP- Processing Virtual Routes");
					ProcessVirtualRoutesOnStartup();
					// then process highway management for first time
					Debug("PFP- Processing Highway Mgmt");
					ProcessHighwayManagement();
					iNextManagementProcessing=5; // prime number
					if (edMainGRD) {
						iOverallTXRXModeStatus = edMainGRD->getifmode();
						iState = edMainGRD->getstate();
					}
					bAutomaticStarting = FALSE;
					Debug("PFP- Dump Tallies");
					DumpAllTallies();
					//
					iNextDestinationCheck=30;
					Debug("PFP- CheckVirt dests");
					CheckAndRecalculateVirtualDests();
					//
					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Running OK" );
					// set timer for device management
					SetTimer( hWnd, ONE_SECOND_TIMER_ID, ONE_SECOND_TIMER_INTERVAL, NULL ); 
					// start up flag - so auto will poll for all routers and process first pass

					//bAutoParkHighways = TRUE;
					//Debug("++ Turning on auto Park highways");
					//CheckMenuItem(GetSubMenu(GetMenu(hWnd), 2), ID_OPTIONS_AUTOPARKHIGHWAYS, MF_CHECKED);

					bAbletoProcessIncomingCommand = TRUE;
					Debug( "Entering Normal running");

				break;


				case ONE_SECOND_TIMER_ID : // used for connection control, cyclic polling of hardware and housekeeping
				{
					//Debug( "one second timer");

					// 1. processnext command if any in buffer - if nothing in outgoing buffer - check incoming command que
					if (qCommands.size()>0) 
						ProcessNextCommand(1); 
					else 
						bAbletoProcessIncomingCommand = TRUE;
					//
					if ((qIncoming.size()>0)&&(bAbletoProcessIncomingCommand==TRUE)) {
						ProcessNextIncomingCommand();
					}
					else 
						bAbletoProcessIncomingCommand = TRUE;

					// 2. check for pending or hung highways -- auto to resolve / repair in ProcessHighwayManagement
					iNextManagementProcessing--;
					if (iNextManagementProcessing<=0) {
						ProcessHighwayManagement();
						iNextManagementProcessing = iUserDefinedMngmntProc;
					}

					// 3. check resililence
					iNextResilienceProcessing--;
					if (iNextResilienceProcessing<=0) {
						iNextResilienceProcessing=37; // prime number 
						CheckAutoResilienceState();
					}

					// 4. check if recently failed over
					if (iOverallModeChangedOver>0) {
						iOverallModeChangedOver--;
						if (iOverallModeChangedOver<=0) 
							Restart_CSI_NI_NO_Messages();
					}

					// 5 check valid routes to dests
					WorkThruAndProcessAnyPendingRoutes();

				}
				break;

				case COMMAND_TIMER_ID :
				{
					ProcessNextCommand(1);
				} 
				break;

				case DATABASE_TIMER_ID:
					// reload all panelsets and reassert panel devices
					KillTimer( hWndMain, DATABASE_TIMER_ID );
					bRMDatabaseTimerRunning = FALSE;
					ssl_RMChanges.clear();
					Debug( " RMtimer - clearing list ");
				break;

				case RESPONSE_TIMER_ID: 
					ProcessAnyMoreResponses(200);
				break;

				case FORCETXRX_TIMER_ID:
					Debug("ForceTxrx timer");
					KillTimer(hWndMain, FORCETXRX_TIMER_ID);
					ForceAutoIntoTXRX();
				break;

				case LISTBOX_TIMER_ID :
					{
						if (pRouterVirtual) {
							// check on loading sources
							if ((iLoadSrcListBoxEntry>0)&&(iLoadSrcListBoxEntry<pRouterVirtual->getMaxSources())) {
								// sources only
								LoadSourcesListbox();
							}
							// check on loading dests
							if ((iLoadDestListBoxEntry>0)&&(iLoadDestListBoxEntry<pRouterVirtual->getMaxDestinations())) {
								// destinations only
								LoadDestinationsListbox();
							}
							// see if timer still needs to keep running
							if ((iLoadSrcListBoxEntry>pRouterVirtual->getMaxSources())&&(iLoadDestListBoxEntry>pRouterVirtual->getMaxDestinations())) KillTimer( hWndMain, LISTBOX_TIMER_ID );
						}
						if ((iLoadSrcListBoxEntry==0)&&(iLoadDestListBoxEntry==0)) KillTimer( hWndMain, LISTBOX_TIMER_ID );
					}
					break;
					
			}  // switch(wparam) on timer types

		break; // wmtimer

		case WM_SIZE: // if the main window is resized, resize the listbox too
		//Don't resize window with child dialog
		break;
		
	case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		// Parse the menu selections:
		switch (wmId)
		{
			case IDM_ABOUT:
				DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
			
			case IDM_EXIT:
				DestroyWindow(hWnd);
			break;
			
			case ID_DEBUG_CLEAR:
				SendMessage(hWndList,LB_RESETCONTENT,0,0L);
			break;
			
			case ID_DEBUG_ON:
				if (iDebug){
					iDebug=FALSE;
				} 
				else{
					iDebug=TRUE;	
					fDebugHide=FALSE;
				}
				AssertDebugSettings(hWnd);
			break;
			
			case ID_DEBUG_HIDE:
				if (fDebugHide){
					fDebugHide=FALSE;
					iDebug=TRUE;
				} 
				else {
					fDebugHide=TRUE;
					iDebug=FALSE;
				}
				AssertDebugSettings(hWnd);
			break;

			case  ID_DEBUG_SHOWALLMESSAGES : 
				if (bShowAllDebugMessages==TRUE) {
					Debug( "-- cease showing most debug messages");
					bShowAllDebugMessages = FALSE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_UNCHECKED);
				}
				else {
					Debug( "++ now showing all debug messages");
					bShowAllDebugMessages = TRUE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_CHECKED);
				}
			break;
				
			case ID_OPTIONS_FORCETXRX:
			{
				Debug( "Forcing Driver into TXRX Mode" );
				ForceAutoIntoTXRX();
			}
			break;
				
			case ID_OPTIONS_FORCERXONLY :
			{
				Debug( " Trying to force driver into RXONLY mode");
				ForceAutoIntoRXONLY();
			}
			break;
				
			case ID_OPTIONS_POLLROUTERS :
				Debug( " Polling physical routers ");
				iStartupCounter=0;
				PollPhysicalRouters(0);
				SetTimer( hWnd, ROUTERPOLL_TIMER_ID, 2000, NULL ); 				
			break;

			case ID_OPTIONS_CHECKROUTERDESTS:
				Debug("Checking dests for zero sources");
				CheckRevertivesReceivedOK(FALSE);
				CheckAndRecalculateVirtualDests();
			break;

			case ID_OPTIONS_INCREMENTMANAGEMENT:
				iUserDefinedMngmntProc = iUserDefinedMngmntProc + 3;
				SetDlgItemInt(hWndDlg, IDC_CONF_MANAGEMENT, iUserDefinedMngmntProc, TRUE);
				Debug("Incrementing Highway Management to %d seconds", iUserDefinedMngmntProc);
			break;

			case ID_OPTIONS_DECREMENTMANAGEMENT:
				if (iUserDefinedMngmntProc > 9) {
					iUserDefinedMngmntProc = iUserDefinedMngmntProc - 3;
					SetDlgItemInt(hWndDlg, IDC_CONF_MANAGEMENT, iUserDefinedMngmntProc, TRUE);
					Debug("Decrementing Highway Management to %d seconds", iUserDefinedMngmntProc);
				}
				else
					Debug("Will not set Highway Managment interval any lower than %d", iUserDefinedMngmntProc);
			break;

			case ID_OPTIONS_AUTOPARKHIGHWAYS:
				//if (bAutoParkHighways) {
					bAutoParkHighways = FALSE;
					Debug("-- Turning off auto Park highways");
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 2), ID_OPTIONS_AUTOPARKHIGHWAYS, MF_UNCHECKED);
				//}
				//else {
				//	bAutoParkHighways = TRUE;
				//	Debug("++ Turning on auto Park highways");
				//	CheckMenuItem(GetSubMenu(GetMenu(hWnd), 2), ID_OPTIONS_AUTOPARKHIGHWAYS, MF_CHECKED);
				//}
			break;

			default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		} // switch (wmId)
		break;

	case WM_DESTROY:
		CloseApp(hWnd);
		PostQuitMessage(0);
		break;
		
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}



//
//  FUNCTION: DlgProcChild()
//
//  PURPOSE: Message handler for child dialog.
//
//  COMMENTS: Displays the interface status
//
// 
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId    = LOWORD(wParam); 
	int wmEvent = HIWORD(wParam); 
	char szData[MAX_BUFFER_STRING]= "";
	int iIndex=0;
	
	HWND hButton = (HWND)lParam;
	LONG idButton=GetDlgCtrlID(hButton);

	CRouterHighway* pHigh=NULL;
	char szCmd[MAX_BUFFER_STRING]="";
	
	//char szdata[256];
	//wsprintf( szdata, "DlgChild message %d, BUTTON %d \n", message, idButton);
	//OutputDebugString( szdata );
	switch (message)	{

		case WM_COMMAND :
			switch (wmId)	{
				case IDC_LISTHIGHWAYS:		// list of highways - display details
					iIndex = (SendDlgItemMessage(hWndDlg, IDC_LISTHIGHWAYS, LB_GETCURSEL , 0,0))+1;
					if ((iIndex>0) && (iTotalHighwaysAndWraps>0) && (pRouterVirtual) && (mapOfAllHighways)) {
						iChosenHighway = iIndex;
						DisplayHighwayData(iChosenHighway);
					}
				break;

				case IDC_LISTSRC:		// list of sources - display details
					iIndex = (SendDlgItemMessage(hWndDlg, IDC_LISTSRC, LB_GETCURSEL , 0,0))+1;
					if ((iIndex>0) && (pRouterVirtual) && (mapOfAllHighways)) {
						iChosenSource = iIndex;
						SetDlgItemInt( hWndDlg, IDC_RTR_SOURCE, iChosenSource, TRUE );
						SetDlgItemText( hWndDlg, IDC_RTR_SOURCE2, " " );
						SetDlgItemText( hWndDlg, IDC_RTR_SOURCE3, " " );
						if (pRouterVirtual) {
							int ihw = pRouterVirtual->getHighwayAssociatedtoSource(iChosenSource);
							pHigh = GetRouterHighway(ihw);
							if (pHigh) {
								if (pHigh->getHighwayType() == HIGHWAY_TX_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_SOURCE2, "tx");
								else if (pHigh->getHighwayType() == HIGHWAY_LIVE_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_SOURCE2, "lv");
								else if (pHigh->getHighwayType() == HIGHWAY_ENG_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_SOURCE2, "en");
								else if (pHigh->getHighwayType() == HIGHWAY_SUPVR_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_SOURCE2, "sv");
								else if (pHigh->getHighwayType() == HIGHWAY_CMVP_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_SOURCE2, "Cm");
								else if (pHigh->getHighwayType() == HIGHWAY_LMVP_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_SOURCE2, "Lm");
								else if (pHigh->getHighwayType() == HIGHWAY_POOL_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_SOURCE2, "pl");
								else if (pHigh->getHighwayType() == HIGHWAY_WRAP_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_SOURCE2, "wr");
								else  SetDlgItemText( hWndDlg, IDC_RTR_SOURCE2, "hy");
							}
							SetDlgItemText(hWndDlg, IDC_RTR_SOURCE4, LPCSTR(pRouterVirtual->getListOfDestsUsingSource(iChosenSource)));
						}
					}
					break;
					
				case IDC_LISTDEST:		// list of destinations - display details
					iIndex = (SendDlgItemMessage(hWndDlg, IDC_LISTDEST, LB_GETCURSEL , 0,0))+1;
					if ((iIndex>0) && (pRouterVirtual) && (mapOfAllHighways)) {
						iChosenDestination = iIndex;
						// get current routed source and highlight if highlight option set
						if (pRouterVirtual) {		
							int iCurrSrc = pRouterVirtual->getRoutedSourceForDestination(iChosenDestination);
							wsprintf( szData, "%d (%d)", iChosenDestination, iCurrSrc);
							// highlight in source box
							if (iCurrSrc > 0) {
								iChosenSource = iCurrSrc;
								HWND hCtl = GetDlgItem(hWndDlg, IDC_LISTSRC);
								SendMessage(hCtl, LB_SETCURSEL, iChosenSource - 1, 0);
							}
							// update gui
							SetDlgItemText( hWndDlg, IDC_RTR_DEST, szData );	
							SetDlgItemText(hWndDlg, IDC_RTR_DEST3, LPCSTR(pRouterVirtual->getDestinationTrace(iChosenDestination)));
							SetDlgItemText(hWndDlg, IDC_RTR_DEST2, " ");
							int ihw = pRouterVirtual->getHighwayAssociatedtoDest(iChosenDestination);
							pHigh = GetRouterHighway(ihw);
							if (pHigh) {
								if (pHigh->getHighwayType() == HIGHWAY_TX_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "tx");
								else if (pHigh->getHighwayType() == HIGHWAY_LIVE_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "lv");
								else if (pHigh->getHighwayType() == HIGHWAY_ENG_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "en");
								else if (pHigh->getHighwayType() == HIGHWAY_SUPVR_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "sv");
								else if (pHigh->getHighwayType() == HIGHWAY_CMVP_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "Cm");
								else if (pHigh->getHighwayType() == HIGHWAY_LMVP_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "Lm");
								else if (pHigh->getHighwayType() == HIGHWAY_POOL_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "pl");
								else if (pHigh->getHighwayType() == HIGHWAY_WRAP_TYPE) SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "wr");
								else  SetDlgItemText(hWndDlg, IDC_RTR_DEST2, "hy");
							}
						}
					}
				break;

				case IDC_TAKE:
					if ((iChosenDestination>0)&&(iChosenSource>0)&&(mapOfAllHighways)) {
						ProcessVirtualRouterCrosspoint( iChosenSource, iChosenDestination, iChosenHwayType );
					}
				break;

				case IDC_TYPE1:
					iChosenHwayType = HIGHWAY_TX_TYPE;
					SetDlgItemInt(hWndDlg, IDC_HWAY_TYPE, HIGHWAY_TX_TYPE, TRUE);
					break;

				case IDC_TYPE2:
					iChosenHwayType = HIGHWAY_LIVE_TYPE;
					SetDlgItemInt(hWndDlg, IDC_HWAY_TYPE, HIGHWAY_LIVE_TYPE, TRUE);
					break;

				case IDC_TYPE3:
					iChosenHwayType = HIGHWAY_ENG_TYPE;
					SetDlgItemInt(hWndDlg, IDC_HWAY_TYPE, HIGHWAY_ENG_TYPE, TRUE);
					break;

				case IDC_TYPE4:
					iChosenHwayType = HIGHWAY_SUPVR_TYPE;
					SetDlgItemInt(hWndDlg, IDC_HWAY_TYPE, HIGHWAY_SUPVR_TYPE, TRUE);
					break;

				case IDC_TYPE5:
					iChosenHwayType = HIGHWAY_CMVP_TYPE;
					SetDlgItemInt(hWndDlg, IDC_HWAY_TYPE, HIGHWAY_CMVP_TYPE, TRUE);
					break;

				case IDC_TYPE6:
					iChosenHwayType = HIGHWAY_LMVP_TYPE;
					SetDlgItemInt(hWndDlg, IDC_HWAY_TYPE, HIGHWAY_LMVP_TYPE, TRUE);
					break;

				case IDC_FREEHWAY:
					if ((iChosenHighway>0) && (iChosenHighway <= iTotalHighwaysAndWraps) && (mapOfAllHighways)) {
						CRouterHighway* pHigh = GetRouterHighway( iChosenHighway );
						if (pHigh) {
							if (pHigh->getHighwayType()!=HIGHWAY_WRAP_TYPE) {
								Debug( "Parking highway %d", iChosenHighway );
								// by writing to slot - means other rx-only instance of auto keeps in sync
								wsprintf( szCmd, "IW %d '%d,%d' %d", iInfoDevice, pHigh->getFromRouter(), pHigh->getFromDestination(), SLOT_HIGHWAY_PARK_CMD );
								ecCSIClient->txinfocmd(szCmd);
							}
							else
								Debug("Invalid to route park to a WRAP");
						}
					}
				break;

				case IDC_ROUTETEST:
					if ((iChosenHighway>0) && (iChosenHighway <= iTotalHighwaysAndWraps) && (mapOfAllHighways)) {
						CRouterHighway* pHigh = GetRouterHighway( iChosenHighway );
						if (pHigh) {
							if (pHigh->getHighwayType()!=HIGHWAY_WRAP_TYPE) {
								Debug( "Disabling / Routing TEST to highway %d", iChosenHighway );
								// by writing to slot - means other rx-only instance of auto keeps in sync
								wsprintf( szCmd, "IW %d '%d,%d' %d", iInfoDevice, pHigh->getFromRouter(), pHigh->getFromDestination(), SLOT_HIGHWAY_TEST_CMD );
								ecCSIClient->txinfocmd(szCmd);
							}
							else
								Debug("Invalid to put a WRAP highway into test/disabled mode");
						}
					}
				break;

			}
		break;
	
		case WM_INITDIALOG: 
			//This will colour the background of the child dialog on startup to defined colour
			// brushes are defined in INIT_APP function below
			HBr = hbrTurq;
			return (LRESULT)HBr;
			//HBr = CreateSolidBrush( COL_TURQ );
			//return (LRESULT)HBr;
		break;

		case WM_CTLCOLORDLG:
			// colour all diag at startup
			SetTextColor((HDC) wParam,COL_DK_BLUE);
			SetBkColor((HDC) wParam, COL_TURQ);
			HBr = hbrTurq;
			return (LRESULT)HBr;
		break;

		case WM_CTLCOLORSTATIC:
			 // to colour labels --  this will be called on startup of the dialog by windows
			 // this section will be called every time you do a SetDlgItemText() command for a given label
			 // by testing the value of global vars this can be used to colour labels for Comms OK/Fail, TXRX/Rxonly etc
			switch (idButton)	{	

			case IDC_AUTO_STATUS: 
				if (iLabelAutoStatus==1) {
					SetTextColor((HDC) wParam,COL_DK_BLUE);
					SetBkColor((HDC) wParam, COL_YELLOW);
					HBr = hbrYellow;
				}
				else if (iLabelAutoStatus==2) {
					SetTextColor((HDC) wParam,COL_WHITE);
					SetBkColor((HDC) wParam, COL_LT_RED);
					HBr = hbrLightRed;
				}
				else if (iLabelAutoStatus==3) {
					SetTextColor((HDC) wParam,COL_BLACK);
					SetBkColor((HDC) wParam, COL_LT_ORANGE);
					HBr = hbrLightOrange;
				}
				else {
					SetTextColor((HDC) wParam,COL_WHITE);
					SetBkColor((HDC) wParam, COL_DK_GRN);
					HBr = hbrDarkGreen;
				}
				break;
				
			case IDC_INFODRV_STATUS:
				if (iLabelInfoStatus == 1) {
					SetTextColor((HDC)wParam, COL_DK_BLUE);
					SetBkColor((HDC)wParam, COL_YELLOW);
					HBr = hbrYellow;
				}
				else if (iLabelInfoStatus == 2) {
					SetTextColor((HDC)wParam, COL_WHITE);
					SetBkColor((HDC)wParam, COL_LT_RED);
					HBr = hbrLightRed;
				}
				else if (iLabelInfoStatus == 3) {
					SetTextColor((HDC)wParam, COL_BLACK);
					SetBkColor((HDC)wParam, COL_LT_ORANGE);
					HBr = hbrLightOrange;
				}
				else {
					SetTextColor((HDC)wParam, COL_WHITE);
					SetBkColor((HDC)wParam, COL_DK_GRN);
					HBr = hbrDarkGreen;
				}
				break;

			case IDC_INFODRV_STATUS2:
				if (iLabelTraceStatus == 1) {
					SetTextColor((HDC)wParam, COL_DK_BLUE);
					SetBkColor((HDC)wParam, COL_YELLOW);
					HBr = hbrYellow;
				}
				else if (iLabelTraceStatus == 2) {
					SetTextColor((HDC)wParam, COL_WHITE);
					SetBkColor((HDC)wParam, COL_LT_RED);
					HBr = hbrLightRed;
				}
				else if (iLabelTraceStatus == 3) {
					SetTextColor((HDC)wParam, COL_BLACK);
					SetBkColor((HDC)wParam, COL_LT_ORANGE);
					HBr = hbrLightOrange;
				}
				else {
					SetTextColor((HDC)wParam, COL_WHITE);
					SetBkColor((HDC)wParam, COL_DK_GRN);
					HBr = hbrDarkGreen;
				}
				break;

				case IDC_VERSION: case IDC_IDRX: case IDC_IDTX: case IDC_QUEUE: case IDC_AUTO_INSTANCE: case IDC_CONF_HIGHWAYS3: case IDC_CONF_HIGHWAYS4:
				case IDC_CONF_RTRS: case IDC_ZERO_ROUTERS: case IDC_CONF_HIGHWAYS: case IDC_CONF_HIGHWAYS2: case IDC_RTR_SOURCE2: case IDC_CONF_HIGHWAYS5:
				case IDC_CONF_WRAPS: case IDC_RTR_DEST: case IDC_RTR_SOURCE: case IDC_AUTO_MSG1: case IDC_HIGHWAY_FROMDEST: case IDC_CONF_OFFSETS:
				case IDC_HIGHWAY_FROMRTR: case IDC_HIGHWAY_TORTR: case IDC_HIGHWAY_TOSRC: case IDC_HIGHWAY_STATE: case IDC_HIGHWAY_ACTSOURCE:
				case IDC_HIGHWAY_USERS: case IDC_HIGHWAY_TYPE: case IDC_INFODRV_INDEX: case IDC_HIGHWAY_USERS2: case IDC_RTR_DEST2: case IDC_CONF_HIGHWAYS10:
				case IDC_CONF_HIGHWAYS6: case IDC_CONF_HIGHWAYS7: case IDC_CONF_HIGHWAYS8: case IDC_CONF_HIGHWAYS9: case IDC_INCOMING: case IDC_OUTGOING:
				case IDC_ZERO_EMERALD: case IDC_ZERO_VIRTUAL: case IDC_GRDDRV_INDEX: case IDC_CONF_DUALSRCS: case IDC_CONF_MANAGEMENT: case IDC_INFODRV_INDEX2:
					SetTextColor((HDC) wParam,COL_DK_BLUE);	 
					SetBkColor((HDC) wParam, COL_WHITE);	
					HBr = hbrWhite;
				break;
				
				case IDD_STATUS : case IDC_STATIC:
					SetTextColor((HDC) wParam,COL_DK_BLUE);
					SetBkColor((HDC) wParam, COL_TURQ);
					HBr = hbrTurq;
				break;

			default:
					SetTextColor((HDC) wParam,COL_DK_BLUE);
					SetBkColor((HDC) wParam, COL_TURQ);
					HBr = hbrTurq;
			}
			return (LRESULT)HBr;
		break;
	
	default:
		//Debug( "dlg child def");
		break;
	}
	
    return FALSE;

}



//
//  FUNCTION: Version Info()
//
void VersionInfo() {
	LPBYTE   abData;
	DWORD  handle;
	DWORD  dwSize;
	LPBYTE lpBuffer;
	LPSTR szName , szVer, szModFileName , szBuf;
	char acBuf[16];
#define	PATH_LENGTH	256
	
	szName=(LPSTR)malloc(PATH_LENGTH);
	szModFileName=(LPSTR)malloc(PATH_LENGTH);
	szVer=(LPSTR)malloc(PATH_LENGTH);
	szBuf=(LPSTR)malloc(PATH_LENGTH);
	
	/*get version info*/
	
	GetModuleFileName(hInst,szModFileName,PATH_LENGTH);
	
	dwSize = GetFileVersionInfoSize(szModFileName , &handle);
	abData=(LPBYTE)malloc(dwSize);
	GetFileVersionInfo(szModFileName , handle , dwSize , abData);
	if (dwSize) {
		/* get country translation */
		VerQueryValue((LPVOID)abData , "\\VarFileInfo\\Translation" , (LPVOID*)&lpBuffer , (UINT *) &dwSize);
		/* make country code */
		wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
		if (dwSize!=0) {
			/* get a versioninfo file version number */
			wsprintf(szName,"\\StringFileInfo\\%s\\PRODUCTVERSION",(LPSTR)acBuf);
			VerQueryValue((LPVOID)abData , (LPSTR) szName , (LPVOID*)&lpBuffer,(UINT *) &dwSize);
		}
		/* copy version number from byte buffer to a string buffer */
		lstrcpyn(szVer,(LPSTR)lpBuffer,(int)dwSize);
		/* copy to the dialog static text box */
		wsprintf(szBuf,"%s",szVer);
		// replace any commas with full stops
		for (int c=0; c<int(strlen(szBuf)); c++)
			if (szBuf[c]==44 ) szBuf[c]=46; 
			SetDlgItemText(hWndDlg , IDC_VERSION , szBuf);
	}			
	free((PVOID)abData);
	free((PVOID)szName);
	free((PVOID)szModFileName);
	free((PVOID)szVer);
	free((PVOID)szBuf);
}




//
//  FUNCTION: About()
//
//  PURPOSE: Message handler for about box.
//
//  COMMENTS: Displays the compile date info
//				and used to display version info from
//				a version resource
//
// 
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	
	switch (message)
	{
	case WM_INITDIALOG:
		{
			LPBYTE   abData;
			DWORD  handle;
			DWORD  dwSize;
			LPBYTE lpBuffer;
			LPSTR szName, szModFileName, szBuf;
			char acBuf[16];
			#define	PATH_LENGTH	256
			
			szName=(LPSTR)malloc(PATH_LENGTH);
			szModFileName=(LPSTR)malloc(PATH_LENGTH);
			szBuf=(LPSTR)malloc(PATH_LENGTH);
			
			/*get version info*/
			
			GetModuleFileName(hInst,szModFileName,PATH_LENGTH);
			
			dwSize = GetFileVersionInfoSize(szModFileName, &handle);
			
			abData=(LPBYTE)malloc(dwSize);
			
			GetFileVersionInfo(szModFileName, handle, dwSize, abData);
			
			if (dwSize)
			{
				LPSTR szFileVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szProductVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szComments=(LPSTR)alloca(PATH_LENGTH);
				
				/* get country translation */
				VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
				/* make country code */
				wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
				
				if (dwSize!=0)
				{
					/* get a versioninfo file version number */
					wsprintf(szName,"\\StringFileInfo\\%s\\FileVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szFileVersion,(LPSTR)lpBuffer,(int)dwSize);
				
				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\ProductVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szProductVersion,(LPSTR)lpBuffer,(int)dwSize);
				
				/* copy to the dialog static text box */
				wsprintf(szBuf,"Version %s  (%s)",szFileVersion, szProductVersion);
				SetDlgItemText (hDlg, IDC_VERSION, szBuf);
				
				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\Comments",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				/* copy to the dialog static text box */
				lstrcpyn(szComments,(LPSTR)lpBuffer,(int)dwSize);
				SetDlgItemText (hDlg, IDC_COMMENT, szComments);
				
			}
			
			wsprintf(szName,"%s - %s",(LPSTR)__DATE__,(LPSTR)__TIME__);
			SetDlgItemText(hDlg,IDC_COMPID,szName);
			
			free((PVOID)abData);
			free((PVOID)szName);
			free((PVOID)szModFileName);
			free((PVOID)szBuf);
		}
		return TRUE;
		
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
    return FALSE;
	
}

//
//  FUNCTION: InitApp()
//
//  PURPOSE: Things to do when main window is created
//
//  COMMENTS: Called from WM_CREATE message
//			 We create a list box to fill the main window to use as a
//			debugging tool, and configure the serial port and infodriver 
//			connections, as well as a circular fifo buffer
//
//
LRESULT InitApp(HWND hWnd)
{

	/* return 0 if OK, or -1 if an error occurs*/
	RECT rctWindow;
	BOOL bContinue=TRUE;
	char szMsg[120];
	
	// Init Status Dialogs
	hWndDlg = CreateDialog(hInst, (LPCTSTR)IDD_STATUS, hWnd, (DLGPROC)DlgProcChild);
	GetClientRect(hWndDlg, &rctWindow);
	iChildHeight = rctWindow.bottom - rctWindow.top + 2;
		
	GetClientRect(hWnd,&rc);
	hWndList=CreateWindow("LISTBOX","",WS_CHILD | WS_VSCROLL | LBS_NOINTEGRALHEIGHT | LBS_USETABSTOPS,
						  0,iChildHeight,rc.right-rc.left,rc.bottom-rc.top-iChildHeight,
						  hWnd,(HMENU)IDW_LIST,hInst,0);
	ShowWindow(hWndList,SW_SHOW);

	// init all driver globals etc
	CreateBrushes();	
	iLabelAutoStatus = 1;
	iLabelCSIStatus = 2;
	bShowAllDebugMessages = FALSE;
	iNextResilienceProcessing=3;
	iOverallStateJustChanged=0;
	ss_AutoId="";
	iLabelInfoStatus = 0;
	iLabelTraceStatus = 0;
	iNumberLoadedHighways=0;
	iTotalHighwaysAndWraps=0;
	bAutoParkHighways = FALSE;  // until after first pass

	iLoadSrcListBoxEntry=0;
	iUserDefinedMngmntProc = 7;
	iLoadDestListBoxEntry=0;
	iNextManagementProcessing = 60; // after startup polling
	iOverallAutoAlarmState = 0;

	iChosenSource=0;
	iChosenHighway=0;
	iChosenHwayType=1;
	iChosenDestination=0;
	bOutgoingTimerRunning = FALSE;
	bRMDatabaseTimerRunning = FALSE;
	bNextCommandTimerRunning = FALSE;
	bAbletoProcessIncomingCommand = FALSE;
	ssl_RMChanges = bncs_stringlist( "", '|' );
	OutputDebugString("TIELN - init 1 \n");

	LoadIni();
	VersionInfo();
	iWorkstation = getWS();
	SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Initialising");

	OutputDebugString("TIELN - init 2 \n");
	// connect to csi as driver
	edMainGRD = new extdriver;
	if (edMainGRD) {

		edMainGRD->notify(CSIDriverNotify);
		switch (edMainGRD->connect(iGRDDevice))	{
			case CONNECTED: 
				Debug("Connected OK to CSI for driver %d",edMainGRD->iDevice);
				edMainGRD->setcounters(&lTXID, &lRXID);
				iLabelCSIStatus = 0;		
			break;
			
			case CANT_FIND_CSI:
				Debug("ERROR - - - Connect FAILED to CSI for driver %d", edMainGRD->iDevice);
				bContinue = FALSE;
				iLabelCSIStatus = 2;
				wsprintf(szMsg, "CSI DRV Connection FAILED for %d", iGRDDevice);
				break;

			case DRIVERNUM_ALREADY:
				Debug("ERROR - - Device %d already running as GRD or INFOdrv " , edMainGRD->iDevice);
				bContinue = FALSE;
				iLabelCSIStatus = 2;
				wsprintf(szMsg, "CSI DRV Device %d already running as GRD or INFOdrv", iGRDDevice);
				break;

		default:
			Debug("ERROR - - - Connect  FAILED to CSI for driver %d - - - Error - code %d",edMainGRD->iDevice, edMainGRD->getstate());
			iLabelCSIStatus = 2;		
			bContinue = FALSE;
			wsprintf( szMsg, "CSI DRV Connection FAILED for %d code %d", iGRDDevice, edMainGRD->getstate() );
		}
		
	}
	else {
		// major error - halt automatic
		bContinue = FALSE;
		wsprintf( szMsg, "CSI DRIVER CLASS INIT FAIL AUTO STOPPING" );
	}

	OutputDebugString("TIELN - init 3  \n");
	// connect to csi as client for other revertives
	ecCSIClient = new extclient;
	if (ecCSIClient) {
		ecCSIClient->notify(CSIClientNotify);
		switch (ecCSIClient->connect()) 	{
		case CONNECTED: 
			iLabelCSIStatus = 0;		
			Debug("Connected OK to CSI-Client");
			break;
			
		case CANT_FIND_CSI:
			Debug("CSI CLIENT Conn FAILED");
			iLabelCSIStatus = 2;		
			bContinue = FALSE;
			wsprintf( szMsg, "CSI CLIENT Connection FAILED" );
			break;
			
		default:
			iLabelCSIStatus = 2;		
			Debug("CSI CLIENT ERROR code %d",ecCSIClient->getstate());
			wsprintf( szMsg, "CSI CLIENT FAIL" );
		}
		
	}
	else {
		// major error - halt automatic
		bContinue = FALSE;
		wsprintf( szMsg, "CSI CLIENT CLASS INIT FAIL" );
	}
	
	OutputDebugString("TIELN - init 4   \n");
	eiInfoGrd = new extinfo;
	if (iInfoDevice>0)
	{
		// connect to csi as client for other revertives
		if (eiInfoGrd) {
			eiInfoGrd->notify(InfoNotify);
			int iStat=eiInfoGrd->connect(iInfoDevice);
			switch ( iStat ) 	{
			case CONNECTED: 
				iLabelInfoStatus = 0;		
				Debug("Connected OK to rtr Infodriver %d ", iInfoDevice );
				SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS, "Connect OK");
				break;
				
			default:
				iLabelInfoStatus=2;
				Debug("ERROR ERROR - Connect failed to rtr infodriver , code %d",iStat );
				SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS, "CONN FAIL");
				wsprintf( szMsg, "FAIL TO CONNECT TO RTR INFODRIVER" );
				bContinue = FALSE;
			}
		}
	}
	else {
		SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS, "INVALID ID");
		Debug("ERROR ERROR - Invalid infodriver number from XML file - check config" );
		wsprintf( szMsg, "NO INFODRIVER VALUE IN XML FILE" );
		bContinue = FALSE;
	}
	
	OutputDebugString("TIELN - init 5  \n");
	eiInfoTracePti = new extinfo;
	if (iInfoTracePti>0)
	{
		// connect to csi as client for other revertives
		if (eiInfoTracePti) {
			eiInfoTracePti->notify(InfoNotify);
			int iStat = eiInfoTracePti->connect(iInfoTracePti);
			switch (iStat) 	{
			case CONNECTED:
				iLabelTraceStatus = 0;
				Debug("Connected OK to TRACE PTI Infodriver %d ", iInfoTracePti);
				SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS2, "Connect OK");
				break;

			default:
				iLabelTraceStatus = 2;
				Debug("ERROR ERROR - Connect failed to  PTI infodriver , code %d", iStat);
				SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS2, "CONN FAIL");
				wsprintf(szMsg, "FAIL TO CONNECT TO PTI INFODRIVER");
				bContinue = FALSE;
			}
		}
	}
	else {
		SetDlgItemText(hWndDlg, IDC_INFODRV_STATUS2, "INVALID ID");
		Debug("ERROR ERROR - Invalid Trace infodriver number from XML file - check config");
		wsprintf(szMsg, "NO TRACE PTT INFODRIVER VALUE IN XML FILE");
		bContinue = FALSE;
	}

	OutputDebugString("TIELN - init 6  \n");
	if (bContinue == FALSE) {
		// close app with error message
		MessageBox(hWndMain, szMsg, "Tieline-Mgr auto error", MB_OK);
		//PostMessage( hWndMain, WM_CLOSE, 0, 0 );
		return -1;
	}

	iLabelAutoStatus=1;
	// contiue - set timer 
	iOneSecondPollTimerId = SetTimer( hWnd, STARTUP_TIMER_ID, STARTUP_TIMER_INTERVAL , NULL );
	SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Starting Initialisation ");
	SetDlgItemInt(hWndDlg, IDC_HWAY_TYPE, 1, TRUE);

	return 0;
}

//
//  FUNCTION: CloseApp()
//
//  PURPOSE: Things to do when main window is destroyed
//
//  COMMENTS: Called from WM_DESTROY message
//			The listbox debug window is a child of the main, so it will be 
//			destroyed automatically. 
//
void CloseApp(HWND hWnd)
{
	// kill any timers
	KillTimer( hWnd , COMMAND_TIMER_ID );
	KillTimer( hWnd , ONE_SECOND_TIMER_ID );
	
	// set current driver into rxonly ( if txrx )- give resilient one immediate chance to take over
	//if (iOverallTXRXModeStatus==INFO_TXRXMODE)
		ForceAutoIntoRXONLY();
	
	// clear command and poll queues
	ClearCommandQueue();
	
	// delete maps etc
	Debug("Clear Maps " );
	if (mapOfAllHighways) {
		while (mapOfAllHighways->begin() != mapOfAllHighways->end()) {
			map<int,CRouterHighway*>::iterator it = mapOfAllHighways->begin();
			if (it->second) delete it->second;
			mapOfAllHighways->erase(it);
		}
		delete mapOfAllHighways;
	}

	if (pRouterVirtual) {
		delete pRouterVirtual;
	}
	if (mapOfAllRouters) {
		while (mapOfAllRouters->begin() != mapOfAllRouters->end()) {
			map<int, CRevsRouterData*>::iterator it = mapOfAllRouters->begin();
			if (it->second) delete it->second;
			mapOfAllRouters->erase(it);
		}
		delete mapOfAllRouters;
	}

	if (ecCSIClient)
		delete ecCSIClient;
	
	if (eiInfoGrd) {
		// xxx PostMessage(eiInfoGrd->hWndInfo, WM_CLOSE, 0, 0);
		delete eiInfoGrd;
	}
	if (eiInfoTracePti) {
		//xxx PostMessage(eiInfoTracePti->hWndInfo, WM_CLOSE, 0, 0);
		delete eiInfoTracePti;
	}

	if (edMainGRD)
		delete edMainGRD;
	
	// delete classes brushes etc
	// tidy up colours
	ReleaseBrushes();
	DeleteObject(HBr);
	
}

//
//  FUNCTION: LoadIni()
//
//  PURPOSE: Assign (global) variables with values from .ini file
//
//  COMMENTS: The [section] name is gotten from a stringtable resource
//			 which defines it. It is the same name as the application, defined by the wizard
//			
//			
void LoadIni(void)
{
	iDebug = 0;
	fDebugHide = TRUE;
	bncs_string ssXmlConfigFile = "tieline_manager_auto";
	int iVal = getXMLItemValue(ssXmlConfigFile, "tieline_manager_auto", "debug_mode").toInt();
	if (iVal > 0) {
		iDebug = 1;
		fDebugHide = FALSE;
		if (iVal > 1) bShowAllDebugMessages = TRUE;
	}

	// get auto instance - display on gui
	ss_AutoId = "tieline_manager_auto";
	Debug( "LoadIni - Automatic instance %s ", LPCSTR(ss_AutoId) );
	SetDlgItemText( hWndDlg, IDC_AUTO_INSTANCE, LPCSTR(ss_AutoId));
	SetDlgItemInt(hWndDlg, IDC_INFODRV_INDEX, iInfoDevice, TRUE);
	SetDlgItemInt(hWndDlg, IDC_INFODRV_INDEX2, iInfoTracePti, TRUE);
	
}

/*!
\returns int The workstation number

\brief Function to get the workstation number

First checks to see if %CC_WORKSTATION% is defined
otherwise returns the WS stored in %WINDIR%\csi.ini

NOTE: 
In BNCSWizard apps the following change should be made to LoadINI()
- Replace:
	iWorkstation=atoi(r_p("CSI.INI","Network","Workstation","0",FALSE));
- With:
	iWorkstation=getWS();
- Also add the following to "app_name.h":
int		getWS(void);
*/
int getWS(void)
{
	char* szWS = getenv( "CC_WORKSTATION" );
	
	if(szWS)
	{
		return atoi(szWS);
	}
	else
	{
		return atoi(r_p("CSI.INI","Network","Workstation","0",FALSE));
	}

}

// new function added to version 4.2 assigns the globals    szBNCS_File_Path and szBNCS_Log_File_Path
// called from r_p and w_p functions immediately below
// could easily be called once on first start up of app

char *	getBNCS_File_Path(void)
{
	char* szCCRoot = getenv("CC_ROOT");
	char* szCCSystem = getenv("CC_SYSTEM");
	char szV3Path[MAX_BUFFER_STRING] = "";
	GetPrivateProfileString("Config", "ConfigPath", "", szV3Path, sizeof(szV3Path), "C:\\bncs_config.ini");
	char szWinDir[MAX_BUFFER_STRING] = "";
	GetWindowsDirectory(szWinDir, MAX_BUFFER_STRING);

	// first check for 4.5 system settings
	if (szCCRoot && szCCSystem)	{
		sprintf(szBNCS_File_Path, "%s\\%s\\config\\system", szCCRoot, szCCSystem);
		sprintf(szBNCS_Log_File_Path, "%s\\%s\\logs", szCCRoot, szCCSystem);
	}
	else if (strlen(szV3Path)>0) {
		sprintf(szBNCS_File_Path, "%s", szV3Path);
		strcpy_s(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	else {
		// all other BNCS / Colledia systems - get windows or winnt directory inc v3  that are not using bncs_config.ini
		sprintf(szBNCS_File_Path, "%s", szWinDir);
		strcpy_s(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	return szBNCS_File_Path;

}

/*!
\returns char* Pointer to data obtained
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* defval The default value to return if not found
\param BOOL fWrite Flag - write the default value to the file

\brief Function to read one item of user data from an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
char* r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite)
{
	char szFilePath[MAX_BUFFER_STRING];	
	char szPathOnly[MAX_BUFFER_STRING];

	strcpy( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );


	GetPrivateProfileString(section, entry, defval, szResult, sizeof(szResult), szFilePath);
	if (fWrite)
		w_p(file,section,entry,szResult);
	
	return szResult;

}

/*!
\returns void 
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* setting The value to write - =value

\brief Function to write one item of user data to an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
void w_p(char* file, char* section, char* entry, char* setting)
{

	char szFilePath[MAX_BUFFER_STRING];	
	char szPathOnly[MAX_BUFFER_STRING];

	strcpy( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );

	WritePrivateProfileString(section, entry, setting, szFilePath);

}
//
//  FUNCTION: UpdateCounters()
//
//  PURPOSE: Writes current counter values to child dialog
//
//  COMMENTS: 
//				
//			
//			
void UpdateCounters(void)
{

	char szBuf[32];
	//Update Infodriver External Message Count
	wsprintf(szBuf, "%08d", lTXID);
	SetDlgItemText(hWndDlg, IDC_IDTX, szBuf);
	wsprintf(szBuf, "%08d", lRXID);
	SetDlgItemText(hWndDlg, IDC_IDRX, szBuf);
}

//
//  FUNCTION: Debug()
//
//  PURPOSE: Writes debug information to the listbox
//
//  COMMENTS: The function works the same way as printf
//				example: Debug("Number=%d",iVal);
//			
//			
void Debug(LPCSTR szFmt, ...)
{
	if (iDebug)
	{
		LPSTR szDebug=(LPSTR)malloc(0x10000);
		va_list argptr;
		
		// output to windebug too
		//char tBuffer[9];
		char szDate[16];
		struct tm *newtime;
		time_t long_time;
		
		time( &long_time );                // Get time as long integer. 
		newtime = localtime( &long_time ); // Convert to local time. 
		//_strtime( tBuffer );
		
		va_start(argptr,szFmt);
		vsprintf(szDebug,szFmt,argptr);
		va_end(argptr);
		
		if (strlen(szDebug) > 190)
			szDebug[190]=0; //Truncate long debug messages
		
		sprintf(szDate, "%02d:%02d:%02d",	newtime->tm_hour, newtime->tm_min, newtime->tm_sec );
		char szWinDbg[MAX_BUFFER_STRING] = "";
		char szLogDbg[MAX_BUFFER_STRING] = "";
		wsprintf(szWinDbg, "%s - %s", szDate, szDebug);
		SendMessage(hWndList, LB_ADDSTRING,0,(LPARAM) szWinDbg);	//Add message
		long lCount = SendMessage(hWndList,LB_GETCOUNT,0,0);	//Get List Count
		if (lCount > MAX_LB_ITEMS)
		{
			SendMessage(hWndList, LB_DELETESTRING, 0, 0);	//Delete item
			lCount--;
		}
		SendMessage(hWndList, LB_SETTOPINDEX, (WPARAM) lCount - 1, 0);	//Set Top Index
		
		// log to windows - with full time and date
		wsprintf(szLogDbg, "TIELINE-MGR - %s \n", szWinDbg);
		OutputDebugString( szLogDbg );
		
		// now free memory used
		free((PVOID)szDebug);
	}
}

//
// FUNCTION CreateLogDirectory
//
// new function Version 4.2 of the Wizard - to create log file directory according to Colledia system type
//
BOOL CreateLogFileDirectory ( void )
{
	static bool bFirst = true;
	
	//create the log directory the first time
	if (bFirst) {
		_mkdir(szBNCS_Log_File_Path);	
		bFirst = false;
		return TRUE;
	}
		return FALSE;

}

//
//  FUNCTION: Log()
//
//  PURPOSE: Writes information to the log file
//
//  COMMENTS: The function works the same way as printf
//				example: Debug("Number=%d",iVal);
//			
//		rewritten in wizard version 4.2 to log crashes too - 
//
void Log(LPCSTR szFmt, ...)
{
	char szLogFile[MAX_BUFFER_STRING];
	char tBuffer[9];
	char szDate[16];
	FILE *fp;
	LPSTR szLog=(LPSTR)malloc(0x10000);
	va_list argptr;
	struct tm *newtime;
	time_t long_time;
		
	va_start(argptr,szFmt);
	vsprintf(szLog,szFmt,argptr);
	va_end(argptr);
	
	time( &long_time );                // Get time as long integer. 
	newtime = localtime( &long_time ); // Convert to local time. 
	_strtime( tBuffer );
	sprintf(szDate, "%04d%02d%02d", newtime->tm_year+1900, newtime->tm_mon+1, newtime->tm_mday);
	
	wsprintf(szLogFile, "%s\\%s_child2_%d.log", szBNCS_Log_File_Path, szDate, iGRDDevice);
	
	// create log file directory if it doesn't exist
	CreateLogFileDirectory();
	
	if (fp = fopen(szLogFile, "a"))
	{
		fprintf(fp, tBuffer);
		fprintf(fp, " - ");
		fprintf(fp, szLog);
		fprintf(fp, "\r\n");
		fclose(fp);
	}
	
	free((PVOID)szLog);
}


//
// Function name	: SplitString
// Description	   : Split a delimited string to an array of pointers to strings
// Return type		: int number of elements returned
//
//----------------------------------------------------------------------------//
// Takes a string and splits it up creating an array of pointers to the start //
// of the sections delimted by the array of specified char delimiters         //
// the delimter characters in "string" are overwritten with NULLs             //
// Usage:                                                                     //
// char szString[] = "1,2|3'4,5,6,7,";                                        //
// char delim[] = {',', '|', '\'', ',', ',', ',', ',',};                      //
// UINT count=7;                                                              //
// char *pElements[7];                                                        //
// int iLen;                                                                  //
//	iLen = SplitString( szString, delim, 7, pElements);                       //
//                                                                            //
// NOTE: This funcion works differently to strtok in that consecutive         //
// delimiters are not overlooked and so a NULL output string is possible      //
// i.e. a string                                                              //
//    hello,,dave                                                             //
// where ,,, is the delimiter will produce 3 output strings the 2nd of which  //
// will be NULL                                                               //
//----------------------------------------------------------------------------//
int SplitString(char *string, char *delim, UINT count, char **outarray )
{

	UINT x;
	UINT y;
	UINT len;					// length of the input string
	static char *szNull = "";	// generic NULL output string
	int delimlen;
	int dp;

	len = strlen( string );
	delimlen = strlen( delim );

	if(!len)					// if the input string is a NULL then set all the output strings to NULL
	{
		for (x = 0 ; x < count; x++ )
			outarray[x] = szNull;
		return 0;
	}

	outarray[0] = string;		// set the 1st output string to the beginning of the string

	for( x = 0,y = 1 ; (x < len) && (y < count); x++ )
	{
		if( delimlen < 2 )
			dp = 1;
		else
			dp = y;
		if(string[x] == delim[dp-1])
		{
			string[ x ] = '\0';
			if((x+1) < len)		// if there is another string following this one....
			{
				outarray[ y ] = &string[x+1];
				y++;			// increment the number of assigned buffer
			}
			else
			{
				outarray[ y ] = szNull;
			}
		}
	}

	x = y;
	while( x < count )
		outarray[x++] = szNull;
	return y;					// return the number of strings allocated
	
}


/////////////////////////////////////////////////////////////////////////
// BRUSHES FOR BUTTONS & LABELS
/////////////////////////////////////////////////////////////////////////

/*********************************************************************************************************/
/*** Function to Create Colour brushes ***/
void CreateBrushes()
{ 
	hbrWindowsGrey = CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
	hbrBlack = CreateSolidBrush(COL_BLACK);
	hbrWhite = CreateSolidBrush(COL_WHITE);
	
	hbrDarkRed = CreateSolidBrush(COL_DK_RED);
	hbrDarkGreen = CreateSolidBrush(COL_DK_GRN);
	hbrDarkOrange = CreateSolidBrush(COL_DK_ORANGE);
	hbrDarkBlue = CreateSolidBrush(COL_DK_BLUE);
	
	hbrYellow = CreateSolidBrush(COL_YELLOW);
	hbrTurq = CreateSolidBrush(COL_TURQ);
	
	hbrLightRed = CreateSolidBrush(COL_LT_RED);
	hbrLightGreen = CreateSolidBrush(COL_LT_GRN);
	hbrLightOrange = CreateSolidBrush(COL_LT_ORANGE);
	hbrLightBlue = CreateSolidBrush(COL_LT_BLUE);
	
	return;
}

/********************************************************************************************************/
void ReleaseBrushes()
{
	if (hbrBlack) DeleteObject(hbrBlack);
	if (hbrWhite) DeleteObject(hbrWhite);
	if (hbrWindowsGrey) DeleteObject(hbrWindowsGrey);
	
	if (hbrDarkRed) DeleteObject(hbrDarkRed);
	if (hbrDarkGreen) DeleteObject(hbrDarkGreen);
	if (hbrDarkBlue) DeleteObject(hbrDarkBlue);
	if (hbrDarkOrange) DeleteObject(hbrDarkOrange);
	
	if (hbrYellow)	DeleteObject(hbrYellow);
	if (hbrTurq)	DeleteObject(hbrTurq);
	if (hbrLightRed) DeleteObject(hbrLightRed);
	if (hbrLightBlue) DeleteObject(hbrLightBlue);
	if (hbrLightGreen) DeleteObject(hbrLightGreen);
	if (hbrLightOrange) DeleteObject(hbrLightOrange);
	
	return;
}
