
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"
#include "tieline_manager_common.h"


class CRouterDestData
{
public:
	CRouterDestData( int iDest);
	~CRouterDestData();

	int m_iMapIndex;
	int m_iRoutedSource;		 // straight revertive from hardware  - probel index 
	bncs_string m_ssRoutedSource;  // raw string revertive -- from infodrivers
	bncs_string m_ssAncillaryData;  // IP routing info or other data returned from infodriver router revs often 2000+ eg vidio ipath

	int m_iPendingSource;           // source that should come back from recent RC/IW routing command
	int m_iPreviousRoutedSource; // could be used to restore route incorrectly if lost ???
	bncs_string m_ssPreviousSource;

	int m_iDestLockState;

	int m_iAssociated_Highway;      // highway, wrap etc - the "from dest"

	bncs_stringlist ssl_Trace;
	int m_HWTypeUsedInRoute;

};

