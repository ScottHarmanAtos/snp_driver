﻿using driver_template.helpers;
using driver_template.model;
using driver_template.model.bncs;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace driver_template.viewmodel
{
    public class TemplatesViewModel : INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private ObservableCollection<TemplateModel> templates;

        public ObservableCollection<TemplateModel> Templates
        {
            get { return templates; }
            set
            {
                if (templates != value)
                {
                    templates = value;

                    OnPropertyChanged("Templates");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand Refresh
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    logger.Trace($"Refresh templates pressed");

                    BNCSConfig.ReLoadJsonTemplates(BNCSConfig.ClientInstances);
                });
                return command;
            }
        }

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public TemplatesViewModel(MainModel m)
        {
            Templates = m.Templates;
        }
    }
}
