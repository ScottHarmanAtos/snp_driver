﻿using driver_template.model;
using driver_template.helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace driver_template.viewmodel
{
    public class PanelsViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<PanelModel> panels;

        public ObservableCollection<PanelModel> Panels 
        { get { return panels; }

            set
            { if (panels != value)
                {
                    panels = value;
                    OnPropertyChanged("Panels");
                }
            }
        
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public PanelsViewModel(MainModel m)
        {
            Panels = m.Panels;
        }
    }
}
