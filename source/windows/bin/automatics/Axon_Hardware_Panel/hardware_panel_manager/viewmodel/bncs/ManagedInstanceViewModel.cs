﻿using instances_and_devicetypes;
using driver_template.model.bncs;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace driver_template.viewmodel
{
    public class ManagedInstanceViewModel
    {
        public ObservableCollection<ManagedInstanceItemViewModel> ManagedInstanceItems { get; private set; } = new ObservableCollection<ManagedInstanceItemViewModel>();

        private ManagedInstances Instances { get; }

        public ManagedInstanceViewModel(ManagedInstances instances)
        {
            this.Instances = instances;
            instances.OnInstanceAdd += Instances_OnInstanceAdd;

            foreach (var v in instances.InstanceSlots)
            {
                foreach (var ps in v.Slots)
                {
                    ManagedInstanceItems.Add(new ManagedInstanceItemViewModel(v.Instance, ps.Param, ps.Index, ps.Slot));
                }
            }
        }

        private void Instances_OnInstanceAdd(object sender, EventInstanceAdded arg)
        {
            var i = Instances.InstanceSlots.Where(x => x.Instance.Id == arg.Instance.Id).First();

            foreach (var ps in i.Slots)
            {
                ManagedInstanceItems.Add(new ManagedInstanceItemViewModel(arg.Instance, ps.Param, ps.Index, ps.Slot));
            }
        }
    }

    public class ManagedInstanceItemViewModel : INotifyPropertyChanged
    {
        public String Instance { get; }
        public String Param { get; }
        public uint Device { get; }
        public uint Slot { get; }
        public uint SlotWithOffset { get; }
        public string Value { get; private set; }

        public ManagedInstanceItemViewModel(Instance instance, Parameter param, uint index, Slot slot)
        {
            Instance = instance.Id;
            Param = param?.Name;
            Device = instance.Device;
            slot.PropertyChanged += Slot_PropertyChanged;
            Slot = index;
            SlotWithOffset = index + instance.Offset;
            Value = slot.Value;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnChange(String name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void Slot_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is Slot s && e.PropertyName == "Value")
            {
                Value = s.Value;
                OnChange("Value");
            }
        }
    }
}
