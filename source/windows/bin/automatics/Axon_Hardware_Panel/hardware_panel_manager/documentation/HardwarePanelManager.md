# Axon Hardware Panel Manager

This is a customised version of the original Hardware Panel Manager to load a lightweight set of functions and buttons onto an Axon Hardware Panel (32 or 48 button models only)

## 1. Initial Configuration ##

This automatic relies on a number of files, and an external driver for correct functioning.

### LUA Script ###

The first element, is the lua script
This defined the positional data for layout of text on the buttons, and is requred by the axon_panel_driver.  Documentation is packaged with the driver, but suffice to say, it's easily missed.
A sample lua file is provided in the documentation folder, and this needs to be placed in windows\lib\AxonPanelDriver, and named according to the panel name i.e `BitmapTextSettings_14402_PANEL_007.lua`.  If it's missing, or misnamed, you may see text appearing like the image on the left below:

![alt text](images\bitmaptext_lua.jpg "BitmapText LUA")

As you can see on the right hand side image, the LUA script may need to be tweaked to support larger numbers of letters on each line.  At the moment, it's based on 2 8-character lines, and the point size may need to be reduced to support larger numbers of letters


## 2. Loading JSON Templates ##
JSON Templates are loaded based on a reformatted panel instance name folder (i.e. instead of 12345/PANEL/001, the slashes are replaced with tildes to become 12345~PANEL~001) and evaluated against a JSON schema for validity.

*Only files which meet the defined requirements from the schema will be considered for loading*
Backup fules from the the subfolder backup will be checked against the schema, so if files are missing from the panel folder, or there are errors - then the most recent backup will be loaded.

The customer will be responsible for managing the backups.

![alt text](images\template_refresh.png "Template View")

The template column will show which template is currently loaded, and the default destination for the panel if it's currently mapped.

![alt text](images\axon_hardware_panel_manager_goodcfg.png "Panel View")

Live editing is allowed - edit the JSON file (the path is displayed as a tooltip) - refresh the templates - then reload the desired panel.  In this instance, the edit broke schema rules, and the most recent valid backup was loaded in place.

![alt text](images\axon_hardware_panel_manager_backuploaded.png "Panel View")

## 3. Device/Driver configuration ##

The documentation for the Axon Panel Driver contains the required configuration information at detailed above

What follows is a brief precis on the minimum config for both the driver and the automatic

### Device.ini ###

Define your Axon Hardware Panel instances, to refer back to the `instances.xml` file
```ini
[Instance_1]
; Hardware Control Panel (TX 1) - (TX1-DESK.Centre)
instance=23001/PANEL/003
ip=192.168.1.127

[Instance_2]
; Hardware Control Panel (TX 2) - (TX2-DESK.Centre)
instance=14402/PANEL/007
ip=192.168.1.136
```
### Instances.xml ###

The instance gives us the template, mode and tieline/routing mode - 

```xml
<instance id="axon_hardware_panel_driver_live" alt_id="Axon hardware panel driver TX" type="driver" location="" ref="device=407,offset=0" composite="no" />
<instance id="c_hwp_manager_live" composite="yes" alt_id="Hardware panel manager TX" type="" location="">
  <group id="self" instance="hwp_manager_live"/>    
  <group id="panel_01" instance="23001/PANEL/003"/>
  <group id="panel_02" instance="14402/PANEL/007"/>
</instance>

<instance id="23001/PANEL/003" alt_id="Hardware Control Panel (TX 1)" type="CCP-3201B" location="TX1-DESK.Centre" ref="device=407,offset=0" composite="no">
  <setting id="template_id" value="23001~PANEL~003" />
  <setting id="room_mode" value="live" />
  <setting id="tieline_mode" value="packager" /> 
</instance>
<instance id="14402/PANEL/007" alt_id="Hardware Control Panel (TX 2)" type="CCP-4801B" location="TX2-DESK.Centre" ref="device=407,offset=200" composite="no">
    <setting id="template_id" value="14402~PANEL~007" />
    <setting id="room_mode" value="live" />
    <setting id="tieline_mode" value="packager" />
</instance>
```

## 4. JSON Configuration ##

An exmaple JSON template follows

Take note of the following elements:

1. Blanks no longer need to be defined
2. Each functional button has an index
3. Values are integers
4. Labels are optional - if not present, the value will be retrieved from the associated database

```json
[{
    "_comment": "Automatically generated data from D:\\db4242\\01 BNCS Configuration\\03 DN and SY Masters\\Discovery_DN29_hardware_panels_config_model_autov2.xlsx, Dated: 13/01/2021, Version: 8, Author: Tim Hall, Extracted by Tim on DESKTOP-UDAU6FD, 13/01/2021 16:13:00 using BNCSConfigBuilder 1.35.13.12120, Changes: Minor tweaks to match export requirements",
    "type": "function",
    "timestamp": "2021-1-15T15:08:51.141Z",
    "label": "LT-1",
    "location": "LDC_PCR1-DESK.Left",
    "destination": 36686,
    "options": [{
        "type": "page",
        "label": "LT-1|PAGE 1",
        "tags": null,
        "landing_index": 0,
        "options": [{
            "type": "source",
            "index": 1,
            "value": 36501,
            "label": "OS1"
        }, {
            "type": "source",
            "index": 2,
            "value": 36502,
            "label": "OS2"
        }, {
          <--- continues --->
        }, {
            "type": "pageselect",
            "index": 32,
            "value": 2,
            "label": "Next|Page"
        }]
    }, {
        "type": "page",
        "label": "LT-1|PAGE 2",
        "tags": null,
        "landing_index": 0,
        "options": [{
            "type": "source",
            "index": 1,
            "value": 36711,
            "label": "LT1 i/p 1"
        }, {
            "type": "source",
            "index": 2,
            "value": 36712,
            "label": "LT1 i/p 2"
        }, {
            "type": "source",
            "index": 3,
            "value": 36713,
            "label": "LT1 i/p 3"
        }, {
          <--- continues --->
        }, {
            "type": "pageselect",
            "index": 31,
            "value": 1,
            "label": "Prv|Page"
        }]
    }]
}]

```


## 5.  Version Control ##
Version|Date|Author|Comments
-|-|-|-
< 1.0.0.17||Christian Recoseanu|Initial Axon panel support
1.0.1.1|25/4/2020|Scott Harman|Initial support for Packager routes
1.1.0.8|25/9/2020|Scott Harman|Initial implementation of Discovery Simplified model
1.1.0.12|3/11/2020|Scott Harman|AudioMode latching support
1.1.0.14|11/11/2020|Scott Harman|AudioMode changes
1.1.0.15|24/11/20202|Scott Harman|Check for null object when loading page
1.1.1.0|15/01/2021|Scott Harman|Implemented backup of correctly loaded JSON documents
1.1.1.1|26/01/2021|Scott Harman|Interface tidied up
1.1.1.3|11/3/2021|Scott Harman|Add support to ignore empty JSON elements.  Update functions to load JSON docs from root of hardware_panel_templates, and change backup location.