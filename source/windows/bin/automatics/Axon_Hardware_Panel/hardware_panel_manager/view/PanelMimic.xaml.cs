﻿using driver_template.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace driver_template.view
{
    /// <summary>
    /// Interaction logic for PanelMimic.xaml
    /// </summary>
    public partial class PanelMimic : Window
    {
        public static void ShowPanelMimic(PanelModel dataContext)
        {
            if (Application.Current.Dispatcher != null)
                Application.Current.Dispatcher.Invoke(
                () =>
                {
                    PanelMimic p = new PanelMimic();
                    p.DataContext = dataContext;
                    p.ShowDialog();
                });
        }

        private PanelMimic()
        {
            InitializeComponent();        
        }
    }
}
