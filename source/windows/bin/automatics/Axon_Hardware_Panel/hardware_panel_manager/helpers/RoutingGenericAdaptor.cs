﻿using driver_template.model.mimic;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.helpers
{
    public static class RoutingGenericAdaptor
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static string ParseRouteToDo(string type, TielineMode tielineMode, int source)
        {
            if (type == "tieline_router")
            {
                switch (tielineMode)
                {
                    case TielineMode.TX:
                        {
                            return $"{source},{(int)tielineMode}";
                        }                        

                    case TielineMode.ENG:
                        {
                            return $"{source},{(int)tielineMode}";
                        }                        

                    case TielineMode.LIVE:
                        {
                            return $"{source},{(int)tielineMode}";
                        }                        

                    case TielineMode.SUP:
                        {
                            return $"{source},{(int)tielineMode}";
                        }
                    case TielineMode.PACKAGER:
                        {
                            return $"index={source}";
                            //return $"{source}";
                        }
                    case TielineMode.IPX:
                        {
                            return $"index={source}";
                            //return $"{source}";
                        }

                    default:
                        {
                            return $"{source},{(int) tielineMode}";
                        }                        
                }
            }

            return null;
        }

        public static int WhatRouteWasMade(string type, string msg)
        {
            if (type == "tieline_router")
            {
                string tempRoute = msg;
                if (msg.Contains("index="))
                {
                    var splitroute = msg.Split('=');
                    if (splitroute.Length >= 2)
                    {
                            tempRoute = splitroute[1];

                    }

                }
                var split = tempRoute.Split(',');
                //override split if we are a packager


                if (split.Length == 2)
                {
                    if (Int32.TryParse(split[0], out int tempIndex))
                    {
                        return tempIndex;
                    }
                }
            }

            return 0;
        }
    }
}
