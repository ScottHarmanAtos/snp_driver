﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace driver_template.helper
{
    /// <summary>
    /// Interaction logic for PopupMessageBox.xaml
    /// </summary>
    public partial class PopupMessageBox : Window
    {
        DispatcherTimer timer = null;
        int timerLength = 15;
        string Message = null;

        public static void Show(string Message, string Title, int TimerLength = 15)
        {
            if (Application.Current.Dispatcher != null)
                Application.Current.Dispatcher.Invoke(
                () =>
                {
                    PopupMessageBox p = new PopupMessageBox(Message, Title, TimerLength);
                });
        }

        public static void ShowMessageBox(string Message, string Title)
        {
            if (Application.Current.Dispatcher != null)
                Application.Current.Dispatcher.Invoke(
                () =>
                {
                    PopupMessageBox p = new PopupMessageBox(Message, Title, 0);
                });
        }

        private PopupMessageBox(string Message, string Title, int TimerLength = 15)
        {
            InitializeComponent();

            this.Message = Message;
            this.timerLength = TimerLength;
            Time.Content = this.timerLength;
            Text.Text = this.Message;
            this.Title = Title;

            if (TimerLength > 0)
            {
                timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 1);
                timer.Tick += timer_Tick;
                timer.Start();
            }
            else
            {
                this.COPY.Visibility = System.Windows.Visibility.Hidden;
                this.TimeLbl.Visibility = System.Windows.Visibility.Hidden;
                this.Time.Visibility = System.Windows.Visibility.Hidden;
            }

            this.ShowDialog();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            timerLength -= 1;
            Time.Content = timerLength;
            if (timerLength <= 0)
            {
                timer.Stop();
                Close();
            }
        }

        private void Button_OK(object sender, RoutedEventArgs e)
        {
            if (timer != null)
                timer.Stop();
            Close();
        }

        private void Button_COPY(object sender, RoutedEventArgs e)
        {
            Clipboard.SetDataObject(Message);
        }
    }
}
