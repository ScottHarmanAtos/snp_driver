﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.helpers
{
    public enum ErrorCode
    {
        NoError = 0,
        Error = 1,
        Exception = 2,
        StartupError = 3,
        InfodriverClosed = 4,
        MissingArgument = 5,
        DriverAlreadyRunning = 6
    }
}
