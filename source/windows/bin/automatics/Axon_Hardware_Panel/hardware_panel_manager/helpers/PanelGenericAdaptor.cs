using driver_template.json;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.helpers
{
    public static class PanelGenericAdaptor
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static string ParseColour(JBasePanelType type, string colour)
        {
            if (type.colours_translation.TryGetValue(colour, out var colourOutgoing))
            {
                return colourOutgoing;
            }
            else
            {
                return null;                
            }
        }

        public static string ParseText(string type, bool has2lines, string line1, string line2 = "")
        {
            // if (type == "CCP-3201B")
            if (type != null)
                 {
                if (has2lines)
                {
                    return $"2|{line1}|{line2}";
                }
                else
                {
                    return $"1|{line1}";
                }
            }

            return null;            
        }

        public static bool IsButtonPressed(string type, int myIndex, string msg)
        {
            //if (type == "CCP-3201B")
            if (type != null)
            {
                var split = msg.Split('|');
                if (split.Length == 3)
                {
                    if (Int32.TryParse(split[0], out int tempIndex))
                    {
                        if (myIndex == tempIndex)
                        {
                            if (split[1] == "1")
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }
    }
}
