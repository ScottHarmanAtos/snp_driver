﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.json
{
    public class JGroupMenu
    {
        public string type;

        public string label;

        public int destination;

        public List<JPageMenu> options;
    }
}
