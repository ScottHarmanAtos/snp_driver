﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.json
{
    public class JOption
    {
        public string type;

        public string label;

        public int value;

        public int index;

        public object destination { get; internal set; }
    }
}
