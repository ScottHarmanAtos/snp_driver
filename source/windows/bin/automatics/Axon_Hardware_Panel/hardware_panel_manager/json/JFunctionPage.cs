﻿using instances_and_devicetypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.json
{
    public class JFunctionPage
    {
        public string type;

        public string label;

        public int index;

        public List<JOption> options;
    }
}
