﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.json
{
    public class JPageMenu
    {
        public string type;

        public string label;

        public List<JOption> options;

        public List<string> tags;

        public int landing_index;

        public int destination;
    }
}
