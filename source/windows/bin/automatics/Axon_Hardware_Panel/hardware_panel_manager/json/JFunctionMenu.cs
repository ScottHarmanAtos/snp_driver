﻿using instances_and_devicetypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.json
{
    public class JFunctionMenu
    {
        public string type;

        public string location;

        public string label;

        public List<JPageMenu> options;

    }
}
