﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.json
{
    public class JPanelMenu
    {
        public string type;

        public string label;

        public string location;

        public List<JGroupMenu> options;
    }
}

