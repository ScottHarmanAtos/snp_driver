﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.json
{
    public class JBasePanelType
    {
        public Dictionary<string, JComponentStyle> components_styles;

        public Dictionary<string, string> colours_translation;

        public Dictionary<string, int> buttons_translation;

        public int rows;

        public int columns;

        public bool top_button_separate;

        public int button_offsets;

        public int colour_offsets;

        public int text_offsets;

        public int top_button_index;
    }
}
