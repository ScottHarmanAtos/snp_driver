﻿using driver_template.helpers;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using driver_template.model.bncs;
using System.Collections.ObjectModel;
using helpers;

namespace driver_template.model
{
    //Driver Template Version:0.0.12.0

    public sealed class MainModel : IDisposable, INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// This is a list of all the devices that this driver controls
        /// </summary>
        public List<DeviceModel> Devices = new List<DeviceModel>();

        private ObservableCollection<PanelModel> panels = new ObservableCollection<PanelModel>();

        public ObservableCollection<PanelModel> Panels { get => panels; set => panels = value; }

        private ObservableCollection<TemplateModel> templates;

        public ObservableCollection<TemplateModel> Templates
        {
            get { return templates; }
            set
            {
                if (templates != value)
                {
                    templates = value;

                    OnPropertyChanged("Templates");
                }
            }
        }

        public MainModel()
        {
            //Here is an example of a place you the program may differ if you are running in simulation
            if (BNCSConfig.StartupArguments.Simulation)
            {
                //DO Simulation stuff
            }
            else
            {
                //DO Normal stuff

                Templates = new ObservableCollection<TemplateModel>();
                CreateTemplates(true);

                Mediator.Instance.Register((Object o) =>
                {
                    CreateTemplates((bool)o);

                }, ViewModelMessages.TemplatesRendered);

                var deviceModel = new DeviceModel();
                Devices.Add(deviceModel);
                deviceModel.CommsStatus = DeviceState.Connected;
                                
                foreach (var p in BNCSConfig.ClientInstances.Instances.ToList())
                {
                    Panels.Add(new PanelModel(p.Value));
                }

                /*var panelInstances = BNCSConfig.MainInstance.GetAllChildInstances().Where(w => w.Key.StartsWith("panel_"));
                foreach (var p in panelInstances)
                {
                    Panels.Add(new PanelModel(p.Value));
                }*/

                /*var panelInstances = BNCSConfig.MainInstance.GetAllChildInstances().Where(w => w.Key.StartsWith("panel_"));                
                foreach (var p in panelInstances)
                {
                    Panels.Add(new PanelModel(p.Value));
                }*/

                /*if (instances_and_devicetypes.Instance.TryGetInstance("17052/RTR/001", out var i))
                {
                    //Get a clientInstance slot by index
                    var (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(i, 1, 1);
                    if (Success)
                    {
                        //Cast the slot to a SlotRouter <- as we know it is one. You can use Type to check
                        var r = Slots[0].Slot as SlotRouter;

                        //Set the Source to 2
                        r.Set(2);

                        //Set a callback to get notifed when it changes
                        Slots[0].Slot.OnChange += Slot_OnChange;
                    }
                }*/
            }

            /* Example of how to get Slots
             * 
             * 
             * Get an index to send a RC to.
                //Get Instance
                if (instances_and_devicetypes.Instance.TryGetInstance("rtr_ip_facility", out var i))
                {
                    //Get a clientInstance slot by index
                    var (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(i, 1, 1);
                    if (Success)
                    {
                        //Cast the slot to a SlotRouter <- as we know it is one. You can use Type to check
                        var r = Slots[0].Slot as SlotRouter;

                        //Set the Source to 2
                        r.Set(2);

                        //Set a callback to get notifed when it changes
                        Slots[0].Slot.OnChange += Slot_OnChange;
                    }
                }


                //The callback
                private void Slot_OnChange(object sender, EventSlotChange e)
                {
                    logger.Error($"RR {e.Value}");
                }

                -------------------

                Get a database to send an RM to

                Get the Instance
                if (instances_and_devicetypes.Instance.TryGetInstance("rtr_sdi_fcut_x", out var i2))
                {
                    //Get the Database by index
                    var (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(i2, 1, 1, 0);
                    if (SuccessDb)
                    {
                        //Send the RM
                        Database[0].DatabaseSlot.Set("lkjsflasjdf");

                        //Set a callback to get notifed when it changes.
                        Database[0].DatabaseSlot.OnChange += DatabaseSlot_OnChange;
                    }
                }

                //Database callback come here        
                private void DatabaseSlot_OnChange(object sender, EventSlotChange e)
                {
                    logger.Error($"RM {e.Value}");
                }

                --------------------------------
                Set a Value in a managed Slot

                //Get the parameter for an instance
                BNCSConfig.MainInstance.DeviceType.TryGetParameter("Comms", out var p);

                // Get the slot by Instance and Parameter
                var (SuccessManaged, SlotsManaged) = BNCSConfig.ManagedInstances.GetSlot(BNCSConfig.MainInstance, p.Parameter);
                if (SuccessManaged)
                {
                    //Set the Slot
                    SlotsManaged.Set("lkjsflasjdf");

                    //Register for IW to the slot
                    SlotsManaged.OnChange += (s, e) => { Slot_OnChange1(s, e, p); };
                }

                //IW to the slot come here
                private void Slot_OnChange1(object sender, EventSlotChange e)
                {
                    logger.Error($"Managed: {e.Value}");

                    //This sets the slot on the callback, so it acts like an echo slot
                    e.Slot.Set(e.Value);
                }

                ----------------------------------

                //Get all the slot for an Instance

                var (SuccessManagedAll, SlotsManagedAll) = BNCSConfig.ManagedInstances.GetSlots(BNCSConfig.MainInstance);
                if (SuccessManagedAll)
                {
                    //Register for all of them
                    foreach (var param in SlotsManagedAll)
                    {
                        //Get the ParameterValue for each, this hold info of the instance and parameter
                        BNCSConfig.MainInstance.TryGetParameterValue(param.Param, out var pv);

                        //Add a closure to the callback so, you get the additional instance and parameter information in the callback
                        param.Slot.OnChange += (s, e) => { Slot_OnChange1(s, e, pv); };
                    }
                }

                //Callback with the additional ParameterValue info
                private void Slot_OnChange1(object sender, EventSlotChange e, ParameterValue pv)
                {
                    logger.Error($"Managed: {pv.Instance.Id}:{pv.Parameter.Name} = {e.Value}");
                    e.Slot.Set(e.Value);
                }

             */
        }

        public void CreateTemplates(bool flag)
        {
            Templates.Clear();
            foreach (var t in BNCSConfig.PanelTemplates)
            {
                string timestamp;  //exhaustively check for timestamps - if nothing, then panic and set it to N/A
                if (t.Value.First().SelectToken("timestamp") != null)
                    timestamp = DateTime.Parse(t.Value.First().SelectToken("timestamp").ToString()).ToString();
                else if (BNCSConfig.PanelFiles.TryGetValue(t.Key, out string filepath))
                    timestamp = System.IO.File.GetLastWriteTime(filepath).ToString();
                else
                    timestamp = "N/A";
                //Add timestamp values as recorded - rather than using the time of refreshing the file list.
                Templates.Add(new TemplateModel(t.Key, 
                    timestamp, //parse the datetime stored in the JSON to tidy it.
                    t.Value.First().SelectToken("backupTimestamp") != null ? 
                        DateTime.Parse(t.Value.First().SelectToken("backupTimestamp").ToString()).ToString() : "N/A")); //check that the token in the JSON isn't null, if it is - spit out 'n/a'
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void Dispose()
        {
            
        }
    }
}
