using driver_template.helpers;
using driver_template.json;
using driver_template.model.bncs;
using driver_template.model.mimic;
using driver_template.view;
using instances_and_devicetypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing.Text;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using System.Windows.Media;


namespace driver_template.model
{
    public class PanelModel : INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private Instance instance;

        public Instance Instance { get => instance; set => instance = value; }

        private Instance mfrInstance;

        private static Packager packager;
        public static Packager Packager { get => packager; set => packager = value; }

        private string title;

        public string Title { get => title; set => title = value; }

        private string template;

        public string Template
        {
            get { return template; }

            set
            {
                if (template != value)
                    template = value;
                OnPropertyChanged("Template");
            }
            
        }

        private string pathName;
        public string PathName
        {
            get { return pathName; }

            set
            {
                if (pathName != value)
                    pathName = value;
                OnPropertyChanged("PathName");
            }

        }

        private RoomMode roomMode = RoomMode.none;

        public RoomMode RoomMode { get => roomMode; set => roomMode = value; }

        private TielineMode tielineMode = TielineMode.none;

        public TielineMode TielineMode
        {
            get { return tielineMode; }
            set
            {
                if (tielineMode != value)
                {
                    tielineMode = value;

                    OnPropertyChanged("TielineMode");
                }
            }
        }

        private uint destinationNo;

        private string destination;

        public string Destination
        {
            get { return destination; }
            set
            {
                if (destination != value)
                {
                    destination = value;

                    OnPropertyChanged("Destination");
                }
            }
        }

        private JBasePanelType myBaseType;

        private Dictionary<int, object> buttonMapping;

        public Dictionary<int, object> ButtonMapping { get => buttonMapping; set => buttonMapping = value; }

        private MenuSources lastMenuSelected;

        public MenuSources LastMenuSelected
        {
            get { return lastMenuSelected; }
            set
            {
                if (lastMenuSelected != value)
                {
                    lastMenuSelected = value;
                }
            }
        }

        private MenuTop menuTop;

        public MenuTop MenuTop
        {
            get { return menuTop; }
            set
            {
                if (menuTop != value)
                {
                    menuTop = value;

                    OnPropertyChanged("MenuTop");
                }
            }
        }


        private ObservableCollection<ButtonModel> buttons;

        public ObservableCollection<ButtonModel> Buttons
        {
            get { return buttons; }
            set
            {
                if (buttons != value)
                {
                    buttons = value;

                    OnPropertyChanged("Buttons");
                }
            }
        }

        private int columns = 16;

        public int Columns
        {
            get { return columns; }
            set
            {
                if (columns != value)
                {
                    columns = value;

                    OnPropertyChanged("Columns");
                }
            }
        }

        private int rows = 2;

        public int Rows
        {
            get { return rows; }
            set
            {
                if (rows != value)
                {
                    rows = value;

                    OnPropertyChanged("Rows");
                }
            }
        }

        private List<JObject> templateModel;

        public List<JObject> TemplateModel
        {
            get { return templateModel; }
            set
            {
                if (templateModel != value)
                {
                    templateModel = value;
                }
            }
        }

        public int maxButtons { get; private set; }

        private BncsState bncsState = BncsState.RXOnly;

        public BncsState BncsState
        {
            get { return bncsState; }
            set
            {
                if (bncsState != value)
                {
                    bncsState = value;

                    if (bncsState == BncsState.TXRX)
                    {
                        if (ProcessingFinished)
                        {
                            txRxRefreshTimer.Stop();

                            foreach (var b in Buttons)
                            {
                                b.Resend();
                            }
                        }
                        else
                        {
                            txRxRefreshTimer.Start();
                        }
                    }
                }
            }
        }

        private bool processingFinished = false;

        public bool ProcessingFinished
        {
            get { return processingFinished; }
            set
            {
                if (processingFinished != value)
                {
                    processingFinished = value;
                }
            }
        }

        private Timer txRxRefreshTimer;

        public PanelModel(Instance instance)
        {
            this.Instance = instance;

            if(BNCSConfig.Packager != null)
                Packager = new Packager(BNCSConfig.Packager);

            Title = $"{Instance.AltId} | {Instance.Id}";

            txRxRefreshTimer = new Timer(2000);
            txRxRefreshTimer.Elapsed += TxRxRefreshTimer_Elapsed;            

            bncsState = BNCSStatusModel.Instance.CommsStatus;
            BNCSStatusModel.Instance.CommsStateChanged += CommsStateChanged;

            if (Instance.TryGetSetting("template_id", out var templateId))
            {

                //Set displayed Template name from the templates we evaluate to see which is the best match for the requirements.
                Template = EvaluateLoadedTemplates(templateId);
                if (BNCSConfig.PanelFiles.TryGetValue(Template, out string templatePath))
                    PathName = templatePath;
                else PathName = "N/A - Not loaded";

                if (Instance.TryGetSetting("room_mode", out var room_mode))
                {
                    switch (room_mode)
                    {
                        case "mfr":
                            {
                                RoomMode = RoomMode.MFR;
                            }
                            break;

                        case "pcr":
                            {
                                RoomMode = RoomMode.PCR;
                            }
                            break;

                        case "scr":
                            {
                                RoomMode = RoomMode.SCR;
                            }
                            break;

                        case "tx":
                            {
                                RoomMode = RoomMode.TX;
                            }
                            break;

                        case "tx_sup":
                            {
                                RoomMode = RoomMode.TX_SUP;
                            }
                            break;

                        case "live":
                            {
                                RoomMode = RoomMode.LIVE;
                            }
                            break;
                        case "dynamic":
                            { 
                                RoomMode = RoomMode.DYNAMIC;
                            }
                            break;

                    }

                    if (RoomMode != RoomMode.MFR)
                    {
                        if (Instance.TryGetSetting("tieline_mode", out var tieline_mode))
                        {
                            switch (tieline_mode)
                            {
                                case "tx":
                                    {
                                        TielineMode = TielineMode.TX;
                                    }
                                    break;

                                case "live":
                                    {
                                        TielineMode = TielineMode.LIVE;
                                    }
                                    break;

                                case "eng":
                                    {
                                        TielineMode = TielineMode.ENG;
                                    }
                                    break;

                                case "sup":
                                    {
                                        TielineMode = TielineMode.SUP;
                                    }
                                    break;
                                case "packager":
                                    {
                                        TielineMode = TielineMode.PACKAGER;
                                    }
                                    break;
                                case "ipx":
                                    {
                                        TielineMode = TielineMode.IPX;
                                    }
                                    break;
                            }
                        }
                    }

                    


                    if (BNCSConfig.PanelBaseTypes.TryGetValue(instance.DeviceType.Name, out myBaseType))
                    {
                        Rows = myBaseType.rows;
                        Columns = myBaseType.columns;
                        /*
                         * TODO:
                         * Lock out all buttons until a destination is selected on the panel, if operating in PACKAGER mode
                         */

                        var destObj = BNCSConfig.HardwarePanelsDestinations.panels.Find(f => f.id == Instance.Id);
                        if (destObj != null)
                        {

                            //test and see if my rtrInstance is a composite - if so, evaluate and see what my child instance is for my instanceNo router offset.
                            if (instances_and_devicetypes.Instance.TryGetInstance(destObj.destination.First().source_names, out var rtrInstance))
                            {
                                uint database = BNCSConfig.DestinationsNamesDatabase;
                                if (destObj.destination.Any())
                                {
                                    if (!UInt32.TryParse(destObj.destination.First().dest, out destinationNo))
                                    {
                                        destinationNo = 1;
                                        BNCSConfig.destinationNo = destinationNo;
                                        //disable buttons until destination selected

                                        logger.Warn($"Panel {instance.Id}, {instance.AltId} destination {destObj.destination.First().dest} in hardware_panel_destinations.xml is not a valid number.  Locking panel until destination is chosen.");
                                    }

                                    bool SuccessDb;
                                    List<(uint Index, SlotDatabase DatabaseSlot)> Database;

                                    bool isPackager = Packager.IsPackager?.Invoke(rtrInstance) ?? false;
                                    if (isPackager)
                                    {
                                        //var infoDriverPackager = BNCSConfig.Packager;

                                        //return a compbination of composite instance and modulo destintionNumber - failing?
                                        Packager.RouterDestination instanceNo = Packager.getInstanceFromDestination(rtrInstance, destinationNo);
                                        (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(instanceNo.router, instanceNo.destination, 1, database);
                                    }
                                    else
                                    {
                                        (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(rtrInstance, destinationNo, 1, database);
                                    }
                                            

                                    //Get the Database by index
                                        
                                    if (SuccessDb)
                                    {

                                        //Set a callback to get notifed when it changes.
                                        Database[0].DatabaseSlot.OnChange += DatabaseSlot_OnChange;

                                        if (BNCSConfig.PanelTemplates.TryGetValue(Template, out var myTemplateModel))
                                        {
                                            TemplateModel = myTemplateModel;

                                            UpdateDestination();

                                            InitialiseButtons();

                                            PrepareTielineDetectionMode();

                                            ProcessingFinished = true;
                                        }
                                        else
                                        {
                                            logger.Error($"Panel {instance.Id}, {instance.AltId} failed to find a template model for: {Template}");

                                            TemplateModel = new List<JObject>();

                                            UpdateDestination();

                                            InitialiseButtons();

                                            PrepareTielineDetectionMode();

                                            ProcessingFinished = true;
                                        }
                                    }
                                    else
                                    {
                                        logger.Error($"Panel {instance.Id}, {instance.AltId} router_instance {destObj.destination.First().source_names} destination {destObj.destination.First().dest} cannot retrieve a valid slot object");
                                    }


                                }
                                else
                                {
                                    logger.Error($"Panel {instance.Id}, {instance.AltId} destination {destObj} in hardware_panel_destinations.xml is missing required destination");
                                }
                            }
                            else
                            {
                                logger.Error($"Panel {instance.Id}, {instance.AltId} failed to find router instance {destObj.destination.First().source_names}");
                            }                            
                        }
                        else
                        {
                            logger.Warn($"Panel {instance.Id}, {instance.AltId} failed to find controlled destination in hardware_panel_destinations.xml");

                        }                        
                    }
                    else
                    {
                        logger.Error($"Panel {instance.Id}, {instance.AltId} failed to find a base panel type for my device type: {instance.DeviceType.Name}");
                    }
                }
            }
            else
            {
                logger.Error($"Panel {instance.Id}, {instance.AltId} failed to find a template_id setting");
            }
        }

        private string EvaluateLoadedTemplates(string templateId)
        {

            Dictionary<DateTime, string> templatesByTimestamp = new Dictionary<DateTime, string>();
            Dictionary<DateTime, string> templatesByBackupTimestamp = new Dictionary<DateTime, string>();
            foreach (var templateValue in BNCSConfig.PanelTemplates.Where(x => x.Key.StartsWith(templateId)))
            {
                if (Instance.TryGetSetting("template_id", out var originalTemplate))
                    templateId = templateId == originalTemplate ? templateId : originalTemplate;
                //List<JObject> template = JsonConvert.DeserializeObject<List<JObject>>(templateValue.Value);
                DateTime templatebackupTimestamp = DateTime.Parse(templateValue.Value.First().SelectToken("backupTimestamp") != null ? templateValue.Value.First().SelectToken("backupTimestamp").ToString() : DateTime.MinValue.ToString());
                DateTime templateTimestamp = DateTime.Parse(templateValue.Value.First().SelectToken("timestamp").ToString());
                if (templateTimestamp != null && templatebackupTimestamp == DateTime.MinValue)
                {
                    //logger.Info($"Converted {templateDate} to {convertedDate.Kind} time {convertedDate}");
                    templatesByTimestamp.Add(templateTimestamp, templateValue.Key);
                }
                else if (templateTimestamp != null && templatebackupTimestamp != null && !templatesByBackupTimestamp.ContainsKey(templatebackupTimestamp))
                {
                    //logger.Info($"Converted {templateDate} to {convertedDate.Kind} time {convertedDate}");
                    templatesByBackupTimestamp.Add(templatebackupTimestamp, templateValue.Key);
                }
            }
            //if (BNCSConfig.PanelTemplates.TryGetValue(templateId, out var myTemplateExists))
             //   return templateId;
            if (templatesByTimestamp.Count > 0)
                return templatesByTimestamp[templatesByTimestamp.Keys.Max()];
            else if (templatesByBackupTimestamp.Count > 0)
                return templatesByBackupTimestamp[templatesByBackupTimestamp.Keys.Max()];
            else return templateId;
        }

        private void TxRxRefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (ProcessingFinished)
            {
                if (BncsState == BncsState.TXRX)
                {
                    txRxRefreshTimer.Stop();

                    foreach (var b in Buttons)
                    {
                        b.Resend();
                    }
                }                
            }            
        }

        private void CommsStateChanged(object sender, EventArgs e)
        {
            BncsState = BNCSStatusModel.Instance.CommsStatus;            
        }

        private void PrepareTielineDetectionMode()
        {
            if (RoomMode == RoomMode.MFR)
            {
                var (SuccessClient, SlotClient) = BNCSConfig.ClientInstances.GetSlots(mfrInstance);
                if (SuccessClient)
                {
                    var roleObj = SlotClient.Find(f => f.Param.Name == "role");
                    var SlotMfrRole = roleObj.Slot;

                    SlotMfrRole.OnChange += (s, e) => { SlotMfrRole_OnChange(s, e, roleObj.Param); };
                }
            }
            else
            {
                foreach (var mV in ButtonMapping.Values)
                {
                    if (mV is MenuSources menu)
                    {
                        menu.SetTielineMode(TielineMode);
                    }
                }
            }
        }

        private void SlotMfrRole_OnChange(object s, EventSlotChange e, Parameter param)
        {
            TielineMode newTielineMode = TielineMode.none;
            if (TielineMode.TryParse<TielineMode>(e.Value, out newTielineMode))
            {
                TielineMode = newTielineMode;
            }

            logger.Info($"Panel {instance.Id}, {instance.AltId}: MFR: {mfrInstance.Id}, {mfrInstance.AltId}, role: {TielineMode.ToString()}");

            foreach (var mV in ButtonMapping.Values)
            {
                if (mV is MenuSources menu)
                {
                    menu.SetTielineMode(TielineMode);
                }
            }
        }

        private void DatabaseSlot_OnChange(object sender, EventSlotChange e)
        {
            Destination = $"{destinationNo}: {e.Value}";
        }

        private void InitialiseButtons()
        {
            Buttons = new ObservableCollection<ButtonModel>();

            maxButtons = Rows * Columns;
            for (int i = 1; i <= maxButtons; i++)
            {
                Buttons.Add(new ButtonModel(i, Instance, myBaseType));
            }

            var topButtonModel = new ButtonModel(myBaseType.top_button_index, Instance, myBaseType, false);

            ButtonMapping = new Dictionary<int, object>();

            MenuTop = new MenuTop(myBaseType.top_button_index, "TOP", "Silver", topButtonModel);
            ButtonMapping.Add(myBaseType.top_button_index, MenuTop);

            MenuTop.MyTopButtonPressed += MyTopButtonPressed;

            ProcessTemplate(TemplateModel);
        }

        private void UpdateDestination(bool destinationUpdate = true)
        {
            //PanelState(false);
            var destObj = BNCSConfig.HardwarePanelsDestinations.panels.Find(f => f.id == Instance.Id);
            if (destObj != null)
            {

                //test and see if my rtrInstance is a composite - if so, evaluate and see what my child instance is for my instanceNo router offset.
                    uint database = BNCSConfig.DestinationsNamesDatabase;
                    if (instances_and_devicetypes.Instance.TryGetInstance(destObj.destination.First().source_names, out var rtrInstance))
                    {
                        bool SuccessDb;
                        List<(uint Index, SlotDatabase DatabaseSlot)> Database;

                        bool isPackager = Packager.IsPackager?.Invoke(rtrInstance) ?? false;
                        if (isPackager)
                        {
                            //var infoDriverPackager = BNCSConfig.Packager;

                            //return a compbination of composite instance and modulo destintionNumber - failing?
                            Packager.RouterDestination instanceNo = Packager.getInstanceFromDestination(rtrInstance, destinationNo);
                            (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(instanceNo.router, instanceNo.destination, 1, database);
                        }
                        else
                        {
                            (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(rtrInstance, destinationNo, 1, database);
                        }

                    }
                }
            foreach (var dest in TemplateModel)
            {
                JGroupMenu menu = JsonConvert.DeserializeObject<JGroupMenu>(dest.ToString());
                if (menu.type == "group" || menu.type == "function")
                {


                    if (menu.destination > 0)
                    {
                        Destination = $"{menu.destination}";
                        destinationNo = (uint)menu.destination;
                        destObj.destination.First().dest = destinationNo.ToString();
                        destinationUpdate = false;

                    }
                }
                BNCSConfig.destinationNo = destinationNo;
               
                }

            }

        private void ProcessTemplate(List<JObject> template)
        {
            int index = 1;
            while(index <= (myBaseType.rows * myBaseType.columns))
            {
                if (index <= template.Count())
                {
                    foreach (var item in template)
                    {

                        try
                        {
                            JButton button = JsonConvert.DeserializeObject<JButton>(item.ToString());
                            if (button != null)
                            {
                                if (button.type == "group")
                                {
                                    try
                                    {
                                        JGroupMenu menu = JsonConvert.DeserializeObject<JGroupMenu>(item.ToString());
                                        if (menu != null)
                                        {
                                            if (myBaseType.components_styles.TryGetValue(button.type, out var myStyle))
                                            {
                                                List<PageMenuSources> myOptions = new List<PageMenuSources>();

                                                int optionIndex = 1;
                                                foreach (var o in menu.options)
                                                {
                                                    if (myBaseType.components_styles.TryGetValue(o.type, out var optionStyle))
                                                    {
                                                        if (o.type == "page")
                                                        {
                                                            var myPage = GeneratePageMenu(optionIndex, o, optionStyle, false);
                                                            if (myPage != null)
                                                            {
                                                                myOptions.Add(myPage);
                                                            }
                                                        }
                                                        else if (o.type == "blank")
                                                        {
                                                            myOptions.Add(new PageMenuSourcesBlank(optionIndex, string.Empty, optionStyle.background_colour, Buttons[optionIndex - 1], null, false, o.landing_index, o.tags));
                                                        }

                                                        optionIndex++;
                                                    }
                                                    else
                                                    {
                                                        logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {o.type}");
                                                        break;
                                                    }
                                                }

                                                GroupMenuSources myMenu = new GroupMenuSources(
                                                    index,
                                                    menu.label,
                                                    myStyle.background_colour,
                                                    Buttons[index - 1],
                                                    myOptions,
                                                    RoomMode
                                                );

                                                ButtonMapping.Add(index, myMenu);

                                                myMenu.MyMenuSelected += MenuSelected;
                                                myMenu.OptionSelected += OptionSelected;
                                            }
                                            else
                                            {
                                                logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {button.type}");
                                            }
                                        }
                                    }
                                    catch (Exception exp_menu)
                                    {
                                        logger.Error($"Exception deserialising JGroupMenu, exp: {exp_menu.ToString()}");
                                    }
                                }
                                else if (button.type == "contextual_group")
                                {
                                    try
                                    {
                                        JGroupMenu menu = JsonConvert.DeserializeObject<JGroupMenu>(item.ToString());
                                        if (menu != null)
                                        {
                                            if (myBaseType.components_styles.TryGetValue(button.type, out var myStyle))
                                            {
                                                List<PageMenuSources> myOptions = new List<PageMenuSources>();

                                                int optionIndex = 1;
                                                foreach (var o in menu.options)
                                                {
                                                    if (myBaseType.components_styles.TryGetValue(o.type, out var optionStyle))
                                                    {
                                                        if (o.type == "page")
                                                        {
                                                            var myPage = GeneratePageMenu(optionIndex, o, optionStyle, false);
                                                            if (myPage != null)
                                                            {
                                                                myOptions.Add(myPage);
                                                            }
                                                        }
                                                        else if (o.type == "blank")
                                                        {
                                                            myOptions.Add(new PageMenuSourcesBlank(optionIndex, string.Empty, optionStyle.background_colour, Buttons[optionIndex - 1], null, false, o.landing_index, o.tags));
                                                        }

                                                        optionIndex++;
                                                    }
                                                    else
                                                    {
                                                        logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {o.type}");
                                                        break;
                                                    }
                                                }

                                                if (RoomMode == RoomMode.MFR)
                                                {
                                                    if (Instance.TryGetSetting("mfr_instance", out string myMfrInstance))
                                                    {
                                                        if (instances_and_devicetypes.Instance.TryGetInstance(myMfrInstance, out mfrInstance))
                                                        {

                                                        }
                                                        else
                                                        {
                                                            logger.Error($"Panel: {Instance.Id}, {Instance.AltId}, cannot find associated mfr_instance: {myMfrInstance}");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        logger.Error($"Panel: {Instance.Id}, {Instance.AltId}, cannot find setting mfr_instance but room_mode is mfr");
                                                    }
                                                }

                                                GroupMenuSourcesContextual myMenu = new GroupMenuSourcesContextual(
                                                    index,
                                                    menu.label,
                                                    myStyle.background_colour,
                                                    Buttons[index - 1],
                                                    myOptions,
                                                    RoomMode,
                                                    mfrInstance
                                                );

                                                myMenu.MyTopButtonPressed += MyTopButtonPressed;

                                                ButtonMapping.Add(index, myMenu);

                                                myMenu.MyMenuSelected += MenuSelected;
                                                myMenu.OptionSelected += OptionSelected;
                                            }
                                            else
                                            {
                                                logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {button.type}");
                                            }
                                        }
                                    }
                                    catch (Exception exp_menu)
                                    {
                                        logger.Error($"Exception deserialising JGroupMenu, exp: {exp_menu.ToString()}");
                                    }
                                }
                                else if (button.type == "page")
                                {
                                    if (myBaseType.components_styles.TryGetValue(button.type, out var myStyle))
                                    {
                                        var page = GeneratePageMenu(index, item, myStyle, true);
                                        if (page != null)
                                        {
                                            ButtonMapping.Add(index, page);

                                            page.MyMenuSelected += MenuSelected;
                                            page.OptionSelected += OptionSelected;
                                        }
                                    }
                                    else
                                    {
                                        logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {button.type}");
                                    }
                                }
                                else if (button.type == "function") //room function
                                {
                                    try
                                    {
                                        JGroupMenu menu = JsonConvert.DeserializeObject<JGroupMenu>(item.ToString());
                                        if (menu != null)
                                        {
                                            if (myBaseType.components_styles.TryGetValue(button.type, out var myStyle))
                                            {
                                                List<PageMenuSources> myOptions = new List<PageMenuSources>();

                                                if (menu.destination > 0)
                                                    UpdateDestination();

                                                int optionIndex = 1;
                                                foreach (var o in menu.options)
                                                {
                                                    if (myBaseType.components_styles.TryGetValue(o.type, out var optionStyle))
                                                    {
                                                        if (o.type == "page")
                                                        {
                                                            var myPage = GeneratePageMenu(optionIndex, o, optionStyle, false);
                                                            if (myPage != null)
                                                            {
                                                                myOptions.Add(myPage);
                                                            }
                                                        }
                                                        else if (o.type == "blank")
                                                        {
                                                            myOptions.Add(new PageMenuSourcesBlank(optionIndex, string.Empty, optionStyle.background_colour, Buttons[optionIndex - 1], null, false, o.landing_index, o.tags));

                                                        }

                                                        optionIndex++;
                                                    }
                                                    else
                                                    {
                                                        logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {o.type}");
                                                        break;
                                                    }
                                                }

                                                GroupMenuSources myMenu = new GroupMenuSources(
                                                    index,
                                                    menu.label,
                                                    myStyle.background_colour,
                                                    Buttons[index - 1],
                                                    myOptions,
                                                    RoomMode
                                                );

                                                ButtonMapping.Add(index, myMenu);

                                                myMenu.MyMenuSelected += MenuSelected;
                                                myMenu.OptionSelected += OptionSelected;
                                            }
                                            else
                                            {
                                                logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {button.type}");
                                            }
                                        }
                                    }
                                    catch (Exception exp_menu)
                                    {
                                        logger.Error($"Exception deserialising JGroupMenu, exp: {exp_menu.ToString()}");
                                    }
                                }
                                else if (button.type == "blank")
                                {
                                    if (myBaseType.components_styles.TryGetValue(button.type, out var myStyle))
                                    {
                                        var blank = new PageMenuSourcesBlank(index, string.Empty, myStyle.background_colour, Buttons[index - 1], null, false, 0, null);

                                        ButtonMapping.Add(index, blank);
                                    }
                                    else
                                    {
                                        logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {button.type}");
                                    }
                                }
                            }
                            /*
                             * Need to fill root buttons with blanks - need to rethink this
                             */




                            index++;
                        }

                        catch (Exception exp_button)
                        {
                            logger.Error($"Exception deserialising JButton, exp: {exp_button.ToString()}");
                        }

                    }
                }
            else if (index > template.Count() && index <= (myBaseType.rows * myBaseType.columns))
            {
                if (myBaseType.components_styles.TryGetValue("blank", out var myStyle))
                {
                    var blank = new PageMenuSourcesBlank(index, string.Empty, myStyle.background_colour, Buttons[index - 1], null, false, 0, null);

                    ButtonMapping.Add(index, blank);
                }
                    index++;
                }

            }
        }

        private PageMenuSources GeneratePageMenu(int index, JObject model, JComponentStyle style, bool isOnTopLevel)
        {
            try
            {
                JPageMenu menu = JsonConvert.DeserializeObject<JPageMenu>(model.ToString());
                if (menu != null)
                {
                    List<Option> myOptions = new List<Option>();
                    //List<DestinationOption> myDestinationOptions = new List<DestinationOption>();

                    for (int optionIndex = 1; optionIndex <= maxButtons; optionIndex++)
                    {
                        if (menu.options.First().index > 0)
                        {
                            foreach (var o in menu.options.Where(x => x.index == optionIndex))
                            {
                                if (myBaseType.components_styles.TryGetValue(o.type, out var optionStyle) && o.index == optionIndex)
                                {
                                    if (o.type == "source")
                                    {
                                        myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Source));
                                    }
                                    else if (o.type == "blank")
                                    {
                                        myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, optionStyle.background_colour, optionStyle.background_colour, Buttons[optionIndex - 1], 0, OptionType.Blank));
                                    }
                                    else if (o.type == "destination")
                                    {
                                        myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Destination));

                                    }
                                    else if (o.type == "destinationlocal")
                                    {
                                        myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.DestinationLocal));
                                    }
                                    else if (o.type == "tally")
                                    {
                                        myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Tally));
                                    }
                                    else if (o.type == "back")
                                    {
                                        myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Back));
                                    }
                                    else if (o.type == "pageselect")
                                    {
                                        myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.PageSelect));
                                    }
                                    else if (o.type == "audiomode")
                                    {
                                        myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.AudioMode));
                                    }

                                }

                            }
                        }
                        else
                        {
                            foreach (var o in menu.options)
                            {
                                if (myBaseType.components_styles.TryGetValue(o.type, out var optionStyle) && o.index == optionIndex)
                                {
                                    if (o.type == "source")
                                    {
                                        myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Source));
                                    }
                                    else if (o.type == "blank")
                                    {
                                        myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, optionStyle.background_colour, optionStyle.background_colour, Buttons[optionIndex - 1], 0, OptionType.Blank));
                                    }
                                    else if (o.type == "destination")
                                    {
                                        myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Destination));

                                    }
                                    else if (o.type == "destinationlocal")
                                    {
                                        myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.DestinationLocal));
                                    }
                                    else if (o.type == "tally")
                                    {
                                        myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Tally));
                                    }
                                    else if (o.type == "back")
                                    {
                                        myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Back));
                                    }
                                    else if (o.type == "pageselect")
                                    {
                                        myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.PageSelect));
                                    }
                                    else if (o.type == "audiomode")
                                    {
                                        myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.AudioMode));
                                    }
                                    else
                                    {
                                        myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, "black", "black", Buttons[optionIndex - 1], 0, OptionType.Blank));
                                    }

                                    //optionIndex++;
                                }


                                else
                                {
                                    logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {o.type}");
                                    break;
                                }

                            }
                        }
                        if (myOptions.Count < optionIndex)
                        {
                            if (myBaseType.components_styles.TryGetValue("blank", out var blankoptionStyle))
                                myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, blankoptionStyle.background_colour, blankoptionStyle.background_colour, Buttons[optionIndex - 1], 0, OptionType.Blank));
                        }
                    }

                    ButtonModel mappedButton;

                    if (RoomMode == RoomMode.MFR)
                    {
                        if (menu.landing_index > 0)
                            mappedButton = Buttons[menu.landing_index - 1];
                        else
                            mappedButton = Buttons[index - 1];
                    }
                    else
                    {
                        mappedButton = Buttons[index - 1];
                    }

                    PageMenuSources mySourceMenu = new PageMenuSources(
                        index,
                        menu.label,
                        style.background_colour,
                        mappedButton,
                        myOptions,
                        Instance.Id,
                        isOnTopLevel,
                        menu.landing_index,
                        menu.tags
                    );
                    return mySourceMenu;
                }
                else
                    return null;
            }
            catch (Exception exp_menu)
            {
                logger.Error($"Exception deserialising JPageMenu, exp: {exp_menu.ToString()}");
                return null;
            }
        }

        private PageMenuSources GeneratePageMenu(int index, JPageMenu menu, JComponentStyle style, bool isOnTopLevel)
        {
            if (menu != null && menu.options.Count > 0)
            {
                List<Option> myOptions = new List<Option>();
                //List<DestinationOption> myDestOptions = new List<DestinationOption>();
                //can I add a new dest option here?
                for (int optionIndex = 1; optionIndex <= maxButtons; optionIndex++)
                {
                    if (menu.options.First().index > 0)
                    {
                        foreach (var o in menu.options.Where(x => x.index == optionIndex))
                        {
                            if (myBaseType.components_styles.TryGetValue(o.type, out var optionStyle) && o.index == optionIndex)
                            {
                                if (o.type == "source")
                                {
                                    myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Source));
                                }
                                else if (o.type == "blank")
                                {
                                    myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, optionStyle.background_colour, optionStyle.background_colour, Buttons[optionIndex - 1], 0, OptionType.Blank));
                                }
                                else if (o.type == "destination")
                                {
                                    myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Destination));

                                }
                                else if (o.type == "destinationlocal")
                                {
                                    myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.DestinationLocal));
                                }
                                else if (o.type == "tally")
                                {
                                    myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Tally));
                                }
                                else if (o.type == "back")
                                {
                                    myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Back));
                                }
                                else if (o.type == "pageselect")
                                {
                                    myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.PageSelect));
                                }
                                else if (o.type == "audiomode")
                                {
                                    myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.AudioMode));
                                }
                                /*else
                                {
                                    myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, optionStyle.background_colour, optionStyle.background_colour, Buttons[optionIndex - 1], 0));
                                }*/

                                //optionIndex++;
                            }


                            else
                            {
                                logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {o.type}");
                                break;
                            } 
                        }

                    }
                    else
                    {
                        foreach (var o in menu.options)
                        {
                            if (myBaseType.components_styles.TryGetValue(o.type, out var optionStyle) && o.index == optionIndex)
                            {
                                if (o.type == "source")
                                {
                                    myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Source));
                                }
                                else if (o.type == "blank")
                                {
                                    myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, optionStyle.background_colour, optionStyle.background_colour, Buttons[optionIndex - 1], 0, OptionType.Blank));
                                }
                                else if (o.type == "destination")
                                {
                                    myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Destination));

                                }
                                else if (o.type == "destinationlocal")
                                {
                                    myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.DestinationLocal));
                                }
                                else if (o.type == "tally")
                                {
                                    myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Tally));
                                }
                                else if (o.type == "back")
                                {
                                    myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.Back));
                                }
                                else if (o.type == "pageselect")
                                {
                                    myOptions.Add(new SourceOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.PageSelect));
                                }
                                else if (o.type == "audiomode")
                                {
                                    myOptions.Add(new DestinationOption(optionIndex, o.label, optionStyle.background_colour, optionStyle.background_colour_active, Buttons[optionIndex - 1], o.value, OptionType.AudioMode));
                                }
                                else
                                {
                                    myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, "black", "black", Buttons[optionIndex - 1], 0, OptionType.Blank));
                                }

                                //optionIndex++;
                            }


                            else
                            {

                                logger.Error($"Panel {instance.Id}, {instance.AltId}, devType: {instance.DeviceType.Name} failed to find a style for my component of type: {o.type}");
                                break;
                            }

                        }


                    }
                    if (myOptions.Count < optionIndex)
                    {
                        if (myBaseType.components_styles.TryGetValue("blank", out var blankoptionStyle))
                            myOptions.Add(new SourceOptionBlank(optionIndex, string.Empty, blankoptionStyle.background_colour, blankoptionStyle.background_colour, Buttons[optionIndex - 1], 0, OptionType.Blank));
                    }
                }

                        ButtonModel mappedButton;

                if (RoomMode == RoomMode.MFR)
                {
                    if (menu.landing_index > 0)
                        mappedButton = Buttons[menu.landing_index - 1];
                    else
                        mappedButton = Buttons[index - 1];
                }
                else
                {
                    mappedButton = Buttons[index - 1];
                }

                PageMenuSources myMenu = new PageMenuSources(
                    index,
                    menu.label,
                    style.background_colour,
                    mappedButton,
                    myOptions,
                    Instance.Id,
                    isOnTopLevel,
                    menu.landing_index,
                    menu.tags
                );

                return myMenu;
            }
            else
                return null;
        }

        private void MyTopButtonPressed(object sender, TopMenuPressedEventArgs e)
        {
            //logger.Trace($"Still waiting");
            foreach (var mV in ButtonMapping.Values)
            {
                if (mV is MenuSources menu)
                {
                    menu.ChangeCanBeSelected(true);
                    menu.OpenMenu(false);
                }
            }

            foreach (var mV in ButtonMapping.Values)
            {
                if (mV is MenuSources menu)
                {
                    menu.Render();
                }
            }

            LastMenuSelected = null;
        }
      

        private void OptionSelected(object sender, OptionSelectedEventArgs e)         
        {
            if (sender is SourceOption source)
                if (source.DestinationType != OptionType.Source)
                {
                    TallyUpdate(source.Label);
                }
                else if (source.Type == OptionType.Back)
                {
                    logger.Debug("Go Back Young Man");
                    if (ButtonMapping.ElementAt(source.MyValue).Value is GroupMenuSources functiongroup)
                    {

                        //FIXME: Clear currently selected sources and destinations in my functiongroup so that my source and destination referencesare correct.
                        LastMenuSelected = null;
                        MenuSelected(functiongroup, new MenuSelectedEventArgs());
                    }
                }
            if (sender is PageMenuSources sourceMenu)
                if (sourceMenu.SelectedOption != null)
                {
                    if (sourceMenu.SelectedOption.DestinationType != OptionType.Source)
                    {
                        TallyUpdate(sourceMenu.SelectedOption.Label);
                    }
                }
        }

        public void TallyUpdate(string label)
        {
            // TODO: Untidy - must be a nicer way to write this)
            foreach (var mV in ButtonMapping.Values.Where(x => x is GroupMenuSources || x is PageMenuSources))
{
            if (mV is GroupMenuSources menu)
            {
                    if (menu.Options != null)
                    {
                        foreach (var menuOption in menu.Options)
                        {
                            if (menuOption is PageMenuSources subpage)
                                foreach (var sp in subpage.Options ?? Enumerable.Empty<Option>()) 
                                {
                                    if (sp is SourceOption source)
                                        if (source.Background == "White")
                                            source.Label = label;
                                }
                        }
                    }
                    if (menu.Background == "White")
                        menu.Label = label;

            }
            else if (mV is PageMenuSources pagemenu)
                {
                    if (pagemenu.Options != null)
                    {
                        foreach (var menuOption in pagemenu.Options ?? Enumerable.Empty<Option>())
                        {
                            if (menuOption is SourceOption source)
                                if (source.Background == "White")
                                    source.Label = label;
                        }
                    }
                    if (pagemenu.Background == "White")
                        pagemenu.Label = label;

                }
            }
        }

        private void MenuSelected(object sender, MenuSelectedEventArgs e)
        {
            MenuSources newMenu = sender as MenuSources;

            if (LastMenuSelected != null)
            {
                if (!LastMenuSelected.Equals(newMenu))
                {
                    LastMenuSelected.OpenMenu(false);
                    LastMenuSelected.UnRender();

                    LastMenuSelected = newMenu;

                    LastMenuSelected.OpenMenu(true);
                    LastMenuSelected.Render();
                }
            }
            else
            {
                foreach (var mV in ButtonMapping.Values)
                {
                    if (mV is MenuSources menu)
                    {
                        menu.ChangeCanBeSelected(false);
                        menu.OpenMenu(false);
                        menu.UnRender();                        
                    }
                }

                LastMenuSelected = newMenu;

                newMenu.ChangeCanBeSelected(true);
                newMenu.OpenMenu(true);
                newMenu.Render();
            }
        }

        public ICommand InspectPanel
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    PanelMimic.ShowPanelMimic(this);
                });
                return command;
            }
        }

        public ICommand ReloadPanel
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    DisposeChildren();
                    ReloadTemplate();
                });
                return command;
            }
        }

        public ICommand ResendPanel
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    if (ProcessingFinished && BncsState == BncsState.TXRX)
                    {
                        foreach (var b in Buttons)
                        {
                            b.Resend();
                        }
                    }                    
                });
                return command;
            }
        }

        private void DisposeChildren()
        {
            if(MenuTop != null)
                MenuTop.MyTopButtonPressed -= MyTopButtonPressed;

            if (ButtonMapping != null)
            {
                foreach (var mV in ButtonMapping.Values)
                {
                    if (mV is PageMenuSources pms)
                    {
                        pms.MyMenuSelected -= MenuSelected;
                        pms.OptionSelected -= OptionSelected;
                    }
                    else if (mV is GroupMenuSources gms)
                    {
                        gms.MyMenuSelected -= MenuSelected;
                        gms.OptionSelected -= OptionSelected;
                    }
                    else if (mV is GroupMenuSourcesContextual gmsc)
                    {
                        gmsc.MyMenuSelected -= MenuSelected;
                        gmsc.OptionSelected -= OptionSelected;
                    }
                }
                //}            

                foreach (IDisposable item in ButtonMapping.Values)
                {
                    item.Dispose();
                }

                MenuTop.Dispose();

                foreach (IDisposable item in Buttons)
                {
                    item.Dispose();
                }

                ButtonMapping.Clear();
                Buttons.Clear();

                ProcessingFinished = false;
            }
        }

        private void ReloadTemplate()
        {
            if (Instance.TryGetSetting("template_id", out var templateId))
            {   
                //Set displayed Template name from the templates we evaluate to see which is the best match for the requirements.
                Template = EvaluateLoadedTemplates(templateId);
                if (BNCSConfig.PanelFiles.TryGetValue(Template, out string templatePath))
                    PathName = templatePath;
                else PathName = "N/A - Not loaded";

                if (BNCSConfig.PanelTemplates.TryGetValue(Template, out var myTemplateModel))
                {
                    TemplateModel = myTemplateModel;

                    maxButtons = Rows * Columns;
                    for (int i = 1; i <= maxButtons; i++)
                    {
                        Buttons.Add(new ButtonModel(i, Instance, myBaseType));
                    }

                    var topButtonModel = new ButtonModel(myBaseType.top_button_index, Instance, myBaseType, false);

                    MenuTop = new MenuTop(myBaseType.top_button_index, "TOP", "Silver", topButtonModel);
                    ButtonMapping.Add(myBaseType.top_button_index, MenuTop);

                    MenuTop.MyTopButtonPressed += MyTopButtonPressed;

                    ProcessTemplate(TemplateModel);

                    foreach (var mV in ButtonMapping.Values)
                    {
                        if (mV is MenuSources menu)
                        {
                            menu.SetTielineMode(TielineMode);
                        }
                    }

                    UpdateDestination();

                    ProcessingFinished = true;
                }
                else
                {
                    logger.Error($"Panel {instance.Id}, {instance.AltId} failed to find a template model for: {Template}");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
