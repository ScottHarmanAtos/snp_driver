using driver_template.helpers;
using driver_template.json;
using driver_template.model.bncs;
using driver_template.xml;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    public delegate void MenuSelected(object sender, MenuSelectedEventArgs e);

    public class PageMenuSources : MenuSources, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event MenuSelected MyMenuSelected;

        public event OptionSelected OptionSelected;

        public event OptionSelected DestinationSelected;

        public SourceOption AudioModeSelected { get; private set; }

        protected ButtonModel button;

        private bool canBeSelected = false;

        public bool CanBeSelected
        {
            get { return canBeSelected; }
            set
            {
                if (canBeSelected != value)
                {
                    canBeSelected = value;
                }
            }
        }

        private bool menuOpened;

        public bool MenuOpened
        {
            get { return menuOpened; }
            set
            {
                if (menuOpened != value)
                {
                    menuOpened = value;

                    if (Options != null)
                    {
                        if (menuOpened)
                        {
                            foreach (var option in Options)
                            {
                                if (option is SourceOption source)
                                    source.CanBeSelected = true;
                                else if (option is DestinationOption dest)
                                    dest.CanBeSelected = true;
                            }
                        }
                        else
                        {
                            foreach (var option in Options)
                            {
                                if (option is SourceOption source)
                                {
                                    source.UnRender();
                                    source.Selected = false;
                                    source.CanBeSelected = false;
                                }
                               else if (option is DestinationOption dest)
                                {
                                    dest.UnRender();
                                    dest.Selected = false;
                                    dest.CanBeSelected = false;
                                }
                            }
                        }
                    }                    
                }
            }
        }


        private List<Option> options;

        public List<Option> Options
        {
            get { return options; }
            set
            {
                if (options != value)
                {
                    options = value;
                }
            }
        }

        private SourceOption selectedOption;

        public SourceOption SelectedOption
        {
            get { return selectedOption; }
            set
            {
                if (selectedOption != value)
                {
                    selectedOption = value;
                }
            }
        }

        private DestinationOption selectedDestination;

        public DestinationOption SelectedDestination
        {
            get { return selectedDestination; }
            set
            {
                if (selectedDestination != value)
                {
                    selectedDestination = value;
                }
            }
        }

        private string actionType;
        public string InstanceID
        {
            get { return actionType; }
            set
            {
                if (actionType != value)
                {
                    actionType = value;
                }
            }
        }

        private XhwPanelConfig destConfigModel;
        public XhwPanelConfig DestConfigModel
        {
            get { return destConfigModel; }
            set
            {
                if (destConfigModel != value)
                {
                    destConfigModel = value;
                }
            }
        }

        private TielineMode tielineMode = TielineMode.TX;

        public TielineMode TielineMode
        {
            get { return tielineMode; }
            set
            {
                if (tielineMode != value)
                {
                    tielineMode = value;
                }
            }
        }

        private int landingIndex;

        public int LandingIndex
        {
            get { return landingIndex; }
            set
            {
                if (landingIndex != value)
                {
                    landingIndex = value;
                }
            }
        }

        private List<string> tags;

        public List<string> Tags
        {
            get { return tags; }
            set
            {
                if (tags != value)
                {
                    tags = value;
                }
            }
        }

        public string DefaultDestination { get; private set; }

        public string DestinationValue { get; private set; }


        private Slot routingIndex;

        private BncsState bncsState = BncsState.RXOnly;

        public BncsState BncsState
        {
            get { return bncsState; }
            set
            {
                if (bncsState != value)
                {
                    bncsState = value;
                }
            }
        }

        private SourceOption lastSelection;

        public DateTime startTime { get; private set; } = DateTime.Now;

        public PageMenuSources(int index, string label, string colour, ButtonModel button, List<Option> options, string instanceID, bool canBeSelected, int landingIndex, List<string> tags)
        {
            this.Index = index;
            this.Label = label;
            this.Background = colour;
            this.button = button;
            this.Options = options;
            this.InstanceID = instanceID;
            this.CanBeSelected = canBeSelected;
            this.LandingIndex = landingIndex;
            this.Tags = tags;
            //Packager = new Packager(BNCSConfig.Packager);

            BncsState = BNCSStatusModel.Instance.CommsStatus;
            BNCSStatusModel.Instance.CommsStateChanged += CommsStateChanged;

            if (string.IsNullOrEmpty(instanceID) == false)
            {
                var destConfigObj = BNCSConfig.HardwarePanelsDestinations.panels.Find(f => f.id == instanceID);
                if (destConfigObj != null)
                {
                    DestConfigModel = destConfigObj;

                    ProvisionAction();
                }
            }



            if (button != null)
            {
                button.MyButtonPressed += MyButtonPressed;
            }

            if (Options != null)
            {
                foreach (var option in Options)
                {
                    if (option is SourceOption source)
                    {
                        if (DestConfigModel != null)
                            source.SetDestinationConfig(DestConfigModel);
                        
                        source.CanBeSelected = true;
                        source.MyOptionSelected += MyOptionSelected;
                    }
                    else if (option is DestinationOption dest)
                    {
                       // if (DestConfigModel != null)
                       //     dest.SetDestinationConfig(DestConfigModel);

                        DestinationValue = dest.MyValue.ToString();
                        DefaultDestination = BNCSConfig.destinationNo.ToString();
                        dest.CanBeSelected = true;


                        dest.MyOptionSelected += MyOptionSelected;
                    }

                }
            }            

            Render();
        }

        private void CommsStateChanged(object sender, EventArgs e)
        {
            BncsState = BNCSStatusModel.Instance.CommsStatus;
        }

        public override void SetTielineMode(TielineMode tielineMode)
        {
            TielineMode = tielineMode;
        }



        public override void Render()
        {
           if (MenuOpened)
            {
                if (Options != null)
                {
                    foreach (var option in options)
                    {
                        if (option is SourceOption source)
                            source.Render();
                        else if (option is DestinationOption dest)
                            dest.Render();
                    }
                }

                if (SelectedOption != null)
                {
                    SelectedOption.Selected = true;
                }
                if (SelectedDestination != null)
                {
                    SelectedDestination.Selected = true;
                }
            }
            else
            {
                if (SelectedOption != null)
                {
                    SelectedOption.Selected = true;
                }
                if (SelectedDestination != null)
                {
                    SelectedDestination.Selected = true;
                }
                if (CanBeSelected)
                {
                    if (IsBlank == false)
                    {
                        button.Background = Background;
                        button.Label = Label;
                    }                    
              }                
            }
        }

        public override void UnRender()
        {
            if (MenuOpened)
            {
                if (Options != null)
                {
                    foreach (var option in options)
                        if (option is SourceOption source)
                            source.UnRender();
                        else if (option is DestinationOption dest)
                            dest.UnRender();
                }                
            }
            else
            {
                button.Background = "Black";
                button.Label = string.Empty;
            }
        }

        public override void OpenMenu(bool state)
        {
            MenuOpened = state;
        }

        public override void ChangeCanBeSelected(bool state)
        {
            CanBeSelected = state;
        }

        private void MyOptionSelected(object sender, OptionSelectedEventArgs e)
        {
            if (CanBeSelected)
            {
                if (sender is SourceOption sourceSelection)
                {
                    if (MenuOpened)
                    {
                        if (SelectedOption != null)
                        {
                            //newSelection = sender as SourceOption;
                            if (!SelectedOption.Equals(sourceSelection))
                            {
                                PerformAction(sourceSelection);
                            }
                        }
                        else
                        {
                            //newSelection = sender as SourceOption;                        
                            PerformAction(sourceSelection);
                            SelectedOption = sourceSelection;
                        }

                    }

                    if (sourceSelection != null && SelectedDestination != null)
                        sourceSelection.DestinationType = this.SelectedDestination.Type;
                    //FIXME: Commmenting out code below to ullustrate error - SAH

                    if (lastSelection != null && lastSelection.Index == sourceSelection.Index && lastSelection.Type == OptionType.PageSelect && lastSelection.Equals(SelectedOption))
                    {
                        //the statement above will check to see that we haven't double processed teh prevvious button selection.. this has the unfortunate effect of forcing a duplicate input which forces the page seleect to load the wrong page.  Need a more elegant solution.
                        //logger.Debug("Duplicate press registered");
                        lastSelection = null;
                        TimeSpan ts = DateTime.Now - startTime;
                        if (ts.TotalMilliseconds < 150)
                        {
                            lastSelection = sourceSelection;
                            logger.Debug($"Total MS: {ts.TotalMilliseconds}");
                        }
                        startTime = DateTime.Now;

                    }
                    else
                    {
                        lastSelection = sourceSelection;
                        startTime = DateTime.Now;
                    }
                    if (lastSelection != null || sourceSelection.Type != OptionType.PageSelect) //Forward the event for pageselect is lastSelection isn't null
                        OptionSelected?.Invoke(sourceSelection, new OptionSelectedEventArgs());

                }
                else if (sender is DestinationOption destSelection)
                {


                    if (SelectedDestination != null && !SelectedDestination.Equals(destSelection))
                        SelectedDestination.Selected = false;

                    DestinationValue = destSelection.MyValue.ToString();
                    SelectedDestination = destSelection;
                    if (destSelection.Type == OptionType.AudioMode)
                    {
                        /*TODO: Document audiomode
                         * 
                         * The intention here is to use the audiomodeselected destination to flag if we are routing only the audio - 
                         * and provide the ability to restore the previous route when the destination is changed.
                         * There was an edge case where you might not be able to escape this, and potentially lose access to your audio virtual
                         */ 

                        if (destSelection == SelectedDestination && destSelection.Selected == true)
                        {
                            logger.Trace($"AudioMode unlatched and restored to default panel route: {DefaultDestination}");
                            //SelectedOption = AudioModeSelected;
                            destSelection.Selected = false;
                            //PerformAction(AudioModeSelected);
                            OptionSelected?.Invoke(AudioModeSelected, new OptionSelectedEventArgs());

                            SelectedDestination = null;
                            AudioModeSelected = null;
                            //restore original destination routing
                            PerformRoute(int.Parse(DefaultDestination));
                            SelectDestination(true);
                            //TODO: Fix Source selection option
                        }
                        else if (SelectedOption != null)
                        {
                            logger.Trace($"AudioMode Selected - restore Audiomode to default panel route: {DefaultDestination}");
                            AudioModeSelected = SelectedOption;
                            SelectDestination();
                            PerformRoute(int.Parse(DefaultDestination));
                        }

                    }
                    else if (destSelection.Type != OptionType.AudioMode && AudioModeSelected != null)
                    {
                        logger.Trace($"AudioMode reverted");
                        destSelection.Selected = true;
                        //PerformRoute(destSelection.MyValue);
                        PerformRoute(int.Parse(DefaultDestination));
                        OptionSelected?.Invoke(AudioModeSelected, new OptionSelectedEventArgs());

                        //SelectedDestination = null;
                        AudioModeSelected = null;
                        SelectDestination();
                    }
                    //if (AudioModeSelected == null && destSelection.Type == OptionType.AudioMode)
                   // {
                        //restore default destinaiton if audiomode is unlatched
                    //    SelectDestination(true);
                    //}
                    else
                    {
                        SelectDestination();
                    }

                    DestinationSelected?.Invoke(destSelection, new OptionSelectedEventArgs());

                }
            }
        }


        public void SelectDestination(bool RestoreDefault = false)
        {
            if (RestoreDefault)
            {
                //DestConfigModel.destination.First().optionType = this.SelectedDestination.Type;
                DestConfigModel.destination.First().dest = DefaultDestination;
                logger.Info($"Updating Router Destination to: {DefaultDestination}:{DestConfigModel.destination.First().ToString()}");
            }
            else if (UInt32.TryParse(DestinationValue, out uint destination))
            {
                if (UInt32.TryParse(DestConfigModel.destination.First().dest, out uint priorDestination))
                    if (destination != priorDestination)
                    {
                        DestConfigModel.destination.First().optionType = this.SelectedDestination.Type;
                        DestConfigModel.destination.First().dest = destination.ToString();
                        logger.Info($"Updating Router Destination to: {destination.ToString()}:{DestConfigModel.destination.First().ToString()}");
                    }
                //reset selected - then 
                // try to provision - but source will be dest.

                if (SelectedDestination != null)
                {
                    //SelectedDestination.Selected = false;

                    var destSelection = Options.Find(f => f is DestinationOption destOption && destOption.MyValue == destination );
                    if (destSelection != null)
                    {   
                        if (destSelection is DestinationOption dest && dest.Type != OptionType.PageSelect)
                        {
                            if (CanBeSelected)
                            {
                                SelectedDestination = (DestinationOption)destSelection;


                                if (MenuOpened)
                                {
                                SelectedDestination.Selected = true;
                                }
                            }
                        }
                    }
                }


            }

        }

        private void ProvisionAction()
        {
            if (UInt32.TryParse(DestConfigModel.destination.First().dest, out uint destination))
            {
                if (instances_and_devicetypes.Instance.TryGetInstance(DestConfigModel.destination.First().router, out var instance))
                {
                    bool Success;
                    List<(uint Index, SlotClient Slot)> Slots;
                    bool isPackager = Packager.IsPackager?.Invoke(instance) ?? false;
                    if (isPackager)
                    {
                        //this.Packager = PanelModel.Packager;

                        //return a compbination of composite instance and modulo destintionNumber - failing?
                        Packager.RouterDestination instanceNo = PanelModel.Packager.getInstanceFromDestination(instance, destination);
                        (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(instanceNo.router, instanceNo.destination);
                    }
                    else
                    {
                        (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(instance, destination);
                    }

                    if (Success)
                    {
                            routingIndex = Slots[0].Slot as Slot;

                            //Set a callback to get notifed when it changes
                            routingIndex.OnChange += Index_OnChanged;
                        

                    }
                }
            }            
        }

        private void Index_OnChanged(object sender, EventSlotChange e)
        {
            int source = RoutingGenericAdaptor.WhatRouteWasMade("tieline_router", e.Value);
            if (SelectedOption != null)
            {
                if (SelectedOption.MyValue != source)
                {
                    SelectedOption.Selected = false;

                    var newSelection = Options.Find(f => f is SourceOption sourceOption && sourceOption.MyValue == source);
                    if (newSelection != null)
                    {
                        SelectedOption = (SourceOption)newSelection;

                        if (CanBeSelected)
                        {
                            if (MenuOpened)
                            {
                                SelectedOption.Selected = true;

                            }
                        }
                    }
                    else
                    {
                        SelectedOption = null;
                    }
                }
            }
            else
            {
                var newSelection = Options.Find(f => f is SourceOption sourceOption && sourceOption.MyValue == source);
                if (newSelection != null)
                {
                    SelectedOption = (SourceOption)newSelection;

                    if (CanBeSelected)
                    {
                        if (MenuOpened)
                        {
                            SelectedOption.Selected = true;
                        }
                    }
                }
                else
                {
                    SelectedOption = null;
                }
            }
        }

        private void PerformAction(SourceOption option)
        {
            if (option != null)
            {
                if (BncsState == BncsState.TXRX && option.MyValue > 0)
                {
                    string toSend = RoutingGenericAdaptor.ParseRouteToDo("tieline_router", TielineMode, option.MyValue);
                    if (string.IsNullOrEmpty(toSend) == false && option.Type == OptionType.Source)
                    {
                        ProvisionAction();
                        routingIndex?.Set(toSend, false);
                    }
                }
            }
        }

        private void PerformRoute(int source)
        {
            if (source > 0)
            {
                if (BncsState == BncsState.TXRX)
                {
                    string toSend = RoutingGenericAdaptor.ParseRouteToDo("tieline_router", TielineMode, source);
                    if (string.IsNullOrEmpty(toSend) == false)
                    {
                        ProvisionAction();
                        routingIndex?.Set(toSend, false);
                    }
                }
            }
        }


        private async void MyButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (canBeSelected)
            {
                await Task.Delay(300);

                if (MenuOpened == false)
                {
                    MyMenuSelected?.Invoke(this, new MenuSelectedEventArgs());
                }                    
            }            
        }

        public void Dispose()
        {
            BNCSStatusModel.Instance.CommsStateChanged -= CommsStateChanged;

            if (Options != null)
            {
                foreach (var option in Options)
                {

                    if (option is SourceOption source && source != null)
                    {
                        source.MyOptionSelected -= MyOptionSelected;
                        source.Dispose();
                    }
                    else if (option is DestinationOption dest && dest != null)
                    {
                        dest.MyOptionSelected -= MyOptionSelected;
                        dest.Dispose();
                    }
                 
                }

                Options.Clear();
            }

            SelectedOption = null;
            DestConfigModel = null;            

            if (button != null)
            {
                button.MyButtonPressed -= MyButtonPressed;
                button = null;
            }

            if (routingIndex != null)
            {
                routingIndex.OnChange -= Index_OnChanged;
                routingIndex.Dispose();

                routingIndex = null;
            }
        }

    }


    public class MenuSelectedEventArgs : EventArgs
    {
        public MenuSelectedEventArgs()
        {

        }
    }
}
