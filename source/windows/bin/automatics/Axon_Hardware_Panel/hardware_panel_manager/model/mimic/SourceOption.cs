using driver_template.json;
using driver_template.model.bncs;
using driver_template.xml;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    public delegate void OptionSelected(object sender, OptionSelectedEventArgs e);

    public class SourceOption : Option,  INotifyPropertyChanged, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event OptionSelected MyOptionSelected;

        private string idleColour;

        private string selectedColour;

        protected ButtonModel button;

        private int myValue;
        public int MyValue
        {
            get { return myValue; }
            set
            {
                if (myValue != value)
                {
                    myValue = value;
                }
            }
        }

        private bool isBlank = false;

        public bool IsBlank
        {
            get { return isBlank; }
            set
            {
                if (isBlank != value)
                {
                    isBlank = value;
                }
            }
        }

        private Instance sourceInstance;

        public Instance SourceInstance
        {
            get { return sourceInstance; }
            set
            {
                if (sourceInstance != value)
                {
                    sourceInstance = value;
                }
            }
        }

        private int index;        

        public int Index
        {
            get { return index; }
            set
            {
                if (index != value)
                {
                    index = value;
                }
            }
        }

        private bool canBeSelected = false;

        public bool CanBeSelected
        {
            get { return canBeSelected; }
            set
            {
                if (canBeSelected != value)
                {
                    canBeSelected = value;
                }
            }
        }

        private bool selected;

        public bool Selected
        {
            get { return selected; }
            set
            {
                if (selected != value)
                {
                    selected = value;

                    if (Selected)
                    {
                        Background = selectedColour;
                    }
                    else
                    {
                        Background = idleColour;
                    }
                }
            }
        }

        private string label;

        public string Label
        {
            get { return label; }
            set
            {
                if (label != value)
                {
                    label = value;

                    if (CanBeSelected)
                    {
                        button.Label = label;
                    }

                    OnPropertyChanged("Label");
                }
            }
        }

        private string background = "LightSlateGray";

        public string Background
        {
            get { return background; }
            set
            {
                if (background != value)
                {
                    background = value;

                    if(canBeSelected)
                        button.Background = background;

                    OnPropertyChanged("Background");
                }
            }
        }

        public Packager Packager { get; private set; }
        public OptionType DestinationType { get; internal set; }

        public OptionType Type;

        private SlotDatabase dbSlot;

        public SourceOption(int index, string label, string idleColour, string selectedColour, ButtonModel button, int value, OptionType option)
        {
            this.Index = index;
            this.Label = label;
            this.Type = option;

            if (value == 0)
            {
                IsBlank = true;
                this.Type = OptionType.Blank;
            }

            this.idleColour = idleColour;
            this.selectedColour = selectedColour;
            this.Background = idleColour;
            this.button = button;
            this.MyValue = value;

            if (button != null)
            {
                button.MyButtonPressed += MyButtonPressed;
            }
        }

        public void SetDestinationConfig(XhwPanelConfig position)
        {
            //Packager = new Packager(BNCSConfig.Packager);
            if (MyValue > 0)
            {
                if (instances_and_devicetypes.Instance.TryGetInstance(position.destination.First().source_names, out var instance))
                {
                    bool SuccessDb;
                    List<(uint Index, SlotDatabase DatabaseSlot)> Database;
                    string slotValue = "";
                    uint database = BNCSConfig.SourcesNamesDatabase;
                    //Get the Database by index - if composite - it's assumed to be a packager.

                    bool isPackager = Packager.IsPackager?.Invoke(instance) ?? false;
                    if (isPackager)
                    {
                        //this.Packager = PanelModel.Packager;

                        //return a compbination of composite instance and modulo destintionNumber - failing?
                        Packager.RouterDestination instanceNo = PanelModel.Packager.getInstanceFromDestination(instance, (uint)MyValue);
                        (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(instanceNo.router, instanceNo.destination, 1, database);
                        if (Database[0].DatabaseSlot.Value == "---")
                            slotValue = PanelModel.Packager.TryGetValueFromPackager(instance, (uint)MyValue);



                    }
                    else
                    {
                        (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(instance, (uint)MyValue, 1, database);
                        slotValue = Database[0].DatabaseSlot.Value;
                    }

                    if (SuccessDb)
                    {
                        dbSlot = Database[0].DatabaseSlot;
						if(Type == OptionType.Source && this.Label == "" || this.Label == "---")
                            Label = slotValue;

                        //Set a callback to get notifed when it changes.
                        dbSlot.OnChange += DatabaseSlot_OnChange;
                    }
                }
            }            
        }

        public void Render()
        {
            button.Background = Background;
            button.Label = Label;
        }

        public void UnRender()
        {
            Background = idleColour;            

            button.Background = "Black";
            button.Label = string.Empty;
        }

        private async void MyButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (canBeSelected)
            {
                await Task.Delay(300);

                if (IsBlank == false)
                {
                    if (index > 0)
                        MyOptionSelected?.Invoke(this, new OptionSelectedEventArgs());
                }                
            }            
        }

        private void DatabaseSlot_OnChange(object sender, EventSlotChange e)
        {
            Label = e.Value;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void Dispose()
        {
            if (dbSlot != null)
            {
                dbSlot.OnChange -= DatabaseSlot_OnChange;
                dbSlot.Dispose();
                dbSlot = null;
            }

            SourceInstance = null;

            if (button != null)
            {
                button.MyButtonPressed -= MyButtonPressed;
                button = null;
            }
        }
    }

    public class OptionSelectedEventArgs : EventArgs
    {
        public OptionSelectedEventArgs()
        {

        }
    }
}
