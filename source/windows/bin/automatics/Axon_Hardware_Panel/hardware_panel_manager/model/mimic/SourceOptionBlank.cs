﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    public class SourceOptionBlank : SourceOption
    {
        public SourceOptionBlank(int index, string label, string idleColour, string selectedColour, ButtonModel button, int value, OptionType option) : base(index, label, idleColour, selectedColour, button, value, option)
        {
            CanBeSelected = false;
        }

        public new void Render()
        {
            base.button.Background = "Black";
            base.button.Label = string.Empty;
        }

        public new void UnRender()
        {
            base.button.Background = "Black";
            base.button.Label = string.Empty;
        }
    }
}
