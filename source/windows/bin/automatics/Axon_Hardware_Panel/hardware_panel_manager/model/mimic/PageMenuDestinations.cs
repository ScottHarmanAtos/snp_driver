using driver_template.helpers;
using driver_template.json;
using driver_template.model.bncs;
using driver_template.xml;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    //public delegate void MenuSelected(object sender, MenuSelectedEventArgs e);

    public class PageMenuDestinations : MenuSources, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event MenuSelected MyMenuSelected;

        public event OptionSelected DestinationSelected;

        protected ButtonModel button;

        private bool canBeSelected = false;

        public bool CanBeSelected
        {
            get { return canBeSelected; }
            set
            {
                if (canBeSelected != value)
                {
                    canBeSelected = value;
                }
            }
        }

        private bool menuOpened;

        public bool MenuOpened
        {
            get { return menuOpened; }
            set
            {
                if (menuOpened != value)
                {
                    menuOpened = value;

                    if (Options != null)
                    {
                        if (menuOpened)
                        {
                            foreach (var option in Options)
                            {
                                option.CanBeSelected = true;
                            }
                        }
                        else
                        {
                            foreach (var option in Options)
                            {
                                option.UnRender();
                                option.Selected = false;
                                option.CanBeSelected = false;
                            }
                        }
                    }
                }
            }
        }


        private List<DestinationOption> options;

        public List<DestinationOption> Options
        {
            get { return options; }
            set
            {
                if (options != value)
                {
                    options = value;
                }
            }
        }

        private DestinationOption selectedOption;

        public DestinationOption SelectedOption
        {
            get { return selectedOption; }
            set
            {
                if (selectedOption != value)
                {
                    selectedOption = value;
                }
            }
        }

        private string actionType;
        public string InstanceID
        {
            get { return actionType; }
            set
            {
                if (actionType != value)
                {
                    actionType = value;
                }
            }
        }

        private XhwPanelConfig destConfigModel;
        public XhwPanelConfig DestConfigModel
        {
            get { return destConfigModel; }
            set
            {
                if (destConfigModel != value)
                {
                    destConfigModel = value;
                }
            }
        }

        private TielineMode tielineMode = TielineMode.TX;

        public TielineMode TielineMode
        {
            get { return tielineMode; }
            set
            {
                if (tielineMode != value)
                {
                    tielineMode = value;
                }
            }
        }

        private int landingIndex;

        public int LandingIndex
        {
            get { return landingIndex; }
            set
            {
                if (landingIndex != value)
                {
                    landingIndex = value;
                }
            }
        }

        private List<string> tags;

        public List<string> Tags
        {
            get { return tags; }
            set
            {
                if (tags != value)
                {
                    tags = value;
                }
            }
        }

        public string DestinationValue { get; private set; }


        private Slot routingIndex;

        private BncsState bncsState = BncsState.RXOnly;

        public BncsState BncsState
        {
            get { return bncsState; }
            set
            {
                if (bncsState != value)
                {
                    bncsState = value;
                }
            }
        }

        public PageMenuDestinations(int index, string label, string colour, ButtonModel button, List<DestinationOption> options, string instanceID, bool canBeSelected, int landingIndex, List<string> tags)
        {
            this.Index = index;
            this.Label = label;
            this.Background = colour;
            this.button = button;
            this.Options = options;
            this.InstanceID = instanceID;
            this.CanBeSelected = canBeSelected;
            this.LandingIndex = landingIndex;
            this.Tags = tags;
            //Packager = new Packager(BNCSConfig.Packager);

            BncsState = BNCSStatusModel.Instance.CommsStatus;
            BNCSStatusModel.Instance.CommsStateChanged += CommsStateChanged;

            if (string.IsNullOrEmpty(instanceID) == false)
            {
                var destConfigObj = BNCSConfig.HardwarePanelsDestinations.panels.Find(f => f.id == instanceID);
                if (destConfigObj != null)
                {
                    DestConfigModel = destConfigObj;

                    ProvisionDestination();
                }
            }



            if (button != null)
            {
                button.MyButtonPressed += MyButtonPressed;
            }

            if (Options != null)
            {
                foreach (var option in Options)
                {
                    if (DestConfigModel != null)
                        option.SetDestinationConfig(DestConfigModel);
                    if (option.Type == OptionType.Destination || option.Type  == OptionType.DestinationLocal || option.Type == OptionType.AudioMode)
                    {
                        DestinationValue = option.MyValue.ToString();
                        option.CanBeSelected = true;
                    }
                    if (option.Type == OptionType.Source)
                    {
                        //DestinationValue = option.MyValue.ToString();
                        option.CanBeSelected = true;
                    }

                    option.MyOptionSelected += MyOptionSelected;
                }
            }

            Render();
        }

        private void CommsStateChanged(object sender, EventArgs e)
        {
            BncsState = BNCSStatusModel.Instance.CommsStatus;
        }

        public override void SetTielineMode(TielineMode tielineMode)
        {
            TielineMode = tielineMode;
        }



        public override void Render()
        {
            if (MenuOpened)
            {
                if (Options != null)
                {
                    foreach (var option in options)
                    {
                        option.Render();
                    }
                }

                if (SelectedOption != null)
                {
                    SelectedOption.Selected = true;
                }
            }
            else
            {
                if (CanBeSelected)
                {
                    if (IsBlank == false)
                    {

                        button.Background = Background;
                        button.Label = Label;
                    }
                }
            }
        }

        public override void UnRender()
        {
            if (MenuOpened)
            {
                if (Options != null)
                {
                    foreach (var option in options)
                    {
                        option.UnRender();
                    }
                }
            }
            else
            {
                button.Background = "Black";
                button.Label = string.Empty;
            }
        }

        public override void OpenMenu(bool state)
        {
            MenuOpened = state;
        }

        public override void ChangeCanBeSelected(bool state)
        {
            CanBeSelected = state;
        }

        private void MyOptionSelected(object sender, OptionSelectedEventArgs e)
        {
            if (CanBeSelected)
            {
                if (MenuOpened)
                {
                    if (SelectedOption != null)
                    {
                        DestinationOption newSelection = sender as DestinationOption;
                        if (!SelectedOption.Equals(newSelection))
                        {
                            SelectedOption = newSelection;
                        }
                    }
                    else
                    {
                        var newSelection = sender as DestinationOption;

                        SelectedOption = newSelection;
                    }

                    DestinationSelected?.Invoke(SelectedOption, new OptionSelectedEventArgs());
                }
                else if (UInt32.TryParse(DestinationValue, out uint destinationVal))
                {
                    //var newSelection = sender as SourceOption;
                    //newSelection.Selected = true;
                    if (SelectedOption != null)
                    {
                        DestinationOption newSelection = sender as DestinationOption;
                        if (!SelectedOption.Equals(newSelection))
                        {
                            SelectedOption = newSelection;
                        }
                    }
                    else
                    {
                        var newSelection = sender as DestinationOption;
                        SelectedOption = newSelection; 

                    }
                    //did I override the desinationValue in my panel model - if so, on select, try and gather the value, and compare to the current panel destination
                    if (UInt32.TryParse(DestConfigModel.destination.First().dest, out uint destination))
                        if (destinationVal != destination)
                            SelectDestination();

                    DestinationSelected?.Invoke(SelectedOption, new OptionSelectedEventArgs());
                    //Dest_Changed(SelectedOption);
                    //this.Label = SelectedOption.Label;

                    //SelectedOption = null;

                }
               
            }
        }


        public void SelectDestination()
        {
            if (UInt32.TryParse(DestinationValue, out uint destination))
            {
                if (UInt32.TryParse(DestConfigModel.destination.First().dest, out uint priorDestination))
                    if (destination != priorDestination)
                    {
                        DestConfigModel.destination.First().optionType = this.SelectedOption.Type;
                        DestConfigModel.destination.First().dest = destination.ToString();
                        logger.Info($"Updating Router Destination to: {destination.ToString()}:{DestConfigModel.destination.First().ToString()}");
                    }
                //reset selected - then 
                // try to provision - but source will be dest.
                
                ProvisionDestination();

            }

        }

        private void ProvisionDestination()
        {
            if (UInt32.TryParse(DestConfigModel.destination.First().dest, out uint destination))
            {
                if (instances_and_devicetypes.Instance.TryGetInstance(DestConfigModel.destination.First().router, out var instance))
                {
                    bool Success;
                    List<(uint Index, SlotClient Slot)> Slots;

                    bool isPackager = Packager.IsPackager?.Invoke(instance) ?? false;
                    if (isPackager)
                    {
                        //this.Packager = PanelModel.Packager;

                        //return a compbination of composite instance and modulo destintionNumber - failing?
                        Packager.RouterDestination instanceNo = PanelModel.Packager.getInstanceFromDestination(instance, destination);
                        (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(instanceNo.router, instanceNo.destination);
                    }
                    else
                    {
                        (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(instance, destination);
                    }

                    //var (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(instance, destination);
                    if (Success)
                    {
                        //routingIndex = Slots[0].Slot as Slot;

                        //Set a callback to get notifed when it changes
                        //routingIndex.OnChange += Dest_Changed;
                        if (SelectedOption != null)
                        {
                            //if (SelectedOption.MyValue != destination)
                            //{
                            SelectedOption.Selected = false;

                            var newSelection = Options.Find(f => f.MyValue == destination);
                            if (newSelection != null)
                            {
                                SelectedOption = newSelection;

                                if (CanBeSelected)
                                {
                                    //if (MenuOpened)
                                    //{
                                    SelectedOption.Selected = true;
                                    //}
                                }
                                //  }
                                //  else
                                //  {
                                //      SelectedOption = null;
                                //  }
                            }
                        }
                    }
                }
            }
        }



        private void Dest_Changed(object sender, EventSlotChange e)
        {
            if (UInt32.TryParse(this.DestinationValue, out uint destination))
            {
                if (SelectedOption != null)
                {
                    //if (SelectedOption.MyValue != destination)
                    //{
                        SelectedOption.Selected = false;

                        var newSelection = Options.Find(f => f.MyValue == destination);
                        if (newSelection != null)
                        {
                            SelectedOption = newSelection;

                            if (CanBeSelected)
                            {
                                //if (MenuOpened)
                                //{
                                SelectedOption.Selected = true;
                                //}
                            }
                      //  }
                      //  else
                      //  {
                      //      SelectedOption = null;
                      //  }
                    }
                }/*
                else
                {
                    var newSelection = Options.Find(f => f.MyValue == destination);
                    if (newSelection != null)
                    {
                        SelectedOption = newSelection;

                        if (CanBeSelected)
                        {
                            //if (MenuOpened)
                            //{
                            SelectedOption.Selected = true;
                            //}
                        }
                    }
                    else
                    {
                        SelectedOption = null;
                    }
                }*/
                    }
        }
      

/*        private void Index_OnChanged(object sender, EventSlotChange e)
        {
            int source = RoutingGenericAdaptor.WhatRouteWasMade("tieline_router", e.Value);
            if (SelectedOption != null)
            {
                if (SelectedOption.MyValue != source)
                {
                    SelectedOption.Selected = false;

                    var newSelection = Options.Find(f => f.MyValue == source);
                    if (newSelection != null)
                    {
                        SelectedOption = newSelection;

                        if (CanBeSelected)
                        {
                            //if (MenuOpened)
                            //{
                            SelectedOption.Selected = true;
                            //}
                        }
                    }
                    else
                    {
                        SelectedOption = null;
                    }
                }
            }
            else
            {
                var newSelection = Options.Find(f => f.MyValue == source);
                if (newSelection != null)
                {
                    SelectedOption = newSelection;

                    if (CanBeSelected)
                    {
                        //if (MenuOpened)
                        //{
                        SelectedOption.Selected = true;
                        //}
                    }
                }
                else
                {
                    SelectedOption = null;
                }
            }
        }*/

/*        private void PerformAction(SourceOption option)
        {
            if (BncsState == BncsState.TXRX)
            {
                string toSend = RoutingGenericAdaptor.ParseRouteToDo("tieline_router", TielineMode, option.MyValue);
                if (string.IsNullOrEmpty(toSend) == false)
                {
                    ProvisionAction();
                    routingIndex?.Set(toSend, false);
                }
            }
        }*/

        private async void MyButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (canBeSelected)
            {
                if (MenuOpened == false)
                {
                    await Task.Delay(300);
                    MyMenuSelected?.Invoke(this, new MenuSelectedEventArgs());
                }
            }
        }

        public void Dispose()
        {
            BNCSStatusModel.Instance.CommsStateChanged -= CommsStateChanged;

            if (Options != null)
            {
                foreach (var option in Options)
                {
                    if (option != null)
                    {
                        option.MyOptionSelected -= MyOptionSelected;
                        option.Dispose();
                    }
                }

                Options.Clear();
            }

            SelectedOption = null;
            DestConfigModel = null;

            if (button != null)
            {
                button.MyButtonPressed -= MyButtonPressed;
                button = null;
            }

            if (routingIndex != null)
            {
                routingIndex.OnChange -= Dest_Changed;
                routingIndex.Dispose();

                routingIndex = null;
            }
        }

    }

}
