using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    public enum RoomMode
    {
        none = 0,
        MFR = 1,
        PCR = 2,
        SCR = 3,
        TX = 4,
        TX_SUP = 5,
        LIVE = 6,
        DYNAMIC = 7
    }

    public class GroupMenuSources : MenuSources, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event MenuSelected MyMenuSelected;

        public event OptionSelected OptionSelected;        

        private ButtonModel button;

        private bool canBeSelected = true;

        public bool CanBeSelected
        {
            get { return canBeSelected; }
            set
            {
                if (canBeSelected != value)
                {
                    canBeSelected = value;
                }
            }
        }

        private bool menuOpened;

        public bool MenuOpened
        {
            get { return menuOpened; }
            set
            {
                if (menuOpened != value)
                {
                    menuOpened = value;

                    if (Options != null)
                    {
                        if (menuOpened)
                        {
                            foreach (var option in Options)
                            {
                                option.CanBeSelected = true;
                            }
                        }
                        else
                        {
                            foreach (var option in Options)
                            {
                                option.UnRender();
                                option.MenuOpened = false;
                                option.CanBeSelected = false;
                            }

                            SelectedOption = null;
                        }
                    }                                        
                }
            }
        }

        private List<PageMenuSources> options;

        public List<PageMenuSources> Options
        {
            get { return options; }
            set
            {
                if (options != value)
                {
                    options = value;
                }
            }
        }

        private MenuSources selectedOption;

        public MenuSources SelectedOption
        {
            get { return selectedOption; }
            set
            {
                if (selectedOption != value)
                {
                    selectedOption = value;
                }
            }
        }

        private RoomMode roomMode = RoomMode.none;

        public RoomMode RoomMode
        {
            get { return roomMode; }
            set
            {
                if (roomMode != value)
                {
                    roomMode = value;
                }
            }
        }

        private TielineMode tielineMode = TielineMode.none;

        public TielineMode TielineMode
        {
            get { return tielineMode; }
            set
            {
                if (tielineMode != value)
                {
                    tielineMode = value;

                    if (Options != null)
                    {
                        foreach (var option in Options)
                        {
                            option.SetTielineMode(TielineMode);
                        }
                    }
                }
            }
        }

        public GroupMenuSources(int index, string label, string colour, ButtonModel button, List<PageMenuSources> options, RoomMode roomMode)
        {
            this.Index = index;
            this.Label = label;
            this.Background = colour;
            this.button = button;
            this.Options = options;
                        
            this.RoomMode = roomMode;

            if (RoomMode == RoomMode.TX)
                TielineMode = TielineMode.TX;

            if (button != null)
            {
                button.MyButtonPressed += MyButtonPressed;
            }

            if (Options != null)
            {
                foreach (var option in Options)
                {
                    option.SetTielineMode(TielineMode);
                    option.MyMenuSelected += MySubMenuSelected;
                    option.OptionSelected += MySubMenuOptionSelected;
                }
            }            

            Render();
        }        

        private void MySubMenuOptionSelected(object sender, OptionSelectedEventArgs e)
        {
            if (CanBeSelected)
            {
                if (MenuOpened)
                {
                    SourceOption newSelection = sender as SourceOption;
                    if (newSelection != null)
                    {
                        if (SelectedOption != null && newSelection.Type != OptionType.Back)
                        {
                            if (newSelection != null && newSelection.Type == OptionType.PageSelect)
                            {
                                foreach (var o in Options)
                                {
                                    if (o.SelectedOption != null)
                                        o.SelectedOption = null;
                                    if (o.SelectedDestination != null)
                                        o.SelectedDestination = null;
                                }
                                PageMenuSources newPageSelection = Options[newSelection.MyValue - 1];
                                //OptionSelected?.Invoke(newPageSelection, new OptionSelectedEventArgs());

                                MySubMenuSelected(newPageSelection, new MenuSelectedEventArgs());
                            }

                        }
                        else
                            OptionSelected?.Invoke(newSelection, new OptionSelectedEventArgs());
                    }
                }
            }
        }
        

        public override void SetTielineMode(TielineMode tielineMode)
        {
            TielineMode = tielineMode;
        }

        public override void Render()
        {
            if (MenuOpened)
            {
                if (Options != null)
                {
                    foreach (var option in options)
                    {
                        option.Render();
                    }
                }                
            }
            else
            {
                button.Background = Background;
                button.Label = Label;
            }
        }

        public override void UnRender()
        {
            if (MenuOpened)
            {
                if (Options != null)
                {
                    foreach (var option in options)
                    {
                        option.UnRender();
                    }
                }                
            }
            else if (button != null)
            {
                button.Background = "Black";
                button.Label = string.Empty;
            }
        }

        public override void OpenMenu(bool state)
        {
            MenuOpened = state;
        }

        public override void ChangeCanBeSelected(bool state)
        {
            CanBeSelected = state;
        }

        private void MySubMenuSelected(object sender, MenuSelectedEventArgs e)
        {
            if (canBeSelected)
            {
                if (MenuOpened == true)
                {
                    MenuSources newMenu = sender as MenuSources;

                    if (SelectedOption != null)
                    {
                        if (!SelectedOption.Equals(newMenu.Index))
                        {
                            SelectedOption.ChangeCanBeSelected(false);
                            SelectedOption.OpenMenu(false);
                            //SelectedOption.UnRender();

                            SelectedOption = newMenu;

                            SelectedOption.ChangeCanBeSelected(true);
                            SelectedOption.OpenMenu(true);
                            SelectedOption.Render();
                        }
                    }
                    else
                    {
                        if (Options != null)
                        {
                            foreach (var menu in Options)
                            {
                                menu.ChangeCanBeSelected(false);
                            }
                        }

                        SelectedOption = newMenu;

                        SelectedOption.ChangeCanBeSelected(true);
                        SelectedOption.OpenMenu(true);
                        SelectedOption.Render();
                    }
                }                
            }            
        }

        private async void MyButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (canBeSelected)
            {
                if (MenuOpened == false)
                {
                    await Task.Delay(300);
                    MyMenuSelected?.Invoke(this, new MenuSelectedEventArgs());
                }                    
            }
        }

        public void Dispose()
        {
            if (Options != null)
            {
                foreach (var option in Options)
                {
                    if (option != null)
                    {
                        option.MyMenuSelected -= MySubMenuSelected;
                        option.OptionSelected -= MySubMenuOptionSelected;
                        option.Dispose();
                    }                    
                }

                Options.Clear();
            }

            SelectedOption = null;

            if (button != null)
            {
                button.MyButtonPressed -= MyButtonPressed;
                button = null;
            }
        }
    }
}
