using driver_template.helpers;
using driver_template.json;
using driver_template.model.bncs;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    public delegate void ButtonPressed(object sender, ButtonPressedEventArgs e);
    public class ButtonModel : INotifyPropertyChanged, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event ButtonPressed MyButtonPressed;

        private int index;
        public int Index
        {
            get { return index; }
            set
            {
                if (index != value)
                {
                    index = value;
                }
            }
        }

        private int remoteIndex;
        public int RemoteIndex
        {
            get { return remoteIndex; }
            set
            {
                if (remoteIndex != value)
                {
                    remoteIndex = value;
                }
            }
        }

        private string label;

        public string Label
        {
            get { return label; }
            set
            {
                if (label != value)
                {
                    label = value;

                    if (label != null && label.Contains("|"))
                    {
                        var split = label.Split('|');
                        if (split.Length >= 2)
                        {
                            Has2lines = true;

                            Line1 = split[0];
                            Line2 = split[1];
                        }
                    }
                    else
                    {
                        Has2lines = false;
                        Line1 = label;
                    }

                    SendText();
                }
            }
        }

        private bool has2lines;

        public bool Has2lines
        {
            get { return has2lines; }
            set
            {
                if (has2lines != value)
                {
                    has2lines = value;

                    OnPropertyChanged("Has2lines");
                }
            }
        }

        private string line1;

        public string Line1
        {
            get { return line1; }
            set
            {
                if (line1 != value)
                {
                    line1 = value;

                    OnPropertyChanged("Line1");
                }
            }
        }

        private string line2;

        public string Line2
        {
            get { return line2; }
            set
            {
                if (line2 != value)
                {
                    line2 = value;

                    OnPropertyChanged("Line2");
                }
            }
        }

        private string background = "LightSlateGray";

        public string Background
        {
            get { return background; }
            set
            {
                if (background != value)
                {
                    background = value;

                    SendColour();

                    var wpfColour = (SolidColorBrush)new BrushConverter().ConvertFromString(background);

                    MimicBackground = wpfColour;
                }
            }
        }

        private SolidColorBrush mimicBackground = Brushes.LightSlateGray;

        public SolidColorBrush MimicBackground
        {
            get { return mimicBackground; }
            set
            {
                if (mimicBackground != value)
                {
                    mimicBackground = value;

                    OnPropertyChanged("MimicBackground");
                }
            }
        }

        private Instance instance;

        public Instance Instance
        {
            get { return instance; }
            set
            {
                if (instance != value)
                {
                    instance = value;
                }
            }
        }

        private JBasePanelType myBaseType;

        public JBasePanelType MyBaseType
        {
            get { return myBaseType; }
            set
            {
                if (myBaseType != value)
                {
                    myBaseType = value;
                }
            }
        }

        private bool isLcd;

        private Slot slotButtonPressed;

        private Slot slotButtonColour;

        private Slot slotButtonText;

        public ICommand ButtonPressed
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    logger.Trace($"Mimic button {index} pressed");

                    if(BNCSConfig.StartupArguments.Development)
                        MyButtonPressed?.Invoke(this, new ButtonPressedEventArgs());
                });
                return command;
            }
        }

        private BncsState bncsState = BncsState.RXOnly;

        public BncsState BncsState
        {
            get { return bncsState; }
            set
            {
                if (bncsState != value)
                {
                    bncsState = value;
                }
            }
        }

        public ButtonModel(int index, Instance instance, JBasePanelType myBaseType, bool isLcd = true)
        {
            this.Index = index;
            this.Instance = instance;
            this.MyBaseType = myBaseType;

            this.isLcd = isLcd;

            BncsState = BNCSStatusModel.Instance.CommsStatus;
            BNCSStatusModel.Instance.CommsStateChanged += CommsStateChanged;

            ProcessRemoteHooks();
        }

        private void CommsStateChanged(object sender, EventArgs e)
        {
            BncsState = BNCSStatusModel.Instance.CommsStatus;
        }

        private void ProcessRemoteHooks()
        {
            int myRemoteIndex = Index;
            MyBaseType.buttons_translation.TryGetValue(Index.ToString(), out myRemoteIndex);

            RemoteIndex = myRemoteIndex;

            uint hwPanelButtonPressedIndex = (uint)MyBaseType.button_offsets + (uint)RemoteIndex;
            uint hwPanelButtonColourIndex = (uint)MyBaseType.colour_offsets + (uint)RemoteIndex;
            uint hwPanelButtonTextIndex = (uint)MyBaseType.text_offsets + (uint)RemoteIndex;

            var (SlotsButtonPressedSuccess, SlotsButtonPressed) = BNCSConfig.ClientInstances.GetSlots(Instance, hwPanelButtonPressedIndex, hwPanelButtonPressedIndex);
            if (SlotsButtonPressedSuccess)
            {
                slotButtonPressed = SlotsButtonPressed[0].Slot as Slot;                

                //Set a callback to get notifed when it changes
                slotButtonPressed.OnChange += RemoteButtonPressed;
            }

            var (SlotsButtonColourSuccess, SlotsButtonColour) = BNCSConfig.ClientInstances.GetSlots(Instance, hwPanelButtonColourIndex, hwPanelButtonColourIndex);
            if (SlotsButtonColourSuccess)
            {
                slotButtonColour = SlotsButtonColour[0].Slot as Slot;
            }

            var (SlotsButtonTextSuccess, SlotsButtonText) = BNCSConfig.ClientInstances.GetSlots(Instance, hwPanelButtonTextIndex, hwPanelButtonTextIndex);
            if (SlotsButtonTextSuccess)
            {
                slotButtonText = SlotsButtonText[0].Slot as Slot;
            }
        }

        public void Resend()
        {
            SendText(true);
            SendColour(true);
        }

        public void SendText(bool overrideTxRxState = false)
        {
            if (isLcd)
            {
                if (overrideTxRxState == true || BncsState == BncsState.TXRX)
                {
                    string outgoingText = PanelGenericAdaptor.ParseText(Instance.DeviceType.Name, Has2lines, Line1, Line2);

                    slotButtonText?.Set(outgoingText, false);
                }
            }
        }

        public void SendColour(bool overrideTxRxState = false)
        {
            if (isLcd)
            {
                if (overrideTxRxState == true || BncsState == BncsState.TXRX)
                {
                    string colourOutgoing = PanelGenericAdaptor.ParseColour(myBaseType, Background);

                    if (string.IsNullOrEmpty(colourOutgoing) == false)
                    {
                        slotButtonColour?.Set(colourOutgoing, false);
                    }
                    else
                    {
                        logger.Error($"Button {index} tried to update with background colour: {Background} which does not exist in hardware_panel_types.json");
                    }
                }                
            }
        }

        private void RemoteButtonPressed(object sender, EventSlotChange e)
        {
            if (PanelGenericAdaptor.IsButtonPressed(Instance.DeviceType.Name, RemoteIndex, e.Value))
            {
                MyButtonPressed?.Invoke(this, new ButtonPressedEventArgs());
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void Dispose()
        {
            if (isLcd)
            {
                Label = "";
                Background = "Black";
            }

            BNCSStatusModel.Instance.CommsStateChanged -= CommsStateChanged;

            if (slotButtonPressed != null)
            {
                slotButtonPressed.OnChange -= RemoteButtonPressed;
                slotButtonPressed.Dispose();
            }
            
            slotButtonColour?.Dispose();
            slotButtonText?.Dispose();

            slotButtonPressed = null;
            slotButtonColour = null;
            slotButtonText = null;
            Instance = null;
        }
    }

    public class ButtonPressedEventArgs : EventArgs
    {
        public ButtonPressedEventArgs()
        {

        }
    }
}
