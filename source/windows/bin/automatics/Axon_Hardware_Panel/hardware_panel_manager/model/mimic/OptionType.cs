﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.model.mimic
{
    public enum OptionType
    {
        Source,
        Destination,
        DestinationLocal, //creates a one to one mapping with a tally button.  Only one tally button is allowed per panel.
        AudioMode,
        Tally,
        Back,
        Function,
        Blank,
        PageSelect
    }

}
