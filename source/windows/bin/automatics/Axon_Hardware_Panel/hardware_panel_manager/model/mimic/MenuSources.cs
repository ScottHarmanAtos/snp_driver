﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    public abstract class MenuSources : INotifyPropertyChanged
    {
        private int index;

        public int Index
        {
            get { return index; }
            set
            {
                if (index != value)
                {
                    index = value;
                }
            }
        }

        private bool isBlank = false;

        public bool IsBlank
        {
            get { return isBlank; }
            set
            {
                if (isBlank != value)
                {
                    isBlank = value;
                }
            }
        }

        private string label;

        public string Label
        {
            get { return label; }
            set
            {
                if (label != value)
                {
                    label = value;

                    OnPropertyChanged("Label");
                }
            }
        }

        private string background = "LightSlateGray";

        public string Background
        {
            get { return background; }
            set
            {
                if (background != value)
                {
                    background = value;

                    OnPropertyChanged("Background");
                }
            }
        }

        public abstract void SetTielineMode(TielineMode tielineMode);        

        public abstract void Render();

        public abstract void UnRender();

        public abstract void OpenMenu(bool state);

        public abstract void ChangeCanBeSelected(bool state);

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
