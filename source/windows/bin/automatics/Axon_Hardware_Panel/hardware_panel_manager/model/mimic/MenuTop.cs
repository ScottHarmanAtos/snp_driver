﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    public delegate void TopMenuPressed(object sender, TopMenuPressedEventArgs e);

    public class MenuTop : INotifyPropertyChanged, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event TopMenuPressed MyTopButtonPressed;

        private ButtonModel button;

        private int index;

        public int Index
        {
            get { return index; }
            set
            {
                if (index != value)
                {
                    index = value;
                }
            }
        }

        private string label;

        public string Label
        {
            get { return label; }
            set
            {
                if (label != value)
                {
                    label = value;

                    OnPropertyChanged("Label");
                }
            }
        }

        private string background = "LightSlateGray";

        public string Background
        {
            get { return background; }
            set
            {
                if (background != value)
                {
                    background = value;

                    OnPropertyChanged("Background");
                }
            }
        }

        public ButtonModel Button { get => button; set => button = value; }

        public MenuTop(int index, string label, string colour, ButtonModel button)
        {
            this.Index = index;
            this.Label = label;
            this.Background = colour;
            this.Button = button;

            if (button != null)
            {
                button.MyButtonPressed += MyButtonPressed;

                Render();
            }
        }

        public void Render()
        {
            Button.Background = Background;
            Button.Label = Label;
        }

        public void UnRender()
        {
            Button.Background = "Black";
            Button.Label = string.Empty;
        }

        private void MyButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (index > 0)
                MyTopButtonPressed?.Invoke(this, new TopMenuPressedEventArgs());
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void Dispose()
        {
            if (button != null)
            {
                button.MyButtonPressed -= MyButtonPressed;
                button = null;
            }
        }
    }

    public class TopMenuPressedEventArgs : EventArgs
    {
        public TopMenuPressedEventArgs()
        {

        }
    }
}
