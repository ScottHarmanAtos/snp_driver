﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace driver_template.model.mimic
{
    public class PageMenuSourcesBlank : PageMenuSources
    {
        public PageMenuSourcesBlank(int index, string label, string colour, ButtonModel button, List<Option> options, bool canBeSelected, int landingIndex, List<string> tags) : 
            base(index, label, colour, button, options, string.Empty, canBeSelected, landingIndex, tags)
        {
            CanBeSelected = false;
            MenuOpened = false;

            Render();

            IsBlank = true;
        }

        public new void Render()
        {
            base.button.Background = "Black";
            base.button.Label = string.Empty;
        }

        public new void UnRender()
        {
            base.button.Background = "Black";
            base.button.Label = string.Empty;
        }
    }
}
