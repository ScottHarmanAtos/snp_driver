﻿using driver_template.model.bncs;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.model.mimic
{
    public enum TielineMode
    {
        none = 0,
        TX = 1,
        LIVE = 2,
        ENG = 3,
        SUP = 4,
        MV1 = 5,
        MV2 = 6,
        PACKAGER = 7,
        IPX = 8
    }

    public class GroupMenuSourcesContextual : MenuSources, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event TopMenuPressed MyTopButtonPressed;

        public event MenuSelected MyMenuSelected;

        public event OptionSelected OptionSelected;

        private ButtonModel button;

        private bool canBeSelected = true;

        public bool CanBeSelected
        {
            get { return canBeSelected; }
            set
            {
                if (canBeSelected != value)
                {
                    canBeSelected = value;
                }
            }
        }

        private bool menuOpened;

        public bool MenuOpened
        {
            get { return menuOpened; }
            set
            {
                if (menuOpened != value)
                {
                    menuOpened = value;
                }
            }
        }

        private List<PageMenuSources> options;

        public List<PageMenuSources> Options
        {
            get { return options; }
            set
            {
                if (options != value)
                {
                    options = value;
                }
            }
        }

        private MenuSources selectedOption;

        public MenuSources SelectedOption
        {
            get { return selectedOption; }
            set
            {
                if (selectedOption != value)
                {
                    selectedOption = value;
                }
            }
        }

        private RoomMode roomMode = RoomMode.none;

        public RoomMode RoomMode
        {
            get { return roomMode; }
            set
            {
                if (roomMode != value)
                {
                    roomMode = value;
                }
            }
        }

        private TielineMode tielineMode = TielineMode.none;

        public TielineMode TielineMode
        {
            get { return tielineMode; }
            set
            {
                if (tielineMode != value)
                {
                    tielineMode = value;

                    if (Options != null)
                    {
                        foreach (var option in Options)
                        {
                            option.SetTielineMode(TielineMode);
                        }
                    }
                }
            }
        }

        private Instance mfrInstance;

        public Instance MfrInstance
        {
            get { return mfrInstance; }
            set
            {
                if (mfrInstance != value)
                {
                    mfrInstance = value;
                }
            }
        }

        private SlotClient slotMfrChannel;

        public SlotClient SlotMfrChannel
        {
            get { return slotMfrChannel; }
            set
            {
                if (slotMfrChannel != value)
                {
                    slotMfrChannel = value;
                }
            }
        }

        private string selectedChannel;

        public string SelectedChannel
        {
            get { return selectedChannel; }
            set
            {
                if (selectedChannel != value)
                {
                    selectedChannel = value;

                    if (SelectedOption != null)
                    {
                        SelectedOption.ChangeCanBeSelected(false);
                        SelectedOption.OpenMenu(false);
                        SelectedOption.UnRender();

                        SelectedOption = null;
                    }

                    MyTopButtonPressed?.Invoke(this, new TopMenuPressedEventArgs());
                }
            }
        }

        public GroupMenuSourcesContextual(int index, string label, string colour, ButtonModel button, List<PageMenuSources> options, RoomMode roomMode, Instance mfrInstance)
        {
            this.Index = index;
            this.Label = label;
            this.Background = colour;
            this.button = button;
            this.Options = options;
            this.MfrInstance = mfrInstance;
                        
            this.RoomMode = roomMode;

            if (Options != null)
            {
                foreach (var option in Options)
                {
                    option.SetTielineMode(TielineMode);
                    option.MyMenuSelected += MySubMenuSelected;
                    option.OptionSelected += MySubMenuOptionSelected;
                }
            }

            if (RoomMode == RoomMode.MFR)
            {
                MenuOpened = true;

                var (SuccessClient, SlotClient) = BNCSConfig.ClientInstances.GetSlots(mfrInstance);
                if (SuccessClient)
                {
                    //Register for IW to the slot
                    var chObj = SlotClient.Find(f => f.Param.Name == "channel");
                    SlotMfrChannel = chObj.Slot;

                    SetChannelWithDelay(SlotMfrChannel.Value);

                    SlotMfrChannel.OnChange += SlotMfrChannel_OnChange;
                }
            }

            if (button != null)
            {
                button.MyButtonPressed += MyButtonPressed;
            }

            Render();
        }

        private async void SetChannelWithDelay(string value)
        {
            await Task.Delay(100);
            SelectedChannel = value;
        }

        private void SlotMfrChannel_OnChange(object sender, EventSlotChange e)
        {
            logger.Info($"ContextualGroup: {Label}, MFR: {MfrInstance.Id}, {MfrInstance.AltId}, channel: {e.Value}");

            SelectedChannel = e.Value;
        }

        private void MySubMenuOptionSelected(object sender, OptionSelectedEventArgs e)
        {
            if (CanBeSelected)
            {
                if (MenuOpened)
                {
                    OptionSelected?.Invoke(SelectedOption, new OptionSelectedEventArgs());
                }
            }
        }

        public override void SetTielineMode(TielineMode tielineMode)
        {
            TielineMode = tielineMode;
        }

        public override void Render()
        {
            if (Options != null)
            {
                if (SelectedChannel == "none")
                {
                    button.Background = Background;
                    button.Label = "Pick|Channel";

                    foreach (var option in Options)
                    {
                        if (option.IsBlank == false)
                        {                            
                            option.MenuOpened = false;
                            option.CanBeSelected = false;
                        }                        
                    }
                }
                else
                {
                    bool atLeastOnePage = false;

                    foreach (var option in Options)
                    {
                        if (option.IsBlank == false)
                        {
                            if (option.Tags != null && option.Tags.Contains(SelectedChannel))
                            {
                                atLeastOnePage = true;
                                break;
                            }
                        }
                    }

                    if (atLeastOnePage)
                    {
                        foreach (var option in Options)
                        {
                            if (option.IsBlank == false)
                            {
                                if (option.Tags != null && option.Tags.Contains(SelectedChannel))
                                {
                                    option.CanBeSelected = true;
                                    option.Render();
                                }
                                else
                                {
                                    option.MenuOpened = false;
                                    option.CanBeSelected = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        button.Background = Background;
                        button.Label = "No pages|for ch";

                        foreach (var option in Options)
                        {
                            if (option.IsBlank == false)
                            {
                                option.MenuOpened = false;
                                option.CanBeSelected = false;
                            }
                        }
                    }                    
                }
            }
        }

        public override void UnRender()
        {
            if (Options != null)
            {
                foreach (var option in options)
                {
                    option.UnRender();
                }
            }
        }

        public override void OpenMenu(bool state)
        {
            if (state == false)
            {
                foreach (var option in Options)
                {
                    option.UnRender();
                    option.MenuOpened = false;
                    option.CanBeSelected = false;
                }

                SelectedOption = null;
            }
        }

        public override void ChangeCanBeSelected(bool state)
        {
            CanBeSelected = state;
        }

        private async void MySubMenuSelected(object sender, MenuSelectedEventArgs e)
        {
            if (canBeSelected)
            {
                MenuSources newMenu = sender as MenuSources;

                if (SelectedOption != null)
                {
                    if (!SelectedOption.Equals(newMenu))
                    {
                        SelectedOption.ChangeCanBeSelected(false);
                        SelectedOption.OpenMenu(false);
                        SelectedOption.UnRender();

                        SelectedOption = newMenu;

                        SelectedOption.ChangeCanBeSelected(true);
                        SelectedOption.OpenMenu(true);
                        SelectedOption.Render();

                        await Task.Delay(300);
                        MyMenuSelected?.Invoke(SelectedOption, new MenuSelectedEventArgs());
                    }
                }
                else
                {
                    if (Options != null)
                    {
                        foreach (var menu in Options)
                        {
                            menu.ChangeCanBeSelected(false);
                        }
                    }

                    SelectedOption = newMenu;

                    SelectedOption.ChangeCanBeSelected(true);
                    SelectedOption.OpenMenu(true);
                    SelectedOption.Render();

                    await Task.Delay(300);
                    MyMenuSelected?.Invoke(SelectedOption, new MenuSelectedEventArgs());
                }
            }
        }

        private async void MyButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            if (canBeSelected)
            {
                if (MenuOpened == false)
                {
                    await Task.Delay(300);
                    MyMenuSelected?.Invoke(this, new MenuSelectedEventArgs());
                }                    
            }
        }

        public void Dispose()
        {
            if (Options != null)
            {
                foreach (var option in Options)
                {
                    if (option != null)
                    {
                        option.MyMenuSelected -= MySubMenuSelected;
                        option.OptionSelected -= MySubMenuOptionSelected;
                        option.Dispose();
                    }
                }

                Options.Clear();
            }

            SelectedOption = null;

            if (button != null)
            {
                button.MyButtonPressed -= MyButtonPressed;
                button = null;
            }

            if (slotMfrChannel != null)
            {
                SlotMfrChannel.OnChange -= SlotMfrChannel_OnChange;
                slotMfrChannel.Dispose();

                slotMfrChannel = null;
            }

            MfrInstance = null;
        }
    }
}
