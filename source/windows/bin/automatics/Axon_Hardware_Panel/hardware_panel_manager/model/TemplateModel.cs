﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.model
{
    public class TemplateModel : INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private string timestamp;

        public string Timestamp
        {
            get { return timestamp; }
            set
            {
                if (timestamp != value)
                {
                    timestamp = value;

                    OnPropertyChanged("Timestamp");
                }
            }
        }

        private string backupTimestamp;
        public string BackupTimestamp
        {
            get { return backupTimestamp; }
            set
            {
                if (backupTimestamp != value)
                {
                    backupTimestamp = value;

                    OnPropertyChanged("BackupTimestamp");
                }
            }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;

                    OnPropertyChanged("Name");
                }
            }
        }

        public TemplateModel(string name, string timestamp, string backupTimestamp)
        {
            this.Name = name;
            this.Timestamp = timestamp;
            this.BackupTimestamp = backupTimestamp;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
