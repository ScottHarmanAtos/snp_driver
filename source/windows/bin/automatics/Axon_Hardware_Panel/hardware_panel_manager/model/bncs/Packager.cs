﻿using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace driver_template.model.bncs
{
    public class Packager
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private enum MainDB
        {
            DEST_AUDIO_INDEX = 15,
            DEST_AUDIO_LANG_TYPE = 16,
            AUDIO_PRESET_NAME = 71,
            AUDIO_PRESET_LANG_TYPE = 72,
            SOURCE_PACKAGE_NAME = 2,
            EVENT_NAME = 30, //SPEV event name
            EVENT_ID = 31, //SPEV + Variant - Number from source code for playout
            RECORD_ID = 32, //Record ID @TODO Get actual database for ingest
            ROUTING_HINT = 81, //@TODO get real number
            AUDIO_LANG = 43,
            AUDIO_TAG = 44
        }

        public const uint MaxPackagesPerInfodriver = 4000;
        public uint PackageDivisor = 0;
        public uint NumberOfPackages = 0;
        Instance MainRouter;
        //Instance MainAudio;

        SourcePackage[] SourcePackages;
        Dictionary<uint,Instance> SourceInfodrivers = new Dictionary<uint, Instance>();

        DestPackage[] DestPackages;
        uint[] Devices; //Holds the device numbers for the infodrivers.
        Dictionary<uint, uint> DeviceToIndex = new Dictionary<uint, uint>();


        public struct RouterDestination
        {
            public Instance router;
            public uint destination;
        }

        public static Predicate<Instance> IsPackager
        {
            get;
            set;
        }


        public RouterDestination getInstanceFromDestination(Instance composite, uint destinationNo)
        {
            //method to return the modulo instance from a router
            RouterDestination compositeRouterDest = new RouterDestination();

            if (destinationNo < 0 || destinationNo > NumberOfPackages)
                return new RouterDestination();

            //offset index to get key
            uint deviceIndex = (destinationNo / MaxPackagesPerInfodriver) + 1;
            compositeRouterDest.destination = destinationNo % MaxPackagesPerInfodriver;
            if(SourceInfodrivers.TryGetValue(deviceIndex, out Instance value))
            compositeRouterDest.router = value;
            else return new RouterDestination();

            //@TODO - Hint should be sent as an infowrite
            //hint = hint ?? String.Empty;
            //BNCSConfig.BncsClient.databaseModify(Devices[0], destPackage, (uint)MainDB.ROUTING_HINT, hint);
            //BNCSConfig.BncsClient.databaseModify(Devices[0], destPackage, (uint)MainDB.DEST_AUDIO_LANG_TYPE, audioPreset.AudioLangTypes);
            //BNCSConfig.BncsClient.infoWrite(device, index, $"index={sourcePackage}");


            return compositeRouterDest;


        }

        public string TryGetValueFromPackager (Instance instance, uint destination)
        {
            string slotValue = "";
            if (SourceInfodrivers.TryGetValue(1, out Instance value))
            {
                var (SuccessDb, Database) = BNCSConfig.Database.GetDatabases(value, destination, 1, 0);
                if (SuccessDb)
                    slotValue = Database[0].DatabaseSlot.Value;

            }
            return slotValue;
        }

        public RouterDestination TryGetInstanceFromPackager (Instance instance, uint destination)
        {
            RouterDestination compositeInstance = new RouterDestination();
            if (instance.IsComposite)
            {
                //if I'm a composite, I need to check all the groups within each instance, to make sure it's applicable to a packager.
                //We are only interested in things that we can route.


                foreach(var childInstances in instance.GetAllChildInstances())
                {
                    if (childInstances.Value.IsComposite)
                        compositeInstance = TryGetInstanceFromPackager(childInstances.Value, destination);

                }
            }
            else
            {
                //really was expecting a composite here.  let's assume that this is valid, and we just stepped too far down the rabbit hole.
                compositeInstance.router = instance;
                compositeInstance.destination = destination;
            }
            return compositeInstance;
        }

        public string GetSourceName (Instance intance, uint source)
        {


            return null;
        }

        
        public Packager(Instance instance)
        {
            //Get Routers
            if (!instance.TryGetChildInstance("router", out var routerComp))
            {
                throw new Exception($"Failed to find group 'router' in {instance.Id}");
            }
            if (!routerComp.TryGetChildInstance("packager_router_1", out MainRouter))
            {
                throw new Exception($"Failed to find group 'packager_router_1' in {routerComp.Id}");
            }
            
            List<(uint infodriverNumber, uint device)> InfoDevices = new List<(uint infodriverNumber, uint device)>();
            foreach (var rtr in routerComp.GetAllChildInstances())
            {
                //@Assumption - continguous and starts from 1
                //Get all router instances
                var split = rtr.Key.Split('_'); //split this packager_router_1
                if (split.Length == 3 && uint.TryParse(split[2], out var i))
                {
                    InfoDevices.Add((i, rtr.Value.Device));
                    SourceInfodrivers.Add(i, rtr.Value);
                }
                else
                {
                    logger.Error($"Unexpected group in {routerComp.Id}. Group: {rtr.Key}, expecting package_router_N");
                }

            }

            //Get Audio
            /*if (!instance.TryGetChildInstance("audio_package_returns", out var audioComp))
            {
                throw new Exception($"Failed to find group 'audio_package_returns' in {instance.Id}");
            }
            if (!audioComp.TryGetChildInstance("packager_audio_package_1", out MainAudio))
            {
                throw new Exception($"Failed to find group 'packager_audio_package_1' in {audioComp.Id}");
            }*/

            //This grabs the packager information required
            //@Clean up, reutrn the values found
            LoadXml();
            SourcePackages = new SourcePackage[NumberOfPackages + 1];
            DestPackages = new DestPackage[NumberOfPackages + 1];

            //Check if instances and xml agree
            if (!(NumberOfPackages <= InfoDevices.Count * MaxPackagesPerInfodriver))
            {
                throw new Exception($"There are not enough package router infodrivers for the number of packages defined! Max Packages: {NumberOfPackages} - Infodrivers: {InfoDevices.Count} * {MaxPackagesPerInfodriver} = {InfoDevices.Count * MaxPackagesPerInfodriver}");
            }

            BNCSConfig.BncsClient.InfoRevertiveEvent += BncsClient_InfoRevertiveEvent;
            BNCSConfig.BncsClient.DatabaseChangeEvent += BncsClient_DatabaseChangeEvent;

            //Set the device list
            Devices = InfoDevices.OrderBy(x => x.infodriverNumber).Select(x => x.device).ToArray();
            for (uint i = 0; i < Devices.Length; ++i)
            {
                //Setup lookup
                DeviceToIndex.Add(Devices[i], i);
                //Register for all dest package routing.
                BNCSConfig.BncsClient.revertiveRegister(Devices[i], 1, MaxPackagesPerInfodriver);
                BNCSConfig.BncsClient.infoPoll(Devices[i], 1, MaxPackagesPerInfodriver);
            }

            //Audio lookup tables, 
            /*AudioLangLookup = CheckInUseState(DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.AUDIO_LANG));
            AudioTypeLookup = CheckInUseState(DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.AUDIO_TAG));

            //Store the Audio Prest names
            var audioPresetName = DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.AUDIO_PRESET_NAME);
            var audioPresetLangType = DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.AUDIO_PRESET_LANG_TYPE);
            foreach (var pn in audioPresetName)
            {
                if (audioPresetLangType.TryGetValue(pn.Key, out var lang))
                {
                    var audioPreset = new AudioPreset
                    {
                        Index = pn.Key,
                        Name = pn.Value,
                        AudioLangTypes = lang
                        //PresetLegs = GetPresetLegs(lang) @Just calc this every time?                       
                    };

                    //Store each way lookup
                    AudioPresets.Add(pn.Key, audioPreset);
                    if (!String.IsNullOrEmpty(audioPreset.Name))
                    {
                        if (!AudioPresetsByNameLookup.ContainsKey(audioPreset.Name))
                        {
                            AudioPresetsByNameLookup.Add(audioPreset.Name, audioPreset.Index);
                        }
                    }

                    //Check to ensure there are no duplicates 
                    //@Cleanup report duplicates?
                    if (!AudioPresetsLangTypeLookup.ContainsKey(audioPreset.AudioLangTypes))
                    {
                        AudioPresetsLangTypeLookup.Add(audioPreset.AudioLangTypes, audioPreset.Index);
                    }
                }
            }*/

            //Load Source Packages
            //Load Sport event ids, store them with the source packages and record event ids
            var sourceNames = DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.SOURCE_PACKAGE_NAME);
            //var sportEventId = DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.EVENT_ID);
            //var recordId = DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.RECORD_ID);
            for (uint i = 1; i <= NumberOfPackages; ++i)
            {
                SourcePackage sp = new SourcePackage
                {
                    Index = i
                };
                SourcePackages[i] = sp;
                sourceNames.TryGetValue(i, out sp.Name);

                //Sporting events are stored on the source package index
                //SetSportingEventID(i, sportEventId);
                //SetRecordId(i, recordId);
            }

            //Load Dest Packages
            for (uint i = 1; i <= NumberOfPackages; ++i)
            {
                DestPackage dp = new DestPackage
                {
                    Index = i
                };
                DestPackages[i] = dp;
            }

            //Store the Dest Package Indexes and Lang Types
            /*
            var destAudioIndexes = DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.DEST_AUDIO_INDEX);
            var destAudioLangType = DatabaseReader.ReadDatabase(MainRouter.Device, (uint)MainDB.DEST_AUDIO_LANG_TYPE);
            //Stores the dest package information
            foreach (var ai in destAudioIndexes)
            {
                if (destAudioLangType.TryGetValue(ai.Key, out var lang))
                {
                    var audioInfo = new AudioInfo
                    {
                        Index = ai.Key,
                        AudioIndexes = ai.Value,
                        AudioLangType = lang
                    };
                    DestPackages[ai.Key].Audio = audioInfo;
                }
            }*/
        }

        public struct SourcePackage
        {
            public static SourcePackage Empty = new SourcePackage();

            public uint Index;
            public string Name;
            //public SportingEvent SportingEvent;
            //public RecordEvent RecordEvent;
        }

        public struct DestPackage
        {
            public static DestPackage Empty = new DestPackage { Index = 0, Name = null, SourceIndex = 0 };

            public uint Index;
            public string Name;
            public uint SourceIndex;
            //public AudioInfo Audio;
        }

        private void BncsClient_DatabaseChangeEvent(uint device, uint index, int database)
        {
            if (database == (int)MainDB.EVENT_ID || database == (int)MainDB.RECORD_ID || database == (int)MainDB.DEST_AUDIO_LANG_TYPE)
            {
                if (index > NumberOfPackages)
                {
                    logger.Error($"Database index: {index} too high, device: {device} database: {database}");
                    return;
                }
                //not touching the audio stuff - commmenting out until required
                //Database changes here
                /*
                if (database == (int)MainDB.EVENT_ID)
                {
                    SetSportingEventID(index);
                }
                else if (database == (int)MainDB.RECORD_ID)
                {
                    SetRecordId(index);
                }
                else if (database == (int)MainDB.DEST_AUDIO_LANG_TYPE)
                {
                    BNCSConfig.BncsClient.databaseName(MainRouter.Device, index, (uint)MainDB.DEST_AUDIO_LANG_TYPE, out var value);
                    DestPackages[index].Audio.AudioLangType = value;
                }*/
            }/*
            else if (database == (int)MainDB.AUDIO_PRESET_NAME)
            {
                if (AudioPresets.TryGetValue(index, out var audio))
                {
                    BNCSConfig.BncsClient.databaseName(MainRouter.Device, index, (uint)MainDB.AUDIO_PRESET_NAME, out var newAudioName);
                    if (audio.Name == newAudioName) return; //Do nothing, as nothing has changed

                    var oldAudioName = audio.Name;
                    audio.Name = newAudioName;
                    AudioPresets[index] = audio;

                    //Add new lookup
                    if (!String.IsNullOrEmpty(audio.Name) && !AudioPresetsByNameLookup.ContainsKey(audio.Name))
                    {
                        AudioPresetsByNameLookup.Add(audio.Name, audio.Index);
                    }
                    else if (AudioPresetsByNameLookup.ContainsKey(audio.Name))
                    {
                        logger.Error($"Audio Preset Name is duplicated: {audio.Name}");
                    }

                    //Remove old name from lookup
                    if (!String.IsNullOrEmpty(oldAudioName))
                    {
                        AudioPresetsByNameLookup.Remove(oldAudioName);
                    }
                }
            }
            else if (database == (int)MainDB.AUDIO_PRESET_LANG_TYPE)
            {
                if (AudioPresets.TryGetValue(index, out var audio))
                {
                    BNCSConfig.BncsClient.databaseName(MainRouter.Device, index, (uint)MainDB.AUDIO_PRESET_LANG_TYPE, out var newAudioLangType);
                    if (audio.AudioLangTypes == newAudioLangType) return; //Do nothing, as nothing has changed

                    var oldAudioLangType = audio.AudioLangTypes;
                    audio.AudioLangTypes = newAudioLangType;
                    AudioPresets[index] = audio;

                    //Add new lookup
                    if (!String.IsNullOrEmpty(audio.AudioLangTypes) && !AudioPresetsLangTypeLookup.ContainsKey(audio.AudioLangTypes))
                    {
                        AudioPresetsLangTypeLookup.Add(audio.AudioLangTypes, audio.Index);
                    }
                    else if (AudioPresetsLangTypeLookup.ContainsKey(audio.AudioLangTypes))
                    {
                        logger.Error($"Audio Preset Lang Type is duplicated: {audio.AudioLangTypes}");
                    }

                    //Remove old name from lookup
                    if (!String.IsNullOrEmpty(oldAudioLangType))
                    {
                        AudioPresetsLangTypeLookup.Remove(oldAudioLangType);
                    }
                }
            }*/
            //@Todo handle audio dest changes
            //@Todo handle audio source changes
        }

        private void BncsClient_InfoRevertiveEvent(uint device, uint index, string value)
        {
            //Revertives here
            if (DeviceToIndex.TryGetValue(device, out var deviceIndex))
            {
                var destIndex = (deviceIndex * 4000) + index;

                var p = PackageRevertive.Parse(value);
                if (p.Valid)
                {
                    DestPackages[destIndex].SourceIndex = p.Index;
                }
            }
        }

        private void LoadXml()
        {
            string path = Path.Combine(Helper.ConfigPath, "packager_config.xml");
            if (!File.Exists(path))
            {
                throw new Exception($"Failed to find packager_config.xml in {path}");
            }
            XDocument xdoc = XDocument.Load(path);

            var settings = xdoc.Descendants("setting");
            foreach (var s in settings)
            {
                var id = s.Attribute("id").Value;
                var value = s.Attribute("value").Value;
                if (id == "number_of_packages")
                {
                    if (!uint.TryParse(value, out NumberOfPackages))
                    {
                        throw new Exception($"Failed to parse 'number_of_packages' in {path}");
                    }
                }
                else if (id == "packager_divisor")
                {
                    if (!uint.TryParse(value, out PackageDivisor))
                    {
                        throw new Exception($"Failed to parse 'packager_divisor' in {path}");
                    }
                }
            }

            if (NumberOfPackages == 0)
            {
                throw new Exception($"Failed to find 'number_of_packages' in {path}");
            }
        }

        public struct PackageRevertive
        {
            public uint Index;
            public uint V_Level;
            public bool Valid;

            public static PackageRevertive Parse(string value)
            {
                PackageRevertive pr = new PackageRevertive();
                pr.Valid = true;
                pr.Index = 0;
                pr.V_Level = 0;

                if (string.IsNullOrEmpty(value))
                {
                    pr.Valid = false;
                    return pr;
                }

                //index=1,v_level=0
                var split = value.Split(',');
                for (int i = 0; i < split.Length; ++i)
                {
                    var split2 = split[i].Split('=');
                    if (split2.Length == 2)
                    {
                        switch (split2[0])
                        {
                            case "index":
                                if (!uint.TryParse(split2[1], out pr.Index))
                                {
                                    pr.Valid = false;
                                }
                                break;
                            case "v_level":
                                uint.TryParse(split2[1], out pr.V_Level);
                                break;
                        }
                    }
                    else
                    {
                        pr.Valid = false;
                    }
                }
                return pr;
            }

        }

        public bool Route(uint destPackage, uint sourcePackage)
        {
            if (destPackage < 0 || destPackage > NumberOfPackages)
                return false;

            if (sourcePackage < 0 || sourcePackage > NumberOfPackages)
                return false;

            
            //if (!AudioPresetsByNameLookup.TryGetValue(presetName, out var audioPresetIndex))
            //    return false;
            //var audioPreset = AudioPresets[audioPresetIndex];

            uint deviceIndex = destPackage / MaxPackagesPerInfodriver;
            uint index = destPackage % MaxPackagesPerInfodriver;
            uint device = Devices[deviceIndex];
            //@TODO - Hint should be sent as an infowrite
            //hint = hint ?? String.Empty;
            //BNCSConfig.BncsClient.databaseModify(Devices[0], destPackage, (uint)MainDB.ROUTING_HINT, hint);
            //BNCSConfig.BncsClient.databaseModify(Devices[0], destPackage, (uint)MainDB.DEST_AUDIO_LANG_TYPE, audioPreset.AudioLangTypes);
            BNCSConfig.BncsClient.infoWrite(device, index, $"index={sourcePackage}");

            return true;
        }


        public void InfoPoll(uint destPackage)
        {
            //This is a stub for InfoPoll - only takes the destintion package, as I can work out which device i'm talking to in trivial fashion.

            uint deviceIndex = destPackage / MaxPackagesPerInfodriver;
            uint index = destPackage % MaxPackagesPerInfodriver;
            uint device = Devices[deviceIndex];
            BNCSConfig.BncsClient.infoPoll(device, index, index, true); 


        }
    }
}