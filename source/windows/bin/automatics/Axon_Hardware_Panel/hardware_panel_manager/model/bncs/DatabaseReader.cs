﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.model.bncs
{
    /// <summary>
    /// To slow reading in 64000 items from CSI
    /// </summary>
    public static class DatabaseReader
    {
        public static Dictionary<uint, string> ReadDatabase(uint device, uint database)
        {
            //@Assumption - Database will always be in db files, and always in the expected number format
            var databaseIthLeadingZero = database <= 9 ? database.ToString() : database.ToString("D4");
            var databasePath = Path.Combine(instances_and_devicetypes.Helper.ConfigPath, "system", $"dev_{device}.DB{databaseIthLeadingZero}");

            Dictionary<uint, string> DBItems = new Dictionary<uint, string>();

            if (!File.Exists(databasePath)) return DBItems;

            bool headerFound = false;
            foreach (var l in File.ReadAllLines(databasePath))
            {
                //Skip Comments
                if (l.StartsWith(";")) continue;
                if (l.StartsWith("[") && l.EndsWith("]"))
                {
                    //Header
                    if (l == $"[Database_{database}]")
                    {
                        headerFound = true;
                    }
                    else if (headerFound)
                    {
                        //End of the section we are interested in
                        break;
                    }

                    continue;
                }
                if (headerFound)
                {
                    //Only deal with happy path, if we hit an error just ignore
                    var keyValue = l.Split('=');
                    if (keyValue.Length >= 2)
                    {
                        if (uint.TryParse(keyValue[0], out var k))
                        {
                            var s = keyValue[1];
                            if (keyValue.Length > 2)
                            {
                                s = String.Join("=", keyValue.Skip(1));
                            }
                            if (!DBItems.ContainsKey(k))
                            {
                                DBItems.Add(k, s);
                            }
                        }
                    }
                }
            }
            return DBItems;
        }
    }
}
