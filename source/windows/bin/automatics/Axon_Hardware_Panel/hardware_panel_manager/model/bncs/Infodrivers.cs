﻿using BncsInfodriverLibrary;
using driver_template.helpers;
using instances_and_devicetypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace driver_template.model.bncs
{

    public interface IInfodriverStatus
    {
        BncsState Status { get; }
    }

    public static class Infodrivers
    {
        public static BncsInfodriver Main { get; private set; }
        private static uint MainDevice { get; set; } //Maybe it would be useful for this not to be private?
        public static Dictionary<uint, BncsInfodriver> All { get; private set; } = new Dictionary<uint, BncsInfodriver>();

        static Infodrivers()
        {
            Settings.UseMasterSlaveTXRX.Changed += UseMasterSlaveTXRX_Changed;
        }

        private static void UseMasterSlaveTXRX_Changed(object sender, EventSettingChange<bool> arg)
        {
            //If this setting changed
            if (arg.Setting)
            {
                All.ToList().ForEach(X => X.Value.setRedundancyMaster(MainDevice));
            }
            else
            {
                //Set all infodrivers to manage themselfs.
                All.ToList().ForEach(x => x.Value.setRedundancyMaster(x.Key));
            }
        }

        /// <summary>
        /// Attempts to add the infodriver for the instance, this is synchronous
        /// </summary>
        /// <param name="instance"></param>
        /// <returns>ture on success or if the infodriver is already in the list</returns>
        public static bool Add(Instance instance)
        {
            //Check if it has already been added.
            if (!All.ContainsKey(instance.Device))
            {
                //Create new infodriver
                var infodriver = new BncsInfodriver();

                //Connect to that infodriver
                var r = infodriver.connect(instance.Device); //False is connected!

                if (r == false)
                {
                    infodriver.RedundancyStateEvent += Infodriver_RedundancyStateEvent;
                    infodriver.DisconnectedEvent += Infodriver_DisconnectedEvent;

                    //If in Development mode don't close the infodriver on exit.
                    if (BNCSConfig.StartupArguments.Development)
                        infodriver.setCloseHostOnExit(false);
                    else
                        infodriver.setCloseHostOnExit(true);

                    if (Main == null)
                    {
                        Main = infodriver;
                        MainDevice = instance.Device;
                    }

                    if (Settings.UseMasterSlaveTXRX)
                        infodriver.setRedundancyMaster(MainDevice);
                    else
                        infodriver.setRedundancyMaster(instance.Device);

                    All.Add(instance.Device, infodriver);
                }
                InfodriverAdded?.Invoke(typeof(Infodrivers), new EventInfodriverAdded(instance.Device, infodriver));
                UpdateInfodriverStatus();
                return !r;
            }
            return true;
        }

        private static void Infodriver_DisconnectedEvent(uint device)
        {
            UpdateInfodriverStatus();
        }

        private static void Infodriver_RedundancyStateEvent(eRedundancyState state)
        {
            UpdateInfodriverStatus();
        }

        public static void Dispose()
        {
            if (All != null)
            {
                //Force all the RX for closing.
                All.ToList().ForEach(x => x.Value.setRedundancyState(eRedundancyState.rx, true));
                All.ToList().ForEach(x => x.Value.Dispose());
            }
        }
        private static void UpdateInfodriverStatus()
        {
            if (All.Any(x => x.Value.isConnected() == false))
            {
                Status = BncsState.Error;
                return;
            }
            int TX = All.Where(x => x.Value.redundancyState() == eRedundancyState.tx).Count();
            if (TX == All.Count)
                Status = BncsState.TXRX;
            else if (TX == 0)
                Status = BncsState.RXOnly;
            else
                Status = BncsState.Mixed;
        }
        private static BncsState status = BncsState.RXOnly;

        public static BncsState Status
        {
            get { return status; }
            private set
            {
                if (value != status)
                {
                    status = value;
                    InfodriverStatusChanged?.Invoke(typeof(Infodrivers), new EventInfodriverStatusChange(status));
                }
            }
        }

        public static void ForceTXRX()
        {
            if (Settings.UseMasterSlaveTXRX)
            {
                if (Main != null)
                    Main.setRedundancyState(eRedundancyState.tx, true);
            }
            else
                All.ToList().ForEach(x => x.Value.setRedundancyState(eRedundancyState.tx, true));
        }

        public static void ForceRxOnly()
        {
            if (Settings.UseMasterSlaveTXRX)
            {
                if (Main != null)
                    Main.setRedundancyState(eRedundancyState.rx, true);
            }
            else
                All.ToList().ForEach(x => x.Value.setRedundancyState(eRedundancyState.rx, true));
        }

        public static void SetDriverState(eDriverState DriverState, bool Now, bool Force)
        {
            if (Settings.UseMasterSlaveTXRX)
            {
                Main.setDriverState(DriverState, Now, Force);
            }
            else
                All.ToList().ForEach(x => x.Value.setDriverState(DriverState, Now, Force));
        }

        public static event EventHandler<EventInfodriverStatusChange> InfodriverStatusChanged;
        public static event EventHandler<EventInfodriverAdded> InfodriverAdded;
    }

    public class EventInfodriverStatusChange : EventArgs
    {
        public BncsState State { get; }
        public EventInfodriverStatusChange(BncsState state)
        {
            State = state;
        }
    }

    public class EventInfodriverAdded : EventArgs
    {
        public uint Device { get; }
        public BncsInfodriver Infodriver { get; }
        public EventInfodriverAdded(uint device, BncsInfodriver infodriver)
        {
            Device = device;
            Infodriver = infodriver;
        }

    }
}
