﻿using System;
using System.ComponentModel;

namespace driver_template.model.bncs
{
    /// <summary>
    /// This is database slot the driver can read from and write too
    /// </summary>
    public class SlotDatabase : Slot
    {

        public new static (SlotDatabase Slot, Action<String> Update) Create(string InitialValue, Action<string> set, Action updateTxCount, Action updateRxCount)
        {
            var s = new SlotDatabase(InitialValue, set, updateTxCount, updateRxCount);
            return (s, (str) => { s.Changed(str); });
        }


        protected SlotDatabase(string initialValue, Action<string> set, Action updateTxCount, Action updateRxCount) : base(initialValue, set, updateTxCount, updateRxCount)
        {
            Type = SlotType.Database;
        }

        /// <summary>
        /// Set a Value, this will do a routermodify
        /// </summary>
        /// <param name="value"></param>
        protected override void SetValue(String value)
        {
            this.Set(value);
        }

        /// <summary>
        /// RM arriving from the network
        /// </summary>
        /// <param name="value"></param>
        protected override void Changed(string value)
        {
            this.value = value;
            base.Changed(value);
        }

    }
}
