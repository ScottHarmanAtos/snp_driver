﻿using BncsClientLibrary;
using driver_template.json;
using driver_template.xml;
using helpers;
using instances_and_devicetypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace driver_template.model.bncs
{
    /// <summary>
    /// This class hold all the config that has been loaded for the driver.
    /// It is a static class so can be called from anywhere
    /// </summary>
    public static class BNCSConfig
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static StartupArguments StartupArguments { get; set; }
        public static Instance MainInstance { get; private set; }
        public static BncsClient BncsClient { get; private set; }
        public static ManagedInstances ManagedInstances { get; private set; }
        public static ClientInstances ClientInstances { get; private set; }
        //packager definition
        public static Instance Packager { get; set; }
        public static Databases Database { get; private set; }

        public static Dictionary<string, JBasePanelType> PanelBaseTypes { get; private set; }        

        public static Dictionary<string, List<JObject>> PanelTemplates { get; private set; }

        public static Dictionary<string, string> PanelFiles { get; private set; }

        public static XhwPanelsConfig HardwarePanelsDestinations { get; private set; }

        public static uint SourcesNamesDatabase { get; private set; }

        public static uint DestinationsNamesDatabase { get; private set; }

        public static void Set(
            Instance mainInstance,
            BncsClient bncsClient,
            ManagedInstances managedInstances,
            ClientInstances clientInstances,
            Databases databases,
            Dictionary<string, JBasePanelType> panelBaseTypes,            
            Dictionary<string, List<JObject>> panelTemplates,
            Dictionary<string, string> panelFiles,
            XhwPanelsConfig hardwarePanelsDestination
            )
        {
            MainInstance = mainInstance;
            BncsClient = bncsClient;
            ManagedInstances = managedInstances;
            ClientInstances = clientInstances;
            Database = databases;
            PanelBaseTypes = panelBaseTypes;            
            PanelTemplates = panelTemplates;
            PanelFiles = panelFiles;
            HardwarePanelsDestinations = hardwarePanelsDestination;


            SourcesNamesDatabase = 0;
            DestinationsNamesDatabase = 1;
        }

        public static uint destinationNo { get; internal set; }

        public static void ReLoadJsonTemplates(ClientInstances clientInstances)
        {
            //filter on .json flles only
            List<string> templates = new List<string>();

            string[] schemas = Directory.GetFiles(Path.Combine(instances_and_devicetypes.Helper.ConfigPath, "hardware_panels"), "*schema.json");
            JSchema jsonSchema = new JSchema();

            Dictionary<string, List<JObject>> newPanelTemplates = new Dictionary<string, List<JObject>>();
            Dictionary<string, string> newPanelFiles = new Dictionary<string,string>();

            foreach (var instance in clientInstances.Instances)
            {
                //string instance_dir_string = instance.Key.Replace('/', '~');
                //String path = Path.Combine(Path.Combine(Path.Combine(instances_and_devicetypes.Helper.ConfigPath, "hardware_panels"), "hardware_panel_templates"), instance_dir_string);
                String path = Path.Combine(Path.Combine(instances_and_devicetypes.Helper.ConfigPath, "hardware_panels"), "hardware_panel_templates");
                if (Directory.Exists(path))
                    templates.AddRange(Directory.GetFiles(path, "*.json"));
                String backupPath = path + "/backup";
                if (Directory.Exists(backupPath))
                    templates.AddRange(Directory.GetFiles(backupPath, "*.bak"));
                else Directory.CreateDirectory(backupPath);
            }

            foreach (var schema in schemas)
            {
                logger.Info($"Using {Path.GetFileNameWithoutExtension(schema)} for input validation");
                string schemacontent = File.ReadAllText(schema);
                jsonSchema = JSchema.Parse(schemacontent);
            }

            foreach (var s in templates)
            {
                var fullFileName = Path.GetFileName(s);

                if (File.Exists(s))
                {
                    string content = File.ReadAllText(s);

                    try
                    {
                        JArray validateContent = JsonConvert.DeserializeObject<JArray>(content);
                        IList<string> errorMessages;
                        bool valid = validateContent.IsValid(jsonSchema, out errorMessages);
                        if (valid)
                        {
                            logger.Info($"Validation of {fullFileName} succeeded.");

                            List<JObject> template = JsonConvert.DeserializeObject<List<JObject>>(content);
                            if (template != null)
                            {
                                if (!newPanelTemplates.ContainsKey(fullFileName))
                                {
                                    if (template.First().SelectToken("timestamp") == null)
                                        template.First().Add("timestamp", File.GetLastWriteTime(s).ToUniversalTime());
                                    //suppress adding existing backup files
                                    DateTime backupTimestamp = DateTime.Now.ToUniversalTime();
                                    newPanelTemplates.Add(fullFileName, template);
                                    newPanelFiles.Add(fullFileName, s);

                                    //Don't consider backing up if we are evaluating files already in the backup folder.
                                    if (!s.Contains("backup"))
                                    {
                                        IEnumerable<string> backupFileExists = Directory.EnumerateFiles(Path.Combine(Path.GetDirectoryName(s), "backup"));
                                        //string pathCheck = fullFileName.Substring(0, fullFileName.IndexOf("_"));
                                        if (!backupFileExists.Select(x => x.Contains(fullFileName.Substring(0, fullFileName.IndexOf(".json")))).Any())
                                        {
                                            /*
                                             * Backup the file if there isn't one that matchs the naming pattern of files already in the folder
                                             * 
                                             * 
                                             */
                                            using (StreamWriter backupFile = File.CreateText(Path.Combine(Path.GetDirectoryName(s), Path.Combine("backup", $"{Path.GetFileNameWithoutExtension(s)}_{backupTimestamp:yyyy_MM_dd_HH-mm-ss}.bak"))))
                                            {
                                                List<JObject> backupTemplate = template;
                                                backupTemplate.First().Remove("backupTimestamp");
                                                backupTemplate.First().Add("backupTimestamp", backupTimestamp);
                                                backupFile.Write(JsonConvert.SerializeObject(backupTemplate));
                                            }
                                        }
                                        else
                                        {
                                            /*
                                             * Backup files if the file in the root folder has been modified.
                                             * 
                                             */
                                            bool anyBackup = false;
                                            foreach (var file in backupFileExists.Where(x => x.Contains(fullFileName.Substring(0, fullFileName.IndexOf("_")))))
                                            {
                                                string backupcontent = File.ReadAllText(file);
                                                List <JObject> backuptemplate = JsonConvert.DeserializeObject<List<JObject>>(backupcontent);
                                                backuptemplate.First().Remove("backupTimestamp");
                                                if (JToken.DeepEquals(template.First(), backuptemplate.First()))
                                                {
                                                    anyBackup = true;
                                                }
                                            }
                                            if(!anyBackup)
                                            {
                                                using (StreamWriter backupFile = File.CreateText(Path.Combine(Path.GetDirectoryName(s), Path.Combine("backup", $"{Path.GetFileNameWithoutExtension(s)}_{backupTimestamp:yyyy_MM_dd_HH-mm-ss}.bak"))))
                                                {
                                                    List<JObject> backupTemplate = template;
                                                    backupTemplate.First().Remove("backupTimestamp");
                                                    backupTemplate.First().Add("backupTimestamp", backupTimestamp);
                                                    backupFile.Write(JsonConvert.SerializeObject(backupTemplate));
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                        }
                        else
                        {
                            logger.Error($"Validation of {fullFileName} failed.");
                            foreach (var error in errorMessages) { logger.Error($"{error}"); }
                            throw new FileFormatException($"{fullFileName} does not meet minimum requirements outlined in schema.");
                        }
                    }
                    catch (Exception exp)
                    {
                        logger.Error($"Exception deserialising panel template {s}, exp: {exp.ToString()}");
                    }
                }
            }

            PanelTemplates = newPanelTemplates;
            PanelFiles = newPanelFiles;

            Mediator.Instance.NotifyColleagues(ViewModelMessages.TemplatesRendered, true);
        }

        private static bool Disposing = false;

        public static void Dispose()
        {
            if (Disposing)
                return;
            Disposing = true;

            if (BncsClient != null)
            {
                if (BncsClient.isConnected())
                {
                    BncsClient.writeEnable(false);
                    BncsClient.disconnect();
                }
                BncsClient.Dispose();
            }
        }
    }
}
