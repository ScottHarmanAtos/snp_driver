﻿using driver_template.helpers;
using System;

namespace driver_template.model
{
    /// <summary>
    /// Settings holds settings that are needed for the driver.
    /// These are static so can be accessed from anywhere, they are loaded on startup
    /// The settings are stored in drivers_config.xml
    /// Add any setting that is required by the program that does not make sense to add to instances
    /// 
    /// The class is updated from the DriverSettings Class
    /// </summary>
    public static class Settings
    {
        public static Setting<DebugLevels> DebugLevel { get; private set; } =
            new Setting<DebugLevels>(DebugLevels.Normal, String.Join(",", Enum.GetNames(typeof(DebugLevels))), false, Enum.TryParse<DebugLevels>, true);
        public static Setting<DebugLevels> LogLevel { get; private set; } =
            new Setting<DebugLevels>(DebugLevels.Normal, String.Join(",", Enum.GetNames(typeof(DebugLevels))), false, Enum.TryParse<DebugLevels>, true);

        public static Setting<bool> UseMasterSlaveTXRX { get; private set; } =
            new Setting<bool>(true, "Newer version of CSI allow a single infodriver to control the TXRX state of all infodrivers controlled.", false);

        /// <summary>
        /// Additional checks are called after all settings are loaded.
        /// </summary>
        /// <returns></returns>
        public static (bool Success, String ErrorMessage) AdditionalChecks()
        {

            Logging.SetLevelDebug(DebugLevel);
            Logging.SetLevelLog(LogLevel);
            return (true, null);
        }
    }

}
