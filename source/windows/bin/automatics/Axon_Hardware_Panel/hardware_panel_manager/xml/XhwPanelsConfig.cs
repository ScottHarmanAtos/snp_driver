﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace driver_template.xml
{
    [Serializable, XmlRoot("hardware_panel_destinations")]
    public class XhwPanelsConfig
    {
        [XmlElement("panel")]
        public List<XhwPanelConfig> panels { get; set; }
    }
}
