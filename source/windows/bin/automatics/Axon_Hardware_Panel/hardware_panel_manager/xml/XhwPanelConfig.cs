﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace driver_template.xml
{
    [Serializable()]
    public class XhwPanelConfig
    {
        [XmlAttribute("id")]
        public string id { get; set; }

        [XmlElement("destination")]
        public List<XhwPanelDestination> destination { get; set; }

    }
}
