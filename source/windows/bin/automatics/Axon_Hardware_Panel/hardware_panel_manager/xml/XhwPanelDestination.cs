﻿using driver_template.model.mimic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace driver_template.xml
{

    [Serializable()]
    public class XhwPanelDestination
    {
        internal OptionType optionType { get; set; }

        [XmlAttribute("source_names")]
        public string source_names { get; set; }

        [XmlAttribute("router")]
        public string router { get; set; }

        [XmlAttribute("dest")]
        public string dest { get; set; }

    }
}
