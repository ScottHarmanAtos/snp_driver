﻿using System;
using System.ComponentModel;

namespace driver_template.model.bncs
{
    /// <summary>
    /// This is an infodriver slot the driver_template can read from and write too, but is not in control of
    /// </summary>
    public class SlotClient : Slot
    {
        public static (SlotClient Slot, Action<String> Update) Create(uint device, uint index, string InitialValue, Action<string> set, Action updateTxCount, Action updateRxCount, Action poll)
        {
            var s = new SlotClient(device, index, InitialValue, set, updateTxCount, updateRxCount, poll);
            return (s, (str) => { s.Changed(str); });
        }

        protected SlotClient(uint device, uint index, string InitialValue, Action<string> set, Action updateTxCount, Action updateRxCount, Action poll) : base(device, index, InitialValue, set, updateTxCount, updateRxCount)
        {
            Type = SlotType.InfodriverClient;
            this.poll = poll;
        }

        /// <summary>
        /// Set a Value, this will do an infodriver write on the network
        /// </summary>
        /// <param name="value"></param>
        protected override void SetValue(String value)
        {
            this.Set(value);
        }

        /// <summary>
        /// Revertive arriving from the network
        /// </summary>
        /// <param name="value"></param>
        protected override void Changed(string value)
        {
            this.value = value;
            base.Changed(value);
        }

        readonly Action poll = null;

        public void Poll()
        {
            poll?.Invoke();
        }
    }
}
