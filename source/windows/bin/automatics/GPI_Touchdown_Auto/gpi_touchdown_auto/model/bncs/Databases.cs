﻿using BncsClientLibrary;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace driver_template.model.bncs
{
    /// <summary>
    /// Links to Databases
    /// </summary>
    public class Databases
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private BncsClient BncsClient;
        private Dictionary<(uint Device, uint Index, uint Db), (SlotDatabase DatabaseSlot, Action<String> Update)> Slots = new Dictionary<(uint Device, uint Index, uint Db), (SlotDatabase DatabaseSlot, Action<String> Update)>();
        readonly HashSet<uint> InfodriversLinked = new HashSet<uint>();
        public uint MaxDatabase { get; }

        public Databases(BncsClient BncsClient)
        {
            this.BncsClient = BncsClient;
            BncsClient.DatabaseChangeEvent += BncsClient_DatabaseChangeEvent;
            MaxDatabase = BncsClient.maxDatabases();
        }

        private void BncsClient_DatabaseChangeEvent(uint device, uint index, int database)
        {
            if (Slots.TryGetValue((device, index, (uint)database), out var u))
            {
                BncsClient.databaseName(device, index, (uint)database, out var s);
                u.Update(s);
            }
        }

        public (bool Success, List<(uint Index, SlotDatabase DatabaseSlot)> DatabaseSlots) GetDatabases(Instance Instance, uint StartIndex, uint Length, uint Database)
        {
            //Check if the DeviceType is Good
            if (StartIndex == 0)
            {
                logger.Error($"Failed to Get Database for Instance:{Instance.Id}, StartIndex cannot be 0.");
            }
            if (Length == 0)
            {
                logger.Error($"Failed to Get Database for Instance:{Instance.Id}, Length cannot be 0.");
            }

            var l = new List<(uint, SlotDatabase)>();

            //Get all the slots
            for (uint i = StartIndex; i < StartIndex + Length; ++i)
            {
                var r = LoadSlot(Instance, i, Database);
                if (r.Success == false)
                {
                    logger.Error($"Failed to Get Slots for Instance:{Instance.Id}, Slot:{i} caused an error.");
                    return (false, null);
                }
                l.Add((i, r.Slot));
            }
            return (true, l);
        }

        public (bool Success, SlotDatabase DatabaseSlot) GetDatabase(Instance Instance, Parameter Param, uint Database)
        {
            return LoadSlot(Instance, Param.Slot, Database);
        }

        public (bool Success, List<(Parameter Param, SlotDatabase DatabaseSlot)> DatabaseSlots) GetDatabases(Instance Instance, uint Database)
        {
            //Check that the DeviceType is Good
            if (!Instance.DeviceType.IsGood)
            {
                logger.Error($"Failed to Get Database for Instance:{Instance.Id}, the DeviceType:{Instance.DeviceType} is not valid.");
                return (false, null);
            }

            var l = new List<(Parameter, SlotDatabase)>();

            //Get all the slots
            foreach (var p in Instance.DeviceType.Parameters.Where(x => x.Slot > 0))
            {
                var (Success, DatabaseSlot) = GetDatabase(Instance, p, Database);
                if (Success == false)
                {
                    logger.Error($"Failed to Get Database for Instance:{Instance.Id}, the DeviceType:{Instance.DeviceType}, Param:{p.Name} caused an error.");
                    return (false, null);
                }
                l.Add((p, DatabaseSlot));
            }
            return (true, l);
        }

        private (bool Success, SlotDatabase Slot) LoadSlot(Instance Instance, uint SlotIndex, uint Database)
        {
            if (Instance.Device <= 0)
            {
                logger.Error($"Failed to get Instance:{Instance.Id}, Slot:{SlotIndex}, Device:{Database}, Device cannot be zero.");
                return (false, null);
            }

            if (SlotIndex <= 0)
            {
                logger.Error($"Failed to get Instance:{Instance.Id}, Slot:{SlotIndex}, Database:{Database}, Index cannot be zero.");
                return (false, null);
            }

            if (Database > MaxDatabase)
            {
                logger.Error($"Failed to get  Instance:{Instance.Id}, Slot:{SlotIndex}, Database:{Database}, Max Database is {MaxDatabase}.");
                return (false, null);
            }

            var slotWithOffset = Instance.Offset + SlotIndex;

            var t = (Instance.Device, slotWithOffset, Database);
            if (Slots.TryGetValue(t, out var slot))
                return (true, slot.DatabaseSlot);

            return LoadSlot(Instance.Device, slotWithOffset, Database);
        }

        /// <summary>
        /// Load the Slot
        /// </summary>
        /// <param name="Instance"></param>
        /// <param name="Slot">The Device Slot, without the offset added</param>
        /// <returns></returns>
        private (bool Success, SlotDatabase DatabaseSlot) LoadSlot(uint Device, uint SlotWithOffset, uint Database)
        {
            //If are have not registered to get these rms, register
            if (!InfodriversLinked.Contains(Device))
            {
                BncsClient.revertiveRegister(Device, 1, 1, false);
                InfodriversLinked.Add(Device);
            }

            BncsClient.databaseName(Device, SlotWithOffset, Database, out var s);

            var newSlot = SlotDatabase.Create(Device, SlotWithOffset, Database, s, (str) =>
            {
                BncsClient.databaseModify(Device, SlotWithOffset, Database, str);
            }, BNCSStatusModel.Instance.TXCountIncrement, BNCSStatusModel.Instance.RXCountIncrement);

            Slots.Add((Device, SlotWithOffset, Database), newSlot);

            return (true, newSlot.Slot);
        }

    }
}
