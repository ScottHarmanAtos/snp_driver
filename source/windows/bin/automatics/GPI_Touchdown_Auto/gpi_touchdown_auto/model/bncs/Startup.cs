﻿using driver_template.model.bncs;
using driver_template.helpers;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BncsClientLibrary;
using BncsInfodriverLibrary;
using System.Reflection;
using System.Net.NetworkInformation;
using System.Xml.Serialization;
using System.IO;
using DigitalGiles.INIFile;
using System.Xml.Linq;

namespace driver_template.model.bncs
{
    public sealed class Startup
    {
        private const Int32 CSIConnectTimeoutInms = 2000;
        private const Int32 InfodriverConnectTimeoutInms = 500;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static string GPIInputInstance { get; private set; }
        public static string SDIRouterInstance { get; private set; }

        /// <summary>
        /// Stop the class being created, should only be created using Load()
        /// </summary>
        private Startup() { }

        /// <summary>
        /// Loads config.
        /// </summary>
        /// <returns></returns>
        public static (bool Success, String ErrorMessage) Load(MethodInfo getManagedAndClientInstancesMethod)
        {
            logger.Info("Startup Load");

            //Load the Arguments
            StartupArguments StartupArguments;

            BncsClient BncsClient = null;
            try
            {
                StartupArguments = LoadArgs(Environment.GetCommandLineArgs().Skip(1));
                BNCSConfig.StartupArguments = StartupArguments;

                instances_and_devicetypes.Logging.DefaultFactory = new NLog.Extensions.Logging.NLogLoggerFactory();
                //Load the devicetype and instance config
                DeviceType.LoadDeviceTypes();
                Instance.LoadInstances();

                //Load Config
                var config = LoadConfig(StartupArguments, getManagedAndClientInstancesMethod);

                //Load Driver Settings
                DriverSettings.LoadSettings(config.Main.Id);

                //Load CSI
                BncsClient = CSICheck(StartupArguments);


                /*GpiDevice = touchdowns.Source.GPI_Input_Instance;

                if (!Instance.TryGetInstance(GpiDevice, out var gpiDeviceInstance))
                {
                    return (false, $"Failed to find GPI Instance instance: {GpiDevice.ToString()} in instances");
                }
                else
                {
                    config.ClientInstances.Add(GpiDevice, gpiDeviceInstance);
                    BNCSConfig.GPIInstance = gpiDeviceInstance;

                }


                GpiRouter = touchdowns.Destination.SDI_Router_Instance;

                if (!Instance.TryGetInstance(GpiRouter, out var gpiRouterInstance))
                {
                    return (false, $"Failed to find GPI Instance instance: {GpiRouter.ToString()} in instances");
                }
                else
                {
                    config.ClientInstances.Add(GpiRouter, gpiRouterInstance);
                    BNCSConfig.RouterInstance = gpiRouterInstance;

                }*/

                // Add my instances and connect to it


                //Connect to BNCS Stuff
                InfodriverConnect(config.ManagedInstances);
                

                var databases = new Databases(BncsClient);

                var loadManagedInstance = LoadManagedInstances(config.ManagedInstances.Select(x => x.Value).ToList());
                if (!loadManagedInstance.Success)
                    throw new Exception("Error whilst loading Managed Instances");

                //The GPI Input and SDI Video router instances are stored as settings against my MainInstance.

                if (config.Main.TryGetChildInstance("config", out var configInstance))
                {
                    //rewritten to take unconditional update from external XML file.
                    string path = Path.Combine(Helper.ConfigPath, "gpi_touchdown_config.xml");
                    if (!File.Exists(path))
                    {
                        throw new Exception($"Failed to find gpi_touchdown_config.xml in {path}");
                    }
                    XDocument xdoc = XDocument.Load(path);

                    var settings = xdoc.Descendants("setting");
                    var touchdown_instances = xdoc.Descendants("instance").Where(id => id.Attribute("id").Value == configInstance.Id.ToString());
                    foreach (var s in touchdown_instances.Descendants("setting"))
                    {
                        var id = s.Attribute("id").Value;
                        var value = s.Attribute("value").Value;

                        if (id == "gpi_input_instance")
                        {
                            GPIInputInstance = value;
                            if (Instance.TryGetInstance(GPIInputInstance, out var gpiDeviceInputInstance))
                            {
                                config.ClientInstances.Add(GPIInputInstance, gpiDeviceInputInstance);
                                BNCSConfig.GPIInstance = gpiDeviceInputInstance;
                            }
                        }
                        else if (id == "sdi_router_instance")
                        {
                            SDIRouterInstance = value;
                            if (Instance.TryGetInstance(SDIRouterInstance, out var sdiRouterInstance))
                            {
                                config.ClientInstances.Add(SDIRouterInstance, sdiRouterInstance);
                                BNCSConfig.RouterInstance = sdiRouterInstance;
                            }
                        }
                    }

                }
                
                

               var clientInstances = LoadClientInstances(BncsClient, config.ClientInstances);



                BNCSConfig.Set(config.Main, BncsClient, loadManagedInstance.Managed, clientInstances, databases);

                return (true, null);
            }
            catch (Exception e)
            {
                if (BncsClient != null)
                    BncsClient.Dispose();
                return (false, e.Message);
            }
        }
        
        /// <summary>
        /// If changes to this method are required, it should be overridden within a static class in the model.bncs namespace
        /// </summary>
        public static (Dictionary<string, Instance> ManagedInstances, Dictionary<string, Instance> ClientInstances) GetManagedAndClientInstances(List<(string Id, Instance Instance)> composites, List<(string Group, Instance Instance)> instances)
        {
            //The default is to allocate all instances as managed instances, the driver_template will attempt to connect to an infodriver for each device number provided.
            return (instances.ToDictionary(x => x.Instance.Id, y => y.Instance), null);
        }

        private static StartupArguments LoadArgs(IEnumerable<string> args)
        {
            logger.Info("Loading Arguments");
            //Expecting Args: Instance (-sim) (-dev)

            string instance = args.FirstOrDefault();
            if (instance == null)
                throw new Exception("No Instance supplied as an argument");

            bool simulation = args.Any(x => x.ToLower() == "-sim");

            bool development = args.Any(x => x.ToLower() == "-dev");

            return new StartupArguments(instance, simulation, development);
        }

        private static (List<String> Errors, List<(string Id, Instance Instance)> Composites, List<(string Group, Instance Instance)> Instances) GetAllCompositesRecursively(Instance i, String parentName = null)
        {
            var instances = new List<(string Group, Instance Instance)>();
            var composites = new List<(string Id, Instance Instance)>();
            var errors = new List<string>();

            if (i.Error)
            {
                errors.Add($"Instance Error:{i.Id}");
            }

            foreach (var c in i.GetAllChildInstances())
            {
                String NewParentName = c.Key;
                if (parentName != null)
                    NewParentName = parentName + "." + c.Key;

                if (!c.Value.IsComposite)
                {
                    //It is not a composite
                    if (!instances.Any(x => x.Instance.Id == c.Value.Id))
                        instances.Add((NewParentName, c.Value));
                }
                else
                {
                    //It is a composite
                    if (!composites.Any(x => x.Id == c.Value.Id))
                        composites.Add((NewParentName, c.Value));

                    var getAll = GetAllCompositesRecursively(c.Value, NewParentName);
                    errors.AddRange(getAll.Errors);

                    foreach (var r in getAll.Instances)
                    {
                        if (!instances.Any(x => x.Instance.Id == r.Instance.Id))
                            instances.Add((r.Group, r.Instance));
                    }
                    foreach (var s in getAll.Composites)
                    {
                        if (!composites.Any(x => x.Id == s.Id))
                            composites.Add(s);
                    }
                }
            }
            return (Errors: errors, Composites: composites, Instances: instances);
        }

        private static (Instance Main, Dictionary<string, Instance> ManagedInstances, Dictionary<string, Instance> ClientInstances) LoadConfig(StartupArguments args, MethodInfo getManagedAndClientInstancesMethod)
        {   //foreach (var i in instances_and_devicetypes.Instance.Instances)
            //    logger.Info($"");
            if (!Instance.TryGetInstance(args.Instance, out var main))
                throw new Exception($"Failed to find main instance:{args.Instance}");

            if (main.Error == true)
                throw new Exception($"Error with the main instance: {args.Instance}");

            (List<String> Errors, List<(string Id, Instance Instance)> Composites, List<(string Group, Instance Instance)> Instances) result;
            if (main.IsComposite)
                result = GetAllCompositesRecursively(main);
            else
                result = (new List<String>(), new List<(string Id, Instance Instance)>(), new List<(string Group, Instance Instance)>() { ("_main", main) });

            if (result.Errors.Count > 0)
            {
                throw new Exception($"Error with supplied Instances: {String.Join("\n", result.Errors)}");
            }

            //Check for errors in the instances we are trying to load
            foreach (var r in result.Composites)
            {
                if (r.Instance.Error == true)
                    throw new Exception($"Error with the composite instance: {args.Instance}");
            }

            foreach (var r in result.Instances)
            {
                if (r.Instance.Error == true)
                    throw new Exception($"Error with the instance: {args.Instance}");
            }

            Dictionary<string, Instance> managedInstances;
            Dictionary<string, Instance> clientInstances;
            //At this point here we can figure out if the instances are to be managed or are just client instances.

            //This is how the driver_template tells if the instance should be treated as a router
            ClientInstances.IsRouter = ((inst) =>
            {
                //This checks if the type name begins with router or rtr, if it does than it is assumed to be a router.
                if (inst.DeviceType.Name.ToLower().StartsWith("router") || inst.DeviceType.Name.ToLower().StartsWith("rtr"))
                {
                    return true;
                }
                return false;
            });

            if (getManagedAndClientInstancesMethod == null)
            {
                //This may throw exceptions
                (managedInstances, clientInstances) = GetManagedAndClientInstances(result.Composites, result.Instances);
            }
            else
            {
                // More excessive use of tuples
                var objectResult = getManagedAndClientInstancesMethod.Invoke(null, new object[] { result.Composites, result.Instances });
                (managedInstances, clientInstances) = ((Dictionary<string, Instance>, Dictionary<string, Instance>))objectResult;
            }

            //Check if these are null, create empty dictionaries if they are
            if (managedInstances == null)
                managedInstances = new Dictionary<string, Instance>();
            if (clientInstances == null)
                clientInstances = new Dictionary<string, Instance>();

            //@@Why don't we do this for client instances as well?
            foreach (var i in managedInstances.Values)
            {
                if (i.DeviceType.IsGood == false)
                {
                    logger.Error("DeviceType:{0} failed to load correctly", i.DeviceType.Name);
                }
            }

            return (main, managedInstances, clientInstances);
        }

        /// <summary>
        /// Check if CSI is running
        /// </summary>
        /// <returns></returns>
        private static BncsClient CSICheck(StartupArguments startupArguments)
        {
            var csi = new BncsClient();

            if (csi == null)
            {
                throw new Exception("Failed to create CSI object");
            }

            logger.Info("Connecting to CSI");
            var result = csi.connect();

            if (!csi.isConnected())
            {
                helper.PopupMessageBox.Show("Failed to connect to CSI.\n\n1) Check CSI is running. - Start CSI and the needed infodrivers. \n2) If CSI is running check if anything is blocking the IPC.\n\n -- If 2 -- \nA) Open Resource Monitor before closing this dialog.\nB) Find CSI (It will probably be RED)\nC) Right click and select Analyse Wait Chain\nD) This will show what program is stopping the IPC.\nE) Close the program.\nF) Click OK to this dialog, the program should successfully connect to CSI\n\n Use -dev to get more time to do this", "Failed to connect to CSI", startupArguments.Development ? 60 : 15);

                //Try and connect again, it maybe that something was blocking which has now been removed
                csi.connect();

                //See if we are connected now.
                if (!csi.isConnected())
                {
                    csi.Dispose();
                    throw new Exception("Failed to connect to CSI.");
                }
            }

            //Register us as a client
            csi.writeEnable(true);
            return csi;
        }

        private static void InfodriverConnect(Dictionary<String, Instance> managedInstances)
        {
            logger.Info("Connecting to Infodrivers");

            var failedConnections = new List<(uint device, string instances)>();
            //Get one of each device, as we don't want to try to connect to the same infodriver multiple times
            foreach (var instance in managedInstances.GroupBy(x => x.Value.Device).Select(x => (InstanceDevice: x.First().Value, AllInstanceForThisDevice: String.Join(",", x.Select(y => y.Value.Id)))))
            {
                //logger.Info("Device:{0} - Instances:{1}", instance.InstanceDevice.Device, instance.AllInstanceForThisDevice);
                var r = Infodrivers.Add(instance.InstanceDevice);
                if (!r)
                {
                    failedConnections.Add((instance.InstanceDevice.Device, instance.AllInstanceForThisDevice));
                }
            }

            if (failedConnections.Count > 0)
            {
                throw new Exception($"Failed to connect to infodriver(s):\r\n{string.Join("\r\n", failedConnections.Select(x => x.device + ": " + x.instances))}");
            }
        }

        private static (bool Success, ManagedInstances Managed) LoadManagedInstances(List<Instance> managedInstancesList)
        {
            bool success = true;
            var mi = new ManagedInstances();
            foreach (var m in managedInstancesList)
            {
                if (!mi.Add(m))
                    success = false;
            }
            return (success, mi);
        }

        private static ClientInstances LoadClientInstances(BncsClient client, Dictionary<string, Instance> instances)
        {
            ClientInstances c = new ClientInstances(client);
            foreach (var i in instances)
            {
                c.Add(i.Value);
            }
            return c;
        }
    }
}
