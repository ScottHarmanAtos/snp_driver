﻿using driver_template.helpers;
using NLog;
using System;
using System.ComponentModel;
using System.Threading;

namespace driver_template.model.bncs
{

    public enum SlotType
    {
        Infodriver,
        RouterClient,
        InfodriverClient,
        Database
    }

    public class Slot : IDisposable
    {
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public static (Slot Slot, Action<String> Update) Create(uint device, uint index, string initialValue, Action<string> set, Action updateTxCount, Action updateRxCount)
        {
            var s = new Slot(device, index, initialValue, set, updateTxCount, updateRxCount);
            return (s, (str) => { s.Changed(str); });
        }

        protected Slot(uint device, uint index, string initialValue, Action<string> set, Action updateTxCount, Action updateRxCount)
        {
            Device = device;
            Index = index;
            this.value = initialValue;
            this.set = set;
            this.updateTxCount = updateTxCount;
            this.updateRxCount = updateRxCount;
        }

        public SlotType Type { get; protected set; } = SlotType.Infodriver;
        protected string value = null;
        private readonly Action<string> set;
        private readonly Action updateTxCount;
        private readonly Action updateRxCount;
        public uint Device { get; }
        public uint Index { get; }

        /// <summary>
        /// A Value of "---" may mean the db slot does not exist
        /// </summary>
        public virtual string Value
        {
            get { return value; }
            private set
            {
                this.value = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Value"));
            }
        }

        Timer NagleDelay = null;

        string NextValueToSet = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="ifChanged">If Trye will only send the change if it is different than the current value</param>
        /// <param name="useNagle">Turns on a delay similar the nagle algorithm, where the value will not be set until a time has passed. This removes jitter from chatty devices.</param>
        /// <param name="nagleTimeMs">Time before the value will be sent, if multiple values are receved with this time all but the last is discarded</param>
        /// <param name="nagleSendFirstStraightAway">If true the first time set is called a value will be send and then the nagle timer will start</param>
        /// <param name="redundantTXRXReceive">Ignores the TXRX state (of the driver) and send the value anyway</param>
        public void Set(string value, bool ifChanged = true, bool useNagle = false, int nagleTimeMs = 10, bool nagleSendFirstStraightAway = true, bool redundantTXRXReceive = true)
        {
            if (ifChanged == false || this.value != value)
            {
                if (!useNagle)
                {

                    if (redundantTXRXReceive || Infodrivers.Status == BncsState.TXRX)
                    {
                        SetValue(value);
                        updateTxCount();
                    }
                }
                else
                {
                    //If not running set straight away
                    if (nagleSendFirstStraightAway && NagleDelay == null)
                    {
                        NextValueToSet = null;

                        if (redundantTXRXReceive || BNCSStatusModel.Instance.CommsStatus == BncsState.TXRX)
                        {
                            SetValue(value);
                            updateTxCount();
                        }

                        NagleDelay = new Timer((t) => {
                            NagleDelay = null;
                            if (NextValueToSet != null)
                            {
                                if (ifChanged == false || this.value != NextValueToSet)
                                {
                                    Set(NextValueToSet);
                                }
                            }
                        }, null, nagleTimeMs, Timeout.Infinite);
                    }
                    else if (NagleDelay == null)
                    {
                        NextValueToSet = value;
                        NagleDelay = new Timer((t) =>
                        {
                            NagleDelay.Dispose();
                            NagleDelay = null;
                            if (NextValueToSet != null)
                            {
                                Set(NextValueToSet);
                            }
                        }, null, nagleTimeMs, Timeout.Infinite
                            );
                    }
                    else
                    {
                        NextValueToSet = value;
                    }
                }
            }
        }

        protected void Set(String value)
        {
            this.set(value);
            ValueSent?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// Send revertive onto the network
        /// </summary>
        /// <param name="value"></param>
        protected virtual void SetValue(String value)
        {
            this.Set(value);
            this.Value = value;
        }

        /// <summary>
        /// Infowrite received
        /// </summary>
        /// <param name="value"></param>
        protected virtual void Changed(string value)
        {
            updateRxCount();
            OnChange?.Invoke(this, new EventSlotChange(this, value));
        }

        private bool disposed = false;
        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                if (NagleDelay != null)
                {
                    NagleDelay.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        public event EventHandler<EventSlotChange> OnChange;

        public event EventHandler ValueSent;

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class EventSlotChange : EventArgs
    {
        public Slot Slot { get; }
        public String Value { get; }
        public EventSlotChange(Slot slot, String value)
        {
            Slot = slot;
            Value = value;
        }
    }
}
