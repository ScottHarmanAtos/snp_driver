﻿using System;
using System.ComponentModel;

namespace driver_template.model.bncs
{
    /// <summary>
    /// This is database slot the driver_template can read from and write too
    /// </summary>
    public class SlotDatabase : Slot
    {
        public uint Database { get; }
        public static (SlotDatabase Slot, Action<String> Update) Create(uint device, uint index, uint database, string InitialValue, Action<string> set, Action updateTxCount, Action updateRxCount)
        {
            var s = new SlotDatabase(device, index, database, InitialValue, set, updateTxCount, updateRxCount);
            return (s, (str) => { s.Changed(str); });
        }


        protected SlotDatabase(uint device, uint index, uint database, string initialValue, Action<string> set, Action updateTxCount, Action updateRxCount) : base(device, index, initialValue, set, updateTxCount, updateRxCount)
        {
            Database = database;
            Type = SlotType.Database;
        }

        /// <summary>
        /// Set a Value, this will do a routermodify
        /// </summary>
        /// <param name="value"></param>
        protected override void SetValue(String value)
        {
            this.Set(value);
        }

        /// <summary>
        /// RM arriving from the network
        /// </summary>
        /// <param name="value"></param>
        protected override void Changed(string value)
        {
            this.value = value;
            base.Changed(value);
        }

    }
}
