﻿using driver_template.helpers;
using System;

namespace driver_template.model.bncs
{
    /// <summary>
    /// Settings holds settings that are needed for the driver_template.
    /// The settings are stored in drivers_config.xml
    /// Add any setting that is required by the program that does not make sense to add to instances
    /// 
    /// The class is updated from the DriverSettings Class
    /// </summary>
    public class Settings
    {
        public Setting<DebugLevels> DebugLevel { get; private set; } =
            new Setting<DebugLevels>(DebugLevels.Normal, string.Join(",", Enum.GetNames(typeof(DebugLevels))), false, Enum.TryParse, true);
        public Setting<DebugLevels> LogLevel { get; private set; } =
            new Setting<DebugLevels>(DebugLevels.Normal, string.Join(",", Enum.GetNames(typeof(DebugLevels))), false, Enum.TryParse, true);

        public Setting<bool> UseMasterSlaveTXRX { get; private set; } =
            new Setting<bool>(true, "Newer version of CSI allow a single infodriver to control the TXRX state of all infodrivers controlled.", false);

        /// <summary>
        /// Additional checks are called after all settings are loaded.
        /// </summary>
        /// <returns></returns>
        public void RunAdditionalChecks()
        {
            Logging.SetLevelDebug(DebugLevel);
            Logging.SetLevelLog(LogLevel);
        }
    }

}
