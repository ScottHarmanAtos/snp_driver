﻿using driver_template.helpers;
using driver_template.model.interfaces;
using System;

namespace driver_template.model
{
    public class DeviceModel : IDeviceStatus
    {
        public DeviceModel()
        {
        }

        private DeviceState commsStatus;
        public DeviceState CommsStatus
        {
            get { return commsStatus; }
            set
            {
                if (commsStatus != value)
                {
                    commsStatus = value;
                    CommsStatusChange?.Invoke(this, new EventArgs());
                }
            }
        }

        public event EventHandler CommsStatusChange;

        private void IncrementTX()
        {
            TXIncrement?.Invoke(this, new EventArgs());
        }

        private void IncrementRX()
        {
            RXIncrement?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// Call this when a message is sent to the device
        /// </summary>
        public event EventHandler TXIncrement;

        /// <summary>
        /// Call this when a message is received from the device
        /// </summary>
        public event EventHandler RXIncrement;
    }
}
