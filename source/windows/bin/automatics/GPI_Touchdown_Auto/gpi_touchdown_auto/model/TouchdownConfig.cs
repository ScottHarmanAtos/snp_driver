﻿using driver_template.model.bncs;
using instances_and_devicetypes;
using NLog;
using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace driver_template.model
{
    public class TouchdownConfig : IDisposable, INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        public Instance ConfigInstance { get; private set; }
        public uint touchdownDestination { get; private set; }
        public uint wfmDestination { get; private set; }
        public uint rcpFlyback { get; private set; }
        public uint rcpDefaultFlyback { get; private set; }
        public uint roboticDestination { get; private set; }
        public bool wfmMonitor { get; private set; }
        public bool touchdownMonitor { get; private set; }

        public TouchdownConfig(Instance value)
        {
            this.ConfigInstance = value;
            MapConfigSlots();
        }

        private void MapConfigSlots()
        {
            //Map all of my named instance slots - we will mirror and capture the values from the database then reflect them on change.  
            //Might not be needed, but we then get a nice reference back to the named parameters
            if (ConfigInstance != null)
            {
                logger.Info($"Found Config instance {ConfigInstance.Id}");
                string path = Path.Combine(Helper.ConfigPath, "gpi_touchdown_config.xml");
                if (!File.Exists(path))
                {
                    throw new Exception($"Failed to find gpi_touchdown_config.xml in {path}");
                }
                XDocument xdoc = XDocument.Load(path);

                var settings = xdoc.Descendants("setting");
                var touchdown_instances = xdoc.Descendants("instance").Where(id => id.Attribute("id").Value == ConfigInstance.Id.ToString());
                foreach (var param in ConfigInstance.DeviceType.Parameters)
                {
                    // Get the slot by Instance and Parameter
                    var (success, slot) = BNCSConfig.ManagedInstances.GetSlot(ConfigInstance, param);

                    if (success)
                        logger.Trace($"Found Parameter {param.Name} in  {slot.Index}.");
                    else
                        continue;
                    //string databaseSlotValue = ConfigureSlotDatabase(param);
                    string databaseSlotValue = slot.Value;

                    //rewritten to take unconditional update from external XML file.

                    foreach (var s in touchdown_instances.Descendants("setting"))
                    {
                        var id = s.Attribute("id").Value;
                        var value = s.Attribute("value").Value;
                        string default_id = id.Replace("default_", "");
                        if (id == param.Name || default_id == param.Name)
                        {

                            //logger.Warn($"Empty slot - checking XML for values");
                            if (databaseSlotValue != value)
                                slot.Set(value);

                            //UpdateDatabaseSlot(param, value);
                            UpdateParamValue(param, value);

                            slot.OnChange += (sender, e) => { Slot_OnChange(sender, e); };
                        }


                    }
                }
            }

        }

        private string UpdateDatabaseSlot(Parameter param, string value)
        {
            //We don't need or want to map slot 1.  It's status, and should be ephemeral
                //Get the Database by index- using database 4, as zero and one are normally used for sources and destinations
            var (SuccessDb, dbSlot) = BNCSConfig.Database.GetDatabase(ConfigInstance, param, 4);
            if (SuccessDb)
            {
                    //Send the RM if the value doesn't match what we've already got.
                    if(dbSlot.Value != value)
                        dbSlot.Set($"{value}");
                dbSlot.OnChange += DatabaseSlot_OnChange;
                return dbSlot.Value;
            }
            return "";
        }




        private void DatabaseSlot_OnChange(object sender, EventSlotChange e)
        {
            //logger.Trace($"{MainInstance.Id} DatabaseSlot_OnChange {e.Value}");
            //update my slot from database update

            var (success, slot) = BNCSConfig.ManagedInstances.GetSlots(ConfigInstance);

            if (success)
            {


                foreach (var s in slot.Where(x => x.Slot.Index == e.Slot.Index))
                {
                    //logger.Trace($"Found slot {s.Param.Name}.");
                    s.Slot.Set(e.Value);
                    //logger.Trace($"{s.Param.Slot} {s.Param.Name} - set to {e.Slot.Value}");
                    UpdateParamValue(s.Param, e.Slot.Value);

                }
            }
            }

        private void Slot_OnChange(object sender, EventSlotChange e)
        {
            var (success, slot) = BNCSConfig.ManagedInstances.GetSlots(ConfigInstance);

            if (success)
            {


                foreach (var s in slot.Where(x => x.Slot.Index == e.Slot.Index))
                {
                    //logger.Trace($"Found slot {s.Param.Name}.");
                    s.Slot.Set(e.Value);
                    //logger.Trace($"{s.Param.Slot} {s.Param.Name} - set to {e.Slot.Value}");
                    //UpdateDatabaseSlot(s.Param, e.Slot.Value);
                    UpdateParamValue(s.Param, e.Slot.Value);

                }
            }
        }



        private void UpdateParamValue(Parameter param, string value)
        {
            switch (param.Name)
            {
                case "touchdown_destination":
                    if (uint.TryParse(value, out uint touchdown_destination)) { touchdownDestination = touchdown_destination; }
                    else logger.Error($"monitor_destination parameter must be a UInt");
                    break;
                case "wfm_destination":
                    if (uint.TryParse(value, out uint wfm_value)) { wfmDestination = wfm_value; }
                    else logger.Error($"wfm_destination parameter must be a UInt");
                    break;
                case "rcp_flyback":
                    if (uint.TryParse(value, out uint rcp_flyback_value)) { rcpFlyback = rcp_flyback_value; }
                    else logger.Error($"rcp_flyback parameter must be a UInt");
                    break;
                case "robotic_destination":
                    if (uint.TryParse(value, out uint robotic_destination)) { roboticDestination = robotic_destination; }
                    else logger.Error($"robotic_monitor parameter must be a UInt");
                    break;
                case "default_rcp_flyback":
                    if (uint.TryParse(value, out uint default_rcp_flyback_value)) { rcpDefaultFlyback = default_rcp_flyback_value; }
                    else logger.Error($"default_rcp_flyback parameter must be a UInt");
                    break;
                case "wfm_monitor":
                    wfmMonitor = value.Equals("1") ? true : false;
                    break;
                case "touchdown_monitor":
                    touchdownMonitor = value.Equals("1") ? true : false;
                    break;
                default:
                    break;

            }
        }

        
        public int CompareFlybacks()
        {
            if (!(rcpFlyback > 0))
                return (int)rcpDefaultFlyback;
            return (int)rcpFlyback; //if rcp flyback is greater than 0, then use that value


        }

        public void Dispose()
        {


        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

}