using BNCS;
using DigitalGiles.INIFile;
using driver_template.helpers;
using driver_template.model.bncs;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xaml;
using System.Xml.Linq;

namespace driver_template.model
{
    public class TouchdownAuto : IDisposable, INotifyPropertyChanged
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public Instance TouchdownInstance { get; private set; }
        public Instance GPIInstance { get; private set; }
        public Instance RouterInstance { get; private set; }
        public uint gpiSource { get; private set; }

        public Dictionary<uint, SlotClient> GPIInputSlots { get; private set; }

        public Dictionary<uint, uint> SourceMapping { get; private set; }

        public Dictionary<uint, uint> DefaultSourceMapping { get; private set; }

        //Not currently in spec.
        public Dictionary<uint, bool> GPILatching { get; private set; }
        public TouchdownType TouchdownType { get; private set; }

        private SlotRouter routerSlot;
        public uint GPIInput { get; private set; }
        public uint DefaultSDISource { get; private set; }
        public uint SDISource { get; private set; }
        public bool GPILatch { get; private set; }
        public SlotClient gpiInputSlot { get; private set; }

        public Dictionary<uint, bool> GPIQueue { get; set; }
        public TouchdownConfig TouchdownConfig { get; private set; }
        public Dictionary<uint, TouchdownType> TouchdownTypeMapping { get; private set; }

        public TouchdownAuto(Instance instance, TouchdownConfig touchdownConfig, Dictionary<uint, bool> gpiQueue, 
                Dictionary<uint, SlotClient> gpiInputSlots,Dictionary<uint, uint> sourceMapping, Dictionary<uint, uint> defaultSourceMapping, Dictionary<uint, bool> gpiLatchingMapping, Dictionary<uint,TouchdownType> touchdownType)
        {
            if (instance != null)
            {
                this.TouchdownInstance = instance;
                this.TouchdownConfig = touchdownConfig;
                this.GPIInstance = BNCSConfig.GPIInstance;
                this.RouterInstance = BNCSConfig.RouterInstance;
                this.GPIQueue = gpiQueue;
                this.GPIInputSlots = gpiInputSlots;
                this.SourceMapping = sourceMapping;
                this.DefaultSourceMapping = defaultSourceMapping;
                this.GPILatching = gpiLatchingMapping;
                this.TouchdownTypeMapping = touchdownType;
                //TouchdownConfigs = new Dictionary<string, TouchdownConfig>();
                //GPIInputSlots 

            }
                

            //Using the imported XML config, map the defined destinations to appropriate slots

            // Likewise - for GPIs, map the GPI inputs to the apprpriate slots
            MapInstanceSlots();
            ConfigureDestinations();

            if (GPIInput > 0)
            {
                DefaultSourceMapping.Add(GPIInput, DefaultSDISource);
                SourceMapping.Add(GPIInput, SDISource);
                GPILatching.Add(GPIInput, GPILatch);
            }
            //TouchdownTypeMapping.Add(GPIInput, TouchdownType);


        }



        private void MapInstanceSlots()
        {
            //Map all of my named instance slots - we will mirror and capture the values from the database then reflect them on change.  
            //Might not be needed, but we then get a nice reference back to the named parameters
            if (TouchdownInstance != null)
            {
                logger.Info($"Found Touchdown instance {TouchdownInstance.Id}");
                //rewritten to take unconditional update from external XML file.
                string path = Path.Combine(Helper.ConfigPath, "gpi_touchdown_config.xml");
                if (!File.Exists(path))
                {
                    throw new Exception($"Failed to find gpi_touchdown_config.xml in {path}");
                }
                XDocument xdoc = XDocument.Load(path);

                var settings = xdoc.Descendants("setting");
                var touchdown_instances = xdoc.Descendants("instance").Where(id => id.Attribute("id").Value == TouchdownInstance.Id.ToString());
                foreach (var param in TouchdownInstance.DeviceType.Parameters)
                {
                    // Get the slot by Instance and Parameter
                    var (success, slot) = BNCSConfig.ManagedInstances.GetSlot(TouchdownInstance, param);

                    if (success)
                        logger.Trace($"Found Parameter {param.Name} in  {slot.Index}.");
                    else
                        continue;
                    string databaseSlotValue = slot.Value;
                    //string databaseSlotValue = slot.Value;

                    foreach (var s in touchdown_instances.Descendants("setting"))
                    {
                        var id = s.Attribute("id").Value;
                        var value = s.Attribute("value").Value;
                        string default_id = id.Replace("default_", "");
                        if (id == param.Name || default_id == param.Name)
                        {
                            try
                            {
                                if (id == "gpi_input")
                                {
                                    if(uint.TryParse(value, out uint gpi) && !GPIInputSlots.ContainsKey(gpi))
                                        MapGPIInput(gpi);
                                    else logger.Fatal($"No valid gpi_input found in {s.Name} - and either remove the instance, or correct the error");

                                }
                            }
                            catch (Exception e)
                            { 
                                logger.Fatal($"No valid gpi_input found in {s.Name} - and either remove the instance, or correct the error");
                                throw new Exception($"No valid gpi_input found in {s.Name} - please correct");
                            }

                            //logger.Warn($"Empty slot - checking XML for values");
                            if (databaseSlotValue != value)
                                slot.Set(value);

                            //UpdateDatabaseSlot(param, value);
                            UpdateParamValue(param, value);

                            slot.OnChange += (sender, e) => { Slot_OnChange(sender, e); };
                        }
                    }

                    
                }
            }

        }

        private void ConfigureDestinations()
        {

                logger.Info($"Found instance {TouchdownInstance.Id}");

                foreach (var param in TouchdownInstance.DeviceType.Parameters)
                {

                    // Get the slot by Instance and Parameter
                    var (success, slot) = BNCSConfig.ManagedInstances.GetSlot(TouchdownInstance, param);
                    if (success)
                    {
                        UpdateParamValue(param, slot.Value);
                    }

                }
            //}
        }


        private string UpdateDatabaseSlot(Parameter param, string value)
        {
            //We don't need or want to map slot 1.  It's status, and should be ephemeral
                //Get the Database by index- using database 4, as zero and one are normally used for sources and destinations
            var (SuccessDb, dbSlot) = BNCSConfig.Database.GetDatabase(TouchdownInstance, param, 4);
            if (SuccessDb)
            {
                //Send the RM if the value doesn't match what we've already got.
                if (value != dbSlot.Value)
                {
                    dbSlot.Set($"{value}");
     
                }
                dbSlot.OnChange += DatabaseSlot_OnChange;
                return dbSlot.Value;
            }
            return "";
        }

        private void MapGPIInput (uint input)
        {
            var (Success, inputGPI) = BNCSConfig.ClientInstances.GetSlots(GPIInstance, input, 1);
            if (Success)
            {
                gpiInputSlot = inputGPI.First().Slot;
                gpiInputSlot.OnChange += Source_OnChange;
                GPIInputSlots.Add(input, gpiInputSlot);
            }

        }

        private int CompareSources(uint mappedvalue)
        {
            if (!(SourceMapping[mappedvalue] > 0))
                return (int)DefaultSourceMapping[mappedvalue];
            uint mappedSource = DefaultSourceMapping[mappedvalue] == SourceMapping[mappedvalue] ? DefaultSourceMapping[mappedvalue] : SourceMapping[mappedvalue];
            return (int)mappedSource;

        }
        public void UpdateTouchdownDestination(uint gpiValue)
        {

            if (TouchdownConfig.touchdownMonitor)
            {
                //Get the Database by index
                var (SuccessDb, Slots) = BNCSConfig.ClientInstances.GetSlots(RouterInstance, TouchdownConfig.touchdownDestination, 1);
                if (SuccessDb)
                {
                    //Send the RM
                    routerSlot = Slots[0].Slot as SlotRouter;

                    //dbSlot.Set($"{param}:{mappedInstanceSlot}");
                    if (routerSlot != null)
                    {
                        if (gpiValue > 0)
                            routerSlot.Set(CompareSources(gpiValue));
                        else
                            routerSlot.Set(TouchdownConfig.CompareFlybacks());
                    }

                    //Set a callback to get notifed when it changes.
                    routerSlot.OnChange += DestinationSlot_OnChange;

                }
            }
        }

        public void UpdateRoboticDestination(uint gpiValue)
        {

            //Get the Database by index
            var (SuccessDb, Slots) = BNCSConfig.ClientInstances.GetSlots(RouterInstance, TouchdownConfig.roboticDestination, 1);
            if (SuccessDb)
            {
                //Send the RM
                routerSlot = Slots[0].Slot as SlotRouter;

                //dbSlot.Set($"{param}:{mappedInstanceSlot}");
                if (routerSlot != null)
                {
                    if (gpiValue > 0)
                        routerSlot.Set(CompareSources(gpiValue));
                    else
                        routerSlot.Set(TouchdownConfig.CompareFlybacks());
                }

                //Set a callback to get notifed when it changes.
                routerSlot.OnChange += DestinationSlot_OnChange;


            }
        }

        public void UpdateTouchdownWFM(uint gpiValue)
        {
            if (TouchdownConfig.wfmMonitor)
            {
                //Get the Database by index
                var (SuccessDb, Slots) = BNCSConfig.ClientInstances.GetSlots(RouterInstance, TouchdownConfig.wfmDestination, 1);
                if (SuccessDb)
                {
                    //Send the RM
                    routerSlot = Slots[0].Slot as SlotRouter;

                    //dbSlot.Set($"{param}:{mappedInstanceSlot}");
                    if (routerSlot != null)
                    {
                        if (gpiValue > 0)
                            routerSlot.Set(CompareSources(gpiValue));
                        else
                            routerSlot.Set(TouchdownConfig.CompareFlybacks());
                    }

                    //Set a callback to get notifed when it changes.
                    routerSlot.OnChange += DestinationSlot_OnChange;

                }
            }
            
        }

        


        //Database callback come here        
        private void DatabaseSlot_OnChange(object sender, EventSlotChange e)
        {
            //logger.Trace($"{MainInstance.Id} DatabaseSlot_OnChange {e.Value}");
            //update my slot from database update
            var (success, slot) = BNCSConfig.ManagedInstances.GetSlots(TouchdownInstance);

            if (success)
            {


                foreach(var s in slot.Where(x => x.Slot.Index == e.Slot.Index))
                {
                    //logger.Trace($"Found slot {s.Param.Name}.");
                    s.Slot.Set(e.Value);
                    //logger.Trace($"{s.Param.Slot} {s.Param.Name} - set to {e.Slot.Value}");
                   UpdateParamValue(s.Param, e.Slot.Value);

                }
            }
        }

        private void DestinationSlot_OnChange(object sender, EventSlotChange e)
        {
            //logger.Trace($"{TouchdownInstance.Id} DestinationSlot_OnChange {e.Value}");
        }

        private void Destination_OnChange(object sender, EventSlotChange e)
        {
            if (e.Value != null && e.Value != "")
            {
                logger.Trace($"{TouchdownInstance.Id} Destination_OnChange {e.Value}");
            }
        }

        private void Source_OnChange(object sender, EventSlotChange e)
        {
            if (e.Value == "1" || e.Value == "3")
            {

                //foreach (var entry in GPIInputSlots.Where(x => x.Value == e.Slot))
               // {
                    logger.Trace($"GpiSlot matches: {GPIInput}");
                    //check and see if our 'queue' includes the current key, if it does - remove it, then readd to the latest queue position (as we can only be in a queue once.  We're british, not bloody colonials.
                    if (GPIQueue.ContainsKey(GPIInput))
                    {
                        GPIQueue.Remove(GPIInput);
                    }
                    if (GPILatch && BNCSConfig.LatchOn == GPIInput)
                        BNCSConfig.LatchOn = 0;
                    else if (GPILatch && BNCSConfig.LatchOn != GPIInput)
                        BNCSConfig.LatchOn = GPIInput;
                    else
                    {
                        BNCSConfig.LatchOn = 0;
                        GPIQueue.Add(GPIInput, true);
                    }
                    if (TouchdownType == TouchdownType.Touchdown)
                    {
                        UpdateTouchdownWFM(GPIInput);
                        UpdateTouchdownDestination(GPIInput);
                    }
                    else if (TouchdownType == TouchdownType.Robotic)
                        UpdateRoboticDestination(GPIInput);

                }
           // }
            //off is 0, 2 is what is also used to indicate off in the original automatic - not sure if it's needed - will confirm on working system if it's necessary
            else if (e.Value == "0" || e.Value == "2")
            {
               // foreach (var entry in GPIInputSlots.Where(x => x.Value == e.Slot))
              //  {
                    logger.Trace($"GpiSlot matches: {GPIInput}");
                    //if our GPI is low, remove us from the queue - and and if anyone remains, promote them to the current source.  If no entry in queue, then flyback.
                    if (GPIQueue.ContainsKey(GPIInput))
                    {
                        GPIQueue.Remove(GPIInput);
                    }
                   if (BNCSConfig.LatchOn == 0 && routerSlot != null)
                    {
                        if (GPIQueue.Count > 0)
                        {
                            //Kludgey priority queue
                            uint slotIndex = GPIQueue.Keys.Last();
                            //touchdown currently pressed - send it.
                            
                            if (TouchdownType == TouchdownType.Touchdown)
                            {
                            UpdateTouchdownWFM(slotIndex);
                            UpdateTouchdownDestination(slotIndex);
                            }
                            else if (TouchdownType == TouchdownType.Robotic)
                                UpdateRoboticDestination(slotIndex);
                        }
                        else
                        {
                            //flyback if the queue is empty - one is the magic number to route the flyback
                            if (TouchdownType == TouchdownType.Touchdown)
                            {
                                UpdateTouchdownWFM(0);
                                UpdateTouchdownDestination(0);
                            }
                            else if (TouchdownType == TouchdownType.Robotic)
                                UpdateRoboticDestination(GPIInput);
                        }
                   }
                }
            }

       // }

        private void Slot_OnChange(object sender, EventSlotChange e)
        {
            var (success, slot) = BNCSConfig.ManagedInstances.GetSlots(TouchdownInstance);

            if (success)
            {


                foreach (var s in slot.Where(x => x.Slot.Index == e.Slot.Index))
                {
                    //logger.Trace($"Found slot {s.Param.Name}.");
                    s.Slot.Set(e.Value);
                    //logger.Trace($"{s.Param.Slot} {s.Param.Name} - set to {e.Slot.Value}");
                    //UpdateDatabaseSlot(s.Param, e.Slot.Value);
                    UpdateParamValue(s.Param, e.Slot.Value);

                }
            }
        }

        private void UpdateParamValue(Parameter param, string value)
        {
            switch (param.Name)
            {
                case "gpi_input":
                    if (uint.TryParse(value, out uint gpi_input))
                    { 
                        GPIInput = gpi_input;
                    }
                    else logger.Error($"gpi_input parameter must be a UInt");
                    break;
                case "touchdown_type":
                    if (TouchdownType.TryParse(value, out TouchdownType touchdown))
                    {
                        TouchdownType = touchdown;
                    }
                    else logger.Error($"touchdown_type parameter must be 1 for Touchdown or 2 for Robotic");
                    break;
                case "default_sdi_source":
                    if (uint.TryParse(value, out uint default_sdi_source)) 
                    { 
                        DefaultSDISource = default_sdi_source;
                        if(DefaultSourceMapping.ContainsKey(GPIInput))
                            DefaultSourceMapping[GPIInput] = default_sdi_source;
                    }
                    else logger.Error($"default_sdi_source parameter must be a UInt");
                    break;
                case "sdi_source":
                    if (uint.TryParse(value, out uint sdi_source)) 
                    { 
                        SDISource = sdi_source;
                        if(SourceMapping.ContainsKey(GPIInput))
                            SourceMapping[GPIInput] = sdi_source;
                    }
                    else logger.Error($"_sdi_source parameter must be a UInt");
                    break;
                case "latching_input":
                    GPILatch = value.Equals("1") ? true : false;
                    if (GPILatching.ContainsKey(GPIInput))
                        GPILatching[GPIInput] = GPILatch;
                    break;
                default:
                    break;

            }
        }

        public void Dispose()
        {


        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}