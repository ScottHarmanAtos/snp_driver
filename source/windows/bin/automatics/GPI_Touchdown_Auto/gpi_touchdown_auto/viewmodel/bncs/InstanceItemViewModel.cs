﻿using driver_template.model.bncs;
using instances_and_devicetypes;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace driver_template.viewmodel
{
    public class InstanceItemViewModel : INotifyPropertyChanged
    {
        private string value;
        private string tempValue;

        public InstanceItemViewModel(Instance instance, Parameter parameter, uint index, Slot slot, bool isManaged)
        {
            Instance = instance;
            Parameter = parameter;
            Index = index;
            SlotWithOffset = index + instance.Offset;
            value = slot.Value;
            TempValue = slot.Value;

            // Adding OnChange event handler to managed instances results in slot values persisting incorrect data if write failed
            // OnChange event handler is required for client instances as PropertyChanged isn't raised
            if (isManaged)
                slot.PropertyChanged += Slot_PropertyChanged;
            else
                slot.OnChange += Slot_OnChange;

            if (Parameter is ParameterEnum)
                States = ((ParameterEnum)Parameter).States.ToDictionary(s => s.Value, s => s.Caption);

            if (Parameter is ParameterRange)
            {
                Min = ((ParameterRange)Parameter).Min;
                Max = ((ParameterRange)Parameter).Max;
                Step = ((ParameterRange)Parameter).Step;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Instance Instance { get; }
        public Parameter Parameter { get; }
        public uint Device { get; }
        public uint Index { get; }
        public uint SlotWithOffset { get; }
        public Dictionary<string, string> States { get; }
        public double Min { get; }
        public double Max { get; }
        public double Step { get; }

        public string TempValue
        {
            get { return tempValue; }
            set
            {
                if (tempValue != value)
                {
                    tempValue = value;
                    OnPropertyChanged("TempValue");
                }
            }
        }

        public string Value
        {
            get { return value; }
            set
            {
                if (this.value != value && Parameter.Access != DeviceType.ParameterAccess.@readonly)
                    BNCSConfig.BncsClient.infoWrite(Instance.Device, SlotWithOffset, value);
            }
        }

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void Slot_OnChange(object sender, EventSlotChange e)
        {
            value = e.Value;
            TempValue = e.Value;
            OnPropertyChanged("Value");
        }

        private void Slot_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (sender is Slot s && e.PropertyName == "Value")
            {
                value = s.Value;
                TempValue = s.Value;
                OnPropertyChanged("Value");
            }
        }
    }
}
