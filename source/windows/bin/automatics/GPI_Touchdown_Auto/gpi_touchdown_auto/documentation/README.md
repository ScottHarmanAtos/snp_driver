﻿# GPI Touchdown Auto #

## 1. Overview ##

This Automatic is a port of the original GPI Touchdown Automatic to be able to handle increased numbers of monitor destinations and flyback sources  

This BNCS automatic, GPI_Touchdown_Auto.exe, will route specific SDI routes to a given monitor in each
of the LVCR rooms, triggered by a user pressing joystick hardware touchdown.
Routes will be calculated based on what is set in the panel and in the database/instance configuration, and if no override is set on the panel - then the default route in the config will be used.

The Robotic control panel is defined as a momentary input, so it will be treated as latching - when a single press will turn it on, then will turn it off again.
Equally, if another input is selected, this will have the effect of releasing the latch, and selecting the new input.

The regular touchdowns are queued, so if held down - they will remain queued and will then be routed in order.

i.e.
  Touchdown 1 is selected.  Touchdown 1's defined source is routed to the defined monitor.
  Touchdown 2 is selected.  Touchdown 2's defined source is routed to the defined monitor.
  Touchdown 2 is released.  Touchdown 1's defined source is routed to the defined monitor.
  Touchdown 3 is selected.  Touchdown 3's defined source is routed to the defined monitor.
  Touchdown 1 is released.  Touchdown 3's defined source remains routed to the defined monitor.
  Touchdown 3 is released.  The defined flyback source is routed to the defined monitor.

  ***When the Robotic panel is added to this mix:***
Robotic 1 is triggered.  Robotic 1's defined source is routed to the defined monitor.
Robotic 2 is triggered.  Robotic 2's defined source is routed to the defined monitor.
Robitic 2 is triggered.  The defined flyback source is routed to the defined monitor.

This can be seen in the GIF that is included in the markdown version of the document, or in the documentation folder.

![Touchdown Panel and GUI](images/touchdownpanel.gif)

## 2. Configuration ##

When gpi_touchdown_auto.exe starts up, appropriate configuration data will be read in. If any required
configuration is missing the automatic will report any errors and stop.
On startup the executable is passed a BNCS device number, eg  for LVCR 1 this number is dev_116.

The Configuration data is stored in the following files :

**dev_xxx.ini**  
Where xxx is the device number passed on startup – and has details of the Database which is accessed by the panel.  We will be using dev_xxx.db4 to hold our non-transient values._
  [Database]
  DatabaseFile_0=DEV_030.INI
  DatabaseFile_1=DEV_030.INI
  DatabaseFile_2=DEV_030.INI
  DatabaseFile_3=DEV_030.INI
  DatabaseFile_4=DEV_030.DB4
  DatabaseFile_5=DEV_030.INI
  ...

**devicetype: gpi_touchdown_auto.xml**
The devicetype defines the Infodriver and Database slots used for the Automatic configuration.  There is a second devicetype that handles the touchdowns themselves, and the associated sources.

        <?xml version="1.0" encoding="utf-8"?>
        <?xml-stylesheet type="text/xsl" href="device_description.xslt"?>
        <device_description name="gpi_touchdown_auto"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <param name="status" class="enum" style="button" access="readwrite" slot="1">
            <state value="Available" caption="Available" />
            <state value="NotAvailable" caption="NotAvailable" />
          </param>
          <param name="monitor_destination" class="smart_string" style="label" access="readwrite" slot="2">
            <text value="" />
          </param>
          <param name="wfm_destination" class="smart_string" style="label" access="readwrite" slot="3">
            <text value="" />
          </param>
          <param name="rcp_type" class="smart_string" style="label" access="readwrite" slot="4">
            <text value="" />
          </param>
          <param name="default_rcp_flybck" class="smart_string" style="label" access="readwrite" slot="5">
            <text value="" />
          </param>
          <param name="rcp_flyback" class="smart_string" style="label" access="readwrite" slot="6">
            <text value="" />
          </param>
          <param name="robotic_monitor" class="smart_string" style="label" access="readwrite" slot="7">
            <text value="" />
          </param>
          <param name="wfm_monitor" slot="8" access="readwrite" style="label" class="enum" description="WFM Active">
            <state value="0" caption="Not Active" style="label" msg="" />
            <state value="1" caption="Active" style="label" msg="" />
          </param>
          <param name="rac_monitor" slot="9" access="readwrite" style="label" class="enum" description="RACs Active">
            <state value="0" caption="Not Active" style="label" msg="" />
            <state value="1" caption="Active" style="label" msg="" />
          </param>
        </device_description>

**devicetype: gpi_touchdown.xml**
This devicetype defines the Infodriver and Database slots used for the touchdowns and the associated sources

    <?xml version="1.0" encoding="utf-8"?>
    <device_description xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="gpi_touchdown">
        <param name="gpi_input" class="smart_string" style="label" access="readwrite" slot="1">
            <text value=""/>
            </param>
            <param name="default_sdi_source" class="smart_string" style="label" access="readwrite" slot="2">
                <text value="" />
            </param>
            <param name="sdi_source" class="smart_string" style="label" access="readwrite" slot="3">
                <text value="" />
            </param>
            <param name="latching_input" access="readwrite" style="label" class="enum" description="Momentary GPI - Latching?" slot="4">
            <state value="0" caption="false" style="label" msg="" />
            <state value="1" caption="true" style="label" msg="" />
            </param>
        </device_description>

**instances.xml**  
We are defining a composite instance to hold the Main instance used for controlling our automatic, plus it's default settings, and we will define a group for each of the touchdowns needed for the automatic.
We have defined that we will have 6 configured Touchdowns initially, and this can be adapted to the size of the control room as needed.  This has been tested with up to 6 regular touchdowns, and 6 latching robotics.

**gpi_touchdown_config.xml**

All settings have been migrated from `instances.xml` into `gpi_touchdown_config.xml`

    <?xml version="1.0" encoding="utf-8"?>
    <gpi_touchdown_config>
      <instance id="sto/touchdown/config">
        <setting id="gpi_input_instance" value="23237/GPIO/001/inputs" />
        <setting id="sdi_router_instance" value="51237/SDIRTR/001" />
        <setting id="touchdown_destination" value="28" />
        <setting id="wfm_destination" value="27" />
        <setting id="robotic_destination" value="33" />
        <setting id="default_rcp_flyback" value="11" />
        <setting id="wfm_monitor" value="0" />
        <setting id="touchdown_monitor" value="1" />
      </instance>

      <instance id="sto/touchdown/input/1">
        <setting id="touchdown_type" value="1" />
        <setting id="gpi_input" value="1" />
        <setting id="default_sdi_source" value="129" />
        <setting id="latching_input" value="0" />
      </instance>
      <instance id="sto/touchdown/input/2">
        <setting id="touchdown_type" value="1" />
        <setting id="gpi_input" value="2" />
        <setting id="default_sdi_source" value="130" />
        <setting id="latching_input" value="0" />
      </instance>
      <instance id="sto/touchdown/input/3">
        <setting id="touchdown_type" value="1" />
        <setting id="gpi_input" value="3" />
        <setting id="default_sdi_source" value="131" />
        <setting id="latching_input" value="0" />
      </instance>
      <instance id="sto/robotic/input/1">
        <setting id="touchdown_type" value="2" />
        <setting id="gpi_input" value="7" />
        <setting id="default_sdi_source" value="129" />
        <setting id="latching_input" value="1" />
      </instance>  
      <instance id="sto/robotic/input/2">
        <setting id="touchdown_type" value="2" />
        <setting id="gpi_input" value="8" />
        <setting id="default_sdi_source" value="130" />
        <setting id="latching_input" value="1" />
      </instance>  


**launch.xml**
The only arguments are the instance id, and whether you are running in dev mode or not.

e.g. `<application name="automatics/gpi_touchdown_auto" arguments="sto/touchdown -dev" />`

## Running ##

The settings are initially read from the gpi_touchdown_config.xml, and if no panel defaults are provided, the initial values will be populated from settings provided in gpi_touchdown_config.xml.  
e.g.

        [Database_4]
        0001=
        0002=28
        0003=27
        0004=33
        0005=11
        0006=11
        0007=
        0008=0
        0009=1
        0010=
        0011=1
        0012=1
        0013=129
        0014=129
        0015=0
        0016=
        0017=
        0018=
        0019=
        0020=
        0021=1
        0022=2
        0023=130
        0024=130
        0025=0

This means a bare configuration to allow you to boot in a new instance can be started up with enough functionality to launch a panel and complete your configuration.

You are able to run this, and generate a partial configuration without using a panel for the purposes of expediency, but once operational it's expected that all changes will be made through a panel so you can indicate whether the Robotics interfaces are in use, or if you are using an RCP.

The automatic will register against the nominated infodriver slots for the GPI, and will route the source associated with each corresponding GPI input to the two monitoring destinations.
If more than one touchdown is activated, the most recently pressed will be routed, and when released, the automatic will route the most recently selected corresponding source.  

If there are no GPI sources high, then the flyback will be routed.
If a GPI source remains high, then this will continue to be routed to the monitoring destination(s)

Presently the Robotics interface ha been developed, but not yet tested as no operational model has been supplied.
Likewise, *if neither the WFM_Monitor nor the RAC_Monitor are enabled, then nothing will be routed when the GPIs go high*.  

If the flyback is changed in the GUI, or the panel - the no routes will be changed until the touchdown is triggered or released again

![GPI Touchdown Automatic Main](images/gpi_touchdown_automatic_main.png)

**Panel GUI and Operation**
The panel has been configured to allow a special use case where if you set "test_mode" to "1" in the settings block in instances.xml you are able to test the GPI functionality

i.e.

    <instance id="sto/touchdown/config" alt_id="STO GPI Touchdown Config" ref="device=30,offset=0" type="gpi_touchdown_auto" location="STO" composite="no">
      <setting id="test_mode" value="1"/>
    </instance>


### Versions ###

|  Verion | Date  | Author  | Notes  |  
|---|---|---|---|
| 1.0.0.2  |  2/8/2020 | Scott Harman  | Initial touchdown support - only allows two chained touchdowns  |
| 1.0.0.5  |  6/8/2020 | Scott Harman  | Multiple chained touchdowns - allows you to keep a touchdown active and switch between two, or, rotate between multiple provided you have sufficient appendages.  |
| 1.0.1.0  | 11/8/2020  |  Scott Harman |  Migrate configuration to instances.xml, with site-specific settings supported, and no practical limit on the number of touchdowns.
|1.0.1.1|12/8/2020|Scott Harman|Corrected an error with routing Flyback to WFM under certain conditions.|
|1.0.1.2|16/9/2020|Scott Harman|Added latching support for Robotic panels.|
|1.0.1.3|12/10/2020|Scott Harman|Renamed SDI source names in configuration|
