// Mapping_TBS.h: interface for the CMapping_TBS class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAPPING_TBS_H__8B8AE2EC_1315_460C_890D_B23ADFFA10AD__INCLUDED_)
#define AFX_MAPPING_TBS_H__8B8AE2EC_1315_460C_890D_B23ADFFA10AD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786)
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"


class CMapping_TBS  
{
public:
	CMapping_TBS();
	virtual ~CMapping_TBS();

	bncs_string ssKeyId;
	int SrcToConference[129];  

	AddMappingTBRecord( bncs_string ssIdEntry );

};

#endif // !defined(AFX_MAPPING_TBS_H__8B8AE2EC_1315_460C_890D_B23ADFFA10AD__INCLUDED_)
