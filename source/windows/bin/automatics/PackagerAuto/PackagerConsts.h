

/* max size of buffer for data from string resource */
#define		MAX_LOADSTRING	100

/* max number of items in debug listbox */
#define		MAX_LB_ITEMS	1500

/* buffer constant for strings */
#define		MAX_AUTO_BUFFER_STRING 280 
#define		UNKNOWNVAL -1

#define		APP_WIDTH	1000
#define		APP_HEIGHT	900

#define		ROUTERCOMMAND 1
#define		INFODRVCOMMAND 2
#define		GPIOCOMMAND 3
#define		DATABASECOMMAND 4

#define		BNCS_COMMAND_RATE  20  // was 10           // number commands sent out per iteration of command timer

// defined colours used in app
#define  COL_BLACK  RGB(0,0,0)
#define  COL_WHITE RGB(255,255,255)

#define  COL_DK_RED RGB(127,0,0)
#define  COL_DK_GRN RGB(0,127,0)
#define  COL_DK_BLUE RGB(0,0,80)
#define  COL_DK_ORANGE RGB(128,64,0)

#define  COL_LT_RED RGB(255,0,0)
#define  COL_LT_GRN RGB(0,255,0)
#define  COL_LT_BLUE RGB(100,100,255)
#define  COL_LT_ORANGE RGB(255,128,0)

#define  COL_YELLOW RGB(255,255,0)
#define  COL_TURQ RGB(210,250,210)


#define ONE_SECOND_TIMER_ID  1	 // could be used to relook for comms port / infodrivers -- for more intelligent driver
#define COMMAND_TIMER_ID		  2							// 
#define STARTUP_TIMER_ID	    	  3							// 
#define DATABASE_TIMER_ID	      4							// 
#define TIKTOK_ALIVE_TIMER_ID  5
#define REVERTIVE_TIMER_ID		  6							// 
#define FORCETXRX_TIMER_ID       7					// 
#define SHAREDRESOURCE_TIMER_ID  8


#define ONE_SECOND_TIMER_INTERVAL	 1000 // 1.5 sec timer used for housekeeping - mainly for re connects to info driver, comport connection failures, device interrogation 
#define COMMAND_TIMER_INTERVAL			100 // 0.1 sec interval on commands being sent from the fifobuffer 
#define STARTUP_TIMER_INTERVAL			1000 // 
#define TENSECONDS								10  
#define SIXTYSECONDS							20  

#define STRINGSEQUAL 0

#define REV_ROUTER   1
#define REV_INFODRV  2
#define REV_GPISTAT  3
#define REV_DBASERM  4

// slot usage
#define DYN_CONF_USE_SLOT    4010
#define DYN_IFB1_USE_SLOT     4011
#define DYN_IFB2_USE_SLOT     4012
#define DYN_IFB3_USE_SLOT     4013
#define DYN_IFB4_USE_SLOT     4014
#define PARK_SOURCE_CMD_SLOT 4081
#define COMM_STATUS_SLOT		4091    
#define PARK_CMD_QUEUE_SLOT 4096             // dev 1001 slot 4096 can be used to stop commands from Packager if going rogue - enter PAUSE

// infodriver groups
#define AUTO_PACK_ROUTE_INFO               1
#define AUTO_MM_STACK_INFO                  2
#define AUTO_AUDIO_PKG_INFO                 3                              // reverse vision; dyn conf assignment;  dyn ifb assignment + status returned
#define AUTO_DEST_PKG_STATUS_INFO    4
#define AUTO_SRCE_PKG_PTI_INFO            5
#define AUTO_RTR_DEST_USE_INFO   		 6
#define AUTO_RIEDEL_CONFERENCE_INFO     7
#define AUTO_RIEDEL_PORT_USE_INFO    8
#define AUTO_RIEDEL_IFBS_INFO               9                   

#define MAX_INFODRIVERS  201                  // 1..200 at present   - 100 used at present -  in 5 designated blocks of 32 to handle 128000 packages + some for reidel conf / ifb processing

#define MAX_PKG_CONFIFB 5                   // 1..4  ifbs / 2 conf  per audio src package
#define MAX_PKG_AUDIO 33                    // 1..32 audio packages in src / dest pkgs / audio levels in APs
#define MAX_AUDLVL_AUDIO 17                    // 1..16 audio audio levels in APs
#define MAX_PKG_VIDEO  5                    // 1..4 levels and other 4 blocks in src / dest pkgs

#define MAX_RIEDEL_RINGS       50                      // 1..49 rings
#define MAX_CONFERENCES     500   				   // allows for up to  1..499 conf in riedel
#define MAX_IFBS                      1500   				    // allows for up to  1500 ifbs  --- PER RING
#define MAX_RIEDEL_SLOTS   4001                     //   1..4000  -- monitor ports per ring
#define MAX_MIXERS_LOGIC    500

// ring master
#define RMSTR_HIGHWAYS_INFO                   3000   // slot 3000 for totals used - specific ring highway usage then from 3001..3050  -- max 50 rings
#define RMSTR_HIGHWAYS_ERROR_MSGS  3900   // slot 3901..3950 - error msgs for highways from that ring/trunk 

#define RMSTR_RINGS_ERROR_MSGS            4000    // 4001..4050 - other error messages linked to trunk / ring

#define AUDIO_PACKAGE 1
#define SOURCE_PACKAGE 2
#define DESTINATION_PACKAGE 3

#define AUDIO_TYPE 1
#define SOURCE_TYPE 2
#define DESTINATION_TYPE 3

#define ROUTER_CMDS_REVS 1          // madi/MADI could be router or infodriver based
#define INFODRV_CMDS_REVS 2

#define ADDTOLIST 1
#define REMOVEFROMLIST -1

// packager database usage 
// source package dbs for first infodriver number
// audio package dbs for 2nd infodriver number
// sporting events dbs for 3rd infodriver number

#define DB_SOURCE_BUTTON	          0
#define DB_SOURCE_NAME		          2
#define DB_SOURCE_INFO		              4
#define DB_SOURCE_AUDIO_PKGS   6            //  list of associated audio pacakages
#define DB_SOURCE_MCR_UMDS	  9
#define DB_SOURCE_PROD_UMDS   10
#define DB_SOURCE_VIDEO_LVLS    11			// main source pacakages

#define DB_DESTINATION_BUTTON	1
#define DB_DESTINATION_NAME		3
#define DB_DESTINATION_INFO			5
#define DB_DESTINATION_DEFN			7
#define DB_DESTINATION_AUDIO_DESTS32    15     // 1..32 audio dests -- replacess 17/18
#define DB_DESTINATION_AUDIO_TAGS32     16     // 1..32 audio tags assoc to dests
#define DB_DESTINATION_AUDIO_SUBS_1     17     // 1..32 audio tags assoc to dests
#define DB_DESTINATION_AUDIO_SUBS_2     18     // 1..32 audio tags assoc to dests
#define DB_DESTINATION_AUDIO_RETURN      19

// audio source packages variants
#define DB_AUDIOPKG_BUTTON		60
#define DB_AUDIOPKG_NAME		 61
#define DB_AUDIOPKG_INFO		 62
#define DB_AUDIOPKG_DEFN		 63                      // audio src packages
#define DB_AUDIOPKG_LINKS16	     64			               
#define DB_AUDIOPKG_RETURN		 65		               
#define DB_AUDIOPKG_VIDLINK		 66		               
#define DB_AUDIOPKG_CFLABEL		 67		               
#define DB_AUDIOPKG_CONFLBL		 68		               

// Sport Events Details    
#define DB_SPEV_BUTTON_NAME		30
#define DB_SPEV_EVENT_ID		31
#define DB_SPEV_ASSET_ID		32
#define DB_SPEV_CLASS_ID		33
#define DB_SPEV_DATE_TIME		34
#define DB_SPEV_TEAMS_ID		35
#define DB_SPEV_STATUS   		36

// TAGs databases and key types
#define DB_TAGS_VIDEO			41
#define DB_TAGS_AUDIO			42
#define DB_TAGS_LANGUAGE		43
#define DB_TAGS_AUDIOTYPE		44
#define DB_TAGS_OWNERS			45
#define DB_TAGS_DELEGATES		46
#define DB_TAGS_RIEDEL			47
#define DB_TAGS_RESV_1		    48
#define DB_TAGS_RESV_2		    49

//  PKGR start-up SYNC Databases
#define DB_SYNC_PACKAGE_ROUTING 101
#define DB_SYNC_DYN_CONF	           104
#define DB_SYNC_DYN_IFBS  	           105

#define DB_RIEDEL_SUBTITLE  12

#define MAX_TAGS_ENTRIES  1000

//Riedel databases
#define DATABASE_RIEDEL_4WNAME	    1
#define DATABASE_RIEDEL_PORT_TYPE	    9

// list delimiters used in data
#define TAB_CHAR 0x09
#define HASH_CHAR 0x23
#define QUOTECHAR1 0x27
#define QUOTECHAR2 0x60

// riedel port types
#define PORT_TYPE_NONE     0
#define PORT_TYPE_INPUT     1
#define PORT_TYPE_OUTPUT   2
#define PORT_TYPE_SPLIT      3
#define PORT_TYPE_4WIRE     4
#define PORT_TYPE_PANEL     5

// these numbers are also index into data check list box
// revertive types
#define VIDEO_HD_ROUTER_TYPE   0
#define VIDEO_UHD_ROUTER_TYPE   1
#define AUDIO_ROUTER_TYPE    2
#define ANCIL_ROUTER_TYPE    3
#define JPEGXS_ROUTER_TYPE   4
#define RINGMASTER_MAIN      5
#define RINGMASTER_CONFS     6 
#define RINGMASTER_PORTS     7
#define RINGMASTER_IFBS         8
#define AUTO_CONTROL_INFO 11


// video or audio status ok or some error
#define SIGNAL_STATUS_NONE  0
#define SIGNAL_STATUS_EXPECTED  1
#define SIGNAL_STATUS_UNEXPECTED   2

#define AUDIO_STATUS_NONE  0
#define AUDIO_STATUS_EXPECTED  1
#define AUDIO_STATUS_UNEXPECTED   2

#define PTI_OVERFLOW_INDICATOR '$'

#define IDENT_OFF 0
#define IDENT_ON 1

#define AUDIO_UNKNOWN 0  // 
#define AUDIO_IDENT   1  // srcs are ident 
#define AUDIO_SILENCE 2  // defined silent src to all dests 
#define AUDIO_SRCPKG  3  // audio srcs defined in src pkg routed

#define AUDIO_TAG_STD 0
#define AUDIO_TAG_SUB1 1
#define AUDIO_TAG_SUB2 2
#define AUDIO_NOMATCH 3


#define NEVION_PARKED           0
#define NEVION_SCHEDULED   1
#define NEVION_ACTIVE            2
#define NEVION_REQUESTED   3
#define NEVION_OTHERPLANE 4
#define NEVION_NOTMAPPED 5

#define NEVION_UNLOCKED 0
#define NEVION_LOCKED       1

// use types for dynamic conferences - stored in db104 

#define IFB_MODE_PRIMARY_LEFT 1 
#define IFB_MODE_SECONDARY_RIGHT 2 

#define DYN_IFB_4WCONF_1 11
#define DYN_IFB_4WCONF_2 12
#define DYN_IFB_4WCONF_3 13
#define DYN_IFB_4WCONF_4 14

#define DYN_TB_CONF_1        21
#define DYN_TB_CONF_2        22

#define DYN_IFB_1 31
#define DYN_IFB_2 32
#define DYN_IFB_3 33
#define DYN_IFB_4 34

#define OTHER_PKGR_ASSIGNED 77777
#define OTHER_PKGR_USAGE       77

#define PORT_USECODE_4WTB 1
#define PORT_USECODE_CFIFB 2
#define PORT_USECODE_MMIFB 3

// consts for slot use in automatics control infodriver

// Locality Code / main riedel ring types ( NOT their trunk address )
#define LDC_LOCALE_RING       1
#define NHN_LOCALE_RING      2

// all listed for completeness
// Packager base offsets
#define CTRL_INFO_PKGR_LDC_BASE  10
#define CTRL_INFO_PKGR_NHN_BASE 20

// Ringmaster base offsets
//#define CTRL_INFO_RMSTR_LDC_BASE  1010
//#define CTRL_INFO_RMSTR_NHN_BASE 1020

// ctrl info common slot offsets -- added to the base values above - which are according to locality code
#define CTRL_INFO_TXRX_TIKTOK      1
#define CTRL_INFO_RXONLY_TIKTOK 2
// offsets for shared riedel pool data for confs and ifbs
#define CTRL_INFO_CONFPOOL  4            // txrx auto - rxonly may use next slot
#define CTRL_INFO_IFBPOOL       6
#define CTRL_INFO_HIGHWAYS   8
