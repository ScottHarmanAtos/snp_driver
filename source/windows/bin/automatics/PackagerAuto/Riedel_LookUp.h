// Riedel_LookUp.h: interface for the CRiedel_LookUp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RIEDEL_LOOKUP_H__4AC3D53B_C95C_4BB0_A8CD_B83C3F73CC90__INCLUDED_)
#define AFX_RIEDEL_LOOKUP_H__4AC3D53B_C95C_4BB0_A8CD_B83C3F73CC90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CRiedel_LookUp  
{
private:
	// set up Address at starup from config
	int m_iTrunkAddr;
	int m_iPort;
	int m_iPanel;
	int m_iShift;
	int m_iKey;

	// these next params are assigned at runtime
	int m_iAssociated_Source_Package;
	int m_iAssociated_Destination_Package;
	int m_iAssociated_Conf;
	int m_iAssociated_IFB;

public:
	CRiedel_LookUp( void );
	virtual ~CRiedel_LookUp();

	void SetAddressString(bncs_string ssData);
	bncs_string GetAddressString(void);

	int GetAddressTrunk(void);
	int GetAddressPort(void);
	int GetAddressPanel(void);
	int GetAddressShift(void);
	int GetAddressKey(void);

	// runtime settings
	void SetAssociated_Source_Package(int iPackage);
	void SetAssociated_Destination_Package(int iPackage);
	void SetAssociated_Conference(int iConf);
	void SetAssociated_IFB(int iIFB);

	int GetAssociated_Source_Package(void);
	int GetAssociated_Destination_Package(void);
	int GetAssociated_Conference(void);
	int GetAssociated_IFB(void);

};

#endif // !defined(AFX_RIEDEL_LOOKUP_H__4AC3D53B_C95C_4BB0_A8CD_B83C3F73CC90__INCLUDED_)
