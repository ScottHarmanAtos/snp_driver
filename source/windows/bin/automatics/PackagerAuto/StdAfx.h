// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
#define AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996 4244)

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

// Windows Header Files
#include <windows.h>
#include <commctrl.h> 

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdio.h>
#include <time.h>
#include <WINBASE.h>

#include <iostream>
#include <direct.h>

// maps used all over the shop
#include <map>
	using namespace std;
#include <queue>
	using namespace std;

// Colledia Control Header Files
#include <bncs32.h>
#include <bncs.h>
#include <extinfo.h>
#include <csiclient.h>
#include <bncs_config.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <decodeBNCSString.h>
#include <decodecaptionstring.h>
#include <database.h>
#include <databaseMgr.h>

// common  classes
#include "bncs_auto_helper.h"

// auto classes
#include "AudioPackages.h"
#include "Command.h"
#include "CompInstances.h"
#include "DestinationPackages.h"
#include "PackageTags.h"
#include "RevsRouterData.h"
#include "RouterDestData.h"
#include "RouterSrceData.h"
#include "SourcePackages.h"
#include "SportEvent.h"

#include "Riedel_LookUp.h"
#include "RiedelPortPackageUse.h"

#include "Riedel_Rings.h"
#include "RingMaster_CONF.h"
#include "RingMaster_IFB.h"
#include "RingMaster_LOGIC.h"
#include "RingMaster_Ports.h"

#include "except.h"

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
