// SourcePackages.cpp: implementation of the CSourcePackages class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SourcePackages.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSourcePackages::CSourcePackages()
{
	iPackageIndex = -1;
	ss_PackageLongName = "";
	ss_PackageButtonName = "";
	ss_PackageKeyInfo = "";
	ss_PackageDefinition = "";
	ss_PackageMCRUMD = "";
	ss_PackageProdUMDs = "";
	ss_PackageVideoLvls = "";

	m_iOwnerType=0;
	m_iDelegateType=0;
	m_iAssociatedSportEvent=0;    

	m_iNumberAssignedAudioPackages = 0;
	for (int iidp = 0; iidp < MAX_PKG_AUDIO; iidp++) m_iAssoc_Audio_Packages[iidp] = 0;

	for (int iivp = 0; iivp < MAX_PKG_VIDEO; iivp++) {
		// vid
		st_PkgVideoLevels[iivp].stVideoLevel.iAssocIndex = 0;
		st_PkgVideoLevels[iivp].stVideoLevel.iAssocHub_or_TypeTag = 0;
		st_PkgVideoLevels[iivp].stVideoLevel.iAssocLang_or_UseTag = 0;
		// anc 1-4
		for (int iianc = 1; iianc < MAX_PKG_VIDEO; iianc++) {
			st_PkgVideoLevels[iivp].stANCLevel[iianc].iAssocIndex = 0;
			st_PkgVideoLevels[iivp].stANCLevel[iianc].iAssocHub_or_TypeTag = 0;
			st_PkgVideoLevels[iivp].stANCLevel[iianc].iAssocLang_or_UseTag = 0;
		}
		// audio
		st_SourceAudioLevels[iivp].iAssocIndex = 0;
		st_SourceAudioLevels[iivp].iAssocHub_or_TypeTag = 0;
		st_SourceAudioLevels[iivp].iAssocLang_or_UseTag = 0;
	}

	ssl_Pkg_DirectlyRoutedTo = bncs_stringlist("", ',');
	ssl_Pkg_FinallyRoutedTo = bncs_stringlist("", ',');


}

CSourcePackages::~CSourcePackages()
{

}
