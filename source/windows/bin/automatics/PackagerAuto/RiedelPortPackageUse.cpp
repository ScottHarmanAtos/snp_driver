// RIEDELPortPackageUse.cpp: implementation of the CRiedelPortPackageUse class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RiedelPortPackageUse.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRiedelPortPackageUse::CRiedelPortPackageUse( int iIndx )
{
	iRecordIndex = iIndx;
	ssl_Ring_11_TB_Details = bncs_stringlist("", '|');
	ssl_Ring_12_TB_Details = bncs_stringlist("", '|');
	ssl_Ring_13_TB_Details = bncs_stringlist("", '|');
	ssl_Ring_14_TB_Details = bncs_stringlist("", '|');

	ssl_Ring_11_CF_Details = bncs_stringlist("", '|');
	ssl_Ring_12_CF_Details = bncs_stringlist("", '|');
	ssl_Ring_13_CF_Details = bncs_stringlist("", '|');
	ssl_Ring_14_CF_Details = bncs_stringlist("", '|');

	ssl_Ring_11_MM_Details = bncs_stringlist("", '|');
	ssl_Ring_12_MM_Details = bncs_stringlist("", '|');
	ssl_Ring_13_MM_Details = bncs_stringlist("", '|');
	ssl_Ring_14_MM_Details = bncs_stringlist("", '|');
}

CRiedelPortPackageUse::~CRiedelPortPackageUse()
{
	// burn baby burn
}

void CRiedelPortPackageUse::SetUpdateInfodriver(void)
{
	bUpdateInfodriver = TRUE;
}

BOOL  CRiedelPortPackageUse::IsUpdateInfodriver(void)
{
	if (bUpdateInfodriver ) {  
		// flag is set so reset and return true
		bUpdateInfodriver = FALSE;
		return TRUE;
	}
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////

BOOL CRiedelPortPackageUse::AddDetailsByRingAddr(int iRingAddr, int iUseCode, bncs_string ssData)
{
	// NOTE -***- user needs to remove entries for packages that already exist as class no longer deletes old/prev entry
	// now add
	if (iUseCode == PORT_USECODE_4WTB) {
		switch (iRingAddr) {
		case 11: if (ssl_Ring_11_TB_Details.find(ssData) < 0) { ssl_Ring_11_TB_Details.append(ssData); return TRUE; } break;
		case 12: if (ssl_Ring_12_TB_Details.find(ssData) < 0) { ssl_Ring_12_TB_Details.append(ssData); return TRUE; } break;
		case 13: if (ssl_Ring_13_TB_Details.find(ssData) < 0) { ssl_Ring_13_TB_Details.append(ssData); return TRUE; } break;
		case 14: if (ssl_Ring_14_TB_Details.find(ssData) < 0) { ssl_Ring_14_TB_Details.append(ssData); return TRUE; } break;
		}
	}
	else if (iUseCode == PORT_USECODE_CFIFB) {
		switch (iRingAddr) {
		case 11: if (ssl_Ring_11_CF_Details.find(ssData) < 0) { ssl_Ring_11_CF_Details.append(ssData); return TRUE; } break;
		case 12: if (ssl_Ring_12_CF_Details.find(ssData) < 0) { ssl_Ring_12_CF_Details.append(ssData); return TRUE; } break;
		case 13: if (ssl_Ring_13_CF_Details.find(ssData) < 0) { ssl_Ring_13_CF_Details.append(ssData); return TRUE; } break;
		case 14: if (ssl_Ring_14_CF_Details.find(ssData) < 0) { ssl_Ring_14_CF_Details.append(ssData); return TRUE; } break;
		}
	}
	else if (iUseCode == PORT_USECODE_MMIFB) {
		switch (iRingAddr) {
		case 11: if (ssl_Ring_11_MM_Details.find(ssData) < 0) { ssl_Ring_11_MM_Details.append(ssData); return TRUE; } break;
		case 12: if (ssl_Ring_12_MM_Details.find(ssData) < 0) { ssl_Ring_12_MM_Details.append(ssData); return TRUE; } break;
		case 13: if (ssl_Ring_13_MM_Details.find(ssData) < 0) { ssl_Ring_13_MM_Details.append(ssData); return TRUE; } break;
		case 14: if (ssl_Ring_14_MM_Details.find(ssData) < 0) { ssl_Ring_14_MM_Details.append(ssData); return TRUE; } break;
		}
	}
	return FALSE;
}

int CRiedelPortPackageUse::GetIndexPositionForPackageByRingAddr(int iRingAddr, int iUseCode, bncs_string ssPkgIndx)
{
	if (iUseCode == PORT_USECODE_4WTB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_TB_Details.find(ssPkgIndx); break;
		case 12: return ssl_Ring_12_TB_Details.find(ssPkgIndx); break;
		case 13: return ssl_Ring_13_TB_Details.find(ssPkgIndx); break;
		case 14: return ssl_Ring_14_TB_Details.find(ssPkgIndx); break;
		}
	}
	else 	if (iUseCode == PORT_USECODE_CFIFB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_CF_Details.find(ssPkgIndx); break;
		case 12: return ssl_Ring_12_CF_Details.find(ssPkgIndx); break;
		case 13: return ssl_Ring_13_CF_Details.find(ssPkgIndx); break;
		case 14: return ssl_Ring_14_CF_Details.find(ssPkgIndx); break;
		}
	}
	else 	if (iUseCode == PORT_USECODE_MMIFB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_MM_Details.find(ssPkgIndx); break;
		case 12: return ssl_Ring_12_MM_Details.find(ssPkgIndx); break;
		case 13: return ssl_Ring_13_MM_Details.find(ssPkgIndx); break;
		case 14: return ssl_Ring_14_MM_Details.find(ssPkgIndx); break;
		}
	}
	return UNKNOWNVAL;
}



int CRiedelPortPackageUse::GetNumberOfEntriesByRingAddr(int iRingAddr, int iUseCode)
{
	if (iUseCode == PORT_USECODE_4WTB) {
		switch (iRingAddr) {
			case 11: return ssl_Ring_11_TB_Details.count(); break;
			case 12: return ssl_Ring_12_TB_Details.count(); break;
			case 13: return ssl_Ring_13_TB_Details.count(); break;
			case 14: return ssl_Ring_14_TB_Details.count(); break;
		}
	}
	else 	if (iUseCode == PORT_USECODE_CFIFB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_CF_Details.count(); break;
		case 12: return ssl_Ring_12_CF_Details.count(); break;
		case 13: return ssl_Ring_13_CF_Details.count(); break;
		case 14: return ssl_Ring_14_CF_Details.count(); break;
		}
	}
	else 	if (iUseCode == PORT_USECODE_MMIFB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_MM_Details.count(); break;
		case 12: return ssl_Ring_12_MM_Details.count(); break;
		case 13: return ssl_Ring_13_MM_Details.count(); break;
		case 14: return ssl_Ring_14_MM_Details.count(); break;
		}
	}
	return 0;
}


bncs_string CRiedelPortPackageUse::GetDetailsByRingAddr(int iRingAddr, int iUseCode)
{
	if (iUseCode == PORT_USECODE_4WTB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_TB_Details.toString('|'); break;
		case 12: return ssl_Ring_12_TB_Details.toString('|'); break;
		case 13: return ssl_Ring_13_TB_Details.toString('|'); break;
		case 14: return ssl_Ring_14_TB_Details.toString('|'); break;
		}
	}
	else if (iUseCode == PORT_USECODE_CFIFB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_CF_Details.toString('|'); break;
		case 12: return ssl_Ring_12_CF_Details.toString('|'); break;
		case 13: return ssl_Ring_13_CF_Details.toString('|'); break;
		case 14: return ssl_Ring_14_CF_Details.toString('|'); break;
		}
	}
	else if (iUseCode == PORT_USECODE_MMIFB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_MM_Details.toString('|'); break;
		case 12: return ssl_Ring_12_MM_Details.toString('|'); break;
		case 13: return ssl_Ring_13_MM_Details.toString('|'); break;
		case 14: return ssl_Ring_14_MM_Details.toString('|'); break;
		}
	}
	return "";
}

//////////////////////////////////////////////////////////////////////////////

void CRiedelPortPackageUse::ClearAllDetails(void)
{
	ssl_Ring_11_TB_Details.clear();
	ssl_Ring_12_TB_Details.clear();
	ssl_Ring_13_TB_Details.clear();
	ssl_Ring_14_TB_Details.clear();
	ssl_Ring_11_CF_Details.clear();
	ssl_Ring_12_CF_Details.clear();
	ssl_Ring_13_CF_Details.clear();
	ssl_Ring_14_CF_Details.clear();
	ssl_Ring_11_MM_Details.clear();
	ssl_Ring_12_MM_Details.clear();
	ssl_Ring_13_MM_Details.clear();
	ssl_Ring_14_MM_Details.clear();

}

void CRiedelPortPackageUse::ClearDetailsByRingAddr(int iRingAddr, int iUseCode)
{
	if (iUseCode == PORT_USECODE_4WTB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_TB_Details.clear();
		case 12: return ssl_Ring_12_TB_Details.clear();
		case 13: return ssl_Ring_13_TB_Details.clear();
		case 14: return ssl_Ring_14_TB_Details.clear();
		}
	}
	else if (iUseCode == PORT_USECODE_CFIFB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_CF_Details.clear(); break;
		case 12: return ssl_Ring_12_CF_Details.clear(); break;
		case 13: return ssl_Ring_13_CF_Details.clear(); break;
		case 14: return ssl_Ring_14_CF_Details.clear(); break;
		}
	}
	else if (iUseCode == PORT_USECODE_MMIFB) {
		switch (iRingAddr) {
		case 11: return ssl_Ring_11_MM_Details.clear(); break;
		case 12: return ssl_Ring_12_MM_Details.clear(); break;
		case 13: return ssl_Ring_13_MM_Details.clear(); break;
		case 14: return ssl_Ring_14_MM_Details.clear(); break;
		}
	}
}

void CRiedelPortPackageUse::ClearDetailsPerRingPerEntry(int iRingAddr, int iUseCode, int iEntry)
{
	if (iEntry >= 0) {
		if (iUseCode == PORT_USECODE_4WTB) {
			switch (iRingAddr) {
			case 11: if (iEntry < ssl_Ring_11_TB_Details.count()) ssl_Ring_11_TB_Details.deleteItem(iEntry); break;
			case 12: if (iEntry < ssl_Ring_12_TB_Details.count()) ssl_Ring_12_TB_Details.deleteItem(iEntry); break;
			case 13: if (iEntry < ssl_Ring_13_TB_Details.count()) ssl_Ring_13_TB_Details.deleteItem(iEntry); break;
			case 14: if (iEntry < ssl_Ring_14_TB_Details.count()) ssl_Ring_14_TB_Details.deleteItem(iEntry); break;
			}
		}
		else if (iUseCode == PORT_USECODE_CFIFB) {
			switch (iRingAddr) {
			case 11: if (iEntry < ssl_Ring_11_CF_Details.count()) ssl_Ring_11_CF_Details.deleteItem(iEntry); break;
			case 12: if (iEntry < ssl_Ring_12_CF_Details.count()) ssl_Ring_12_CF_Details.deleteItem(iEntry); break;
			case 13: if (iEntry < ssl_Ring_13_CF_Details.count()) ssl_Ring_13_CF_Details.deleteItem(iEntry); break;
			case 14: if (iEntry < ssl_Ring_14_CF_Details.count()) ssl_Ring_14_CF_Details.deleteItem(iEntry); break;
			}
		}
		else if (iUseCode == PORT_USECODE_MMIFB) {
			switch (iRingAddr) {
			case 11: if (iEntry < ssl_Ring_11_MM_Details.count()) ssl_Ring_11_MM_Details.deleteItem(iEntry); break;
			case 12: if (iEntry < ssl_Ring_12_MM_Details.count()) ssl_Ring_12_MM_Details.deleteItem(iEntry); break;
			case 13: if (iEntry < ssl_Ring_13_MM_Details.count()) ssl_Ring_13_MM_Details.deleteItem(iEntry); break;
			case 14: if (iEntry < ssl_Ring_14_MM_Details.count()) ssl_Ring_14_MM_Details.deleteItem(iEntry); break;
			}
		}
	}
}

