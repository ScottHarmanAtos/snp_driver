//////////////////////////////////////
// header file for PackagerAuto project //
//////////////////////////////////////

#if !defined(AFX_PACKAGERAUTO_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
#define AFX_PACKAGERAUTO_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996 4244)

#include "resource.h"
#include "PackagerConsts.h"

/* main.cpp */
/* prototypes for main functionality */
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT				InitApp(HWND);
void				CloseApp(HWND);

void				LoadIni(void);
char *				getBNCS_File_Path( void );    // new for v4.2 of wizard - assigns the global szBNCS_File_Path
char*				r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite);
void				w_p(char* file, char* section, char* entry, char* setting);
int					getWS(void);

void				Debug(LPCSTR szFmt, ...);
void				Log(LPCSTR szDate);


BOOL			SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight);

int				SplitString(char *string, char *delim, UINT count, char **outarray);

void          InitStatusListboxDialog( void );  // helper function to initialise listbox on app dialog

/* PackagerAuto.cpp /*

/* prototypes for the notification functions */
void UpdateCounters(void);
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot);
LRESULT CSIClientNotify(extclient* pec,LPCSTR szMsg);

BOOL LoadPackagerCompositeInstances(void);
BOOL LoadAllInitialisationData( void );
BOOL ConnectAllInfodrivers( void );
void ProcessFirstPass_Logic( void );

void ProcessListOfDestinationPackageStatusCalc( void );

BOOL ReloadASourcePackageData(int iIndex, BOOL bReassertRouting);
BOOL ReloadAnAudioPackageData(int iPkgIndex, BOOL bReassertRoutes);
BOOL ReloadADestinationPackageData(int iIndex, BOOL bReassertRouting);

char* ExtractNamefromDevIniFile(int iDev, int iDbFile, int iEntry);

BOOL AutomaticRegistrations(void);
void PollAutomaticDrivers( int iCounter );
void VersionInfo();
void CheckAndSetInfoResilience( void );
void ForceAutoIntoRXOnlyMode( void );
void ForceAutoIntoTXRXMode( void );
void Restart_CSI_NI_NO_Messages(void);

void SendPackagerBNCSCommand(int iAssocIndx, int iCmdInfoType, LPCSTR szCommand);
void SendPackagerBNCSRevertive(int iAssocIndx, int iRevInfoType, BOOL bSetSlot, LPCSTR szRevertive);

void ClearAutomaticMaps();

void ClearCommandQueue( void );
void AddCommandToQue( LPCSTR szCmd, int iCmdType, int iDev, int iSlot, int iDB );
void ProcessNextCommand(int iRateRevs);

void ClearRevertiveQueue(void);
void AddRevertiveToQue(bncs_string ssCmd, int iCmdType, int iDev, int iSlot, int iDB);
void ProcessNextRevertive(int iRateRevs);

void AddAudioPackageToMSP(int iMainSrcPkg, int iAudioPackage);
void RemoveAudioPackageToMSP(int iMainSrcPkg, int iAudioPackage);

void DisplayAudioSourcePackageData();
void DisplaySourcePackageData();
void DisplayDestinationPackageData( BOOL bDecExp );
void DisplaySelectedInfodriver(void);
void DisplayRiedelRingData();
void DisplayGRDDestData();


void ExportIFBDatabase();
void ExportConferenceDatabase();
void ExportPkgRoutingDatabase();

/*function to create nice new paint brushes*/
void CreateBrushes();
/*function to clean paint brushes and put them away*/
void ReleaseBrushes();

void CloseAllInfodrivers();
void SendOutClosingMessage();

#ifndef EXT
#define EXT extern
#endif

// previous conf ports / ifb ports + labels to determine any changes
struct stAPConf_IFB_Details {
	int i_in_ring;
	int i_in_port;
	int i_mm_ring;
	int i_mm_port;
	int i_out_ring;
	int i_out_port;
	bncs_string m_ss_MainLabel;
	bncs_string m_ss_SubTitleLabel;
	int i_Use_Which_DestPkgIFB;
};


/////////////////////////////////////////////////////////////////////
// Global Variables for the PackagerAuto project
/////////////////////////////////////////////////////////////////////

EXT HINSTANCE hInst;						// current instance
EXT TCHAR szTitle[MAX_LOADSTRING];			// The title bar text
EXT TCHAR szWindowClass[MAX_LOADSTRING];	// The title bar text
EXT char szBNCS_File_Path[MAX_AUTO_BUFFER_STRING]; // global for Colledia system file path - assigned in getBNCS_File_Path, used in r_p and w_p
EXT char szBNCS_Log_File_Path[MAX_AUTO_BUFFER_STRING];  // global for crash log file path - assigned in getBNCS_File_Path
EXT char szAppName[ MAX_LOADSTRING];                     // application name used in crash log messages
EXT char szResult[MAX_AUTO_BUFFER_STRING];						// global for profile reads
EXT char szClientCmd[256];						// global for commands via ecclient
EXT char szRevString[512];						// global for revertive string responses in infodrivers etc
EXT char szGloCommand[MAX_AUTO_BUFFER_STRING];               // global strings for commands and revs 
EXT char szGloRevertive[MAX_AUTO_BUFFER_STRING];
EXT char szRMData[512];														// global for RM handling and r_p database reads
EXT char szDebug[1000];						// global strings data for debug msgs
EXT char szLogDbg[1100];
EXT BOOL fLog;								// bncs log to file enable flag
EXT int iDebugFlag;							   // debug enable flag
EXT BOOL fDebugHide;							   // debug enable flag
EXT BOOL fDbgView;							   // debug view outputstring enable flag
EXT HWND hWndMain,hWndList;					// window handles, main and debug
EXT HWND hWndDlg;							// window handle for child dialog
EXT int iChildHeight;						// height of child dialog
EXT RECT rc;

EXT ULONG lTXID;							// ID tx count
EXT ULONG lRXID;							// ID rx count

EXT extclient *ecCSIClient;				// a client connection class

EXT bncs_string ssCompositeInstance;   // parameter passed in on startup line
EXT CCompInstances* cl_KeyInstances;  // Packager composite instances loaded at startup

EXT int iPackager_router_main;              // base infodriver for auto - determined from composite passed in now
EXT int iPackager_mm_stack_main;
EXT int iPackager_audio_package_main;
EXT int iPackager_dest_status_main;
EXT int iPackager_source_pti_main;
EXT int iPackager_nevion_dest_main;

EXT int iPackager_riedel_conference_main;
EXT int iPackager_riedel_port_use_main;
EXT int iPackager_riedel_ifbs_main;


EXT int iAutomaticLocation;                        // LDC or NHN or ???? other multi sites ???
EXT int iAutomatics_CtrlInfo_Local;         // used for tic-tok alive notifications between sites and passing shared resources data
EXT int iAutomatics_CtrlInfo_Remote;    // used for tic-tok alive notifications between sites and passing shared resources data
EXT int iPrevCtrlStatus, iControlStatus, iCtrlStatusCalcCounter, iCtrlRequestRemoteCounter;      // overall status from cntrol infodriver reports - colours status field
EXT int iLDC_TXRX_TicTocStatus, iLDC_RXONLY_TicTocStatus;
EXT int iNHN_TXRX_TicTocStatus, iNHN_RXONLY_TicTocStatus;
EXT bncs_string ssLDC_TX_Revertive, ssLDC_RX_Revertive;
EXT bncs_string ssNHN_TX_Revertive, ssNHN_RX_Revertive;

EXT int iOtherSite_Pkgr_router_main;        // base infodriver for "other" packager 

EXT BOOL bSendingOutDynConfData, bSendingOutDynIFBData;
EXT bncs_stringlist sslSendingOutDynConfData, sslSendingOutDynIFBData;


// 10 main infodrivers used in app - all will be kept in ststus sync with base info device number -- all TXRX or all RXONLY
EXT int iNumberOfInfodriversRequired;     // 196 infodrivers required at present ( 32*6 blocks of pkgr infos + 3 riedel )
EXT int iNumberInfosInBlock;              // default 32 infodrivers in a block ( 128000 )

EXT int iInfodriverDivisor;               // default is 4000 - ie pkg 4001 is next infodriver etc -- could be 4096 if push comes to shove

EXT int i_rtr_Infodrivers[MAX_INFODRIVERS];   // 1..200
EXT extinfo eiInfoDrivers[MAX_INFODRIVERS];    	// 1..200 infodriver for automatic  1 == base device number

EXT int iNumberOfPackages;       // 64000 or 128000
EXT int iPackagerBarsSrcePkg;       // 64001 or 128001
EXT int iPackagerParkSrcePkg;       // 64002 or  128002
EXT int iNumberAudioSourcePackages;

EXT queue<CCommand*> qCommands;            // queue of commands to go to bncs drivers, routers, other devices 
EXT queue<CCommand*> qRevertives;            // queue of revertives from bncs drivers and other automatics 

EXT map<int, CCommand*>*  mapOfDBFile;   // map to fast read load eg  db101 in first pass and other db and init data files

// package maps
EXT map<int, CSourcePackages*>*  mapOfAllSrcPackages;
EXT map<int, CAudioPackages*>*  mapOfAllAudioPackages;
EXT map<int, CSportEvent*>*  mapOfAllSportingEvents;
EXT map<int, CDestinationPackages*>*  mapOfAllDestPackages;

EXT map<int, CPackageTags*>*  mapOfAllTags;
EXT int iTotalNumberTags;

//  router maps
EXT int iIP_RouterVideoHD;	            // Nevion IP for video base device, and number of Infodrivers used for vision, audio and anc
EXT int iIP_RouterAudio2CH;																	// Nevion Audio,    -- 2 channel audio 
EXT int iIP_RouterANC;																				// Nevion ANC, 

EXT int iIP_RouterVideoUHD;																		// Nevion UHD router,
EXT int iIP_RouterJpegXS;																		// Nevion JPEGXS router

EXT int iIP_RouterInfoDivider;																// to be in line with ip_router infodriver_divider value, default=4096, may be set to 4000

EXT map<int, CRevsRouterData*>* cl_RouterRecords;    // int key is bncs device number of router in question

// Ring Master vars
EXT int i_RingMaster_Main, iRemote_RingMaster_Main;          // main ring master infodriver dev number
EXT int i_RingMaster_Conf, iRemote_RingMaster_Conf;           // ring master conf infodriver
EXT int i_RingMaster_Ifb_Start, i_RingMaster_Ifb_End;            // ifb infodrivers - one per ring now
EXT int i_RingMaster_Ports_Start, i_RingMaster_Ports_End;   // ports infodrvs - on per ring- 
EXT int i_RingMaster_Ports_Pti;                                 // not required yet in this auto at this time

// Ring Master maps

EXT int iNumberRiedelRings;
EXT map<int, CRiedel_Rings*>*  cl_Riedel_Rings;                                       // data pertaining to ringmaster ring / trunk 
EXT int iNumberRMstrIFBRecords;
EXT map<int, CRingMaster_IFB*>*  cl_RingMaster_IFBs;                         // data pertaining to ringmaster view of Riedel conferences
EXT int iNumberRMstrPortsRecords;
EXT map<int, CRingMaster_Ports*>* cl_RingMaster_Ports;           // data on known ports across rings from ringmaster revs
EXT int iNumberRMstrConferenceRecords;    // these are overall totals of records across all rings (eg conf = 500 * number rings )
EXT map<int, CRingMaster_CONF*>*  cl_RingMaster_Conferences;     // data pertaining to ringmaster view of Riedel conferences
EXT int iNumberRMstrLogicRecords;
EXT map<int, CRingMaster_LOGIC*>*  cl_RingMaster_Logic;  // key is logic source index
//
EXT int iNumberRiedePortPackageRecords;
EXT map<int, CRiedelPortPackageUse*>*  map_RiedelPortPackageUse; // stores riedel port use across all rings  determined from source package data

EXT bncs_string iKeyCurrentIFB[MAX_RIEDEL_SLOTS];    // current data in dev 594 - loaded at first pass, used to clear keys of mon ifs 

EXT BOOL bPackageRoutingUsageFromDB;    // used to indicate that DB101 is active as data source for routing rather than original infodriver slots as precedence

///////////////////////////////////////////
EXT int iOverallTXRXModeStatus;			// overall mode -- based on base infodriver i.e gpi input driver
EXT BOOL bConnectedToInfoDriverOK;	// connected to the infodriver or not
EXT BOOL bValidCollediaStatetoContinue;
EXT BOOL bAutomaticStarting;
EXT BOOL bShowAllDebugMessages;

EXT int iOneSecondPollTimerId;
EXT BOOL bOneSecondPollTimerRunning;
EXT int iNextResilienceProcessing;        // count down to zero -- check info tx/rx status 
EXT int iOverallStateJustChanged;
EXT int iOverallModeChangedOver;

EXT int iNextCommandTimerId;
EXT BOOL bNextCommandTimerRunning;
EXT BOOL bNextRevertiveTimerRunning;
EXT BOOL bRMDatabaseTimerRunning;

EXT int iLabelAutoStatus, iLabelInfoStatus; // used to distinguish diff cols for label
EXT int iStartupCounter;

EXT int iChosenSrcPkg, iChosenAudioSrcPkg;
EXT int iChosenDestPkg;
EXT int iChosenRing, iChosenTrunk, iChosenRiedelDest;
EXT int iChosenConference, iChosenIFB;
EXT int iChosenMonitorPort;
EXT int iChosenLogic, iChosenInfodriver;

EXT databaseMgr dbmPkgs;

// used in recursive routine to store trace for dest pkg when being calculated
EXT bncs_stringlist ssl_Calc_Trace;
EXT bncs_stringlist ssl_RoutedDestPackages;  // holds list of dest pkgs downstream from a source / virtual - used in processDestPkgCmd and srce pkg reloads

EXT BOOL bFoundVirtualSourceAhead;    // used when checking to find if vir likely to be routed to itself somehow
EXT int iRecursiveCounter; // used in recursive loops to (try) and limit recursion in event of routing loops

EXT int i_Start_VirtualPackages;   // if 0 then no virtual packages, for this system starts at 3001

EXT int intHD_BLACK; // all these from packager xml
EXT int intHD_BARS; 
EXT int intHD_SILENCE; 

// rm cmds - store db,index for src / dest RM changes
EXT bncs_stringlist ssl_RMChanges;
EXT bncs_stringlist ssl_RM_SrcPkg_Changes;
EXT bncs_stringlist ssl_RM_DestPkg_Changes;
EXT bncs_stringlist ssl_RM_AudioPkg_Changes;
// list of dest pkgs to check for sdi / audio status after routing / RM / change of some form
EXT bncs_stringlist ssl_DestPkgs_For_SDIStatus_Calc;  
EXT int iSDIStatusCalcCounter;

EXT int iCheckRing;
EXT int iCheckMonitor;
EXT int iCheckConference;
EXT int iCheckIFB;

EXT int iMarkerNotMMQueue, iMarkerInMMQueue, iMarkerTopMMQueue, iMarkerIFBDisabled; 
// markers read in from xml config file - marker"in"queue - means member of but not top of mm que

EXT bncs_string ssKeyClearCmd;

EXT int iLoadSrcPkgs, iLoadDestPkgs;

EXT int iDynConferencePoolStart, iDynConferencePoolEnd;
EXT int iDynConfPoolSize, iDynConfPoolInUse;    // both TB and CF confs

EXT int iDynIFBPoolStart, iDynIFBPoolEnd, iDynIFBPoolSize;
EXT int iDynIFBPoolInUse[5];  // 1..4 rings

EXT HANDLE hCSIClientSemaphore;
EXT BOOL bCSIClientProcessing;
EXT int iHighestCommandQueue;
EXT int iPauseCommandProcessing;        // counter- set via slot 4096 in 1001 PAUSE command processing - to stop routing storms

EXT struct stAPConf_IFB_Details stPreDPIFBS[MAX_PKG_CONFIFB];
EXT struct stAPConf_IFB_Details stPreIFBS[MAX_PKG_CONFIFB];
EXT struct stAPConf_IFB_Details stPreTBConfs[MAX_PKG_CONFIFB];



///////////////////////////////////////////////////////////////////

EXT	HBRUSH hbrWindowsGrey;
EXT	HBRUSH hbrBlack;
EXT	HBRUSH hbrWhite;

EXT	HBRUSH hbrDarkRed;
EXT	HBRUSH hbrDarkGreen;
EXT	HBRUSH hbrDarkBlue;
EXT	HBRUSH hbrDarkOrange;

EXT	HBRUSH hbrYellow;
EXT	HBRUSH hbrTurq;

EXT	HBRUSH hbrLightRed;
EXT	HBRUSH hbrLightGreen;
EXT	HBRUSH hbrLightOrange;
EXT	HBRUSH hbrLightBlue;

EXT HBRUSH HBr, HBrOld;

EXT COLORREF iCurrentTextColour;
EXT COLORREF iCurrentBackgroundColour;
EXT HBRUSH iCurrentBrush;



#endif // !defined(AFX_PACKAGERAUTO_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
