// SlaveIndex.h: interface for the CSlaveIndex class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SLAVEINDEX_H__90833E1F_1236_46A4_A21F_9DA93D521218__INCLUDED_)
#define AFX_SLAVEINDEX_H__90833E1F_1236_46A4_A21F_9DA93D521218__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
	#include <windows.h>
	#include "PackagerConsts.h"
	#include <bncs_string.h>
	#include <bncs_stringlist.h>
	#include "bncs_auto_helper.h"

class CSlaveIndex  
{
private:
	int m_iSlave_Package_Index;
	int m_iMaster_Package_Index;

public:
	CSlaveIndex();
	virtual ~CSlaveIndex();

	void AddSlaveRecord( int iSlave, int iMaster );
	int GetSlaveIndex( void );
	int GetMasterIndex( void );

};

#endif // !defined(AFX_SLAVEINDEX_H__90833E1F_1236_46A4_A21F_9DA93D521218__INCLUDED_)
