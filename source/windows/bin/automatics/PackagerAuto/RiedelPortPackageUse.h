// RiedelPortPackageUse.h: interface for the CRiedelPortPackageUse class.
//
//  to hold info per TB port on source package usage details for PTI into info 597
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RIEDELPORTPACKAGEUSE_H__12F7C9E4_E5C5_4A15_A87A_A388C568EDB4__INCLUDED_)
#define AFX_RIEDELPORTPACKAGEUSE_H__12F7C9E4_E5C5_4A15_A87A_A388C568EDB4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CRiedelPortPackageUse  
{
private:

	int iRecordIndex;
	BOOL bUpdateInfodriver;

	// QnD method string for each known ring type
	bncs_stringlist ssl_Ring_11_TB_Details;   // delimted list each contains hub,pkg(s) for 4W TB ( audio packages )
	bncs_stringlist ssl_Ring_12_TB_Details;   
	bncs_stringlist ssl_Ring_13_TB_Details;  
	bncs_stringlist ssl_Ring_14_TB_Details;   

	bncs_stringlist ssl_Ring_11_CF_Details;   // delimted list each contains hub,pkg(s) for CF IFBs ( audio packages )
	bncs_stringlist ssl_Ring_12_CF_Details;
	bncs_stringlist ssl_Ring_13_CF_Details;
	bncs_stringlist ssl_Ring_14_CF_Details;

	bncs_stringlist ssl_Ring_11_MM_Details;   // delimted list each contains hub,pkg(s) for MM IFBs ( destination packages )
	bncs_stringlist ssl_Ring_12_MM_Details;
	bncs_stringlist ssl_Ring_13_MM_Details;
	bncs_stringlist ssl_Ring_14_MM_Details;

public:

	CRiedelPortPackageUse( int iRecordIndx);
	virtual ~CRiedelPortPackageUse();

	BOOL AddDetailsByRingAddr(int iRingAddr, int iUseCode, bncs_string ssData );   // will add entry of hub#package_number

	bncs_string GetDetailsByRingAddr(int iRingAddr, int iUseCode);   // lists all packages and codes
	int GetNumberOfEntriesByRingAddr(int iRingAddr, int iUseCode);
	int GetIndexPositionForPackageByRingAddr(int iRingAddr, int iUseCode, bncs_string ssPkgIndx);


	void ClearAllDetails( void );
	void ClearDetailsByRingAddr(int iRingAddr, int iUseCode);
	void ClearDetailsPerRingPerEntry(int iRingAddr, int iUseCode, int iEntry);   // 0..(number of entries-1) 

	void SetUpdateInfodriver(void);
	BOOL IsUpdateInfodriver(void );

};

#endif // !defined(AFX_RIEDELPORTPACKAGEUSE_H__12F7C9E4_E5C5_4A15_A87A_A388C568EDB4__INCLUDED_)
