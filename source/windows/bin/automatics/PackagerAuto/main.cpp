///////////////////////////////////////////////
//	main.cpp - core code for the application //
///////////////////////////////////////////////

/************************************
  REVISION LOG ENTRY
  Revision By: Chris Gil
  Revised on 23/07/2002 19:40:00
  Version: V
  Comments: Added SNMP 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 18/07/2001 11:29:51
  Version: V
  Comments: Modified extinfo, extclient, extdriver, to have connect() function so
			all constructors take no parameters
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 28/03/2001 14:42:29
  Version: V
  Comments: new comms class specifies the serial port number in the open() function
  now, rather than the constructor
 ************************************/


/************************************
  REVISION LOG ENTRY
  Revision By: PaulW
  Revised on 20/09/2006 12:12:12
  Version: V4.2 
  Comments: r_p and w_p -- will look first for 4.5 CC_ROOT and SYSTEM,   
                        then  ConfigPath defined for V3 systems 
						or as a last resort the default Windows/Winnt directory for dev ini files etc 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: RichardK
  Revised on 03/08/2007
  Version: V4.3
  Comments: Debug turns off redraw of list-box while updating it.  To stop flicker problems seen on
		systems accessed using Remote Desktop.
 ************************************/


#include "stdafx.h"
#define EXT
#include "PackagerAuto.h"

//
//  FUNCTION: WinMain()
//
//  PURPOSE: 
//
//  COMMENTS: main entry point for BNCS Driver "PackagerAuto"
//
//
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	try
	{
	MSG msg;
	HACCEL hAccelTable;
	char acTemp[256] = "", szLeft[256] = "", szRight[256] = "";
	char szDebug[256] = "";
	ssCompositeInstance = "";

	lstrcpyn(acTemp,lpCmdLine,255);

	// deterime system type
	getBNCS_File_Path();

	// MUST pass in parameter - a composite packager instance id- 
	// this check is done as windows shortcuts sometimes pass in whole target path and parameters sometimes
	char *pdest = strstr(acTemp, " ");
	if (pdest != NULL) {
		// space present - as string may include full path to driver
		wsprintf(szDebug, " \n PACKAGER -winmain- passed in=%s \n", acTemp);
		OutputDebugString(szDebug);
		if (SplitAString(acTemp, ' ', szLeft, szRight)) strcpy(acTemp, szRight);
		wsprintf(szDebug, " \n PACKAGER -winmain- after split  instance=%s \n", acTemp);
		OutputDebugString(szDebug);
		// now call xml parser to extract device for instance name
		ssCompositeInstance = bncs_string(acTemp);
		wsprintf(szDebug, " \n PACKAGER -winmain-2- composite instance=%s \n", acTemp);
		OutputDebugString(szDebug);
	}
	else {
		ssCompositeInstance = bncs_string(acTemp);
		wsprintf(szDebug, " \n PACKAGER -winmain-1- composite instance=%s \n", acTemp);
		OutputDebugString(szDebug);
	}

	if (ssCompositeInstance.length()==0) {
		OutputDebugString("PACKAGER -winmain- -ERROR- MISSING COMPOSITE INSTANCE - PACKAGER WILL NOT RUN \n");
	}

	// Initialize global strings
	wsprintf(szTitle, "BNCS Packager - %s", acTemp);
	//LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDS_SECTION, szAppName, MAX_LOADSTRING);
	LoadString(hInstance, IDC_APPCLASS, szWindowClass, MAX_LOADSTRING);

	MyRegisterClass(hInstance);

	BOOL bCont = TRUE;
	bCSIClientProcessing = FALSE;
	hCSIClientSemaphore = CreateSemaphore(NULL, 1, 1, NULL);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_PACKAGERAUTO);
		
	// Main message loop:
	//while (GetMessage(&msg, NULL, 0, 0)) 
	//{
	//		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
	//	{
	//		TranslateMessage(&msg);
	//		DispatchMessage(&msg);
	//	}
	//}
	do {

		bCont = GetMessage(&msg, NULL, 0, 0);
		if (bCont) {
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
			OutputDebugString("GetMessage returned -1 \n");

	} while (bCont);

	return msg.wParam;

	}
	catch (structured_exception const & e)
	{
		Debug("%s:WinMain() - thrown exception %x from %x", szAppName, e.what(), e.where());
		return 0;
		//exit(1);
	}
	catch (...)
	{
		Debug("%s:WinMain() - Unknown exception ", szAppName);
		return 0;
		//exit(1);
	}
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_REG);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCSTR)IDC_PACKAGERAUTO;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hWndMain = CreateWindow(szWindowClass, szTitle, WS_SYSMENU|WS_MINIMIZEBOX,
		CW_USEDEFAULT, 0, APP_WIDTH, APP_HEIGHT, NULL, NULL, hInstance, NULL);

	if (!hWndMain)
	{
		return FALSE;
	}

//#ifdef _DEBUG
ShowWindow(hWndMain,nCmdShow);
//#else
	// minimise window on startup now
	//ShowWindow(hWndMain, SW_MINIMIZE);
//#endif

	UpdateWindow(hWndMain);

	return TRUE;
}


///////////////////////////////////////////////////////////////////////////////
// generic method to split string into two halves around a specified character
//
BOOL SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight)
{
	int result = 0;
	char szInStr[MAX_AUTO_BUFFER_STRING] = "";
	strcpy(szInStr, szStr);
	strcpy(szLeft, "");
	strcpy(szRight, "");

	char *pdest = strchr(szInStr, cSplit);
	if (pdest != NULL) {
		result = pdest - szInStr + 1;
		if (result>0) {
			if (result == strlen(szInStr)) {
				strcpy(szLeft, szInStr);
			}
			else {
				strncpy(szLeft, szInStr, result - 1);
				szLeft[result - 1] = 0; // terminate
				strncpy(szRight, szInStr + result, strlen(szInStr) - result + 1);
			}
			return TRUE;
		}
		else {
			Debug("SplitAString pdest not null, BUT result 0 or less : %d", result);
		}
	}
	// error condition
	strcpy(szLeft, szInStr);
	return FALSE;
}




//
//  FUNCTION: AssertDebugSettings(HWND)
//
//  PURPOSE:  Sorts out window size and ini file settings for the two debug
//            controls - debug and hide.
//  
//	COMMENTS:
//	
void AssertDebugSettings(HWND hWnd)
{
	char szFileName[256];
	char szSection [MAX_LOADSTRING];
	LoadString(hInst, IDS_SECTION, szSection, MAX_LOADSTRING);
	sprintf(szFileName, "dev_%03d.ini", iPackager_router_main);

	if (iDebugFlag>0)
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_ON,MF_CHECKED);
		//w_p(szFileName, szSection, "DebugMode", "1");
	} 
	else
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_ON,MF_UNCHECKED);
		//w_p(szFileName, szSection, "DebugMode", "0");
	}

	RECT rctWindow;
	GetWindowRect(hWnd, &rctWindow);
	if (fDebugHide)
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, iChildHeight + 42, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_HIDE,MF_CHECKED);
	} 
	else
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, APP_HEIGHT, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_HIDE,MF_UNCHECKED);
	}
}


void ReloadChosenSourcePackageData(int iSrcPkg)
{
	char szRMData[MAX_AUTO_BUFFER_STRING] = "";
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 0, iSrcPkg));
	dbmPkgs.setName(iPackager_router_main, 0, iSrcPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 2, iSrcPkg));
	dbmPkgs.setName(iPackager_router_main, 2, iSrcPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 4, iSrcPkg));
	dbmPkgs.setName(iPackager_router_main, 4, iSrcPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 6, iSrcPkg));
	dbmPkgs.setName(iPackager_router_main, 6, iSrcPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 9, iSrcPkg));
	dbmPkgs.setName(iPackager_router_main, 9, iSrcPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 11, iSrcPkg));
	dbmPkgs.setName(iPackager_router_main, 11, iSrcPkg, szRMData, false);
	ReloadASourcePackageData(iSrcPkg, TRUE);
	DisplaySourcePackageData();
}


void ReloadChosenDestinationPackageData(int iDstPkg)
{
	char szRMData[MAX_AUTO_BUFFER_STRING] = "";
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 1, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 1, iDstPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 3, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 3, iDstPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 5, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 5, iDstPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 7, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 7, iDstPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 15, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 15, iDstPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 16, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 16, iDstPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 17, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 17, iDstPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 18, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 18, iDstPkg, szRMData, false);
	strcpy_s(szRMData, ExtractNamefromDevIniFile(iPackager_router_main, 19, iDstPkg));
	dbmPkgs.setName(iPackager_router_main, 19, iDstPkg, szRMData, false);
	ReloadADestinationPackageData(iDstPkg, TRUE);
	DisplayDestinationPackageData(FALSE);

}

void RequestOtherPackagerDynamicConferenceData()
{
	// request from other pkgr dyn conf data - list of confs deemed to be assigned on remote (NHN) packager
	int iRegionBase = 0;
	if (iAutomaticLocation == LDC_LOCALE_RING) iRegionBase = CTRL_INFO_PKGR_LDC_BASE;
	else if (iAutomaticLocation == NHN_LOCALE_RING) iRegionBase = CTRL_INFO_PKGR_NHN_BASE;
	//
	char szCommand[MAX_AUTO_BUFFER_STRING] = "";
	wsprintf(szCommand, "IW %d 'REQUEST' %d", iAutomatics_CtrlInfo_Local, iRegionBase + CTRL_INFO_CONFPOOL);
	AddCommandToQue(szCommand, INFODRVCOMMAND, iAutomatics_CtrlInfo_Local, 0, 0);
	ProcessNextCommand(1);
}

void RequestOtherPackagerDynamicIFBData()
{
	int iRegionBase = 0;
	if (iAutomaticLocation == LDC_LOCALE_RING) iRegionBase = CTRL_INFO_PKGR_LDC_BASE;
	else if (iAutomaticLocation == NHN_LOCALE_RING) iRegionBase = CTRL_INFO_PKGR_NHN_BASE;
	//
	char szCommand[MAX_AUTO_BUFFER_STRING] = "";
	wsprintf(szCommand, "IW %d 'REQUEST' %d", iAutomatics_CtrlInfo_Local, iRegionBase + CTRL_INFO_IFBPOOL);
	AddCommandToQue(szCommand, INFODRVCOMMAND, iAutomatics_CtrlInfo_Local, 0, 0);
	ProcessNextCommand(1);
}


void SendOutTicTocStatus()
{
	if (iAutomatics_CtrlInfo_Local > 0) {
		char szCommand[MAX_AUTO_BUFFER_STRING] = "";

		int iRegionBase = 0;
		bncs_string ssRegRevtx = "", ssRegRevrx = "";
		if (iAutomaticLocation == LDC_LOCALE_RING) {
			iRegionBase = CTRL_INFO_PKGR_LDC_BASE;
			ssRegRevtx = ssLDC_TX_Revertive;
			ssRegRevrx = ssLDC_RX_Revertive;
		}
		else if (iAutomaticLocation == NHN_LOCALE_RING) {
			iRegionBase = CTRL_INFO_PKGR_NHN_BASE;
			ssRegRevtx = ssNHN_TX_Revertive;
			ssRegRevrx = ssNHN_RX_Revertive;
		}

		if (iOverallTXRXModeStatus == IFMODE_TXRX) {
			if (ssRegRevtx.find("TIC") >= 0)
				wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, "2 TOC PKGR TX", iRegionBase + CTRL_INFO_TXRX_TIKTOK);
			else
				wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, "1 TIC PKGR TX", iRegionBase + CTRL_INFO_TXRX_TIKTOK);
		}
		else {
			if (ssRegRevrx.find("TIC") >= 0)
				wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, "2 TOC PKGR RX", iRegionBase + CTRL_INFO_RXONLY_TIKTOK);
			else
				wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, "1 TIC PKGR RX", iRegionBase + CTRL_INFO_RXONLY_TIKTOK);
		}
		if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
	}
}

void CalculateAndReportControlInfoStatus()
{
	// deterimine changes been received from  autos control infodriver for registered slots
	int iPrevCtrlStat = iControlStatus;
	iControlStatus = 0;
	// cumulative sum provides for unique parsing
	if (iLDC_RXONLY_TicTocStatus > 0) iControlStatus = iControlStatus + 1;
	if (iLDC_TXRX_TicTocStatus > 0) iControlStatus = iControlStatus + 2;
	if (iNHN_RXONLY_TicTocStatus > 0) iControlStatus = iControlStatus + 4;
	if (iNHN_TXRX_TicTocStatus > 0) iControlStatus = iControlStatus + 8;
	SetDlgItemInt(hWndDlg, IDC_AUTO_CTRLSTAT2, iControlStatus, TRUE);
		// report state of sum
	switch (iControlStatus) {
		case 0: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "NO LDC -- NO NHN"); break;
		case 1: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC RX -- NO NHN"); break;
		case 2: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC TX -- NO NHN"); break;
		case 3: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC OK -- NO NHN"); break;
		case 4: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "NO LDC -- NHN RX"); break;
		case 5: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC RX -- NHN RX"); break;
		case 6: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC TX -- NHN RK"); break;
		case 7: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC OK -- NHN RX"); break;
		case 8: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "NO LDC -- NHN TX"); break;
		case 9: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC RX -- NHN TX"); break;
		case 10: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC TX -- NHN TX"); break;
		case 11: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC OK -- NHN TX"); break;
		case 12: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "NO LDC -- NHN OK"); break;
		case 13: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC RX -- NHN OK"); break;
		case 14: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC TX -- NHN OK"); break;
		case 15: SetDlgItemText(hWndDlg, IDC_CTRL_STATUS, "LDC OK -- NHN OK"); break;	
	}
	// reset 4 site status vars - as they need to be set / change by the arriving revs
	iLDC_TXRX_TicTocStatus = 0;
	iNHN_TXRX_TicTocStatus = 0;
	iLDC_RXONLY_TicTocStatus = 0;
	iNHN_RXONLY_TicTocStatus = 0;

	// report on shared resources
	BOOL bWasDualSites = FALSE, bNowDualSites = FALSE;
	if ((iPrevCtrlStat == 10) || (iPrevCtrlStat == 11) || (iPrevCtrlStat == 14) || (iPrevCtrlStat == 15)) bWasDualSites = TRUE;
	if ((iControlStatus == 10) || (iControlStatus == 11) || (iControlStatus == 14) || (iControlStatus == 15)) bNowDualSites = TRUE;
	if ((!bWasDualSites) && (bNowDualSites) && (iAutomatics_CtrlInfo_Local>0)) {
		// change in state of autos pairs to both being present
		// special poll for allocated dyn confs from pool at OTHER site 
		// write REQUEST to other's slot ( 14 or 24 )
		RequestOtherPackagerDynamicConferenceData();
		// special poll for allocated ifbs from pools at OTHER site 16 or 26 
		RequestOtherPackagerDynamicIFBData();
		//
		iCtrlRequestRemoteCounter = 50;
	}
	
}




//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  
//	COMMENTS:
//	
//	WM_COMMAND	- process the application menu
//  WM_DESTROY	- post a quit message and return
//	
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	BOOL bContinue = TRUE;
	
	switch (message) 	{
		case WM_CREATE:
			return InitApp(hWnd);
		break;
		
		case WM_TIMER:

			switch (wParam) {
			case STARTUP_TIMER_ID: // go thru start up now

				if (iStartupCounter == 0) {
					// stop timer as it takes a while to load all data in
					KillTimer(hWnd, STARTUP_TIMER_ID);
					iLabelAutoStatus = 1;
					bAutomaticStarting = TRUE;
					AssertDebugSettings(hWnd);

					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Loading composite inst");
					if (LoadPackagerCompositeInstances()) {

						if (fLog) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_CHECKED);
						if (fDbgView) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_CHECKED);
						if (bShowAllDebugMessages) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_CHECKED);

						if (ConnectAllInfodrivers()) {

							SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Loading Data...very busy");

							CheckAndSetInfoResilience();
							// load all required data for automatic
							if (LoadAllInitialisationData()) {

								// register with router, panel infodrivers etc
								SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Registering...");
								AutomaticRegistrations();

								// restart start timer for polling devices
								iOneSecondPollTimerId = SetTimer(hWnd, STARTUP_TIMER_ID, 1000, NULL);
								bOneSecondPollTimerRunning = TRUE;
								iStartupCounter = 1;
								SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Polling...");
								bContinue = TRUE;

							}
							else {
								bContinue = FALSE;
							}
						}
						else {
							bContinue = FALSE;
						}
					}
					else {
						bContinue = FALSE;
					}

					if (!bContinue) {
						// set Auto in NONRUNNING STATE
						Debug("**** PACKAGER AUTO -- NOT RUNNING - COMPOSITE INSTANCE ERROR OR  ****");
						Debug("**** PACKAGER AUTO -- NOT RUNNING -    INFODRIVER OR INIT ERRORS ****");
						iLabelAutoStatus = 2;
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "INIT ERRORS");
						ForceAutoIntoRXOnlyMode();
						iOverallTXRXModeStatus = IFMODE_RXONLY;
					}
				}
				else { // startupcounter >0

					// poll routers etc and get revertives - on a staggered poll 
					iLabelAutoStatus = 1;
					// turn all debug off during initial polling
					bShowAllDebugMessages = FALSE;
					PollAutomaticDrivers(iStartupCounter);
					iStartupCounter++;

					if (iStartupCounter == 60) {
						// now poll for tic-tok status - and then as a consequence shared resource data from other pkgr ??? 
						if ((iAutomatics_CtrlInfo_Local > 0) && (iAutomatics_CtrlInfo_Remote > 0)) {
							char szCommand[MAX_AUTO_BUFFER_STRING] = "";
							int iLocalBase = 0, iOtherBase = 0;
							if (iAutomaticLocation == LDC_LOCALE_RING) {
								iLocalBase = CTRL_INFO_PKGR_LDC_BASE;
								iOtherBase = CTRL_INFO_PKGR_NHN_BASE;
							}
							else if (iAutomaticLocation == NHN_LOCALE_RING) {
								iLocalBase = CTRL_INFO_PKGR_NHN_BASE;
								iOtherBase = CTRL_INFO_PKGR_LDC_BASE;
							}
							// start tictoc timer
							SetTimer(hWnd, TIKTOK_ALIVE_TIMER_ID, 15000, NULL);   // every 15 secs send out a heartbeat
							iCtrlStatusCalcCounter = 5;
							iCtrlRequestRemoteCounter = 50;
							SendOutTicTocStatus();
							wsprintf(szCommand, "IP %d %d %d", iAutomatics_CtrlInfo_Local, iLocalBase + CTRL_INFO_TXRX_TIKTOK, iLocalBase + CTRL_INFO_RXONLY_TIKTOK);
							if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
							wsprintf(szCommand, "IP %d %d %d", iAutomatics_CtrlInfo_Remote, iOtherBase + CTRL_INFO_TXRX_TIKTOK, iOtherBase + CTRL_INFO_RXONLY_TIKTOK);
							if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
							//
							if (iOverallTXRXModeStatus == IFMODE_TXRX) {
								RequestOtherPackagerDynamicConferenceData();
								RequestOtherPackagerDynamicIFBData();
							}
							//
						}

					}

					if (iStartupCounter > 70) {
						// 70secs - should be time enough to get all revs now
						KillTimer(hWnd, STARTUP_TIMER_ID);
						if (iLabelAutoStatus == 1) iLabelAutoStatus = 0; // ie no init errors / issues

						bAutomaticStarting = FALSE;
						//bShowAllDebugMessages = TRUE;
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "1st Processing");

						// FIRST PASS AT PROCESSING 
						Debug("1st Processing pass for startup");
						// determine current state of packages etc
						ProcessFirstPass_Logic();
						// now deal with command que
						ProcessNextCommand(BNCS_COMMAND_RATE);

						bShowAllDebugMessages = FALSE;   // turn off all messages now
						CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_UNCHECKED);

						iOneSecondPollTimerId = SetTimer(hWnd, ONE_SECOND_TIMER_ID, ONE_SECOND_TIMER_INTERVAL, NULL);
						Debug("One Second  timer start returned %d ", iOneSecondPollTimerId);

						if (iLabelAutoStatus == 3)
							SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Running: but INIT Errors");
						else
							SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Running OK");
					}

				}
				break;


			case ONE_SECOND_TIMER_ID: // used for housekeeping and cyclic polling of hardware 

				// check on infodriver status
				if (iNextResilienceProcessing > 0) {
					iNextResilienceProcessing--;
					if (iNextResilienceProcessing <= 0) {
						CheckAndSetInfoResilience();
					}
				}

				// 2. check if recently failed over
				if (iOverallModeChangedOver > 0) {
					iOverallModeChangedOver--;
					if (iOverallModeChangedOver <= 0) {
						if (bShowAllDebugMessages) Debug("RestartCSI_NI-NO messages called");
						Restart_CSI_NI_NO_Messages();
					}
				}

				// any other actions required during housekeeping timer -- process any pending commands if command timer not already running or paused
				if (iPauseCommandProcessing > 0) {
					iPauseCommandProcessing--;
					if (iPauseCommandProcessing <= 0) {
						Debug("Main- COMMAND PROCESSING RESUMING - cmd que size is %d ", qCommands.size());
						char szval[64] = "";
						wsprintf(szval, "q max = %d ", iHighestCommandQueue);
						SetDlgItemText(hWndDlg, IDC_CMD_QUEUE2, szval);
						eiInfoDrivers[1].updateslot(4096, szval);
						ProcessNextCommand(BNCS_COMMAND_RATE);
					}
					else {
						if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
					}
				}
				UpdateCounters();

				if (iCtrlStatusCalcCounter > 0) {
					iCtrlStatusCalcCounter--;
					if (iCtrlStatusCalcCounter <= 0) {
						CalculateAndReportControlInfoStatus();
						iCtrlStatusCalcCounter = 33;
						iCtrlRequestRemoteCounter--;
						if (iCtrlRequestRemoteCounter <= 0) {
							if (iOverallTXRXModeStatus == IFMODE_TXRX) {
								RequestOtherPackagerDynamicConferenceData();
								RequestOtherPackagerDynamicIFBData();
							}
							iCtrlRequestRemoteCounter = 50;
						}
					}
				}

				if (iSDIStatusCalcCounter > 0) {
					iSDIStatusCalcCounter--;
					Debug("Main - Processing DestPkg Status calc : %d : %d entries", iSDIStatusCalcCounter, ssl_DestPkgs_For_SDIStatus_Calc.count());
					if (iSDIStatusCalcCounter <= 0) {
						ProcessListOfDestinationPackageStatusCalc();
						iSDIStatusCalcCounter = 0;
					}
					if (iSDIStatusCalcCounter < 0) iSDIStatusCalcCounter = 0;
				}
				break;

			case COMMAND_TIMER_ID:
				// process next command in command queue if any 
				if (iPauseCommandProcessing <= 0) ProcessNextCommand(BNCS_COMMAND_RATE);
				if (qCommands.size() == 0) { // queue empty so stop timer
					if (bNextCommandTimerRunning) KillTimer(hWndMain, COMMAND_TIMER_ID);
					bNextCommandTimerRunning = FALSE;
				}
				break;

			case REVERTIVE_TIMER_ID:
			{
				// process revertive queue if any 
				int iRevsToProc = 500;
				if (bAutomaticStarting) iRevsToProc = 1500;
				ProcessNextRevertive(iRevsToProc);
				if (qRevertives.size() == 0) { // queue empty so stop timer
					if (bNextRevertiveTimerRunning) KillTimer(hWndMain, REVERTIVE_TIMER_ID);
					bNextRevertiveTimerRunning = FALSE;
				}
			}
			break;

			case DATABASE_TIMER_ID:
			{
				// reload listed packages and process - dest first then sources
				KillTimer(hWndMain, DATABASE_TIMER_ID);
				bRMDatabaseTimerRunning = FALSE;
				for (int iisp = 0; iisp < ssl_RM_SrcPkg_Changes.count(); iisp++) {
					int iIndexNum = ssl_RM_SrcPkg_Changes[iisp].toInt();
					ReloadASourcePackageData(iIndexNum, TRUE);
				}
				for (int iiap = 0; iiap < ssl_RM_AudioPkg_Changes.count(); iiap++) {
					int iIndexNum = ssl_RM_AudioPkg_Changes[iiap].toInt();
					ReloadAnAudioPackageData(iIndexNum, TRUE);
				}
				for (int iie = 0; iie < ssl_RM_DestPkg_Changes.count(); iie++) {
					int iIndexNum = ssl_RM_DestPkg_Changes[iie].toInt();
					Debug("Main dbtimer processing dest pkg %d ", iIndexNum);
					ReloadADestinationPackageData(iIndexNum, TRUE);
				}
				ssl_RMChanges.clear();
				ssl_RM_SrcPkg_Changes.clear();
				ssl_RM_DestPkg_Changes.clear();
				ssl_RM_AudioPkg_Changes.clear();
				Debug(" RMtimer - clearing list ");
			}
			break;

			case TIKTOK_ALIVE_TIMER_ID:
				// depending on location and if tx or rx - determines slot to update
				SendOutTicTocStatus();
				break;

			case SHAREDRESOURCE_TIMER_ID:
			{
				// timer to send out data about shared resources after a REQUEST command from other site
				if ((iAutomatics_CtrlInfo_Local > 0) && (ecCSIClient)) {
					// conf data
					int iRegionBase = 0;
					char szCommand[MAX_AUTO_BUFFER_STRING] = "";
					if (iAutomaticLocation == LDC_LOCALE_RING) iRegionBase = CTRL_INFO_PKGR_LDC_BASE;
					if (iAutomaticLocation == NHN_LOCALE_RING) iRegionBase = CTRL_INFO_PKGR_NHN_BASE;
					if (sslSendingOutDynConfData.count() > 0) {
						wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, LPCSTR(sslSendingOutDynConfData[0]), iRegionBase + CTRL_INFO_CONFPOOL);
						ecCSIClient->txinfocmd(szCommand);
						sslSendingOutDynConfData.deleteItem(0); // delete item just sent
					}
					// any ifb data 
					if (sslSendingOutDynIFBData.count() > 0) {
						wsprintf(szCommand, "IW %d '%s' %d", iAutomatics_CtrlInfo_Local, LPCSTR(sslSendingOutDynIFBData[0]), iRegionBase + CTRL_INFO_IFBPOOL);
						ecCSIClient->txinfocmd(szCommand);
						sslSendingOutDynIFBData.deleteItem(0); // delete item just sent
					}
					// if both ssls now empty can stop timer;
					if (sslSendingOutDynConfData.count() == 0) bSendingOutDynConfData = FALSE;
					if (sslSendingOutDynIFBData.count() == 0) bSendingOutDynIFBData = FALSE;
					if ((sslSendingOutDynConfData.count() == 0) && (sslSendingOutDynIFBData.count() == 0)) 	KillTimer(hWndMain, SHAREDRESOURCE_TIMER_ID);
				}
			}
			break;

			case FORCETXRX_TIMER_ID:
				Debug("ForceTxrx timer");
				KillTimer(hWndMain, FORCETXRX_TIMER_ID);
				ForceAutoIntoTXRXMode();
				iOverallTXRXModeStatus = IFMODE_TXRX;
			break;

			}  // switch(wparam) on timer types

		break; // wmtimer
		
	case WM_SIZE: // if the main window is resized, resize the listbox too
		//Don't resize window with child dialog
		break;
		
	case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		// Parse the menu selections:
		switch (wmId)	{
			case IDM_ABOUT:
				DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
			
			case IDM_EXIT:
				DestroyWindow(hWnd);
			break;
			
			case ID_DEBUG_CLEAR:
				SendMessage(hWndList,LB_RESETCONTENT,0,0L);
			break;
			
			case ID_DEBUG_ON:
				if (iDebugFlag)
					iDebugFlag = 0;
				else	{
					iDebugFlag = 1;
					fDebugHide=FALSE;
				}
				AssertDebugSettings(hWnd);
			break;
			
			case ID_DEBUG_HIDE:
				if (fDebugHide)	{
					fDebugHide=FALSE;
					iDebugFlag = 1;
				} 
				else	{
					fDebugHide=TRUE;
					iDebugFlag = 0;
				}
				AssertDebugSettings(hWnd);
			break;

			case  ID_DEBUG_SHOWALLMESSAGES : 
				if (bShowAllDebugMessages) {
					Debug( "-- turning off most debug messages");
					bShowAllDebugMessages = FALSE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_UNCHECKED);
				}
				else {
					Debug( "++ Turning on all debug messages");
					bShowAllDebugMessages = TRUE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_CHECKED);
				}
			break;
				
			case ID_DEBUG_LOGTOFILE:
			{
				if (fLog) {
					Debug("-- cease logging debug messages to file");
					fLog = FALSE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_UNCHECKED);
				}
				else {
					fLog = TRUE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_CHECKED);
					Debug("++ commence logging debug messages to file");
				}
			}
			break;

			case ID_DEBUG_LOGTODBGVIEW:
				if (fDbgView) {
					Debug("-- cease sending messages to dbgview app");
					fDbgView = FALSE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_UNCHECKED);
				}
				else {
					fDbgView = TRUE;
					CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_CHECKED);
					Debug("++ commence sending messages to dbgview app");
				}
			break;

			case ID_OPTIONS_FORCETXRX:
				Debug( "Forcing Automatic into TXRX Mode" );
				ForceAutoIntoTXRXMode();
				iOverallTXRXModeStatus = IFMODE_TXRX;
				break;
				
			case ID_OPTIONS_FORCERXONLY :
				Debug( " Trying to force driver into RXONLY mode");
				Debug( " *** Now force the other Automatic into TXRX mode to complete operation ***");
				ForceAutoIntoRXOnlyMode();
				iOverallTXRXModeStatus = IFMODE_RXONLY;
				break;

			case ID_OPTIONS_RELOADCHOSENSRCEPKG:
				if (iChosenSrcPkg > 0) {
					Debug(" reloading data for source package %d", iChosenSrcPkg);
					ReloadChosenSourcePackageData(iChosenSrcPkg);
				}
				break;

			case ID_OPTIONS_RELOADCHOSENDESTPKG:
				if (iChosenDestPkg > 0) {
					Debug(" reloading data for destination package %d", iChosenDestPkg);
					ReloadChosenDestinationPackageData(iChosenDestPkg);
				}
				break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		} // switch (wmId)
	break; // case WMCommand

	case WM_DESTROY:
		CloseApp(hWnd);
		PostQuitMessage(0);
	break;
		
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}


//
//  FUNCTION: DlgProcChild()
//
//  PURPOSE: Message handler for child dialog.
//
//  COMMENTS: Displays the interface status
//
// 
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId    = LOWORD(wParam); 
	int wmEvent = HIWORD(wParam); 
	int iLen=0;
	char szAddr[MAX_AUTO_BUFFER_STRING] = "";
	char szCmd[MAX_AUTO_BUFFER_STRING] = "";

	HWND hButton = (HWND)lParam;
	LONG idButton=GetDlgCtrlID(hButton);

	//char szdata[256];
	//wsprintf( szdata, "DlgChild message %d, BUTTON %d \n", message, idButton);
	//OutputDebugString( szdata );
	switch (message)	{

		case WM_COMMAND :
			switch (wmId)	{

				case IDC_LISTINFOS:		// list of infodrivers - display details
					DisplaySelectedInfodriver();
				break;

				case IDC_TAKE:
				case IDC_TAKE1:
				case IDC_TAKE2:
				case IDC_TAKE3:
				case IDC_TAKE4:
				{
								 // take button
								 if (wmEvent == 0) {
									 iLen = GetDlgItemText(hDlg, IDC_SRCE_INDEX, szAddr, 16);
									 int iCheckSrceNumber = atoi(szAddr);
									 iLen = GetDlgItemText(hDlg, IDC_DEST_INDEX, szAddr, 16);
									 int iCheckDestNumber = atoi(szAddr);
									 //Debug("TakeButton -  srce %d dest %d", iCheckSrceNumber, iCheckDestNumber );
									 if ((iCheckSrceNumber > 0) && (iCheckDestNumber > 0) && (iCheckDestNumber <=iNumberOfPackages) && (iCheckSrceNumber <= iPackagerParkSrcePkg)) {
										 //
										 iChosenSrcPkg = iCheckSrceNumber;
										 DisplaySourcePackageData();
										 iChosenDestPkg = iCheckDestNumber;
										 DisplayDestinationPackageData(FALSE); 
										 //
										 char szString[128] = "";
										 wsprintf(szString, "index=%d", iCheckSrceNumber);                                                                    // routing by dest video tag as default
										 if (wmId == IDC_TAKE1) wsprintf(szString, "index=%d,v_level=1", iCheckSrceNumber);  // take - forcing level 1 routing
										 if (wmId == IDC_TAKE2) wsprintf(szString, "index=%d,v_level=2", iCheckSrceNumber);
										 if (wmId == IDC_TAKE3) wsprintf(szString, "index=%d,v_level=3", iCheckSrceNumber);
										 if (wmId == IDC_TAKE4) wsprintf(szString, "index=%d,v_level=4", iCheckSrceNumber);
										 SendPackagerBNCSCommand(iCheckDestNumber, AUTO_PACK_ROUTE_INFO, szString);
										 Debug("TakeButton (lvl %d) -  msg %s", wmId-1026, szString );
									 }
									 else
										 Debug("TakeButton - no selected srce or dest pkg");
								 }
								 else
									 Debug("TAKE button event other than 0, event %d  ", wmEvent);
				}
					break;

				case IDC_SRCPKG:
				{
									 // goto source
									 iLen = GetDlgItemText(hDlg, IDC_SRCE_INDEX, szAddr, 16);
									 int iCheckSrceNumber = atoi(szAddr);
									 if ((iCheckSrceNumber>0) && (iCheckSrceNumber <= iPackagerParkSrcePkg)) {
										 iChosenSrcPkg = iCheckSrceNumber;
										 DisplaySourcePackageData();
									 }
									 else
										 SetDlgItemInt(hWndDlg, IDC_SRCE_INDEX, iChosenSrcPkg, TRUE);
				}
					break;

				case IDC_DESTPKG:
				{
									 // goto source
									 iLen = GetDlgItemText(hDlg, IDC_DEST_INDEX, szAddr, 16);
									 int iCheckDestNumber = atoi(szAddr);
									 if ((iCheckDestNumber>0) && (iCheckDestNumber <= iNumberOfPackages)) {
										 iChosenDestPkg = iCheckDestNumber;
										 DisplayDestinationPackageData(TRUE); // setting to true means repeated selection on gui can clear exp routing counter
									 }
									 else
										 SetDlgItemInt(hWndDlg, IDC_DEST_INDEX, iChosenSrcPkg, TRUE);
				}
					break;

				case IDC_SRCPKG2:
				{
					// display audio source pkackage
					iLen = GetDlgItemText(hDlg, IDC_SRCE_INDEX2, szAddr, 16);
					int iCheckSrceNumber = atoi(szAddr);
					if ((iCheckSrceNumber>0) && (iCheckSrceNumber <=iNumberOfPackages)) {
						iChosenAudioSrcPkg = iCheckSrceNumber;
						DisplayAudioSourcePackageData();
					}
					else
						SetDlgItemInt(hWndDlg, IDC_SRCE_INDEX2, iChosenAudioSrcPkg, TRUE);
				}
				break;

			}
		break;
	
		case WM_INITDIALOG: 
			//This will colour the background of the child dialog on startup to defined colour
			// brushes are defined in INIT_APP function below
			HBr = hbrTurq;
			return (LRESULT)HBr;
			//HBr = CreateSolidBrush( COL_TURQ );
			//return (LRESULT)HBr;
		break;

		case WM_CTLCOLORDLG:
			// colour all diag at startup
			SetTextColor((HDC) wParam,COL_DK_BLUE);
			SetBkColor((HDC) wParam, COL_TURQ);
			HBr = hbrTurq;
			return (LRESULT)HBr;
		break;

		case WM_CTLCOLORSTATIC:
			 // to colour labels --  this will be called on startup of the dialog by windows
			 // this section will be called every time you do a SetDlgItemText() command for a given label
			 // by testing the value of global vars this can be used to colour labels for Comms OK/Fail, TXRX/Rxonly etc
			switch (idButton)	{	
				case IDC_INFO_STATUS2:
				{
					if (iLabelInfoStatus == IFMODE_TXRX) {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_DK_GRN);
						HBr = hbrDarkGreen;
					}
					else if (iLabelInfoStatus == IFMODE_RXONLY) {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_LT_RED);
						HBr = hbrLightRed;
					}
					else {
						SetTextColor((HDC)wParam, COL_DK_BLUE);
						SetBkColor((HDC)wParam, COL_WHITE);
						HBr = hbrWhite;
					}
				}
				break;

				case IDC_AUTO_STATUS:
					if (iLabelAutoStatus==1) {
						SetTextColor((HDC) wParam,COL_DK_BLUE);
						SetBkColor((HDC) wParam, COL_YELLOW);
						HBr = hbrYellow;
					}
					else if (iLabelAutoStatus==2) {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_LT_RED);
						HBr = hbrLightRed;
					}
					else if (iLabelAutoStatus==3) {
						SetTextColor((HDC) wParam,COL_BLACK);
						SetBkColor((HDC) wParam, COL_LT_ORANGE);
						HBr = hbrLightOrange;
					}
					else {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_DK_GRN);
						HBr = hbrDarkGreen;
					}
				break;

				case IDC_CTRL_STATUS:
					if (iControlStatus == 0) {
						// no control data at all
						SetTextColor((HDC)wParam, COL_YELLOW);
						SetBkColor((HDC)wParam, COL_BLACK);
						HBr = hbrBlack;
					}
					else if ((iControlStatus == 1) || (iControlStatus == 4) || (iControlStatus == 5) || (iControlStatus == 7) || 
						         (iControlStatus == 9) || (iControlStatus == 12) || (iControlStatus == 13)) {
						// some NO, some RX
						SetTextColor((HDC)wParam, COL_WHITE);
						SetBkColor((HDC)wParam, COL_LT_RED);
						HBr = hbrLightRed;
					}
					else if ((iControlStatus == 2) || (iControlStatus == 3) || (iControlStatus == 6) || (iControlStatus == 8)) {
						// ldc tx or both  and no nhn
						SetTextColor((HDC)wParam, COL_BLACK);
						SetBkColor((HDC)wParam, COL_LT_ORANGE);
						HBr = hbrLightOrange;
					}
					else if ((iControlStatus == 10) || (iControlStatus == 11) || (iControlStatus == 14)) {
						// ldc and nhn both tx - some rx too
						// states 10,11,14, and 15 all mean full resource sharing can be used - as both tx are running as a minimum
						SetTextColor((HDC)wParam, COL_DK_BLUE);
						SetBkColor((HDC)wParam, COL_YELLOW);
						HBr = hbrYellow;
					}
					else {
						// all valid control data from both sites -- icontrolstatus = 15 
						SetTextColor((HDC)wParam, COL_WHITE);
						SetBkColor((HDC)wParam, COL_DK_GRN);
						HBr = hbrDarkGreen;
					}
				break;

				case IDC_VERSION: case IDC_IDRX: case IDC_IDTX: case IDC_AUTO_SDIRTR: case IDC_CMD_QUEUE1:
				case IDC_IFBPOOL1: case IDC_IFBPOOL2: case IDC_IFBPOOL3: case IDC_IFBPOOL4: case IDC_DPKG_TOPMMQS:
				case IDC_AUTO_INFNUM1: case IDC_AUTO_RIEDELS: case IDC_CONFPOOL: case IDC_DPKG_ANC: case IDC_DPKG_LOCKED:
				case IDC_INFO_STATUS1: case IDC_AUTO_VIRTUALS: case IDC_AUTO_RINGMSTR: case IDC_DPKG_ISSUES: case IDC_APKG_REVENA:
				case IDC_SPKG_NAME4: case IDC_SPKG_NAME5: case IDC_DPKG_DATA1: case IDC_DPKG_DATA2: case IDC_AUTO_AREA:
				case IDC_SPKG_NAME: case IDC_SPKG_NAME2: case IDC_AUTO_DBTAGS: case IDC_DPKG_DATA3:
				case IDC_SPKG_VIDEO1: case IDC_SPKG_VIDEO2: case IDC_SPKG_VIDEO3: case IDC_SPKG_VIDEO4: case IDC_DPKG_REVVIS:
				case IDC_SPKG_AUDIO1: case IDC_SPKG_AUDIO2: case IDC_SPKG_AUDIO3: case IDC_SPKG_AUDIO4: case IDC_DPKG_VIDEO:
				case IDC_SPKG_IFB1: case IDC_SPKG_IFB2: case IDC_SPKG_IFB3: case IDC_SPKG_IFB4: case IDC_SPKG_4W1: case IDC_SPKG_4W2:
				case IDC_SPKG_ROUTED: case IDC_SPKG_ROUTED2: case IDC_SPKG_AUDIOPKGS: case IDC_SPKG_NAME3 : case IDC_AUTO_CTRLINFO:
				case IDC_SPEV_NAME: case IDC_SPEV_DATA1: case IDC_SPEV_DATA2: case IDC_SPEV_DATA3: case IDC_SPEV_DATA4:
				case IDC_SPEV_DATA5: case IDC_SPEV_DATA6: case IDC_AUDIO_NAME1: case IDC_AUDIO_NAME2: case IDC_AUDIO_DATA1:
				case IDC_AUDIO_DATA2: case IDC_AUDIO_DATA3: case IDC_AUDIO_DATA4: case IDC_AUDIO_TOMSP:  case IDC_DPKG_VISFROM:
				case IDC_DPKG_TAG1: case IDC_SPKG_TAG1:case IDC_SPKG_TAG2:case IDC_SPKG_TAG3:case IDC_SPKG_TAG4: case IDC_AUTO_CTRLSTAT2:
				case IDC_DPKG_PARKSCE: case IDC_DPKG_NAME: case IDC_DPKG_NAME2: case IDC_DPKG_VIDMODE: case IDC_DPKG_VIDEO2:
				case IDC_DPKG_IFB1: case IDC_DPKG_IFB2: case IDC_DPKG_AUDIOPAIRS: case IDC_DPKG_AUDIORTN: case IDC_DPKG_AUDFROM:
				case IDC_DPKG_ROUTED: case IDC_DPKG_TRACED: case IDC_DPKG_SLAVEINDX: case IDC_DPKG_AUDIO2: case IDC_DPKG_PARKPKG:
					SetTextColor((HDC) wParam,COL_DK_BLUE);	 
					SetBkColor((HDC) wParam, COL_WHITE);	
					HBr = hbrWhite;
				break;
				
				case IDD_STATUS : case IDC_STATIC:
					SetTextColor((HDC) wParam,COL_DK_BLUE);
					SetBkColor((HDC) wParam, COL_TURQ);
					HBr = hbrTurq;
				break;

			default:
					SetTextColor((HDC) wParam,COL_DK_BLUE);
					SetBkColor((HDC) wParam, COL_TURQ);
					HBr = hbrTurq;
			}
			return (LRESULT)HBr;
		break;
	
	default:
		//Debug( "dlg child def");
		break;
	}
	
    return FALSE;


}

//
//  FUNCTION: Version Info()
//
void VersionInfo() {
	LPBYTE   abData;
	DWORD  handle;
	DWORD  dwSize;
	LPBYTE lpBuffer;
	LPSTR szName, szModFileName, szBuf;
	char acBuf[16];
	#define	PATH_LENGTH	256
			
	szName=(LPSTR)malloc(PATH_LENGTH);
	szModFileName=(LPSTR)malloc(PATH_LENGTH);
	szBuf=(LPSTR)malloc(PATH_LENGTH);

	/*get version info*/

	GetModuleFileName(hInst,szModFileName,PATH_LENGTH);

	dwSize = GetFileVersionInfoSize(szModFileName, &handle);
	abData=(LPBYTE)malloc(dwSize);
	GetFileVersionInfo(szModFileName, handle, dwSize, abData);
	if (dwSize) {
		/* get country translation */
		VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
		/* make country code */
		wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
		if (dwSize!=0) {
			/* get a versioninfo file version number */
			wsprintf(szName,"\\StringFileInfo\\%s\\PRODUCTVERSION",(LPSTR)acBuf);
			VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
		}
		/* copy version number from byte buffer to a string buffer */
		lstrcpyn(szModFileName,(LPSTR)lpBuffer,(int)dwSize);
		/* copy to the dialog static text box */
		wsprintf(szBuf,"%s",szModFileName);
		// replace any commas with full stops
		for (int c=0; c<int(strlen(szBuf)); c++)
			if (szBuf[c]==44 ) szBuf[c]=46; 
		SetDlgItemText(hWndDlg, IDC_VERSION, szBuf);
	}			
	free((PVOID)abData);
	free((PVOID)szName);
	free((PVOID)szModFileName);
	free((PVOID)szBuf);
}



//
//  FUNCTION: About()
//
//  PURPOSE: Message handler for about box.
//
//  COMMENTS: Displays the compile date info
//				and used to display version info from
//				a version resource
//
// 
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
	case WM_INITDIALOG:
		{
			LPBYTE   abData;
			DWORD  handle;
			DWORD  dwSize;
			LPBYTE lpBuffer;
			LPSTR szName, szModFileName, szBuf;
			char acBuf[16];
			#define	PATH_LENGTH	256
			
			szName=(LPSTR)malloc(PATH_LENGTH);
			szModFileName=(LPSTR)malloc(PATH_LENGTH);
			szBuf=(LPSTR)malloc(PATH_LENGTH);
			
			/*get version info*/
			
			GetModuleFileName(hInst,szModFileName,PATH_LENGTH);
			
			dwSize = GetFileVersionInfoSize(szModFileName, &handle);
			
			abData=(LPBYTE)malloc(dwSize);
			
			GetFileVersionInfo(szModFileName, handle, dwSize, abData);
			
			if (dwSize)
			{
				LPSTR szFileVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szProductVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szComments=(LPSTR)alloca(PATH_LENGTH);

				/* get country translation */
				VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
				/* make country code */
				wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
				
				if (dwSize!=0)
				{
					/* get a versioninfo file version number */
					wsprintf(szName,"\\StringFileInfo\\%s\\FileVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szFileVersion,(LPSTR)lpBuffer,(int)dwSize);

				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\ProductVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szProductVersion,(LPSTR)lpBuffer,(int)dwSize);
				
				/* copy to the dialog static text box */
				wsprintf(szBuf,"Version %s  (%s)",szFileVersion, szProductVersion);
				SetDlgItemText (hDlg, IDC_VERSION, szBuf);

				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\Comments",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				/* copy to the dialog static text box */
				lstrcpyn(szComments,(LPSTR)lpBuffer,(int)dwSize);
				SetDlgItemText (hDlg, IDC_COMMENT, szComments);

			}
			
			wsprintf(szName,"%s - %s",(LPSTR)__DATE__,(LPSTR)__TIME__);
			SetDlgItemText(hDlg,IDC_COMPID,szName);
			
			free((PVOID)abData);
			free((PVOID)szName);
			free((PVOID)szModFileName);
			free((PVOID)szBuf);
		}
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
    return FALSE;

}

//
//  FUNCTION: InitApp()
//
//  PURPOSE: Things to do when main window is created
//
//  COMMENTS: Called from WM_CREATE message
//			 We create a list box to fill the main window to use as a
//			debugging tool, and configure the serial port and infodriver 
//			connections, as well as a circular fifo buffer
//
//
LRESULT InitApp(HWND hWnd)
{

	/* return 0 if OK, or -1 if an error occurs*/
		RECT rctWindow;
	
	bAutomaticStarting=TRUE;
	OutputDebugString( " \n DISCOVERY PACKAGER AUTO starting up \n " );

	// define brushes and colours
	CreateBrushes();	
	OutputDebugString("brushes done\n ");

	iLabelAutoStatus=1;
	iLabelInfoStatus = 0;
	iOverallTXRXModeStatus = 0;
	iOverallModeChangedOver = 0;
	iOverallStateJustChanged = 0;
	iNextResilienceProcessing = 1;
	bShowAllDebugMessages = FALSE;
	bValidCollediaStatetoContinue=TRUE;
	bAutomaticStarting=TRUE;
	bRMDatabaseTimerRunning = FALSE;
	bNextCommandTimerRunning = FALSE;
	ssl_RMChanges = bncs_stringlist("", '|');
	ssl_RM_SrcPkg_Changes = bncs_stringlist("", '|');
	ssl_RM_DestPkg_Changes = bncs_stringlist("", '|');
	ssl_RM_AudioPkg_Changes = bncs_stringlist("", '|');
	ssl_DestPkgs_For_SDIStatus_Calc = bncs_stringlist("", '|');
	iSDIStatusCalcCounter = 0;
	iHighestCommandQueue = 5;
	iPauseCommandProcessing = 0;
	iChosenIFB = 0;
	iChosenRing = 0;
	iChosenTrunk = 0;
	iChosenSrcPkg = 0;
	iChosenDestPkg = 0;
	iChosenAudioSrcPkg = 0;
	iChosenInfodriver = 1;
	iChosenRiedelDest = 0;
	iChosenConference = 0;
	iChosenMonitorPort = 0;
	ssKeyClearCmd = "&CLEAR";
	iNumberOfPackages = 0;         
	iInfodriverDivisor = 0;

	// Init Status Dialogs
	hWndDlg = CreateDialog(hInst, (LPCTSTR)IDD_STATUS, hWnd, (DLGPROC)DlgProcChild);
	GetClientRect(hWndDlg, &rctWindow);
	iChildHeight = rctWindow.bottom - rctWindow.top + 2;
		
	GetClientRect(hWnd,&rc);
	hWndList=CreateWindow("LISTBOX","",WS_CHILD | WS_VSCROLL | LBS_NOINTEGRALHEIGHT | LBS_USETABSTOPS,
						  0,iChildHeight,rc.right-rc.left,rc.bottom-rc.top-iChildHeight,
						  hWnd,(HMENU)IDW_LIST,hInst,0);
	ShowWindow(hWndList,SW_SHOW);

	LoadIni();
	VersionInfo();

	if ((iNumberOfPackages <= 0) || (iNumberOfPackages > 132000) || (iInfodriverDivisor <= 0) || (iInfodriverDivisor > 4096))
	{
		Debug("ERROR ERROR - Invalid Packager configuration");
		Debug("ERROR ERROR -  Automatic not functioning");
		iLabelAutoStatus = 2;
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "CONFIG FAIL ");
		bValidCollediaStatetoContinue = FALSE;
		return 0;
	}

	if (ssCompositeInstance.length() > 0) {

		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Initialising");
		OutputDebugString(" initialising \n ");

		ecCSIClient = new extclient;
		if (ecCSIClient) {

			ecCSIClient->notify(CSIClientNotify);
			switch (ecCSIClient->connect()){
			case CONNECTED:
				Debug("Connected OK to CSI");
				//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI Connected ok");
				break;
			case CANT_FIND_CSI:
				Debug("ERROR ERROR - Connect failed to CSI ");
				Debug("ERROR ERROR -  Automatic not functioning");
				//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI connect FAILED");
				iLabelAutoStatus = 2;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
				bValidCollediaStatetoContinue = FALSE;
				break;
			case BAD_WS:
				Debug("ERROR ERROR - Connect failed to CSI - Bas WS ?? WTF ");
				Debug("ERROR ERROR -  Automatic not functioning");
				//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI connect FAILED");
				iLabelAutoStatus = 2;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "BAD WS - in FAIL state");
				bValidCollediaStatetoContinue = FALSE;
				break;
			default:
				Debug("Other Error - code %d", ecCSIClient->getstate());
				Debug("ERROR ERROR - Connect failed to CSI,  code :%d", ecCSIClient->getstate());
				Debug("ERROR ERROR -  Automatic not functioning");
				//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI connect FAILED");
				iLabelAutoStatus = 2;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
				bValidCollediaStatetoContinue = FALSE;
			} // switch ec
			ecCSIClient->setcounters(&lTXID, &lRXID);
		}
		else {
			Debug("ERROR ERROR - CSI Client class fail ");
			Debug("ERROR ERROR -  Automatic not functioning");
			iLabelAutoStatus = 2;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
			bValidCollediaStatetoContinue = FALSE;
		}

		if (bValidCollediaStatetoContinue) {
			iStartupCounter = 0;
			iOneSecondPollTimerId = SetTimer(hWnd, STARTUP_TIMER_ID, STARTUP_TIMER_INTERVAL, NULL);
			iLabelAutoStatus = 1;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Starting Initialisaion ");
		}
	}
	else {
		Debug("ERROR ERROR - MISSING COMPOSITE INSTANCE");
		Debug("ERROR ERROR -  Automatic not functioning");
		iLabelAutoStatus = 2;
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "C-INSTANCE FAIL ");
		bValidCollediaStatetoContinue = FALSE;
	}

	// end of first part of init
	return 0;

}

//
//  FUNCTION: CloseApp()
//
//  PURPOSE: Things to do when main window is destroyed
//
//  COMMENTS: Called from WM_DESTROY message
//			The listbox debug window is a child of the main, so it will be 
//			destroyed automatically. 
//
void CloseApp(HWND hWnd)
{
	SendOutClosingMessage();

	// kill any timers
	KillTimer( hWnd , COMMAND_TIMER_ID );
	KillTimer( hWnd , ONE_SECOND_TIMER_ID );
	
// force all infodrivers into rxonly
	ForceAutoIntoRXOnlyMode();
		
	Debug("CloseApp - clear maps");
	ClearAutomaticMaps();
	ClearCommandQueue();
	ClearRevertiveQueue();
	if (cl_KeyInstances) delete cl_KeyInstances;

	// close all infodrivers
	CloseAllInfodrivers();

	if (ecCSIClient)    delete ecCSIClient;

	// tidy up colours
	ReleaseBrushes();
	DeleteObject(HBr);

	if (hCSIClientSemaphore) CloseHandle(hCSIClientSemaphore);

}



//
//  FUNCTION: LoadIni()
//
//  PURPOSE: Assign (global) variables with values from .ini file
//
//  COMMENTS: The [section] name is gotten from a stringtable resource
//			 which defines it. It is the same name as the application, defined by the wizard
//			
//			
void LoadIni(void)
{

	fLog = FALSE;
	fDbgView = FALSE;
	iDebugFlag = 0;
	fDebugHide = TRUE;
	if (ssCompositeInstance.length() > 0) {
		bncs_string ssXmlConfigFile = "packager_config";
		int iVal = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "DebugMode").toInt();
		if (iVal > 0) {
			iDebugFlag = 1;
			fDebugHide = FALSE;
			if (iVal > 1) bShowAllDebugMessages = TRUE;
		}
		iVal = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "LogToFile").toInt();
		if (iVal >= 1) fLog = TRUE;
		if ((iVal == -1) || (iVal > 1)) fDbgView = TRUE;
		//
		// get number of packages to be expected
		iNumberOfPackages = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "number_of_packages").toInt();
		iInfodriverDivisor = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "packager_divisor").toInt();
		Debug("Loadini - number of packages is %d  and divisor is %d ", iNumberOfPackages, iInfodriverDivisor);
		if ((iNumberOfPackages <= 0) || (iNumberOfPackages>132000))  Debug("Loadini *** ERROR In NumberPackages - Check/Fix config *** ");
		if ((iInfodriverDivisor <= 0) || (iInfodriverDivisor>4096))  Debug("Loadini *** ERROR In Packager Divisor - Check/Fix config *** ");
		iPackagerBarsSrcePkg = iNumberOfPackages + 1;
		iPackagerParkSrcePkg = iNumberOfPackages + 2;
	}
	else {
		iDebugFlag = 2;
		fDebugHide = FALSE;
		bShowAllDebugMessages = TRUE;
		Debug("Loadini - NO COMPOSITE INSTANCE - PACKAGER WILL FAIL");
	}


}


// new function added to version 4.2 assigns the globals    szBNCS_File_Path and szBNCS_Log_File_Path
// called from r_p and w_p functions immediately below
// could easily be called once on first start up of app

char *	getBNCS_File_Path( void  )  
{
	char* szCCRoot = getenv( "CC_ROOT" );
	char* szCCSystem = getenv( "CC_SYSTEM" );
	char szV3Path[MAX_AUTO_BUFFER_STRING] = "";
	GetPrivateProfileString("Config", "ConfigPath", "", szV3Path, sizeof(szV3Path),"C:\\bncs_config.ini" );
	char szWinDir[MAX_AUTO_BUFFER_STRING] = "";
	GetWindowsDirectory(szWinDir, MAX_AUTO_BUFFER_STRING);

	// first check for 4.5 system settings
	if(szCCRoot && szCCSystem)	{
		sprintf(szBNCS_File_Path, "%s\\%s\\config\\system", szCCRoot, szCCSystem);
		sprintf(szBNCS_Log_File_Path, "%s\\%s\\logs", szCCRoot, szCCSystem);
	}
	else if (strlen(szV3Path)>0) {
		sprintf(szBNCS_File_Path,"%s",szV3Path);
		strcpy_s(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	else { 
		// all other BNCS / Colledia systems - get windows or winnt directory inc v3  that are not using bncs_config.ini
		sprintf(szBNCS_File_Path,"%s",szWinDir);
		strcpy_s(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	return szBNCS_File_Path;

}

/*!
\returns char* Pointer to data obtained
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* defval The default value to return if not found
\param BOOL fWrite Flag - write the default value to the file

\brief Function to read one item of user data from an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
char* r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite)
{
	char szFilePath[MAX_AUTO_BUFFER_STRING];	
	char szPathOnly[MAX_AUTO_BUFFER_STRING];

	strcpy_s( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );

	GetPrivateProfileString(section, entry, defval, szResult, sizeof(szResult), szFilePath);
	if (fWrite)
		w_p(file,section,entry,szResult);
	
	return szResult;

}

/*!
\returns void 
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* setting The value to write - =value

\brief Function to write one item of user data to an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
void w_p(char* file, char* section, char* entry, char* setting)
{

	char szFilePath[MAX_AUTO_BUFFER_STRING];	
	char szPathOnly[MAX_AUTO_BUFFER_STRING];

	strcpy_s( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );

	WritePrivateProfileString(section, entry, setting, szFilePath);

}


//
//  FUNCTION: UpdateCounters()
//
//  PURPOSE: Writes current counter values to child dialog
//
//  COMMENTS: 
//				
//			
//			
void UpdateCounters(void)
{
	char szBuf[32];
	//Update Infodriver External Message Count
	SetDlgItemInt(hWndDlg, IDC_CMD_QUEUE1, qCommands.size(), FALSE);
	wsprintf(szBuf, "%011d", lTXID);
	SetDlgItemText(hWndDlg, IDC_IDTX, szBuf);
	wsprintf(szBuf, "%011d", lRXID);
	SetDlgItemText(hWndDlg, IDC_IDRX, szBuf);
	int ipoolsize = iDynConfPoolSize;
	//if ((iControlStatus < 10) || (iControlStatus == 12) || (iControlStatus == 13)) ipoolsize = (iDynConfPoolSize / 2);
	wsprintf(szBuf, "%d|%d", iDynConfPoolInUse, ipoolsize);
	SetDlgItemText(hWndDlg, IDC_CONFPOOL, szBuf);
	wsprintf(szBuf, "%d | %d", iDynIFBPoolInUse[1], iDynIFBPoolSize);
	SetDlgItemText(hWndDlg, IDC_IFBPOOL1, szBuf);
	wsprintf(szBuf, "%d | %d", iDynIFBPoolInUse[2], iDynIFBPoolSize);
	SetDlgItemText(hWndDlg, IDC_IFBPOOL2, szBuf);
	wsprintf(szBuf, "%d | %d", iDynIFBPoolInUse[3], iDynIFBPoolSize);
	SetDlgItemText(hWndDlg, IDC_IFBPOOL3, szBuf);
	wsprintf(szBuf, "%d | %d", iDynIFBPoolInUse[4], iDynIFBPoolSize);
	SetDlgItemText(hWndDlg, IDC_IFBPOOL4, szBuf);
	SetDlgItemInt(hWndDlg, IDC_QUEUE2, qRevertives.size(), FALSE);

}


//
//  FUNCTION: Debug()
//
//  PURPOSE: Writes debug information to the listbox
//
//  COMMENTS: The function works the same way as printf
//				example: Debug("Number=%d",iVal);
//			
//			
void Debug(LPCSTR szFmt, ...)
{
	if ((iDebugFlag>0) || (fLog) || (fDbgView))
	{
		va_list argptr;

		va_start(argptr, szFmt);
		vsprintf(szDebug, szFmt, argptr);
		va_end(argptr);

		if (strlen(szDebug) > 820) szDebug[820] = NULL; // trim very long messages

		// log to dbview app
		if (fDbgView) {
			wsprintf(szLogDbg, "PackagerAuto - %s \n", szDebug);
			OutputDebugString(szLogDbg);
		}

		// app debug window
		if ((iDebugFlag>0) || (fLog)) {

			char tBuffer[32];
			char szDate[64];
			char szTime[64];
			struct tm *newtime;
			time_t long_time;

			time(&long_time);                // Get time as long integer. 
			newtime = localtime(&long_time); // Convert to local time. 
			_strtime(tBuffer);
			sprintf(szDate, "%04d%02d%02d", newtime->tm_year + 1900, newtime->tm_mon + 1, newtime->tm_mday);
			wsprintf(szTime, "%02d:%02d:%02d", newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
			wsprintf(szLogDbg, "%s %s", szTime, szDebug);

			// log to bncs logs dir
			if (fLog) {
				Log(szDate);
			}

			if (iDebugFlag>0) {
				// truncate messages to fit app dbg window
				if (strlen(szLogDbg) > 220) szLogDbg[220] = NULL;
				//
				SendMessage(hWndList, WM_SETREDRAW, FALSE, 0);	//Stop redraws.
				SendMessage(hWndList, LB_ADDSTRING, 0, (LPARAM)szLogDbg);	//Add message
				long lCount = SendMessage(hWndList, LB_GETCOUNT, 0, 0);	//Get List Count
				if (lCount > MAX_LB_ITEMS)
				{
					SendMessage(hWndList, LB_DELETESTRING, 0, 0);	//Delete item
					lCount--;
				}
				SendMessage(hWndList, LB_SETTOPINDEX, (WPARAM)lCount - 1, 0);	//Set Top Index
				SendMessage(hWndList, WM_SETREDRAW, TRUE, 0);	//Restart redraws.
			}
		}
	}
}


//
// FUNCTION CreateLogDirectory
//
// new function Version 4.2 of the Wizard - to create log file directory according to Colledia system type
//
BOOL CreateLogFileDirectory(void)
{
	static bool bFirst = true;
	//create the log directory the first time
	if (bFirst) {
		_mkdir(szBNCS_Log_File_Path);
		bFirst = false;
		return TRUE;
	}
	return FALSE;
}

//
//  FUNCTION: Log()
//
//  PURPOSE: Writes information to the log file
//
//
void Log(LPCSTR szDate)
{
	// this function must only be called from Debug - as this dir sets up time and date
	// uses szLogDbg == global debug string for message logged ( set up in Debug function )

	char szLogFile[300];
	char   c = '\n';
	FILE *fp;

	wsprintf(szLogFile, "%s\\%s_PackagerAuto.log", szBNCS_Log_File_Path, szDate);
	// create log file directory if it doesn't exist
	CreateLogFileDirectory();
	if (fp = fopen(szLogFile, "a"))
	{
		fprintf(fp, "%s%c", szLogDbg, c);
		fclose(fp);
	}

}

//
// Function name	: SplitString
// Description	   : Split a delimited string to an array of pointers to strings
// Return type		: int number of elements returned
//
//----------------------------------------------------------------------------//
// Takes a string and splits it up creating an array of pointers to the start //
// of the sections delimted by the array of specified char delimiters         //
// the delimter characters in "string" are overwritten with NULLs             //
// Usage:                                                                     //
// char szString[] = "1,2|3'4,5,6,7,";                                        //
// char delim[] = {',', '|', '\'', ',', ',', ',', ',',};                      //
// UINT count=7;                                                              //
// char *pElements[7];                                                        //
// int iLen;                                                                  //
//	iLen = SplitString( szString, delim, 7, pElements);                       //
//                                                                            //
// NOTE: This funcion works differently to strtok in that consecutive         //
// delimiters are not overlooked and so a NULL output string is possible      //
// i.e. a string                                                              //
//    hello,,dave                                                             //
// where ,,, is the delimiter will produce 3 output strings the 2nd of which  //
// will be NULL                                                               //
//----------------------------------------------------------------------------//
int SplitString(char *string, char *delim, UINT count, char **outarray )
{

	UINT x;
	UINT y;
	UINT len;					// length of the input string
	static char *szNull = "";	// generic NULL output string
	int delimlen;
	int dp;

	len = strlen( string );
	delimlen = strlen( delim );

	if(!len)					// if the input string is a NULL then set all the output strings to NULL
	{
		for (x = 0 ; x < count; x++ )
			outarray[x] = szNull;
		return 0;
	}

	outarray[0] = string;		// set the 1st output string to the beginning of the string

	for( x = 0,y = 1 ; (x < len) && (y < count); x++ )
	{
		if( delimlen < 2 )
			dp = 1;
		else
			dp = y;
		if(string[x] == delim[dp-1])
		{
			string[ x ] = '\0';
			if((x+1) < len)		// if there is another string following this one....
			{
				outarray[ y ] = &string[x+1];
				y++;			// increment the number of assigned buffer
			}
			else
			{
				outarray[ y ] = szNull;
			}
		}
	}

	x = y;
	while( x < count )
		outarray[x++] = szNull;
	return y;					// return the number of strings allocated
	
}




/////////////////////////////////////////////////////////////////////////
// BRUSHES FOR BUTTONS & LABELS
/////////////////////////////////////////////////////////////////////////

/*********************************************************************************************************/
/*** Function to Create Colour brushes ***/
void CreateBrushes()
{ 
	hbrWindowsGrey = CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
	hbrBlack = CreateSolidBrush(COL_BLACK);
	hbrWhite = CreateSolidBrush(COL_WHITE);

	hbrDarkRed = CreateSolidBrush(COL_DK_RED);
	hbrDarkGreen = CreateSolidBrush(COL_DK_GRN);
	hbrDarkOrange = CreateSolidBrush(COL_DK_ORANGE);
	hbrDarkBlue = CreateSolidBrush(COL_DK_BLUE);

	hbrYellow = CreateSolidBrush(COL_YELLOW);
	hbrTurq = CreateSolidBrush(COL_TURQ);

	hbrLightRed = CreateSolidBrush(COL_LT_RED);
	hbrLightGreen = CreateSolidBrush(COL_LT_GRN);
	hbrLightOrange = CreateSolidBrush(COL_LT_ORANGE);
	hbrLightBlue = CreateSolidBrush(COL_LT_BLUE);

}

 /********************************************************************************************************/
void ReleaseBrushes()
{
	if (hbrBlack) DeleteObject(hbrBlack);
	if (hbrWhite) DeleteObject(hbrWhite);
	if (hbrWindowsGrey) DeleteObject(hbrWindowsGrey);

	if (hbrDarkRed) DeleteObject(hbrDarkRed);
	if (hbrDarkGreen) DeleteObject(hbrDarkGreen);
	if (hbrDarkBlue) DeleteObject(hbrDarkBlue);
	if (hbrDarkOrange) DeleteObject(hbrDarkOrange);

	if (hbrYellow)	DeleteObject(hbrYellow);
	if (hbrTurq)	DeleteObject(hbrTurq);
	if (hbrLightRed) DeleteObject(hbrLightRed);
	if (hbrLightBlue) DeleteObject(hbrLightBlue);
	if (hbrLightGreen) DeleteObject(hbrLightGreen);
	if (hbrLightOrange) DeleteObject(hbrLightOrange);

}

/*********************************************************************************************************/

