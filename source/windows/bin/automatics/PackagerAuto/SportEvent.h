
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>

#include "AudioPackages.h"
#include "PackagerConsts.h"
#include "bncs_auto_helper.h"


class CSportEvent
{
public:
	CSportEvent();
	~CSportEvent();

	int m_iKeyIndex;
	bncs_string m_ssName;    // bncs or button name

	bncs_string m_ssEventID;
	bncs_string m_ssAssetID;
	bncs_string m_ssClassID;

	bncs_string m_ss_TimeDefinition;       // DB4 raw list - start, end date time
	bncs_string m_ss_TeamDefinition;       // DB6 raw list - start, end date time

	bncs_string m_ssTeam1ID;
	bncs_string m_ssTeam2ID;

	bncs_string m_ssStartDate;
	bncs_string m_ssStartTime;
	bncs_string m_ssEndDate;
	bncs_string m_ssEndTime;

	int m_iSPEV_Status;

	int m_iAssociated_SourcePackage;    // most likely 1:1 association
};

