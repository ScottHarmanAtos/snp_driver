#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// holds ring master conference specific information
class CRingMaster_CONF
{
private:
	int m_iIndexNumber;
	int m_iAssigned_Trunk_Address;

	// raw ringmaster revs for conf 
	bncs_string m_ss_RMstr_Label;
	bncs_string m_ss_RMstr_ConfMembers;
	bncs_string m_ss_RMstr_PanelMembers;       // not really used ???
	bncs_string m_ss_RMstr_CalcMonitorMembers;   //  this members / users list is important from ringmaster

	// processed revs / data
	bncs_string m_ss_ConfName;
	bncs_string m_ss4CharLabel;                   // for TB 4w name selected in src package panel 1-128 used as part of conf label
	bncs_string m_ssOverrideLabel;             // from info 1004 db1 slots 1`-500 to override conf  label

	bncs_stringlist m_ssl_Conference_members;    //   extracted from conf members rev below
	bncs_stringlist m_ssl_Conference_privileges;    //   extracted from conf members rev below

	// set at init from db104 if previously in use before restart
	int m_iDynamic_Audio_Package;                      // audio source pkg index for dyn allocated d4W / TB conf from confernce pool   - note 77777 means other packager
	int m_iDynamic_WhichConf;
	int m_iDynamic_Conf_UseType;                      // values of 11,12,13,14 for 4W or 21, 22 for TB conf in audio pkg use    -- 77 means other packager

public:
	CRingMaster_CONF( int iIndex );
	~CRingMaster_CONF();

	void storeRingMasterLabel(bncs_string ssRev);
	void storeRingMasterConfMembers(bncs_string ssRev);
	void storeRingMasterPanelMembers(bncs_string ssRev);
	void storeRingMasterCalcMonitorMembers(bncs_string ssRev);

	bncs_string getRingMasterLabel(void);
	bncs_string getRingMasterConfMembers(void);
	bncs_string getRingMasterPanelMembers(void);
	bncs_string getRingMasterCalcMonitorMembers(void);

	// from old CConference class - now superceeded by this class
	void SetConferenceName(bncs_string ssName);
	bncs_string GetConferenceName(void);
	void SetConferenceLabel(bncs_string ssName);
	bncs_string GetConferenceLabel(void);

	BOOL AddMemberToConference(bncs_string ssMemberPriv, BOOL bPrepend = FALSE);
	BOOL RemoveMemberFromConference(bncs_string ssRingPort);
	bncs_string GetAllConferenceMembers(void);    // list of ports only
	bncs_string GetAllConferenceDetails(char ic);       // list of ports and privileges
	void ClearConferenceMembers(void);

	// ancillary conf data functions
	void SetDynamicAudioPackage(int iPackage, int iWhichConf, int iUseType);
	int GetDynamicPackage(void);
	int GetDynamicWhichConf(void);
	int GetDynamicUseType(void);

	void setAssignedTrunkAddr(int iTrunkAddress);
	int getAssignedTrunkAddr(void);

	void SetOverrideConferenceLabel(bncs_string ssOverRide);
	bncs_string GetOverrideConferenceLabel(void);

};

