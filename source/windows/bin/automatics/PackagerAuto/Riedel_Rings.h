#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"
#include <map>
using namespace std;
//
#include "Riedel_LookUp.h"

// class too hold info on ring info and connection to trunk navigator etc
// can be thought of as master record holding key info pertaining to ring for Automatic

class CRiedel_Rings
{
private:

	map<int, CRiedel_LookUp*> cl_RiedelLookUpData;   // map for lookup data

	CRiedel_LookUp* GetLookUpData(int iIndex);

public:
	CRiedel_Rings( int iKeyIndex, int iTrunkAddr, int iRingType, bncs_string ssName );
	~CRiedel_Rings();

	int m_iRecordIndex;
	int m_iTrunkAddress;
	int m_iRingUseType;
	bncs_string ssRingName;

	bncs_string ssRing_Instance_GRD;
	int iRing_Device_GRD;
	bncs_string ssRing_Instance_Monitor;
	int iRing_Device_Monitor;
	bncs_string ssRing_Instance_Conference;
	int iRing_Device_Conference;
	bncs_string ssRing_Instance_IFB;
	int iRing_Device_IFB;
	bncs_string ssRing_Instance_Extras;
	int iRing_Device_Extras;

	int iRing_Ports_Talk, iRing_Ports_Listen;  // src, dest v -- grd size
	int iRing_RevsRecord_Monitors;

	bncs_string ssAProcessString;                     // a string to store temp data for a given ring - public for ease of use

	void AssignLookupAddress(int iIndex, bncs_string ssData);
	void AssignLookupSrcPackage(int iIndex, int iPackage);
	void AssignLookupDestPackage(int iIndex, int iPackage);
	void AssignLookupConference(int iIndex, int iConf);
	void AssignLookupIFB(int iIndex, int iIFB);

	bncs_string AcquireLookupAddress(int iIndex);
	int  AcquireLookupTrunkAddr(int iIndex);
	int  AcquireLookupPort(int iIndex);
	int  AcquireLookupPanel(int iIndex);
	int  AcquireLookupShift(int iIndex);
	int  AcquireLookupKey(int iIndex);

	int FindLookupIndexByPortKey(int iPort, int iKey);

	int AcquireLookupSrcPackage(int iIndex);
	int AcquireLookupDestPackage(int iIndex);
	int AcquireLookupConference(int iIndex);
	int AcquireLookupIFB(int iIndex);

	// could add trunk usage data from ring master  but adds no functionality required in packager 

};

