// DestinationPackages.h: interface for the CDestinationPackages class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DESTINATIONPACKAGES_H__F0C3ACF9_947B_48AE_B300_28AC1C2CD892__INCLUDED_)
#define AFX_DESTINATIONPACKAGES_H__F0C3ACF9_947B_48AE_B300_28AC1C2CD892__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
	#include <windows.h>
	#include "PackagerConsts.h"
	#include <bncs_string.h>
	#include <bncs_stringlist.h>
	#include "bncs_auto_helper.h"


class CDestinationPackages  
{
private:

	struct stAssocIndexPairTag {
		int iAssocIndex;
		int iAssocHub_or_TypeTag;
		int iAssocLang_or_UseTag;
		int iMSPLevelAPIndx;   // Index of vid level in MSP or which Audio Package index (i.e.1..32) actually used in routing;
		int iASPairIndx;               // Index of audio source pair within Audio Package routed for audio elements;
	};

	struct stSubstituteIndexPairTag {
		int iSub_1_Hub_or_TypeTag;
		int iSub_1_Lang_or_UseTag;
		int iSub_2_Hub_or_TypeTag;
		int iSub_2_Lang_or_UseTag;
	};

	struct stIFB_Details {
		int i_in_ring;
		int i_in_port;
		int i_mm_ring;
		int i_mm_port;
		int i_out_ring;
		int i_out_port;
	};

		 
public:
	CDestinationPackages();
	virtual ~CDestinationPackages();

 	int iPackageIndex;
	int iPackageLockState;                         // from infodriver 5 status

	bncs_string ss_PackageButtonName;   // DB1
	bncs_string ss_PackageLongName;  // DB3

	bncs_string ss_PackagerOwnerDelg; // db5 -- package type indication
	bncs_string ss_Package_Definition; // DB7 in new scheme

	bncs_string ss_Package_AudioDests;   // new 15
	bncs_string ss_Package_AudioTags;     // new 16

	bncs_string ss_AudioTags_Substitues1;     // new 17
	bncs_string ss_AudioTags_Substitues2;     // new 18

	bncs_string ss_Package_AudioRtn; // DB19

	int iNevionPriorityLevel;                                // from command in   pri=xx

	// from db5
	bncs_string ss_PackageUniqueID;
	int m_iOwnerType;
	int m_iDelegateType;

	int i_ParkSourcePackage;

	// parsed from db 7 strings
	struct stAssocIndexPairTag st_VideoHDestination;
	struct stAssocIndexPairTag st_VideoANC[MAX_PKG_VIDEO];
	struct stAssocIndexPairTag  st_Reverse_Vision;

	// parsed from db 15 16 strings
	struct stAssocIndexPairTag st_AudioDestPair[MAX_PKG_AUDIO];

	// parsed from db 17 18 strings
	struct stSubstituteIndexPairTag st_AudioDestSubstitutes[MAX_PKG_AUDIO];

	// parsed from db 19 strings
	struct stAssocIndexPairTag st_AudioReturnPair[MAX_PKG_VIDEO];

	// Riedel INPUT ring.ports = from db7  -- 2 destination pkg ifbs at present
	struct stIFB_Details st_AssocIFBs[MAX_PKG_CONFIFB];

	// when going via virtual packages -- if no virtuals used in routing then routed package will equal traced
	int i_Traced_Src_Package;           // **NOTE** this is the originating src pkg index - traced back thru any virtual packages 
	int i_Routed_Src_Package;           // virtual or real src package routed to dest pkg
	// vision and audio usually come from the traced source - but with overrides now -- need a record of the current pkgs providing vision and audio
	int i_VisionFrom_Pkg;
	int i_AudioFrom_Pkg;                                        // may be from an override from virtual within trace.
	int i_AudioFrom_TagStatus;                            // 0,1,2,3  values - ok/parked  or subs 1 or 2 used or 3 for any of 32
	int i_VisionFrom_Source;                                // - nevion video requested routed source 
	
	// take special settings in pkg cmds
	// extracted from route command into package -- usually tag, but level if assigned and tag=0 then forces level to route from
	int i_CmdRoute_Level;
	int i_CmdRoute_LangUse_Tag;   
	int i_CmdAudio_Returns;

	bncs_string ss_Trace_To_Source;  // trace thru from dest pkg to src pkg via virtuals - first item is src, then next src etc to dest

	int i_Reverse_Vision_Routed_AudioPackage;    // Audio Package rev vision routed to
	int i_Reverse_Audio_Routed_AudioPackage[MAX_PKG_VIDEO];	// Audio Package rev audio routed to

	bncs_stringlist ssl_AtTopOfAudioPkgMMQ;      // list of APs that this destpkg is at top of
	
};

#endif // !defined(AFX_DESTINATIONPACKAGES_H__F0C3ACF9_947B_48AE_B300_28AC1C2CD892__INCLUDED_)
