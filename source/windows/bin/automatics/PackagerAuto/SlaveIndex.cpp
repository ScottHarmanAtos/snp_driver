// SlaveIndex.cpp: implementation of the CSlaveIndex class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SlaveIndex.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSlaveIndex::CSlaveIndex()
{

}

CSlaveIndex::~CSlaveIndex()
{

}

 void CSlaveIndex::AddSlaveRecord( int iSlave, int iMaster )
 {
	m_iSlave_Package_Index = iSlave;
	m_iMaster_Package_Index = iMaster;
 }

int CSlaveIndex::GetSlaveIndex( void )
{
	return m_iSlave_Package_Index;
}


int CSlaveIndex::GetMasterIndex( void )
{
	return m_iMaster_Package_Index;
}
