// RouterData.cpp: implementation of the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RouterData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRouterData::CRouterData( int iIndx )
{
	m_iMapIndex=iIndx;
	m_iRoutedIndex=0;		
	m_iLockState=0;		
	m_iReEnterant_Device=0;
	m_iReEnterant_Index=0;
	m_iHybridAudio=0;
	ssl_SDIIndxUsedInPackages = bncs_stringlist( "", ',' );
}

CRouterData::~CRouterData()
{

}
