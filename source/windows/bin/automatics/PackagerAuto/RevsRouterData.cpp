// RevsRouterData.cpp: implementation of the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RevsRouterData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRevsRouterData::CRevsRouterData( int iDeviceNumber, int iDests, int iSrcs )
{
	// initialise structure
	m_iRouterDeviceNumber=iDeviceNumber;
	m_iMaxDestinations=iDests;
	m_iMaxSources=iSrcs;

	// create entries in maps for dests and sources
	// create entries in maps for dests and sources
	for (int iS = 1; iS <= m_iMaxSources; iS++) {
		CRouterSrceData* pData = new CRouterSrceData(iS);
		if (pData) { // add to map
			cl_SrcRouterData.insert(pair<int, CRouterSrceData*>(iS, pData));
		}
	}
	// create entries in maps for dests and sources
	for (int iD = 1; iD <= m_iMaxDestinations; iD++) {
		CRouterDestData* pData = new CRouterDestData(iD);
		if (pData) { // add to map
			cl_DestRouterData.insert(pair<int, CRouterDestData*>(iD, pData));
		}
	}

}

CRevsRouterData::~CRevsRouterData()
{
	// seek and destroy
	while (cl_DestRouterData.begin() != cl_DestRouterData.end()) {
		map<int, CRouterDestData*>::iterator it = cl_DestRouterData.begin();
		if (it->second) delete it->second;
		cl_DestRouterData.erase(it);
	}
	while (cl_SrcRouterData.begin() != cl_SrcRouterData.end()) {
		map<int, CRouterSrceData*>::iterator it = cl_SrcRouterData.begin();
		if (it->second) delete it->second;
		cl_SrcRouterData.erase(it);
	}
}


////////////////////////////////////////////////////
// private internal functions for src/dest maps 
CRouterSrceData* CRevsRouterData::GetSourceData(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		map<int, CRouterSrceData*>::iterator it = cl_SrcRouterData.find(iSource);
		if (it != cl_SrcRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

CRouterDestData* CRevsRouterData::GetDestinationData(int iDest)
{
	if ((iDest>0) && (iDest <= m_iMaxDestinations)) {
		map<int, CRouterDestData*>::iterator it = cl_DestRouterData.find(iDest);
		if (it != cl_DestRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

///////////////////////////////////////////////////////////////////////////////
// generic method to split string into two halves around a specified character
// these next two functions lifted from ip_router parsing NV9000 data
BOOL CRevsRouterData::SplitAString(char szStr[64], char cSplit, char *szLeft, char* szRight)
{
	char *pdest;
	int result = 0;
	strcpy(szLeft, "");
	strcpy(szRight, "");

	pdest = strchr(szStr, cSplit);
	if (pdest != NULL) {
		result = pdest - szStr + 1;
		if (result>0) {
			if (result == strlen(szStr)) {
				strcpy(szLeft, szStr);
			}
			else {
				strncpy(szLeft, szStr, result - 1);
				szLeft[result - 1] = 0; // terminate
				strncpy(szRight, szStr + result, strlen(szStr) - result + 1);
			}
			return TRUE;
		}
	}
	else {
		strcpy(szLeft, szStr);
		return TRUE;
	}
	return FALSE;
}

void CRevsRouterData::ExtractNVLevelData(char szlvl[64], int* iDestLvl, int* iSrce, int* iSrceLvl)
{
	*iDestLvl = 0;
	*iSrce = 0;
	*iSrceLvl = 0;
	//Debug("ExtractNVLvl data %s", szlvl);
	if (strchr(szlvl, ':') != NULL) {
		char szFirstData[32] = "", szSecondData[32] = "";
		SplitAString(szlvl, ':', szFirstData, szSecondData);
		// if first data is * or ALL or 0 - will result in a route all to that source
		*iDestLvl = atoi(szFirstData);
		if (strchr(szSecondData, '.') != NULL) {
			// srce and level is given
			char szArgData1[32] = "", szArgData2[32] = "";
			SplitAString(szSecondData, '.', szArgData1, szArgData2);
			*iSrce = atoi(szArgData1);
			*iSrceLvl = atoi(szArgData2);
		}
		else {
			// just a srce given
			*iSrce = atoi(szSecondData);
			*iSrceLvl = atoi(szFirstData); // ie srce level is same level as destination one
		}
	}
	else {
		// no levels - so assume a "take" all - as levels will be 0==ROUTE_ALL
		*iSrce = atoi(szlvl);
		*iSrceLvl = 0;
	}
}




///////////////////////////////////////////////////////////////
BOOL CRevsRouterData::storeGRDRevertive(int iDestination, int iNewSource, int iRouteStatus, int iDestLock )
{
	// store given source from actual rev; any re-enterant src/dest calc then done  if required
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			// extract source and status as there should be from ip_router as it returns a string rev
			// parse out from string if scheduled or active or parked
			// if scheduled then store in scheduled var rather than routed var
			if (iRouteStatus == NEVION_SCHEDULED) {
				pData->m_iScheduledIndex = iNewSource;
			}
			else {
				if (pData->m_iRoutedIndex != iNewSource) pData->m_iPreviousRoutedIndex = pData->m_iRoutedIndex;
				pData->m_iRoutedIndex = iNewSource;
				if (iNewSource == pData->m_iScheduledIndex) pData->m_iScheduledIndex = 0; // clear as it is now routed
				pData->m_iExpectedIndex = -1;                                             // clear expected src as there has been a revertive
				pData->m_iExpectedCounter = 0;
			}
			pData->m_iRouteStatus = iRouteStatus;
			pData->m_iLockStatus = iDestLock;
			//
			return TRUE;
		}
		else
			OutputDebugString(LPCSTR(bncs_string("CRevsRouterData::storeGRDRevertive no data class for destination %1\n").arg(iDestination)));
	}
		else
			OutputDebugString(LPCSTR(bncs_string("CRevsRouterData::storeGRDRevertive negative destination or greater than maxUsed %1\n").arg(iDestination)));
	return FALSE;
}


void CRevsRouterData::addRouterDestPackageIndex(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			bncs_stringlist ssl = bncs_stringlist(pData->m_ssDestination_Package, '|');
			if (ssl.find(bncs_string(iPackage))<0) ssl.append(bncs_string(iPackage));
			pData->m_ssDestination_Package = ssl.toString('|');
		}
	}
}

void CRevsRouterData::removeRouterDestPackageIndex(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			bncs_stringlist ssl = bncs_stringlist(pData->m_ssDestination_Package, '|');
			int iPos = ssl.find(bncs_string(iPackage));
			if (iPos >= 0) ssl.deleteItem(iPos);
			pData->m_ssDestination_Package = ssl.toString('|');
		}
	}
}


void CRevsRouterData::addReverseReturnPackageIndex(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData)  {
			bncs_stringlist ssl = bncs_stringlist(pData->m_ssReverseReturn_Package, '|');
			if (ssl.find(bncs_string(iPackage))<0) ssl.append(bncs_string(iPackage));
			pData->m_ssReverseReturn_Package = ssl.toString('|');
		}
	}
}


void CRevsRouterData::removeReverseReturnPackageIndex(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			bncs_stringlist ssl = bncs_stringlist(pData->m_ssReverseReturn_Package, '|');
			int iPos = ssl.find(bncs_string(iPackage));
			if (iPos >= 0) ssl.deleteItem(iPos);
			pData->m_ssReverseReturn_Package = ssl.toString('|');
		}
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CRevsRouterData::getRouterNumber( void )
{
	return m_iRouterDeviceNumber;
}


int CRevsRouterData::getMaximumDestinations( void )
{
	return m_iMaxDestinations;
}


int CRevsRouterData::getMaximumSources( void )
{
	return m_iMaxSources;
}


int CRevsRouterData::getPrimarySourceForDestination(int iDestination)
{
	// return revertive source as  got from grd rev
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iRoutedIndex;
	}
	return 0;
}


int CRevsRouterData::getScheduledSourceForDestination(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iScheduledIndex;
	}
	return 0;
}


int CRevsRouterData::getRouterDestStatus(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_iRouteStatus;
		}
	}
	return UNKNOWNVAL;
}


int CRevsRouterData::getRouterDestLock(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_iLockStatus;
		}
	}
	return NEVION_UNLOCKED;
}

void CRevsRouterData::setExpectedSourceForDestination(int iDestination, int iExpectedSrc, int iCountValue)
{
	// return revertive source as  got from grd rev
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->m_iExpectedIndex = iExpectedSrc;
			pData->m_iExpectedCounter = iCountValue;
		}
	}
}

int CRevsRouterData::getExpectedSourceForDestination(int iDestination, BOOL bDecExp)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			if ((pData->m_iExpectedCounter > 0)&&(bDecExp)) {
				// if repeated routes queried prior to routing then will decrement counter so routing isnt locked out forever
				pData->m_iExpectedCounter--;
				if (pData->m_iExpectedCounter <= 0) {
					// clear the expectedIndex - to prevent inability to issue a route
					pData->m_iExpectedIndex = -1;
					pData->m_iExpectedCounter = 0;
				}
			}
			return pData->m_iExpectedIndex;
		}
	}
	return -1;
}

int CRevsRouterData::getExpectedCounterForDestination(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) 
			return pData->m_iExpectedCounter;
	}
	return 0;
}

bncs_string CRevsRouterData::getRouterDestPackageIndex(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_ssDestination_Package;
		}
	}
	return "";
}


bncs_string CRevsRouterData::getReverseReturnPackageIndex(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_ssReverseReturn_Package;
		}
	}
	return "";
}


//////////////////////////////////////////////////////////////////////////
// source data routines
//////////////////////////////////////////////////////////////////////////

void CRevsRouterData::removePackageFromSourceList(int iSource, int iPackage)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) {
			// is package already in list
			bncs_stringlist ssl = bncs_stringlist(pData->m_ssSDIIndxUsedInPackages, '|');
			int iPos = ssl.find(iPackage);
			if (iPos >= 0) {
				ssl.deleteItem(iPos);
				pData->m_ssSDIIndxUsedInPackages = ssl.toString('|');
			}
		}
	}
}


bncs_string CRevsRouterData::getAllPackagesForSource(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) {
			return pData->m_ssSDIIndxUsedInPackages;
		}
	}
	return "";
}


int CRevsRouterData::getNumberPackagesUsingSource(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// get from map
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) {
			bncs_stringlist ssl = bncs_stringlist(pData->m_ssSDIIndxUsedInPackages, '|');
			return ssl.count();
		}
	}
	return 0;
}

