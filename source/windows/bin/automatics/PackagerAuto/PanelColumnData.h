// PanelColumnData.h: interface for the CPanelColumnData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PANELCOLUMNDATA_H__DAA8B1A3_348E_424D_8451_B218413340E5__INCLUDED_)
#define AFX_PANELCOLUMNDATA_H__DAA8B1A3_348E_424D_8451_B218413340E5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CPanelColumnData  
{
public:
	CPanelColumnData();
	virtual ~CPanelColumnData();

	bncs_string ssColumnId;
	
	bncs_string ss_PanelLocation;
	int iAssocPackage;
	bncs_stringlist ssKeys;

	
};

#endif // !defined(AFX_PANELCOLUMNDATA_H__DAA8B1A3_348E_424D_8451_B218413340E5__INCLUDED_)
