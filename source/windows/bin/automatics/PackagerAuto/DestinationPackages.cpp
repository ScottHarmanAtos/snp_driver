// DestinationPackages.cpp: implementation of the CDestinationPackages class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DestinationPackages.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDestinationPackages::CDestinationPackages()
{
	iPackageIndex = UNKNOWNVAL;
	ss_PackageButtonName = "";    // DB1
	ss_PackageLongName = "";  // DB3
	ss_PackagerOwnerDelg = ""; // db5
	ss_Package_Definition = "";     //DB7
	ss_Package_AudioDests = "";
	ss_Package_AudioTags = "";
	ss_Package_AudioRtn = "";
	ss_PackageUniqueID = "";
	iNevionPriorityLevel = 3;   // default to medium priority level

	iPackageLockState = 0;
	i_ParkSourcePackage = 0;
	i_Traced_Src_Package = 0;
	i_Routed_Src_Package = 0;     

	i_VisionFrom_Pkg = 0;
	i_AudioFrom_Pkg = 0;
	i_VisionFrom_Source = 0;
	i_AudioFrom_TagStatus = 0;

	ss_Trace_To_Source = "0";

	i_CmdRoute_Level = 0;
	i_CmdAudio_Returns = 0;
	i_CmdRoute_LangUse_Tag = 0;


	m_iOwnerType = 0;
	m_iDelegateType = 0;

	st_VideoHDestination.iAssocIndex = 0;
	st_VideoHDestination.iAssocHub_or_TypeTag = 0;
	st_VideoHDestination.iAssocLang_or_UseTag = 0;
	st_VideoHDestination.iASPairIndx = 0;
	st_VideoHDestination.iMSPLevelAPIndx = 0;

	st_Reverse_Vision.iAssocIndex = 0;
	st_Reverse_Vision.iAssocLang_or_UseTag = 0;
	st_Reverse_Vision.iAssocHub_or_TypeTag = 0;
	st_Reverse_Vision.iASPairIndx = 0;
	st_Reverse_Vision.iMSPLevelAPIndx = 0;

	for (int iirp = 0; iirp < MAX_PKG_VIDEO; iirp++) {
		st_VideoANC[iirp].iAssocIndex = 0;
		st_VideoANC[iirp].iAssocLang_or_UseTag = 0;
		st_VideoANC[iirp].iAssocHub_or_TypeTag = 0;
		st_VideoANC[iirp].iASPairIndx = 0;
		st_VideoANC[iirp].iMSPLevelAPIndx = 0;
		//
		st_AudioReturnPair[iirp].iAssocIndex = 0;
		st_AudioReturnPair[iirp].iAssocLang_or_UseTag = 0;
		st_AudioReturnPair[iirp].iAssocHub_or_TypeTag = 0;
		st_AudioReturnPair[iirp].iASPairIndx = 0;
		st_AudioReturnPair[iirp].iMSPLevelAPIndx = 0;
		i_Reverse_Audio_Routed_AudioPackage[iirp] = 0;
	}

	for (int iidp = 0; iidp < MAX_PKG_AUDIO; iidp++) {
		st_AudioDestPair[iidp].iAssocIndex = 0;
		st_AudioDestPair[iidp].iAssocLang_or_UseTag = 0;
		st_AudioDestPair[iidp].iAssocHub_or_TypeTag = 0;
		st_AudioDestPair[iidp].iASPairIndx = 0;
		st_AudioDestPair[iidp].iMSPLevelAPIndx = 0;
		//
		st_AudioDestSubstitutes[iidp].iSub_1_Lang_or_UseTag = 0;
		st_AudioDestSubstitutes[iidp].iSub_1_Hub_or_TypeTag = 0;
		st_AudioDestSubstitutes[iidp].iSub_2_Lang_or_UseTag = 0;
		st_AudioDestSubstitutes[iidp].iSub_2_Hub_or_TypeTag = 0;
		//
	}

	// Riedel
	for (int iiff = 0; iiff < MAX_PKG_CONFIFB; iiff++) {
		st_AssocIFBs[iiff].i_mm_ring = 0;
		st_AssocIFBs[iiff].i_mm_port = 0;
		// these below are here for completeness and future expansion or correct use
		st_AssocIFBs[iiff].i_in_ring = 0;
		st_AssocIFBs[iiff].i_in_port = 0;
		st_AssocIFBs[iiff].i_out_ring = 0;
		st_AssocIFBs[iiff].i_out_port = 0;
	}

	i_Reverse_Vision_Routed_AudioPackage = 0;

	ssl_AtTopOfAudioPkgMMQ = bncs_stringlist("", ',');

}

CDestinationPackages::~CDestinationPackages()
{

}

