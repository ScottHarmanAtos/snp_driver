// RouterSrceData.cpp: implementation of the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RouterSrceData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRouterSrceData::CRouterSrceData( int iIndx )
{
	m_iMapIndex=iIndx;
	m_ssSourceRoutedTo = "";
	m_ssSDIIndxUsedInPackages = "";
	m_isHighway=0;
}

CRouterSrceData::~CRouterSrceData()
{

}
