// SourcePackages.h: interface for the CSourcePackages class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOURCEPACKAGES_H__EFFB27B0_8E6F_41E9_out2C4_3BCB70D38C92__INCLUDED_)
#define AFX_SOURCEPACKAGES_H__EFFB27B0_8E6F_41E9_out2C4_3BCB70D38C92__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>

#include "AudioPackages.h"
#include "PackagerConsts.h"
#include "PackageTags.h"
#include "bncs_auto_helper.h"

class CSourcePackages  
{
private:

	struct stAssocIndexPairTag {
		int iAssocIndex;
		int iAssocHub_or_TypeTag;
		int iAssocLang_or_UseTag;
	};

	struct stVideoAncData {
		struct stAssocIndexPairTag stVideoLevel;
		struct stAssocIndexPairTag stANCLevel[MAX_PKG_VIDEO]; // 1..4
	};

public:
	CSourcePackages();
	virtual ~CSourcePackages();

	int iPackageIndex;
	bncs_string ss_PackageButtonName;    // DB0
	bncs_string ss_PackageLongName;  // DB2
	bncs_string ss_PackageKeyInfo;      // DB4 raw list 
	bncs_string ss_PackageDefinition;   // DB6 raw list  -- list of audio packages
	bncs_string ss_PackageMCRUMD;       // DB9 
	bncs_string ss_PackageProdUMDs;       // DB10 
	bncs_string ss_PackageVideoLvls;       // DB11 

	int m_iOwnerType;
	int m_iDelegateType;

	int m_iAssociatedSportEvent;          // most likely 1:1 connection between src pkgs and SPEVs

	struct stVideoAncData st_PkgVideoLevels[MAX_PKG_VIDEO];         // levels 1-4 from db 11     
	struct stAssocIndexPairTag st_SourceAudioLevels[MAX_PKG_VIDEO];      // defualt audio package assoc to video - diff from the assoc 32 APs below

	int m_iNumberAssignedAudioPackages;
	int m_iAssoc_Audio_Packages[MAX_PKG_AUDIO];   // from db6
	
	// package routed to
	// reals and first step virtuals that src pkg is actually routed to 
	bncs_stringlist ssl_Pkg_DirectlyRoutedTo;
	// final dests this package is routed to i.e. via virtuals
	bncs_stringlist ssl_Pkg_FinallyRoutedTo;

};

#endif // !defined(AFX_SOURCEPACKAGES_H__EFFB27B0_8E6F_41E9_out2C4_3BCB70D38C92__INCLUDED_)
