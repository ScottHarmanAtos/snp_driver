// RouterData.h: interface for the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROUTERDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
#define AFX_ROUTERDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CRouterData  
{
public:
	CRouterData( int iIndx );
	virtual ~CRouterData();

	int m_iMapIndex;
	int m_iRoutedIndex;								// straight revertive from grd -- for dest data only
	int m_iLockState;								// dest data use only 
	int m_iReEnterant_Device;                       // 0 if same device dest reenters on
	int m_iReEnterant_Index;	                    // 0 if not a re-enterant dest / src - else will hold linked src number
	int m_iHybridAudio;
	bncs_stringlist ssl_SDIIndxUsedInPackages;      // list of packages this sdi index is used therein

};

#endif // !defined(AFX_ROUTERDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
