#include "AudioPackages.h"


CAudioPackages::CAudioPackages()
{
	m_iAudioPkg_Indx = 0;
	m_ssPackageName = "";
	m_ssButtonName="";
	m_ssUniqueID="";

	m_ss_PackageKeyInfo = "";
	m_ss_PackageDefinition = "";
	m_ss_AudioSourceDefinition16 = "";
	m_ss_AudioReturnDefinition = "";
	m_ss_AudioCFeedLabelSubtitles = "";
	m_ss_AudioConfLabelSubtitles = "";

	m_iOwnerType = 0;
	m_iDelegateType=0;
	m_iOveralLanguageTag=0;

	st_VideoLink_Vision.iAssocIndex = 0;
	st_VideoLink_Vision.iAssocLang_or_UseTag = 0;
	st_VideoLink_Vision.iAssocHub_or_TypeTag = 0;

	st_Reverse_Vision.iAssocIndex = 0;
	st_Reverse_Vision.iAssocLang_or_UseTag = 0;
	st_Reverse_Vision.iAssocHub_or_TypeTag = 0;
	iRoutedRevVisionPkg=0;
	iRoutedRevAudioPkg=0;
	iEnabledRevVision = 0;   

	for (int iias = 0; iias < MAX_AUDLVL_AUDIO; iias++) {
		st_AudioSourcePair[iias].iAssocIndex = 0;
		st_AudioSourcePair[iias].iAssocLang_or_UseTag = 0;
		st_AudioSourcePair[iias].iAssocHub_or_TypeTag = 0;
	}
	for (int iiar = 0; iiar < MAX_PKG_VIDEO; iiar++) {
		st_AudioReturnPair[iiar].iAssocIndex = 0;
		st_AudioReturnPair[iiar].iAssocLang_or_UseTag = 0;
		st_AudioReturnPair[iiar].iAssocHub_or_TypeTag = 0;
	}
	for (int iitt = 0; iitt < MAX_PKG_CONFIFB; iitt++) {
		// 4W TB confs
		stTBConferences[iitt].i_in_ring = 0;
		stTBConferences[iitt].i_in_port = 0;
		stTBConferences[iitt].i_out_ring = 0;
		stTBConferences[iitt].i_out_port = 0;
		stTBConferences[iitt].m_ss_MainLabel = "";
		stTBConferences[iitt].m_ss_SubTitleLabel = "";
		stTBConferences[iitt].i_DynAssignedIfb = 0;
		stTBConferences[iitt].i_FlagAssignCFConf = 0;
		stTBConferences[iitt].i_DynAssignedConference = 0;
		stTBConferences[iitt].i_Use_Which_DestPkgIFB = 0;
		// CF IFBs / Conferences
		stCFIFBs[iitt].i_in_ring = 0;
		stCFIFBs[iitt].i_in_port = 0;
		stCFIFBs[iitt].i_out_ring = 0;
		stCFIFBs[iitt].i_out_port = 0;
		stCFIFBs[iitt].m_ss_MainLabel = "";
		stCFIFBs[iitt].m_ss_SubTitleLabel = "";
		stCFIFBs[iitt].i_DynAssignedIfb = 0;
		stCFIFBs[iitt].i_FlagAssignCFConf = 0;
		stCFIFBs[iitt].i_DynAssignedConference = 0;
		stCFIFBs[iitt].i_Use_Which_DestPkgIFB = 0;
		if (iitt == 1) stCFIFBs[iitt].i_Use_Which_DestPkgIFB = 1; // as initial default
		if (iitt == 2) stCFIFBs[iitt].i_Use_Which_DestPkgIFB = 2; // as default
	}

	m_iVideoLinkIndex = 0;

	ssl_Pkg_Queue = bncs_stringlist("", ',');
	ssl_TiedToSourcePackages = bncs_stringlist("", ',');
}


CAudioPackages::~CAudioPackages()
{
}
