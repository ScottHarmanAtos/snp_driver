#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>

#include "PackageTags.h"
#include "PackagerConsts.h"
#include "bncs_auto_helper.h"

class CAudioPackages
{

private:

	struct stConf_IFB_Details {
		int i_in_ring;
		int i_in_port;
		int i_out_ring;
		int i_out_port;
		bncs_string m_ss_MainLabel;
		bncs_string m_ss_SubTitleLabel;
		int i_DynAssignedIfb;
		int i_FlagAssignCFConf;       // flag for cf conf enabled 1 or not 0 - used for CF 
		int i_DynAssignedConference;	      // assigned conf number
		int i_Use_Which_DestPkgIFB;   // 1 or 2 - ifb routing buttons
	};
	// conf main / sub title labels from db68,  ifb labels from db67

	struct stAssocIndexPairTag {
		int iAssocIndex;
		int iAssocHub_or_TypeTag;
		int iAssocLang_or_UseTag;
	};

public:
	CAudioPackages( );
	~CAudioPackages();

	int m_iAudioPkg_Indx;
	bncs_string m_ssPackageName;
	bncs_string m_ssButtonName;

	bncs_string m_ss_PackageKeyInfo;      // DB62 raw list - owner, delegate, lang use tag
	bncs_string m_ss_PackageDefinition;       // DB63 raw list - video link, tags, rev vision, riedel
	bncs_string m_ss_AudioSourceDefinition16;       // DB64 raw list - audio index and tag 1-16 - as pair of data
	bncs_string m_ss_AudioReturnDefinition;       // DB65 raw list - 4 return pairs and tags
	bncs_string m_ss_AudioCFeedLabelSubtitles;
	bncs_string m_ss_AudioConfLabelSubtitles;

	bncs_string m_ssUniqueID;
	int m_iOwnerType;
	int m_iDelegateType;
	int m_iOveralLanguageTag;

	struct stAssocIndexPairTag st_VideoLink_Vision;

	struct stAssocIndexPairTag st_Reverse_Vision;
	int iEnabledRevVision;         // flag  via infodriver slot: 0=turned off  1=enabled  for reverse routing for dest pkg has rev vision /audio  and if top of MMQ

	int iRoutedRevVisionPkg;
	int iRoutedRevAudioPkg;

	struct stAssocIndexPairTag st_AudioSourcePair[MAX_AUDLVL_AUDIO];  // 1..16
	struct stAssocIndexPairTag st_AudioReturnPair[MAX_PKG_VIDEO];  // 1..4

	struct stConf_IFB_Details stCFIFBs[MAX_PKG_CONFIFB]; // 1..4 - work along side ifbs ports  -

	struct stConf_IFB_Details stTBConferences[MAX_PKG_CONFIFB]; // 1..2 conferences assignable from pool

	int m_iVideoLinkIndex;    // db66
	
	bncs_stringlist ssl_TiedToSourcePackages;

	bncs_stringlist ssl_Pkg_Queue;

};

