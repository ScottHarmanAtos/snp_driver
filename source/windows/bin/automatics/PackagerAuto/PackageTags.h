
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CPackageTags
{
public:

	int m_iTagDBType;   // indicative of which tag db
	int m_iTagDBIndex; // index in db file 1-300

	bncs_string m_ssTag_AbrevName;
	bncs_string m_ssTag_DescString;
	BOOL m_bTagInUse;

	CPackageTags( );
	~CPackageTags();
};

