#pragma once

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>

#include "PackageTags.h"
#include "PackagerConsts.h"
#include "bncs_auto_helper.h"

class CCompInstances
{
public:
	CCompInstances(bncs_string ssInst);
	~CCompInstances();
	// composites
	bncs_string ssOverallComposite;
	// composite groups
	bncs_string ssRouterComposite;
	bncs_string ssMixMinusComposite;
	bncs_string ssAPReturnsComposite;
	bncs_string ssDestStatusComposite;
	bncs_string ssSrcePTIComposite;
	bncs_string ssNevionDestUseComposite;
	bncs_string ssPkgrRiedelComposite;
	// various device numbers for composite elements instances - never expected to be more than 32 ( ie 128000 packages )
	int i_dev_Pkgr_Router_Main[33];
	int i_dev_Pkgr_MixMinus_Main[33];
	int i_dev_Pkgr_APReturns_Main[33];
	int i_dev_Pkgr_DestStatus_Main[33];
	int i_dev_Pkgr_SrcePTI_Main[33];
	int i_dev_Pkgr_NevionDestUse_Main[33];
	int i_dev_Pkgr_PkgrRiedel_Main[33];

};

