#include "CompInstances.h"


CCompInstances::CCompInstances( bncs_string ssInst )
{
	ssOverallComposite=ssInst;
	// composite groups
	ssRouterComposite="";
	ssMixMinusComposite = "";
	ssAPReturnsComposite = "";
	ssDestStatusComposite = "";
	ssSrcePTIComposite = "";
	ssNevionDestUseComposite = "";
	ssPkgrRiedelComposite = "";
	//
	for (int ii = 0; ii < 33; ii++) {
		i_dev_Pkgr_Router_Main[ii] = 0;
		i_dev_Pkgr_MixMinus_Main[ii] = 0;
		i_dev_Pkgr_APReturns_Main[ii] = 0;
		i_dev_Pkgr_DestStatus_Main[ii] = 0;
		i_dev_Pkgr_SrcePTI_Main[ii] = 0;
		i_dev_Pkgr_NevionDestUse_Main[ii] = 0;
		i_dev_Pkgr_PkgrRiedel_Main[ii] = 0;
	}
}


CCompInstances::~CCompInstances()
{
}
