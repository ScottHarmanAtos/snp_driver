#include "SportEvent.h"


CSportEvent::CSportEvent()
{
	m_iKeyIndex=0;
	m_ssName="";    // bncs or button name

	m_ssEventID = "";
	m_ssAssetID = "";
	m_ssClassID = "";
	m_ssTeam1ID = "";
	m_ssTeam2ID = "";

	m_ssStartDate = "";
	m_ssStartTime = "";
	m_ssEndDate = "";
	m_ssEndTime = "";

	m_iSPEV_Status = 0;
	m_iAssociated_SourcePackage = 0;
	m_ss_TimeDefinition = "";
	m_ss_TeamDefinition = "";
}


CSportEvent::~CSportEvent()
{
}
