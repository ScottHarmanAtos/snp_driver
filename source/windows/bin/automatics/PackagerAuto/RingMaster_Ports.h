#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CRingMaster_Ports
{
public:
	CRingMaster_Ports();
	~CRingMaster_Ports();

	int iTableIndex;
	int iRingTrunk;
	int iRingPort;

	int iPortsInfodrv;
	int iPortsInfodrvSlot;

	// actual rev from ringmaster - includes trace + ultimate source, locked or not
	bncs_string ss_RingMasterRevertive; 

	int iTracedRing;        // extracted from Rev
	int iTracedSource;
	int iPortLocked;

};

