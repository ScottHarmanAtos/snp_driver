/* PackagerAuto.cpp - custom program file for project PackagerAuto */

#include "stdafx.h"
#include "PackagerAuto.h"

/* TODO: place references to your functions and global variables in the PackagerAuto.h file */

// local function prototypes
// this function is called when a valid slotchange notification is extracted from the InfoNotify
BOOL SlotChange(int iWhichInfoType, int iAssocPkgIndex, UINT iDevice, UINT iSlot, LPCSTR szSlot);

void ClearConferencePortMembers(int iConfIndex );
void ClearAllConferenceKeyMembers(CRingMaster_CONF* pConf );

void ReAssignLabelForIFBS(CAudioPackages* pPkg, int iWhichIFB );
void AssignDestinationIFBtoSrcIFBS(CAudioPackages* pPkg );

void UpdateDestPkgSdiStatusInfodriverSlot(int iIndex, BOOL bSetSlot );
void ProcessMMStackCommand( CAudioPackages* pPkg, bncs_string ssCmd );
void ProcessAudioPackageReverseRouting(int iAudioPkg, int iRoutingAction);
BOOL ProcessDestinationPkgCommand( CDestinationPackages* pDstPkg, bncs_string ssCmd );

void UpdateDestinationPackageForMMQ(int iDestPkg, int iAudioPkgIndex, int iActionAddorRemove);
void ProcessDynamicConferenceCommand(int iAPIndex, bncs_string ssCmd);


//////////////////////////////////////////////////////////////////////////
// 
//  COMMAND QUEUE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


void ClearCommandQueue( void )
{
	while (!qCommands.empty()) {
		CCommand* pCmd;
		pCmd = qCommands.front();
		if (pCmd) {
			qCommands.pop();
			delete pCmd;
		}
	}
}
 
void AddCommandToQue( LPCSTR szCmd, int iCmdType, int iDev, int iSlot, int iDB )
{
	CCommand* pCmd = new CCommand;
	if (pCmd) {
		wsprintf( pCmd->szCommand, szCmd );
		pCmd->iTypeCommand = iCmdType;
		pCmd->iWhichDevice = iDev;
		pCmd->iWhichSlotDest = iSlot;
		pCmd->iWhichDatabase = iDB;
		qCommands.push( pCmd );
	}
	else 
		Debug( "AddCommand2Q - cmd record not created");
	if (qCommands.size() > iHighestCommandQueue) {
		iHighestCommandQueue = qCommands.size();
		char szval[64] = "";
		wsprintf(szval, "q max = %d ", iHighestCommandQueue);
		SetDlgItemText(hWndDlg, IDC_CMD_QUEUE2, szval);
	}
}


void ProcessNextCommand( int iRateRevs ) 
{

	if (iPauseCommandProcessing > 0) {
		char szData[64] = "";
		wsprintf(szData, "PAUSED for %d secs", iPauseCommandProcessing);
		SetDlgItemText(hWndDlg, IDC_CMD_QUEUE2, szData);
		Debug("COMMAND PROCESSING PAUSED %d seconds remaining", iPauseCommandProcessing);
		if (bNextCommandTimerRunning) {
			KillTimer(hWndMain, COMMAND_TIMER_ID);
			bNextCommandTimerRunning = FALSE;
		}
		return;
	}

	BOOL bContinue=TRUE;
	do {
		if (qCommands.size()>0) {
			iRateRevs--;
			// something in queue so process it
			CCommand* pCommand = qCommands.front();
			qCommands.pop();
			if (pCommand) {
				// ONLY send cmd if main driver only or in starting mode for rx driver to register and poll initially
				if ((iOverallTXRXModeStatus==IFMODE_TXRX)||(bAutomaticStarting)) {
					int iWhichDevice = pCommand->iWhichDevice;
					int iWhichDB = pCommand->iWhichDatabase;
					int iWhichSlot = pCommand->iWhichSlotDest;
					strcpy(szGloCommand, pCommand->szCommand);
					switch(pCommand->iTypeCommand){
						case ROUTERCOMMAND:	ecCSIClient->txrtrcmd(szGloCommand);	break;
						case INFODRVCOMMAND: ecCSIClient->txinfocmd(szGloCommand); break;
						case GPIOCOMMAND:	ecCSIClient->txgpicmd(szGloCommand); break;
						case DATABASECOMMAND:
							// was eiInfoDrivers[AUTO_PACK_ROUTE_INFO].setdbname(iPackager_router_main, iWhichDB, iWhichSlot, szGloCommand, FALSE);
							ecCSIClient->setdbname(iWhichDevice, iWhichDB, iWhichSlot, pCommand->szCommand, FALSE);
						break;
					default:
						Debug("ProcessNextCmd - unknown type from buffer - %s", szGloCommand);
					} // switch
					if ((bShowAllDebugMessages) && (!bAutomaticStarting)) Debug("ProcessCommand - TX> %s ", szGloCommand);
				}
				else {
					// as in RXonly mode - Clear the buffer - no need to go round loop
					ClearCommandQueue();
					bContinue=FALSE;
					iRateRevs=0;
				}
			}
			delete pCommand; // delete record as now finished 
		}
		else {
			// nothing else in queue
			bContinue = FALSE;
			iRateRevs=0;
		}
	} while ((bContinue)&&(iRateRevs>0));

	// if more commands still in queue and timer not running then start timer, else perhaps kill it
	if (qCommands.size()>0) {
		if (!bNextCommandTimerRunning) {
			SetTimer(hWndMain, COMMAND_TIMER_ID, 100, NULL); // timer for start poll
			bNextCommandTimerRunning = TRUE;
		}
	}
	else {
		if (bNextCommandTimerRunning) {
			KillTimer(hWndMain, COMMAND_TIMER_ID);
			bNextCommandTimerRunning = FALSE;
		}
	}

}


void SendPackagerBNCSCommand(int iAssocIndx, int iCmdInfoType, LPCSTR szCmd)
{
	// determine which infodriver to send command to 
	int iWhichInfo = 0;
	int iWhichSlot = ((iAssocIndx - 1) % iInfodriverDivisor) + 1;
	if (iCmdInfoType == AUTO_PACK_ROUTE_INFO) {
		iWhichInfo = iPackager_router_main + ((iAssocIndx - 1) / iInfodriverDivisor);
		Debug("SendPkgrCmd - pkg %d info %d slot %d cmd %s", iAssocIndx, iWhichInfo, iWhichSlot, szCmd);
		if ((iWhichInfo >= iPackager_router_main)&&(iWhichSlot>0)&&(iWhichSlot<=iInfodriverDivisor)) {
			wsprintf(szGloCommand, "IW %d '%s' %d", iWhichInfo, szCmd, iWhichSlot);
			if (ecCSIClient) ecCSIClient->txinfocmd(szGloCommand);
		}
	}
}


void SendPackagerBNCSRevertive(int iAssocIndx, int iRevInfoType, BOOL bSetSlot, LPCSTR szRev)
{
	// determine which infodriver to send command to 
	int iBaseInfo = 0;
	int iWhichInfo = 0;
	int iWhichSlot = 0;
	if (iRevInfoType == AUTO_RIEDEL_CONFERENCE_INFO) {
		iWhichInfo = (AUTO_RTR_DEST_USE_INFO * iNumberInfosInBlock) + 1;
		iWhichSlot = iAssocIndx;
	}
	else if (iRevInfoType == AUTO_RIEDEL_PORT_USE_INFO) {
		iWhichInfo = (AUTO_RTR_DEST_USE_INFO * iNumberInfosInBlock) + 2;
		iWhichSlot = iAssocIndx;
	}
	else if (iRevInfoType == AUTO_RIEDEL_IFBS_INFO) {
		iWhichInfo = (AUTO_RTR_DEST_USE_INFO * iNumberInfosInBlock) + 3;
		iWhichSlot = iAssocIndx;
	}
	else {
		iBaseInfo = ((iRevInfoType - 1)*iNumberInfosInBlock) + 1;
		iWhichInfo = iBaseInfo + ((iAssocIndx - 1) / iInfodriverDivisor);
		iWhichSlot = ((iAssocIndx - 1) % iInfodriverDivisor) + 1;
	}
	//if (bShowAllDebugMessages) Debug("SendPkgrRev - assoc Indx %d type %d base %d info %d slot %d cmd %s", iAssocIndx, iRevInfoType, iBaseInfo, iWhichInfo, iWhichSlot, szRev);
	if ((iWhichInfo >= 1) && (iWhichInfo < MAX_INFODRIVERS) && (iWhichSlot>0) && (iWhichSlot <= 4096)) {
		if (eiInfoDrivers[iWhichInfo].iDevice > 0) {
			strcpy(szGloRevertive, szRev);
			if (bSetSlot)
				eiInfoDrivers[iWhichInfo].setslot(iWhichSlot, szGloRevertive);
			else
				eiInfoDrivers[iWhichInfo].updateslot(iWhichSlot, szGloRevertive);
		}
	}
}


char* GetPackagerInfodriverSlotContents(int iAssocIndx, int iRevInfoType)
{
	strcpy(szGloRevertive, ""); // use global string to return data
	// determine which infodriver to send command to 
	int iBaseInfo = 0;
	int iWhichInfo = 0;
	int iWhichSlot = 0;
	if (iRevInfoType == AUTO_RIEDEL_CONFERENCE_INFO) {
		iWhichInfo = (AUTO_RTR_DEST_USE_INFO * iNumberInfosInBlock) + 1;
		iWhichSlot = iAssocIndx;
	}
	else if (iRevInfoType == AUTO_RIEDEL_PORT_USE_INFO) {
		iWhichInfo = (AUTO_RTR_DEST_USE_INFO * iNumberInfosInBlock) + 2;
		iWhichSlot = iAssocIndx;
	}
	else if (iRevInfoType == AUTO_RIEDEL_IFBS_INFO) {
		iWhichInfo = (AUTO_RTR_DEST_USE_INFO * iNumberInfosInBlock) + 3;
		iWhichSlot = iAssocIndx;
	}
	else {
		iBaseInfo = ((iRevInfoType - 1)*iNumberInfosInBlock) + 1;
		iWhichInfo = iBaseInfo + ((iAssocIndx - 1) / iInfodriverDivisor);
		iWhichSlot = ((iAssocIndx - 1) % iInfodriverDivisor) + 1;
	}
	if ((iWhichInfo >= 1) && (iWhichInfo < MAX_INFODRIVERS) && (iWhichSlot>0) && (iWhichSlot <= 4096)) {
		if (eiInfoDrivers[iWhichInfo].iDevice > 0) {
			eiInfoDrivers[iWhichInfo].getslot(iWhichSlot, szGloRevertive);
		}
	}
	return szGloRevertive;
}


//////////////////////////////////////////////////////////////////////////
// 
//  MAP PACKAGE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////

void ClearAutomaticMaps()
{
	// router classes
	if (cl_RouterRecords) {
		while (cl_RouterRecords->begin() != cl_RouterRecords->end()) {
			map<int, CRevsRouterData*>::iterator it = cl_RouterRecords->begin();
			if (it->second) delete it->second;
			cl_RouterRecords->erase(it);
		}
		delete cl_RouterRecords;
	}

	// riedel classes
	if (cl_Riedel_Rings) {
		while (cl_Riedel_Rings->begin() != cl_Riedel_Rings->end()) {
			map<int, CRiedel_Rings*>::iterator it = cl_Riedel_Rings->begin();
			if (it->second) delete it->second;
			cl_Riedel_Rings->erase(it);
		}
		delete cl_Riedel_Rings;
	}
	if (cl_RingMaster_Conferences) {
		while (cl_RingMaster_Conferences->begin() != cl_RingMaster_Conferences->end()) {
			map<int, CRingMaster_CONF*>::iterator it = cl_RingMaster_Conferences->begin();
			if (it->second) delete it->second;
			cl_RingMaster_Conferences->erase(it);
		}
		delete cl_RingMaster_Conferences;
	}
	if (cl_RingMaster_IFBs) {
		while (cl_RingMaster_IFBs->begin() != cl_RingMaster_IFBs->end()) {
			map<int, CRingMaster_IFB*>::iterator it = cl_RingMaster_IFBs->begin();
			if (it->second) delete it->second;
			cl_RingMaster_IFBs->erase(it);
		}
		delete cl_RingMaster_IFBs;
	}
	if (cl_RingMaster_Ports) {
		while (cl_RingMaster_Ports->begin() != cl_RingMaster_Ports->end()) {
			map<int, CRingMaster_Ports*>::iterator it = cl_RingMaster_Ports->begin();
			if (it->second) delete it->second;
			cl_RingMaster_Ports->erase(it);
		}
		delete cl_RingMaster_Ports;
	} 

	if (cl_RingMaster_Logic) {
		while (cl_RingMaster_Logic->begin() != cl_RingMaster_Logic->end()) {
			map<int, CRingMaster_LOGIC*>::iterator it = cl_RingMaster_Logic->begin();
			if (it->second) delete it->second;
			cl_RingMaster_Logic->erase(it);
		}
		delete cl_RingMaster_Logic;
	}

	if (map_RiedelPortPackageUse) {
		while (map_RiedelPortPackageUse->begin() != map_RiedelPortPackageUse->end()) {
			map<int,CRiedelPortPackageUse*>::iterator it = map_RiedelPortPackageUse->begin();
			if (it->second) delete it->second;
			map_RiedelPortPackageUse->erase(it);
		}
		delete map_RiedelPortPackageUse;
	}
	
	if (mapOfAllSrcPackages) {
		while (mapOfAllSrcPackages->begin() != mapOfAllSrcPackages->end()) {
			map<int, CSourcePackages*>::iterator it = mapOfAllSrcPackages->begin();
			if (it->second) delete it->second;
			mapOfAllSrcPackages->erase(it);
		}
		delete mapOfAllSrcPackages;
	}
	if (mapOfAllAudioPackages) {
		while (mapOfAllAudioPackages->begin() != mapOfAllAudioPackages->end()) {
			map<int, CAudioPackages*>::iterator it = mapOfAllAudioPackages->begin();
			if (it->second) delete it->second;
			mapOfAllAudioPackages->erase(it);
		}
		delete mapOfAllAudioPackages;
	}
	if (mapOfAllSportingEvents) {
		while (mapOfAllSportingEvents->begin() != mapOfAllSportingEvents->end()) {
			map<int, CSportEvent*>::iterator it = mapOfAllSportingEvents->begin();
			if (it->second) delete it->second;
			mapOfAllSportingEvents->erase(it);
		}
		delete mapOfAllSportingEvents;
	}
	if (mapOfAllDestPackages) {
		while (mapOfAllDestPackages->begin() != mapOfAllDestPackages->end()) {
			map<int, CDestinationPackages*>::iterator it = mapOfAllDestPackages->begin();
			if (it->second) delete it->second;
			mapOfAllDestPackages->erase(it);
		}
		delete mapOfAllDestPackages;
	}

}

////////////////////////////////////////////////////////////////////////////////////////
//
// get records from maps
//
CRingMaster_Ports* GetRingPortsDefintion(int iIndex)
{
	if ((iIndex>0) && (cl_RingMaster_Ports)) {
		map<int, CRingMaster_Ports*>::iterator itp;
		itp = cl_RingMaster_Ports->find(iIndex);
		if (itp != cl_RingMaster_Ports->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRingMaster_Ports* GetRingPortsDefintionByRingDest(int iFromRing, int iFromDest)
{
	// go thru map reg and compare new source and router
	if (cl_RingMaster_Ports) {
		map<int, CRingMaster_Ports*>::iterator itp = cl_RingMaster_Ports->begin();
		while (itp != cl_RingMaster_Ports->end()) {
			if (itp->second) {
				if ((itp->second->iRingTrunk == iFromRing) && (itp->second->iRingPort == iFromDest)) {
					return itp->second;
				}
			}
			itp++;
		} // while
	}
	return NULL;
}

CRingMaster_Ports* GetRingPortsDefintionByInfodriverSlot(int iInfodriver, int iSlot)
{
	// go thru map reg and compare new source and router
	if (cl_RingMaster_Ports) {
		map<int, CRingMaster_Ports*>::iterator itp = cl_RingMaster_Ports->begin();
		while (itp != cl_RingMaster_Ports->end()) {
			if (itp->second) {
				if ((itp->second->iPortsInfodrv == iInfodriver) && (itp->second->iPortsInfodrvSlot == iSlot)) {
					return itp->second;
				}
			}
			itp++;
		} // while
	}
	return NULL;
}

//////////////// get ringmaster records ///////////////

CRiedel_Rings* GetRiedel_Rings_Record(int iIndex)
{
	if (cl_Riedel_Rings) {
		map<int, CRiedel_Rings*>::iterator itp;
		itp = cl_Riedel_Rings->find(iIndex);
		if (itp != cl_Riedel_Rings->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CRiedel_Rings* GetRiedel_Rings_By_TrunkAddress(int iAddress)
{
	if (cl_Riedel_Rings) {
		map<int, CRiedel_Rings*>::iterator itp;
		itp = cl_Riedel_Rings->begin();
		while (itp != cl_Riedel_Rings->end()) {
			CRiedel_Rings* pRing = itp->second;
			if (pRing) {
				if (pRing->m_iTrunkAddress == iAddress) return pRing;
			}
			itp++;
		}
	}
	return NULL;
}


CRiedel_Rings* GetRiedel_Rings_By_GRD_Device(int iGRDDevice)
{
	if (cl_Riedel_Rings) {
		map<int, CRiedel_Rings*>::iterator itp;
		itp = cl_Riedel_Rings->begin();
		while (itp != cl_Riedel_Rings->end()) {
			CRiedel_Rings* pRing = itp->second;
			if (pRing) {
				if (pRing->iRing_Device_GRD == iGRDDevice) return pRing;
			}
			itp++;
		}
	}
	return NULL;
}


CRingMaster_IFB* GetRingMaster_IFB_Record(int iIFBRecIndex)
{
	if (cl_RingMaster_IFBs) {
		map<int, CRingMaster_IFB*>::iterator itp;
		itp = cl_RingMaster_IFBs->find(iIFBRecIndex);
		if (itp != cl_RingMaster_IFBs->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CRingMaster_CONF* GetRingMaster_CONF_Record(int iCONF)
{
	if (cl_RingMaster_Conferences) {
		map<int, CRingMaster_CONF*>::iterator itp;
		itp = cl_RingMaster_Conferences->find(iCONF);
		if (itp != cl_RingMaster_Conferences->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CRingMaster_LOGIC* GetRingMaster_LOGIC_Record(int iLogic)
{
	if (cl_RingMaster_Logic) {
		map<int, CRingMaster_LOGIC*>::iterator itp;
		itp = cl_RingMaster_Logic->find(iLogic);
		if (itp != cl_RingMaster_Logic->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

//////////////// get riedel revertive records  //////////////////////////

/*
CRiedel_GRDs* GetRiedel_GRD_Record(int iGrdIndex)
{
map<int, CRiedel_GRDs*>::iterator itp;
itp = cl_Riedel_GrdRecords->find(iGrdIndex);
if (itp != cl_Riedel_GrdRecords->end()) {
if (itp->second) return itp->second;
}
return NULL;
}

CRiedel_GRDs* GetRiedel_GRD_By_TrunkAddr(int iTrunk)
{
map<int, CRiedel_GRDs*>::iterator itp;
itp = cl_Riedel_GrdRecords->begin();
while (itp != cl_Riedel_GrdRecords->end()) {
CRiedel_GRDs* pGrd = itp->second;
if (pGrd) {
if (pGrd->getRingTrunk() == iTrunk) return pGrd;
}
itp++;
}
return NULL;
}
*/

//////////////////// get video router records 

CRevsRouterData* GetRouterRecord(int iKey)
{
	if ((iKey>0) && (cl_RouterRecords)) {
		map<int, CRevsRouterData*>::iterator itp;
		itp = cl_RouterRecords->find(iKey);
		if (itp != cl_RouterRecords->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CSourcePackages* GetSourcePackage(int iIndex)
{
	// extra 2 are PARK and BARS
	if ((iIndex >= 0) && (iIndex<=(iNumberOfPackages+2) ) && (mapOfAllSrcPackages)) {
		map<int, CSourcePackages*>::iterator itp;
		itp = mapOfAllSrcPackages->find(iIndex);
		if (itp != mapOfAllSrcPackages->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CAudioPackages* GetAudioPackage(int iIndex)
{
	// internal to packager 
	if ((iIndex > 0) && (mapOfAllAudioPackages)) {
		map<int, CAudioPackages*>::iterator itp;
		itp = mapOfAllAudioPackages->find(iIndex);
		if (itp != mapOfAllAudioPackages->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CSportEvent* GetSportEventPackage(int iIndex)
{
	// internal to packager 
	if ((iIndex > 0) && (mapOfAllSportingEvents)) {
		map<int, CSportEvent*>::iterator itp;
		itp = mapOfAllSportingEvents->find(iIndex);
		if (itp != mapOfAllSportingEvents->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CDestinationPackages* GetDestinationPackage(int iIndex)
{
	if ((iIndex>0) && (iIndex<=iNumberOfPackages) && (mapOfAllDestPackages)) {
		map<int,CDestinationPackages*>::iterator itp;
		itp = mapOfAllDestPackages->find(iIndex);
		if (itp!=mapOfAllDestPackages->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CPackageTags* GetPackageTag(int iTagIndex, int iTagType)
{
	// internal to packager 
	if ((mapOfAllTags)) {
		map<int, CPackageTags*>::iterator itp;
		itp = mapOfAllTags->begin();
		while (itp != mapOfAllTags->end()) {
			CPackageTags* pTag = itp->second;
			if (pTag) {
				if ((pTag->m_iTagDBIndex == iTagIndex) && (pTag->m_iTagDBType == iTagType)) return pTag;
			}
			itp++;
		}
	}
	return NULL;
}

void GetTrunkAndPortFromString(bncs_string ssParams, int iDefaultTrunk, int* iTrunk, int* iPort)
{
	// ideally it should be <trunk>.<port> -- if not treated as just a port if only 1 parameter
	*iTrunk = 0;
	*iPort = 0;
	bncs_stringlist ssl = bncs_stringlist(ssParams, '.');
	if (ssl.count() > 1) {
		*iTrunk = ssl[0].toInt();
		*iPort = ssl[1].toInt();
	}
	else if (ssl.count() == 1) {
		*iTrunk = iDefaultTrunk;
		*iPort = ssl[0].toInt();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////

CRiedelPortPackageUse* Get_RiedelPortPackageUseRecord(int iIndex)
{
	if ((iIndex>0) && (iIndex<MAX_RIEDEL_SLOTS) && (map_RiedelPortPackageUse)) {
		map<int, CRiedelPortPackageUse*>::iterator itp;
		itp = map_RiedelPortPackageUse->find(iIndex);
		if (itp != map_RiedelPortPackageUse->end()) {  // was the entry found
			return itp->second;
		}
	}
	return NULL;
}


//////////////////////////////////////////////////////////////////////////
// 
BOOL AreBncsStringsEqual( bncs_string ss1, bncs_string ss2 )
{
	if (ss1.length()==ss2.length()) {
		if (ss1.length()==0)
			return TRUE;
		if (ss1.find(ss2)>=0)
			return TRUE;
	}
	return FALSE;
}

//////////////////////////////////////////////////////////////////////////

int LoadDBFile(int iWhichDevice, int iWhichDB, int iMaxReadsExpected)
{
	int iReadCounter = 0;
	int iHighestIndexRead = 0;
	BOOL bContinue = TRUE;

	if (mapOfDBFile) {

		char* szCCRoot = getenv("CC_ROOT");
		char* szCCSystem = getenv("CC_SYSTEM");
		char szFilename[MAX_AUTO_BUFFER_STRING];
		if (iWhichDB>9)
			wsprintf(szFilename, "%s\\%s\\config\\system\\dev_%03d.db%04d", szCCRoot, szCCSystem, iWhichDevice, iWhichDB);
		else
			wsprintf(szFilename, "%s\\%s\\config\\system\\dev_%03d.db%d", szCCRoot, szCCSystem, iWhichDevice, iWhichDB);
		Debug("Loading DB file data from %s", szFilename);

		FILE *fp = fopen(szFilename, "r");
		if (fp != NULL) {
			// now read data entries
			do {
				char szData[512] = "";
				if (fgets(szData, 512, fp) != NULL) {
					if (strlen(szData) > 0) {
						iReadCounter++;
						// split string into two parts from index=blah data
						char *acParams[4];
						int iIndex = 0;
						int iParamCount = SplitString(szData, "=", 2, acParams);
						if (iParamCount >= 2) {
							iIndex = atoi(acParams[0]);
							if ((iIndex > 0) && (iIndex <= iMaxReadsExpected)) {
								//if ((iIndex % 1000) == 0) Debug("LOAD DB - read data %d > %s", iIndex, acParams[1]);
								if (iIndex>iHighestIndexRead) iHighestIndexRead = iIndex;
								CCommand* pCmd = new CCommand();
								if (pCmd) {
									pCmd->iWhichSlotDest = iIndex;
									pCmd->iWhichDatabase = iWhichDB;
									strcpy(pCmd->szCommand, acParams[1]);
									mapOfDBFile->insert(pair<int, CCommand*>(iIndex, pCmd));
								}
							}
						}
						if ((iReadCounter > (iMaxReadsExpected + 1)) || (iIndex == (iMaxReadsExpected))) bContinue = FALSE;
					}
				}
				else
					bContinue = FALSE;
			} while (bContinue);
			int iCloseState = fclose(fp);
			Debug("ProcessFirstPass - LOAD DB %d -close status %d -  file read total %d ", iWhichDB, iCloseState, iReadCounter);
		}
		else
			Debug("ProcessFirstPass - LOAD DB %d - file open failed", iWhichDB);
	}
	else
		Debug("ProcessFirstPass - LOAD DB %d - ERROR NO MAP class", iWhichDB);
	//
	return iHighestIndexRead;
}

CCommand* GetDBFileRecord(int iKey)
{
	if ((iKey>0) && (mapOfDBFile)) {
		map<int, CCommand*>::iterator itp;
		itp = mapOfDBFile->find(iKey);
		if (itp != mapOfDBFile->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

///////////// VIRTUALS additions ////////////////////

//////////////////////////////////////////////////////////////////////////
// recursive function to
// get traced source package for a given dest pkg
//

int TraceRealSourcePackageCalcTrace(int iStartDestPkg)
{
	// for given dest pkg trace back thru any virtuals until real src pkg found
	// if a virtual has a src sdi defined - then stop at this point as this will video required
	// considered "up-stream" 
	int iRoutedSrcPkg = 0;
	int iDestPkg = iStartDestPkg;
	CDestinationPackages* pDest1 = GetDestinationPackage(iDestPkg);
	if (pDest1) {
		iRoutedSrcPkg = pDest1->i_Routed_Src_Package;
		CSourcePackages* pSrc1 = GetSourcePackage(iRoutedSrcPkg);
		if (pSrc1) {
			ssl_Calc_Trace.prepend(iRoutedSrcPkg);
			if ((iRoutedSrcPkg >= i_Start_VirtualPackages) && (iRoutedSrcPkg<=iNumberOfPackages)) {
				// virtual src - check for defined video - else get vir dest and carry on trace (recursive)
				int iNextStartIndx = pSrc1->iPackageIndex;
				iRoutedSrcPkg = TraceRealSourcePackageCalcTrace(iNextStartIndx); // vir src wraps round into vir dest
			}
			else {	// found real source package so return this index
				//if (bShowAllDebugMessages) Debug("TraceSrcPkgVid - real src pkg %d has sdi %d", iRoutedSrcPkg, pSrc1->i_Device_Srce_SD );
				return pSrc1->iPackageIndex;
			}
		}
		else {
			Debug("TraceSrcPkgVid - invalid src pkg %d - exiting recursion", iRoutedSrcPkg);
			return UNKNOWNVAL;
		}
	}
	else {
		Debug("TraceSrcPkgVid - invalid dest pkg %d - exiting recursion", iDestPkg);
		return UNKNOWNVAL;
	}
	//
	return iRoutedSrcPkg;
}


void TraceVirtualForward(int iSrcToFind, int iVirSrcPkg)
{
	if ((iVirSrcPkg >= i_Start_VirtualPackages) && (iVirSrcPkg<=iNumberOfPackages)) {
		// check on number times function has been called - and limit 
		iRecursiveCounter++;
		if (iRecursiveCounter>1000) {
			Debug("TraceVirtualForward - recursive counter >1000 - aborting function exection src %d vir %d", iSrcToFind, iVirSrcPkg);
			return;
		}
		int iSrcPkg = iVirSrcPkg;
		CSourcePackages* pSrc1 = GetSourcePackage(iSrcPkg);
		if (pSrc1) {
			if (pSrc1->ssl_Pkg_DirectlyRoutedTo.find(iSrcToFind) >= 0) {
				Debug("TraceVirtualForward - found src %d being looked for", iSrcToFind);
				bFoundVirtualSourceAhead = TRUE;
				return;
			}
			else {
				// check any virtuals this vir pkg is routed to
				for (int ic = 0; ic<pSrc1->ssl_Pkg_DirectlyRoutedTo.count(); ic++) {
					int iNextSrc = pSrc1->ssl_Pkg_DirectlyRoutedTo[ic].toInt();
					if ((iNextSrc >= i_Start_VirtualPackages) && (iNextSrc<=iNumberOfPackages)) {
						TraceVirtualForward(iSrcToFind, iNextSrc);
					}
				}
				return;
			}
		}
	}
	return;
}


void BuildDownStreamDestList(int iGivenPkg)
{
	// add given pkg to list
	if (ssl_RoutedDestPackages.find(iGivenPkg)<0) ssl_RoutedDestPackages.append(iGivenPkg);
	// downstream packages are those that a virtual is routed to
	if ((iGivenPkg >= i_Start_VirtualPackages) && (iGivenPkg<=iNumberOfPackages)) {
		CSourcePackages* pSrc1 = GetSourcePackage(iGivenPkg);
		if (pSrc1) {
			// copy entries from directly routed to list into global trace list
			for (int ie = 0; ie<pSrc1->ssl_Pkg_DirectlyRoutedTo.count(); ie++) {
				int iD = pSrc1->ssl_Pkg_DirectlyRoutedTo[ie];
				// check to see if entry already in list before adding it -- if it is - does it imply a recurring loop ??
				if (ssl_RoutedDestPackages.find(iD)<0) {
					ssl_RoutedDestPackages.append(iD);
				}
				else {
					iRecursiveCounter++;
					if (iRecursiveCounter>100) {
						Debug("BuildDownStreamList-Recursive Counter exceeded limit - virsrc %d package %d Aborting function", iGivenPkg, iD);
						return;
					}
				}
				if ((iD >= i_Start_VirtualPackages) && (iD<=iNumberOfPackages)) {
					BuildDownStreamDestList(iD);
				}
			}
			return;
		}
	}
	return;
}



//////////////////////////////////////////////////////////////////////////
// 
//  AUTOMATIC GUI FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


void ClearAudioSrcPkgData()
{
	SetDlgItemText(hWndDlg, IDC_AUDIO_NAME1, "");
	SetDlgItemText(hWndDlg, IDC_AUDIO_NAME2, "");
	SetDlgItemText(hWndDlg, IDC_AUDIO_DATA1, "");
	SetDlgItemText(hWndDlg, IDC_AUDIO_DATA2, "");
	SetDlgItemText(hWndDlg, IDC_AUDIO_DATA3, "");
	SetDlgItemText(hWndDlg, IDC_AUDIO_DATA4, "");
	SetDlgItemText(hWndDlg, IDC_AUDIO_TOMSP, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_IFB1, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_IFB2, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_IFB3, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_IFB4, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_4W1, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_4W2, "");
	SetDlgItemText(hWndDlg, IDC_APKG_REVENA, "");
	SetDlgItemText(hWndDlg, IDC_APKG_REVVIS, "");
}


void DisplayAudioSourcePackageData()
{
	ClearAudioSrcPkgData();
	if ((iChosenAudioSrcPkg > 0) && (iChosenAudioSrcPkg < (iNumberOfPackages+1))) {
		CAudioPackages* pPkg = GetAudioPackage(iChosenAudioSrcPkg);
		if (pPkg) {
			SetDlgItemText(hWndDlg, IDC_AUDIO_NAME1, LPCSTR(pPkg->m_ssButtonName));
			SetDlgItemText(hWndDlg, IDC_AUDIO_NAME2, LPCSTR(pPkg->m_ssPackageName));
			SetDlgItemText(hWndDlg, IDC_AUDIO_DATA1, LPCSTR(pPkg->m_ss_PackageKeyInfo));
			SetDlgItemText(hWndDlg, IDC_AUDIO_DATA2, LPCSTR(pPkg->m_ss_PackageDefinition));
			SetDlgItemText(hWndDlg, IDC_AUDIO_DATA3, LPCSTR(pPkg->m_ss_AudioSourceDefinition16));
			SetDlgItemText(hWndDlg, IDC_AUDIO_DATA4, LPCSTR(pPkg->m_ss_AudioReturnDefinition));
			SetDlgItemText(hWndDlg, IDC_AUDIO_TOMSP, LPCSTR(pPkg->ssl_TiedToSourcePackages.toString(',')));
			// display mm que AND COMMS
			bncs_string ssdata = "";
			ssdata = bncs_string("%1.%2/%3.%4 (%5)").arg(pPkg->stTBConferences[1].i_in_ring).arg(pPkg->stTBConferences[1].i_in_port).
				arg(pPkg->stTBConferences[1].i_out_ring).arg(pPkg->stTBConferences[1].i_out_port).arg(pPkg->stTBConferences[1].i_DynAssignedConference);
			SetDlgItemText(hWndDlg, IDC_SPKG_4W1, LPCSTR(ssdata));
			ssdata = bncs_string("%1.%2/%3.%4 (%5)").arg(pPkg->stTBConferences[2].i_in_ring).arg(pPkg->stTBConferences[2].i_in_port).
				arg(pPkg->stTBConferences[2].i_out_ring).arg(pPkg->stTBConferences[2].i_out_port).arg(pPkg->stTBConferences[2].i_DynAssignedConference);
			SetDlgItemText(hWndDlg, IDC_SPKG_4W2, LPCSTR(ssdata));
			ssdata = bncs_string("%1.%2/%3.%4(%5)").arg(pPkg->stCFIFBs[1].i_in_ring).arg(pPkg->stCFIFBs[1].i_in_port).arg(pPkg->stCFIFBs[1].i_out_ring).arg(pPkg->stCFIFBs[1].i_out_port).arg(pPkg->stCFIFBs[1].i_DynAssignedIfb);
			SetDlgItemText(hWndDlg, IDC_SPKG_IFB1, LPCSTR(ssdata));
			ssdata = bncs_string("%1.%2/%3.%4(%5)").arg(pPkg->stCFIFBs[2].i_in_ring).arg(pPkg->stCFIFBs[2].i_in_port).arg(pPkg->stCFIFBs[2].i_out_ring).arg(pPkg->stCFIFBs[2].i_out_port).arg(pPkg->stCFIFBs[2].i_DynAssignedIfb);
			SetDlgItemText(hWndDlg, IDC_SPKG_IFB2, LPCSTR(ssdata));
			ssdata = bncs_string("%1.%2/%3.%4(%5)").arg(pPkg->stCFIFBs[3].i_in_ring).arg(pPkg->stCFIFBs[3].i_in_port).arg(pPkg->stCFIFBs[3].i_out_ring).arg(pPkg->stCFIFBs[3].i_out_port).arg(pPkg->stCFIFBs[3].i_DynAssignedIfb);
			SetDlgItemText(hWndDlg, IDC_SPKG_IFB3, LPCSTR(ssdata));
			ssdata = bncs_string("%1.%2/%3.%4(%5)").arg(pPkg->stCFIFBs[4].i_in_ring).arg(pPkg->stCFIFBs[4].i_in_port).arg(pPkg->stCFIFBs[4].i_out_ring).arg(pPkg->stCFIFBs[4].i_out_port).arg(pPkg->stCFIFBs[4].i_DynAssignedIfb);
			SetDlgItemText(hWndDlg, IDC_SPKG_IFB4, LPCSTR(ssdata));

			// clear que list
			SendDlgItemMessage(hWndDlg, IDC_LIST3, LB_RESETCONTENT, 0, 0);
			for (int iia = 0; iia < pPkg->ssl_Pkg_Queue.count(); iia++) {
				CDestinationPackages* pDst = GetDestinationPackage(pPkg->ssl_Pkg_Queue[iia].toInt());
				if (pDst) {
					char szTmp[128] = "";
					wsprintf(szTmp, "%04d %s", pDst->iPackageIndex, LPCSTR(pDst->ss_PackageButtonName));
					SendDlgItemMessage(hWndDlg, IDC_LIST3, LB_INSERTSTRING, -1, (LPARAM)szTmp);
				}
			} //for iia
			//		
			SetDlgItemInt(hWndDlg, IDC_APKG_REVVIS, pPkg->st_Reverse_Vision.iAssocIndex, TRUE);
			if (pPkg->iEnabledRevVision>0)
				SetDlgItemText(hWndDlg, IDC_APKG_REVENA, "Y");
			else
				SetDlgItemText(hWndDlg, IDC_APKG_REVENA, "n");
		}
	}
}


void ClearSrcPkgData()
{
	SetDlgItemText(hWndDlg, IDC_SPKG_NAME, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_NAME2, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_NAME3, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_NAME4, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_NAME5, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_VIDEO1, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_AUDIO1, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_TAG1, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_TAG2, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_TAG3, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_TAG4, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_VIDEO2, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_AUDIO2, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_VIDEO3, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_AUDIO3, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_VIDEO4, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_AUDIO4, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED2, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_AUDIOPKGS, "");
	
	SetDlgItemText(hWndDlg, IDC_SPEV_NAME, "");
	SetDlgItemText(hWndDlg, IDC_SPEV_DATA1, "");
	SetDlgItemText(hWndDlg, IDC_SPEV_DATA2, "");
	SetDlgItemText(hWndDlg, IDC_SPEV_DATA3, "");
	SetDlgItemText(hWndDlg, IDC_SPEV_DATA4, "");
	SetDlgItemText(hWndDlg, IDC_SPEV_DATA5, "");
	SetDlgItemText(hWndDlg, IDC_SPEV_DATA6, "");
}

void DisplaySourcePackageData()
{
	// display src pkg data
	char szTmp[256];
	ClearSrcPkgData();
	if ((iChosenSrcPkg>0) && (iChosenSrcPkg<=(iNumberOfPackages+2))) {
		CSourcePackages* pPkg = GetSourcePackage(iChosenSrcPkg);
		if (pPkg) {
			SetDlgItemInt(hWndDlg, IDC_SRCE_INDEX, iChosenSrcPkg, TRUE);
			if ((iChosenSrcPkg >= i_Start_VirtualPackages) && (iChosenSrcPkg<=iNumberOfPackages))
				SetDlgItemText(hWndDlg, IDC_SPKG_NAME, LPCSTR(bncs_string("(vir) %1").arg(pPkg->ss_PackageButtonName)));
			else
				SetDlgItemText(hWndDlg, IDC_SPKG_NAME, LPCSTR(pPkg->ss_PackageLongName));
			SetDlgItemText(hWndDlg, IDC_SPKG_NAME2, LPCSTR(pPkg->ss_PackageButtonName));
			SetDlgItemText(hWndDlg, IDC_SPKG_NAME3, LPCSTR(pPkg->ss_PackageMCRUMD));
			SetDlgItemText(hWndDlg, IDC_SPKG_NAME4, LPCSTR(pPkg->ss_PackageProdUMDs));
			SetDlgItemText(hWndDlg, IDC_SPKG_NAME5, LPCSTR(pPkg->ss_PackageKeyInfo));

			bncs_stringlist ssldata = bncs_stringlist("", ',');
			for (int ii = 1; ii <= pPkg->m_iNumberAssignedAudioPackages; ii++) {
				if (pPkg->m_iAssoc_Audio_Packages[ii] > 0) ssldata.append(bncs_string(pPkg->m_iAssoc_Audio_Packages[ii]));
			}
			SetDlgItemText(hWndDlg, IDC_SPKG_AUDIOPKGS, LPCSTR(ssldata.toString(' ')));

			// display video and audio layers
			char szData[MAX_AUTO_BUFFER_STRING] = "";
			wsprintf(szData, "%d / %d", pPkg->st_PkgVideoLevels[1].stVideoLevel.iAssocIndex, pPkg->st_PkgVideoLevels[1].stANCLevel[1].iAssocIndex);
			SetDlgItemText(hWndDlg, IDC_SPKG_VIDEO1, szData);
			wsprintf(szData, "%d / %d", pPkg->st_PkgVideoLevels[2].stVideoLevel.iAssocIndex, pPkg->st_PkgVideoLevels[2].stANCLevel[1].iAssocIndex);
			SetDlgItemText(hWndDlg, IDC_SPKG_VIDEO2, szData);
			wsprintf(szData, "%d / %d", pPkg->st_PkgVideoLevels[3].stVideoLevel.iAssocIndex, pPkg->st_PkgVideoLevels[3].stANCLevel[1].iAssocIndex);
			SetDlgItemText(hWndDlg, IDC_SPKG_VIDEO3, szData);
			wsprintf(szData, "%d / %d", pPkg->st_PkgVideoLevels[4].stVideoLevel.iAssocIndex, pPkg->st_PkgVideoLevels[4].stANCLevel[1].iAssocIndex);
			SetDlgItemText(hWndDlg, IDC_SPKG_VIDEO4, szData);

			char szTag[128] = "";
			if (pPkg->st_PkgVideoLevels[1].stVideoLevel.iAssocHub_or_TypeTag > 0)
				strcpy(szTag, ExtractNamefromDevIniFile(i_rtr_Infodrivers[1], DB_TAGS_VIDEO, pPkg->st_PkgVideoLevels[1].stVideoLevel.iAssocHub_or_TypeTag));
			wsprintf(szData, "%d %s", pPkg->st_PkgVideoLevels[1].stVideoLevel.iAssocHub_or_TypeTag, szTag);
			SetDlgItemText(hWndDlg, IDC_SPKG_TAG1, szData);
			strcpy(szTag, " ");
			if (pPkg->st_PkgVideoLevels[2].stVideoLevel.iAssocHub_or_TypeTag > 0)
				strcpy(szTag, ExtractNamefromDevIniFile(i_rtr_Infodrivers[1], DB_TAGS_VIDEO, pPkg->st_PkgVideoLevels[2].stVideoLevel.iAssocHub_or_TypeTag));
			wsprintf(szData, "%d %s", pPkg->st_PkgVideoLevels[2].stVideoLevel.iAssocHub_or_TypeTag, szTag);
			SetDlgItemText(hWndDlg, IDC_SPKG_TAG2, szData);
			strcpy(szTag, " ");
			if (pPkg->st_PkgVideoLevels[3].stVideoLevel.iAssocHub_or_TypeTag > 0)
				strcpy(szTag, ExtractNamefromDevIniFile(i_rtr_Infodrivers[1], DB_TAGS_VIDEO, pPkg->st_PkgVideoLevels[3].stVideoLevel.iAssocHub_or_TypeTag));
			wsprintf(szData, "%d %s", pPkg->st_PkgVideoLevels[3].stVideoLevel.iAssocHub_or_TypeTag, szTag);
			SetDlgItemText(hWndDlg, IDC_SPKG_TAG3, szData);
			strcpy(szTag, " ");
			if (pPkg->st_PkgVideoLevels[4].stVideoLevel.iAssocHub_or_TypeTag > 0)
				strcpy(szTag, ExtractNamefromDevIniFile(i_rtr_Infodrivers[1], DB_TAGS_VIDEO, pPkg->st_PkgVideoLevels[4].stVideoLevel.iAssocHub_or_TypeTag));
			wsprintf(szData, "%d %s", pPkg->st_PkgVideoLevels[4].stVideoLevel.iAssocHub_or_TypeTag, szTag);
			SetDlgItemText(hWndDlg, IDC_SPKG_TAG4, szData);

			wsprintf(szData, "%d # %d", pPkg->st_SourceAudioLevels[1].iAssocIndex, pPkg->st_SourceAudioLevels[1].iAssocHub_or_TypeTag);
			SetDlgItemText(hWndDlg, IDC_SPKG_AUDIO1, szData);
			wsprintf(szData, "%d # %d", pPkg->st_SourceAudioLevels[2].iAssocIndex, pPkg->st_SourceAudioLevels[2].iAssocHub_or_TypeTag);
			SetDlgItemText(hWndDlg, IDC_SPKG_AUDIO2, szData);
			wsprintf(szData, "%d # %d", pPkg->st_SourceAudioLevels[3].iAssocIndex, pPkg->st_SourceAudioLevels[3].iAssocHub_or_TypeTag);
			SetDlgItemText(hWndDlg, IDC_SPKG_AUDIO3, szData);
			wsprintf(szData, "%d # %d", pPkg->st_SourceAudioLevels[4].iAssocIndex, pPkg->st_SourceAudioLevels[4].iAssocHub_or_TypeTag);
			SetDlgItemText(hWndDlg, IDC_SPKG_AUDIO4, szData);

			if (pPkg->ssl_Pkg_DirectlyRoutedTo.count() < 12)
				SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED, LPCSTR(pPkg->ssl_Pkg_DirectlyRoutedTo.toString()));
			else {
				wsprintf(szTmp, " routed to %d dest pkges", pPkg->ssl_Pkg_DirectlyRoutedTo.count());
				SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED, szTmp);
			}

			if (pPkg->ssl_Pkg_FinallyRoutedTo.count() < 12)
				SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED2, LPCSTR(pPkg->ssl_Pkg_FinallyRoutedTo.toString()));
			else {
				wsprintf(szTmp, " finally to %d dest pkges", pPkg->ssl_Pkg_FinallyRoutedTo.count());
				SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED2, szTmp);
			}

			// spev data
			if (pPkg->m_iAssociatedSportEvent > 0) {
				CSportEvent* pSPEV = GetSportEventPackage(pPkg->m_iAssociatedSportEvent);
				if (pSPEV) {
					SetDlgItemText(hWndDlg, IDC_SPEV_NAME, LPCSTR(pSPEV->m_ssName));
					SetDlgItemText(hWndDlg, IDC_SPEV_DATA1, LPCSTR(pSPEV->m_ssAssetID));
					SetDlgItemText(hWndDlg, IDC_SPEV_DATA2, LPCSTR(pSPEV->m_ssClassID));
					SetDlgItemText(hWndDlg, IDC_SPEV_DATA3, LPCSTR(pSPEV->m_ssEventID));
					SetDlgItemText(hWndDlg, IDC_SPEV_DATA4, LPCSTR(pSPEV->m_ss_TimeDefinition));
					SetDlgItemText(hWndDlg, IDC_SPEV_DATA5, LPCSTR(pSPEV->m_ss_TeamDefinition));
					SetDlgItemInt(hWndDlg, IDC_SPEV_DATA6, pSPEV->m_iSPEV_Status, TRUE);
				}
			}

		}
		else
			Debug("DisplaySrcPkg - no src pkg in map for %d", iChosenSrcPkg);
	}
}


void ClearDestPkgData()
{
	SetDlgItemText(hWndDlg, IDC_DPKG_NAME, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_NAME2, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_VIDMODE, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_SLAVEINDX, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_ROUTED, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_TRACED, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_VIDEO, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_TAG1, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_ANC, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_REVVIS, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_PARKPKG, "");
	
	SetDlgItemText(hWndDlg, IDC_DPKG_AUDIOPAIRS, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_AUDIORTN, "");

	SetDlgItemText(hWndDlg, IDC_DPKG_IFB1, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_IFB2, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_LOCKED, "");
	
	SetDlgItemText(hWndDlg, IDC_DPKG_VIDEO2, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_AUDIO2, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_ISSUES, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_TOPMMQS, "");

	SetDlgItemText(hWndDlg, IDC_DPKG_VISFROM, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_AUDFROM, "");
}

void DisplayDestinationPackageData(BOOL bDecExp)
{
	// get list of cards for  chosen node
	ClearDestPkgData();
	if ((iChosenDestPkg>0) && (iChosenDestPkg<=iNumberOfPackages)) {
		CDestinationPackages* pPkg = GetDestinationPackage(iChosenDestPkg);
		if (pPkg) {
			SetDlgItemInt(hWndDlg, IDC_DEST_INDEX, iChosenDestPkg, TRUE);
			SetDlgItemText(hWndDlg, IDC_DPKG_NAME, LPCSTR(pPkg->ss_PackageButtonName));
			SetDlgItemText(hWndDlg, IDC_DPKG_NAME2, LPCSTR(pPkg->ss_PackageLongName));
			SetDlgItemText(hWndDlg, IDC_DPKG_DATA1, LPCSTR(pPkg->ss_PackageUniqueID));
			SetDlgItemInt(hWndDlg, IDC_DPKG_DATA2, pPkg->m_iOwnerType, TRUE);
			SetDlgItemInt(hWndDlg, IDC_DPKG_DATA3, pPkg->m_iDelegateType, TRUE);
			if (pPkg->iPackageLockState > 0) SetDlgItemText(hWndDlg, IDC_DPKG_LOCKED, "LOK");
			// display dest pkg data
			SetDlgItemInt(hWndDlg, IDC_DPKG_ANC, pPkg->st_VideoANC[1].iAssocIndex, FALSE);
			SetDlgItemInt(hWndDlg, IDC_DPKG_VIDEO, pPkg->st_VideoHDestination.iAssocIndex, TRUE);
			SetDlgItemInt(hWndDlg, IDC_DPKG_REVVIS, pPkg->st_Reverse_Vision.iAssocIndex, TRUE);
			SetDlgItemInt(hWndDlg, IDC_DPKG_PARKPKG, pPkg->i_ParkSourcePackage, TRUE);
			char szData[MAX_AUTO_BUFFER_STRING] = "";
			char szTag[128] = "";
			if (pPkg->st_VideoHDestination.iAssocHub_or_TypeTag > 0)
				strcpy(szTag, ExtractNamefromDevIniFile(i_rtr_Infodrivers[1], DB_TAGS_VIDEO, pPkg->st_VideoHDestination.iAssocHub_or_TypeTag));
			wsprintf(szData, "%d %s", pPkg->st_VideoHDestination.iAssocHub_or_TypeTag, szTag);
			SetDlgItemText(hWndDlg, IDC_DPKG_TAG1, szData);
			//
			bncs_string ssData = "";
			for (int ii = 1; ii < MAX_PKG_AUDIO; ii++) {
				ssData.append(bncs_string("%1#%2:%3 ").arg(pPkg->st_AudioDestPair[ii].iAssocIndex).arg(pPkg->st_AudioDestPair[ii].iAssocLang_or_UseTag).arg(pPkg->st_AudioDestPair[ii].iAssocHub_or_TypeTag));
			}
			SetDlgItemText(hWndDlg, IDC_DPKG_AUDIOPAIRS, LPCSTR(ssData));
			SetDlgItemText(hWndDlg, IDC_DPKG_AUDIORTN, LPCSTR(bncs_stringlist(pPkg->ss_Package_AudioRtn, ',').toString(' ')));
			//
			SetDlgItemInt( hWndDlg, IDC_DPKG_ROUTED, pPkg->i_Routed_Src_Package, FALSE );
			SetDlgItemInt(hWndDlg, IDC_DPKG_TRACED, pPkg->i_Traced_Src_Package, TRUE);
			SetDlgItemInt(hWndDlg, IDC_DPKG_VISFROM, pPkg->i_VisionFrom_Pkg, TRUE);
			SetDlgItemInt(hWndDlg, IDC_DPKG_AUDFROM, pPkg->i_AudioFrom_Pkg, TRUE);
			SetDlgItemText(hWndDlg, IDC_DPKG_ISSUES, LPCSTR(pPkg->ss_Trace_To_Source));
			//
			if (pPkg->st_VideoHDestination.iAssocIndex > 0) {
				CRevsRouterData* pRtr = GetRouterRecord(iIP_RouterVideoHD);
				if (pRtr) {
					strcpy(szData, "");
					int iVidSrc = pRtr->getPrimarySourceForDestination(pPkg->st_VideoHDestination.iAssocIndex);
					int iVidExpect = pRtr->getExpectedSourceForDestination(pPkg->st_VideoHDestination.iAssocIndex, bDecExp);
					int iExpRemain = pRtr->getExpectedCounterForDestination(pPkg->st_VideoHDestination.iAssocIndex);
					int iVidStat = pRtr->getRouterDestStatus(pPkg->st_VideoHDestination.iAssocIndex);
					int iVidLock = pRtr->getRouterDestLock(pPkg->st_VideoHDestination.iAssocIndex);
					char szStat[8] = "???";
					char szLck[8] = "---";
					switch (iVidStat) {
					case NEVION_PARKED: strcpy(szStat, "park"); break;
					case NEVION_SCHEDULED: strcpy(szStat, "schd"); break;
					case NEVION_ACTIVE: strcpy(szStat, "actv"); break;
					case NEVION_REQUESTED: strcpy(szStat, "reqd"); break;
					case NEVION_OTHERPLANE: strcpy(szStat, "othp"); break;
					case NEVION_NOTMAPPED: strcpy(szStat, "notm"); break;
					}
					if (iVidLock > NEVION_UNLOCKED) strcpy(szLck, "lock");
					if ((iVidExpect > 0) && (iExpRemain >0))
						wsprintf(szData, "src %d ex:%d/%d : %s : %s", iVidSrc, iVidExpect, iExpRemain, szStat, szLck);
					else
						wsprintf(szData, "src %d : %s : %s", iVidSrc, szStat, szLck);
					SetDlgItemText(hWndDlg, IDC_DPKG_VIDEO2, szData);
				}
			}
			if (pPkg->st_AudioDestPair[1].iAssocIndex > 0) {
				CRevsRouterData* pRtr2 = GetRouterRecord(iIP_RouterAudio2CH);
				if (pRtr2) {
					strcpy(szData, "");
					int iAudSrc = pRtr2->getPrimarySourceForDestination(pPkg->st_AudioDestPair[1].iAssocIndex);
					int iAudExpect = pRtr2->getExpectedSourceForDestination(pPkg->st_AudioDestPair[1].iAssocIndex, bDecExp);
					int iExpRemain = pRtr2->getExpectedCounterForDestination(pPkg->st_AudioDestPair[1].iAssocIndex);
					int iAudStat = pRtr2->getRouterDestStatus(pPkg->st_AudioDestPair[1].iAssocIndex);
					int iAudLock = pRtr2->getRouterDestLock(pPkg->st_AudioDestPair[1].iAssocIndex);
					char szStat[8] = "???";
					char szLck[8] = "---";
					switch (iAudStat) {
					case NEVION_PARKED: strcpy(szStat, "park"); break;
					case NEVION_SCHEDULED: strcpy(szStat, "schd"); break;
					case NEVION_ACTIVE: strcpy(szStat, "actv"); break;
					case NEVION_REQUESTED: strcpy(szStat, "reqd"); break;
					case NEVION_OTHERPLANE: strcpy(szStat, "othp"); break;
					case NEVION_NOTMAPPED: strcpy(szStat, "notm"); break;
					}
					if (iAudLock > NEVION_UNLOCKED) strcpy(szLck, "lock");
					if ((iAudExpect >= 0) && (iExpRemain >0))
						wsprintf(szData, "src:%d exp:%d/%d : %s : %s", iAudSrc, iAudExpect, iExpRemain, szStat, szLck);
					else
						wsprintf(szData, "src %d : %s : %s", iAudSrc, szStat, szLck);
					SetDlgItemText(hWndDlg, IDC_DPKG_AUDIO2, szData);
				}
			}

   			// IFB 
			bncs_string ssdata = "";
			ssdata = bncs_string("%1.%2").arg(pPkg->st_AssocIFBs[1].i_mm_ring).arg(pPkg->st_AssocIFBs[1].i_mm_port);
			SetDlgItemText(hWndDlg, IDC_DPKG_IFB1, LPCSTR(ssdata));
			ssdata = bncs_string("%1.%2").arg(pPkg->st_AssocIFBs[2].i_mm_ring).arg(pPkg->st_AssocIFBs[2].i_mm_port);
			SetDlgItemText(hWndDlg, IDC_DPKG_IFB2, LPCSTR(ssdata));
			//
			SetDlgItemText(hWndDlg, IDC_DPKG_TOPMMQS, LPCSTR(pPkg->ssl_AtTopOfAudioPkgMMQ.toString(' ')));

		}
	}
}


void DisplaySelectedInfodriver(void)
{
	iChosenInfodriver = (SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_GETCURSEL, 0, 0)) + 1;
	if ((iChosenInfodriver<1) || (iChosenInfodriver > 8)) 	iChosenInfodriver = 1;
	if ((iChosenInfodriver > 0)&&(iChosenInfodriver<9)) {

		int iStartIndex = ((iChosenInfodriver - 1)*iNumberInfosInBlock)+1;
		int iEndIndex = iChosenInfodriver*iNumberInfosInBlock;
		if (iChosenInfodriver == 8) iStartIndex = (AUTO_RTR_DEST_USE_INFO * iNumberInfosInBlock) + 2;
		if (iChosenInfodriver == 9) iStartIndex = (AUTO_RTR_DEST_USE_INFO * iNumberInfosInBlock) + 3;

		char szName[MAX_LOADSTRING] = "";
		switch (iChosenInfodriver) {
		case 1: wsprintf(szName, "%d-%d pkg route status ", i_rtr_Infodrivers[iStartIndex], i_rtr_Infodrivers[iEndIndex]);  break;
		case 2: wsprintf(szName, "%d-%d MM Queues status", i_rtr_Infodrivers[iStartIndex], i_rtr_Infodrivers[iEndIndex]); break;
		case 3: wsprintf(szName, "%d-%d audio pkgs usage", i_rtr_Infodrivers[iStartIndex], i_rtr_Infodrivers[iEndIndex]); break;
		case 4: wsprintf(szName, "%d-%d dest pkg status", i_rtr_Infodrivers[iStartIndex], i_rtr_Infodrivers[iEndIndex]); break;
		case 5: wsprintf(szName, "%d-%d srce pkg pti/stat", i_rtr_Infodrivers[iStartIndex], i_rtr_Infodrivers[iEndIndex]);break;
		case 6: wsprintf(szName, "%d-%d IP rtr dest use", i_rtr_Infodrivers[iStartIndex], i_rtr_Infodrivers[iEndIndex]); break;
		case 7: wsprintf(szName, "%d riedel conference", i_rtr_Infodrivers[iStartIndex]); break;
		case 8: wsprintf(szName, "%d riedel port use", i_rtr_Infodrivers[iStartIndex]); break;
		case 9: wsprintf(szName, "%d riedel ifbs 1", i_rtr_Infodrivers[iStartIndex]); break;
		}
		if ((iStartIndex>0) && (iStartIndex < MAX_INFODRIVERS)) {
			SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, szName);
			int iInfStat = eiInfoDrivers[iStartIndex].getmode();
			if (iInfStat == IFMODE_TXRX){
				iLabelInfoStatus = IFMODE_TXRX;
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS2, "Infodrv Tx/Rx ");
			}
			else if (iInfStat == IFMODE_RXONLY) {
				iLabelInfoStatus = IFMODE_RXONLY;
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS2, "Infodrv RXonly ");
			}
			else {
				iLabelInfoStatus = 0;
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS2, "Infodrv unkn ?? ");
			}
		}
		else
			SetDlgItemInt(hWndDlg, IDC_INFO_STATUS1, iStartIndex, TRUE);

	}
	else {
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "");
		iLabelInfoStatus = 0;
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS2, "");
	}
}


//update infodriver slot now with all status data inc current conf in use
void UpdateAudioPackageStatusInfodriver(int iAPkgIndex)
{
	CAudioPackages* pAudPkg = GetAudioPackage(iAPkgIndex);
	if (pAudPkg) {
		bncs_string ssStr = "";
		//1 rev vision
		ssStr = bncs_string("rev_enabled=%1,vision=%2").arg(pAudPkg->iEnabledRevVision).arg(pAudPkg->iRoutedRevVisionPkg);
		// 2 TB dyn confs
		ssStr.append(bncs_string(",conf1=%1,conf2=%2").arg(pAudPkg->stTBConferences[1].i_DynAssignedConference).arg(pAudPkg->stTBConferences[2].i_DynAssignedConference));
		// 4 CF dyn confs
		ssStr.append(bncs_string(",cf1_conf=%1,cf2_conf=%2,cf3_conf=%3,cf4_conf=%4").arg(pAudPkg->stCFIFBs[1].i_DynAssignedConference).arg(pAudPkg->stCFIFBs[2].i_DynAssignedConference).
			arg(pAudPkg->stCFIFBs[3].i_DynAssignedConference).arg(pAudPkg->stCFIFBs[4].i_DynAssignedConference));
		// 4 dyn ifbs.  -- translate IFBs to ring.ifb
		for (int ii = 1; ii < MAX_PKG_CONFIFB; ii++) {
			if (pAudPkg->stCFIFBs[ii].i_DynAssignedIfb>0){
				int iring = pAudPkg->stCFIFBs[ii].i_out_ring;
				int iifbrec = pAudPkg->stCFIFBs[ii].i_DynAssignedIfb;
				// convert assigned ifb from internal record indx to ring.ifb, 1, 1501, 3001, 4501 etc
				int iifb = ((iifbrec - 1) % MAX_IFBS) + 1;
				ssStr.append(bncs_string(",cf%1_ifb=%2.%3").arg(ii).arg(iring).arg(iifb));
			}
			else
				ssStr.append(bncs_string(",cf%1_ifb=0.0").arg(ii));
		}
		//
		SendPackagerBNCSRevertive(iAPkgIndex, AUTO_AUDIO_PKG_INFO, FALSE, LPCSTR(ssStr));
	}
	else
		Debug("UpdateAudioPackageStatusInfo - invalid AP index passed %d ", iAPkgIndex);
}


void UpdateAudioPackageMMQInfodriver(CAudioPackages* pAudio)
{
	if (pAudio) {
		bncs_string ssStr = pAudio->ssl_Pkg_Queue.toString(',');
		SendPackagerBNCSRevertive(pAudio->m_iAudioPkg_Indx, AUTO_MM_STACK_INFO, FALSE, LPCSTR(ssStr));
	}
}




//////////////////////////////////////////////////////////////////////////
// 
//  AUTOMATIC STARTUP FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


BOOL GetRouterDeviceDBSizes( bncs_string ssName, int *iRtrDevice, int *iDB0Size, int *iDB1Size ) 
{
	char szFileName[64]="", szEntry[12]="";
	int iNewDevice = 0;
	*iDB0Size=0; *iDB1Size=0;
	iNewDevice=getInstanceDevice( ssName );
	*iRtrDevice = iNewDevice;
	if (iNewDevice>0) {
		// get database sizes db0 and db1
		strcpy_s( szFileName, ssName );
		wsprintf(szFileName, "dev_%03d.ini", iNewDevice );
		*iDB0Size = atoi( r_p(szFileName, "Database", "DatabaseSize_0", "", false) );
		*iDB1Size = atoi( r_p(szFileName, "Database", "DatabaseSize_1", "", false) );
		return TRUE;
	}
	else 
		Debug( "GetRouterDeviceDBSizes - dev %s has ret 0", LPCSTR(ssName));
	return FALSE;
}


//
//  FUNCTION: ExtractNamefromDevIniFile( )
//
//  PURPOSE: Extracts the src name from specified Database dev ini file and returns string 
//
char* ExtractNamefromDevIniFile(int iDev, int iDbFile, int iEntry)
{
	char szFileName[256] = "", szDatabase[32] = "", szIniFile[256] = "", szDBFile[256] = "", szEntry[32] = "", szData[256] = "";

	if ((iDev>0) && (iEntry>0)) {
		// determine db file location - first from inside dev.ini :
		sprintf(szIniFile, "dev_%03d.ini", iDev);
		sprintf(szDBFile, "DatabaseFile_%d", iDbFile);
		sprintf(szDatabase, "Database_%d", iDbFile);
		sprintf(szEntry, "%04d", iEntry);
		// file name from ini file
		strcpy(szFileName, r_p(szIniFile, "Database", szDBFile, "", FALSE));
		//if (bShowAllDebugMessages) Debug("ExtractDB - %s from %d %d", szFileName, iDev, iDbFile);
		if (strlen(szFileName)<1) {
			// determine file name by another route -- using CSI default names
			if (iDbFile>9)
				sprintf(szFileName, "dev_%03d.db%04d", iDev, iDbFile);
			else
				sprintf(szFileName, "dev_%03d.db%d", iDev, iDbFile);
		}
		// now get required db entry
		//if (bShowAllDebugMessages) {
		//	strcpy(szData, r_p(szFileName, szDatabase, szEntry, "", FALSE));
		//	Debug("ExtractDB - entry %d data %s", iEntry, szData);
		//}
		return r_p(szFileName, szDatabase, szEntry, "", FALSE);
	}
	return "";
}


char* GetParameterString( bncs_string sstr )
{
	strcpy_s( szResult, "");
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		wsprintf( szResult, "%s", LPCSTR(ssl1[1]) );
	return szResult;

}

int GetParameterInt( bncs_string sstr  )
{
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		return ssl1[1].toInt();
	else
		return UNKNOWNVAL;
}			

void GetTwinIntParameters( bncs_string sstr, int *i1, int *i2 )
{
	*i1=0;
	*i2=0;
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '/' );
	if (ssl1.count()>0) *i1 = ssl1[0].toInt();
	if (ssl1.count()>1) *i2 = ssl1[1].toInt();
}


// use this to get   pair of  ring.port  for riedel 
void GetRiedelRingAndPort(bncs_string sstr, int *iRing, int *iPort)
{
	*iRing = 0;
	*iPort = 0;
	bncs_stringlist ssl1 = bncs_stringlist(sstr, '.');
	if (ssl1.count() > 1) {
		*iRing = ssl1[0].toInt();
		*iPort = ssl1[1].toInt();
	}
	else if (ssl1.count() == 1) {
		*iRing = 0;  // calling functions will need to resolve required ring if 0 returned
		*iPort = ssl1[0].toInt();
	}
}


void GetTwinRiedelParameters(bncs_string sstr, int *ir1, int *ip1, int *ir2, int *ip2)
{
	*ir1 = 0; *ip1 = 0;
	*ir2 = 0; *ip2 = 0;
	bncs_string ssl="", ssr = "";
	sstr.split('/', ssl, ssr);
	bncs_stringlist ssl1 = bncs_stringlist(ssl, '.');
	if (ssl1.count() > 1) {
		// new style of ring.port
		*ir1 = ssl1[0].toInt();
		*ip1 = ssl1[1].toInt();
	}
	else if (ssl1.count() == 1) {
		// old style - port only
		*ip1 = ssl1[0].toInt();
	}
	//
	bncs_stringlist ssl2 = bncs_stringlist(ssr, '.');
	if (ssl2.count() > 1) {
		// new style of ring.port
		*ir2 = ssl2[0].toInt();
		*ip2 = ssl2[1].toInt();
	}
	else if (ssl2.count() == 1) {
		// old style - port only
		*ip2 = ssl2[0].toInt();
	}
}


// use this to get   pair of  rtr.src    rtr.dest  
void GetRtrDeviceAndPortIndex(bncs_string sstr, int *iDev, int *iPort)
{
	*iDev = iIP_RouterVideoHD;  // default video router to use
	*iPort = 0;
	bncs_stringlist ssl1 = bncs_stringlist(sstr, '.');
	if (ssl1.count() > 1) {
		if (ssl1.count()>0) *iDev = ssl1[0].toInt();
		if (ssl1.count()>1) *iPort = ssl1[1].toInt();
	}
	else if (ssl1.count() == 1) {
		*iDev = iIP_RouterVideoHD;  // default video router to use
		*iPort = ssl1[0].toInt();
	}
}


void ExtractRouterIndexTagData(bncs_string ssData, int *iIndex, int *iFirstTag, int *iSecondTag)
{
	*iIndex = 0;
	*iFirstTag = 0;
	*iSecondTag = 0;
	if (ssData.length() > 0) {
		bncs_stringlist ssl = bncs_stringlist(ssData, '#');
		if (ssl.count() > 1) {
			// index and tag field(s)
			*iIndex = ssl[0].toInt();
			bncs_stringlist ssltag = bncs_stringlist(ssl[1], ':');
			if (ssltag.count() > 1) {
				// two tags
				*iFirstTag = ssltag[0].toInt();
				*iSecondTag = ssltag[1].toInt();
			}
			else {
				// one tag
				*iFirstTag = ssltag[0].toInt();
			}
		}
		else {
			// just an index, no tags present
			*iIndex = ssl[0].toInt();
		}
	}
}

void ExtractJustTagData(bncs_string ssData, int *iFirstTag, int *iSecondTag)
{
	*iFirstTag = 0;
	*iSecondTag = 0;
	if (ssData.length() > 0) {
		bncs_stringlist ssltag = bncs_stringlist(ssData, ':');
		if (ssltag.count() > 1) {
			// two tags
			*iFirstTag = ssltag[0].toInt();
			*iSecondTag = ssltag[1].toInt();
		}
		else {
			// one tag
			*iFirstTag = ssltag[0].toInt();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// FUNCTION LoadPackagerCompositeInstances
//

bncs_string GetCompositeInstance(bncs_string ssRequiredInstance)
{
	bncs_stringlist sslist = bncs_stringlist("", ',');
	Debug("GetCompositeInstance - Given instance %s", LPCSTR(ssRequiredInstance));
	if (ssRequiredInstance.length() > 0) {
		// load in key groups composite
		bncs_string ssItem = bncs_string("instances.%1").arg(ssRequiredInstance);
		bncs_config cfg(ssItem);
		while (cfg.isChildValid())  {
			// get id and instance from cfg child - add to list as id=instance
			bncs_string ssGrpId = cfg.childAttr("id");
			bncs_string ssValue = cfg.childAttr("value");
			bncs_string ssInstance = cfg.childAttr("instance");
			if (ssInstance.length() > 0) {
				sslist.append(bncs_string("%1=%2").arg(ssGrpId).arg(ssInstance));
				Debug("--- composite child - id=%s yields instance=%s ", LPCSTR(ssGrpId), LPCSTR(ssInstance));
			}
			else if (ssValue.length() > 0) {
				sslist.append(bncs_string("%1=%2").arg(ssGrpId).arg(ssValue));
				Debug("--- composite child - id=%s yields value=%s ", LPCSTR(ssGrpId), LPCSTR(ssValue));
			}
			else Debug(" --- ERROR ---  child %d  has both instance AND value missing / null strings ", LPCSTR(ssGrpId));
			cfg.nextChild();				 // go on to the next sibling (if there is one)
		}
	}
	//
	Debug("GetCompositeInstance - Required instance %s returned %d entries ", LPCSTR(ssRequiredInstance), sslist.count());
	return sslist.toString(',');
}


//  LOAD PACKAGER composite instances 
BOOL LoadPackagerCompositeInstances()
{

	for (int ii = 0; ii < MAX_INFODRIVERS; ii++) i_rtr_Infodrivers[ii] = 0;

	iAutomaticLocation = LDC_LOCALE_RING;
	if (ssCompositeInstance.find("nhn") >= 0) {
		SetDlgItemText(hWndDlg, IDC_AUTO_AREA, "NHN");
		iAutomaticLocation = NHN_LOCALE_RING;
	}
	else {
		SetDlgItemText(hWndDlg, IDC_AUTO_AREA, "LDC");
		iAutomaticLocation = LDC_LOCALE_RING;
	}

	cl_KeyInstances = new CCompInstances(ssCompositeInstance);
	if (cl_KeyInstances) {
		// load in key groups composite instance
		bncs_stringlist sslcomp = bncs_stringlist(GetCompositeInstance(ssCompositeInstance), ',');
		if (sslcomp.count() > 6) {
			cl_KeyInstances->ssRouterComposite = sslcomp.getNamedParam("router");
			cl_KeyInstances->ssMixMinusComposite = sslcomp.getNamedParam("mm_stack");
			cl_KeyInstances->ssAPReturnsComposite = sslcomp.getNamedParam("audio_package_returns");
			cl_KeyInstances->ssDestStatusComposite = sslcomp.getNamedParam("dest_status");
			cl_KeyInstances->ssSrcePTIComposite = sslcomp.getNamedParam("source_pti");
			cl_KeyInstances->ssNevionDestUseComposite = sslcomp.getNamedParam("nevion_dest_use");
			cl_KeyInstances->ssPkgrRiedelComposite = sslcomp.getNamedParam("riedel");
		}
		else
			Debug("LoadComposites - FAIL for packager composites - Packager suspended - list had only %d items ", sslcomp.count());

		// load in the mains for each key group just obtained
		if (cl_KeyInstances->ssRouterComposite.length() > 0) {
			bncs_stringlist sslcomp2 = bncs_stringlist(GetCompositeInstance(cl_KeyInstances->ssRouterComposite), ',');
			for (int ii = 0; ii < sslcomp2.count(); ii++) {
				if ((ii + 1) < 33) {
					bncs_string ssleftid = "", ssInst = "";
					sslcomp2[ii].split('=', ssleftid, ssInst);
					Debug("LoadComposites %s %d ", LPCSTR(ssleftid), getInstanceDevice(ssInst));
					cl_KeyInstances->i_dev_Pkgr_Router_Main[ii + 1] = getInstanceDevice(ssInst);
				}
			}
			if ((sslcomp2.count() > 0) && (sslcomp2.count() < 33)) {
				Debug("LoadComposites   1 = %s and %d", LPCSTR(sslcomp2[0]), cl_KeyInstances->i_dev_Pkgr_Router_Main[1]);
				Debug("LoadComposites %d = %s and %d", sslcomp2.count(), LPCSTR(sslcomp2[sslcomp2.count() - 1]), cl_KeyInstances->i_dev_Pkgr_Router_Main[sslcomp2.count()]);
			}
		}
		else
			Debug("LoadComposites - FAIL for key router composite");
		//
		if (cl_KeyInstances->ssMixMinusComposite.length() > 0) {
			bncs_stringlist sslcomp2 = bncs_stringlist(GetCompositeInstance(cl_KeyInstances->ssMixMinusComposite), ',');
			for (int ii = 0; ii < sslcomp2.count(); ii++) {
				if (ii + 1 < 33) {
					bncs_string ssleftid = "", ssInst = "";
					sslcomp2[ii].split('=', ssleftid, ssInst);
					Debug("LoadComposites %s %d ", LPCSTR(ssleftid), getInstanceDevice(ssInst));
					cl_KeyInstances->i_dev_Pkgr_MixMinus_Main[ii + 1] = getInstanceDevice(ssInst);
				}
			}
			if ((sslcomp2.count() > 0) && (sslcomp2.count() < 33)) {
				Debug("LoadComposites  1 = %s and %d", LPCSTR(sslcomp2[0]), cl_KeyInstances->i_dev_Pkgr_MixMinus_Main[1]);
				Debug("LoadComposites  %d = %s and %d", sslcomp2.count(), LPCSTR(sslcomp2[sslcomp2.count() - 1]), cl_KeyInstances->i_dev_Pkgr_MixMinus_Main[sslcomp2.count()]);
			}
		}
		else
			Debug("LoadComposites - FAIL for key MixM composite");
		//
		if (cl_KeyInstances->ssAPReturnsComposite.length() > 0) {
			bncs_stringlist sslcomp2 = bncs_stringlist(GetCompositeInstance(cl_KeyInstances->ssAPReturnsComposite), ',');
			for (int ii = 0; ii < sslcomp2.count(); ii++) {
				if (ii + 1 < 33) {
					bncs_string ssleftid = "", ssInst = "";
					sslcomp2[ii].split('=', ssleftid, ssInst);
					Debug("LoadComposites %s %d ", LPCSTR(ssleftid), getInstanceDevice(ssInst));
					cl_KeyInstances->i_dev_Pkgr_APReturns_Main[ii + 1] = getInstanceDevice(ssInst);
				}
			}
			if ((sslcomp2.count() > 0) && (sslcomp2.count() < 33)) {
				Debug("LoadComposites  1 = %s and %d", LPCSTR(sslcomp2[0]), cl_KeyInstances->i_dev_Pkgr_APReturns_Main[1]);
				Debug("LoadComposites %d = %s and %d", sslcomp2.count(), LPCSTR(sslcomp2[sslcomp2.count() - 1]), cl_KeyInstances->i_dev_Pkgr_APReturns_Main[sslcomp2.count()]);
			}
		}
		else
			Debug("LoadComposites - FAIL for key AP composite");
		//
		if (cl_KeyInstances->ssDestStatusComposite.length() > 0) {
			bncs_stringlist sslcomp2 = bncs_stringlist(GetCompositeInstance(cl_KeyInstances->ssDestStatusComposite), ',');
			for (int ii = 0; ii < sslcomp2.count(); ii++) {
				if (ii + 1 < 33) {
					bncs_string ssleftid = "", ssInst = "";
					sslcomp2[ii].split('=', ssleftid, ssInst);
					Debug("LoadComposites %s %d ", LPCSTR(ssleftid), getInstanceDevice(ssInst));
					cl_KeyInstances->i_dev_Pkgr_DestStatus_Main[ii + 1] = getInstanceDevice(ssInst);
				}
			}
			if ((sslcomp2.count() > 0) && (sslcomp2.count() < 33)) {
				Debug("LoadComposites 1 = %s and %d", LPCSTR(sslcomp2[0]), cl_KeyInstances->i_dev_Pkgr_DestStatus_Main[1]);
				Debug("LoadComposites %d = %s and %d", sslcomp2.count(), LPCSTR(sslcomp2[sslcomp2.count() - 1]), cl_KeyInstances->i_dev_Pkgr_DestStatus_Main[sslcomp2.count()]);
			}
		}
		else
			Debug("LoadComposites - FAIL for key DestStat composite");
		//
		if (cl_KeyInstances->ssSrcePTIComposite.length() > 0) {
			bncs_stringlist sslcomp2 = bncs_stringlist(GetCompositeInstance(cl_KeyInstances->ssSrcePTIComposite), ',');
			for (int ii = 0; ii < sslcomp2.count(); ii++) {
				if (ii + 1 < 33) {
					bncs_string ssleftid = "", ssInst = "";
					sslcomp2[ii].split('=', ssleftid, ssInst);
					Debug("LoadComposites %s %d ", LPCSTR(ssleftid), getInstanceDevice(ssInst));
					cl_KeyInstances->i_dev_Pkgr_SrcePTI_Main[ii + 1] = getInstanceDevice(ssInst);
				}
			}
			if ((sslcomp2.count() > 0) && (sslcomp2.count() < 33)) {
				Debug("LoadComposites -1 = %s and %d", LPCSTR(sslcomp2[0]), cl_KeyInstances->i_dev_Pkgr_SrcePTI_Main[1]);
				Debug("LoadComposites -%d = %s and %d", sslcomp2.count(), LPCSTR(sslcomp2[sslcomp2.count() - 1]), cl_KeyInstances->i_dev_Pkgr_SrcePTI_Main[sslcomp2.count()]);
			}
		}
		else
			Debug("LoadComposites - FAIL for key SrcePTI composite");
		//
		if (cl_KeyInstances->ssNevionDestUseComposite.length() > 0) {
			bncs_stringlist sslcomp2 = bncs_stringlist(GetCompositeInstance(cl_KeyInstances->ssNevionDestUseComposite), ',');
			for (int ii = 0; ii < sslcomp2.count(); ii++) {
				if (ii + 1 < 33) {
					bncs_string ssleftid = "", ssInst = "";
					sslcomp2[ii].split('=', ssleftid, ssInst);
					Debug("LoadComposites %s %d ", LPCSTR(ssleftid), getInstanceDevice(ssInst));
					cl_KeyInstances->i_dev_Pkgr_NevionDestUse_Main[ii + 1] = getInstanceDevice(ssInst);
				}
			}
			if ((sslcomp2.count() > 0) && (sslcomp2.count() < 33)) {
				Debug("LoadComposites -1 = %s and %d", LPCSTR(sslcomp2[0]), cl_KeyInstances->i_dev_Pkgr_NevionDestUse_Main[1]);
				Debug("LoadComposites -%d = %s and %d", sslcomp2.count(), LPCSTR(sslcomp2[sslcomp2.count() - 1]), cl_KeyInstances->i_dev_Pkgr_NevionDestUse_Main[sslcomp2.count()]);
			}
		}
		else
			Debug("LoadComposites - FAIL for key NevionDestUse composite");
		//
		if (cl_KeyInstances->ssPkgrRiedelComposite.length() > 0) {
			bncs_stringlist sslcomp2 = bncs_stringlist(GetCompositeInstance(cl_KeyInstances->ssPkgrRiedelComposite), ',');
			for (int ii = 0; ii < sslcomp2.count(); ii++) {
				if (ii + 1 < 33) {
					bncs_string ssleftid = "", ssInst = "";
					sslcomp2[ii].split('=', ssleftid, ssInst);
					Debug("LoadComposites %s %d ", LPCSTR(ssleftid), getInstanceDevice(ssInst));
					cl_KeyInstances->i_dev_Pkgr_PkgrRiedel_Main[ii + 1] = getInstanceDevice(ssInst);
				}
			}
			if ((sslcomp2.count() > 0) && (sslcomp2.count() < 33)) {
				Debug("LoadComposites -1 = %s and %d", LPCSTR(sslcomp2[0]), cl_KeyInstances->i_dev_Pkgr_PkgrRiedel_Main[1]);
				Debug("LoadComposites -%d = %s and %d", sslcomp2.count(), LPCSTR(sslcomp2[sslcomp2.count() - 1]), cl_KeyInstances->i_dev_Pkgr_PkgrRiedel_Main[sslcomp2.count()]);
			}
		}
		else
			Debug("LoadComposites - FAIL for key PkgrRiedel composite");

		//
		// test for all devs > 0 - then continue
		if ((cl_KeyInstances->i_dev_Pkgr_Router_Main[1] > 0) && (cl_KeyInstances->i_dev_Pkgr_MixMinus_Main[1] > 0) &&
			(cl_KeyInstances->i_dev_Pkgr_APReturns_Main[1] > 0) && (cl_KeyInstances->i_dev_Pkgr_DestStatus_Main[1] > 0) &&
			(cl_KeyInstances->i_dev_Pkgr_SrcePTI_Main[1] > 0) && (cl_KeyInstances->i_dev_Pkgr_NevionDestUse_Main[1] > 0) &&
			(cl_KeyInstances->i_dev_Pkgr_PkgrRiedel_Main[1] > 0)) {
			Debug("LoadComposites -loaded ");
			return TRUE;
		}
		else
			Debug("LoadComposites -ONE or more composite devices is 0 ");
	}
	return FALSE;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: ConnectAllInfodrivers()
//
//  PURPOSE:connects to 4 infodrivers needed for app to run
//  if it fails then no point in continuing to run - so flags major error
//			
BOOL ConnectAllInfodrivers( )
{

	bValidCollediaStatetoContinue = TRUE;
	bncs_string ssXmlConfigFile = "packager_config";
	char szString[MAX_AUTO_BUFFER_STRING] = "";

	if (cl_KeyInstances) {

		// get infodriver numbers from loaded composites now
		iPackager_router_main = cl_KeyInstances->i_dev_Pkgr_Router_Main[1];
		iPackager_mm_stack_main = cl_KeyInstances->i_dev_Pkgr_MixMinus_Main[1];
		iPackager_audio_package_main = cl_KeyInstances->i_dev_Pkgr_APReturns_Main[1];
		iPackager_dest_status_main = cl_KeyInstances->i_dev_Pkgr_DestStatus_Main[1];
		iPackager_source_pti_main = cl_KeyInstances->i_dev_Pkgr_SrcePTI_Main[1];
		iPackager_nevion_dest_main = cl_KeyInstances->i_dev_Pkgr_NevionDestUse_Main[1];
		iPackager_riedel_conference_main = cl_KeyInstances->i_dev_Pkgr_PkgrRiedel_Main[1];
		iPackager_riedel_port_use_main = cl_KeyInstances->i_dev_Pkgr_PkgrRiedel_Main[2];
		iPackager_riedel_ifbs_main = cl_KeyInstances->i_dev_Pkgr_PkgrRiedel_Main[3];

		wsprintf(szString, "%d %s", iPackager_router_main, "router main"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);
		wsprintf(szString, "%d %s", iPackager_mm_stack_main, "mm stack main"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);
		wsprintf(szString, "%d %s", iPackager_audio_package_main, "audio pkg main"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);
		wsprintf(szString, "%d %s", iPackager_dest_status_main, "dest stat main"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);
		wsprintf(szString, "%d %s", iPackager_source_pti_main, "srce pti main"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);
		wsprintf(szString, "%d %s", iPackager_nevion_dest_main, "nevion dest main"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);
		wsprintf(szString, "%d %s", iPackager_riedel_conference_main, "riedel conf"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);
		wsprintf(szString, "%d %s", iPackager_riedel_port_use_main, "riedel port"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);
		//wsprintf(szString, "%d %s", iPackager_riedel_ifbs_main, "riedel ifbs"); SendDlgItemMessage(hWndDlg, IDC_LISTINFOS, LB_INSERTSTRING, -1, (LPARAM)szString);

		iNumberOfPackages = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "number_of_packages").toInt();
		iInfodriverDivisor = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "packager_divisor").toInt();
		if ((iInfodriverDivisor < 1)||(iInfodriverDivisor>4000)) iInfodriverDivisor = 4000;
		iNumberInfosInBlock = iNumberOfPackages / iInfodriverDivisor; // max 32;
		iNumberOfInfodriversRequired = (iNumberInfosInBlock * 6) + 2; // 194;   // may move to 195 if IFBs main ( riedel 3 ) is required
		iPackagerBarsSrcePkg = iNumberOfPackages + 1;
		iPackagerParkSrcePkg = iNumberOfPackages + 2;
		Debug("Packager init - Defined Packages %d - req infodrivers %d  divisor %d NumInfosBlock %d ", 
			iNumberOfPackages, iNumberOfInfodriversRequired, iInfodriverDivisor, iNumberInfosInBlock);

		if ((iNumberOfPackages > 0) && (iNumberOfPackages <= (iInfodriverDivisor*iNumberInfosInBlock)) 
			&& (iNumberOfInfodriversRequired > 0) && (iNumberInfosInBlock > 0) && (iNumberInfosInBlock < 33)) {

			// infodrivers are in 6 groups with blocks of max 32 allocated
			for (int iis = AUTO_PACK_ROUTE_INFO; iis <= AUTO_RTR_DEST_USE_INFO; iis++) {
				for (int iib = 1; iib <= iNumberInfosInBlock; iib++) {
					int iIndex = ((iis - 1)*iNumberInfosInBlock) + iib;
					int iActualInfoDev = 0;
					switch (iis) {
						case AUTO_PACK_ROUTE_INFO:  iActualInfoDev = cl_KeyInstances->i_dev_Pkgr_Router_Main[iib]; break;
						case AUTO_MM_STACK_INFO:iActualInfoDev = cl_KeyInstances->i_dev_Pkgr_MixMinus_Main[iib]; break;
						case AUTO_AUDIO_PKG_INFO: iActualInfoDev = cl_KeyInstances->i_dev_Pkgr_APReturns_Main[iib]; break;
						case AUTO_DEST_PKG_STATUS_INFO: iActualInfoDev = cl_KeyInstances->i_dev_Pkgr_DestStatus_Main[iib]; break;
						case AUTO_SRCE_PKG_PTI_INFO: iActualInfoDev = cl_KeyInstances->i_dev_Pkgr_SrcePTI_Main[iib]; break;
						case AUTO_RTR_DEST_USE_INFO:  iActualInfoDev = cl_KeyInstances->i_dev_Pkgr_NevionDestUse_Main[iib]; break;
					}
					if ((iIndex > 0) && (iIndex < MAX_INFODRIVERS)) i_rtr_Infodrivers[iIndex] = iActualInfoDev;
					Debug("loadAllinit - infos are for group %d block %d index %d val %d", iis, iib, iIndex, iActualInfoDev);
				}
			}
			// then add in 3 Riedel infodrivers 
			int iIndex = (AUTO_RTR_DEST_USE_INFO*iNumberInfosInBlock);
			if (iIndex + 1 < MAX_INFODRIVERS) {
				i_rtr_Infodrivers[iIndex + 1] = iPackager_riedel_conference_main;     // AUTO_RIEDEL_CONF_IFB_INFO type
				Debug("loadAllinit - infos are for riedel conf index %d ", i_rtr_Infodrivers[iIndex + 1]);
			}
			if (iIndex + 2 < MAX_INFODRIVERS) {
				i_rtr_Infodrivers[iIndex + 2] = iPackager_riedel_port_use_main;   // AUTO_RIEDEL_PORT_USE_INFO type
				Debug("loadAllinit - infos are for riedel port index %d ", i_rtr_Infodrivers[iIndex + 2]);
			}
			if (iIndex + 3 < MAX_INFODRIVERS) {
				i_rtr_Infodrivers[iIndex + 3] = iPackager_riedel_ifbs_main;   // AUTO_RIEDEL_RING 1 IFBS _INFO type
				Debug("loadAllinit - infos are for riedel ring IFBS %d  ", i_rtr_Infodrivers[iIndex + 3]);
			}

			// update gui
			Debug("ConnAllInfos : devs : %d - %d", i_rtr_Infodrivers[1], i_rtr_Infodrivers[iIndex + 2]);
			wsprintf(szString, "%d-%d", i_rtr_Infodrivers[1], i_rtr_Infodrivers[iIndex + 2]);
			SetDlgItemText(hWndDlg, IDC_AUTO_INFNUM1, szString);

			bValidCollediaStatetoContinue = TRUE;
			Debug("LoadAllInit - Connecting to Infodrivers");
			// connect to External Infodrivers
			for (int iif = 1; iif <= iNumberOfInfodriversRequired; iif++) {
				if (i_rtr_Infodrivers[iif] > 0) {
					eiInfoDrivers[iif].notify(InfoNotify);
					switch (eiInfoDrivers[iif].connect(i_rtr_Infodrivers[iif])) { // connect to infodriver
					case CONNECTED:
						Debug("Connected OK to packager infodriver %d ", eiInfoDrivers[iif].iDevice);
						eiInfoDrivers[iif].setcounters(&lTXID, &lRXID);
						// set infodriver to master of main base info  --- ???
						eiInfoDrivers[iif].setRedundancyMaster(iPackager_router_main);
						eiInfoDrivers[iif].closeInfodriverOnShutdown(TRUE);   // set flag to close infodriver 
						break;
					default:
						Debug("LoadAllIinit - ERROR ERROR - Connect failed to infodriver %d, code %d", i_rtr_Infodrivers[iif]);
						bValidCollediaStatetoContinue = FALSE;
					}
				}
				else
					Debug("LoadAllIinit - info connects - index %d infodriver zero", iif);
			}
			// register for RMs from main infodriver
			if (ecCSIClient) ecCSIClient->regtallyrange(eiInfoDrivers[AUTO_PACK_ROUTE_INFO].iDevice, 4094, 4094, INSERT);

			if (bValidCollediaStatetoContinue) {
				// check on infodrivers status -- really ought to be either all TX/RX or all RXOnly -- a mix is not good !!
				// when determining overall status -- main infodriver ( iDevice ) is very important
				iOverallTXRXModeStatus = 0; // unknown first time
				// continue on to initialise vars, get object settings, dev registrations and poll for revs 
				eiInfoDrivers[AUTO_PACK_ROUTE_INFO].updateslot(COMM_STATUS_SLOT, "1");	 // ok	
				return TRUE;
			}
			else
				Debug("ConnectAllInfodrivers - invalidState to Continue");

		}
		else
			Debug("ConnectAllInfodrivers *** ERROR In NumberPackages or Divisor - Check/Fix config *** ");

	}
	else 
		Debug("ConnectAllInfodrivers - No Composite Instances class");

	// something has failed so flag it
	bValidCollediaStatetoContinue = FALSE;
	iLabelAutoStatus=2;
	SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
	return FALSE;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: LoadRiedelDBData()
//
//  COMMENTS:  All this riedel data is required for IFBs and CONFs
//			
// Riedel Database info - loaded at start on creation of a ring now
int CreateRiedel_PortKeys( CRiedel_Rings* pRing, int iWhichDb)
{
	int iEntries = 0;
	if (pRing) {
		BOOL bCont = TRUE;
		// load file into db map and process from there
		mapOfDBFile = new map<int, CCommand*>;
		if (mapOfDBFile) {
			int iHighestEntry = LoadDBFile(pRing->iRing_Device_Monitor, iWhichDb, MAX_RIEDEL_SLOTS);
			//
			for (int iIndx = 1; iIndx < MAX_RIEDEL_SLOTS; iIndx++) {
				CCommand* pRec = GetDBFileRecord(iIndx);
				if (pRec) {
					bncs_string ssData = bncs_string(pRec->szCommand);
					if ((ssData.length() > 0) && (ssData.find("NULL") < 0)) {
						bncs_stringlist ssl_modify = bncs_stringlist(ssData, '.');
						if (ssl_modify.count() == 1) {
							ssData.append(".0.1.0");
						}
						else if (ssl_modify.count() == 2)	{
							// Insert in the string the values '0.1' to update it to the latest config setting - Should now be Port/Panel/Shift/Key we are setting it to "x.0.1.x"
							ssData = ssl_modify[0];
							ssData.append(".0.1.");
							ssData.append(ssl_modify[1]);
						}
						//
						ssl_modify = bncs_stringlist(ssData, '.');
						if (ssl_modify.count() == 4) {
							// prepend trunk address now to make address 5 elements long
							ssl_modify.prepend(bncs_string("%1").arg(pRing->m_iTrunkAddress));
							ssData = ssl_modify.toString('.');
						}
						// Debug("CreateRiedelPortKeys - PostMantis 1166 key address - %s ", LPCSTR(ssData));
						iEntries++;
						pRing->AssignLookupAddress(iIndx, ssData);
					}
				}
			} // for iIndx

			// clear out map
			while (mapOfDBFile->begin() != mapOfDBFile->end()) {
				map<int, CCommand*>::iterator it = mapOfDBFile->begin();
				if (it->second) delete it->second;
				mapOfDBFile->erase(it);
			}
			// delete map
			delete mapOfDBFile;
		}
		else
			Debug("CreateRiedel_PortKeys - no MapDBFile created");
	}
	else
		Debug("CreateRiedel_PortKeys - invalid ring class passed");
	return iEntries;
}


//////////////////////////////////////////////////////////////////////////////////////////////


BOOL CreateRiedelPortUseRecords( void )
{
	// load db data for riedel and other known devices that is static / current
	map_RiedelPortPackageUse = new map<int, CRiedelPortPackageUse*>;
	for (int i = 1; i < MAX_RIEDEL_SLOTS; i++) {
		CRiedelPortPackageUse* pTB = new CRiedelPortPackageUse(i);
		if (pTB&&map_RiedelPortPackageUse) {
			map_RiedelPortPackageUse->insert(pair<int, CRiedelPortPackageUse*>(i, pTB));
		}
	}
	return TRUE;
	
}


void CreateParkAndBarsSourcePackages( void )
{
	// BARS
	CSourcePackages* pSPkg2  = new CSourcePackages;
	if (pSPkg2) {
		pSPkg2->iPackageIndex = iPackagerBarsSrcePkg;
		pSPkg2->ss_PackageLongName.append("BARS");
		pSPkg2->ss_PackageButtonName .append("BARS");
		pSPkg2->ss_PackageMCRUMD.append("BARS");
		//create all uhd and silence audio data
		for (int ii = 0; ii < MAX_PKG_VIDEO; ii++) {
			pSPkg2->st_PkgVideoLevels[ii].stVideoLevel.iAssocIndex = intHD_BARS;
			pSPkg2->st_SourceAudioLevels[ii].iAssocIndex = intHD_SILENCE;
			for (int iianc = 0; iianc < MAX_PKG_VIDEO; iianc++) pSPkg2->st_PkgVideoLevels[ii].stANCLevel[iianc].iAssocIndex = intHD_BARS;
		}
		// insert into map
		mapOfAllSrcPackages->insert(pair<int, CSourcePackages*>(iPackagerBarsSrcePkg, pSPkg2));
	}
	else
		Debug("CreateParkandBars - park record fail");
	
	// PARK - also used for black sources when required
	CSourcePackages* pSPkg  = new CSourcePackages;
	if (pSPkg) {
		pSPkg->iPackageIndex = iPackagerParkSrcePkg;
		pSPkg->ss_PackageLongName.append("PARK");
		pSPkg->ss_PackageButtonName.append("PARK");
		pSPkg->ss_PackageMCRUMD.append("PARK");
		//create all black and silence audio data
		for (int ii = 0; ii < MAX_PKG_VIDEO; ii++) {
			pSPkg2->st_PkgVideoLevels[ii].stVideoLevel.iAssocIndex = 0;
			pSPkg2->st_SourceAudioLevels[ii].iAssocIndex = 0;
			for (int iianc = 0; iianc < MAX_PKG_VIDEO; iianc++) pSPkg2->st_PkgVideoLevels[ii].stANCLevel[iianc].iAssocIndex = 0;
		}
		// insert into map
		mapOfAllSrcPackages->insert(pair<int, CSourcePackages*>(iPackagerParkSrcePkg, pSPkg));
	}
	else
		Debug("CreateParkandBars - park record fail");
	
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
void CreateAndQueueFullIFBCommand( int iIFBRecord, BOOL bPowerUpIFB)
{
	CRingMaster_IFB* pRMIfb = GetRingMaster_IFB_Record(iIFBRecord);
	if (pRMIfb) {
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pRMIfb->getAssignedTrunkAddr());
		if (pRing) {

			int iIFB = ((iIFBRecord - 1) % MAX_IFBS) + 1;

			char szCommand[MAX_AUTO_BUFFER_STRING] = "";
			char szInputs[MAX_AUTO_BUFFER_STRING] = "";
			char szMixMinus[MAX_AUTO_BUFFER_STRING] = "";
			char szOutputs[MAX_AUTO_BUFFER_STRING] = "";
			char szFinalLabel[MAX_AUTO_BUFFER_STRING] = "";

			int iIfbRing = pRMIfb->getAssignedTrunkAddr();

			if ((pRMIfb->getAssociatedAudioPackage() > 0) || (bPowerUpIFB)) {
				// get ports etc - chcek MM Q for dest pkg at top of Q etc  -- or assign any xml config ports
				int iWhichAPifb = 0;
				if ((pRMIfb->getAssociatedWhichDynIFB() >= DYN_IFB_4WCONF_1) && (pRMIfb->getAssociatedWhichDynIFB() <= DYN_IFB_4WCONF_4))
					iWhichAPifb = pRMIfb->getAssociatedWhichDynIFB() - DYN_IFB_4WCONF_1 + 1;

				char szRevLabel[MAX_AUTO_BUFFER_STRING] = "";
				char szRevInputs[MAX_AUTO_BUFFER_STRING] = "";
				char szRevOutPorts[MAX_AUTO_BUFFER_STRING] = "";
				char szRevMixMinus[MAX_AUTO_BUFFER_STRING] = "";
				// curr revs
				strcpy_s(szRevInputs, LPCSTR(pRMIfb->getInputs()));
				strcpy_s(szRevMixMinus, LPCSTR(pRMIfb->getMixMinus()));
				strcpy_s(szRevOutPorts, LPCSTR(pRMIfb->getOutputs()));
				strcpy_s(szRevLabel, LPCSTR(pRMIfb->getLabel()));
				if (bShowAllDebugMessages) Debug("CreateIFBCmd - IFB %d  -revs- inputs %s mixMinus %s outputs %s Label %s ", iIFB, szRevInputs, szRevMixMinus, szRevOutPorts, szRevLabel);

				BOOL bSendMsg = FALSE;
				CAudioPackages* pAudio = GetAudioPackage(pRMIfb->getAssociatedAudioPackage());
				if ((pAudio) && (iWhichAPifb>0) && (iWhichAPifb<MAX_PKG_CONFIFB)) {
					// get ports  from audio package; Check for routed DP(s) ?? assoc to AP and which DP ifb to use - add in as MM at this point 
					wsprintf(szInputs, "%d.%d", pAudio->stCFIFBs[iWhichAPifb].i_in_ring, pAudio->stCFIFBs[iWhichAPifb].i_in_port);
					wsprintf(szOutputs, "%d.%d", pAudio->stCFIFBs[iWhichAPifb].i_out_ring, pAudio->stCFIFBs[iWhichAPifb].i_out_port);
					strcpy(szFinalLabel, pAudio->stCFIFBs[iWhichAPifb].m_ss_MainLabel);
					// is there any MM from a Desty pkg AT TOP OF MM Q of audio package
					if ((pAudio->ssl_Pkg_Queue.count() > 0) && (pAudio->stCFIFBs[iWhichAPifb].i_Use_Which_DestPkgIFB>0)) {
						// get dest pkg top of MMQ
						int iDest = pAudio->ssl_Pkg_Queue[0].toInt();
						CDestinationPackages* pDest = GetDestinationPackage(iDest);
						if (pDest) {
							int idr = 0, idp = 0;
							if (pAudio->stCFIFBs[iWhichAPifb].i_Use_Which_DestPkgIFB == 1) {
								if ((pDest->st_AssocIFBs[1].i_mm_ring > 0) && (pDest->st_AssocIFBs[1].i_mm_port > 0)) {
									wsprintf(szMixMinus, "%d.%d", pDest->st_AssocIFBs[1].i_mm_ring, pDest->st_AssocIFBs[1].i_mm_port);
								}
							}
							else if (pAudio->stCFIFBs[iWhichAPifb].i_Use_Which_DestPkgIFB == 2) {
								if ((pDest->st_AssocIFBs[2].i_mm_ring > 0) && (pDest->st_AssocIFBs[2].i_mm_port > 0)) {
									wsprintf(szMixMinus, "%d.%d", pDest->st_AssocIFBs[2].i_mm_ring, pDest->st_AssocIFBs[2].i_mm_port);
								}
							}
						}
					}
					//
					bSendMsg = TRUE;
				}
				else if (bPowerUpIFB) {
					// no associated audio package - but powering up -- from xml data ??? xxx to do  REALLY ????
					// enable if ports assigned for IFB command --- bSendMsg = TRUE;
				}
				else
					Debug("CreateFullIFB - no AudioPkg or power up - no action taken ");

				if (bSendMsg) {
					int iWhichIFBInfoDrv = i_RingMaster_Ifb_Start + pRing->m_iRecordIndex - 1;
					if ((iWhichIFBInfoDrv >= i_RingMaster_Ifb_Start) && (iWhichIFBInfoDrv <= i_RingMaster_Ifb_End)) {
						wsprintf(szCommand, "IW %d '%s|%s|%s|%s' %d", iWhichIFBInfoDrv, szInputs, szMixMinus, szOutputs, szFinalLabel, iIFB);
						AddCommandToQue(szCommand, INFODRVCOMMAND, iWhichIFBInfoDrv, 0, 0);
					}
					else
						Debug("CreateIFBCmd - ERROR - invalid ifb info %d from ifb rec %d ring %d", iWhichIFBInfoDrv, iIFB, pRing->m_iRecordIndex);
				}

			}
			else {
				// no assoc Audio Pkg or depowering - so clear IFB
				int iWhichIFBInfoDrv = i_RingMaster_Ifb_Start + pRing->m_iRecordIndex - 1;
				if ((iWhichIFBInfoDrv >= i_RingMaster_Ifb_Start) && (iWhichIFBInfoDrv <= i_RingMaster_Ifb_End)) {
					wsprintf(szCommand, "IW %d '|||' %d", iWhichIFBInfoDrv, iIFB);
					AddCommandToQue(szCommand, INFODRVCOMMAND, iWhichIFBInfoDrv, 0, 0);
				}
				else
					Debug("CreateIFBCmd - ERROR - invalid ifb info %d from ifb rec %d ring %d", iWhichIFBInfoDrv, iIFB, pRing->m_iRecordIndex);
			}
		}
	}
}


void ApplyGivenLabelToIFBKeys(int iIfbRing, int iIfb, bncs_string ssLabel, bncs_string subTitle)
{
	// for ring - get monitor device,   for ifb get list of known monitor keys - send command to those keys on those rings
	char szCommand[MAX_AUTO_BUFFER_STRING] = "";

	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIfb);
	if (pRMstrIFB) {
		bncs_stringlist ssl = bncs_stringlist(pRMstrIFB->getRingMasterCalcMonitorMembers(), ',');
		for (int iK = 0; iK < ssl.count(); iK++) {
			int iTrunk = iIfbRing;
			int iMonSlot = 0;
			GetTrunkAndPortFromString(ssl[iK], iIfbRing, &iTrunk, &iMonSlot);
			CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
			if ((pRing) && (iMonSlot>0)) {
				wsprintf(szCommand, "IW %d '&LABEL=%s' %d", pRing->iRing_Device_Monitor, LPCSTR(ssLabel), iMonSlot);
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Monitor, 0, 0);
			}
			else
				Debug("ApplyGivenLabel - no ring record for %d or zero mon slot", iTrunk);
		} // for iK
	}
	else
		Debug("ApplyGivenLabel - no IFB record for %d ", iIfb);
}


void ApplyCFConferenceToIFBKeys( int iIfb, int iCF_Conf)
{
	// add cf conference to any keys on ifb
	char szCommand[MAX_AUTO_BUFFER_STRING] = "";
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIfb);
	if (pRMstrIFB) {
		bncs_stringlist ssl = bncs_stringlist(pRMstrIFB->getRingMasterCalcMonitorMembers(), ',');
		for (int iK = 0; iK < ssl.count(); iK++) {
			int iTrunk = pRMstrIFB->getAssignedTrunkAddr();
			int iMonSlot = 0;
			GetTrunkAndPortFromString(ssl[iK], pRMstrIFB->getAssignedTrunkAddr(), &iTrunk, &iMonSlot);
			CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
			if ((pRing) && (iMonSlot>0)) {
				wsprintf(szCommand, "IW %d '&ADD=Conference|%d|TL' %d", pRing->iRing_Device_Monitor, iCF_Conf, iMonSlot);
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Monitor, 0, 0);
			}
			else
				Debug("ApplyCFConferenceToIFBKeys - no ring record for %d or zero mon slot", iTrunk);
		} // for iK
	}
	else
		Debug("ApplyCFConferenceToIFBKeys - no IFB record for %d ", iIfb);
}


void ClearCFConferenceFromIFBKeys(int iIfb, int iCF_Conf)
{
	// clear cf conf from any keys with IFB
	char szCommand[MAX_AUTO_BUFFER_STRING] = "";
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIfb);
	if (pRMstrIFB) {
		bncs_stringlist ssl = bncs_stringlist(pRMstrIFB->getRingMasterCalcMonitorMembers(), ',');
		for (int iK = 0; iK < ssl.count(); iK++) {
			int iTrunk = pRMstrIFB->getAssignedTrunkAddr();
			int iMonSlot = 0;
			GetTrunkAndPortFromString(ssl[iK], pRMstrIFB->getAssignedTrunkAddr(), &iTrunk, &iMonSlot);
			CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
			if ((pRing) && (iMonSlot>0)) {
				wsprintf(szCommand, "IW %d '&REMOVE=Conference|%d|TL' %d", pRing->iRing_Device_Monitor, iCF_Conf, iMonSlot);
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Monitor, 0, 0);
			}
			else
				Debug("ClearCFConferenceFromIFBKeys - no ring record for %d or zero mon slot", iTrunk);
		} // for iK
	}
	else
		Debug("ClearCFConferenceFromIFBKeys - no IFB record for %d ", iIfb);
}

void ClearAllFunctionsOnKnownIFBKeys(int iIfb)
{
	// clear cf conf from any keys with IFB
	char szCommand[MAX_AUTO_BUFFER_STRING] = "";
	CRingMaster_IFB* pRMstrIFB = GetRingMaster_IFB_Record(iIfb);
	if (pRMstrIFB) {
		bncs_stringlist ssl = bncs_stringlist(pRMstrIFB->getRingMasterCalcMonitorMembers(), ',');
		for (int iK = 0; iK < ssl.count(); iK++) {
			int iTrunk = pRMstrIFB->getAssignedTrunkAddr();
			int iMonSlot = 0;
			GetTrunkAndPortFromString(ssl[iK], pRMstrIFB->getAssignedTrunkAddr(), &iTrunk, &iMonSlot);
			CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
			if ((pRing) && (iMonSlot>0)) {
				wsprintf(szCommand, "IW %d '%s' %d", pRing->iRing_Device_Monitor, LPCSTR(ssKeyClearCmd), iMonSlot);
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Monitor, 0, 0);
			}
			else
				Debug("ClearCFConferenceFromIFBKeys - no ring record for %d or zero mon slot", iTrunk);
		} // for iK
	}
	else
		Debug("ClearCFConferenceFromIFBKeys - no IFB record for %d ", iIfb);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
void MakeRiedelGRDRoute(int iSourceRing, int iSource, int iDestRing, int iDest, BOOL bSendNow)
{
	// get ringmaster slot for ring.dest and write source into it
	CRingMaster_Ports* pRMstrPort = GetRingPortsDefintionByRingDest(iDestRing, iDest);
	if (pRMstrPort) {
		char szCommand[MAX_AUTO_BUFFER_STRING] = "";
		// send command to ringmaster -- will route direct or use highway if needed
		if (pRMstrPort->iPortsInfodrv>0) {
			wsprintf(szCommand, "IW %d '%d.%d' %d ", pRMstrPort->iPortsInfodrv, iSourceRing, iSource, pRMstrPort->iPortsInfodrvSlot);
			if ((bSendNow ) && (ecCSIClient))
				ecCSIClient->txinfocmd(szCommand, NOW);
			else
				AddCommandToQue(szCommand, INFODRVCOMMAND, pRMstrPort->iPortsInfodrv, 0, 0);
		}
	}
	else
		Debug("MakeRiedelGRDRoute -ERROR- no ringmaster port entry for %d.%d", iDestRing, iDest);
}


///////////////////////////////////////////////////////////////////////////////////////
//
// SOURCE PACKAGE PROCESSING
//
//////////////////////////////////////////////////////////////////////////////////////

void UpdateRoutedVirtualsVLevel(int iPkgIndex, int  iPrevVirPkg, int iNewVirPkg)
{
	if ((iPkgIndex > 0) && (iPkgIndex < i_Start_VirtualPackages))
	{
		// only do this for real destination packages - that have a virtual routed to them.
		char szCmdRev[MAX_AUTO_BUFFER_STRING] = "";
		CDestinationPackages* pDstPkg = GetDestinationPackage(iPkgIndex);
		if (pDstPkg) {
			// clear previous - if a virtual
			if ((iPrevVirPkg >= i_Start_VirtualPackages) && (iPrevVirPkg < (iNumberOfPackages+1))) {
				CDestinationPackages* pPrevVir = GetDestinationPackage(iPrevVirPkg);
				if (pPrevVir) {
					if (pPrevVir->i_VisionFrom_Source > 0) {
						// update only if above 0 - ie previously set
						pPrevVir->i_VisionFrom_Source = 0;
						pPrevVir->st_VideoHDestination.iMSPLevelAPIndx = 0;
						wsprintf(szCmdRev, "index=%d,v_level=0,v_index=0", pPrevVir->i_Routed_Src_Package);
						SendPackagerBNCSRevertive(iPrevVirPkg, AUTO_PACK_ROUTE_INFO, FALSE, szCmdRev);
						// RM to db101 now too -- but only if different from previous
						bncs_string ssCurrSync = bncs_string(ExtractNamefromDevIniFile(iPackager_router_main, iPrevVirPkg, DB_SYNC_PACKAGE_ROUTING));
						if (!AreBncsStringsEqual(ssCurrSync, bncs_string(szCmdRev)))
							AddCommandToQue(szCmdRev, DATABASECOMMAND, iPackager_router_main, iPrevVirPkg, DB_SYNC_PACKAGE_ROUTING);
					}
				}
			}
			// set new current - if a virtual
			if ((iNewVirPkg >= i_Start_VirtualPackages) && (iNewVirPkg < (iNumberOfPackages+1))) {
				CDestinationPackages* pNVir = GetDestinationPackage(iNewVirPkg);
				if (pDstPkg&&pNVir) {
					if (pNVir->i_VisionFrom_Source != pDstPkg->i_VisionFrom_Source) {
						pNVir->i_VisionFrom_Source = pDstPkg->i_VisionFrom_Source;
						pNVir->st_VideoHDestination.iMSPLevelAPIndx = pDstPkg->st_VideoHDestination.iMSPLevelAPIndx;
						wsprintf(szCmdRev, "index=%d,v_level=%d,v_index=%d", pNVir->i_Routed_Src_Package,
							pNVir->st_VideoHDestination.iMSPLevelAPIndx, pNVir->i_VisionFrom_Source);
						SendPackagerBNCSRevertive(iNewVirPkg, AUTO_PACK_ROUTE_INFO, FALSE, szCmdRev);
						// RM to db101 now too -- but only if different from previous
						bncs_string ssCurrSync = bncs_string(ExtractNamefromDevIniFile(iPackager_router_main, iNewVirPkg, DB_SYNC_PACKAGE_ROUTING));
						if (!AreBncsStringsEqual(ssCurrSync, bncs_string(szCmdRev)))
							AddCommandToQue(szCmdRev, DATABASECOMMAND, iPackager_router_main, iNewVirPkg, DB_SYNC_PACKAGE_ROUTING);
					}
				}
			}
		}
	}
}

void UpdateDestPkgRouteCommandInfodriver(int iPkgIndex)
{
	char szCmdRev[MAX_AUTO_BUFFER_STRING] = "";
	CDestinationPackages* pDstPkg = GetDestinationPackage(iPkgIndex);
	if (pDstPkg) {
		wsprintf(szCmdRev, "index=%d,v_level=%d,v_index=%d", pDstPkg->i_Routed_Src_Package,
			pDstPkg->st_VideoHDestination.iMSPLevelAPIndx, pDstPkg->i_VisionFrom_Source);
		SendPackagerBNCSRevertive(iPkgIndex, AUTO_PACK_ROUTE_INFO, FALSE, szCmdRev);
		// RM to db101 now too -- but only if different from previous
		//bncs_string ssCurrSync = bncs_string(ExtractNamefromDevIniFile(iPackager_router_main, iPkgIndex, DB_SYNC_PACKAGE_ROUTING));
		//if (!AreBncsStringsEqual(ssCurrSync, bncs_string(szCmdRev))) 
			AddCommandToQue(szCmdRev, DATABASECOMMAND, iPackager_router_main, iPkgIndex, DB_SYNC_PACKAGE_ROUTING);
			ProcessNextCommand(5);
	}
}


void ProcessListOfDestinationPackageStatusCalc()
{
	// process thru list checking dest pkg against current routes 
	for (int icount = 0; icount < ssl_DestPkgs_For_SDIStatus_Calc.count(); icount++) {
		// update pkg route command rev - mainly for virtuals if routed source or level has changed
		UpdateDestPkgRouteCommandInfodriver(ssl_DestPkgs_For_SDIStatus_Calc[icount].toInt());
		// get pkg // get dests and levels - get expected sources // get curr routed sources // compare // if diffs report
		UpdateDestPkgSdiStatusInfodriverSlot(ssl_DestPkgs_For_SDIStatus_Calc[icount].toInt(), FALSE);
	}
	//
	ssl_DestPkgs_For_SDIStatus_Calc.clear();
	iSDIStatusCalcCounter = 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//
//   Dest Package Routing functions
//

CSourcePackages*  DetermineVideoSourceLevel(bncs_string ssTrace, int iRequiredTag, int iForceLevel, int iForceLangUse, int *iReturnedRoutingLevel )
{
	*iReturnedRoutingLevel = 0;
	BOOL bPkgFound = FALSE;
	CSourcePackages* pRetPkg = NULL;
	bncs_stringlist sslTrace = bncs_stringlist(ssTrace, '|');
	// go back down trace thru each member looking for a match
	Debug("PROC DetVSrcL - reqtag %d list %d %s", iRequiredTag, sslTrace.count(), LPCSTR(ssTrace));
	for (int ii = sslTrace.count() - 1; ii >= 0; ii--) {
		if (ii >= 0) {
			pRetPkg = GetSourcePackage(sslTrace[ii].toInt());
			if (pRetPkg) {
				if ((iForceLevel > 0) || (iForceLangUse > 0)) {
					for (int iLvl = 1; iLvl < MAX_PKG_VIDEO; iLvl++) {
						if (pRetPkg->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocIndex>0) {
							// forced level ??
							if (iForceLevel == iLvl) {
								// enforced level so return first pkg found that has that level  set
								*iReturnedRoutingLevel = iLvl;
								bPkgFound = TRUE;
								return pRetPkg;
							}
							else if (pRetPkg->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocHub_or_TypeTag == iForceLangUse) {
								// force tag found so return 
								*iReturnedRoutingLevel = iLvl;
								bPkgFound = TRUE;
								return pRetPkg;
							}
						}
					} // for iLvl
				}
				// else find the required tag from the dest pkg
				else if (iRequiredTag > 0) {
					for (int iLvl = 1; iLvl < MAX_PKG_VIDEO; iLvl++) {
						if ((pRetPkg->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocIndex>0) && (pRetPkg->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocHub_or_TypeTag == iRequiredTag)) {
							// required tag found so return 
							*iReturnedRoutingLevel = iLvl;
							bPkgFound = TRUE;
							return pRetPkg;
						}
					} // for iLvl
				}
			}
		}
	} // for ii thru chain list of traced packages back to Primary
	Debug("PROC DetVSrcL no src pkg returned");
	return NULL;
}


int  DetermineParkVideoSourceLevel(int iRequiredTag, int iForceLevel, int iForceLangUse, int iParkPkg)
{
	CSourcePackages* pPark = GetSourcePackage(iParkPkg);
	if (pPark) {
		int iRetLevel = 0;
		if (iForceLevel > 0) return iForceLevel;
		//
		for (int iLvl = 1; iLvl < MAX_PKG_VIDEO; iLvl++) {
			if (pPark->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocIndex>0) {
				if ((iForceLangUse > 0) &&(pPark->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocHub_or_TypeTag == iForceLangUse))  {
					// required tag found so return 
					return iLvl;
				}
				else if ((iRequiredTag > 0) && (pPark->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocHub_or_TypeTag == iRequiredTag)) {
					// required tag found so return 
					return iLvl;
				}
			}
		} // for iLvl
	}
	return 0;
}


CAudioPackages* DetermineAudioSourcePackage(bncs_string ssTrace, int iRequiredLangTag, int iAudioTypeTag, int iRoutedVLevel, int *iWhichASP)
{
	*iWhichASP = 0;
	bncs_stringlist sslTrace = bncs_stringlist(ssTrace, '|');
	if (bShowAllDebugMessages) Debug("DetAudSP - hunting for db6 APs - reqLang %d AudType %d - vir count %d ", iRequiredLangTag, iAudioTypeTag, sslTrace.count());
	// go back down trace thru each member looking for a match
	for (int ii = sslTrace.count() - 1; ii >= 0; ii--) {
		if (ii >= 0) {
			CSourcePackages* pSrce = GetSourcePackage(sslTrace[ii].toInt());
			if (pSrce) {
				// rule 1 - search for audio package with matching tags
				if ((pSrce->m_iNumberAssignedAudioPackages > 0) && (iRequiredLangTag > 0)) {
					for (int iLvl = 1; iLvl < MAX_PKG_AUDIO; iLvl++) {
						if (pSrce->m_iAssoc_Audio_Packages[iLvl] > 0) {
							CAudioPackages* pAudio = GetAudioPackage(pSrce->m_iAssoc_Audio_Packages[iLvl]);
							if (pAudio) {
								//Debug("DetAudSP %d - rule 1  overall lang %d   ", pSrce->m_iAssoc_Audio_Packages[iLvl], pAudio->m_iOveralLanguageTag);
								if (pAudio->m_iOveralLanguageTag == iRequiredLangTag) {
									// now find Audio Source Pair that matches the Audio Type tag 
									for (int iiASP = 1; iiASP < MAX_AUDLVL_AUDIO; iiASP++) {
										if (pAudio->st_AudioSourcePair[iiASP].iAssocHub_or_TypeTag>0) {
											//Debug("DetAudSP %d - rule 1  overall lang %d   ", pSrce->m_iAssoc_Audio_Packages[iLvl], pAudio->m_iOveralLanguageTag);
											if (pAudio->st_AudioSourcePair[iiASP].iAssocHub_or_TypeTag == iAudioTypeTag) {
												*iWhichASP = iiASP;
												if (bShowAllDebugMessages) Debug("DetASPkg - rule 1  AudPkg %d  match - ASpiar %d   from src pkg %d ", pAudio->m_iAudioPkg_Indx, iiASP, pSrce->iPackageIndex);
												return pAudio;
											}
										}
									} // for iiasp
								}
							}
						}
					} // for iilvl
				}
				//Debug("DetAudSP - rule 1 - no matching audio package found for lang %d audio type tag %d ", iRequiredLangTag, iAudioTypeTag);
				//Debug("DetAudSP - rule 2 - checking default audio packages from db11");
				// rule 2 - try and find any associated default source audio AP tied to the package that matches audio requirements
				// was for (int iRVLevel = 1; iRVLevel<MAX_PKG_VIDEO;iRVLevel++) {
				if ((iRoutedVLevel > 0) && (iRoutedVLevel < MAX_PKG_VIDEO)) {
					if (pSrce->st_SourceAudioLevels[iRoutedVLevel].iAssocIndex > 0) {
						CAudioPackages* pAudio = GetAudioPackage(pSrce->st_SourceAudioLevels[iRoutedVLevel].iAssocIndex);
						if (pAudio) {
							// similar test / check for lang of AP and audio type tag
							//Debug("DetAudSP - rule 2  ap %d overall lang %d   ", pAudio->m_iAudioPkg_Indx, pAudio->m_iOveralLanguageTag);
							if (pAudio->m_iOveralLanguageTag == iRequiredLangTag) {
								for (int iiASP = 1; iiASP < MAX_AUDLVL_AUDIO; iiASP++) {
									if (pAudio->st_AudioSourcePair[iiASP].iAssocHub_or_TypeTag>0) {
										if (pAudio->st_AudioSourcePair[iiASP].iAssocHub_or_TypeTag == iAudioTypeTag) {
											*iWhichASP = iiASP;
											if (bShowAllDebugMessages) Debug("DetASPkg - rule 2  (routed video lvl %d )  - assoc AudPkg %d   ASpiar %d   from src pkg %d ", iRoutedVLevel, pAudio->m_iAudioPkg_Indx, iiASP, pSrce->iPackageIndex);
											return pAudio;
										}
									}
								} // for iiasp
							}
						}
					}
				}  // was for - now if 
			}
			// no match - so will route silence 0
		}
	} // for ii
	if (bShowAllDebugMessages) Debug("DetAudSP -ERR- no match found for reqLang %d AudType %d ruteLvl %d  ", iRequiredLangTag, iAudioTypeTag, iRoutedVLevel);
	return NULL;
}


void SendSimpleNevionRouteCommand(int iWhichRouter, int iDestIndx, int iReqSrc, int iNevPriority)
{
	CRevsRouterData* pRtr = GetRouterRecord(iWhichRouter);
	if (pRtr) {
		//
		if ((iReqSrc != pRtr->getPrimarySourceForDestination(iDestIndx)) && (iReqSrc != pRtr->getExpectedSourceForDestination(iDestIndx, TRUE))) {
			pRtr->setExpectedSourceForDestination(iDestIndx, iReqSrc, 8);
			char szCommand[MAX_AUTO_BUFFER_STRING] = "";
			int iCalcDest = ((iDestIndx - 1) % iIP_RouterInfoDivider) + 1;
			int iInfoOffset = (iDestIndx - 1) / iIP_RouterInfoDivider;
			int iInfoDrv = iWhichRouter + iInfoOffset;
			wsprintf(szCommand, "IW %d 'index=%d,pri=%d' %d", iInfoDrv, iReqSrc, iNevPriority, iCalcDest);
			AddCommandToQue(szCommand, INFODRVCOMMAND, iInfoDrv, 0, 0);
			ProcessNextCommand(2);
		}
		else
			Debug("SendSimpleNevionRoute - r:%d  d:%d s:%d - already routed or expected, so not reasserted", iWhichRouter, iDestIndx, iReqSrc );
	}
	else
		Debug("SendNevionVideo - error no Router record for %d", iWhichRouter);

}


void CreateSendNevionVideoAncCommands(int iRouteLevel, CDestinationPackages* pDstPkg, CSourcePackages* pSrcPkg, int iNevPriority, BOOL bReassert)
{
	// route appropriate video and up to 4 anc routes
	if ((pDstPkg&&pSrcPkg) && (iRouteLevel >= 0) && (iRouteLevel<MAX_PKG_VIDEO)) {
		// 
		char szCommand[MAX_AUTO_BUFFER_STRING] = "";
		// video- check UHD or just HD dest video tag 
		int iWhichRouter = iIP_RouterVideoHD;
		// which info - from dest
		pDstPkg->st_VideoHDestination.iMSPLevelAPIndx = iRouteLevel;
		pDstPkg->i_VisionFrom_Source = pSrcPkg->st_PkgVideoLevels[iRouteLevel].stVideoLevel.iAssocIndex;
		pDstPkg->st_VideoHDestination.iASPairIndx = 0;
		if (pDstPkg->st_VideoHDestination.iAssocIndex > 0) {
			// check to see if different to that already routed ?
			int iDest = pDstPkg->st_VideoHDestination.iAssocIndex;
			CRevsRouterData* pRtr = GetRouterRecord(iWhichRouter);
			if (pRtr) {
				if ( (pSrcPkg->st_PkgVideoLevels[iRouteLevel].stVideoLevel.iAssocIndex != pRtr->getPrimarySourceForDestination(iDest)) ||(bReassert) ) {
					if (pSrcPkg->st_PkgVideoLevels[iRouteLevel].stVideoLevel.iAssocIndex != pRtr->getExpectedSourceForDestination(iDest, TRUE)) {
						pRtr->setExpectedSourceForDestination(iDest, pSrcPkg->st_PkgVideoLevels[iRouteLevel].stVideoLevel.iAssocIndex, 8);
						int iCalcDest = ((iDest - 1) % iIP_RouterInfoDivider) + 1;
						int iInfoOffset = (iDest - 1) / iIP_RouterInfoDivider;
						int iInfoDrv = iWhichRouter + iInfoOffset;
						wsprintf(szCommand, "IW %d 'index=%d,pri=%d' %d", iInfoDrv, pSrcPkg->st_PkgVideoLevels[iRouteLevel].stVideoLevel.iAssocIndex, iNevPriority, iCalcDest);
						Debug("VideoRute dp %d dv %d cv %d", pDstPkg->iPackageIndex, iDest, iCalcDest);
						Debug("VideoRute dp %d sp %d cmd %s", pDstPkg->iPackageIndex, pSrcPkg->iPackageIndex, szCommand);
						AddCommandToQue(szCommand, INFODRVCOMMAND, iInfoDrv, 0, 0);
						//
						pDstPkg->i_VisionFrom_Pkg = pSrcPkg->iPackageIndex;
					}
					else
						Debug("SendNevionVideo - already expected so command not reasserted");
				}
				else
					Debug("SendNevionVideo - already routed so command not reasserted");
			}
			else
				Debug("SendNevionVideo - error no Router record for %d", iWhichRouter);
		}
		// anc 1-4
		for (int iAnc = 1; iAnc < MAX_PKG_VIDEO; iAnc++) {
			pDstPkg->st_VideoANC[iAnc].iMSPLevelAPIndx = iRouteLevel;
			pDstPkg->st_VideoANC[iAnc].iASPairIndx = 0;
			if (pDstPkg->st_VideoANC[iAnc].iAssocIndex > 0) {
				int iDest = pDstPkg->st_VideoANC[iAnc].iAssocIndex;
				CRevsRouterData* pRtr = GetRouterRecord(iIP_RouterANC);
				if (pRtr) {
					if ((pSrcPkg->st_PkgVideoLevels[iRouteLevel].stANCLevel[iAnc].iAssocIndex != pRtr->getPrimarySourceForDestination(iDest))||(bReassert)) {
						if (pSrcPkg->st_PkgVideoLevels[iRouteLevel].stVideoLevel.iAssocIndex != pRtr->getExpectedSourceForDestination(iDest, TRUE)) {
							pRtr->setExpectedSourceForDestination(iDest, pSrcPkg->st_PkgVideoLevels[iRouteLevel].stANCLevel[iAnc].iAssocIndex, 8);
							int iCalcDest = ((iDest - 1) % iIP_RouterInfoDivider) + 1;
							int iInfoOffset = (iDest - 1) / iIP_RouterInfoDivider;
							int iInfoDrv = iIP_RouterANC + iInfoOffset;
							wsprintf(szCommand, "IW %d 'index=%d,pri=%d' %d", iInfoDrv, pSrcPkg->st_PkgVideoLevels[iRouteLevel].stANCLevel[iAnc].iAssocIndex, iNevPriority, iCalcDest);
							Debug("VideoRute dp %d danc %d canc %d", pDstPkg->iPackageIndex, iDest, iCalcDest);
							Debug("VideoRute anc cmd %s", szCommand);
							AddCommandToQue(szCommand, INFODRVCOMMAND, iInfoDrv, 0, 0);
						}
						else
							Debug("SendNevionAnc - already expected so command not reasserted");
					}
					else
						Debug("SendNevionAnc - already routed so command not reasserted");
				}
			}
		}
		ProcessNextCommand(6);
	}
}


BOOL CreateSendNevionAudioCommands(int iWhichDAP, int iWhichSAP, CAudioPackages* pAudio, CDestinationPackages* pDstPkg, int iNevPriority, BOOL bReassert)
{
	// notes if iWhichAudio 1 - 32 and pAudio -- then route audio package to relevant dest audio pair
	//       if iWhichAudio == PARK Package == 8002 OR   pAudio == NUL     then parking dest audio 
	char szCommand[MAX_AUTO_BUFFER_STRING] = "";
	if (pDstPkg&&pAudio && (iWhichDAP > 0) && (iWhichDAP < MAX_PKG_AUDIO)) {
		int iPrevAPIndx = pDstPkg->st_AudioDestPair[iWhichDAP].iMSPLevelAPIndx;
		int iPrevASPairIndx = pDstPkg->st_AudioDestPair[iWhichDAP].iASPairIndx;
		pDstPkg->st_AudioDestPair[iWhichDAP].iMSPLevelAPIndx = pAudio->m_iAudioPkg_Indx;
		pDstPkg->st_AudioDestPair[iWhichDAP].iASPairIndx = iWhichSAP;
		// route required audio pair 
		int iSrce = 0;
		if ((iWhichSAP > 0) && (iWhichSAP < MAX_AUDLVL_AUDIO)) iSrce = pAudio->st_AudioSourcePair[iWhichSAP].iAssocIndex;
		int iDest = pDstPkg->st_AudioDestPair[iWhichDAP].iAssocIndex;
		if (iDest > 0) {
			CRevsRouterData* pRtr = GetRouterRecord(iIP_RouterAudio2CH);
			if (pRtr) {
				if ((iSrce != pRtr->getPrimarySourceForDestination(iDest)) || (iPrevASPairIndx != iWhichSAP) || (bReassert)) {
					if (iSrce != pRtr->getExpectedSourceForDestination(iDest, TRUE)) {
						pRtr->setExpectedSourceForDestination(iDest, iSrce, 8);
						int iCalcDest = ((iDest - 1) % iIP_RouterInfoDivider) + 1;
						int iInfoOffset = (iDest - 1) / iIP_RouterInfoDivider;
						int iInfoDrv = iIP_RouterAudio2CH + iInfoOffset;
						wsprintf(szCommand, "IW %d 'index=%d,pri=%d' %d", iInfoDrv, iSrce, iNevPriority, iCalcDest);
						AddCommandToQue(szCommand, INFODRVCOMMAND, iInfoDrv, 0, 0);
					}
					else
						Debug("SendNevionAudio - already expected so command not reasserted");
				}
				else
					Debug("SendNevionAudio note - already routed so command not reasserted");
				return  TRUE;
			}
		}

		// NOW check for previous AP and IFB in use etc - PULL ports
		// also Check for new AP and IFB in use - ADD into new IFB -- check for changes ???
		// desty pkg can only be routed to 1 traced source ( what of virtuals ??? )

	}
	return FALSE;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Find any nasty virtual desty package overrides for tags....thanks Nige.

void SeekforVideoHubTypeVirtualOverride(CDestinationPackages* pGivenDstPkg, int* iVidHubTypeTag)
{
	if (pGivenDstPkg) {
		// go down thru trace chain for any virtuals and look for virt dest vid hub type tags set - if so then use that one --- highly likely to f*@!.up routing
		bncs_stringlist sslSeek = bncs_stringlist(pGivenDstPkg->ss_Trace_To_Source, '|');
		// WAS for (int ii = 0; ii < sslSeek.count(); ii++) {
		Debug("SeekVideoHubOverride dp %d list %s", pGivenDstPkg->iPackageIndex, LPCSTR(sslSeek.toString('|')));
		for (int ii = sslSeek.count()-1; ii >=0; ii--) {
			if (ii >= 0) {
				int iTracPkg = sslSeek[ii].toInt();
				Debug("SeekVideoHubOverride seek val %d pkg  %d", ii, iTracPkg);
				if ((iTracPkg >= i_Start_VirtualPackages) && (iTracPkg < (iNumberOfPackages+1))) {
					CDestinationPackages* pDPVir = GetDestinationPackage(iTracPkg);
					if (pDPVir) {
						Debug("SeekVideoHubOverride seek val %d pkg  %d - Vidhub %d", ii, iTracPkg, pDPVir->st_VideoHDestination.iAssocHub_or_TypeTag);
						if ((pDPVir->st_VideoHDestination.iAssocHub_or_TypeTag > 0) && (pDPVir->st_VideoHDestination.iAssocHub_or_TypeTag != pGivenDstPkg->st_VideoHDestination.iAssocHub_or_TypeTag)) {
							Debug("SeekVideoHubTag for %d OVERRIDE by pkg %d from tag %d to %d", pGivenDstPkg->iPackageIndex, iTracPkg,
								pGivenDstPkg->st_VideoHDestination.iAssocHub_or_TypeTag, pDPVir->st_VideoHDestination.iAssocHub_or_TypeTag);
							*iVidHubTypeTag = pDPVir->st_VideoHDestination.iAssocHub_or_TypeTag;
						}
					}
				}
				else {
					return;  // got to a non virtual in trace
				}
			}
		}
	} // for ii
}


void SeekforAudioPkgTagsVirtualOverride(CDestinationPackages* pGivenDstPkg, int iAPIndx, int* iAudioLangUseTag, int* iAudioHubTypeTag, 
	int* iSubLang1, int* iSubHub1, int* iSubLang2, int* iSubHub2 )
{
	// go down thru trace chain for any virtuals and look for virt dest vid hub type tags set - if so then use that one --- highly likely to completly f*@!.up routing
	if ((pGivenDstPkg) && (iAPIndx > 0) && (iAPIndx < MAX_PKG_AUDIO)) {
		bncs_stringlist sslSeek = bncs_stringlist(pGivenDstPkg->ss_Trace_To_Source, '|');
		// WAS for (int ii = 0; ii < sslSeek.count(); ii++) {
		for (int ii = sslSeek.count() - 1; ii >= 0; ii--) {
			if (ii >= 0) {
				int iTracPkg = sslSeek[ii].toInt();
				if ((iTracPkg >= i_Start_VirtualPackages) && (iTracPkg < (iNumberOfPackages+1))) {
					CDestinationPackages* pDPVir = GetDestinationPackage(iTracPkg);
					if (pDPVir) {
						// lang-use tag --- only check the SAME Audio dest pair as orig given dest
						if ((pDPVir->st_AudioDestPair[iAPIndx].iAssocLang_or_UseTag > 0) && 
							(pDPVir->st_AudioDestPair[iAPIndx].iAssocLang_or_UseTag != pGivenDstPkg->st_AudioDestPair[iAPIndx].iAssocLang_or_UseTag)) {
							Debug("SeekAudioTags for %d LANG OVERRIDE by pkg %d from tag %d to %d", pGivenDstPkg->iPackageIndex, iTracPkg,
								pGivenDstPkg->st_AudioDestPair[iAPIndx].iAssocLang_or_UseTag, pDPVir->st_AudioDestPair[iAPIndx].iAssocLang_or_UseTag);
							*iAudioLangUseTag = pDPVir->st_AudioDestPair[iAPIndx].iAssocLang_or_UseTag;
						}
						// hubtype tag --- only check the SAME Audio dest pair as orig given dest
						if ((pDPVir->st_AudioDestPair[iAPIndx].iAssocHub_or_TypeTag > 0)&&
							(pDPVir->st_AudioDestPair[iAPIndx].iAssocHub_or_TypeTag != pGivenDstPkg->st_AudioDestPair[iAPIndx].iAssocHub_or_TypeTag)){
							Debug("SeekAudioTag for %d HUB OVERRIDE by pkg %d from tag %d to %d", pGivenDstPkg->iPackageIndex, iTracPkg,
								pGivenDstPkg->st_AudioDestPair[iAPIndx].iAssocHub_or_TypeTag, pDPVir->st_AudioDestPair[iAPIndx].iAssocHub_or_TypeTag);
							*iAudioHubTypeTag = pDPVir->st_AudioDestPair[iAPIndx].iAssocHub_or_TypeTag;
						}

						// as substitutes can be defined in virtuals to override those in final / given dest pkg ???
						// lang-use tag --- only check the SAME Audio dest pair as orig given dest
						if ((pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_1_Lang_or_UseTag > 0)&&
							(pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_1_Lang_or_UseTag != pGivenDstPkg->st_AudioDestSubstitutes[iAPIndx].iSub_1_Lang_or_UseTag)){
							Debug("SeekAudioTags for %d SUB1 LANG OVERRIDE by pkg %d from tag %d to %d", pGivenDstPkg->iPackageIndex, iTracPkg,
								pGivenDstPkg->st_AudioDestSubstitutes[iAPIndx].iSub_1_Lang_or_UseTag, pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_1_Lang_or_UseTag);
							*iSubLang1 = pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_1_Lang_or_UseTag;
						}
						// hubtype tag --- only check the SAME Audio dest pair as orig given dest
						if ((pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_1_Hub_or_TypeTag > 0) &&
							(pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_1_Hub_or_TypeTag != pGivenDstPkg->st_AudioDestSubstitutes[iAPIndx].iSub_1_Hub_or_TypeTag)) {
							Debug("SeekAudioTags for %d SUB1 HUB OVERRIDE by pkg %d from tag %d to %d", pGivenDstPkg->iPackageIndex, iTracPkg,
								pGivenDstPkg->st_AudioDestSubstitutes[iAPIndx].iSub_1_Hub_or_TypeTag, pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_1_Hub_or_TypeTag);
							*iSubHub1 = pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_1_Hub_or_TypeTag;
						}
						// lang-use tag --- only check the SAME Audio dest pair as orig given dest
						if ((pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_2_Lang_or_UseTag > 0) && 
							(pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_2_Lang_or_UseTag != pGivenDstPkg->st_AudioDestSubstitutes[iAPIndx].iSub_2_Lang_or_UseTag)){
							Debug("SeekAudioTags for %d SUB2 LANG OVERRIDE by pkg %d from tag %d to %d", pGivenDstPkg->iPackageIndex, iTracPkg,
								pGivenDstPkg->st_AudioDestSubstitutes[iAPIndx].iSub_2_Lang_or_UseTag, pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_2_Lang_or_UseTag);
							*iSubLang2 = pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_2_Lang_or_UseTag;
						}
						// hubtype tag --- only check the SAME Audio dest pair as orig given dest
						if ((pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_2_Hub_or_TypeTag > 0) &&
							(pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_2_Hub_or_TypeTag != pGivenDstPkg->st_AudioDestSubstitutes[iAPIndx].iSub_2_Hub_or_TypeTag)) {
							Debug("SeekAudioTags for %d SUB1 HUB OVERRIDE by pkg %d from tag %d to %d", pGivenDstPkg->iPackageIndex, iTracPkg,
								pGivenDstPkg->st_AudioDestSubstitutes[iAPIndx].iSub_2_Hub_or_TypeTag, pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_2_Hub_or_TypeTag);
							*iSubHub2 = pDPVir->st_AudioDestSubstitutes[iAPIndx].iSub_2_Hub_or_TypeTag;
						}
					}
				}
				else {
					return;  // got to a non virtual in trace
				}
			}
		} // for ii
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Main function to route VIDEO AND AUDIO for given dest package drawing upon previously built list of virtuals chain
//
void ProcessAllDestinationPackageRouting(CDestinationPackages* pGivenDstPkg, int iNevionPriority, BOOL bReassert )
{
	// uses the ssl_VirtualChain list of packages   
	if (pGivenDstPkg) {

		Debug("procAllDestPkgRou - dest %d", pGivenDstPkg->iPackageIndex);

		// get required park package for this dest package
		int iRouteLevel = 0;
		BOOL bVideoParked = FALSE;
		int iParkPackage = iPackagerParkSrcePkg;
		if (pGivenDstPkg->i_ParkSourcePackage > 0) iParkPackage = pGivenDstPkg->i_ParkSourcePackage;
		// VIDEO first

		// Go get / find the type tag and lang tag from given dest pkg or any virts .... NEW to do
		int iAssocHubOrTypeTag = pGivenDstPkg->st_VideoHDestination.iAssocHub_or_TypeTag;
		SeekforVideoHubTypeVirtualOverride(pGivenDstPkg, &iAssocHubOrTypeTag);
		//  is there matching tag between dest video and source 
		CSourcePackages* pUseThisSrcPkg = DetermineVideoSourceLevel(pGivenDstPkg->ss_Trace_To_Source, iAssocHubOrTypeTag, 
			pGivenDstPkg->i_CmdRoute_Level, pGivenDstPkg->i_CmdRoute_LangUse_Tag, &iRouteLevel);
		if ((pUseThisSrcPkg!=NULL)&&(iRouteLevel>0)) {
			// video anc routing -- is tag HD or UHD -- does this determine which video router part to command
			CreateSendNevionVideoAncCommands(iRouteLevel, pGivenDstPkg, pUseThisSrcPkg, iNevionPriority, bReassert);
			if (pUseThisSrcPkg->iPackageIndex == iParkPackage) bVideoParked = TRUE;
		}
		else {
			// use park package for default routing 
			CSourcePackages* pParkSrcPkg = GetSourcePackage(iParkPackage);
			if (pParkSrcPkg) {
				iRouteLevel = DetermineParkVideoSourceLevel(pGivenDstPkg->st_VideoHDestination.iAssocHub_or_TypeTag, pGivenDstPkg->i_CmdRoute_Level, pGivenDstPkg->i_CmdRoute_LangUse_Tag, iParkPackage);
				CreateSendNevionVideoAncCommands(iRouteLevel, pGivenDstPkg, pParkSrcPkg, iNevionPriority, bReassert);
				bVideoParked = TRUE;
			}
		}
		//
		// AUDIO ROUTING
		// audio routing -- go thru all dest pkg audio data 
		pGivenDstPkg->i_AudioFrom_Pkg = 0;
		pGivenDstPkg->i_AudioFrom_TagStatus = UNKNOWNVAL; // clear audio from and tag status
		//
		for (int iiAud = 1; iiAud < MAX_PKG_AUDIO; iiAud++) {
			// REMOVED FOR NIGEL if ((pGivenDstPkg->st_AudioDestPair[iiAud].iAssocIndex > 0) && ((pGivenDstPkg->st_AudioDestPair[iiAud].iAssocLang_or_UseTag > 0) || (pGivenDstPkg->i_CmdRoute_LangUse_Tag > 0))) {
				int iWhichAudioPkgPair = 0;
				int iAudioAssocLangUseTag = pGivenDstPkg->st_AudioDestPair[iiAud].iAssocLang_or_UseTag;
				int iAudioAssocHubTypeTag = pGivenDstPkg->st_AudioDestPair[iiAud].iAssocHub_or_TypeTag;
				int iAudioSub1LangUseTag = pGivenDstPkg->st_AudioDestSubstitutes[iiAud].iSub_1_Lang_or_UseTag;
				int iAudioSub1HubTypeTag = pGivenDstPkg->st_AudioDestSubstitutes[iiAud].iSub_1_Hub_or_TypeTag;
				int iAudioSub2LangUseTag = pGivenDstPkg->st_AudioDestSubstitutes[iiAud].iSub_2_Lang_or_UseTag;
				int iAudioSub2HubTypeTag = pGivenDstPkg->st_AudioDestSubstitutes[iiAud].iSub_2_Hub_or_TypeTag;
				BOOL iallTagsZero = FALSE;
				// are all tags zero ? - if so this iiaud not in use
				if ((iAudioAssocLangUseTag == 0) && (iAudioAssocHubTypeTag == 0) && (iAudioSub1LangUseTag == 0) &&
					(iAudioSub1HubTypeTag == 0) && (iAudioSub2LangUseTag == 0) && (iAudioSub2HubTypeTag == 0)) {
					iallTagsZero = TRUE;
				}
				// Go get / find the type tag and lang tag from given dest pkg or any virtual destination pkgs prior to primary source.... NEW 
				SeekforAudioPkgTagsVirtualOverride(pGivenDstPkg, iiAud, &iAudioAssocLangUseTag, &iAudioAssocHubTypeTag, 
					&iAudioSub1LangUseTag, &iAudioSub1HubTypeTag, &iAudioSub2LangUseTag, &iAudioSub2HubTypeTag);
				// seek main tags
				CAudioPackages* pFoundAudioPkg = NULL;
				pFoundAudioPkg = DetermineAudioSourcePackage(pGivenDstPkg->ss_Trace_To_Source, iAudioAssocLangUseTag, iAudioAssocHubTypeTag, iRouteLevel, &iWhichAudioPkgPair);
				if ((pFoundAudioPkg) && (pGivenDstPkg->i_AudioFrom_Pkg == 0)) {
					pGivenDstPkg->i_AudioFrom_Pkg = pFoundAudioPkg->m_iAudioPkg_Indx;
					pGivenDstPkg->i_AudioFrom_TagStatus = AUDIO_TAG_STD;
				}
				// if not found seek sub 1 tags
				if ((pFoundAudioPkg==NULL) && ((iAudioSub1LangUseTag>0) && (iAudioSub1HubTypeTag>0)) ) {
					pFoundAudioPkg = DetermineAudioSourcePackage(pGivenDstPkg->ss_Trace_To_Source, iAudioSub1LangUseTag, iAudioSub1HubTypeTag, iRouteLevel, &iWhichAudioPkgPair);
					if (pFoundAudioPkg) {
						pGivenDstPkg->i_AudioFrom_Pkg = pFoundAudioPkg->m_iAudioPkg_Indx;
						if (pGivenDstPkg->i_AudioFrom_TagStatus<AUDIO_TAG_SUB1) pGivenDstPkg->i_AudioFrom_TagStatus = AUDIO_TAG_SUB1;
					}
				}
				// if still not found seek sub 2 tags
				if ((pFoundAudioPkg==NULL) && ((iAudioSub2LangUseTag>0) && (iAudioSub2HubTypeTag>0)) ) {
					pFoundAudioPkg = DetermineAudioSourcePackage(pGivenDstPkg->ss_Trace_To_Source, iAudioSub2LangUseTag, iAudioSub2HubTypeTag, iRouteLevel, &iWhichAudioPkgPair);
					if (pFoundAudioPkg) { 
						pGivenDstPkg->i_AudioFrom_Pkg = pFoundAudioPkg->m_iAudioPkg_Indx;
						if (pGivenDstPkg->i_AudioFrom_TagStatus<AUDIO_TAG_SUB2) pGivenDstPkg->i_AudioFrom_TagStatus = AUDIO_TAG_SUB2;
					}
				}
				if (!CreateSendNevionAudioCommands(iiAud, iWhichAudioPkgPair, pFoundAudioPkg, pGivenDstPkg, iNevionPriority, bReassert)) {
					// route silence / 0 as no audio package matches lang use and audio tags
					CSourcePackages* pParkSrcPkg = GetSourcePackage(iParkPackage);
					if ((pParkSrcPkg) && (pGivenDstPkg->st_AudioDestPair[iiAud].iAssocIndex > 0)) {
						// check for iMSPLevelAPIndx assigned previously 
						pGivenDstPkg->st_AudioDestPair[iiAud].iASPairIndx = 0;
						pGivenDstPkg->st_AudioDestPair[iiAud].iMSPLevelAPIndx = 0;
						SendSimpleNevionRouteCommand(iIP_RouterAudio2CH, pGivenDstPkg->st_AudioDestPair[iiAud].iAssocIndex, pParkSrcPkg->st_SourceAudioLevels[1].iAssocIndex, iNevionPriority);
						if ((pGivenDstPkg->i_Traced_Src_Package != iPackagerParkSrcePkg) && (pGivenDstPkg->i_Traced_Src_Package != iParkPackage) && (!iallTagsZero)) {
							// not routing park parkage overall, and some of the tags assigned so error matching  parking this audio level
							if (pGivenDstPkg->i_AudioFrom_TagStatus < AUDIO_NOMATCH) pGivenDstPkg->i_AudioFrom_TagStatus = AUDIO_NOMATCH;
						}
					}
				}
			//}
		}
		ProcessNextCommand(40);

		// check for audio tag status
		if ((pGivenDstPkg->i_Traced_Src_Package == iPackagerParkSrcePkg) || (pGivenDstPkg->i_Traced_Src_Package == iPackagerBarsSrcePkg) || (pGivenDstPkg->i_Traced_Src_Package == iParkPackage)) {
			// dest pkg being parked or bars
			pGivenDstPkg->i_AudioFrom_TagStatus = AUDIO_TAG_STD;    // video parked so audio marked as ok
		}

		// add dest pkg to list to process shortly for sdi status
		if (ssl_DestPkgs_For_SDIStatus_Calc.find(pGivenDstPkg->iPackageIndex) < 0) {
			ssl_DestPkgs_For_SDIStatus_Calc.append(pGivenDstPkg->iPackageIndex);
			if (iSDIStatusCalcCounter == 0) iSDIStatusCalcCounter = 2;
		}

	}
	return;
}


//////////////////////////////////////////////////////////////////////////////////////

void ClearAudioPackageMMQueue( CAudioPackages* pAudPkg )  
{ 
	if (pAudPkg) {
		// for each of 4 ifbs in audio pkg
		if (pAudPkg->ssl_Pkg_Queue.count() > 0) {
			int iTopDestPkg = pAudPkg->ssl_Pkg_Queue[0].toInt();
			UpdateDestinationPackageForMMQ(iTopDestPkg, pAudPkg->m_iAudioPkg_Indx, REMOVEFROMLIST);
		}
		// clear queue
		pAudPkg->ssl_Pkg_Queue.clear();
		// reassert ifb on riedel without dest pkg mm ports  xxx all 4 ??
		for (int iifb = 1; iifb < MAX_PKG_CONFIFB; iifb++) {
			if (pAudPkg->stCFIFBs[iifb].i_DynAssignedIfb>0)
				CreateAndQueueFullIFBCommand(pAudPkg->stCFIFBs[iifb].i_DynAssignedIfb, FALSE);
			////keep conf on keys with ifb now - because ifb is for Audio Package until IFB ports are cleared // if (pAudPkg->stCFIFBs[iifb].i_DynAssignedConference > 0)
			//// 	ClearCFConferenceFromIFBKeys(pAudPkg->stCFIFBs[iifb].i_DynAssignedIfb, pAudPkg->stCFIFBs[iifb].i_DynAssignedConference );
		}
		// process rev vision if assigned 
		if ((pAudPkg->iEnabledRevVision>0) && (pAudPkg->st_Reverse_Vision.iAssocIndex>0))
			ProcessAudioPackageReverseRouting(pAudPkg->m_iAudioPkg_Indx, 0); // clear any routed rev vision/audio
		//
		// now update mmq slot
		SendPackagerBNCSRevertive(pAudPkg->m_iAudioPkg_Indx, AUTO_MM_STACK_INFO, FALSE, "");
	}
	else
		Debug("ClearAudioPkgMMQ - invalid class passed in");
}


/////////////////////////////////////////////////////////////////////////////
//
//
//


/////////////////////////////////////////////////////////////////////////////////////////////////////
//  process get / free commands for 4W conferences
//

int AcquireDynamicConferenceFromPool(int iPkgIndx, int iWhichConf, int iUseType)
{
	// LDC uses pool from 1 upwards, NHN uses pool from PoolEnd down the list - ie from either end of pool towards centre
	if (iAutomaticLocation == LDC_LOCALE_RING) {
		int iEndSearchPoint = iDynConferencePoolEnd;
		if ((iControlStatus < 10) || (iControlStatus == 12) || (iControlStatus == 13)) {
			// as other site not present or break in slink / cross site comms then only use lower half of shared resource
			iEndSearchPoint = (iDynConfPoolSize / 2)-1;
		}
		for (int iicc = iDynConferencePoolStart; iicc <= iEndSearchPoint; iicc++) {
			CRingMaster_CONF* pAConf = GetRingMaster_CONF_Record(iicc);
			if (pAConf) {
				if ((pAConf->GetDynamicPackage() == 0) && (pAConf->GetDynamicUseType() == 0)) {
					pAConf->SetDynamicAudioPackage(iPkgIndx, iWhichConf, iUseType);
					return iicc;
				}
			}
		}
	}
	else if (iAutomaticLocation == NHN_LOCALE_RING) {
		int iEndSearchPoint = iDynConferencePoolStart;
		if ((iControlStatus < 10) || (iControlStatus == 12) || (iControlStatus == 13)) {
			// as other site not present as txrx or break in slink / cross site comms then only use top half of shared resource
			iEndSearchPoint = iDynConferencePoolStart + (iDynConfPoolSize / 2);
		}
		for (int iicc = iDynConferencePoolEnd; iicc >= iEndSearchPoint; iicc--) {
			CRingMaster_CONF* pAConf = GetRingMaster_CONF_Record(iicc);
			if (pAConf) {
				if ((pAConf->GetDynamicPackage() == 0) && (pAConf->GetDynamicUseType() == 0)) {
					pAConf->SetDynamicAudioPackage(iPkgIndx, iWhichConf, iUseType);
					return iicc;
				}
			}
		}
	}
	Debug("AcquireDynConference -WARNING- NO FREE DYNAMIC CONFS IN POOL");
	return 0;
}

void Calculate4WConferencePoolUse()
{
	iDynConfPoolInUse = 0;
	for (int iicc = iDynConferencePoolStart; iicc <= iDynConferencePoolEnd; iicc++) {
		CRingMaster_CONF* pAConf = GetRingMaster_CONF_Record(iicc);
		if (pAConf) {
			if ((pAConf->GetDynamicPackage() >0) && (pAConf->GetDynamicUseType()> 0)) {
				iDynConfPoolInUse++;
			}
		}
	}
	char szBuf[256] = "";
	wsprintf(szBuf, "%d|%d", iDynConfPoolInUse, iDynConfPoolSize);
	SetDlgItemText(hWndDlg, IDC_CONFPOOL, szBuf);
	eiInfoDrivers[1].updateslot(DYN_CONF_USE_SLOT, szBuf);
	// counters updated on next main timer loop
}



int AcquireDynIFBFromPool(int iPkgIndx, int iWhichCFIFB, int iTrunkAddress)
{
	// assign from required ring -- 4 rings at present 2 in london, 2 in hilversum
	CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunkAddress);
	if (pRing) {
		int iWhichRing = pRing->m_iRecordIndex;
		if ((iWhichRing > 0) && (iWhichRing <= iNumberRiedelRings)) {
			//
			if (iAutomaticLocation == LDC_LOCALE_RING) {
				int iFBRecStart = ((iWhichRing - 1)*MAX_IFBS) + iDynIFBPoolStart;
				int iFBRecEnd = iFBRecStart + iDynIFBPoolSize - 1;
				if ((iControlStatus < 10) || (iControlStatus == 12) || (iControlStatus == 13)) iFBRecEnd = iFBRecStart + (iDynIFBPoolSize / 2) - 1;   // as other site not present or break in cross site comms then only use lower half of shared ifb resource
				for (int iicc = iFBRecStart; iicc <= iFBRecEnd; iicc++) {
					CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iicc);
					if (pIFB) {
						if ((pIFB->getAssociatedAudioPackage() == 0) && (pIFB->getAssociatedWhichDynIFB() == 0)) {
							pIFB->setAssociatedAudioPackage(iPkgIndx, iWhichCFIFB);
							return iicc;
						}
					}
				} // for iicc
			}
			else if (iAutomaticLocation == NHN_LOCALE_RING) {
				int iFBRecStart = ((iWhichRing - 1)*MAX_IFBS) + iDynIFBPoolEnd;
				int iFBRecEnd = ((iWhichRing - 1)*MAX_IFBS) + iDynIFBPoolStart;
				if ((iControlStatus < 10) || (iControlStatus == 12) || (iControlStatus == 13)) iFBRecEnd = iFBRecEnd  + (iDynIFBPoolSize / 2);   // as other site not present or break in cross site comms then only use upper half of shared ifb resource
				for (int iicc = iFBRecStart; iicc >= iFBRecEnd; iicc--) {
					CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iicc);
					if (pIFB) {
						if ((pIFB->getAssociatedAudioPackage() == 0) && (pIFB->getAssociatedWhichDynIFB() == 0)) {
							pIFB->setAssociatedAudioPackage(iPkgIndx, iWhichCFIFB);
							return iicc;
						}
					}
				} // for iicc
			} // else
		}
		Debug("AcquireDynIFBFromPool -ERROR- NO FREE DYN IFBS IN POOL for RING %d", iWhichRing);
	}
	else
		Debug("AcquireDynIFBFromPool -ERROR- RING from trunk address %d", iTrunkAddress);

	// fail to acquire
	return 0;
}


void CalculateDynIFBPoolUse()
{
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		if ((iRing > 0) && (iRing <5)) {
			iDynIFBPoolInUse[iRing] = 0;
			for (int iicc = iDynIFBPoolStart; iicc <= iDynIFBPoolEnd; iicc++) {
				int iIFBRecIndx = ((iRing - 1)*MAX_IFBS) + iicc;
				CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iIFBRecIndx);
				if (pIFB) {
					if ((pIFB->getAssociatedAudioPackage()>0) && (pIFB->getAssociatedWhichDynIFB()>0)) {
						iDynIFBPoolInUse[iRing]++;
					}
				}
			}
			char szBuf[256] = "";
			wsprintf(szBuf, "%d|%d", iDynIFBPoolInUse[iRing], iDynIFBPoolSize);
			switch (iRing) {
			case 1: // set dev 1001 slot 4011 directly
				SetDlgItemText(hWndDlg, IDC_IFBPOOL1, szBuf);
				eiInfoDrivers[1].updateslot(DYN_IFB1_USE_SLOT, szBuf);
				break;
			case 2: // set dev 1001 slot 4012 directly
				SetDlgItemText(hWndDlg, IDC_IFBPOOL2, szBuf);
				eiInfoDrivers[1].updateslot(DYN_IFB2_USE_SLOT, szBuf);
				break;
			case 3: // set dev 1001 slot 4013 directly
				SetDlgItemText(hWndDlg, IDC_IFBPOOL3, szBuf);
				eiInfoDrivers[1].updateslot(DYN_IFB3_USE_SLOT, szBuf);
				break;
			case 4: // set dev 1001 slot 4014 directly
				SetDlgItemText(hWndDlg, IDC_IFBPOOL4, szBuf);
				eiInfoDrivers[1].updateslot(DYN_IFB4_USE_SLOT, szBuf);
				break;
			}
		}
	}
}

void SendIFBRMSubtitleLabel(bncs_string ssLabel, int iIFBRecordIndx)
{
	// get ifb rec; get trunk addr; get ring; get ifb device;
	// calc ifb 1-1500 range; send RM string
	CRingMaster_IFB* pRMIfb = GetRingMaster_IFB_Record(iIFBRecordIndx);
	if (pRMIfb) {
		CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pRMIfb->getAssignedTrunkAddr());
		if (pRing) {
			int iIFBDevice = pRing->iRing_Device_IFB;
			int iRealIFBIndx = ((iIFBRecordIndx - 1) % MAX_IFBS) + 1;
			AddCommandToQue(LPCSTR(ssLabel), DATABASECOMMAND, iIFBDevice, iRealIFBIndx, DB_RIEDEL_SUBTITLE);
		}
	}
}

void SendConferenceRMSubtitleLabel(bncs_string ssLabel, int iConfRecordIndx)
{
	// local ringmaster
	AddCommandToQue(LPCSTR(ssLabel), DATABASECOMMAND, i_RingMaster_Conf, iConfRecordIndx, DB_RIEDEL_SUBTITLE);
	// remote ringmaster
	AddCommandToQue(LPCSTR(ssLabel), DATABASECOMMAND, iRemote_RingMaster_Conf, iConfRecordIndx, DB_RIEDEL_SUBTITLE);
}


void ProcessDynamicIFBCommand(CAudioPackages* pAudio, bncs_string ssCmd)
{
	bncs_stringlist ssl_Cmd = bncs_stringlist(ssCmd.upper(), '=');
	if ((pAudio) && (ssl_Cmd.count()>1)) {
		bncs_string ss_data = bncs_stringlist(ssl_Cmd[1], ',');
		int iUseType = 0;
		int iWhichIFB = 0;
		// parse command 
		if (ssl_Cmd[0].find("GET") >= 0) {
			if (ss_data.find("CF_IFB") >= 0) {
				ss_data.replace("CF_IFB", "");
				iWhichIFB = ss_data.toInt();
				iUseType = DYN_IFB_4WCONF_1 + iWhichIFB - 1;
			}
			if ((iWhichIFB > 0) && (iWhichIFB < MAX_PKG_CONFIFB)) {
				// acquire confs for 4W in list for package - make sure not assigned already and that ports are assigned
				if ((iUseType >= DYN_IFB_4WCONF_1) && (iUseType <= DYN_IFB_4WCONF_4)) {
					if ((pAudio->stCFIFBs[iWhichIFB].i_out_port>0) && (pAudio->stCFIFBs[iWhichIFB].i_out_ring > 0)) {
						if (pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb == 0) {
							int iNewIFBRecIndx = AcquireDynIFBFromPool(pAudio->m_iAudioPkg_Indx, iUseType, pAudio->stCFIFBs[iWhichIFB].i_out_ring);
							// new ifb index is INTERNAL ifb record index !!!
							if (iNewIFBRecIndx > 0) {
								pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb = iNewIFBRecIndx;
								// add in audio package ports etc 
								CreateAndQueueFullIFBCommand(iNewIFBRecIndx, TRUE);
								// RM conf changes
								bncs_string ssData = bncs_string("audpkg_index=%1,which_ifb=%2,use_type=%3").arg(pAudio->m_iAudioPkg_Indx).arg(iWhichIFB).arg(iUseType);
								AddCommandToQue(LPCSTR(ssData), DATABASECOMMAND, i_rtr_Infodrivers[1], iNewIFBRecIndx, DB_SYNC_DYN_IFBS);
								//RM for db12 subtitle for real IFB index on ring 
								SendIFBRMSubtitleLabel(pAudio->stCFIFBs[iWhichIFB].m_ss_SubTitleLabel, iNewIFBRecIndx);
							}
							else
								Debug("ProcessDynIFB - GET ERROR- NO FREE IFBS IN POOL for RING %d ", pAudio->stCFIFBs[iWhichIFB].i_out_ring);
						}
						else
							Debug("ProcessDynIFB - GET CFCONF -ALREADY ASSIGNED for AP %d conf %d", pAudio->m_iAudioPkg_Indx, iWhichIFB);
					}
					else
						Debug("ProcessDynIFB - GET -no Riedel ports assigned in package %d ", pAudio->m_iAudioPkg_Indx);
				}
			}
			else
				Debug("ProcessDynIFB - GET IFB -invalid data for AP %d cmd %s", pAudio->m_iAudioPkg_Indx, LPCSTR(ssCmd));
		}
		else if (ssl_Cmd[0].find("FREE") >= 0) {
			if (ss_data.find("CF_IFB") >= 0) {
				ss_data.replace("CF_IFB", "");
				iWhichIFB = ss_data.toInt();
				iUseType = DYN_IFB_4WCONF_1 + iWhichIFB - 1;
			}
			if ((iWhichIFB > 0) && (iWhichIFB < MAX_PKG_CONFIFB)) {
				// free confs in data list from use for package
				if ((iUseType >= DYN_IFB_4WCONF_1) && (iUseType <= DYN_IFB_4WCONF_4)) {
					if (pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb > 0) {
						int iIFBRec = pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb;
						CRingMaster_IFB * pIFB = GetRingMaster_IFB_Record(iIFBRec);
						if (pIFB) pIFB->setAssociatedAudioPackage(0, 0);
						pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb = 0;
						// clear ifb down
						CreateAndQueueFullIFBCommand(iIFBRec, FALSE);
						// remove IFB from any known keys
						ClearAllFunctionsOnKnownIFBKeys(iIFBRec);
						// RM conf changes
						bncs_string ssData = bncs_string("audpkg_index=0,which_ifb=0,use_type=0");
						AddCommandToQue(LPCSTR(ssData), DATABASECOMMAND, i_rtr_Infodrivers[1], iIFBRec, DB_SYNC_DYN_IFBS);
						//RM for db12 to CLEAR subtitle for real IFB index on ring 
						SendIFBRMSubtitleLabel(bncs_string(" "), iIFBRec);
						// FREE any assoc CF conf too now
						if (pAudio->stCFIFBs[iWhichIFB].i_DynAssignedConference > 0) {
							bncs_string sscmd = bncs_string("FREE=CF%1_CONF").arg(iWhichIFB);
							ProcessDynamicConferenceCommand(pAudio->m_iAudioPkg_Indx, sscmd);
						}
					}
					else
						Debug("ProcessDynIFB - FREE IFB -NOT ASSIGNED for AP %d ifb %d", pAudio->m_iAudioPkg_Indx, iWhichIFB);
				}
			}
			else
				Debug("ProcessDynIFB - FREE IFB -invalid data for AP %d cmd %s", pAudio->m_iAudioPkg_Indx, LPCSTR(ssCmd));
		}
		else
			Debug("ProcessDynIFB invalid command");

		ProcessNextCommand(10);

		//update infodriver slot now with all status data inc current conf in use
		UpdateAudioPackageStatusInfodriver(pAudio->m_iAudioPkg_Indx);
		//
		CalculateDynIFBPoolUse();
		// if current package update gui
		if (iChosenAudioSrcPkg == pAudio->m_iAudioPkg_Indx) DisplayAudioSourcePackageData();

	}
	else
		Debug("ProcessDynIFB invalid class passed");
}


void ProcessAudioPackageIFBS(int iWhichIFB, CAudioPackages* pAudio, int iPreInRing, int iPreInPort, int iPreOutRing, int iPreOutPort, BOOL bLabelChanged)
{
	if (pAudio) {
		char szNewOutputs[MAX_AUTO_BUFFER_STRING]="";
		char szCommand[MAX_AUTO_BUFFER_STRING]="";
		char szReply[64]="";			
		int iDestIfb=0, iDestIfbPort=0;

		if ((iWhichIFB > 0) && (iWhichIFB < MAX_PKG_CONFIFB)) {
			// is there a dyn ifb already assigned ?
			if (pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb > 0) {
				// check to see if out port is 0 - so release ifb back into pool
				if ((pAudio->stCFIFBs[iWhichIFB].i_out_ring == 0)||(pAudio->stCFIFBs[iWhichIFB].i_out_port == 0)) {
					// clear - both ifb and conf any keys being used for this ifb 
					if (pAudio->stCFIFBs[iWhichIFB].i_DynAssignedConference > 0) {
						ClearCFConferenceFromIFBKeys(pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb, pAudio->stCFIFBs[iWhichIFB].i_DynAssignedConference);
					}
					// clear down assigned ifb and any associated / assigned cf conf
					bncs_string ssCmd = bncs_string("FREE=CF_IFB%1").arg(iWhichIFB);
					ProcessDynamicIFBCommand(pAudio, ssCmd);
				}
				else {
					// have any elements of the ports changed 
					if ((iPreInRing != pAudio->stCFIFBs[iWhichIFB].i_in_ring) || (iPreInPort != pAudio->stCFIFBs[iWhichIFB].i_in_port) ||
						(iPreOutRing != pAudio->stCFIFBs[iWhichIFB].i_out_ring) || (iPreOutPort != pAudio->stCFIFBs[iWhichIFB].i_out_port) ) {
						// xxx if out ring has changed -- this is a serious issue as it affects the ifb for the ring.... how to resolve that one ???
						CreateAndQueueFullIFBCommand(pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb, TRUE);
					}
					else if (bLabelChanged) {
						// all ports same just label change - update IFB Label only 
						ReAssignLabelForIFBS(pAudio, iWhichIFB);
					}
				}
				ProcessNextCommand(10);
			}
			else {
				// as no dyn ifb -- are out port assigned - if so acquire dyn ifb
				if ((pAudio->stCFIFBs[iWhichIFB].i_out_ring > 0) && (pAudio->stCFIFBs[iWhichIFB].i_out_port > 0) && (pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb == 0)) {
					bncs_string ssCmd = bncs_string("GET=CF_IFB%1").arg(iWhichIFB);
					ProcessDynamicIFBCommand(pAudio, ssCmd);
					// if desty pkg already top of MM Q - this is added into command in ProcessDynamicIFBCommand function
				}
			}
			ProcessNextCommand(10);
		}
		else
			Debug("ProcessAudioPackageIFBs invalid whichifb");
	}
	else
		Debug( "ProcessAudioPackageIFBs - invalid audio pkg record " );
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void UpdateRiedelPortUseInfodriver(int iPortIndex)
{
	CRiedelPortPackageUse* pPort = Get_RiedelPortPackageUseRecord(iPortIndex);
	if (pPort) {
		// get all strings for this port and build infodriver rev
		bncs_stringlist sslist = bncs_stringlist("", '*');   // star delimited per ring
		for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(iRing);
			if (pRing) {
				int iTrunk = pRing->m_iTrunkAddress;
				int iRingHub = pRing->m_iRingUseType;
				// get entry and create string for ring
				bncs_string ss_TB_Data = pPort->GetDetailsByRingAddr(iTrunk, PORT_USECODE_4WTB);
				bncs_string ss_CF_Data = pPort->GetDetailsByRingAddr(iTrunk, PORT_USECODE_CFIFB);
				bncs_string ss_MM_Data = pPort->GetDetailsByRingAddr(iTrunk, PORT_USECODE_MMIFB);
				bncs_string sstr = bncs_string("%1:4W=%2,CF=%3,MM=%4").arg(iTrunk).arg(ss_TB_Data).arg(ss_CF_Data).arg(ss_MM_Data);
				sslist.append(sstr);
				//if ((bShowAllDebugMessages) &&(ssl_Entries.count()>0)) Debug("zzz UpdateRPortUse port %d trunk %d str %s", iPortIndex, iTrunk, LPCSTR(ssl_Entries.toString('|')));
			}
		} // for int iRing
		bncs_string ssfinal = sslist.toString('*');
		if (ssfinal.length()>255) ssfinal.truncate(254);
		SendPackagerBNCSRevertive(iPortIndex, AUTO_RIEDEL_PORT_USE_INFO, FALSE, LPCSTR(ssfinal));
	}
	else
		Debug("UpdateRiedelPortUseInfodriver - ERROR cannot find record for %d ", iPortIndex);
}


// tidy up Riedel port pti
void TidyRiedelPortUsage(int iTrunkAddr, int iPortIndex, int iUseCode, int iPkgInQuestion)
{
	CRiedelPortPackageUse* pPort = Get_RiedelPortPackageUseRecord(iPortIndex);
	if (pPort) {
		int iIndex = pPort->GetIndexPositionForPackageByRingAddr(iTrunkAddr, iUseCode, bncs_string(iPkgInQuestion));
		if (iIndex >= 0) {
			pPort->ClearDetailsPerRingPerEntry(iTrunkAddr, iUseCode, iIndex);
			UpdateRiedelPortUseInfodriver(iPortIndex);
		}
	}
	else
		Debug("TidyRiedelPortUsage - ERROR cannot find record for %d ", iPortIndex);
}

////////////////////////////////////////////////////

void CalculateRiedelPortUse(int iPackage, BOOL bUpdateInfodriver)
{
	// get srce package add pkg to ports currently assigned  -- currently just adds package -- could add hub#package if required
	CAudioPackages* pAudio = GetAudioPackage(iPackage);
	if (pAudio) {
		bncs_stringlist sslistPorts = bncs_stringlist("", ',');
		for (int iitt = 1; iitt<MAX_PKG_CONFIFB; iitt++) {
			// tb
			if ((pAudio->stTBConferences[iitt].i_in_ring > 0) && (pAudio->stTBConferences[iitt].i_in_port > 0)) {
				CRiedelPortPackageUse* pPorta = Get_RiedelPortPackageUseRecord(pAudio->stTBConferences[iitt].i_in_port);
				if (pPorta) {
					if (pPorta->AddDetailsByRingAddr(pAudio->stTBConferences[iitt].i_in_ring, PORT_USECODE_4WTB, bncs_string(iPackage))) {
						if (sslistPorts.find(bncs_string(pAudio->stTBConferences[iitt].i_in_port))<0) sslistPorts.append(bncs_string(pAudio->stTBConferences[iitt].i_in_port));
					}
				}
			}
			if ((pAudio->stTBConferences[iitt].i_out_ring > 0) && (pAudio->stTBConferences[iitt].i_out_port > 0)) {
				CRiedelPortPackageUse* pPortb = Get_RiedelPortPackageUseRecord(pAudio->stTBConferences[iitt].i_out_port);
				if (pPortb) {
					if (pPortb->AddDetailsByRingAddr(pAudio->stTBConferences[iitt].i_out_ring, PORT_USECODE_4WTB, bncs_string(iPackage))) {
						if (sslistPorts.find(bncs_string(pAudio->stTBConferences[iitt].i_out_port))<0) sslistPorts.append(bncs_string(pAudio->stTBConferences[iitt].i_out_port));
					}
				}
			}
			// cf
			if ((pAudio->stCFIFBs[iitt].i_in_ring > 0) && (pAudio->stCFIFBs[iitt].i_in_port > 0)) {
				CRiedelPortPackageUse* pPortc = Get_RiedelPortPackageUseRecord(pAudio->stCFIFBs[iitt].i_in_port);
				if (pPortc) {
					if (pPortc->AddDetailsByRingAddr(pAudio->stCFIFBs[iitt].i_in_ring, PORT_USECODE_CFIFB, bncs_string(iPackage))) {
						if (sslistPorts.find(bncs_string(pAudio->stCFIFBs[iitt].i_in_port))<0) sslistPorts.append(bncs_string(pAudio->stCFIFBs[iitt].i_in_port));
					}
				}
			}
			if ((pAudio->stCFIFBs[iitt].i_out_ring > 0) && (pAudio->stCFIFBs[iitt].i_out_port > 0)) {
				CRiedelPortPackageUse* pPortd = Get_RiedelPortPackageUseRecord(pAudio->stCFIFBs[iitt].i_out_port);
				if (pPortd) {
					if (pPortd->AddDetailsByRingAddr(pAudio->stCFIFBs[iitt].i_out_ring, PORT_USECODE_CFIFB, bncs_string(iPackage))) {
						if (sslistPorts.find(bncs_string(pAudio->stCFIFBs[iitt].i_out_port))<0) sslistPorts.append(bncs_string(pAudio->stCFIFBs[iitt].i_out_port));
					}
				}
			}
		} // for iitt
		if (bUpdateInfodriver) {
			for (int ii = 0; ii < sslistPorts.count(); ii++)
				UpdateRiedelPortUseInfodriver(sslistPorts[ii].toInt());
		}
	}
}

void CalculateDestinationMMRiedelPortUse(int iDestPackage, BOOL bUpdateInfodriver)
{
	// get dest package add pkg to ports currently assigned  -- currently just adds package -- could add hub#package if required
	CDestinationPackages* pDest = GetDestinationPackage(iDestPackage);
	if (pDest) {
		bncs_stringlist sslistPorts = bncs_stringlist("", ',');
		for (int iitt = 1; iitt<3; iitt++) {
			// mm
			if ((pDest->st_AssocIFBs[iitt].i_mm_ring > 0) && (pDest->st_AssocIFBs[iitt].i_mm_port > 0)) {
				CRiedelPortPackageUse* pPorta = Get_RiedelPortPackageUseRecord(pDest->st_AssocIFBs[iitt].i_mm_port);
				if (pPorta) {
					if (pPorta->AddDetailsByRingAddr(pDest->st_AssocIFBs[iitt].i_mm_ring, PORT_USECODE_MMIFB, bncs_string(iDestPackage))) {
						if (sslistPorts.find(bncs_string(pDest->st_AssocIFBs[iitt].i_mm_port))<0) sslistPorts.append(bncs_string(pDest->st_AssocIFBs[iitt].i_mm_port));
					}
				}
			}
		} // for iitt
		if (bUpdateInfodriver) {
			for (int ii = 0; ii < sslistPorts.count(); ii++)
				UpdateRiedelPortUseInfodriver(sslistPorts[ii].toInt());
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
//
//   AUDIO PACKAGE REVERSE VISION
//
// was for routing 

void ProcessAudioPackageReverseRouting(int iAudioPkg, int iRoutingAction)
{
	// check AP MMQ for dest pkg top - check dest pkg for rev vis / aud
	CAudioPackages* pAPkg = GetAudioPackage(iAudioPkg);
	if (pAPkg) {
		//
		if (iRoutingAction > 0)  {
			if ((pAPkg->iEnabledRevVision > 0) && (pAPkg->st_Reverse_Vision.iAssocIndex > 0) && (pAPkg->ssl_Pkg_Queue.count() > 0)) {
				CDestinationPackages* pAPRVDest = GetDestinationPackage(pAPkg->st_Reverse_Vision.iAssocIndex);
				CDestinationPackages* pMMDest = GetDestinationPackage(pAPkg->ssl_Pkg_Queue[0].toInt());
				if (pMMDest&&pAPRVDest) {
					// make reverse vision package route rather than simple NVision vision/audio routes
					int iPkgToRoute = pMMDest->st_Reverse_Vision.iAssocIndex;
					if (iPkgToRoute < 1) pAPRVDest->i_ParkSourcePackage; // use any defined park pkg in dest pkg AP RV if no rev vis in MM dest pkg
					bncs_string ssCmdStr = bncs_string("index=%1").arg(iPkgToRoute);
					ProcessDestinationPkgCommand(pAPRVDest, ssCmdStr);
					pAPkg->iRoutedRevVisionPkg = iPkgToRoute;
				}
				else
					Debug("ProcessAudioPackageReverseRouting - no APRV Dest pkg (%d) or MM Dest pkg (%d)", pAPkg->st_Reverse_Vision.iAssocIndex, pAPkg->ssl_Pkg_Queue[0].toInt());
			}
		}
		else	 if (iRoutingAction < 1) {
			CDestinationPackages* pAPRVDest = GetDestinationPackage(pAPkg->st_Reverse_Vision.iAssocIndex);
			if (pAPRVDest) {
				// route park to reverse vision as a package 
				bncs_string ssCmdStr = bncs_string("index=%1").arg(pAPRVDest->i_ParkSourcePackage);
				ProcessDestinationPkgCommand(pAPRVDest, ssCmdStr);
				pAPkg->iRoutedRevVisionPkg = 0;
			}
			else
				Debug("ProcessAudioPackageReverseRouting - no APRV Dest pkg (%d) ", pAPkg->st_Reverse_Vision.iAssocIndex );
		}
		// build response out to slot
		UpdateAudioPackageStatusInfodriver(iAudioPkg);
	}
	else
		Debug("ProcAudioRevVis - invalid audio pkg index passed %d ", iAudioPkg);

}


void ProcessAudioPackage4WTB(int iWhichTB, CAudioPackages* pAudio, int iPreRingIn, int i_Pre_Src_in, int iPreRingOut, int i_Pre_Src_out, BOOL bLabelChanged)
{
	char szCommand[MAX_AUTO_BUFFER_STRING] = "";
	int iPkgTBConf = 0;
	int iNewTalkRing = 0, iNewListenRing = 0;
	int iNewTalkPort = 0, iNewListenPort = 0;
	bncs_string ssData = "";
	bncs_string ssMainLabel = "", ssSubTitLbl="";

	if (pAudio) {

		// TB conferences
		if ((iWhichTB >0) && (iWhichTB<MAX_PKG_CONFIFB)) {
			iPkgTBConf = pAudio->stTBConferences[iWhichTB].i_DynAssignedConference;
			iNewTalkRing = pAudio->stTBConferences[iWhichTB].i_in_ring;
			iNewTalkPort = pAudio->stTBConferences[iWhichTB].i_in_port;
			iNewListenRing = pAudio->stTBConferences[iWhichTB].i_out_ring;
			iNewListenPort = pAudio->stTBConferences[iWhichTB].i_out_port;
			// labels
			if (pAudio->stTBConferences[iWhichTB].m_ss_MainLabel.length()>0) {
				if (pAudio->stTBConferences[iWhichTB].m_ss_MainLabel.length() <= 8)
					ssMainLabel = pAudio->stTBConferences[iWhichTB].m_ss_MainLabel;
				else
					ssMainLabel = pAudio->stTBConferences[iWhichTB].m_ss_MainLabel.left(8);
			}
			else {
				if (pAudio->m_ssButtonName.length() <= 8)
					ssMainLabel = pAudio->m_ssButtonName;
				else
					ssMainLabel = pAudio->m_ssButtonName.left(8);
			}
			// sub title
			if (pAudio->stTBConferences[iWhichTB].m_ss_SubTitleLabel.length()>0) {
				if (pAudio->stTBConferences[iWhichTB].m_ss_SubTitleLabel.length() <= 16)
					ssSubTitLbl = pAudio->stTBConferences[iWhichTB].m_ss_SubTitleLabel;
				else
					ssSubTitLbl = pAudio->stTBConferences[iWhichTB].m_ss_SubTitleLabel.left(16);
			}

		}
		else {
			Debug("ProcessAudioPackage4WTB -error- invalid TB index %d ", iWhichTB);
			return; // invalid index given
		}

		if (bShowAllDebugMessages)
			Debug("ProcessSrcPkgTB - pkg %d TB %d conf %d - pre %d %d - %d %d  new %d %d - %d %d ", pAudio->m_iAudioPkg_Indx, iWhichTB, iPkgTBConf,
			iPreRingIn, i_Pre_Src_in, iPreRingOut, i_Pre_Src_out, iNewTalkRing, iNewTalkPort, iNewListenRing, iNewListenPort);

		CRingMaster_CONF* pTBConf = GetRingMaster_CONF_Record(iPkgTBConf);
		if (pTBConf) {
			// are all cleared now
			if ((iNewTalkPort == 0) && (iNewListenPort == 0)) {
				if ((i_Pre_Src_in>0) || (i_Pre_Src_out>0)) { // ie a change occurred
					// clear conf and keys assoc to it  ??
					ClearAllConferenceKeyMembers(pTBConf);
					ClearConferencePortMembers(iPkgTBConf);
					// WAS xxx ??? free dynamic conf back into pool
					// bncs_string sstr = bncs_string("FREE=CONF%1").arg(iWhichTB);
					// ProcessDynamicConferenceCommand(pAudio->m_iAudioPkg_Indx, sstr);
				}
				// if just a label change though - and no ports assigned ??
				if (bLabelChanged) {
					// get any override label
					bncs_string ssOverride = pTBConf->GetOverrideConferenceLabel();
					if ((ssOverride.length() > 1) && (ssOverride.toInt() != iPkgTBConf)) {
						wsprintf(szCommand, "IW %d '%s' %d", i_RingMaster_Conf, LPCSTR(ssOverride), iPkgTBConf + 1000);
					}
					else {
						// send main label first to conf
						wsprintf(szCommand, "IW %d '%s' %d", i_RingMaster_Conf, LPCSTR(ssMainLabel), iPkgTBConf + 1000);
					}
					AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);
					SendConferenceRMSubtitleLabel(ssSubTitLbl, iPkgTBConf);
				}
				ProcessNextCommand(10);
			}
			else {
				BOOL bChanged = FALSE;
				// get port type
				// work out new list of port members etc   using ring.port style now
				if ((iNewTalkPort == iNewListenPort)&&(iNewTalkRing==iNewListenRing)) {
					if ((iNewTalkPort != i_Pre_Src_in) || (iNewTalkRing != iPreRingIn)) { // ie change
						if (i_Pre_Src_in>0) pTBConf->RemoveMemberFromConference(bncs_string("%1.%2").arg(iPreRingIn).arg(i_Pre_Src_in));
						if (i_Pre_Src_out>0) pTBConf->RemoveMemberFromConference(bncs_string("%1.%2").arg(iPreRingOut).arg(i_Pre_Src_out));
						// check for split port or 4W
						if ((iNewTalkPort>0) && (iNewTalkPort<MAX_RIEDEL_SLOTS)) {
							int iRiedelDev = 0;
							CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iNewTalkRing);
							if (pRing) iRiedelDev = pRing->iRing_Device_GRD;
							int portType = atoi(ExtractNamefromDevIniFile(iRiedelDev, DATABASE_RIEDEL_PORT_TYPE, iNewTalkPort));
							if (portType == PORT_TYPE_SPLIT) {
								ssData = bncs_string("%1.%2|L|0").arg(iNewTalkRing).arg(iNewTalkPort);  // port same as listen in this part
								pTBConf->AddMemberToConference(ssData, TRUE);
								ssData = bncs_string("%1.%2|TV|0").arg(iNewTalkRing).arg(iNewTalkPort);
								pTBConf->AddMemberToConference(ssData, TRUE);
							}
							else {
								ssData = bncs_string("%1.%2|TLV|0").arg(iNewTalkRing).arg(iNewTalkPort);
								pTBConf->AddMemberToConference(ssData, TRUE);
							}
							bChanged = TRUE;
						}
						else {
							Debug("ProcessAudioPackage4WTB - ring.port index %d.%d out of range ", iNewTalkRing, iNewTalkPort);
						}
					}
				}
				else {
					// in and out are different -- so must be a split port
					if ((iNewListenPort>0) && ((iNewListenPort != i_Pre_Src_out)||(iNewListenRing!=iPreRingOut)) ) {
						if (i_Pre_Src_out>0) pTBConf->RemoveMemberFromConference(bncs_string("%1.%2").arg(iPreRingOut).arg(i_Pre_Src_out));
						ssData = bncs_string("%1.%2|L|0").arg(iNewListenRing).arg(iNewListenPort);
						pTBConf->AddMemberToConference(ssData, TRUE);
						bChanged = TRUE;
					}
					if ((iNewTalkPort>0) && (iNewTalkPort != i_Pre_Src_in) || (iNewTalkRing != iPreRingIn)) {
						if (i_Pre_Src_in>0) pTBConf->RemoveMemberFromConference(bncs_string("%1.%2").arg(iPreRingIn).arg(i_Pre_Src_in));
						ssData = bncs_string("%1.%2|TV|0").arg(iNewTalkRing).arg(iNewTalkPort);
						pTBConf->AddMemberToConference(ssData, TRUE);
						bChanged = TRUE;
					}
				}

				//send new tb member list to riedel
				if (bChanged || bLabelChanged) {
					// get any override label
					bncs_string ssOverride = pTBConf->GetOverrideConferenceLabel();
					if ((ssOverride.length() > 1) && (ssOverride.toInt() != iPkgTBConf)) {
						wsprintf(szCommand, "IW %d '%s' %d", i_RingMaster_Conf, LPCSTR(ssOverride), iPkgTBConf + 1000);
					}
					else {
						// send main label first to conf
						wsprintf(szCommand, "IW %d '%s' %d", i_RingMaster_Conf, LPCSTR(ssMainLabel), iPkgTBConf + 1000);
					}
					AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);

					// send subtitle label --- RM db12 of rmstr conf device ( 782 and 802 )
					if (bLabelChanged) {
						SendConferenceRMSubtitleLabel(ssSubTitLbl, iPkgTBConf);
					}

					ProcessNextCommand(5);
					if (bChanged ) {
						// send details
						bncs_string ssData = pTBConf->GetAllConferenceDetails(',');
						wsprintf(szCommand, "IW %d '%s' %d", i_RingMaster_Conf, LPCSTR(ssData), iPkgTBConf);
						AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);
					}
				}
				//
			}
			ProcessNextCommand(5);
		}
		else
			Debug("ProcessAudioPackage4WTB - currently no TB dynamic conf assigned for pkg index %d ", pAudio->m_iAudioPkg_Indx);
	}
	else
		Debug("ProcessAudioPackage4WTB - no AudioPkg record given for conf %d  ", iWhichTB );
}

//
void ProcessDynamicConferenceCommand(int iAPIndex, bncs_string ssCmd)
{
	bncs_stringlist ssl_Cmd = bncs_stringlist(ssCmd.upper(), '=');
	CAudioPackages* pPkg = GetAudioPackage(iAPIndex);
	if ((pPkg) && (ssl_Cmd.count()>1)) {
		bncs_string ss_data = bncs_stringlist(ssl_Cmd[1], ',');
		int iUseType = 0;
		int iWhichConf = 0;
		// parse command 
		if (ssl_Cmd[0].find("GET") >= 0) {
			if ((ss_data.find("CF") >= 0) && (ss_data.find("_CONF") >= 0)) {
				ss_data.replace("CF", "");
				ss_data.replace("_CONF", "");
				iWhichConf = ss_data.toInt();
				iUseType = DYN_IFB_4WCONF_1 + iWhichConf - 1;
			}
			else if (ss_data.find("CONF") >= 0) {
				ss_data.replace("CONF", "");
				iWhichConf = ss_data.toInt();
				iUseType = DYN_TB_CONF_1 + iWhichConf - 1;
			}
			if ((iWhichConf > 0) && (iWhichConf < MAX_PKG_CONFIFB)) {
				// acquire confs for 4W in list for package - make sure not assigned already and that ports are assigned
				if ((iUseType >= DYN_IFB_4WCONF_1) && (iUseType <= DYN_IFB_4WCONF_4)) {
					pPkg->stCFIFBs[iWhichConf].i_FlagAssignCFConf = 1;
					if (pPkg->stCFIFBs[iWhichConf].i_DynAssignedIfb>0) {
						if (pPkg->stCFIFBs[iWhichConf].i_DynAssignedConference == 0) {
							int iNewConf = AcquireDynamicConferenceFromPool(iAPIndex, iWhichConf, iUseType);
							if (iNewConf > 0) {
								pPkg->stCFIFBs[iWhichConf].i_DynAssignedConference = iNewConf;
								// add conf  to any known keys used for assoc IFB and sset label / subtitle
								ApplyCFConferenceToIFBKeys(pPkg->stCFIFBs[iWhichConf].i_DynAssignedIfb, iNewConf);
									// RM conf changes
								bncs_string ssData = bncs_string("audpkg_index=%1,which_conf=%2,use_type=%3").arg(iAPIndex).arg(iWhichConf).arg(iUseType);
								AddCommandToQue(LPCSTR(ssData), DATABASECOMMAND, i_rtr_Infodrivers[1], iNewConf, DB_SYNC_DYN_CONF);
							}
							else
								Debug("ProcessDynConf - GET -CFCONF-ERROR- NO FREE CONFERENCES IN POOL");
						}
						else
							Debug("ProcessDynConf - GET CFCONF -ALREADY ASSIGNED for AP %d conf %d", iAPIndex, iWhichConf);
					}
					else
						Debug("ProcessDynConf - GET -CFCONF- no dyn assigned IFB in package %d ", iAPIndex);
				}
				else if ((iUseType >= DYN_TB_CONF_1) && (iUseType <= DYN_TB_CONF_2)) {
					if (pPkg->stTBConferences[iWhichConf].i_DynAssignedConference == 0) {
						int iNewConf = AcquireDynamicConferenceFromPool(iAPIndex, iWhichConf, iUseType);
						if (iNewConf > 0) {
							pPkg->stTBConferences[iWhichConf].i_DynAssignedConference = iNewConf;
							// add pkg ports if any to conf
							ProcessAudioPackage4WTB(iWhichConf, pPkg, 0, 0, 0, 0, TRUE);
							// RM conf changes  -- labels/ subtitles sent in ProcessAudioPkg4Wtb function
							bncs_string ssData = bncs_string("audpkg_index=%1,which_conf=%2,use_type=%3").arg(iAPIndex).arg(iWhichConf).arg(iUseType);
							AddCommandToQue(LPCSTR(ssData), DATABASECOMMAND, i_rtr_Infodrivers[1], iNewConf, DB_SYNC_DYN_CONF);
						}
						else
							Debug("ProcessDynConf - GET TB CONF -ERROR- NO FREE CONFERENCES IN POOL");
					}
					else
						Debug("ProcessDynConf - GET TB CONF -ALREADY ASSIGNED for AP %d conf %d", iAPIndex, iWhichConf);
				}
				else
					Debug("ProcessDynConf - GET CF / TB CONF invalid type");
			}
			else
				Debug("ProcessDynConf - GET CONF -invalid data for AP %d cmd %s", iAPIndex, LPCSTR(ssCmd));
		}
		else if (ssl_Cmd[0].find("FREE") >= 0) {
			if ((ss_data.find("CF") >= 0) && (ss_data.find("_CONF") >= 0)) {
				ss_data.replace("CF", "");
				ss_data.replace("_CONF", "");
				iWhichConf = ss_data.toInt();
				iUseType = DYN_IFB_4WCONF_1 + iWhichConf - 1;
			}
			else if (ss_data.find("CONF") >= 0) {
				ss_data.replace("CONF", "");
				iWhichConf = ss_data.toInt();
				iUseType = DYN_TB_CONF_1 + iWhichConf - 1;
			}
			if ((iWhichConf > 0) && (iWhichConf < MAX_PKG_CONFIFB)) {
				// free confs in data list from use for package
				if ((iUseType >= DYN_IFB_4WCONF_1) && (iUseType <= DYN_IFB_4WCONF_4)) {
					pPkg->stCFIFBs[iWhichConf].i_FlagAssignCFConf = 0;
					if (pPkg->stCFIFBs[iWhichConf].i_DynAssignedConference > 0) {
						int iConf = pPkg->stCFIFBs[iWhichConf].i_DynAssignedConference;
						// free this conf and keys assoc to it  
						CRingMaster_CONF* pCFConf = GetRingMaster_CONF_Record(iConf);
						if (pCFConf) {
							ClearAllConferenceKeyMembers(pCFConf);
							ClearConferencePortMembers(iConf);
							// add back into pool
							pCFConf->SetDynamicAudioPackage(0, 0, 0);
						}
						pPkg->stCFIFBs[iWhichConf].i_DynAssignedConference = 0;
						// RM conf changes
						bncs_string ssData = bncs_string("audpkg_index=0,which_conf=0,use_type=0");
						AddCommandToQue(LPCSTR(ssData), DATABASECOMMAND, i_rtr_Infodrivers[1], iConf, DB_SYNC_DYN_CONF);
						// CLEAR  subtitle DB12 to ringmaster 782 / 802 conf 
						SendConferenceRMSubtitleLabel(bncs_string(" "), iConf);
					}
					else
						Debug("ProcessDynConf - FREE CONF -NOT ASSIGNED for AP %d conf %d", iAPIndex, iWhichConf);
				}
				else if ((iUseType >= DYN_TB_CONF_1) && (iUseType <= DYN_TB_CONF_2)) {
					if (pPkg->stTBConferences[iWhichConf].i_DynAssignedConference > 0) {
						int iConf = pPkg->stTBConferences[iWhichConf].i_DynAssignedConference;
						// free this conf and keys assoc to it  
						CRingMaster_CONF* pTBConf = GetRingMaster_CONF_Record(iConf);
						if (pTBConf) {
							ClearAllConferenceKeyMembers(pTBConf);
							ClearConferencePortMembers(iConf);
							// add back into pool
							pTBConf->SetDynamicAudioPackage(0, 0, 0);
						}
						pPkg->stTBConferences[iWhichConf].i_DynAssignedConference = 0;
						// RM conf changes
						bncs_string ssData = bncs_string("audpkg_index=0,which_conf=0,use_type=0");
						AddCommandToQue(LPCSTR(ssData), DATABASECOMMAND, i_rtr_Infodrivers[1], iConf, DB_SYNC_DYN_CONF);
						// CLEAR  subtitle DB12 to ringmaster 782 / 802 conf 
						SendConferenceRMSubtitleLabel(bncs_string(" "), iConf);
					}
					else
						Debug("ProcessDynConf - FREE TB CONF -NOT ASSIGNED for AP %d conf %d", iAPIndex, iWhichConf);
				}
			}
		}
		else
			Debug("ProcessDynConf invalid command");

		ProcessNextCommand(10);

		//update infodriver slot now with all status data inc current conf in use
		UpdateAudioPackageStatusInfodriver(iAPIndex);
		//
		Calculate4WConferencePoolUse();
		// if current package update gui
		if (iChosenAudioSrcPkg == iAPIndex) DisplayAudioSourcePackageData();

	}
	else
		Debug("ProcessDynConf invalid class passed");
}


void ProcessAudioPackageInfodriverCommand(int iAudioPackage, bncs_string ssCommand)
{
	// parse command for (a) rev vision  (b) dyn conf request  or (c) dyn ifb assign request
	// was if ((ssCommand.find("vision") >= 0) || (ssCommand.find("audio") >= 0)) {
	if (ssCommand.find("rev_enabled") >= 0)  {
		CAudioPackages* pAudio = GetAudioPackage(iAudioPackage);
		if (pAudio) {
			// set enable flag to command state given
			bncs_stringlist ssl = bncs_stringlist(ssCommand, ',');
			pAudio->iEnabledRevVision = ssl.getNamedParam("rev_enabled").toInt();
			ProcessAudioPackageReverseRouting(iAudioPackage, pAudio->iEnabledRevVision);
		}
	}
	else if ((ssCommand.find("GET") >= 0)||(ssCommand.find("FREE") >= 0)) {
		// allocate or free dynamic TB conference
		ProcessDynamicConferenceCommand(iAudioPackage, ssCommand);
	}
}


////////////////////////////////////////////////////////////////////////////////////////
void UpdateRoutersDestUseInfodriverSlots( int iRouterDest )
{
	// updates all dests in global list for the infodriver based on router dest usage in packages
		bncs_string ssContents = "";
		CRevsRouterData* pHDMain = GetRouterRecord(iIP_RouterVideoHD);
		CRevsRouterData* pAncMain = GetRouterRecord(iIP_RouterANC);
		CRevsRouterData* pAudio2Ch = GetRouterRecord(iIP_RouterAudio2CH);
		if (pHDMain&&pAncMain&&pAudio2Ch) {
			if (iRouterDest<=pHDMain->getMaximumDestinations()) ssContents.append(bncs_string("hd=%1").arg(pHDMain->getRouterDestPackageIndex(iRouterDest)));   
			if (iRouterDest <= pAudio2Ch->getMaximumDestinations()) ssContents.append(bncs_string(",audio=%1").arg(pAudio2Ch->getRouterDestPackageIndex(iRouterDest)));
			if (iRouterDest <= pAncMain->getMaximumDestinations()) ssContents.append(bncs_string(",anc=%1").arg(pAncMain->getRouterDestPackageIndex(iRouterDest)));
			if (iRouterDest <= pHDMain->getMaximumDestinations()) ssContents.append(bncs_string(",revvis=%1").arg(pHDMain->getReverseReturnPackageIndex(iRouterDest)));
			if (iRouterDest <= pAudio2Ch->getMaximumDestinations()) ssContents.append(bncs_string(",audret=%1").arg(pAudio2Ch->getReverseReturnPackageIndex(iRouterDest)));
		}
		//
		if (ssContents.length()>255) ssContents.truncate(254);
		SendPackagerBNCSRevertive(iRouterDest, AUTO_RTR_DEST_USE_INFO, FALSE, LPCSTR(ssContents));
}


bncs_string UpdateAllRoutersFor_AudioPackageReturns(CAudioPackages* pAudio, int iAddOrRemove)
{ 
	bncs_stringlist sslist = bncs_stringlist("", ',');
	if (pAudio) {
		CRevsRouterData* pHDMain = GetRouterRecord(iIP_RouterVideoHD);
		CRevsRouterData* pAudio2ChRtr = GetRouterRecord(iIP_RouterAudio2CH);
		// 
		if (pHDMain&&pAudio2ChRtr) { 
			// audio level in 4 video levels
			if (pAudio->st_Reverse_Vision.iAssocIndex > 0) {
				if (sslist.find(bncs_string(pAudio->st_Reverse_Vision.iAssocIndex))<0) sslist.append(bncs_string(pAudio->st_Reverse_Vision.iAssocIndex));
				if (iAddOrRemove<0) pHDMain->removeReverseReturnPackageIndex(pAudio->st_Reverse_Vision.iAssocIndex, pAudio->m_iAudioPkg_Indx);
				if (iAddOrRemove>0) pHDMain->addReverseReturnPackageIndex(pAudio->st_Reverse_Vision.iAssocIndex, pAudio->m_iAudioPkg_Indx);
			}
			for (int iiar = 1; iiar<MAX_PKG_VIDEO; iiar++) {
				if (pAudio->st_AudioReturnPair[iiar].iAssocIndex > 0) {
					if (sslist.find(bncs_string(pAudio->st_AudioReturnPair[iiar].iAssocIndex))<0) sslist.append(bncs_string(pAudio->st_AudioReturnPair[iiar].iAssocIndex));
					if (iAddOrRemove<0) pAudio2ChRtr->removeReverseReturnPackageIndex(pAudio->st_AudioReturnPair[iiar].iAssocIndex, pAudio->m_iAudioPkg_Indx);
					if (iAddOrRemove>0) pAudio2ChRtr->addReverseReturnPackageIndex(pAudio->st_AudioReturnPair[iiar].iAssocIndex, pAudio->m_iAudioPkg_Indx);
				}
			}
		}
	}
	else
		Debug("UpdateAllRtrAudio_PkgUse -error- invalid audio pkg classes passed");
	// go thru slots array and build list of those dests involved
	return sslist.toString(',');
}


/////////////////////////////////////////////////////////////////////////////////////////

BOOL ReloadASourcePackageData( int iPkgIndex, BOOL bReassertRoutes  ) 
{
	CSourcePackages* pSPkg = GetSourcePackage(iPkgIndex);
	if (pSPkg) {
		 // get key current data - to act as reference and determine what has changed in updated package
		//
		// store current audio packages to process any removals / changes later
		BOOL bLabelChanged = FALSE, bVideoLevelsChange = FALSE, bAudioPkgsChange = FALSE;
		int iPreviousNumberAPs = 0;
		int iPreviousAudioPackages[40] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		// add 4 APs from video levels into array- as may have changed and need clearing down / sorting out
		for (int jj = 1; jj<MAX_PKG_VIDEO; jj++) {
			if (pSPkg->st_SourceAudioLevels[jj].iAssocIndex>0) {
				if (iPreviousNumberAPs<38) iPreviousNumberAPs++;
				iPreviousAudioPackages[iPreviousNumberAPs] = pSPkg->st_SourceAudioLevels[jj].iAssocIndex;
			}
		}
		for (int iaa = 1; iaa <= pSPkg->m_iNumberAssignedAudioPackages; iaa++) {
			if (pSPkg->m_iAssoc_Audio_Packages[iaa]>0) {
				if (iPreviousNumberAPs < 38) iPreviousNumberAPs++;
				iPreviousAudioPackages[iPreviousNumberAPs] = pSPkg->m_iAssoc_Audio_Packages[iaa];
			}
		}
		bncs_string ssPrev_VidLevels = pSPkg->ss_PackageVideoLvls;
		bncs_string ssPrev_AudPkgs = pSPkg->ss_PackageDefinition;

		//
		// store and get new data
		pSPkg->ss_PackageButtonName = dbmPkgs.getName(iPackager_router_main, DB_SOURCE_BUTTON, iPkgIndex);
		pSPkg->ss_PackageButtonName.replace('|', ' ');
		pSPkg->ss_PackageLongName = dbmPkgs.getName(iPackager_router_main, DB_SOURCE_NAME, iPkgIndex);
		pSPkg->ss_PackageKeyInfo = dbmPkgs.getName(iPackager_router_main, DB_SOURCE_INFO, iPkgIndex);
		pSPkg->ss_PackageDefinition = dbmPkgs.getName(iPackager_router_main, DB_SOURCE_AUDIO_PKGS, iPkgIndex);                   // list of assigned audio packages
		pSPkg->ss_PackageMCRUMD = dbmPkgs.getName(iPackager_router_main, DB_SOURCE_MCR_UMDS, iPkgIndex);
		pSPkg->ss_PackageProdUMDs = dbmPkgs.getName(iPackager_router_main, DB_SOURCE_PROD_UMDS, iPkgIndex);
		pSPkg->ss_PackageVideoLvls = dbmPkgs.getName(iPackager_router_main, DB_SOURCE_VIDEO_LVLS, iPkgIndex);

		// if (!AreBncsStringsEqual(pSPkg->ss_PackageDefinition, ssPrev_AudPkgs)) bAudioPkgsChange = TRUE;
		// if (!AreBncsStringsEqual(pSPkg->ss_PackageVideoLvls, ssPrev_VidLevels)) bVideoLevelsChange = TRUE;

		// load all src pkg stuff required db4
		bncs_stringlist sslData = bncs_stringlist(pSPkg->ss_PackageKeyInfo, ',');
		if (sslData.count() > 0) {
			pSPkg->m_iOwnerType = sslData.getNamedParam("owner").toInt();
			pSPkg->m_iDelegateType = sslData.getNamedParam("delegate").toInt();
		}
		// db6
		sslData = bncs_stringlist(pSPkg->ss_PackageDefinition, '|');
		pSPkg->m_iNumberAssignedAudioPackages = 0;
		for (int iic = 0; iic < MAX_PKG_AUDIO; iic++) pSPkg->m_iAssoc_Audio_Packages[iic] = 0;
		for (int ii = 0; ii < sslData.count(); ii++) {
			int iAPIndx = sslData[ii].toInt();			
			if ((iAPIndx>0) && (pSPkg->m_iNumberAssignedAudioPackages < 32)) {
				pSPkg->m_iNumberAssignedAudioPackages++;
				if (pSPkg->m_iAssoc_Audio_Packages[pSPkg->m_iNumberAssignedAudioPackages] != iAPIndx) bAudioPkgsChange = TRUE;
				pSPkg->m_iAssoc_Audio_Packages[pSPkg->m_iNumberAssignedAudioPackages] = iAPIndx;
				// add into audio package pti list
				CAudioPackages* pAudio = GetAudioPackage(iAPIndx);
				if (pAudio) {
					if (pAudio->ssl_TiedToSourcePackages.find(bncs_string(iPkgIndex)) < 0) 
						pAudio->ssl_TiedToSourcePackages.append(bncs_string(iPkgIndex));
				}
			}
		}

		// db 11  - 4 levels to be processed
		sslData = bncs_stringlist(pSPkg->ss_PackageVideoLvls, ',');
		for (int iLvl = 1; iLvl < MAX_PKG_VIDEO; iLvl++) {
			bncs_stringlist sslvl = bncs_stringlist(sslData.getNamedParam(bncs_string("L%1").arg(iLvl)), '|');
			if (sslvl.count()>5) {
				// should be six entries
				int iDataIndex = 0, iLangUseTag = 0, iHubTypeTag = 0;
				// video
				bncs_string ssLvlD = sslvl[0];
				ExtractRouterIndexTagData(ssLvlD, &iDataIndex, &iHubTypeTag, &iLangUseTag);
				if (pSPkg->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocIndex != iDataIndex) bVideoLevelsChange = TRUE;
				if (pSPkg->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocHub_or_TypeTag != iHubTypeTag) bVideoLevelsChange = TRUE;
				pSPkg->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocIndex = iDataIndex;
				pSPkg->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocHub_or_TypeTag = iHubTypeTag;
				// anc 1
				for (int iiAnc = 1; iiAnc < MAX_PKG_VIDEO; iiAnc++) {
					ssLvlD = sslvl[iiAnc];
					ExtractRouterIndexTagData(ssLvlD, &iDataIndex, &iHubTypeTag, &iLangUseTag);
					pSPkg->st_PkgVideoLevels[iLvl].stANCLevel[iiAnc].iAssocIndex = iDataIndex;
					pSPkg->st_PkgVideoLevels[iLvl].stANCLevel[iiAnc].iAssocHub_or_TypeTag = iHubTypeTag;
				}
				// default audio connected to video -- Audio Package ?? or audio router source index ??
				ssLvlD = sslvl[5];
				ExtractRouterIndexTagData(ssLvlD, &iDataIndex, &iHubTypeTag, &iLangUseTag);
				if (pSPkg->st_SourceAudioLevels[iLvl].iAssocIndex != iDataIndex) bVideoLevelsChange = TRUE;
				if (pSPkg->st_SourceAudioLevels[iLvl].iAssocHub_or_TypeTag != iHubTypeTag) bVideoLevelsChange = TRUE;
				pSPkg->st_SourceAudioLevels[iLvl].iAssocIndex = iDataIndex;
				pSPkg->st_SourceAudioLevels[iLvl].iAssocHub_or_TypeTag = iHubTypeTag;
				// add into audio package pti list
				CAudioPackages* pAudio = GetAudioPackage(iDataIndex);
				if (pAudio) {
					if (pAudio->ssl_TiedToSourcePackages.find(bncs_string(iPkgIndex)) < 0)
						pAudio->ssl_TiedToSourcePackages.append(bncs_string(iPkgIndex));
				}
			}
		}

		if (!bAutomaticStarting) {
			// process any changes as not starting - (auto starting just loads up stuff)
			// possible change in sources for vision / audio - so reassert routes

			// go thru old list of APs - determine if any been removed from current list
			// is this AP no longer in prev list - ie been removed -- to builds list to clear down routes, ifbs, etc 
			if (iPreviousNumberAPs > 0)  {
				bncs_stringlist ssl_removedAPs = bncs_stringlist("", ',');
				for (int iiprev = 1; iiprev <= iPreviousNumberAPs; iiprev++) {
					int iPrevAP = iPreviousAudioPackages[iiprev];
					BOOL bFound = FALSE;
					if (pSPkg->st_SourceAudioLevels[1].iAssocIndex == iPrevAP) bFound = TRUE;
					if (pSPkg->st_SourceAudioLevels[2].iAssocIndex == iPrevAP) bFound = TRUE;
					if (pSPkg->st_SourceAudioLevels[3].iAssocIndex == iPrevAP) bFound = TRUE;
					if (pSPkg->st_SourceAudioLevels[4].iAssocIndex == iPrevAP) bFound = TRUE;
					if (!bFound) {
						for (int iicurr = 1; iicurr < pSPkg->m_iNumberAssignedAudioPackages; iicurr++) {
							if (pSPkg->m_iAssoc_Audio_Packages[iicurr] == iPrevAP) bFound = TRUE;
							if (bFound) iicurr = pSPkg->m_iNumberAssignedAudioPackages + 2; // break loop early if found
						}
					}
					// if still not found then was previously assigned and not now
					if (!bFound) {
						CAudioPackages* pPrevAudio = GetAudioPackage(iPrevAP);
						if (pPrevAudio) {
							int iPos = pPrevAudio->ssl_TiedToSourcePackages.find(bncs_string(iPkgIndex));
							if (iPos >= 0) pPrevAudio->ssl_TiedToSourcePackages.deleteItem(iPos);
						}
						
					}
				} // for iiprev
			}

			if (bReassertRoutes) {
				if (bVideoLevelsChange || bAudioPkgsChange) {
					// now get list of all dests routed from this src package that may need routing changes
					iRecursiveCounter = 0;
					ssl_RoutedDestPackages.clear();
					for (int i = 0; i < pSPkg->ssl_Pkg_DirectlyRoutedTo.count(); i++) {
						BuildDownStreamDestList(pSPkg->ssl_Pkg_DirectlyRoutedTo[i].toInt());
					}
					// now process thru this list of calc dest packages
					for (int iis = 0; iis < ssl_RoutedDestPackages.count(); iis++) {
						int iDestPkgIndx = ssl_RoutedDestPackages[iis].toInt();
						CDestinationPackages* pDestNext = GetDestinationPackage(iDestPkgIndx);
						if (pDestNext) {
							// video and audio - build list back to primary source
							ProcessAllDestinationPackageRouting(pDestNext, 1, bReassertRoutes);
							if ((pDestNext->iPackageIndex > 0) && (pDestNext->iPackageIndex < i_Start_VirtualPackages) &&
								(pDestNext->i_Routed_Src_Package >= i_Start_VirtualPackages) && (pDestNext->iPackageIndex < (iNumberOfPackages+1))) {
									UpdateRoutedVirtualsVLevel(pDestNext->iPackageIndex, 0, pDestNext->i_Routed_Src_Package);
							}
						}
					} // for directly routed to
				}
				else
					Debug("ReloadASrourcePkg -1- RM reassert routing skipped as no data changes for srce pkg %d ", iPkgIndex);

			} // if reassert routes

		} // if !bautoStarting 

		if (iChosenSrcPkg == iPkgIndex) DisplaySourcePackageData();

		return TRUE;

	}
	else {
		Debug( "ReloadASrcPackage -- invalid class for index %d", iPkgIndex );
	}
	return FALSE;
}

/* */
BOOL  ReloadAnAudioPackageData(int iPkgIndex, BOOL bReassertRoutes)
{
	bncs_string  ssData = "";
	CAudioPackages* pAudio = GetAudioPackage(iPkgIndex);
	if (pAudio) {
		// get key current data - to act as reference and determine what has changed in updated package
		//
		// store current importnat stuff 
		BOOL bLabelsChanged = FALSE, bCommsChanges=FALSE;
		BOOL bAudioDataChanged = FALSE;
		bncs_string ssPrevAudio16 = pAudio->m_ss_AudioSourceDefinition16;
		bncs_string ssPrevLabels67 = pAudio->m_ss_AudioCFeedLabelSubtitles;
		bncs_string ssPrevLabels68 = pAudio->m_ss_AudioConfLabelSubtitles;
		int iPreviousLanguageTag = pAudio->m_iOveralLanguageTag;
		int iPreviousRevVision = pAudio->st_Reverse_Vision.iAssocIndex;
		int iPreviousVidLink = pAudio->m_iVideoLinkIndex;
		int iPreviousRevAudio[MAX_PKG_VIDEO] = { 0, 0, 0, 0, 0 };
		for (int ii = 1; ii < MAX_PKG_VIDEO; ii++) iPreviousRevAudio[ii] = pAudio->st_AudioReturnPair[ii].iAssocIndex;

		for (int iifc = 0; iifc < MAX_PKG_CONFIFB; iifc++) {
			stPreIFBS[iifc].i_out_ring = pAudio->stCFIFBs[iifc].i_in_ring;
			stPreIFBS[iifc].i_out_port = pAudio->stCFIFBs[iifc].i_in_port;
			stPreIFBS[iifc].i_out_ring = pAudio->stCFIFBs[iifc].i_out_ring;
			stPreIFBS[iifc].i_out_port = pAudio->stCFIFBs[iifc].i_out_port;
			stPreIFBS[iifc].m_ss_MainLabel = pAudio->stCFIFBs[iifc].m_ss_MainLabel;
			stPreIFBS[iifc].m_ss_SubTitleLabel = pAudio->stCFIFBs[iifc].m_ss_SubTitleLabel;
			stPreIFBS[iifc].i_Use_Which_DestPkgIFB = pAudio->stCFIFBs[iifc].i_Use_Which_DestPkgIFB;
			//
			stPreTBConfs[iifc].i_in_ring = pAudio->stTBConferences[iifc].i_in_ring;
			stPreTBConfs[iifc].i_in_port = pAudio->stTBConferences[iifc].i_in_port;
			stPreTBConfs[iifc].i_out_ring = pAudio->stTBConferences[iifc].i_out_ring;
			stPreTBConfs[iifc].i_out_port = pAudio->stTBConferences[iifc].i_out_port;
			stPreTBConfs[iifc].m_ss_MainLabel = pAudio->stTBConferences[iifc].m_ss_MainLabel;
			stPreTBConfs[iifc].m_ss_SubTitleLabel = pAudio->stTBConferences[iifc].m_ss_SubTitleLabel;
			stPreTBConfs[iifc].i_Use_Which_DestPkgIFB = 0;
			//
		}

		// get all return rtr dests currently assoc to this audio package and unlink the audio package from rtr dests
		bncs_stringlist ssl_RouterDestsUpdate = bncs_stringlist("", ',');
		if (!bAutomaticStarting) {
			bncs_stringlist ssll = bncs_stringlist(UpdateAllRoutersFor_AudioPackageReturns(pAudio, -1), ',');  // remove elements
			for (int ii = 0; ii < ssll.count(); ii++) ssl_RouterDestsUpdate.append(ssll[ii]);
		}

		// store and get new data -- device 1001
		pAudio->m_ssButtonName = dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_BUTTON, iPkgIndex);
		pAudio->m_ssButtonName.replace('|', ' ');
		pAudio->m_ssPackageName = dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_NAME, iPkgIndex);
		pAudio->m_ss_PackageKeyInfo = dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_INFO, iPkgIndex);
		pAudio->m_ss_PackageDefinition = dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_DEFN, iPkgIndex);
		pAudio->m_ss_AudioSourceDefinition16 = dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_LINKS16, iPkgIndex);
		pAudio->m_ss_AudioReturnDefinition = dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_RETURN, iPkgIndex);

		pAudio->m_iVideoLinkIndex = bncs_string(dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_VIDLINK, iPkgIndex)).toInt();
		pAudio->m_ss_AudioCFeedLabelSubtitles = dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_CFLABEL, iPkgIndex);
		pAudio->m_ss_AudioConfLabelSubtitles = dbmPkgs.getName(iPackager_router_main, DB_AUDIOPKG_CONFLBL, iPkgIndex);

		if (!AreBncsStringsEqual(pAudio->m_ss_AudioSourceDefinition16, ssPrevAudio16)) bAudioDataChanged = TRUE;
		if (!AreBncsStringsEqual(pAudio->m_ss_AudioCFeedLabelSubtitles, ssPrevLabels67)) bLabelsChanged = TRUE;
		if (!AreBncsStringsEqual(pAudio->m_ss_AudioConfLabelSubtitles, ssPrevLabels68)) bLabelsChanged = TRUE;

		// audio parse and store
		bncs_stringlist sslData = bncs_stringlist(pAudio->m_ss_PackageKeyInfo, ',');
		if (sslData.count() > 0) {
			pAudio->m_ssUniqueID = sslData.getNamedParam("id");
			pAudio->m_iOwnerType = sslData.getNamedParam("owner").toInt();
			pAudio->m_iDelegateType = sslData.getNamedParam("delegate").toInt();
		}
		int iDataIndex = 0, iLangUseTag = 0, iHubTypeTag = 0;
		bncs_string ssdata = "";
		// db63 
		sslData = bncs_stringlist(pAudio->m_ss_PackageDefinition, ',');
		if (sslData.count() > 0) {
			// video ????? 
			//ExtractRouterIndexTagData(sslData.getNamedParam("video"), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			//pAudio->st_VideoLink_Vision.iAssocIndex = iDataIndex;
			//pAudio->st_VideoLink_Vision.iAssocHub_or_TypeTag = iHubTypeTag;
			// rev vision
			ExtractRouterIndexTagData(sslData.getNamedParam("rev_vision"), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pAudio->st_Reverse_Vision.iAssocIndex = iDataIndex;
			pAudio->st_Reverse_Vision.iAssocHub_or_TypeTag = iHubTypeTag;
			pAudio->st_Reverse_Vision.iAssocLang_or_UseTag = iLangUseTag;
			// overall lang tag                                                                                    
			pAudio->m_iOveralLanguageTag = sslData.getNamedParam("language_tag").toInt();
			if (pAudio->m_iOveralLanguageTag != iPreviousLanguageTag) bAudioDataChanged = TRUE;
			// ifbs and 4W
			for (int iifb = 1; iifb < MAX_PKG_CONFIFB; iifb++) {
				int ir1 = 0, ip1 = 0, ir2 = 0, ip2 = 0;
				//  TB confs
				GetTwinRiedelParameters(sslData.getNamedParam(bncs_string("conf%1").arg(iifb)), &ir1, &ip1, &ir2, &ip2);
				pAudio->stTBConferences[iifb].i_in_ring = ir1;
				pAudio->stTBConferences[iifb].i_in_port = ip1;
				pAudio->stTBConferences[iifb].i_out_ring = ir2;
				pAudio->stTBConferences[iifb].i_out_port = ip2;
				if ((ir1 != stPreTBConfs[iifb].i_in_ring) || (ir2 != stPreTBConfs[iifb].i_out_ring) || (ip1 != stPreTBConfs[iifb].i_in_port) || (ip2 != stPreTBConfs[iifb].i_out_port)) bCommsChanges = TRUE;
				// CF IFB ports -- for conf too ??
				GetTwinRiedelParameters(sslData.getNamedParam(bncs_string("cf%1").arg(iifb)), &ir1, &ip1, &ir2, &ip2);
				pAudio->stCFIFBs[iifb].i_in_ring = ir1;
				pAudio->stCFIFBs[iifb].i_in_port = ip1;
				pAudio->stCFIFBs[iifb].i_out_ring = ir2;
				pAudio->stCFIFBs[iifb].i_out_port = ip2;
				if ((ir1 != stPreIFBS[iifb].i_in_ring) || (ir2 != stPreIFBS[iifb].i_out_ring) || (ip1 != stPreIFBS[iifb].i_in_port) || (ip2 != stPreIFBS[iifb].i_out_port)) bCommsChanges = TRUE;
				//
				pAudio->stCFIFBs[iifb].i_Use_Which_DestPkgIFB = sslData.getNamedParam(bncs_string("cf%1_ifb").arg(iifb)).toInt();
				if (pAudio->stCFIFBs[iifb].i_Use_Which_DestPkgIFB!=stPreIFBS[iifb].i_Use_Which_DestPkgIFB) bCommsChanges = TRUE;
			}

		}
		// db64
		sslData = bncs_stringlist(pAudio->m_ss_AudioSourceDefinition16, ',');
		for (int ii = 1; ii < MAX_AUDLVL_AUDIO; ii++) {
			iDataIndex = 0, iLangUseTag = 0, iHubTypeTag = 0;
			if (sslData.count() > 0)  ExtractRouterIndexTagData(sslData.getNamedParam(bncs_string("a%1").arg(ii)), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pAudio->st_AudioSourcePair[ii].iAssocIndex = iDataIndex;
			pAudio->st_AudioSourcePair[ii].iAssocHub_or_TypeTag = iHubTypeTag;
		}
		// db65
		sslData = bncs_stringlist(pAudio->m_ss_AudioReturnDefinition, ',');
		for (int ii = 1; ii < MAX_PKG_VIDEO; ii++) {
			iDataIndex = 0, iLangUseTag = 0, iHubTypeTag = 0;
			if (sslData.count() > 0)  ExtractRouterIndexTagData(sslData.getNamedParam(bncs_string("a%1").arg(ii)), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pAudio->st_AudioReturnPair[ii].iAssocIndex = iDataIndex;
			pAudio->st_AudioReturnPair[ii].iAssocHub_or_TypeTag = iHubTypeTag;
			pAudio->st_AudioReturnPair[ii].iAssocLang_or_UseTag = iLangUseTag;
		}
		// db67 and 68 to extract labels and subtitles 1 and 2;
		sslData = bncs_stringlist(pAudio->m_ss_AudioCFeedLabelSubtitles, ',');
		for (int iifb = 1; iifb < MAX_PKG_CONFIFB; iifb++) {
			pAudio->stCFIFBs[iifb].m_ss_MainLabel = sslData.getNamedParam(bncs_string("label_%1").arg(iifb));
			pAudio->stCFIFBs[iifb].m_ss_SubTitleLabel = sslData.getNamedParam(bncs_string("sub_%1").arg(iifb));
		}
		sslData = bncs_stringlist(pAudio->m_ss_AudioConfLabelSubtitles, ',');
		for (int iconf = 1; iconf < MAX_PKG_CONFIFB; iconf++) {
			pAudio->stTBConferences[iconf].m_ss_MainLabel = sslData.getNamedParam(bncs_string("label_%1").arg(iconf));
			pAudio->stTBConferences[iconf].m_ss_SubTitleLabel = sslData.getNamedParam(bncs_string("sub_%1").arg(iconf));
		}

		bncs_stringlist ssll2 = bncs_stringlist(UpdateAllRoutersFor_AudioPackageReturns(pAudio, 1), ',');
		for (int ii2 = 0; ii2 < ssll2.count(); ii2++) {
			if (ssl_RouterDestsUpdate.find(ssll2[ii2]) < 0) ssl_RouterDestsUpdate.append(ssll2[ii2]);
		}

		//
		if (!bAutomaticStarting) {

			// update infodrivers for router destination usage
			for (int iiu = 0; iiu < ssl_RouterDestsUpdate.count(); iiu++) {
				UpdateRoutersDestUseInfodriverSlots(ssl_RouterDestsUpdate[iiu].toInt());
			}

			// according to what has changed - do audio routing - for linked source packages 
			if ((bReassertRoutes)&&(bAudioDataChanged)) {
				// 
				for (int iit = 0; iit < pAudio->ssl_TiedToSourcePackages.count(); iit++) {
					CSourcePackages* pSPkg = GetSourcePackage(pAudio->ssl_TiedToSourcePackages[iit].toInt());
					if (pSPkg) {
						// now get list of all dests routed from this src package that may need routing changes
						iRecursiveCounter = 0;
						ssl_RoutedDestPackages.clear();
						for (int iid = 0; iid < pSPkg->ssl_Pkg_DirectlyRoutedTo.count(); iid++) {
							BuildDownStreamDestList(pSPkg->ssl_Pkg_DirectlyRoutedTo[iid].toInt());
						}
						// now process thru this list of calc dest packages
						for (int iis = 0; iis < ssl_RoutedDestPackages.count(); iis++) {
							int iDestPkgIndx = ssl_RoutedDestPackages[iis].toInt();
							CDestinationPackages* pDestNext = GetDestinationPackage(iDestPkgIndx);
							if (pDestNext) {
								// video and audio - build list back to primary source
								ProcessAllDestinationPackageRouting(pDestNext, 1, bReassertRoutes);
							}
						} // for iis - list of affected dest packages
					}
				} // for iit
			} // if breassert

			// reverse vision changes  // TOP MM Q ??
			// if previous was assigned and enab and something in mmq - then park prev rev vision
			if ((iPreviousRevVision > 0) && (pAudio->st_Reverse_Vision.iAssocIndex != iPreviousRevVision) && (pAudio->iEnabledRevVision>0) && (pAudio->ssl_Pkg_Queue.count()>0)) {
				pAudio->iRoutedRevVisionPkg = 0;
				SendSimpleNevionRouteCommand(iIP_RouterVideoHD, iPreviousRevVision, 0, 3);
			}
			// reverse audio changes -park old rev audio route ???
			for (int iiaud = 1; iiaud < MAX_PKG_VIDEO; iiaud++) {
				if ((iPreviousRevAudio[iiaud] > 0) && (pAudio->st_AudioReturnPair[iiaud].iAssocIndex != iPreviousRevAudio[iiaud]) && (pAudio->iEnabledRevVision>0) && (pAudio->ssl_Pkg_Queue.count()>0)) {
					pAudio->iRoutedRevAudioPkg = 0;
					SendSimpleNevionRouteCommand(iIP_RouterAudio2CH, iPreviousRevVision, 0, 3);
				}
			}
			//
			// change to rev vision -- if ena and dest pkg top of MM Q - and in dest pkg there is rev src to route - else park
			if ((pAudio->st_Reverse_Vision.iAssocIndex > 0) && (pAudio->st_Reverse_Vision.iAssocIndex != iPreviousRevVision) && (pAudio->iEnabledRevVision > 0) && (pAudio->ssl_Pkg_Queue.count()>0)) {
				ProcessAudioPackageReverseRouting(pAudio->m_iAudioPkg_Indx, 1);
			}

			/// Riedel comms changes -- any implications for virtuals 
			// update 4wire and ifb pti here now - rather than in routines below
			// need to remove / tidy up pre ports for package  -- only need to process if there is a change
			if ((bCommsChanges)||(bLabelsChanged)) {


				for (int iity = 1; iity < MAX_PKG_CONFIFB; iity++) {
					// tidy TB conf
					if ((stPreTBConfs[iity].i_in_port>0) && (stPreTBConfs[iity].i_in_port != pAudio->stTBConferences[iity].i_in_port))  {
						CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(stPreTBConfs[iity].i_in_ring);
						if (pRing) {
							TidyRiedelPortUsage(stPreTBConfs[iity].i_in_ring, stPreTBConfs[iity].i_in_port, PORT_USECODE_4WTB, iPkgIndex);
						}
					}
					if ((stPreTBConfs[iity].i_out_port > 0) && (stPreTBConfs[iity].i_out_port != pAudio->stTBConferences[iity].i_out_port))  {
						CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(stPreTBConfs[iity].i_out_ring);
						if (pRing) {
							TidyRiedelPortUsage(stPreTBConfs[iity].i_out_ring, stPreTBConfs[iity].i_out_port, PORT_USECODE_4WTB, iPkgIndex);
						}
					}
					// tidy CF ifb
					if ((stPreIFBS[iity].i_in_port > 0) && (stPreIFBS[iity].i_in_port != pAudio->stCFIFBs[iity].i_in_port))  {
						CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(stPreIFBS[iity].i_in_ring);
						if (pRing) {
							TidyRiedelPortUsage(stPreIFBS[iity].i_in_ring, stPreIFBS[iity].i_in_port, PORT_USECODE_CFIFB, iPkgIndex);
						}
					}
					if ((stPreIFBS[iity].i_out_port > 0) && (stPreIFBS[iity].i_out_port != pAudio->stCFIFBs[iity].i_out_port))  {
						CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(stPreIFBS[iity].i_out_ring);
						if (pRing) {
							TidyRiedelPortUsage(stPreIFBS[iity].i_out_ring, stPreIFBS[iity].i_out_port, PORT_USECODE_CFIFB, iPkgIndex);
						}
					}
				}
				// now calculate and update port use for defined ports in package
				CalculateRiedelPortUse(iPkgIndex, TRUE);

				// TBS
				// check if username or commslabel has changed
				for (int iicc = 1; iicc < MAX_PKG_CONFIFB; iicc++) {
					if (pAudio->stTBConferences[iicc].i_DynAssignedConference>0)
						ProcessAudioPackage4WTB(iicc, pAudio, stPreTBConfs[iicc].i_in_ring, stPreTBConfs[iicc].i_in_port, stPreTBConfs[iicc].i_out_ring, stPreTBConfs[iicc].i_out_port, bLabelsChanged);
				}
				//
				// IFBS
				for (int iifb = 1; iifb < MAX_PKG_CONFIFB; iifb++) {
					// determine if label has changed - and perhaps port change - else no need to process + waste time
					ProcessAudioPackageIFBS(iifb, pAudio, stPreIFBS[iifb].i_in_ring, stPreIFBS[iifb].i_in_port, stPreIFBS[iifb].i_out_ring, stPreIFBS[iifb].i_out_port, bLabelsChanged);
					// assoc cf dyn confs --- WHAT if labels / subtitles change whilst assigned ???
				}
			}

			//update infodriver slot now with all status data - rev vis; 4W confs; IFBs
			UpdateAudioPackageStatusInfodriver(iPkgIndex);
		}

		// update gui
		if (iChosenAudioSrcPkg == iPkgIndex) DisplayAudioSourcePackageData();

		// all done
		return TRUE;
	}
	return FALSE;
}


BOOL ReloadSportingEventData(int iIndex, BOOL bReassert)
{
	bncs_string  ssData = "";
	CSportEvent* pSpev = GetSportEventPackage(iIndex);
	if (pSpev) {

		// store and get new data - device 1001 dbs 30 - 39
		pSpev->m_ssName = dbmPkgs.getName(iPackager_router_main, DB_SPEV_BUTTON_NAME, iIndex);
		pSpev->m_ssEventID = dbmPkgs.getName(iPackager_router_main, DB_SPEV_EVENT_ID, iIndex);
		pSpev->m_ssAssetID = dbmPkgs.getName(iPackager_router_main, DB_SPEV_ASSET_ID, iIndex);
		pSpev->m_ssClassID = dbmPkgs.getName(iPackager_router_main, DB_SPEV_CLASS_ID, iIndex);
		pSpev->m_ss_TimeDefinition = dbmPkgs.getName(iPackager_router_main, DB_SPEV_DATE_TIME, iIndex);
		pSpev->m_ss_TeamDefinition = dbmPkgs.getName(iPackager_router_main, DB_SPEV_TEAMS_ID, iIndex);
		ssData = dbmPkgs.getName(iPackager_router_main, DB_SPEV_STATUS, iIndex);
		pSpev->m_iSPEV_Status = ssData.toInt();
		return TRUE;
	}
	else
		return FALSE;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////
//

bncs_string UpdateAllRoutersFor_DestPackageUsage(CDestinationPackages* pDPkg, int iAddOrRemove)
{ 
	BOOL bUHD = FALSE, bJpegXS=FALSE;
	bncs_stringlist sslist = bncs_stringlist("", ',');
	if (pDPkg) {
		CRevsRouterData* pHDMain = GetRouterRecord(iIP_RouterVideoHD);
		CRevsRouterData* pAncMain = GetRouterRecord(iIP_RouterANC);
		CRevsRouterData* pAudio2Ch = GetRouterRecord(iIP_RouterAudio2CH);
		// 
		// video - store in updateDSlot [0]
		if (pHDMain) {
			if (pDPkg->st_VideoHDestination.iAssocIndex>0) {
				if (sslist.find(bncs_string(pDPkg->st_VideoHDestination.iAssocIndex))<0) sslist.append(bncs_string(pDPkg->st_VideoHDestination.iAssocIndex));
				if (iAddOrRemove<0) pHDMain->removeRouterDestPackageIndex(pDPkg->st_VideoHDestination.iAssocIndex, pDPkg->iPackageIndex);
				if (iAddOrRemove>0) pHDMain->addRouterDestPackageIndex(pDPkg->st_VideoHDestination.iAssocIndex, pDPkg->iPackageIndex);
				//Debug("UpdateRtrDest HD %d for package %d -- add/rem %d", pDPkg->st_VideoHDestination.iAssocIndex, pDPkg->iPackageIndex, iAddOrRemove);
			}
		}
		// anc - store in updateDSlot [1..4]
		if (pAncMain) {
			for (int ii = 1; ii < MAX_PKG_VIDEO; ii++) {
				if (pDPkg->st_VideoANC[ii].iAssocIndex>0) {
					if (sslist.find(bncs_string(pDPkg->st_VideoANC[ii].iAssocIndex))<0) sslist.append(bncs_string(pDPkg->st_VideoANC[ii].iAssocIndex));
					if (iAddOrRemove<0) pAncMain->removeRouterDestPackageIndex(pDPkg->st_VideoANC[ii].iAssocIndex, pDPkg->iPackageIndex);
					if (iAddOrRemove>0) pAncMain->addRouterDestPackageIndex(pDPkg->st_VideoANC[ii].iAssocIndex, pDPkg->iPackageIndex);
				}
			}
		}
		// audio pairs
		if (pAudio2Ch) {
			for (int ii = 1; ii < MAX_PKG_AUDIO; ii++) {
				if (pDPkg->st_AudioDestPair[ii].iAssocIndex>0) {
					if (sslist.find(bncs_string(pDPkg->st_AudioDestPair[ii].iAssocIndex))<0) sslist.append(bncs_string(pDPkg->st_AudioDestPair[ii].iAssocIndex));
					if (iAddOrRemove<0) pAudio2Ch->removeRouterDestPackageIndex(pDPkg->st_AudioDestPair[ii].iAssocIndex, pDPkg->iPackageIndex);
					if (iAddOrRemove>0) pAudio2Ch->addRouterDestPackageIndex(pDPkg->st_AudioDestPair[ii].iAssocIndex, pDPkg->iPackageIndex);
				}
			}
		}
	}
	else
		Debug("UpdateRtrDest_PkgUse -error- invalid classes passed");
	return sslist.toString(',');
}


BOOL ReloadADestinationPackageData( int iPkgIndex, BOOL bReassertRouting ) 
{
	char szCommand[MAX_AUTO_BUFFER_STRING] = "", szData[64] = "";
	char szRouterCommand[MAX_AUTO_BUFFER_STRING] = "";
	int iPackage = 0;
	BOOL bVirtualPackage = FALSE;
	BOOL bCommsChanges = FALSE, bRoutingChanges = FALSE;
	if (iPkgIndex >= i_Start_VirtualPackages) bVirtualPackage = TRUE;

	CDestinationPackages* pDPkg = GetDestinationPackage(iPkgIndex);
	if (pDPkg) {
		//Debug("ReloadDestPkg -1- pkg %d ", iPkgIndex); 
		int iPrev_RouterDest = pDPkg->st_VideoHDestination.iAssocIndex;
		int iPrev_RouterTag = pDPkg->st_VideoHDestination.iAssocHub_or_TypeTag;
		int iPrev_AncDests[MAX_PKG_VIDEO];
		int iPrev_AudioDests[MAX_PKG_AUDIO];
		for (int ii = 0; ii < MAX_PKG_VIDEO; ii++) iPrev_AncDests[ii] = pDPkg->st_VideoANC[ii].iAssocIndex;
		for (int iiad = 0; iiad < MAX_PKG_AUDIO; iiad++) iPrev_AudioDests[iiad] = pDPkg->st_AudioDestPair[iiad].iAssocIndex;
		// reverse vision audio
		int iPreviousRevVision = pDPkg->st_Reverse_Vision.iAssocIndex;
		int iPreviousRevAudio[MAX_PKG_VIDEO] = { 0, 0, 0, 0, 0 };
		for (int ii = 1; ii < MAX_PKG_VIDEO; ii++) iPreviousRevAudio[ii] = pDPkg->st_AudioReturnPair[ii].iAssocIndex;

		bncs_string ssPrev_Definition = pDPkg->ss_Package_Definition;
		bncs_string ssPrev_AudioDests = pDPkg->ss_Package_AudioDests;
		bncs_string ssPrev_AudioTags = pDPkg->ss_Package_AudioTags;
		bncs_string ssPrev_AudioSub1 = pDPkg->ss_AudioTags_Substitues1;
		bncs_string ssPrev_AudioSub2 = pDPkg->ss_AudioTags_Substitues2;
		bncs_string ssPrev_AudioRet = pDPkg->ss_Package_AudioRtn;

		for (int iifc = 1; iifc < 3; iifc++) {
			stPreDPIFBS[iifc].i_mm_ring = pDPkg->st_AssocIFBs[iifc].i_mm_ring;
			stPreDPIFBS[iifc].i_mm_port = pDPkg->st_AssocIFBs[iifc].i_mm_port;
		}
		// get all rtr dests currently assoc to this package and unlink the package from rtr dests
		bncs_stringlist ssl_RouterDestsUpdate = bncs_stringlist("", ',');
		if (!bAutomaticStarting) {
			bncs_stringlist ssll = bncs_stringlist(UpdateAllRoutersFor_DestPackageUsage(pDPkg, -1), ',');  // remove elements
			for (int ii = 0; ii < ssll.count(); ii++) ssl_RouterDestsUpdate.append(ssll[ii]);
		}

		// store and get new data
		pDPkg->ss_PackageButtonName = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_BUTTON, iPkgIndex);
		pDPkg->ss_PackageButtonName.replace('|', ' ');
		pDPkg->ss_PackageLongName = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_NAME, iPkgIndex);
		pDPkg->ss_PackagerOwnerDelg = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_INFO, iPkgIndex);
		pDPkg->ss_Package_Definition = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_DEFN, iPkgIndex);
		pDPkg->ss_Package_AudioDests = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_AUDIO_DESTS32, iPkgIndex);       // new
		pDPkg->ss_Package_AudioTags = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_AUDIO_TAGS32, iPkgIndex);       // new
		pDPkg->ss_AudioTags_Substitues1 = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_AUDIO_SUBS_1, iPkgIndex);
		pDPkg->ss_AudioTags_Substitues2 = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_AUDIO_SUBS_2, iPkgIndex);
		pDPkg->ss_Package_AudioRtn = dbmPkgs.getName(iPackager_router_main, DB_DESTINATION_AUDIO_RETURN, iPkgIndex);

		// check for main changes
		BOOL bAudioDstChange = FALSE, bAudioTagChange = FALSE, bAudioRetChange = FALSE;
		if (!AreBncsStringsEqual(pDPkg->ss_Package_AudioDests, ssPrev_AudioDests)) bAudioDstChange = TRUE;
		if (!AreBncsStringsEqual(pDPkg->ss_Package_AudioTags, ssPrev_AudioTags)) bAudioTagChange = TRUE;
		if (!AreBncsStringsEqual(pDPkg->ss_AudioTags_Substitues1, ssPrev_AudioSub1)) bAudioTagChange = TRUE;
		if (!AreBncsStringsEqual(pDPkg->ss_AudioTags_Substitues2, ssPrev_AudioSub2)) bAudioTagChange = TRUE;
		if (!AreBncsStringsEqual(pDPkg->ss_Package_AudioRtn, ssPrev_AudioRet)) bAudioRetChange = TRUE;

		// parse from db 5
		//Debug("ReloadDestPkg -2- db5");
		bncs_stringlist sslData = bncs_stringlist(pDPkg->ss_PackagerOwnerDelg, ',');
		if (sslData.count() > 0) {
			pDPkg->ss_PackageUniqueID = sslData.getNamedParam("id");
			pDPkg->m_iOwnerType = sslData.getNamedParam("owner").toInt();
			pDPkg->m_iDelegateType = sslData.getNamedParam("delegate").toInt();
		}

		// parse entries from db7 package mode and index
		//Debug("ReloadDestPkg -2- db7");
		int iRing = 0, iPort=0, iDataIndex = 0, iLangUseTag = 0, iHubTypeTag = 0;
		bncs_string ssdata = "";
		sslData = bncs_stringlist(pDPkg->ss_Package_Definition, ',');
		if (sslData.count() > 0) {
			// video
			ExtractRouterIndexTagData(sslData.getNamedParam("video"), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pDPkg->st_VideoHDestination.iAssocIndex = iDataIndex;
			pDPkg->st_VideoHDestination.iAssocHub_or_TypeTag = iHubTypeTag;
			if ((iDataIndex != iPrev_RouterDest) || (iHubTypeTag != iPrev_RouterTag)) {
				if (!bAutomaticStarting) Debug("ReloadDstPkg - routing change : %d to %d  tag %d to %d", iPrev_RouterDest, iDataIndex, iPrev_RouterTag, iHubTypeTag);
				bRoutingChanges = TRUE;
			}
			// anc1-4
			ExtractRouterIndexTagData(sslData.getNamedParam("anc1"), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pDPkg->st_VideoANC[1].iAssocIndex = iDataIndex;
			pDPkg->st_VideoANC[1].iAssocHub_or_TypeTag = iHubTypeTag;
			ExtractRouterIndexTagData(sslData.getNamedParam("anc2"), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pDPkg->st_VideoANC[2].iAssocIndex = iDataIndex;
			pDPkg->st_VideoANC[2].iAssocHub_or_TypeTag = iHubTypeTag;
			ExtractRouterIndexTagData(sslData.getNamedParam("anc3"), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pDPkg->st_VideoANC[3].iAssocIndex = iDataIndex;
			pDPkg->st_VideoANC[3].iAssocHub_or_TypeTag = iHubTypeTag;
			ExtractRouterIndexTagData(sslData.getNamedParam("anc4"), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pDPkg->st_VideoANC[4].iAssocIndex = iDataIndex;
			pDPkg->st_VideoANC[4].iAssocHub_or_TypeTag = iHubTypeTag;
			// reverse v
			ExtractRouterIndexTagData(sslData.getNamedParam("rev_vision"), &iDataIndex, &iHubTypeTag, &iLangUseTag);
			pDPkg->st_Reverse_Vision.iAssocIndex = iDataIndex;
			pDPkg->st_Reverse_Vision.iAssocHub_or_TypeTag = iHubTypeTag;
			// ifbs ports for INPUTS 
			GetRiedelRingAndPort(sslData.getNamedParam("ifb1"), &iRing, &iPort);
			pDPkg->st_AssocIFBs[1].i_mm_ring = iRing;
			pDPkg->st_AssocIFBs[1].i_mm_port = iPort;
			GetRiedelRingAndPort(sslData.getNamedParam("ifb2"), &iRing, &iPort);
			pDPkg->st_AssocIFBs[2].i_mm_ring = iRing;
			pDPkg->st_AssocIFBs[2].i_mm_port = iPort;
			// check for comms changes
			if (pDPkg->st_AssocIFBs[1].i_mm_ring != stPreDPIFBS[1].i_mm_ring) bCommsChanges = TRUE;
			if (pDPkg->st_AssocIFBs[1].i_mm_port != stPreDPIFBS[1].i_mm_port) bCommsChanges = TRUE;
			if (pDPkg->st_AssocIFBs[2].i_mm_ring != stPreDPIFBS[2].i_mm_ring) bCommsChanges = TRUE;
			if (pDPkg->st_AssocIFBs[2].i_mm_port != stPreDPIFBS[2].i_mm_port) bCommsChanges = TRUE;
		}

		// DB15
		sslData = bncs_stringlist(pDPkg->ss_Package_AudioDests, ',');
		for (int ii = 1; ii < MAX_PKG_AUDIO; ii++) pDPkg->st_AudioDestPair[ii].iAssocIndex = 0;
		if (sslData.count() > 0) {
			for (int ii = 0; ii<sslData.count(); ii++) {
				if (ii < 32) {
					// 1..32 entries ARE expected
					pDPkg->st_AudioDestPair[ii + 1].iAssocIndex = sslData[ii].toInt();
				}
			}
		}
		//DB16
		sslData = bncs_stringlist(pDPkg->ss_Package_AudioTags, ',');
		for (int iat = 1; iat < MAX_PKG_AUDIO; iat++) {
			pDPkg->st_AudioDestPair[iat].iAssocLang_or_UseTag = 0;
			pDPkg->st_AudioDestPair[iat].iAssocHub_or_TypeTag = 0;
		}
		if (sslData.count() > 0) {
			for (int ii = 0; ii<sslData.count(); ii++) {
				iLangUseTag = 0, iHubTypeTag = 0;
				if (ii < 32) { 
					// 1..32 entries ARE expected
					ExtractJustTagData(sslData[ii], &iLangUseTag, &iHubTypeTag);
					pDPkg->st_AudioDestPair[ii+1].iAssocLang_or_UseTag = iLangUseTag;
					pDPkg->st_AudioDestPair[ii+1].iAssocHub_or_TypeTag = iHubTypeTag;
				}
			}
		}
		// db 17 and 18 for substitutes audio tags -- completely mental and over the top
		sslData = bncs_stringlist(pDPkg->ss_AudioTags_Substitues1, ',');
		for (int isub = 1; isub <MAX_PKG_AUDIO; isub++) {
			pDPkg->st_AudioDestSubstitutes[isub].iSub_1_Lang_or_UseTag = 0;
			pDPkg->st_AudioDestSubstitutes[isub].iSub_1_Hub_or_TypeTag = 0;
			pDPkg->st_AudioDestSubstitutes[isub].iSub_2_Lang_or_UseTag = 0;
			pDPkg->st_AudioDestSubstitutes[isub].iSub_2_Hub_or_TypeTag = 0;
		}
		if (sslData.count() > 0) {
			for (int iisub1 = 0; iisub1<sslData.count(); iisub1++) {
				iLangUseTag = 0, iHubTypeTag = 0;
				if (iisub1 < 32) {
					// 1..32 entries possible
					ExtractJustTagData(sslData[iisub1], &iLangUseTag, &iHubTypeTag);
					pDPkg->st_AudioDestSubstitutes[iisub1 + 1].iSub_1_Lang_or_UseTag = iLangUseTag;
					pDPkg->st_AudioDestSubstitutes[iisub1 + 1].iSub_1_Hub_or_TypeTag = iHubTypeTag;
				}
			}
		}
		sslData = bncs_stringlist(pDPkg->ss_AudioTags_Substitues2, ',');
		if (sslData.count() > 0) {
			for (int iisub2 = 0; iisub2<sslData.count(); iisub2++) {
				iLangUseTag = 0, iHubTypeTag = 0;
				if (iisub2 < 32) {
					// 1..32 entries possible
					ExtractJustTagData(sslData[iisub2], &iLangUseTag, &iHubTypeTag);
					pDPkg->st_AudioDestSubstitutes[iisub2 + 1].iSub_2_Lang_or_UseTag = iLangUseTag;
					pDPkg->st_AudioDestSubstitutes[iisub2 + 1].iSub_2_Hub_or_TypeTag = iHubTypeTag;
				}
			}
		}
		// db19
		//Debug("ReloadDestPkg -2- db19");
		sslData = bncs_stringlist(pDPkg->ss_Package_AudioRtn, ',');
		for (int iiaret = 0; iiaret < 5; iiaret++) {
			pDPkg->st_AudioReturnPair[iiaret].iAssocIndex = 0;
			pDPkg->st_AudioReturnPair[iiaret].iAssocHub_or_TypeTag = 0;
			pDPkg->st_AudioReturnPair[iiaret].iAssocLang_or_UseTag = 0;
		}
		if (sslData.count() > 0) {
			ExtractRouterIndexTagData(sslData.getNamedParam("a1"), &iDataIndex, &iLangUseTag, &iHubTypeTag);
			pDPkg->st_AudioReturnPair[1].iAssocIndex = iDataIndex;
			pDPkg->st_AudioReturnPair[1].iAssocHub_or_TypeTag = iHubTypeTag;
			pDPkg->st_AudioReturnPair[1].iAssocLang_or_UseTag = iLangUseTag;
			ExtractRouterIndexTagData(sslData.getNamedParam("a2"), &iDataIndex, &iLangUseTag, &iHubTypeTag);
			pDPkg->st_AudioReturnPair[2].iAssocIndex = iDataIndex;
			pDPkg->st_AudioReturnPair[2].iAssocHub_or_TypeTag = iHubTypeTag;
			pDPkg->st_AudioReturnPair[2].iAssocLang_or_UseTag = iLangUseTag;
			// may only be two ?? 
			ExtractRouterIndexTagData(sslData.getNamedParam("a3"), &iDataIndex, &iLangUseTag, &iHubTypeTag);
			pDPkg->st_AudioReturnPair[3].iAssocIndex = iDataIndex;
			pDPkg->st_AudioReturnPair[3].iAssocHub_or_TypeTag = iHubTypeTag;
			pDPkg->st_AudioReturnPair[3].iAssocLang_or_UseTag = iLangUseTag;
			ExtractRouterIndexTagData(sslData.getNamedParam("a4"), &iDataIndex, &iLangUseTag, &iHubTypeTag);
			pDPkg->st_AudioReturnPair[4].iAssocIndex = iDataIndex;
			pDPkg->st_AudioReturnPair[4].iAssocHub_or_TypeTag = iHubTypeTag;
			pDPkg->st_AudioReturnPair[4].iAssocLang_or_UseTag = iLangUseTag;
		}

		bncs_stringlist ssll2 = bncs_stringlist(UpdateAllRoutersFor_DestPackageUsage(pDPkg, 1), ',');  // add dests  
		for (int ii = 0; ii < ssll2.count(); ii++) {
			if (ssl_RouterDestsUpdate.find(ssll2[ii])<0) ssl_RouterDestsUpdate.append(ssll2[ii]);
		}

		//if (pDPkg->ss_Package_Definition.length()>0)	Debug( "ReloadDestPkg - index %d updated sdi dest defn %s ", pDPkg->iPackageIndex, LPCSTR(pDPkg->ss_Package_Definition));
		// now with changed router destination fields 
		if (!bAutomaticStarting) {

			for (int ii = 0; ii < ssl_RouterDestsUpdate.count(); ii++) {
				UpdateRoutersDestUseInfodriverSlots(ssl_RouterDestsUpdate[ii].toInt());
			}

			// ptis of rtr dest in package and any 
			// verify "REAL"/traced source - as opposed to "routed" src pkg which may be virtual 
			ssl_Calc_Trace = bncs_stringlist("", '|');
			int iTraced = TraceRealSourcePackageCalcTrace(pDPkg->iPackageIndex);
			if (iTraced != pDPkg->i_Traced_Src_Package) {
				Debug("ReloadADestPkg -ERR- calc traced %d different from stored %d", iTraced, pDPkg->i_Traced_Src_Package);
				pDPkg->i_Traced_Src_Package = iTraced;
				if (ssl_Calc_Trace.count() == 0) ssl_Calc_Trace.append("0");
				pDPkg->ss_Trace_To_Source = ssl_Calc_Trace.toString('|');
			}

			if ((bReassertRouting || bRoutingChanges || bAudioDstChange || bAudioTagChange) && (pDPkg->iPackageLockState == 0)) {

				// Use TRACED source package - for correct sdi, audio srcs -- will be traced forward to virtuals src pkg 
				// this way checks if virtuals routed to this and they have an overriding video source defined 
				CSourcePackages* pTSPkg = GetSourcePackage(pDPkg->i_Traced_Src_Package);
				if (pTSPkg) {
					if (bRoutingChanges || bAudioDstChange || bAudioTagChange) { //	reassert routing 
						Debug("ReloadDestPkgData -1-");
						ProcessAllDestinationPackageRouting(pDPkg, 1, bReassertRouting);
					}
					else
						Debug("ReloadDestPkg -1- RM reassert routing - but no routing changes detected for dest pkg %d ", iPkgIndex);
				}
				else
					Debug("ReloadDestPkg ***ERROR*** no TRACED source pkg %d for dest pkg %d", pDPkg->i_Routed_Src_Package, iPkgIndex);

				// also if virtual dest - then check / go thru all routing for downstream dests FROM THIS virtual ,not from the traced source
				if ((iPkgIndex >= i_Start_VirtualPackages) && (iPkgIndex < (iNumberOfPackages+1))) {
					//
					CSourcePackages* pVirSPkg = GetSourcePackage(iPkgIndex);
					if (pVirSPkg) {
						// now get list of all downstream dests routed from this VIRTUAL package that may need routing changes
						if (bRoutingChanges || bAudioDstChange || bAudioTagChange)  {

							iRecursiveCounter = 0;
							ssl_RoutedDestPackages.clear();
							for (int i = 0; i < pVirSPkg->ssl_Pkg_DirectlyRoutedTo.count(); i++) {
								BuildDownStreamDestList(pVirSPkg->ssl_Pkg_DirectlyRoutedTo[i].toInt());
							}
							// now process thru this list of calc dest packages
							for (int iis = 0; iis < ssl_RoutedDestPackages.count(); iis++) {
								int iDestPkgIndx = ssl_RoutedDestPackages[iis].toInt();
								CDestinationPackages* pDestNext = GetDestinationPackage(iDestPkgIndx);
								if (pDestNext) {
									// video and audio - build list back to primary source
									Debug("ReloadDestPkgData -2- %d", iDestPkgIndx);
									ProcessAllDestinationPackageRouting(pDestNext, 1, bReassertRouting);
									if ((pDestNext->iPackageIndex > 0) && (pDestNext->iPackageIndex < i_Start_VirtualPackages) &&
										(pDestNext->i_Routed_Src_Package >= i_Start_VirtualPackages) && (pDestNext->iPackageIndex < (iNumberOfPackages+1))) {
										UpdateRoutedVirtualsVLevel(pDestNext->iPackageIndex, 0, pDestNext->i_Routed_Src_Package);
									}
								}
							} // for directly routed to
						}
						else
							Debug("ReloadDestPkg -2/vir- RM reassert routing - but no data changes detected for dest pkg %d ", iPkgIndex);
					}
				}

				/* NO LONGER PARK PREVIOUS reverse vision / audio -- as per Nigel W request */

				// if rev vision/audio changes and currently routed then check audio package for rev enabled and dest pkg top of MM Q - reroute as required
				if ((pDPkg->st_Reverse_Vision.iAssocIndex > 0) && (iPreviousRevVision != pDPkg->st_Reverse_Vision.iAssocIndex) &&(pDPkg->i_Reverse_Vision_Routed_AudioPackage>0)) {
					CAudioPackages* pAudio = GetAudioPackage(pDPkg->i_Reverse_Vision_Routed_AudioPackage);
					if (pAudio) {
						if ((pAudio->iEnabledRevVision > 0) && (pAudio->ssl_Pkg_Queue.count()>0)) {
							if (pAudio->ssl_Pkg_Queue[0].toInt() == pDPkg->iPackageIndex) {
								ProcessAudioPackageReverseRouting(pDPkg->i_Reverse_Vision_Routed_AudioPackage, 1 );
							}
						}
					}
				}

			} // if bReassert

			// changes for IFB mm ports 
			if (bCommsChanges) {
				// tidy MM CF ifb

				for (int iity = 1; iity < 3; iity++) {
					if ((stPreIFBS[iity].i_mm_port > 0) && (stPreIFBS[iity].i_mm_port != pDPkg->st_AssocIFBs[iity].i_mm_port))  {
						CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(stPreIFBS[iity].i_mm_ring);
						if (pRing) {
							TidyRiedelPortUsage(stPreIFBS[iity].i_mm_ring, stPreIFBS[iity].i_mm_port, PORT_USECODE_MMIFB, iPkgIndex);
						}
					}
				}
				CalculateDestinationMMRiedelPortUse(iPkgIndex, TRUE);

				// do some processing on IFBs - dynamics etc
				for (int iicc = 0; iicc < pDPkg->ssl_AtTopOfAudioPkgMMQ.count(); iicc++) {
					int iAPkg = pDPkg->ssl_AtTopOfAudioPkgMMQ[iicc].toInt();
					Debug("ReloadADestPkg - comms changes for top MMq for AP %d ", iAPkg);
					CAudioPackages* pAudio = GetAudioPackage(iAPkg);
					if (pAudio) {
						// reassert riedel ifb commands
						AssignDestinationIFBtoSrcIFBS(pAudio);
					}
				}
			}

			ProcessNextCommand(10);

			// go off and calc status now
			if (ssl_DestPkgs_For_SDIStatus_Calc.find(iPkgIndex) < 0) {
				ssl_DestPkgs_For_SDIStatus_Calc.append(iPkgIndex);
				if (iSDIStatusCalcCounter == 0) iSDIStatusCalcCounter = 2;
			}

		} // if !bAutoStarting and bReassertingRouting

		if (iPkgIndex == iChosenDestPkg) DisplayDestinationPackageData(FALSE);

		return TRUE;
	}
	else
		Debug("ReloadADestPackage -- invalid dest pkg class for index %d", iPkgIndex);
	return FALSE;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: LoadSourcePackageData()
//
//  COMMENTS:  Source package details 
//		
BOOL LoadSourcePackageData( void )
{
	for (int iIndex=1; iIndex<=iNumberOfPackages; iIndex++) {
		CSourcePackages* pSPkg  = new CSourcePackages;
		if (pSPkg&&mapOfAllSrcPackages) {
			pSPkg->iPackageIndex = iIndex;
			pSPkg->m_iAssociatedSportEvent = iIndex; // 1:1 association
			mapOfAllSrcPackages->insert(pair<int, CSourcePackages*>(iIndex, pSPkg));
			// now populate
			ReloadASourcePackageData(iIndex, FALSE);
		}
		else
			Debug( "LoadSrcPkg - record failed for src %d", iIndex);
	} // for 
	// add BARS and PARK to end of map for index 1000 and 1001 respectively
	CreateParkAndBarsSourcePackages();
	return TRUE;
	
}


BOOL LoadAudioPackageData()
{
	for (int iIndex = 1; iIndex<=iNumberOfPackages; iIndex++) {
		CAudioPackages* pSPkg = new CAudioPackages;
		if (pSPkg&&mapOfAllAudioPackages) {
			pSPkg->m_iAudioPkg_Indx = iIndex;
			mapOfAllAudioPackages->insert(pair<int, CAudioPackages*>(iIndex, pSPkg));
			// now populate
			ReloadAnAudioPackageData(iIndex, FALSE);
		}
		else
			Debug("LoadSrcPkg - record failed for src %d", iIndex);
	} // for 
	return TRUE;

}


BOOL LoadSportEventData()
{
	for (int iIndex = 1; iIndex<=iNumberOfPackages; iIndex++) {
		CSportEvent* pSPEV = new CSportEvent;
		if (pSPEV&&mapOfAllSportingEvents) {
			pSPEV->m_iKeyIndex = iIndex;
			pSPEV->m_iAssociated_SourcePackage = iIndex;
			mapOfAllSportingEvents->insert(pair<int, CSportEvent*>(iIndex, pSPEV));
			// link spev to src pkg 1:1
			CSourcePackages* pSrc = GetSourcePackage(iIndex);
			if (pSrc) pSrc->m_iAssociatedSportEvent = iIndex;
			// now populate
			ReloadSportingEventData(iIndex, FALSE);
		}
		else
			Debug("LoadSportEventData - record failed for src %d", iIndex);
	} // for 
	return TRUE;

}



//////////////////////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: LoadDestinationPackageData()
//
//  COMMENTS:  Destination package details 
//		
BOOL LoadDestinationPackageData( void )
{
	for (int iIndex=1;iIndex<=iNumberOfPackages; iIndex++) {
		CDestinationPackages* pDPkg  = new CDestinationPackages;
		if (pDPkg&&mapOfAllDestPackages) {
			pDPkg->iPackageIndex = iIndex;
			// insert into map
			mapOfAllDestPackages->insert(pair<int, CDestinationPackages*>(iIndex, pDPkg));
			// get dest pkg data
			ReloadADestinationPackageData( iIndex, FALSE );
		}
		else 
			Debug( "LoadDestPkg - record failed for dest %d", iIndex);
	}
	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////

int getRMstrAutoInfodriver(bncs_string ssInstance)
{
	int iDev = getInstanceDevice(ssInstance);
	if (iDev > 0) {
		Debug("getAutoInfodriver -element %s == device %d", LPCSTR(ssInstance), iDev);
		return iDev;
	}
	Debug("getRMstrAutoInfodriver ERROR -instance %s NO device - AUTO STARTUP WILL FAIL", LPCSTR(ssInstance));
	return -1;
}


BOOL LoadRingMasterDeviceIds()
{
	// uses composite now - get composite required from packager config
	bncs_string ssRmstrComposite = getXMLItemValue("packager_config", ssCompositeInstance, "ring_master_auto");
	if (ssRmstrComposite.length() > 0) {
		bncs_stringlist sslcomp = bncs_stringlist(GetCompositeInstance(ssRmstrComposite), ',');
		if (sslcomp.count() > 0) {
			// local ringmaster instances
			i_RingMaster_Main = getRMstrAutoInfodriver(sslcomp.getNamedParam("main"));
			i_RingMaster_Conf = getRMstrAutoInfodriver(sslcomp.getNamedParam("conferences"));
			i_RingMaster_Ifb_Start = getRMstrAutoInfodriver(sslcomp.getNamedParam("ifbs_1"));
			i_RingMaster_Ports_Start = getRMstrAutoInfodriver(sslcomp.getNamedParam("ports_1"));
			i_RingMaster_Ifb_End = getRMstrAutoInfodriver(sslcomp.getNamedParam(bncs_string("ifbs_%1").arg(iNumberRiedelRings)));
			i_RingMaster_Ports_End = getRMstrAutoInfodriver(sslcomp.getNamedParam(bncs_string("ports_%1").arg(iNumberRiedelRings)));
			Debug("LoadRMSTR Device IDs main %d  conf %d", i_RingMaster_Main, i_RingMaster_Conf);
			Debug("LoadRMSTR Device IDs ifb start info %d  end info %d", i_RingMaster_Ifb_Start, i_RingMaster_Ifb_End);
			Debug("LoadRMSTR Device IDs ports start info %d  end info %d", i_RingMaster_Ports_Start, i_RingMaster_Ports_End);
		}
		// load other side Rmstr - main and conference only 
		bncs_string ssRmstrRemote = getXMLItemValue("packager_config", ssCompositeInstance, "other_site_ring_master");
		if (ssRmstrRemote.length() > 0) {
			bncs_stringlist sslremote = bncs_stringlist(GetCompositeInstance(ssRmstrRemote), ',');
			if (sslremote.count() > 0) {
				// remote ringmaster instances
				iRemote_RingMaster_Main = getRMstrAutoInfodriver(sslremote.getNamedParam("main"));
				iRemote_RingMaster_Conf = getRMstrAutoInfodriver(sslremote.getNamedParam("conferences"));
				Debug("LoadRMSTR REMOTE Device IDs main %d  conf %d", iRemote_RingMaster_Main, iRemote_RingMaster_Conf);
			}
		}

	}
	if ((i_RingMaster_Main > 0) && (i_RingMaster_Conf > 0) && (i_RingMaster_Ifb_Start > 0) && (i_RingMaster_Ports_Start > 0)
		&& (i_RingMaster_Ifb_End > 0) && (i_RingMaster_Ports_End > 0))
		return TRUE;
	else
		Debug("LoadRingMasterDeviceIds -ERROR- check RMSTR config/instances are not determined ");
	return FALSE;
}



int LoadRiedelRings()
{
	// load all defined rings from config xnl file
	bncs_string ssXmlConfigFile = "ring_master_config";
	int iEntry = 0;
	BOOL bCont = TRUE;
	do {
		iEntry++;
		bncs_string ssEntry = bncs_string("Riedel_%1").arg(iEntry);
		bncs_stringlist sslData = bncs_stringlist(getXMLItemValue(ssXmlConfigFile, "RIEDEL_SYSTEMS", ssEntry), ',');
		if (sslData.count()>1) {
			int iTrunkAddress = sslData.getNamedParam("trunk_address").toInt();
			bncs_string ssRingId = sslData.getNamedParam("name");
			bncs_string ssBaseId = sslData.getNamedParam("base_instance");
			Debug("LoadRiedelRing base inst %s name %s trunk address %d", LPCSTR(ssBaseId), LPCSTR(ssRingId), iTrunkAddress);
			if ((ssBaseId.length() > 0) && (iTrunkAddress > 0)) {
				// local or remote ring for discovery
				int iRingType = 0;
				if (ssBaseId.find("lond") >= 0) iRingType = LDC_LOCALE_RING;
				if (ssBaseId.find("hilv") >= 0) iRingType = NHN_LOCALE_RING;
				CRiedel_Rings* pRing = new CRiedel_Rings(iEntry, iTrunkAddress, iRingType, ssRingId);
				if (pRing) {
					// load from composite 
					bncs_stringlist sslcomp = bncs_stringlist(GetCompositeInstance(ssBaseId), ',');
					//
					pRing->ssRing_Instance_GRD = sslcomp.getNamedParam("grd");
					pRing->ssRing_Instance_Monitor = sslcomp.getNamedParam("monitors");
					pRing->ssRing_Instance_Conference = sslcomp.getNamedParam("conference");
					pRing->ssRing_Instance_IFB = sslcomp.getNamedParam("ifbs");
					pRing->ssRing_Instance_Extras = sslcomp.getNamedParam("extras");
					//
					pRing->iRing_Device_GRD = getInstanceDevice(pRing->ssRing_Instance_GRD);
					pRing->iRing_Device_Monitor = getInstanceDevice(pRing->ssRing_Instance_Monitor);
					pRing->iRing_Device_Conference = getInstanceDevice(pRing->ssRing_Instance_Conference);
					pRing->iRing_Device_IFB = getInstanceDevice(pRing->ssRing_Instance_IFB);
					pRing->iRing_Device_Extras = getInstanceDevice(pRing->ssRing_Instance_Extras);
					//
					int iDev = 0, iSrc = 0, iDest = 0;
					GetRouterDeviceDBSizes(pRing->ssRing_Instance_GRD, &iDev, &iSrc, &iDest);
					pRing->iRing_Ports_Talk = iSrc;
					pRing->iRing_Ports_Listen = iDest;
					// add to map
					cl_Riedel_Rings->insert(pair<int, CRiedel_Rings*>(iEntry, pRing));
					//
					Debug("LoadRiedelRing - %d %d %s - grd %d src %d dest %d", iEntry, iTrunkAddress, LPCSTR(ssRingId), pRing->iRing_Device_GRD, pRing->iRing_Ports_Talk, pRing->iRing_Ports_Listen);

					// set up Monitor Port Key record data 
					int iNumEntries = 0;
					if (pRing->iRing_Device_Monitor > 0) iNumEntries = CreateRiedel_PortKeys(pRing, DATABASE_RIEDEL_PORT_TYPE);
					Debug("LoadRiedelRings - ring %d - Monitor Port Keys - entries loaded %d ", pRing->m_iTrunkAddress, iNumEntries);

				}
				else
					Debug("LoadRiedelRing -ERROR- IFB dev zero for ring %d trunk %d %s", iEntry, iTrunkAddress, LPCSTR(ssRingId));
			}
			else
				bCont = FALSE;
		}
		else
			bCont = FALSE;
	} while (bCont);
	iEntry--;
	return iEntry;

}

int LoadInRingPortsTable()
{
	bncs_stringlist ssl_rmp_inst = bncs_stringlist("", ',');
	bncs_stringlist ssl_rmp_dev = bncs_stringlist("", ',');
	//
	bncs_string ssFileName = bncs_string("%1.%2").arg("comms_ringmaster_outputs").arg("outputs");
	int iNdx = 0;
	bncs_config cfg(ssFileName);
	Debug("getXMLTable - file : %s", LPCSTR(ssFileName));
	while (cfg.isChildValid())  {
		iNdx++;
		CRingMaster_Ports* pDefn = new CRingMaster_Ports();
		if (pDefn) {
			pDefn->iRingTrunk = cfg.childAttr("ring").toInt();
			pDefn->iRingPort = cfg.childAttr("port").toInt();
			bncs_string ssInst = cfg.childAttr("instance");
			pDefn->iPortsInfodrvSlot = cfg.childAttr("slot_index");
			// cache instances found / required for faster loading
			int iPos = ssl_rmp_inst.find(ssInst);
			if ((iPos >= 0)&&(iPos<ssl_rmp_dev.count())) {
				// instance found from earlier load - use device
				pDefn->iPortsInfodrv = ssl_rmp_dev[iPos].toInt();
			}
			else {
				pDefn->iPortsInfodrv = getInstanceDevice(ssInst);
				ssl_rmp_inst.append(ssInst);
				ssl_rmp_dev.append(bncs_string(pDefn->iPortsInfodrv));
				Debug("LoadInRingPortsTable instance %s  ==  device %d  at i %d  d %d", LPCSTR(ssInst), pDefn->iPortsInfodrv, ssl_rmp_inst.count(), ssl_rmp_dev.count());
			}
			cl_RingMaster_Ports->insert(pair<int, CRingMaster_Ports*>(iNdx, pDefn));
		}
		else
			Debug("LoadInRingPortsTable - ERROR - in creating class for index %d", iNdx);
		cfg.nextChild();				 // go on to the next sibling (if there is one)
	}
	Debug("LoadInRingPortsTable - %d items from table", iNdx);
	return iNdx;
}

/**********************************************
void 	LoadIFBCommsMappingXml()
{
	bncs_string ssfileName = "comms_mapping_ifb";
	for (int iifb = 1; iifb <= MAX_IFBS; iifb++) {
		// is there an entry in
		CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iifb);
		if (pIFB)  {
			// get expected entries from file -- if comms delim list of numbers or a range specified by dash - then  further mods required
			bncs_string ssSection = bncs_string("%1").arg(iifb);
			int iInputPort = getXMLItemValue(ssfileName, ssSection, "ifb_input_port").toInt();
			int iOutputPort = getXMLItemValue(ssfileName, ssSection, "ifb_output_port").toInt();
			int iMixMinus = getXMLItemValue(ssfileName, ssSection, "ifb_mix_minus_port").toInt();
			int iAssocRing = getXMLItemValue(ssfileName, ssSection, "ifb_associated_ring").toInt();
			int iAssocLogic = getXMLItemValue(ssfileName, ssSection, "ifb_associated_logic").toInt();
			int iAssocDestPkg = getXMLItemValue(ssfileName, ssSection, "ifb_associated_dest_package").toInt();
			bncs_string ssAssocPkgMode = getXMLItemValue(ssfileName, ssSection, "ifb_mode").upper();
			int iPkgMode = IFB_MODE_PRIMARY_LEFT;
			if (ssAssocPkgMode.find("RIGHT") >= 0) iPkgMode = IFB_MODE_SECONDARY_RIGHT;
			// store values found
			if (iInputPort > 0) pIFB->setAssociatedInputPort(iInputPort);
			if (iOutputPort > 0) pIFB->setAssociatedOutputPort(iOutputPort);
			if (iMixMinus > 0) pIFB->setAssociatedMixMinusPort(iMixMinus);
			if (iAssocRing > 0) pIFB->setAssignedTrunkAddr(iAssocRing);
			if (iAssocLogic > 0) {
				// store in ifb
				pIFB->setAssociatedRemoteLogic(iAssocLogic);
				// link IFB and logic records here
				CRingMaster_LOGIC* pLogic = GetRingMaster_LOGIC_Record(iAssocLogic);
				if (pLogic) {
					pLogic->setAssociatedIFB(iifb);
					pLogic->setAssociatedListenToPort(pIFB->getAssociatedInputPort());
					pLogic->setAssociatedTrunkAddress(pIFB->getAssignedTrunkAddr());
				}
				else
					Debug("LoadIFBCommsMappingXml - no LOGIC record founf for IFB %d ", iifb);
			}
			if ((iAssocDestPkg > 0)&&(iAssocDestPkg<=iNumberOfPackages)) {
				pIFB->setAssociatedDestinationPackage(iAssocDestPkg, iPkgMode);
				CDestinationPackages* pDest = GetDestinationPackage(iAssocDestPkg);
				if (pDest) {
					if (iPkgMode == IFB_MODE_SECONDARY_RIGHT)
						pDest->st_AssocIFBs[2].i_DynAssignedIfb = iifb;
					else
						pDest->st_AssocIFBs[1].i_DynAssignedIfb = iifb;
				}
			}
		}

	}// for
}
******************************************/

BOOL ReloadAPackageTag(int iTagDB, int iTagIndex, bncs_string ssData )
{
	if (mapOfAllTags) {
		//
		bncs_stringlist ssl = bncs_stringlist(ssData, '|');
		if ((ssData.length()>1) && (iTagIndex != ssData.toInt()) && (ssl.count()>0)) {
			CPackageTags* pTag = GetPackageTag(iTagDB, iTagIndex);
			if (!pTag) {
				pTag = new CPackageTags();  // doesnt exist so create
				iTotalNumberTags++;
			}
			if (pTag) {
				pTag->m_bTagInUse = TRUE;
				pTag->m_iTagDBType = iTagDB;
				pTag->m_iTagDBIndex = iTagIndex;
				pTag->m_ssTag_AbrevName = ssl[0];   // first field in list - always at least 1 entry - abreviated code or full name
				pTag->m_ssTag_DescString = ssl[0];   // copy here as default- always at least 1 entry
				if ((ssl.count()>1) && (ssl[1].find("INACTIVE")<0)) pTag->m_ssTag_DescString = ssl[1];  // if 2nd entry - longer string and not the inactive flag
				if (ssData.find("INACTIVE") >= 0) pTag->m_bTagInUse = FALSE;   // if word inactive in the entry from db
				// add to map
				mapOfAllTags->insert(pair<int, CPackageTags*>(iTotalNumberTags, pTag));
			}
		}
		return TRUE;
	}
	else
		Debug("ReloadTag - no tag map");
	return FALSE;
}

BOOL LoadAllPackageTags()
{
	iTotalNumberTags = 0;
	mapOfDBFile = new map<int, CCommand*>;
	if (mapOfAllTags&&mapOfDBFile) {
		// load from db all the tags and their types 
		for (int iiType = DB_TAGS_VIDEO; iiType <= DB_TAGS_RESV_2; iiType++) {
			int iTagdbCount = 0;
			int iHighestEntry = LoadDBFile(iPackager_router_main, iiType, 650);
			Debug("LoadTags load file == Highest entry %d from LoadDBfile %d ", iHighestEntry, iiType);
			for (int iiIndx = 1; iiIndx < iHighestEntry; iiIndx++) {
				char szRMData[MAX_AUTO_BUFFER_STRING] = "";
				CCommand* pRec = GetDBFileRecord(iiIndx);
				if (pRec) {
					dbmPkgs.setName(iPackager_router_main, iiType, iiIndx, pRec->szCommand, false);
					if (ReloadAPackageTag(iiType, iiIndx, bncs_string(pRec->szCommand))) iTagdbCount++;
				}
			} // for iindx
			Debug("LoadTags stored %d tags from DB %d ", iTagdbCount, iiType);
			// clear out map
			while (mapOfDBFile->begin() != mapOfDBFile->end()) {
				map<int, CCommand*>::iterator it = mapOfDBFile->begin();
				if (it->second) delete it->second;
				mapOfDBFile->erase(it);
			}
		} // for iitype
		// delete map
		delete mapOfDBFile;
		SetDlgItemInt(hWndDlg, IDC_AUTO_DBTAGS, iTotalNumberTags, TRUE);
		return TRUE;
	}
	Debug("LoadTags ERROR no tags or DBFile class map");
	return FALSE;
}


void LoadDynamicConferenceDBData()
{
	// step 1 - load local file
	BOOL bAssignedLocally[MAX_CONFERENCES+2];
	int iSyncDBCount = 0, iOtherDBCount = 0;
	// load in db file into map
	mapOfDBFile = new map<int, CCommand*>;
	if (mapOfDBFile) {

		LoadDBFile(i_rtr_Infodrivers[1], DB_SYNC_DYN_CONF, MAX_CONFERENCES);

		for (int iConf = 1; iConf <= MAX_CONFERENCES; iConf++) {
			bAssignedLocally[iConf] = FALSE;
			CRingMaster_CONF* pRmstrConf = GetRingMaster_CONF_Record(iConf);
			if (pRmstrConf) {
				// get sync rm data -- deemed to be definitive over anything in infodriver slot - provided there are values therein
				CCommand* pRec = GetDBFileRecord(iConf);
				if (pRec) {
					bncs_string ssRMInData = bncs_string(pRec->szCommand);
					if ((ssRMInData.length() > 0) && (ssRMInData.find("NULL") < 0)) {
						if ((ssRMInData.find("audpkg_index") >= 0) && (ssRMInData.find("which_conf") >= 0)) {
							bncs_stringlist ssl = bncs_stringlist(bncs_string(ssRMInData), ',');
							int iPkg = 0, i4W = 0, iUseType = 0;
							if (ssl.count() > 1) {
								// ie not an empty string, or just a string of the index number   0001=0001 etc
								iPkg = ssl.getNamedParam("audpkg_index").toInt();
								i4W = ssl.getNamedParam("which_conf").toInt();
								iUseType = ssl.getNamedParam("use_type").toInt();
								if ((iPkg > 0) && (i4W > 0) && (i4W < MAX_PKG_CONFIFB) && (iUseType >= DYN_IFB_4WCONF_1)) {
									pRmstrConf->SetDynamicAudioPackage(iPkg, i4W, iUseType);
									bAssignedLocally[iConf] = TRUE;
									iSyncDBCount++;
									// get AP - assign there too
									CAudioPackages* pAud = GetAudioPackage(iPkg);
									if (pAud) {
										if ((i4W > 0) && (i4W < MAX_PKG_CONFIFB)) {
											if (iUseType >= DYN_TB_CONF_1)
												pAud->stTBConferences[i4W].i_DynAssignedConference = iConf;
											else
												pAud->stCFIFBs[i4W].i_DynAssignedConference = iConf;
										}
									}
									else
										Debug("LoadAllInit -DynConfs ERROR- no Audio Package %d for conf %d", iPkg, iConf);
								}
							}
						}
					}
				}
			}
		} // for iconf
		// clear db map load as finished with it now
		while (mapOfDBFile->begin() != mapOfDBFile->end()) {
			map<int, CCommand*>::iterator it = mapOfDBFile->begin();
			if (it->second) delete it->second;
			mapOfDBFile->erase(it);
		}

		if ((iOtherSite_Pkgr_router_main > 0) && (mapOfDBFile)) {

			LoadDBFile(iOtherSite_Pkgr_router_main, DB_SYNC_DYN_CONF, MAX_CONFERENCES);

			for (int iConf = 1; iConf <= MAX_CONFERENCES; iConf++) {
				CRingMaster_CONF* pRmstrConf = GetRingMaster_CONF_Record(iConf);
				if (pRmstrConf) {
					// get sync rm data -- deemed to be definitive over anything in infodriver slot - provided there are values therein
					CCommand* pRec = GetDBFileRecord(iConf);
					if (pRec) {
						bncs_string ssOtherRMData = bncs_string(pRec->szCommand);
						if ((ssOtherRMData.find("audpkg_index") >= 0) && (ssOtherRMData.find("which_conf") >= 0)) {
							bncs_stringlist ssl = bncs_stringlist(bncs_string(ssOtherRMData), ',');
							int iPkg = 0, i4W = 0, iUseType = 0;
							if (ssl.count() > 1) {
								// ie not an empty string, or just a string of the index number   0001=0001 etc
								iPkg = ssl.getNamedParam("audpkg_index").toInt();
								i4W = ssl.getNamedParam("which_conf").toInt();
								iUseType = ssl.getNamedParam("use_type").toInt();
								if ((iPkg > 0) && (i4W > 0) && (iUseType >= 0)) {
									if (!bAssignedLocally[iConf]) {
										pRmstrConf->SetDynamicAudioPackage(OTHER_PKGR_ASSIGNED, OTHER_PKGR_USAGE, OTHER_PKGR_USAGE); // means conf is in use on other side
										iOtherDBCount++;
									}
									else
										Debug("LoadAllInit - DynConfs ERROR- conf %d assigned locally and remotely - potential clash ", iConf);
								}
							}
						}
					}
				}
			} // for

			// clear db map load as finished with it now
			while (mapOfDBFile->begin() != mapOfDBFile->end()) {
				map<int, CCommand*>::iterator it = mapOfDBFile->begin();
				if (it->second) delete it->second;
				mapOfDBFile->erase(it);
			}
		}
		// now delete memory allocation
		delete mapOfDBFile;

	}
	Debug("LoadAllInit - Conference DB 104 SYNC -- assigned conferences - LOCAL %d  |  REMOTE %d ", iSyncDBCount, iOtherDBCount);

}


void LoadDynamicIFBDBData()
{
	// step 1 - load local file
	BOOL bAssignedLocally[6002];
	int iMaxIFBs = (iNumberRiedelRings*MAX_IFBS);
	int iSyncDBCount = 0, iOtherDBCount = 0;
	// load in db file into map
	mapOfDBFile = new map<int, CCommand*>;
	if (mapOfDBFile) {

		// LOAD LOCAL Dyn IFB assignments here from db 105
		LoadDBFile(i_rtr_Infodrivers[1], DB_SYNC_DYN_IFBS, iMaxIFBs);

		for (int iPkgrIFB = 1; iPkgrIFB <= iMaxIFBs; iPkgrIFB++) {
			CRingMaster_IFB* pRMIFB = GetRingMaster_IFB_Record(iPkgrIFB);
			if (pRMIFB) {
				bAssignedLocally[iPkgrIFB] = FALSE;
				// get sync rm data -- deemed to be definitive over anything in infodriver slot - provided there are values therein
				CCommand* pRec = GetDBFileRecord(iPkgrIFB);
				if (pRec) {
					bncs_string ssRMInData = bncs_string(pRec->szCommand);
					if ((ssRMInData.find("audpkg_index") >= 0) && (ssRMInData.find("which_ifb") >= 0)) {
						bncs_stringlist ssl = bncs_stringlist(bncs_string(ssRMInData), ',');
						int iPkg = 0, iWhichIfb = 0, iUseType = 0;
						if (ssl.count() > 1) {
							// ie not an empty string, or just a string of the index number   0001=0001 etc
							iPkg = ssl.getNamedParam("audpkg_index").toInt();
							iWhichIfb = ssl.getNamedParam("which_ifb").toInt();
							iUseType = ssl.getNamedParam("use_type").toInt();
							if ((iPkg > 0) && (iWhichIfb > 0) && (iWhichIfb < MAX_PKG_CONFIFB)) {
								pRMIFB->setAssociatedAudioPackage(iPkg, iUseType);
								bAssignedLocally[iPkgrIFB] = TRUE;
								iSyncDBCount++;
								// get AP - assign there too
								CAudioPackages* pAud = GetAudioPackage(iPkg);
								if (pAud) {
									if ((iWhichIfb > 0) && (iWhichIfb < MAX_PKG_CONFIFB)) pAud->stCFIFBs[iWhichIfb].i_DynAssignedIfb = iPkgrIFB;
								}
								else
									Debug("LoadAllInit -DynIFBS ERROR- no Audio Package %d for pkgrIFB %d", iPkg, iPkgrIFB);
							}
						}
					}
				}
			}
		} // for iPkgrIfb
		// clear db map load as finished with it now
		while (mapOfDBFile->begin() != mapOfDBFile->end()) {
			map<int, CCommand*>::iterator it = mapOfDBFile->begin();
			if (it->second) delete it->second;
			mapOfDBFile->erase(it);
		}

		// check other side packager's db 105 for ifb use
		if ((iOtherSite_Pkgr_router_main > 0) && (mapOfDBFile)) {
			LoadDBFile(iOtherSite_Pkgr_router_main, DB_SYNC_DYN_IFBS, iMaxIFBs);

			for (int iPkgrIFB = 1; iPkgrIFB <= iMaxIFBs; iPkgrIFB++) {
				CRingMaster_IFB* pRMIFB = GetRingMaster_IFB_Record(iPkgrIFB);
				if (pRMIFB) {
					CCommand* pRec = GetDBFileRecord(iPkgrIFB);
					if (pRec) {
						bncs_string ssOtherRMData = bncs_string(pRec->szCommand);
						if ((ssOtherRMData.find("audpkg_index") >= 0) && (ssOtherRMData.find("which_ifb") >= 0)) {
							bncs_stringlist ssl = bncs_stringlist(bncs_string(ssOtherRMData), ',');
							int iPkg = 0, iWhichIfb = 0, iUseType = 0;
							if (ssl.count() > 1) {
								// ie not an empty string, or just a string of the index number   0001=0001 etc
								iPkg = ssl.getNamedParam("audpkg_index").toInt();
								iWhichIfb = ssl.getNamedParam("which_ifb").toInt();
								iUseType = ssl.getNamedParam("use_type").toInt();
								if ((iPkg > 0) && (iWhichIfb > 0) && (iWhichIfb < MAX_PKG_CONFIFB)) {
									if (!bAssignedLocally) {
										pRMIFB->setAssociatedAudioPackage(OTHER_PKGR_ASSIGNED, OTHER_PKGR_USAGE);
										iOtherDBCount++;
									}
									else
										Debug("LoadAllInit - DynIfbs ERROR- (pkgrIFB %d) assigned locally and remotely - potential clash ", iPkgrIFB);
								}
							}
						}
					}
				}
			} // for iifb
			// clear db map load as finished with it now
			while (mapOfDBFile->begin() != mapOfDBFile->end()) {
				map<int, CCommand*>::iterator it = mapOfDBFile->begin();
				if (it->second) delete it->second;
				mapOfDBFile->erase(it);
			}
		}
		// now delete memory allocation
		delete mapOfDBFile;
	}
	Debug("LoadAllInit - Dynamic IFB DB 105 SYNC -- assigned ifbs  LOCAL %d  |  REMOTE %d ", iSyncDBCount, iOtherDBCount);
}


int GetIPRouterInfodriverDividerSetting(int iRtrDev)
{
	int iRetValue = 0;
	if (iRtrDev > 0) {
		char szFileName[32];
		wsprintf(szFileName, "DEV_%03d.ini", iRtrDev);
		iRetValue = atoi(r_p(szFileName, "IP_Router", "Infodriver_Divider", "-1", FALSE));
	}
	return iRetValue;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: LoadAllInitialisationData()
//
//  PURPOSE: Function called on startup to load data from ini files / object settings etc relevant for driver
//                 creates new memory instances/
//
BOOL LoadAllInitialisationData( void ) 
{
	Debug("into Load All init data");

	for (int iifc = 0; iifc < MAX_PKG_CONFIFB; iifc++) {
		stPreIFBS[iifc].i_in_ring = 0;
		stPreIFBS[iifc].i_in_port = 0;
		stPreIFBS[iifc].i_mm_ring = 0;
		stPreIFBS[iifc].i_mm_port = 0;
		stPreIFBS[iifc].i_out_ring = 0;
		stPreIFBS[iifc].i_out_port = 0;
		stPreIFBS[iifc].m_ss_MainLabel = "";
		stPreIFBS[iifc].m_ss_SubTitleLabel = "";
		stPreIFBS[iifc].i_Use_Which_DestPkgIFB = 0;
		stPreDPIFBS[iifc].i_in_ring = 0;
		stPreDPIFBS[iifc].i_in_port = 0;
		stPreDPIFBS[iifc].i_mm_ring = 0;
		stPreDPIFBS[iifc].i_mm_port = 0;
		stPreDPIFBS[iifc].i_out_ring = 0;
		stPreDPIFBS[iifc].i_out_port = 0;
		stPreDPIFBS[iifc].m_ss_MainLabel = "";
		stPreDPIFBS[iifc].m_ss_SubTitleLabel = "";
		stPreDPIFBS[iifc].i_Use_Which_DestPkgIFB = 0;
		stPreTBConfs[iifc].i_in_ring = 0;
		stPreTBConfs[iifc].i_in_port = 0;
		stPreTBConfs[iifc].i_mm_ring = 0;
		stPreTBConfs[iifc].i_mm_port = 0;
		stPreTBConfs[iifc].i_out_ring = 0;
		stPreTBConfs[iifc].i_out_port = 0;
		stPreTBConfs[iifc].m_ss_MainLabel = "";
		stPreTBConfs[iifc].m_ss_SubTitleLabel = "";
		stPreTBConfs[iifc].i_Use_Which_DestPkgIFB = 0;
	}

	char szEntry[256]="";
	int iRouterDev = 0;
	int iMaxSrcs = 0, iMaxDests = 0;
	bncs_string ssXmlConfigFile = "packager_config";
	bncs_string strValue = "", strIdName = "", ssData = "";
	bSendingOutDynConfData = FALSE;
	sslSendingOutDynConfData = bncs_stringlist("", '|');
	bSendingOutDynIFBData = FALSE;
	sslSendingOutDynIFBData = bncs_stringlist("", '|');
	ssl_Calc_Trace = bncs_stringlist("", '|');
	ssl_RoutedDestPackages = bncs_stringlist("", ',');
	iOtherSite_Pkgr_router_main = 0;
	iRecursiveCounter = 0;
	iIP_RouterANC = 0;
	iIP_RouterVideoHD = 0;
	iIP_RouterAudio2CH = 0;
	iIP_RouterJpegXS = 0;
	iIP_RouterVideoUHD = 0;
	iIP_RouterInfoDivider = 4096;  // unles set in ip_router or nevion router dev ini to be otherwise 
	// create maps 
	cl_RouterRecords = new map<int, CRevsRouterData*>;
	if (!cl_RouterRecords) {
		Debug("LoadInit -- created  ROUTER CLASSES FAILED - AUTO STOPPING ");
		return FALSE;
	}

	mapOfAllTags = new map<int, CPackageTags*>;
	if (!LoadAllPackageTags()) {
		Debug("LoadInit -- created  TAGS CLASSES FAILED - AUTO STOPPING ");
		return FALSE;
	}

	cl_Riedel_Rings = new map<int, CRiedel_Rings*>;
	cl_RingMaster_IFBs = new map<int, CRingMaster_IFB*>;
	cl_RingMaster_Ports = new map<int, CRingMaster_Ports*>;
	cl_RingMaster_Logic = new map<int, CRingMaster_LOGIC*>;
	cl_RingMaster_Conferences = new map<int, CRingMaster_CONF*>;
	if ((!cl_Riedel_Rings) || (!cl_RingMaster_IFBs) || (!cl_RingMaster_Ports) || (!cl_RingMaster_Conferences) || (!cl_RingMaster_Logic)) {
		Debug("LoadInit -- some part of  RING MASTER CLASS creation FAILED - AUTO STOPPING ");
		return FALSE;
	}
	//

	i_Start_VirtualPackages = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "first_virtual_package").toInt();
	if ((i_Start_VirtualPackages == 0) || (i_Start_VirtualPackages>(iNumberOfPackages+1)))
		Debug("loadini - NO VIRTUAL PACKAGES DEFINED");
	else
		Debug("loadini - Virtual Packages start at %d ", i_Start_VirtualPackages);
	SetDlgItemInt(hWndDlg, IDC_AUTO_VIRTUALS, i_Start_VirtualPackages, TRUE);

	// get control infodrivers
	iAutomatics_CtrlInfo_Local = 0;
	iAutomatics_CtrlInfo_Remote = 0;
	iAutomatics_CtrlInfo_Local = getInstanceDevice(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "automatics_control_local"));
	iAutomatics_CtrlInfo_Remote = getInstanceDevice(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "automatics_control_remote"));
	SetDlgItemInt(hWndDlg, IDC_AUTO_CTRLINFO, iAutomatics_CtrlInfo_Local, TRUE);
	SetDlgItemInt(hWndDlg, IDC_AUTO_CTRLINFO2, iAutomatics_CtrlInfo_Remote, TRUE);

	// get other side packager
	iOtherSite_Pkgr_router_main = 0;
	iOtherSite_Pkgr_router_main = getInstanceDevice(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "other_site_pkgr"));
	Debug("LoadAllInit - OTHER PACKAGER device %d", iOtherSite_Pkgr_router_main);

	iControlStatus = 0;
	iPrevCtrlStatus = -1;
	iCtrlStatusCalcCounter = 33;      
	iLDC_TXRX_TicTocStatus = 0;
	iLDC_RXONLY_TicTocStatus = 0;
	iNHN_TXRX_TicTocStatus = 0;
	iNHN_RXONLY_TicTocStatus=0;
	ssLDC_TX_Revertive = "x"; 
	ssLDC_RX_Revertive = "x";
	ssNHN_TX_Revertive = "x";  
	ssNHN_RX_Revertive = "x";

	Debug("LoadAllInitData  - loading Ringmaster data more");
	// set up some base records
	Debug("LoadAllInitData  - loading default data");
	iNumberRiedelRings = 0;
	i_RingMaster_Main = 0;
	i_RingMaster_Conf = 0;
	i_RingMaster_Ifb_Start = 0;
	i_RingMaster_Ifb_End = 0;
	i_RingMaster_Ports_Start = 0;
	i_RingMaster_Ports_End = 0;
	iRemote_RingMaster_Main = 0;
	iRemote_RingMaster_Conf = 0;

	// now Load Riedel Rings, setting up ringmaster rev and riedel rev records
	iNumberRiedelRings = LoadRiedelRings();
	//
	if (!LoadRingMasterDeviceIds()) {
		Debug("LoadAllInit -ERROR- Fail to determine RINGMASTER auto dev IDs - exiting init");
		return FALSE;
	}

	int iNumberTableEntries = LoadInRingPortsTable();

	Debug("Load All init data - conf ");
	for (int iConf = 1; iConf <= MAX_CONFERENCES; iConf++) {
		CRingMaster_CONF* pRmstrConf = new CRingMaster_CONF(iConf);
		if (cl_RingMaster_Conferences&&pRmstrConf) {
			// get defined ring trunk for conf
			bncs_string ssItem = bncs_string("conf_").append(iConf);
			pRmstrConf->setAssignedTrunkAddr(11);
			cl_RingMaster_Conferences->insert(pair<int, CRingMaster_CONF*>(iConf, pRmstrConf));
		}
		else
			Debug("LoadAllInit -ERROR- ringmstr conf rec for conf %d not created", iConf);
	} // for conf

	// init array 
	for (int iikey = 0; iikey < MAX_RIEDEL_SLOTS; iikey++) iKeyCurrentIFB[iikey] = bncs_string("0");

	if (cl_RingMaster_IFBs && cl_RingMaster_Logic) {
		int iNumberIFBs = 0;
		int iNumberLogs = 0;
		for (int iiRing = 1; iiRing <= iNumberRiedelRings; iiRing++) {
			CRiedel_Rings* pRing = GetRiedel_Rings_Record(iiRing);
			if (pRing) {
				int iDefTrunk = pRing->m_iTrunkAddress;
				// ifbs
				for (int iifb = 1; iifb <= MAX_IFBS; iifb++) {
					iNumberIFBs++;
					CRingMaster_IFB* pRmstrIfb = new CRingMaster_IFB(iNumberIFBs, iDefTrunk);
					if (pRmstrIfb) {
						cl_RingMaster_IFBs->insert(pair<int, CRingMaster_IFB*>(iNumberIFBs, pRmstrIfb));
					}
					else
						Debug("LoadAllInit -ERROR- ringmstr ifb rec %d / %d not created", iifb, iNumberIFBs);
				}
				// logic
				for (int iLogic = 1; iLogic <= MAX_MIXERS_LOGIC; iLogic++) {
					iNumberLogs++;
					CRingMaster_LOGIC* pRmstrLogic = new CRingMaster_LOGIC(iNumberLogs, iDefTrunk);
					if (pRmstrLogic) {
						cl_RingMaster_Logic->insert(pair<int, CRingMaster_LOGIC*>(iNumberLogs, pRmstrLogic));
					}
					else
						Debug("LoadAllInit -ERROR- ringmstr Logic rec %d / %d not created", iLogic, iNumberLogs);
				}
			}
		} // for iring
		Debug("LoadAllInit - creeated %d ifb and %d logic records ", iNumberIFBs, iNumberLogs);
	}
	else {
		Debug("LoadAllInit -ERROR- no Ring Master IFBS class created - exiting init");
		return FALSE;
	}



	// load Video ANC and Audio Nevion Routers 
	Debug("LoadAllInitData  - loading HD video router data");
	bncs_string ssConfigComposite = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "video_router_hd");
	bncs_stringlist sslcomp = bncs_stringlist(GetCompositeInstance(ssConfigComposite), ',');
	bncs_string ssRtrInstance = sslcomp.getNamedParam("section_01");
	if (GetRouterDeviceDBSizes(ssRtrInstance, &iRouterDev, &iMaxSrcs, &iMaxDests)) {
		if (iRouterDev > 0) {
			// this will be base device - as router driver may be > 4096 and hence mutiple infodrivers
			iIP_RouterVideoHD = iRouterDev;
			CRevsRouterData* pSdi = new CRevsRouterData(iRouterDev, iMaxDests, iMaxSrcs);
			if (pSdi != NULL) {
				cl_RouterRecords->insert(pair<int, CRevsRouterData*>(iRouterDev, pSdi));
				Debug("LoadAllInitData - rtr_HD_Router %s / %s  device %d srcs %d dests %d  ", LPCSTR(ssConfigComposite), LPCSTR(ssRtrInstance), iRouterDev, iMaxSrcs, iMaxDests);
			}
			else {
				Debug("LoadAllInit -ERROR- router class creation failed for HD router %s %d", LPCSTR(ssRtrInstance), iRouterDev);
				return FALSE;
			}
		}
		else {
			Debug("LoadAllInit -ERROR-- HD rtr device is 0 - PACKAGER SUSPENDED from %s %s", LPCSTR(ssConfigComposite), LPCSTR(ssRtrInstance));
			return FALSE;
		}
	}

	Debug("LoadAllInitData  - loading ANC router data");
	ssConfigComposite = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "anc_router");
	sslcomp = bncs_stringlist(GetCompositeInstance(ssConfigComposite), ',');
	ssRtrInstance = sslcomp.getNamedParam("section_01");
	if (GetRouterDeviceDBSizes(ssRtrInstance, &iRouterDev, &iMaxSrcs, &iMaxDests)) {
		if (iRouterDev > 0) {
			// this will be base device - as router driver may be > 4096 and hence mutiple infodrivers
			iIP_RouterANC = iRouterDev;
			CRevsRouterData* pSdi = new CRevsRouterData(iRouterDev, iMaxDests, iMaxSrcs);
			if (pSdi != NULL) {
				cl_RouterRecords->insert(pair<int, CRevsRouterData*>(iRouterDev, pSdi));
				Debug("LoadAllInitData - ANC_Router %s / %s  device %d srcs %d dests %d  ", LPCSTR(ssConfigComposite), LPCSTR(ssRtrInstance), iRouterDev, iMaxSrcs, iMaxDests);
			}
			else {
				Debug("LoadAllInit -ERROR- ANC router class creation failed for %s %d", LPCSTR(ssRtrInstance), iRouterDev);
				return FALSE;
			}
		}
		else {
			Debug("LoadAllInit -ERROR-- ANC device is 0 - PACKAGER SUSPENDED from %s %s", LPCSTR(ssConfigComposite), LPCSTR(ssRtrInstance));
			return FALSE;
		}
	}

	Debug("LoadAllInitData  - loading 2CH AUDIO router data");
	ssConfigComposite = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "audio_router");
	sslcomp = bncs_stringlist(GetCompositeInstance(ssConfigComposite), ',');
	ssRtrInstance = sslcomp.getNamedParam("section_01");
	if (GetRouterDeviceDBSizes(ssRtrInstance, &iRouterDev, &iMaxSrcs, &iMaxDests)) {
		if (iRouterDev > 0) {
			// this will be base device - as router driver may be > 4096 and hence mutiple infodrivers
			iIP_RouterAudio2CH = iRouterDev;
			CRevsRouterData* pSdi = new CRevsRouterData(iRouterDev, iMaxDests, iMaxSrcs);
			if (pSdi != NULL) {
				cl_RouterRecords->insert(pair<int, CRevsRouterData*>(iRouterDev, pSdi));
				Debug("LoadAllInitData - Audio_Router%s / %s  device %d srcs %d dests %d  ", LPCSTR(ssConfigComposite), LPCSTR(ssRtrInstance), iRouterDev, iMaxSrcs, iMaxDests);
			}
			else {
				Debug("LoadAllInit -ERROR- audio router class creation failed for %s %d", LPCSTR(ssRtrInstance), iRouterDev);
				return FALSE;
			}
		}
		else {
			Debug("LoadAllInit -ERROR-- Audio device is 0 - PACKAGER SUSPENDED from %s %s", LPCSTR(ssConfigComposite), LPCSTR(ssRtrInstance)); 
			return FALSE;
		}
	}

	// this packager router
	Debug("LoadAllInitData - Packager is %d size %d", iPackager_router_main, iNumberOfPackages);
	wsprintf(szEntry, "[ 1 - %d ]", iNumberOfPackages);
	SetDlgItemText(hWndDlg, IDC_SPKG_MAXPKGS, szEntry);
	SetDlgItemText(hWndDlg, IDC_DPKG_MAXPKGS, szEntry);

	// update GUI
	wsprintf(szEntry, "%d, %d, %d", iIP_RouterVideoHD, iIP_RouterANC, iIP_RouterAudio2CH);
	SetDlgItemText(hWndDlg, IDC_AUTO_SDIRTR, szEntry);

	intHD_BLACK = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "router_source_park").toInt();
	intHD_BARS = getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "router_source_bars").toInt();
	intHD_SILENCE = intHD_BLACK;
	Debug("LoadAllInit - Default BLACK, HD:%d BARS %d  SILENCE=%d", intHD_BLACK, intHD_BARS, intHD_SILENCE);

	// load Source Packages db data
	Debug("LoadAllInitData - loading Audio + Src Package data ");

	mapOfAllAudioPackages = new map<int, CAudioPackages*>;
	LoadAudioPackageData();

	mapOfAllSrcPackages = new map<int, CSourcePackages*>;
	LoadSourcePackageData();

	mapOfAllSportingEvents = new map<int, CSportEvent*>;
	LoadSportEventData();

	// load Destination Packages db data
	Debug("LoadAllInitData - loading Dest Package data ");
	mapOfAllDestPackages = new map<int, CDestinationPackages*>;
	LoadDestinationPackageData();
	Debug("LoadAllInitData - finished Dest Package data ");

	//
	ssData = "";
	for (int ir = 1; ir <= iNumberRiedelRings; ir++) {
		CRiedel_Rings* pRing = GetRiedel_Rings_Record(ir);
		if (pRing)  ssData.append(bncs_string(" %1").arg(pRing->iRing_Device_GRD));
	}
	SetDlgItemText(hWndDlg, IDC_AUTO_RIEDELS, LPCSTR(ssData));
	//
	ssData = bncs_string("%1 %2 %3-%4 %5-%6").arg(i_RingMaster_Main).arg(i_RingMaster_Conf)
		.arg(i_RingMaster_Ports_Start).arg(i_RingMaster_Ports_End).arg(i_RingMaster_Ifb_Start).arg(i_RingMaster_Ifb_End);
	SetDlgItemText(hWndDlg, IDC_AUTO_RINGMSTR, LPCSTR(ssData));

	// load static Riedel data
	Debug("LoadAllInitData - creating Riedel port use data ");
	BOOL bXMLOK = CreateRiedelPortUseRecords();
	if (!bXMLOK) {
		Debug("LoadInit -- INIT LOAD for Riedel DB Data failed  - no point in continuing");
		return FALSE;
	}

	Debug("LoadAllInitData - creating conference data ");
	iDynConferencePoolStart = 0;
	iDynConferencePoolEnd = 0;
	iDynConfPoolSize = 0;
	iDynConfPoolInUse = 0;
	// get conf pool start and end, hence size -- first pass determines what is in use etc
	bncs_stringlist sslConf = bncs_stringlist(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "riedel_ring_conference_pool"), ',');
	if (sslConf.count() > 1) {
		iDynConferencePoolStart = sslConf[0].toInt();
		iDynConferencePoolEnd = sslConf[1].toInt();
		if ((iDynConferencePoolEnd >= iDynConferencePoolStart) && (iDynConferencePoolEnd > 0) && (iDynConferencePoolStart > 0)) {
			iDynConfPoolSize = iDynConferencePoolEnd - iDynConferencePoolStart + 1;
		}
	}
	Debug("LoadAllInit - 4WConf pool start %d, end %d  = size %d", iDynConferencePoolStart, iDynConferencePoolEnd, iDynConfPoolSize);
	SetDlgItemInt(hWndDlg, IDC_CONFPOOL, iDynConfPoolSize, TRUE);

	Debug("LoadAllInitData - creating dyn ifb data ");
	iDynIFBPoolStart = 0;
	iDynIFBPoolEnd = 0;
	iDynIFBPoolSize = 0;
	for (int iidd=0; iidd<5;iidd++) iDynIFBPoolInUse[iidd] = 0;
	// get ifb pool start and end, hence size -- first pass determines what is in use etc
	bncs_stringlist sslifb = bncs_stringlist(getXMLItemValue(ssXmlConfigFile, ssCompositeInstance, "riedel_ring_ifb_pool"), ',');
	if (sslifb.count() > 1) {
		iDynIFBPoolStart = sslifb[0].toInt();
		iDynIFBPoolEnd = sslifb[1].toInt();
		if ((iDynIFBPoolEnd >= iDynIFBPoolStart) && (iDynIFBPoolEnd > 0) && (iDynIFBPoolStart > 0)) {
			iDynIFBPoolSize = iDynIFBPoolEnd - iDynIFBPoolStart + 1;
		}
	}
	Debug("LoadAllInit - DynIFB pool start %d, end %d  = size %d", iDynIFBPoolStart, iDynIFBPoolEnd, iDynIFBPoolSize);
	SetDlgItemInt(hWndDlg, IDC_IFBPOOL1, iDynIFBPoolSize, TRUE);
	SetDlgItemInt(hWndDlg, IDC_IFBPOOL2, iDynIFBPoolSize, TRUE);
	SetDlgItemInt(hWndDlg, IDC_IFBPOOL3, iDynIFBPoolSize, TRUE);
	SetDlgItemInt(hWndDlg, IDC_IFBPOOL4, iDynIFBPoolSize, TRUE);

	// LOAD Dyn Conf assignments here from db 104
	Debug("Load All init data - dynamic conf and ifb ---  db data");
	LoadDynamicConferenceDBData();
	LoadDynamicIFBDBData();

	// load comms ifb xml -- xxx not required ?? --all dynamic ???
	//Debug("LoadAllInitData - ifb comms mapping xml ");
	//LoadIFBCommsMappingXml();
	// Load Riedel keys data for destination packages ??? 

	Debug( "LoadAllInitData - finished init data " );
	return TRUE;

}



/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: AutomaticRegistrations()
//
//  PURPOSE:register with CSI for gory details
//
//  COMMENTS: 
//			
BOOL AutomaticRegistrations(void)
{

	// go thru defined data types and register with devices - need to find out router sizes for registration
	Debug( "into AutoRegistrations " );

	// vision, anc and audio routers - go thru all router records
	map<int, CRevsRouterData*>::iterator itp = cl_RouterRecords->begin();
	while (itp != cl_RouterRecords->end()) {
		CRevsRouterData* pRtr = itp->second;
		if (pRtr) {
			if (pRtr->getMaximumDestinations() > iIP_RouterInfoDivider) {
				// will require multiple infodrivers
				int iNumberReqInfodrivers = ((pRtr->getMaximumDestinations() - 1) / iIP_RouterInfoDivider) + 1;
				int iLastInfosize = ((pRtr->getMaximumDestinations() - 1) % iIP_RouterInfoDivider) + 1;
				int iInfodriverDevice = pRtr->getRouterNumber();
				Debug("AutoReg - BASE rtr reg dev %d num dests %d  num infodrivers %d last", pRtr->getRouterNumber(), pRtr->getMaximumDestinations(), iNumberReqInfodrivers, iLastInfosize);
				for (int iirr = 1; iirr < iNumberReqInfodrivers; iirr++) {
					Debug("AutoReg - rtr reg dev %d from 1 to %d", iInfodriverDevice, iIP_RouterInfoDivider);
					ecCSIClient->regtallyrange(iInfodriverDevice, 1, iIP_RouterInfoDivider, INSERT);
					iInfodriverDevice++;  // assumes CONTIGIOUS infodriver device numbers
				}
				// last info:
				Debug("AutoReg - rtr reg dev %d from 1 to %d", iInfodriverDevice, iLastInfosize);
				ecCSIClient->regtallyrange(iInfodriverDevice, 1, iLastInfosize, INSERT);
			}
			else {
				// simple registration for single device
				if (pRtr->getMaximumDestinations()>0) {
					Debug("AutoReg - rtr reg dev %d from %d to %d", pRtr->getRouterNumber(), 1, pRtr->getMaximumDestinations());
					ecCSIClient->regtallyrange(pRtr->getRouterNumber(), 1, pRtr->getMaximumDestinations(), INSERT);
				}
				else 
					Debug("AutoReg - NO rtr reg dev %d NO DESTINATIONS ", pRtr->getRouterNumber() );
			}
		}
		itp++;
	}

	// ring master
	ecCSIClient->regtallyrange(i_RingMaster_Conf, 1, 500, INSERT);      // members
	ecCSIClient->regtallyrange(i_RingMaster_Conf, 1001, 1500, INSERT);  // labels
	ecCSIClient->regtallyrange(i_RingMaster_Conf, 2001, 2500, INSERT);  // panel members
	ecCSIClient->regtallyrange(i_RingMaster_Conf, 3001, 3500, INSERT);  // monitor members - calc from mon revs

	for (int iRing = 0; iRing <iNumberRiedelRings; iRing++) {
		//
		ecCSIClient->regtallyrange(i_RingMaster_Ifb_Start+iRing, 1, MAX_IFBS, INSERT);
		ecCSIClient->regtallyrange(i_RingMaster_Ifb_Start + iRing, 1501, 1500 + MAX_IFBS, INSERT); // monitor members - calc from mon revs
		// do not need mixers revs at present from 3001 to 3500 range
		ecCSIClient->regtallyrange(i_RingMaster_Ifb_Start + iRing, 3501, 3500 + MAX_MIXERS_LOGIC, INSERT); //  LOGIC states
		// ports rev
		ecCSIClient->regtallyrange(i_RingMaster_Ports_Start + iRing, 1, 4096, INSERT);
	}

	// automatics control infodriver   
	if ((iAutomatics_CtrlInfo_Local > 0)&&(iAutomatics_CtrlInfo_Remote>0)) {

		int iLocalBase = 0, iOtherBase = 0;
		if (iAutomaticLocation == LDC_LOCALE_RING) {
			iLocalBase = CTRL_INFO_PKGR_LDC_BASE;
			iOtherBase = CTRL_INFO_PKGR_NHN_BASE;
		}
		else if (iAutomaticLocation == NHN_LOCALE_RING) {
			iLocalBase = CTRL_INFO_PKGR_NHN_BASE;
			iOtherBase = CTRL_INFO_PKGR_LDC_BASE;
		}
		// all packagers register for revs from ALL tic-toc slots to determine running status of packagers
		ecCSIClient->regtallyrange(iAutomatics_CtrlInfo_Local, iLocalBase + CTRL_INFO_TXRX_TIKTOK, iLocalBase + CTRL_INFO_RXONLY_TIKTOK, INSERT);   // LDC
		// register for the "other" packager ctrl info for data on shared resources
		ecCSIClient->regtallyrange(iAutomatics_CtrlInfo_Remote, iOtherBase + CTRL_INFO_TXRX_TIKTOK, iOtherBase + CTRL_INFO_HIGHWAYS, INSERT);   // NHN
	}

	if (iOtherSite_Pkgr_router_main > 0) {
		// register with the other packager main infodriver to get RMs about shared resources confs and ifbs 
		if (ecCSIClient) ecCSIClient->regtallyrange(iOtherSite_Pkgr_router_main, 4094, 4094, INSERT);
	}

	Debug( "out of AutoRegistrations " );
	return TRUE;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//
// FUNCTION PollAutomaticDrivers();
//
// to query all drivers to get this automatic up to speed - at startup
void PollAutomaticDrivers( int iCounter )
{
	// during startup this function is called on one second intervals to spread poll out and process revs in ordered manner 
	char szCommand[MAX_AUTO_BUFFER_STRING];
	Debug( "into PollAutomaticDrivers  counter=%d  ", iCounter );
	SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, " -*-*- Auto Polling -*-*-");

	//  poll ringmaster infodrivers here -- first confs
	switch (iCounter) {
	case 3:
		if (iIP_RouterVideoHD > 0) {
			CRevsRouterData* pRtr = GetRouterRecord(iIP_RouterVideoHD);
			if (pRtr) {
				if (pRtr->getMaximumDestinations() > iIP_RouterInfoDivider) {
					// will require multiple infodrivers
					int iNumberReqInfodrivers = ((pRtr->getMaximumDestinations() - 1) / iIP_RouterInfoDivider) + 1;
					int iLastInfosize = ((pRtr->getMaximumDestinations() - 1) % iIP_RouterInfoDivider) + 1;
					int iInfodriverDevice = pRtr->getRouterNumber();
					for (int iirr = 1; iirr < iNumberReqInfodrivers; iirr++) {
						Debug("Poll - HD vision rtr reg dev %d from 1 to %d", iInfodriverDevice, iIP_RouterInfoDivider);
						wsprintf(szCommand, "IP %d 1 %d", iInfodriverDevice, iIP_RouterInfoDivider);
						AddCommandToQue(szCommand, INFODRVCOMMAND, iIP_RouterVideoHD, 0, 0);
						iInfodriverDevice++;  // assumes CONTIGIOUS infodriver device numbers
					}
					// last info:
					Debug("Poll - HD vision rtr reg dev %d from 1 to %d", iInfodriverDevice, iLastInfosize);
					wsprintf(szCommand, "IP %d 1 %d", iInfodriverDevice, iLastInfosize);
					AddCommandToQue(szCommand, INFODRVCOMMAND, iIP_RouterVideoHD, 0, 0);
				}
				else {
					// poll for single device
					Debug("Poll - HD vision rtr reg dev %d from %d to %d", pRtr->getRouterNumber(), 1, pRtr->getMaximumDestinations());
					wsprintf(szCommand, "IP %d 1 %d", pRtr->getRouterNumber(), pRtr->getMaximumDestinations());
					AddCommandToQue(szCommand, INFODRVCOMMAND, pRtr->getRouterNumber(), 0, 0);
				}
			}
		}
		break;

	case 7:
		// automatics control infodriver
		if ((iAutomatics_CtrlInfo_Remote > 0) && (iAutomatics_CtrlInfo_Local>0)) {

			int iLocalBase = 0, iOtherBase = 0;
			if (iAutomaticLocation == LDC_LOCALE_RING) {
				iLocalBase = CTRL_INFO_PKGR_LDC_BASE;
				iOtherBase = CTRL_INFO_PKGR_NHN_BASE;
			}
			else if (iAutomaticLocation == NHN_LOCALE_RING) {
				iLocalBase = CTRL_INFO_PKGR_NHN_BASE;
				iOtherBase = CTRL_INFO_PKGR_LDC_BASE;
			}

			wsprintf(szCommand, "IP %d %d %d", iAutomatics_CtrlInfo_Local, iLocalBase + CTRL_INFO_TXRX_TIKTOK, iLocalBase + CTRL_INFO_RXONLY_TIKTOK);
			ecCSIClient->txinfocmd(szCommand);
			wsprintf(szCommand, "IP %d %d %d", iAutomatics_CtrlInfo_Remote,iOtherBase + CTRL_INFO_TXRX_TIKTOK, iOtherBase + CTRL_INFO_RXONLY_TIKTOK);
			ecCSIClient->txinfocmd(szCommand);

		}
		break;

	case 8:
		if (iIP_RouterAudio2CH > 0) {
			CRevsRouterData* pRtr = GetRouterRecord(iIP_RouterAudio2CH);
			if (pRtr) {
				if (pRtr->getMaximumDestinations() > iIP_RouterInfoDivider) {
					// will require multiple infodrivers
					int iNumberReqInfodrivers = ((pRtr->getMaximumDestinations() - 1) / iIP_RouterInfoDivider) + 1;
					int iLastInfosize = ((pRtr->getMaximumDestinations() - 1) % iIP_RouterInfoDivider) + 1;
					int iInfodriverDevice = pRtr->getRouterNumber();
					for (int iirr = 1; iirr < iNumberReqInfodrivers; iirr++) {
						Debug("Poll - audio rtr reg dev %d from 1 to %d", iInfodriverDevice, iIP_RouterInfoDivider);
						wsprintf(szCommand, "IP %d 1 %d", iInfodriverDevice, iIP_RouterInfoDivider);
						AddCommandToQue(szCommand, INFODRVCOMMAND, iIP_RouterVideoHD, 0, 0);
						iInfodriverDevice++;  // assumes CONTIGIOUS infodriver device numbers
					}
					// last info:
					Debug("Poll - audio rtr reg dev %d from 1 to %d", iInfodriverDevice, iLastInfosize);
					wsprintf(szCommand, "IP %d 1 %d", iInfodriverDevice, iLastInfosize);
					AddCommandToQue(szCommand, INFODRVCOMMAND, iIP_RouterVideoHD, 0, 0);
				}
				else {
					// poll for single device
					Debug("Poll - audio rtr reg dev %d from %d to %d", pRtr->getRouterNumber(), 1, pRtr->getMaximumDestinations());
					wsprintf(szCommand, "IP %d 1 %d", pRtr->getRouterNumber(), pRtr->getMaximumDestinations());
					AddCommandToQue(szCommand, INFODRVCOMMAND, pRtr->getRouterNumber(), 0, 0);
				}
			}
		}
		break;

	case 13:
		// poll ANC router  infodrivers
		if (iIP_RouterANC > 0) {
			CRevsRouterData* pRtr = GetRouterRecord(iIP_RouterANC);
			if (pRtr) {
				if (pRtr->getMaximumDestinations() > 0) {
					if (pRtr->getMaximumDestinations() > iIP_RouterInfoDivider) {
						// will require multiple infodrivers
						int iNumberReqInfodrivers = ((pRtr->getMaximumDestinations() - 1) / iIP_RouterInfoDivider) + 1;
						int iLastInfosize = ((pRtr->getMaximumDestinations() - 1) % iIP_RouterInfoDivider) + 1;
						int iInfodriverDevice = pRtr->getRouterNumber();
						for (int iirr = 1; iirr < iNumberReqInfodrivers; iirr++) {
							Debug("Poll - ANC rtr reg dev %d from 1 to %d", iInfodriverDevice, iIP_RouterInfoDivider);
							wsprintf(szCommand, "IP %d 1 %d", iInfodriverDevice, iIP_RouterInfoDivider);
							AddCommandToQue(szCommand, INFODRVCOMMAND, iIP_RouterVideoHD, 0, 0);
							iInfodriverDevice++;  // assumes CONTIGIOUS infodriver device numbers
						}
						// last info:
						Debug("Poll - ANC rtr reg dev %d from 1 to %d", iInfodriverDevice, iLastInfosize);
						wsprintf(szCommand, "IP %d 1 %d", iInfodriverDevice, iLastInfosize);
						AddCommandToQue(szCommand, INFODRVCOMMAND, iIP_RouterVideoHD, 0, 0);
					}
					else {
						// poll for single device
						Debug("Poll - ANC rtr reg dev %d from %d to %d", pRtr->getRouterNumber(), 1, pRtr->getMaximumDestinations());
						wsprintf(szCommand, "IP %d 1 %d", pRtr->getRouterNumber(), pRtr->getMaximumDestinations());
						AddCommandToQue(szCommand, INFODRVCOMMAND, pRtr->getRouterNumber(), 0, 0);
					}
				}
				else
					Debug("Poll - ANC - NO rtr reg dev %d NO DESTINATIONS ", pRtr->getRouterNumber());
			}
		}
		break;

	case 16:
		if (i_RingMaster_Conf > 0) {
			wsprintf(szCommand, "IP %d 1 %d", i_RingMaster_Conf, MAX_CONFERENCES - 1);
			AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);
		}
		break;
	case 18:
		if (i_RingMaster_Conf > 0) {
			wsprintf(szCommand, "IP %d 1001 %d", i_RingMaster_Conf, 1000 + MAX_CONFERENCES - 1);
			AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);
		}
		break;
	case 20:
		if (i_RingMaster_Conf > 0) {
			wsprintf(szCommand, "IP %d 2001 %d", i_RingMaster_Conf, 2000 + MAX_CONFERENCES - 1);
			AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);
		}
		break;
	case 22:
		if (i_RingMaster_Conf > 0) {
			wsprintf(szCommand, "IP %d 3001 %d", i_RingMaster_Conf, 3000 + MAX_CONFERENCES - 1);
			AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);
		}
		break;

	case 24:
		if (i_RingMaster_Ifb_Start > 0) {
			wsprintf(szCommand, "IP %d 1 %d", i_RingMaster_Ifb_Start, MAX_IFBS);
			AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start, 0, 0);
		}
		break;
	case 27:
		if (i_RingMaster_Ifb_Start > 0) {
			wsprintf(szCommand, "IP %d 1501 %d", i_RingMaster_Ifb_Start, 1500 + MAX_IFBS);
			AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start, 0, 0);
		}
		break;
	case 30:
		if (i_RingMaster_Ifb_Start > 0) {
			wsprintf(szCommand, "IP %d 3501 %d", i_RingMaster_Ifb_Start, 3500 + MAX_MIXERS_LOGIC);
			AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start, 0, 0);
		}
		break;

	case 32:
		wsprintf(szCommand, "IP %d 1 %d", i_RingMaster_Ifb_Start + 1, MAX_IFBS);
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 1, 0, 0);
		break;
	case 35:
		wsprintf(szCommand, "IP %d 1501 %d", i_RingMaster_Ifb_Start + 1, 1500 + MAX_IFBS);
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 1, 0, 0);
		break;
	case 38:
		wsprintf(szCommand, "IP %d 3501 %d", i_RingMaster_Ifb_Start + 1, 3500 + MAX_MIXERS_LOGIC);
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 1, 0, 0);
		break;

	case 40:
		wsprintf(szCommand, "IP %d 1 %d", i_RingMaster_Ifb_Start + 2, MAX_IFBS );
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 2, 0, 0);
		break;
	case 43:
		wsprintf(szCommand, "IP %d 1501 %d", i_RingMaster_Ifb_Start + 2, 1500 + MAX_IFBS );
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 1, 0, 0);
		break;
	case 46:
		wsprintf(szCommand, "IP %d 3501 %d", i_RingMaster_Ifb_Start + 2, 3500 + MAX_MIXERS_LOGIC);
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 2, 0, 0);
		break;

	case 48:
		wsprintf(szCommand, "IP %d 1 %d", i_RingMaster_Ifb_Start + 3, MAX_IFBS );
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 3, 0, 0);
		break;
	case 51:
		wsprintf(szCommand, "IP %d 1501 %d", i_RingMaster_Ifb_Start + 3, 1500 + MAX_IFBS );
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 3, 0, 0);
		break;
	case 54:
		wsprintf(szCommand, "IP %d 3501 %d", i_RingMaster_Ifb_Start + 3, 3500 + MAX_MIXERS_LOGIC);
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ifb_Start + 3, 0, 0);
		break;

	case 56:
			wsprintf(szCommand, "IP %d 1 4096", i_RingMaster_Ports_Start);
			AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ports_Start, 0, 0);
		break;
	case 59:
		wsprintf(szCommand, "IP %d 1 4096", i_RingMaster_Ports_Start+1);
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ports_Start+1, 0, 0);
		break;
	case 62:
		wsprintf(szCommand, "IP %d 1 4096", i_RingMaster_Ports_Start+2);
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ports_Start+2, 0, 0);
		break;
	case 65:
		wsprintf(szCommand, "IP %d 1 4096", i_RingMaster_Ports_Start+3);
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Ports_Start+3, 0, 0);
		break;

	} // switch

	ProcessNextCommand(1); // start off timer to send out from queue
	UpdateCounters();

}

///////////////////////////////////////////////////////////////////////////
//
void CalculateSrcePkgPtiString(CSourcePackages* pSrce)
{
	strcpy(szResult, "");
	if (pSrce) {
		for (int iid = 0; iid < pSrce->ssl_Pkg_DirectlyRoutedTo.count(); iid++) {
			char szbit[8] = "";
			if (iid>0)
				wsprintf(szbit, ",%d", pSrce->ssl_Pkg_DirectlyRoutedTo[iid].toInt());
			else
				strcpy(szbit, LPCSTR(pSrce->ssl_Pkg_DirectlyRoutedTo[iid]));
			if ((strlen(szResult) + strlen(szbit))<245) {
				strcat(szResult, szbit);
			}
			else {
				strcat(szResult, ",$");
				iid = pSrce->ssl_Pkg_DirectlyRoutedTo.count() + 2;
			}
		} // for iid
		if (strlen(szResult) < 245) {
			// if space permits add what you can of the finally routed to
			strcat(szResult, "|");
			for (int iif = 0; iif < pSrce->ssl_Pkg_FinallyRoutedTo.count(); iif++) {
				char szbit[8] = "";
				if (iif>0)
					wsprintf(szbit, ",%d", pSrce->ssl_Pkg_FinallyRoutedTo[iif].toInt());
				else
					strcpy(szbit, LPCSTR(pSrce->ssl_Pkg_FinallyRoutedTo[iif]));
				if ((strlen(szResult) + strlen(szbit))<245) {
					strcat(szResult, szbit);
				}
				else {
					strcat(szResult, ",$");
					iif = pSrce->ssl_Pkg_FinallyRoutedTo.count() + 2;
				}
			} // for iif
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INITIAL PROCESSING TO GET AUTO INTO CORRECT STATE
//
//
void ProcessFirstPass_Logic( void )
{

	int iIndex=0, iLen=0;
	// 1. go thru all the 8000 dest pkages slots and see what is currently stored to determine routed packages etc
	Debug("First Pass - dest pkgs part 1");
	Debug("First Pass - dest pkgs RM load DB file ");

	mapOfDBFile = new map<int, CCommand*>;
	LoadDBFile(iPackager_router_main, DB_SYNC_PACKAGE_ROUTING, (iNumberOfPackages+1));

	Debug("First Pass - dest pkgs RM process file at 1");
	for (iIndex = 1; iIndex<=iNumberOfPackages; iIndex++) {
		CDestinationPackages* pDstPkg = GetDestinationPackage(iIndex);
		if (pDstPkg) {

			bncs_string ssRMInData = "";
			if ((iIndex % 1000) == 0) Debug("First Pass - dest pkgs RM processing at %d", iIndex);
			// get data from db101 first -- is it valid - then use that else get slot data
			// get sync rm data  from db101 first -- is it valid - then use that else get slot data as db deemed to be definitive over anything in infodriver slot 
			//dbmPkgs.getName(iPackager_router_main, DB_SYNC_PACKAGE_ROUTING, iIndex, szRMData, iLen);
			//ssRMInData = bncs_string(szRMData);
			if (mapOfDBFile) {
				CCommand* pData = GetDBFileRecord(iIndex);
				if (pData) {
					ssRMInData = bncs_string(pData->szCommand);
				}
			}
			else {
				ssRMInData = bncs_string(ExtractNamefromDevIniFile(iPackager_router_main, DB_SYNC_PACKAGE_ROUTING, iIndex));
			}
			if (ssRMInData.find("index=") <0) {
				//Debug("FirstPAss - dest %d route loading from infodriver as getname failed", iIndex);
				ssRMInData = bncs_string(GetPackagerInfodriverSlotContents(iIndex, AUTO_PACK_ROUTE_INFO));
			}
			if (ssRMInData.length() == 0) ssRMInData = bncs_string("index=PARK");
			if (ssRMInData.length()>0) {
				// parse and set vars in dest pkg
				int iNewSrcPkg = 0;
				//
				bncs_stringlist ssl = bncs_stringlist(ssRMInData, ',');
				// parse 
				bncs_string ssIndex = ssl.getNamedParam("index").upper();
				if (ssIndex.length()>0) {
					if (ssIndex.find("BARS") >= 0) iNewSrcPkg = iPackagerBarsSrcePkg;
					else if (ssIndex.find("PARK") >= 0) iNewSrcPkg = iPackagerParkSrcePkg;
					else iNewSrcPkg = ssIndex.toInt();
					if (iNewSrcPkg <= 0) iNewSrcPkg = iPackagerParkSrcePkg; // use park pkg data
				}
				else {
					// no index given so use current routed package -- as init it will be PARK
					iNewSrcPkg = iPackagerParkSrcePkg;
				}
				// other parameters - Nevion priority
				int iNevPri = ssl.getNamedParam("pri").toInt();
				if ((iNevPri<1) || (iNevPri>5)) iNevPri = 3;
				pDstPkg->iNevionPriorityLevel = iNevPri;
				//
				pDstPkg->i_Routed_Src_Package = iNewSrcPkg;
				pDstPkg->i_Traced_Src_Package = iNewSrcPkg;
				pDstPkg->ss_Trace_To_Source = bncs_string("%1").arg(iNewSrcPkg);
				// add take special options -- how to perserve when diff selections made ???
				pDstPkg->i_CmdRoute_Level = ssl.getNamedParam("v_level").toInt();
				pDstPkg->i_VisionFrom_Source = ssl.getNamedParam("v_index").toInt();
				pDstPkg->i_CmdRoute_LangUse_Tag = ssl.getNamedParam("tag").toInt();
				pDstPkg->i_CmdAudio_Returns = ssl.getNamedParam("returns").toInt();
				//
				if (pDstPkg->st_VideoHDestination.iAssocIndex>0) pDstPkg->st_VideoHDestination.iMSPLevelAPIndx = pDstPkg->i_CmdRoute_Level;
				for (int iianc = 1; iianc < MAX_PKG_VIDEO; iianc++) {
					if (pDstPkg->st_VideoANC[iianc].iAssocIndex>0) pDstPkg->st_VideoANC[iianc].iMSPLevelAPIndx = pDstPkg->i_CmdRoute_Level;
				}
				//
				// write back out to slot 
				char szCmdRev[MAX_AUTO_BUFFER_STRING] = "";
				if ((pDstPkg->i_Routed_Src_Package == 0) || (pDstPkg->i_Routed_Src_Package == iPackagerParkSrcePkg))
					strcpy(szCmdRev, "index=PARK,v_level=0,v_index=0");
				else if (pDstPkg->i_Routed_Src_Package == iPackagerBarsSrcePkg)
					strcpy(szCmdRev, "index=BARS,v_level=0,v_index=0");
				else
					wsprintf(szCmdRev, "index=%d,v_level=%d,v_index=%d", pDstPkg->i_Routed_Src_Package, pDstPkg->st_VideoHDestination.iMSPLevelAPIndx, pDstPkg->i_VisionFrom_Source);
				SendPackagerBNCSRevertive(iIndex, AUTO_PACK_ROUTE_INFO, FALSE, szCmdRev);
			}
		}
	}

	// clear db map load as finished with it now
	if (mapOfDBFile) {
		while (mapOfDBFile->begin() != mapOfDBFile->end()) {
			map<int, CCommand*>::iterator it = mapOfDBFile->begin();
			if (it->second) delete it->second;
			mapOfDBFile->erase(it);
		}
		delete mapOfDBFile;
	}


	char szData[MAX_AUTO_BUFFER_STRING]="";
	char szBlank[MAX_AUTO_BUFFER_STRING] = "";
	char szReply[64] = "";
	int iExpSrc = 0;

	Debug("First Pass - dest pkgs part 2");
	for (iIndex = 1; iIndex < (iNumberOfPackages+1); iIndex++) {

		CDestinationPackages* pDstPkg = GetDestinationPackage(iIndex);
		if (pDstPkg) {
			BOOL bVirtualDestPkg = FALSE;
			BOOL bVirtualSrcPkg = FALSE;
			if ((iIndex >= i_Start_VirtualPackages) && (iIndex<=iNumberOfPackages)) bVirtualDestPkg = TRUE;

			// parse and set vars in dest pkg
			int iNewSrcPkg = pDstPkg->i_Routed_Src_Package;
			if ((iNewSrcPkg >= i_Start_VirtualPackages) && (iNewSrcPkg < (iNumberOfPackages+1))) bVirtualSrcPkg = TRUE;

			// check for attempting to route a virtual into itself
			// also check that if this virtual is going to another virtual with itself inalready, eg 2001->2002; when 2002->2001 already
			if (bVirtualDestPkg&&bVirtualSrcPkg) {
				// test 1 - routing itself to itself
				if (iNewSrcPkg == iIndex) {
					Debug("ProcessFirstPass - ERROR- rejecting virtual routing %d to itself %d ", iNewSrcPkg, iIndex);
					bVirtualSrcPkg = FALSE;
					iNewSrcPkg = iPackagerParkSrcePkg;
					strcpy(szData, "index=PARK,v_level=0,v_index=0");
					pDstPkg->i_Routed_Src_Package = iNewSrcPkg;
					SendPackagerBNCSRevertive(iIndex, AUTO_PACK_ROUTE_INFO, TRUE, szData);
				}
				// test 2 - try and see if vir is used in chain where it is already being used - trace forward
				bFoundVirtualSourceAhead = FALSE;
				iRecursiveCounter = 0;
				TraceVirtualForward(iNewSrcPkg, iIndex);
				if (bFoundVirtualSourceAhead ) {
					Debug("ProcessFirstPass - ERROR- rejecting virtual routing %d FORWARD TRACE FAILED %d ", iNewSrcPkg, iIndex);
					bVirtualDestPkg = FALSE;
					iNewSrcPkg = iPackagerParkSrcePkg;
					strcpy(szData, "index=PARK,v_level=0,v_index=0");
					pDstPkg->i_Routed_Src_Package = iNewSrcPkg;
					SendPackagerBNCSRevertive(iIndex, AUTO_PACK_ROUTE_INFO, TRUE, szData);
				}
			}

			// assign in src class
			if ((iNewSrcPkg > 0) && (iNewSrcPkg < (iNumberOfPackages+1))) {
				CSourcePackages* pSrcPkg = GetSourcePackage(iNewSrcPkg);
				if (pSrcPkg) {
					// add to current src pkg pti list
					if (pSrcPkg->ssl_Pkg_DirectlyRoutedTo.find(pDstPkg->iPackageIndex) < 0)
						pSrcPkg->ssl_Pkg_DirectlyRoutedTo.append(pDstPkg->iPackageIndex);
				}

				if (bVirtualSrcPkg) {
					// calc and assign traced 
					ssl_Calc_Trace = bncs_stringlist("", '|');
					int iTracedSrcPkg = TraceRealSourcePackageCalcTrace(iIndex);
					pDstPkg->i_Traced_Src_Package = iTracedSrcPkg;
					if (ssl_Calc_Trace.count() == 0) ssl_Calc_Trace.append("0");
					pDstPkg->ss_Trace_To_Source = ssl_Calc_Trace.toString('|');
					if ((iTracedSrcPkg > 0) && (iTracedSrcPkg < i_Start_VirtualPackages) && (iTracedSrcPkg != iNewSrcPkg)) {
						CSourcePackages* pSrcPkg2 = GetSourcePackage(iTracedSrcPkg);
						if ((pSrcPkg2) && (pDstPkg->iPackageIndex < i_Start_VirtualPackages)) {
							// add to current src pkg pti list
							if (pSrcPkg2->ssl_Pkg_FinallyRoutedTo.find(pDstPkg->iPackageIndex) < 0)
								pSrcPkg2->ssl_Pkg_FinallyRoutedTo.append(pDstPkg->iPackageIndex);
						}
					}
				}
				else {
					pDstPkg->i_Traced_Src_Package = iNewSrcPkg;
					pDstPkg->ss_Trace_To_Source = bncs_string("%1").arg(iNewSrcPkg);
				}
			}
			else {
				// bars and park
				pDstPkg->i_Traced_Src_Package = iNewSrcPkg;
				pDstPkg->ss_Trace_To_Source = bncs_string("%1").arg(iNewSrcPkg);
			}

			// dest package lock state 
			bncs_string ssRMInData = bncs_string(GetPackagerInfodriverSlotContents(iIndex, AUTO_DEST_PKG_STATUS_INFO));
			bncs_stringlist ssl_cmd = bncs_stringlist(ssRMInData, ',');
			pDstPkg->iPackageLockState = ssl_cmd.getNamedParam("lock").toInt();

			// dest pkg riedel port use
			CalculateDestinationMMRiedelPortUse(iIndex, FALSE);

		}
	} // for index


	Debug("First Pass - Audio Packages - dynamics; mmQs; riedel port use");
	for (iIndex = 1; iIndex < (iNumberOfPackages+1); iIndex++)  {
		// audio packages
		CAudioPackages* pAudio = GetAudioPackage(iIndex);
		if (pAudio) {
			bncs_string ssRMInData = bncs_string(GetPackagerInfodriverSlotContents(iIndex, AUTO_AUDIO_PKG_INFO));
			bncs_stringlist ssl_cmd = bncs_stringlist(ssRMInData, ',');
			// reverse vision / audio
			pAudio->iEnabledRevVision = ssl_cmd.getNamedParam("rev_enabled").toInt();
			pAudio->iRoutedRevVisionPkg = ssl_cmd.getNamedParam("vision").toInt();
			pAudio->iRoutedRevAudioPkg = ssl_cmd.getNamedParam("audio").toInt();

			if (pAudio->iRoutedRevVisionPkg > 0) {
				CDestinationPackages* pDest = GetDestinationPackage(pAudio->iRoutedRevVisionPkg);
				if (pDest) pDest->i_Reverse_Vision_Routed_AudioPackage = iIndex;
			}
			if (pAudio->iRoutedRevVisionPkg > 0) {
				CDestinationPackages* pDest = GetDestinationPackage(pAudio->iRoutedRevVisionPkg);
				if (pDest) {
					pDest->i_Reverse_Audio_Routed_AudioPackage[1] = iIndex;
					pDest->i_Reverse_Audio_Routed_AudioPackage[2] = iIndex;
				}
			}
			// as comms params --- dyn confs and ifbs already loaded from db backup files - update slot
			// db backup file has precedence.
			UpdateAudioPackageStatusInfodriver(iIndex);
			//
			// get AP mixMinus Queue data and load
			ssRMInData = bncs_string(GetPackagerInfodriverSlotContents(iIndex, AUTO_MM_STACK_INFO));
			bncs_stringlist ssl_mmq = bncs_stringlist(ssRMInData, ',');
			if (ssl_mmq.count() > 0) {
				for (int iicc = 0; iicc < ssl_mmq.count(); iicc++) {
					pAudio->ssl_Pkg_Queue.append(ssl_mmq[iicc]);
				}
				// update dest pkg for top mm q
				int iDestP = pAudio->ssl_Pkg_Queue[0].toInt();
				if ((iDestP > 0) && (iDestP < (iNumberOfPackages+1))) {
					CDestinationPackages* pDest = GetDestinationPackage(iDestP);
					if (pDest) pDest->ssl_AtTopOfAudioPkgMMQ.append(bncs_string(iIndex));
				}
			}
			//Debug("FirstPass -- Calc Riedel Port Use");
			CalculateRiedelPortUse(iIndex, FALSE);

		}
	}


	// 2. update all the 1999 pti list slots for determined routed packages from step 1
	Debug("First Pass - process srce pkgs");
	for (iIndex = 1; iIndex<=iNumberOfPackages; iIndex++) {
		CSourcePackages* pSrcPkg = GetSourcePackage( iIndex );
		if (pSrcPkg) {
			// update pti slot
			//
			BOOL bVirtualSrcePkg = FALSE;
			if ((iIndex >= i_Start_VirtualPackages) && (iIndex<=iNumberOfPackages)) bVirtualSrcePkg = TRUE;
			// 
			if (pSrcPkg->ssl_Pkg_DirectlyRoutedTo.count()>10) Debug("Process_1st_Logic - SrcPkg %d is routed to %d dest pkages", iIndex, pSrcPkg->ssl_Pkg_DirectlyRoutedTo.count());
			// uses global szResult string
			CalculateSrcePkgPtiString(pSrcPkg);
			SendPackagerBNCSRevertive(pSrcPkg->iPackageIndex, AUTO_SRCE_PKG_PTI_INFO, FALSE, szResult);
		}
	}

	// redo / check thru  -- first pass processing
	// go thru all dest pkg ifbs and check that perm member is currently routed - if not then add and send to riedel
	// if at top of src package queue - then reassert the complete port list for completeness sake
	Debug("First Pass - routing status");
	for (iIndex = 1; iIndex<=iNumberOfPackages; iIndex++) {
		CDestinationPackages* pDstPkg = GetDestinationPackage( iIndex );
		if (pDstPkg) {

			// try to determine lAudio pkg / AudSrce pairs  -- check audio pkgs ( video sorted earlier )
			CRevsRouterData* pAudioRtr = GetRouterRecord(iIP_RouterAudio2CH);
			if (pAudioRtr) {
				for (int iiaud = 1; iiaud<MAX_PKG_AUDIO; iiaud++) {
					if (pDstPkg->st_AudioDestPair[iiaud].iAssocIndex>0) {
						int irutedSrc = pAudioRtr->getPrimarySourceForDestination(pDstPkg->st_AudioDestPair[iiaud].iAssocIndex);
						if ((irutedSrc>0) && (pDstPkg->i_Traced_Src_Package>0) && (pDstPkg->i_Traced_Src_Package<i_Start_VirtualPackages)) {
							// find audio source pkg / and pair in traced src pkg
							CSourcePackages* pSPkg = GetSourcePackage(pDstPkg->i_Traced_Src_Package);
							if (pSPkg) {
								for (int iiap = 1; iiap <= pSPkg->m_iNumberAssignedAudioPackages; iiap++) {
									if (pSPkg->m_iAssoc_Audio_Packages[iiap]>0) {
										CAudioPackages* pAudPkg = GetAudioPackage(pSPkg->m_iAssoc_Audio_Packages[iiap]);
										if (pAudPkg) {
											for (int iwasp = 1; iwasp < MAX_AUDLVL_AUDIO; iwasp++) {
												if (pAudPkg->st_AudioSourcePair[iwasp].iAssocIndex == irutedSrc) {
													pDstPkg->st_AudioDestPair[iiaud].iMSPLevelAPIndx = pAudPkg->m_iAudioPkg_Indx;
													pDstPkg->st_AudioDestPair[iiaud].iASPairIndx = iwasp;
													// terminate inner two loops now as indices found
													iwasp = MAX_PKG_AUDIO;
													iiap = MAX_PKG_AUDIO;
												}
											} // for iwasp
										}
									}
								} // for iiap
							}
						}
					}
				} // for iiaud
			}
			UpdateDestPkgSdiStatusInfodriverSlot(pDstPkg->iPackageIndex, FALSE);
		}
	} // for iIndex

	// update various riedel  info
	CalculateDynIFBPoolUse();
	Calculate4WConferencePoolUse();
	for (int iipp = 1; iipp < MAX_RIEDEL_SLOTS; iipp++) {
		UpdateRiedelPortUseInfodriver(iipp);
	}

	// update router dests in use in audio and dest packages
	Debug("First Pass - sdi dest in use in pkgs");
	for (int iirr = 1; iirr < iNumberOfPackages; iirr++) {
		UpdateRoutersDestUseInfodriverSlots(iirr);
	}

	Debug("First Pass - completed");

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    EXTERNAL INFODRIVER COMMANDS & RESPONSES
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
void ClearAllConferenceKeyMembers( CRingMaster_CONF* pConf )
{
	// go thru and clear all monitors keys for tb or Ifb conferences
	if (pConf) {
		char szCommand[MAX_AUTO_BUFFER_STRING]="";
		bncs_stringlist ssl = bncs_stringlist(pConf->getRingMasterCalcMonitorMembers(), ' ');
		for (int iM = 0; iM<ssl.count(); iM++) {
			int iMonTrnk = 0;
			int iMonIndex = 0;
			GetTrunkAndPortFromString(bncs_string(ssl[iM]), pConf->getAssignedTrunkAddr(), &iMonTrnk, &iMonIndex);
			CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iMonTrnk);
			if (pRing) {
				if (pRing->iRing_Device_Monitor>0) {
					wsprintf(szCommand, "IW %d '%s' %d", pRing->iRing_Device_Monitor, LPCSTR(ssKeyClearCmd), iMonIndex);
					AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Monitor, 0, 0);
				}
			}
			else
				Debug("ClearAllConferenceKeyMembers - no ring from conf mem %s", LPCSTR(ssl[iM]));
		}
	}
}

void ClearConferencePortMembers( int iConfIndex )
{
	// go thru and clear members for tb conferences  -- IFB conf handled elsewhere
	if ((iConfIndex>0) && (iConfIndex<MAX_CONFERENCES) && (i_RingMaster_Conf>0)) {
		char szCommand[MAX_AUTO_BUFFER_STRING]="";
		// clear the label
		wsprintf( szCommand, "IW %d '' %d", i_RingMaster_Conf,  iConfIndex+1000 );  
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);
		// clear all port members by sending null string
		wsprintf(szCommand, "IW %d '&REMOVE=EVERYTHING' %d", i_RingMaster_Conf, iConfIndex);  // was ' ' - empty string 
		AddCommandToQue(szCommand, INFODRVCOMMAND, i_RingMaster_Conf, 0, 0);
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////////

void UpdateDestPkgSdiStatusInfodriverSlot(int iIndex, BOOL bSetSlot )
{
	try
	{

		bncs_string ssStatusStr = "";
		int iVidStat = SIGNAL_STATUS_EXPECTED, iAudStat = SIGNAL_STATUS_EXPECTED, iAncStat = SIGNAL_STATUS_EXPECTED;

		if ((iIndex > 0) && (iIndex < (iNumberOfPackages+1))) {
			// 
			CRevsRouterData* pHDMain = GetRouterRecord(iIP_RouterVideoHD);
			CRevsRouterData* pAncMain = GetRouterRecord(iIP_RouterANC);
			CRevsRouterData* pAudio2Ch = GetRouterRecord(iIP_RouterAudio2CH);
			CDestinationPackages* pDPkg = GetDestinationPackage(iIndex);
			if (pDPkg&&pHDMain&&pAncMain&&pAudio2Ch) {
				//
				CSourcePackages* pSrce = GetSourcePackage(pDPkg->i_Traced_Src_Package);
				if (pSrce) {
					// video and anc
					if (pDPkg->st_VideoHDestination.iAssocIndex > 0) {
						int iRutedSrc = 0;
						int iWhichRouter = iIP_RouterVideoHD;
						int iLvl = pDPkg->st_VideoHDestination.iMSPLevelAPIndx;
						iRutedSrc = pHDMain->getPrimarySourceForDestination(pDPkg->st_VideoHDestination.iAssocIndex);
						if ((iLvl > 0) && (iLvl < MAX_PKG_VIDEO)) {
							if (iRutedSrc != pSrce->st_PkgVideoLevels[iLvl].stVideoLevel.iAssocIndex) iVidStat = SIGNAL_STATUS_UNEXPECTED;  // some video error
						}
						// anc
						for (int iianc = 1; iianc < MAX_PKG_VIDEO; iianc++) {
							if (pDPkg->st_VideoANC[iianc].iAssocIndex>0) {
								iRutedSrc = pAncMain->getPrimarySourceForDestination(pDPkg->st_VideoANC[iianc].iAssocIndex);
								if ((iLvl > 0) && (iLvl < MAX_PKG_VIDEO)) {
									if (iRutedSrc != pSrce->st_PkgVideoLevels[iLvl].stANCLevel[iianc].iAssocIndex) {
										iAncStat = SIGNAL_STATUS_UNEXPECTED;  // some anc error
										iianc = MAX_PKG_VIDEO; // short circuit loop
									}
								}
							}
						}
					}
					// audio  
					for (int iiad = 1; iiad < MAX_PKG_AUDIO; iiad++) {
						if (pDPkg->st_AudioDestPair[iiad].iAssocIndex>0) {
							if ((pDPkg->st_AudioDestPair[iiad].iMSPLevelAPIndx > 0) && (pDPkg->st_AudioDestPair[iiad].iMSPLevelAPIndx < (iNumberOfPackages+1))) {
								int iPairIndx = pDPkg->st_AudioDestPair[iiad].iASPairIndx;
								int iAPIndx = pDPkg->st_AudioDestPair[iiad].iMSPLevelAPIndx;
								int iRutedSrc = pAudio2Ch->getPrimarySourceForDestination(pDPkg->st_AudioDestPair[iiad].iAssocIndex);
								CAudioPackages* pRoutedAudPkg = GetAudioPackage(iAPIndx);
								if (pRoutedAudPkg) {
									if ((iPairIndx > 0) && (iPairIndx < MAX_AUDLVL_AUDIO)) {
										if (iRutedSrc != pRoutedAudPkg->st_AudioSourcePair[iPairIndx].iAssocIndex) {
											iAudStat = SIGNAL_STATUS_UNEXPECTED;
											iiad = MAX_PKG_AUDIO; // shortcircuit loop as routing error encountered;
											if (bShowAllDebugMessages) Debug("UpdateDPSdiStat AP mismatch- DP %d pair %d apIndx %d routed src %d  expectsrc %d ", 
												iIndex, iPairIndx, iAPIndx, iRutedSrc, pRoutedAudPkg->st_AudioSourcePair[iPairIndx].iAssocIndex );
										}
									}
								}
							}
						}
					} // for iiad
				}
				// build string
				ssStatusStr = bncs_string("video=%1,audio=%2,anc=%3,lock=%4,vision_from=%5,sound_from=%6,audio_tag=%7,trace=%8").arg(iVidStat).arg(iAudStat).
					arg(iAncStat).arg(pDPkg->iPackageLockState).arg(pDPkg->i_VisionFrom_Pkg).arg(pDPkg->i_AudioFrom_Pkg).arg(pDPkg->i_AudioFrom_TagStatus).arg(pDPkg->ss_Trace_To_Source);
				if (ssStatusStr.length() > 255) ssStatusStr.truncate(254);
				// update slot
				SendPackagerBNCSRevertive(iIndex, AUTO_DEST_PKG_STATUS_INFO, bSetSlot, LPCSTR(ssStatusStr));
			}
			else
				Debug("UpdateSdiDestPkgInfodriverSlot - invalid pkg or rtr class from %d ", iIndex);
		}
		else
			Debug("UpdateSdiDestPkgInfodriverSlot - invalid index %d ", iIndex);

	}
	catch (structured_exception const & e)
	{
		Debug("%s:UpdateDestPkgSdiStatusInfodriverSlot() - thrown exception %x from %x", szAppName, e.what(), e.where());
	}
	catch (...)
	{
		Debug("%s:UpdateDestPkgSdiStatusInfodriverSlot() - Unknown exception ", szAppName);
	}

}


void RemoveDestinationPackageFromSourcePTIList(int iDest, int iPrevSrcPkg)
{
	CSourcePackages* pOldSrc = GetSourcePackage(iPrevSrcPkg);
	if (pOldSrc) {
		// update OLD src package pti infodriver 
		// remove from to current src pkg pti list
		int iPos = pOldSrc->ssl_Pkg_DirectlyRoutedTo.find(iDest);
		if (iPos >= 0) pOldSrc->ssl_Pkg_DirectlyRoutedTo.deleteItem(iPos);
		// update pti slot -- but only for real packages -- not bars and park
		if ((pOldSrc->iPackageIndex>0) && (pOldSrc->iPackageIndex<=iNumberOfPackages)) {
			// uses global szResult string
			CalculateSrcePkgPtiString(pOldSrc);
			SendPackagerBNCSRevertive(pOldSrc->iPackageIndex, AUTO_SRCE_PKG_PTI_INFO, FALSE, szResult);
		}
		// update gui if applicable
		if (iChosenSrcPkg == pOldSrc->iPackageIndex) DisplaySourcePackageData();
	}
}

void AddDestinationPackageToSourcePTIList(int iDest, int iNewSrcPkg)
{
	CSourcePackages* pSrc = GetSourcePackage(iNewSrcPkg);
	if (pSrc) {
		// add to current src pkg pti list
		int iPos = pSrc->ssl_Pkg_DirectlyRoutedTo.find(iDest);
		if (iPos<0) pSrc->ssl_Pkg_DirectlyRoutedTo.append(iDest);
		// update pti slot -- but only for real/virtual packages -- BUT not bars and park
		if ((pSrc->iPackageIndex>0) && (pSrc->iPackageIndex < (iNumberOfPackages+1))) {
			// uses global szResult string
			CalculateSrcePkgPtiString(pSrc);
			SendPackagerBNCSRevertive(pSrc->iPackageIndex, AUTO_SRCE_PKG_PTI_INFO, FALSE, szResult);
		}
		// update gui if applicable
		if (iChosenSrcPkg == pSrc->iPackageIndex) {
			SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED, LPCSTR(pSrc->ssl_Pkg_DirectlyRoutedTo.toString()));
		}
	}
}

void RemoveDestinationPackageFromTracedPTIList(int iDest, int iPrevTracedPkg)
{
	CSourcePackages* pOldSrc = GetSourcePackage(iPrevTracedPkg);
	if (pOldSrc) {
		// remove from to current src pkg pti list
		int iPos = pOldSrc->ssl_Pkg_FinallyRoutedTo.find(iDest);
		if (iPos >= 0) pOldSrc->ssl_Pkg_FinallyRoutedTo.deleteItem(iPos);
		// update pti slot -- but only for real/virtual packages -- BUT not bars and park
		if ((pOldSrc->iPackageIndex>0) && (pOldSrc->iPackageIndex<=iNumberOfPackages)) {
			// uses global szResult string
			CalculateSrcePkgPtiString(pOldSrc);
			SendPackagerBNCSRevertive(pOldSrc->iPackageIndex, AUTO_SRCE_PKG_PTI_INFO, FALSE, szResult);
		}
		// update gui if applicable
		if (iChosenSrcPkg == pOldSrc->iPackageIndex) {
			SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED2, LPCSTR(pOldSrc->ssl_Pkg_FinallyRoutedTo.toString()));
		}
	}
}

void AddDestinationPackageToTracedPTIList(int iDest, int iNewTracedPkg)
{
	CSourcePackages* pSrc = GetSourcePackage(iNewTracedPkg);
	if (pSrc) {
		// remove from to current src pkg pti list
		if ((iDest>0) && (iDest<i_Start_VirtualPackages)) {
			int iPos = pSrc->ssl_Pkg_FinallyRoutedTo.find(iDest);
			if (iPos<0) pSrc->ssl_Pkg_FinallyRoutedTo.append(iDest);
		}
		// update pti slot -- but only for real/virtual packages -- BUT not bars and park
		if ((pSrc->iPackageIndex>0) && (pSrc->iPackageIndex<=iNumberOfPackages)) {
			// uses global szResult string
			CalculateSrcePkgPtiString(pSrc);
			SendPackagerBNCSRevertive(pSrc->iPackageIndex, AUTO_SRCE_PKG_PTI_INFO, FALSE, szResult);
		}
		// update gui if applicable
		if (iChosenSrcPkg == pSrc->iPackageIndex) {
			SetDlgItemText(hWndDlg, IDC_SPKG_ROUTED2, LPCSTR(pSrc->ssl_Pkg_FinallyRoutedTo.toString()));
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
BOOL ProcessDestinationPkgCommand(CDestinationPackages* pDstPkg, bncs_string ssCmd)
{

	try {

		int iVideoDest = 0, iVideoSrc = 0, iEntry = 0;
		char szCommand[MAX_AUTO_BUFFER_STRING] = "", szData[32] = "";

		if (pDstPkg) {

			int iNewSrcPkg = 0;
			int iPkgIndex = pDstPkg->iPackageIndex;                                      // equals slot number
			int iPrevSrcPkg = pDstPkg->i_Routed_Src_Package;
			int iPrevTracedPkg = pDstPkg->i_Traced_Src_Package;
			BOOL bVirtualSrcPkg = FALSE;
			BOOL bVirtualDestPkg = FALSE;
			BOOL bChangedPackage = FALSE;
			BOOL bChangedPTBSetting = FALSE;  // will be false for most pkages most of the time
			if ((pDstPkg->iPackageIndex >= i_Start_VirtualPackages) && (pDstPkg->iPackageIndex < (iNumberOfPackages+1))) bVirtualDestPkg = TRUE;

			// parse command 
			// 1. get index
			// test for bars or park 
			bncs_stringlist ssl = bncs_stringlist(ssCmd, ',');
			bncs_string ssIndex = ssl.getNamedParam("index").upper();
			if (ssIndex.length() > 0) {
				if (ssIndex.find("BARS") >= 0) iNewSrcPkg = iPackagerBarsSrcePkg;
				else if (ssIndex.find("PARK") >= 0) iNewSrcPkg = iPackagerParkSrcePkg;
				else iNewSrcPkg = ssIndex.toInt();
				if ((iNewSrcPkg <= 0) || (iNewSrcPkg > iPackagerBarsSrcePkg)) {
					if (pDstPkg->i_ParkSourcePackage>0)
						iNewSrcPkg = pDstPkg->i_ParkSourcePackage; // use assigned park pkg index
					else 
						iNewSrcPkg = iPackagerParkSrcePkg; // use package park pkg 64002
				}
			}
			else {
				// no index given so use current routed package or park - so no change
				iNewSrcPkg = pDstPkg->i_Routed_Src_Package;
			}
			// other parameters - Nevion priority
			int iNevPri = ssl.getNamedParam("pri").toInt();
			if ((iNevPri < 1) || (iNevPri>5)) iNevPri = 3;
			pDstPkg->iNevionPriorityLevel = iNevPri;
			// xxx take special options -- how to perserve when diff selections made ???
			pDstPkg->i_CmdRoute_Level = ssl.getNamedParam("v_level").toInt();
			pDstPkg->i_CmdRoute_LangUse_Tag = ssl.getNamedParam("tag").toInt();
			pDstPkg->i_CmdAudio_Returns = ssl.getNamedParam("returns").toInt();

			if ((iNewSrcPkg >= i_Start_VirtualPackages) && (iNewSrcPkg < (iNumberOfPackages+1))) bVirtualSrcPkg = TRUE;
			if ((iNewSrcPkg != iPrevSrcPkg) || (iNewSrcPkg == iPackagerParkSrcePkg))
				bChangedPackage = TRUE;

			// test for attempting to route a virtual into itself
			// also check that if this virtual is going to another virtual with itself inalready, eg 2001->2002; when 2002->2001 already
			if (bVirtualDestPkg&&bVirtualSrcPkg) {
				// test 1 - routing itself to itself
				if (iNewSrcPkg == iPkgIndex) {
					Debug("ProcessDestPkgCmd - ERROR- rejecting virtual routing %d to itself %d ", iNewSrcPkg, iPkgIndex);
					return FALSE;
				}
				// test 2 - try and see if vir is used in chain where it is already being used
				bFoundVirtualSourceAhead = FALSE;
				iRecursiveCounter = 0;
				TraceVirtualForward(iNewSrcPkg, iPkgIndex);
				if (bFoundVirtualSourceAhead) {
					Debug("ProcessDestPkgCmd - ERROR- rejecting virtual routing %d FORWARD TRACE FAILED %d ", iNewSrcPkg, iPkgIndex);
					return FALSE;
				}
			}

			if (bChangedPackage)  {

				// ---------------------- sort out old package routing, comms etc ----------------------
				// get downstream list - includes current dest pkg - to simplify processing
				ssl_RoutedDestPackages = bncs_stringlist("", ',');
				ssl_RoutedDestPackages.append(iPkgIndex); // add current dest package into list - to aid processing
				if (bVirtualDestPkg) {
					// now get list of all dests routed from this virtual src ( from current virtual dest )
					iRecursiveCounter = 0;
					BuildDownStreamDestList(iPkgIndex);
					if (bShowAllDebugMessages) Debug("ProcDestPkg -1- BuildDownStrL has %d entries %s ", ssl_RoutedDestPackages.count(), LPCSTR(ssl_RoutedDestPackages.toString()));
				}

				// old trace go back along and resolve directly / finally routed to 
				bncs_stringlist ssl_oldTrace = bncs_stringlist(pDstPkg->ss_Trace_To_Source, '|');
				if (bShowAllDebugMessages) Debug("ProcDestPkg - old trace has %d entries %s ", ssl_oldTrace.count(), LPCSTR(ssl_oldTrace.toString()));

				// do all tidying up here - comms routing, ifbs, keys etc --- uses downstream list just generated
				for (iEntry = 0; iEntry < ssl_RoutedDestPackages.count(); iEntry++) {
					CDestinationPackages* pThisDest = GetDestinationPackage(ssl_RoutedDestPackages[iEntry].toInt());
					if (pThisDest) {
						// tidy up old package links first -- eg MM que, ifbs etc 
						// sort out the dest package links to previous src package 
						for (int iios = ssl_oldTrace.count() - 1; iios >= 0; iios--) {
							int iOldSrcPkg = ssl_oldTrace[iios].toInt();
							if ((iOldSrcPkg > 0) && (iOldSrcPkg < (iNumberOfPackages+1)) && (iOldSrcPkg != iNewSrcPkg)) {
								CSourcePackages* pOldSrc = GetSourcePackage(pThisDest->i_Traced_Src_Package);
								if (pOldSrc) {
									// go thru all assigned APs - check MMQ for dest index - if found then REMOVE from queue
									// check main 4 APs 
									for (int iiMap = 0; iiMap < MAX_PKG_VIDEO; iiMap++) {
										if (pOldSrc->st_SourceAudioLevels[iiMap].iAssocIndex>0) {
											CAudioPackages* pAudio = GetAudioPackage(pOldSrc->st_SourceAudioLevels[iiMap].iAssocIndex);
											if (pAudio) {
												if (pAudio->ssl_Pkg_Queue.find(bncs_string(iPkgIndex)) >= 0) {
													bncs_string ssQcmd = bncs_string("REMOVE=%1").arg(iPkgIndex);
													ProcessMMStackCommand(pAudio, ssQcmd);
													ProcessNextCommand(4);
												}
											}
										}
									}
									// check 32 other APs
									for (int iiAP = 0; iiAP < MAX_PKG_AUDIO; iiAP++) {
										if (pOldSrc->m_iAssoc_Audio_Packages[iiAP] > 0) {
											CAudioPackages* pAudio = GetAudioPackage(pOldSrc->m_iAssoc_Audio_Packages[iiAP]);
											if (pAudio) {
												if (pAudio->ssl_Pkg_Queue.find(bncs_string(iPkgIndex)) >= 0) {
													bncs_string ssQcmd = bncs_string("REMOVE=%1").arg(iPkgIndex);
													ProcessMMStackCommand(pAudio, ssQcmd);
													ProcessNextCommand(4);
												}
											}
										}
									}
								}
								// update from old pkg ptis
								RemoveDestinationPackageFromSourcePTIList(pThisDest->iPackageIndex, iOldSrcPkg);
								RemoveDestinationPackageFromTracedPTIList(pThisDest->iPackageIndex, iOldSrcPkg);
								// update gui if required
								if (iChosenSrcPkg == iOldSrcPkg) DisplaySourcePackageData();
							}
						} // for iios

					}
					else
						Debug("ProcDeskPkg - invalid dest pkg from DownStreamList %d ", ssl_RoutedDestPackages[iEntry].toInt());

				} // for ientry -- tidy up

				//------------------------------ new package from here -----------------
				// assign new package variables
				pDstPkg->i_Routed_Src_Package = iNewSrcPkg;
				AddDestinationPackageToSourcePTIList(pDstPkg->iPackageIndex, iNewSrcPkg);

				// update all OTHER downstream dests etc with their own NEW trace and pti
				for (iEntry = 0; iEntry < ssl_RoutedDestPackages.count(); iEntry++) {
					CDestinationPackages* pThisDest = GetDestinationPackage(ssl_RoutedDestPackages[iEntry].toInt());
					if (pThisDest) {
						// update new src pkg routed to list  
						ssl_Calc_Trace = bncs_stringlist("", '|');
						int iTracedSourcePackage = TraceRealSourcePackageCalcTrace(pThisDest->iPackageIndex);
						pThisDest->i_Traced_Src_Package = iTracedSourcePackage;
						if (ssl_Calc_Trace.count() == 0) ssl_Calc_Trace.append("0");
						pThisDest->ss_Trace_To_Source = ssl_Calc_Trace.toString('|');

						// now go down thru trace and if not a virt dest then add into finally routed to list of all src pkgs
						if ((pThisDest->iPackageIndex > 0) && (pThisDest->iPackageIndex < i_Start_VirtualPackages)) {
							for (int iins = ssl_Calc_Trace.count() - 1; iins >= 0; iins--) {
								int iNSrcPkg = ssl_Calc_Trace[iins].toInt();
								CSourcePackages* pNSrc = GetSourcePackage(iNSrcPkg);
								if (pNSrc) {   // for final real destys these need adding to the traced lists provided not directly routed to
									if (pNSrc->ssl_Pkg_DirectlyRoutedTo.find(bncs_string(pThisDest->iPackageIndex)) < 0) {
										AddDestinationPackageToTracedPTIList(pThisDest->iPackageIndex, iNSrcPkg);
									}
								}
							} // for iins
						}
					}
				} // for ientry all other 

				// process NEW command and new source package  - T video starts with routed pkg in case of virtual overrides in video
				// 4. PROCESS SDI COMPONENT OF DEST PACKAGE   --  Process all downstream video -- tracing back required source 
				// ********uses previously list of downstream pkgs -- first one in list is the current dest pkg anyway ******
				//if (bShowAllDebugMessages) Debug("ProcDestPkg -3- StartProcRtring list has %d entries %s ", ssl_RoutedDestPackages.count(), LPCSTR(ssl_RoutedDestPackages.toString()));
				for (iEntry = 0; iEntry < ssl_RoutedDestPackages.count(); iEntry++) {
					//Debug("ProcDestPkg -4- ENTRY %d StartProcRtring list has %d entries %s ", iEntry, ssl_RoutedDestPackages.count(), LPCSTR(ssl_RoutedDestPackages.toString()));
					CDestinationPackages* pThisDest = GetDestinationPackage(ssl_RoutedDestPackages[iEntry].toInt());
					if (pThisDest) {
						ProcessAllDestinationPackageRouting(pThisDest, iNevPri, FALSE);
						if ((pThisDest->iPackageIndex > 0) && (pThisDest->iPackageIndex < i_Start_VirtualPackages) &&
							(pThisDest->i_Routed_Src_Package >= i_Start_VirtualPackages) && (pThisDest->iPackageIndex < (iNumberOfPackages+1))) {
							if ((pThisDest->iPackageIndex == iPkgIndex) && (pThisDest->i_Routed_Src_Package != iPrevSrcPkg))
								UpdateRoutedVirtualsVLevel(pThisDest->iPackageIndex, iPrevSrcPkg, pThisDest->i_Routed_Src_Package);
							else
								UpdateRoutedVirtualsVLevel(pThisDest->iPackageIndex, 0, pThisDest->i_Routed_Src_Package);
						}
					}
				}

				// any comms processing for NEW Source / APs therein - would be done here

				// all done now - set revertive for slot  -- add in new levels, legs and other changes  
				UpdateDestPkgRouteCommandInfodriver(iPkgIndex);

				// update gui for changes to packages too
				if (iChosenDestPkg == iPkgIndex) DisplayDestinationPackageData(FALSE);
				if (iChosenSrcPkg == iNewSrcPkg) DisplaySourcePackageData();

				return TRUE;

			}
			else {
				// package has not changed but reassert video and audio ????
				// AS NEW DO THE MININMUM -- so DO NOT REASSERT OR PROCESS VIDEO FOR PACKAGES downstream
				Debug("ProcDeskPkg -7- %d no change to routed package %d - SO now nothing to do ", iPkgIndex, pDstPkg->i_Routed_Src_Package);
				ProcessAllDestinationPackageRouting(pDstPkg, 3, FALSE);
				// clear previous routed pkg of v_index; & if curr src is a virtual then put v_index in there too -- for SL
				if ((iPkgIndex > 0) && (iPkgIndex < i_Start_VirtualPackages) && (iNewSrcPkg >= i_Start_VirtualPackages)) UpdateRoutedVirtualsVLevel(iPkgIndex, 0, iNewSrcPkg);
				// 7. FINISH - JUST set revertive for command slot - build and send response  	
				UpdateDestPkgRouteCommandInfodriver(iPkgIndex);

				// update gui for changes to packages too
				if (iChosenDestPkg == iPkgIndex) DisplayDestinationPackageData(FALSE);
				return TRUE;
			}
		}
		else
			Debug("ProcessDestPkgCmd - ERROR invalid package passed ");
		return FALSE;

	}
	catch (structured_exception const & e)
	{
		Debug("%s:ProcessDestPkgCmd() - thrown exception %x from %x", szAppName, e.what(), e.where());
	}
	catch (...)
	{
		Debug("%s:ProcessDestPkgCmd() - Unknown exception ", szAppName);
	}
	return FALSE;

}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  redo all labels for IFBs / OSs for Label changes only for all dest packages that has this src package routed to it
//
void ReAssignLabelForIFBS(CAudioPackages* pAudio, int iWhichIFB)
{
	if (pAudio)  {
		if (pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb > 0) {
			int iIFBRecord = pAudio->stCFIFBs[iWhichIFB].i_DynAssignedIfb;
			CRingMaster_IFB* pRMIfb = GetRingMaster_IFB_Record(iIFBRecord);
			if (pRMIfb) {
				CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(pRMIfb->getAssignedTrunkAddr());
				if (pRing) {
					int iIFB = ((iIFBRecord - 1) % MAX_IFBS) + 1;
					// get label
					char szFinalLabel[MAX_AUTO_BUFFER_STRING] = "";
					strcpy(szFinalLabel, pAudio->stCFIFBs[iWhichIFB].m_ss_MainLabel);
					//
					int iWhichIFBInfoDrv = i_RingMaster_Ifb_Start + pRing->m_iRecordIndex - 1;
					if ((iWhichIFBInfoDrv >= i_RingMaster_Ifb_Start) && (iWhichIFBInfoDrv <= i_RingMaster_Ifb_End)) {
						char szCommand[MAX_AUTO_BUFFER_STRING] = "";
						wsprintf(szCommand, "IW %d '&LABEL=%s' %d", iWhichIFBInfoDrv, szFinalLabel, iIFB);
						AddCommandToQue(szCommand, INFODRVCOMMAND, iWhichIFBInfoDrv, 0, 0);
					}
					else
						Debug("ReAssignLabelForIFBS - ERROR - invalid ifb info %d from ifb rec %d ring %d", iWhichIFBInfoDrv, iIFB, pRing->m_iRecordIndex);
					//
					//RM for db12 subtitle for real IFB index on ring 
					SendIFBRMSubtitleLabel(pAudio->stCFIFBs[iWhichIFB].m_ss_SubTitleLabel, iIFBRecord);
					//
					// xxx add label to any assigned keys too - cant remember if Riedel driver updates keys with ifb functions ???
				}
			}
		}
	}
	else
		Debug("ReAssignLabelForIFBS - invalid class passed in");
}


void AssignDestinationIFBtoSrcIFBS( CAudioPackages* pAPkg )
{
	// top of MM Q - dest pkg - call createfull ifb cmd for all relevent ifbs in audio pkg if ports assigned in dest pkg ifbs 1+2  // xxx all 4 ???
	if (pAPkg) {
		for (int iifb = 1; iifb <= MAX_PKG_CONFIFB; iifb++) {
			if ((pAPkg->stCFIFBs[iifb].i_DynAssignedIfb > 0) && (pAPkg->stCFIFBs[iifb].i_DynAssignedIfb < MAX_IFBS) && (pAPkg->ssl_Pkg_Queue.count() > 0)) {
				CreateAndQueueFullIFBCommand(pAPkg->stCFIFBs[iifb].i_DynAssignedIfb, TRUE);
				// add CF conf to any known keys assoc to this ifb 
				if ((pAPkg->stCFIFBs[iifb].i_DynAssignedConference > 0) && (pAPkg->stCFIFBs[iifb].i_DynAssignedConference < MAX_CONFERENCES)) {
					ApplyCFConferenceToIFBKeys(pAPkg->stCFIFBs[iifb].i_DynAssignedIfb, pAPkg->stCFIFBs[iifb].i_DynAssignedConference);
				}
			}
		}
	}
	else
		Debug("AssignDestinationIFBtoSrcIFBS invalid class passed");
}


void AssignMMQCFConferences(CAudioPackages* pAudio)
{
	if (pAudio) {
		// Which of the 4 ifb should conf  be assigned to ???   pAudio->stCFIFBs[iifb].i_Use_Which_DestPkgIFB
		for (int iifb = 1; iifb < MAX_PKG_CONFIFB; iifb++) {
			if ((pAudio->stCFIFBs[iifb].i_DynAssignedIfb>0) && (pAudio->stCFIFBs[iifb].i_DynAssignedConference == 0) && 
				(pAudio->stCFIFBs[iifb].i_Use_Which_DestPkgIFB>0) && (pAudio->ssl_Pkg_Queue.count()>0)) {
				// get CF conf for this ifb
				bncs_string sscmd = bncs_string("GET=CF%1_CONF").arg(iifb);
				ProcessDynamicConferenceCommand(pAudio->m_iAudioPkg_Indx, sscmd);
			}
		}
	}
	else
		Debug("AssignMMQCFConferences error in class passed");
}


void UpdateDestinationPackageForMMQ(int iDestPkg, int iAudioPkgIndex, int iActionAddorRemove )
{
	CDestinationPackages* pDest = GetDestinationPackage(iDestPkg);
	if (pDest) {
		if (iActionAddorRemove > 0) {
			// adding to list
			if (pDest->ssl_AtTopOfAudioPkgMMQ.find(bncs_string(iAudioPkgIndex)) < 0) {
				pDest->ssl_AtTopOfAudioPkgMMQ.append(bncs_string(iAudioPkgIndex));
			}
		}
		else if (iActionAddorRemove < 0) {
			int iPos = pDest->ssl_AtTopOfAudioPkgMMQ.find(bncs_string(iAudioPkgIndex));
			if (iPos >= 0) pDest->ssl_AtTopOfAudioPkgMMQ.deleteItem(iPos);
		}
	}
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
void ProcessMMStackCommand( CAudioPackages* pPkg, bncs_string ssCmd )
{

	bncs_stringlist ssl_Cmd = bncs_stringlist( ssCmd.upper(), '=' );
	CRingMaster_CONF* pIFBConf = NULL;
	CDestinationPackages* pDPkg = NULL;
	int iDestPkg=0;

	if ((pPkg)&&(ssl_Cmd.count()>0)) {
		int iPkgIndex = pPkg->m_iAudioPkg_Indx;
		// parse command 
		if (ssl_Cmd[0].find("ADD")>=0) {
			// add dst pkg to queue
			if (ssl_Cmd.count()>1) {
				iDestPkg = ssl_Cmd[1].toInt();
				if ((iDestPkg>0)&&(iDestPkg<=iNumberOfPackages))  {
					if (pPkg->ssl_Pkg_Queue.find(iDestPkg)<0) {
						// only add if not already in que
						pPkg->ssl_Pkg_Queue.append( iDestPkg );
						if (pPkg->ssl_Pkg_Queue.count()==1) {
							UpdateDestinationPackageForMMQ(iDestPkg, pPkg->m_iAudioPkg_Indx, ADDTOLIST);
							// set up ifb conf if not there already
							AssignMMQCFConferences(pPkg);
							// add in dest pkg ifb ports
							AssignDestinationIFBtoSrcIFBS(pPkg);
							// process rev vision if assigned 
							if ((pPkg->iEnabledRevVision>0) && (pPkg->st_Reverse_Vision.iAssocIndex>0)) 
								ProcessAudioPackageReverseRouting(pPkg->m_iAudioPkg_Indx, 1);						}
					}
					else
						Debug( "ProcessMMStackCommand - ADD Command - dest package %d already in que", iDestPkg );
				}
				else 
					Debug( "ProcessMMStackCommand - ADD Command - invalid package number %d ", iDestPkg );
			}
			else 
				Debug( "ProcessMMStackCommand - ADD Command - missing package number ");
		}

		else if (ssl_Cmd[0].find("SET_TOP")>=0) {
			// drop one on the top to 2nd place and replace with entry specified 
			if (ssl_Cmd.count()>1) {
				int iNewTopPkg = ssl_Cmd[1].toInt();
				if ((iNewTopPkg>0)&&(iNewTopPkg<=iNumberOfPackages))  {
					if (pPkg->ssl_Pkg_Queue.count()>0) {
						UpdateDestinationPackageForMMQ(pPkg->ssl_Pkg_Queue[0].toInt(), pPkg->m_iAudioPkg_Indx, REMOVEFROMLIST);
						int iPos = pPkg->ssl_Pkg_Queue.find( iNewTopPkg );
						if (iPos>0) pPkg->ssl_Pkg_Queue.deleteItem( iPos );	
						// only set top if not
						if (iPos!=0) {
							// as more than one top item no need to clear any cf confs - keep for new top; check rev vision 
							// set up new top pkg
							pPkg->ssl_Pkg_Queue.prepend(iNewTopPkg);
							// as queue  has new top item then process it  set up ifb conf
							UpdateDestinationPackageForMMQ(iNewTopPkg, pPkg->m_iAudioPkg_Indx, ADDTOLIST);
							// check / set up ifb conf if not there already for new top 
							AssignMMQCFConferences(pPkg);
							//
							AssignDestinationIFBtoSrcIFBS(pPkg);
							// process rev vision if assigned 
							if ((pPkg->iEnabledRevVision>0) && (pPkg->st_Reverse_Vision.iAssocIndex>0))
								ProcessAudioPackageReverseRouting(pPkg->m_iAudioPkg_Indx, 1);
						}
					}
					else { // as que empty - add to top
						pPkg->ssl_Pkg_Queue.prepend(iNewTopPkg);
						UpdateDestinationPackageForMMQ(iNewTopPkg, pPkg->m_iAudioPkg_Indx, ADDTOLIST);
						// as queue  has new top item then process it  set up ifb conf etc
						AssignMMQCFConferences(pPkg);
						// add in dest pkg ifb ports
						AssignDestinationIFBtoSrcIFBS(pPkg);
						// process rev vision if assigned 
						if ((pPkg->iEnabledRevVision>0) && (pPkg->st_Reverse_Vision.iAssocIndex>0))
							ProcessAudioPackageReverseRouting(pPkg->m_iAudioPkg_Indx, 1);
					}
				}
				else 
					Debug( "ProcessMMStackCommand - SET_TOP Command - invalid package number %d ", iNewTopPkg );
			}
			else 
				Debug( "ProcessMMStackCommand - SET_TOP Command - missing package number ");
			//		
		}

		else if ((ssl_Cmd[0].find("REMOVE")>=0)||(ssl_Cmd[0].find("DROP")>=0)||(ssl_Cmd[0].find("DROP_TOP")>=0)) {
			// remove dst pkg from queue - if top then move next to top
			int iDPPos = 0;
			if (ssl_Cmd[0].find("DROP_TOP")>=0) {
				if (pPkg->ssl_Pkg_Queue.count()>0) iDestPkg = pPkg->ssl_Pkg_Queue[0].toInt();
				iDPPos = 0;
			}
			else if (ssl_Cmd.count()>1) { // remove and drop
				iDestPkg = ssl_Cmd[1].toInt();
			}
			if ((iDestPkg>0)&&(iDestPkg<=iNumberOfPackages))  {
				iDPPos = pPkg->ssl_Pkg_Queue.find(iDestPkg);
				if (iDPPos == 0) {
					if (pPkg->ssl_Pkg_Queue.count()==1) {  // DROP TOP and is only item in queue
						// if at top and only item in que - now need to clear ifb
						ClearAudioPackageMMQueue(pPkg);
					}
					else if (pPkg->ssl_Pkg_Queue.count()>1) { // DROPING/REMOVING ITEM but que has new top item
						UpdateDestinationPackageForMMQ(iDestPkg, pPkg->m_iAudioPkg_Indx, REMOVEFROMLIST);
						pPkg->ssl_Pkg_Queue.deleteItem(iDPPos);
						// as queue  has new top item then process it  set up ifb conf etc
						UpdateDestinationPackageForMMQ(pPkg->ssl_Pkg_Queue[0].toInt(), pPkg->m_iAudioPkg_Indx, ADDTOLIST);
						AssignMMQCFConferences(pPkg);
						// add in dest pkg ifb ports
						AssignDestinationIFBtoSrcIFBS(pPkg);
						// process rev vision if assigned 
						if ((pPkg->iEnabledRevVision>0) && (pPkg->st_Reverse_Vision.iAssocIndex>0))
							ProcessAudioPackageReverseRouting(pPkg->m_iAudioPkg_Indx, 1);
					}
				}
				else if (iDPPos>0) {
					// delete item being dropped / removed from queue
					pPkg->ssl_Pkg_Queue.deleteItem(iDPPos);
				}
				else
					Debug( "ProcessMMStackCommand - REMOVE Command - package %d not in queue", iDestPkg );
				//		
			}
			else 
				Debug( "ProcessMMStackCommand - REMOVE Command %s - invalid dest package %d ", LPCSTR(ssCmd), iDestPkg );
		}

		else if (ssl_Cmd[0].find("SWAP")>=0) {
			// swaps current top and next item - but keeps item in queue
			if (pPkg->ssl_Pkg_Queue.count()>1) {
				UpdateDestinationPackageForMMQ(pPkg->ssl_Pkg_Queue[0].toInt(), pPkg->m_iAudioPkg_Indx, REMOVEFROMLIST);
				int iNewTop = pPkg->ssl_Pkg_Queue[1].toInt();
				pPkg->ssl_Pkg_Queue.deleteItem(1);
				pPkg->ssl_Pkg_Queue.prepend(iNewTop);
				UpdateDestinationPackageForMMQ(pPkg->ssl_Pkg_Queue[0].toInt(), pPkg->m_iAudioPkg_Indx, ADDTOLIST);
				// as queue  has new top item then process it  set up ifb conf etc
				AssignMMQCFConferences(pPkg);
				// add in dest pkg ifb ports
				AssignDestinationIFBtoSrcIFBS(pPkg);
				// process rev vision if assigned 
				if ((pPkg->iEnabledRevVision>0) && (pPkg->st_Reverse_Vision.iAssocIndex>0))
					ProcessAudioPackageReverseRouting(pPkg->m_iAudioPkg_Indx, 1);
			}
			else 
				Debug( "ProcessMMStackCommand - SWAP command - que too short to swap");
			//		
		}

		else if (ssl_Cmd[0].find("NUDGE_UP")>=0) {
			// nudge up chosen pkg  - but keeps item in queue
			if (pPkg->ssl_Pkg_Queue.count()>2) {
				if (ssl_Cmd.count()>1) {
					iDestPkg = ssl_Cmd[1].toInt();
					// find dst pkg in que - check it is 3rd or more in q
					if ((iDestPkg>0)&&(iDestPkg<=iNumberOfPackages))  {
						int iPos = pPkg->ssl_Pkg_Queue.find( iDestPkg );
						if (bShowAllDebugMessages) Debug("ProcessIFBQ - Nudgeup - pkg:%d pos %d que %d", iDestPkg, iPos, pPkg->ssl_Pkg_Queue.count() );
						int iMinPos = 1;
						if ((iPos>iMinPos)&&(iPos<pPkg->ssl_Pkg_Queue.count())) {
							int iOthPkg = pPkg->ssl_Pkg_Queue[iPos-1];
							pPkg->ssl_Pkg_Queue[iPos-1] = iDestPkg;
							pPkg->ssl_Pkg_Queue[iPos] = iOthPkg;
						}
						else 
							Debug( "ProcessMMStackCommand - NUDGE UP command - dst pkg %d too close to top or sharing", iDestPkg);
					}
					else
						Debug( "ProcessIFBQCmd - Nudge_up invalid dest package number, %d", iDestPkg );
				}
				else
					Debug( "ProcessIFBQCmd - Nudge_up missing dest package number");
			}
			else 
				Debug( "ProcessMMStackCommand - NUDGE UP command - que too short to nudge up");
			//		
		}
		
		else if (ssl_Cmd[0].find("NUDGE_DOWN")>=0) {
			// nudge down chosen pkg
			if (pPkg->ssl_Pkg_Queue.count()>2) {
				if (ssl_Cmd.count()>1) {
					iDestPkg = ssl_Cmd[1].toInt();
					// find dst pkg in que - check it is NOT THE TOP or the bottom entry
					if ((iDestPkg>0)&&(iDestPkg<=iNumberOfPackages))  {
						int iPos = pPkg->ssl_Pkg_Queue.find( iDestPkg );
						if (bShowAllDebugMessages) Debug("ProcessIFBQ - NudgeDwn - pkg:%d pos %d que %d", iDestPkg, iPos, pPkg->ssl_Pkg_Queue.count() );
						int iMinPos = 0;
						if ((iPos>iMinPos)&&(iPos<(pPkg->ssl_Pkg_Queue.count()-1))) {
							int iOthPkg = pPkg->ssl_Pkg_Queue[iPos+1];
							pPkg->ssl_Pkg_Queue[iPos+1] = iDestPkg;
							pPkg->ssl_Pkg_Queue[iPos] = iOthPkg;
						}
						else 
							Debug( "ProcessMMStackCommand - NUDGE DOWN command - dst pkg %d either at top, sharing or bottom", iDestPkg);
					}
					else
						Debug( "ProcessIFBQCmd - Nudge_down invalid dest package number, %d", iDestPkg );
				}
				else
					Debug( "ProcessIFBQCmd - Nudge_down missing dest package number");
			}
			else 
				Debug( "ProcessMMStackCommand - NUDGE UP command - que too short to nudge up");
		}
		
		else if (ssl_Cmd[0].find("CLEAR")>=0) {
			// clear the whole queue
			ClearAudioPackageMMQueue(pPkg);
		}

		else { 
			Debug( "ProcessMMStackCommand - NO Keyword Found - data from slot - %s ", LPCSTR(ssCmd));
		}
		//		
		UpdateAudioPackageMMQInfodriver(pPkg);
				
		ProcessNextCommand(10);
		// update gui for changes to packages too
		if (iChosenSrcPkg == pPkg->m_iAudioPkg_Indx) DisplayAudioSourcePackageData();

	}
	else 
		Debug( "ProcessMMStackCommand - ERROR invalid package or bad command");

}


/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: DetermineWhichInfoType() is notification from a valid infodriver
//

BOOL DetermineWhichInfoType( int iDeviceNumber, int iIncomingSlot, int *iWhichInfoType, int *iCalcPackageIndex )
{
	if (iDeviceNumber == iPackager_riedel_conference_main) {
		*iWhichInfoType = AUTO_RIEDEL_CONFERENCE_INFO;
		*iCalcPackageIndex = iIncomingSlot;
		return TRUE;
	}
	else if (iDeviceNumber == iPackager_riedel_port_use_main) {
		*iWhichInfoType = AUTO_RIEDEL_PORT_USE_INFO;
		*iCalcPackageIndex = iIncomingSlot;
		return TRUE;
	}
	else if (iDeviceNumber == iPackager_riedel_ifbs_main) {
		*iWhichInfoType = AUTO_RIEDEL_IFBS_INFO;
		*iCalcPackageIndex = iIncomingSlot;
		return TRUE;
	}
	else {
		for (int iis = AUTO_PACK_ROUTE_INFO; iis <= AUTO_RTR_DEST_USE_INFO; iis++) {
			for (int iib = 1; iib <= iNumberInfosInBlock; iib++) {
				int iIndex = ((iis - 1)*iNumberInfosInBlock) + iib;
				if ((iIndex > 0) && (iIndex < MAX_INFODRIVERS)) {
					if (i_rtr_Infodrivers[iIndex] == iDeviceNumber) {
						//Debug("DetInfoType %d %d -> type %d pkg %d ", iDeviceNumber, iIncomingSlot, iis, ((iib - 1)*iInfodriverDivisor) + iIncomingSlot);
						*iWhichInfoType = iis;
						*iCalcPackageIndex = ((iib - 1)*iInfodriverDivisor) + iIncomingSlot;
						return TRUE;
					}
				}
			} // for iib
		} // for iis
	}
	// the two new infodrivers for SRC-DEST and TB-STATUS are both read only 
	//     - ie auto will not respond to any writes / slot changes to these infodrivers
	*iWhichInfoType = 0;
	*iCalcPackageIndex = 0;
	return FALSE;
	
}

///////////////////////////////////////////////////////////////////////
//
//
void ProcessParkSourceRouted(bncs_string ssData)
{
	// go thru and park all destination packages using this source
	int iSrcPkg = ssData.toInt();
	if ((iSrcPkg>0) && (iSrcPkg<=iNumberOfPackages)) {
		CSourcePackages* pSrc = GetSourcePackage(iSrcPkg);
		if (pSrc) {
			Debug("ProcessParkSourcePkg -- PARKING users of src pkg %d ", iSrcPkg);
			int iRtrTo = pSrc->ssl_Pkg_DirectlyRoutedTo.count();
			bncs_stringlist ssl_Users = pSrc->ssl_Pkg_DirectlyRoutedTo;
			for (int iD = 0; iD<iRtrTo; iD++) {
				int iDestPkg = ssl_Users[iD].toInt();
				if ((iDestPkg>0) && (iDestPkg < (iNumberOfPackages+1))) {
					Debug("ProcessParkSourcePkg -- PARKING dest pkg %d", iDestPkg);
					CDestinationPackages* pDest = GetDestinationPackage(iDestPkg);
					if (pDest) {
						bncs_string sstr = bncs_string("index=%1").arg(iPackagerParkSrcePkg);
						if (pDest->iPackageLockState == 0) 
							ProcessDestinationPkgCommand(pDest, sstr);
						else
							Debug("ProcessParkSourceRouted - PackageRouting - LOCKED Dest Pkg %d - routing ignored", pDest->iPackageIndex);
					}
				}
			}
		}
		else
			Debug("ProcessParkSrcRouted - invalid src pkg %d ", iSrcPkg);
	}
}


void ProcessDestPackageLockState(CDestinationPackages* pPkg, bncs_string ssData)
{
	if (pPkg) {
		int iLockStatus = 0;
		if (ssData.find("=") >= 0) {
			bncs_string ssleft = "", ssState = "";
			ssData.split('=', ssleft, ssState);
			iLockStatus = ssState.toInt();
		}
		else {
			iLockStatus = ssData.toInt();
		}
		pPkg->iPackageLockState = iLockStatus;
		UpdateDestPkgSdiStatusInfodriverSlot(pPkg->iPackageIndex, TRUE);
		if (pPkg->iPackageIndex == iChosenDestPkg) 	{
			if (pPkg->iPackageLockState > 0)
				SetDlgItemText(hWndDlg, IDC_DPKG_LOCKED, "LOK");
			else
				SetDlgItemText(hWndDlg, IDC_DPKG_LOCKED, "");
		}
	}
	else
		Debug("ProcDestPkgLock - invalid package class");

}



/////////////////////////////////////////////////////////////////////////
//
//  CSI DRIVER  FUNCTIONS
//
///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

int CalculateRandomOffset()
{
	// used to create a time offset before calling TXRX function - to stop multiInfodriver tx clash
	int iRand1 = 0;
	srand(time(0)); // generate random seed point
	iRand1 = (rand() % 2007);
	return (iRand1);
}


//
//  FUNCTION: InfoNotify()
//
//  PURPOSE: Callback function from an infodriver, notifying event
//
//  COMMENTS: the pointer pex references the infodriver which is notifying
//			  iSlot and szSlot are provided for convenience
//			
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot)
{

	if (pex&&szSlot) {

		int iAssocPkgIndex = 0;
		int iWhichInfoDriverType = 0;
		if (DetermineWhichInfoType(pex->iDevice, iSlot, &iWhichInfoDriverType, &iAssocPkgIndex)) {
			switch (pex->iStatus) {
			case CONNECTED: // this is the "normal" situation
				if (bShowAllDebugMessages)
					Debug("InfoNotify -INFO type %d  device %d slot %d changed to %s", iWhichInfoDriverType, pex->iDevice, iSlot, szSlot);

				if (!SlotChange(iWhichInfoDriverType, iAssocPkgIndex, pex->iDevice, iSlot, szSlot))
					pex->setslot(iSlot, szSlot);
				break;

			case DISCONNECTED:
				Debug("Infodriver %d has disconnected-PackagerAuto CLOSING DOWN", pex->iDevice);
				eiInfoDrivers[AUTO_PACK_ROUTE_INFO].updateslot(COMM_STATUS_SLOT, "0");	 // error	
				bValidCollediaStatetoContinue = FALSE;
				PostMessage(hWndMain, WM_CLOSE, 0, 0);
				return;
				break;

			case TO_RXONLY:
				if ((pex->iDevice == iPackager_router_main) && (iOverallTXRXModeStatus == IFMODE_TXRX) && (iOverallStateJustChanged == 0)) {
					Debug("Infodriver %d received request to go RX Only -- bouncing request ", pex->iDevice);
					eiInfoDrivers[AUTO_PACK_ROUTE_INFO].setmode(IFMODE_TXRX);
					eiInfoDrivers[AUTO_PACK_ROUTE_INFO].requestmode = TO_TXRX;
					iOverallModeChangedOver = 4;
				}
				break;

			case TO_TXRX:
				if ((pex->iDevice == iPackager_router_main) && (iOverallTXRXModeStatus == IFMODE_RXONLY) && (iOverallStateJustChanged == 0)) {
					iOverallStateJustChanged = 1;
					int iTimerOffset = CalculateRandomOffset();
					SetTimer(hWndMain, FORCETXRX_TIMER_ID, iTimerOffset, NULL);
					SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "Req to Tx/Rx to main ");
					if (bShowAllDebugMessages) Debug("InfoNotify - force go TXRX - in %d ms", iTimerOffset);
				}
				break;

			case QUERY_TXRX:
			case 2190:
				if (iLabelAutoStatus == 3) Debug("Infodriver %d received Query_TXRX", pex->iDevice);
				// ignore this state -- means all left as it was 
				break;

			default:
				Debug("iStatus=%d", pex->iStatus);
			} // switch
		}
		else {
			Debug("InfoNotify - unknown infodriver %d slot %d - input ignored", pex->iDevice, iSlot);
		}
	}
	else
		Debug("InfoNotify -error- invalid pex class or szslot ");

}


//
//  FUNCTION: SlotChange()
//
//  PURPOSE: Function called when slot change is notified
//
//  COMMENTS: All we need here are the driver number, the slot, and the new contents
//		
//	RETURNS: TRUE for processed (don't update the slot),
//			 FALSE for ignored (so calling function can just update the slot 
//								like a normal infodriver)
//			

BOOL SlotChange(int iWhichInfoType, int iAssocPkgIndex, UINT iDevice,UINT iSlot, LPCSTR szSlot)
{
	// slots 1..1999 are for destination package routing cmds and responses

	switch (iWhichInfoType) {
		case AUTO_PACK_ROUTE_INFO: 
		{
			 if ((iSlot > 0) && (iSlot<4001)) {
				 if ((iAssocPkgIndex>0) && (iAssocPkgIndex<=iNumberOfPackages)) {
					 // process change for the appropriate dest package
					 CDestinationPackages* pPkg = GetDestinationPackage(iAssocPkgIndex);
					 if (pPkg) {
						 if (pPkg->iPackageLockState==0)
							 ProcessDestinationPkgCommand(pPkg, szSlot);
						 else
							 Debug("SlotChange - PackageRouting - LOCKED Dest Pkg %d - routing ignored", iAssocPkgIndex);
					 }
					 else
						 Debug("Slotchange PKAGE RTR function no package for slot %d ", iAssocPkgIndex);
				 }
				 else
					 Debug("Slotchange PKAGE RTR function ignored - dest pkg %d  - slot %d has NOT changed to '%s'", iAssocPkgIndex, iSlot, szSlot);
			 }
			 else if (iSlot == PARK_SOURCE_CMD_SLOT) {
				 ProcessParkSourceRouted(bncs_string(szSlot));
				 SendPackagerBNCSRevertive(iAssocPkgIndex, AUTO_PACK_ROUTE_INFO, FALSE, szSlot);
			 }
			 else if (iSlot == PARK_CMD_QUEUE_SLOT) {
				 bncs_string sstr = bncs_string(szSlot).upper();
				 if (sstr.find("PAUSE") >= 0) {
					 int iVal = sstr.toInt(); // if number preceeds PAUSE then this is set as value within reason
					 if ((iVal > 0) && (iVal < 130)) iPauseCommandProcessing = iVal;
					 else iPauseCommandProcessing = 45;
					 Debug("SlotChange - Command Queue Paused for %d seconds", iPauseCommandProcessing);
					 eiInfoDrivers[1].updateslot(iSlot, "CMD QUE PAUSED");
				 }
				 else if (sstr.find("RESUME") >= 0) {
					 iPauseCommandProcessing = 1;
					 Debug("SlotChange - Command Queue resumed");
					 eiInfoDrivers[1].updateslot(iSlot, "cmd que resumed");
					 // start on next main timer cycle
				 }
				 else if (sstr.find("CLEAR") >= 0) {
					 ClearCommandQueue();
					 iPauseCommandProcessing = 1;
					 Debug("SlotChange - Command Queue CLEARED");
					 eiInfoDrivers[1].updateslot(iSlot, "cmd que cleared");
				 }
			 }
			 else
				 Debug("Slotchange PKAGE RTR function outside device %d slot range %d ", iDevice, iSlot);
		}
		break;
		
		case AUTO_MM_STACK_INFO:
		{
			if ((iSlot > 0) && (iSlot<4001)) {
				if ((iAssocPkgIndex>0) && (iAssocPkgIndex < (iNumberOfPackages+1))) {
					// process queue management for src package
					CAudioPackages* pPkg = GetAudioPackage(iAssocPkgIndex);
					if (pPkg) {
						ProcessMMStackCommand(pPkg, szSlot);
					}
					else
						Debug("Slotchange MM_STACK function no src package '%d'", iAssocPkgIndex);
				}
				else
					Debug("Slotchange MM_STACK function ignored - audio pkg indx %d - slot %d has NOT changed to '%s'", iAssocPkgIndex, iSlot, szSlot);
			}
			else
				Debug("Slotchange MM_STACK function outside device %d slot range %d ", iDevice, iSlot);
		}
		break;

		case AUTO_AUDIO_PKG_INFO:
		{
			if ((iSlot > 0) && (iSlot<4001)) {
				if ((iAssocPkgIndex>0) && (iAssocPkgIndex < (iNumberOfPackages+1))) {
					// process audio pkg update
					ProcessAudioPackageInfodriverCommand(iAssocPkgIndex, szSlot);
				}
				else
					Debug("Slotchange AUDIO Pkg function ignored - A pkg indx %d - slot %d has NOT changed to '%s'", iAssocPkgIndex, iSlot, szSlot);
			}
			else
				Debug("Slotchange AUDIO pkg function outside device %d slot range %d ", iDevice, iSlot);
		}
		break;

		case AUTO_DEST_PKG_STATUS_INFO:
		{
			if ((iSlot > 0) && (iSlot<4001)) {
				if ((iAssocPkgIndex>0) && (iAssocPkgIndex < (iNumberOfPackages+1))) {
					// process lock status update for dest package
					CDestinationPackages* pPkg = GetDestinationPackage(iAssocPkgIndex);
					if (pPkg) {
						ProcessDestPackageLockState(pPkg, szSlot);
					}
					else
						Debug("Slotchange DPkg Lock function no package '%d'", iAssocPkgIndex);
				}
				else
					Debug("Slotchange DPkg Lock function ignored - dest pkg indx %d - slot %d has NOT changed to '%s'", iAssocPkgIndex, iSlot, szSlot);
			}
			else
				Debug("Slotchange DPkg Lock function outside device %d slot range %d ", iDevice, iSlot);
		}
		break;

		case AUTO_RIEDEL_CONFERENCE_INFO:
		{
			// this info used for CONF commands stuff if required --- not used as yet 
		}
		break;

		case AUTO_RIEDEL_IFBS_INFO:
		{
			// this info used for IFB commands stuff if required --- not used as yet -- cmds would mmust state which RING
		}
		break;

		default:
			// no user writeable slots - all changes ignored
			Debug("Slotchange Readonly infodrv %d changes ignored - slot %d has NOT changed ", iDevice, iSlot);
		break;

	}

	// all return true as under automatic control - no free user slots 
	return TRUE;

}



////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
//   CSI CLIENT FUNCTIONS FOR ROUTER REVERTIVES MESSAGES

BOOL ValidRouterDevice( int iDev, int *iWhichRtrType ) 
{
	*iWhichRtrType = UNKNOWNVAL;
	CRevsRouterData* pRtr = GetRouterRecord(iDev);
	if (pRtr) 
		return TRUE;
	else
		return FALSE;
}

BOOL StoreRouterRevertive(int iRtrNum, int iRtrDest, int iSource, int *iPrevSrc)
{
	// no longer RR revs
	*iPrevSrc = 0;
	return FALSE;
}

void ProcessRouterRevertive(int iRtrNum, int iRtrDest)
{
	// no longer RR revs
}


/////////////////////////////////////


BOOL StoreNevionRouterRevertive( int iBaseRtrNum, int iRevRtrNum, int iRtrDest, bncs_string ssRev  )
{
	try {

		CRevsRouterData* pRtr = GetRouterRecord(iBaseRtrNum);
		if (pRtr) {
			//
			int iBNCSDest = iRtrDest;
			iBNCSDest = (iIP_RouterInfoDivider * (iRevRtrNum - iBaseRtrNum)) + iRtrDest;
			if ((iBNCSDest > 0) && (iBNCSDest <= pRtr->getMaximumDestinations())) {
				int iPrev = pRtr->getPrimarySourceForDestination(iBNCSDest);
				bncs_stringlist ssl = bncs_stringlist(ssRev, ',');
				int iNewSrce = ssl.getNamedParam("index").toInt();
				int iDestStatus = ssl.getNamedParam("status").toInt();
				int iDestLock = ssl.getNamedParam("lock").toInt();
				pRtr->storeGRDRevertive(iBNCSDest, iNewSrce, iDestStatus, iDestLock);
				if (!bAutomaticStarting&&bShowAllDebugMessages) Debug("StoreRtrRev - calc dest %d src %d prev %d", iBNCSDest, iNewSrce, iPrev);
				// is rev assigned to a dest pkg  -- return if changed source and linked to a dest pkg
				if ((iNewSrce != iPrev) && (pRtr->getRouterDestPackageIndex(iBNCSDest).length() > 0) && (!bAutomaticStarting))
					return TRUE;  // further processing required as linked to dest pkgs
			}
			else
				Debug("StoreNevionRouterRevertive -- ERROR invalid BNCS dest  %d from rev dev %d dest %d", iBNCSDest, iRevRtrNum, iRtrDest);
		}
		// not changed, not linked to dest pkg - but means nothing further to do with revertive
		return FALSE;

	}
	catch (structured_exception const & e)
	{
		Debug("%s:StoreNevionRouterRevertive() - thrown exception %x from %x", szAppName, e.what(), e.where());
		return FALSE;
	}
	catch (...)
	{
		Debug("%s:StoreNevionRouterRevertive() - Unknown exception ", szAppName);
		return FALSE;
	}

}


void ProcessNevionRouterRevertive(int iBaseRtrNum, int iRevRtrNum, int iRtrDest)
{
	try {
		// check to see if dest has correct source as expected from package or not !!
		//if (bShowAllDebugMessages) Debug("intoProcRtrRev %d %d", iRtrNum, iRtrDest);
		CRevsRouterData* pRtr = GetRouterRecord(iBaseRtrNum);
		if (pRtr) {
			int iBNCSDest = iRtrDest;
			iBNCSDest = (iIP_RouterInfoDivider * (iRevRtrNum - iBaseRtrNum)) + iRtrDest;
			if ((iBNCSDest > 0) && (iBNCSDest <= pRtr->getMaximumDestinations())) {
				// get all dest pkgs associated to this router destination -- usually only 1, but possibly more
				bncs_stringlist ssl = bncs_stringlist(pRtr->getRouterDestPackageIndex(iBNCSDest), '|');
				Debug("ProcNevionRev- dev %d slot %d dest %d has pkg(s) %s", iRevRtrNum, iRtrDest, iBNCSDest, LPCSTR(ssl.toString('|')));
				for (int iidp = 0; iidp < ssl.count(); iidp++) {
					int iAssignedDestPkg = ssl[iidp].toInt();
					if ((iAssignedDestPkg > 0) && (iAssignedDestPkg < (iNumberOfPackages+1))) {
						UpdateDestPkgSdiStatusInfodriverSlot(iAssignedDestPkg, FALSE);
						if (iChosenDestPkg == iAssignedDestPkg) DisplayDestinationPackageData(FALSE);
					}
				}
			}
			else
				Debug("ProcessNevionRouterRevertive -- ERROR invalid BNCS dest  %d from rev dev %d dest %d", iBNCSDest, iRevRtrNum, iRtrDest);
		}
		else
			Debug("ProcessNevionRouterRevertive -- ERROR no ROUTER class for  %d ", iBaseRtrNum);
	}
	catch (structured_exception const & e)
	{
		Debug("%s:ProcessNevionRouterRevertive() - thrown exception %x from %x", szAppName, e.what(), e.where());
	}
	catch (...)
	{
		Debug("%s:ProcessNevionRouterRevertive() - Unknown exception ", szAppName);
	}

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
//   CSI CLIENT FUNCTIONS FOR INFODRIVER REVERTIVES MESSAGES -- MGN revs for the most part


int ValidInfodriverDevice(int iDev, int *iWhichRingIndex )
{
	*iWhichRingIndex = 0;
	// is it GV router rev ? 
	if ((iDev >= iIP_RouterVideoHD) && (iDev < (iIP_RouterVideoHD + 20))) {
		return VIDEO_HD_ROUTER_TYPE;
	}
	if ((iDev >= iIP_RouterAudio2CH) && (iDev < (iIP_RouterAudio2CH + 20))) {
		return AUDIO_ROUTER_TYPE;
	}
	if ((iDev >= iIP_RouterANC) && (iDev < (iIP_RouterANC + 20))) {
		return ANCIL_ROUTER_TYPE;
	}

	if (iDev == iAutomatics_CtrlInfo_Local) return AUTO_CONTROL_INFO;
	if (iDev == iAutomatics_CtrlInfo_Remote) return AUTO_CONTROL_INFO;

	// is ringmaster rev ?
	if (iDev == i_RingMaster_Main) return RINGMASTER_MAIN;
	if (iDev == i_RingMaster_Conf) return RINGMASTER_CONFS;
	if ((iDev >= i_RingMaster_Ports_Start) && (iDev <= i_RingMaster_Ports_End)) {
		*iWhichRingIndex = iDev - i_RingMaster_Ports_Start + 1;
		return RINGMASTER_PORTS;
	}
	if ((iDev >= i_RingMaster_Ifb_Start) && (iDev <= i_RingMaster_Ifb_End))  {
		*iWhichRingIndex = iDev - i_RingMaster_Ifb_Start + 1;
		return RINGMASTER_IFBS;
	}
	//
	return UNKNOWNVAL;
}

/////////////////////////////////////////////////////////////////////////////////////////////


bncs_string GetOldIFBEntries(bncs_string ssPrev, bncs_string ssData)
{
	// go thru and find those entries in prev and not in new data
	bncs_stringlist ssl_Ret = bncs_stringlist("", ',');
	bncs_stringlist ssl_old = bncs_stringlist(ssPrev, ',');
	bncs_stringlist ssl_new = bncs_stringlist(ssData, ',');
	for (int ii = 0; ii < ssl_old.count(); ii++) {
		bncs_string ssent = ssl_old[ii];
		if (ssl_new.find(ssent) < 0) {
			ssl_Ret.append(ssent);
		}
	}
	return ssl_Ret.toString(',');
}


bncs_string GetNewIFBEntries(bncs_string ssPrev, bncs_string ssData)
{
	// go thru and find those entries in new data and not in prev
	bncs_stringlist ssl_Ret = bncs_stringlist("", ',');
	bncs_stringlist ssl_old = bncs_stringlist(ssPrev, ',');
	bncs_stringlist ssl_new = bncs_stringlist(ssData, ',');
	for (int ii = 0; ii < ssl_new.count(); ii++) {
		bncs_string ssent = ssl_new[ii];
		if (ssl_old.find(ssent) < 0) {
			ssl_Ret.append(ssent);
		}
	}
	return ssl_Ret.toString(',');
}

void ProcessIFBMembersDifferences(int iRecordIndx, CRingMaster_IFB* pIFB, bncs_string ssPrev, bncs_string ssData)
{
	if (pIFB) {
		// determine those monitors keys in prev and not in curr data 
		// is this IFB assoc to an audio package ?? and which dyn ifb 
		if (pIFB->getAssociatedAudioPackage() > 0) {
			CAudioPackages* pAudio = GetAudioPackage(pIFB->getAssociatedAudioPackage());
			if (pAudio) {
				if ((pIFB->getAssociatedWhichDynIFB() > 0) && (pIFB->getAssociatedWhichDynIFB() < MAX_PKG_CONFIFB)) {
					int iCFConf = pAudio->stCFIFBs[pIFB->getAssociatedWhichDynIFB()].i_DynAssignedConference;
					if ((iCFConf > 0) && (iCFConf < MAX_CONFERENCES)) {
	
						// get list of old keys to remove this CF Conf from them
						bncs_stringlist sslold = bncs_stringlist(GetOldIFBEntries(ssPrev, ssData), ',');
						Debug("ProcessIFBMembersDifferences - ifb %d old %s", iRecordIndx, LPCSTR(sslold.toString(',')));
						for (int iOld = 0; iOld < sslold.count(); iOld++) {
							// is there need to check if CF CONF on key already too ???
							int iTrunk = pIFB->getAssignedTrunkAddr();
							int iMonSlot = 0;
							GetTrunkAndPortFromString(sslold[iOld], pIFB->getAssignedTrunkAddr(), &iTrunk, &iMonSlot);
							CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
							if ((pRing) && (iMonSlot>0)) {
								char szCommand[MAX_BUFFER_STRING] = "";
								wsprintf(szCommand, "IW %d '&REMOVE=Conference|%d|TL' %d", pRing->iRing_Device_Monitor, iCFConf, iMonSlot);
								AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Monitor, 0, 0);
							}
							else
								Debug("ProcIFBMarker -old- no ring record for %d or zero mon slot", iTrunk);
						} // for iOld

						// determine those monitors keys new in data - so add relevant CF Conf as required - if IFB in use at top of Que ??
						int iKeyMarker = pIFB->getAppliedMarker();
						// get list of new keys to update
						bncs_stringlist ssl = bncs_stringlist(GetNewIFBEntries(ssPrev, ssData), ',');
						Debug("ProcessIFBMembersDifferences - ifb %d CF CONF %d new %s", iRecordIndx, iKeyMarker, LPCSTR(ssl.toString(',')));
						for (int iK = 0; iK < ssl.count(); iK++) {
							int iTrunk = pIFB->getAssignedTrunkAddr();
							int iMonSlot = 0;
							GetTrunkAndPortFromString(ssl[iK], pIFB->getAssignedTrunkAddr(), &iTrunk, &iMonSlot);
							CRiedel_Rings* pRing = GetRiedel_Rings_By_TrunkAddress(iTrunk);
							if ((pRing) && (iMonSlot>0)) {
								char szCommand[MAX_BUFFER_STRING] = "";
								wsprintf(szCommand, "IW %d '&ADD=Conference|%d|TL' %d", pRing->iRing_Device_Monitor, iCFConf, iMonSlot);
								AddCommandToQue(szCommand, INFODRVCOMMAND, pRing->iRing_Device_Monitor, 0, 0);
							}
							else
								Debug("ApplyGivenMarker - no ring record for %d or zero mon slot", iTrunk);
						} // for iK

					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

BOOL StoreRingMasterRevertive(int iInfoRevType, int iWhichRing, int iDevNum, int iSlotNum, bncs_string ssData, BOOL* bChanged)
{
	try {

		*bChanged = FALSE;

		if (iInfoRevType == RINGMASTER_MAIN) {
			// anything to do ?? --- if not so why register or poll ???
		}
		else if (iInfoRevType == RINGMASTER_IFBS) {
			// ifb revs and ifb monitor and logic revertives / lists
			if ((iSlotNum > 0) && (iSlotNum < MAX_IFBS))  {
				int iIFB = iSlotNum;
				int iIFBRecordIndex = ((iWhichRing - 1)*MAX_IFBS) + iIFB;
				CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
				if (pIFB) {
					if (pIFB->getRingMasterRevertive() != ssData) *bChanged = TRUE;
					pIFB->storeRingMasterRevertive(ssData);
					// no further processing on these at present
					return TRUE;
				}
				else
					Debug("StoreRingmasterRev - no IFB main record for ring %d slot %d record index %d", iWhichRing, iSlotNum, iIFBRecordIndex);
			}
			else if ((iSlotNum > 1500) && (iSlotNum < (1500 + MAX_IFBS)))  {
				// ringmaster calc monitor members for ifb
				int iIFB = iSlotNum - 1500;
				int iIFBRecordIndex = ((iWhichRing - 1)*MAX_IFBS) + iIFB;
				CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iIFBRecordIndex);
				if (pIFB) {
					bncs_string ssPrev = pIFB->getRingMasterCalcMonitorMembers();
					pIFB->storeRingMasterCalcMonitorRevertive(ssData);
					if (bShowAllDebugMessages) Debug("StoreRingMasterRevertive ring %d IFB %d/%d mon rev %s was %s", iWhichRing, iIFB, iIFBRecordIndex, LPCSTR(ssData), LPCSTR(ssPrev));
					if ((!AreBncsStringsEqual(ssPrev, ssData) && (!bAutomaticStarting))) {
						// processing to add assoc dyn CF conference onto new keys - and remove from old keys no longer in list
						ProcessIFBMembersDifferences(iIFBRecordIndex, pIFB, ssPrev, ssData);
					}
					return TRUE;
				}
				else Debug("StoreRingmasterRev - no IFB mon record for ring %d slot %d record index %d", iWhichRing, iSlotNum, iIFBRecordIndex);
			}
			else if ((iSlotNum > 3500) && (iSlotNum < (3500 + MAX_MIXERS_LOGIC)))  {
				// store monitor members for Listen to Ports when IFB in MM que is disabled
				int iLogicIndex = iSlotNum - 3500;
				int iLogicRecordIndex = ((iWhichRing - 1)*MAX_MIXERS_LOGIC) + iLogicIndex;
				CRingMaster_LOGIC* pRMLogic = GetRingMaster_LOGIC_Record(iLogicRecordIndex);
				if (pRMLogic) {
					pRMLogic->storeRingMasterRevertive(ssData);
				}
				else {
					Debug("StoreRingmasterRev - no LOGIC record (mons) for ring %d slot %d record index %d", iWhichRing, iSlotNum, iLogicRecordIndex);
				}
			}
		}
		else if (iInfoRevType == RINGMASTER_CONFS) {
			// 
			int iRecordIndx = iSlotNum;                                                                                         // conf members
			if ((iSlotNum > 1000) && (iSlotNum < 2000)) iRecordIndx = iSlotNum - 1000;  // label
			if ((iSlotNum > 2000) && (iSlotNum < 3000)) iRecordIndx = iSlotNum - 2000;  // panel members
			if ((iSlotNum > 3000) && (iSlotNum < 4000)) iRecordIndx = iSlotNum - 3000;  // Calc Monitor Port members
			CRingMaster_CONF* pCONF = GetRingMaster_CONF_Record(iRecordIndx);
			if (pCONF) {
				if ((iSlotNum > 1000) && (iSlotNum < 2000)) {
					// store label
					if (pCONF->getRingMasterLabel() != ssData) *bChanged = FALSE;   // no further action needed for label
					pCONF->storeRingMasterLabel(ssData);
					return TRUE;
				}
				else if ((iSlotNum > 2000) && (iSlotNum < 3000)) {
					// panels
					//if (pCONF->getRingMasterPanelMembers() != ssData) *bChanged = TRUE;  // as these are read only no further action required
					pCONF->storeRingMasterPanelMembers(ssData);
					return TRUE;
				}
				else if ((iSlotNum > 3000) && (iSlotNum < 4000)) {
					// calc monitor port members
					//if (pCONF->getRingMasterCalcMonitorMembers() != ssData) *bChanged = TRUE;  // as these are read only no further action required
					pCONF->storeRingMasterCalcMonitorMembers(ssData);
					return TRUE;
				}
				else {
					// members
					if (pCONF->getRingMasterConfMembers() != ssData) {
						*bChanged = TRUE;
						pCONF->storeRingMasterConfMembers(ssData);
					}
					return TRUE;
				}
			}
			else {
				if (!bAutomaticStarting) Debug("StoreRingmasterRev - no CONF record for ring %d slot %d record index %d", iWhichRing, iSlotNum, iRecordIndx);
			}
		}
		else if (iInfoRevType == RINGMASTER_PORTS) {
			// ports revertive - riedel grd revertive -- may not require procecssing
			//Debug("Ringmaster GRD port rev - ring %d device %d slot %d ", iWhichRing, iDevNum, iSlotNum);
			CRingMaster_Ports* pPort = GetRingPortsDefintionByInfodriverSlot(iDevNum, iSlotNum);
			if (pPort) {
				// if (pPort->ss_RingMasterRevertive != ssData) *bChanged = TRUE;
				pPort->ss_RingMasterRevertive = ssData;
				return TRUE;
			}
		}
		return FALSE;

	}
	catch (structured_exception const & e)
	{
		Debug("%s:StoreRingMstrRev() - thrown exception %x from %x", szAppName, e.what(), e.where());
	}
	catch (...)
	{
		Debug("%s:StoreRingMstrRev() - Unknown exception ", szAppName);
	}
	return FALSE;

}


void ProcessRingMasterRevertive(int iInfoRevType, int iWhichRingIndex, int iSlotNumIndex )
{
	try
	{

		// currently only really need to process conference revs
		if (iInfoRevType == RINGMASTER_CONFS) {
			//  ringmaster conf rev processing 
			if ((iSlotNumIndex > 0) && (iSlotNumIndex < MAX_CONFERENCES)) {
				int iRecordIndx = iSlotNumIndex;       // process conf members
				CRingMaster_CONF* pCONF = GetRingMaster_CONF_Record(iRecordIndx);
				if (pCONF) {
					bncs_stringlist ssl_new_mem = bncs_stringlist(pCONF->getRingMasterConfMembers(), ',');
					pCONF->ClearConferenceMembers();
					if (ssl_new_mem.count() > 0) {
						for (int iMem = 0; iMem < ssl_new_mem.count(); iMem++) {
							pCONF->AddMemberToConference(ssl_new_mem[iMem], FALSE);
							if (bShowAllDebugMessages) Debug("ProcRMstrConfRev - conf %d adding member %s %s", iRecordIndx, LPCSTR(ssl_new_mem[iMem]), LPCSTR(pCONF->GetAllConferenceDetails('|')));
						}
					}
					// also if applicable update gui for new conf
				}
				else {
					Debug("ProcRMstrConfRev - no conf record for index %d", iRecordIndx);
				}
			}
		}
		/*
		else if (iInfoRevType == RINGMASTER_IFBS) {
		if (bShowAllDebugMessages) Debug("ProcessRingmasterRev - IFB rev - " );
		}
		*/

	}
	catch (structured_exception const & e)
	{
		Debug("%s:ProcessRingMasterRevertive() - thrown exception %x from %x", szAppName, e.what(), e.where());
	}
	catch (...)
	{
		Debug("%s:ProcessRingMasterRevertive() - Unknown exception ", szAppName);
	}

}

///////////////////////////////////////////////////// 
// start of processing control infodriver revs  
//
//
BOOL StoreControlInfoDrvRevertive(int iSlotDest, bncs_string ssRevertiveContent)
{

	switch (iSlotDest) {
	case CTRL_INFO_PKGR_LDC_BASE + CTRL_INFO_TXRX_TIKTOK:
		if (!AreBncsStringsEqual(ssRevertiveContent, ssLDC_TX_Revertive)) {
			// rev needs to have changed
			ssLDC_TX_Revertive = ssRevertiveContent;
			iLDC_TXRX_TicTocStatus = ssLDC_TX_Revertive.toInt();
			return TRUE;
		}
	break;

	case CTRL_INFO_PKGR_LDC_BASE + CTRL_INFO_RXONLY_TIKTOK:
		if (!AreBncsStringsEqual(ssRevertiveContent, ssLDC_RX_Revertive)) {
			ssLDC_RX_Revertive = ssRevertiveContent;
			iLDC_RXONLY_TicTocStatus = ssLDC_RX_Revertive.toInt();
			return TRUE;
		}
		break;

	case CTRL_INFO_PKGR_NHN_BASE + CTRL_INFO_TXRX_TIKTOK:
		if (!AreBncsStringsEqual(ssRevertiveContent, ssNHN_TX_Revertive)) {
			ssNHN_TX_Revertive = ssRevertiveContent;
			iNHN_TXRX_TicTocStatus = ssNHN_TX_Revertive.toInt();
			return TRUE;
		}
		break;

	case CTRL_INFO_PKGR_NHN_BASE + CTRL_INFO_RXONLY_TIKTOK:
		if (!AreBncsStringsEqual(ssRevertiveContent, ssNHN_RX_Revertive)) {
			ssNHN_RX_Revertive = ssRevertiveContent;
			iNHN_RXONLY_TicTocStatus = ssNHN_RX_Revertive.toInt();
			return TRUE;
		}
		break;

		// xxx add in conf and ifb shared resources proscessing
	}
	return FALSE;
}

// process changes based on new rev ( not if still the same )...
void ProcessControlInfoDrvRevertive(int iSlotDest)
{
	// nothing to process - wait on timer event
}


////////////////////////////////////////////////////////////////////////////////////
//
// processing for sending out dynamic data on confs
//
void GenerateDynConfDataStrings()
{
	bSendingOutDynConfData = TRUE;
	sslSendingOutDynConfData = bncs_stringlist("", '|');
	sslSendingOutDynConfData.append(bncs_string("START_CONFS"));
	int ielements = 0;
	bncs_string sstrev = "";
	for (int iicc = iDynConferencePoolStart; iicc <= iDynConferencePoolEnd; iicc++) {
		//
		if (ielements == 0) sstrev.append(bncs_string("CONF,%1").arg(iicc));
		ielements++;
		CRingMaster_CONF* pAConf = GetRingMaster_CONF_Record(iicc);
		if (pAConf) {
			// is conf used by LOCAL pkgr - do not include those flagged as pkg 77777 and usetype 77 - ie those marked as use in other site packager
			if ((pAConf->GetDynamicPackage() > 0) && (pAConf->GetDynamicPackage() < (iNumberOfPackages+1)) && (pAConf->GetDynamicUseType() > 0) && (pAConf->GetDynamicUseType() < OTHER_PKGR_USAGE)) {
				sstrev.append(bncs_string(",1")); // in use locally
			}
			else {
				sstrev.append(bncs_string(",0")); // not in use locally
			}
		}
		else {
			sstrev.append(bncs_string(",0")); // not in use locally
		}
		if (ielements == 100) {
			// add generated string to list
			sslSendingOutDynConfData.append(sstrev);
			// reset these
			ielements = 0;
			sstrev = bncs_string("");
		}
	} // for iicc
	// any part of string left to add to stringlist of revs
	if (sstrev.length() > 0) sslSendingOutDynConfData.append(sstrev);
	sslSendingOutDynConfData.append(bncs_string("FINISH_CONFS"));
	// each bit of list is sent out as a revertive and removed from list in 1/4 sec timer processing
	SetTimer(hWndMain, SHAREDRESOURCE_TIMER_ID, 250, NULL);
}

void GenerateDynIFBDataStrings()
{
	bSendingOutDynIFBData = TRUE;
	sslSendingOutDynIFBData = bncs_stringlist("", '|');
	sslSendingOutDynIFBData.append(bncs_string("START_IFBS"));
	for (int iRing = 1; iRing <= iNumberRiedelRings; iRing++) {
		int ielements = 0;
		bncs_string sstrev = "";
		for (int iicc = iDynIFBPoolStart; iicc <= iDynIFBPoolEnd; iicc++) {
			//
			if (ielements == 0) sstrev.append(bncs_string("RING,%1,IFB,%2").arg(iRing).arg(iicc));
			ielements++;
			int iIFBRecIndx = ((iRing - 1)*MAX_IFBS) + iicc;
			CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iIFBRecIndx);
			if (pIFB) {
				// is ifb used locally - do not include those flagged as pkg 77777 and usetype 77 - ie those marked as use in other site packager
				if ((pIFB->getAssociatedAudioPackage() > 0) && (pIFB->getAssociatedAudioPackage() < (iNumberOfPackages+1)) && (pIFB->getAssociatedWhichDynIFB() > 0) && (pIFB->getAssociatedWhichDynIFB() < OTHER_PKGR_USAGE)) {
					sstrev.append(bncs_string(",1")); // in use locally
				}
				else {
					sstrev.append(bncs_string(",0")); // not in use locally
				}
			}
			else {
				sstrev.append(bncs_string(",0")); // not in use locally
			}
			if (ielements == 100) {
				// add generated string to list
				sslSendingOutDynIFBData.append(sstrev);
				// reset these
				ielements = 0;
				sstrev = bncs_string("");
			}
		} // for iicc
		if (sstrev.length() > 0) sslSendingOutDynIFBData.append(sstrev);
	} // for iring
	//
	sslSendingOutDynIFBData.append(bncs_string("FINISH_IFBS"));
	// each bit of list is sent out as a revertive and removed from list in 1/4 sec timer processing
	SetTimer(hWndMain, SHAREDRESOURCE_TIMER_ID, 250, NULL);
}


void ProcessDynConfDataRevertive(bncs_string ssRevData)
{
	if ((ssRevData.find("CONF") >= 0) && (ssRevData.find("START") < 0) && (ssRevData.find("FINISH") < 0)) {
		bncs_stringlist ssl = bncs_stringlist(ssRevData, ',');
		// list of confs in use at OTHER site
		if (ssl[0].find("CONF") >= 0) {
			int iFirstConf = ssl[1].toInt();
			if (iFirstConf > 0) {
				int iEntry = 1;
				for (int iConf = iFirstConf; iConf <= (iFirstConf + ssl.count() - 3); iConf++) {
					iEntry++;
					int iInUseRemotely = ssl[iEntry].toInt();
					CRingMaster_CONF* pConf = GetRingMaster_CONF_Record(iConf);
					if (pConf) {
						// check not used locally
						if (((pConf->GetDynamicPackage() == 0) || (pConf->GetDynamicPackage() == OTHER_PKGR_ASSIGNED)) && (iInUseRemotely == 1)) {
							pConf->SetDynamicAudioPackage(OTHER_PKGR_ASSIGNED, OTHER_PKGR_USAGE, OTHER_PKGR_USAGE);
							if (bShowAllDebugMessages) Debug("ProcDynConf - conf %d set as inuse on remote site", iConf);
						}
						else if ((pConf->GetDynamicPackage() == OTHER_PKGR_ASSIGNED) && (iInUseRemotely == 0)) {
							// previously set as in use remotely - set free
							pConf->SetDynamicAudioPackage(0, 0, 0);
						}
						else if ((pConf->GetDynamicPackage() > 0) && (pConf->GetDynamicPackage() < (iNumberOfPackages+1)) && (iInUseRemotely == 1)) {
							// flagged as use locally - usage CLASH - but local assignment must remain
							Debug("ProcessDynConfDataRevertive -ERR- conf %d ALREADY IN USE LOCALLY pkg %d use %d- not set to Other pkgr use",
								pConf->GetDynamicPackage(), pConf->GetDynamicUseType());
						}
					}
				} // for iconf
			}
			else
				Debug("ProcessDynConfDataRev -2- invalid first conf", LPCSTR(ssRevData));
		}
		else
			Debug("ProcessDynConfDataRev -1- invalid format", LPCSTR(ssRevData));
	}
	else
		Debug("ProcessDynConfDataRev - start/finish - %s", LPCSTR(ssRevData));
	Calculate4WConferencePoolUse();
}


void ProcessDynIFBDataRevertive(bncs_string ssRevData)
{
	if ((ssRevData.find("START") < 0) && (ssRevData.find("FINISH") < 0) && (ssRevData.find("RING") >= 0) && (ssRevData.find("IFB") >= 0)) {
		bncs_stringlist ssl = bncs_stringlist(ssRevData, ',');
		// Find RING, Find start of IFBs for list given
		if (ssl.count() > 4) {
			if ((ssl[0].find("RING") >= 0) && (ssl[2].find("IFB") >= 0)) {
				int iRing = ssl[1].toInt();
				if ((iRing > 0) && (iRing <= iNumberRiedelRings)) {
					int iFirstIFB = ssl[3].toInt();
					if (iFirstIFB > 0) {
						int iEntry = 3;
						for (int iIFB = iFirstIFB; iIFB <= (iFirstIFB + ssl.count() - 5); iIFB++) {
							iEntry++;
							int iInUseRemotely = ssl[iEntry].toInt();
							int iIFBRecIndx = ((iRing - 1)*MAX_IFBS) + iIFB;
							CRingMaster_IFB* pIFB = GetRingMaster_IFB_Record(iIFBRecIndx);
							if (pIFB) {
								// check not used locally
								if (((pIFB->getAssociatedAudioPackage() == 0) || (pIFB->getAssociatedAudioPackage() == OTHER_PKGR_ASSIGNED)) && (iInUseRemotely == 1)) {
									pIFB->setAssociatedAudioPackage(OTHER_PKGR_ASSIGNED, OTHER_PKGR_USAGE);
									if (bShowAllDebugMessages) Debug("ProcDynIFB - ring %d ifb %d recIndx %d set as inuse on remote site", iRing, iIFB, iIFBRecIndx);
								}
								else if ((pIFB->getAssociatedAudioPackage() == OTHER_PKGR_ASSIGNED) && (iInUseRemotely == 0)) {
									// previously set as in use remotely - set free
									pIFB->setAssociatedAudioPackage(0, 0);
								}
								else if ((pIFB->getAssociatedAudioPackage() > 0) && (pIFB->getAssociatedAudioPackage() < (iNumberOfPackages+1)) && (iInUseRemotely == 1)) {
									// already flagged as use locally - usage CLASH - but local assignment must remain
									Debug("ProcessDynIFBDataRevertive -ERR- conf %d ALREADY IN USE LOCALLY pkg %d use %d- not set to Other pkgr use",
										pIFB->getAssociatedAudioPackage(), pIFB->getAssociatedWhichDynIFB());
								}
							}
						} // for iifb
					}
					else
						Debug("ProcessDynIfbDataRev -3- invalid first ifb", LPCSTR(ssRevData));
				}
				else
					Debug("ProcessDynIfbDataRev -2- invalid ring number", LPCSTR(ssRevData));
			}
			else
				Debug("ProcessDynIFBDataRev -1- invalid format", LPCSTR(ssRevData));
		}
		else
			Debug("ProcessDynIFBDataRev -0- invalid format", LPCSTR(ssRevData));
	}
	else
		Debug("ProcessDynIFBDataRev - start/finish - %s", LPCSTR(ssRevData));
	CalculateDynIFBPoolUse();
}



/////////////////////////////////////////////////////////////////////////////////// 

/// RM recent list manipulation

void AddEntryToRecentRMList(int iDev, int iDB, int iIndex)
{
	bncs_string ssStr = bncs_string("%1,%2,%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.find(ssStr)<0) {
		//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s not found so adding", LPCSTR(ssStr));
		ssl_RMChanges.append(ssStr);
		// reset timer
		KillTimer(hWndMain, DATABASE_TIMER_ID);
		SetTimer(hWndMain, DATABASE_TIMER_ID, 200, NULL);   // was 100
	}
}

BOOL FindEntryInRecentRMList(int iDev, int iDB, int iIndex)
{
	bncs_string ssStr = bncs_string("%1,%2,%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.count()>0) {
		if (ssl_RMChanges.find(ssStr) >= 0) {
			//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s found", LPCSTR(ssStr));
			return TRUE;
		}
	}
	return FALSE;
}





//////////////////////////////////////////////////////////////////////////
// 
//  REVERTIVE QUEUE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


void ClearRevertiveQueue(void)
{
	while (!qRevertives.empty()) {
		CCommand* pRev;
		pRev = qRevertives.front();
		qRevertives.pop();
		delete pRev;
	}
}

void AddRevertiveToQue(bncs_string ssRev, int iCmdType, int iDev, int iSlot, int iDB)
{
	CCommand* pRev = new CCommand;
	if (pRev) {
		if (ssRev.length() > 260) ssRev.truncate(260);
		strcpy(pRev->szCommand, LPCSTR(ssRev));
		pRev->iTypeCommand = iCmdType;
		pRev->iWhichDevice = iDev;
		pRev->iWhichSlotDest = iSlot;
		pRev->iWhichDatabase = iDB;
		qRevertives.push(pRev);
	}
	else
		Debug("AddRevertive2Q - cmd record not created");
}

void ProcessNextRevertive(int iRateRevs)
{
	try {

		// semaphore used  to prevent clash of incoming revertives and processing of already received revs
		DWORD dResult = WaitForSingleObject(hCSIClientSemaphore, 2000);
		if (dResult == WAIT_OBJECT_0) {

			//Debug("into ProcessNextRev");
			BOOL bContinue = TRUE;
			do {
				if (qRevertives.size() > 0) {
					strcpy(szClientCmd, "");
					iRateRevs--;
					// something in queue so process it
					BOOL bNow = FALSE;
					if ((iRateRevs == 0) || (qRevertives.size() == 1)) bNow = TRUE;
					CCommand* pRevertive = qRevertives.front();
					qRevertives.pop();
					if (pRevertive) {
						// process revertive now
						int iWhichRing = 0;
						int iDevNum = pRevertive->iWhichDevice;
						int iSlotDest = pRevertive->iWhichSlotDest;
						//
						if (pRevertive->iTypeCommand == REV_ROUTER) {

							if (ValidRouterDevice(iDevNum, &iWhichRing)) {
								int iPrevSource = 0;
								int iRtrSource = atoi(pRevertive->szCommand);
								StoreRouterRevertive(iDevNum, iSlotDest, iRtrSource, &iPrevSource);
								// process changes based on new rev ( not if still the same )...
								if ((iRtrSource != iPrevSource) && (!bAutomaticStarting)) {
									ProcessRouterRevertive(iDevNum, iSlotDest);
									ProcessNextCommand(BNCS_COMMAND_RATE); // in event anything added to buffer 
								}
							}
							else
								Debug("ProcessNextRev - Unexpected router revertive from device %d ", iDevNum);

						}
						else if (pRevertive->iTypeCommand == REV_INFODRV) {
							bncs_string ssRevertiveContent = bncs_string(pRevertive->szCommand);
							BOOL bChanged = FALSE;
							int iInfoRevType = ValidInfodriverDevice(iDevNum, &iWhichRing);
							//
							//
							if (iInfoRevType == VIDEO_HD_ROUTER_TYPE) {
								bChanged = StoreNevionRouterRevertive(iIP_RouterVideoHD, iDevNum, iSlotDest, ssRevertiveContent);
								// process changes based on new rev ( not if still the same )...
								if (bChanged) ProcessNevionRouterRevertive(iIP_RouterVideoHD, iDevNum, iSlotDest);
							}
							else if (iInfoRevType == AUDIO_ROUTER_TYPE) {
								bChanged = StoreNevionRouterRevertive(iIP_RouterAudio2CH, iDevNum, iSlotDest, ssRevertiveContent);
								// process changes based on new rev ( not if still the same )...
								if (bChanged) ProcessNevionRouterRevertive(iIP_RouterAudio2CH, iDevNum, iSlotDest);
							}
							else if (iInfoRevType == ANCIL_ROUTER_TYPE) {
								bChanged = StoreNevionRouterRevertive(iIP_RouterANC, iDevNum, iSlotDest, ssRevertiveContent);
								// process changes based on new rev ( not if still the same )...
								if (bChanged) ProcessNevionRouterRevertive(iIP_RouterANC, iDevNum, iSlotDest);
							}
							else if ((iInfoRevType >= RINGMASTER_MAIN) && (iInfoRevType <= RINGMASTER_IFBS)) {
								if (StoreRingMasterRevertive(iInfoRevType, iWhichRing, iDevNum, iSlotDest, ssRevertiveContent, &bChanged)) {
									if (bChanged) ProcessRingMasterRevertive(iInfoRevType, iWhichRing, iSlotDest);
								}
							}
							else if (iInfoRevType == AUTO_CONTROL_INFO) {
								// registered for all 4 tic tok slots -> for 2 sites each with 2 packager intstances
								if ((iSlotDest == (CTRL_INFO_PKGR_LDC_BASE + CTRL_INFO_TXRX_TIKTOK)) || (iSlotDest == (CTRL_INFO_PKGR_LDC_BASE + CTRL_INFO_RXONLY_TIKTOK)) ||
									(iSlotDest == (CTRL_INFO_PKGR_NHN_BASE + CTRL_INFO_TXRX_TIKTOK)) || (iSlotDest == (CTRL_INFO_PKGR_NHN_BASE + CTRL_INFO_RXONLY_TIKTOK))) {
									bChanged = StoreControlInfoDrvRevertive(iSlotDest, ssRevertiveContent);
									// process changes based on new rev ( not if still the same )...
									if (bChanged) ProcessControlInfoDrvRevertive(iSlotDest);
								}
								else {
									//  respond from remote info slots registered with for incoming REQUEST, or lists for shared resources data
									if (iDevNum == iAutomatics_CtrlInfo_Remote) {
										if ((iSlotDest == (CTRL_INFO_PKGR_LDC_BASE + CTRL_INFO_CONFPOOL)) || (iSlotDest == (CTRL_INFO_PKGR_NHN_BASE + CTRL_INFO_CONFPOOL))) {
											// AS only registered for conf / ifb data from OTHER site - process incoming data - process REQUEST command,or begin action on START, integer stringlist data and FINISH revs- else ignore
											Debug("Ctrl Auto slot %d rev %s", iSlotDest, LPCSTR(ssRevertiveContent));
											if (ssRevertiveContent.find("REQUEST") >= 0) {
												// start sending out current dyn conf data
												if ((!bSendingOutDynConfData) && (iOverallTXRXModeStatus == IFMODE_TXRX)) {
													GenerateDynConfDataStrings();
												}
											}
											else {
												// receiving data START, lists of confs in use at other site, FINSIH
												ProcessDynConfDataRevertive(ssRevertiveContent);
											}
										}
										else if ((iSlotDest == (CTRL_INFO_PKGR_LDC_BASE + CTRL_INFO_IFBPOOL)) || (iSlotDest == (CTRL_INFO_PKGR_NHN_BASE + CTRL_INFO_IFBPOOL))) {
											// AS only registered for conf / ifb data from OTHER site - process incoming data - ignore REQUEST rev, begin action on START, integer stringlist data and FINISH revs- else ignore
											Debug("Ctrl Auto slot %d rev %s", iSlotDest, LPCSTR(ssRevertiveContent));
											if (ssRevertiveContent.find("REQUEST") >= 0) {
												// start sending out current dyn conf data
												if ((!bSendingOutDynIFBData) && (iOverallTXRXModeStatus == IFMODE_TXRX)) {
													GenerateDynIFBDataStrings();
												}
											}
											else {
												// receiving data START, lists of ifbs in use at other site
												ProcessDynIFBDataRevertive(ssRevertiveContent);
											}
										} // ifb
									}
								}
							}
							else
								Debug("CSIClient - Unexpected infodriver revertive from device %d slot %d", iDevNum, iSlotDest);
						}
						//
						delete pRevertive; // delete record as now finished 
					}
					else
						Debug("ProcessNextRevertive -ERROR- invalid revertive class");
				}
				else {
					// nothing else in queue
					bContinue = FALSE;
					iRateRevs = 0;
				}
			} while ((bContinue) && (iRateRevs > 0));

			// if more commands still in queue and timer not running then start timer, else perhaps kill it
			if (qRevertives.size() > 0) {
				if (!bNextRevertiveTimerRunning) {
					int iTimerVal = 240;
					if (bAutomaticStarting) iTimerVal = 1140;
					SetTimer(hWndMain, REVERTIVE_TIMER_ID, iTimerVal, NULL); // timer for start poll
					bNextRevertiveTimerRunning = TRUE;
				}
			}
			else {
				if (bNextRevertiveTimerRunning) {
					KillTimer(hWndMain, REVERTIVE_TIMER_ID);
					bNextRevertiveTimerRunning = FALSE;
				}
			}
			SetDlgItemInt(hWndDlg, IDC_QUEUE2, qRevertives.size(), FALSE);
			//Debug("outof ProcessNextRev");

			// release semaphore
			if (!ReleaseSemaphore(hCSIClientSemaphore, 1, NULL))
				Debug("ProcNextRevertive -ERROR- fail to release semaphore !! ");

		}
		else
			Debug("ProcNextRevertive - fail / timeout to acquire semaphore ");

	}
	catch (structured_exception const & e)
	{
		Debug("%s:ProcNextRevertive() - thrown exception %x from %x", szAppName, e.what(), e.where());
	}
	catch (...)
	{
		Debug("%s:ProcNextRevertive() - Unknown exception ", szAppName);
	}

}


////////////////////////////////////////////////////////////////////////////////////////////
//
// strip quotes from info revertives 
//
char* StripOutQuotes( LPSTR szStr ) 
{
	int iI=0, iOut=0, iLen = strlen( szStr );
	int iNumQuotes = 0;
	strcpy(szRevString, "");

	if (iLen > 2) {
		for (iI = 0; iI < iLen; iI++) {
			if ((szStr[iI] == QUOTECHAR1) || (szStr[iI] == QUOTECHAR2)) {
				iNumQuotes++;
				if (iNumQuotes >= 2) iI = iLen + 1;
			}
			else {
				if (iOut < 255) {
					szRevString[iOut] = szStr[iI];
					szRevString[iOut + 1] = NULL; // terminate string
					iOut++;
				}
			}
		}
	}
	//Debug("StripQuotes out str %s", szRevString);
	return szRevString;
}

//
//  FUNCTION: CSIClientNotify()
//
//  PURPOSE: Callback function for incoming CSI messages for client class
//
//  COMMENTS: the message is supplied in szMsg
LRESULT CSIClientNotify(extclient* pec, LPCSTR szMsg)
{

	try
	{

	// semaphore used  to prevent clash of incoming revertives and processing of already received revs
	DWORD dResult = WaitForSingleObject(hCSIClientSemaphore, 2000);
	if (dResult == WAIT_OBJECT_0) {

		//Debug("CSIClientNotify - into");
		bCSIClientProcessing = TRUE;
		bncs_string ssIncomingContent = bncs_string("");
		if (pec&&szMsg) {
			UINT iParamCount = pec->getparamcount();

			BOOL bChanged = FALSE;
			char szContent[512] = "";
			int iWhichTypeIndex = 0, iDevNum = 0, iDBNum = 0, iRtrDest = 0, iRtrSource = 0, iWhichGpi = 0, iGPIState = 0;
			int iInfoRevType = UNKNOWNVAL, iSlotNum = 0, iIndexNum = 0, iPrevSource = 0, iPrevState = UNKNOWNVAL;

			LPSTR szParamRev, szDevNum, szSlot, szIndex; // , szContent;

			if (iParamCount > 0) szParamRev = pec->getparam(0);
			if (iParamCount > 1)  szDevNum = pec->getparam(1);
			if (iParamCount > 2)  szSlot = pec->getparam(2);
			if (iParamCount > 3)  szIndex = pec->getparam(3);
			if (iParamCount > 4)  {
				strcpy(szContent, pec->getparam(4));
				//Debug("CSI szdev %s szSlotNum %s param count %d - len %d str %s", szDevNum, szSlot, iParamCount, strlen(szContent), szContent);
			}

			if (szDevNum) {
				iDevNum = atoi(szDevNum);
			}

			if (szSlot)  {
				iDBNum = atoi(szSlot);
				iSlotNum = atoi(szSlot);
				iRtrDest = atoi(szSlot);
				iWhichGpi = atoi(szSlot);
			}

			if (szIndex)  {
				iRtrSource = atoi(szIndex);
				iIndexNum = atoi(szIndex);
				iGPIState = atoi(szIndex);
			}

			if (bValidCollediaStatetoContinue ) {

				switch (pec->getstate())
				{
				case REVTYPE_R:
					//Debug("CSIClientNotify - point_RR");
					// NEW - add inot revertives queue for controlled processing away from this event
					ssIncomingContent = bncs_string(iRtrSource);
					AddRevertiveToQue(ssIncomingContent, REV_ROUTER, iDevNum, iRtrDest, iDBNum);
					if (!bNextRevertiveTimerRunning) {
						int iTimerVal = 340;
						if (bAutomaticStarting) iTimerVal = 1340;
						SetTimer(hWndMain, REVERTIVE_TIMER_ID, iTimerVal, NULL); // timer for start poll
						bNextRevertiveTimerRunning = TRUE;
					}
				break;

				case REVTYPE_I:
					if ((iParamCount >= 5) && (szContent)) {
						if (bShowAllDebugMessages) Debug("CSIClient - infodriver rev from dev %d slot %d - params %d string %s", iDevNum, iSlotNum, iParamCount, szContent);
						ssIncomingContent = bncs_string(szContent);
						// remove both quotes from start and end of revertive
						if (ssIncomingContent.length() > 1) {
							ssIncomingContent.replace(bncs_string("'"), bncs_string(""));
							ssIncomingContent.replace(bncs_string("'"), bncs_string(""));
						}
						// NEW - add inot revertives queue for controlled processing away from this event
						AddRevertiveToQue(ssIncomingContent, REV_INFODRV, iDevNum, iSlotNum, iDBNum);
						if (!bNextRevertiveTimerRunning) {
							int iTimerVal = 340;
							if (bAutomaticStarting) iTimerVal = 1340;
							SetTimer(hWndMain, REVERTIVE_TIMER_ID, iTimerVal, NULL); // timer for start poll
							bNextRevertiveTimerRunning = TRUE;
						}
					}
					else
						Debug("CSIClient - Incorrectly formed infodriver revertive from dev %d slot %d - params %d or null string", iDevNum, iSlotNum, iParamCount);
				break;

				case DATABASECHANGE:
					// significant part of processing for setting up of packages
					// RM commands come in from src and dest package dbs and from 556 and 557
					// get first RM command 
					// because using internal database manager need to update internal record but not update actual file ( csi does that )

					if (iDevNum == iPackager_router_main) {

						if (bShowAllDebugMessages) Debug("CSIClient - Database change message - param count %d - device %d db %d indx %d ", iParamCount, iDevNum, iDBNum, iIndexNum);

						if (!FindEntryInRecentRMList(iDevNum, iDBNum, iIndexNum)) {

							//add most recent data to list of changed indexes to ignore the 2nd RM
							AddEntryToRecentRMList(iDevNum, iDBNum, iIndexNum);

							if (iDevNum == iPackager_router_main) {

								if ((iDBNum == DB_SOURCE_BUTTON) || (iDBNum == 2) || (iDBNum == 4) || (iDBNum == 6) || (iDBNum == 9) || (iDBNum == 10) || (iDBNum == 11)) {
									// Source Packages
									strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
									if (bShowAllDebugMessages)
										Debug("CSIClient - Database change message  device %d db %d indx %d content %s", iDevNum, iDBNum, iIndexNum, szRMData);
									// source package changes
									if ((iIndexNum > 0) && (iIndexNum <= iNumberOfPackages)) {
										dbmPkgs.setName(iPackager_router_main, iDBNum, iIndexNum, szRMData, false);
										CSourcePackages* pSpkg = GetSourcePackage(iIndexNum);
										if (pSpkg) {
											if ((iDBNum == DB_SOURCE_AUDIO_PKGS) || (iDBNum == DB_SOURCE_VIDEO_LVLS)) {
												if (ssl_RM_SrcPkg_Changes.find(bncs_string(iIndexNum)) < 0) ssl_RM_SrcPkg_Changes.append(bncs_string(iIndexNum));
											}
											else {
												// only need to process now if not pending a preceeding major process from a db 6/111 change
												if (ssl_RM_SrcPkg_Changes.find(bncs_string(iIndexNum)) < 0)  bChanged = ReloadASourcePackageData(iIndexNum, FALSE);
											}
										}
									}
								}
								else if ((iDBNum == DB_DESTINATION_BUTTON) || (iDBNum == 3) || (iDBNum == 5) || (iDBNum == 7) || ((iDBNum >= 15) && (iDBNum <= 19)) ) {
									// destination package changes
									strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
									if (bShowAllDebugMessages)
										Debug("CSIClient - Database change message - device %d db %d indx %d content %s", iDevNum, iDBNum, iIndexNum, szRMData);

									if ((iIndexNum > 0) && (iIndexNum <= iNumberOfPackages)) {
										dbmPkgs.setName(iPackager_router_main, iDBNum, iIndexNum, szRMData, false);
										CDestinationPackages* pDpkg = GetDestinationPackage(iIndexNum);
										if (pDpkg) {
											if ((iDBNum == 7) || ((iDBNum >= 15) && (iDBNum <= 19))) {
												if (ssl_RM_DestPkg_Changes.find(bncs_string(iIndexNum)) < 0) ssl_RM_DestPkg_Changes.append(bncs_string(iIndexNum));
											}
											else {
												// only need to process now if not pending a preceeding major process from a db 7/15-19 change
												if (ssl_RM_DestPkg_Changes.find(bncs_string(iIndexNum)) < 0) bChanged = ReloadADestinationPackageData(iIndexNum, FALSE);
											}
											// update gui
											if (iChosenDestPkg == iIndexNum) DisplayDestinationPackageData(FALSE);
										}
										else
											Debug("CSIClient - unknown dest package %d ", iIndexNum);
									}
								}
								else if ((iDBNum >= DB_AUDIOPKG_BUTTON) && (iDBNum <= DB_AUDIOPKG_CONFLBL)) {
									// Audio Source packages
									strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
									if (bShowAllDebugMessages)
										Debug("CSIClient - AudioPkg change message  device %d db %d indx %d content %s", iDevNum, iDBNum, iIndexNum, szRMData);
									// source package changes
									if ((iIndexNum > 0) && (iIndexNum <= iNumberOfPackages)) {
										dbmPkgs.setName(iPackager_router_main, iDBNum, iIndexNum, szRMData, false);
										CAudioPackages* pApkg = GetAudioPackage(iIndexNum);
										if (pApkg) {
											BOOL bReassert = FALSE;
											if ((iDBNum == DB_AUDIOPKG_DEFN) || (iDBNum == DB_AUDIOPKG_LINKS16) || (iDBNum == DB_AUDIOPKG_RETURN)) bReassert = TRUE;
											ReloadAnAudioPackageData(iIndexNum, bReassert);
										}
									}
								}
								else if ((iDBNum >= DB_SPEV_BUTTON_NAME) && (iDBNum <= DB_SPEV_STATUS)) {
									// SPEVs - reload for information only
									strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
									if (bShowAllDebugMessages) Debug("CSIClient - SPEV Database change message - device %d db %d indx %d content %s", iDevNum, iDBNum, iIndexNum, szRMData);
									if ((iIndexNum > 0) && (iIndexNum <= iNumberOfPackages)) {
										dbmPkgs.setName(iPackager_router_main, iDBNum, iIndexNum, szRMData, false);
										CSportEvent* pSpev = GetSportEventPackage(iIndexNum);
										if (pSpev) {
											switch (iDBNum) {
											case DB_SPEV_BUTTON_NAME: pSpev->m_ssName = dbmPkgs.getName(iPackager_router_main, DB_SPEV_BUTTON_NAME, iIndexNum); break;
											case DB_SPEV_EVENT_ID: pSpev->m_ssEventID = dbmPkgs.getName(iPackager_router_main, DB_SPEV_EVENT_ID, iIndexNum); break;
											case DB_SPEV_ASSET_ID: pSpev->m_ssAssetID = dbmPkgs.getName(iPackager_router_main, DB_SPEV_ASSET_ID, iIndexNum); break;
											case DB_SPEV_CLASS_ID: pSpev->m_ssClassID = dbmPkgs.getName(iPackager_router_main, DB_SPEV_CLASS_ID, iIndexNum); break;
											case DB_SPEV_DATE_TIME: pSpev->m_ss_TimeDefinition = dbmPkgs.getName(iPackager_router_main, DB_SPEV_DATE_TIME, iIndexNum); break;
											case DB_SPEV_TEAMS_ID: pSpev->m_ss_TeamDefinition = dbmPkgs.getName(iPackager_router_main, DB_SPEV_TEAMS_ID, iIndexNum); break;
											case DB_SPEV_STATUS:
												bncs_string ssData = dbmPkgs.getName(iPackager_router_main, DB_SPEV_STATUS, iIndexNum);
												pSpev->m_iSPEV_Status = ssData.toInt();
												break;
											}
											//
											if (iChosenSrcPkg == iIndexNum) DisplaySourcePackageData();
										}
									}
								}
								else if ((iDBNum >= DB_TAGS_VIDEO) && (iDBNum <= DB_TAGS_RESV_2) && (iIndexNum > 0) && (iIndexNum < MAX_TAGS_ENTRIES)) {
									// tag databases - reload / add entry 
									strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
									dbmPkgs.setName(iPackager_router_main, iDBNum, iIndexNum, szRMData, false);
									bncs_string ssData = bncs_string(szRMData).upper();
									ReloadAPackageTag(iDBNum, iIndexNum, ssData);
									SetDlgItemInt(hWndDlg, IDC_AUTO_DBTAGS, iTotalNumberTags, TRUE);
								}
								else if (iDBNum == DB_SYNC_PACKAGE_ROUTING) {
									// update dest pkg record ?? -- cause a loop ??
									if ((iIndexNum > 0) && (iIndexNum <= iNumberOfPackages)) {
										strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
										dbmPkgs.setName(iPackager_router_main, iDBNum, iIndexNum, szRMData, false);
									}
								}
								else if (iDBNum == DB_SYNC_DYN_CONF) {
									if ((iIndexNum > 0) && (iIndexNum < MAX_CONFERENCES)) {
										strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
										dbmPkgs.setName(iPackager_router_main, iDBNum, iIndexNum, szRMData, false);
									}
								}
								else if (iDBNum == DB_SYNC_DYN_IFBS) {
									if ((iIndexNum > 0) && (iIndexNum < 4096)) {
										strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
										dbmPkgs.setName(iPackager_router_main, iDBNum, iIndexNum, szRMData, false);
									}
								}
							}
						}
					}
					else if (iDevNum == iOtherSite_Pkgr_router_main) {
						if (iDBNum == DB_SYNC_DYN_CONF) {
							if ((iIndexNum > 0) && (iIndexNum < MAX_CONFERENCES)) {
								strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
								dbmPkgs.setName(iOtherSite_Pkgr_router_main, iDBNum, iIndexNum, szRMData, false);
								// need to parse - update internal conf use records - either inuse or now free
								bncs_stringlist ssl = bncs_stringlist(szRMData, ',');
								BOOL bFreeConf = TRUE;
								if (ssl.count() > 1) {
									if (ssl.getNamedParam("audpkg_index").toInt() > 0) bFreeConf = FALSE;
								}
								CRingMaster_CONF* pAConf = GetRingMaster_CONF_Record(iIndexNum);
								if (pAConf) {
									if (bFreeConf) {
										// check conf assigned remotely ie 77777
										if (pAConf->GetDynamicPackage() == OTHER_PKGR_ASSIGNED) pAConf->SetDynamicAudioPackage(0, 0, 0);
									}
									else {
										if (pAConf->GetDynamicPackage() == 0)
											pAConf->SetDynamicAudioPackage(OTHER_PKGR_ASSIGNED, OTHER_PKGR_USAGE, OTHER_PKGR_USAGE); // indicates it is in use at other site
									}
								}
							}
						}
						else if (iDBNum == DB_SYNC_DYN_IFBS) {
							if ((iIndexNum > 0) && (iIndexNum <= (iNumberRiedelRings*MAX_IFBS))) {
								strcpy_s(szRMData, ExtractNamefromDevIniFile(iDevNum, iDBNum, iIndexNum));
								dbmPkgs.setName(iOtherSite_Pkgr_router_main, iDBNum, iIndexNum, szRMData, false);
								// need to parse - update internal conf use records - either inuse or now free
								bncs_stringlist ssl = bncs_stringlist(szRMData, ',');
								BOOL bFreeConf = TRUE;
								if (ssl.count() > 1) {
									if (ssl.getNamedParam("audpkg_index").toInt() > 0) bFreeConf = FALSE;
								}
								CRingMaster_IFB* pAIFB = GetRingMaster_IFB_Record(iIndexNum);
								if (pAIFB) {
									if (bFreeConf) {
										// check ifb assigned remotely ie 77777-- do not clear if assigned locally
										if (pAIFB->getAssociatedAudioPackage() == OTHER_PKGR_ASSIGNED) pAIFB->setAssociatedAudioPackage(0, 0);
									}
									else {
										if (pAIFB->getAssociatedAudioPackage() == 0) //-- only assign for remote if unassigned
											pAIFB->setAssociatedAudioPackage(OTHER_PKGR_ASSIGNED, OTHER_PKGR_USAGE); // indicates it is in use at other site
									}
								}
							}
						}
					}
				break;

				case REVTYPE_G:
					Debug("CSIClient - IGNORED GPI Revertive from dev (%d), gpi (%d), state %d", iDevNum, iWhichGpi, iGPIState);
					break;

				case STATUS:
					if (szMsg)
						Debug("CSIClient - Status message is %s", szMsg);
					else
						Debug("CSIClient - null Status message");
					break;

				case DISCONNECTED:
					Debug("CSIClient - CSI has closed down - driver in FAIL  or  ERROR  state");
					//SetDlgItemText(hWndDlg, IDC_CSI_STATUS, "CSI connect FAILED");
					iLabelAutoStatus = 2;
					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
					eiInfoDrivers[AUTO_PACK_ROUTE_INFO].updateslot(COMM_STATUS_SLOT, "0");	 // error	
					bValidCollediaStatetoContinue = FALSE;
					PostMessage(hWndMain, WM_CLOSE, 0, 0);
					break;

				} // switch
			}
			else
				Debug("CSIClient - Automatic in INVALID or FAIL  State - info revertive ignored");
		}
		else
			Debug("CSIClient - invalid pec class passed");

		bCSIClientProcessing = FALSE;
		// release semaphore
		if (!ReleaseSemaphore(hCSIClientSemaphore, 1, NULL))
			Debug("DiscoveryPackager - CSIClient -ERROR- fail to release semaphore !! ");

	}
	else	Debug("DiscoveryPackager - CSIClient - fail / timeout to acquire semaphore ");

	/* always return TRUE so CSI doesn't delete your client registration */
	return TRUE;

	}
	catch (structured_exception const & e)
	{
		Debug("%s:CSIClientNotify() - thrown exception %x from %x", szAppName, e.what(), e.where());
		return TRUE;
		//exit(1);
	}
	catch (...)
	{
		Debug("%s:CSIClientNotify() - Unknown exception ", szAppName);
		return TRUE;
		//exit(1);
	}

}


///////////////////////////////////////////////////////////////////////////////////////////
//
//  Infodriver Resilience
//
///////////////////////////////////////////////////////////////////////////////////////////
// 
//  Check infodriver status for resilience
//
// 
void CheckAndSetInfoResilience( void )
{
	// having connected to driver now then getmode for redundancy
	// get state of infodriver
	char szNM[32] = "", szCM[32] = "", szStr[128] = "";
	int iMode = eiInfoDrivers[1].getmode();     // main / first  info determines the status of all others -- to prevent mix occuring
	iOverallStateJustChanged = 0;

	Debug("CheckAndSetInfoResilience- master info mode %d ", iMode);
	// determine string for driver dialog window
	if (iMode ==IFMODE_RXONLY) {
		strcpy_s(szNM, "RXONLY" );
		iLabelAutoStatus = 2;
		wsprintf(szStr, "Pkgr Main %d RX only", iPackager_router_main);
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS2, szStr);
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "RXonly - RESERVE");
	}
	if (iMode ==IFMODE_TXRX){
		strcpy_s(szNM, "TX-RX" );
		iLabelAutoStatus = 0;
		wsprintf(szStr, "Pkgr  main %d TXRX", iPackager_router_main);
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS2, szStr);
		SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Tx/Rx - MAIN OK");
	}

	if (iMode!=iOverallTXRXModeStatus) {
		if (iOverallTXRXModeStatus ==IFMODE_RXONLY) strcpy_s(szCM, "RXONLY" );
		if (iOverallTXRXModeStatus ==IFMODE_TXRX) strcpy_s(szCM, "TX-RX" );
		Debug("CheckInfoMode- new mode %s(%d) different to current %s(%d) -- changing status", szNM, iMode, szCM, iOverallTXRXModeStatus);
		switch (iMode) {
			case IFMODE_RXONLY: case IFMODE_TXRXINQ:
				iLabelAutoStatus = 2;
				ForceAutoIntoRXOnlyMode();
				iOverallTXRXModeStatus = IFMODE_RXONLY;
				Debug( "Packager Automatic running in  RXONLY  mode ");
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "RXonly RESERVE Mode");
			break;
			case IFMODE_TXRX:
				iLabelAutoStatus = 0;
				ForceAutoIntoTXRXMode();
				iOverallTXRXModeStatus = IFMODE_TXRX;
				Debug( "Packager Automatic running in  TX/RX  mode ");
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Tx/Rx - MAIN OK");
				eiInfoDrivers[AUTO_PACK_ROUTE_INFO].updateslot(COMM_STATUS_SLOT, "1"); // automatic in OK mode
			break;
			default:
				iLabelAutoStatus = 3;
				iOverallTXRXModeStatus = IFMODE_NONE;
				Debug( "Packager Automatic in an  UNKNOWN  infodriver mode ");
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "??");
		}
	}
	// go thru all infos and report status
	int iTXRXCount=0, iRXOnlyCount=0, iOthModeCount=0;
	for (int iif = 1; iif <= iNumberOfInfodriversRequired; iif++) {
		if (iif < MAX_INFODRIVERS) {
			if (eiInfoDrivers[iif].getmode() == IFMODE_TXRX) {
				iTXRXCount++; 
				if (iOverallTXRXModeStatus == IFMODE_RXONLY) {
					Debug("CheckSetInfodriver %d returns txrx mode when AUTO is rxonly (master:%d)", eiInfoDrivers[iif].iDevice, eiInfoDrivers[1].getmode());
				}
			}
			else if (eiInfoDrivers[iif].getmode() == IFMODE_RXONLY) {
				iRXOnlyCount++; 
				if (iOverallTXRXModeStatus == IFMODE_TXRX) {
					Debug("CheckSetInfodriver %d returns rxonly mode when AUTO is txrx (master:%d)", eiInfoDrivers[iif].iDevice, eiInfoDrivers[1].getmode());
				}
			}
			else {
				iOthModeCount++;
				Debug("CheckSetInfodriver %d returns ??? %d mode when AUTO master has mode %d", eiInfoDrivers[iif].iDevice, eiInfoDrivers[iif].getmode(), eiInfoDrivers[1].getmode());
			}
		}
	}

	iNextResilienceProcessing = 37; 

	if (iOverallTXRXModeStatus==IFMODE_TXRX) {
		if (iTXRXCount<iNumberOfInfodriversRequired) {
			Debug("ERROR -- Driver has mixed txrx states:: txrx %d  rxonly %d other %d - trying to correct", iTXRXCount, iRXOnlyCount, iOthModeCount);
			iLabelAutoStatus = 3;
			wsprintf(szStr, "Mixed TX/rx - %d+%d/%d", iTXRXCount, iRXOnlyCount, iNumberOfInfodriversRequired);
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, szStr);
			// force all into txrx
			ForceAutoIntoTXRXMode();
			iOverallTXRXModeStatus = IFMODE_TXRX;
		}
	}
	else if (iOverallTXRXModeStatus==IFMODE_RXONLY) {
		if (iRXOnlyCount<iNumberOfInfodriversRequired) {
			Debug("ERROR -- Driver has mixed rxonly states:: rxonly %d txrx %d  other %d - trying to correct", iRXOnlyCount, iTXRXCount, iOthModeCount);
			iLabelAutoStatus = 3;
			wsprintf(szStr, "Mixed RX/tx - %d+%d/%d",iRXOnlyCount,  iTXRXCount, iNumberOfInfodriversRequired);
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, szStr);
			// force all into rxonly
			ForceAutoIntoRXOnlyMode();
			iOverallTXRXModeStatus = IFMODE_RXONLY;
		}
	}
	DisplaySelectedInfodriver();
}


void ForceAutoIntoRXOnlyMode( void )
{
	//
	eiInfoDrivers[AUTO_PACK_ROUTE_INFO].setslot(COMM_STATUS_SLOT, "0"); // currently automatic in lost fail mode
	// just set master infodriver -- others should follow if csi is doing its job properly
	eiInfoDrivers[AUTO_PACK_ROUTE_INFO].setmode(IFMODE_RXONLY);					// force to rxonly -- so that resilient pair can take over
	eiInfoDrivers[AUTO_PACK_ROUTE_INFO].requestmode = TO_RXONLY; // force other driver into tx immeadiately if running ??
	SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "FORCE RX ONLY");
	iNextResilienceProcessing = 3; // reset counter 
	iOverallModeChangedOver = 4;
	iOverallTXRXModeStatus = UNKNOWNVAL;
}


void ForceAutoIntoTXRXMode( void )
{
	//
	// just set master infodriver -- others should follow if csi is doing its job properly
	eiInfoDrivers[AUTO_PACK_ROUTE_INFO].setmode(IFMODE_TXRX);					// force to txrx -- so that resilient pair can take over
	eiInfoDrivers[AUTO_PACK_ROUTE_INFO].requestmode = TO_TXRX;
	iNextResilienceProcessing = 5; // reset counter 
	SetDlgItemText(hWndDlg, IDC_INFO_STATUS1, "FORCE TXRX");
	iNextResilienceProcessing = 3; // reset counter 
	iOverallModeChangedOver = 4;
	iOverallTXRXModeStatus = UNKNOWNVAL;
}


void Restart_CSI_NI_NO_Messages(void)
{
	iOverallModeChangedOver = 0;
	if (iOverallTXRXModeStatus == IFMODE_TXRX) 
		eiInfoDrivers[AUTO_PACK_ROUTE_INFO].requestmode = IFMODE_TXRX;
	else
		eiInfoDrivers[AUTO_PACK_ROUTE_INFO].requestmode = IFMODE_TXRXINQ;
}


void CloseAllInfodrivers()
{
	// close all connected infodrivers
	for (int iif = 1; iif <= iNumberOfInfodriversRequired; iif++) {
		if (iif < MAX_INFODRIVERS) {
			eiInfoDrivers[iif].closeInfodriver(); 
		}
	}

}

void SendOutClosingMessage()
{
	// set relevant autos control info slot to 0 if possible 
	// set relevant autos control info slot to 0 if possible 
	// set relevant autos control info slot to 0 if possible 
	if (iAutomatics_CtrlInfo_Local > 0) {
		char szCommand[256] = "0";
		char szBase[12] = "";
		int iLocalBase = 0;
		if (iAutomaticLocation == LDC_LOCALE_RING) {
			iLocalBase = CTRL_INFO_PKGR_LDC_BASE;
			strcpy(szBase, " LDC pkgr ");
		}
		else if (iAutomaticLocation == NHN_LOCALE_RING) {
			iLocalBase = CTRL_INFO_PKGR_NHN_BASE;
			strcpy(szBase, " NHN pkgr ");
		}
		if (iOverallTXRXModeStatus == IFMODE_TXRX)
			wsprintf(szCommand, "IW %d '0 %s stopped ' %d", iAutomatics_CtrlInfo_Local, szBase, iLocalBase + CTRL_INFO_TXRX_TIKTOK);
		else
			wsprintf(szCommand, "IW %d '0 %s stopped' %d", iAutomatics_CtrlInfo_Local, szBase, iLocalBase + CTRL_INFO_RXONLY_TIKTOK);
		if (ecCSIClient) ecCSIClient->txinfocmd(szCommand);
	}
	// get WS - write to info 998 slot WS stating closing Packager - with system time
	char* szCCWS = getenv("CC_WORKSTATION");
	int iWS = atoi(szCCWS);

	char szMsg[255] = "";
	char tBuffer[32];
	char szDate[64];
	struct tm *newtime;
	time_t long_time;

	time(&long_time);                // Get time as long integer. 
	newtime = localtime(&long_time); // Convert to local time. 
	_strtime(tBuffer);
	wsprintf(szDate, "%02d:%02d:%02d", newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
	// truncate long messages and add date and time
	wsprintf(szMsg, "IW %d 'WS %d - PACKAGER CLOSING at %s' 1", iAutomatics_CtrlInfo_Local,  iWS, szDate);
	if ((ecCSIClient)&&(iWS>0)) ecCSIClient->txinfocmd(szMsg, TRUE);
}



