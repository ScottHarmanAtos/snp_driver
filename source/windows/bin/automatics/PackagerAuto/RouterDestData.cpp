// RouterData.cpp: implementation of the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RouterDestData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRouterDestData::CRouterDestData( int iIndx )
{
	m_iMapIndex=iIndx;

	m_iRoutedIndex = 0;
	m_iExpectedIndex = -1;
	m_iScheduledIndex = 0;
	m_iExpectedCounter = 0;
	m_iPreviousRoutedIndex = 0;
	m_iRouteStatus = NEVION_ACTIVE;
	m_iLockStatus = NEVION_UNLOCKED;

	m_ssDestination_Package = "";
	m_ssReverseReturn_Package = "";

}

CRouterDestData::~CRouterDestData()
{

}
