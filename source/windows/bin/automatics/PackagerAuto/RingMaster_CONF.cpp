#include "RingMaster_CONF.h"


CRingMaster_CONF::CRingMaster_CONF( int iIndex )
{
	m_iIndexNumber = iIndex;
	m_ss_RMstr_Label = "";
	m_iAssigned_Trunk_Address = 0;
	m_ss_RMstr_ConfMembers = "";
	m_ss_RMstr_PanelMembers = "";
	m_ss_RMstr_CalcMonitorMembers = "";

	// from Conference class
	m_ss_ConfName = "-";
	m_ss4CharLabel = "4Wx";
	m_ssOverrideLabel = "";

	// if these next two are set - then package is in use from pool
	m_iDynamic_WhichConf = 0;
	m_iDynamic_Audio_Package = 0;
	m_iDynamic_Conf_UseType = 0;
	//
	m_ssl_Conference_members = bncs_stringlist("", ',');    // <ring.port>,<ring.port> style now
	m_ssl_Conference_privileges = bncs_stringlist("", ',');

}


CRingMaster_CONF::~CRingMaster_CONF()
{
}


void CRingMaster_CONF::storeRingMasterConfMembers(bncs_string ssRev)
{
	m_ss_RMstr_ConfMembers = ssRev;
}

bncs_string CRingMaster_CONF::getRingMasterConfMembers()
{
	return m_ss_RMstr_ConfMembers;
}

void CRingMaster_CONF::storeRingMasterPanelMembers(bncs_string ssRev)
{
	m_ss_RMstr_PanelMembers = ssRev;
}

bncs_string CRingMaster_CONF::getRingMasterPanelMembers()
{
	return m_ss_RMstr_PanelMembers;
}

void CRingMaster_CONF::storeRingMasterLabel(bncs_string ssRev)
{
	m_ss_RMstr_Label = ssRev;
	m_ss_ConfName = ssRev;
}

bncs_string CRingMaster_CONF::getRingMasterLabel()
{
	return m_ss_RMstr_Label;
}

// calc monitor port members

void CRingMaster_CONF::storeRingMasterCalcMonitorMembers(bncs_string ssRev)
{
	m_ss_RMstr_CalcMonitorMembers = ssRev;
}

bncs_string CRingMaster_CONF::getRingMasterCalcMonitorMembers(void)
{
	return m_ss_RMstr_CalcMonitorMembers;
}


////////////////////////////////////////////////////////////////////////////////


void CRingMaster_CONF::SetConferenceName(bncs_string ssName)
{
	m_ss_ConfName = ssName;
}


bncs_string CRingMaster_CONF::GetConferenceName(void)
{
	return m_ss_ConfName;
}

void CRingMaster_CONF::SetConferenceLabel(bncs_string ssName)
{
	if (ssName.length()>4)
		m_ss4CharLabel = ssName.left(4);
	else
		m_ss4CharLabel = ssName;
}


bncs_string CRingMaster_CONF::GetConferenceLabel(void)
{
	return m_ss4CharLabel;
}

void CRingMaster_CONF::SetOverrideConferenceLabel(bncs_string ssOverRide)
{
	m_ssOverrideLabel = ssOverRide;
}

bncs_string CRingMaster_CONF::GetOverrideConferenceLabel(void)
{
	return m_ssOverrideLabel;
}


void CRingMaster_CONF::SetDynamicAudioPackage(int iPackage, int iWhichConf, int iUseType)
{
	m_iDynamic_WhichConf = iWhichConf;
	m_iDynamic_Audio_Package = iPackage;
	m_iDynamic_Conf_UseType = iUseType;
	if ((m_iDynamic_WhichConf > 0) && (m_iDynamic_WhichConf < 5))
		m_ss4CharLabel = bncs_string("4W%1").arg(m_iDynamic_WhichConf);
	else
		m_ss4CharLabel = bncs_string("4Wx");
}


int CRingMaster_CONF::GetDynamicPackage(void)
{
	return m_iDynamic_Audio_Package;
}


int CRingMaster_CONF::GetDynamicWhichConf(void)
{
	return m_iDynamic_WhichConf;
}


int CRingMaster_CONF::GetDynamicUseType(void)
{
	return m_iDynamic_Conf_UseType;
}



BOOL CRingMaster_CONF::AddMemberToConference(bncs_string ssMemberPriv, BOOL bPrepend)
{
	bncs_stringlist ssl2 = bncs_stringlist(ssMemberPriv, '|');
	if (ssl2.count()>1) {
		// check for missing Vox with Talk or Talk+Listen- if not present then add in for when reasserted
		if (((ssMemberPriv.find("|T|") >= 0) || (ssMemberPriv.find("|TL|") >= 0)) && (ssMemberPriv.find("V|")<0)) {
			ssl2[1].append("V");
			ssMemberPriv = ssl2.toString('|'); // rebuild with added Vox 
		}
		//
		int iPos = m_ssl_Conference_privileges.find(ssMemberPriv);
		if (iPos<0) {
			if (bPrepend) {
				m_ssl_Conference_members.prepend(ssl2[0]);
				m_ssl_Conference_privileges.prepend(ssMemberPriv);  // stores all details
			}
			else {
				m_ssl_Conference_members.append(ssl2[0]);
				m_ssl_Conference_privileges.append(ssMemberPriv);  // stores all details
			}
			return TRUE;
		}
		else {
			// entry found -- but should it be ammended if privileges have changed ???
			return TRUE;
		}
	}
	return FALSE; // already added or invalid index
}


BOOL CRingMaster_CONF::RemoveMemberFromConference(bncs_string ssRingPort)
{
	int iDelEnt = 0;
	if (m_ssl_Conference_members.count()>0) {
		int iPos = m_ssl_Conference_members.find(ssRingPort);
		if (iPos >= 0) {
			m_ssl_Conference_members.deleteItem(iPos);
			m_ssl_Conference_privileges.deleteItem(iPos);
			return TRUE;
		}
	}
	return FALSE;
}


bncs_string CRingMaster_CONF::GetAllConferenceMembers(void)
{
	return m_ssl_Conference_members.toString(' ');  // was ','
}


bncs_string CRingMaster_CONF::GetAllConferenceDetails(char ic)
{
	return m_ssl_Conference_privileges.toString(ic);
}

void CRingMaster_CONF::ClearConferenceMembers(void)
{
	m_ssl_Conference_members.clear();
	m_ssl_Conference_privileges.clear();
}


void CRingMaster_CONF::setAssignedTrunkAddr(int iTrunkAddress)
{
	m_iAssigned_Trunk_Address = iTrunkAddress;
}

int CRingMaster_CONF::getAssignedTrunkAddr(void)
{
	return m_iAssigned_Trunk_Address;
}



