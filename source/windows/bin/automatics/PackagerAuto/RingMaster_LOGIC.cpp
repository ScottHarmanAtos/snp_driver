#include "RingMaster_LOGIC.h"


CRingMaster_LOGIC::CRingMaster_LOGIC(int iIndex, int iTrunkAddr)
{
	m_RecordIndex = iIndex;
	m_ss_RMstr_Revertive = "0|0|0|0|0|0";
	m_iAssociatedTrunkAddress = iTrunkAddr;
	m_iAssociatedListenToPort = 0;
	m_iAssociatedIFB = 0;
}


CRingMaster_LOGIC::~CRingMaster_LOGIC()
{
}

void CRingMaster_LOGIC::storeRingMasterRevertive(bncs_string ssRev)
{
	m_ss_RMstr_Revertive = ssRev;
}

bncs_string CRingMaster_LOGIC::getRingMasterRevertive()
{
	return m_ss_RMstr_Revertive;
}

int CRingMaster_LOGIC::getAssociatedTrunkAddress(void)
{
	return m_iAssociatedTrunkAddress;
}

void CRingMaster_LOGIC::setAssociatedIFB(int iIFB)
{
	m_iAssociatedIFB = iIFB;
}

int CRingMaster_LOGIC::getAssociatedIFB(void)
{
	return m_iAssociatedIFB;
}

void CRingMaster_LOGIC::setAssociatedListenToPort(int iPort)
{
	m_iAssociatedListenToPort = iPort;
}

int CRingMaster_LOGIC::getAssociatedInputPort(void)
{
	return m_iAssociatedListenToPort;
}


