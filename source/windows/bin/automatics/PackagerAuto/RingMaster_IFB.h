#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// holds ring master explicit info for each IFB
// pointers into various IFB records for each of the rings

class CRingMaster_IFB
{
private:
	int m_RecordIndex;

	// ifb associated ring - from config xml
	int m_iAssigned_Trunk_Address;

	// associated indices --- XXXX would come from comms_mappinfg ifb -- not used at present
	int m_iAssociatedInputPort;     // listen in on port
	int m_iAssociatedOutputPort;     // speak out to port
	int m_iAssociatedMixMinusPort;     // MixMinus port
	int m_iAssociatedRemoteLogic;

	// set at init from comms_package_tb
	int m_iDynamic_Audio_Package;                      // audio source pkg index for dyn allocated ifb from pool for ring
	int m_iDynamic_IFB_UseType;                                 // values of 1,2,3,4 for ifbs in audio pkg use    
	bncs_string m_ssRMSyncData;                  // contents of RM - so as to compare 

	// any other data required for IFBs at ringmaster level rather than just ifb revertive 
	bncs_string m_ss_RMstr_Revertive;
	bncs_string m_ss_CalcMonitorMembers;       // from ringmaster calc list of monitors using this conf - calc from monitor revs
	bncs_string m_ssInputs, m_ssMixMinus, m_ssOutputs, m_ssLabel;

	int iAppliedRiedelMarker;

	bncs_string m_ss_RMstr_ListenToPortMonitorMembers;   // list of users of listen 2 Port from monitor revs for assoc listen to Port linked to IFB

public:
	CRingMaster_IFB(int iRecordIndex, int iTrunkAddress);
	~CRingMaster_IFB();

	void storeRingMasterRevertive(bncs_string ssRev);
	void storeRingMasterCalcMonitorRevertive(bncs_string ssRev);

	int getAssignedTrunkAddr(void);

	void setAssociatedInputPort(int iPort);
	int getAssociatedInputPort(void);
	void setAssociatedOutputPort(int iPort);
	int getAssociatedOutputPort(void);
	void setAssociatedMixMinusPort(int iPort);
	int getAssociatedMixMinusPort(void);

	void setAssociatedRemoteLogic(int iLogic);
	int getAssociatedRemoteLogic(void);

	void setAssociatedAudioPackage(int iPackage, int iWhichUseType);
	int getAssociatedAudioPackage(void);
	int getAssociatedWhichDynIFB(void);

	// get other stored revertives data
	bncs_string getRingMasterRevertive(void);
	bncs_string getInputs(void);
	bncs_string getMixMinus(void);
	bncs_string getOutputs(void);
	bncs_string getLabel(void);

	bncs_string getRingMasterCalcMonitorMembers(void);

	void setAppliedMarker(int iMarker);
	int getAppliedMarker(void);

	void storeRMListen2PortCalcMonitorRevertive(bncs_string ssRev);
	bncs_string getRMListen2PortCalcMonitorMembers(void);

};

