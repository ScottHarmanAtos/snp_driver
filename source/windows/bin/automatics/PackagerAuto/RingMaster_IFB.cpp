#include "RingMaster_IFB.h"


CRingMaster_IFB::CRingMaster_IFB(int iIndex, int iTrunkAddress)
{
	m_RecordIndex = iIndex;
	m_iAssigned_Trunk_Address = iTrunkAddress;

	m_iAssociatedInputPort = 0;
	m_iAssociatedOutputPort = 0;
	m_iAssociatedMixMinusPort = 0;
	m_iAssociatedRemoteLogic = 0;

	m_iDynamic_Audio_Package = 0;
	m_iDynamic_IFB_UseType = 0;

	m_ss_RMstr_Revertive = "";
	m_ss_CalcMonitorMembers = "";
	m_ssInputs = "";
	m_ssMixMinus = "";
	m_ssOutputs = "";
	m_ssLabel = "";
	iAppliedRiedelMarker = 0;

	m_ss_RMstr_ListenToPortMonitorMembers = "";

}


CRingMaster_IFB::~CRingMaster_IFB()
{
}


void CRingMaster_IFB::storeRingMasterRevertive(bncs_string ssRev)
{
	m_ss_RMstr_Revertive = ssRev;
	bncs_stringlist ssl = bncs_stringlist(ssRev, '|');
	m_ssInputs = "";
	m_ssMixMinus = "";
	m_ssOutputs = "";
	m_ssLabel = "";
	if (ssl.count() > 0) m_ssInputs = ssl[0];
	if (ssl.count() > 1) m_ssMixMinus = ssl[1];
	if (ssl.count() > 2) m_ssOutputs = ssl[2];
	if (ssl.count() > 3) m_ssLabel = ssl[3];
}

bncs_string CRingMaster_IFB::getRingMasterRevertive()
{
	return m_ss_RMstr_Revertive;
}

bncs_string CRingMaster_IFB::getInputs()
{
	return m_ssInputs;
}

bncs_string CRingMaster_IFB::getOutputs()
{
	return m_ssOutputs;
}

bncs_string CRingMaster_IFB::getMixMinus()
{
	return m_ssMixMinus;
}

bncs_string CRingMaster_IFB::getLabel()
{
	return m_ssLabel;
}

void CRingMaster_IFB::storeRingMasterCalcMonitorRevertive(bncs_string ssRev)
{
	m_ss_CalcMonitorMembers = ssRev;
}

bncs_string CRingMaster_IFB::getRingMasterCalcMonitorMembers(void)
{
	return m_ss_CalcMonitorMembers;
}


int CRingMaster_IFB::getAssignedTrunkAddr(void)
{
	return m_iAssigned_Trunk_Address;
}

void CRingMaster_IFB::setAppliedMarker(int iMarker)
{
	iAppliedRiedelMarker = iMarker;
}

int CRingMaster_IFB::getAppliedMarker(void)
{
	return iAppliedRiedelMarker;
}

void CRingMaster_IFB::setAssociatedInputPort(int iPortAddr)
{
	m_iAssociatedInputPort = iPortAddr;
}

int CRingMaster_IFB::getAssociatedInputPort(void)
{
	return m_iAssociatedInputPort;
}

void CRingMaster_IFB::setAssociatedOutputPort(int iPort)
{
	m_iAssociatedOutputPort = iPort;
}

int CRingMaster_IFB::getAssociatedOutputPort(void)
{
	return m_iAssociatedOutputPort;
}


void CRingMaster_IFB::setAssociatedMixMinusPort(int iLogic)
{
	m_iAssociatedMixMinusPort = iLogic;
}

int CRingMaster_IFB::getAssociatedMixMinusPort(void)
{
	return m_iAssociatedMixMinusPort;
}


void CRingMaster_IFB::setAssociatedRemoteLogic(int iLogic)
{
	m_iAssociatedRemoteLogic = iLogic;
}

int CRingMaster_IFB::getAssociatedRemoteLogic(void)
{
	return m_iAssociatedRemoteLogic;
}


void CRingMaster_IFB::setAssociatedAudioPackage(int iPackage, int iWhichUseType)
{
	m_iDynamic_Audio_Package = iPackage;
	m_iDynamic_IFB_UseType = iWhichUseType;
}

int CRingMaster_IFB::getAssociatedAudioPackage(void)
{
	return m_iDynamic_Audio_Package;
}


int CRingMaster_IFB::getAssociatedWhichDynIFB(void)
{
	return m_iDynamic_IFB_UseType;
}


/////////////////////////////////////////////////////////////////////////////////////


void CRingMaster_IFB::storeRMListen2PortCalcMonitorRevertive(bncs_string ssRev)
{
	m_ss_RMstr_ListenToPortMonitorMembers = ssRev;
}


bncs_string CRingMaster_IFB::getRMListen2PortCalcMonitorMembers(void)
{
	return m_ss_RMstr_ListenToPortMonitorMembers;
}


