#include "stdafx.h"
#include "Riedel_Rings.h"

CRiedel_Rings::CRiedel_Rings(int iKeyIndex, int iTrunkAddr, int iRingType, bncs_string ssName)
{
	m_iRecordIndex = iKeyIndex;
	m_iTrunkAddress = iTrunkAddr;   
	m_iRingUseType = iRingType;
	ssRingName = ssName;

	ssRing_Instance_GRD = "";
	ssRing_Instance_Monitor = "";
	ssRing_Instance_Conference = "";
	ssRing_Instance_IFB = "";
	ssRing_Instance_Extras = "";
	ssAProcessString = "";

	iRing_Device_GRD=0;
	iRing_Device_Monitor=0;
	iRing_Device_Conference=0;
	iRing_Device_IFB=0;
	iRing_Device_Extras = 0;

	iRing_Ports_Talk = 0;
	iRing_Ports_Listen = 0;
	iRing_RevsRecord_Monitors = 0;

	for (int ii = 0; ii < MAX_RIEDEL_SLOTS; ii++) {
		CRiedel_LookUp* pData = new CRiedel_LookUp();
		if (pData) {
			// add to map
			cl_RiedelLookUpData.insert(pair<int, CRiedel_LookUp*>(ii, pData)); 
		}
	} // for

}

CRiedel_Rings::~CRiedel_Rings()
{
	// fade to black
	while (cl_RiedelLookUpData.begin() != cl_RiedelLookUpData.end()) 	{
		map<int, CRiedel_LookUp*>::iterator it = cl_RiedelLookUpData.begin();
		if (it->second) delete it->second;
		cl_RiedelLookUpData.erase(it);
	}
}

//////////////////////// private function

CRiedel_LookUp* CRiedel_Rings::GetLookUpData(int iIndex)
{
	if ((iIndex>0) && (iIndex < MAX_RIEDEL_SLOTS)) {
		map<int, CRiedel_LookUp *>::iterator it = cl_RiedelLookUpData.find(iIndex);
		if (it != cl_RiedelLookUpData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;

}

///////////////////////

void CRiedel_Rings::AssignLookupAddress(int iIndex, bncs_string ssData)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook) {
		pLook->SetAddressString(ssData);
	}
}

void CRiedel_Rings::AssignLookupSrcPackage(int iIndex, int iPackage)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook) {
		pLook->SetAssociated_Source_Package(iPackage);
	}
}
void CRiedel_Rings::AssignLookupDestPackage(int iIndex, int iPackage)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook) {
		pLook->SetAssociated_Destination_Package(iPackage);
	}
}
void CRiedel_Rings::AssignLookupConference(int iIndex, int iConf)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook) {
		pLook->SetAssociated_Conference(iConf);
	}
}
void CRiedel_Rings::AssignLookupIFB(int iIndex, int iIFB)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook) {
		pLook->SetAssociated_IFB(iIFB);
	}
}

/////////////////////////////////////////////////////////////////////////////////

bncs_string CRiedel_Rings::AcquireLookupAddress(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAddressString();
	else
		return "";
}

int  CRiedel_Rings::AcquireLookupTrunkAddr(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAddressTrunk();
	else
		return 0;
}

int  CRiedel_Rings::AcquireLookupPort(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAddressPort();
	else
		return 0;
}

int  CRiedel_Rings::AcquireLookupPanel(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAddressPanel();
	else
		return 0;
}

int  CRiedel_Rings::AcquireLookupShift(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAddressShift();
	else
		return 0;
}

int  CRiedel_Rings::AcquireLookupKey(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAddressKey();
	else
		return 0;
}

int CRiedel_Rings::FindLookupIndexByPortKey(int iPort, int iKey)
{
	int iRetIndex = UNKNOWNVAL;
	for (int ii = 1; ii < MAX_RIEDEL_SLOTS; ii++) {
		if ((iPort == AcquireLookupPort(ii)) && (iKey == AcquireLookupKey(ii))) {
			return ii;
		}
	} // for ii
	return iRetIndex;
}



//////////////////////////////////////////////////////////////////////////

int CRiedel_Rings::AcquireLookupSrcPackage(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAssociated_Source_Package();
	else
		return 0;

}

int CRiedel_Rings::AcquireLookupDestPackage(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAssociated_Destination_Package();
	else
		return 0;
}

int CRiedel_Rings::AcquireLookupConference(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAssociated_Conference();
	else
		return 0;
}

int CRiedel_Rings::AcquireLookupIFB(int iIndex)
{
	CRiedel_LookUp* pLook = GetLookUpData(iIndex);
	if (pLook)
		return pLook->GetAssociated_IFB();
	else
		return 0;
}

