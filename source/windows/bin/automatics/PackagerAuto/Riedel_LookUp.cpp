// Riedel_LookUp.cpp: implementation of the CRiedel_LookUp class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Riedel_LookUp.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRiedel_LookUp::CRiedel_LookUp( void )
{
	// port address
	m_iTrunkAddr = 0;
	m_iPort=0;
	m_iPanel=0;
	m_iShift=0;
	m_iKey=0;

	// assignable data
	m_iAssociated_Source_Package = 0;
	m_iAssociated_Destination_Package = 0;
	m_iAssociated_Conf = 0;
	m_iAssociated_IFB = 0;
	
}


CRiedel_LookUp::~CRiedel_LookUp()
{
	// fade to black
}

void CRiedel_LookUp::SetAddressString(bncs_string ssData)
{
	bncs_stringlist ssl = bncs_stringlist(ssData, '.');
	if (ssl.count()>0) {
		if (ssl.count()>0) m_iTrunkAddr = ssl[0].toInt();
		if (ssl.count()>1) m_iPort = ssl[1].toInt();
		if (ssl.count()>2) m_iPanel = ssl[2].toInt();
		if (ssl.count()>3) m_iShift = ssl[3].toInt();
		if (ssl.count()>4) m_iKey = ssl[4].toInt();
	}
}

bncs_string CRiedel_LookUp::GetAddressString(void)
{
	return bncs_string("%1.%2.%3.%4.%5").arg(m_iTrunkAddr).arg(m_iPort).arg(m_iPanel).arg(m_iShift).arg(m_iKey);
}

int CRiedel_LookUp::GetAddressTrunk(void)
{
	return m_iTrunkAddr;
}

int CRiedel_LookUp::GetAddressPort(void)
{
	return m_iPort;
}

int CRiedel_LookUp::GetAddressPanel(void)
{
	return m_iPanel;
}

int CRiedel_LookUp::GetAddressShift(void)
{
	return m_iShift;
}

int CRiedel_LookUp::GetAddressKey(void)
{
	return m_iKey;
}


void CRiedel_LookUp::SetAssociated_Source_Package(int iPackage)
{
	m_iAssociated_Source_Package = iPackage;
}

void CRiedel_LookUp::SetAssociated_Destination_Package(int iPackage)
{
	m_iAssociated_Destination_Package = iPackage;
}

void CRiedel_LookUp::SetAssociated_Conference(int iConf)
{
	m_iAssociated_Conf = iConf;
}

void CRiedel_LookUp::SetAssociated_IFB(int iIFB)
{
	m_iAssociated_IFB = iIFB;
}


int CRiedel_LookUp::GetAssociated_Source_Package(void)
{
	return m_iAssociated_Source_Package;
}

int CRiedel_LookUp::GetAssociated_Destination_Package(void)
{
	return m_iAssociated_Destination_Package;
}

int CRiedel_LookUp::GetAssociated_Conference(void)
{
	return m_iAssociated_Conf;
}

int CRiedel_LookUp::GetAssociated_IFB(void)
{
	return m_iAssociated_IFB;
}

