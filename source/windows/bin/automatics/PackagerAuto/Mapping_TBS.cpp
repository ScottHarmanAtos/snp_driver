// Mapping_TBS.cpp: implementation of the CMapping_TBS class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Mapping_TBS.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMapping_TBS::CMapping_TBS()
{
	for (int i=0;i<129; i++ )
		SrcToConference[i]=UNKNOWNVAL;
}

CMapping_TBS::~CMapping_TBS()
{

}

CMapping_TBS::AddMappingTBRecord( bncs_string ssIdEntry )
{
	ssKeyId = ssIdEntry;
	// read in record from file
	bncs_string ssValue="";
	bncs_string ssFileName="comms_mapping_tb";   
	BOOL bCont = TRUE;
	int iIndex= 1;
	int iNumEnts=0;
	bncs_string ssbase;
	if (ssIdEntry.find("source")>=0) ssbase = "source_";
	if (ssIdEntry.find("aux")>=0) ssbase = "aux_";

	do {
		bncs_string ssItem = bncs_string( ssbase ).append(iIndex);
		bncs_string ssVal = getXMLItemValue(ssFileName, ssIdEntry, ssItem  );
		//OutputDebugString( bncs_string("XML AddTBRec %1  %2 \n").arg(ssItem).arg(ssVal) );	 
		if (ssVal.length()>11) {
			int iIFB = ssVal.right(ssVal.length() - 11).toInt();	// strip out "conference_"
			OutputDebugString( bncs_string("XML AddMappingTBRecord dest_%1  TB4W_%2 \n").arg(iIndex).arg(iIFB) );	 
			if ((iIFB>0)&&(iIndex<121)) {
				SrcToConference[iIndex] = iIFB;
				iNumEnts++;
			}
		}
		iIndex++;	
	} while (iIndex<121);
	OutputDebugString( bncs_string("XML AddMappingTBRecord %1  entries %2 \n").arg(ssIdEntry).arg(iNumEnts) );	 

}
