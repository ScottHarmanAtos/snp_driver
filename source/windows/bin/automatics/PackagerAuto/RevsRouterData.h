// RevsRouterData.h: interface for the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
#define AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
	//
	#include <windows.h>
	#include "PackagerConsts.h"
	#include <bncs_string.h>
	#include <bncs_stringlist.h>
	#include "bncs_auto_helper.h"

#include "RouterSrceData.h"	
#include "RouterDestData.h"	

class CRevsRouterData
{
private:
	int m_iRouterDeviceNumber;     // base dev ini number
	int m_iRouterLockDevice;     // lock dev ini number
	int m_iMaxSources;                  // db0 size
	int m_iMaxDestinations;            // db1 size

	map<int, CRouterDestData*> cl_DestRouterData;   // map for Destination data
	map<int, CRouterSrceData*> cl_SrcRouterData;    // map for source data

	//router data maps are not seen outside class 
	CRouterSrceData* GetSourceData(int iSource);
	CRouterDestData* GetDestinationData(int iDestination);
	void ExtractNVLevelData(char szlvl[64], int* iDestLvl, int* iSrce, int* iSrceLvl);
	BOOL SplitAString(char szStr[64], char cSplit, char *szLeft, char* szRight);

public:
	CRevsRouterData( int iDeviceNumber, int iDests, int iSrcs );
	virtual ~CRevsRouterData();

	// router revertives 

	BOOL storeGRDRevertive(int iDestination, int iNewSource, int iRouteStatus, int iDestLock );  // store given source from actual rev; 

	int getPrimarySourceForDestination(int iDestination);  // return source as got from grd rev
	int getScheduledSourceForDestination(int iDestination);  // return SCHEDULED source lastreturned from grd rev
	int getRouterDestStatus(int iDestination);                          // routed status for dest
	int getRouterDestLock(int iDestination);                             // lock state of dest

	void setExpectedSourceForDestination(int iDestination, int iExpectedSrc, int iCountValue);
	int getExpectedSourceForDestination(int iDestination, BOOL bDecExp);  // return expected source 
	int getExpectedCounterForDestination(int iDestination);

	void addRouterDestPackageIndex(int iDestination, int iPackage);
	void removeRouterDestPackageIndex(int iDestination, int iPackage);
	bncs_string getRouterDestPackageIndex(int iDestination);

	void addReverseReturnPackageIndex(int iDestination, int iPackage);
	void removeReverseReturnPackageIndex(int iDestination, int iPackage);
	bncs_string getReverseReturnPackageIndex(int iDestination);

	int getRouterNumber(void);
	int getMaximumDestinations(void);
	int getMaximumSources(void);

	void addPackageToSourceList(int iSource, int iPackage);
	void removePackageFromSourceList(int iSource, int iPackage);
	bncs_string getAllPackagesForSource(int iSource);
	int getNumberPackagesUsingSource(int iSource);

};

#endif // !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
