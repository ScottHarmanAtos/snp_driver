# Microsoft Developer Studio Project File - Name="PackagerAuto" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=PackagerAuto - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "PackagerAuto.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "PackagerAuto.mak" CFG="PackagerAuto - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "PackagerAuto - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "PackagerAuto - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "PackagerAuto"
# PROP Scc_LocalPath "..\.."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "PackagerAuto - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "..\..\common" /I "$(CC_ROOT)\$(CC_SYSTEM)\source\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 csicln.lib asyncPost.lib extinfo.lib kernel32.lib user32.lib gdi32.lib comctl32.lib version.lib decodecaptionstring.lib bncsif32.lib bncs_string.lib bncs_config.lib /nologo /subsystem:windows /machine:I386 /libpath:"$(CC_ROOT)\$(CC_SYSTEM)\source\lib"
# SUBTRACT LINK32 /map

!ELSEIF  "$(CFG)" == "PackagerAuto - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\..\common" /I "$(CC_ROOT)\$(CC_SYSTEM)\source\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 csiclndb.lib asyncPostdb.lib extinfodb.lib kernel32.lib user32.lib gdi32.lib comctl32.lib version.lib decodecaptionstring.lib bncsif32.lib bncs_string.lib bncs_config.lib /nologo /subsystem:windows /map /debug /machine:I386 /pdbtype:sept /libpath:"$(CC_ROOT)\$(CC_SYSTEM)\source\lib"

!ENDIF 

# Begin Target

# Name "PackagerAuto - Win32 Release"
# Name "PackagerAuto - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\CalrecRevs.cpp
# End Source File
# Begin Source File

SOURCE=.\Command.cpp
# End Source File
# Begin Source File

SOURCE=.\CommonXMLData.cpp
# End Source File
# Begin Source File

SOURCE=.\Conference.cpp
# End Source File
# Begin Source File

SOURCE=.\DestinationPackages.cpp
# End Source File
# Begin Source File

SOURCE=.\main.cpp
# End Source File
# Begin Source File

SOURCE=.\PackagerAuto.cpp
# End Source File
# Begin Source File

SOURCE=.\PackagerAuto.rc
# End Source File
# Begin Source File

SOURCE=.\RevsRiedelData.cpp
# End Source File
# Begin Source File

SOURCE=.\RevsRouterData.cpp
# End Source File
# Begin Source File

SOURCE=.\Riedel_LookUp.cpp
# End Source File
# Begin Source File

SOURCE=.\SourcePackages.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TBPortPackageUse.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\CalrecRevs.h
# End Source File
# Begin Source File

SOURCE=.\Command.h
# End Source File
# Begin Source File

SOURCE=.\CommonXMLData.h
# End Source File
# Begin Source File

SOURCE=.\Conference.h
# End Source File
# Begin Source File

SOURCE=.\DestinationPackages.h
# End Source File
# Begin Source File

SOURCE=.\PackagerAuto.h
# End Source File
# Begin Source File

SOURCE=.\PackagerConsts.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\RevsRiedelData.h
# End Source File
# Begin Source File

SOURCE=.\RevsRouterData.h
# End Source File
# Begin Source File

SOURCE=.\Riedel_LookUp.h
# End Source File
# Begin Source File

SOURCE=.\SourcePackages.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TBPortPackageUse.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\DownArrow.bmp
# End Source File
# Begin Source File

SOURCE=.\PackagerAuto.ico
# End Source File
# Begin Source File

SOURCE=.\PackAutoTitle.bmp
# End Source File
# Begin Source File

SOURCE=.\small.ico
# End Source File
# Begin Source File

SOURCE=.\UpArrow.bmp
# End Source File
# End Group
# Begin Group "common"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\common\bncs_auto_helper.cpp
# End Source File
# Begin Source File

SOURCE=..\..\common\bncs_auto_helper.h
# End Source File
# Begin Source File

SOURCE=.\database.cpp
# End Source File
# Begin Source File

SOURCE=.\database.h
# End Source File
# Begin Source File

SOURCE=.\databaseMgr.cpp
# End Source File
# Begin Source File

SOURCE=.\databaseMgr.h
# End Source File
# End Group
# Begin Group "XMLData"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Area_ObjectSettings.cpp
# End Source File
# Begin Source File

SOURCE=.\Area_ObjectSettings.h
# End Source File
# Begin Source File

SOURCE=.\ConferenceAssign.cpp
# End Source File
# Begin Source File

SOURCE=.\ConferenceAssign.h
# End Source File
# Begin Source File

SOURCE=.\Dest_Pkages_Collections.cpp
# End Source File
# Begin Source File

SOURCE=.\Dest_Pkages_Collections.h
# End Source File
# Begin Source File

SOURCE=.\Mapping_IFBS.cpp
# End Source File
# Begin Source File

SOURCE=.\Mapping_IFBS.h
# End Source File
# Begin Source File

SOURCE=.\SlaveIndex.cpp
# End Source File
# Begin Source File

SOURCE=.\SlaveIndex.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\PackagerAuto.txt
# End Source File
# End Target
# End Project
