// RouterData.h: interface for the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROUTERDESTDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
#define AFX_ROUTERDESTDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "PackagerConsts.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CRouterDestData  
{
private:

/*	struct iRoutedData{
		int iDestLevel;
		int iSrceIndex;
		int iSrceLevel;
	};
	struct iRoutedData stRoutedData[65];
*/

public:
	CRouterDestData( int iIndx );
	virtual ~CRouterDestData();

	int m_iMapIndex;       // bncs destination index too
	//
	int m_iRoutedIndex;   // store first index from router rev string -- will be primary source IF active
	int m_iExpectedIndex;    // route command has been issued - but revertive not returned yet
	int m_iPreviousRoutedIndex;
	//
	int m_iScheduledIndex;
	int m_iRouteStatus;      // parked, scheduled, active etc
	int m_iLockStatus;

	// now lists to cope with multiple associations
	bncs_string m_ssDestination_Package;            //  assoc dest pkg using this router destination
	bncs_string m_ssReverseReturn_Package;       //  assoc audio pkg for dest index for reverse vision/audio return

	int m_iExpectedCounter;     // counter used to limit expectedIndex - to prevent locking out of routing 
};

#endif // !defined(AFX_ROUTERDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
