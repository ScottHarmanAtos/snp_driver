//////////////////////////////////////
// header file for Router_Umd_Tally project //
//////////////////////////////////////

#if !defined(AFX_Router_Umd_Tally_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
#define AFX_Router_Umd_Tally_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include "resource.h"
#include "UMD_Tally_Common.h"


// TODO:	Insert additional constants, function prototypes etc. in this file
/* max number of items in listbox */
#define		MAX_LB_ITEMS	1500

#define		APP_WIDTH	1060
#define		APP_HEIGHT	900

#define		INFO_DISCONNECTED  0	// infodriver not there or whatever
#define		INFO_RXMODE               1		// rx mode only - dual redundancy
#define		INFO_TXRXMODE          2		// connected, and in true rxtx mode

// defined colours used in app
#define  COL_BLACK  RGB(0,0,0)
#define  COL_WHITE RGB(255,255,255)

#define  COL_DK_RED RGB(127,0,0)
#define  COL_DK_GRN RGB(0,127,0)
#define  COL_DK_BLUE RGB(0,0,80)
#define  COL_DK_ORANGE RGB(128,64,0)

#define  COL_LT_RED RGB(255,0,0)
#define  COL_LT_GRN RGB(0,255,0)
#define  COL_LT_BLUE RGB(100,100,255)
#define  COL_LT_ORANGE RGB(255,128,0)

#define  COL_YELLOW RGB(255,255,0)
#define  COL_TURQ RGB(180,180,210)


#define ONE_SECOND_TIMER_ID		1	 // could be used to relook for comms port / infodrivers -- for more intelligent driver
#define COMMAND_TIMER_ID		  2							// 
#define STARTUP_TIMER_ID	    	3							// 
#define DATABASE_TIMER_ID	       4							// 
#define FORCETXRX_TIMER_ID      7					// 


#define ONE_SECOND_TIMER_INTERVAL	 1000 // 1.5 sec timer used for housekeeping - mainly for re connects to info driver, comport connection failures, device interrogation 
#define COMMAND_TIMER_INTERVAL			100 // 0.1 sec interval on commands being sent from the fifobuffer 
#define STARTUP_TIMER_INTERVAL			1000 // 
#define TENSECONDS								10  
#define SIXTYSECONDS							60  


// slot usage
// panels have 10 slots each for use in auto 11..19, 21..29, 31..39 etc
// first 10 are reserved for auto use
#define COMM_STATUS_SLOT		4001

#define DEBUG_STATUS_SLOT	    4002


/* main.cpp */
/* prototypes for main functionality */
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT				InitApp(HWND);
void				CloseApp(HWND);

void				LoadIni(void);
char *				getBNCS_File_Path( void );    // new for v4.2 of wizard - assigns the global szBNCS_File_Path
char*				r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite);
void				w_p(char* file, char* section, char* entry, char* setting);
int					getWS(void);

void				Debug(LPCSTR szFmt, ...);
void				Log(LPCSTR szFmt, ...);
void				AssertDebugSettings(HWND hWnd);

int				SplitString(char *string, char *delim, UINT count, char **outarray );
void          InitStatusListboxDialog( void );  // helper function to initialise listbox on app dialog
BOOL			SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight);


/* Router_Umd_Tally.cpp /*

/* prototypes for the notification functions */
void UpdateCounters(void);
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot);
LRESULT CSIClientNotify(extclient* pec,LPCSTR szMsg);

void ProcessNextCommand( int iRateRevs );
BOOL LoadAllInitialisationData( void );
void ObtainLastKnownState( void );

BOOL AutomaticRegistrations(void);
void PollPhysicalRouters(int iCounter);
void DisplayListviewsPanels( void );
void VersionInfo();
void CheckAndSetInfoResilience( void );
void Restart_CSI_NI_NO_Messages(void);
void ForceAutoIntoRXOnlyMode(void);
void ForceAutoIntoTXRXMode(void);

void ClearCommandQueue( void );
void RefeshUMDDevice( int iWhichDevice );
void DisplayUmdDeviceData();

void ProcLabelsForGivenDevice( CUmdDevice* pUmd, bncs_string ssDevice );
void ProcessLabelsForAllUMDS();    //calc umd for all labels - called at startup
BOOL TracePrimaryRouterSource(int iStartRtr, int iStartSrc, int *iFinRtr, int *iFinSrc);


/*function to create nice new paint brushes*/
void CreateBrushes();
/*function to clean paint brushes and put them away*/
void ReleaseBrushes();
void FillTheLabel( int iWhichButton, int iColour, char* szText  );
BOOL ExtractSrcNamefromDevIniFile( int iDev, int iEntry, int iDbFile, bncs_string* ssName );

CUmdDevice* GetValidUmdDevice( int iDev);
CRevsRouterData* GetMainSDIRouter( );
CRevsRouterData* GetValidRouter( int iWhichRouter );


#ifndef EXT
#define EXT extern
#endif


/////////////////////////////////////////////////////////////////////
// Global Variables for the Router_Umd_Tally project
/////////////////////////////////////////////////////////////////////

EXT HINSTANCE hInst;						// current instance
EXT TCHAR szTitle[MAX_LOADSTRING];			// The title bar text
EXT TCHAR szWindowClass[MAX_LOADSTRING];	// The title bar text
EXT char szBNCS_File_Path[MAX_BUFFER_STRING]; // global for Colledia system file path - assigned in getBNCS_File_Path, used in r_p and w_p
EXT char szBNCS_Log_File_Path[MAX_BUFFER_STRING];  // global for crash log file path - assigned in getBNCS_File_Path
EXT char szAppName[ MAX_LOADSTRING];                     // application name used in crash log messages
EXT char szResult[MAX_BUFFER_STRING];						// global for profile reads
EXT BOOL fLog;								// log enable flag
EXT int iDebugFlag;							   // debug enable flag
EXT BOOL fDebugHide;						// debug visible flag
EXT HWND hWndMain,hWndList;					// window handles, main and debug
EXT HWND hWndDlg;							// window handle for child dialog
EXT int iChildHeight;						// height of child dialog
EXT RECT rc;
EXT int iDevice;							// command line parameter = external driver #
EXT int iWorkstation;						// workstation number from CSI.INI
EXT ULONG lTXDEV;							// Device tx count
EXT ULONG lRXDEV;							// Device rx count
EXT ULONG lTXID;							// ID tx count
EXT ULONG lRXID;							// ID rx count

EXT bncs_string ssAutoInstance;   // auto instance passed in as parameter for startup - no more integers; get device id from given instance

// CLASSES used by auto
EXT extclient *ecCSIClient;				// a client connection class
EXT extinfo *eiExtInfoId;			// external infodriver for driver

EXT int i_rtr_TielineRtr_Index;                          // main SDI / Tieline router index from ini file -- other routers loaded as and when from mv_inputs_outputs as required
EXT int i_rtr_CMRRtr_Index;                          // CMR router index from ini file -- other routers loaded as and when from mv_inputs_outputs as required
EXT int i_CMR_VirtualsStart;                          // 577 to 640 are virtuals in router - so trace real source on cmr - what id src is incoming highway from TX routers ? - trace too

EXT bncs_stringlist ssl_AllRoutersIndices;               // list of integer ( router device numbers ) keys into map
EXT map<int, CRevsRouterData*>* cl_AllRouters;    // int key is number of device, primarily identified by dev 

EXT bncs_stringlist ssl_AllUMDDevices;                                  // list of keys into map
EXT map<bncs_string,  CUmdDevice*>* cl_AllUmdDevices;    // bncs_string is key is number of device, primarily identified by dev and slot number

EXT queue<CCommand*> qCommands;                                   // queue of commands to go to bncs drivers, routers, other devices 

EXT char szAutoId[ MAX_LOADSTRING];

EXT int iOverallTXRXModeStatus;			// overall mode -- based on base infodriver i.e gpi input driver
EXT BOOL bConnectedToInfoDriverOK;	// connected to the infodriver or not
EXT BOOL bValidCollediaStatetoContinue;
EXT BOOL bAutomaticStarting;
EXT BOOL bForceUMDUpdate; 
EXT BOOL bShowAllDebugMessages;
EXT BOOL bRecalcAllTalliesFromRM;
EXT int iStartupCounter;

EXT int iOneSecondPollTimerId;
EXT BOOL bOneSecondPollTimerRunning;
EXT int iNextResilienceProcessing;        // count down to zero -- check info tx/rx status 
EXT int iOverallStateJustChanged;
EXT int iOverallModeChangedOver;

EXT int iNextCommandTimerId;
EXT BOOL bNextCommandTimerRunning;
EXT BOOL bRMDatabaseTimerRunning;

// auto globals
EXT int iNumberActiveUmdDevs, iNumberActiveRouters;
EXT int iCurrentChosenDevice;
EXT bncs_string ssCurrentChosenDevice;

EXT int i_SourceName_DB;
EXT int i_SoftName_DB;
EXT int i_Max_UMD_Length;

EXT databaseMgr dbm;  // for speedy getting of database names

EXT bncs_string ss_UMD_Join; // configureable string to use between db4 and db2 portions of umd

// rm cmds - store db,index for src / dest RM changes
EXT bncs_stringlist ssl_RMChanges;

EXT bncs_stringlist ssl_UseNoPrefix_Sources;


///////////////////////////////////////////////////////////////////

EXT	HBRUSH hbrWindowsGrey;
EXT	HBRUSH hbrBlack;
EXT	HBRUSH hbrWhite;

EXT	HBRUSH hbrDarkRed;
EXT	HBRUSH hbrDarkGreen;
EXT	HBRUSH hbrDarkBlue;
EXT	HBRUSH hbrDarkOrange;

EXT	HBRUSH hbrYellow;
EXT	HBRUSH hbrTurq;

EXT	HBRUSH hbrLightRed;
EXT	HBRUSH hbrLightGreen;
EXT	HBRUSH hbrLightOrange;
EXT	HBRUSH hbrLightBlue;

EXT HBRUSH HBr, HBrOld;

EXT COLORREF iCurrentTextColour;
EXT COLORREF iCurrentBackgroundColour;
EXT HBRUSH iCurrentBrush;

#endif // !defined(AFX_Router_Umd_Tally_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
