// UmdDataEntry.cpp: implementation of the CCommand class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UmdDataEntry.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUmdDataEntry::CUmdDataEntry( int iIndex)
{
	// init vars
	m_iUmdIndex = iIndex;
	m_iUmd_Router=0;         
	m_iUmd_Src_Dest=0;         
	m_iUmd_SrcDest_Type=0;     

	m_ssCalcUmdName="";  
	m_ssAuxLabel1="";     

	m_ssLabelCommand="";  
	m_ssAuxCommand="";    

}

CUmdDataEntry::~CUmdDataEntry()
{
    // fade to black
}
