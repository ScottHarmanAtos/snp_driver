#include "RouterDataDest.h"

CRouterDataDest::CRouterDataDest( int iIndex )
{
	m_iDestIndex = iIndex;
	m_iRoutedSourceNumber=0;							 
	m_iDestLockState=0;

	m_ssLinkedToUmdDevice = bncs_stringlist("",',');              

	// tieline info                             // used between main and gfx routers in 6
	 m_iTielineToRtr=0;                           // other router this dest feeds 
	 m_iTielineToSrc=0;                           // source this dest feeds on other router
}


CRouterDataDest::~CRouterDataDest()
{
}
