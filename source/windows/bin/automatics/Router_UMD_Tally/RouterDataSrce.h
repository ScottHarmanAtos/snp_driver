
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"
#include "UMD_Tally_Common.h"
//
class CRouterDataSrce
{
public:
	CRouterDataSrce( int iIndex );
	~CRouterDataSrce();

	int m_iSrceIndex;

	bncs_stringlist m_ssRoutedToDest;                   // quick lookup of dests that src is routed to -- to set tallies for linked devices to those dests...
	bncs_stringlist m_ssLinkedToUmdDevice;              // quick lookup for umd device assoc with this src 

	bncs_string m_ssAssoc_InfodriverRevs1;              // links to infodriver revs records -- used in this auto for tx chain alarm status
	bncs_string m_ssAssoc_InfodriverRevs2;              // links to infodriver revs records -- used in this auto for tx chain alarm inhibit
	bncs_string m_ssAssoc_InfodriverRevs3;              // links to infodriver revs records -- used in this auto for tx chain alarm comms

	// tieline info                                          // used between cmr and chain routers 
	int m_iTielineFromRtr;                           // other router this source is fed from 
	int m_iTielineFromDest;                          // dest that feeds this src from other router

	int m_iAssociatedFriend;

};

