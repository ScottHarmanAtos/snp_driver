// InfodriverRevs.cpp: implementation of the CInfodriverRevs class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "InfodriverRevs.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "InfodriverRevs.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInfodriverRevs::CInfodriverRevs(bncs_string ssKey, int iDriver, int iSlot)
{

	ssRecordKey = ssKey;
	ssName="";
	iRecordUseType = 0;
	iInfoDriver = iDriver;
	iInfoSlot = iSlot;

	ss_SlotData = "";
	i_SlotValue_part1 = 0;
	i_SlotValue_part2 = 0;

	i_Associated_Resource_ID = 0;
	i_Associated_Resource_Index = 0;

	ss_LinkedTo_InfoRevsRecord = "";

}

CInfodriverRevs::~CInfodriverRevs()
{
	// oblivion beckons
}
