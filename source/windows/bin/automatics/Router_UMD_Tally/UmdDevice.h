// UmdDevice.h: interface for the CUmdDevice class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UMDDEVICE_H__40CE825D_7AC9_4FEC_9674_6A51640C22A0__INCLUDED_)
#define AFX_UMDDEVICE_H__40CE825D_7AC9_4FEC_9674_6A51640C22A0__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
	#pragma warning(disable : 4786 4996)
//
	#include <windows.h>
	#include "UMD_Tally_Common.h"
	//
	#include <map>
	using namespace std;

	#include "UmdDataEntry.h"

class CUmdDevice  
{
private:
	bncs_string m_ssDeviceName;   // key into record  -- same key used for common xml data
	bncs_string m_ssDeviceType;     // umd or mv type string
	bncs_string m_ssDeviceLocation;     // location of mv 
	bncs_string m_ssAltDeviceId;     //  alt id from instances
	int m_iDeviceType;                    // device type : TSL UMD,  Evertz Multiviewer
	int m_iListBoxIndex;

	int iDriverDevice;
	int iDeviceStartOffset;
	int i_TSLUMD_Address;

	int m_iNumberUMDS;
	bncs_stringlist m_ssl_Sources_Destinations;    // all sources or destinations for umd 
	//
	int m_iNumberUmdDataEntries;         // umds for this device
	map<int, CUmdDataEntry*>* cl_AllUmdData;
	// private function for use within this class only
	CUmdDataEntry* getUmdDataEntry(int iIndex);


public:
	CUmdDevice(bncs_string ssDevName, bncs_string ssDevType, int iDevType, int iDev, int iOffset, bncs_string ssLoc, bncs_string ssAltId);
	virtual ~CUmdDevice();

	void storeUMDDetails( int iIndex, int iRouter, int iSrcDest, int iSrcDestType, bncs_string ssFixed="" );
	void storeCalculatedUmdName( int iIndex, bncs_string ssName );
	void storeUmdAuxLabel( int iIndex, bncs_string ssLabel );

	void getUMDDevice(bncs_string*  ssDevName, bncs_string* ssDevType, bncs_string* ssLoc, bncs_string* ssAlt, int* iDevType, int* iNumberUmds);
	int getUMDDevType( void );
	int getNumberOfUMDs( void );
	int getBNCSDriver( void );
	int getDriverOffset( void );

	bncs_string getAllSourceDestinations( void );
	bncs_string getAllUmdDetails( int iIndex, int* iSrcDestType, int* iSrcDest, bncs_string *ssAuxLabel );

	int getIndexGivenSrcDest( int iSrcDest, int iType, int iRouter );
	int getUmdTypeGivenIndex( int iIndex );
	int getUmdTypeAndSrcDestByIndex( int iIndex, int* iSrcDest );
	int getUmdSrcDestRouter( int iIndex );
	bncs_string getCalcUmdText( int iIndex );

	void storeListBoxEntry( int iIndex );
	int getListBoxEntry( void );

	void  setRecentLabelCommand( int iIndex, bncs_string ssCmd );
	bncs_string  getRecentLabelCommand( int iIndex );
	void  setRecentAuxCommand( int iIndex, bncs_string ssAuxCmd );
	bncs_string  getRecentAuxCommand( int iIndex );
	bncs_string getUmdAuxLabel( int iIndex );

	void setTSLAddress( int iAddr );
	int  getTSLAddress( void );

};

#endif // !defined(AFX_UMDDEVICE_H__40CE825D_7AC9_4FEC_9674_6A51640C22A0__INCLUDED_)
