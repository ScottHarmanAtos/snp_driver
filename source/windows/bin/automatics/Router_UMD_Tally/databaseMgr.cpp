/********************************************/
/* Written by David Yates                   */
/* Copyright Siemens Business Services 2006 */
/********************************************/

/** 
\class databaseMgr
\brief BNCS "Databases" Access Class

This class is used to cache and access BNCS "databases" stored in database objects.
 */
#include "databaseMgr.h"
#include <windows.h>

/** Default constructor
This constructor will set the internal path member variable to a default value. <p>
If CC_ROOT and CC_SYSTEM environment variables exist then the path is set to 
%CC_ROOT%\\%CC_SYSTEM%\\config\\system, 
otherwise it is set to the Windows directory for this installation.
\sa path
*/
databaseMgr::databaseMgr()
{
	char *p = getenv( "CC_ROOT" );
	char* s = getenv( "CC_SYSTEM" );

	// see if we have a V4 environment installed
	if( p && s )
		sprintf( m_path, "%s\\%s\\config\\system", p, s );
	// otherwise just go off and get the windows directory
	else
		GetWindowsDirectory( m_path, sizeof( m_path ));
}

/** Constructor
\param path The path used to find/store database information (without trailing slash)
Set the path to the configuration files explicitly. Default behaviour for BNCS systems is 
already available in the default constructor which should be used by default.
\sa path
*/
databaseMgr::databaseMgr( const char * path )
{
	setPath( path );
}

/** Set path to data files
If any database files have already been cached then these are deleted and the path reset
*/
void databaseMgr::setPath( const char * path )
{
	// remove any existing cached entries
	clear();

	if( path )
	{
		// work out how long the path string is
		int len = strlen( path );

		// only copy what we need or have room for
		strncpy( m_path, path, (len < sizeof( m_path ))? len+1 : (sizeof( m_path ) - 1));
		// and make sure it's got a term
		m_path[ sizeof( m_path ) - 1 ] = '\0';
	}
	else
		OutputDebugString( "databaseMgr::setPath( const char * path ) - path variable is zero" );
}

/** Destructor
Cleans up all the resources used by this class
*/
databaseMgr::~databaseMgr()
{
	clear();
}

/** Cleanup the database objects that we've loaded
This is an internal function only
*/
void databaseMgr::clear(void )
{
	// just rattle through the map and delete anything that we find
	for( map< int, database *>::iterator it = dbs.begin() ; it != dbs.end() ; ++it )
		delete it->second;

	// remove all our now deleted references
	dbs.clear();
}

/** 
\return Returns pointer to current path variable (as set or determined in the constructor(s))
 */
const char * databaseMgr::path( void )
{
	return m_path;
}

/** 
\param device CC device number
\param db database number (usually 0-9)
\param index Index within this database
\return Name from database (note this pointer is only guaranteed to be valid until the next database call)

\par Example:
\code
databaseMgr dbm;

const char *name = dbm.getName( 123, 0, 1 );		// name contains the value of database name at 123/0/1
\endcode
 */
const char* databaseMgr::getName( int device, int db, int index )
{
	database *d = getDatabaseObject( device, db );
	if( d )
		return d->getName( index );
	else
		return "- - -";
}

/** Create the database object required for the specified params
\param device CC Device number
\param db database number
This is a private function
 */
database * databaseMgr::doCreate( int device, int db )
{
	return new database( m_path, device, db );
}

/** Get database index for the given name
\param device CC Device number
\param db CC database number
\param name The name to searh for
\return the index of the name supplied or zero if this name was not found

\par Example:
\code
databaseMgr dbm;

int i = dbm.getIndex( 123, 0, "Hello" );		// i contains the index of the first instance of the name "Hello" or zero if this entry was not found.
\endcode
 */
int databaseMgr::getIndex( int device, int db, const char* name )
{
	database *d = getDatabaseObject( device, db );
	if( d )
		return d->getIndex( name );
	return 0;
}

/** Get database index for the given name
\param device CC Device number
\param db CC database number
\param name The name to searh for
\param caseInsensitive Whether search is not case sensitive
\param substring Whether to match the first occurance of any part of the string
\return the index of the name supplied or zero if this name was not found

\par Example:
\code
databaseMgr dbm;

int i = dbm.getIndex( 123, 0, "Hello" );		// i contains the index of the first instance of the name "Hello" or zero if this entry was not found.
\endcode
 */
int databaseMgr::getIndex( int device, int db, const char* name, bool caseInsensitive, bool substring )
{
	database *d = getDatabaseObject( device, db );
	if( d )
		return d->getIndex( name, caseInsensitive, substring );
	return 0;
}

/** 
\return The number of loaded databases
 */
int databaseMgr::numLoaded( void )
{
	return dbs.size();
}

/** Set a database name
\param device CC device number
\param db CC database number
\param index index within database
\param name the new name
\returns zero on success

\par Example:
\code
databaseMgr dbm;

dbm.setName( 123, 0, 1, "Hello" );		// sets device 123, database zero (usually .db0 file), index 1 to name "hello"
\endcode
 */
bool databaseMgr::setName( int device, int db, int index, const char * name, bool writeToFile )
{
	database *d = getDatabaseObject( device, db );
	if( d )
		return d->setName( index, name, writeToFile );
	return true;
}

/** Create a new entry for the specified params
\param name The name to use in the database - this just identifies this database
\param device The CC device number
\param sizeDB0 The size of database 0
\param sizeDB1 The size of database 1
\param sizeDB2 The size of database 2
\param sizeDB3 The size of database 3
\param sizeDB4 The size of database 4
\param sizeDB5 The size of database 5
\param sizeDB6 The size of database 6
\param sizeDB7 The size of database 7
\param sizeDB8 The size of database 8
\param sizeDB9 The size of database 9
\return zero on success

Creates a DEV_<i>zzz</i>.INI file of the following format:

\code
[Device]
Name=Device Name Here!

[Database]
DatabaseFile_0=DEV_100.DB0
DatabaseFile_1=DEV_100.DB1
DatabaseFile_2=DEV_100.DB2
DatabaseFile_3=DEV_100.DB3
DatabaseFile_4=DEV_100.DB4
DatabaseFile_5=DEV_100.DB5
DatabaseFile_6=DEV_100.DB6
DatabaseFile_7=DEV_100.DB7
DatabaseFile_8=DEV_100.DB8
DatabaseFile_9=DEV_100.DB9
DatabaseSize_0=0
DatabaseSize_1=0
DatabaseSize_2=0
DatabaseSize_3=0
DatabaseSize_4=0
DatabaseSize_5=0
DatabaseSize_6=0
DatabaseSize_7=0
DatabaseSize_8=0
DatabaseSize_9=0
\endcode
\note Existing settings are simply overwritten
 */
bool databaseMgr::createDatabase(	const char * name, 
						int device, 
						int sizeDB0, 
						int sizeDB1, 
						int sizeDB2, 
						int sizeDB3, 
						int sizeDB4, 
						int sizeDB5, 
						int sizeDB6, 
						int sizeDB7, 
						int sizeDB8, 
						int sizeDB9)
{
	char filename[ MAX_PATH ];
	char entry[ 64 ];
	char section[ 64 ];

	sprintf( filename, "%s\\dev_%03d.ini", m_path, device );

	sprintf( entry, "Device %d", device );
	WritePrivateProfileString( "Device", "Name", entry, filename );

	for( int x = 0 ; x < 10 ; x++ )
	{
		sprintf( section, "DatabaseFile_%d", x );
		sprintf( entry, "dev_%03d.db%d", device, x );
		WritePrivateProfileString( "Database", section, entry, filename );
	}

	WritePrivateProfileInt( "Database", "DatabaseSize_0", sizeDB0, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_1", sizeDB1, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_2", sizeDB2, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_3", sizeDB3, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_4", sizeDB4, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_5", sizeDB5, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_6", sizeDB6, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_7", sizeDB7, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_8", sizeDB8, filename );
	WritePrivateProfileInt( "Database", "DatabaseSize_9", sizeDB9, filename );
	return false;
}

/** WritePrivateProfileInt
This is a simple internal helper class since Windows doesn't supply one of its own.
*/
int databaseMgr::WritePrivateProfileInt( const char* section, const char* entry, int value, const char* filename )
{
	char sValue[ 32 ];

	sprintf( sValue, "%d", value );
	return WritePrivateProfileString( section, entry,sValue, filename );
}

/** Get database object for specified params
\param device CC Device number
\param db Database number
\returns database object
This is an internal helper class to get the database object for the specified device and level
 */
inline database * databaseMgr::getDatabaseObject( int device, int db )
{
	map< int, database *>::iterator it = dbs.find( ( device << 16 ) + db );
	// if we don't find a database object - go off and create one
	if( it == dbs.end())
	{
		database *d = doCreate( device, db );
		// remember it in the map
		dbs.insert( pair<int, database*>( ( device << 16 ) + db, d ));
		return d;
	}

	return it->second;
}

int databaseMgr::size( int device, int db )
{
	char filename[ MAX_PATH ];
	char entry[ 64 ];

	sprintf( filename, "%s\\dev_%03d.ini", m_path, device );
	sprintf( entry, "DatabaseSize_%d", db );

	return GetPrivateProfileInt( "Database", entry, 0, filename );
}

int databaseMgr::exists( int device, int db )
{
	database *d = getDatabaseObject( device, db );

	return d->size();
}
