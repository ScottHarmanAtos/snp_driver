// Include in all files to enable debug and status dialog updates

/* max size of buffer for data from string resource */
#define	MAX_LOADSTRING	100

/* buffer constant for strings */
#define	MAX_BUFFER_STRING 256

// controlled device types
#define TSL_UMD_TYPE_16           1   // v3 or v4 tsl umd protocol
#define TSL_UMD_TYPE_2X8          2
#define TSL_V5UMD_SAMVIEW         3  // v5 umd protocol --- uses different slots than for v3 in umddrv3 driver
#define BARCO_TSL_MVIEW           4
#define KALEIDO_MVIP              5
#define EVERTZ_MVIP				  6

#define MAX_SLOTS_PER_EVERTZ     500
#define MAX_SLOTS_PER_TSL              10     // new driver is now 10 slots per device -- same for SAM MV using TSL 5 protocol
#define MAX_SLOTS_PER_BARCO        10      

#define TSL_CUE_ALLOFF   0
#define TSL_CUE_LEFTON   1
#define TSL_CUE_RIGHTON  2
#define TSL_CUE_BOTHON   3

#define TSL_TXT_COLOUR   4

#define TSL_BRIGHT_OFF  0
#define TSL_BRIGHT_SEVENTH  1
#define TSL_BRIGHT_HALF  2
#define TSL_BRIGHT_FULL  3

#define TALLY_OFF      0     //  tally off by default
#define TALLY_RED_ON   1     // left red tally on
#define TALLY_GRN_ON   2     // right tallies are green
#define TALLY_YEL_ON   3     // - so needed ???
#define TALLY_BLU_ON   4     // - so needed ???
#define TALLY_TEXT_ON  5     // 3rd tally in studio 6 and also yellow tally in news

#define	MAX_EVERTZ_TEXT      16              
#define	MAX_TSL_TEXT               16             
#define	MAX_BARCO_TEXT          16             
#define	MAX_SAM_MV_TEXT       16             

// tally types
#define	LEFT_TALLY  1   // for red tallies
#define	RIGHT_TALLY 2   // for green tallies
#define	TEXT_TALLY  3   // from lshape revs - text colour tallies
#define	MISC_TX_TALLY  4   // from alarms TX or misc tallies

// these need to align with revertives from HP card
#define	MISC_TX_TALLY_OK        1   // from alarms TX or misc tallies
#define	MISC_TX_TALLY_WARN      2   // from alarms TX or misc tallies
#define	MISC_TX_TALLY_FAIL      3    // from alarms TX or misc tallies

#define HP10_SDI_STATUS_OK   1
#define HP10_SDI_STATUS_WARN 2
#define HP10_SDI_STATUS_FAIL 3

#define HP10_COMMS_STATUS_OK 1
#define HP10_COMMS_STATUS_FAIL 0

#define   INFODRIVER_REC_TX_ALARM_STATUS  1
#define   INFODRIVER_REC_TX_ALARM_INHIBIT 2
#define   INFODRIVER_REC_TX_ALARM_COMMS 3

#define	MAXROUTERSIZE 2001          //1..2000   was 1152        
#define	MAXINFODRIVERSIZE 4097    //1..4096           

#define	SOFT_NAME_DB 2              // default values -- 
#define	SOURCE_NAME_DB 4

#define	ROUTERCOMMAND 1
#define	INFODRVCOMMAND 2
#define	GPIOCOMMAND 3
#define	DATABASECOMMAND 4
#define	AUTOINFOREVERTIVE 5

#define	UNKNOWNVAL -1

#define	BNCS_COMMAND_RATE  100 // 50 //30 //5 //10          // number commands sent out per iteration of command timer

#define DEST_UMD_TYPE   0
#define SOURCE_UMD_TYPE 1
#define FIXED_UMD_TYPE  2

#define	ROUTER_SRCE_TYPE 1
#define	ROUTER_DEST_TYPE 2
#define	ROUTER_DYNAMIC_TYPE 3     // kahuna dynamic tally type


// studio mode -- from mgn / studio logic auto 
#define MODE_RIG 0
#define MODE_REH 1
#define MODE_TX 2

// mixer state   -- from studio logic auto 
#define MIXER_MAIN 0
#define MIXER_RESERVE 1
#define MIXER_ECUT 2
#define MIXER_BYPASS 3


// $ = special char meaning permanent UMD override
#define PERM_UMD_CHAR 36

#define UMD_TEXT_PRIMARY    1
#define UMD_TEXT_SECONDARY  2

#define SLOT_OFFSET_AUX_UMD		2000
#define SLOT_OFFSET_VIP_AUX_UMD 50
