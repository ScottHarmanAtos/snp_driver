// InfodriverRevs.h: interface for the CInfodriverRevs class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INFODRIVERREVS_H__080CB23A_40C5_426F_A25C_40CAA79D1F34__INCLUDED_)
#define AFX_INFODRIVERREVS_H__080CB23A_40C5_426F_A25C_40CAA79D1F34__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include "UMD_Tally_Common.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"


class CInfodriverRevs  
{
public:
	CInfodriverRevs(bncs_string ssKey, int iDriver, int iSlot);
	virtual ~CInfodriverRevs();

	// this class used to store infodriver revs - linked to TX alarms  assignments etc
	bncs_string ssRecordKey;
	bncs_string ssName;

	int iInfoDriver;
	int iInfoSlot;
	int iRecordUseType;

	bncs_string ss_SlotData;    // raw data from revertive from infodriver
	// in event of string list data :
	int i_SlotValue_part1;
	int i_SlotValue_part2;

	int i_Associated_Resource_ID;        // eg main router number -- often this data loaded from object settings xml, or area mixer 
	int i_Associated_Resource_Index;   // eg router source, or mixer channel etc...

	bncs_string ss_LinkedTo_InfoRevsRecord;   // link to any other infodriver record  
	
};

#endif // !defined(AFX_INFODRIVERREVS_H__080CB23A_40C5_426F_A25C_40CAA79D1F34__INCLUDED_)
