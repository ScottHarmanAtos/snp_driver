///////////////////////////////////////////////
//	main.cpp - core code for the application //
///////////////////////////////////////////////

/************************************
  REVISION LOG ENTRY
  Revision By: Chris Gil
  Revised on 23/07/2002 19:40:00
  Version: V
  Comments: Added SNMP 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 18/07/2001 11:29:51
  Version: V
  Comments: Modified extinfo, extclient, extdriver, to have connect() function so
			all constructors take no parameters
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 28/03/2001 14:42:29
  Version: V
  Comments: new comms class specifies the serial port number in the open() function
  now, rather than the constructor
 ************************************/


/************************************
  REVISION LOG ENTRY
  Revision By: PaulW
  Revised on 20/09/2006 12:12:12
  Version: V4.2 
  Comments: r_p and w_p -- will look first for 4.5 CC_ROOT and SYSTEM,   
                        then  ConfigPath defined for V3 systems 
						or as a last resort the default Windows/Winnt directory for dev ini files etc 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: RichardK
  Revised on 03/08/2007
  Version: V4.3
  Comments: Debug turns off redraw of list-box while updating it.  To stop flicker problems seen on
		systems accessed using Remote Desktop.
 ************************************/


#include "stdafx.h"

#define EXT
#include "Router_Umd_Tally.h"

//
//  FUNCTION: WinMain()
//
//  PURPOSE: 
//
//  COMMENTS: main entry point for BNCS Driver "Router_Umd_Tally"
//
//
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;
	char acTemp[256] = "", szLeft[256] = "", szRight[256] = "";
	char szDebug[256] = "";
	ssAutoInstance = "";
	iDevice = 0;

	lstrcpyn(acTemp, lpCmdLine, 255);

	// deterime system type
	getBNCS_File_Path();
	// passed in parameter an instance name id- 
	// this check is done as windows shortcuts sometimes pass in whole target path and parameters sometimes

	// was iDevice = atoi(acTemp);
	iDevice = atoi(acTemp);
	if (iDevice>0) {
		wsprintf(szDebug, "RTR UMDT -winmain- integer device %d \n", iDevice);
		OutputDebugString(szDebug);
	}
	else {
		char *pdest = strstr(acTemp, " ");
		if (pdest != NULL) {
			// space present - as string may include full path to driver
			if (SplitAString(acTemp, ' ', szLeft, szRight)) ssAutoInstance = bncs_string(szRight);
		}
		else
			ssAutoInstance = bncs_string(acTemp);
		// now call xml parser to extract device for instance name
		iDevice = getInstanceDevice(ssAutoInstance);
		wsprintf(szDebug, "RTR UMDT -winmain- instance %s yielded device index %d \n", LPCSTR(ssAutoInstance), iDevice);
		OutputDebugString(szDebug);
	}

	if (iDevice<1) {
		// if still 0 or lower then get hardcoded instance name
		wsprintf(szDebug, "RTR UMDT -winmain- NULL instance device - so using instance UMD_Tally_Auto \n");
		OutputDebugString(szDebug);
		ssAutoInstance = "router_umd_auto";
		iDevice = getInstanceDevice(ssAutoInstance);
		wsprintf(szDebug, "RTR UMDT -winmain- 2nd test - instance umd t auto device index %d \n", iDevice);
		OutputDebugString(szDebug);
	}
	
	// Initialize global strings
	LoadString(hInstance, IDS_SECTION, szAppName, MAX_LOADSTRING);
	LoadString(hInstance, IDS_APP_TITLE, acTemp, MAX_LOADSTRING);
	wsprintf(szTitle, acTemp, iDevice);
	LoadString(hInstance, IDC_APPCLASS, szWindowClass, MAX_LOADSTRING);

	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow)) return FALSE;

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_Router_Umd_Tally);
	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;

}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_REG);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCSTR)IDC_Router_Umd_Tally;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hWndMain = CreateWindow(szWindowClass, szTitle, WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|CW_USEDEFAULT,
		10,10, APP_WIDTH, APP_HEIGHT, NULL, NULL, hInstance, NULL);

	if (!hWndMain)	{
		return FALSE;
	}

//#ifdef _DEBUG
	ShowWindow(hWndMain,nCmdShow);
//#else
//	ShowWindow(hWndMain, SW_MINIMIZE);
//#endif

	UpdateWindow(hWndMain);

	return TRUE;
}



///////////////////////////////////////////////////////////////////////////////
// generic method to split string into two halves around a specified character
//
BOOL SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight)
{
	int result = 0;
	char szInStr[MAX_BUFFER_STRING] = "";
	strcpy(szInStr, szStr);
	strcpy(szLeft, "");
	strcpy(szRight, "");

	char *pdest = strchr(szInStr, cSplit);
	if (pdest != NULL) {
		result = pdest - szInStr + 1;
		if (result>0) {
			if (result == strlen(szInStr)) {
				strcpy(szLeft, szInStr);
			}
			else {
				strncpy(szLeft, szInStr, result - 1);
				szLeft[result - 1] = 0; // terminate
				strncpy(szRight, szInStr + result, strlen(szInStr) - result + 1);
			}
			return TRUE;
		}
		else {
			Debug("SplitAString pdest not null, BUT result 0 or less : %d", result);
		}
	}
	// error condition
	strcpy(szLeft, szInStr);
	return FALSE;
}




//
//  FUNCTION: AssertDebugSettings(HWND)
//
//  PURPOSE:  Sorts out window size and ini file settings for the two debug
//            controls - debug and hide.
//  
//	COMMENTS:
//	
void AssertDebugSettings(HWND hWnd)
{
	char szFileName[256];
	char szSection[MAX_LOADSTRING];
	LoadString(hInst, IDS_SECTION, szSection, MAX_LOADSTRING);
	sprintf(szFileName, "dev_%03d.ini", iDevice);

	if (iDebugFlag>0)
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_ON, MF_CHECKED);
		//w_p(szFileName, szSection, "DebugMode", "1");
	}
	else
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_ON, MF_UNCHECKED);
		//w_p(szFileName, szSection, "DebugMode", "0");
	}

	RECT rctWindow;
	GetWindowRect(hWnd, &rctWindow);
	if (fDebugHide)
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, iChildHeight + 42, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_HIDE, MF_CHECKED);
	}
	else
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, APP_HEIGHT, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_HIDE, MF_UNCHECKED);
	}
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  
//	COMMENTS:
//	
//	WM_COMMAND	- process the application menu
//  WM_DESTROY	- post a quit message and return
//	
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	
	switch (message) 
	{
		case WM_CREATE:
			return InitApp(hWnd);
		break;
		
		case WM_TIMER:

			switch ( wParam ) {
				case STARTUP_TIMER_ID : // go thru start up now

					if (iStartupCounter==0) {
						// get required fields from instances and object settings
						bAutomaticStarting = TRUE;
						SetDlgItemText(hWndDlg, IDC_AUTO_ID, szAutoId );
						AssertDebugSettings(hWnd);

						// load all required data for automatic
						if ( LoadAllInitialisationData()==TRUE ) {					
							//bShowAllDebugMessages = TRUE;
							// register with router, panel infodrivers etc
							AutomaticRegistrations();
							PollPhysicalRouters(iStartupCounter);
							iStartupCounter++;
						}
						else {
							// set Auto in NONRUNNING STATE
							KillTimer( hWnd , STARTUP_TIMER_ID );
							Debug("**** UMD AUTO -- NOT RUNNING - LOAD INIT ERRORS ****");
							Debug("**** UMD AUTO -- NOT RUNNING - LOAD INIT ERRORS ****");
							Debug("**** UMD AUTO -- NOT RUNNING - LOAD INIT ERRORS ****");
							SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "INIT ERRORS" );
							ForceAutoIntoRXOnlyMode();
							KillTimer(hWnd, STARTUP_TIMER_ID);
						}
					}
					else {
						// poll routers etc and get revertives - 
						PollPhysicalRouters(iStartupCounter);
						iStartupCounter++;

						if (iStartupCounter>16) {
							// all revs should be in - so first pass processing
							// startupcounter >0
							KillTimer( hWnd , STARTUP_TIMER_ID );

							CheckAndSetInfoResilience();
							iNextResilienceProcessing = 1; 
							if (bShowAllDebugMessages) CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_CHECKED);
							
							// check all rtr revs in from routers -- if ok do a first pass calc  -- one time only
							bAutomaticStarting = FALSE;
							KillTimer( hWnd , ONE_SECOND_TIMER_ID );
							Debug( "1st Processing pass for startup");
							CheckAndSetInfoResilience();
								
							// FIRST PASS AT PROCESSING 
							bForceUMDUpdate = TRUE;
							ProcessLabelsForAllUMDS();    // calc and output all umds 
							bForceUMDUpdate = FALSE;
								
							// now deal with command que
							ProcessNextCommand(BNCS_COMMAND_RATE);
								
							// set initial value for umd recalc
							bShowAllDebugMessages = FALSE;   // turn off all messages now
							CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_UNCHECKED);
								
							// finished first pass etc - now fully running
							
							// set timer for device polls - to check all revertives returned
							iOneSecondPollTimerId = SetTimer( hWnd, ONE_SECOND_TIMER_ID, ONE_SECOND_TIMER_INTERVAL, NULL );
							Debug( "One Second  timer start returned %d ", iOneSecondPollTimerId );
							bOneSecondPollTimerRunning = TRUE;
						}

					}

				break;


				case ONE_SECOND_TIMER_ID : // used for housekeeping and cyclic polling of hardware 

					// check on infodriver status
					iNextResilienceProcessing--;
					if (iNextResilienceProcessing<=0) {
						CheckAndSetInfoResilience();
						iNextResilienceProcessing = SIXTYSECONDS; // reset counter 
					}

					// 2. check if recently failed over
					if (iOverallModeChangedOver>0) {
						iOverallModeChangedOver--;
						if (iOverallModeChangedOver <= 0) {
							if (bShowAllDebugMessages) Debug("RestartCSI_NI-NO messages called");
							Restart_CSI_NI_NO_Messages();
						}
					}

					// any other actions required during housekeeping timer -- process any pending commands if command timer not already running
					if (bNextCommandTimerRunning==FALSE) {
						ProcessNextCommand(BNCS_COMMAND_RATE);
					}

				break;

				case COMMAND_TIMER_ID:
					// process next command in command queue if any 
					ProcessNextCommand(BNCS_COMMAND_RATE);
					if (qCommands.size()==0) { // queue empty so stop timer
						KillTimer(hWndMain, COMMAND_TIMER_ID);
						bNextCommandTimerRunning = FALSE;
					}
				break;	
				
				case DATABASE_TIMER_ID:
					// reload all panelsets and reassert panel devices
					KillTimer( hWndMain, DATABASE_TIMER_ID );
					bRMDatabaseTimerRunning = FALSE;
					ssl_RMChanges.clear();
					if (bShowAllDebugMessages) Debug( " RMtimer - clearing list ");
					// xxx redo umds ???
				break;

				case FORCETXRX_TIMER_ID:
					Debug("ForceTxrx timer");
					KillTimer(hWndMain, FORCETXRX_TIMER_ID);
					ForceAutoIntoTXRXMode();
				break;

			}  // switch(wparam) on timer types

		break; // wmtimer

		case WM_SIZE: // if the main window is resized, resize the listbox too
			//Don't resize window with child dialog
		break;
		
		case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
			
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
			
		case ID_DEBUG_CLEAR:
			SendMessage(hWndList,LB_RESETCONTENT,0,0L);
			break;
			
		case ID_DEBUG_ON:
			if (iDebugFlag)
				iDebugFlag = 0;
			else	{
				iDebugFlag = 1;
				fDebugHide = FALSE;
			}
			AssertDebugSettings(hWnd);
			break;

		case ID_DEBUG_HIDE:
			if (fDebugHide)	{
				fDebugHide = FALSE;
				iDebugFlag = 1;
			}
			else	{
				fDebugHide = TRUE;
				iDebugFlag = 0;
			}
			AssertDebugSettings(hWnd);
			break;


		case  ID_DEBUG_SHOWALLMESSAGES : 
			if (bShowAllDebugMessages==TRUE) {
				Debug( "-- turning off most debug messages");
				bShowAllDebugMessages = FALSE;
				CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_UNCHECKED);
			}
			else {
				Debug( "++ Turning on all debug messages");
				bShowAllDebugMessages = TRUE;
				CheckMenuItem(GetSubMenu(GetMenu(hWnd),1),ID_DEBUG_SHOWALLMESSAGES,MF_CHECKED);
			}
		break;

		case ID_OPTIONS_FORCETXRX:
			Debug("Forcing Automatic into TXRX Mode");
			ForceAutoIntoTXRXMode();
		break;

		case ID_OPTIONS_FORCERXONLY:
			Debug(" Trying to force driver into RXONLY mode");
			Debug(" *** Now force the other Automatic into TXRX mode to complete operation ***");
			ForceAutoIntoRXOnlyMode();
		break;

		case ID_OPTIONS_POLLROUTER:
		{
			CRevsRouterData* pSDI = GetMainSDIRouter();
			if (pSDI) {
				char szMsg[256];
				Debug( " *** Polling SDI router ***");
				wsprintf( szMsg, "RP %d %d %d", i_rtr_TielineRtr_Index, 1, pSDI->getMaximumDestinations() );
				ecCSIClient->txrtrcmd( szMsg );
			}
		}
		break;

		case ID_OPTIONS_RECALCULATETALLIES:
		{
			Debug(" Recalculating all labels");
			bForceUMDUpdate = TRUE;
			ProcessLabelsForAllUMDS();    // calc and output all umds 
			bForceUMDUpdate = FALSE;
		}
		break;

		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		} // switch (wmId)
		break;

	case WM_DESTROY:
		CloseApp(hWnd);
		PostQuitMessage(0);
		break;
		
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}


void ClearDeviceData() 
{
	SetDlgItemText(hWndDlg, IDC_DEV_NAME, ""); // name
	SetDlgItemText(hWndDlg, IDC_DEV_LOC, ""); // location
	SetDlgItemText(hWndDlg, IDC_DEV_ALTID, ""); // altid
	SetDlgItemText( hWndDlg, IDC_DEV_TYPE, "" ); // type
	SetDlgItemText( hWndDlg, IDC_DEV_INFO, "" ); // infodriver dev
	SetDlgItemText( hWndDlg, IDC_DEV_SLOTS, "" ); // preset
	SendDlgItemMessage(hWndDlg, IDC_LIST2, LB_RESETCONTENT, 0, 0);
}


void DisplayUmdDeviceData()
{
	// display list of umds for chosen dev
	char szData[256];
	ClearDeviceData();
	int iMVDevice = (SendDlgItemMessage(hWndDlg, IDC_LIST1,  LB_GETCURSEL , 0,0))+1; // LB_GETTEXT ??
	if ((iMVDevice>0) && (iMVDevice <= iNumberActiveUmdDevs)) {
		CUmdDevice* pDev = GetValidUmdDevice(iMVDevice);
		if (pDev) {
			int iUMDS, iType, iSrcDestType, iSrcDest;
			bncs_string ssName, ssType, ssLoc, ssAlt;
			iCurrentChosenDevice = iMVDevice;
			pDev->getUMDDevice(&ssName, &ssType, &ssLoc, &ssAlt, &iType, &iUMDS);
			ssCurrentChosenDevice = ssName;
			SetDlgItemText(hWndDlg, IDC_DEV_LOC, LPCSTR(ssLoc)); // loc
			SetDlgItemText(hWndDlg, IDC_DEV_ALTID, LPCSTR(ssAlt)); // alt id
			SetDlgItemInt(hWndDlg, IDC_DEV_INFO, pDev->getBNCSDriver(), FALSE); // drv
			SetDlgItemInt( hWndDlg, IDC_DEV_SLOTS, pDev->getDriverOffset(), FALSE ); // drv
			SetDlgItemText( hWndDlg, IDC_DEV_NAME, LPCSTR(ssName) ); // name
			SetDlgItemText( hWndDlg, IDC_DEV_TYPE, LPCSTR(ssType) ); // type
			SetDlgItemInt( hWndDlg, IDC_DEV_NUMUMDS, iUMDS, FALSE );

			for (int iU=1;iU<=iUMDS;iU++) {
				// get and display umd details in list2
				bncs_string ssAuxLabel="";
				bncs_string ssLabel = pDev->getAllUmdDetails (iU, &iSrcDestType, &iSrcDest,&ssAuxLabel );
				int iWhichRtr = pDev->getUmdSrcDestRouter(iU);
				CRevsRouterData* pSDI = GetValidRouter(iWhichRtr);
				int iRoutedSrc = iSrcDest;
				int iTracedRtr=0, iTracedSrc = 0;
				BOOL bVirtual = FALSE;
				if (pSDI) {
					if (iSrcDestType == DEST_UMD_TYPE) {
						iRoutedSrc = pSDI->getRoutedSourceForDestination(iSrcDest);
					}
				}
				// get traced if virtual
				bVirtual = TracePrimaryRouterSource(iWhichRtr, iRoutedSrc, &iTracedRtr, &iTracedSrc);

				char szTmp1[64] = "", szTmp2[32] = "", szBit[4] = " ";
				if (iWhichRtr != i_rtr_TielineRtr_Index) strcpy(szBit, "*");
				if (iSrcDestType == DEST_UMD_TYPE) {
					if ((bVirtual) && (iTracedSrc>0))
						wsprintf(szTmp1, "%s DEST   %03d   %03d   %03d %03d", szBit, iWhichRtr, iSrcDest, iRoutedSrc, iTracedSrc);
					else
						wsprintf(szTmp1, "%s DEST   %03d   %03d   %03d\t", szBit, iWhichRtr, iSrcDest, iRoutedSrc);
				}
				else if (iSrcDestType == SOURCE_UMD_TYPE) {
					if ((bVirtual) && (iTracedSrc>0))
						wsprintf(szTmp1, "%s SRCE   %03d           %03d %03d", szBit, iWhichRtr, iSrcDest, iTracedSrc);
					else
						wsprintf(szTmp1, "%s SRCE   %03d           %03d\t", szBit, iWhichRtr, iSrcDest);
				}
				else if (iSrcDestType == FIXED_UMD_TYPE) {
					wsprintf(szTmp1, "%s FIXED            \t       ", szBit);
				}
				wsprintf(szData, "%02d    %s       \t%s\t%s", iU, szTmp1, LPCSTR(ssLabel), LPCSTR(ssAuxLabel));
				SendDlgItemMessage(hWndDlg, IDC_LIST2, LB_INSERTSTRING, -1, (LPARAM)szData);
			}
		}
	}

}


//
//  FUNCTION: DlgProcChild()
//
//  PURPOSE: Message handler for child dialog.
//
//  COMMENTS: Displays the interface status
//
// 
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	int wmId    = LOWORD(wParam); 
	int wmEvent = HIWORD(wParam); 
	char szNum[16]="0", szAddr[MAX_BUFFER_STRING]= "";

	HWND hButton = (HWND)lParam;
	LONG idButton=GetDlgCtrlID(hButton);

	switch (message)	{

		case WM_COMMAND :
			switch (wmId)	{
				case IDC_LIST1:		// list of devices - display details
					DisplayUmdDeviceData();
				break;
				case IDC_BUTTON1:
					if ((iCurrentChosenDevice>0)&&(iCurrentChosenDevice<=iNumberActiveUmdDevs)) {
						CUmdDevice* pDev = GetValidUmdDevice(iCurrentChosenDevice);
						if (pDev) { 
							bForceUMDUpdate = TRUE;
							ProcLabelsForGivenDevice( pDev, ssCurrentChosenDevice );
							bForceUMDUpdate = FALSE;
						}
					}
				break;
			}
		break;
	
		 case WM_INITDIALOG: 
			//This will colour the background of the child dialog on startup to defined colour
			// brushes are defined in INIT_APP function below
			SetTextColor((HDC) wParam,COL_DK_BLUE);
			SetBkColor((HDC) wParam, COL_TURQ);
			HBr = hbrTurq;
			return (LRESULT)HBr;
			//HBr = CreateSolidBrush( COL_TURQ );
			//return (LRESULT)HBr;
		break;

		case WM_CTLCOLORDLG:
			// colour all diag at startup
			SetTextColor((HDC) wParam,COL_DK_BLUE);
			SetBkColor((HDC) wParam, COL_TURQ);
			HBr = hbrTurq;
			return (LRESULT)HBr;
		break;

		case WM_CTLCOLORLISTBOX:
			SetTextColor((HDC) wParam,COL_DK_BLUE);
			SetBkColor((HDC) wParam, COL_WHITE);
			HBr = hbrWhite;
			return (LRESULT)HBr;
		break;

		case WM_CTLCOLORSTATIC:
			 // to colour labels --  this will be called on startup of the dialog by windows
			 // this section will be called every time you do a SetDlgItemText() command for a given label
			 // by testing the value of global vars this can be used to colour labels for Comms OK/Fail, TXRX/Rxonly etc
			switch (idButton)	{	
				case IDC_INFO_STATUS : 
					if (iOverallTXRXModeStatus==INFO_TXRXMODE) {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_DK_GRN);
						HBr = hbrDarkGreen;
					}
					else if (iOverallTXRXModeStatus==INFO_RXMODE) {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_LT_RED);
						HBr = hbrLightRed;
					}
					else if (iOverallTXRXModeStatus==-1) {
						SetTextColor((HDC) wParam,COL_DK_BLUE);
						SetBkColor((HDC) wParam, COL_LT_ORANGE);
						HBr = hbrLightOrange;
					}
					else {
						SetTextColor((HDC) wParam,COL_DK_BLUE);
						SetBkColor((HDC) wParam, COL_WHITE);
						HBr = hbrWhite;
					}
				break;
					
				case IDC_VERSION: case IDC_IDRX: case IDC_IDTX: case IDC_QUEUE: case IDC_STUDIOLSHAPE: case IDC_STUDIO_MXRMAIN2:
				case IDC_AUTO_ID: case IDC_DEV_TYPE: case IDC_DEV_NAME: case IDC_DEV_INFO: case IDC_DEV_SLOTS: case IDC_STUDIO_MXRMAIN3:
				case IDC_DEV_LOC: case IDC_DEV_NUMUMDS: case IDC_AUTO_SDIRTR: case IDC_AUTO_MIXER: case IDC_STUDIO_MXRMAIN5:
				case IDC_DEV_ALTID: case IDC_STUDIOLOGIC: case IDC_STUDIO_MXRMAIN: case IDC_DEST_STATE3: case IDC_STUDIO_MXRMAIN6:
				case IDC_STUDIO_MXRRESV: case IDC_SRC_DB: case IDC_SOFT_DB: case IDC_DEST_STATE: case IDC_DEST_STATE2: 
				case IDC_EMERG_STATE: case IDC_EMERG_STATE2: case IDC_EMERG_STATE3: case IDC_STUDIO_MODE: case IDC_LSHAPEREV3:
				case IDC_BYPASS_STATE: case IDC_BYPASS_STATE2: case IDC_BYPASS_STATE3: case IDC_EGGBOX_INDEX:
				case IDC_LSHAPEREV1: case IDC_LSHAPEREV2: case IDC_LSHAPE_ONAIR: case IDC_STUDIO_MXRMAIN4: case IDC_STUDIO_MXRMAIN7:
				case IDC_STUDIO_MXRMAIN8: case IDC_STUDIO_MXRMAIN9: case IDC_STUDIO_MXRMAIN10:
					SetTextColor((HDC) wParam,COL_DK_BLUE);	 
					SetBkColor((HDC) wParam, COL_WHITE);	
					HBr = hbrWhite;
				break;

				case IDC_EMERG_STATE4: case IDC_EMERG_STATE5: case IDC_EMERG_STATE6: 
				case IDC_DEST_STATE4: case IDC_DEST_STATE5: case IDC_DEST_STATE6: case IDC_STUDIO_MODE2:
						SetTextColor((HDC) wParam,COL_DK_BLUE);
						SetBkColor((HDC) wParam, COL_TURQ);
						HBr = hbrTurq;
					break;

			default:
				SetTextColor((HDC) wParam,COL_DK_BLUE);
				SetBkColor((HDC) wParam, COL_TURQ);
				HBr = hbrTurq;
			}
			return (LRESULT)HBr;
		break;
	
	default:
		break;
	}
	
    return FALSE;

}


//
//  FUNCTION: Version Info()
//
void VersionInfo() {
	LPBYTE   abData;
	DWORD  handle;
	DWORD  dwSize;
	LPBYTE lpBuffer;
	LPSTR szName, szModFileName, szBuf;
	char acBuf[16];
	#define	PATH_LENGTH	256
			
	szName=(LPSTR)malloc(PATH_LENGTH);
	szModFileName=(LPSTR)malloc(PATH_LENGTH);
	szBuf=(LPSTR)malloc(PATH_LENGTH);

	/*get version info*/

	GetModuleFileName(hInst,szModFileName,PATH_LENGTH);

	dwSize = GetFileVersionInfoSize(szModFileName, &handle);
	abData=(LPBYTE)malloc(dwSize);
	GetFileVersionInfo(szModFileName, handle, dwSize, abData);
	if (dwSize) {
		/* get country translation */
		VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
		/* make country code */
		wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
		if (dwSize!=0) {
			/* get a versioninfo file version number */
			wsprintf(szName,"\\StringFileInfo\\%s\\PRODUCTVERSION",(LPSTR)acBuf);
			VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
		}
		/* copy version number from byte buffer to a string buffer */
		lstrcpyn(szModFileName,(LPSTR)lpBuffer,(int)dwSize);
		/* copy to the dialog static text box */
		wsprintf(szBuf,"%s",szModFileName);
		// replace any commas with full stops
		for (int c=0; c<int(strlen(szBuf)); c++)
			if (szBuf[c]==44 ) szBuf[c]=46; 
		SetDlgItemText(hWndDlg, IDC_VERSION, szBuf);
	}			
	free((PVOID)abData);
	free((PVOID)szName);
	free((PVOID)szModFileName);
	free((PVOID)szBuf);
}

//
//  FUNCTION: About()
//
//  PURPOSE: Message handler for about box.
//
//  COMMENTS: Displays the compile date info
//				and used to display version info from
//				a version resource
//
// 
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
	case WM_INITDIALOG:
		{
			LPBYTE   abData;
			DWORD  handle;
			DWORD  dwSize;
			LPBYTE lpBuffer;
			LPSTR szName, szModFileName, szBuf;
			char acBuf[16];
#define	PATH_LENGTH	256
			
			szName=(LPSTR)malloc(PATH_LENGTH);
			szModFileName=(LPSTR)malloc(PATH_LENGTH);
			szBuf=(LPSTR)malloc(PATH_LENGTH);
			
			/*get version info*/
			
			GetModuleFileName(hInst,szModFileName,PATH_LENGTH);
			
			dwSize = GetFileVersionInfoSize(szModFileName, &handle);
			
			abData=(LPBYTE)malloc(dwSize);
			
			GetFileVersionInfo(szModFileName, handle, dwSize, abData);
			
			if (dwSize)
			{
				LPSTR szFileVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szProductVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szComments=(LPSTR)alloca(PATH_LENGTH);
				
				/* get country translation */
				VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
				/* make country code */
				wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
				
				if (dwSize!=0)
				{
					/* get a versioninfo file version number */
					wsprintf(szName,"\\StringFileInfo\\%s\\FileVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szFileVersion,(LPSTR)lpBuffer,(int)dwSize);
				
				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\ProductVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szProductVersion,(LPSTR)lpBuffer,(int)dwSize);
				
				/* copy to the dialog static text box */
				wsprintf(szBuf,"Version %s  (%s)",szFileVersion, szProductVersion);
				SetDlgItemText (hDlg, IDC_VERSION, szBuf);
				
				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\Comments",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				/* copy to the dialog static text box */
				lstrcpyn(szComments,(LPSTR)lpBuffer,(int)dwSize);
				SetDlgItemText (hDlg, IDC_COMMENT, szComments);			
			}
			
			wsprintf(szName,"%s - %s",(LPSTR)__DATE__,(LPSTR)__TIME__);
			SetDlgItemText(hDlg,IDC_COMPID,szName);
			
			free((PVOID)abData);
			free((PVOID)szName);
			free((PVOID)szModFileName);
			free((PVOID)szBuf);
		}
		return TRUE;
		
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
    return FALSE;

}

//
//  FUNCTION: InitApp()
//
//  PURPOSE: Things to do when main window is created
//
//  COMMENTS: Called from WM_CREATE message
//			 We create a list box to fill the main window to use as a
//			debugging tool, and configure the serial port and infodriver 
//			connections, as well as a circular fifo buffer
//
//
LRESULT InitApp(HWND hWnd)
{

	/* return 0 if OK, or -1 if an error occurs*/
		RECT rctWindow;
	
	OutputDebugString( " \n ROUTER UMD TALLY starting up \n " );

	// define brushes and colours
	CreateBrushes();	
	iOverallTXRXModeStatus=0;
	bValidCollediaStatetoContinue = TRUE;
	bAutomaticStarting = TRUE;
	bForceUMDUpdate = FALSE;
	bRMDatabaseTimerRunning = FALSE;
	bNextCommandTimerRunning = FALSE;
	bShowAllDebugMessages = FALSE;
	bRecalcAllTalliesFromRM = FALSE;

	iOverallModeChangedOver = 0;
	iOverallStateJustChanged = 0;
	iNextResilienceProcessing = 27;

	// Init Status Dialogs
	hWndDlg = CreateDialog(hInst, (LPCTSTR)IDD_STATUS, hWnd, (DLGPROC)DlgProcChild);
	GetClientRect(hWndDlg, &rctWindow);
	iChildHeight = rctWindow.bottom - rctWindow.top + 2;
		
	GetClientRect(hWnd,&rc);
	hWndList=CreateWindow("LISTBOX","",WS_CHILD | WS_VSCROLL | LBS_NOINTEGRALHEIGHT | LBS_USETABSTOPS,
						  0,iChildHeight,rc.right-rc.left,rc.bottom-rc.top-iChildHeight,
						  hWnd,(HMENU)IDW_LIST,hInst,0);
	ShowWindow(hWndList,SW_SHOW);

	LoadIni();
	VersionInfo();
	//fDebug = 0; // off as default;
	//fDebugHide = 1; // hide as defualt;
	SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Initialising");

		// connect to all the infodrivers
		ecCSIClient = new extclient;
		// connect to all the infodrivers
		eiExtInfoId = new extinfo;

		if ((ecCSIClient==NULL)||(eiExtInfoId==NULL)) {
			OutputDebugString( " \n ROUTER UMD TALLY FAILED TO CREATE CSICLIENT OR EXTINFO CLASS \n " );
			Debug( " ROUTER UMD TALLY FAILED TO CREATE CSICLIENT OR EXTINFO CLASS" );
			bValidCollediaStatetoContinue = FALSE;
		}

		if (bValidCollediaStatetoContinue==TRUE ) {

			// declare instances of client class and ext inf
			// instantiate an instance of fifi buffer  -- yeah very fluffy 
			//fifoBuffer = new CFiFoBuffer();

			// connect to CSI first
			ecCSIClient->notify(CSIClientNotify);
			switch (ecCSIClient->connect()){
			case CONNECTED: 
				Debug("Connected OK to CSI");
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "CSI Connected ok");
				break;

			case CANT_FIND_CSI:
				Debug("ERROR ERROR - Connect failed to CSI ");
				Debug("ERROR ERROR -  Automatic not functioning");
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "ERROR-in FAIL state");
				bValidCollediaStatetoContinue	= FALSE;
				break;

			case BAD_WS:
				Debug("ERROR ERROR - Connect failed to CSI - Bas WS ?? WTF ");
				Debug("ERROR ERROR -  Automatic not functioning");
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "BAD WS-in FAIL state");
				bValidCollediaStatetoContinue	= FALSE;
				break;

			default:
				Debug("Other Error - code %d",ecCSIClient->getstate());
				Debug("ERROR ERROR - Connect failed to CSI,  code :%d",ecCSIClient->getstate() );
				Debug("ERROR ERROR -  Automatic not functioning");
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "ERROR-in FAIL state");
				bValidCollediaStatetoContinue	= FALSE;
			} // switch ec
			ecCSIClient->setcounters(&lTXID, &lRXID);
		}

		if (bValidCollediaStatetoContinue==TRUE ) {
			iOverallTXRXModeStatus = UNKNOWNVAL; // unknown first time
			// connect to External Infodriver
			if (eiExtInfoId) {
				eiExtInfoId->notify(InfoNotify);
				switch (eiExtInfoId->connect(iDevice)) { // connect to infodriver
				case CONNECTED: 
					Debug("Connected OK to infodriver %d",eiExtInfoId->iDevice);
					SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Connected OK");
					break;	
				case CANT_FIND_INFODRIVER:
					Debug("ERROR ERROR - Connect failed to infodriver %d",eiExtInfoId->iDevice);
					Debug("ERROR ERROR -  Automatic not functioning");
					SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Info connect FAILED");
					bValidCollediaStatetoContinue	= FALSE;
					break;
				default:
					Debug("ERROR ERROR - Connect failed to infodriver, code %d",eiExtInfoId->connect(iDevice));
					Debug("ERROR ERROR -  Automatic not functioning");
					SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Info connect FAILED");
					bValidCollediaStatetoContinue	= FALSE;
				}
				eiExtInfoId->setcounters(&lTXID, &lRXID);
			}
			else {
				OutputDebugString( " \n SKY UMD TALLY AUTO NO EXTINFO CLASS \n " );
				Debug( " SKY UMD TALLY AUTO NO EXTINFO CLASS" );
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "ERROR-in FAIL state");
				bValidCollediaStatetoContinue	= FALSE;
			}
		}
		else 
			return 0;

		if (bValidCollediaStatetoContinue==TRUE ) {
			// check on infodrivers status -- really ought to be either all TX/RX or all RXOnly -- a mix is not good !!
			// when determining overall status -- main infodriver ( iDevice ) is very important
			//Debug( "Trying to set driver into TXRX mode");
			//eiExtInfoId->setmode(INFO_TXRXMODE);
			Debug( "Determining Automatic mode for first time");
			CheckAndSetInfoResilience();
			iNextResilienceProcessing = 2;
			//if (iOverallTXRXModeStatus==INFO_RXMODE) {
			//	// try and force here once 
			//	eiExtInfoId->setmode(INFO_TXRXMODE);
			//	eiExtInfoId->requestmode = TO_TXRX;
			//	CheckAndSetInfoResilience();  // check again
			//}
			// continue on to initialise vars, get object settings, dev registrations and poll for revs 

			iOneSecondPollTimerId = SetTimer( hWnd, STARTUP_TIMER_ID, STARTUP_TIMER_INTERVAL , NULL );
			iStartupCounter = 0;
			eiExtInfoId->updateslot( COMM_STATUS_SLOT, "1" );	 // ok	

			// end of initialisation - driver running normally now on
		}
		
		return 0;
}

//
//  FUNCTION: CloseApp()
//
//  PURPOSE: Things to do when main window is destroyed
//
//  COMMENTS: Called from WM_DESTROY message
//			The listbox debug window is a child of the main, so it will be 
//			destroyed automatically. 
//
void CloseApp(HWND hWnd)
{
	// kill any timers
	KillTimer( hWnd , COMMAND_TIMER_ID );
	KillTimer( hWnd , ONE_SECOND_TIMER_ID );
	ecCSIClient->unregtallyrange();
	
	if (bValidCollediaStatetoContinue==TRUE ) {
		eiExtInfoId->updateslot( COMM_STATUS_SLOT, "0" ); // currently automatic in lost fail mode
		eiExtInfoId->setmode(INFO_RXMODE);					// force to rxonly -- so that resilient pair can take over
		eiExtInfoId->requestmode = TO_RXONLY; // force other driver into tx immeadiately if running ??

		if (cl_AllRouters) {
			while (cl_AllRouters->begin() != cl_AllRouters->end()) {
				map<int,CRevsRouterData*>::iterator it = cl_AllRouters->begin();
				if (it->second) delete it->second;
				cl_AllRouters->erase(it);
			}
			delete cl_AllRouters;
		}
		
		if (cl_AllUmdDevices) {
			while (cl_AllUmdDevices->begin() != cl_AllUmdDevices->end()) {
				map<bncs_string,CUmdDevice*>::iterator it = cl_AllUmdDevices->begin();
				if (it->second) delete it->second;
				cl_AllUmdDevices->erase(it);
			}
			delete cl_AllUmdDevices;
		}

		ClearCommandQueue();

	}

	// shutdown the infodriver
	if (eiExtInfoId) PostMessage( eiExtInfoId->hWndInfo, WM_CLOSE, 0, 0 );

	//delete fifoBuffer;
	if (eiExtInfoId)	delete eiExtInfoId;
	if (ecCSIClient)    delete ecCSIClient;

	// tidy up colours
	ReleaseBrushes();
	DeleteObject(HBr);

}

//
//  FUNCTION: LoadIni()
//
//  PURPOSE: Assign (global) variables with values from .ini file
//
//  COMMENTS: The [section] name is gotten from a stringtable resource
//			 which defines it. It is the same name as the application, defined by the wizard
//			
//			
void LoadIni(void)
{

	iDebugFlag = 0;
	fDebugHide = TRUE;
	bncs_string ssXmlConfigFile = "router_umd_auto";
	bncs_string ssXmlSectionId = "Router_Umd_Auto";
	int iVal = getXMLItemValue(ssXmlConfigFile, ssXmlSectionId, "DebugMode").toInt();
	if (iVal > 0) {
		iDebugFlag = 1;
		fDebugHide = FALSE;
		if (iVal > 1) bShowAllDebugMessages = TRUE;
	}
	// force on for now
	iDebugFlag = 1;
	fDebugHide = FALSE;
	bShowAllDebugMessages = TRUE;

}

/*!
\returns int The workstation number

\brief Function to get the workstation number

First checks to see if %CC_WORKSTATION% is defined
otherwise returns the WS stored in %WINDIR%\csi.ini

NOTE: 
In BNCSWizard apps the following change should be made to LoadINI()
- Replace:
	iWorkstation=atoi(r_p("CSI.INI","Network","Workstation","0",FALSE));
- With:
	iWorkstation=getWS();
- Also add the following to "app_name.h":
int		getWS(void);
*/
int getWS(void)
{
	char* szWS = getenv( "CC_WORKSTATION" );
	
	if(szWS)
	{
		return atoi(szWS);
	}
	else
	{
		return atoi(r_p("CSI.INI","Network","Workstation","0",FALSE));
	}

}

// new function added to version 4.2 assigns the globals    szBNCS_File_Path and szBNCS_Log_File_Path
// called from r_p and w_p functions immediately below
// could easily be called once on first start up of app

char *	getBNCS_File_Path( void  )  
{
	char* szCCRoot = getenv( "CC_ROOT" );
	char* szCCSystem = getenv( "CC_SYSTEM" );
	char szV3Path[MAX_BUFFER_STRING] = "";
	GetPrivateProfileString("Config", "ConfigPath", "", szV3Path, sizeof(szV3Path),"C:\\bncs_config.ini" );
	char szWinDir[MAX_BUFFER_STRING] = "";
	GetWindowsDirectory(szWinDir, MAX_BUFFER_STRING);

	// first check for 4.5 system settings
	if(szCCRoot && szCCSystem)	{
		sprintf(szBNCS_File_Path, "%s\\%s\\config\\system", szCCRoot, szCCSystem);
		sprintf(szBNCS_Log_File_Path, "%s\\%s\\logs", szCCRoot, szCCSystem);
	}
	else if (strlen(szV3Path)>0) {
		sprintf(szBNCS_File_Path,"%s",szV3Path);
		strcpy(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	else { 
		// all other BNCS / Colledia systems - get windows or winnt directory inc v3  that are not using bncs_config.ini
		sprintf(szBNCS_File_Path,"%s",szWinDir);
		strcpy(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	return szBNCS_File_Path;

}

/*!
\returns char* Pointer to data obtained
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* defval The default value to return if not found
\param BOOL fWrite Flag - write the default value to the file

\brief Function to read one item of user data from an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
char* r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite)
{
	char szFilePath[MAX_BUFFER_STRING];	
	char szPathOnly[MAX_BUFFER_STRING];

	strcpy( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );


	GetPrivateProfileString(section, entry, defval, szResult, sizeof(szResult), szFilePath);
	if (fWrite)
		w_p(file,section,entry,szResult);
	
	return szResult;

}

/*!
\returns void 
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* setting The value to write - =value

\brief Function to write one item of user data to an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
void w_p(char* file, char* section, char* entry, char* setting)
{

	char szFilePath[MAX_BUFFER_STRING];	
	char szPathOnly[MAX_BUFFER_STRING];

	strcpy( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );

	WritePrivateProfileString(section, entry, setting, szFilePath);

}
//
//  FUNCTION: UpdateCounters()
//
//  PURPOSE: Writes current counter values to child dialog
//
//  COMMENTS: 
//				
//			
//			
void UpdateCounters(void)
{

	char szBuf[32];
	//Update Infodriver External Message Count
	wsprintf(szBuf, "%010d", lTXID);
	SetDlgItemText(hWndDlg, IDC_IDTX, szBuf);
	wsprintf(szBuf, "%010d", lRXID);
	SetDlgItemText(hWndDlg, IDC_IDRX, szBuf);
}

//
//  FUNCTION: Debug()
//
//  PURPOSE: Writes debug information to the listbox
//
//  COMMENTS: The function works the same way as printf
//				example: Debug("Number=%d",iVal);
//			
//			
void Debug(LPCSTR szFmt, ...)
{
	if (iDebugFlag)
	{
		LPSTR szDebug=(LPSTR)malloc(0x1000);
		va_list argptr;
		
		va_start(argptr,szFmt);
		vsprintf(szDebug,szFmt,argptr);
		va_end(argptr);
		
		char tBuffer[9];
		char szDate[64];
		struct tm *newtime;
		time_t long_time;
		
		time( &long_time );                // Get time as long integer. 
		newtime = localtime( &long_time ); // Convert to local time. 
		_strtime( tBuffer );
		wsprintf(szDate, "%04d-%02d-%02d-%02d:%02d:%02d", newtime->tm_year+1900, 
			newtime->tm_mon+1, newtime->tm_mday, newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
		
		// log full message to windows debugger 
		char szLogDbg[MAX_BUFFER_STRING] = "";
		wsprintf(szLogDbg, "ROUTER UMD TALLY - %s \n", szDebug);
		OutputDebugString( szLogDbg );
		
		// truncate long messages and add date and time
		if (strlen(szDebug) > 220) szDebug[220]=0; 
		char szFinal[MAX_BUFFER_STRING]="";
		wsprintf( szFinal, "%s %s", szDate, szDebug );
		//
		SendMessage(hWndList, LB_ADDSTRING,0,(LPARAM) szFinal);	//Add message
		long lCount = SendMessage(hWndList,LB_GETCOUNT,0,0);	//Get List Count
		if (lCount > MAX_LB_ITEMS)
		{
			SendMessage(hWndList, LB_DELETESTRING, 0, 0);	//Delete item
			lCount--;
		}
		SendMessage(hWndList, LB_SETTOPINDEX, (WPARAM) lCount - 1, 0);	//Set Top Index
		
		// now free memory used
		free((PVOID)szDebug);
	}
}


//
// FUNCTION CreateLogDirectory
//
// new function Version 4.2 of the Wizard - to create log file directory according to Colledia system type
//
BOOL CreateLogFileDirectory ( void )
{
	static bool bFirst = true;
	
	//create the log directory the first time
	if (bFirst) {
		_mkdir(szBNCS_Log_File_Path);	
		bFirst = false;
		return TRUE;
	}
		return FALSE;

}

//
//  FUNCTION: Log()
//
//  PURPOSE: Writes information to the log file
//
//  COMMENTS: The function works the same way as printf
//				example: Debug("Number=%d",iVal);
//			
//		rewritten in wizard version 4.2 to log crashes too - 
//
void Log(LPCSTR szFmt, ...)
{
	char szLogFile[MAX_BUFFER_STRING];
	char tBuffer[9];
	char szDate[16];
	FILE *fp;
	LPSTR szLog=(LPSTR)malloc(0x10000);
	va_list argptr;
	struct tm *newtime;
	time_t long_time;
		
	va_start(argptr,szFmt);
	vsprintf(szLog,szFmt,argptr);
	va_end(argptr);
	
	time( &long_time );                // Get time as long integer. 
	newtime = localtime( &long_time ); // Convert to local time. 
	_strtime( tBuffer );
	sprintf(szDate, "%04d%02d%02d", newtime->tm_year+1900, newtime->tm_mon+1, newtime->tm_mday);
	
	wsprintf(szLogFile, "%s\\%s_child2_%d.log", szBNCS_Log_File_Path, szDate, iDevice);
	
	// create log file directory if it doesn't exist
	CreateLogFileDirectory();
	
	if (fp = fopen(szLogFile, "a"))
	{
		fprintf(fp, tBuffer);
		fprintf(fp, " - ");
		fprintf(fp, szLog);
		fprintf(fp, "\r\n");
		fclose(fp);
	}
	
	free((PVOID)szLog);
}


//
// Function name	: SplitString
// Description	   : Split a delimited string to an array of pointers to strings
// Return type		: int number of elements returned
//
//----------------------------------------------------------------------------//
// Takes a string and splits it up creating an array of pointers to the start //
// of the sections delimted by the array of specified char delimiters         //
// the delimter characters in "string" are overwritten with NULLs             //
// Usage:                                                                     //
// char szString[] = "1,2|3'4,5,6,7,";                                        //
// char delim[] = {',', '|', '\'', ',', ',', ',', ',',};                      //
// UINT count=7;                                                              //
// char *pElements[7];                                                        //
// int iLen;                                                                  //
//	iLen = SplitString( szString, delim, 7, pElements);                       //
//                                                                            //
// NOTE: This funcion works differently to strtok in that consecutive         //
// delimiters are not overlooked and so a NULL output string is possible      //
// i.e. a string                                                              //
//    hello,,dave                                                             //
// where ,,, is the delimiter will produce 3 output strings the 2nd of which  //
// will be NULL                                                               //
//----------------------------------------------------------------------------//
int SplitString(char *string, char *delim, UINT count, char **outarray )
{

	UINT x;
	UINT y;
	UINT len;					// length of the input string
	static char *szNull = "";	// generic NULL output string
	int delimlen;
	int dp;

	len = strlen( string );
	delimlen = strlen( delim );

	if(!len)					// if the input string is a NULL then set all the output strings to NULL
	{
		for (x = 0 ; x < count; x++ )
			outarray[x] = szNull;
		return 0;
	}

	outarray[0] = string;		// set the 1st output string to the beginning of the string

	for( x = 0,y = 1 ; (x < len) && (y < count); x++ )
	{
		if( delimlen < 2 )
			dp = 1;
		else
			dp = y;
		if(string[x] == delim[dp-1])
		{
			string[ x ] = '\0';
			if((x+1) < len)		// if there is another string following this one....
			{
				outarray[ y ] = &string[x+1];
				y++;			// increment the number of assigned buffer
			}
			else
			{
				outarray[ y ] = szNull;
			}
		}
	}

	x = y;
	while( x < count )
		outarray[x++] = szNull;
	return y;					// return the number of strings allocated
	
}





/////////////////////////////////////////////////////////////////////////
// BRUSHES FOR BUTTONS & LABELS
/////////////////////////////////////////////////////////////////////////

/*********************************************************************************************************/
/*** Function to Create Colour brushes ***/
void CreateBrushes()
{ 
	hbrWindowsGrey = CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
	hbrBlack = CreateSolidBrush(COL_BLACK);
	hbrWhite = CreateSolidBrush(COL_WHITE);

	hbrDarkRed = CreateSolidBrush(COL_DK_RED);
	hbrDarkGreen = CreateSolidBrush(COL_DK_GRN);
	hbrDarkOrange = CreateSolidBrush(COL_DK_ORANGE);
	hbrDarkBlue = CreateSolidBrush(COL_DK_BLUE);

	hbrYellow = CreateSolidBrush(COL_YELLOW);
	hbrTurq = CreateSolidBrush(COL_TURQ);

	hbrLightRed = CreateSolidBrush(COL_LT_RED);
	hbrLightGreen = CreateSolidBrush(COL_LT_GRN);
	hbrLightOrange = CreateSolidBrush(COL_LT_ORANGE);
	hbrLightBlue = CreateSolidBrush(COL_LT_BLUE);

	return;
}

 /********************************************************************************************************/
void ReleaseBrushes()
{
	if (hbrBlack) DeleteObject(hbrBlack);
	if (hbrWhite) DeleteObject(hbrWhite);
	if (hbrWindowsGrey) DeleteObject(hbrWindowsGrey);

	if (hbrDarkRed) DeleteObject(hbrDarkRed);
	if (hbrDarkGreen) DeleteObject(hbrDarkGreen);
	if (hbrDarkBlue) DeleteObject(hbrDarkBlue);
	if (hbrDarkOrange) DeleteObject(hbrDarkOrange);

	if (hbrYellow)	DeleteObject(hbrYellow);
	if (hbrTurq)	DeleteObject(hbrTurq);
	if (hbrLightRed) DeleteObject(hbrLightRed);
	if (hbrLightBlue) DeleteObject(hbrLightBlue);
	if (hbrLightGreen) DeleteObject(hbrLightGreen);
	if (hbrLightOrange) DeleteObject(hbrLightOrange);

	return;
}

/*********************************************************************************************************/

