// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
#define AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

// Windows Header Files
#include <windows.h>
#include <commctrl.h> 

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdio.h>
#include <time.h>
#include <WINBASE.h>

#include <iostream>
#include <direct.h>
#include <stdlib.h>

#include <string>
	using std::string;
// maps used all over the shop
#include <map>
	using namespace std;
#include <queue>
	using namespace std;

// Colledia Control Header Files
#include <bncs32.h>
#include <bncs.h>
#include <extinfo.h>
#include <csiclient.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <bncs_config.h>

// common  classes
#include "bncs_auto_helper.h"

// auto classes
#include "Command.h"
#include "databaseMgr.h"
#include "InfodriverRevs.h"
#include "RevsRouterData.h"
#include "RouterDataDest.h"
#include "RouterDataSrce.h"
#include "UmdDevice.h"
#include "UmdDataEntry.h"



//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
