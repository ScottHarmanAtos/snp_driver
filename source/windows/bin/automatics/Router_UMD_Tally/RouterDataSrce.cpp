#include "RouterDataSrce.h"


CRouterDataSrce::CRouterDataSrce( int iIndex)
{
	m_iSrceIndex = iIndex;

	m_ssRoutedToDest = bncs_stringlist("", ',');                   
	m_ssLinkedToUmdDevice = bncs_stringlist("", ',');              

	m_ssAssoc_InfodriverRevs1 = "";              
	m_ssAssoc_InfodriverRevs2 = "";             
	m_ssAssoc_InfodriverRevs3 = "";              

	// tieline info                                          
	m_iTielineFromRtr=0;                           
	m_iTielineFromDest=0;       
	m_iAssociatedFriend = 0;

}


CRouterDataSrce::~CRouterDataSrce()
{
}
