// Command.h: interface for the CCommand class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UMDDATAENRTY_H__8F140286_08D2_461C_A9FF_9ADF33FD9514__INCLUDED_)
#define AFX_UMDDATAENRTY_H__8F140286_08D2_461C_A9FF_9ADF33FD9514__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
        #pragma warning(disable : 4786 4996)
        //
	//
	#include <windows.h>
	#include "UMD_Tally_Common.h"

class CUmdDataEntry
{
public:
	CUmdDataEntry(int iIndex);
	virtual ~CUmdDataEntry();

	int m_iUmdIndex;                 // entry in map

	int m_iUmd_Router;               // router for src dest
	int m_iUmd_Src_Dest;             // individual dest from all SDI dest list -- but can be source for fixed UMDs, or 0 for FIXED
	int m_iUmd_SrcDest_Type;         // dest by default

	bncs_string m_ssCalcUmdName;   // calculated umd text based on source name or revertive from rtr
	bncs_string m_ssAuxLabel1;     // TH Mantis 589 second string for UMD display

	bncs_string m_ssLabelCommand;  // last label command sent  are stored to help reduce unnecessary repeated commands
	bncs_string m_ssAuxCommand;    // last aux command sent  are stored to help reduce unnecessary repeated commands

};

#endif // !defined(AFX_UMDDATAENRTY_H__8F140286_08D2_461C_A9FF_9ADF33FD9514__INCLUDED_)
