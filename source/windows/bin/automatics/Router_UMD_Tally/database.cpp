/********************************************/
/* Written by David Yates                   */
/* Copyright Siemens Business Services 2006 */
/********************************************//** 
\class database
\brief BNCS "Databases" internal database object (use databaseMgr)

This class is an internal class used to represent a single BNCS database. It should not be used directly - use databaseMgr instead.
 */
#include <windows.h>
#include "database.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

/** Construct a new database object
\param path The file path to the CC Database files (set usually externally by fixed path (windows) or environment variables)
\param device The CC device number
\param level The database number

\par Example:
\code
database db( "c:\\windows", 123, 0 );
\endcode
 */
database::database( const char * path, int device, int level )
{
	m_maxSize=0;

	load( path, device, level );
}

/** Internal load function
\param path The file path to the CC Database files (set usually externally by fixed path (windows) or environment variables)
\param device The CC device number
\param level The database number
 */
void database::load( const char * path, int indevice, int inlevel )
{
	// remember what database we're looking at
	m_device = indevice;
	m_level = inlevel;

	// if there's already any entries - go off and clear them 
	if( db.size())
		db.clear();

	char buf[ MAX_PATH ];
	char fn[ MAX_PATH ];
	char sec[ 32 ];

	// work out where to get our settings from (this is not necessarily the same 
	//  as where the database names will come from - we look up where this is here
	sprintf( fn, "%s\\dev_%03d.ini", path, m_device );

	// get the size of this database (this is only used as a reference not to dimension anything)
	sprintf( sec, "DatabaseSize_%d", m_level );
	m_maxSize = GetPrivateProfileInt( "Database", sec, 0, fn );

	// work out what file it is that we should be using
	sprintf( sec, "DatabaseFile_%d", m_level );

	GetPrivateProfileString( "Database", sec, "", buf, sizeof( buf ), fn );
	if( !strlen( buf ))
	{
		OutputDebugString( "database::load - Can't find " );
		OutputDebugString( sec );
		OutputDebugString( " setting in file: ");
		OutputDebugString( fn );
		OutputDebugString( ". Does this file exist?\n" );
		return;
	}

	sprintf( fn, "%s\\%s", path, buf );

	// remember where this database came from so that we can do changes to it if we need to later
	m_filename = fn;

	// nothing sexy about the file parsing - just open a file and wiz through it
	FILE * f = fopen( fn, "r" );
	if( f )
	{
		// this flag is just used to determine whether we've hit a section that we're interested in
		bool reading = false;

		while( fgets( buf, sizeof( buf ), f ))
		{
			if( buf[ 0 ] == '[' )
			{
				// remvoe the newline character
				char *p;
				if( p = strchr( buf, '\n' ))
					*p = '\0';
				if( p = strchr( buf, '\r' ))
					*p = '\0';
				if( strlen( buf ) >= 12 )
				{
					if( !strncmp( buf, "[Database_", strlen( "[Database_" )))
					{
						if( buf[ 11 ] != ']' )
							continue;

						if( atoi( &buf[ 10 ] ) == m_level )
						{
							reading = true;
							continue;
						}
						else 
						{
							reading = false;
						}
					}
				}
			}
			if( reading )
			{
				char *p;

				// remove end of line chars
				if( p = strchr( buf, '\n' ))
					*p = '\0';
				if( p = strchr( buf, '\r' ))
					*p = '\0';

				if( p = strchr( buf, '=' ))
				{
					*p = '\0';

					int index = atoi( buf );
					int len = strlen( p + 1 );

					for( char *q = p + len ; q > p+1 ; q-- )
						if( *q == ' ' )
							*q = '\0';
						else
							break;

					string value = (p+1);

					db.insert( pair<int, string>( index, value ) );
				}
			}
		}
		fclose( f );
	}
	else
	{
		OutputDebugString( "database::load - can't open file: ");
		OutputDebugString( fn );
		OutputDebugString( "\n" );
	}
}

/** Get a name from our database cache
\param index The index of the name to get in this database
\return pointer to the string (note only valid until the next database call

\par Example:
\code
database db( "c:\\windows", 123, 0 );
const char * name = getName( 1 );		// name contains a pointer to the first element in the database
\endcode
 */
const char* database::getName( int index )
{ 
	if( index )
	{
		map< int, string>::iterator it;

		it = db.find( index );
		if( it != db.end())
		{
			return (it->second).c_str();
		}
	}
	if( m_level < 2 )
		return "---";
	else
		return "!!!";
}

/** Get the first index of the name specified
\param name The name to search for
\return Index of the name, or zero if it is not found

\par Example:
\code
database db( "c:\\windows", 123, 0 );
int i = getIndex( "Hello" );		// i contains the index of the name "Hello" if it exists in the current database
\endcode
 */
int database::getIndex( const char * name)
{ 
	for( map< int, string>::iterator it = db.begin() ; it != db.end() ; ++it )
	{
		if( it->second == name )
			return it->first;
	}
	return 0;
}

/** Get the first index of the name specified
\param name The name to search for
\return Index of the name, or zero if it is not found

\par Example:
\code
database db( "c:\\windows", 123, 0 );
int i = getIndex( "Hello" );		// i contains the index of the name "Hello" if it exists in the current database
\endcode
 */
int database::getIndex( const char * name, bool caseInsensitive, bool substring )
{ 
	int ret = 0;
	char entry[ 1024 ];
	char *copyName;

	copyName = new char[ strlen( name ) + 1 ];
	strcpy( copyName, name );
	if( caseInsensitive )
		strlwr( copyName );

	for( map< int, string>::iterator it = db.begin() ; it != db.end() ; ++it )
	{
		strncpy( entry, it->second.c_str(), sizeof( entry ) );
		
		if( caseInsensitive )
			strlwr( entry );

		if( substring )
		{
			if( strstr( entry, copyName))
			{
				ret = it->first;
				break;
			}
		}
		else
		{
			if( !strcmp( entry, copyName))
			{
				ret = it->first;
				break;
			}
		}
	}
	delete copyName;
	return ret;
}


/** Set a database entry
\param index The index of the entry to set
\param name The new name for this index
\return zero on no error

\par Example:
\code
database db( "c:\\windows", 123, 0 );
db.setName( 1, "hello" );	// set the name of index 1 of this database to name "Hello". This not only changes the value contained in memory but also writes this value to the correct place in the ini file
\endcode
\bug hello
 */

bool database::setName( int index, const char * name, bool writeToFile )
{
	// remove the entry from the map and add a new one back in again
	db.erase( index );
	db.insert(pair<int, string>( index, name ));

	if( writeToFile )
	{
		char sec[ 32 ];
		char entry[ 32 ];

		// work out the section for this level
		sprintf( sec, "Database_%d", m_level );

		// work out the entry name for this index
		sprintf( entry, "%04d", index);

		// work out the filename of the database file (from the ini file mapping info)
		return !WritePrivateProfileString( sec, entry, name, m_filename.c_str() );
	}
	return 0;
}

int database::size( void )
{
	return db.size();
}
