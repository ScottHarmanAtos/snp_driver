
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"
#include "UMD_Tally_Common.h"
//
class CRouterDataDest
{

public:
	CRouterDataDest( int iIndex );
	~CRouterDataDest();

	int m_iDestIndex;
	int m_iRoutedSourceNumber;							// straight revertive from grd 
	int m_iDestLockState;

	bncs_stringlist m_ssLinkedToUmdDevice;              // quick lookup for umd device assoc with this dest

	// tieline info                             // used between cmr and chain routers 
	int m_iTielineToRtr;                           // other router this dest feeds 
	int m_iTielineToSrc;                           // source this dest feeds on other router

};

