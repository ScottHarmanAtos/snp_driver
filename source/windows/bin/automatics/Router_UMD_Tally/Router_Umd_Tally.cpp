/* Router_Umd_Tally.cpp - custom program file for project Router_Umd_Tally */

#include "stdafx.h"
#include "Router_Umd_Tally.h"

/* TODO: place references to your functions and global variables in the Router_Umd_Tally.h file */

// local function prototypes
// this function is called when a valid slotchange notification is extracted from the InfoNotify
BOOL SlotChange(UINT iDriver,UINT uiSlot, LPCSTR szSlot);
void ApplySmartUMDTruncation2( bncs_string *ssCalcLabel );
void ProcessRouterModifyForSourceChange( int iChangedRouter, int iChangedSource, BOOL bFromSlotChange );
CRevsRouterData* GetRouterLinkedToUmdOverrideInfoSlot(int iInfoSlot);
void UpdateAlarmTXGui();


//////////////////////////////////////////////////////////////////////////
// 
//  COMMAND QUEUE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


void ClearCommandQueue( void )
{
	while (!qCommands.empty()) {
		CCommand* pCmd;
		pCmd = qCommands.front();
		qCommands.pop();
		delete pCmd;
	}
	SetDlgItemInt( hWndDlg, IDC_QUEUE, qCommands.size(), FALSE );
}

 
void AddCommandToQue( LPCSTR szCmd, int iCmdType, int iDev, int iSlot, int iDB )
{
	CCommand* pCmd = new CCommand;
	if (pCmd) {
		wsprintf( pCmd->szCommand, szCmd );
		pCmd->iTypeCommand = iCmdType;
		pCmd->iWhichDevice = iDev;
		pCmd->iWhichSlotDest = iSlot;
		pCmd->iWhichDatabase = iDB;
		qCommands.push( pCmd );
	}
	else 
		Debug( "AddCommand2Q - ERROR cmd record not created");

}

void ProcessNextCommand( int iRateRevs ) {
	
	BOOL bContinue=TRUE;
	do {
		if (qCommands.size()>0) {
			iRateRevs--;
			// something in queue so process it
			CCommand* pCommand = qCommands.front();
			qCommands.pop();
			if (pCommand) {
				// ONLY send cmd if main driver only or in starting mode for rx driver to register and poll initially
				if ((iOverallTXRXModeStatus==INFO_TXRXMODE)||(bAutomaticStarting==TRUE)) {
					//if ((bShowAllDebugMessages)&&(!bAutomaticStarting)) Debug( "ProcessCommand - TX> %s ", pCommand->szCommand  );
					switch(pCommand->iTypeCommand){
						case ROUTERCOMMAND:	ecCSIClient->txrtrcmd(pCommand->szCommand);	break;
						case INFODRVCOMMAND: ecCSIClient->txinfocmd(pCommand->szCommand); break;
						case GPIOCOMMAND:	ecCSIClient->txgpicmd(pCommand->szCommand); break;
						case DATABASECOMMAND:	
							eiExtInfoId->setdbname(iDevice, pCommand->iWhichDatabase, pCommand->iWhichSlotDest, pCommand->szCommand, FALSE );
						break;
						case AUTOINFOREVERTIVE:	eiExtInfoId->updateslot( pCommand->iWhichSlotDest, pCommand->szCommand );  break;
					default:
						Debug( "ProcessNextCmd - unknown type from buffer - %d %s", pCommand->iTypeCommand, pCommand->szCommand );
					} // switch
				}
				else {
					// as in RXonly mode - Clear the buffer - no need to go round loop
					ClearCommandQueue();
					bContinue=FALSE;
					iRateRevs=0;
				}
			}
			delete pCommand; // delete record as now finished 
		}
		else {
			// nothing else in queue
			bContinue = FALSE;
			iRateRevs=0;
		}
	} while ((bContinue==TRUE)&&(iRateRevs>0));

	SetDlgItemInt( hWndDlg, IDC_QUEUE, qCommands.size(), FALSE );
	UpdateCounters();
	
	// if more commands still in queue and timer not running then start timer, else perhaps kill it
	//if (qCommands.size()>0) {
	// NOW always start timer - in event there are more cmds in que...
		if (bNextCommandTimerRunning==FALSE) SetTimer( hWndMain, COMMAND_TIMER_ID, 50, NULL ); // timer for start poll // was 100
		bNextCommandTimerRunning = TRUE;
	//}
	//else {
	//	KillTimer( hWndMain, COMMAND_TIMER_ID ); 
	//	bNextCommandTimerRunning = FALSE;
	//}

}


void SendCommandImmediately( LPCSTR szCmd, int iCmdType, int iDev )
{
	if (iOverallTXRXModeStatus == INFO_TXRXMODE) {
		// only send if txrx
		switch (iCmdType){
		case ROUTERCOMMAND:	ecCSIClient->txrtrcmd(szCmd);	break;
		case INFODRVCOMMAND: ecCSIClient->txinfocmd(szCmd, NOW); break;
		case GPIOCOMMAND:	ecCSIClient->txgpicmd(szCmd); break;
		default:
			Debug("SendCommandImmediately - unknown type from buffer - %d %s", iCmdType, szCmd);
		} // switch
		if (bShowAllDebugMessages) Debug("SendCommandImmediately - TX> %s ", szCmd);
	}
}



//////////////////////////////////////////////////////////////////////////
// 
//  AUTOMATIC STARTUP FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


BOOL GetRouterDeviceDBSizes( bncs_string ssName, int *iRtrDevice, int *iDB0Size, int *iDB1Size ) 
{
	char szFileName[64]="", szEntry[12]="";
	int iNewDevice = 0;
	*iDB0Size=0; *iDB1Size=0;
	iNewDevice=getInstanceDevice( ssName );
	strcpy( szFileName, ssName );
	if (iNewDevice>0) {
		*iRtrDevice = iNewDevice;
		// get database sizes db0 and db1
		wsprintf(szFileName, "dev_%03d.ini", iNewDevice );
		*iDB0Size = atoi( r_p(szFileName, "Database", "DatabaseSize_0", "", false) );
		*iDB1Size = atoi( r_p(szFileName, "Database", "DatabaseSize_1", "", false) );
		return TRUE;
	}
	else
		return FALSE;
}


//
//  FUNCTION: ExtractSrcNamefromDevIniFile( )
//
//  PURPOSE: Extracts the src name from specified Database dev ini file and returns string 
//
BOOL ExtractSrcNamefromDevIniFile( int iDev, int iEntry, int iDbFile, bncs_string* ssName )
{
	char szFileName[256], szEntry[32], szDatabase[32], szName[256]="           -";

	if ((iDev>0)&&(iEntry>0)) {	
		sprintf(szFileName, "dev_%03d.db%d", iDev, iDbFile);
		sprintf(szEntry, "%04d", iEntry );
		sprintf(szDatabase, "Database_%d", iDbFile );
		strcpy( szName, r_p( szFileName, szDatabase, szEntry,"",FALSE) );
		bncs_string ssTmp = bncs_string(szName).replace('|', ' ');
		// remove excess spaces at this point before returning string
		bncs_string ssTmp1 = ssTmp.simplifyWhiteSpace();
		// Debug( "Extracted DB Name %s - after is %s", szName, LPCSTR( ssTmp1 ));
		*ssName = ssTmp1;
		//if (bShowAllDebugMessages)	Debug( "Extracted DB Name %s - after is %s", szName, LPCSTR( ssTmp1 ));
		// Debug( "Extracted DB Name %s - after is %s", szName, LPCSTR( ssTmp1 ));
		return TRUE;
	}
	return FALSE;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

char* LoadEntryFromDatabase(int iMixerDevice, int iDatabase, int iEntry)
{
	char szFileName[256] = "", szDatabase[32] = "", szIniFile[256] = "", szDBFile[256] = "", szEntry[32] = "";
	wsprintf(szIniFile, "dev_%03d.ini", iMixerDevice);
	wsprintf(szDBFile, "DatabaseFile_%d", iDatabase);
	wsprintf(szDatabase, "Database_%d", iDatabase);
	wsprintf(szEntry, "%04d", iEntry);
	// file name from ini file
	strcpy(szFileName, r_p(szIniFile, "Database", szDBFile, "", FALSE));
	if (bShowAllDebugMessages) Debug("LoadEntryFromDatabase - %s from %d %d", szFileName, iDevice, iDatabase);
	if (strlen(szFileName)<1) {
		// determine file name by another route -- using CSI default names
		if (iDatabase>9)
			sprintf(szFileName, "dev_%03d.db%04d", iMixerDevice, iDatabase);
		else
			sprintf(szFileName, "dev_%03d.db%d", iMixerDevice, iDatabase);
	}
	// now get required db entry -- r_p returns string in global szResult
	return r_p(szFileName, szDatabase, szEntry, "", FALSE);

}


////////////////////////////////////////////////////////////////////////////////////////////////////

char* GetParameterString( bncs_string sstr )
{
	strcpy( szResult, "");
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		wsprintf( szResult, "%s", LPCSTR(ssl1[1]) );
	return szResult;

}

int GetParameterInt( bncs_string sstr  )
{
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		return ssl1[1].toInt();
	else
		return UNKNOWNVAL;
}			


bncs_string GetStudio_Switch_Setting( bncs_string id, bncs_string parameter )
{
	bncs_string ret = bncs_config ( bncs_string("studio_output_switch.%1.%2").arg(id).arg(parameter) ).attr("value");
	Debug("GetStudio_Switch_Setting %s . %s -> %s", LPCSTR(id), LPCSTR(parameter), LPCSTR(ret) );
	return ret;
}



int LoadAllRouterTielines( void )
{
	int iEntry = 0;
	// get tieline data from main router dev db 9
	if (cl_AllRouters) {
		char szEntry[MAX_BUFFER_STRING] = "", szData1[64] = "", szData2[64] = "";
		// load table from config file
		// get table of defined rings for IFB
		bncs_string ssFileName = bncs_string("%1.%2").arg("tieline_manager_auto").arg("tieline_highways");
		bncs_config cfg(ssFileName);
		Debug("LoadHighways - getXMLTable - file : %s", LPCSTR(ssFileName));
		while (cfg.isChildValid())  {

			bncs_string strIdIndx = cfg.childAttr("id");
			bncs_string strValue = cfg.childAttr("value");

			if ((strValue.length()>0) && (strIdIndx.length()>0)) {
				//
				int iConfigIndex = strIdIndx.right(4).toInt();
				bncs_stringlist ssl = bncs_stringlist(strValue, ',');
				bncs_stringlist sslfrom = bncs_stringlist(ssl.getNamedParam("from"), '|');
				bncs_stringlist sslto = bncs_stringlist(ssl.getNamedParam("to"), '|');

				if ((sslfrom.count()>1) && (sslto.count()>1)) {

					int iFromRtr = 0, iToRtr = 0, iFromDest = 0, iToSrc = 0;
					// validate data 
					iFromRtr = getInstanceDevice(sslfrom[0]);
					iFromDest = sslfrom[1].toInt();
					iToRtr = getInstanceDevice(sslto[0]);
					iToSrc = sslto[1].toInt();

					if ((iFromRtr>0) && (iToRtr) && (iFromDest>0) && (iToSrc>0)) {
						// valid data so add to map etc
						iEntry++;
						Debug("LoadHighways entry %d index %d from %d %d to %d %d  ", iEntry, iConfigIndex, iFromRtr, iFromDest, iToRtr, iToSrc);
						// from
						CRevsRouterData* pFromRtr = GetValidRouter(iFromRtr);
						if (pFromRtr) {
							pFromRtr->setDestinationTielineData(iFromDest, iToRtr, iToSrc);
						}
						//to
						CRevsRouterData* pToRtr = GetValidRouter(iToRtr);
						if (pToRtr) {
							pToRtr->setSourceTielineData(iToSrc, iFromRtr, iFromDest);
						}

					}
					else
						Debug("loadAllInit - invalid/inactive highway/wrap data - entry ignored from %s - %s", LPCSTR(strIdIndx), LPCSTR(strValue));
				}
				else
					Debug("loadAllInit - invalid highway/wrap data - entry ignored from %s - %s", LPCSTR(strIdIndx), LPCSTR(strValue));
			}

			cfg.nextChild();				 // go on to the next sibling (if there is one)
		}
		Debug("LoadTableOfAllHighwaysAndWraps - %d items from tieline higways table", iEntry);
	}
	return iEntry;
}


/////////////////////////////////////////////////////////////////////////////

CRevsRouterData* GetOrAddRouterFromMap( bncs_string	ssRtrInstance, int iWhichRtr )
{
	// 
	CRevsRouterData* pRtr = NULL;
	int i_rtr_indx=0, iSrcs=0, iDests=0;
	pRtr = GetValidRouter(iWhichRtr);
	if (pRtr==NULL) {
		// router not in map - so add
		if ( GetRouterDeviceDBSizes( ssRtrInstance, &i_rtr_indx, &iSrcs, &iDests )==TRUE ) {
			pRtr = new CRevsRouterData(i_rtr_indx, iDests, iSrcs, 0 );
			if (pRtr) {
				Debug( "GetOrAddRouterFromMap - ADDED rtr %s - %d srcs %d dests %d ", LPCSTR(ssRtrInstance), i_rtr_indx, iSrcs, iDests );
				// add into map
				cl_AllRouters->insert( pair<int,CRevsRouterData*>(i_rtr_indx, pRtr ));
				ssl_AllRoutersIndices.append( i_rtr_indx );
				iNumberActiveRouters++;	
				SetDlgItemText( hWndDlg, IDC_AUTO_SDIRTR, LPCSTR(ssl_AllRoutersIndices.toString(',')));
			}
			else {
				Debug( "GetOrAddRouterFromMap -- CLASS FAILED for additional Router %d ", i_rtr_indx );
				return NULL;
			}
		}
		else {
			Debug( "LoadInit -- ERROR -- NO Router %s defined in instances ", LPCSTR(ssRtrInstance) );
			return NULL;
		}
	}
	return pRtr;
}



BOOL GetMVInstanceDeviceSettings(bncs_string ssMVId, int *iMVDev, int *iMVOffset, bncs_string *ssLoc, bncs_string *ssType, bncs_string *ssAltId)
{
	// extract details for mview record 
	int iDev = 0, iOffset = UNKNOWNVAL;
	char szLoc[MAX_BUFFER_STRING] = "", szType[MAX_BUFFER_STRING] = "", szAltId[MAX_BUFFER_STRING] = "";
	bncs_string ssInstDev = ssMVId;
	// is there a device and offset from pure mvid ?
	if (getInstanceDeviceSettings(ssInstDev, &iDev, &iOffset, szLoc, szType, szAltId) == TRUE) {
		if ((iDev > 0) && (iOffset >= 0)) {
			// values found so return
			*iMVDev = iDev;
			*iMVOffset = iOffset;
			*ssLoc = bncs_string(szLoc);
			*ssType = bncs_string(szType);
			*ssAltId = bncs_string(szAltId);
			Debug("LoadAllInit-GetMVDevSettings -a- %s - dev %d offset %d", LPCSTR(ssInstDev), iDev, iOffset);
			return TRUE;
		}
	}
	// else add umd suffix and try again
	iDev = 0;
	iOffset = UNKNOWNVAL;
	ssInstDev.append("#UMD_01");
	if (getInstanceDeviceSettings(ssInstDev, &iDev, &iOffset, szLoc, szType, szAltId) == TRUE) {
		if ((iDev > 0) && (iOffset >= 0)) {
			// values found so return
			*iMVDev = iDev;
			*iMVOffset = iOffset;
			*ssLoc = bncs_string(szLoc);
			*ssType = bncs_string(szType);
			*ssAltId = bncs_string(szAltId);
			Debug("LoadAllInit-GetMVDevSettings -b- %s - dev %d offset %d", LPCSTR(ssInstDev), iDev, iOffset);
			return TRUE;
		}
	}
	//
	Debug("LoadAllInit-GetMVDevSettings -c- NO INSTANCE for %s nor %s - mv skipped", LPCSTR(ssMVId), LPCSTR(ssInstDev));
	return FALSE;
}




//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: LoadAllInitialisationData()
//
//  PURPOSE: Function called on startup to load data from ini files / object settings etc relevant for driver
//                 creates new memory instances/
//
BOOL LoadAllInitialisationData( void ) 
{

	ssl_UseNoPrefix_Sources= bncs_stringlist("",',');

	char szEntry[256];
	bncs_string ssXmlConfigFile = "router_umd_auto";
	bncs_string ssXmlSectionId = "router_umd_auto";
	bncs_string strValue = "", ssMVId = "", strIdName = "";
	int iSrcs=0, iDests=0, iEntry=0, iI=0;
	BOOL bContinue = TRUE, bLoadAlarmTallyData = FALSE;

	i_rtr_TielineRtr_Index=0;
	i_rtr_CMRRtr_Index = 0;
	ssl_AllRoutersIndices = bncs_stringlist("", ',');

	iNumberActiveUmdDevs = 0;
	cl_AllUmdDevices = new map<bncs_string, CUmdDevice* >;   
	if (cl_AllUmdDevices==NULL) {
		Debug( "LoadInit -- ERROR - failed to create map of all devices ");
		return FALSE;
	}
	
	iNumberActiveRouters = 0;
	cl_AllRouters = new map<int, CRevsRouterData* >;   
	if (cl_AllRouters==NULL) {
		Debug( "LoadInit -- ERROR - failed to create map for all routers ");
		return FALSE;
	}
		
	Debug( "LoadAllInitData  - loading router data" );
	// routers - load from object settings
	strValue = getXMLItemValue(ssXmlConfigFile, ssXmlSectionId, "Main_Video_Router");
	if ( GetRouterDeviceDBSizes( strValue, &i_rtr_TielineRtr_Index, &iSrcs, &iDests )==TRUE ) {
		Debug( "LoadAllInitData - video rtr is %s - %d srcs %d dests %d ", LPCSTR(strValue), i_rtr_TielineRtr_Index, iSrcs, iDests );
		CRevsRouterData* pSDI = new CRevsRouterData(i_rtr_TielineRtr_Index, iDests, iSrcs, 0);
		if (pSDI) {
			SetDlgItemInt(hWndDlg, IDC_AUTO_SDIRTR, i_rtr_TielineRtr_Index, FALSE );
			// as main sdi -- so set the umd override value to 1 - by default  ( in event of older xml file without any defined entries for router overrides
			pSDI->setUMDOverrideStartSlot(1);
			// add into map
			cl_AllRouters->insert( pair<int,CRevsRouterData*>(i_rtr_TielineRtr_Index, pSDI ));
			ssl_AllRoutersIndices.append( i_rtr_TielineRtr_Index );
			iNumberActiveRouters++;

		}
		else {
			Debug( "LoadInit -- CLASS FAILED for SDI Router %d ", i_rtr_TielineRtr_Index );
			return FALSE;
		}
	}
	else {
		Debug( "LoadInit -- ERROR -- NO SDI Router defined in xml " );
		return FALSE;
	}

	// load other routers from tieline manager xml file
	bncs_string ssRtrsXml = getXMLItemValue(ssXmlConfigFile, ssXmlSectionId, "Routers_config_xml");
	// get  CMR and TX chain routers - ie those defined in xml file 
	bncs_string ssOff = "";
	int iiRtr = 0;
	for (int iiRtr = 1; iiRtr < 10; iiRtr++) {
		// load routers table - instance, offset, test and park srcs
		bncs_string ssArg = bncs_string("router_%1").arg(iiRtr);
		bncs_stringlist ssl_Data = bncs_stringlist(getXMLItemValue(ssRtrsXml, "tieline_routers", ssArg, "value"), ',');
		bncs_string ssInst = ssl_Data.getNamedParam("instance");
		int iOffset = ssl_Data.getNamedParam("virtual_offset").toInt();
		int iRtrIndx = 0, iMaxSrcs = 0, iMaxDests = 0;
		GetRouterDeviceDBSizes(ssInst, &iRtrIndx, &iMaxSrcs, &iMaxDests);
		if (iRtrIndx>0) {
			CRevsRouterData* pNewRtr = new CRevsRouterData(iRtrIndx, iMaxDests, iMaxSrcs, iOffset);
			if (pNewRtr) {
				ssl_AllRoutersIndices.append(bncs_string(iRtrIndx));
				// add into routers map
				if (cl_AllRouters) cl_AllRouters->insert(pair<int, CRevsRouterData*>(iRtrIndx, pNewRtr));
				Debug("LoadAllInit -  loading %d router %d srcs %d dests %d VIR offset %d ", iiRtr, iRtrIndx, iMaxSrcs, iMaxDests, iOffset);
				ssOff.append(bncs_string(" %1").arg(iOffset));
				if (iiRtr == 1) {
					i_rtr_CMRRtr_Index = iRtrIndx;
					i_CMR_VirtualsStart = 577;     // 577-640 are internal virtuals on CMR router
				}
			}
			else {
				Debug("LoadAllInit - **** ERROR - loading %d tieline ROUTER %d FALIED ", iiRtr, iRtrIndx);
			}
		}
	}
	SetDlgItemText(hWndDlg, IDC_AUTO_SDIRTR, LPCSTR(ssl_AllRoutersIndices.toString(',')));

	// load tielines - add links into router records
	LoadAllRouterTielines();

	i_SourceName_DB = SOURCE_NAME_DB; // as default
	strValue = getXMLItemValue(ssXmlConfigFile, ssXmlSectionId,"Source_Name_DB");
	i_SourceName_DB = strValue.toInt();
	SetDlgItemInt( hWndDlg, IDC_SRC_DB, i_SourceName_DB, FALSE );

	i_SoftName_DB = -1;  // soft names are not always required
	strValue = getXMLItemValue(ssXmlConfigFile, ssXmlSectionId,"Soft_Name_DB");
	if (strValue.length()>0) {
		i_SoftName_DB = strValue.toInt();
		SetDlgItemInt( hWndDlg, IDC_SOFT_DB, i_SoftName_DB, FALSE );
	}
	Debug( "LoadInit -- Source DB = %d   SoftName DB = %d  ", i_SourceName_DB, i_SoftName_DB );

	i_Max_UMD_Length = getXMLItemValue(ssXmlConfigFile, ssXmlSectionId,"Max_UMD_Length").toInt();
	if (i_Max_UMD_Length<2) i_Max_UMD_Length = 32;

	ss_UMD_Join = bncs_string(" - "); // default string 
	strValue = getXMLItemValue(ssXmlConfigFile, ssXmlSectionId,"UMD_Concat_String");
	if (strValue.length()>0) ss_UMD_Join = strValue;
	Debug( "LoadInit -- UMD Concat string is %s", LPCSTR(ss_UMD_Join));

	// get list of mv and tsl devices
	bncs_string ssMVFile = getXMLItemValue(ssXmlConfigFile, ssXmlSectionId, "Multiviewers_config_xml");
	bncs_stringlist ssl_mvs = bncs_stringlist(getXMLListOfItems(ssMVFile));
	Debug( "LoadAllInitData - loading multiview device data " );
	int iMVCount = 0;

	for (int iiMV = 0; iiMV < ssl_mvs.count(); iiMV++) {
		iMVCount++;
		// 
		bncs_string ssMVId = ssl_mvs[iiMV];
		bncs_string ssMVarea = getXMLItemIdSetting(ssMVFile, ssMVId, "area");
		Debug("LoadAllInit MV count %02d  mv %s area %s", iMVCount, LPCSTR(ssMVId), LPCSTR(ssMVarea));
		if (ssMVId.length()>0) {
			//
			int iMVDev = 0, iMVOffset = UNKNOWNVAL;
			bncs_string ssLoc = "", ssMVType = "", ssAltId = "";
			if (GetMVInstanceDeviceSettings(ssMVId, &iMVDev, &iMVOffset, &ssLoc, &ssMVType, &ssAltId)) {
				//
				int iType = KALEIDO_MVIP;
				if ((ssMVType.find("SAM") >= 0) || (ssMVType.find("TSLV5") >= 0)) iType = TSL_V5UMD_SAMVIEW;
				else if (ssMVType.find("BARCO") >= 0)  iType = BARCO_TSL_MVIEW;
				// 
				CUmdDevice* pUmd = new CUmdDevice(ssMVId, ssMVType, iType, iMVDev, iMVOffset, ssLoc, ssAltId);
				if (pUmd) {
					int iNumUmd = 0;
					BOOL bCont = TRUE;
					do {
						iNumUmd++;
						bncs_string ssItem = bncs_string("input_%1").arg(iNumUmd);
						if (iNumUmd < 10)  ssItem = bncs_string("input_0%1").arg(iNumUmd);
						// required new format data
						int iUmdType = UNKNOWNVAL;
						bncs_string strUmdType = getXMLItemValue(ssMVFile, ssMVId, ssItem, "type").upper();
						bncs_string strRtrInstance = getXMLItemValue(ssMVFile, ssMVId, ssItem, "router_instance");
						int iRtrIndx = getXMLItemValue(ssMVFile, ssMVId, ssItem, "router_index").toInt();
						bncs_string ssStatic = getXMLItemValue(ssMVFile, ssMVId, ssItem, "static_text");
						int iWhichRtr = getInstanceDevice(strRtrInstance);

						if (strUmdType.find("DEST") >= 0) iUmdType = DEST_UMD_TYPE;
						else if (strUmdType.find("SOURCE") >= 0) iUmdType = SOURCE_UMD_TYPE;
						else if (strUmdType.find("FIXED") >= 0) iUmdType = FIXED_UMD_TYPE;

						// was if ((strRtrInstance.length()>0) && (iWhichRtr>0) && (iRtrIndx>0)) {
						if (iUmdType>UNKNOWNVAL) {
							pUmd->storeUMDDetails(iNumUmd, iWhichRtr, iRtrIndx, iUmdType, ssStatic);
							Debug(" LoadAllInit- mv %s - %s type %d rtr %d indx %d -- %d ", LPCSTR(ssMVId),
								LPCSTR(ssItem), iUmdType, iWhichRtr, iRtrIndx, pUmd->getNumberOfUMDs());
							//
							// see if this router is known in list of routers - if not then add into map
							if ((iWhichRtr>0) && (iRtrIndx>0)) {
								CRevsRouterData* pRTR = GetOrAddRouterFromMap(strRtrInstance, iWhichRtr);
								if (pRTR) {
										if (iUmdType == DEST_UMD_TYPE) pRTR->setLinkedUmdDeviceforDest(iRtrIndx, ssMVId);
										else if (iUmdType == SOURCE_UMD_TYPE) pRTR->setLinkedUmdDeviceforSrc(iRtrIndx, ssMVId);
										else if ((iUmdType == FIXED_UMD_TYPE) && (iRtrIndx>0)) pRTR->setLinkedUmdDeviceforSrc(iRtrIndx, ssMVId);
								}
							}
						}
						else
							bCont = FALSE;

						//
					} while (bCont);

					// add to list box 
					wsprintf(szEntry, "%s ", LPCSTR(ssMVId));
					SendDlgItemMessage(hWndDlg, IDC_LIST1, LB_INSERTSTRING, -1, (LPARAM)szEntry);
					pUmd->storeListBoxEntry(iNumberActiveUmdDevs);
					// store into map
					if (cl_AllUmdDevices) {
						cl_AllUmdDevices->insert(pair<bncs_string, CUmdDevice*>(ssMVId, pUmd));
						ssl_AllUMDDevices.append(ssMVId);
						iNumberActiveUmdDevs++;
					}

				}
				else
					Debug("LoadAllInit -- failed to create umd class for %s", LPCSTR(ssMVId));
			}
			else
				Debug("LoadAllInit -- failed to find MV device for %s", LPCSTR(ssMVId));
		}
	}

	Debug( "LoadInit -- loaded %d records for ROUTER devices %s", iNumberActiveRouters, LPCSTR(ssl_AllRoutersIndices.toString(',')) );
	Debug( "LoadInit -- loaded %d records for umd devices ", iNumberActiveUmdDevs );

	if ((iNumberActiveUmdDevs>0)&&(iNumberActiveRouters>0))
		return TRUE;
	else
		return FALSE;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: AutomaticRegistrations()
//
//  PURPOSE:register with CSI for gory details
//
//  COMMENTS: 
//			
BOOL AutomaticRegistrations(void)
{
	// go thru defined data types and register with devices - need to find out router sizes for registration
	Debug( "into AutoRegistrations " );

	// register for all defined routers
	for (int iiRtr = 0; iiRtr < ssl_AllRoutersIndices.count(); iiRtr++) {
		CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iiRtr]);
		if (pRtr && ecCSIClient) {
			ecCSIClient->regtallyrange(pRtr->getRouterNumber(), 1, pRtr->getMaximumDestinations(), INSERT);
			Debug("AutoReg - router %d 1-%d ", pRtr->getRouterNumber(), pRtr->getMaximumDestinations());
		}
	}

	Debug( "out of AutoRegistrations " );
	return TRUE;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//

void PollPhysicalRouters(int iCounter)
{
	char szCommand[MAX_BUFFER_STRING] = "";

	if ((iCounter >= 0) && (iCounter<ssl_AllRoutersIndices.count())) {
		// use counter as index into rtr list for polling
		CRevsRouterData* pRtr = GetValidRouter(ssl_AllRoutersIndices[iCounter]);
		if (pRtr && ecCSIClient) {
				wsprintf(szCommand, "RP %d 1 %d", pRtr->getRouterNumber(), pRtr->getMaximumDestinations());
				ecCSIClient->txrtrcmd(szCommand);
				Debug("AutoPoll - GRD %d 1-%d ", pRtr->getRouterNumber(), pRtr->getMaximumDestinations());
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

int CalculateRandomOffset()
{
	// used to create a time offset before calling TXRX function - to stop multiInfodriver tx clash
	int iRand1 = 0;
	srand(time(0)); // generate random seed point
	iRand1 = (rand() % 2007);
	return (iRand1);
}



//
//  FUNCTION: InfoNotify()
//
//  PURPOSE: Callback function from an infodriver, notifying event
//
//  COMMENTS: the pointer pex references the infodriver which is notifying
//			  iSlot and szSlot are provided for convenience
//			
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot)
{

	if (pex) {
		switch (pex->iStatus) {
		case CONNECTED: // this is the "normal" situation
			UpdateCounters();
			if (!SlotChange(pex->iDevice, iSlot, szSlot)) pex->setslot(iSlot, szSlot);
		break;

		case DISCONNECTED:
			Debug("Infodriver %d has disconnected", pex->iDevice);
			SetDlgItemText(hWndDlg, IDC_INFO_STATUS, " ERROR: Disconnected");
			eiExtInfoId->updateslot(COMM_STATUS_SLOT, "0");	 // error	
			bValidCollediaStatetoContinue = FALSE;
			PostMessage(hWndMain, WM_CLOSE, 0, 0);
			return;
		break;

		case TO_RXONLY:
			if (bShowAllDebugMessages) Debug("Infodriver %d received request to go RX Only", pex->iDevice);
			if ((pex->iDevice == iDevice) && (iOverallTXRXModeStatus == INFO_TXRXMODE) && (iOverallStateJustChanged == 0)) {
				// need to bounce this request by reasserting this txrx  
				iOverallStateJustChanged = 1;
				eiExtInfoId->setmode(INFO_TXRXMODE);
				eiExtInfoId->requestmode = TO_TXRX;
				iOverallModeChangedOver = 3;
				iNextResilienceProcessing = 2;
			}
		break;

		case TO_TXRX:
			if (bShowAllDebugMessages) Debug("Infodriver %d received request to go TO TXRX ", pex->iDevice);
			if ((pex->iDevice == iDevice) && (iOverallTXRXModeStatus == INFO_RXMODE) && (iOverallStateJustChanged == 0)) {
				// current driver rxonly - therefore try and change mode
				iOverallStateJustChanged = 1;
				int iTimerOffset = CalculateRandomOffset();
				Debug("InfoNotify - force go TXRX - in %d ms", iTimerOffset);
				SetTimer(hWndMain, FORCETXRX_TIMER_ID, iTimerOffset, NULL);
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Req Tx/Rx :- MAIN ");
			}
		break;

		case QUERY_TXRX:
		case 2190:
			// ignore this state -- means all left as it was 
		break;

		default:
			Debug("iStatus=%d", pex->iStatus);

		}
	}
	else 
		Debug("Infonotify - invalid Pex class passed in");


}

//
//  FUNCTION: SlotChange()
//
//  PURPOSE: Function called when slot change is notified
//
//  COMMENTS: All we need here are the driver number, the slot, and the new contents
//		
//	RETURNS: TRUE for processed (don't update the slot),
//			 FALSE for ignored (so calling function can just update the slot 
//								like a normal infodriver)
//			

BOOL SlotChange(UINT iDevice,UINT uiSlot, LPCSTR szSlot)
{
	// 	
	int iSlot = int(uiSlot);
	int iRtrSourceFromSlotIndex = 0;
	CRevsRouterData* pSDIRtr = NULL;
	//
	if ((iSlot > 0) && (iSlot < SLOT_OFFSET_AUX_UMD)) {
		// slot number equates to source number for umd override
		pSDIRtr = GetRouterLinkedToUmdOverrideInfoSlot(iSlot);
		if (pSDIRtr) {
			if ((iSlot >= pSDIRtr->getUMDOverrideStartSlot()) && (iSlot < (pSDIRtr->getUMDOverrideStartSlot() + pSDIRtr->getMaximumSources())))
			{
				iRtrSourceFromSlotIndex = iSlot - pSDIRtr->getUMDOverrideStartSlot() + 1;
				bncs_string ssUmdOverride = bncs_string(szSlot);
				eiExtInfoId->setslot(iSlot, szSlot);
				if (ssUmdOverride.length()>0)
					Debug("Slot change %d equates to rtr %d source %d - override umd is now '%s'", iSlot, pSDIRtr->getRouterNumber(), iRtrSourceFromSlotIndex, szSlot);
				else
					Debug("Slot change %d equates to rtr %d source %d - umds reverts to Router Source name", iSlot, pSDIRtr->getRouterNumber(), iRtrSourceFromSlotIndex );
				// now process change for Umds for relevant devices
				ProcessRouterModifyForSourceChange(pSDIRtr->getRouterNumber(), iRtrSourceFromSlotIndex, TRUE);
			}
		}
		else {
			Debug("SlotChange -warn- no defined router for UMD override slot %d", iSlot);
		}
	}

	return TRUE;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// 
//  UMD DEVICE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////
BOOL TracePrimaryRouterSource(int iStartRtr, int iStartSrc, int *iFinRtr, int *iFinSrc )
{
	// check for CMR and virtual src - trace back to real CMR source in range 1-576
	int iTracRtr = iStartRtr;
	int iTracSrc = iStartSrc;
	int iTracDest = 0;
	// set outs to ins in event of no change
	BOOL bRet = FALSE;
	*iFinRtr = iStartRtr;
	*iFinSrc = iStartSrc;
	//
	if ((iTracRtr == i_rtr_CMRRtr_Index) && (iTracSrc >= i_CMR_VirtualsStart)) {
		// note - assumption is that it is single route in virtual, not a virtual into a virtual - no recursion in this function
		iTracDest = iTracSrc;
		CRevsRouterData* pCMRRtr = GetValidRouter(iTracRtr);
		if (pCMRRtr) {
			iTracSrc = pCMRRtr->getRoutedSourceForDestination(iTracDest);
			*iFinRtr = iTracRtr;
			*iFinSrc = iTracSrc;
			bRet = TRUE;
			if (bShowAllDebugMessages) Debug("TracePrimary - CMR vir %d %d  to cmr src %d from vir dest %d", iStartRtr, iStartSrc, iTracSrc, iTracDest);
		}
	}
	// check for single highway step from CMR back to a chain router - qnd trace
	iTracDest = 0;
	CRevsRouterData* pInRtr = GetValidRouter(iTracRtr);
	if (pInRtr) {
		pInRtr->getSourceTielineData(iTracSrc, &iTracRtr, &iTracDest);
		if ((iTracRtr > 0) && (iTracDest > 0)) {
			// has tieline values so trace back to tx router
			CRevsRouterData* pTracRtr = GetValidRouter(iTracRtr);
			if (pTracRtr) {
				iTracSrc = pTracRtr->getRoutedSourceForDestination(iTracDest);
				bRet = TRUE;
				if (bShowAllDebugMessages) Debug("TracePrimary - orig %d %d to traced %d %d from dest %d", iStartRtr, iStartSrc, iTracRtr, iTracSrc, iTracDest);
				*iFinRtr = iTracRtr;
				*iFinSrc = iTracSrc;
				return bRet ;
			}
			else
				Debug("TracePrimaryRtrSrce - no TRAC rtr %d ", iTracRtr);
		}
	}
	else
		Debug("TracePrimaryRtrSrce - no IN rtr %d ", iStartRtr);
	return bRet;
}


BOOL DetermineActualUmdLabels( int iWhichRouter, int iUmdSource, bncs_string* ssUmdPart1, bncs_string* ssUmdPart2, BOOL bNotFromRM )
{
	// part1 is main/override label, part 2 is used only for soft names where applicable
	bncs_string ssUmdSourceLabel = "";   // primary name from given umdsource - used as prefix for traced / virtual sources
	bncs_string ssCalcLabel = "";                 // calculated final umd name from traced source
	bncs_string ssSoftName="";
	char szOverUmd[MAX_BUFFER_STRING]="";
	BOOL bUseOverride=FALSE;

	if ((iUmdSource>0)&&(iUmdSource<MAXROUTERSIZE) ) {

		// check is source on the given router is a tieline/highway - if so then trace back to primary source
		int iFinUmdRtr = iWhichRouter;
		int iFinUmdSrc = iUmdSource;
		BOOL bTracedSrc = TracePrimaryRouterSource(iWhichRouter, iUmdSource, &iFinUmdRtr, &iFinUmdSrc);
		if (bShowAllDebugMessages) Debug("DetUmdLabel -starting rtr %d src %d    fin rtr %d fin src %d", iWhichRouter, iUmdSource, iFinUmdRtr, iFinUmdSrc);
		CRevsRouterData* pRtr = GetValidRouter(iFinUmdRtr);
		if (pRtr) {

			// see if there is an override first -- now applies to all routers if defined
			if (pRtr->getUMDOverrideStartSlot()>0) {

				int iInfoSlot = pRtr->getUMDOverrideStartSlot() + iFinUmdSrc - 1;
				if ((iInfoSlot > 0) && (iInfoSlot < SLOT_OFFSET_AUX_UMD)) {
					eiExtInfoId->getslot(iInfoSlot, szOverUmd);
					// remove perm $ char from start of string if found

					bncs_string ssUMD = bncs_string(szOverUmd);
					bncs_string sstest1 = ssUMD;

					int iPos = sstest1.contains('$');
					if (iPos>0) {
						ssUMD = sstest1.remove(iPos - 1, 1);
						Debug("DetActualUMD - $ found at pos %d, before %s after %s", iPos, LPCSTR(sstest1), LPCSTR(ssUMD));
					}
					// test now for just spaces - fix if just white space in slot
					bncs_string sstest2 = ssUMD.stripWhiteSpace();
					if ((sstest2.length() == 0) && (strlen(szOverUmd)>0)) {
						eiExtInfoId->updateslot(iInfoSlot, "");
					}
					else
						ssCalcLabel = ssUMD; // restore to label ( minus any perm char "$" for rest of processing
					if (ssCalcLabel.length()>0) bUseOverride = TRUE;
				}
			}
		}
		else
			Debug("DetermineUMDLabel - warning - no router for %d ", iFinUmdRtr);

		if (bUseOverride) {
			// umd is that from override slot
			if (bShowAllDebugMessages) Debug( "DetermineUMDLabel - OVERRIDE as %s ", LPCSTR(ssCalcLabel));
			*ssUmdPart1 = ssCalcLabel;
			*ssUmdPart2 = ssSoftName;
			return TRUE;
		}
		else {
			// now get names from correct router databases
			if (pRtr) {
				ExtractSrcNamefromDevIniFile(pRtr->getRouterNumber(), iFinUmdSrc, i_SourceName_DB, &ssCalcLabel);
				if (bTracedSrc) {
					CRevsRouterData* pUmdRtr = GetValidRouter(iWhichRouter);
					if (pUmdRtr) {
						ExtractSrcNamefromDevIniFile(pUmdRtr->getRouterNumber(), iUmdSource, i_SourceName_DB, &ssUmdSourceLabel);
						if (bShowAllDebugMessages) Debug("DetermineUMDLabel - traced UMD is %s - %s", LPCSTR(ssUmdSourceLabel), LPCSTR(ssCalcLabel));
						*ssUmdPart1 = ssUmdSourceLabel;
						*ssUmdPart2 = ssCalcLabel;
						return TRUE;
					}
				}
				else {
					if (i_SoftName_DB>0) ExtractSrcNamefromDevIniFile(pRtr->getRouterNumber(), iFinUmdSrc, i_SoftName_DB, &ssSoftName);
					bncs_stringlist ssl1 = bncs_stringlist( ssl_UseNoPrefix_Sources.toString(','), ',' );
					if (ssl1.find(iFinUmdSrc) >= 0) {
						// if found in list then use only the softname as umd label
						ssCalcLabel = ssSoftName;
						ssSoftName = "";
						if (bShowAllDebugMessages) Debug("DetermineUMDLabel - umd src %d found in NO prefix list", iFinUmdSrc);
					}
					if (bShowAllDebugMessages) Debug("DetermineUMDLabel - calc UMD is %s - %s", LPCSTR(ssCalcLabel), LPCSTR(ssSoftName));
					*ssUmdPart1 = ssCalcLabel;
					*ssUmdPart2 = ssSoftName;
					return TRUE;
				}
			}
			else
				Debug("DetermineUMDLabel - ERROR - no router for %d ", iFinUmdRtr);
		}
	}
	if (bShowAllDebugMessages) Debug( "DetermineUMDLabel - ERROR in rtr %d or src number %d ", iWhichRouter, iUmdSource );
	*ssUmdPart1 = ssCalcLabel;
	*ssUmdPart2 = ssSoftName;
	return FALSE;
}



CUmdDevice* GetValidUmdDevice( int iIndx )
{
	if ((iIndx>0) && (iIndx<=iNumberActiveUmdDevs )) {
		// get device
		bncs_string ssDevice = ssl_AllUMDDevices[iIndx-1];
		map<bncs_string,CUmdDevice*>::iterator itp;
		if (ssDevice.length()>0) {
			itp =  cl_AllUmdDevices->find(ssDevice);
			if (itp != cl_AllUmdDevices->end())	return itp->second;
		}
		// alternate way
		itp = cl_AllUmdDevices->begin();
		while (itp!=cl_AllUmdDevices->end()) {
			if (itp->second) {
				if (itp->second->getListBoxEntry()==(iIndx-1)) return itp->second;
			}
			itp++;
		} // while
	}
	return NULL;
}


CUmdDevice* GetUmdDeviceRecord( bncs_string ssKey )
{
	map<bncs_string,CUmdDevice*>::iterator itp;
	itp =  cl_AllUmdDevices->find(ssKey);
	if (itp != cl_AllUmdDevices->end()) {
		return itp->second;
	}
	else {
		Debug( "GetUMDRecord -- ERROR - no entry for %s ", LPCSTR(ssKey) );
	}
	return NULL;
}


CRevsRouterData* GetMainSDIRouter()
{
	// this is the combined tieline manager router
	map<int, CRevsRouterData*>::iterator itp;
	itp = cl_AllRouters->find(i_rtr_TielineRtr_Index);
	if (itp != cl_AllRouters->end()) {
		return itp->second;
	}
	else {
		Debug("GetMainSDIRouter -- ERROR - no MAIN SDI entry for %d ", i_rtr_TielineRtr_Index);
	}
	return NULL;
}

CRevsRouterData* GetCMRRouter()
{
	// real CMR router
	map<int, CRevsRouterData*>::iterator itp;
	itp = cl_AllRouters->find(i_rtr_CMRRtr_Index);
	if (itp != cl_AllRouters->end()) {
		return itp->second;
	}
	else {
		Debug("GetMainSDIRouter -- ERROR - no CMR entry for %d ", i_rtr_CMRRtr_Index);
	}
	return NULL;
}

CRevsRouterData* GetValidRouter( int iWhichRtr )
{
	if (iWhichRtr > 0) {
		map<int, CRevsRouterData*>::iterator itp;
		itp = cl_AllRouters->find(iWhichRtr);
		if (itp != cl_AllRouters->end()) {
			return itp->second;
		}
	}
	return NULL;
}

CRevsRouterData* GetRouterLinkedToUmdOverrideInfoSlot(int iInfoSlot)
{
	map<int, CRevsRouterData*>::iterator itp;
	itp = cl_AllRouters->begin();
	while (itp != cl_AllRouters->end()) {
		CRevsRouterData* pRtr = itp->second;
		if (itp->second) {
			int iStartSlotRange = itp->second->getUMDOverrideStartSlot();
			if (iStartSlotRange > 0) {
				int iEndSlotRange = iStartSlotRange + itp->second->getMaximumSources() - 1;
				// override normal umd within slot range ??
				if ((iInfoSlot >= iStartSlotRange) && (iInfoSlot <= iEndSlotRange)) return itp->second;
				// override aux umd within slot range + 2000 ?? -- for main sdi router only
				if ((iInfoSlot >= (iStartSlotRange + SLOT_OFFSET_AUX_UMD)) && (iInfoSlot <= (iEndSlotRange + SLOT_OFFSET_AUX_UMD))) return itp->second;
			}
		}
		itp++;
	} // while			
	return NULL;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// UMD LABELS
void SendUmdLabel( CUmdDevice* pUmd, int iIndex, bncs_string ssDevice )
{

	if (pUmd) {
		int iType = pUmd->getUMDDevType();
		if ((iType==TSL_UMD_TYPE_16)||(iType==TSL_UMD_TYPE_2X8)) {
			int iBNCSdev = pUmd->getBNCSDriver();  
			int iBNCSoff = pUmd->getDriverOffset();  
			if (iIndex==2) // right side of 2 X 8 tsl umd ( aka virtual )
				iBNCSoff = iBNCSoff + 6;  // 6th slot in group of 10 slots per hardware device
			else
				iBNCSoff = iBNCSoff + 1;
			// compare prev and new commands - only send out if different or forcing an update
			bncs_string ssPrevCmd1= pUmd->getRecentLabelCommand(iIndex);
			bncs_string ssNewCmd1 = bncs_string( "IW %1 '%2' %3" ).arg(iBNCSdev).arg(pUmd->getCalcUmdText(iIndex)).arg(iBNCSoff);
			if ((ssPrevCmd1.find(ssNewCmd1)<0)||(bForceUMDUpdate==TRUE))  {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue( LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0 );
				}
				else {	// send command immediately 
					SendCommandImmediately( LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev );
				}
				pUmd->setRecentLabelCommand( iIndex, ssNewCmd1 );
				Debug("SendLabel TSL device %s index %d : %s", LPCSTR(ssDevice), iIndex, LPCSTR(ssNewCmd1));
			}
		}
		else if ((iType == BARCO_TSL_MVIEW) || (iType == TSL_V5UMD_SAMVIEW)) {
			// get dev and slot and send command 
			int iBNCSdev = pUmd->getBNCSDriver();
			int iBNCSoff = pUmd->getDriverOffset();
			iBNCSoff = ((iIndex - 1) * 10) + iBNCSoff + 1;  // 10 slots per tsl style umd for barco
			// compare prev and new commands - only send out if different or forcing an update
			bncs_string ssPrevCmd1 = pUmd->getRecentLabelCommand(iIndex);
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pUmd->getCalcUmdText(iIndex)).arg(iBNCSoff);
			if ((ssPrevCmd1.find(ssNewCmd1)<0) || (bForceUMDUpdate == TRUE))  {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentLabelCommand(iIndex, ssNewCmd1);
				Debug("SendLabel Tv5 device %s index %d : %s", LPCSTR(ssDevice), iIndex, LPCSTR(ssNewCmd1));
			}
		}
		else if (iType == KALEIDO_MVIP) {
			// compare prev and new commands - only send out if different or forcing an update --  
			int iBNCSdev = pUmd->getBNCSDriver();
			int iBNCSoff = pUmd->getDriverOffset();
			bncs_string ssPrevCmd1 = pUmd->getRecentLabelCommand(iIndex);
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pUmd->getCalcUmdText(iIndex)).arg(iBNCSoff + iIndex + 100);
			if ((ssPrevCmd1.find(ssNewCmd1)<0) || (bForceUMDUpdate == TRUE))  {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentLabelCommand(iIndex, ssNewCmd1);
				Debug("SendLabel KX device %s index %d : %s", LPCSTR(ssDevice), iIndex, LPCSTR(ssNewCmd1));
			}
		}
		else if (iType == EVERTZ_MVIP) {
			int iBNCSdev = pUmd->getBNCSDriver();  
			int iBNCSoff = pUmd->getDriverOffset();  
			// compare prev and new commands - only send out if different or forcing an update
			bncs_string ssPrevCmd1=pUmd->getRecentLabelCommand(iIndex);
			bncs_string ssNewCmd1 = bncs_string( "IW %1 '%2' %3" ).arg(iBNCSdev).arg(pUmd->getCalcUmdText(iIndex)).arg(iBNCSoff+iIndex);
			//
			if ((ssPrevCmd1.find(ssNewCmd1)<0)||(bForceUMDUpdate==TRUE))  {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue( LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0 );
				}
				else {	// send command immediately 
					SendCommandImmediately( LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev );
				}
				pUmd->setRecentLabelCommand( iIndex, ssNewCmd1 );
				Debug("SendLabel VIP device %s index %d : %s",  LPCSTR(ssDevice), iIndex, LPCSTR(ssNewCmd1));
			}
		}
	}
}


void ApplySmartUMDTruncation2( bncs_string *ssCalcLabel )
{
	bncs_string ssInLabel = *ssCalcLabel;
	int iterationCount = 0, iEndLength=0;
	do {
		iterationCount++;
		int iStartLength = ssInLabel.length();
		bncs_stringlist ssList = bncs_stringlist( ssInLabel, ' ');
		bncs_stringlist ssLengths = bncs_stringlist("0,0,0,0,0,0,0,0,0,0,0,0", ',');
		// load lengths of words
		int iWords=0, iPosLongestWord=0, iLongestWord=0;
		for (iWords=0;iWords<ssList.count();iWords++) {
			int iL = ssList[iWords].length();
			ssLengths[iWords] = bncs_string(iL);
			if (iL>=iLongestWord) {
				iLongestWord = iL;
				iPosLongestWord = iWords;
			}
		}
		// if one very long word then chop middle out of it

		// else go thru words and cut chars from the longest words
		for (iWords=0;iWords<ssList.count();iWords++) {
			bncs_string ssTmp;
			if ((iWords>1)&&(iWords<ssList.count())) {
				// trim chars off words in middle of string down to min of 4 
				if (ssList[iWords].length()>5) {
					if (iPosLongestWord==iWords) {
						ssTmp = ssList[iWords].left(ssLengths[iWords].toInt()-4);
						ssTmp.append("~");
						ssList[iWords] = ssTmp;
					}
					else {
						if (iterationCount>2) {
							ssTmp = ssList[iWords].left(ssLengths[iWords].toInt()-1);
							ssList[iWords] = ssTmp;
						}
					}
				}
			}
			else {
				// only trim first and last words if longer than 4 chars - only after several iterations to trim other words
				if ((ssList[iWords].length()>4)&&(iterationCount>4)) {
					bncs_string ssTmp = ssList[iWords].left(ssLengths[iWords].toInt()-1);
					ssList[iWords] = ssTmp;
				}
			}
		}
		// rebuild the string and check length
		ssInLabel = ssList.toString(' ');
		iEndLength = ssInLabel.length();
		Debug( "Smart2 - iter %d : string:%s len:%d", iterationCount, LPCSTR(ssInLabel), iEndLength);
	} while ((iEndLength>i_Max_UMD_Length)&&(iterationCount<10));
	
	if (bShowAllDebugMessages) Debug( "SmartTruncation2 - start:%s end:%s ", LPCSTR( *ssCalcLabel ), LPCSTR(ssInLabel) );
	*ssCalcLabel = ssInLabel;

}


void ProcLabelsForGivenDevice( CUmdDevice* pUmd, bncs_string ssDevice )
{
	bncs_string ssCalcLabel, ssSoftName, ssAuxLabel;

	if (pUmd) {
		// get src / dests / fixed for umds
		for (int iU=1;iU<=pUmd->getNumberOfUMDs();iU++) {
			int iSrcDest=UNKNOWNVAL;
			int iSrcDestType = pUmd->getUmdTypeAndSrcDestByIndex( iU, &iSrcDest );
			int iWhichRtr = pUmd->getUmdSrcDestRouter( iU );			
			if (iWhichRtr==0) iWhichRtr=i_rtr_TielineRtr_Index;  // as default 
			bncs_string ssPrevLabel = pUmd->getCalcUmdText( iU );   
			bncs_string ssPrevAuxLabel = pUmd->getUmdAuxLabel( iU );

			if (iSrcDestType==FIXED_UMD_TYPE) { 
				SendUmdLabel( pUmd, iU, ssDevice );
			}
			else {
				// source and dest type labels need known router
				CRevsRouterData* pRtr = GetValidRouter(iWhichRtr);
				if (pRtr) {
					//
					if (iSrcDestType == SOURCE_UMD_TYPE) {
						DetermineActualUmdLabels(iWhichRtr, iSrcDest, &ssCalcLabel, &ssSoftName, TRUE);
						if ((pUmd->getUMDDevType() != TSL_UMD_TYPE_2X8) && (i_SoftName_DB>0)) {
							// for umds other than 2X8, those with 16 chars or so - add softname
							//ExtractSrcNamefromDevIniFile( pSDI->getRouterNumber(), iSrcDest, i_SoftName_DB, &ssSoftName );
							if (ssSoftName.length()>0) {
								ssCalcLabel.append(ss_UMD_Join);
								ssCalcLabel.append(ssSoftName);
							}
						}

						if (int(ssCalcLabel.length())>i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssCalcLabel);
						pUmd->storeCalculatedUmdName(iU, ssCalcLabel);
						SendUmdLabel(pUmd, iU, ssDevice);  // send will only actually send if changes to labels or force update

					}
					else if (iSrcDestType == DEST_UMD_TYPE) {
						int iRoutedSrc = pRtr->getRoutedSourceForDestination(iSrcDest);
						if (iRoutedSrc>0) {
							DetermineActualUmdLabels(iWhichRtr, iRoutedSrc, &ssCalcLabel, &ssSoftName, TRUE);
							if ((pUmd->getUMDDevType() != TSL_UMD_TYPE_2X8) && (ssSoftName.length()>0)) {
								ssCalcLabel.append(ss_UMD_Join);
								ssCalcLabel.append(ssSoftName);
							}
							if (int(ssCalcLabel.length())>i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssCalcLabel);
							pUmd->storeCalculatedUmdName(iU, ssCalcLabel);
							SendUmdLabel(pUmd, iU, ssDevice); // send will only actually send if changes to labels or force update

						}
					}
				}
				else
					Debug("ProcessLabelsForDevice -ERROR- no rtr class for %d", iWhichRtr);
			}

		} // for iU

		// do all processing first as this is very fast in cpu terms
		if (bNextCommandTimerRunning==FALSE ) ProcessNextCommand( BNCS_COMMAND_RATE );
	}
}


void ProcessLabelsForAllUMDS( )
{
	// go thru all devices and update for  all  labels
	for (int iD=0; iD<ssl_AllUMDDevices.count(); iD++ )  {
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev=ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord( ssCurDev );
		if ( pUmd ) {
			ProcLabelsForGivenDevice( pUmd, ssCurDev );
		} 
	} // for ID
}




////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
//   CSI CLIENT FUNCTIONS FOR ROUTER REVERTIVES MESSAGES

void StoreRouterRevertive( int iRtrNum, int iRtrDest, int iRtrSource, int *iPrevSource )
{
	*iPrevSource = UNKNOWNVAL;
		CRevsRouterData* pRtr = GetValidRouter( iRtrNum );
		if (pRtr) {
			// video router rev   -- if src is assoc to a tieline then store rev and force process by not setting prev source
			if (!pRtr->isSourceAssocToTieline(iRtrSource)) *iPrevSource = pRtr->getRoutedSourceForDestination( iRtrDest );
			pRtr->storeGRDRevertive( iRtrDest, iRtrSource );
		}
		else
			Debug("StoreRtrRev - unknown router %d", iRtrNum );
}


BOOL IsThisATielineDest( int iRtrNum, int iRtrDest )
{
	CRevsRouterData* pRtr = GetValidRouter(iRtrNum);
	if (pRtr) {
		return pRtr->isDestinationAssocToTieline(iRtrDest);
	}
	return FALSE;
}


void ProcessRouterRevertive( int iRtrNum, int iRtrDest )
{
	// function works for both SDI and ( if st6 ) GFX router revs 
	bncs_string ssCalcLabel="", ssSrcName="", ssSoftName="", ssAuxLabel="";
	
	CRevsRouterData* pRtr = GetValidRouter(iRtrNum);
	if (pRtr) {

		int iNewSrc = pRtr->getRoutedSourceForDestination( iRtrDest );
		BOOL bUpdateGui = FALSE;
		
		if (iNewSrc>0) {
			// go thru all devices and update for  all  labels using the changed src
			ProcessLabelsForAllUMDS();		
		}

	}
	else
		Debug("ProcessRtrRev -ERROR- no router in class for %d ", iRtrNum );
}


void ProcessRouterModifyForSourceChange( int iChangedRouter, int iChangedSource, BOOL bFromSlotChange  )
{
	bncs_string ssUmdLabel="", ssSoftName="", ssAuxLabel="";

	DetermineActualUmdLabels( iChangedRouter, iChangedSource, &ssUmdLabel, &ssSoftName, bFromSlotChange );
	Debug("ProcessRouterModifyForSourceChange %d %d umdlabel=%s softname=%s", iChangedRouter, iChangedSource, LPCSTR(ssUmdLabel), LPCSTR(ssSoftName));
	// go thru all devices and update for  all  labels using the changed src
	for (int iD=0; iD<ssl_AllUMDDevices.count(); iD++ )  {
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev=ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord( ssCurDev );
		if ( pUmd ) {
			for (int iU=1;iU<=pUmd->getNumberOfUMDs();iU++) {
				int iSrcDest=UNKNOWNVAL;
				int iUmdType = pUmd->getUmdTypeAndSrcDestByIndex( iU, &iSrcDest );
				int iWhichRtr = pUmd->getUmdSrcDestRouter( iU );
				bncs_string ssPrevLabel = pUmd->getCalcUmdText( iU );
				if (bShowAllDebugMessages&&iChangedSource==iSrcDest) Debug( "ProcRMforSource device %d type %d %s - srcdest %d", iD, iUmdType, LPCSTR( ssCurDev), iSrcDest  );
				if (iUmdType==SOURCE_UMD_TYPE) {
					if ((iWhichRtr==iChangedRouter)&&(iSrcDest==iChangedSource)) { 
						bncs_string ssCalcLabel = ssUmdLabel;
						if ((pUmd->getUMDDevType()!=TSL_UMD_TYPE_2X8)&&(i_SoftName_DB>0)) {
							// for umds other than 2X8, those with 16 chars or so - add softname
							if (ssSoftName.length()>0) {
								ssCalcLabel.append(ss_UMD_Join);
								ssCalcLabel.append(ssSoftName);
							}
						}
						if (int(ssCalcLabel.length())>i_Max_UMD_Length) ApplySmartUMDTruncation2( &ssCalcLabel );
						pUmd->storeCalculatedUmdName(iU, ssCalcLabel );
						SendUmdLabel( pUmd, iU, ssCurDev ); // send will only actually send if changes to labels or force update
					}
				}
				else if (iUmdType==DEST_UMD_TYPE) { 
					CRevsRouterData* pRtr = GetValidRouter(iWhichRtr);
					if (pRtr) {
						int iRoutedSrc = pRtr->getRoutedSourceForDestination( iSrcDest );
						if ((iWhichRtr==iChangedRouter)&&(iRoutedSrc==iChangedSource)) {
							bncs_string ssCalcLabel = ssUmdLabel;
							if ((pUmd->getUMDDevType()!=TSL_UMD_TYPE_2X8)&&(i_SoftName_DB>0)) {
								if (ssSoftName.length()>0) {
									ssCalcLabel.append(ss_UMD_Join);
									ssCalcLabel.append(ssSoftName);
								}
							}
							if (int(ssCalcLabel.length())>i_Max_UMD_Length) ApplySmartUMDTruncation2( &ssCalcLabel );
							pUmd->storeCalculatedUmdName(iU, ssCalcLabel );
							SendUmdLabel( pUmd, iU, ssCurDev ); // send will only actually send if changes to labels or force update
						}
					}
				}
			} // for iU
			// update gui as required
			if (ssCurrentChosenDevice.find(ssCurDev)>=0) DisplayUmdDeviceData();
		} 
	}
	// do all processing first as cpu very fast
	if (bNextCommandTimerRunning==FALSE ) ProcessNextCommand( BNCS_COMMAND_RATE );				
}



//////////////////////////////////////////////////////////////////////////////
//
/// RM recent list manipulation
BOOL FindEntryInRecentRMList(int iDev, int iDB, int iIndex)
{
	bncs_string ssStr = bncs_string("%1,%2,%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.find(ssStr)>=0) {
		return TRUE;
	}
	else{
		return FALSE;
	}
} 


void AddEntryToRecentRMList( int iDev, int iDB, int iIndex )
{
	bncs_string ssStr = bncs_string("%1,%2,%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.find(ssStr)<0) {
		ssl_RMChanges.append( ssStr );		
		if (bRMDatabaseTimerRunning==TRUE) KillTimer( hWndMain, DATABASE_TIMER_ID );
		// reset timer
		SetTimer( hWndMain, DATABASE_TIMER_ID, 350, NULL ); 
		bRMDatabaseTimerRunning = TRUE;
	}
} 

////////////////////////////////////////////////////////////////////////////////////////////
//
// strip quotes from info revertives 
//
void StripOutQuotes( LPSTR szStr ) 
{
	int iI, iLen = strlen( szStr );
	char szTmp[256]="";

	wsprintf(szTmp,"",NULL);
	if (iLen > 2) {
		for ( iI=0; iI < (iLen-2); iI++)
			szTmp[iI] = szStr[iI+1];
		szTmp[iI] = NULL; // terminate string
		iLen = strlen( szTmp );
	}
	strcpy( szStr, szTmp );	
}

//
//  FUNCTION: CSIClientNotify()
//
//  PURPOSE: Callback function for incoming CSI messages for client class
//
//  COMMENTS: the message is supplied in szMsg
LRESULT CSIClientNotify(extclient* pec, LPCSTR szMsg)
{

	UINT iParamCount = pec->getparamcount();
	LPSTR szParamRev = pec->getparam(0);
	LPSTR szDevNum = pec->getparam(1);
	LPSTR szSlot = pec->getparam(2);
	LPSTR szIndex = pec->getparam(3);
	LPSTR szContent = pec->getparam(4);

	char szRMData[MAX_BUFFER_STRING] = "";

	int iDevNum = atoi(szDevNum);
	int iDBNum = atoi(szSlot);

	// infodriver revs 
	int iSlotNum = atoi(szSlot);
	int iIndexNum = atoi(szIndex);
	int iContent = 0;
	if ((iParamCount > 4) && (strlen(szContent) > 0)) 
		iContent = atoi(szContent);  // if revertives are just a number;

	// router revs
	int iRtrDest = atoi( szSlot );
	int iRtrSource = atoi( szIndex );
	
	// gpi revs
	int iWhichGpi = atoi( szSlot );
	int iGPIState = atoi( szIndex );
		
	int iPrevSource=0, iPrevState=UNKNOWNVAL;
	int iButtonPressed=0, iWhichPanel=0;
	BOOL bChanged=FALSE;
	int iRevType = UNKNOWNVAL;
	
	UpdateCounters();
	switch (pec->getstate())
	{
	case REVTYPE_R:

		if ((bShowAllDebugMessages) && (!bAutomaticStarting)) Debug("CSIClient - Router Rev from dev %d, dest %d, src %d ", iDevNum, iRtrDest, iRtrSource);
		//
		StoreRouterRevertive(iDevNum, iRtrDest, iRtrSource, &iPrevSource);			// not in startup mode - process revs after all in first time -- faster startup
		// process changes based on new rev ( not if still the same )..
		if (iRtrSource != iPrevSource) {

			if (!bAutomaticStarting) {
				//
				ProcessRouterRevertive(iDevNum, iRtrDest);  // SDI or GFX revertive for dest assoc to multiviewer or mixer
				//
			}
			ProcessNextCommand(BNCS_COMMAND_RATE); // in event anything added to buffer 
		}
		break;

	case REVTYPE_I:
	{
					  if (bValidCollediaStatetoContinue == TRUE) {
						  if (iParamCount == 5) {
							  if ((bShowAllDebugMessages) && (!bAutomaticStarting)) Debug("CSIClient - Info Rev from dev %d, slot %d, state %s ", iDevNum, iSlotNum, szContent);
							  StripOutQuotes(szContent);
							  bncs_string ssInfoRev = szContent;
							  // check for pipe char ( from Kahuna mixer revs )
							  bncs_stringlist ssl = bncs_stringlist(ssInfoRev, '|');
							  if (ssl.count() > 1)
								  iContent = ssl[0].toInt();  // for revertives that have pipe delim list, take first element in list - ie could be Kahuna rev as list
							  else
								  iContent = atoi(szContent);  // for revertives that are just numbers; no pipe in list
							  // xxxx anything to do --- pti from rtr tieline manager ???
						  }
						  else
							  Debug("CSIClient - Incorrectly formed infodriver revertive from device %d ", iDevNum);
					  }
					  else
						  Debug("CSIClient - Automatic in INVALID or FAIL  State - info revertive ignored");
	}
	break;

	case DATABASECHANGE:
		if ((bValidCollediaStatetoContinue == TRUE) && (!bAutomaticStarting)) {
			if (ssl_AllRoutersIndices.find(iDevNum) >= 0) {
				if ((iDBNum == i_SourceName_DB) || (iDBNum == i_SoftName_DB)) {
					if (FindEntryInRecentRMList(iDevNum, iDBNum, iIndexNum) == FALSE) {
						//add most recent data to list of changed indexes for 591 - to ignore the 2nd RM
						AddEntryToRecentRMList(iDevNum, iDBNum, iIndexNum);

						//RM should wipe out slot contents override label if db2 changes for valid defined router 
						CRevsRouterData* pRtr = GetValidRouter(iDevNum);
						if ((iDBNum == i_SoftName_DB) && (pRtr)) {
							// check for defined umd offset - if none then there are no override labels for that router
							if (pRtr->getUMDOverrideStartSlot() > 0) {
								// check for perm umd label or not - by presence of $ at start of string
								char szLabel[MAX_BUFFER_STRING] = "";
								int iUMDInfoSlot = pRtr->getUMDOverrideStartSlot() + iIndexNum - 1;
								if ((iUMDInfoSlot > 0) && (iUMDInfoSlot < SLOT_OFFSET_AUX_UMD)) {
									eiExtInfoId->getslot(iUMDInfoSlot, szLabel);
									bncs_string ssLabel = bncs_string(szLabel);
									int iPos = ssLabel.contains('$');
									if (iPos < 1) {
										// no $ found so clear temp override label
										char szBlank[MAX_BUFFER_STRING] = "";
										eiExtInfoId->updateslot(iUMDInfoSlot, szBlank);
									}
								}
							}
						}

						if (bShowAllDebugMessages) Debug("CSIClient - Database change message - %s; device %d db %d indx %d ", szMsg, iDevNum, iDBNum, iIndexNum);
						ProcessRouterModifyForSourceChange(iDevNum, iIndexNum, FALSE);
					}
				}
			}
		}
	break;

	case REVTYPE_G:
		Debug("CSIClient - IGNORED GPI Revertive from dev (%d), gpi (%d), state %d", iDevNum, iWhichGpi, iGPIState);
		break;

	case STATUS:
		Debug("CSIClient - Status message is %s", szMsg);
		break;

	case DISCONNECTED:
		Debug("CSIClient - CSI has closed down - driver in FAIL  or  ERROR  state");
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS, " ERROR: Disconnected");
		eiExtInfoId->updateslot(COMM_STATUS_SLOT, "0");	 // error	
		bValidCollediaStatetoContinue = FALSE;
		PostMessage(hWndMain, WM_CLOSE, 0, 0);
		break;

		} // switch

		/* always return TRUE so CSI doesn't delete your client registration */
		return TRUE;
}


///////////////////////////////////////////////////////////////////////////////////////////
//
//  Infodriver Resilience
//
///////////////////////////////////////////////////////////////////////////////////////////
// 
//  Check infodriver status for resilience
//
// 
void CheckAndSetInfoResilience( void )
{

	// having connected to driver now then getmode for redundancy
	// get state of infodriver
	char szNM[32] = "", szCM[32] = "", szStr[64] = " ??unknown??";
	int iMode = eiExtInfoId->getmode(); // main info determines the status of all others -- to prevent mix occuring
	iOverallStateJustChanged = 0;

	// determine string for driver dialog window
	if ((iMode == INFO_RXMODE) || (iMode == IFMODE_TXRXINQ)) { // if still in rx
		strcpy(szStr, "RXonly - RESERVE");
		strcpy(szNM, "RXONLY");
	}
	if (iMode == INFO_TXRXMODE){
		strcpy(szStr, "Tx/Rx - MAIN OK");
		strcpy(szNM, "TX-RX");
	}

	if (iOverallTXRXModeStatus == INFO_RXMODE) strcpy(szCM, "RXONLY");
	if (iOverallTXRXModeStatus == INFO_TXRXMODE) strcpy(szCM, "TX-RX");

	if (iMode != iOverallTXRXModeStatus) {
		Debug("CheckInfoMode- new mode %s(%d) different to current %s(%d) -- changing status", szNM, iMode, szCM, iOverallTXRXModeStatus);
		switch (iMode) {
			case INFO_RXMODE: case IFMODE_TXRXINQ:
				strcpy(szStr, "RXonly - RESERVE");
				iOverallModeChangedOver = 10;
				iOverallTXRXModeStatus = INFO_RXMODE;
				if (eiExtInfoId->getmode() != INFO_RXMODE) {
					eiExtInfoId->setmode(INFO_RXMODE);
					eiExtInfoId->requestmode = TO_RXONLY;
				}
				Debug("UMD Automatic running in  RXONLY  mode ");
			break;

			case INFO_TXRXMODE:
				strcpy(szStr, "Tx/Rx - MAIN OK");
				iOverallModeChangedOver = 10;
				iOverallTXRXModeStatus = INFO_TXRXMODE;
				if (eiExtInfoId->getmode() != INFO_TXRXMODE) {
					eiExtInfoId->setmode(INFO_TXRXMODE);
					eiExtInfoId->requestmode = TO_TXRX;
				}
				eiExtInfoId->setslot(4092, "UMDT txrx on workstation %d ", iWorkstation);
				Debug("UMD Automatic running in  TX/RX  mode ");
			break;
		default:
			strcpy(szStr, " ??unknown??");
			iOverallTXRXModeStatus = IFMODE_NONE;
			Debug("UMD Automatic in an  UNKNOWN  infodriver mode ");
		}
	}
	SetDlgItemText(hWndDlg, IDC_INFO_STATUS, szStr);
	iNextResilienceProcessing = 47; // reset counter 

}


void Restart_CSI_NI_NO_Messages(void)
{
	if (eiExtInfoId) eiExtInfoId->requestmode = INFO_TXRXMODE;
	iOverallModeChangedOver = 0;
}


void ForceAutoIntoRXOnlyMode(void)
{
	iOverallTXRXModeStatus = UNKNOWNVAL;
	if (eiExtInfoId) {
		eiExtInfoId->setmode(INFO_RXMODE);					// force to rxonly -- so that resilient pair can take over
		eiExtInfoId->requestmode = TO_RXONLY;            // force other driver into tx immeadiately if running ??
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "FORCE RX");
	}
	iNextResilienceProcessing = 4; // reset counter 
	iOverallModeChangedOver = 3;
}


void ForceAutoIntoTXRXMode(void)
{
	iOverallTXRXModeStatus = UNKNOWNVAL;
	if (eiExtInfoId) {
		eiExtInfoId->setmode(INFO_TXRXMODE);
		eiExtInfoId->requestmode = TO_TXRX;
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Req go Tx/Rx");
	}
	iNextResilienceProcessing = 4; // reset counter 
	iOverallModeChangedOver = 3;
}






