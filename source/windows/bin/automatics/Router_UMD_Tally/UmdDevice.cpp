// UmdDevice.cpp: implementation of the CUmdDevice class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UmdDevice.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUmdDevice::CUmdDevice(bncs_string ssDevName, bncs_string ssDevType, int iDevType, int iDev, int iOffset, bncs_string ssLoc, bncs_string ssAltId )
{
	m_ssDeviceName = ssDevName;
	m_ssDeviceType = ssDevType.upper();
	m_iDeviceType = iDevType;
	m_ssDeviceLocation = ssLoc;
	m_ssAltDeviceId = ssAltId;

	iDriverDevice = iDev;
	iDeviceStartOffset = iOffset;
	i_TSLUMD_Address =0;

	m_iNumberUMDS = 0;
	m_iListBoxIndex = -1;

	m_iNumberUmdDataEntries = 0;

	cl_AllUmdData = new map<int, CUmdDataEntry* >;
	if (cl_AllUmdData == NULL) {
		char szDbg[256] = "";
		wsprintf(szDbg, "LoadInit -- MAJOR ERROR - failed to create map of all umds for device  %s \n", LPCSTR(ssDevName));
		OutputDebugString(szDbg);
	}

}



CUmdDevice::~CUmdDevice()
{
	// kill 'em all
	// fade to black -- but first empty maps to stop mem leaks
	if (cl_AllUmdData!=NULL) {
		while (cl_AllUmdData->begin() != cl_AllUmdData->end()) {
			map<int, CUmdDataEntry*>::iterator it = cl_AllUmdData->begin();
			if (it->second) delete it->second;
			cl_AllUmdData->erase(it);
		}
		delete cl_AllUmdData;
	}
}

/////////////////////////////////////////////////////////////////
//
// map functions

CUmdDataEntry* CUmdDevice::getUmdDataEntry(int iIndex)
{
	map<int, CUmdDataEntry*>::iterator it = cl_AllUmdData->find(iIndex);
	if (it !=cl_AllUmdData->end()) {  // ie the entry found 
			if (it->second) return it->second;
	}
	return NULL;
}


//////////////////////////////////////////////////////////////////

void CUmdDevice::storeUMDDetails( int iIndex, int iRtr, int iSrcDest, int iSrcDestType, bncs_string ssFixed )
{
	if (iIndex>0){
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt == NULL) {
			// need to create a new entry
			pEnt = new CUmdDataEntry(iIndex);
			if (pEnt != NULL) {
				pEnt->m_iUmd_Router = iRtr;
				pEnt->m_iUmd_Src_Dest = iSrcDest;
				pEnt->m_iUmd_SrcDest_Type = iSrcDestType;
				if (iSrcDestType == FIXED_UMD_TYPE) pEnt->m_ssCalcUmdName = ssFixed;
				// ??? is this method of determining number umds still required
				m_ssl_Sources_Destinations.append(bncs_string(iSrcDest));
				m_iNumberUMDS = m_ssl_Sources_Destinations.count();
				//
				cl_AllUmdData->insert(pair<int, CUmdDataEntry*>(iIndex, pEnt));
				m_iNumberUmdDataEntries++;
			}
			else 
				OutputDebugString("StoreUMDDetails -ERROR_ in creating umd data entry \n");
		}
		else 
			OutputDebugString("StoreUMDDetails -ERROR_ ENTRY ALREADY EXISTS \n");
	}
}


void CUmdDevice::storeCalculatedUmdName( int iIndex, bncs_string ssName )
{
	if ((iIndex>0)&&(iIndex<=m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) {
			pEnt->m_ssCalcUmdName = ssName;  
		}
		else
			OutputDebugString("storeCalculatedUmdName -ERROR_ in finding umd data entry \n");
	}
}

void CUmdDevice::storeUmdAuxLabel( int iIndex, bncs_string ssLabel )
{
	if ((iIndex>0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) {
			pEnt->m_ssAuxLabel1 = ssLabel;
		}
	}
}

void CUmdDevice::storeListBoxEntry( int iIndex )
{
	m_iListBoxIndex = iIndex;
}

int CUmdDevice::getListBoxEntry( void )
{ 
	return m_iListBoxIndex;
}

void CUmdDevice::getUMDDevice(bncs_string* ssDevName, bncs_string* ssDevType, bncs_string* ssLoc, bncs_string* ssAlt, int* iDevType, int* iNumberUmds)
{
	*ssDevName = m_ssDeviceName;
	*ssDevType = m_ssDeviceType;
	*ssLoc = m_ssDeviceLocation;
	*ssAlt = m_ssAltDeviceId;
	*iDevType = m_iDeviceType;
	*iNumberUmds = m_iNumberUMDS;
}

int CUmdDevice::getNumberOfUMDs( )
{
	if (m_iNumberUMDS != m_iNumberUmdDataEntries) {
		char szStr[256] = "";
		wsprintf(szStr, "WARN-NumberUmds %d does not match mapEntries %d for dev %s \n", m_iNumberUMDS, m_iNumberUmdDataEntries, LPCSTR(m_ssDeviceName));
		OutputDebugString(szStr);
	}
	return m_iNumberUMDS;
}


int CUmdDevice::getBNCSDriver( )
{
	return iDriverDevice;
}

int CUmdDevice::getDriverOffset( )
{
	return iDeviceStartOffset;
}


void CUmdDevice::setTSLAddress( int iAddr )
{
	i_TSLUMD_Address = iAddr;
}

int  CUmdDevice::getTSLAddress( void )
{
	return i_TSLUMD_Address;
}


int CUmdDevice::getIndexGivenSrcDest( int iSrcDest, int iType, int iRouter )
{
	if (iSrcDest>=0) {
		for (int idx=1;idx<=m_iNumberUMDS; idx++ ) {
			CUmdDataEntry* pEnt = getUmdDataEntry(idx);
			if (pEnt) {
				if ((pEnt->m_iUmd_Src_Dest == iSrcDest)
					&& (pEnt->m_iUmd_SrcDest_Type == iType)
					&& (pEnt->m_iUmd_Router == iRouter))
					return idx;
			}
		}
	}
	return UNKNOWNVAL;
}


int CUmdDevice::getUmdTypeAndSrcDestByIndex( int iIndex, int* iSrcDest )
{
	*iSrcDest=UNKNOWNVAL;
	if ((iIndex>0)&&(iIndex<=m_iNumberUMDS)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) {
			*iSrcDest = pEnt->m_iUmd_Src_Dest;
			return pEnt->m_iUmd_SrcDest_Type;
		}
	}
	return UNKNOWNVAL;
}


bncs_string CUmdDevice::getAllUmdDetails( int iIndex, int* iSrcDestType, int* iSrcDest, bncs_string *ssAuxLabel )
{
	*iSrcDest=UNKNOWNVAL;
	*iSrcDestType=UNKNOWNVAL;

	if ((iIndex>0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) {
			*iSrcDest = pEnt->m_iUmd_Src_Dest;
			*iSrcDestType = pEnt->m_iUmd_SrcDest_Type;
			*ssAuxLabel = pEnt->m_ssAuxLabel1;
			return pEnt->m_ssCalcUmdName;
		}
		else
			OutputDebugString("getallUmdDetails -ERROR_ in finding umd data entry \n");
	}
	return "";
}

int CUmdDevice::getUmdSrcDestRouter( int iIndex )
{
	if ((iIndex > 0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) {
			return pEnt->m_iUmd_Router;
		}
	}
	return 0;
}

int CUmdDevice::getUMDDevType( void )
{
	return m_iDeviceType;
}

bncs_string CUmdDevice::getCalcUmdText( int iIndex )
{
	if ((iIndex>0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) {
			return pEnt->m_ssCalcUmdName;
		}
	}
	return "";
}


bncs_string CUmdDevice::getAllSourceDestinations( void )
{
	return m_ssl_Sources_Destinations.toString(',');
}


void  CUmdDevice::setRecentLabelCommand( int iIndex, bncs_string ssCmd )
{
	if ((iIndex>0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) pEnt->m_ssLabelCommand = ssCmd;
	}
}

bncs_string  CUmdDevice::getRecentLabelCommand( int iIndex )
{
	if ((iIndex>0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) return pEnt->m_ssLabelCommand;
	}
	OutputDebugString(LPCSTR(bncs_string("getRecentLabelCommand -ERROR2_ in finding umd data entry index %1 max %2\n").arg(iIndex).arg(m_iNumberUmdDataEntries)));
	return "";
}

void  CUmdDevice::setRecentAuxCommand( int iIndex, bncs_string ssAuxCmd )
{
	if ((iIndex>0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) pEnt->m_ssAuxCommand = ssAuxCmd;
	}
}

bncs_string  CUmdDevice::getRecentAuxCommand( int iIndex )
{
	if ((iIndex>0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) return pEnt->m_ssAuxCommand;
	}
	return "";
}

bncs_string CUmdDevice::getUmdAuxLabel( int iIndex )
{
	if ((iIndex>0) && (iIndex <= m_iNumberUmdDataEntries)) {
		CUmdDataEntry* pEnt = getUmdDataEntry(iIndex);
		if (pEnt) return pEnt->m_ssAuxLabel1;
	}
	return "";
}

