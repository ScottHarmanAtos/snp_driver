// RevsRouterData.cpp: implementation of the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RevsRouterData.h"


////////////////////////////////////////////////////
// private internal functions for src/dest maps 
CRouterDataSrce* CRevsRouterData::GetSourceData(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		map<int, CRouterDataSrce*>::iterator it = cl_SrcRouterData.find(iSource);
		if (it != cl_SrcRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

CRouterDataDest* CRevsRouterData::GetDestinationData(int iDest)
{
	if ((iDest>0) && (iDest <= m_iMaxDestinations)) {
		map<int, CRouterDataDest*>::iterator it = cl_DestRouterData.find(iDest);
		if (it != cl_DestRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRevsRouterData::CRevsRouterData( int iDeviceNumber, int iDests, int iSrcs, int iRtrOffset )
{
	// initialise structure
	m_iRouterDeviceNumber=iDeviceNumber;
	m_iMaxDestinations=iDests;
	m_iMaxSources=iSrcs;
	m_iTielineRouterOffset = iRtrOffset;

	m_iStartUMDOverrideSlot = -1;  // -1 / 0 means no overrides for this router -- unless set with 1, 501, 1001 etc

	// create entries in maps for dests and sources
	// create entries in maps for dests and sources
	for (int iS = 1; iS <= m_iMaxSources; iS++) {
		CRouterDataSrce* pData = new CRouterDataSrce(iS);
		if (pData) { // add to map
			cl_SrcRouterData.insert(pair<int, CRouterDataSrce*>(iS, pData));
		}
	}
	// create entries in maps for dests and sources
	for (int iD = 1; iD <= m_iMaxDestinations; iD++) {
		CRouterDataDest* pData = new CRouterDataDest(iD);
		if (pData) { // add to map
			cl_DestRouterData.insert(pair<int, CRouterDataDest*>(iD, pData));
		}
	}
}

CRevsRouterData::~CRevsRouterData()
{
	//seek and destroy
	// seek and destroy
	while (cl_DestRouterData.begin() != cl_DestRouterData.end()) {
		map<int, CRouterDataDest*>::iterator it = cl_DestRouterData.begin();
		if (it->second) delete it->second;
		cl_DestRouterData.erase(it);
	}
	while (cl_SrcRouterData.begin() != cl_SrcRouterData.end()) {
		map<int, CRouterDataSrce*>::iterator it = cl_SrcRouterData.begin();
		if (it->second) delete it->second;
		cl_SrcRouterData.erase(it);
	}
}


BOOL CRevsRouterData::storeGRDRevertive( int iDestination, int iSource )
{
  // store given source from actual rev; any re-enterant src/dest calc then done  if required
	if (iSource>=-1) {
		if ((iDestination>0)&&(iDestination<=m_iMaxDestinations)) {
			// first unset prev dest from routed to list in sources
			// get from map
			CRouterDataDest* pDestData = GetDestinationData(iDestination);
			if (pDestData) {
				int iPrevSrc = pDestData->m_iRoutedSourceNumber;
				if ((iPrevSrc>0) && (iPrevSrc <= m_iMaxSources)) {
					CRouterDataSrce* pPreSrc = GetSourceData(iPrevSrc);
					if (pPreSrc) {
						int iPos = pPreSrc->m_ssRoutedToDest.find(iDestination);
						if (iPos >= 0) pPreSrc->m_ssRoutedToDest.deleteItem(iPos);
					}
				}
				// now store
				pDestData->m_iRoutedSourceNumber = iSource;
				// now update source table routed to list
				if ((iSource>0) && (iSource <= m_iMaxSources)) {
					CRouterDataSrce* pSrc = GetSourceData(iSource);
					if (pSrc) {
						int iPos = pSrc->m_ssRoutedToDest.find(iDestination); // check it is not already listed somehow
						if (iPos < 0) pSrc->m_ssRoutedToDest.append(iDestination);
					}
				}
				return TRUE;
			}
		}
	}
	return FALSE;
}


BOOL CRevsRouterData::storeGRDLockState( int iDestination, int iState )
{
    // store given source from actual rev; any re-enterant src/dest calc then done  if required
	if ((iDestination>0)&&(iDestination<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDestination);
		if (pDestData) {
			pDestData->m_iDestLockState = iState;
			return TRUE;
		}
	}
	return FALSE;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CRevsRouterData::getRouterNumber( void )
{
	return m_iRouterDeviceNumber;
}


int CRevsRouterData::getMaximumDestinations( void )
{
	return m_iMaxDestinations;
}


int CRevsRouterData::getMaximumSources( void )
{
	return m_iMaxSources;
}


int CRevsRouterData::getRoutedSourceForDestination( int iDestination )
{
  // return revertive source as  got from grd rev
	if ((iDestination>0)&&(iDestination<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDestination);
		if (pDestData) {
			return pDestData->m_iRoutedSourceNumber;
		}
	}
	return UNKNOWNVAL;
}


int CRevsRouterData::getGRDLockState( int iDestination )
{
 	if ((iDestination>0)&&(iDestination<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDestination);
		if (pDestData) {
			return pDestData->m_iDestLockState;
		}
	}
	return UNKNOWNVAL;
}

int CRevsRouterData::getTielineRouterOffset(void)
{
	return m_iTielineRouterOffset;
}


//////////////////////////////////////////////////////////////////////////
// lock data routines
//////////////////////////////////////////////////////////////////////////

void CRevsRouterData::setRouterLockDevice( int iLockDev, int iStartSlot )
{
	m_iRouterLockDevice = iLockDev;
	m_iStartLockInfoSlot = iStartSlot;
}

int CRevsRouterData::getRouterLockDevice( void )
{
	return m_iRouterLockDevice;
}

void CRevsRouterData::getRouterLockSlotRange( int *iStart, int *iEnd )
{
	*iStart = m_iStartLockInfoSlot;
	*iEnd = m_iStartLockInfoSlot+m_iMaxDestinations-1;
}

int CRevsRouterData::getDestinationLockState( int iDestination )
{
	// return destination panelid as got from grd rev
	if ((iDestination>0)&&(iDestination<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDestination);
		if (pDestData) {
			return pDestData->m_iDestLockState;
		}
	}
	return UNKNOWNVAL;
}


//////////////////////////////////////////////////////////////////////////
// linked umd device routines
//////////////////////////////////////////////////////////////////////////

void CRevsRouterData::setLinkedUmdDeviceforDest( int iDest, bncs_string ssUmdDev )
{
	if ((iDest>0)&&(iDest<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDest);
		if (pDestData) {
			if (pDestData->m_ssLinkedToUmdDevice.find(ssUmdDev) < 0)
				pDestData->m_ssLinkedToUmdDevice.append(ssUmdDev); 
		}
	}
}


void CRevsRouterData::setLinkedUmdDeviceforSrc( int iSrc, bncs_string ssUmdDev )
{
	if ((iSrc>0)&&(iSrc<=m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSrc);
		if (pSrc) {
			if (pSrc->m_ssLinkedToUmdDevice.find(ssUmdDev) < 0) pSrc->m_ssLinkedToUmdDevice.append(ssUmdDev); // not found so add
		}
	}
}

bncs_string CRevsRouterData::getLinkedUmdDevicesforDest( int iDest )
{
	if ((iDest>0)&&(iDest<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDest);
		if (pDestData) {
			if (pDestData->m_ssLinkedToUmdDevice.count() > 0)
				return pDestData->m_ssLinkedToUmdDevice.toString(',');
		}
	}
	return "";
}

int CRevsRouterData::getNumberLinkedDevicesforDest( int iDest )
{
	if ((iDest > 0) && (iDest <= m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDest);
		if (pDestData) {
			return pDestData->m_ssLinkedToUmdDevice.count();
		}
	}
	return 0;
}


bncs_string CRevsRouterData::getLinkedUmdDevicesforSrc( int iSrc )
{
	if ((iSrc>0)&&(iSrc<=m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSrc);
		if (pSrc) {
			if (pSrc->m_ssLinkedToUmdDevice.count() > 0) return pSrc->m_ssLinkedToUmdDevice.toString(',');
		}
	}
	return "";
}


int CRevsRouterData::getNumberLinkedDevicesforSrc( int iSrc )
{
	if ((iSrc > 0) && (iSrc <= m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSrc);
		if (pSrc) {
			return pSrc->m_ssLinkedToUmdDevice.count();
		}
	}
	return 0;
}


bncs_string CRevsRouterData::getAllDestinationsRoutedTo( int iSrc)
{
	if ((iSrc>0)&&(iSrc<=m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSrc);
		if (pSrc) {
			if (pSrc->m_ssRoutedToDest.count() > 0)
				return pSrc->m_ssRoutedToDest.toString(',');
		}
	}
	return "";
}


////////////////////////////////////////////////////////////////////////
// tielines

void CRevsRouterData::setDestinationTielineData( int iDest, int iToRtr, int iToSrc )
{
	if ((iDest>0)&&(iDest<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDest);
		if (pDestData) {
			pDestData->m_iTielineToRtr = iToRtr;
			pDestData->m_iTielineToSrc = iToSrc;
		}
	}
}


void CRevsRouterData::getDestinationTielineData( int iDest, int *iToRtr, int *iToSrc )
{
	*iToSrc=0;
	*iToRtr=0;
	if ((iDest>0)&&(iDest<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDest);
		if (pDestData) {
			*iToRtr = pDestData->m_iTielineToRtr;
			*iToSrc = pDestData->m_iTielineToSrc;
		}
	}	
}


BOOL CRevsRouterData::isDestinationAssocToTieline( int iDest )
{
	if ((iDest>0)&&(iDest<=m_iMaxDestinations)) {
		CRouterDataDest* pDestData = GetDestinationData(iDest);
		if (pDestData) {
			if ((pDestData->m_iTielineToRtr > 0) && (pDestData->m_iTielineToSrc > 0)) return TRUE;
		}
	}	
	return FALSE;
}


void CRevsRouterData::setSourceTielineData( int iSrc, int iFromRtr, int iFromDest )
{
	if ((iSrc>0)&&(iSrc<=m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSrc);
		if (pSrc) {
			pSrc->m_iTielineFromRtr = iFromRtr;
			pSrc->m_iTielineFromDest = iFromDest;
		}
	}
}


void CRevsRouterData::getSourceTielineData( int iSrc, int *iFromRtr, int *iFromDest )
{
	*iFromDest=0;
	*iFromRtr=0;
	if ((iSrc>0)&&(iSrc<=m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSrc);
		if (pSrc) {
			*iFromRtr = pSrc->m_iTielineFromRtr;
			*iFromDest = pSrc->m_iTielineFromDest;
		}
	}
}

BOOL CRevsRouterData::isSourceAssocToTieline(int iSrc)
{
	if ((iSrc>0) && (iSrc <= m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSrc);
		if (pSrc) {
			if ((pSrc->m_iTielineFromRtr>0) &&(pSrc->m_iTielineFromDest>0)) return TRUE;
		}
	}
	return FALSE;
}


bncs_string CRevsRouterData::isSourceRoutedToDestTieline( int iSrc )
{
	bncs_stringlist sslList=bncs_stringlist("",',');
	if ((iSrc>0)&&(iSrc<=m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSrc);
		if (pSrc) {
			if (pSrc->m_ssRoutedToDest.count() > 0) {
				for (int iD = 0; iD<pSrc->m_ssRoutedToDest.count(); iD++) {
					int iDest = pSrc->m_ssRoutedToDest[iD];
					if ((iDest>0) && (iDest<=m_iMaxDestinations)) {
						CRouterDataDest* pDest = GetDestinationData(iDest);
						if (pDest) {
							if ((pDest->m_iTielineToRtr>0) && (pDest->m_iTielineToSrc > 0)) {
								sslList.append(iDest);
							}
						}
					}
				}
			}
		}
	}
	return sslList.toString(',');
}


void CRevsRouterData::setUMDOverrideStartSlot(int iStartSlot)
{
	m_iStartUMDOverrideSlot = iStartSlot;
}


int CRevsRouterData::getUMDOverrideStartSlot(void)
{
	return m_iStartUMDOverrideSlot;
}


void CRevsRouterData::setAssociatedInfoRevs(int iSdiSrce, int iWhichAssoc, bncs_string ssRecKey)
{
	if ((iSdiSrce > 0) && (iSdiSrce <= m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSdiSrce);
		if (pSrc) {
			switch (iWhichAssoc) {
				case INFODRIVER_REC_TX_ALARM_STATUS: pSrc->m_ssAssoc_InfodriverRevs1 = ssRecKey; break;
				case INFODRIVER_REC_TX_ALARM_INHIBIT: pSrc->m_ssAssoc_InfodriverRevs2 = ssRecKey; break;
				case INFODRIVER_REC_TX_ALARM_COMMS: pSrc->m_ssAssoc_InfodriverRevs3 = ssRecKey; break;
			}
		}
	}
}


bncs_string CRevsRouterData::getAssociatedInfoRevs(int iSdiSrce, int iWhichAssoc)
{
	if ((iSdiSrce > 0) && (iSdiSrce <= m_iMaxSources)) {
		CRouterDataSrce* pSrc = GetSourceData(iSdiSrce);
		if (pSrc) {
			switch (iWhichAssoc) {
				case INFODRIVER_REC_TX_ALARM_STATUS: return pSrc->m_ssAssoc_InfodriverRevs1; break;
				case INFODRIVER_REC_TX_ALARM_INHIBIT: return pSrc->m_ssAssoc_InfodriverRevs2; break;
				case INFODRIVER_REC_TX_ALARM_COMMS: return pSrc->m_ssAssoc_InfodriverRevs3; break;
			}
		}
	}
	return "";
}
