// RevsRouterData.h: interface for the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
#define AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
	#include <windows.h>
	#include "UMD_Tally_Common.h"
	#include <bncs_string.h>
	#include <bncs_stringlist.h>
	#include "bncs_auto_helper.h"

	#include "RouterDataSrce.h"	
	#include "RouterDataDest.h"	


class CRevsRouterData  
{
private:
	int m_iRouterDeviceNumber;     // dev ini number
	int m_iRouterLockDevice;       // lock dev ini number
	int m_iStartLockInfoSlot;      // may not be 1, depends on Evertz router config
	int m_iMaxSources;             // db0 size
	int m_iMaxDestinations;        // db1 size

	int m_iTielineRouterOffset;    // from router xml file for offset within main tieline router

	int m_iStartUMDOverrideSlot;    // slot index for start of override umds for router ( main SDI this will be 1, others read in from umd_tally_autos.xml, eg 501 for second router etc
															// note - also AUX / 2nd line umds start from 2000 + this index by the way, 0 or -1 will mean no umd overrides defined for router
	bncs_stringlist ssl_AllDestsAssocToOutGoingTieline;
	bncs_stringlist ssl_AllSourcesAssocToInComingTieline;

	map<int, CRouterDataDest*> cl_DestRouterData;   // map for Destination data
	map<int, CRouterDataSrce*> cl_SrcRouterData;    // map for source data

	CRouterDataSrce* GetSourceData(int iSource);
	CRouterDataDest* GetDestinationData(int iDestination);

public:
	CRevsRouterData(int iDeviceNumber, int iDests, int iSrcs, int iRtrOffset);
	virtual ~CRevsRouterData();

	// router revertives 

	BOOL storeGRDRevertive( int iDestination, int iSource );  // store given source from actual rev; 
	BOOL storeGRDLockState( int iDestination, int iState );

	void setRouterLockDevice( int iLockDev, int iStartSlot );

	void setLinkedUmdDeviceforDest( int iDest, bncs_string ssUmdDev );  // list of devices linked to this dest 
	void setLinkedUmdDeviceforSrc( int iSrc, bncs_string ssUmdDev );  // list of devices linked to this src 

	int getRouterNumber( void );
	int getMaximumDestinations( void );
	int getMaximumSources( void );
	int getRouterLockDevice( void );
	int getDestinationLockState( int iDestination );
	void getRouterLockSlotRange( int *iStart, int *iEnd );
	int getGRDLockState( int iDestination );
	int getTielineRouterOffset(void);

	int getRoutedSourceForDestination( int iDestination );  // return revertive source as got from grd rev

	bncs_string getAllDestinationsRoutedTo( int iSrc);

	bncs_string getLinkedUmdDevicesforDest( int iDest );  // list of devices linked to this dest 
	int getNumberLinkedDevicesforDest( int iDest );

	bncs_string getLinkedUmdDevicesforSrc( int iSrc );  // list of devices linked to this src 
	int getNumberLinkedDevicesforSrc( int iDest );
	
	void setDestinationTielineData( int iDest, int iToRtr, int iToSrc );
	void getDestinationTielineData( int iDest, int *iToRtr, int *iToSrc );
	BOOL isDestinationAssocToTieline( int iDest );

	void setSourceTielineData( int iSrc, int iFromRtr, int iFromDest );
	void getSourceTielineData( int iSrc, int *iFromRtr, int *iFromDest );
	BOOL isSourceAssocToTieline(int iSrc);

	bncs_string isSourceRoutedToDestTieline( int iSrc );  // returns number of tielines routed to

	void setUMDOverrideStartSlot(int iOffset);
	int getUMDOverrideStartSlot();

	void setAssociatedInfoRevs(int iSdiSrce, int iWhichAssoc, bncs_string ssRecKey);
	bncs_string getAssociatedInfoRevs(int iSdiSrce, int iWhichAssoc);

};

#endif // !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
