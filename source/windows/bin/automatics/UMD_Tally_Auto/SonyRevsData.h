// SonyRevsData.h: interface for the CSonyRevsData class.  -  also holds gallery specifc data too
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SONYREVSDATA_H__E91EFE9A_005D_4168_900E_54541243E119__INCLUDED_)
#define AFX_SONYREVSDATA_H__E91EFE9A_005D_4168_900E_54541243E119__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
	//
	#include <windows.h>
	#include "UMD_Tally_Common.h"


class CSonyRevsData  
{
private:
	int m_iSonyMixerDeviceId;     // main infodriver for Sony driver -- GALLERY MIXER A..G
	int m_iSonyMixerOffset   ;     // any device offset from instances
	int m_iAssociatedGallery;     // gallery mixer is primary used with

	int m_iTalliesEnabledState;         // 1=disabled,  0, 2 or higher enabled per mixer / pcr
	
	struct SonyDataStates {
		int iSony_RevData;
		int iAssociatedIndex1;      // sdi or package index -- but considered package as default
		int iAssociatedType1;        // src or dest package or sdi_rtr type  - dest pkg as default
		//int iSharedWithIndex;      // shared with - sdi or package index -- but considered package as default -- for those channels in shared galleries
		//int iSharedWithType;        // shared with - src or dest package or sdi_rtr type  - dest pkg as default
		bncs_string ssMxrSrcName;    // source name from mixer infodriver
		//
	};

	struct SonyDataStates  stMixerRev[MAXMIXERENTRIES];

	// below added for version 1.1 -- gallery specific 

	// unique monitor for mixer TX output -- adding "eggbox" functionality
	bncs_string ss_MON_TX_Instance;   // tsl instance device for umd under TX monitor associated to mixer
	int i_MON_TX_dev;
	int i_MON_TX_Offset;
	// unique monitor for SCR TX output - in pcr a,b,c only
	bncs_string ss_SCR_TX_Instance;   // tsl instance device for umd under SCR TX monitor associated to mixer
	int i_SCR_TX_dev;
	int i_SCR_TX_Offset;

	int i_LowestOnAir_MixerChannel;     // lowest on air index from all sony states
	bncs_string ss_TX_UMD_Text;         // current text displaying on TX mon umd for lowest on air channel
	BOOL bEggboxTimerRunning;

	bncs_string ss_ECUT_Instance;       // snell instance device for ecut or not for gallery that mixer linked to
	int i_ECUT_dev;
	int i_ECUT_Offset;
	int i_ECUT_GalleryState;            // mixer (0) or ECUT (1)

	int i_ECUT_Destination_Package;     // ecut dest package for gallery


public:
	CSonyRevsData( int iSonyBaseInfo, int iDriverOffset, int iAssocGallery );
	virtual ~CSonyRevsData();

	// sony mixer specific

	int getMixerDeviceId(void);
	int getMixerDeviceOffset(void);
	int getMixerGallery( void );

	void storeMixerAssocIndx( int iGPI, int iIndx1, int iType );
	int getMixerAssocIndx( int iGPI, int *iType );  // or packages

	void storeMixerSourceName( int iGPI, bncs_string ssName );
	bncs_string getMixerSourceName( int iGPI );

	void storeAssocFriends( int iGPI, bncs_string ssFr );
	bncs_string getAssocFriends( int iGPI );

	void storeMixerState( int iGPI, int iState );
	int getMixerState( int iGPI );

	bncs_string getallOnAirChannels( void );
	bncs_string getAnyOnAirSrcPackages( void );   // me banks etc
	bncs_string getallOnAirDestPackages( void );

	// gallery specific

	void store_MON_TX_Device( bncs_string ssInstance, int iDevice, int iOffset );
	void store_SCR_TX_Device( bncs_string ssInstance, int iDevice, int iOffset ); // only applicable for A, B and C

	int get_MON_TX_Device( void );   // returns bncs device number
	int get_MON_TX_Offset( void );   // returns bncs offset
	int get_SCR_TX_Device( void );   // returns bncs device number
	int get_SCR_TX_Offset( void );   // returns bncs offset

	int get_Lowest_MixerTX_OnAir(void);
	int get_Lowest_MixerPreset_OnAir(void);

	void store_MixerTX_UMDLabel( bncs_string ssUMDText );
	bncs_string get_MixerTX_UMDLabel( void );

	void set_EggboxTimerState( BOOL bState );
	BOOL get_EggboxTimerState( void );

	void store_ECUT_Settings( bncs_string ssInstance, int iDevice, int iOffset, int iEcutDestPkg );
	void store_ECUT_Status( int iState );
	int get_ECUT_Device( void );   // returns bncs device number
	int get_ECUT_Offset( void );   // returns bncs offset
	int get_ECUT_Status( void );
	int get_ECUT_Destination_Package( void );

	void set_Tallies_Enabled_Status( int iNewStatus );
	int get_Tallies_Enabled_Status();

	};

#endif // !defined(AFX_SONYREVSDATA_H__E91EFE9A_005D_4168_900E_54541243E119__INCLUDED_)
