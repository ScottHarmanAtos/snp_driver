// Include in all files to enable debug and status dialog updates


/* max size of buffer for data from string resource */
#define		MAX_LOADSTRING	100

/* buffer constant for strings */
#define		MAX_AUTO_BUFFER_STRING 270

#define		MAX_UMD_TEXT      32              
#define     SECOND_UMD_OFFSET 5   // slots 6 for 2nd umds     in tsl v5 driver -- if ever implemented     

#define		LEFT_TALLY 1        // red
#define		RIGHT_TALLY 2       // grn
#define		THIRD_TALLY 3       // 3rd level und tally

#define     PRIMARY_UMD   1
#define     SECONDARY_UMD 2

#define     TALLY_OFF 0     // red tally by default
#define     TALLY_ON  1     // red
#define     TALLY_GRN_OFF 3 // but right tallies are green - so needed ??
#define     TALLY_GRN_ON  4 // but right tallies are green - so needed ??

#define     TSL_CUE_ALLOFF   0
#define     TSL_CUE_LEFTON   1
#define     TSL_CUE_RIGHTON  2
#define     TSL_CUE_BOTHON   3
#define     TSL_CUE_TEXTON   3

#define		ROUTERCOMMAND   1
#define		INFODRVCOMMAND  2
#define		GPIOCOMMAND     3
#define		DATABASECOMMAND 4
#define		AUTOINFOREVERTIVE 5

#define		UNKNOWNVAL -1

#define		BNCS_COMMAND_RATE  20 // 100 //30 //5 //10          // number commands sent out per iteration of command timer

#define STRINGSEQUAL 0

#define AUTO_PACK_RTR_INFO                       1

#define SOURCE_PACKAGE 1
#define DESTINATION_PACKAGE 2

#define SOURCE_TYPE 1
#define DESTINATION_TYPE 2

#define PKGR_ROUTER_TYPE 1
#define PKGR_DEST_STAT_TYPE 2

// UMD and TALLY types
#define     FIXED_UMD_TYPE          1             // static umd
#define     SOURCE_UMD_TYPE      2             // linked to associated packages
#define     DEST_UMD_TYPE            3             // dest package verbatim 
#define     DEST_PREPEND_TYPE    4             // dest package prepend version
#define     DEST_CAMERA_TYPE      5            // dest package ammended rules for cameras 
#define     TX_MON_UMD_TYPE      6            // TX MON eggbox umd style
#define     PV_MON_UMD_TYPE      7            // Preview Mon eggbox for NEXT from Kahuna umd style

#define     SRCE_MAIN_SDI_INDEX	8	     // umds-tallies linked to src/dest on a defined SDI rtr rather than packages or the Nevion live core router
#define     DEST_MAIN_SDI_INDEX  9      

#define		MAXMIXERENTRIES        201  // usually 1..700  for all potential tallies per defined kahuna tally block  -- 1--200 for just inputs, 620-684 for outputs

// umd device types
#define     TAG_MV                             0        // as default for Discovery UMD T
#define     TSL_UMD_TYPE_16         1
#define     TSL_V5_DRIVER_10        2
#define      KALEIDO_MVIP               3

#define    SLOT_OFFSET_TAG_TSLV5 10         // as per instances for #umds entries per mv for tag and tsl5 inputs
#define    SLOT_OFFSET_KX_MV 100     // as per instances for #umds entries per mv
#define    SLOT_OFFSET_BM_MV 100     // as per instances for #umds entries per mv

#define	   TAG_UMD_BASE_OFFSET 400

// packager database usage
#define DB_SOURCE_NAME 0
#define DB_SOURCE_TITLE 2
#define DB_SOURCE_MCR_UMD   9
#define DB_SOURCE_PCR_UMD   10
#define DB_SOURCE_VID_LVLS   11

#define DB_DESTINATION_NAME 1
#define DB_DESTINATION_MCR2 3
#define DB_DESTINATION_AUDIOMASK 5
#define DB_DESTINATION_DEFINITION 7

// databases for umd names from packager and router databases
#define		RTR_SOURCE_NAME_DB  0			// default values -- 
#define		RTR_DEST_NAME_DB  1			// default values -- 

// defined Video Levels for packages
#define VIDEO_NONE 0
#define LEVEL_HD        1
#define LEVEL_UHD     2
#define LEVEL_SPARE1 3        
#define LEVEL_SPARE2 4       // 3 4 5 6  are spare for use if needed
#define LEVEL_SPARE3 5
#define LEVEL_SPARE4 6
#define MAX_LEVELS    7

#define DB_BUTTON_MAPPING 8

#define TAB_CHAR 0x09

#define SDI_STATUS_NONE  0
#define SDI_STATUS_EXPECTED  1
#define SDI_STATUS_UNEXPECTED   2

#define REV_CCU_ASS_TYPE 1            // eg slot 12   from ccu auto infodriver
#define REV_CCU_PTI_TYPE  2            // eg slot 13
#define REV_CAM_CCU_TYPE 3          // eg slot 1011
#define REV_CAM_ASS_TYPE 4           // eg slot 1012   from ccu auto infodriver

#define REV_CAM_FREE  0
#define REV_CAM_ROUTED  1
#define REV_CAM_ASSIGNED  2

#define MAX_CCU_ASSIGNMENT   20    // was 20 ccus added 3 remote cams

#define MAX_AREAS_WITH_CAMS        5     // pcr 0,2a, 2b, fma, other
#define MAX_CAMS_PER_AREA			20

#define CCU_SLOT_OFFSET              10
#define CCU_PTI_OFFSET               3

#define CCU_PCR1_START_SLOT 1000
#define CCU_PCR2_START_SLOT 2000
#define CCU_PCR3_START_SLOT 3000
#define CCU_PCR4_START_SLOT 3500
#define CCU_OTH_START_SLOT 3700

// to distinguish special types of packages
// for special processing -- often virtuals ....

#define PCR_CAM_PKG 1
#define PCR_OS_PKG  2
#define PCR_EVS_PKG 3

// databases for friends etc -- similar to that used in sky...

#define DB_LINES_A  2
#define DB_LINES_B  3
#define DB_MAIN_RTR 4
#define DB_PACKAGER 5

#define SRCE_TYPE 1
#define DEST_TYPE 2


// kahuna specifics
#define		KAHUNA_MIXER_INPUTS_DB   4
#define		KAHUNA_MIXER_OUTPUTS_DB  5

#define     KAHUNA_MIXER_MAXLEVELS  33  // tally levels possible from mixer  -  0..30 0=now, 1=next, 2..32 are 30 iso levels
#define     KAHUNA_LEVEL_NOW  0
#define     KAHUNA_LEVEL_NEXT 1
#define     KAHUNA_LEVEL_ISOBASE 1      // add to this which iso 1..30 to get iso level value
#define     KAHUNA_LEVEL_ISO1 2
#define     KAHUNA_LEVEL_ISO30 32

#define MIXER_TALLIES_ENABLED  0
#define MIXER_TALLIES_DISABLED 1


/// Nevion router levels
#define NEVION_VIDEO_SDHD 1
#define NEVION_VIDEO_UHD   2
#define NEVION_VIDEO_ANC    3
#define NEVION_VIDEO_XSJPG 4

// area types
#define MV_AREA_PCR 1
#define MV_AREA_MCR 2
#define MV_AREA_MFR 3

// slot alloc for areas
// slot 11 to 19 will be area 1 - pcr1,  area 2 uses slots 21-29  etc - used to assign mixer instance and division - for dynamic assignment
#define SLOT_AREAS_START 10
#define SLOTS_PER_AREA   10

#define AUTO_AREA_LDC 1
#define AUTO_AREA_NHN 2