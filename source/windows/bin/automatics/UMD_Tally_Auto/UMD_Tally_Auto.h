//////////////////////////////////////
// header file for UMD_Tally_Auto project //
//////////////////////////////////////

#if !defined(AFX_UMD_TALLY_AUTO_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
#define AFX_UMD_TALLY_AUTO_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4018 4786 4996 4244)
//
#include "resource.h"
#include "UMD_Tally_Common.h"


// TODO:	Insert additional constants, function prototypes etc. in this file
/* max number of items in listbox */
#define		MAX_LB_ITEMS	1500

#define		APP_WIDTH    1160
#define		APP_HEIGHT	1040

#define		INFO_DISCONNECTED  0	// infodriver not there or whatever
#define		INFO_RXMODE               1		// rx mode only - dual redundancy
#define		INFO_TXRXMODE          2		// connected, and in true rxtx mode
#define		INFO_TXRXINQ 4

// defined colours used in app
#define  COL_BLACK  RGB(0,0,0)
#define  COL_WHITE RGB(255,255,255)

#define  COL_DK_RED RGB(127,0,0)
#define  COL_DK_GRN RGB(0,127,0)
#define  COL_DK_BLUE RGB(0,0,80)
#define  COL_DK_ORANGE RGB(128,64,0)

#define  COL_LT_RED RGB(255,0,0)
#define  COL_LT_GRN RGB(0,255,0)
#define  COL_LT_BLUE RGB(100,100,255)
#define  COL_LT_ORANGE RGB(255,128,0)

#define  COL_YELLOW RGB(255,255,0)
#define  COL_TURQ RGB(180,180,210)

#define ONE_SECOND_TIMER_ID		1	 // could be used to relook for comms port / infodrivers -- for more intelligent driver
#define COMMAND_TIMER_ID		2							// 
#define STARTUP_TIMER_ID		3							// 
#define DATABASE_TIMER_ID	    4							// 
#define FORCETXRX_TIMER_ID      7					// 

// for eggbox slugs
#define GALLERY_EGGBOX_TIMER   10   // base index to which is added gallery number 1..7

#define ONE_SECOND_TIMER_INTERVAL	 1000 // 1.5 sec timer used for housekeeping - mainly for re connects to info driver, comport connection failures, device interrogation 
#define COMMAND_TIMER_INTERVAL		  100 // 0.1 sec interval on commands being sent from the fifobuffer 
#define EGGBOX_TIMER_INTERVAL		   30 // quarter sec interval on commands being sent to eggox umds to allow for going up mixer  
#define STARTUP_TIMER_INTERVAL		4000 // 2sec interval for polling each command out to pkgr/router/mixers etc
#define TENSECONDS								10  
#define SIXTYSECONDS							60  

// slot usage -- defined in device type for UMD Tally auto now

#define COMM_STATUS_SLOT		4095

/* main.cpp */
/* prototypes for main functionality */
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT				InitApp(HWND);
void				CloseApp(HWND);

void				LoadIni(void);
char *				getBNCS_File_Path( void );    // new for v4.2 of wizard - assigns the global szBNCS_File_Path
char*				r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite);
void				w_p(char* file, char* section, char* entry, char* setting);
int					getWS(void);

void				Debug(LPCSTR szFmt, ...);
void				Log(LPCSTR szDate);

int				SplitString(char *string, char *delim, UINT count, char **outarray );
void			InitStatusListboxDialog( void );  // helper function to initialise listbox on app dialog
BOOL			SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight);


/* UMD_Tally_Auto.cpp /*

/* prototypes for the notification functions */
void UpdateCounters(void);
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot);
LRESULT CSIClientNotify(extclient* pec,LPCSTR szMsg);

void ProcessNextCommand( int iRateRevs );
BOOL LoadAllInitialisationData( void );
void ProcessFirstPass_Logic( void );

BOOL AutomaticRegistrations(void);
void PollAutomaticDrivers(void);
void DisplayListviewsPanels( void );
void VersionInfo();

void CheckAndSetInfoResilience(void);
void ForceAutoIntoRXOnlyMode(void);
void ForceAutoIntoTXRXMode(void);
void Restart_CSI_NI_NO_Messages(void);

void ClearCommandQueue( void );
void RefeshUMDDevice( int iWhichDevice );
void DisplayUmdDeviceData();

void SendMixerTXUmdLabel( int iWhichGallery );

void ProcLabelsForGivenDevice( CUmdDevice* pUmd );
void ProcessLabelsForAllDevices( void );    //calc umd for all labels - called at startup
void ProcessLabelsForOutKilterList(void);
void CheckAllUmdRouting( ); // called at startup to verify state of video routes, especially for those mv inputs following dests or packages 

void ProcAllTalliesForGivenDevice( CUmdDevice* pUmd );
void ProcessTalliesForAllDevices( void );    //set umd tally lights  - called at startup
void SendTestTally( int iIndex, int iTally );
void ProcessAllRegionalCameraTallies(BOOL bForce);
void ProcessARegionTallies(int iRegion, BOOL bForce, int iDestInQ, int iPrevSrce);
int TraceRegionalPrimaryRouterSource(int iStartRtr, int iStartSrc);

CDestinationPackages* GetDestinationPackage( int iIndex );
CSourcePackages* GetSourcePackage( int iIndex );

void DisplaySourcePackageData();
void DisplayDestinationPackageData();

/*function to create nice new paint brushes*/
void CreateBrushes();
/*function to clean paint brushes and put them away*/
void ReleaseBrushes();
void FillTheLabel( int iWhichButton, int iColour, char* szText  );
bncs_string ExtractStringfromDevIniFile( int iDev, int iEntry, int iDbFile );

CRevsRouterData* GetNevionVideoRouter();             // HD router associated to packager  -- first in list of all known routers
CRevsRouterData* GetValidRouter(int iWhichRouter);

CUmdDevice* GetValidUmdDevice(int iDev);

CStudioRevsData* GetMixerByKeyIndex(int iKeyNum);
CStudioRevsData* GetMixerByDevice(int iDevNum);

CArea_Gallery* GetAreaGallery_ByKey(int iAreaGallery);
CArea_Gallery* GetAreaGallery_ByName(bncs_string ssName);
CArea_Gallery* GetAreaGallery_ByInstance(bncs_string ssName);

CDeviceTypeConfig* GetDeviceTypeByName(bncs_string ssName);

#ifndef EXT
#define EXT extern
#endif


/////////////////////////////////////////////////////////////////////
// Global Variables for the UMD_Tally_Auto project
/////////////////////////////////////////////////////////////////////

EXT HINSTANCE hInst;						// current instance
EXT TCHAR szTitle[MAX_LOADSTRING];			// The title bar text
EXT TCHAR szWindowClass[MAX_LOADSTRING];	// The title bar text
EXT char szBNCS_File_Path[MAX_AUTO_BUFFER_STRING]; // global for Colledia system file path - assigned in getBNCS_File_Path, used in r_p and w_p
EXT char szBNCS_Log_File_Path[MAX_AUTO_BUFFER_STRING];  // global for crash log file path - assigned in getBNCS_File_Path
EXT char szAppName[ MAX_LOADSTRING];                     // application name used in crash log messages
EXT char szResult[MAX_AUTO_BUFFER_STRING];						// global for profile reads
EXT char szClientCmd[MAX_AUTO_BUFFER_STRING];						// global for commands via ecclient
EXT char szRMData[MAX_AUTO_BUFFER_STRING];        // global for RMs 
EXT char szDebug[1000];						// global strings data for debug msgs
EXT char szLogDbg[1100];

EXT BOOL fLog;								// log enable flag
EXT BOOL fDebug;							// debug enable flag
EXT BOOL fDebugHide;						// debug visible flag
EXT BOOL fDbgView;							   // debug view outputstring enable flag

EXT HWND hWndMain, hWndList;					// window handles, main and debug
EXT HWND hWndDlg;							// window handle for child dialog
EXT int iChildHeight;						// height of child dialog
EXT RECT rc;
EXT int iDevice;							// command line parameter = external driver #
EXT int iWorkstation;						// workstation number from CSI.INI
EXT ULONG lTXDEV;							// Device tx count
EXT ULONG lRXDEV;							// Device rx count
EXT ULONG lTXID;							// ID tx count
EXT ULONG lRXID;							// ID rx count

EXT bncs_string ssAutoInstance;   // auto instance passed in as parameter for startup - no more integers; get device id from given instance

// CLASSES used by auto
EXT extclient *ecCSIClient;				// a client connection class
EXT extinfo *eiExtInfoId;			// external infodriver for driver

EXT map<int, CDeviceTypeConfig*>* cl_UMDDeviceTypes;     // stores data from umd tally auto device type - key is slot number

EXT bncs_stringlist ssl_RouterInstances;          // list of instance names for known routers in this auto -- fcut, d3 etc
EXT bncs_stringlist ssl_AllRouters;                     // list of integer ( router device numbers ) keys into map

//  router maps
EXT int iIP_RouterVideoHD, iNumberIPRouterInfodrivers;	            // Nevion IP for video base device, and number of Infodrivers used for vision, audio and anc
EXT int iIP_RouterVideoUHD;											// Nevion UHD router,
EXT int iIP_RouterInfoDivider;									// to be in line with ip_router infodriver_divider value, default=4096, may be set to 4000

EXT map<int, CRevsRouterData*>* cl_AllRouters;    // int key is bncs device number of router in question

EXT int iNumberAreaGalleries;
EXT map<int, CArea_Gallery*>* cl_AllGalleries;    // int key to gallery / area - holds config for area, on air packages etc - new class for this auto

EXT int iNumberKahunaMixers;
EXT map <int, CStudioRevsData*>* cl_MixerRevertives;  // map for Kahuna Mixer related revertives and states required for tallies

EXT bncs_stringlist ssl_AllUMDDevices;                                  // list of keys into map
EXT map<bncs_string,  CUmdDevice*>* cl_AllUmdDevices;    // bncs_string is key is number of device, primarily identified by dev and slot number

EXT map<int, CSourcePackages*>* cl_SourcePackages;          // stored data and revertive details for packages
EXT map<int, CDestinationPackages*>* cl_DestinationPackages;

EXT int iNumberOfUserPackages;       // 64000 or  128000
EXT int iPackagerBarsSrcePkg;       // 64001 or 128001
EXT int iPackagerParkSrcePkg;       // 64002 or  128002

// 10 main infodrivers used in app - all will be kept in ststus sync with base info device number -- all TXRX or all RXONLY
EXT int iNumberOfInfodriversRequired;     // 196 infodrivers required at present ( 32*6 blocks of pkgr infos + 3 riedel )
EXT int iNumberInfosInBlock;              // default 32 infodrivers in a block ( 128000 )
EXT int iInfodriverDivisor;               // default is 4000 - ie pkg 4001 is next infodriver etc -- could be 4096 if push comes to shove

// PACKAGER globla vars
EXT int iPackagerAuto;                                          // base infodriver of packager
EXT int iPackagerAutoInfos[33];				         // 1..32 - for  array  of main  packager  infodrivers -- loaded at 
EXT int iPackagerDestStatInfos[33];				//  array  of dest status  packager  infodrivers 

EXT int i_Start_VirtualPackages;
EXT int iRecursionTraceCounter, iRegRtrTraceCounter;

EXT map<int, CInfodriverRevs*>* cl_CCU_Assignments;     // stores various info data - for CCU by info slot number

EXT queue<CCommand*> qCommands;                         // queue of commands to go to bncs drivers, routers, other devices 

EXT int iOverallTXRXModeStatus;			// overall mode -- based on base infodriver i.e gpi input driver
EXT BOOL bConnectedToInfoDriverOK;	// connected to the infodriver or not
EXT BOOL bValidCollediaStatetoContinue;
EXT BOOL bAutomaticStarting;
EXT BOOL bForceUMDUpdate; 
EXT BOOL bShowAllDebugMessages;
EXT int iStartupCounter;

EXT int iOneSecondPollTimerId;
EXT BOOL bOneSecondPollTimerRunning;
EXT int iNextResilienceProcessing;        // count down to zero -- check info tx/rx status 
EXT int iOverallStateJustChanged;
EXT int iOverallModeChangedOver;

EXT int iNextCommandTimerId;
EXT BOOL bNextCommandTimerRunning;
EXT BOOL bRMDatabaseTimerRunning;

EXT int iLabelAutoStatus; // used to distinguish diff cols for label

// auto globals
EXT int iNumberActiveUmdDevs;
EXT int iCurrentChosenDevice;
EXT bncs_string ssCurrentChosenDevice;
EXT int iChosenMVIndex;
EXT int iLeftT, iRightT, iThirdT;

EXT int iChosenSrcPkg;
EXT int iChosenDestPkg;
EXT int iCheckSDIDest;
EXT int iCurrentRegion;    // display of regional ccu data

EXT databaseMgr dbmPkgs;

EXT int i_Max_UMD_Length;
EXT bncs_string ss_UMD_Join; // configureable string to use between db4 and db2 portions of umd

EXT int iWhichAutoArea;
EXT bncs_string ssAutoAreaString;
EXT bncs_stringlist ssl_AutoRegions;    // list of associated regions for umdt instance

// list of dest pkgs requiring recalc of umd / tallies - arising from packager revs
EXT bncs_stringlist ssl_SrcePkgs_ToBeProcessed;  
EXT bncs_stringlist ssl_DestPkgs_ToBeProcessed;  

EXT bncs_stringlist ssl_TraceToSource;    // global stringlist of source packages filled for dest pkg when TraceSourcePackageVideo called

EXT int iCCU_AssignmentsInfo;             // ccu camera pcr assignments auto -- needed here ??? xxx
EXT int iGalleryStatesAuto;               // from IDS apparently


// rm cmds - store db,index for src / dest RM changes
EXT bncs_stringlist ssl_RMChanges;

EXT bncs_stringlist ssl_NoTalliesSrcePackages;

EXT int iEggbox_Timer_Interval;

EXT int i_SDI_Source_Black;
EXT int i_SDI_Source_Bars;

EXT int iFriendsDatabase;

/// stockholm / regional additions 

EXT int iNumberRegionals;
EXT map<int, CRegionals*>* cl_AllRegionals;    // int key to regional - holds config for area, on air packages etc - new class for this auto

EXT int iNumberGPIOs;
EXT map<int, CGPIODevice*>* cl_AllGPIO;    // int key to gpio / ccu eetc  - ties in / out with regionals etc 


///////////////////////////////////////////////////////////////////

EXT	HBRUSH hbrWindowsGrey;
EXT	HBRUSH hbrBlack;
EXT	HBRUSH hbrWhite;

EXT	HBRUSH hbrDarkRed;
EXT	HBRUSH hbrDarkGreen;
EXT	HBRUSH hbrDarkBlue;
EXT	HBRUSH hbrDarkOrange;

EXT	HBRUSH hbrYellow;
EXT	HBRUSH hbrTurq;

EXT	HBRUSH hbrLightRed;
EXT	HBRUSH hbrLightGreen;
EXT	HBRUSH hbrLightOrange;
EXT	HBRUSH hbrLightBlue;

EXT HBRUSH HBr, HBrOld;

EXT COLORREF iCurrentTextColour;
EXT COLORREF iCurrentBackgroundColour;
EXT HBRUSH iCurrentBrush;

#endif // !defined(AFX_UMD_TALLY_AUTO_H__6E1F4185_1C5F_11D4_82C5_00105AC8787D__INCLUDED_)
