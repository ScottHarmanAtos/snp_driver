// SonyRevsData.cpp: implementation of the CSonyRevsData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SonyRevsData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSonyRevsData::CSonyRevsData( int iSonyBaseInfo, int iDriverOffset, int iAssocGallery )
{
	m_iSonyMixerDeviceId = iSonyBaseInfo;   
	m_iSonyMixerOffset = iDriverOffset;
	m_iAssociatedGallery = iAssocGallery;
	m_iTalliesEnabledState = MIXER_TALLIES_ENABLED;

	// init arrarys
	for (int i=0; i<MAXMIXERENTRIES; i++) {
		stMixerRev[i].iAssociatedIndex1 = 0;   // pkg or sdi
		stMixerRev[i].iAssociatedType1 = 0;
		stMixerRev[i].iSony_RevData = 0;
		stMixerRev[i].ssMxrSrcName = "";
		//
	}

	// gallery
	i_LowestOnAir_MixerChannel = 0;
	i_MON_TX_dev = 0;
	i_MON_TX_Offset = 0;
	i_SCR_TX_dev = 0;
	i_SCR_TX_Offset = -1;
	ss_MON_TX_Instance = "";
	ss_SCR_TX_Instance = "";
	ss_TX_UMD_Text = "";
	bEggboxTimerRunning=FALSE;

	// ecut
	ss_ECUT_Instance="";
	i_ECUT_dev = 0;
	i_ECUT_Offset = 0;
	i_ECUT_GalleryState = 0;
	i_ECUT_Destination_Package = 0;

}

CSonyRevsData::~CSonyRevsData()
{
	// burn baby burn
}



int CSonyRevsData::getMixerDeviceId(void)
{
	return m_iSonyMixerDeviceId;
}

int CSonyRevsData::getMixerDeviceOffset(void)
{
	return m_iSonyMixerOffset;
}

int CSonyRevsData::getMixerGallery( void )
{
	return m_iAssociatedGallery;
}



void CSonyRevsData::storeMixerState( int iGPI, int iState )
{
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)) {
		stMixerRev[iGPI].iSony_RevData = iState;   
	}
	// now check / calculate lowest on air mixer state
	i_LowestOnAir_MixerChannel = 0;
	for (int iChan=1;iChan<MAXMIXERENTRIES;iChan++) {
		if (stMixerRev[iChan].iSony_RevData>0) {
			i_LowestOnAir_MixerChannel = iChan;
			// lowest channel onair found so exit
			return;
		}
	}
}



void CSonyRevsData::storeMixerAssocIndx( int iGPI, int iIndx1, int iType )
{
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)) {
		stMixerRev[iGPI].iAssociatedIndex1 = iIndx1;
		stMixerRev[iGPI].iAssociatedType1 = iType;
	}
}



int CSonyRevsData::getMixerState( int iGPI )
{
	// if tallies disabled for mixer then all "deemed" off air 
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)&&(m_iTalliesEnabledState==MIXER_TALLIES_ENABLED)) {
		return stMixerRev[iGPI].iSony_RevData;
	}
	return 0;
}


int CSonyRevsData::getMixerAssocIndx( int iGPI, int *iType  )
{
	*iType = 0;
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)) {	
		*iType = stMixerRev[iGPI].iAssociatedType1;
		return stMixerRev[iGPI].iAssociatedIndex1;
	}
	return UNKNOWNVAL;
}


bncs_string CSonyRevsData::getallOnAirChannels( void )
{
	bncs_stringlist ssl_Ret=bncs_stringlist("",',');
	// if tallies disabled for mixer then all is "deemed" off air 
	if (m_iTalliesEnabledState == MIXER_TALLIES_ENABLED) {
		for (int iC = 1; iC<MAXMIXERENTRIES; iC++) {
			if (stMixerRev[iC].iSony_RevData == 1)
				ssl_Ret.append(iC);
		}
	}
	return ssl_Ret.toString(',');
}


bncs_string CSonyRevsData::getAnyOnAirSrcPackages( void )
{
	bncs_stringlist ssl_Ret=bncs_stringlist("",',');
	// if tallies disabled for mixer then all is "deemed" off air 
	if (m_iTalliesEnabledState == MIXER_TALLIES_ENABLED) {
		if (i_ECUT_GalleryState == 0) {
			for (int iC = 1; iC<MAXMIXERENTRIES; iC++) {
				if ((stMixerRev[iC].iSony_RevData == 1) && (stMixerRev[iC].iAssociatedType1 == SOURCE_UMD_TYPE))
					ssl_Ret.append(stMixerRev[iC].iAssociatedIndex1);
			}
		}
	}
	return ssl_Ret.toString(',');
}


bncs_string CSonyRevsData::getallOnAirDestPackages( void )
{
	bncs_stringlist ssl_Ret=bncs_stringlist("",',');
	// if tallies disabled for mixer then all is "deemed" off air 
	if (m_iTalliesEnabledState == MIXER_TALLIES_ENABLED) {
		if (i_ECUT_GalleryState == 0) {
			for (int iC = 1; iC < MAXMIXERENTRIES; iC++) {
				if ((stMixerRev[iC].iSony_RevData == 1) && (stMixerRev[iC].iAssociatedType1 == DEST_UMD_TYPE))
					ssl_Ret.append(stMixerRev[iC].iAssociatedIndex1);
			}
		}
		else {
			// as in ecut - return the single dest pkg for ecut for gallery
			ssl_Ret.append(i_ECUT_Destination_Package);
		}
	}
	return ssl_Ret.toString(',');
}


void CSonyRevsData::store_MON_TX_Device( bncs_string ssInstance, int iDevice, int iOffset )
{
	ss_MON_TX_Instance = ssInstance;
	i_MON_TX_dev = iDevice;
	i_MON_TX_Offset = iOffset;
}


void CSonyRevsData::store_SCR_TX_Device( bncs_string ssInstance, int iDevice, int iOffset )
{
	ss_SCR_TX_Instance = ssInstance;
	i_SCR_TX_dev = iDevice;
	i_SCR_TX_Offset = iOffset;
}


void CSonyRevsData::store_MixerTX_UMDLabel( bncs_string ssUMDText )
{
	ss_TX_UMD_Text = ssUMDText;
}


int CSonyRevsData::get_MON_TX_Device( void )
{
	return i_MON_TX_dev;
}


int CSonyRevsData::get_MON_TX_Offset( void )
{
	return i_MON_TX_Offset;
}


int CSonyRevsData::get_SCR_TX_Device( void )
{
	return i_SCR_TX_dev;
}


int CSonyRevsData::get_SCR_TX_Offset( void )
{
	return i_SCR_TX_Offset;
}


int CSonyRevsData::get_Lowest_MixerTX_OnAir( void )
{
	return i_LowestOnAir_MixerChannel;
}


bncs_string CSonyRevsData::get_MixerTX_UMDLabel( void )
{
	return ss_TX_UMD_Text;
}

void CSonyRevsData::set_EggboxTimerState( BOOL bState )
{
	bEggboxTimerRunning=bState;
}

BOOL CSonyRevsData::get_EggboxTimerState( void )
{
	return bEggboxTimerRunning;
}




void CSonyRevsData::storeMixerSourceName( int iGPI, bncs_string ssName )
{
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)) {
		stMixerRev[iGPI].ssMxrSrcName = ssName;   
	}
}


bncs_string CSonyRevsData::getMixerSourceName( int iGPI )
{
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)) {
		return stMixerRev[iGPI].ssMxrSrcName;
	}
	return "";
}



// ECUT

void CSonyRevsData::store_ECUT_Settings( bncs_string ssInstance, int iDevice, int iOffset, int iEcutDestPkg )
{
	ss_ECUT_Instance = ssInstance;
	i_ECUT_dev = iDevice;
	i_ECUT_Offset = iOffset;
	i_ECUT_Destination_Package = iEcutDestPkg;
}

int CSonyRevsData::get_ECUT_Device( void )
{
	return i_ECUT_dev;
}


int CSonyRevsData::get_ECUT_Offset( void )
{
	return i_ECUT_Offset;
}


void CSonyRevsData::store_ECUT_Status( int iState )
{
	i_ECUT_GalleryState = iState;
}


int CSonyRevsData::get_ECUT_Status( void )
{
	return i_ECUT_GalleryState;
}


int CSonyRevsData::get_ECUT_Destination_Package( void )
{
	return i_ECUT_Destination_Package;
}


void CSonyRevsData::set_Tallies_Enabled_Status(int iNewStatus)
{
	if (iNewStatus == MIXER_TALLIES_DISABLED)
		m_iTalliesEnabledState = MIXER_TALLIES_DISABLED;
	else
		m_iTalliesEnabledState = MIXER_TALLIES_ENABLED;
}

int CSonyRevsData::get_Tallies_Enabled_Status()
{
	return m_iTalliesEnabledState;
}



