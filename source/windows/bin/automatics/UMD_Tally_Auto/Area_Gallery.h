
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "UMD_Tally_Common.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CArea_Gallery
{

	// area / gallery specific data 

public:
	CArea_Gallery(int iIndex, int iTypeCode, bncs_string ssGallInstance, bncs_string ssGallName);
	~CArea_Gallery();

	int iAreaGallery_Index;
	int iAreaTypeCode;
	bncs_string ssAreaInstance_Id;
	bncs_string ssAreaGallery_Name;

	bncs_string ssAreaGallery_SourcePackages;
	bncs_string ssAreaGallery_DestinationPackages;
	bncs_string ssAreaGallery_VirtualPackages;
	bncs_stringlist ssl_CameraGalleryVirtuals;

	bncs_string ss_TX_UMD_Text;         // eggbox text displaying on TX mon umd - for lowest on air channel from assigned mixer 

	bncs_string ssAssigned_MixerInstance;
	int iAssigned_LogicalSwitcher;

	// public runtime - calculated list of on air src packages ( vir + real ) FOR ASSIGNED mixer and switcher ONLY
	// if not assigned 
	bncs_stringlist ssl_ThusSrcPackages_OnAir;        // list of just sources (from packager)
	bncs_stringlist ssl_ThusSrcPackages_ISO;          // iso is on air - for GRN level tallies - from packager

	bncs_stringlist ssl_ThusSrcRtrSDI_OnAir;          // complex list of rtr|src,rtr|src etc for on air
	bncs_stringlist ssl_ThusSrcRtrSDI_ISO;            // complex list of rtr|src,rtr|src etc for ISO

};

