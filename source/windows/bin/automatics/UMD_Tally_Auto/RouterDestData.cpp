// RouterData.cpp: implementation of the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RouterDestData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRouterDestData::CRouterDestData( int iIndx )
{
	m_iMapIndex=iIndx;
	m_iRoutedIndex = 0;
	m_iPreviousIndex = 0;
	ss_RouterRevertive = "";

	m_iTracedGrd = 0;
	m_iTracedRing = 0;
	m_iTracedSource = 0;

	m_bAssocToMixer = FALSE;
	m_ssl_SDIIndxUsedInPackages = bncs_stringlist("", ',');
	m_ssl_LinkedToUmdDeviceAndIndex = bncs_stringlist("", '|');

	ss_Override_UMD_Name = "";
	bOverride_Perm = FALSE;
	iOverride_SlotIndex = 0;
}

CRouterDestData::~CRouterDestData()
{

}