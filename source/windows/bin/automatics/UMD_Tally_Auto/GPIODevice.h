// GPIODevice.h: interface for the CGPIODevice class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GPIODEVICE_H__03FEF6FA_B177_41C7_825D_9110301C2759__INCLUDED_)
#define AFX_GPIODEVICE_H__03FEF6FA_B177_41C7_825D_9110301C2759__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include "UMD_Tally_Common.h"


///   used to store gpio ccu based data 


class CGPIODevice  
{
public:
	CGPIODevice( int iMapKey );
	virtual ~CGPIODevice();

	int iRecordKey;                          // value stored in regionals record

	int iBNCS_GPIO_Device;			// bncs device id ( squids )
	int iBNCS_GPIO_Index;            // index from config + offset from instances
	int iBNCS_LastStateSent;        // fire and forget - just so as not to keep repeating command

	int iAssociated_Index;                       // package or rtr index or any number to tie it all together
	int iAssociated_Regional_Key;        // link into  regional record
};

#endif // !defined(AFX_GPIODEVICE_H__03FEF6FA_B177_41C7_825D_9110301C2759__INCLUDED_)
