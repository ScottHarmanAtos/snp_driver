#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include "UMD_Tally_Common.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

// class to store regional based data
// used first for stockholm

class CRegionals
{
public:
	CRegionals();
	~CRegionals();

	int iRegionalKey;
	bncs_string ssRegionalName;

	int iRegionalRouter;

	bncs_stringlist ssl_key_router_sources;
	bncs_stringlist ssl_key_router_wrap_pairs;                   // src|dest,
	bncs_stringlist ssl_key_router_destinations;
	bncs_stringlist ssl_key_packager_sources;                       // where destinations land in packager
	bncs_stringlist ssl_key_LinkedGPIORecords;                   // links to keys into the gpio map

	// some means of mapping packages to rtr dests - 1:1 ? or a method to dynamically associate across WAN from SDI router into packager ??
	bncs_stringlist ssl_dynamicWANAssocs;            // format "package|dest, pkg|dest, etc" -- tie 1:1 until other method determined xxx

	// some means of mapping rtr srcs to assoc gpios
	bncs_stringlist ssl_dynamicGPIOAssocs;         // format "source|gpio key, source|gpio key, etc" -- tie via config until other method determined xxx

	// functions to process thru the dynamic lists abd return data
	int getRouterDestinationLinkedtoPackage(int iSrcPkg);
	int getGPIOlinkedtoRouterSource(int iRtrSrc);

};

