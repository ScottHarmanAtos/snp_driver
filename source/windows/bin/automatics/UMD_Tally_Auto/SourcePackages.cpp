// SourcePackages.cpp: implementation of the CSourcePackages class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SourcePackages.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSourcePackages::CSourcePackages()
{
	iPackageIndex= UNKNOWNVAL;
	ss_PackageFixedTitle = "";
	ss_PackageUserTitle = "";
	ss_PCR_UMD_Names1 = "";
	ss_PCR_UMD_Names2= "";
	ss_PCR_UMD_Names3 = "";
	ss_PCR_UMD_Names4 = "";
	ss_MCR_UMD_Names = "";

	i_Device_Srce_HD=0;              // source device - XXXX section needs reworking for discovery if ever required
	for (int ii=0;ii<5;ii++) i_Device_Srce_UHD[ii] = 0;     // UHD=0  210, quadrants use 1..4    211..214
	i_Device_Srce_SpareLvl1=0; // 4th spare - another spare level if ever required
	i_Device_Srce_SpareLvl2=0;  // 5th spare - another spare level if ever required
	i_Device_Srce_SpareLvl3=0; // 6th spare - another spare level if ever required

	ssl_Pkg_routedTo = bncs_stringlist("", ','); // list of destination pkgs that have this src pkg routed to it only

	ssl_Assoc_MVDeviceAndIndex = bncs_stringlist("",'|');

	iLinkedAreaGallery = 0;
	iAssignedMixerDesk = 0;
	iAssignedMixerChannel = 0;
	
}

CSourcePackages::~CSourcePackages()
{

}
