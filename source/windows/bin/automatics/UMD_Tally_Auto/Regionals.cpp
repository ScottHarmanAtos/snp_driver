#include "Regionals.h"


CRegionals::CRegionals()
{

	iRegionalKey = 0;
	ssRegionalName = "";
	iRegionalRouter = 0;
	ssl_key_router_sources = bncs_stringlist("", ',');
	ssl_key_router_wrap_pairs = bncs_stringlist("", ',');
	ssl_key_router_destinations = bncs_stringlist("", ',');
	ssl_key_packager_sources = bncs_stringlist("", ',');
	ssl_key_LinkedGPIORecords = bncs_stringlist("", ',');

	ssl_dynamicWANAssocs = bncs_stringlist("", ',');
	ssl_dynamicGPIOAssocs = bncs_stringlist("", ',');

}


CRegionals::~CRegionals()
{
}


int CRegionals::getRouterDestinationLinkedtoPackage(int iSrcPkg)
{
	for (int ii = 0; ii < ssl_dynamicWANAssocs.count(); ii++) {
		bncs_stringlist ssl = bncs_stringlist(ssl_dynamicWANAssocs[ii], '|');
		if (ssl.count() == 2) {
			if (ssl[0].toInt() == iSrcPkg) {
				// return the associated rtr dest
				return ssl[1].toInt();
			}
		}
	}
	return UNKNOWNVAL;
}


int CRegionals::getGPIOlinkedtoRouterSource(int iRtrSrc)
{
	for (int ii = 0; ii < ssl_dynamicGPIOAssocs.count(); ii++) {
		bncs_stringlist ssl = bncs_stringlist(ssl_dynamicGPIOAssocs[ii], '|');
		if (ssl.count() == 2) {
			if (ssl[0].toInt() == iRtrSrc) {
				// return the assocated gpio record key
				return ssl[1].toInt();
			}
		}
	}
	return UNKNOWNVAL;

}
