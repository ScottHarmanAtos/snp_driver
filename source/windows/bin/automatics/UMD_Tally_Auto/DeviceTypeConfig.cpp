#include "DeviceTypeConfig.h"


CDeviceTypeConfig::CDeviceTypeConfig(int iSlotIndx, bncs_string ssName)
{
	iKeySlot = iSlotIndx;
	ssKeyName = ssName;
	ssClass="";
	ssAccess="";

	// other data 
	iAssocPackageOrIndex=0;
	ssAssocInstance="";
	ssAssocArea = "";
	iAssocDevice = 0;
	iAssocOffset=0; 

}


CDeviceTypeConfig::~CDeviceTypeConfig()
{
}
