// RouterData.h: interface for the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROUTERDESTDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
#define AFX_ROUTERDESTDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CRouterDestData  
{
private:

public:
	CRouterDestData( int iIndx );
	virtual ~CRouterDestData();

	int m_iMapIndex;
	int m_iRoutedIndex;   // store first index from router rev string -- will be primary source - usually video
	int m_iPreviousIndex;   // previous
	bncs_string ss_RouterRevertive;
	
	// from trace if on another ring - but if on same ring, no trace and will be same as primary source, ring will be 0
	int m_iTracedRing;
	int m_iTracedGrd;
	int m_iTracedSource;

	bncs_stringlist m_ssl_SDIIndxUsedInPackages;       //  list of packages using this video index
	bncs_stringlist m_ssl_LinkedToUmdDeviceAndIndex;  // list of devices assoc to rtr srce

	BOOL m_bAssocToMixer;           // true if dest linked to any mixer

	bncs_string ss_Override_UMD_Name;
	BOOL bOverride_Perm; 
	int iOverride_SlotIndex;   // infodriver slot used of override label
};

#endif // !defined(AFX_ROUTERDATA_H__50A57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
