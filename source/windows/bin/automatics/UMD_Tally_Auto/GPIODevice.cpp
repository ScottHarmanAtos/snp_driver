// GPIODevice.cpp: implementation of the CGPIODevice class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GPIODevice.h"


///   ******   NOTE ::: ONLY GP OUTPUTS STORED HERE *****

CGPIODevice::CGPIODevice( int iMapKey )
{
	iRecordKey = iMapKey;

	iBNCS_GPIO_Device = 0;
	iBNCS_GPIO_Index = 0;
	iBNCS_LastStateSent = 0;

	iAssociated_Index = 0;
	iAssociated_Regional_Key = 0;
		
}

CGPIODevice::~CGPIODevice()
{

}

