// UmdDevice.cpp: implementation of the CUmdDevice class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UmdDevice.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUmdDevice::CUmdDevice( bncs_string  ssDevName, int iDev, int iOffset, bncs_string ssLoc, bncs_string ssLabel )
{
	m_ssDeviceName = ssDevName;
	m_ssDeviceLabel = ssLabel;
	m_ssDeviceLocation = ssLoc;
	iDriverDevice = iDev;
	iDeviceStartOffset = iOffset;
	m_iDeviceType = 0;
	m_iNumberUmds = 0;
	m_iListBoxIndex = -1;
}

CUmdDevice::~CUmdDevice()
{
	// kill 'em all
	while (cl_AllUmdData.begin() != cl_AllUmdData.end()) {
		map<int, CUmdData*>::iterator it = cl_AllUmdData.begin();
		if (it->second) delete it->second;
		cl_AllUmdData.erase(it);
	}
}


////////////////////////////////////////////////////
// private internal functions for src/dest maps 

CUmdData* CUmdDevice::GetSpecificUmdData(int iIndex)
{
	map<int, CUmdData*>::iterator it = cl_AllUmdData.find(iIndex);
	if (it != cl_AllUmdData.end()) {  // was the entry found
		if (it->second)
			return it->second;
	}
	return NULL;
}


void CUmdDevice::createUMDDetails(int iIndex, int iSrcDestType, int iMVRtr, int iMVFeed, int iAreaRecord, int iAreaCode, bncs_string ssFixed)
{
	CUmdData* pData = new CUmdData(iIndex);
	if (pData) {
		pData->m_iUmd_Type = iSrcDestType;
		pData->m_iMV_RtrDev = iMVRtr;
		pData->m_iMV_Src_Dest = iMVFeed;
		pData->m_ssPrimaryUmdName = ssFixed;
		if (iIndex > m_iNumberUmds) m_iNumberUmds = iIndex;
		pData->m_iUMDAreaGallery = iAreaRecord;
		pData->m_iUMDAreaCode = iAreaCode;
		// add into mapp 
		cl_AllUmdData.insert(pair<int, CUmdData*>(iIndex, pData));
	}
}

void CUmdDevice::setDeviceType( int iType )
{
	m_iDeviceType = iType;
}

void CUmdDevice::setUMDDeviceArea( int iIndex, int iAreaRecord, int iAreaCode )
{
	CUmdData* pData = new CUmdData(iIndex);
	if (pData) {
		pData->m_iUMDAreaGallery = iAreaRecord;
		pData->m_iUMDAreaCode = iAreaCode;
	}
}

void CUmdDevice::setChangeUmdType(int iIndex, int iNewType)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) pData->m_iUmd_Type = iNewType;
}

void CUmdDevice::storePrimaryUmdName( int iIndex, bncs_string ssName )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) pData->m_ssPrimaryUmdName = ssName;  
}

void CUmdDevice::storeSecondUmdName(int iIndex, bncs_string ssName)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) pData->m_ssSecondUmdName = ssName;
}

void CUmdDevice::storeUmdComment(int iIndex, bncs_string ssStr)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) pData->m_ssComment = ssStr;
}

BOOL CUmdDevice::storeTallyState(int iIndex, int iLeftOrRight, int iState)
{
	int iPrevState=0;
	BOOL bChangedState=FALSE;
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		if (iLeftOrRight == LEFT_TALLY) {
			iPrevState = pData->m_iLeftTallyState;
			pData->m_iLeftTallyState = iState;
		}
		else if (iLeftOrRight == RIGHT_TALLY) {
			iPrevState = pData->m_iRightTallyState;
			pData->m_iRightTallyState = iState;
		}
		else if (iLeftOrRight == THIRD_TALLY) {
			iPrevState = pData->m_iThirdTallyState;
			pData->m_iThirdTallyState = iState;
		}
	}
	if (iState!=iPrevState) bChangedState=TRUE;
	return bChangedState;
}


void CUmdDevice::storeListBoxEntry( int iIndex )
{
	m_iListBoxIndex = iIndex;
}

int CUmdDevice::getListBoxEntry( void )
{ 
	return m_iListBoxIndex;
}

void CUmdDevice::getUMDDevice( bncs_string*  ssDevName, bncs_string* ssDevLabel, bncs_string* ssDevLoc, int* iNumberUmds )
{
	*ssDevName = m_ssDeviceName;
	*ssDevLabel = m_ssDeviceLabel;
	*ssDevLoc = m_ssDeviceLocation;
	*iNumberUmds = m_iNumberUmds;
}

int CUmdDevice::getNumberOfUMDs( )
{
	return m_iNumberUmds;
}

int CUmdDevice::getUMDDeviceType()
{
	return m_iDeviceType;
}

int CUmdDevice::getUMDDeviceAreaGallery(int iIndex)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		return pData->m_iUMDAreaGallery;
	}
	return UNKNOWNVAL;
}

int CUmdDevice::getUMDDeviceAreaCode(int iIndex)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		return pData->m_iUMDAreaCode;
	}
	return UNKNOWNVAL;
}

int CUmdDevice::getBNCSDriver()
{
	return iDriverDevice;
}

int CUmdDevice::getDriverOffset( )
{
	return iDeviceStartOffset;
}


int CUmdDevice::getUmdTypeGivenIndex( int iIndex )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		return pData->m_iUmd_Type;
	}
	return UNKNOWNVAL;
}


int CUmdDevice::getUmdSrcDestRouter(int iIndex)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		return pData->m_iMV_RtrDev;
	}
	return 0;
}


int CUmdDevice::getUMDSrceDestIndex(int iIndex)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		return pData->m_iMV_Src_Dest;
	}
	return 0;
}


int CUmdDevice::getUmdTypeAndPackageByIndex( int iIndex, int* iAssocPackage )
{
	*iAssocPackage=UNKNOWNVAL;
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		*iAssocPackage = pData->m_iMV_Src_Dest;
		return pData->m_iUmd_Type;
	}
	return UNKNOWNVAL;
}


int CUmdDevice::getUmdTypeAndSrcDestByIndex( int iIndex, int* iRtr, int* iSrcDest )
{
	*iRtr=0;
	*iSrcDest=0;
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		*iRtr = pData->m_iMV_RtrDev;
		*iSrcDest = pData->m_iMV_Src_Dest;
		return pData->m_iUmd_Type;
	}
	return UNKNOWNVAL;
}



bncs_string CUmdDevice::getAllUmdDetails( int iIndex, int* iSrcDestType, int* iRtrFeedMV, int* iFeedMV  )
{
	*iSrcDestType=UNKNOWNVAL;
	*iRtrFeedMV=0;
	*iFeedMV=0;

	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		*iSrcDestType = pData->m_iUmd_Type;
		*iRtrFeedMV = pData->m_iMV_RtrDev;
		*iFeedMV = pData->m_iMV_Src_Dest;
		return pData->m_ssPrimaryUmdName;
	}
	return "";
}


int CUmdDevice::getSrcDestLinkedToIndexAndDevice( int iIndex, int iWhichDevice )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		if (pData->m_iMV_RtrDev == iWhichDevice) 
			return pData->m_iMV_Src_Dest;
	}
		return UNKNOWNVAL;
}



bncs_string CUmdDevice::getPrimaryUmdText( int iIndex )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) 
		return pData->m_ssPrimaryUmdName;
	else
		return "";
}


bncs_string CUmdDevice::getSecondUmdText( int iIndex )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) 
		return pData->m_ssSecondUmdName;
	else
		return "";
}

bncs_string CUmdDevice::getUmdComment(int iIndex)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) 
		return pData->m_ssComment;
	else
		return "";
}

int CUmdDevice::getTallyState( int iIndex, int iLeftOrRight )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		if (iLeftOrRight == RIGHT_TALLY)
			return pData->m_iRightTallyState;
		else if (iLeftOrRight == THIRD_TALLY)
			return pData->m_iThirdTallyState;
		else
			return pData->m_iLeftTallyState;
	}
	return UNKNOWNVAL;
}


void  CUmdDevice::setRecentTallyCommand( int iIndex, bncs_string ssCmd, int iLeftOrRight )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		if (iLeftOrRight == RIGHT_TALLY)
			pData->m_ssTallyCommand2 = ssCmd;
		else if (iLeftOrRight == THIRD_TALLY)
			pData->m_ssTallyCommand3 = ssCmd;
		else
			pData->m_ssTallyCommand1 = ssCmd;
	}
}


bncs_string  CUmdDevice::getRecentTallyCommand( int iIndex, int iLeftOrRight )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		if (iLeftOrRight == RIGHT_TALLY)
			return pData->m_ssTallyCommand2;
		else if (iLeftOrRight == THIRD_TALLY)
			return pData->m_ssTallyCommand3;
		else
			return pData->m_ssTallyCommand1;
	}
	return "";
}


void  CUmdDevice::setRecentUmdCommand( int iIndex, bncs_string ssCmd, int iFirstOrSecond )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		if (iFirstOrSecond == 2)
			pData->m_ssUmdCommand2 = ssCmd;
		else
			pData->m_ssUmdCommand1 = ssCmd;
	}
}


bncs_string  CUmdDevice::getRecentUmdCommand( int iIndex, int iFirstOrSecond )
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		if (iFirstOrSecond == 2)
			return pData->m_ssUmdCommand2;
		else
			return pData->m_ssUmdCommand1;
	}
	return "";
}

///////////////////////////////////////

// internal functions 
bncs_string CUmdDevice::ParseFormatString(bncs_string ssInStr)
{
	bncs_string ssOut = "";
	// remove all but digits and : char
	char szString[MAX_AUTO_BUFFER_STRING] = "";
	strcpy(szString, LPCSTR(ssInStr));
	for (int ii = 0; ii < strlen(szString); ii++) {
		if ((szString[ii] >= '0') && (szString[ii] <= ':')) ssOut.append(szString[ii]);
	}
	return ssOut;
}

bncs_string CUmdDevice::ParseReplaceString(bncs_string ssInStr)
{
	bncs_string ssOut = "";
	// remove all but spaces, numbers or alphabetic chars
	char szString[MAX_AUTO_BUFFER_STRING] = "";
	strcpy(szString, LPCSTR(ssInStr));
	for (int ii = 0; ii < strlen(szString); ii++) {
		if ((szString[ii] == 32) ||
			((szString[ii]>=48)&&(szString[ii] <= 57)) ||
			  ((szString[ii] >= 65) && (szString[ii] <= 90)) ||
			  ((szString[ii] >= 97) && (szString[ii] <= 122))) ssOut.append(szString[ii]);
	}
	return ssOut;
}

//////////////////////////////////////////////////

void CUmdDevice::setUMDFormatRules(int iIndex, bncs_string ssRules, bncs_string ssArgs)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		// parse rules and use args remove all non integer chars except : or , - end up with x:y to easily split and use
		pData->m_ssl_umd_format = ParseFormatString(ssRules);
		pData->m_ssl_umd_args = ParseFormatString(ssArgs.replace(',',':'));
	}
}

bncs_string CUmdDevice::getUMDFormatRules(int iIndex, bncs_string* ssargs)
{
	*ssargs = "";
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		*ssargs = pData->m_ssl_umd_args;
		return pData->m_ssl_umd_format;
	}
	return "";
}

void CUmdDevice::setUMDReplaceRules(int iIndex, bncs_string ssReplace, bncs_string ssArgs)
{
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		// parse the given strings
		pData->m_sstr_replace = ParseReplaceString(ssReplace);
		pData->m_sstr_repByThis = ParseReplaceString(ssArgs);
	}
}

bncs_string CUmdDevice::getUMDReplaceRules(int iIndex, bncs_string* ssrepbystr )
{
	*ssrepbystr = "";
	CUmdData* pData = GetSpecificUmdData(iIndex);
	if (pData) {
		*ssrepbystr = pData->m_sstr_repByThis;
		return pData->m_sstr_replace;
	}
	return "";
}

