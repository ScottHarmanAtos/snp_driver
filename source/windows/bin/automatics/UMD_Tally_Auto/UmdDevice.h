// UmdDevice.h: interface for the CUmdDevice class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UMDDEVICE_H__40CE825D_7AC9_4FEC_9674_6A51640C22A0__INCLUDED_)
#define AFX_UMDDEVICE_H__40CE825D_7AC9_4FEC_9674_6A51640C22A0__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
	#pragma warning(disable : 4786 4996 4018)
	//
	#include <windows.h>
	#include "UMD_Tally_Common.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

	#include "UmdData.h"	

class CUmdDevice  
{
private:
	
	bncs_string m_ssDeviceName;   // key into record  -- same key used for common xml data
	bncs_string m_ssDeviceLabel;     // label info on mv use
	bncs_string m_ssDeviceLocation;     // location of mv

	int m_iDeviceType;                    // device type : TSL UMD,  Evertz Multiviewer
	int m_iListBoxIndex;

	int iDriverDevice;
	int iDeviceStartOffset;
	
	int m_iNumberUmds;
	map<int, CUmdData*> cl_AllUmdData;   // map for Destination data
	CUmdData* GetSpecificUmdData(int iUmdIndex);

	bncs_string ParseFormatString(bncs_string ssInStr);
	bncs_string ParseReplaceString(bncs_string ssInStr);

public:
	CUmdDevice( bncs_string  ssDevName, int iDev, int iOffset, bncs_string ssLoc, bncs_string ssLabel );
	virtual ~CUmdDevice();
	void createUMDDetails(int iIndex, int iSrcDestType, int iMVRtr, int iMVFeed, int iAreaRecord, int iAreaCode, bncs_string ssFixed = "");
	void storePrimaryUmdName( int iIndex, bncs_string ssName );
	void storeSecondUmdName( int iIndex, bncs_string ssName );
	void storeUmdComment(int iIndex, bncs_string ssStr);
	void setDeviceType(int iType);
	void setChangeUmdType(int iIndex, int iNewType);

	BOOL storeTallyState( int iIndex, int iLeftOrRight, int iState );
	void storeTallyRevertive( int iIndex, int iRevState );
	int getTallyRevertive( int iIndex );

	void getUMDDevice( bncs_string*  ssDevName, bncs_string* ssDevLabel, bncs_string* ssDevLoc, int* iNumberUmds );
	int getNumberOfUMDs( void );
	int getBNCSDriver( void );
	int getDriverOffset( void );

	bncs_string getAllSourceDestinations( void );
	bncs_string getAllAssociatedPackages( void );

	int getIndexGivenPackage( int iPackage );
	int getIndexGivenSrcDest( int iRtr, int iSrcDest );

	int getUMDDeviceType( void );
	int getUmdTypeGivenIndex( int iIndex );

	int getUmdTypeAndPackageByIndex( int iIndex, int* iAssocPackage );
	int getUmdTypeAndSrcDestByIndex( int iIndex, int* iRtr, int* iSrcDest );
	
	bncs_string getAllUmdDetails( int iIndex, int* iSrcDestType, int* iMVRtr, int* iMVFeed );
	int getSrcDestLinkedToIndexAndDevice(int iIndex, int iWhichDevice);
	int getUmdSrcDestRouter(int iIndex);
	int getUMDSrceDestIndex(int iIndex);
	bncs_string getPrimaryUmdText( int iIndex );
	bncs_string getSecondUmdText( int iIndex );
	bncs_string getUmdComment(int iIndex);
	int getTallyState(int iIndex, int iLeftOrRight);
	int getSrcDestLinkedToDevice(int iIndex);

	void  setRecentUmdCommand( int iIndex, bncs_string ssCmd, int iFirstOrSecondUmd );
	bncs_string  getRecentUmdCommand( int iIndex, int iFirstOrSecond );

	void  setRecentTallyCommand( int iIndex, bncs_string ssCmd, int iLeftOrRight );
	bncs_string  getRecentTallyCommand( int iIndex, int iLeftOrRight );

	void storeListBoxEntry( int iIndex );
	int getListBoxEntry( void );

	void setUMDFormatRules(int iIndex, bncs_string ssRules, bncs_string ssArgs);
	bncs_string getUMDFormatRules(int iIndex, bncs_string* ssArgs );

	void setUMDReplaceRules(int iIndex, bncs_string ssReplace, bncs_string ssRepBy );
	bncs_string getUMDReplaceRules(int iIndex, bncs_string* ssreplace);

	void setUMDDeviceArea(int iIndex, int iAreaRecord, int iAreaCode);
	int getUMDDeviceAreaGallery(int iIndex);
	int getUMDDeviceAreaCode(int iIndex);

};

#endif // !defined(AFX_UMDDEVICE_H__40CE825D_7AC9_4FEC_9674_6A51640C22A0__INCLUDED_)
