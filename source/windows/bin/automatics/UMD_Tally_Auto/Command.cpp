// Command.cpp: implementation of the CCommand class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Command.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCommand::CCommand()
{
	// init vars
	 strcpy( szCommand, "?" );
	 iTypeCommand = UNKNOWNVAL;
	 iWhichDevice = UNKNOWNVAL;
	 iWhichSlotDest = UNKNOWNVAL;
	 iWhichDatabase = UNKNOWNVAL;
}

CCommand::~CCommand()
{
    // fade to black
}
