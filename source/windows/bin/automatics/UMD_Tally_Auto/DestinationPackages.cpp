// DestinationPackages.cpp: implementation of the CDestinationPackages class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DestinationPackages.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDestinationPackages::CDestinationPackages()
{
	iPackageIndex = UNKNOWNVAL;
	ss_PackageFixedName = "";  // DB1
	ss_PackageUserTitle = "";    // DB3
	ss_PackagerPermLevels = "";    //DB5
	ss_Package_Definition7 = "";     //DB7

	i_Master_HD_Device = 0;
	for (int ii = 0; ii<5; ii++) i_Master_UHD_Device[ii] = 0;    // 0 for UHD all,    1..4 for UHD quandrants

	i_PersistVideoLevel = LEVEL_HD;      // DB5 persistent and only video 	i_PersistAudioLevel = AUDIO_USE_C;   // audio 16 is defualt level to use
	i_SourceVideoStream = 1;

	iTraced_Src_Package = 0;    
	iRouted_Src_Package = 0;   
	iVisionFrom_Src_Package = 0;
	iAudioFrom_Src_Package = 0;
	ssl_SrcPkgTrace = bncs_stringlist("", ',');
	
	ssl_Assoc_MVDeviceAndIndex = bncs_stringlist("",'|');
	ssl_Assoc_Friends = bncs_stringlist("",',');

	iLinkedAreaGallery = 0;
	iAssignedMixerDesk = 0;
	iAssignedMixerChannel = 0;      // if assoc to a mixer, then this is defined output from mixer desk 

}

CDestinationPackages::~CDestinationPackages()
{

}


