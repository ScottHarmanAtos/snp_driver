// StudioRevsData.cpp: implementation of the CStudioRevsData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "StudioRevsData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStudioRevsData::CStudioRevsData(int iRecordKey, bncs_string ssInstance, int iMixerBaseInfo, int iDriverOffset)
{
	m_iRecordKey = iRecordKey;
	m_ssMixerInstance = ssInstance;
	m_iStudioMixerDeviceId = iMixerBaseInfo;
	m_iStudioMixerOffset = iDriverOffset;

	for (int iitt = 0; iitt < KAHUNA_MIXER_MAXLEVELS; iitt++)  {
		m_iAssignedAreaToSwitcher[iitt] = 0;
	}

	// init arrarys
	for (int i = 0; i<MAXMIXERENTRIES; i++) {
		stChannelRev[i].iMixer_RevData = 0;
		stChannelRev[i].iAssociatedType1 = 0;
		stChannelRev[i].iAssociatedIndex1 = 0;   // pkg or sdi
		stChannelRev[i].iAssociatedDevice1 = 0;  // always packager for discovery at present
		stChannelRev[i].bOnAirISO = FALSE;
		stChannelRev[i].bOnAirNOW = FALSE;
		stChannelRev[i].iMxrLogicalSwitcher = 0;
	}

	for (int i = 0; i<65; i++) {
		stOutputsRev[i].iMixer_RevData = 0;
		stOutputsRev[i].iAssociatedType1 = 0;
		stOutputsRev[i].iAssociatedIndex1 = 0;   // pkg or sdi
		stOutputsRev[i].iAssociatedDevice1 = 0;  // always packager for discovery at present
		stOutputsRev[i].bOnAirISO = FALSE;
		stOutputsRev[i].bOnAirNOW = FALSE;
		stOutputsRev[i].iMxrLogicalSwitcher = 0;
	}

	m_iHighestMixerConfigEntry = 0;
	i_LowestOnAir_MixerChannel = 0;

	ssl_ListTracedSrcPackages_ISO = bncs_stringlist("", ',');
	ssl_ListTracedSrcPackages_OnAir = bncs_stringlist("", ',');


}

CStudioRevsData::~CStudioRevsData()
{
	// burn baby burn
}



int CStudioRevsData::getRecordKey(void)
{
	return m_iRecordKey;
}

bncs_string CStudioRevsData::getMixerInstance(void)
{
	return m_ssMixerInstance;
}

int CStudioRevsData::getMixerDeviceId(void)
{
	return m_iStudioMixerDeviceId;
}

int CStudioRevsData::getMixerDeviceOffset(void)
{
	return m_iStudioMixerOffset;
}

int CStudioRevsData::getMixerHighestEntry(void)
{
	return m_iHighestMixerConfigEntry;
}


BOOL CStudioRevsData::storeMixerState( int iGPI, bncs_string ssDataStates, int iState )
{
	BOOL bChanged = FALSE;
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)) {
		int iPrevState = stChannelRev[iGPI].iMixer_RevData;
		BOOL bPrevNOW = stChannelRev[iGPI].bOnAirNOW;
		BOOL bPrevISO = stChannelRev[iGPI].bOnAirISO;
		//
		stChannelRev[iGPI].bOnAirNOW = FALSE;
		stChannelRev[iGPI].bOnAirISO = FALSE;
		stChannelRev[iGPI].iMixer_RevData = iState;   
		if (ssDataStates.find("NOW") >= 0) stChannelRev[iGPI].bOnAirNOW = TRUE;
		if (ssDataStates.find("ISO") >= 0) stChannelRev[iGPI].bOnAirISO = TRUE;
		// change ?
		if ((iPrevState != iState) || (stChannelRev[iGPI].bOnAirNOW != bPrevNOW) || (stChannelRev[iGPI].bOnAirISO != bPrevISO)) bChanged = TRUE;
		// now check / calculate lowest on air mixer state
		i_LowestOnAir_MixerChannel = 0;
		for (int iChan = 1; iChan<=m_iHighestMixerConfigEntry; iChan++) {
			if (stChannelRev[iChan].bOnAirNOW) {
				// as lowest channel NOW onair found exit loop and function
				i_LowestOnAir_MixerChannel = iChan;
				return bChanged;
			}
		}
	}
	return bChanged;
}


BOOL CStudioRevsData::storeOutputState(int iGPI, bncs_string ssData, int iState)
{
	BOOL bChanged = FALSE;
	if ((iGPI>0) && (iGPI<MAXMIXERENTRIES)) {
		int iPrevState = stOutputsRev[iGPI].iMixer_RevData;
		BOOL bPrevNOW = stOutputsRev[iGPI].bOnAirNOW;
		BOOL bPrevISO = stOutputsRev[iGPI].bOnAirISO;
		//
		stOutputsRev[iGPI].bOnAirNOW = FALSE;
		stOutputsRev[iGPI].bOnAirISO = FALSE;
		stOutputsRev[iGPI].iMixer_RevData = iState;
		if (ssData.find("NOW") >= 0) stOutputsRev[iGPI].bOnAirNOW = TRUE;
		if (ssData.find("ISO") >= 0) stOutputsRev[iGPI].bOnAirISO = TRUE;
		// change ?
		if ((iPrevState != iState) || (stOutputsRev[iGPI].bOnAirNOW != bPrevNOW) || (stOutputsRev[iGPI].bOnAirISO != bPrevISO)) bChanged = TRUE;
	}
	return bChanged;
}


void CStudioRevsData::storeMixerAssocIndx( int iGPI, int iRtrDev, int iIndx1, int iType )
{
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)&&(iType>0)) {
		stChannelRev[iGPI].iAssociatedDevice1 = iRtrDev;
		stChannelRev[iGPI].iAssociatedIndex1 = iIndx1;
		stChannelRev[iGPI].iAssociatedType1 = iType;
		if (iGPI > m_iHighestMixerConfigEntry) m_iHighestMixerConfigEntry = iGPI;
	}
}


int CStudioRevsData::getMixerState( int iGPI )
{
	// if tallies disabled for mixer then all "deemed" off air 
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)) {
		return stChannelRev[iGPI].iMixer_RevData;
	}
	return 0;
}

BOOL CStudioRevsData::isMixerOnAirISO(int iGPI)
{
	// ISO level onair or not
	if ((iGPI>0) && (iGPI<MAXMIXERENTRIES)) {
		return stChannelRev[iGPI].bOnAirISO;
	}
	return FALSE;
}


BOOL CStudioRevsData::isMixerOnAirNOW(int iGPI)
{
	// NOW level on air or not
	if ((iGPI>0) && (iGPI<MAXMIXERENTRIES)) {
		return stChannelRev[iGPI].bOnAirNOW;
	}
	return FALSE;
}


int CStudioRevsData::getOutputState(int iGPI)
{
	// if tallies disabled for mixer then all "deemed" off air 
	if ((iGPI>0) && (iGPI<65)) {
		return stOutputsRev[iGPI].iMixer_RevData;
	}
	return 0;
}

BOOL CStudioRevsData::isOutputOnAirISO(int iGPI)
{
	// ISO level onair or not
	if ((iGPI>0) && (iGPI<65)) {
		return stOutputsRev[iGPI].bOnAirISO;
	}
	return FALSE;
}

BOOL CStudioRevsData::isOutputOnAirNOW(int iGPI)
{
	// NOW level on air or not
	if ((iGPI>0) && (iGPI<65)) {
		return stOutputsRev[iGPI].bOnAirNOW;
	}
	return FALSE;
}



int CStudioRevsData::getMixerAssocIndx(int iGPI, int *iType, int *iRtrDev)
{
	*iType = 0;
	if ((iGPI>0)&&(iGPI<MAXMIXERENTRIES)) {	
		*iRtrDev = stChannelRev[iGPI].iAssociatedDevice1;
		*iType = stChannelRev[iGPI].iAssociatedType1;
		return stChannelRev[iGPI].iAssociatedIndex1;
	}
	return UNKNOWNVAL;
}

void CStudioRevsData::storeOutputAssocIndx(int iGPI, int iRtrDev, int iIndx1, int iType)
{
	if ((iGPI>0) && (iGPI<65) && (iType>0)) {
		stOutputsRev[iGPI].iAssociatedDevice1 = iRtrDev;
		stOutputsRev[iGPI].iAssociatedIndex1 = iIndx1;
		stOutputsRev[iGPI].iAssociatedType1 = iType;
	}
}


int CStudioRevsData::getOutputAssocIndx(int iGPI, int *iType, int *iRtrDev)
{
	*iType = 0;
	if ((iGPI>0) && (iGPI<65)) {
		*iRtrDev = stOutputsRev[iGPI].iAssociatedDevice1;
		*iType = stOutputsRev[iGPI].iAssociatedType1;
		return stOutputsRev[iGPI].iAssociatedIndex1;
	}
	return UNKNOWNVAL;
}


bncs_string CStudioRevsData::getAnyOnAirSources(int iSrceType, int iTallyLevel)
{
	bncs_stringlist ssl_Ret=bncs_stringlist("",',');
	// if tallies disabled for mixer then all is "deemed" off air 
	for (int iC = 1; iC<m_iHighestMixerConfigEntry; iC++) {
		if (stChannelRev[iC].iAssociatedType1 == iSrceType) {
			if (iTallyLevel == KAHUNA_LEVEL_NOW) {
				if (stChannelRev[iC].bOnAirNOW) {
					if (iSrceType == SRCE_MAIN_SDI_INDEX) {
						ssl_Ret.append(bncs_string("%1|%2").arg(stChannelRev[iC].iAssociatedDevice1).arg(stChannelRev[iC].iAssociatedIndex1));
					}
					else {
						ssl_Ret.append(stChannelRev[iC].iAssociatedIndex1);
					}
				}
			}
			else if (iTallyLevel == KAHUNA_LEVEL_ISO1) {
				if (stChannelRev[iC].bOnAirISO) {
					if (iSrceType == SRCE_MAIN_SDI_INDEX) {
						ssl_Ret.append(bncs_string("%1|%2").arg(stChannelRev[iC].iAssociatedDevice1).arg(stChannelRev[iC].iAssociatedIndex1));
					}
					else {
						ssl_Ret.append(stChannelRev[iC].iAssociatedIndex1);
					}
				}
			}
		}
	}
	return ssl_Ret.toString(',');
}


bncs_string CStudioRevsData::getallOnAirDestinations(int iDestType, int iTallyLevel)
{
	bncs_stringlist ssl_Ret=bncs_stringlist("",',');
	// if tallies disabled for mixer then all is "deemed" off air 
	for (int iC = 1; iC < m_iHighestMixerConfigEntry; iC++) {
		if (stChannelRev[iC].iAssociatedType1 == iDestType) {
			if (iTallyLevel == KAHUNA_LEVEL_NOW) {
				if (stChannelRev[iC].bOnAirNOW) {
					if (iDestType == DEST_MAIN_SDI_INDEX) {
						ssl_Ret.append(bncs_string("%1|%2").arg(stChannelRev[iC].iAssociatedDevice1).arg(stChannelRev[iC].iAssociatedIndex1));
					}
					else {
						ssl_Ret.append(stChannelRev[iC].iAssociatedIndex1);
					}
				}
			}
			else if (iTallyLevel == KAHUNA_LEVEL_ISO1) {
				if (stChannelRev[iC].bOnAirISO) {
					if (iDestType == DEST_MAIN_SDI_INDEX) {
						ssl_Ret.append(bncs_string("%1|%2").arg(stChannelRev[iC].iAssociatedDevice1).arg(stChannelRev[iC].iAssociatedIndex1));
					}
					else {
						ssl_Ret.append(stChannelRev[iC].iAssociatedIndex1);
					}
				}
			}
		}
	}
	return ssl_Ret.toString(',');
}


int CStudioRevsData::get_Lowest_MixerTX_OnAir(void)
{
	return i_LowestOnAir_MixerChannel;
}


void CStudioRevsData::setGalleryDivisionAssignment(int iMixerDivision, int iAreaGallery)
{
	if ((iMixerDivision > 0) && (iMixerDivision < KAHUNA_MIXER_MAXLEVELS)) {
		m_iAssignedAreaToSwitcher[iMixerDivision] = iAreaGallery;
	}
}

int CStudioRevsData::getGalleryDivisionAssignment(int iMixerDivision)
{
	if ((iMixerDivision > 0) && (iMixerDivision < KAHUNA_MIXER_MAXLEVELS)) {
		return m_iAssignedAreaToSwitcher[iMixerDivision];
	}
	else
		return 0;
}

void CStudioRevsData::setMixerInputLogicalSwitcher(int iMixerInput, int iAssocDiv)
{
	if ((iMixerInput>0) && (iMixerInput < MAXMIXERENTRIES)) {
		stChannelRev[iMixerInput].iMxrLogicalSwitcher = iAssocDiv;
	}
}

int CStudioRevsData::getMixerInputLogicalSwitcher(int iMixerInput)
{
	if ((iMixerInput>0) && (iMixerInput < MAXMIXERENTRIES)) {
		return stChannelRev[iMixerInput].iMxrLogicalSwitcher;
	}
	return 0;
}

void CStudioRevsData::setMixerOutputLogicalSwitcher(int iMixerOutput, int iAssocDiv)
{
	if ((iMixerOutput>0) && (iMixerOutput < 65)) {
		stOutputsRev[iMixerOutput].iMxrLogicalSwitcher = iAssocDiv;
	}
}

int CStudioRevsData::getMixerOutputLogicalSwitcher(int iMixerOutput)
{
	if ((iMixerOutput>0) && (iMixerOutput < MAXMIXERENTRIES)) {
		return stOutputsRev[iMixerOutput].iMxrLogicalSwitcher;
	}
	return 0;
}
