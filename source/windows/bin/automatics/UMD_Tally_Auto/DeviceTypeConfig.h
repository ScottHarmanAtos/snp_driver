// holds entries from device types -- map of slots/names

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include "UMD_Tally_Common.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CDeviceTypeConfig
{
public:
	CDeviceTypeConfig(int iSlotIndx, bncs_string ssName);
	~CDeviceTypeConfig();

	int iKeySlot;
	bncs_string ssKeyName;
	bncs_string ssClass;
	bncs_string ssAccess;

	// other data 
	int iAssocPackageOrIndex;
	bncs_string ssAssocInstance;

	int iAssocDevice, iAssocOffset; // obtained from assoc instance
	
	bncs_string ssAssocArea;            
};

