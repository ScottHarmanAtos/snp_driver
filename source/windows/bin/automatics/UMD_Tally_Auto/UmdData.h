#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include "UMD_Tally_Common.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"


class CUmdData
{

public:
	CUmdData( int iDataIndex);
	~CUmdData();

	int m_iUmdData_Index;              // key
	//
	int m_iUmd_Type;                   // PACKAGE dest, srce, fixed, eggbox  or ROUTER source dest fixed etc
	int m_iMV_RtrDev;			     // Packager or Router Device feeding MV input  
	int m_iMV_Src_Dest;             // Router srce or dest feeding MV input -- or source for SOURCE UMDs / FIXED static video feeds
	int m_iUMDAreaGallery;
	int m_iUMDAreaCode;
	//
	// primary and secondary umd 
	bncs_string m_ssPrimaryUmdName;       // static text or calculated umd text based on src package or from rtr revertive or given value in xml
	bncs_string m_ssSecondUmdName;      // calculated 2nd tier umd 
	bncs_string m_ssComment;                    // comment from xml config
	bncs_string m_ssUmdCommand1;       // MAIN UMD last sdi name command sent  are stored to help reduce unnecessary repeated commands
	bncs_string m_ssUmdCommand2;       // 2nd UMD command sent  are stored to help reduce unnecessary repeated commands
	//
	int m_iumd_styletype;                            // flag for mcr dual; or any other special variant - dest or dest prepend                      
	bncs_string m_ssl_umd_format;         // certain rules for evs, cameras whatever for this mv tile raw from xml
	bncs_string m_ssl_umd_args;             // certain rules for evs, cameras whatever for this mv tile raw from xml
	bncs_string m_sstr_replace;    // string needing replacing or removing
	bncs_string m_sstr_repByThis;   // replacing by this...
	//
	// runtime umd tallies 
	int m_iLeftTallyState;
	int m_iRightTallyState;
	int m_iThirdTallyState;
	bncs_string m_ssTallyCommand1;  // mv tally command - left 
	bncs_string m_ssTallyCommand2;  // mv tally command - right 
	bncs_string m_ssTallyCommand3;  // mv tally command - right 

};

