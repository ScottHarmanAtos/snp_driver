// RouterSrceData.cpp: implementation of the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RouterSrceData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRouterSrceData::CRouterSrceData( int iIndx )
{
	m_iMapIndex=iIndx;
	m_ssl_SourceRoutedTo = bncs_stringlist( "", ',' );
	m_ssl_SDIIndxUsedInPackages = bncs_stringlist("", ',');
	m_ssl_LinkedToUmdDeviceAndIndex = bncs_stringlist("", '|');
	m_isHighwayOrWrap=0;
	m_bAssocToMixer = FALSE;
}

CRouterSrceData::~CRouterSrceData()
{

}
