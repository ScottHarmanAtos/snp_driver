// RouterData.h: interface for the CRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROUTERSRCEDATA_H__50B57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
#define AFX_ROUTERSRCEDATA_H__50B57040_A837_4D26_81CE_F77D18131A49__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
//
#include <windows.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"

class CRouterSrceData  
{
public:
	CRouterSrceData( int iIndx );
	virtual ~CRouterSrceData();

	int m_iMapIndex;
	int m_isHighwayOrWrap;                             // index of assoc highway record or part of a wrap - value is corresponding dest
	bncs_stringlist m_ssl_SourceRoutedTo;    // list of dests this src is routed to on LOCAL ring
	bncs_stringlist m_ssl_SDIIndxUsedInPackages;    // list of packages this src is used in
	bncs_stringlist m_ssl_LinkedToUmdDeviceAndIndex;  // list of devices assoc to rtr srce
	BOOL m_bAssocToMixer;           // true if dest linked to any mixer

};

#endif // !defined(AFX_ROUTERSRCEDATA_H__50B57040_A837_4D26_81CE_F77D18131A49__INCLUDED_)
