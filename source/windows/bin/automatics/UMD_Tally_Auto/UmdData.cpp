#include "UmdData.h"


CUmdData::CUmdData(int iDataIndex)
{
	m_iUmdData_Index = iDataIndex;
	m_iUmd_Type = 0;       
	//
	m_iMV_RtrDev=0;			    
	m_iMV_Src_Dest = 0;            
	m_iUMDAreaCode = 0;
	m_iUMDAreaGallery = 0;
	//
	// primary and secondary umd 
	m_ssPrimaryUmdName="";       
	m_ssSecondUmdName = "";      
	m_ssComment = "";                   
	m_ssUmdCommand1 = "";      
	m_ssUmdCommand2 = "";       
	//
	m_iumd_styletype = 0;
	m_ssl_umd_format = "";
	m_ssl_umd_args = "";
	m_sstr_replace = "";   
	m_sstr_repByThis = "";   
	//
	// runtime umd tallies 
	m_iLeftTallyState = 0;
	m_iRightTallyState = 0;
	m_iThirdTallyState = 0;
	m_ssTallyCommand1 = "";  
	m_ssTallyCommand2 = ""; 
	m_ssTallyCommand3 = ""; 

}


CUmdData::~CUmdData()
{
}
