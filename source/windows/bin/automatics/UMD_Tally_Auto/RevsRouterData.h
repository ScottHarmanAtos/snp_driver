// RevsRouterData.h: interface for the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
#define AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
	//
	#include <windows.h>
	#include <bncs_string.h>
	#include <bncs_stringlist.h>
	#include "bncs_auto_helper.h"

#include "RouterSrceData.h"	
#include "RouterDestData.h"	

class CRevsRouterData
{
private:
	int m_iRouterDeviceNumber;     // BASE dev infodriver number -- number dests 
	int m_iRouterLockDevice;     // base lock dev ini number
	int m_iMaxSources;                  // db0 size
	int m_iMaxDestinations;            // db1 size  -- will determine how many infodrivers used
	int m_iNumberVirtuals;
	int m_iStartVirtualSource, m_iStartVirtualDestination;
	int m_iAssocAreaGallery;        // area gallery that router is assoc to -- to aid in red / green tallies

	map<int, CRouterDestData*> cl_DestRouterData;   // map for Destination data
	map<int, CRouterSrceData*> cl_SrcRouterData;    // map for source data

	//router data maps are not seen outside class 
	CRouterSrceData* GetSourceData(int iSource);
	CRouterDestData* GetDestinationData(int iDestination);

public:
	CRevsRouterData( int iDeviceNumber, int iDests, int iSrcs, int iArea, int iNumberVirtuals );
	virtual ~CRevsRouterData();

	// router revertives 

	BOOL storeGRDRevertive(int iDestination, int iSource, bncs_string ssRevData );  // store given source from actual rev; 

	int getRouterNumber(void);
	int getMaximumDestinations(void);
	int getMaximumSources(void);
	int getRouterAreaGallery(void);
	int getRouterStartVirtualSource(void);
	int getRouterStartVirtualDestination(void);

	int getRoutedSourceForDestination(int iDestination);  // return source as got from grd rev
	int getPreviousSourceForDestination(int iDestination);  // return prev src 

	void addPackageToSourceList(int iSource, int iPackage);
	void removePackageFromSourceList(int iSource, int iPackage);
	bncs_string getAllPackagesForSource(int iSource);
	int getNumberPackagesUsingSource(int iSource);

	void addPackageToDestinationList(int iDestination, int iPackage);
	void removePackageFromDestinationList(int iDestination, int iPackage);
	bncs_string getAllPackagesForDestination(int iDestination);
	int getNumberPackagesUsingDestination(int iDestination);

	void addLinkedUmdDeviceforDest(int iDest, bncs_string ssUmdDev, int iInput);  // list of devices linked to this dest 
	void addLinkedUmdDeviceforSrc(int iSrc, bncs_string ssUmdDev, int iInput);  // list of devices linked to this src 

	bncs_string getLinkedUmdDevicesforDest(int iDest);  // list of devices linked to this dest 
	bncs_string getLinkedUmdDevicesforSrc(int iSrc);  // list of devices linked to this src 

	void setSourceLinkedToMixer(int iSource);
	void setDestinationLinkedToMixer(int iDestination);
	BOOL isSourceLinkedToAMixer(int iSource);
	BOOL isDestinationLinkedToAMixer(int iDestination);

	void setUMDOverrideSlotIndex(int iDestination, int iSlot);
	void setUMDOverrideLabel(int iDestination, bncs_string ssLabel, BOOL bPerm);
	int getUMDOverrideSlotIndex(int iDestination);
	bncs_string getUMDOverrideLabel(int iDestination);
	BOOL isUMDOverridePermanent(int iDestination);

	void setSourceLinkedtoHighwayOrWrap(int iSource, int iWrapDest);
	int getDestOfHighwayOrWrap(int iSource);

};

#endif // !defined(AFX_REVSROUTERDATA_H__E34205BE_41D3_4D42_82C1_6013D7836FE1__INCLUDED_)
