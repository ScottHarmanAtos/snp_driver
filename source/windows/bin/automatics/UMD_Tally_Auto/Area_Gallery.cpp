#include "Area_Gallery.h"


CArea_Gallery::CArea_Gallery(int iIndex, int iTypeCode, bncs_string ssGallInstance, bncs_string ssGallName)
{
	iAreaGallery_Index = iIndex;
	iAreaTypeCode = iTypeCode;
	ssAreaInstance_Id = ssGallInstance;
	ssAreaGallery_Name = ssGallName;
	ssAreaGallery_SourcePackages = "";
	ssAreaGallery_VirtualPackages = "";

	ssl_CameraGalleryVirtuals=bncs_stringlist("", ',');

	ssAssigned_MixerInstance = "";
	iAssigned_LogicalSwitcher = 0;

	ssl_ThusSrcRtrSDI_ISO = bncs_stringlist("", ',');
	ssl_ThusSrcRtrSDI_OnAir = bncs_stringlist("", ',');
	ssl_ThusSrcPackages_ISO = bncs_stringlist("", ',');
	ssl_ThusSrcPackages_OnAir = bncs_stringlist("", ',');

}


CArea_Gallery::~CArea_Gallery()
{
}
