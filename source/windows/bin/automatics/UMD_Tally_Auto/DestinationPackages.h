// DestinationPackages.h: interface for the CDestinationPackages class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DESTINATIONPACKAGES_H__F0C3ACF9_947B_48AE_B300_28AC1C2CD892__INCLUDED_)
#define AFX_DESTINATIONPACKAGES_H__F0C3ACF9_947B_48AE_B300_28AC1C2CD892__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
	//
	#include <windows.h>
	#include "UMD_Tally_Common.h"
	#include <bncs_string.h>
	#include <bncs_stringlist.h>
	#include "bncs_auto_helper.h"

class CDestinationPackages  
{
public:
	CDestinationPackages();
	virtual ~CDestinationPackages();

 	int iPackageIndex;
	int iPackageMode;                                // main or virtual
	int iPackageLockState;                         // from infodriver 5 status

	bncs_string ss_PackageFixedName;  // DB1
	bncs_string ss_PackageUserTitle;   // DB3  -- mcr 2 umd in here for certain dests
	bncs_string ss_PackagerPermLevels;  // db5 -- package type indication
	bncs_string ss_Package_Definition7;  // DB7 in new scheme

	// parsed from db strings
	int  i_Master_HD_Device;
	int  i_Master_UHD_Device[5];    // 0 for UHD all,    1..4 for UHD quandrants

	int i_PersistVideoLevel;                      // DB5   hd/uhd/bitc value for persistent video    -- type of dest pkg    default 1  but 0,1,2..8
	int i_SourceVideoStream;                  // from command in infodriver to override persist video level for take special

	int iRouted_Src_Package;
	int iTraced_Src_Package;    // ultimate source pkg - if routed goes via virtuals
	int iVisionFrom_Src_Package;  // most often from traced source package -- but virtual overrides may be in play
	int iAudioFrom_Src_Package;
	bncs_stringlist ssl_SrcPkgTrace;          // list of intergers of source packages in trace to traced source package
	
	int iLinkedAreaGallery;           // from area gallery config - if package is linked to package 
	int iAssignedMixerDesk;        // kahunas 1 -5 -- from mixer - then gallery assignment can be determined from elsewhere
	int iAssignedMixerChannel;  // if assoc to a mixer for tied_to area, then this is defined output from that area's mixer desk   XXXXXX currently no distinction between in and outputs
	
	bncs_stringlist ssl_Assoc_MVDeviceAndIndex; // list of <device,index>|<device,index> that are mv inputs linked to this src/dest pkg at init

	bncs_stringlist  ssl_Assoc_Friends;         // list of friends assoc to this dest package

};

#endif // !defined(AFX_DESTINATIONPACKAGES_H__F0C3ACF9_947B_48AE_B300_28AC1C2CD892__INCLUDED_)
