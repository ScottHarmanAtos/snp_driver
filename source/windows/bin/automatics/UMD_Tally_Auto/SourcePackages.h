// SourcePackages.h: interface for the CSourcePackages class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOURCEPACKAGES_H__EFFB27B0_8E6F_41E9_out2C4_3BCB70D38C92__INCLUDED_)
#define AFX_SOURCEPACKAGES_H__EFFB27B0_8E6F_41E9_out2C4_3BCB70D38C92__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
	//
	#include <windows.h>
	#include "UMD_Tally_Common.h"
	#include <bncs_string.h>
	#include <bncs_stringlist.h>
	#include "bncs_auto_helper.h"

class CSourcePackages  
{
public:
	CSourcePackages();
	virtual ~CSourcePackages();

	int iPackageIndex;
	int iPackageType; 

	bncs_string ss_PackageFixedTitle;  // DB0
	bncs_string ss_PackageUserTitle;    // DB2
	bncs_string ss_MCR_UMD_Names;        // DB9
	bncs_string ss_PCR_UMD_Names1;       // DB10  umd1
	bncs_string ss_PCR_UMD_Names2;       // DB10  umd2
	bncs_string ss_PCR_UMD_Names3;       // DB10 etc
	bncs_string ss_PCR_UMD_Names4;       // DB10 

	// db6 info extracted
	int i_Device_Srce_HD;              // source device for NV 2002 -- level 1 only at present
	int i_Device_Srce_UHD[5];     // UHD=0  2010, quadrants use 1..4    2011..2014
	int i_Device_Srce_SpareLvl1; // 4th spare - another spare level if ever required
	int i_Device_Srce_SpareLvl2;  // 5th spare - another spare level if ever required
	int i_Device_Srce_SpareLvl3; // 6th spare - another spare level if ever required

	bncs_stringlist ssl_Pkg_routedTo;   // routed to destination packages -- calc in first pass

	bncs_stringlist ssl_Assoc_MVDeviceAndIndex;  // list of <device,index>|<device,index> that are mv inputs linked to this src pkg at init

	int iLinkedAreaGallery;
	int iAssignedMixerDesk;        // kahunas 1 -5 -- from mixer - then gallery assignment can be determined from elsewhere
	int iAssignedMixerChannel;  // if assoc to a mixer for tied_to area, then this is defined output from that area's mixer desk 

	bncs_stringlist  ssl_Assoc_Friends;         // list of friends assoc to this package
	
};

#endif // !defined(AFX_SOURCEPACKAGES_H__EFFB27B0_8E6F_41E9_out2C4_3BCB70D38C92__INCLUDED_)
