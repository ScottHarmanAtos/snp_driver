/* UMD_Tally_Auto.cpp - custom program file for project UMD_Tally_Auto */

#include "stdafx.h"
#include "UMD_Tally_Auto.h"

/* TODO: place references to your functions and global variables in the UMD_Tally_Auto.h file */

// local function prototypes
// this function is called when a valid slotchange notification is extracted from the InfoNotify
BOOL SlotChange(UINT iDriver,UINT iSlot, LPCSTR szSlot);
void ApplySmartUMDTruncation2( bncs_string *ssCalcLabel );
void ProcessRouterModifyForSourceChange( int iChangedSource, BOOL bFromSlotChange );
CUmdDevice* GetUmdDeviceRecord( bncs_string ssKey );
void CalculateCurrentOnAirIndices(void);
void CalculateCurrentISOPackages(void);
void CalculateCCUGalleryUmd(CInfodriverRevs* pCCU);

BOOL IsSrcPkgOnAirElseWhere( int iSrcPkg, int iActiveGallery );

void ProcessCCURevertive( int iDevNum, int iSlotNum );
void ProcessStudioMixerRevertive( int iStudioDev, int iRevType, int iIndex );
 
//void RouteSourceToMVFollowingSrcPkg( int iSourcePkg );

bncs_string DetermineMixerUmdName( CDestinationPackages* pDest );
void UpdateMixerUmdName( CDestinationPackages* pDest, bncs_string ssUmdName );
void ProcessOverrideForDestinationIndexChange(int iChangedDevice, int iChangedDestPkg);
void ProcessISOTallyEnableDisable(int iWhichISO, int iStateChange, bncs_string ssArea);

bncs_string ProcessAndUpdateTXMonitors( CStudioRevsData* pMixer, int iWhichGallery );
void ProcessMultiplexOnAirState( int iRoutedSrcPkg, int iMixerState, int iWhichGallery );
void ProcessCCUTallyAssignments(int iRoutedSrcPkg, int iTracedSrcPkg, int iCameraState, int iWhichGallery, int iMxrChannel);

//////////////////////////////////////////////////////////////////////////
// 
//  COMMAND QUEUE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


void ClearCommandQueue( void )
{
	while (!qCommands.empty()) {
		CCommand* pCmd;
		pCmd = qCommands.front();
		qCommands.pop();
		delete pCmd;
	}
	SetDlgItemInt( hWndDlg, IDC_QUEUE, qCommands.size(), FALSE );
}

 
void AddCommandToQue( LPCSTR szCmd, int iCmdType, int iDev, int iSlot, int iDB )
{
	CCommand* pCmd = new CCommand;
	if (pCmd) {
		wsprintf( pCmd->szCommand, szCmd );
		pCmd->iTypeCommand = iCmdType;
		pCmd->iWhichDevice = iDev;
		pCmd->iWhichSlotDest = iSlot;
		pCmd->iWhichDatabase = iDB;
		qCommands.push( pCmd );
	}
	else 
		Debug( "AddCommand2Q - ERROR cmd record not created");

}

void ProcessNextCommand( int iRateRevs ) {
	
	BOOL bContinue=TRUE;
	do {
		if (qCommands.size()>0) {
			iRateRevs--;
			strcpy(szClientCmd, "");
			// something in queue so process it
			CCommand* pCommand = qCommands.front();
			qCommands.pop();
			if (pCommand) {
				// ONLY send cmd if main driver only or in starting mode for rx driver to register and poll initially
				if ((iOverallTXRXModeStatus==INFO_TXRXMODE)||(bAutomaticStarting)) {
					strcpy(szClientCmd, pCommand->szCommand);
					if ((bShowAllDebugMessages) && (!bAutomaticStarting)) Debug("ProcessCommand - TX> %s ", szClientCmd);
					switch (pCommand->iTypeCommand){
					case ROUTERCOMMAND:	ecCSIClient->txrtrcmd(szClientCmd);	break;
					case INFODRVCOMMAND: ecCSIClient->txinfocmd(szClientCmd); break;
					case GPIOCOMMAND:	ecCSIClient->txgpicmd(szClientCmd); break;
					case DATABASECOMMAND:
						eiExtInfoId->setdbname(iDevice, pCommand->iWhichDatabase, pCommand->iWhichSlotDest, szClientCmd, FALSE);
						break;
					case AUTOINFOREVERTIVE:	eiExtInfoId->updateslot(pCommand->iWhichSlotDest, szClientCmd);  break;
					default:
						Debug("ProcessNextCmd - unknown type from buffer - %d %s", pCommand->iTypeCommand, szClientCmd);
					} // switch
				}
				else {
					// as in RXonly mode - Clear the buffer - no need to go round loop
					ClearCommandQueue();
					bContinue=FALSE;
					iRateRevs=0;
				}
				delete pCommand; // delete record as now finished 
			}
			else
				Debug("ProcessNextCommand -ERROR- invalid command class");
		}
		else {
			// nothing else in queue
			bContinue = FALSE;
			iRateRevs=0;
		}
	} while ((bContinue)&&(iRateRevs>0));

	SetDlgItemInt( hWndDlg, IDC_QUEUE, qCommands.size(), FALSE );
	UpdateCounters();
	
	// if more commands still in queue and timer not running then start timer, else perhaps kill it
	if (qCommands.size()>0) {
		if ((!bNextCommandTimerRunning)&&(!bAutomaticStarting)) {
			SetTimer(hWndMain, COMMAND_TIMER_ID, 100, NULL); // timer for start poll
			bNextCommandTimerRunning = TRUE;
		}
	}
	else {
		if (bNextCommandTimerRunning) {
			KillTimer(hWndMain, COMMAND_TIMER_ID);
			bNextCommandTimerRunning = FALSE;
		}
	}

}


void SendCommandImmediately( LPCSTR szCmd, int iCmdType, int iDev  )
{
	if ((iOverallTXRXModeStatus==INFO_TXRXMODE)||(bAutomaticStarting)) {
		strcpy(szClientCmd, szCmd);
		switch (iCmdType){
		case ROUTERCOMMAND:	ecCSIClient->txrtrcmd(szClientCmd);	break;
		case INFODRVCOMMAND: ecCSIClient->txinfocmd(szClientCmd, TRUE); break;
		case GPIOCOMMAND:	ecCSIClient->txgpicmd(szClientCmd); break;
		default:
			Debug("SendCommandImmediately - unknown type from buffer - %d %s", iCmdType, szClientCmd);
		} // switch
		if (bShowAllDebugMessages) Debug("SendCommandImmediately - TX> %s ", szClientCmd);
	}
}



//////////////////////////////////////////////////////////////////////////
// 
//  AUTOMATIC STARTUP FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////


BOOL GetRouterDeviceDBSizes( bncs_string ssName, int *iRtrDevice, int *iDB0Size, int *iDB1Size ) 
{
	char szFileName[64]="", szEntry[12]="";
	int iNewDevice = 0;
	*iRtrDevice = 0; *iDB0Size = 0; *iDB1Size = 0;
	if (ssName.length() > 0) {
		iNewDevice = getInstanceDevice(ssName);
		strcpy(szFileName, ssName);
		if (iNewDevice > 0) {
			*iRtrDevice = iNewDevice;
			// get database sizes db0 and db1
			wsprintf(szFileName, "dev_%03d.ini", iNewDevice);
			*iDB0Size = atoi(r_p(szFileName, "Database", "DatabaseSize_0", "", false));
			*iDB1Size = atoi(r_p(szFileName, "Database", "DatabaseSize_1", "", false));
			return TRUE;
		}
	}
	return FALSE;
}


//
//  FUNCTION: ExtractStringfromDevIniFile( )
//
//  PURPOSE: Extracts the src name from specified Database dev ini file and returns string 
//
bncs_string ExtractStringfromDevIniFile( int iDev, int iEntry, int iDbFile )
{
	char szFileName[256] = "", szDatabase[32] = "", szIniFile[256] = "", szDBFile[256] = "", szEntry[32] = "", szName[256] = "";

	if ((iDev>0) && (iEntry>0)) {
		// determine db file location - first from inside dev.ini :
		sprintf(szIniFile, "dev_%03d.ini", iDev);
		sprintf(szDBFile, "DatabaseFile_%d", iDbFile);
		sprintf(szDatabase, "Database_%d", iDbFile);
		sprintf(szEntry, "%04d", iEntry);
		// file name from ini file
		strcpy(szFileName, r_p(szIniFile, "Database", szDBFile, "", FALSE));
		if (strlen(szFileName)<1) {
			// determine file name by another route -- using CSI default names
			if (iDbFile>9)
				sprintf(szFileName, "dev_%03d.db%04d", iDev, iDbFile);
			else
				sprintf(szFileName, "dev_%03d.db%d", iDev, iDbFile);
		}
		// now get required db entry
		strcpy(szName, r_p(szFileName, szDatabase, szEntry, "", FALSE));
		bncs_string ssTmp = bncs_string(szName).replace('|', ' ');
		bncs_string ssTmp1 = bncs_string(ssTmp).replace('|', ' ');
		// remove excess spaces at this point before returning string
		bncs_string ssTmp2 = ssTmp1.simplifyWhiteSpace();
		return ssTmp2;
	}
	return "";
}



char* GetParameterString( bncs_string sstr )
{
	strcpy( szResult, "");
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		wsprintf( szResult, "%s", LPCSTR(ssl1[1]) );
	return szResult;

}

int GetParameterInt( bncs_string sstr  )
{
	bncs_stringlist ssl1 = bncs_stringlist( sstr, '=' );
	if (ssl1.count()>1)
		return ssl1[1].toInt();
	else
		return UNKNOWNVAL;
}			


//////////////////////////////////////////////////////////////////////////
// 
BOOL AreBncsStringsEqual( bncs_string ss1, bncs_string ss2 )
{
	if (ss1.length()==ss2.length()) {
		if (ss1.length()==0)
			return TRUE;
		if (ss1.find(ss2)>=0)  // will find either whole match or just a matching subset
			return TRUE;
	}
	return FALSE;
}


void GetTwinParameters( bncs_string sstr, int *i1, int *i2 )
{
	*i1=0;
	*i2=0;
	bncs_stringlist ssl1 = bncs_stringlist( sstr, ',' );
	if (ssl1.count()>0) *i1 = ssl1[0].toInt();
	if (ssl1.count()>1) *i2 = ssl1[1].toInt();
}


int GetNumericList(bncs_string ssInstr, bncs_string* ssOutList)
{
	// read in a string of numbers - e.g. 1-4,6,7,8,10-12 uses dashes for ranges as well as commas for single entrires
	// output a list of numbers, and the number within the list
	// get list of entries split by commas
	bncs_stringlist ssl_outList = bncs_stringlist("", ',');
	bncs_stringlist ssl_inList = bncs_stringlist(ssInstr, ',');
	for (int iIndex = 0; iIndex < ssl_inList.count(); iIndex++) {
		// check for '-' to indicate a range
		bncs_string ssEntry = ssl_inList[iIndex];
		if (ssEntry.find("-")>0) {
			// split the string and process as a range of values
			bncs_string ssStart = "", ssEnd = "";
			ssEntry.split('-', ssStart, ssEnd);
			int iStart = ssStart.toInt();
			int iEnd = ssEnd.toInt();
			if ((iStart > 0) && (iEnd > 0)) {
				if (iEnd > iStart) {
					for (int irr = iStart; irr <= iEnd;irr++) {
						ssl_outList.append(irr);
					}
				}
				else if (iStart == iEnd) {
					ssl_outList.append(iStart);
				}
				else
					Debug("GetNumericList Invalid RANGE : %s %d-%d ", LPCSTR(ssEntry), iStart, iEnd);
			}
			else
				Debug("GetNumericList Invalid RANGE : %s %d-%d ", LPCSTR(ssEntry), iStart, iEnd);
		}
		else {
			// process single entry
			if (ssEntry.toInt() > 0) 
				ssl_outList.append(ssEntry);
			else
				Debug("GetNumericList Invalid single entry : %s %d ", LPCSTR(ssEntry), ssEntry.toInt() );
		}
	}
	//if (ssl_outList.count()>0) Debug("GetNumericList - returning list of %d items", ssl_outList.count());
	*ssOutList = ssl_outList.toString(',');
	return ssl_outList.count();
}

/////////////////////////////////////////////////////////////////////////


CRevsRouterData* GetMainVideoRouter()
{
	// main Nevion router is the one linked to the packager
	map<int, CRevsRouterData*>::iterator itp;
	if (cl_AllRouters) {
		if (ssl_AllRouters.count() > 0) {
			itp = cl_AllRouters->find(ssl_AllRouters[0].toInt());
			if (itp != cl_AllRouters->end()) {
				return itp->second;
			}
			else
				Debug("GetMainVideoRouter -- ERROR - no entry for %d ", ssl_AllRouters[0].toInt());
		}
		else
			Debug("GetMainVideoRouter -- ERROR - no entries within ssl_allrouters ");
	}
	else
		Debug("GetMainVideoRouter -- ERROR - no cl_allRouters class ");
	return NULL;
}



CRevsRouterData* GetValidRouter(int iWhichRtr)
{
	if (cl_AllRouters) {
		if (iWhichRtr > 0) {
			map<int, CRevsRouterData*>::iterator itp;
			itp = cl_AllRouters->find(iWhichRtr);
			if (itp != cl_AllRouters->end()) {
				return itp->second;
			}
		}
		else
			Debug("GetValidRouter -- zero router number passed ");
	}
	else
		Debug("GetValidRouter -- ERROR - no cl_allRouters class ");
	return NULL;
}

//////////////////////////////////////////////////////////////////////

CDeviceTypeConfig* GetDeviceTypeBySlot(int iKeySlot)
{
	if (cl_UMDDeviceTypes) {
		map<int, CDeviceTypeConfig*>::iterator itp;
		itp = cl_UMDDeviceTypes->find(iKeySlot);
		if (itp != cl_UMDDeviceTypes->end()) {
			return itp->second;
		}
	}
	return NULL;
}


CDeviceTypeConfig* GetDeviceTypeByName(bncs_string ssName)
{
	if (cl_UMDDeviceTypes) {
		map<int, CDeviceTypeConfig*>::iterator itp = cl_UMDDeviceTypes->begin();
		while (itp != cl_UMDDeviceTypes->end()) {
			if (itp->second) {
				if (itp->second->ssKeyName == ssName) return itp->second;
			}
			itp++;
		} // while
	}
	return NULL;
}


CRegionals* GetRegionalRecordByKey(int iKey)
{
	if (cl_AllRegionals) {
		map<int, CRegionals *>::iterator itp;
		itp = cl_AllRegionals->find(iKey);
		if (itp != cl_AllRegionals->end()) {
			return itp->second;
		}
	}
	return NULL;
}


CRegionals* GetRegionByRouterNumber(int iWhichRtr)
{
	if (cl_AllRegionals) {
		map<int, CRegionals*>::iterator itp = cl_AllRegionals->begin();
		while (itp != cl_AllRegionals->end()) {
			if (itp->second) {
				if (itp->second->iRegionalRouter == iWhichRtr) return itp->second;
			}
			itp++;
		} // while
	}
	return NULL;
}

CGPIODevice* GetGPIORecordByKey(int iKey)
{
	if (cl_AllGPIO) {
		map<int, CGPIODevice *>::iterator itp;
		itp = cl_AllGPIO->find(iKey);
		if (itp != cl_AllGPIO->end()) {
			return itp->second;
		}
	}
	return NULL;
}


//////////////////////////////////////////////////////////////////////////
// 
CSourcePackages* GetSourcePackage( int iIndex ) 
{
	// extra 2 are PARK and BARS
	if ((iIndex>0) && (iIndex <= iPackagerParkSrcePkg) && (cl_SourcePackages)) {
		map<int,CSourcePackages*>::iterator itp;
		itp = cl_SourcePackages->find(iIndex);
		if (itp!=cl_SourcePackages->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CDestinationPackages* GetDestinationPackage( int iIndex ) 
{
	if ((iIndex>0) && (iIndex<=iNumberOfUserPackages )&&(cl_DestinationPackages)) {
		map<int,CDestinationPackages*>::iterator itp;
		itp = cl_DestinationPackages->find(iIndex);
		if (itp!=cl_DestinationPackages->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


BOOL ReloadASourcePackageData( int iIndex, CSourcePackages* pSPkg  ) 
{
	BOOL bChanged = FALSE;
	CRevsRouterData* pSDIPkgr = GetMainVideoRouter();
	if (pSPkg&&pSDIPkgr) {

		// get key current data - to act as reference and determine what has changed in updated package
		int i_Pre_Src_SD  = pSPkg->i_Device_Srce_HD;
		bncs_string ss_Prev_UserName1 = pSPkg->ss_PCR_UMD_Names1;
		BOOL bLabelChanged=FALSE;

		// store and get new data
		pSPkg->ss_PackageFixedTitle = dbmPkgs.getName(iPackagerAuto, DB_SOURCE_NAME, iIndex);
		pSPkg->ss_PackageUserTitle = dbmPkgs.getName(iPackagerAuto, DB_SOURCE_TITLE, iIndex);
		pSPkg->ss_MCR_UMD_Names = dbmPkgs.getName(iPackagerAuto, DB_SOURCE_MCR_UMD, iIndex);
		bncs_stringlist ssl10 = bncs_stringlist( dbmPkgs.getName(iPackagerAuto, DB_SOURCE_PCR_UMD, iIndex), ',');
		pSPkg->ss_PCR_UMD_Names1 = ssl10.getNamedParam(bncs_string("umd1"));
		pSPkg->ss_PCR_UMD_Names2 = ssl10.getNamedParam(bncs_string("umd2"));
		pSPkg->ss_PCR_UMD_Names3 = ssl10.getNamedParam(bncs_string("umd3"));
		pSPkg->ss_PCR_UMD_Names4 = ssl10.getNamedParam(bncs_string("umd4"));

		// parse db 11 
		bncs_string ss11 = dbmPkgs.getName(iPackagerAuto, DB_SOURCE_VID_LVLS, iIndex);
		bncs_stringlist ssl2 = bncs_stringlist(ss11, '|');
		bncs_stringlist sslL1 = bncs_stringlist(ssl2.getNamedParam("L1"), '#');
		pSPkg->i_Device_Srce_HD = sslL1[0].toInt();
		// sort out list of packages this sdi used in - only needed if a change of sdi index has occurred
		if (i_Pre_Src_SD != pSPkg->i_Device_Srce_HD) {
			// remove previous
			if (i_Pre_Src_SD > 0) pSDIPkgr->removePackageFromSourceList(i_Pre_Src_SD, pSPkg->iPackageIndex);
			// add new
			if (pSPkg->i_Device_Srce_HD > 0) pSDIPkgr->addPackageToSourceList(pSPkg->i_Device_Srce_HD, pSPkg->iPackageIndex);
		}
		
		// has db2 or db9 changed - if so - umds may need recalculating -- if using db9 uncomment and use this if
		// has db2 or db10 changed - if so - umds may need recalculating
		if (AreBncsStringsEqual(ss_Prev_UserName1, pSPkg->ss_PCR_UMD_Names1)==FALSE){
			bChanged = TRUE;
		}

	}
	else {
		Debug( "ReloadASrcPackage -- invalid class or rtr for index %d", iIndex );
	}
	return bChanged;
}


BOOL ReloadADestinationPackageData( int iIndex, CDestinationPackages* pDPkg ) 
{
	char szCommand[MAX_AUTO_BUFFER_STRING]="", szData[64]="";

	CRevsRouterData* pSDIPkgr = GetMainVideoRouter();
	if (pDPkg&&pSDIPkgr) {
		
		// get key current data - 
		int iPrevDest = pDPkg->i_Master_HD_Device;		
		// unlink the rtr dest from package - if dest >0
		if (iPrevDest>0) pSDIPkgr->removePackageFromDestinationList(iPrevDest, pDPkg->iPackageIndex);
		
		// store and get new data
		pDPkg->ss_PackageFixedName = dbmPkgs.getName(iPackagerAuto, DB_DESTINATION_NAME, iIndex );
		pDPkg->ss_PackageUserTitle = dbmPkgs.getName(iPackagerAuto, DB_DESTINATION_MCR2, iIndex);
		pDPkg->ss_PackagerPermLevels = dbmPkgs.getName(iPackagerAuto, DB_DESTINATION_AUDIOMASK, iIndex );	
		pDPkg->ss_Package_Definition7 = dbmPkgs.getName(iPackagerAuto, DB_DESTINATION_DEFINITION, iIndex );	

		bncs_stringlist ssl = bncs_stringlist( pDPkg->ss_Package_Definition7, ',');
		if (ssl.count()>0) {			
			bncs_stringlist ssl2 = bncs_stringlist(ssl.getNamedParam("video"), '#');    // video#tag
			pDPkg->i_Master_HD_Device = ssl2[0].toInt();
			// xxx rewrite for discovery structure
			int iNewSource = pSDIPkgr->getRoutedSourceForDestination(pDPkg->i_Master_HD_Device);
		}

		// check 
		if (pDPkg->i_Master_HD_Device>0) pSDIPkgr->addPackageToDestinationList(pDPkg->i_Master_HD_Device, pDPkg->iPackageIndex);
		return TRUE;

	}
	else 
		Debug( "ReloadADestPackage -- invalid class or rtr for index %d", iIndex );
	return FALSE;
}

void CreateParkAndBarsSourcePackages(void)
{
	if (cl_SourcePackages) {
		// BARS
		bncs_string ssAreaPkgr = "c_packager_ldc";
		if (iWhichAutoArea == AUTO_AREA_NHN) ssAreaPkgr = "c_packager_nhn";
		//
		CSourcePackages* pSPkg2 = new CSourcePackages;
		if (pSPkg2) {
			pSPkg2->iPackageIndex = iPackagerBarsSrcePkg;
			pSPkg2->ss_PackageFixedTitle.append("BARS");
			pSPkg2->ss_PackageUserTitle.append("BARS");
			pSPkg2->ss_MCR_UMD_Names.append("BARS");
			pSPkg2->ss_PCR_UMD_Names1.append("BARS");
			pSPkg2->ss_PCR_UMD_Names2.append("BARS");
			pSPkg2->ss_PCR_UMD_Names3.append("BARS");
			pSPkg2->ss_PCR_UMD_Names4.append("BARS");
			i_SDI_Source_Bars = getXMLItemValue("packager_config", ssAreaPkgr, "router_source_bars").toInt();
			pSPkg2->i_Device_Srce_HD = i_SDI_Source_Bars;
			Debug("CreateBarsPackage %d src %d", iPackagerBarsSrcePkg, i_SDI_Source_Bars);
			// insert into map
			cl_SourcePackages->insert(pair<int, CSourcePackages*>(iPackagerBarsSrcePkg, pSPkg2));
		}
		else
			Debug("CreateParkandBars - park record fail");

		// PARK - also used for black sources when required
		CSourcePackages* pSPkg = new CSourcePackages;
		if (pSPkg) {
			pSPkg->iPackageIndex = iPackagerParkSrcePkg;
			pSPkg->ss_PackageFixedTitle.append("PARK");
			pSPkg->ss_PackageUserTitle.append("PARK");
			pSPkg->ss_MCR_UMD_Names.append("- - -");
			pSPkg->ss_PCR_UMD_Names1.append("- - -");
			pSPkg->ss_PCR_UMD_Names2.append("- - -");
			pSPkg->ss_PCR_UMD_Names3.append("- - -");
			pSPkg->ss_PCR_UMD_Names4.append("- - -");
			i_SDI_Source_Black = getXMLItemValue("packager_config", ssAreaPkgr, "router_source_park").toInt();
			pSPkg->i_Device_Srce_HD = i_SDI_Source_Black;
			pSPkg->iPackageType = UNKNOWNVAL; // so no marker
			Debug("CreateParkPackage %d src %d", iPackagerParkSrcePkg, i_SDI_Source_Black);
			// insert into map
			cl_SourcePackages->insert(pair<int, CSourcePackages*>(iPackagerParkSrcePkg, pSPkg));
		}
		else
			Debug("CreateParkandBars - park record fail");
	}
}



//////////////////////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: LoadSourcePackageData()
//
//  COMMENTS:  Source package details 
//		
BOOL LoadSourcePackageData( void )
{
	for (int iIndex=1; iIndex<=iNumberOfUserPackages; iIndex++) {  
		CSourcePackages* pSPkg  = new CSourcePackages;
		if (pSPkg) {
			pSPkg->iPackageIndex = iIndex;
			ReloadASourcePackageData(iIndex, pSPkg );
			// xxx friends config ???
			// insert into map
			if (cl_SourcePackages)
				cl_SourcePackages->insert(pair<int, CSourcePackages*>(iIndex, pSPkg));
			else
				Debug("LoadAllSrcPkgData - no cl_src_pkg class");
		}
		else
			Debug( "LoadSrcPkg - record failed for src %d", iIndex);
	} // for 
	// add BARS and PARK to end of map 
	CreateParkAndBarsSourcePackages();
	return TRUE;
	
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION: LoadDestinationPackageData()
//
//  COMMENTS:  Destination package details 
//		
BOOL LoadDestinationPackageData( void )
{
	if (cl_DestinationPackages) {
		for (int iIndex = 1; iIndex <= iNumberOfUserPackages; iIndex++) {
			CDestinationPackages* pDPkg = new CDestinationPackages;
			if (pDPkg) {
				pDPkg->iPackageIndex = iIndex;
				pDPkg->iRouted_Src_Package = iPackagerParkSrcePkg;
				pDPkg->iTraced_Src_Package = iPackagerParkSrcePkg;
				//Debug("ReloadDPkg %d ", iIndex);
				ReloadADestinationPackageData(iIndex, pDPkg);
				// currently no friends against dest packages --  pDPkg->ssl_Assoc_Friends = bncs_stringlist( ExtractStringfromDevIniFile(iDevice, iIndex, DB_PACKAGER ), '|' );
				// insert into map
				cl_DestinationPackages->insert(pair<int, CDestinationPackages*>(iIndex, pDPkg));
			}
			else
				Debug("LoadDestPkg - record failed for dest %d", iIndex);
		}
	}
	return TRUE;
	
	
}


int GetPackageIndexGivenRtrSrcDest( int iRtrIndex, int iSrcOrDestType )
{
	if (iRtrIndex>0) {
		if (iSrcOrDestType==SOURCE_UMD_TYPE) {
			for (int iIndex=1;iIndex<=iNumberOfUserPackages; iIndex++) {
				CSourcePackages* pSPkg = GetSourcePackage(iIndex);
				if (pSPkg) {
					if (pSPkg->i_Device_Srce_HD == iRtrIndex) return iIndex;
				}
				else 
					Debug( "GetPackageIndexGivenRtrSrcDest - record failed for src pkg %d", iIndex);
			}
		}
		else if (iSrcOrDestType==DEST_UMD_TYPE) {	// look thru dest packages
			for (int iIndex=1;iIndex<=iNumberOfUserPackages; iIndex++) {
				CDestinationPackages* pDPkg = GetDestinationPackage(iIndex);
				if (pDPkg) {
					if (pDPkg->i_Master_HD_Device == iRtrIndex) return iIndex;
				}
				else 
					Debug( "GetPackageIndexGivenRtrSrcDest - record failed for dest pkg %d", iIndex);
			}
		}
	}
	return 0;
}


CArea_Gallery* GetAreaGallery_ByKey(int iAreaGallery)
{
	if (cl_AllGalleries) {
		map<int, CArea_Gallery*>::iterator itp;
		itp = cl_AllGalleries->find(iAreaGallery);
		if (itp != cl_AllGalleries->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;

}


CArea_Gallery* GetAreaGallery_ByName(bncs_string ssName)
{
	if (cl_AllGalleries) {
		map<int, CArea_Gallery*>::iterator itp;
		itp = cl_AllGalleries->begin();
		while (itp != cl_AllGalleries->end()) {
			if (itp->second) {
				if (itp->second->ssAreaGallery_Name == ssName) return itp->second;
			}
			itp++;
		} // while
	}
	return NULL;

}


CArea_Gallery* GetAreaGallery_ByInstance(bncs_string ssName)
{
	if (cl_AllGalleries) {
		map<int, CArea_Gallery*>::iterator itp;
		itp = cl_AllGalleries->begin();
		while (itp != cl_AllGalleries->end()) {
			if (itp->second) {
				if (itp->second->ssAreaInstance_Id == ssName) return itp->second;
			}
			itp++;
		} // while
	}
	return NULL;

}


CStudioRevsData* GetMixerByKeyIndex(int iKeyNum)
{
	if (cl_MixerRevertives) {
		map<int, CStudioRevsData*>::iterator itp;
		itp = cl_MixerRevertives->begin();
		while (itp != cl_MixerRevertives->end()) {
			if (itp->second) {
				if (itp->second->getRecordKey() == iKeyNum) return itp->second;
			}
			itp++;
		} // while
	}
	return NULL;
}


CStudioRevsData* GetMixerByInstance(bncs_string ssInstance)
{
	if (cl_MixerRevertives) {
		map<int, CStudioRevsData*>::iterator itp;
		itp = cl_MixerRevertives->begin();
		while (itp != cl_MixerRevertives->end()) {
			if (itp->second) {
				if (itp->second->getMixerInstance() == ssInstance) return itp->second;
			}
			itp++;
		} // while
	}
	return NULL;
}


CStudioRevsData* GetMixerByDevice(int iDevNum)
{
	if ((iDevNum>0) && (cl_MixerRevertives)) {
		map<int, CStudioRevsData*>::iterator itp;
		itp = cl_MixerRevertives->find(iDevNum);
		if (itp != cl_MixerRevertives->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CInfodriverRevs* GetCCURecordByInfoSlot(int iInfoSlotCCU)
{
	if (cl_CCU_Assignments) {
		map<int, CInfodriverRevs*>::iterator itp;
		itp = cl_CCU_Assignments->find(iInfoSlotCCU);   // ccu records are based on key ccu infodriver slot ie 12,22,32,42 etc
		if (itp != cl_CCU_Assignments->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CInfodriverRevs* GetCCUAssignment(int iWhichCCU)
{
	if ((iWhichCCU>0) && (iWhichCCU <= MAX_CCU_ASSIGNMENT) && (cl_CCU_Assignments)) {
		map<int, CInfodriverRevs*>::iterator itp;
		// calc base slot for ccu 12,22,32,42 etc
		int iCalcSlot = (iWhichCCU * 10) + 2;
		itp = cl_CCU_Assignments->find(iCalcSlot);   
		if (itp != cl_CCU_Assignments->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}

CInfodriverRevs* GetCCURouted(int iWhichCCU)
{
	if ((iWhichCCU>0) && (iWhichCCU <= MAX_CCU_ASSIGNMENT) && (cl_CCU_Assignments)) {
		map<int, CInfodriverRevs*>::iterator itp;
		// calc routed slot for ccu :13,23,33,43 etc
		int iCalcSlot = (iWhichCCU * 10) + 3;
		itp = cl_CCU_Assignments->find(iCalcSlot);   
		if (itp != cl_CCU_Assignments->end()) {
			if (itp->second) return itp->second;
		}
	}
	return NULL;
}


CInfodriverRevs* GetCCUFromSourcePackage(int Package)
{
	if (cl_CCU_Assignments) {
		map<int, CInfodriverRevs*>::iterator itp;
		itp = cl_CCU_Assignments->begin();
		while (itp != cl_CCU_Assignments->end()) {
			if (itp->second) {
				if (itp->second->i_AssociatedSrcPackage1 == Package) return itp->second;      
			}
			itp++;
		} // while
	}
	return NULL;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////

int DetermineFriendType( bncs_string ssStr )
{
	bncs_string ssType = ssStr.upper();
	if (ssType.find("PACKAGER")>=0) return SOURCE_UMD_TYPE;         // packager
	if (ssType.find("SDI_MAIN")>=0) return SRCE_MAIN_SDI_INDEX;   // main router sdi - not packages
	return UNKNOWNVAL;
}


//////////////////////////////////////////////////////////////////////////
// 
//  AUTOMATIC GUI FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////

void ClearSrcPkgData()
{
	SetDlgItemText( hWndDlg, IDC_SPKG_NAME, "" );
	SetDlgItemText( hWndDlg, IDC_SPKG_UMD, "" );
	SetDlgItemText( hWndDlg, IDC_SPKG_SRC, "" );
	SetDlgItemText( hWndDlg, IDC_SPKG_SRCNAME, "" );	
}

void ClearDestPkgData()
{
	SetDlgItemText( hWndDlg, IDC_DPKG_NAME, "" );
	SetDlgItemText( hWndDlg, IDC_DPKG_DEST, "" );
	SetDlgItemText( hWndDlg, IDC_DPKG_SRC, "" );
	SetDlgItemText( hWndDlg, IDC_DPKG_SRCPKG, "" );
	SetDlgItemText( hWndDlg, IDC_DPKG_MVDEV, "" );
	SetDlgItemText( hWndDlg, IDC_DPKG_UMD, "" );
	SetDlgItemText( hWndDlg, IDC_DPKG_RUTEOK, "" );
	SetDlgItemText( hWndDlg, IDC_DPKG_AREA, "" );
	SetDlgItemText(hWndDlg, IDC_DPKG_AREA2, "");
	SetDlgItemText(hWndDlg, IDC_SPKG_FRIENDS, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_DEST2, "");
	SetDlgItemText(hWndDlg, IDC_DPKG_DEST3, "");
}


void DisplaySourcePackageData()
{
	// get list of cards for  chosen node
	char szTmp[256]="";
	ClearSrcPkgData();
	if ((iChosenSrcPkg>0) && (iChosenSrcPkg <= iPackagerParkSrcePkg)) {
		CSourcePackages* pPkg = GetSourcePackage(iChosenSrcPkg);
		CRevsRouterData* pSDIPkgr = GetMainVideoRouter();
		if (pPkg&&pSDIPkgr) {
			SetDlgItemText( hWndDlg, IDC_SPKG_NAME, LPCSTR(pPkg->ss_PackageUserTitle) );
			SetDlgItemText(hWndDlg, IDC_SPKG_UMD, LPCSTR(bncs_string("%1").arg(pPkg->ss_PCR_UMD_Names1)));
			SetDlgItemText(hWndDlg, IDC_SPKG_UMD2, LPCSTR(bncs_string("%1").arg(pPkg->ss_MCR_UMD_Names)));
			SetDlgItemInt( hWndDlg,  IDC_SPKG_SRC, pPkg->i_Device_Srce_HD, FALSE );
			// get src name from sdi rtr db0
			SetDlgItemText(hWndDlg, IDC_SPKG_SRCNAME, LPCSTR(ExtractStringfromDevIniFile(pSDIPkgr->getRouterNumber(), pPkg->i_Device_Srce_HD, 0 )));
			SetDlgItemText(hWndDlg, IDC_SPKG_MVDEV, LPCSTR(pPkg->ssl_Assoc_MVDeviceAndIndex.toString(' ')));
			SetDlgItemText(hWndDlg, IDC_SPKG_FRIENDS, LPCSTR(pPkg->ssl_Assoc_Friends.toString(' ')));
			//
		}
		else 
			Debug("DisplaySrcPack - no class entry for %d ", iChosenSrcPkg);
	}
}


void DisplayDestinationPackageData()
{
	// get list of cards for  chosen node
	char szTmp[256]="";
	ClearDestPkgData();
	if ((iChosenDestPkg>0) && (iChosenDestPkg <= iNumberOfUserPackages)) {
		CDestinationPackages* pPkg = GetDestinationPackage(iChosenDestPkg);
		CRevsRouterData* pSDIPkgr = GetMainVideoRouter();
		if (pPkg&&pSDIPkgr) {
			SetDlgItemText( hWndDlg, IDC_DPKG_NAME, LPCSTR(pPkg->ss_PackageFixedName) );
			SetDlgItemInt( hWndDlg, IDC_DPKG_DEST, pPkg->i_Master_HD_Device, FALSE );
			int iSdiSrc = 0;
			iSdiSrc = pSDIPkgr->getRoutedSourceForDestination(pPkg->i_Master_HD_Device);
			SetDlgItemInt( hWndDlg, IDC_DPKG_SRC, iSdiSrc, TRUE );
			SetDlgItemInt( hWndDlg, IDC_DPKG_SRCPKG, pPkg->iRouted_Src_Package, TRUE );
			SetDlgItemInt( hWndDlg, IDC_DPKG_TRACED, pPkg->iTraced_Src_Package, TRUE );
			SetDlgItemText(hWndDlg, IDC_DPKG_MVDEV, LPCSTR(pPkg->ssl_Assoc_MVDeviceAndIndex.toString(' ')));
			SetDlgItemText(hWndDlg, IDC_DPKG_UMD, LPCSTR(pPkg->ssl_SrcPkgTrace.toString(' ')));
			SetDlgItemInt(hWndDlg, IDC_DPKG_DEST2, pPkg->iVisionFrom_Src_Package, TRUE);
			SetDlgItemInt(hWndDlg, IDC_DPKG_DEST3, pPkg->iAudioFrom_Src_Package, TRUE);
			//
			SetDlgItemInt(hWndDlg, IDC_DPKG_AREA, pPkg->iAssignedMixerDesk, TRUE);
			SetDlgItemInt(hWndDlg, IDC_DPKG_AREA2, pPkg->iAssignedMixerChannel, TRUE);
			// xxx show area if mixer assigned + channel to area
		}
		else 
			Debug("DisplayDestPack - no class entry for %d or rtr class", iChosenDestPkg);
	}
}



void UpdateGuiForEcutState( int iWhichGallery, int iState )
{
	char szMsg[16]="";
	if (iState==1) strcpy( szMsg, "ecut" );
	int iLabel = 2130 + iWhichGallery;
	SetDlgItemText(hWndDlg, iLabel, szMsg); 
}


void UpdateGuiForMixerOnAir( int iWhichMixer ) 
{
	CStudioRevsData* pMixer = GetMixerByKeyIndex(iWhichMixer);
	if (pMixer) {
		int iLabel = 2140+iWhichMixer;
		if ((iLabel > 2140) && (iLabel < 2149)) {
			bncs_string sstr = bncs_string("%1 // %2").arg(pMixer->ssl_ListTracedSrcPackages_OnAir.toString(',')).arg(pMixer->ssl_ListTracedSrcPackages_ISO.toString(','));
			SetDlgItemText(hWndDlg, iLabel, LPCSTR(sstr));
		}
		// gall assignment
		iLabel = 2150 + iWhichMixer;
		if ((iLabel > 2150) && (iLabel < 2159)) {
			bncs_string sstr = bncs_string("%1 %2 %3 %4 %5 %6").arg(pMixer->getGalleryDivisionAssignment(1)).arg(pMixer->getGalleryDivisionAssignment(2))
				.arg(pMixer->getGalleryDivisionAssignment(3)).arg(pMixer->getGalleryDivisionAssignment(4)).arg(pMixer->getGalleryDivisionAssignment(5)).arg(pMixer->getGalleryDivisionAssignment(6));
			SetDlgItemText(hWndDlg, iLabel, LPCSTR(sstr));
		}
	}
}


////////////////////////////////////////////////////////////////////////////
void LoadPCRCameraVirtuals(CArea_Gallery* pGallery)
{
	if (pGallery) {
		int iEntry = 0;
		BOOL bCont = TRUE;
		do {
			iEntry++;
			//Debug("LoadPCRCameraVirtuals - gall %d item %s entry %d ", pGallery->iAreaGallery_Index, LPCSTR(pGallery->ssAreaGallery_Name), iEntry);
			int iCamVirPkg = getXMLItemValue("pcr_camera_virtuals", pGallery->ssAreaInstance_Id, bncs_string(iEntry)).toInt();
			if (iCamVirPkg > 0) {
				pGallery->ssl_CameraGalleryVirtuals.append(bncs_string(iCamVirPkg));
			}
			else
				bCont = FALSE;
		} while (bCont);
		Debug("LoadPCRCameraVirtuals - gall %d %s - loaded %d virtuals %s", pGallery->iAreaGallery_Index, LPCSTR(pGallery->ssAreaInstance_Id), iEntry - 1, LPCSTR(pGallery->ssl_CameraGalleryVirtuals.toString()));
	}
	else
		Debug("LoadPCRCAMVirs - invalid gallery class passed in");
}


////////////////////////////////////////////////////////////////////////////////////////////////////////

void AssignDestinationPackagesToGallery(int iGalleryArea, int iWhichPkg)
{
	CDestinationPackages* pDest = GetDestinationPackage(iWhichPkg);
	if (pDest) {
		pDest->iLinkedAreaGallery = iGalleryArea; // for gallery as an area
	}
}

void AssignSourcePackagesToGallery(int iGalleryArea, int iWhichPkg)
{
	CSourcePackages* pSrc = GetSourcePackage(iWhichPkg);
	if (pSrc) {
		pSrc->iLinkedAreaGallery = iGalleryArea; // for gallery as an area
	}
}

void AssignVirtualsToGallery(int iGalleryArea, int iWhichPkg)
{
	// source side
	CSourcePackages* pSrc = GetSourcePackage(iWhichPkg);
	if (pSrc) {
		pSrc->iLinkedAreaGallery = iGalleryArea; // for gallery as an area
	}
	// virtual dest side
	CDestinationPackages* pDest = GetDestinationPackage(iWhichPkg);
	if (pDest) {
		pDest->iLinkedAreaGallery = iGalleryArea; // for gallery as an area
	}

}



void LoadMixerConfiguration( bncs_string ssMxrInstance, int iMixerDeviceId, int iDeviceOffset )
{
	CStudioRevsData* pMixer = new CStudioRevsData(iNumberKahunaMixers, ssMxrInstance, iMixerDeviceId, iDeviceOffset);
	if (cl_MixerRevertives&&pMixer) {
		//
		bncs_string ssConfgFile = "pcr_mixers";
		if (iWhichAutoArea == AUTO_AREA_NHN) ssConfgFile = "pcr_mixers_nhn";
		//
		// MIXER CHANNEL INPUTS
		// may need to get mixer defn from kahuna xml / dev ini file to know reds, greens, iso etc
		for (int iEntry = 1; iEntry<MAXMIXERENTRIES; iEntry++)
		{
			bncs_string ssEntry = bncs_string("Mixer_Channel_%1").arg(iEntry);
			bncs_stringlist ssl = bncs_stringlist(getXMLItemValue(ssConfgFile, ssMxrInstance, ssEntry), ',');
			if (ssl.count()>1)
			{
				bncs_string ssType = ssl.getNamedParam("type").upper();
				bncs_string ssRtr = ssl.getNamedParam("router");
				int iRtrDevice = getInstanceDevice(ssRtr);
				int iPackRtrIndex = ssl.getNamedParam("index").toInt();
				int iSwitcher = ssl.getNamedParam("logical_switcher").toInt();

				// now parse entries -- packages are default
				int iType = 0;
				// distinguish type from packager or different router
				// is it router ?
				if (iRtrDevice == iPackagerAuto) {
					if ((ssType.find("SRCE") >= 0)||(ssType.find("SOURCE")) >= 0) iType = SOURCE_UMD_TYPE;
					if (ssType.find("DEST") >= 0) iType = DEST_UMD_TYPE;
				}
				//Debug("loadMxrAss mixer %d INPUT %d type %d dev %d package %d switcher %d FROM %s", iNumberKahunaMixers, iEntry, iType, iRtrDevice, iPackRtrIndex, iSwitcher, LPCSTR(ssl.toString(',')));

				if (iType > 0) {
					pMixer->storeMixerAssocIndx(iEntry, iRtrDevice, iPackRtrIndex, iType);
					pMixer->setMixerInputLogicalSwitcher(iEntry, iSwitcher);

					if (iType == DEST_UMD_TYPE) {
						CDestinationPackages* pDest = GetDestinationPackage(iPackRtrIndex);
						if (pDest) {
							pDest->iAssignedMixerDesk = iNumberKahunaMixers; // for mixer
							pDest->iAssignedMixerChannel = iEntry;
							// get min and max dest packages
						}
					}
					else if (iType == SOURCE_UMD_TYPE) {
						CSourcePackages* pSrc = GetSourcePackage(iPackRtrIndex);
						if (pSrc) {
							pSrc->iAssignedMixerDesk = iNumberKahunaMixers; // for mixer
							pSrc->iAssignedMixerChannel = iEntry;
						}
					}
				}
			}
		} // for

		// MIXER OUTPUTS
		// may need to get mixer defn from kahuna xml / dev ini file to know reds, greens, iso etc
		for (int iOutp = 1; iOutp < 65; iOutp++)
		{
			bncs_string ssEntry = bncs_string("Mixer_Output_%1").arg(iOutp);
			bncs_stringlist ssl = bncs_stringlist(getXMLItemValue(ssConfgFile, ssMxrInstance, ssEntry), ',');
			if (ssl.count()>1)
			{
				bncs_string ssType = ssl.getNamedParam("type").upper();
				bncs_string ssRtr = ssl.getNamedParam("router");
				int iRtrDevice = getInstanceDevice(ssRtr);
				int iPackRtrIndex = ssl.getNamedParam("index");
				int iSwitcher = ssl.getNamedParam("logical_switcher").toInt();

				// now parse entries -- packages are default
				int iType = 0;
				// distinguish type from packager or different router
				if ((ssType.find("SRCE") >= 0) || (ssType.find("SOURCE")) >= 0) iType = SOURCE_UMD_TYPE;
				if (ssType.find("DEST") >= 0) iType = DEST_UMD_TYPE;
				//Debug("loadMxrAss mixer %d OUTPUT %d type %d dev %d package %d switcher %d", iNumberKahunaMixers, iOutp, iType, iRtrDevice, iPackRtrIndex, iSwitcher);

				if (iType > 0) {
					pMixer->storeOutputAssocIndx(iOutp, iRtrDevice, iPackRtrIndex, iType);
					pMixer->setMixerOutputLogicalSwitcher(iOutp, iSwitcher);

					if (iType == DEST_UMD_TYPE) {
						CDestinationPackages* pDest = GetDestinationPackage(iPackRtrIndex);
						if (pDest) {
							pDest->iAssignedMixerDesk = iNumberKahunaMixers; // for mixer
							pDest->iAssignedMixerChannel = iOutp;
							// get min and max dest packages
						}
					}
					else if (iType == SOURCE_UMD_TYPE) {
						CSourcePackages* pSrc = GetSourcePackage(iPackRtrIndex);
						if (pSrc) {
							pSrc->iAssignedMixerDesk = iNumberKahunaMixers; // for mixer
							pSrc->iAssignedMixerChannel = iOutp;
						}
					}
				}
			}
		}

		//
		if (cl_MixerRevertives) cl_MixerRevertives->insert(pair<int, CStudioRevsData*>(iMixerDeviceId, pMixer));
	} // if pmxr
	else 
		Debug("LoadMixerAssignments - invalid class for mixer  %d", iNumberKahunaMixers);
}


/////////////////////////////////////////////////////////////////////////////

CRevsRouterData* GetOrAddRouterFromMap(bncs_string ssRtrInstance, int iRtrDevice, int iAssocGallery, int iStartVirtuals)
{
	// 
	CRevsRouterData* pRtr = NULL;
	int i_rtr_indx = 0, iSrcs = 0, iDests = 0;
	pRtr = GetValidRouter(iRtrDevice);
	if (pRtr == NULL) {
		// router not in map - so add
		if (GetRouterDeviceDBSizes(ssRtrInstance, &i_rtr_indx, &iSrcs, &iDests)) {
			pRtr = new CRevsRouterData(i_rtr_indx, iDests, iSrcs, iAssocGallery, iStartVirtuals);
			if (pRtr&&cl_AllRouters) {
				Debug("GetOrAddRouterFromMap - ADDED rtr %s - %d srcs %d dests %d ", LPCSTR(ssRtrInstance), i_rtr_indx, iSrcs, iDests);
				// add into map
				cl_AllRouters->insert(pair<int, CRevsRouterData*>(i_rtr_indx, pRtr));
				ssl_RouterInstances.append(ssRtrInstance);
				ssl_AllRouters.append(i_rtr_indx);
				SetDlgItemText(hWndDlg, IDC_AUTO_SDIRTR, LPCSTR(ssl_AllRouters.toString(',')));
			}
			else {
				Debug("GetOrAddRouterFromMap -- CLASS FAILED for additional Router %d ", i_rtr_indx);
				return NULL;
			}
		}
		else {
			Debug("LoadInit -- ERROR -- NO Router %s defined in instances ", LPCSTR(ssRtrInstance));
			return NULL;
		}
	}
	return pRtr;
}


BOOL GetMVInstanceDeviceSettings(bncs_string ssMVId, int *iMVDev, int *iMVOffset, bncs_string *ssLoc, bncs_string *ssType, bncs_string *ssAltId)
{
	// extract details for mview record 
	int iDev = 0, iOffset = UNKNOWNVAL;
	char szLoc[MAX_AUTO_BUFFER_STRING] = "", szType[MAX_AUTO_BUFFER_STRING] = "", szAltId[MAX_AUTO_BUFFER_STRING] = "";
	// remove "c_" if at front of instance - as it is WRONG
	// is there a device and offset from pure mvid ?
	if (ssMVId.length() > 0) {
		if (getInstanceDeviceSettings(ssMVId, &iDev, &iOffset, szLoc, szType, szAltId)) {
			if ((iDev > 0) && (iOffset >= 0)) {
				// values found so return
				*iMVDev = iDev;
				*iMVOffset = iOffset;
				*ssLoc = bncs_string(szLoc);
				*ssType = bncs_string(szType);
				*ssAltId = bncs_string(szAltId);
				return TRUE;
			}
		}
	}
	//
	Debug("LoadAllInit-GetMVDevSettings -c- NO INSTANCE for: %s - mv skipped", LPCSTR(ssMVId));
	return FALSE;
}


void LoadUMDTallyAutoDeviceTypes()
{
	// load all device types from xml config file - for use / parsing later
	char* szCCRoot = getenv("CC_ROOT");
	char* szCCSystem = getenv("CC_SYSTEM");
	// first check for 4.5 system settings
	if (szCCRoot && szCCSystem && cl_UMDDeviceTypes)	{
		//bncs_string ssBNCS_File_Path = bncs_string("%1\\%2\\config\\devicetypes\\UMD_Tally_Auto").arg(szCCRoot).arg(szCCSystem);
		bncs_string ssBNCS_File_Path = bncs_string("\\devicetypes\\UMD_Tally_Auto");
		bncs_config cfg(ssBNCS_File_Path);
		int iNumberItemsLoaded = 0;
		if (cfg.isValid()) {
			while (cfg.isChildValid())  {
				bncs_string ssName = cfg.childAttr("name");
				bncs_string ssClass = cfg.childAttr("class");
				bncs_string ssAccess = cfg.childAttr("access");
				int iKeySlot = cfg.childAttr("slot").toInt();
				if (iKeySlot > 0) {
					bncs_string ssGalleryArea = cfg.childAttr("area");
					bncs_string ssDevTInstance = cfg.childAttr("instance");
					int iDevTIndex = cfg.childAttr("index").toInt();
					CDeviceTypeConfig* pDevT = new CDeviceTypeConfig(iKeySlot, ssName);
					if (pDevT) {
						iNumberItemsLoaded++;
						pDevT->ssClass = ssClass;
						pDevT->ssAccess = ssAccess;
						pDevT->ssAssocArea = ssGalleryArea;
						pDevT->ssAssocInstance = ssDevTInstance;
						pDevT->iAssocPackageOrIndex = iDevTIndex;
						if (ssDevTInstance.length() > 0) {
							pDevT->iAssocDevice = getInstanceDevice(ssDevTInstance);
							pDevT->iAssocOffset = getInstanceOffset(ssDevTInstance);
						}
						// add to map
						cl_UMDDeviceTypes->insert(pair<int, CDeviceTypeConfig*>(iKeySlot, pDevT));
					}
					//Debug("LoadUMDDeviceType - entry %d %s slot %d ASSOC instance %s index %d", iNumberItemsLoaded, LPCSTR(ssName), iKeySlot, LPCSTR(ssDevTInstance), iDevTIndex);
				}
				cfg.nextChild();				 // go on to the next sibling (if there is one)
			} // while
		}
		else
			Debug("LoadUMDTallyAutoDeviceType invalid cfg for  %s\n", LPCSTR(ssBNCS_File_Path));
	}
}


void LoadFriendsDatabase()
{
	for (int ii = 1; ii <= iNumberOfUserPackages; ii++) {
		// get entry from db -- ignore 0 or same number as ii index 
		bncs_stringlist ssl = bncs_stringlist(bncs_string(dbmPkgs.getName(iDevice, iFriendsDatabase, ii)), ',');
		if (ssl.count() > 0) {
			CSourcePackages* pSrce = GetSourcePackage(ii);
			if (pSrce) {
				for (int ic = 0; ic < ssl.count(); ic++) {
					int ifriend = ssl[ic].toInt();
					if ((ifriend > 0) && (ifriend != ii)) {
						pSrce->ssl_Assoc_Friends.append(ssl[ic]);
					}
				} // for ic
				if (pSrce->ssl_Assoc_Friends.count() > 0) Debug("LoadFriends - Srce Pkg %d has friend packages %s", ii, LPCSTR(pSrce->ssl_Assoc_Friends.toString(',')));
			}
		}
	} // for ii
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: LoadAllInitialisationData()
//
//  PURPOSE: Function called on startup to load data from ini files / object settings etc relevant for driver
//                 creates new memory instances/
//
BOOL LoadAllInitialisationData( void ) 
{

	char szEntry[256];
	BOOL bContinue=TRUE;
	int iRouterDev, iSrcs=0, iDests=0, iP=0;
	bncs_string ssXMLConfigFile = "umd_tally_auto", strValue = "", s1, s2, sstr;

	Debug("LoadIni -- id section is %s from xml config file %s ", LPCSTR(ssAutoInstance), LPCSTR(ssXMLConfigFile));

	wsprintf(szEntry, "%d %s", iWhichAutoArea, LPCSTR(ssAutoAreaString));
	SetDlgItemText(hWndDlg, IDC_AUTO_AREA, szEntry );
	bncs_string ssAreaPkgr = "c_packager_ldc";
	if (iWhichAutoArea == AUTO_AREA_NHN) ssAreaPkgr = "c_packager_nhn";

	// PACKAGER globla vars
	for (int iivv = 0; iivv < 33; iivv++) {
		iPackagerAutoInfos[iivv] = 0;
		iPackagerDestStatInfos[iivv] = 0;
	}
	// package variables definitions -- loaded from relevant area section of packager_config.xml
	iNumberOfUserPackages = getXMLItemValue("packager_config", ssAreaPkgr, "number_of_packages").toInt();
	iInfodriverDivisor = getXMLItemValue("packager_config", ssAreaPkgr, "packager_divisor").toInt();
	i_Start_VirtualPackages = getXMLItemValue("packager_config", ssAreaPkgr, "first_virtual_package").toInt();
	if ((i_Start_VirtualPackages <= 0)||(i_Start_VirtualPackages >(iNumberOfUserPackages+1))) {
		Debug("LoadAllInitData - Warn/ Info -- NO VIRTUAL PACKAGES DEFINED in packager_config.xml - so setting beyond Number Packages");
		i_Start_VirtualPackages = iNumberOfUserPackages + 3;
	}
	SetDlgItemInt(hWndDlg, IDC_AUTO_PKGR2, i_Start_VirtualPackages, TRUE);
	iPackagerBarsSrcePkg = iNumberOfUserPackages + 1;
	iPackagerParkSrcePkg = iNumberOfUserPackages + 2;

	if (iInfodriverDivisor >0 ) iNumberInfosInBlock = iNumberOfUserPackages / iInfodriverDivisor; // max 32;
	Debug("Packager init - Defined Packages %d -  divisor %d - thus %d infodrivers per block", iNumberOfUserPackages, iInfodriverDivisor, iNumberInfosInBlock);

	bncs_string strPkrRtrValue = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Packager_Router");
	iPackagerAuto = getInstanceDevice(strPkrRtrValue);
	iPackagerAutoInfos[1] = iPackagerAuto;
	bncs_string strDestStatValue = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Packager_Dest_Status");
	iPackagerDestStatInfos[1] = getInstanceDevice(strDestStatValue);
	Debug("Packager init - Packager Router base Info %d -  Dest Status base Info %d ", iPackagerAuto, iPackagerDestStatInfos[1]);

	if ((iNumberOfUserPackages <= 0) || (iInfodriverDivisor <= 0) || (iNumberInfosInBlock <= 0) || (iPackagerAuto <= 0) || (iPackagerDestStatInfos[1]<=0)) {
		SetDlgItemText(hWndDlg, IDC_AUTO_PKGR, "CONFIG ERRORS");
		Debug("LoadInit -***- PACKAGE CONFIG ERRORS - Check packager / umd-t xml files -- auto suspended ***");
		return FALSE;
	}

	BOOL bZerosFound = FALSE;
	strPkrRtrValue.replace("_main", "");
	strDestStatValue.replace("_main", "");
	for (int iiInfos = 2; iiInfos <= iNumberInfosInBlock; iiInfos++) {
		if (iiInfos < 33) {
			// router
			bncs_string ssrtr = bncs_string("%1_%2").arg(strPkrRtrValue).arg(iiInfos);
			iPackagerAutoInfos[iiInfos] = getInstanceDevice(ssrtr);
			// dest status
			bncs_string ssdest = bncs_string("%1_%2").arg(strDestStatValue).arg(iiInfos);
			iPackagerDestStatInfos[iiInfos] = getInstanceDevice(ssdest);
			//
			if ((iPackagerAutoInfos[iiInfos] <= 0) || (iPackagerDestStatInfos[iiInfos] <= 0)) {
				Debug("LoadAllInit - Zero Infodriver device for pkgr router %s or dest status %s", LPCSTR(ssrtr), LPCSTR(ssdest));
				bZerosFound = TRUE;
			}
		}
	}
	// exit and error if required packager variables not set adequtely
	if (bZerosFound) {
		SetDlgItemText(hWndDlg, IDC_AUTO_PKGR, "ZERO INFOS");
		Debug("LoadInit -***- INFODRIVER CONFIG ERRORS - Check instances for packager router / dest_status devices ***");
		return FALSE;
	}

	wsprintf(szEntry, "%d-%d", iPackagerAutoInfos[1], iPackagerAutoInfos[iNumberInfosInBlock]);
	SetDlgItemText(hWndDlg, IDC_AUTO_PKGR, szEntry);
	Debug("LoadAllInit - PACKAGER INFOS are   %d to %d", iPackagerAutoInfos[1], iPackagerAutoInfos[iNumberInfosInBlock]);
	Debug("LoadAllInit - PKGR DEST STATUS INFOS are   %d to %d", iPackagerDestStatInfos[1], iPackagerDestStatInfos[iNumberInfosInBlock]);

	wsprintf(szEntry, "[ %d ]", iNumberOfUserPackages);
	SetDlgItemText(hWndDlg, IDC_SPKG_MAXPKGS, szEntry);
	SetDlgItemText(hWndDlg, IDC_DPKG_MAXPKGS, szEntry);

	// router variable definitions
	iIP_RouterVideoHD = 0;
	iNumberIPRouterInfodrivers = 0;
	iIP_RouterInfoDivider = 4096;  // unles set in nevion router dev ini to be otherwise 
	iRecursionTraceCounter = 0;
	iRegRtrTraceCounter = 0;
	iFriendsDatabase = 4;    // by defualt
	iNumberActiveUmdDevs = 0;
	ssl_NoTalliesSrcePackages = bncs_stringlist("", ',');
	ssl_AllRouters = bncs_stringlist("", ',');
	ssl_RouterInstances = bncs_stringlist("", ',');
	ssl_TraceToSource = bncs_stringlist("", ',');

	cl_MixerRevertives = new map<int, CStudioRevsData* >;

	cl_UMDDeviceTypes = new map < int, CDeviceTypeConfig* >;
	if (cl_UMDDeviceTypes) {
		LoadUMDTallyAutoDeviceTypes();
	}

	cl_AllRouters = new map<int, CRevsRouterData* >;
	if (cl_AllRouters == NULL) {
		Debug("LoadInit -- ERROR - map of all routers failed to create ");
		return FALSE;
	}

	cl_AllUmdDevices = new map<bncs_string, CUmdDevice* >;   
	if (cl_AllUmdDevices==NULL) {
		Debug( "LoadInit -- ERROR - map of all devices failed to create ");
		return FALSE;
	}

	cl_AllGalleries = new map < int, CArea_Gallery* > ;
	if (cl_AllGalleries == NULL) {
		Debug("LoadInit -- ERROR - map of all Galleries failed to create ");
		return FALSE;
	}

	iNumberRegionals = 0;
	cl_AllRegionals = new map< int, CRegionals* >;
	if (cl_AllRegionals == NULL) {
		Debug("LoadInit -- ERROR - map of all Regionals failed to create ");
		return FALSE;
	}
	iNumberGPIOs = 0;
	cl_AllGPIO = new map< int, CGPIODevice* >;
	if (cl_AllGPIO == NULL) {
		Debug("LoadInit -- ERROR - map of all GPIOdevices failed to create ");
		return FALSE;
	}

	Debug( "LoadAllInitData  - loading Nevion router data" );
	////// Main Nevion router linked to pacakages -- only used when an mv / tile is linked to router dest rather than package
	strValue = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Main_Video_Router");
	if (GetRouterDeviceDBSizes(strValue, &iRouterDev, &iSrcs, &iDests)) {
		Debug("LoadAllInitData - main rtr is %s - %d srcs %d dests %d ", LPCSTR(strValue), iRouterDev, iSrcs, iDests);
		CRevsRouterData* pSDI = new CRevsRouterData(iRouterDev, iDests, iSrcs, 0, 0);
		if (pSDI&&cl_AllRouters) {
			Debug("LoadAllInit - ADDED rtr %s - %d srcs %d dests %d ", LPCSTR(strValue), iRouterDev, iSrcs, iDests);
			// add into map
			cl_AllRouters->insert(pair<int, CRevsRouterData*>(iRouterDev, pSDI));
			ssl_RouterInstances.append(strValue);
			ssl_AllRouters.append(iRouterDev);   // NOTE: base infodriver for Nevion router assoc to packager is FIRST entry in this list -- key and important
			iIP_RouterVideoHD = iRouterDev;
			iNumberIPRouterInfodrivers = ((iDests - 1) / iIP_RouterInfoDivider) + 1;
			wsprintf(szEntry, "%d-%d", iRouterDev, iRouterDev + iNumberIPRouterInfodrivers - 1);
			SetDlgItemText(hWndDlg, IDC_AUTO_SDIRTR, szEntry);
		}
		else {
			Debug("LoadInit -- CLASS FAILED for main Nevion Router of the router dev %d -- auto suspended", iRouterDev);
			return FALSE;
		}
	}
	else {
		Debug("LoadInit -- NULL rtr_ip_facility instance entry so no main SDI for auto -- auto suspended");
		return FALSE;
	}

	//////////////// PACKAGER and packages config 

	Debug("LoadAllInit - packages");
	// load up all source and dest packages
	// load Source Packages db data
	cl_SourcePackages = new map<int,CSourcePackages*>;
	cl_DestinationPackages = new map<int,CDestinationPackages*>;
	if (cl_SourcePackages&&cl_DestinationPackages) {
		Debug("LoadAllInitData - loading Src Package data ");
		LoadSourcePackageData();
		// load Destination Packages db data
		Debug("LoadAllInitData - loading Dest Package data ");
		LoadDestinationPackageData();
	}
	else {
		Debug("LoadInit -ERROR- NULL packages classes -- auto suspended");
		return FALSE;
	}

	ssl_NoTalliesSrcePackages = bncs_stringlist(getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "No_tally_Source_Package"), ',');
	Debug("LoadAllInitData - no tallies for srce pkgs : %s ", LPCSTR(ssl_NoTalliesSrcePackages.toString(',')));

	/// MIXERS CONFIG
	iNumberKahunaMixers = 0;
	if (cl_MixerRevertives) {
		bncs_string ssMixerXml = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Mixers_config_xml");
		// get list of frames XXXX to do - on how they are logically carved up
		bncs_stringlist sslmxrs = bncs_stringlist(getXMLListOfItems(ssMixerXml), ',');
		Debug("LoadAllInit - LIST FROM PCR_MIXERS : %s", LPCSTR(sslmxrs.toString(',')));
		// is there a mixer instance -- then load all mixer associated stuff for gallery
		bncs_string ssMxrDevs = "";
		for (int iiMxr = 0; iiMxr < sslmxrs.count(); iiMxr++) {
			bncs_string ssMixer = sslmxrs[iiMxr];
			if ((ssMixer.length() > 0) && (ssMixer.find("frames") < 0)) {
				int iMxrDev = getInstanceDevice(ssMixer);
				int iMxrOffset = getInstanceOffset(ssMixer);
				if (iMxrDev > 0)  {
					ssMxrDevs.append(bncs_string("%1 ").arg(iMxrDev));
					iNumberKahunaMixers++;
					LoadMixerConfiguration(ssMixer, iMxrDev, iMxrOffset);
				}
				else
					Debug("LoadAllInit -ERR- fail to get device number Mixer Instance : %s", LPCSTR(ssMixer));
			}
		}
		SetDlgItemText(hWndDlg, IDC_AUTO_MGNDEV, LPCSTR(ssMxrDevs));
		Debug("LoadAllInit - loaded %d kahuna mixer devices %s ", iNumberKahunaMixers, LPCSTR(ssMxrDevs));
	}
	else
		Debug("LOADALL INIT - ERROR - NO MIXERS defined");

	//// AREA GALLERIES CONFIG 
	Debug("LoadAllInit - area_config / gallery mixers");
	iNumberAreaGalleries = 0;
	// load area galleries xml data and mixers assoc to galleries	
	bncs_string ssAreaXml = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Areas_config_xml");
	bncs_stringlist sslgals = bncs_stringlist(getXMLListOfItems(ssAreaXml), ',');
	Debug("LoadAllInit - AreaGallery config file is %s with list count %d", LPCSTR(ssAreaXml), sslgals.count());
	for (int iiGal = 0; iiGal < sslgals.count(); iiGal++) {
		// 
		bncs_string ssGal = sslgals[iiGal];
		bncs_string ssName = getXMLItemValue(ssAreaXml, ssGal, "area_name");
		if (ssName.length()>0) {
			iNumberAreaGalleries++;
			int iAreaTypeCode = 0;
			if (ssGal.find("pcr") >= 0) iAreaTypeCode = MV_AREA_PCR;
			if (ssGal.find("mfr") >= 0) iAreaTypeCode = MV_AREA_MFR;
			if (ssGal.find("mcr") >= 0) iAreaTypeCode = MV_AREA_MCR;
			CArea_Gallery* pGallery = new CArea_Gallery(iNumberAreaGalleries, iAreaTypeCode, ssGal, ssName);
			if (pGallery) {
				//
				pGallery->ssAreaGallery_SourcePackages = getXMLItemValue(ssAreaXml, ssGal, "source_packages");
				pGallery->ssAreaGallery_DestinationPackages = getXMLItemValue(ssAreaXml, ssGal, "destination_packages");
				pGallery->ssAreaGallery_VirtualPackages = getXMLItemValue(ssAreaXml, ssGal, "virtual_packages");
				pGallery->ssAssigned_MixerInstance = getXMLItemValue(ssAreaXml, ssGal, "assigned_mixer");
				pGallery->iAssigned_LogicalSwitcher = getXMLItemValue(ssAreaXml, ssGal, "logical_switcher").toInt();

				bncs_string ssListOfNumbers = "";
				if (GetNumericList(pGallery->ssAreaGallery_SourcePackages, &ssListOfNumbers) > 0) {
					bncs_stringlist ssl_Numbers = bncs_stringlist(ssListOfNumbers, ',');
					//Debug("LOADGALL - AssignSrce %s Source Pkgs (%d) from %d to %d gallery %d ", LPCSTR(strValue), ssl_Numbers.count(), ssl_Numbers[0].toInt(), ssl_Numbers[ssl_Numbers.count() - 1].toInt(), iNumberAreaGalleries);
					for (int iIndex = 0; iIndex < ssl_Numbers.count(); iIndex++) AssignSourcePackagesToGallery(iNumberAreaGalleries, ssl_Numbers[iIndex].toInt());
				}
				if (GetNumericList(pGallery->ssAreaGallery_DestinationPackages, &ssListOfNumbers) > 0) {
					bncs_stringlist ssl_Numbers = bncs_stringlist(ssListOfNumbers, ',');
					//Debug("LOADGALL - AssignDest %s Mixer Pkgs (%d) from %d to %d gallery %d ", LPCSTR(strValue), ssl_Numbers.count(), ssl_Numbers[0].toInt(), ssl_Numbers[ssl_Numbers.count() - 1].toInt(), iNumberAreaGalleries);
					for (int iIndex = 0; iIndex < ssl_Numbers.count(); iIndex++) AssignDestinationPackagesToGallery(iNumberAreaGalleries, ssl_Numbers[iIndex].toInt());
				}
				if (GetNumericList(pGallery->ssAreaGallery_VirtualPackages, &ssListOfNumbers) > 0) {
					bncs_stringlist ssl_Numbers = bncs_stringlist(ssListOfNumbers, ',');
					//Debug("LOADGALL - AssignVir %s Mixer Pkgs (%d) from %d to %d gallery %d ", LPCSTR(strValue), ssl_Numbers.count(), ssl_Numbers[0].toInt(), ssl_Numbers[ssl_Numbers.count() - 1].toInt(), iNumberAreaGalleries);
					for (int iIndex = 0; iIndex < ssl_Numbers.count(); iIndex++) AssignVirtualsToGallery(iNumberAreaGalleries, ssl_Numbers[iIndex].toInt());
				}
				//
				cl_AllGalleries->insert(pair<int, CArea_Gallery*>(iNumberAreaGalleries, pGallery));
			}
		}
	}
	Debug("LoadAllInit - loaded %d area galleries", iNumberAreaGalleries);

	//////// general bits and pieces config from xml file
	// defaults for format rules

	iEggbox_Timer_Interval = 30; // default for delay of eggbox tx umds
	iEggbox_Timer_Interval = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Eggbox_TX_UMD_Delay").toInt();
	if (iEggbox_Timer_Interval<10) iEggbox_Timer_Interval = 10;
	Debug("LoadAllInit - eggbox delay set to %d ms", iEggbox_Timer_Interval);

	i_Max_UMD_Length = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Max_UMD_Length").toInt();
	if (i_Max_UMD_Length<2) i_Max_UMD_Length = 32;

	ss_UMD_Join = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "UMD_Concat_String");
	Debug( "LoadInit -- maxLength %d UMD Concat string is %s", i_Max_UMD_Length, LPCSTR(ss_UMD_Join));
	
	// local regions influenced by this UMDT 
	ssl_AutoRegions = bncs_stringlist( getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Associated_regions"), ',');
	Debug("LoadInit -- Associated Regions are %s", LPCSTR(ssl_AutoRegions.toString(',')));

	//  Stockholm or other regional CCU tallies 
	// 1 get local router
	if ((cl_AllRegionals&&cl_AllGPIO) && (ssl_AutoRegions.count()>0)) {

		for (int iir = 0; iir < ssl_AutoRegions.count(); iir++) {

			int iRegionalRtr = 0;
			// first region e.g. Stockholm 
			bncs_string ssRegion = ssl_AutoRegions[iir];
			Debug("LoadInit -- Getting Region data for %s", LPCSTR(ssRegion));
			strValue = getXMLItemValue(ssXMLConfigFile, ssRegion, "local_router");
			if (strValue.length() > 0) {

				if (GetRouterDeviceDBSizes(strValue, &iRegionalRtr, &iSrcs, &iDests)) {
					// get virtuals for any router#
					char szFileName[128] = "";
					wsprintf(szFileName, "dev_%03d.ini", iRegionalRtr);
					int iNumberVirtuals = atoi(r_p(szFileName, "Router_01", "virtual_routes", "0", FALSE));
					Debug("LoadAllInitData - %s Regional rtr is %s - %d srcs %d dests %d virtuals %d", LPCSTR(ssRegion), LPCSTR(strValue), iRegionalRtr, iSrcs, iDests, iNumberVirtuals);
					CRevsRouterData* pSDI = new CRevsRouterData(iRegionalRtr, iDests, iSrcs, 0, iNumberVirtuals);
					if (pSDI&&cl_AllRouters) {

						// 2 load connections - key srcs/dests/links into packager 
						iNumberRegionals++;
						CRegionals* pRegion = new CRegionals();
						if (pRegion) {
							pRegion->iRegionalKey = iNumberRegionals;
							pRegion->iRegionalRouter = iRegionalRtr;
							pRegion->ssRegionalName = getXMLItemValue(ssXMLConfigFile, ssRegion, "market");
							pRegion->ssl_key_router_sources = bncs_stringlist(getXMLItemValue(ssXMLConfigFile, ssRegion, "key_sources"), ',');
							pRegion->ssl_key_router_wrap_pairs = bncs_stringlist(getXMLItemValue(ssXMLConfigFile, ssRegion, "key_wrap_srce_dest_pairs"), ',');
							pRegion->ssl_key_router_destinations = bncs_stringlist(getXMLItemValue(ssXMLConfigFile, ssRegion, "key_destinations"), ',');
							pRegion->ssl_key_packager_sources = bncs_stringlist(getXMLItemValue(ssXMLConfigFile, ssRegion, "key_source_packages"), ',');
							Debug("LoadAllInit - region %d %s - rtr %d srcs %s", iNumberRegionals, LPCSTR(pRegion->ssRegionalName), iRegionalRtr, LPCSTR(pRegion->ssl_key_router_sources.toString(',')));
							// associate pkg to dest ( 1:1 - until a better mechanism defined - perhaps via auto infodriver )
							for (int ii = 0; ii < pRegion->ssl_key_packager_sources.count(); ii++) {
								if (ii < pRegion->ssl_key_router_destinations.count()) {
									pRegion->ssl_dynamicWANAssocs.append(bncs_string("%1|%2").arg(pRegion->ssl_key_packager_sources[ii]).arg(pRegion->ssl_key_router_destinations[ii]));
								}
							}
							Debug("LoadAllInit - region %d - dynWan is %s", iNumberRegionals, LPCSTR(pRegion->ssl_dynamicWANAssocs.toString(',')));
							// store wrap links in router for region
							for (int iiwr = 0; iiwr < pRegion->ssl_key_router_wrap_pairs.count(); iiwr++) {
								bncs_stringlist sslPair = bncs_stringlist(pRegion->ssl_key_router_wrap_pairs[iiwr], '|');
								if (sslPair.count() > 1) {
									int isrce = sslPair[0].toInt();
									int idest = sslPair[1].toInt();
									pSDI->setSourceLinkedtoHighwayOrWrap(isrce, idest);
									Debug("loadAllInit - region %d - rtr %d wrap from src %d to dest %d", iNumberRegionals, iRegionalRtr, isrce, idest);
								}
							}
							// 3 get associated CCU gpios
							BOOL bCont = TRUE;
							int iCCU = 0;
							while (bCont) {
								iCCU++;
								bncs_stringlist ssl_data = bncs_stringlist(getXMLItemValue(ssXMLConfigFile, ssRegion, bncs_string("ccu_%1").arg(iCCU)), ',');
								if (ssl_data.count() > 0) {
									bncs_string ssInst = ssl_data.getNamedParam("instance");
									int igpio = ssl_data.getNamedParam("gpio_index").toInt();
									int iRtrSrce = ssl_data.getNamedParam("assoc_router_source").toInt();
									iNumberGPIOs++;
									CGPIODevice* pGpio = new CGPIODevice(iNumberGPIOs);
									if (pGpio) {
										int igpdev = 0, igpoffset = 0;
										igpdev = getInstanceDevice(ssInst);
										igpoffset = getInstanceOffset(ssInst);
										pGpio->iBNCS_GPIO_Device = igpdev;
										pGpio->iBNCS_GPIO_Index = igpoffset + igpio;
										pGpio->iAssociated_Index = iRtrSrce;
										pGpio->iAssociated_Regional_Key = iNumberRegionals;
										// add into map
										cl_AllGPIO->insert(pair<int, CGPIODevice*>(iNumberGPIOs, pGpio));
										// associate in regionals
										pRegion->ssl_key_LinkedGPIORecords.append(bncs_string(iNumberGPIOs));
										pRegion->ssl_dynamicGPIOAssocs.append(bncs_string("%1|%2").arg(iRtrSrce).arg(iNumberGPIOs));
										Debug("LoadAllInit - region %d %s - ccu %d gpio %d - dev %d indx %d srce %d", iNumberRegionals,
											LPCSTR(pRegion->ssRegionalName), iCCU, iNumberGPIOs, igpdev, igpoffset + igpio, iRtrSrce);
									}
									// add into map
									cl_AllRegionals->insert(pair<int, CRegionals*>(iNumberRegionals, pRegion));
									Debug("LoadAllInit - region %d - dyn gpio %s ", iNumberRegionals, LPCSTR(pRegion->ssl_dynamicGPIOAssocs.toString(',')));
								}
								else
									bCont = FALSE;
							} // while

						}
						else
							Debug("LoadAllinit -- create region class failed");

						// add router into map
						cl_AllRouters->insert(pair<int, CRevsRouterData*>(iRegionalRtr, pSDI));
						ssl_RouterInstances.append(strValue);
						ssl_AllRouters.append(iRegionalRtr);

					}
				}
				else
					Debug("LoadAllInit - ERROR - no REGIONAL router from instances for %s");
			}
		}
		iCurrentRegion = 1;
	}
	else
		Debug("LoadAllInit - ERROR - regionals/gpio maps - cannot load regional data");

	SetDlgItemInt(hWndDlg, IDC_AUTO_NUMREGIONS, iNumberRegionals, TRUE);

	////// MULTIVIEWERS / TSL  config 
	// it will contain all data for MV required 

	Debug( "LoadAllInitData - loading MV device data " );
	iNumberActiveUmdDevs = 0;
	int iMVCount =0;

	// get list of all mv ids 
	bncs_string ssMVFile = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Multiviewers_config_xml");
	bncs_stringlist ssl_mvs = bncs_stringlist(getXMLListOfItems(ssMVFile), ',');
	
	for (int iiMV=0;iiMV<ssl_mvs.count();iiMV++) {
		iMVCount++;
		// 
		bncs_string ssMVId = ssl_mvs[iiMV];
		Debug("LoadAllInit MV count %02d  mv %s ", iMVCount, LPCSTR(ssMVId));
		if (ssMVId.length()>0) {
			//
			if (ssMVId.find("c_") >= 0) ssMVId.replace("c_", "");
			int iMVDev = 0, iMVOffset = UNKNOWNVAL;
			bncs_string ssLoc = "", ssType = "", ssAltId = "";
			if (GetMVInstanceDeviceSettings(ssMVId, &iMVDev, &iMVOffset, &ssLoc, &ssType, &ssAltId)) {

				CUmdDevice* pUmd = new CUmdDevice( ssMVId, iMVDev, iMVOffset, ssAltId, ssLoc );
				if (pUmd) {
					int iInput=0;
					BOOL bAnotherInput=TRUE;

					// determine type
					pUmd->setDeviceType(TAG_MV);  // as default
					// change device type if found
					if (ssType.find("TSL_UMD_V5") >= 0) pUmd->setDeviceType(TSL_V5_DRIVER_10);
					else if ((ssType.find("TSL") >= 0) && ((ssType.find("16") >= 0) || (ssType.find("8") >= 0))) pUmd->setDeviceType(TSL_UMD_TYPE_16);
					//Debug("LoadAllInit - LoadMVs - type %d from %s for MV %s ", pUmd->getUMDDeviceType(), LPCSTR(ssType), LPCSTR(ssMVId));

					// process inputs for mv
					do {
						iInput++;
						bncs_string ssItem = bncs_string("input_%1").arg(iInput);
						if (iInput < 10)  ssItem = bncs_string("input_0%1").arg(iInput);
						// required new format data
						bncs_string strType = getXMLItemValue(ssMVFile, ssMVId, ssItem, "type").upper();
						bncs_string strRtrInstance = getXMLItemValue(ssMVFile, ssMVId, ssItem, "router_instance");
						int iRtrIndex = getXMLItemValue(ssMVFile, ssMVId, ssItem, "router_index").toInt();
						bncs_string strStatic = getXMLItemValue(ssMVFile, ssMVId, ssItem, "static_text");
						bncs_string strComment = getXMLItemValue(ssMVFile, ssMVId, ssItem, "comment");
						// extra nice to haves:
						bncs_string strFormat = getXMLItemValue(ssMVFile, ssMVId, ssItem, "format");
						bncs_string strFormatArgs = getXMLItemValue(ssMVFile, ssMVId, ssItem, "format_args").upper();
						bncs_string strReplace = getXMLItemValue(ssMVFile, ssMVId, ssItem, "replace");
						bncs_string strReplaceArgs = getXMLItemValue(ssMVFile, ssMVId, ssItem, "replace_args");
						bncs_string ssMVIndxarea = getXMLItemValue(ssMVFile, ssMVId, ssItem, "area");

						int iSrcDestType = 0;
						if ((strType.find("FIXED") >= 0) || (strType.find("STATIC") >= 0)) iSrcDestType = FIXED_UMD_TYPE;
						else if (strType.find("SRCE") >= 0) iSrcDestType = SOURCE_UMD_TYPE;  // type changed below if linked to router other than packager
						else if (strType.find("DEST") >= 0) iSrcDestType = DEST_UMD_TYPE;
						else if ((strType.find("TXMON") >= 0) || (strType.find("EGGBOX") >= 0)) iSrcDestType = TX_MON_UMD_TYPE;
						else if (strType.find("PVMON") >= 0) iSrcDestType =PV_MON_UMD_TYPE;
						//Debug("load init - id %s item %s -> type %s (%d) xml inst %s indx %d area %s", 
						//	LPCSTR(ssMVId), LPCSTR(ssItem), LPCSTR(strType), iSrcDestType, LPCSTR(strRtrInstance), iRtrIndex, LPCSTR(ssMVIndxarea));

						if ((strType.length() > 0) && (iSrcDestType > 0)) {

							// package or 
							// rtr and dest feeding MV  - if this then type will be changed below
							int iMVRtr = getInstanceDevice(strRtrInstance);
							int iMVFeed = iRtrIndex;
							int iAreaGallery = 0;
							int iAreaCode = 0;
							// determine area /gallery for index
							CArea_Gallery* pGall = GetAreaGallery_ByInstance(ssMVIndxarea);
							if (pGall) {
								iAreaGallery = pGall->iAreaGallery_Index;
								iAreaCode = pGall->iAreaTypeCode;
								//Debug("LoadAllInit - device %s index %d associated to   AREA %d   code %d   from %s", LPCSTR(ssMVId), iInput, iAreaGallery, iAreaCode, LPCSTR(ssMVIndxarea));
							}
							// store all other umd data obtained from file
							pUmd->createUMDDetails(iInput, iSrcDestType, iMVRtr, iMVFeed, iAreaGallery, iAreaCode, strStatic);

							// sort out params for umd type 
							// FIXED UMDS 
							if (iSrcDestType == FIXED_UMD_TYPE) {
								// get just umd label and mv dest
								if ((iMVRtr == iPackagerAuto) && (iMVFeed > 0)) {
									CSourcePackages* pSrc = GetSourcePackage(iMVFeed);
									if (pSrc) {
										sstr = bncs_string("%1,%2").arg(ssMVId).arg(iInput);
										pSrc->ssl_Assoc_MVDeviceAndIndex.append(sstr);
										pSrc->iLinkedAreaGallery = iAreaGallery;
									}
									//Debug("LoadInit - FIXED - MV %s input %d string %s assoc pkg=%d", LPCSTR(ssMVId), iInput, LPCSTR(strStatic), iMVFeed);
								}
								else if ((iMVRtr > 0) && (iMVFeed > 0)) {
									// make link in rtr class  -- see if this router is known in list of routers - if not then add into map
									if (iMVRtr > 0) {
										CRevsRouterData* pRTR = GetOrAddRouterFromMap(strRtrInstance, iMVRtr, iAreaGallery, 0);
										if (pRTR) pRTR->addLinkedUmdDeviceforSrc(iMVFeed, ssMVId, iInput);
									}
									//Debug("LoadInit - FIXED - MV %s input %d assoc ROUTER %s %d destination %d", LPCSTR(ssMVId), iInput, LPCSTR(strRtrInstance), iMVRtr, iMVFeed);
								}

							}
							else if (iSrcDestType == SOURCE_UMD_TYPE) {
								if ((iMVRtr == iPackagerAuto) && (iMVFeed > 0)) {
									// make link in dest/src packages
									CSourcePackages* pSrc = GetSourcePackage(iMVFeed);
									if (pSrc) {
										sstr = bncs_string("%1,%2").arg(ssMVId).arg(iInput);
										pSrc->ssl_Assoc_MVDeviceAndIndex.append(sstr);
										pSrc->iLinkedAreaGallery = iAreaGallery;
									}
									//Debug("LoadInit - SRCE - MV %s input %d assoc PACKAGE %d", LPCSTR(ssMVId), iInput, iMVFeed);
								}
								else if ((iMVRtr > 0) && (iMVFeed > 0)) {
									// make link in rtr class  -- see if this router is known in list of routers - if not then add into map
									if (iMVRtr > 0) {
										CRevsRouterData* pRTR = GetOrAddRouterFromMap(strRtrInstance, iMVRtr, iAreaGallery, 0);
										if (pRTR) {
											pUmd->setChangeUmdType(iInput, SRCE_MAIN_SDI_INDEX);
											pRTR->addLinkedUmdDeviceforSrc(iMVFeed, ssMVId, iInput);
										}
									}
									//Debug("LoadInit - SRCE - MV %s input %d assoc ROUTER %s %d destination %d", LPCSTR(ssMVId), iInput, LPCSTR(strRtrInstance), iMVRtr, iMVFeed);
								}
								else
									Debug("LoadAllIni ERROR MV SRCE data - neither package nor rtr defined %s %d", LPCSTR(ssMVId), iInput);
							}
							else if (iSrcDestType == DEST_UMD_TYPE) {
								if ((iMVRtr == iPackagerAuto) && (iMVFeed > 0)) {
									// make link in dest/src packages
									CDestinationPackages* pDest = GetDestinationPackage(iMVFeed);
									if (pDest) {
										sstr = bncs_string("%1,%2").arg(ssMVId).arg(iInput);
										pDest->ssl_Assoc_MVDeviceAndIndex.append(sstr);
										pDest->iLinkedAreaGallery = iAreaGallery;
									}
									//Debug("LoadInit - DEST - MV %s input %d assoc PACKAGE %d assoc area gall %d", LPCSTR(ssMVId), iInput, iMVFeed, pDest->iLinkedAreaGallery);
								}
								else if ((iMVRtr > 0) && (iMVFeed > 0)) {
									// make link in rtr class  -- see if this router is known in list of routers - if not then add into map
									if (iMVRtr > 0) {
										CRevsRouterData* pRTR = GetOrAddRouterFromMap(strRtrInstance, iMVRtr, iAreaGallery, 0);
										if (pRTR) {
											pUmd->setChangeUmdType(iInput, DEST_MAIN_SDI_INDEX);
											pRTR->addLinkedUmdDeviceforDest(iMVFeed, ssMVId, iInput);
										}
									}
									//Debug("LoadInit - DEST - MV %s input %d assoc ROUTER %s %d destination %d", LPCSTR(ssMVId), iInput, LPCSTR(strRtrInstance), iMVRtr, iMVFeed);
								}
								else
									Debug("LoadAllIni ERROR MV DEST data - neither package nor rtr defined %s %d", LPCSTR(ssMVId), iInput);
							}
							else if (iSrcDestType == TX_MON_UMD_TYPE) {
								// xxx sort out
							}
							else if (iSrcDestType == PV_MON_UMD_TYPE) {
								// xxx sort out
							}
							if (strComment.length() > 0) pUmd->storeUmdComment(iInput, strComment);
							//
							// store format and replace rules -- finish
							pUmd->setUMDFormatRules(iInput, strFormat, strFormatArgs);
							pUmd->setUMDReplaceRules(iInput, strReplace, strReplaceArgs);
						}
						else
							bAnotherInput = FALSE;
					} while (bAnotherInput);
					iInput--;
					Debug( "LoadInit - MV Device %s has %d inputs defined ", LPCSTR(ssMVId), iInput );

					// add to gui listbox and store listbox index in rec
					wsprintf( szEntry, "%s ", LPCSTR(ssMVId) );
					SendDlgItemMessage(hWndDlg, IDC_LIST1, LB_INSERTSTRING, -1, (LPARAM)szEntry);
					pUmd->storeListBoxEntry( iNumberActiveUmdDevs );
					// add to map
					if (cl_AllUmdDevices) {
						cl_AllUmdDevices->insert(pair<bncs_string, CUmdDevice*>(ssMVId, pUmd) );
						ssl_AllUMDDevices.append(ssMVId);
						iNumberActiveUmdDevs++;
					}
					else 
						Debug("Load init -- ERROR no allUMDDevices class - cannot add to map");
				}
				else 
					Debug("Load init -- ERROR failed to create UMD class ");
			}
			else
				Debug( "LoadInit -- ERROR failed to find mv device %s in instances.xml", LPCSTR(ssMVId) );
		}
		else 
			bContinue = FALSE;
	} // while	
	Debug( "LoadInit -- TOTAL loaded %d records for MV/TSL devices ", iNumberActiveUmdDevs );

	// Friends DB assignment
	iFriendsDatabase = getXMLItemValue(ssXMLConfigFile, ssAutoInstance, "Friends_Database").toInt();
	if (iFriendsDatabase < 1) iFriendsDatabase = 4; // override to defualt if 0
	SetDlgItemInt(hWndDlg, IDC_AUTO_FRIENDDB, iFriendsDatabase, TRUE);
	Debug("LoadAllInit - Friends DB assignment is %d", iFriendsDatabase);
	// load src pkg friends
	LoadFriendsDatabase();

	// GalleryStates Auto

	// show list of known routers
	if (ssl_AllRouters.count()>1) SetDlgItemText(hWndDlg, IDC_AUTO_SDIRTR, LPCSTR(ssl_AllRouters.toString(',')));

	if (iNumberActiveUmdDevs>0)
		return TRUE;
	else {
		Debug("LoadAllInit -ERROR- NO MV UMD DEVICES Defined - fix mv_input_outputs for hub");
		return FALSE;
	}
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  FUNCTION: AutomaticRegistrations()
//
//  PURPOSE:register with CSI for gory details
//
//  COMMENTS: 
//			

BOOL AutomaticRegistrations(void)
{
	// register for rtr packager = needs to be all pkgs for virtuals
	Debug("AutoRegistrations - Pkgr rtr %d to %d  ||  pkgr dest stat %d to %d // slots 1 to %d", iPackagerAutoInfos[1], 
		iPackagerAutoInfos[iNumberInfosInBlock], iPackagerDestStatInfos[1], iPackagerDestStatInfos[iNumberInfosInBlock], iInfodriverDivisor);
	for (int iiInfos = 1; iiInfos <= iNumberInfosInBlock; iiInfos++) {
		if (iiInfos < 33) {
			ecCSIClient->regtallyrange(iPackagerAutoInfos[iiInfos], 1, iInfodriverDivisor, INSERT);
			ecCSIClient->regtallyrange(iPackagerDestStatInfos[iiInfos], 1, iInfodriverDivisor, INSERT);
		}
	}
		
	// reg for main NEVION router associated to packager ???
	// OR register JUST FOR RTR DESTS listed in MV devices ONLY  resolve

	int iU = 0;
	// MVs -- just register with required other router dests to simplify the revertive handling and amount of processing
	for (int iD = 1; iD <= iNumberActiveUmdDevs; iD++) {
		CUmdDevice* pUmd = GetValidUmdDevice(iD);
		if (pUmd) {
			// get dests for umds
			for (iU = 1; iU <= pUmd->getNumberOfUMDs(); iU++) {
				int iRtrDev = 0, iSrcDest = 0, iSrcDestType = 0;
				iSrcDestType = pUmd->getUmdTypeAndSrcDestByIndex(iU, &iRtrDev, &iSrcDest);
				if ((iSrcDestType == DEST_MAIN_SDI_INDEX) && (iRtrDev>0) && (iSrcDest>0)) {
					ecCSIClient->regtallyrange(iRtrDev, iSrcDest, iSrcDest, INSERT);
					if (bShowAllDebugMessages) Debug("register %d %d for rtr %d dest %d ", iD, iU, iRtrDev, iSrcDest);
				}
			} // for iU
		} // if pUmd
	} // for iD

		
	// register for (if applic) Studio mixer states --- go thru and find those gpis only linked to src and/or dest
	if (cl_MixerRevertives)
	{
		// go thru and register for mixer device
		for (int iWhichMixer = 1; iWhichMixer<=iNumberKahunaMixers; iWhichMixer++) {
			CStudioRevsData* pMixer = GetMixerByKeyIndex(iWhichMixer);
			if (pMixer) {
				if (pMixer->getMixerDeviceId() > 0) {
					// poll for mixer - needs to return verbose revertives specifying levels onair
					ecCSIClient->regtallyrange(pMixer->getMixerDeviceId(), 1, MAXMIXERENTRIES - 1, INSERT);
					Debug("register for Kahuna mixer %d inputs 1 200 ", pMixer->getMixerDeviceId());
					ecCSIClient->regtallyrange(pMixer->getMixerDeviceId(), 621, 684, INSERT);     // outputs slot allocation as per driver docs
					Debug("register for Kahuna mixer %d outputs 621 - 684 ", pMixer->getMixerDeviceId());
				}
				/* xxx  // ECUT
				if (pMixer->get_ECUT_Device()>0) ecCSIClient->regtallyrange(pMixer->get_ECUT_Device(), pMixer->get_ECUT_Offset(), pMixer->get_ECUT_Offset(), INSERT);
				*/
			}
			else
				Debug("AutoReg - GalleryMixer FAIL - no mixer for %d ", iWhichMixer);
		}
	}

	// regionals -- register for router
	for (int ii = 1; ii <= iNumberRegionals; ii++) {
		CRegionals* pRegion = GetRegionalRecordByKey(ii);
		if (pRegion) {
			// get router 
			CRevsRouterData* pRtr = GetValidRouter(pRegion->iRegionalRouter);
			if (pRtr) {
				ecCSIClient->regtallyrange(pRtr->getRouterNumber(), 1, pRtr->getMaximumDestinations(), INSERT);
				Debug("register for Regionals routers %d -dests- 1 %d ", pRtr->getRouterNumber(), 1, pRtr->getMaximumDestinations());
			}
			else
				Debug("Autoreg Regional %d router NOT FOUND %d ", ii, pRtr->getRouterNumber());
		}
	}
		
	// NOTE MV and tsl devices are fire and forget - do not need to get revs from them

	Debug( "out of AutoRegistrations " );
	return TRUE;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//
// FUNCTION PollAutomaticDrivers();
//
// to query all drivers to get this automatic up to speed - at startup


void PollMixerDevice( int iWhichMixer )
{			
	CStudioRevsData* pMixer = GetMixerByKeyIndex(iWhichMixer);
	if (pMixer) {
		char szCommand[MAX_AUTO_BUFFER_STRING];
		if (pMixer->getMixerDeviceId() > 0) {
			wsprintf(szCommand, "IP %d %d %d", pMixer->getMixerDeviceId(), 1, MAXMIXERENTRIES - 1);
			AddCommandToQue(szCommand, INFODRVCOMMAND, pMixer->getMixerDeviceId(), 0, 0);
			Debug("poll for pcr mixer %d inputs device %d 1 %d ", iWhichMixer, pMixer->getMixerDeviceId(), MAXMIXERENTRIES-1);
			//
			wsprintf(szCommand, "IP %d %d %d", pMixer->getMixerDeviceId(), 621, 684);
			AddCommandToQue(szCommand, INFODRVCOMMAND, pMixer->getMixerDeviceId(), 0, 0);
			Debug("poll for pcr mixer %d outputs device %d 621 684", iWhichMixer, pMixer->getMixerDeviceId());
		}
	}
}


void PollAutomaticDrivers(  )
{
	int  iState=0;
	char szCommand[MAX_AUTO_BUFFER_STRING];
	Debug("Polling Packager %d to %d  ||  pkgr dest stat %d to %d // slots 1 to %d", iPackagerAutoInfos[1],
		iPackagerAutoInfos[iNumberInfosInBlock], iPackagerDestStatInfos[1], iPackagerDestStatInfos[iNumberInfosInBlock], iInfodriverDivisor);
	for (int iiInfos = 1; iiInfos <= iNumberInfosInBlock; iiInfos++) {
		if (iiInfos < 33) {
			wsprintf(szCommand, "IP %d %d %d", iPackagerAutoInfos[iiInfos], 1, iInfodriverDivisor);
			AddCommandToQue(szCommand, INFODRVCOMMAND, 0, 0, 0);
			//
			wsprintf(szCommand, "IP %d %d %d", iPackagerDestStatInfos[iiInfos], 1, iInfodriverDivisor);
			AddCommandToQue(szCommand, INFODRVCOMMAND, 0, 0, 0);
		}
	}
	//
	Debug("Polling router IP explitictly defined dests for MV tiles/ inputs ");
	for (int iD = 1; iD <= iNumberActiveUmdDevs; iD++) {
		CUmdDevice* pUmd = GetValidUmdDevice(iD);
		if (pUmd) {
			// get dests for umds
			for (int iU = 1; iU <= pUmd->getNumberOfUMDs(); iU++) {
				int iRtrDev = 0, iSrcDest = 0, iSrcDestType = 0;
				iSrcDestType = pUmd->getUmdTypeAndSrcDestByIndex(iU, &iRtrDev, &iSrcDest);
				if ((iSrcDestType == DEST_MAIN_SDI_INDEX) && (iRtrDev>0) && (iSrcDest>0))
				{  // only poll for those linked to IP or other SDI routers
					// xxxx need to know if IP - hence  Infopoll else RP for any other routers
					wsprintf(szCommand, "RP %d %d %d", iRtrDev, iSrcDest, iSrcDest);
					AddCommandToQue(szCommand, ROUTERCOMMAND, 0, 0, 0);
				}
			} // for iU
		} // if pUmd
	} // for iD

	// polling regional routers
	// regionals -- poll for router
	Debug("Polling Regional routers - total regionals %d ", iNumberRegionals);
	for (int ii = 1; ii <= iNumberRegionals; ii++) {
		CRegionals* pRegion = GetRegionalRecordByKey(ii);
		if (pRegion) {
			// get router 
			CRevsRouterData* pRtr = GetValidRouter(pRegion->iRegionalRouter);
			if (pRtr) {
				wsprintf(szCommand, "RP %d 1 %d", pRtr->getRouterNumber(), pRtr->getMaximumDestinations());
				AddCommandToQue(szCommand, ROUTERCOMMAND, 0, 0, 0);
				Debug("Polling Regional router %d ", pRtr->getRouterNumber());
			}
			else
				Debug("Polling Regional %d router NOT FOUND %d ", ii, pRtr->getRouterNumber());
		}
	}

	Debug("Polling Studio Mixers ");
	if ((cl_MixerRevertives) && (cl_AllGalleries))
	{
		for (int iM = 1; iM <= iNumberAreaGalleries; iM++) PollMixerDevice(iM);
	}
	
	ProcessNextCommand(1); // start off timer to send out from queue	

}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//
//    INITIAL PROCESSING TO GET AUTO INTO CORRECT STATE
//
//

void CalculateMixerOnAirSources(int iWhichMixer)
{
	CStudioRevsData* pMixer = GetMixerByKeyIndex(iWhichMixer);
	if (pMixer) {
		pMixer->ssl_ListTracedSrcPackages_OnAir.clear();
		pMixer->ssl_ListTracedSrcPackages_ISO.clear();
		bncs_stringlist sslareas = bncs_stringlist("", ',');
		// go thru all divisions - reset areas assigned; then fill area on air list assoc to divisions
		for (int iidiv = 1; iidiv < KAHUNA_MIXER_MAXLEVELS; iidiv++) {
			if ((pMixer->getGalleryDivisionAssignment(iidiv)>0) && (pMixer->getGalleryDivisionAssignment(iidiv) <= iNumberAreaGalleries)) {
				if (sslareas.find(bncs_string(pMixer->getGalleryDivisionAssignment(iidiv))) < 0) {
					sslareas.append(bncs_string(pMixer->getGalleryDivisionAssignment(iidiv)));
					CArea_Gallery* pGall = GetAreaGallery_ByKey(pMixer->getGalleryDivisionAssignment(iidiv));
					if (pGall) {
						pGall->ssl_ThusSrcPackages_OnAir.clear();
						pGall->ssl_ThusSrcPackages_ISO.clear();
					}
				}
			}
		}

		//  GO THRU MIXER INPUTS FIRST, then OUTPUTS below
		// to limit for first 120 at present - for (int iiInp = 1; iiInp < MAXMIXERENTRIES; iiInp++) {
		for (int iiInp = 1; iiInp < 121; iiInp++) {
			// NOW
			if (pMixer->isMixerOnAirNOW(iiInp)) {
				int iType = 0, iDev = 0;
				int iPkgIndx = pMixer->getMixerAssocIndx(iiInp, &iType, &iDev);
				int iDivv = pMixer->getMixerInputLogicalSwitcher(iiInp);
				if (iType == DEST_UMD_TYPE) {
					CDestinationPackages* pDPkg = GetDestinationPackage(iPkgIndx);
					if (pDPkg) {
						//traced for gui mixer
						if ((pDPkg->iTraced_Src_Package > 0) && (pDPkg->iTraced_Src_Package <= iNumberOfUserPackages)) {
							if (pMixer->ssl_ListTracedSrcPackages_OnAir.find(bncs_string(pDPkg->iTraced_Src_Package)) < 0)
								pMixer->ssl_ListTracedSrcPackages_OnAir.append(bncs_string(pDPkg->iTraced_Src_Package));
						}
						// routed / virtuals and traced into full list held by area data class
						for (int ii = 0; ii < pDPkg->ssl_SrcPkgTrace.count(); ii++) {
							int iSrcPkg = pDPkg->ssl_SrcPkgTrace[ii].toInt();
							if ((iSrcPkg>0) && (iSrcPkg <= iNumberOfUserPackages)) {
								// now add indx to area list if shared or division is assigned to area
								if ((iDivv>0) && (iDivv < KAHUNA_MIXER_MAXLEVELS)) {
									int iAssignedArea = pMixer->getGalleryDivisionAssignment(iDivv);
									CArea_Gallery* pGall = GetAreaGallery_ByKey(iAssignedArea);
									if (pGall) {
										if (pGall->ssl_ThusSrcPackages_OnAir.find(bncs_string(pDPkg->ssl_SrcPkgTrace[ii])) < 0) {
											pGall->ssl_ThusSrcPackages_OnAir.append(bncs_string(pDPkg->ssl_SrcPkgTrace[ii]));
											// friends ???
											CSourcePackages* pSrc = GetSourcePackage(pDPkg->ssl_SrcPkgTrace[ii]);
											if (pSrc) {
												if (pSrc->ssl_Assoc_Friends.count() > 0) {
													for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
														if (pGall->ssl_ThusSrcPackages_OnAir.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
															pGall->ssl_ThusSrcPackages_OnAir.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
														}
													} // for iifrnd
												}
											}
										}
									}
								}
								else {
									// shared - so set for all defined areas to this mixer
									for (int iish = 0; iish < sslareas.count(); iish++) {
										CArea_Gallery* pGall2 = GetAreaGallery_ByKey(sslareas[iish].toInt());
										if (pGall2) {
											if (pGall2->ssl_ThusSrcPackages_OnAir.find(bncs_string(pDPkg->ssl_SrcPkgTrace[ii])) < 0) {
												pGall2->ssl_ThusSrcPackages_OnAir.append(bncs_string(pDPkg->ssl_SrcPkgTrace[ii]));
												// friends ???
												CSourcePackages* pSrc = GetSourcePackage(pDPkg->ssl_SrcPkgTrace[ii]);
												if (pSrc) {
													if (pSrc->ssl_Assoc_Friends.count() > 0) {
														for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
															if (pGall2->ssl_ThusSrcPackages_OnAir.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
																pGall2->ssl_ThusSrcPackages_OnAir.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
															}
														} //for iifrnd
													}
												}
											}
										}
									} // for iish
								}
							}
						} // for ii
					}
				}
				else if (iType == SOURCE_UMD_TYPE) {
					if ((iPkgIndx>0) && (iPkgIndx < iNumberOfUserPackages)) {
						if (pMixer->ssl_ListTracedSrcPackages_OnAir.find(bncs_string(iPkgIndx)) < 0) pMixer->ssl_ListTracedSrcPackages_OnAir.append(bncs_string(iPkgIndx));
						// now add indx to area list if shared or division is assigned to area
						if ((iDivv>0) && (iDivv < KAHUNA_MIXER_MAXLEVELS)) {
							int iAssignedArea = pMixer->getGalleryDivisionAssignment(iDivv);
							CArea_Gallery* pGall = GetAreaGallery_ByKey(iAssignedArea);
							if (pGall) {
								if (pGall->ssl_ThusSrcPackages_OnAir.find(bncs_string(iPkgIndx)) < 0) {
									pGall->ssl_ThusSrcPackages_OnAir.append(bncs_string(iPkgIndx));
									// friends ???
									CSourcePackages* pSrc = GetSourcePackage(iPkgIndx);
									if (pSrc) {
										if (pSrc->ssl_Assoc_Friends.count() > 0) {
											for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
												if (pGall->ssl_ThusSrcPackages_OnAir.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
													pGall->ssl_ThusSrcPackages_OnAir.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
												}
											} // for iifrnd
										}
									}
								}
							}
						}
						else {
							// shared - so set for all defined areas to this mixer
							for (int iish = 0; iish < sslareas.count(); iish++) {
								CArea_Gallery* pGall2 = GetAreaGallery_ByKey(sslareas[iish].toInt());
								if (pGall2) {
									if (pGall2->ssl_ThusSrcPackages_OnAir.find(bncs_string(iPkgIndx)) < 0) {
										pGall2->ssl_ThusSrcPackages_OnAir.append(bncs_string(iPkgIndx));
										// friends ???
										CSourcePackages* pSrc = GetSourcePackage(iPkgIndx);
										if (pSrc) {
											if (pSrc->ssl_Assoc_Friends.count() > 0) {
												for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
													if (pGall2->ssl_ThusSrcPackages_OnAir.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
														pGall2->ssl_ThusSrcPackages_OnAir.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
													}
												} // for iifrnd
											}
										}
									}
								}
							} // for iish
						}
					}
				}
			}
			// ISOs on air
			if (pMixer->isMixerOnAirISO(iiInp)) {
				int iType = 0, iDev = 0;
				int iPkgIndx = pMixer->getMixerAssocIndx(iiInp, &iType, &iDev);
				int iDivv = pMixer->getMixerInputLogicalSwitcher(iiInp);
				if (iType == DEST_UMD_TYPE) {
					CDestinationPackages* pDPkg = GetDestinationPackage(iPkgIndx);
					if (pDPkg) {
						//traced for gui mixer
						if ((pDPkg->iTraced_Src_Package > 0) && (pDPkg->iTraced_Src_Package <= iNumberOfUserPackages)) {
							if (pMixer->ssl_ListTracedSrcPackages_ISO.find(bncs_string(pDPkg->iTraced_Src_Package)) < 0)
								pMixer->ssl_ListTracedSrcPackages_ISO.append(bncs_string(pDPkg->iTraced_Src_Package));
						}
						// routed / virtuals and traced into full list held by area data class
						for (int ii = 0; ii < pDPkg->ssl_SrcPkgTrace.count(); ii++) {
							int iSrcPkg = pDPkg->ssl_SrcPkgTrace[ii].toInt();
							if ((iSrcPkg>0) && (iSrcPkg <= iNumberOfUserPackages)) {
								// now add indx to area list if shared or division is assigned to area
								if ((iDivv>0) && (iDivv < KAHUNA_MIXER_MAXLEVELS)) {
									int iAssignedArea = pMixer->getGalleryDivisionAssignment(iDivv);
									CArea_Gallery* pGall = GetAreaGallery_ByKey(iAssignedArea);
									if (pGall) {
										if (pGall->ssl_ThusSrcPackages_ISO.find(bncs_string(pDPkg->ssl_SrcPkgTrace[ii])) < 0) {
											pGall->ssl_ThusSrcPackages_ISO.append(bncs_string(pDPkg->ssl_SrcPkgTrace[ii]));
											// friends ???
											CSourcePackages* pSrc = GetSourcePackage(pDPkg->ssl_SrcPkgTrace[ii]);
											if (pSrc) {
												if (pSrc->ssl_Assoc_Friends.count() > 0) {
													for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
														if (pGall->ssl_ThusSrcPackages_ISO.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
															pGall->ssl_ThusSrcPackages_ISO.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
														}
													} // for iifrnd
												}
											}
										}
									}
								}
								else {
									// shared - so set for all defined areas to this mixer
									for (int iish = 0; iish < sslareas.count(); iish++) {
										CArea_Gallery* pGall2 = GetAreaGallery_ByKey(sslareas[iish].toInt());
										if (pGall2) {
											if (pGall2->ssl_ThusSrcPackages_ISO.find(bncs_string(pDPkg->ssl_SrcPkgTrace[ii])) < 0) {
												pGall2->ssl_ThusSrcPackages_ISO.append(bncs_string(pDPkg->ssl_SrcPkgTrace[ii]));
												// friends ???
												CSourcePackages* pSrc = GetSourcePackage(pDPkg->ssl_SrcPkgTrace[ii]);
												if (pSrc) {
													if (pSrc->ssl_Assoc_Friends.count() > 0) {
														for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
															if (pGall2->ssl_ThusSrcPackages_ISO.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
																pGall2->ssl_ThusSrcPackages_ISO.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
															}
														} //for iifrnd
													}
												}
											}
										}
									} // for
								}
							}
						}
					}
				}
				else if (iType == SOURCE_UMD_TYPE) {
					if ((iPkgIndx>0) && (iPkgIndx <=iNumberOfUserPackages)) {
						if (pMixer->ssl_ListTracedSrcPackages_ISO.find(bncs_string(iPkgIndx)) < 0) pMixer->ssl_ListTracedSrcPackages_ISO.append(bncs_string(iPkgIndx));
						// now add indx to area list if shared or division is assigned to area
						if ((iDivv>0) && (iDivv < KAHUNA_MIXER_MAXLEVELS)) {
							int iAssignedArea = pMixer->getGalleryDivisionAssignment(iDivv);
							CArea_Gallery* pGall = GetAreaGallery_ByKey(iAssignedArea);
							if (pGall) {
								if (pGall->ssl_ThusSrcPackages_ISO.find(bncs_string(iPkgIndx)) < 0) {
									pGall->ssl_ThusSrcPackages_ISO.append(bncs_string(iPkgIndx));
									// friends ???
									CSourcePackages* pSrc = GetSourcePackage(iPkgIndx);
									if (pSrc) {
										if (pSrc->ssl_Assoc_Friends.count() > 0) {
											for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
												if (pGall->ssl_ThusSrcPackages_ISO.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
													pGall->ssl_ThusSrcPackages_ISO.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
												}
											} //for iifrnd
										}
									}
								}
							}
						}
						else {
							// shared - so set for all defined areas to this mixer
							for (int iish = 0; iish < sslareas.count(); iish++) {
								CArea_Gallery* pGall2 = GetAreaGallery_ByKey(sslareas[iish].toInt());
								if (pGall2) {
									if (pGall2->ssl_ThusSrcPackages_ISO.find(bncs_string(iPkgIndx)) < 0) {
										pGall2->ssl_ThusSrcPackages_ISO.append(bncs_string(iPkgIndx));
										// friends ???
										CSourcePackages* pSrc = GetSourcePackage(iPkgIndx);
										if (pSrc) {
											if (pSrc->ssl_Assoc_Friends.count() > 0) {
												for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
													if (pGall2->ssl_ThusSrcPackages_ISO.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
														pGall2->ssl_ThusSrcPackages_ISO.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
													}
												} //for iifrnd
											}
										}
									}
								}
							} // for
						}
					}
				}
			}
			//
		} // for inputs
		// NOW OUTPUTS  1-48 only used in discovery at present -- to speed calculation
		for (int iiOutp = 1; iiOutp < 49; iiOutp++) {
			// NOW
			if (pMixer->isOutputOnAirNOW(iiOutp)) {
				int iType = 0, iDev = 0;
				//was int iPkgIndx = pMixer->getMixerAssocIndx(iiOutp, &iType, &iDev);      
				int iPkgIndx = pMixer->getOutputAssocIndx(iiOutp, &iType, &iDev);      
				int iDivv = pMixer->getMixerOutputLogicalSwitcher(iiOutp);
				if (iType == SOURCE_UMD_TYPE) {
					if ((iPkgIndx>0) && (iPkgIndx <= iNumberOfUserPackages)) {
						if (pMixer->ssl_ListTracedSrcPackages_OnAir.find(bncs_string(iPkgIndx)) < 0) {
							pMixer->ssl_ListTracedSrcPackages_OnAir.append(bncs_string(iPkgIndx));
						}
						// now add indx to area list if shared or division is assigned to area
						if ((iDivv>0) && (iDivv < KAHUNA_MIXER_MAXLEVELS)) {
							int iAssignedArea = pMixer->getGalleryDivisionAssignment(iDivv);
							CArea_Gallery* pGall = GetAreaGallery_ByKey(iAssignedArea);
							if (pGall) {
								if (pGall->ssl_ThusSrcPackages_OnAir.find(bncs_string(iPkgIndx)) < 0) {
									pGall->ssl_ThusSrcPackages_OnAir.append(bncs_string(iPkgIndx));
									// friends ???
									CSourcePackages* pSrc = GetSourcePackage(iPkgIndx);
									if (pSrc) {
										if (pSrc->ssl_Assoc_Friends.count() > 0) {
											for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
												if (pGall->ssl_ThusSrcPackages_OnAir.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
													pGall->ssl_ThusSrcPackages_OnAir.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
												}
											} //for iifrnd
										}
									}
								}
							}
						}
					}
				}
			}
			// ISOs on air ???
			if (pMixer->isOutputOnAirISO(iiOutp)) {
				int iType = 0, iDev = 0;
				int iPkgIndx = pMixer->getOutputAssocIndx(iiOutp, &iType, &iDev);      
				int iDivv = pMixer->getMixerOutputLogicalSwitcher(iiOutp);
				if (iType == SOURCE_UMD_TYPE) {
					if ((iPkgIndx>0) && (iPkgIndx <= iNumberOfUserPackages)) {
						if (pMixer->ssl_ListTracedSrcPackages_ISO.find(bncs_string(iPkgIndx)) < 0) 
							pMixer->ssl_ListTracedSrcPackages_ISO.append(bncs_string(iPkgIndx));
						// now add indx to area list if shared or division is assigned to area
						if ((iDivv>0) && (iDivv < KAHUNA_MIXER_MAXLEVELS)) {
							int iAssignedArea = pMixer->getGalleryDivisionAssignment(iDivv);
							CArea_Gallery* pGall = GetAreaGallery_ByKey(iAssignedArea);
							if (pGall) {
								if (pGall->ssl_ThusSrcPackages_ISO.find(bncs_string(iPkgIndx)) < 0) {
									pGall->ssl_ThusSrcPackages_ISO.append(bncs_string(iPkgIndx));
									// friends ???
									CSourcePackages* pSrc = GetSourcePackage(iPkgIndx);
									if (pSrc) {
										if (pSrc->ssl_Assoc_Friends.count() > 0) {
											for (int iifrnd = 0; iifrnd < pSrc->ssl_Assoc_Friends.count(); iifrnd++) {
												if (pGall->ssl_ThusSrcPackages_ISO.find(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd])) < 0) {
													pGall->ssl_ThusSrcPackages_ISO.append(bncs_string(pSrc->ssl_Assoc_Friends[iifrnd]));
												}
											} //for iifrnd
										}
									}
								}
							}
						}
					}
				}
			}
		} // for outputs

		// exit as on air list for mixer complete

		// comment out debug below eventually to reduce calc overhead processing time
		//Debug("CalcOnAirSrcs for Mixer %d - NOW count %d %s", iWhichMixer, pMixer->ssl_ListTracedSrcPackages_OnAir.count(), LPCSTR(pMixer->ssl_ListTracedSrcPackages_OnAir.toString(',')));
		//Debug("CalcOnAirSrcs for Mixer %d -    ISO  count %d %s", iWhichMixer, pMixer->ssl_ListTracedSrcPackages_ISO.count(), LPCSTR(pMixer->ssl_ListTracedSrcPackages_ISO.toString(',')));
		//for (int iish = 0; iish < sslareas.count(); iish++) {
		//	CArea_Gallery* pGall = GetAreaGallery_ByKey(sslareas[iish].toInt());
		//	if (pGall) {
		//		Debug("CalcOnAirSrcs for area %d %s - NOW count %d %s", sslareas[iish].toInt(), LPCSTR(pGall->ssAreaGallery_Name),
		//			pGall->ssl_ThusSrcPackages_OnAir.count(), LPCSTR(pGall->ssl_ThusSrcPackages_OnAir.toString(',')));
		//		Debug("CalcOnAirSrcs for area %d %s - ISO count %d %s", sslareas[iish].toInt(), LPCSTR(pGall->ssAreaGallery_Name),
		//			pGall->ssl_ThusSrcPackages_ISO.count(), LPCSTR(pGall->ssl_ThusSrcPackages_ISO.toString(',')));
		//	}
		//}

	}
}

//////////////////////////////////////////////////////////////////////////
// recursive function to
// get traced source package for a given dest pkg
//
int TraceSourcePackageVideo(int iStartDestPkg)
{
	// for given dest pkg trace back thru any virtuals until real src pkg found
	// if a virtual has a src sdi defined - then stop at this point as this will video required
	// considered "up-stream" 
	int iRoutedSrcPkg=0;
	int iDestPkg = iStartDestPkg;
	iRecursionTraceCounter++;
	if (iRecursionTraceCounter>500) {
		Debug("TraceSourcePackageVideo - recursive counter >500 - aborting function exection dest pkg %d", iStartDestPkg);
		return iRoutedSrcPkg;
	}
	CDestinationPackages* pDest1 = GetDestinationPackage(iDestPkg);
	if (pDest1) {
		iRoutedSrcPkg = pDest1->iRouted_Src_Package;
		CSourcePackages* pSrc1 = GetSourcePackage(iRoutedSrcPkg);
		if (pSrc1) {				
			ssl_TraceToSource.append(bncs_string(iRoutedSrcPkg));   // this traced stringlist is global -- must be cleared/reset BEFORE this function ever called
			if ((iRoutedSrcPkg>=i_Start_VirtualPackages)&&(iRoutedSrcPkg<=iNumberOfUserPackages)) {
				// virtual src - check for defined video - else get vir dest and carry on trace (recursive)
				int iNextStartIndx = pSrc1->iPackageIndex;
				iRoutedSrcPkg = TraceSourcePackageVideo( iNextStartIndx ); // vir src wraps round into vir dest
			}
			else {	// found real source package so return this index
				return iRoutedSrcPkg;
			}
		}
		else {
			Debug("TraceSourcePackageVideo - invalid src pkg %d - exiting recursion", pSrc1->iPackageIndex );
			return iRoutedSrcPkg;
		}
	}
	else {
		Debug("TraceSourcePackageVideo - invalid dest pkg %d - exiting recursion", pDest1->iPackageIndex );
		return iRoutedSrcPkg;
	}
	//
	return iRoutedSrcPkg;
}


int TraceRegionalPrimaryRouterSource(int iStartRtr, int iStartDest )
{
	// check regional rtr and virtual src - trace back to real  source 
	CRevsRouterData* pRegRtr = GetValidRouter(iStartRtr);
	if (pRegRtr) {
		int iTracDest = iStartDest;
		int iTracSrc = pRegRtr->getRoutedSourceForDestination(iTracDest);
		if (pRegRtr->getRouterStartVirtualSource() > 0) {
			if ((iTracSrc >= pRegRtr->getRouterStartVirtualSource()) && (iTracSrc <= pRegRtr->getMaximumSources())) {
				// convert vir src index to appropriate vir dest 
				int iVirIndx = (iTracSrc - pRegRtr->getRouterStartVirtualSource()) + pRegRtr->getRouterStartVirtualDestination();
				iTracSrc = TraceRegionalPrimaryRouterSource(iStartRtr, iVirIndx );
			}
			// check for wrap src and get routed source to wrap dest -- assumes only ONE Wrap around 
			int iWrapDest = pRegRtr->getDestOfHighwayOrWrap(iTracSrc);
			if (iWrapDest > 0) {
				iTracSrc = pRegRtr->getRoutedSourceForDestination(iWrapDest);
			}
		}
		return iTracSrc;
	}
	else {
		// invalid router
		return UNKNOWNVAL;
	}
}


//////////////////////////////////////////////////////////////////////////////////////////////////

void ProcessFirstPass_Logic( void )
{
	char szCommand[ MAX_AUTO_BUFFER_STRING]="";
	char szData[MAX_AUTO_BUFFER_STRING]="";
	char szData2[MAX_AUTO_BUFFER_STRING]="";
	char szBlank[MAX_AUTO_BUFFER_STRING]="";
	int iExpSrc=0;

	int iIndex;
	// 1. go thru all the dest pkages slots and see what is currently stored to determine routed / traced packages etc
	Debug("ProcFirstPass -- processing traces");
	// now process current package routing
	for (iIndex=1;iIndex<=iNumberOfUserPackages;iIndex++) {
		//Debug("PFP -- 1 -- index %d", iIndex);
		CDestinationPackages* pDstPkg = GetDestinationPackage(iIndex);
		if (pDstPkg) {
			//
			BOOL bVirtualDestPkg = FALSE;
			BOOL bVirtualSrcPkg = FALSE;
			if ((iIndex>=i_Start_VirtualPackages)&&(iIndex<=iNumberOfUserPackages)) bVirtualDestPkg = TRUE;
			//
			int iNewSrcPkg=pDstPkg->iRouted_Src_Package;
			if ((iNewSrcPkg>=i_Start_VirtualPackages)&&(iNewSrcPkg<=iNumberOfUserPackages)) bVirtualSrcPkg = TRUE;
			// assign in src class
			if ((iNewSrcPkg>0) && (iNewSrcPkg <= iPackagerParkSrcePkg)) {
				CSourcePackages* pSrcPkg = GetSourcePackage( iNewSrcPkg );
				if (pSrcPkg) {
					ssl_TraceToSource.clear();
					iRecursionTraceCounter = 0;
					pDstPkg->ssl_SrcPkgTrace.clear();
					int iTracedSrcPkg = TraceSourcePackageVideo(iIndex);
					pDstPkg->iTraced_Src_Package = iTracedSrcPkg;
					if (ssl_TraceToSource.count() > 1) {
						for (int ii = 0; ii < ssl_TraceToSource.count(); ii++) pDstPkg->ssl_SrcPkgTrace.append(ssl_TraceToSource[ii]);
						Debug("PFirstPass -- 1b - so trace src list has count %d %s", ssl_TraceToSource.count(), LPCSTR(ssl_TraceToSource.toString(',')));
					}
					else
						pDstPkg->ssl_SrcPkgTrace.append(bncs_string(iTracedSrcPkg));
				}		
				// if (iNewSrcPkg<=iNumberOfUserPackages) Debug("FirstPass dstpkg %d  rou %d trac %d - %s", iIndex, iNewSrcPkg, pDstPkg->iTraced_Src_Package, LPCSTR(pDstPkg->ssl_SrcPkgTrace.toString(',')));
			}
		} // if pdstPkg
	} // for iDest

	// calc ccu / evs etc and calc tallies ??
	Debug("ProcFirstPass - SKIPPING process ccu rev start- requires ccu auto spec");
	//xxx for (int i = 1; i <= MAX_CCU_ASSIGNMENT; i++) {
	//	ProcessCCURevertive(iCCU_AssignmentsInfo, i);
	//}

	Debug("ProcFirstPass - assigning areas to mixers - based on XML");
	for (int iiar = 1; iiar <= iNumberAreaGalleries; iiar++) {
		CArea_Gallery* pGall = GetAreaGallery_ByKey(iiar);
		if (pGall) {
			CStudioRevsData* pMixer = GetMixerByInstance(pGall->ssAssigned_MixerInstance);
			if (pMixer) {
				int iLogSwi = pGall->iAssigned_LogicalSwitcher;
				if ((iLogSwi > 0) && (iLogSwi < KAHUNA_MIXER_MAXLEVELS)) {
					pMixer->setGalleryDivisionAssignment(iLogSwi, pGall->iAreaGallery_Index);
					Debug("ProcFirstPass area %d %s - assigned to mixer %d division %d ", 
						pGall->iAreaGallery_Index, LPCSTR(pGall->ssAreaGallery_Name), pMixer->getRecordKey(), iLogSwi);
				} 
			}
			else
				Debug("ProcFirstPass -error- area %d %s has invalid mixer ", pGall->iAreaGallery_Index, LPCSTR(pGall->ssAreaGallery_Name));
		}
	}

	// turn off all ccu camera tallies to prevent stuck oness 
	Debug("ProcFirstPass - turning off CCU tallies at startup");
	for (int ii = 1; ii <= iNumberGPIOs; ii++) {
		CGPIODevice* pGPio = GetGPIORecordByKey(ii);
		if (pGPio) {
			char szCommand[MAX_AUTO_BUFFER_STRING] = "";
			wsprintf(szCommand, "IW %d '0' %d", pGPio->iBNCS_GPIO_Device, pGPio->iBNCS_GPIO_Index);
			AddCommandToQue(szCommand, INFODRVCOMMAND, pGPio->iBNCS_GPIO_Device, 0, 0);
			pGPio->iBNCS_LastStateSent = 0;
		}
	}


	Debug("ProcFirstPass - calc onair packages from mixer revs");
	for (int iim = 1; iim <= iNumberKahunaMixers;iim++)
		CalculateMixerOnAirSources(iim);

	int iM=0;
	for (iM = 1; iM <= iNumberKahunaMixers; iM++) {
		CStudioRevsData* pMixer = GetMixerByKeyIndex(iM);
		if (pMixer) {
			ProcessAndUpdateTXMonitors(pMixer, iM );
			UpdateGuiForMixerOnAir( iM );
		}
	}
	
	// any processing for src packages on startup ??? xxx need to set current on air CCUs for STO and other regionals

}


CArea_Gallery* GetAreaGalleryBySlot(int iGivenSlot)
{
	int iCalcIndex = (iGivenSlot - 1) / 10;
	int iModIndx = iGivenSlot % 10;
	// only works for slots 11, 21, 31, 41 etc for each area 1,2,3,4, etc
	if ((iCalcIndex > 0) && (iCalcIndex <= iNumberAreaGalleries)&&(iModIndx==1)) {
		CArea_Gallery* pGall = GetAreaGallery_ByKey(iCalcIndex);
		if (pGall) {
			return pGall;
		}
		else
			Debug("GetAreaGalleryBySlot - no AreaGall record for calc index %d from slot %d", iCalcIndex, iGivenSlot);
	}
	else
		Debug("GetAreaGalleryBySlot - invalid slot %d calc index %d modulus %d", iGivenSlot, iCalcIndex, iModIndx);
	return NULL;
}

int CalculateRandomOffset()
{
	// used to create a time offset before calling TXRX function - to stop multiInfodriver tx clash
	int iRand1 = 0;
	srand(time(0)); // generate random seed point
	iRand1 = (rand() % 2007);
	return (iRand1);
}


////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

//
//  FUNCTION: InfoNotify()
//
//  PURPOSE: Callback function from an infodriver, notifying event
//
//  COMMENTS: the pointer pex references the infodriver which is notifying
//			  iSlot and szSlot are provided for convenience
//			
void InfoNotify(extinfo* pex,UINT iSlot,LPCSTR szSlot)
{

	if (pex&&szSlot) {
		switch (pex->iStatus) {
		case CONNECTED: // this is the "normal" situation
			UpdateCounters();
			if (!SlotChange(pex->iDevice, iSlot, szSlot))
				pex->setslot(iSlot, szSlot);
			break;

		case DISCONNECTED:
			Debug("Infodriver %d has disconnected", pex->iDevice);
			SetDlgItemText(hWndDlg, IDC_INFO_STATUS, " ERROR: Disconnected");
			eiExtInfoId->updateslot(COMM_STATUS_SLOT, "0");	 // error	
			bValidCollediaStatetoContinue = FALSE;
			PostMessage(hWndMain, WM_CLOSE, 0, 0);
			return;
			break;

		case TO_RXONLY:
			if ((pex->iDevice == iDevice) && (iOverallTXRXModeStatus == IFMODE_TXRX) && (iOverallStateJustChanged == 0)) {
				Debug("Infodriver %d received request to go RX Only -- bouncing request ", pex->iDevice);
				if (eiExtInfoId) {
					eiExtInfoId->setmode(IFMODE_TXRX);
					eiExtInfoId->requestmode = TO_TXRX;
				}
				iOverallModeChangedOver = 4;
			}
			break;

		case TO_TXRX:
			if ((pex->iDevice == iDevice) && (iOverallTXRXModeStatus == IFMODE_RXONLY) && (iOverallStateJustChanged == 0)) {
				iOverallStateJustChanged = 1;
				int iTimerOffset = CalculateRandomOffset();
				SetTimer(hWndMain, FORCETXRX_TIMER_ID, iTimerOffset, NULL);
				SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Req to Tx/Rx to main ");
				if (bShowAllDebugMessages) Debug("InfoNotify - force go TXRX - in %d ms", iTimerOffset);
			}
			break;

		case QUERY_TXRX:
		case 2190:
			Debug("Infodriver %d query txrx -- ignoring ", pex->iDevice);
			// ignore this state -- means all left as it was 
			break;

		default:
			Debug("iStatus=%d", pex->iStatus);
		}
	}
	else
		Debug("InfoNotify -error- invalid pex class or szslot ");

}



//
//  FUNCTION: SlotChange()
//
//  PURPOSE: Function called when slot change is notified
//
//  COMMENTS: All we need here are the driver number, the slot, and the new contents
//		
//	RETURNS: TRUE for processed (don't update the slot),
//			 FALSE for ignored (so calling function can just update the slot 
//								like a normal infodriver)
//			
BOOL SlotChange(UINT iDevice,UINT iSlot, LPCSTR szSlot)
{
	/***************************************************
	// CURRENTLY AREA USE OF MIXER LOGICAL SWITCHERS - vai XML in area_galleries / or one day from frames in pcr_mixers
	CArea_Gallery* pGall = GetAreaGalleryBySlot(iSlot);
	if (pGall) {
		// record exists from device types - does it have a function
		// first clear any previous assignment
		Debug("SlotChange %d -%s- for area/Gallery %d %s ", iSlot, szSlot, pGall->iAreaGallery_Index, LPCSTR(pGall->ssAreaGallery_Name));
		if (pGall->ssAssigned_MixerInstance.length() > 0) {
			CStudioRevsData* pMixer = GetMixerByInstance(pGall->ssAssigned_MixerInstance);
			if (pMixer) {
				for (int ii = 1; ii < KAHUNA_MIXER_MAXLEVELS; ii++){
					if (pMixer->getGalleryDivisionAssignment(ii) == pGall->iAreaGallery_Index) {
						Debug("Slot change Clearing mixer %d ass lvl %d  of previous gall assoc %d ", pMixer->getRecordKey(), ii, pMixer->getGalleryDivisionAssignment(ii));
						pMixer->setGalleryDivisionAssignment(ii, 0);
					}
				}
			}
			pGall->ssAssigned_MixerInstance = "";
			// clear list of determined on air packages for this gallery
			pGall->ssl_ThusSrcPackages_OnAir.clear();
		}
		//
		bncs_stringlist ssl_Data = bncs_stringlist(bncs_string(szSlot), ',');
		if (ssl_Data.count() > 1) {
			bncs_string ssInst = ssl_Data[0];
			if (ssInst.length() > 0)  {
				CStudioRevsData* pMixer = GetMixerByInstance(ssInst);
				if (pMixer) {
					pGall->ssAssigned_MixerInstance = ssInst;
					pGall->ssl_ThusSrcPackages_OnAir.clear();
					for (int iicc = 1; iicc<ssl_Data.count(); iicc++) {
						int iMxrDiv = ssl_Data[iicc].toInt();
						if ((iMxrDiv > 0) && (iMxrDiv < KAHUNA_MIXER_MAXLEVELS)) {
							pMixer->setGalleryDivisionAssignment(iMxrDiv, pGall->iAreaGallery_Index);
							Debug("SlotChange slot %d - area %d %s - assigned to mixer %d division %d ", iSlot,
								pGall->iAreaGallery_Index, LPCSTR(pGall->ssAreaGallery_Name), pMixer->getRecordKey(), iMxrDiv);
						}
					} // for iicc
					eiExtInfoId->updateslot(iSlot, szSlot);
				}
				else {
					Debug("SlotChange -err- slot %d %s - invalid mixer - clearing slot", iSlot, szSlot);
					eiExtInfoId->updateslot(iSlot, "");
				}
			}
		}
		// recalc all tallies for new / cleared assignments
		ProcessTalliesForAllDevices();
	}
	else 
	*****************************/
	// all others are ignored
	Debug("Slot %d change ignored - not defined in UMD T device type", iSlot);
	// always return true - updated slots contents under auto control

	return TRUE;

}


////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// 
//  UMD DEVICE FUNCTIONS
//
//////////////////////////////////////////////////////////////////////////

CSourcePackages* GetBestUmdVirSourcePackage(CDestinationPackages* pDest)
{
	CSourcePackages* pRetPkg = NULL;
	if (pDest) {
		pRetPkg = GetSourcePackage(pDest->iRouted_Src_Package);
		if (pDest->iRouted_Src_Package != pDest->iTraced_Src_Package) {
			if (pDest->ssl_SrcPkgTrace.count() > 2) {
				// try to find most applicable virtual as multiple virtuals are in trace - else just use routed pkg
				for (int ii = 0; ii < pDest->ssl_SrcPkgTrace.count(); ii++) {
					int iVirSrcPkg = pDest->ssl_SrcPkgTrace[ii].toInt();
					if ((iVirSrcPkg >= i_Start_VirtualPackages) && (iVirSrcPkg <= iNumberOfUserPackages)) {
						CSourcePackages* pSrc = GetSourcePackage(iVirSrcPkg);
						if (pSrc) {
							if (pSrc->ss_PCR_UMD_Names1.length() > 2) {
								if ((pSrc->ss_PCR_UMD_Names1.find("OS") >= 0) || (pSrc->ss_PCR_UMD_Names1.find("|OS") >= 0) ||
									(pSrc->ss_PackageUserTitle.find("OS") >= 0) || (pSrc->ss_PackageUserTitle.find("|OS") >= 0)) {
									if (bShowAllDebugMessages) Debug("GetBestVirSrcPkg -1- returning virtual %d", iVirSrcPkg);
									return pSrc;
								}
							}
							else {
								if ((pSrc->ss_PackageUserTitle.find("OS") >= 0) || (pSrc->ss_PackageUserTitle.find("|OS") >= 0)) {
									if (bShowAllDebugMessages) Debug("GetBestVirSrcPkg -2- returning virtual %d", iVirSrcPkg);
									return pSrc;
								}
							}
						}
					}
				}
			}
		}
	}
	//
	if (bShowAllDebugMessages) Debug("GetBestVirSrcPkg - returning routed %d", pDest->iRouted_Src_Package);
	return pRetPkg;
}


bncs_string TrimSameGalleryOrReplaceGivenStr( bncs_string ssUMD, int iGallery, bncs_string ssGiven, bncs_string ssReplace )
{
	// remove / replace ssFind first  
	if (ssGiven.length() > 1) {
		int iPos = ssUMD.find(ssGiven, 0, FALSE);
		if (iPos >= 0) {
			// remove found string as in gallery x
			ssUMD.remove(iPos, ssGiven.length());  //  remove given string exactly
			if (ssReplace.length() > 1) {
				ssUMD.insert(iPos, ssReplace);
			}
		}
	}
	// 2nd method for gallery (linked to mixers) remove area name or variants of area name
	CArea_Gallery* pGallery = GetAreaGallery_ByKey(iGallery);
	if (pGallery) {
		bncs_string ssFind = pGallery->ssAreaGallery_Name;
		if (ssFind.length() > 1) {
			int iPos = ssUMD.find(ssFind, 0, FALSE);
			if (iPos >= 0) ssUMD.remove(iPos, ssFind.length() + 1);  //  remove string + space after 
			// variants - underscore in gallery name to space
			ssFind.replace('_', ' ');
			iPos = ssUMD.find(ssFind, 0, FALSE);
			if (iPos >= 0) ssUMD.remove(iPos, ssFind.length() + 1);  //  remove string + space after 
			// variants - space removed from gallery name
			ssFind.replace(" ", "");
			iPos = ssUMD.find(ssFind, 0, FALSE);
			if (iPos >= 0) ssUMD.remove(iPos, ssFind.length() + 1);  //  remove string + space after 
		}
	}
	return ssUMD;
}



void DetermineActualUmdLabels( CUmdDevice* pUmd, int iUMDIndex, bncs_string* ssPrimaryUmd, bncs_string* ssSecondaryUmd, BOOL bPrependQuestionMark )
{
	// part1 is main/override label, part 2 is used only for soft names where applicable
	bncs_string ssCalcLabel="", ssCalcSecond="";
	bncs_string ssSoftName="";
	char szPkg[12]="";
	*ssPrimaryUmd = "";   // main / primary umd
	*ssSecondaryUmd = "";

	int iAssocRtr=0, iRtrSrcDest=0;
	int iUmdType=UNKNOWNVAL;

	if (pUmd) {

		bncs_string ssPrevLabel = pUmd->getAllUmdDetails(iUMDIndex, &iUmdType, &iAssocRtr, &iRtrSrcDest);
		// following used in construncting umd
		bncs_string ssFmtArgs = "";
		bncs_string ssFormat = pUmd->getUMDFormatRules(iUMDIndex, &ssFmtArgs);
		bncs_stringlist ssl_FmtArgs = bncs_stringlist(ssFmtArgs, ':');
		// following used in trimming umd after constructed
		bncs_string ssReplace = "";
		bncs_string ssFindGiven = pUmd->getUMDReplaceRules(iUMDIndex, &ssReplace);

		switch (iUmdType) {

		case FIXED_UMD_TYPE:
		{
							 *ssPrimaryUmd = pUmd->getPrimaryUmdText(iUMDIndex);
		}
			break;

		case SOURCE_UMD_TYPE:
		{
								// find package associated to this src - being just src can only be single construct umd
								CSourcePackages* pSrc = GetSourcePackage(iRtrSrcDest);
								if (pSrc) {
									// get umd
									if ((iRtrSrcDest > 0) && (iRtrSrcDest <= iNumberOfUserPackages)) {
										if ((ssFormat.length() > 0) && (ssFmtArgs.length() > 0)) {
											int idb1 = -1, idb2 = -1;
											bncs_string ss1 = "", ss2 = "";
											if ((ssFormat.find(":") > 0) && (ssl_FmtArgs.count() > 1)) {
												// dual entry format
												ss1 = ExtractStringfromDevIniFile(iPackagerAuto, iRtrSrcDest, ssl_FmtArgs[0].toInt());
												ss2 = ExtractStringfromDevIniFile(iPackagerAuto, iRtrSrcDest, ssl_FmtArgs[1].toInt());
												ssCalcLabel = bncs_string("%1%2%3").arg(ss1.replace("|", " ")).arg(ss_UMD_Join).arg(ss2.replace("|", " "));
											}
											else {
												// single entry 
												ss1 = ExtractStringfromDevIniFile(iPackagerAuto, iRtrSrcDest, ssl_FmtArgs[0].toInt());
												ssCalcLabel = bncs_string("%1").arg(ss1.replace("|", " "));
											}
										}
										if ((ssCalcLabel.length() < 2) && (pSrc->ss_PCR_UMD_Names1.length()>0)) {
												ssCalcLabel = bncs_string("%1").arg(pSrc->ss_PCR_UMD_Names1.replace("|", " "));
										}
										else {
											// use traced package name as ultimate default if rest are blank
											ssCalcLabel = bncs_string("%1").arg(pSrc->ss_PackageUserTitle.replace("|", " "));
										}
									}
									else if (iRtrSrcDest == iPackagerBarsSrcePkg) {
										ssCalcLabel = " BARS ";
									}
									else if (iRtrSrcDest == iPackagerParkSrcePkg) {
										ssCalcLabel = "- - -";
									}
									if (bShowAllDebugMessages) Debug("DetUmd %d - pkg src- label is %s ", iUMDIndex, LPCSTR(ssCalcLabel));
									// trim now
									*ssPrimaryUmd = TrimSameGalleryOrReplaceGivenStr(ssCalcLabel, pUmd->getUMDDeviceAreaGallery(iUMDIndex), ssFindGiven, ssReplace);
									//
									if (pUmd->getUMDDeviceAreaCode(iUMDIndex) == MV_AREA_MCR) {
										*ssSecondaryUmd = bncs_string("%1").arg(pSrc->ss_MCR_UMD_Names.replace("|", " "));
									}
								}
								else
									Debug("DetermineActualUmdLabels -1- ERROR no srcpkg class for %d", iRtrSrcDest);
		}
			break;

			//
		case DEST_UMD_TYPE:
		{
							  // dest umd type
							  CSourcePackages* pRouSrc = NULL;     // looking for virtuals
							  CSourcePackages* pTracSrc = NULL;    // ultimate src pkg
							  CDestinationPackages* pDest = GetDestinationPackage(iRtrSrcDest);
							  if (pDest) {
								  int iRoutedSrcPkg = pDest->iRouted_Src_Package;
								  int iReqVisionSrcPkg = pDest->iTraced_Src_Package;
								  // use VISION FROM routed package / else traced pkg umd label
								  if ((pDest->iVisionFrom_Src_Package >= i_Start_VirtualPackages) && (pDest->iVisionFrom_Src_Package <= iNumberOfUserPackages)) {
									  iReqVisionSrcPkg = pDest->iVisionFrom_Src_Package;    // Virtual Pkg override in play from packager status revertive
								  }
								  pTracSrc = GetSourcePackage(iReqVisionSrcPkg);
								  // get most applicable routed src pkg if many virtuals are in play for dest pkg
								  pRouSrc = GetBestUmdVirSourcePackage(pDest);
								  if (pRouSrc&&pTracSrc) {
									  // // could check for actual video correctly routed -- see older umd-ts
									  if ((ssFormat.length() > 0) && (ssFmtArgs.length() > 0)) {
										  int idb1 = -1, idb2 = -1;
										  bncs_string ss1 = "", ss2 = "";
										  if ((ssFormat.find(":") > 0) && (ssl_FmtArgs.count() > 1)) {
											  // dual entry format - if routed == traced then just do single variant
											  if (iRoutedSrcPkg != pDest->iTraced_Src_Package) {
												  // most likely due to a virtual in route - so use both
												  ss1 = ExtractStringfromDevIniFile(iPackagerAuto, iRoutedSrcPkg, ssl_FmtArgs[0].toInt());
												  ss2 = ExtractStringfromDevIniFile(iPackagerAuto, iReqVisionSrcPkg, ssl_FmtArgs[1].toInt());
												  if (iReqVisionSrcPkg == iPackagerBarsSrcePkg) ss2 = "BARS";
												  if (iReqVisionSrcPkg == iPackagerParkSrcePkg) ss2 = "---";
												  if (ss_UMD_Join.length()>0)
													  ssCalcLabel = bncs_string("%1%2%3").arg(ss1.replace("|", " ")).arg(ss_UMD_Join).arg(ss2.replace("|", " "));
												  else
													  ssCalcLabel = bncs_string("%1 %2").arg(ss1.replace("|", " ")).arg(ss2.replace("|", " "));
												  if (bShowAllDebugMessages) Debug("DetUmd -%d- dual vir + trac pkg dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
											  }
											  else {
												  // no virtual in path traced == routed so - do just use src pkg -- no point in having mv dest pkg name in umd
												  //ss1 = ExtractStringfromDevIniFile(iPackagerAuto, iRoutedSrcPkg, 3); // only use dest pkg umd field from db3 if there is an entry ???
												  ss2 = ExtractStringfromDevIniFile(iPackagerAuto, iReqVisionSrcPkg, ssl_FmtArgs[1].toInt());
												  if (iReqVisionSrcPkg == iPackagerBarsSrcePkg) ss2 = "BARS";
												  if (iReqVisionSrcPkg == iPackagerParkSrcePkg) ss2 = " ";
												  if (ss1.length() > 1) {
													  if (ss_UMD_Join.length()>0)
														  ssCalcLabel = bncs_string("%1%2%3").arg(ss1.replace("|", " ")).arg(ss_UMD_Join).arg(ss2.replace("|", " "));
													  else
														  ssCalcLabel = bncs_string("%1 %2").arg(ss1.replace("|", " ")).arg(ss2.replace("|", " "));
													  if (bShowAllDebugMessages) Debug("DetUmd -%d- dual destdb3 + traced pkg dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
												  }
												  else {
													  ssCalcLabel = bncs_string("%1 ").arg(ss2.replace("|", " "));
													  if (bShowAllDebugMessages) Debug("DetUmd -%d- dual singl traced pkg dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
												  }
											  }
										  }
										  else {
											  // single entry 
											  ss1 = ExtractStringfromDevIniFile(iPackagerAuto, iReqVisionSrcPkg, ssl_FmtArgs[0].toInt());
											  if (iReqVisionSrcPkg == iPackagerBarsSrcePkg) ss1 = "BARS";
											  if (iReqVisionSrcPkg == iPackagerParkSrcePkg) ss1 = "---";
											  ssCalcLabel = bncs_string("%1").arg(ss1.replace("|", " "));
											  if (bShowAllDebugMessages) Debug("DetUmd -%d- singl traced pkg dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
										  }
									  }
									  else {
										  // create defualt format dest : srce if vir   else   just src if routed == traced
										  if (iRoutedSrcPkg != pDest->iTraced_Src_Package) {
											  // most likely due to a virtual in route - so use both
											  bncs_string ss1 = "";
											  bncs_string ss2 = "";
											  if (pRouSrc->ss_PCR_UMD_Names1.length()>0) {
												  ss1 = bncs_string("%1").arg(pRouSrc->ss_PCR_UMD_Names1.replace("|", " "));
											  }
											  else { // use traced package name as ultimate default if rest are blank
												  ss1 = bncs_string("%1").arg(pRouSrc->ss_PackageUserTitle.replace("|", " "));
											  }
											  if (pTracSrc->ss_PCR_UMD_Names1.length()>0) {
												  ss2 = bncs_string("%1").arg(pTracSrc->ss_PCR_UMD_Names1.replace("|", " "));
											  }
											  else { // use traced package name as ultimate default if rest are blank
												  ss2 = bncs_string("%1").arg(pTracSrc->ss_PackageUserTitle.replace("|", " "));
											  }
											  // build string now
											  if (ss_UMD_Join.length()>0)
												  ssCalcLabel = bncs_string("%1%2%3").arg(ss1).arg(ss_UMD_Join).arg(ss2);
											  else
												  ssCalcLabel = bncs_string("%1 %2").arg(ss1).arg(ss2);
											  if (bShowAllDebugMessages) Debug("DetUmd -%d- default vir + trac pkg dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
										  }
										  else {
											  if (pTracSrc->ss_PCR_UMD_Names1.length()>0) {
												  ssCalcLabel = bncs_string("%1").arg(pTracSrc->ss_PCR_UMD_Names1.replace("|", " "));
											  }
											  else {
												  // use traced package name as ultimate default if rest are blank
												  ssCalcLabel = bncs_string("%1").arg(pTracSrc->ss_PackageUserTitle.replace("|", " "));
											  }
											  if (bShowAllDebugMessages) Debug("DetUmd -%d- default single umd pkg dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
										  }
									  }
									  // remove / replace PCR A if in gallery A etc...
									  *ssPrimaryUmd = TrimSameGalleryOrReplaceGivenStr(ssCalcLabel, pUmd->getUMDDeviceAreaGallery(iUMDIndex), ssFindGiven, ssReplace);
									  //
									  if (pUmd->getUMDDeviceAreaCode(iUMDIndex) == MV_AREA_MCR) {
										  *ssSecondaryUmd = bncs_string("%1").arg(pTracSrc->ss_MCR_UMD_Names.replace("|", " "));
									  }
								  }
								  else
									  Debug("DetermineActualUmdLabels -2b- ERROR - no srcpkg trac %d / Vfrom %d / rou %d class for destpkg %d", 
										pDest->iTraced_Src_Package, iReqVisionSrcPkg, iRoutedSrcPkg, iRtrSrcDest);
							  }
							  else
								  Debug("DetermineActualUmdLabels -2- no destpkg class for %d", iRtrSrcDest);
		}
			break;

		case SRCE_MAIN_SDI_INDEX:
		{
									// find package associated to this src - being just src can only be single construct umd
									CRevsRouterData* pRtr = GetValidRouter(iAssocRtr);
									if (pRtr) {
										// get umd
										bncs_string ss1 = "", ss2 = "";
										if ((iRtrSrcDest > 0) && (iRtrSrcDest <= pRtr->getMaximumSources())) {
											if ((ssFormat.length() > 0) && (ssFmtArgs.length() > 0)) {
												int idb1 = -1, idb2 = -1;
												if ((ssFormat.find(":") > 0) && (ssl_FmtArgs.count() > 1)) {
													// dual entry format
													ss1 = ExtractStringfromDevIniFile(iAssocRtr, iRtrSrcDest, ssl_FmtArgs[0].toInt());
													ss2 = ExtractStringfromDevIniFile(iAssocRtr, iRtrSrcDest, ssl_FmtArgs[1].toInt());
													if (ss_UMD_Join.length()>0)
														ssCalcLabel = bncs_string("%1%2%3").arg(ss1.replace("|", " ")).arg(ss_UMD_Join).arg(ss2.replace("|", " "));
													else
														ssCalcLabel = bncs_string("%1 %2").arg(ss1.replace("|", " ")).arg(ss2.replace("|", " "));
													if (bShowAllDebugMessages) Debug("DetUmd %d - dual rtr src- label is %s ", iUMDIndex, LPCSTR(ssCalcLabel));
												}
												else {
													// single entry 
													ss1 = ExtractStringfromDevIniFile(iAssocRtr, iRtrSrcDest, ssl_FmtArgs[0].toInt());
													ssCalcLabel = bncs_string("%1").arg(ss1.replace("|", " "));
													if (bShowAllDebugMessages) Debug("DetUmd %d - sing rtr src- label is %s ", iUMDIndex, LPCSTR(ssCalcLabel));
												}
											}
											if (ssCalcLabel.length() < 1) {
												// as label empty - single entry - use default db
												ss1 = ExtractStringfromDevIniFile(iAssocRtr, iRtrSrcDest, RTR_SOURCE_NAME_DB);
												ssCalcLabel = bncs_string("%1").arg(ss1.replace("|", " "));
												if (bShowAllDebugMessages) Debug("DetUmd %d - def rtr src- label is %s ", iUMDIndex, LPCSTR(ssCalcLabel));
											}
										}
										// trim now
										*ssPrimaryUmd = TrimSameGalleryOrReplaceGivenStr(ssCalcLabel, pUmd->getUMDDeviceAreaGallery(iUMDIndex), ssFindGiven, ssReplace);
									}
									else
										Debug("DetermineActualUmdLabels -1- no rtr class for %d %d", iAssocRtr, iRtrSrcDest);
		}
			break;

		case DEST_MAIN_SDI_INDEX:
		{
									CRevsRouterData* pRtr = GetValidRouter(iAssocRtr);
									if (pRtr) {
										bncs_string ss1 = "", ss2 = "";
										int iActSrc = pRtr->getRoutedSourceForDestination(iRtrSrcDest);
										if ((ssFormat.length() > 1) && (ssFmtArgs.length() > 1)) {
											int idb1 = -1, idb2 = -1;
											if ((ssFormat.find(":") > 0) && (ssl_FmtArgs.count() > 1)) {
												// dual entry format
												ss1 = ExtractStringfromDevIniFile(iAssocRtr, iRtrSrcDest, ssl_FmtArgs[0].toInt());
												ss2 = ExtractStringfromDevIniFile(iAssocRtr, iActSrc, ssl_FmtArgs[1].toInt());
												if (ss_UMD_Join.length()>0)
													ssCalcLabel = bncs_string("%1%2%3").arg(ss1.replace("|", " ")).arg(ss_UMD_Join).arg(ss2.replace("|", " "));
												else
													ssCalcLabel = bncs_string("%1 %2").arg(ss1.replace("|", " ")).arg(ss2.replace("|", " "));
												if (bShowAllDebugMessages) Debug("DetUmd -%d- dual rtr dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
											}
											else {
												// single entry 
												ss1 = ExtractStringfromDevIniFile(iAssocRtr, iActSrc, ssl_FmtArgs[0].toInt());
												ssCalcLabel = bncs_string("%1").arg(ss1.replace("|", " "));
												if (bShowAllDebugMessages) Debug("DetUmd -%d- singl rtr dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
											}
										}
										if (ssCalcLabel.length() < 1) {
											// create defualt format dest : srce
											ss1 = ExtractStringfromDevIniFile(iAssocRtr, iRtrSrcDest, RTR_DEST_NAME_DB);
											ss2 = ExtractStringfromDevIniFile(iAssocRtr, iActSrc, RTR_SOURCE_NAME_DB);
											if (ss_UMD_Join.length()>0)
												ssCalcLabel = bncs_string("%1%2%3").arg(ss1.replace("|", " ")).arg(ss_UMD_Join).arg(ss2.replace("|", " "));
											else
												ssCalcLabel = bncs_string("%1 %2").arg(ss1.replace("|", " ")).arg(ss2.replace("|", " "));
											if (bShowAllDebugMessages) Debug("DetUmd -%d- default rtr dest = %s", iUMDIndex, LPCSTR(ssCalcLabel));
										}
										// remove / replace PCR A if in gallery A etc...
										*ssPrimaryUmd = TrimSameGalleryOrReplaceGivenStr(ssCalcLabel, pUmd->getUMDDeviceAreaGallery(iUMDIndex), ssFindGiven, ssReplace);
										//
									}
									else
										Debug("DetermineActualUmdLabels -1- no rtr class for %d %d", iAssocRtr, iRtrSrcDest);
		}
			break;

		} // switch 

	}

}


bncs_string DetermineMixerTXUMDLabel( CSourcePackages* pRou, int iGallery, int iLowestInput )
{
	bncs_string ssLabel="";

	if ((iGallery>=1)&&(iGallery<=iNumberAreaGalleries)) {
		CStudioRevsData* pMixer = GetMixerByKeyIndex(iGallery);
		if (pMixer) {
			// if mixer is NOT in ecut then try label from name from router
			// XXXX redo if (pMixer->get_ECUT_Status()==0) ssLabel = pMixer->getMixerSourceName(iLowestInput);
		}
	}
	//Debug("DetermineMixerTXUMDLabel gallery %d lowest %d MIXER name (%s)", iGallery, iLowestInput, LPCSTR(ssLabel));

	BOOL bGetPackagerLabel=FALSE;
	if ((ssLabel.length()>0)&&(ssLabel.length()<3)) {
		int iVal = ssLabel.toInt();
		if ((iVal>=1)&&(iVal<=99)) bGetPackagerLabel = TRUE; // get packager if single or two digit number ( ie cameras )
	}
	else if (ssLabel.length()==0) 
		bGetPackagerLabel = TRUE;  // get packager label if length is zero

	// if no label from mixer name revertive then try packager
	if (bGetPackagerLabel) {
		if (pRou) {
			bncs_string ssTemp = pRou->ss_PackageFixedTitle.replace('|',' ').simplifyWhiteSpace();
			ssLabel = TrimSameGalleryOrReplaceGivenStr(ssTemp, iGallery, "", "" );
		}
		else {
			Debug("DetermineMixerTXlabel -ERROR- no src pkg for gallery %d lowest %d",iGallery, iLowestInput);
		}
	}

	if ((ssLabel.length()>0)&&(ssLabel.length()<16)) 
	{
		// centre label for eggbox
		bncs_string ssTemp1 = ssLabel;
		float result = (float)(16-ssTemp1.length()) / (float)2;
		int padlength = (int)result;
		if ((padlength>0)&&(padlength<16)) {
			ssLabel=""; // reset as for statement will build label afresh
			for (int i=0;i<padlength;i++) ssLabel.append(' ');
			ssLabel.append(ssTemp1);
		}
	}
	//Debug( "DetermineMixerTXUMDLabel gallery %d lowest %d final name (%s)", iGallery, iLowestInput, LPCSTR(ssLabel));

	return ssLabel;
}


void SendMixerTXUmdLabel( int iWhichGallery )
{
	/********* XXXX redo
	CStudioRevsData* pMixer = GetMixerByKeyIndex(iWhichGallery);
	if (pMixer) {
		pMixer->set_EggboxTimerState(FALSE);
		// mon tx
		if (bShowAllDebugMessages) Debug("SendMixerTXUmdLabel - Updating gallery %d TXMon to %s", iWhichGallery, LPCSTR(pMixer->get_MixerTX_UMDLabel()));
		int iBNCSdev = pMixer->get_MON_TX_Device();
		int iBNCSoff = pMixer->get_MON_TX_Offset();
		// send out immediately - using tsl
		bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pMixer->get_MixerTX_UMDLabel()).arg(iBNCSoff + 1);
		if (iBNCSdev>0) SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
		//
		// mon pv
		if (bShowAllDebugMessages) Debug("SendMixerTXUmdLabel - Updating gallery %d PVMon to %s", iWhichGallery, LPCSTR(pMixer->get_MixerPV_UMDLabel()));
		iBNCSdev = pMixer->get_MON_PV_Device();
		iBNCSoff = pMixer->get_MON_PV_Offset();
		// send out immediately  - using tsl 
		if (iBNCSdev > 0) {
			ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pMixer->get_MixerPV_UMDLabel()).arg(iBNCSoff + 1);
			SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
		}
		//
		// SCR monitor if present
		iBNCSdev = pMixer->get_SCR_TX_Device();
		iBNCSoff = pMixer->get_SCR_TX_Offset();		
		// send out immediately
		if (iBNCSdev>0) {
			ssNewCmd1 = bncs_string( "IW %1 '%2' %3" ).arg(iBNCSdev).arg(pMixer->get_MixerTX_UMDLabel()).arg(iBNCSoff+1);
			SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
		}
		//
	}
	else
		Debug( "SendMixerTXLabel -ERROR- no StudioMixer class passed");
		****************/
}


CUmdDevice* GetValidUmdDevice( int iIndx )
{
	if ((iIndx>0) && (iIndx <= iNumberActiveUmdDevs) && (cl_AllUmdDevices)) {
		// get device
		bncs_string ssDevice = ssl_AllUMDDevices[iIndx-1];
		map<bncs_string,CUmdDevice*>::iterator itp;
		if (ssDevice.length()>0) {
			itp =  cl_AllUmdDevices->find(ssDevice);
			if (itp != cl_AllUmdDevices->end())	return itp->second;
		}
		// alternate way
		itp = cl_AllUmdDevices->begin();
		while (itp!=cl_AllUmdDevices->end()) {
			if (itp->second) {
				if (itp->second->getListBoxEntry()==(iIndx-1)) return itp->second;
			}
			itp++;
		} // while
	}
	return NULL;
}


CUmdDevice* GetUmdDeviceRecord( bncs_string ssKey )
{
	if (cl_AllUmdDevices) {
		map<bncs_string, CUmdDevice*>::iterator itp;
		itp = cl_AllUmdDevices->find(ssKey);
		if (itp != cl_AllUmdDevices->end()) {
			return itp->second;
		}
		else {
			Debug("GetUMDRecord -- ERROR - no entry for %s ", LPCSTR(ssKey));
		}
	}
	return NULL;
}


//
//   ultimate function to calc umd display and send cmds to hardware drivers
//
void DetermineAndSendUMDTallies( CUmdDevice* pUmd, int iIndex )  
{
	// 
	if (pUmd) {
		// determine tally state for left and right side and then set for device
		int iSDType=0, iMVRtr=0, iMVFeed=0;
		pUmd->getAllUmdDetails( iIndex, &iSDType, &iMVRtr, &iMVFeed  );
		int iDevType = pUmd->getUMDDeviceType();
		// get dev and slot and send command
		int iBNCSdev = pUmd->getBNCSDriver();
		int iBNCSoff = pUmd->getDriverOffset();

		if (iDevType == TAG_MV) {
			// tallies for tag mv device 
			bncs_string ssPrevCmd1 = pUmd->getRecentTallyCommand(iIndex, LEFT_TALLY);
			bncs_string ssPrevCmd2 = pUmd->getRecentTallyCommand(iIndex, RIGHT_TALLY);
			// calc tally value for red and green
			int iLeftal = pUmd->getTallyState(iIndex, LEFT_TALLY);
			int iRightal = pUmd->getTallyState(iIndex, RIGHT_TALLY);
			int iumdindex = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET;
			bncs_string ssLeftRed = "#FF0000";
			bncs_string ssRightGreen = "#00FF00";
			bncs_string ssTallyOff = "#000000";
			bncs_string ssTalString = ssTallyOff;
			if (iLeftal == TALLY_ON) ssTalString = ssLeftRed;    //red colour
			// left tally
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(ssTalString).arg(iBNCSoff + iumdindex + 5);
			if ((ssPrevCmd1.find(ssNewCmd1)<0) || (bForceUMDUpdate)) {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentTallyCommand(iIndex, ssNewCmd1, LEFT_TALLY);
				//if (bShowAllDebugMessages) 	Debug("DetAndSendUMDTali - TAG TALLY %d (dev:%d slot:%d ) tally updated %d ", iIndex, iBNCSdev, iBNCSoff, iUmdTal );
			}
			// right tally
			ssTalString = ssTallyOff;
			if (iRightal >= TALLY_ON) ssTalString = ssRightGreen;  // green colour for ISOs now
			bncs_string ssNewCmd2 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(ssTalString).arg(iBNCSoff + iumdindex + 6);
			if ((ssPrevCmd2.find(ssNewCmd2)<0) || (bForceUMDUpdate)) {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentTallyCommand(iIndex, ssNewCmd2, RIGHT_TALLY);
				//if (bShowAllDebugMessages) 	Debug("DetAndSendUMDTali - TAG TALLY %d (dev:%d slot:%d ) tally updated %d ", iIndex, iBNCSdev, iBNCSoff, iUmdTal );
			}

		}
		else if ((iDevType==TSL_UMD_TYPE_16)||(iDevType==TSL_V5_DRIVER_10)) {
			// tallies for tsl V5 device 
			bncs_string ssPrevCmd1 = pUmd->getRecentTallyCommand(iIndex, LEFT_TALLY); 
			bncs_string ssPrevCmd2 = pUmd->getRecentTallyCommand(iIndex, RIGHT_TALLY);
			bncs_string ssPrevCmd3 = pUmd->getRecentTallyCommand(iIndex, THIRD_TALLY);
			// calc tally value for red and green
			int iLeftal = pUmd->getTallyState(iIndex, LEFT_TALLY);
			int iRightal = pUmd->getTallyState(iIndex, RIGHT_TALLY);
			int iThirdtal = pUmd->getTallyState(iIndex, THIRD_TALLY);
			int iumdindex = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5);

			if (iLeftal == TALLY_ON) iLeftal = TSL_CUE_LEFTON;    //red colour
			if (iRightal >= TALLY_ON) iRightal = TSL_CUE_RIGHTON;  // green colour
			if (iThirdtal >= TALLY_ON) iThirdtal = TSL_CUE_TEXTON;  // amber colour
			// left tally
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(iLeftal).arg(iBNCSoff + iumdindex + 5);
			if ((ssPrevCmd1.find(ssNewCmd1)<0) || (bForceUMDUpdate)) {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentTallyCommand(iIndex, ssNewCmd1, LEFT_TALLY);
				//if (bShowAllDebugMessages) 	Debug("DetAndSendUMDTali - TSL TALLY %d (dev:%d slot:%d ) tally updated %d ", iIndex, iBNCSdev, iBNCSoff, iUmdTal );
			}
			// right tally
			bncs_string ssNewCmd2 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(iRightal).arg(iBNCSoff + iumdindex + 10);
			if ((ssPrevCmd2.find(ssNewCmd2)<0) || (bForceUMDUpdate)) {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentTallyCommand(iIndex, ssNewCmd2, RIGHT_TALLY);
				//if (bShowAllDebugMessages) 	Debug("DetAndSendUMDTali - TSL TALLY %d (dev:%d slot:%d ) tally updated %d ", iIndex, iBNCSdev, iBNCSoff, iUmdTal );
			}

			// text tally
			bncs_string ssNewCmd3 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(iThirdtal).arg(iBNCSoff + iumdindex + 4);
			if ((ssPrevCmd3.find(ssNewCmd3)<0) || (bForceUMDUpdate)) {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd3), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd3), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentTallyCommand(iIndex, ssNewCmd3, THIRD_TALLY);
				//if (bShowAllDebugMessages) 	Debug("DetAndSendUMDTali - TSL TALLY %d (dev:%d slot:%d ) tally updated %d ", iIndex, iBNCSdev, iBNCSoff, iUmdTal );
			}

		}
		else if (iDevType==KALEIDO_MVIP) {
			// tallies for tsl V5 device 
			bncs_string ssPrevCmd1 = pUmd->getRecentTallyCommand(iIndex, LEFT_TALLY);
			bncs_string ssPrevCmd2 = pUmd->getRecentTallyCommand(iIndex, RIGHT_TALLY);
			bncs_string ssPrevCmd3 = pUmd->getRecentTallyCommand(iIndex, THIRD_TALLY);
			// calc tally value for red and green
			int iLeftal = pUmd->getTallyState(iIndex, LEFT_TALLY);
			int iRightal = pUmd->getTallyState(iIndex, RIGHT_TALLY);
			int iThirdtal = pUmd->getTallyState(iIndex, THIRD_TALLY);

			if (iLeftal == 1) iLeftal = TSL_CUE_LEFTON;    //red colour
			if (iRightal >= 1) iRightal = TSL_CUE_RIGHTON;  // green colour
			// left tally
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(iLeftal).arg(iBNCSoff + iIndex + 200);
			if ((ssPrevCmd1.find(ssNewCmd1)<0) || (bForceUMDUpdate)) {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentTallyCommand(iIndex, ssNewCmd1, LEFT_TALLY);
				//if (bShowAllDebugMessages) 	Debug("DetAndSendUMDTali - TSL TALLY %d (dev:%d slot:%d ) tally updated %d ", iIndex, iBNCSdev, iBNCSoff, iUmdTal );
			}
			// right tally
			bncs_string ssNewCmd2 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(iRightal).arg(iBNCSoff + iIndex + 300);
			if ((ssPrevCmd2.find(ssNewCmd2)<0) || (bForceUMDUpdate)) {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentTallyCommand(iIndex, ssNewCmd2, RIGHT_TALLY);
				//if (bShowAllDebugMessages) 	Debug("DetAndSendUMDTali - TSL TALLY %d (dev:%d slot:%d ) tally updated %d ", iIndex, iBNCSdev, iBNCSoff, iUmdTal );
			}
			// third tally alarm 
			bncs_string ssNewCmd3 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(iThirdtal).arg(iBNCSoff + iIndex);
			if ((ssPrevCmd3.find(ssNewCmd3)<0) || (bForceUMDUpdate)) {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd3), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd3), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentTallyCommand(iIndex, ssNewCmd3, THIRD_TALLY);
			}
		}

	}
}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// UMD LABELS


void SendUmdLabel( CUmdDevice* pUmd, int iIndex )
{
	char szSrcName[128]="", szSoftName[128]="";
	if (pUmd) {
		// get dev and slot and send command
		int iBNCSdev = pUmd->getBNCSDriver();
		int iBNCSoff = pUmd->getDriverOffset();
		// send umd labels for tsl, kmx  --   of 2 tier umds for mcr - just send primary in this case
		int iType = pUmd->getUMDDeviceType();

		if (iType == TAG_MV) {
			// compare prev and new commands - only send out if different or forcing an update
			int iumdIndex = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 1;
			bncs_string ssPrevCmd1 = pUmd->getRecentUmdCommand(iIndex, PRIMARY_UMD);
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pUmd->getPrimaryUmdText(iIndex)).arg(iBNCSoff + iumdIndex);
			if ((ssPrevCmd1.find(ssNewCmd1)<0) || (bForceUMDUpdate))  {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentUmdCommand(iIndex, ssNewCmd1, PRIMARY_UMD);
			}
			//  MCR umds are 2 liners
			if (pUmd->getUMDDeviceAreaCode(iIndex) == MV_AREA_MCR) {
				//
				iumdIndex = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 3;
				bncs_string ssPrevCmd2 = pUmd->getRecentUmdCommand(iIndex, SECONDARY_UMD);
				bncs_string ssNewCmd2 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pUmd->getSecondUmdText(iIndex)).arg(iBNCSoff + iumdIndex);
				if ((ssPrevCmd2.find(ssNewCmd2)<0) || (bForceUMDUpdate))  {
					if (bForceUMDUpdate) {	// during startup and device refresh
						AddCommandToQue(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev, 0, 0);
					}
					else {	// send command immediately 
						SendCommandImmediately(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev);
					}
					pUmd->setRecentUmdCommand(iIndex, ssNewCmd2, SECONDARY_UMD);
				}
			}
		}

		else if ((iType == TSL_V5_DRIVER_10) || (iType == TSL_UMD_TYPE_16)) {
			// compare prev and new commands - only send out if different or forcing an update
			int iumdIndex = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 1;
			bncs_string ssPrevCmd1 = pUmd->getRecentUmdCommand(iIndex, PRIMARY_UMD);
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pUmd->getPrimaryUmdText(iIndex)).arg(iBNCSoff + iumdIndex);
			if ((ssPrevCmd1.find(ssNewCmd1)<0) || (bForceUMDUpdate))  {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0);
				}
				else {	// send command immediately 
					SendCommandImmediately(LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev);
				}
				pUmd->setRecentUmdCommand(iIndex, ssNewCmd1, PRIMARY_UMD);
			}
		}
		else if (iType == KALEIDO_MVIP) {
			// compare prev and new commands - only send out if different or forcing an update --  
			bncs_string ssPrevCmd1 = pUmd->getRecentUmdCommand(iIndex, PRIMARY_UMD);
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pUmd->getPrimaryUmdText(iIndex)).arg(iBNCSoff + iIndex + 100);
			if ((ssPrevCmd1.find(ssNewCmd1)<0)||(bForceUMDUpdate))  {
				if (bForceUMDUpdate) {	// during startup and device refresh
					AddCommandToQue( LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev, 0, 0 );
				}
				else {	// send command immediately 
					SendCommandImmediately( LPCSTR(ssNewCmd1), INFODRVCOMMAND, iBNCSdev );
				}
				pUmd->setRecentUmdCommand( iIndex, ssNewCmd1, PRIMARY_UMD );

				//  MCR umds are 2 liners
				if (pUmd->getUMDDeviceAreaCode(iIndex) == MV_AREA_MCR) {
					//
					int iumdIndex = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 3;
					bncs_string ssPrevCmd2 = pUmd->getRecentUmdCommand(iIndex, SECONDARY_UMD);
					bncs_string ssNewCmd2 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pUmd->getPrimaryUmdText(iIndex)).arg(iBNCSoff + iumdIndex);
					if ((ssPrevCmd2.find(ssNewCmd2)<0) || (bForceUMDUpdate))  {
						if (bForceUMDUpdate) {	// during startup and device refresh
							AddCommandToQue(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev, 0, 0);
						}
						else {	// send command immediately 
							SendCommandImmediately(LPCSTR(ssNewCmd2), INFODRVCOMMAND, iBNCSdev);
						}
						pUmd->setRecentUmdCommand(iIndex, ssNewCmd2, SECONDARY_UMD);
					}
				}
			}
		}
	}
}


void ApplySmartUMDTruncation2( bncs_string *ssCalcLabel )
{
	bncs_string ssInLabel = *ssCalcLabel;
	int iterationCount = 0, iEndLength=0;
	do {
		iterationCount++;
		int iStartLength = ssInLabel.length();
		bncs_stringlist ssList = bncs_stringlist( ssInLabel, ' ');
		bncs_stringlist ssLengths = bncs_stringlist("0,0,0,0,0,0,0,0,0,0,0,0", ',');
		// load lengths of words
		int iWords=0, iPosLongestWord=0, iLongestWord=0;
		for (iWords=0;iWords<ssList.count();iWords++) {
			int iL = ssList[iWords].length();
			ssLengths[iWords] = bncs_string(iL);
			if (iL>=iLongestWord) {
				iLongestWord = iL;
				iPosLongestWord = iWords;
			}
		}
		// if one very long word then chop middle out of it

		// else go thru words and cut chars from the longest words
		for (iWords=0;iWords<ssList.count();iWords++) {
			bncs_string ssTmp;
			if ((iWords>1)&&(iWords<ssList.count())) {
				// trim chars off words in middle of string down to min of 4 
				if (ssList[iWords].length()>5) {
					if (iPosLongestWord==iWords) {
						ssTmp = ssList[iWords].left(ssLengths[iWords].toInt()-4);
						ssTmp.append("~");
						ssList[iWords] = ssTmp;
					}
					else {
						if (iterationCount>2) {
							ssTmp = ssList[iWords].left(ssLengths[iWords].toInt()-1);
							ssList[iWords] = ssTmp;
						}
					}
				}
			}
			else {
				// only trim first and last words if longer than 4 chars - only after several iterations to trim other words
				if ((ssList[iWords].length()>4)&&(iterationCount>3)) {
					bncs_string ssTmp = ssList[iWords].left(ssLengths[iWords].toInt()-1);
					ssList[iWords] = ssTmp;
				}
			}
		}
		// rebuild the string and check length
		ssInLabel = ssList.toString(' ');
		iEndLength = ssInLabel.length();
		if (bShowAllDebugMessages) Debug( "Smart2 - iter %d : string:%s len:%d", iterationCount, LPCSTR(ssInLabel), iEndLength);
	} while ((iEndLength>i_Max_UMD_Length)&&(iterationCount<8));
	
	if (bShowAllDebugMessages) Debug( "SmartTruncation2 - start:%s end:%s ", LPCSTR( *ssCalcLabel ), LPCSTR(ssInLabel) );
	*ssCalcLabel = ssInLabel;

}


void ProcLabelsForGivenDevice( CUmdDevice* pUmd )
{
	bncs_string ssFirstLabel="", ssSecondLabel="";

	if (pUmd) {
		// get src / dests / fixed for umds
		// now package based
		for (int iU=1;iU<=pUmd->getNumberOfUMDs();iU++) {
			bncs_string ssPrevLabel = pUmd->getPrimaryUmdText( iU ); 
			bncs_string ssPrev2ndLbl = pUmd->getSecondUmdText(iU );
			// process changed label
			if (pUmd->getUmdTypeGivenIndex(iU)==FIXED_UMD_TYPE) { 
				// get string and display 
				SendUmdLabel( pUmd, iU );
			}
			else { 
				DetermineActualUmdLabels( pUmd, iU, &ssFirstLabel, &ssSecondLabel, FALSE );
				if (ssFirstLabel.length()>i_Max_UMD_Length) ApplySmartUMDTruncation2( &ssFirstLabel );
				if (ssSecondLabel.length()>i_Max_UMD_Length) ApplySmartUMDTruncation2( &ssSecondLabel );
				pUmd->storePrimaryUmdName(iU, ssFirstLabel );
				pUmd->storeSecondUmdName(iU, ssSecondLabel );
				if ((ssFirstLabel != ssPrevLabel) || (ssSecondLabel != ssPrev2ndLbl) || (bForceUMDUpdate)) {
					SendUmdLabel(pUmd, iU);
				}
			}
		} // for iU

		// do all processing first as this is very fast in cpu terms
		if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
	}
}


void ProcessLabelsForAllDevices( )
{
	// go thru all devices and update for  all  labels
	for (int iD=0; iD<ssl_AllUMDDevices.count(); iD++ )  {
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev=ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord( ssCurDev );
		if ( pUmd ) {
			ProcLabelsForGivenDevice( pUmd );
		} 
	} // for ID
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// UMD GPO TALLY LIGHTS -- CCUs etc
//
//

BOOL IsSrcPkgOnAirElseWhere( int iSrcPkg, int iActiveGallery )
{
	// see if src package is active in any galleries other than the active one
	BOOL bOnAir = FALSE;
	if ((iSrcPkg>0)&&(iSrcPkg<=iNumberOfUserPackages)&&(iActiveGallery>0)) {
		// go thru all galleries
		map<int, CArea_Gallery*>::iterator itp = cl_AllGalleries->begin();
		while (itp != cl_AllGalleries->end()) {
			CArea_Gallery* pArea = itp->second;
			if (pArea) {
				if (pArea->iAreaGallery_Index != iActiveGallery) {
					if (pArea->ssl_ThusSrcPackages_OnAir.find(bncs_string(iSrcPkg)) >= 0) {
						bOnAir = TRUE;
						// once found to be on air in another gallery - then return
						return bOnAir;
					}
				}
			}
			itp++;
		} // while
	}
	return bOnAir;
}


void GetTallyStateForSourcePackage(int iSrcPkg, int iUMDGallery, int *iRedTally, int *iGrnTally, int *iTxtTally)
{
	*iRedTally=TALLY_OFF;
	*iGrnTally = TALLY_OFF;
	*iTxtTally = TALLY_OFF;
	CArea_Gallery* pAreaGall = GetAreaGallery_ByKey(iUMDGallery);
	if (pAreaGall) {
		if ((iSrcPkg>0) && (iSrcPkg <= iNumberOfUserPackages)) {
			// is this src package calculated to be on air in that gallery ?
			if (pAreaGall->ssl_ThusSrcPackages_OnAir.find(bncs_string(iSrcPkg)) >= 0) {
				if (bShowAllDebugMessages) Debug("GetTallyStateForSourcePackage - src pkg %d on air in assoc gallery %d", iSrcPkg, iUMDGallery);
				*iRedTally = TALLY_ON;
			}
			// is src on air in list for ISOs 
			if (pAreaGall->ssl_ThusSrcPackages_ISO.find(bncs_string(iSrcPkg)) >= 0) {
				if (bShowAllDebugMessages) Debug("GetTallyStateForSourcePackage - src pkg %d ISO  in assoc gallery %d", iSrcPkg, iUMDGallery);
				*iGrnTally = TALLY_ON;
			}
			// check for onair in other gallery - so green enabled
			if (IsSrcPkgOnAirElseWhere(iSrcPkg, iUMDGallery)) {
				if (bShowAllDebugMessages) Debug("GetTallyStateForSourcePackage - src pkg %d on air in other gallery %d", iSrcPkg, iUMDGallery);
				*iTxtTally = TALLY_ON;
			}
		}
	}
}

void GetTallyStateForDestinationPackage(int iDestPkg, int iUMDGallery, int *iRedTally, int *iGrnTally, int *iTxtTally)
{
	*iRedTally=TALLY_OFF;
	*iGrnTally=TALLY_OFF;
	*iTxtTally = TALLY_OFF;
	// if traced source pkg is used by mixer dest pkg see if src is on air 
	//
	CDestinationPackages* pDstPkg = GetDestinationPackage( iDestPkg );
	if (pDstPkg) {
		int iDestPkgGallery = pDstPkg->iLinkedAreaGallery;
		//
		CArea_Gallery* pGallery = GetAreaGallery_ByKey(iDestPkgGallery);
		if (pGallery) {
			// check against calculated on air src pkgs  
			if ((pDstPkg->iRouted_Src_Package>0) && (pDstPkg->iRouted_Src_Package <= iNumberOfUserPackages)) {
				//
				// red -- if galleries the same 
				if (iDestPkgGallery==iUMDGallery) {
					// check ON AIR for NOW first for routed package
					int iFVal = pGallery->ssl_ThusSrcPackages_OnAir.find(bncs_string(pDstPkg->iRouted_Src_Package));
					if ((iFVal >= 0) && (bShowAllDebugMessages) ) {
						Debug("GetTallyStateForDestPkage - area %d  on air srcs %s ", iDestPkgGallery, LPCSTR(pGallery->ssl_ThusSrcPackages_OnAir.toString(',')));
						Debug("GetTallyStateForDestPkage - dest pkg %d  routed source %d ", iDestPkg, pDstPkg->iRouted_Src_Package);
						Debug("GetTallyStateForDestPkage find value %d ", iFVal);
					}
					if (pGallery->ssl_ThusSrcPackages_OnAir.find(bncs_string(pDstPkg->iRouted_Src_Package)) >= 0) {
						if (bShowAllDebugMessages) Debug("GetTallyStateForDestPkage - dest pkg %d ROUTED src %d NOW in assoc gallery %d",
							pDstPkg->iPackageIndex, pDstPkg->iRouted_Src_Package, pDstPkg->iLinkedAreaGallery );
						*iRedTally = TALLY_ON;
					}
					else if ((pDstPkg->iRouted_Src_Package >= i_Start_VirtualPackages) && (pDstPkg->iRouted_Src_Package <= iNumberOfUserPackages)) {
						// new addition -- check if routed src pkg is a vir - then see if traced source pkg is on air for gallery
						// XXX may need to check for OS lines even with park /bars cut up on mixer
						if (pGallery->ssl_ThusSrcPackages_OnAir.find(bncs_string(pDstPkg->iTraced_Src_Package)) >= 0) {
							if (bShowAllDebugMessages) Debug("GetTallyStateForDestPkage - dest pkg %d TRACED src %d NOW in assoc gallery %d",
								pDstPkg->iPackageIndex, pDstPkg->iTraced_Src_Package, pDstPkg->iLinkedAreaGallery);
							*iRedTally = TALLY_ON;
						}
					}
					//ISO level tally
					if (pGallery->ssl_ThusSrcPackages_ISO.find(bncs_string(pDstPkg->iRouted_Src_Package)) >= 0) {
						if (bShowAllDebugMessages) Debug("GetTallyStateForDestPkage - dest pkg %d ROUTED src %d ISO in assoc gallery %d",
							pDstPkg->iPackageIndex, pDstPkg->iRouted_Src_Package, pDstPkg->iLinkedAreaGallery);
						*iGrnTally = TALLY_ON;
					}
					else if ((pDstPkg->iRouted_Src_Package >= i_Start_VirtualPackages) && (pDstPkg->iRouted_Src_Package <= iNumberOfUserPackages)) {
						// new addition -- check if routed src pkg is a vir - then see if traced source pkg is on air for gallery
						if (pGallery->ssl_ThusSrcPackages_ISO.find(bncs_string(pDstPkg->iTraced_Src_Package)) >= 0) {
							if (bShowAllDebugMessages) Debug("GetTallyStateForDestPkage - dest pkg %d TRACED src %d ISO in assoc gallery %d",
								pDstPkg->iPackageIndex, pDstPkg->iTraced_Src_Package, pDstPkg->iLinkedAreaGallery);
							*iGrnTally = TALLY_ON;
						}
					}
				}
				// check for onair in other gallery - so 3rd level enabled // was green in old days
				// was if (IsSrcPkgOnAirElseWhere(pDstPkg->iTraced_Src_Package, iDestPkgGallery) ) {
				if (IsSrcPkgOnAirElseWhere(pDstPkg->iTraced_Src_Package, iUMDGallery) ) {
					if (bShowAllDebugMessages) Debug("GetTallyStateForDestPkage - dest pkg %d trac src %d on air in ANOTHER gallery so 3rd Tally ON ", iDestPkg, pDstPkg->iTraced_Src_Package);
					*iTxtTally = TALLY_ON;
				}
			}
		}
	}
}


BOOL IsRouterSourceOnAirElseWhere(CRevsRouterData* pRtr, int iSource, int iActiveGallery)
{
	// see if src package is active in any galleries other than the active one
	if (pRtr) {
		if ((iSource>0) && (iSource <= pRtr->getMaximumSources()) && (iActiveGallery>0)) {
			// go thru all galleries
			map<int, CArea_Gallery*>::iterator itp = cl_AllGalleries->begin();
			while (itp != cl_AllGalleries->end()) {
				CArea_Gallery* pGallery = itp->second;
				if (pGallery) {
					if (pGallery->iAreaGallery_Index != iActiveGallery) {
						if (pGallery->ssl_ThusSrcRtrSDI_OnAir.find(bncs_string("%1|%2").arg(pRtr->getRouterNumber()).arg(iSource)) >= 0) {
							// once found to be on air in another gallery - then return
							return TRUE;
						}
					}
				}
				itp++;
			} // while
		}
	}
	return FALSE;
}


void GetTallyStateforRouterSource(CRevsRouterData* pRtr,  int iSource, int iUMDGallery, int *iRedTally, int *iGrnTally, int *iTxtTally)
{
	*iRedTally = TALLY_OFF;
	*iGrnTally = TALLY_OFF;
	*iTxtTally = TALLY_OFF;
	if (pRtr) {
		// see if pkg is linked to gallery mixer and that package is on-air  
		CArea_Gallery* pAreaGall = GetAreaGallery_ByKey(iUMDGallery);
		if (pAreaGall) {
			if ((iSource>0) && (iSource <= pRtr->getMaximumSources())) {
				// is this src calculated to be on air in that gallery ?
				if (pAreaGall->ssl_ThusSrcRtrSDI_OnAir.find(bncs_string("%1|%2").arg(pRtr->getRouterNumber()).arg(iSource)) >= 0) {
					if (bShowAllDebugMessages) Debug("GetTallyStateForRtrSrce - RTR src %d on air in ASSOC gallery %d", iSource, iUMDGallery);
					*iRedTally = TALLY_ON;
				}
				// is rtr src on air in list for ISOs 
				if (pAreaGall->ssl_ThusSrcRtrSDI_ISO.find(bncs_string("%1|%2").arg(pRtr->getRouterNumber()).arg(iSource)) >= 0) {
					if (bShowAllDebugMessages) Debug("GetTallyStateForRtrSrce - RTR Src %d ISO  in ASSOC gallery %d", iSource, iUMDGallery);
					*iGrnTally = TALLY_ON;
				}
				// check for onair in other gallery - so green enabled
				if (IsRouterSourceOnAirElseWhere(pRtr, iSource, iUMDGallery) ) {
					if (bShowAllDebugMessages) Debug("GetTallyStateForRtrSrce - RTR src %dd on air in OTHER gallery %d", iSource, iUMDGallery);
					*iTxtTally = TALLY_ON;
				}
			}
		}
	}
}


void GetTallyStateforRouterDestination(CRevsRouterData* pRtr, int iDestination, int iUMDGallery, int *iRedTally, int *iGrnTally, int *iTxtTally)
{
	*iRedTally = TALLY_OFF;
	*iGrnTally = TALLY_OFF;
	*iTxtTally = TALLY_OFF;
	// if traced source pkg is used by mixer dest pkg see if src is on air 
	//
	if ((pRtr)&&(iDestination>0)) {
		//
		CArea_Gallery* pGallery = GetAreaGallery_ByKey(iUMDGallery);
		if (pGallery) {
			// check against calculated on air src   
			int iSource = pRtr->getRoutedSourceForDestination(iDestination);
			if (pGallery->ssl_ThusSrcRtrSDI_OnAir.find(bncs_string("%1|%2").arg(pRtr->getRouterNumber()).arg(iSource)) >= 0) {
				if (bShowAllDebugMessages) Debug("GetTallyStateForRtrDest - rtr dest %d ROUTED src %d NOW in assoc gallery",iDestination, iSource);
				*iRedTally = TALLY_ON;
			}
			// ISO level tally
			if (pGallery->ssl_ThusSrcRtrSDI_ISO.find(bncs_string("%1|%2").arg(pRtr->getRouterNumber()).arg(iSource)) >= 0) {
				if (bShowAllDebugMessages) Debug("GetTallyStateForRtrDest - rtr dest %d ROUTED src %d ISO in assoc gallery", iDestination, iSource);
				*iGrnTally = TALLY_ON;
			}
			if (IsRouterSourceOnAirElseWhere(pRtr, iSource, iUMDGallery) ) {
				if (bShowAllDebugMessages) Debug("GetTallyStateForRtrDest - rtr dest %d src %d on air in ANOTHER gallery so GRN ON ", iDestination, iSource);
				*iTxtTally = TALLY_ON;
			}
		}
	}
}

void  ProcAllTalliesForGivenDevice( CUmdDevice* pUmd )
{
	if (pUmd) {
		// get src / dests / fixed for umds
		// now package based
		for (int iU=1;iU<=pUmd->getNumberOfUMDs();iU++) {
			int iUmdType = 0;
			int iAssocRtr = 0;
			int iSrcDest = 0;

			int iRedState= pUmd->getTallyState(iU, LEFT_TALLY );   // keeps current setting as starting point
			int iGrnState = pUmd->getTallyState(iU, RIGHT_TALLY);  // keeps current setting as starting point
			int iTxtState = pUmd->getTallyState(iU, THIRD_TALLY);  // keeps current setting as starting point

			bncs_string ssUmd = pUmd->getAllUmdDetails( iU, &iUmdType, &iAssocRtr, &iSrcDest ); 
			int iGalleryDevice = pUmd->getUMDDeviceAreaGallery( iU );

			BOOL bRedChange = FALSE, bGrnChange = FALSE, bTxtChange = FALSE;
			
			if (iUmdType==FIXED_UMD_TYPE) { 
				// get current state of Studio mixer source 
				if ((iAssocRtr == iPackagerAuto) && (iSrcDest>0)) {
					CSourcePackages* pSrc = GetSourcePackage(iSrcDest);
					if (pSrc) {
						GetTallyStateForSourcePackage(iSrcDest, iGalleryDevice, &iRedState, &iGrnState, &iTxtState);
						bRedChange = pUmd->storeTallyState(iU, LEFT_TALLY, iRedState);
						bGrnChange = pUmd->storeTallyState(iU, RIGHT_TALLY, iGrnState);
						bTxtChange = pUmd->storeTallyState(iU, THIRD_TALLY, iTxtState);
					}
				}
				else if ((iAssocRtr>0) && (iSrcDest>0)) {
					CRevsRouterData* pRtr = GetValidRouter(iAssocRtr);
					if (pRtr) {
						GetTallyStateforRouterSource(pRtr, iSrcDest, iGalleryDevice, &iRedState, &iGrnState, &iTxtState);
						bRedChange = pUmd->storeTallyState(iU, LEFT_TALLY, iRedState);
						bGrnChange = pUmd->storeTallyState(iU, RIGHT_TALLY, iGrnState);
						bTxtChange = pUmd->storeTallyState(iU, THIRD_TALLY, iTxtState);
					}
				}
			}
			else if (iUmdType==SOURCE_UMD_TYPE) { 
				// get current state of Studio mixer source 
				if ((iAssocRtr == iPackagerAuto) && (iSrcDest>0)) {
					CSourcePackages* pSrc = GetSourcePackage(iSrcDest);
					if (pSrc) {
						GetTallyStateForSourcePackage(iSrcDest, iGalleryDevice, &iRedState, &iGrnState, &iTxtState);
						bRedChange = pUmd->storeTallyState(iU, LEFT_TALLY, iRedState);
						bGrnChange = pUmd->storeTallyState(iU, RIGHT_TALLY, iGrnState);
						bTxtChange = pUmd->storeTallyState(iU, THIRD_TALLY, iTxtState);
					}
				}
			}
			else if (iUmdType==DEST_UMD_TYPE) { 
				// get current state of Studio mixer source for
				//     (a) dest pkg
				//     (b) src pkg routed to dest pkg 
				if ((iAssocRtr == iPackagerAuto) && (iSrcDest > 0)) {
					CDestinationPackages* pDest = GetDestinationPackage(iSrcDest);
					if (pDest) {
						GetTallyStateForDestinationPackage(iSrcDest, iGalleryDevice, &iRedState, &iGrnState, &iTxtState);
						bRedChange = pUmd->storeTallyState(iU, LEFT_TALLY, iRedState);
						bGrnChange = pUmd->storeTallyState(iU, RIGHT_TALLY, iGrnState);
						bTxtChange = pUmd->storeTallyState(iU, THIRD_TALLY, iTxtState);
					}
				}
			}
			else if (iUmdType == DEST_MAIN_SDI_INDEX) {
				if ((iAssocRtr>0) && (iSrcDest>0)) {
					CRevsRouterData* pRtr = GetValidRouter(iAssocRtr);
					if (pRtr) {
						GetTallyStateforRouterDestination(pRtr, iSrcDest, iGalleryDevice, &iRedState, &iGrnState, &iTxtState);
						bRedChange = pUmd->storeTallyState(iU, LEFT_TALLY, iRedState);
						bGrnChange = pUmd->storeTallyState(iU, RIGHT_TALLY, iGrnState);
						bTxtChange = pUmd->storeTallyState(iU, THIRD_TALLY, iTxtState);
					}
					else
						Debug("ProcAllTallies - SDI dest - no rtr for %d ", iAssocRtr);
				}
			}
			else if (iUmdType == SRCE_MAIN_SDI_INDEX) {
				if ((iAssocRtr>0) && (iSrcDest>0)) {
					CRevsRouterData* pRtr = GetValidRouter(iAssocRtr);
					if (pRtr) {
						GetTallyStateforRouterSource(pRtr, iSrcDest, iGalleryDevice, &iRedState, &iGrnState, &iTxtState);
						bRedChange = pUmd->storeTallyState(iU, LEFT_TALLY, iRedState);
						bGrnChange = pUmd->storeTallyState(iU, RIGHT_TALLY, iGrnState);
						bTxtChange = pUmd->storeTallyState(iU, THIRD_TALLY, iTxtState);
					}
				}
			}

			// update device if a detected change in state
			if (bRedChange || bGrnChange || bTxtChange || bForceUMDUpdate) DetermineAndSendUMDTallies(pUmd, iU);
			
		} // for iU

		// do all processing 
		if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
	}
}


void ProcessTalliesForAllDevices( )
{

	// for better use of cpu time etc - get lists of all src pkages on-air in all galleries - just once - then use this info when processing thru all mvs/etc
	CalculateCurrentOnAirIndices();
	for (int i = 1; i<=iNumberKahunaMixers; i++) UpdateGuiForMixerOnAir(i);

	// set start state for Regional CCUs
	ProcessAllRegionalCameraTallies(bForceUMDUpdate);

	// go thru all devices and update for  all  tallies
	for (int iD=0; iD<ssl_AllUMDDevices.count(); iD++ )  {
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev=ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord( ssCurDev );
		if ( pUmd ) {
			ProcAllTalliesForGivenDevice( pUmd );
		} 
	} // for ID
}


////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
//   CSI CLIENT FUNCTIONS FOR ROUTER REVERTIVES MESSAGES

void StoreValidRouterRevertive( int iRtrNum, int iRtrDest, int iRtrSource, int *iPrevSource )
{
	*iPrevSource = UNKNOWNVAL;
	CRevsRouterData* pRtr = GetValidRouter(iRtrNum);
	if (pRtr) {
		// some form of sdi /  video router rev  // xxx if other than stockholm then may not have virtuals etc
		*iPrevSource = TraceRegionalPrimaryRouterSource(iRtrNum, iRtrDest);
		pRtr->storeGRDRevertive(iRtrDest, iRtrSource, "" );
		// check for any overrides
	}
}


/////////////////////////////////////////////////////////////////////////
//
void ProcessValidRouterRevertive(int iRtrNum, int iRtrDest, int iPrevSource)
{
	// function works for all SDI routers 
	bncs_string ssCalcLabel = "", ssSrcName = "", ssSoftName = "", ssAuxLabel = "";

	CRevsRouterData* pRtr = GetValidRouter(iRtrNum);
	if (pRtr) {
		int iNewSrc = pRtr->getRoutedSourceForDestination(iRtrDest);
		BOOL bUpdateGui = FALSE;
		if (iNewSrc>0) {
			// if dest linked to any mixer - recalc
			if (pRtr->isDestinationLinkedToAMixer(iRtrDest)) {
				CalculateCurrentOnAirIndices();
				for (int i=1;i<=iNumberKahunaMixers;i++) UpdateGuiForMixerOnAir(i);
			}

			// is rtr associated to a region - eg Stockholm and dest is in list or virtual dest on rtr
			CRegionals* pRegion = GetRegionByRouterNumber(iRtrNum);
			if (pRegion) {
				//Debug("ProcRtrRev - rtr %d == region %d", iRtrNum, pRegion->iRegionalKey);
				ProcessARegionTallies(pRegion->iRegionalKey, FALSE, iRtrDest, iPrevSource);
			}

			// go thru all devices and update for  all  labels using the changed src
			for (int iD = 0; iD<ssl_AllUMDDevices.count(); iD++)  {
				// get device record and store rev - recalc on new src and send to hardware
				bncs_string ssCurDev = ssl_AllUMDDevices[iD];
				CUmdDevice* pUmd = GetUmdDeviceRecord(ssCurDev);
				if (pUmd) {

					// update tallies for this umd device
					ProcAllTalliesForGivenDevice(pUmd);
					if (ssCurrentChosenDevice.find(ssCurDev) >= 0) bUpdateGui = TRUE;

					// update umds - rtr dest could be used repeatedly on device - so check all 
					for (int iU = 1; iU <= pUmd->getNumberOfUMDs(); iU++) {
						int iUmdRtr = 0, iUmdSrcDest = 0;
						int iUmdType = pUmd->getUmdTypeAndSrcDestByIndex(iU, &iUmdRtr, &iUmdSrcDest);
						// if pkg dest -- is sdi dest that of one rtrdest here too -- or dest pkg that equals
						if ( ((iUmdRtr == iRtrNum) && (iUmdSrcDest == iRtrDest)) && ((iUmdType == DEST_MAIN_SDI_INDEX) || (iUmdType == DEST_UMD_TYPE)) )
						{
							bncs_string ssUmdLabel = "", ssSecondLabel = "";
							bncs_string ssPrevLabel = pUmd->getPrimaryUmdText(iU);
							bncs_string ssPrev2ndLbl = pUmd->getSecondUmdText(iU);
							//
							DetermineActualUmdLabels(pUmd, iU, &ssUmdLabel, &ssSecondLabel, FALSE);
							if (ssUmdLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssUmdLabel);
							if (ssSecondLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssSecondLabel);
							//
							pUmd->storePrimaryUmdName(iU, ssUmdLabel);
							pUmd->storeSecondUmdName(iU, ssSecondLabel);
							//if ((ssUmdLabel.find(ssPrevLabel) < 0) || (ssSecondLabel.find(ssPrev2ndLbl) < 0) || (bForceUMDUpdate )) {
							if ((ssUmdLabel != ssPrevLabel) || (ssSecondLabel != ssPrev2ndLbl) || (bForceUMDUpdate )) {
								SendUmdLabel(pUmd, iU);
								if (ssCurrentChosenDevice.find(ssCurDev) >= 0) bUpdateGui = TRUE;
							}
						}
					} // for int iU
				}
			} // for iD
			// update gui as required
			if (bUpdateGui ) DisplayUmdDeviceData();
		}
	}
	else
		Debug("ProcessRtrRev -ERROR- no router in class for %d ", iRtrNum);
}



//////////////////////////////////////////////////////////////////////////////////////
//
//

void ProcessRouterModifyForSourcePackageChange(int iChangedSourcePkg, BOOL bLabelChanged)
{
	BOOL bUpdateGui = FALSE;
	// go thru all devices and update for UMD labels IF linked to this src package
	for (int iD = 0; iD < ssl_AllUMDDevices.count(); iD++)  {
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev = ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord(ssCurDev);
		if (pUmd) {
			// tallies should not be affected by RM command -- only changed either from gpi state change or packager revertive change of dest routing
			// now sort out any changes to umd labels
			for (int iU = 1; iU <= pUmd->getNumberOfUMDs(); iU++) {

				BOOL bCont = FALSE;
				if (pUmd->getUmdTypeGivenIndex(iU) == SOURCE_UMD_TYPE) {
					//was  if (pUmd->getLinkedToPackage(iU)==iChangedSourcePkg) 
					bCont = TRUE;  // always true now for source packages
				}
				else if (pUmd->getUmdTypeGivenIndex(iU) == DEST_UMD_TYPE) {
					// does dest package have this src package routed to it ??
					CDestinationPackages* pDest = GetDestinationPackage(pUmd->getUMDSrceDestIndex(iU));
					if (pDest) {
						if ((pDest->iTraced_Src_Package == iChangedSourcePkg) || (pDest->iRouted_Src_Package == iChangedSourcePkg)) bCont = TRUE;
					}
				}

				if (bCont) {
					//
					bncs_string ssUmdLabel = "", ssSecondLabel = "";
					bncs_string ssPrevLabel = pUmd->getPrimaryUmdText(iU);
					bncs_string ssPrev2ndLbl = pUmd->getSecondUmdText(iU);
					//
					DetermineActualUmdLabels(pUmd, iU, &ssUmdLabel, &ssSecondLabel, FALSE);
					if (ssUmdLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssUmdLabel);
					if (ssSecondLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssSecondLabel);
					//
					pUmd->storePrimaryUmdName(iU, ssUmdLabel);
					pUmd->storeSecondUmdName(iU, ssSecondLabel);
					//if ((ssUmdLabel.find(ssPrevLabel) < 0) || (ssSecondLabel.find(ssPrev2ndLbl) < 0) || (bForceUMDUpdate )) {
					if ((ssUmdLabel!=ssPrevLabel) || (ssSecondLabel!=ssPrev2ndLbl) || (bForceUMDUpdate )) {
						SendUmdLabel(pUmd, iU);
						if (ssCurrentChosenDevice.find(ssCurDev) >= 0) bUpdateGui = TRUE;
					}
				}
			} // for iU
		}
		else
			Debug("ProcRMSrcPkg - no umd class for %s ", LPCSTR(ssCurDev));
	} // for iD
	if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
	// update gui as required
	if (iChosenSrcPkg == iChangedSourcePkg) DisplaySourcePackageData();
	if (bUpdateGui) DisplayUmdDeviceData();

}

void ProcessRouterModifyForDestinationPackageChange(int iPkgNum)
{
	BOOL bUpdateGui = FALSE;
	// go thru all devices and update for UMD labels IF linked to this src package
	for (int iD = 0; iD < ssl_AllUMDDevices.count(); iD++)  {
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev = ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord(ssCurDev);
		if (pUmd) {
			// tallies should not be affected by RM command -- only changed either from gpi state change or packager revertive change of dest routing
			// now sort out any changes to umd labels
			for (int iU = 1; iU <= pUmd->getNumberOfUMDs(); iU++) {

				BOOL bCont = FALSE;
				if ((pUmd->getUmdTypeGivenIndex(iU) == DEST_UMD_TYPE) && (pUmd->getUMDSrceDestIndex(iU) == iPkgNum)) {
					CDestinationPackages* pDest = GetDestinationPackage(pUmd->getUMDSrceDestIndex(iU));
					if (pDest) {
						bncs_string ssUmdLabel = "", ssSecondLabel = "";
						bncs_string ssPrevLabel = pUmd->getPrimaryUmdText(iU);
						bncs_string ssPrev2ndLbl = pUmd->getSecondUmdText(iU);
						//
						DetermineActualUmdLabels(pUmd, iU, &ssUmdLabel, &ssSecondLabel, FALSE);
						if (ssUmdLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssUmdLabel);
						if (ssSecondLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssSecondLabel);
						//
						pUmd->storePrimaryUmdName(iU, ssUmdLabel);
						pUmd->storeSecondUmdName(iU, ssSecondLabel);
						//if ((ssUmdLabel.find(ssPrevLabel) < 0) || (ssSecondLabel.find(ssPrev2ndLbl) < 0) || (bForceUMDUpdate )) {
						if ((ssUmdLabel != ssPrevLabel) || (ssSecondLabel != ssPrev2ndLbl) || (bForceUMDUpdate )) {
							SendUmdLabel(pUmd, iU);
							if (ssCurrentChosenDevice.find(ssCurDev) >= 0) bUpdateGui = TRUE;
						}
					}
				}
			} // for iU
		}
		else
			Debug("ProcRMSrcPkg - no umd class for %s ", LPCSTR(ssCurDev));
	} // for iD
	if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
	// update gui as required
	if (bUpdateGui) DisplayUmdDeviceData();

}



//////////////////////////////////////////////////////////////////////////////////////
//
//

void ProcessOverrideForDestinationIndexChange( int iChangedDevice, int iChangedDestPkg  )
{
	//
	BOOL bUpdateGui = FALSE;
	// go thru all devices and update for labels if linked to this dest package
	for (int iD=0; iD<ssl_AllUMDDevices.count(); iD++ )  {
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev=ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord( ssCurDev );
		if ( pUmd ) {
			//
			bncs_string ssCalcLabel, ssSecondLabel;
			// now package based
			for (int iU=1;iU<=pUmd->getNumberOfUMDs();iU++) {
				//
				bncs_string ssPrevLabel = pUmd->getPrimaryUmdText( iU ); 
				bncs_string ssPrev2ndLbl = pUmd->getSecondUmdText( iU ); 
				// process changed label
				if ((pUmd->getUmdSrcDestRouter(iU)==iChangedDevice)&& 
					 (pUmd->getUMDSrceDestIndex(iU) == iChangedDestPkg) && 
					 (pUmd->getUmdTypeGivenIndex(iU) == DEST_UMD_TYPE)) {
					//
					DetermineActualUmdLabels(pUmd, iU, &ssCalcLabel, &ssSecondLabel, FALSE);
					if (ssCalcLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssCalcLabel);
					if (ssSecondLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssSecondLabel);
					pUmd->storePrimaryUmdName(iU, ssCalcLabel);
					pUmd->storeSecondUmdName(iU, ssSecondLabel);
					//
					if ((ssCalcLabel != ssPrevLabel) || (ssSecondLabel != ssPrev2ndLbl) || (bForceUMDUpdate )) {
						SendUmdLabel(pUmd, iU);
						if (ssCurrentChosenDevice.find(ssCurDev) >= 0) bUpdateGui = TRUE;
					}
				}
			} // for iU				
		} 
	} // for ID
	if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
		
	// update gui as required
	if (iChosenDestPkg==iChangedDestPkg) DisplayDestinationPackageData();
	if (bUpdateGui) DisplayUmdDeviceData();
}

void ProcessRouterModifyForSDIChange( int iRtrNum, int iDestSrceNum )
{
	BOOL bUpdateGui = FALSE;
	// go thru all devices and update for UMD labels IF linked to this src package
	for (int iD=0; iD<ssl_AllUMDDevices.count(); iD++ )  {
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev=ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord( ssCurDev );
		if ( pUmd ) {
			// tallies should not be affected by RM command -- only changed either from gpi state change or packager revertive change of dest routing
			// now sort out any changes to umd labels
			for (int iU=1;iU<=pUmd->getNumberOfUMDs();iU++) {
				BOOL bCont = FALSE;
				int iUmdType=UNKNOWNVAL;
				int iMVRtr=0, iMVFeed=0, iCurrUmdSrc=UNKNOWNVAL;
				bncs_string ssPrevLabel = pUmd->getAllUmdDetails( iU, &iUmdType, &iMVRtr, &iMVFeed  );
				
				if ((iMVRtr == iRtrNum) && (iMVFeed>0)) {
					// 
					if (iUmdType == DEST_MAIN_SDI_INDEX) {
						CRevsRouterData* pRtr = GetValidRouter(iRtrNum);
						if (pRtr) {
							if ((iDestSrceNum == iMVFeed) || (pRtr->getRoutedSourceForDestination(iMVFeed)==iDestSrceNum)) bCont = TRUE;
						}
					}
					else if (iUmdType == SRCE_MAIN_SDI_INDEX) {
						if (iDestSrceNum == iMVFeed) bCont=TRUE;
					}
					if (bCont) {
						bncs_string ssUmdLabel = "", ssSecondLabel = "";
						bncs_string ssPrev2ndLbl = pUmd->getSecondUmdText(iU);
						//
						DetermineActualUmdLabels(pUmd, iU, &ssUmdLabel, &ssSecondLabel, FALSE);
						if (ssUmdLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssUmdLabel);
						if (ssSecondLabel.length() > i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssSecondLabel);
						//
						pUmd->storePrimaryUmdName(iU, ssUmdLabel);
						pUmd->storeSecondUmdName(iU, ssSecondLabel);
						//if ((ssUmdLabel.find(ssPrevLabel) < 0) || (ssSecondLabel.find(ssPrev2ndLbl) < 0) || (bForceUMDUpdate )) {
						if ((ssUmdLabel != ssPrevLabel) || (ssSecondLabel != ssPrev2ndLbl) || (bForceUMDUpdate )) {
							SendUmdLabel(pUmd, iU);
							if (ssCurrentChosenDevice.find(ssCurDev) >= 0) bUpdateGui = TRUE;
						}
					}
				}
			}
		}
		else
			Debug( "ProcRMSDIChange - no umd class for %s ", LPCSTR(ssCurDev));
	}
	if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
	//
	if (bUpdateGui) DisplayUmdDeviceData();
}



////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//
//   CSI CLIENT FUNCTIONS FOR INFODRIVER REVERTIVES MESSAGES 
//
//    PACKAGER REVS FIRST
//  
//

//////////////////////////////////////////////////////////////////////////////////////////
//
// *** recurrsive function -- new version
// note uses global list: ssl_DestPkgs_ToBeProcessed -- this needs resetting outside of this function as required
void DetermineListOfAffectedDestinationPackages( int iPkgNum )
{
	// starting dest pkg index
	if ((iPkgNum>0)&&(iPkgNum<=iNumberOfUserPackages)) {
		// add to global var list to be processed
		ssl_DestPkgs_ToBeProcessed.append(iPkgNum);
		if ((iPkgNum>0)&&(iPkgNum<i_Start_VirtualPackages)) 
			return;
		else if ((iPkgNum>=i_Start_VirtualPackages)&&(iPkgNum<=iNumberOfUserPackages)){ 
			// virtual package - trace forward and get all dests using equiv vir src
			CSourcePackages* pVirSrc = GetSourcePackage(iPkgNum);
			if (pVirSrc) {
				for (int iS=0;iS<pVirSrc->ssl_Pkg_routedTo.count();iS++) {
					DetermineListOfAffectedDestinationPackages( pVirSrc->ssl_Pkg_routedTo[iS].toInt() );
				}	
			}
		}
	}
	return;
}


// *** recurrsive function -- new version
// note uses global list ssl_SrcePkgs_ToBeProcessed
void DetermineListOfAffectedSourcePackages( int iPkgNum )
{
	// starting src pkg index
	if ((iPkgNum>0) && (iPkgNum <= iNumberOfUserPackages) && (ssl_NoTalliesSrcePackages.find(bncs_string(iPkgNum))<0) ) {
		// add to global var list to be processed
		ssl_SrcePkgs_ToBeProcessed.append(iPkgNum);
		if ((iPkgNum>0)&&(iPkgNum<i_Start_VirtualPackages)) 
			return;
		else if ((iPkgNum>=i_Start_VirtualPackages)&&(iPkgNum<=iNumberOfUserPackages)){ 
			// virtual package - trace backward and get all srcs using equiv vir src
			CDestinationPackages* pVirDest = GetDestinationPackage(iPkgNum);
			if (pVirDest) {
				DetermineListOfAffectedSourcePackages( pVirDest->iRouted_Src_Package );	
			}
		}
	}
	return;
}

// this calculates ALL ON AIR sources for ALL mixers /  galleries that have associated mixers
void CalculateCurrentOnAirIndices()
{
	// clear all areas of determined onairs
	for (int iia = 1; iia <= iNumberAreaGalleries; iia++) {
		CArea_Gallery* pGall = GetAreaGallery_ByKey(iia);
		if (pGall) {
			pGall->ssl_ThusSrcPackages_OnAir.clear();
			pGall->ssl_ThusSrcPackages_ISO.clear();
			pGall->ssl_ThusSrcRtrSDI_OnAir.clear();
			pGall->ssl_ThusSrcRtrSDI_ISO.clear();
		}
	}
	// go thru all mixers - calc tallies - which does areas too at same time
	for (int iim = 1; iim <= iNumberKahunaMixers; iim++) {
		CalculateMixerOnAirSources(iim);
	}

}




BOOL isInfodriverAPackagerDevice( int iDeviceNumber, int iIncomingSlot, int *iRevType,  int *iWhichInfoIndex, int *iCalcPackageIndex)
{
	*iRevType = 0;
	*iWhichInfoIndex = 0;
	*iCalcPackageIndex = 0;
	// check packager router or dest status
	for (int iiInfo = 1; iiInfo <= iNumberInfosInBlock; iiInfo++) {
		if (iDeviceNumber == iPackagerAutoInfos[iiInfo]) {
			*iRevType = PKGR_ROUTER_TYPE;
			*iWhichInfoIndex = iiInfo;
			*iCalcPackageIndex = ((iiInfo-1)* 4000) + iIncomingSlot;
			return TRUE;
		}
		else if (iDeviceNumber == iPackagerDestStatInfos[iiInfo]) {
			*iRevType = PKGR_DEST_STAT_TYPE;
			*iWhichInfoIndex = iiInfo;
			*iCalcPackageIndex = ((iiInfo - 1) * 4000) + iIncomingSlot;
			return TRUE;
		}
	}
	// fails to find
	return FALSE;
}


BOOL StorePackagerRevertive(int iPkgIndx, bncs_string ssCmd, BOOL *bChanged, int *iPrevTracedPkg, int *iPrevRoutedPkg)
{
	*bChanged = FALSE;
	*iPrevTracedPkg = 0;
	*iPrevRoutedPkg = 0;

	if ((iPkgIndx>0)&&(iPkgIndx<=iNumberOfUserPackages)) {
		CDestinationPackages* pDstPkg = GetDestinationPackage( iPkgIndx );
		if (pDstPkg) {
			bncs_stringlist ssl = bncs_stringlist (ssCmd, ',' );
			bncs_string ssIndex = ssl.getNamedParam("index").upper();
			int iNewSrcPkg = 0, iNewLevel=1;
			if (ssIndex.length()>0) {
				if (ssIndex.find("BARS") >= 0) iNewSrcPkg = iPackagerBarsSrcePkg;
				else if (ssIndex.find("PARK") >= 0) iNewSrcPkg = iPackagerParkSrcePkg;
				else iNewSrcPkg = ssIndex.toInt();
				if (iNewSrcPkg <= 0) iNewSrcPkg = iPackagerParkSrcePkg; // use park pkg data
			}
			else {
				// no index given so use current routed package or park
				iNewSrcPkg = pDstPkg->iTraced_Src_Package;
			}
			// store values and test for changes
			int iOldSrcPkg = pDstPkg->iRouted_Src_Package;
			int iOldTraced = pDstPkg->iTraced_Src_Package;

			pDstPkg->iRouted_Src_Package = iNewSrcPkg;
			pDstPkg->iTraced_Src_Package = iNewSrcPkg;

			//  sort out ptis
			if (iOldSrcPkg != iNewSrcPkg)  {
				*bChanged = TRUE;
				*iPrevTracedPkg = iOldTraced;
				*iPrevRoutedPkg = iOldSrcPkg;
				//Debug( "StorePackagerRev-changed");
				// prev src pkg ptiS
				if ((iOldSrcPkg > 0) && (iOldSrcPkg <= iNumberOfUserPackages)) {
					CSourcePackages* pOldSrc = GetSourcePackage(iOldSrcPkg);
					if (pOldSrc) {
						int iPos = pOldSrc->ssl_Pkg_routedTo.find(bncs_string(iPkgIndx));
						if (iPos >= 0) pOldSrc->ssl_Pkg_routedTo.deleteItem(iPos);
					}
				}
				// new src pkg ptiS
				if ((iNewSrcPkg > 0) && (iNewSrcPkg <= iNumberOfUserPackages)) {
					CSourcePackages* pSrc = GetSourcePackage(iNewSrcPkg);
					if (pSrc) {	// add to current src pkg pti list
						if (pSrc->ssl_Pkg_routedTo.find(bncs_string(pDstPkg->iPackageIndex)) < 0)
							pSrc->ssl_Pkg_routedTo.append(pDstPkg->iPackageIndex);
					}
				}
			}

			if (!bAutomaticStarting) {

				// determine real traced package 
				ssl_TraceToSource.clear();
				iRecursionTraceCounter = 0;
				pDstPkg->ssl_SrcPkgTrace.clear();
				int iTraced = TraceSourcePackageVideo(pDstPkg->iPackageIndex);
				if (iTraced <=0)  iTraced = iNewSrcPkg;
				pDstPkg->iTraced_Src_Package = iTraced;
				if (pDstPkg->iRouted_Src_Package != pDstPkg->iTraced_Src_Package) {
					for (int ii = 0; ii < ssl_TraceToSource.count(); ii++) pDstPkg->ssl_SrcPkgTrace.append(ssl_TraceToSource[ii]);
				}
				else
					pDstPkg->ssl_SrcPkgTrace.append(bncs_string(iNewSrcPkg));

				pDstPkg->iVisionFrom_Src_Package = iTraced; // usually -- follwing rev from dest status will update if required

				if ((bShowAllDebugMessages) && (iNewSrcPkg != iPackagerParkSrcePkg) && (!bAutomaticStarting))
					Debug("StorePackagerRev - DstPkg %d data (%s) -> new srcpkg %d traced pkg %d was %d %d ", iPkgIndx, LPCSTR(ssCmd), iNewSrcPkg, iTraced, iOldSrcPkg, iOldTraced);

				// do some processing for virtual dest packages
				if ((iPkgIndx >= i_Start_VirtualPackages) && (iPkgIndx <= iNumberOfUserPackages)) {

					// first get list of all dest pkgs affected by this change to vitual pkg
					ssl_DestPkgs_ToBeProcessed = bncs_stringlist("", ',');
					CSourcePackages* pSrc = GetSourcePackage(iPkgIndx); // virtual dest, becomes virt src pkg
					for (int iC = 0; iC < pSrc->ssl_Pkg_routedTo.count(); iC++) {
						DetermineListOfAffectedDestinationPackages(pSrc->ssl_Pkg_routedTo[iC].toInt());
					}
					if (bShowAllDebugMessages) Debug("StorePkgRev - %d dest pkgs associated to VIR srcpkg %d", ssl_DestPkgs_ToBeProcessed.count(), iPkgIndx);
					//
					if (ssl_DestPkgs_ToBeProcessed.count() > 0) {				//
						for (int iD = 0; iD < ssl_DestPkgs_ToBeProcessed.count(); iD++) {
							CDestinationPackages* pDest = GetDestinationPackage(ssl_DestPkgs_ToBeProcessed[iD].toInt());
							if (pDest) {
								// NEED to calculate revised trace here for affected dest pkgs
								ssl_TraceToSource.clear();
								iRecursionTraceCounter = 0;
								pDest->ssl_SrcPkgTrace.clear();
								int iVisTraced = TraceSourcePackageVideo(pDest->iPackageIndex);
								for (int ii = 0; ii < ssl_TraceToSource.count(); ii++) pDest->ssl_SrcPkgTrace.append(ssl_TraceToSource[ii]);
								pDest->iTraced_Src_Package = iVisTraced;
								pDest->iVisionFrom_Src_Package = iVisTraced; // usually -- follwing rev from dest status will update if required
							}
						}
					}
				}

			} // if !bAutoStarting
			return TRUE;

		}
		else
			Debug( "StorePackagerRev - no dest pkg class for %d ", iPkgIndx );
	}
	else
		Debug( "StorePackagerRev - invalid dest pkg index - %d ", iPkgIndx );

	return FALSE;
}


void ProcessPackagerRevertive( int iDestPkgIndex, int iPreviousTracedSrcPkg, int iPrevRoutedSrcPkg )
{	
	
	// for better use of cpu time etc - get lists of all src pkages on-air in all galleries - just once - then use this info when processing thru all mvs
	CalculateCurrentOnAirIndices();
	for (int i = 1; i <= iNumberKahunaMixers; i++) UpdateGuiForMixerOnAir(i);

	DetermineListOfAffectedDestinationPackages( iDestPkgIndex );

	// set start state for CCUs
	ProcessAllRegionalCameraTallies(FALSE);

	int iGalleryForRevertive = 0;
	int iRoutedSrcForRevertive = 0;
	int iTracedSrcForRevertive = 0;
	BOOL bEcutPackageForGallery = FALSE;
	int iGalleryEcutState=0;
	BOOL bCCUChangeOccured = FALSE;

	CDestinationPackages* pThisPkg = GetDestinationPackage( iDestPkgIndex );
	if (pThisPkg) {
		iGalleryForRevertive = pThisPkg->iLinkedAreaGallery;
		iRoutedSrcForRevertive = pThisPkg->iRouted_Src_Package;
		iTracedSrcForRevertive = pThisPkg->iTraced_Src_Package;
		if (bShowAllDebugMessages) Debug( "ProcPkgRev - destpkg %d routed %d traced %d gallrev %d", iDestPkgIndex, iRoutedSrcForRevertive, iTracedSrcForRevertive, iGalleryForRevertive );

		// if tied to gallery - is this dest pkg the predefined ecut dest pk
		// xxxx is destg pkg assoc to area and is thats area ecut dest pkg then do something -- REDO
		/* 
		if ((iGalleryForRevertive >= 1) && (iGalleryForRevertive <= iNumberAreaGalleries)) {
			CStudioRevsData* pMixer = GetMixerByKeyIndex( iGalleryForRevertive );
			if (pMixer)  {
				if (pMixer->get_ECUT_Destination_Package()==iDestPkgIndex) {
					bEcutPackageForGallery = TRUE;
					iGalleryEcutState = pMixer->get_ECUT_Status();
					if (iGalleryEcutState==1) {
						Debug("ProcPackagerRev - in ecut for pkg %d trac %d and gallery %d", iDestPkgIndex, iTracedSrcForRevertive, iGalleryForRevertive);
						// IN ECUT so update TX /SCR MONs with new umdlabel based on new src package
						ProcessAndUpdateTXMonitors( pMixer, iGalleryForRevertive );
						// CCUs 
						ProcessAllCCUCameraTallies();
						//
					}
				}
			}
			*/

			// xxxx fix this OLD as this dst pkg a gallery mixer one - then need to check if CCU routed or derouted - i.e. change for CCU umds for AR/lighting
			if ((pThisPkg->iAssignedMixerDesk>0) && (pThisPkg->iAssignedMixerChannel>0)) {
				// is new or prev src pkg linked to gallery camera virtual - then recalc umd label for said ccu
				/* XXXXXX
				CInfodriverRevs* pCCUwas = GetCCUFromSourcePackage(iPreviousTracedSrcPkg);
				if (pCCUwas) {
					CalculateCCUGalleryUmd(pCCUwas);
					bCCUChangeOccured = TRUE;
				}
				CInfodriverRevs* pCCUnew = GetCCUFromSourcePackage(pThisPkg->iTraced_Src_Package);
				if (pCCUwas) {
					CalculateCCUGalleryUmd(pCCUnew);
					bCCUChangeOccured = TRUE;
				}
			}
			*/
		}
	}

	// now update any mv device linked to any destination package with src pkg listed as on air for assoc gallery
	BOOL bUpdateGui = FALSE;
	for (int iD=0; iD<ssl_AllUMDDevices.count(); iD++ )  {
		//
		// get device record and store rev - recalc on new src and send to hardware
		bncs_string ssCurDev=ssl_AllUMDDevices[iD];
		CUmdDevice* pUmd = GetUmdDeviceRecord( ssCurDev );
		if ( pUmd ) {
			// now sort out tallies and umd labels
			for (int iU=1;iU<=pUmd->getNumberOfUMDs();iU++) {
				int iUmdType=UNKNOWNVAL;
				int iMVRtr=0, iMVFeed=0;
				bncs_string ssPrevLabel = pUmd->getAllUmdDetails( iU, &iUmdType, &iMVRtr, &iMVFeed  );
				
				if (iUmdType==DEST_UMD_TYPE) { 
					CDestinationPackages* pDstPkg = GetDestinationPackage(iMVFeed);
					if (pDstPkg) {

						// UMD TALLIES
						int iAssocGallery = pDstPkg->iLinkedAreaGallery;
						if ((iAssocGallery>0) && (iAssocGallery <= iNumberAreaGalleries) && (iAssocGallery == iGalleryForRevertive)) {
							//if (bShowAllDebugMessages) Debug( "ProcPkgRev - destpkg %d assocgall %d gallrev %d", iAssocPkg, iAssocGallery, iGalleryForRevertive );
							// check against calculated on air src pkgs

							int iRedState= pUmd->getTallyState(iU, LEFT_TALLY );   // keeps current setting as starting point
							int iGrnState = TALLY_OFF; // default to OFF - else they got stuck/lost
							int iTxtState = TALLY_OFF; // default to OFF - else they got stuck/lost

							// new way of determining tallies
							GetTallyStateForDestinationPackage(iMVFeed, pUmd->getUMDDeviceAreaCode(iU), &iRedState, &iGrnState, &iTxtState);

							BOOL bRedChange = pUmd->storeTallyState(iU, LEFT_TALLY, iRedState );
							BOOL bGrnChange = pUmd->storeTallyState(iU, RIGHT_TALLY, iGrnState);
							BOOL bTxtChange = pUmd->storeTallyState(iU, THIRD_TALLY, iTxtState);
							// now get dev data to iw for device
							if (bRedChange || bGrnChange || bTxtChange || bForceUMDUpdate) {
								DetermineAndSendUMDTallies(pUmd, iU);
								if (ssCurrentChosenDevice.find(ssCurDev) >= 0) bUpdateGui = TRUE;
							}
							
						}

						// UMD LABELS
						if (ssl_DestPkgs_ToBeProcessed.find(bncs_string(iMVFeed)) >= 0) {
							// the package associated to mv is in the list, or has same src routed to it - so process
							// get umd label from routed src pkage
							//
							bncs_string ssFirstUmdLabel="", ssSecondUmdLabel="";
							DetermineActualUmdLabels(pUmd, iU, &ssFirstUmdLabel, &ssSecondUmdLabel, FALSE);
							if (ssFirstUmdLabel.length()>i_Max_UMD_Length) ApplySmartUMDTruncation2( &ssFirstUmdLabel );
							if (ssSecondUmdLabel.length()>i_Max_UMD_Length) ApplySmartUMDTruncation2( &ssSecondUmdLabel );
							if (bShowAllDebugMessages) Debug("ProcessPackagerRev - dstPkg %d new label %s  %s", iMVFeed, LPCSTR(ssFirstUmdLabel), LPCSTR(ssSecondUmdLabel));
							// umd label
							pUmd->storePrimaryUmdName(iU, ssFirstUmdLabel );
							pUmd->storeSecondUmdName(iU, ssSecondUmdLabel );
							SendUmdLabel( pUmd, iU );
						}

					}
				}
				else if (iUmdType==SOURCE_UMD_TYPE) {
					// what if defined src umd is same as src package routed to given dest pkg -- esp for tallies in ecut mode
					if (iGalleryEcutState==1) {
						if (iMVFeed == iRoutedSrcForRevertive) {
							// tallies if in ecut
							// XXX ??? more needed here ??? -- what if src pkg now on air having been routed to mixer dest ??? or prev src no longer routed to mixer so not onair 
						}
					}
				}

			} // for iU
		}
		//if (ssCurrentChosenDevice.find(ssCurDev)>=0) DisplayUmdDeviceData();
		// xxx if ((iGalleryForRevertive >= 1) && (iGalleryForRevertive <= iNumberAreaGalleries)) UpdateGuiForGalleryOnAir(iGalleryForRevertive);
	} // For iD
	if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
}


BOOL StorePackagerDestStatus(int iPkgIndx, bncs_string ssCmd, BOOL *bChanged)
{
	*bChanged = FALSE;
	int iPreVisionFrom = 0;
	int iPreAudioFrom = 0;
	CDestinationPackages* pDest = GetDestinationPackage(iPkgIndx);
	if (pDest) {
		iPreVisionFrom = pDest->iVisionFrom_Src_Package;
		iPreAudioFrom = pDest->iAudioFrom_Src_Package;
		bncs_stringlist ssl = bncs_stringlist(ssCmd, ',');
		pDest->iVisionFrom_Src_Package = ssl.getNamedParam("vision_from").toInt();
		pDest->iAudioFrom_Src_Package = ssl.getNamedParam("sound_from").toInt();
		if (pDest->iVisionFrom_Src_Package != iPreVisionFrom) *bChanged = TRUE;
	}
	return TRUE;
}

void ProcessPackagerDestStatus(int iDestPkgIndex)
{

	// now update the mv device linked this destination package status change
	BOOL bUpdateGui = FALSE;
	CDestinationPackages* pDest = GetDestinationPackage(iDestPkgIndex);
	if (pDest) {
		if (pDest->ssl_Assoc_MVDeviceAndIndex.count() > 0) {
			for (int iic = 0; iic < pDest->ssl_Assoc_MVDeviceAndIndex.count(); iic++) {
				bncs_stringlist  ssl = bncs_stringlist(pDest->ssl_Assoc_MVDeviceAndIndex[iic], ',');
				if (ssl.count() > 1) {
					bncs_string ssMVid = ssl[0];
					int iMVInput = ssl[1].toInt();
					CUmdDevice* pUmd = GetUmdDeviceRecord(ssMVid);
					if (pUmd) {
						// now sort out tallies and umd labels
						if ((iMVInput > 0) && (iMVInput <= pUmd->getNumberOfUMDs())) {
							int iUmdType = UNKNOWNVAL;
							int iMVRtr = 0, iMVFeed = 0;
							bncs_string ssPrevLabel = pUmd->getAllUmdDetails(iMVInput, &iUmdType, &iMVRtr, &iMVFeed);
							if ((iUmdType == DEST_UMD_TYPE) && (iMVFeed = iDestPkgIndex)) {
								bncs_string ssFirstUmdLabel = "", ssSecondUmdLabel = "";
								DetermineActualUmdLabels(pUmd, iMVInput, &ssFirstUmdLabel, &ssSecondUmdLabel, FALSE);
								if (ssFirstUmdLabel.length()>i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssFirstUmdLabel);
								if (ssSecondUmdLabel.length()>i_Max_UMD_Length) ApplySmartUMDTruncation2(&ssSecondUmdLabel);
								if (bShowAllDebugMessages) Debug("ProcessPackagerRev - dstPkg %d new label %s  %s", iMVFeed, LPCSTR(ssFirstUmdLabel), LPCSTR(ssSecondUmdLabel));
								// umd label
								pUmd->storePrimaryUmdName(iMVInput, ssFirstUmdLabel);
								pUmd->storeSecondUmdName(iMVInput, ssSecondUmdLabel);
								SendUmdLabel(pUmd, iMVInput);
							}
						}
					}
				}
			}
		}
	}
	if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);
}


/////////////////////////////////////////////////////////////////////////
//
// Nevion Router router revertives -- may not actiually be needed
//
//////////////////////////////////////////////////////////////////////////
BOOL IsNevionRouterRevertive(int iDevNum)
{
	int iPos = ssl_AllRouters.find(bncs_string(iDevNum));
	if ((iPos == 0) || (iPos == 1)) return TRUE;   // first and maybe second is Nevion - depending on its size
	return FALSE;
}


BOOL StoreNevionRouterRevertive(int iBaseRtrNum, int iRevRtrNum, int iRtrDest, bncs_string ssRev)
{
	CRevsRouterData* pRtr = GetValidRouter(iBaseRtrNum);
	if (pRtr) {
		//
		int iBNCSDest = iRtrDest;
		iBNCSDest = (iIP_RouterInfoDivider * (iRevRtrNum - iBaseRtrNum)) + iRtrDest;
		if ((iBNCSDest > 0) && (iBNCSDest <= pRtr->getMaximumDestinations())) {
			//if (bShowAllDebugMessages) Debug("StoreRtrRev - calc dest %d src %d prev %d", iBNCSDest, iRtrSource, pRtr->getRoutedSourceForDestination(iBNCSDest));
			int iPrev = pRtr->getRoutedSourceForDestination(iBNCSDest);
			bncs_stringlist ssl = bncs_stringlist(ssRev, ',');
			int iNewSrce = ssl.getNamedParam("index").toInt();
			int iDestStatus = ssl.getNamedParam("status").toInt();
			int iDestLock = ssl.getNamedParam("lock").toInt();
			pRtr->storeGRDRevertive(iBNCSDest, iNewSrce, ssRev);
			// is rev assigned to a dest pkg 
			// return if changed source and linked to a dest pkg
			if ((iNewSrce != iPrev) && (pRtr->getNumberPackagesUsingDestination(iBNCSDest) > 0) && (!bAutomaticStarting))
				return TRUE;  // further processing required
		}
		else
			Debug("StoreNevionRtrRev -- ERROR invalid BNCS dest  %d from rev dev %d dest %d", iBNCSDest, iRevRtrNum, iRtrDest);
	}
	// not changed, not linked to dest pkg - but means nothing further to do with revertive
	return FALSE;
}


void ProcessNevionRouterRevertive(int iBaseRtrNum, int iRevRtrNum, int iRtrDest)
{
	// check to see if dest has correct source as expected from package or not !!
	//if (bShowAllDebugMessages) Debug("intoProcRtrRev %d %d", iRtrNum, iRtrDest);
	CRevsRouterData* pRtr = GetValidRouter(iBaseRtrNum);
	if (pRtr) {
		int iBNCSDest = iRtrDest;
		iBNCSDest = (iIP_RouterInfoDivider * (iRevRtrNum - iBaseRtrNum)) + iRtrDest;
		if ((iBNCSDest>0) && (iBNCSDest <= pRtr->getMaximumDestinations())) {
			// get all dest pkgs associated to this router destination -- usually only 1, but possibly more
		}
		else
			Debug("ProcNevionRtrRev -- ERROR invalid BNCS dest  %d from rev dev %d dest %d", iBNCSDest, iRevRtrNum, iRtrDest);
	}
	else
		Debug("ProcNevionRtrRev -- ERROR no ROUTER class for  %d ", iBaseRtrNum);
}


/////////////////////////////////////////////////////////////////////////
//
//// Studio MIXER CHANNEL STATE PROCESSING 
//
/////////////////////////////////////////////////////////////////////////

bncs_string ProcessAndUpdateTXMonitors( CStudioRevsData* pMixer, int iWhichGallery )
{
	bncs_string ssTXUmd = "";
	/****************************  XXXXXXXXXX redo tc monitors when known 

	if (pMixer) {
		int iLowAssocType = 0, iLowest = 0, iLowDev = 0;
		CSourcePackages* pRou = NULL;

		if (pMixer->get_ECUT_Status()==1) {
			// gallery in ecut - get src pkg routed to ecut dest pkg
			CDestinationPackages* pDst = GetDestinationPackage(pMixer->get_ECUT_Destination_Package());
			if (pDst) {
				pRou = GetSourcePackage(pDst->iRouted_Src_Package);  // should it be traced pkg rather than routed one ????
			}
			else
				Debug( "ProcessUpdateTXMon -ERROR- NO ECUT DEST PKG DEFINED for gallery %d", iWhichGallery );
		}
		else {
			// not in ecut so mixer output is TX
			int iRtrDev = 0;
			iLowest = pMixer->get_Lowest_MixerTX_OnAir();
			int iLowAssocPkg = pMixer->getMixerAssocIndx( iLowest, &iLowAssocType, &iRtrDev );
			if ((iLowAssocPkg>0)&&(iLowAssocPkg<=(iNumberOfUserPackages+1))) {
				if (iLowAssocType==DEST_UMD_TYPE) {
					// get routed source for this dest pkg
					CDestinationPackages* pDst = GetDestinationPackage(iLowAssocPkg);
					if (pDst) pRou = GetSourcePackage(pDst->iRouted_Src_Package);
				}
				else if (iLowAssocType==SOURCE_UMD_TYPE) {
					pRou = GetSourcePackage( iLowAssocPkg );
				}
			}
		}

		// got src pkg - so send umd labels for TX / PV / SCR Monitors  // xxx sort for preview
		ssTXUmd = DetermineMixerTXUMDLabel( pRou, iWhichGallery, iLowest );
		bncs_string ssPrevUMD = pMixer->get_MixerTX_UMDLabel();
		pMixer->store_MixerTX_UMDLabel( ssTXUmd );
		if (!AreBncsStringsEqual(ssTXUmd, ssPrevUMD)) {
			if (pMixer->get_EggboxTimerState()==FALSE) {
				pMixer->set_EggboxTimerState(TRUE);
				SetTimer( hWndMain, GALLERY_EGGBOX_TIMER+iWhichGallery, iEggbox_Timer_Interval, NULL ); 
			}
		}
	}
	*******************/
	return ssTXUmd;
}


int IsSrcePkgOnAirAnywhere(int iSrcPkg)
{
	if ((iSrcPkg>0) && (iSrcPkg<=iNumberOfUserPackages)) {
		// go thru all galleries
		map<int, CArea_Gallery*>::iterator itp = cl_AllGalleries->begin();
		while (itp != cl_AllGalleries->end()) {
			CArea_Gallery* pArea = itp->second;
			if (pArea) {
				if ((pArea->ssl_ThusSrcPackages_OnAir.find(bncs_string(iSrcPkg)) >= 0) ||
					(pArea->ssl_ThusSrcPackages_ISO.find(bncs_string(iSrcPkg))  >= 0)) {
					// once found to be on air in any gallery - either as NOW or as part of ISO then return
					return pArea->iAreaGallery_Index;
				}
			}
			itp++;
		} // while
	}
	// not found so offair
	return UNKNOWNVAL;
}


void ProcessARegionTallies(int iRegion, BOOL bForce, int iDestInQ, int iPrevSrce )
{
	CRegionals* pRegion = GetRegionalRecordByKey(iRegion);
	if (pRegion) {
		//
		if ((iPrevSrce > 0) && (iDestInQ > 0)) {
			// if dest is a key one and src has changed on regional router - and prev source is a key source and was deemed on air then switch off
			if (pRegion->ssl_key_router_destinations.find(bncs_string(iDestInQ)) >= 0) {
				if (pRegion->ssl_key_router_sources.find(bncs_string(iPrevSrce)) >= 0) {
					int igpioKey = pRegion->getGPIOlinkedtoRouterSource(iPrevSrce);
					CGPIODevice* pGPio = GetGPIORecordByKey(igpioKey);
					if (pGPio) {
						if (pGPio->iBNCS_LastStateSent > 0) {
							char szCommand[MAX_AUTO_BUFFER_STRING] = "";
							wsprintf(szCommand, "IW %d '0' %d", pGPio->iBNCS_GPIO_Device, pGPio->iBNCS_GPIO_Index);
							AddCommandToQue(szCommand, INFODRVCOMMAND, pGPio->iBNCS_GPIO_Device, 0, 0);
							pGPio->iBNCS_LastStateSent = 0;
						}
					}
					//else
					//	Debug("ProcRegTally -prev- no gpio for key dest %d prev  key src %d", iDestInQ, iPrevSrce );
				}
			}
		}
		// go thru the key src packages - get regional rtr src - get gpio assoc to source - 
		//              if key src package is on air - fire gpio high
		//              if key src package not air - set gpio low
		for (int iIndx = 0; iIndx < pRegion->ssl_key_packager_sources.count(); iIndx++) {
			// get linked dest and hence source and hence gpio key
			int inTracedSrc = 0, igpioKey = 0, igpioState = 0;
			int iSrcPkg = pRegion->ssl_key_packager_sources[iIndx].toInt();
			int iLinkedDest = pRegion->getRouterDestinationLinkedtoPackage(iSrcPkg);
			inTracedSrc = TraceRegionalPrimaryRouterSource(pRegion->iRegionalRouter, iLinkedDest);
			if (inTracedSrc > 0) {
				if (pRegion->ssl_key_router_sources.find(bncs_string(inTracedSrc)) >= 0) {
					igpioKey = pRegion->getGPIOlinkedtoRouterSource(inTracedSrc);
				}
			}
			if ((bShowAllDebugMessages)||((iLinkedDest>0)&&(inTracedSrc==0))) {
				Debug("ProcRegTally region %d - pkg %d - dest %d traced src %d gpiokey %d", iRegion, iSrcPkg, iLinkedDest, inTracedSrc, igpioKey);
				if (inTracedSrc == 0) Debug("Router Source 0 implies NO rev from %s router", LPCSTR(pRegion->ssRegionalName));
			}
			CGPIODevice* pGPio = GetGPIORecordByKey(igpioKey);
			if (pGPio) {
				// find if onair or not
				char szCommand[MAX_AUTO_BUFFER_STRING] = "";
				int iGalleryOnAir = IsSrcePkgOnAirAnywhere(iSrcPkg);
				if (iGalleryOnAir > 0) {
					// on air - fire high
					if ((pGPio->iBNCS_LastStateSent != 1) || (bForce)) {
						wsprintf(szCommand, "IW %d '1' %d", pGPio->iBNCS_GPIO_Device, pGPio->iBNCS_GPIO_Index);
						AddCommandToQue(szCommand, INFODRVCOMMAND, pGPio->iBNCS_GPIO_Device, 0, 0);
						pGPio->iBNCS_LastStateSent = 1;
					}
					igpioState = 1;
					if (bShowAllDebugMessages) Debug("ProcRegionTally - GPIO record from key %d - src %d dest %d pkg %d Deemed ON AIR", igpioKey, inTracedSrc, iLinkedDest, iSrcPkg);
				}
				else {
					// off air fire low
					if ((pGPio->iBNCS_LastStateSent != 0) || (bForce)) {
						wsprintf(szCommand, "IW %d '0' %d", pGPio->iBNCS_GPIO_Device, pGPio->iBNCS_GPIO_Index);
						AddCommandToQue(szCommand, INFODRVCOMMAND, pGPio->iBNCS_GPIO_Device, 0, 0);
						pGPio->iBNCS_LastStateSent = 0;
					}
					if (bShowAllDebugMessages) Debug("ProcRegionTally - GPIO record from key %d - src %d dest %d pkg %d Deemed off air", igpioKey, inTracedSrc, iLinkedDest, iSrcPkg);
				}
			}
			//else
			//	Debug("ProcRegTally -now- no gpio for key dest %d  traced src %d", iLinkedDest, inTracedSrc);


			// update gui for current region and up first 4 pkgr dests only
			if ((iRegion == iCurrentRegion) && (iIndx >= 0) && (iIndx < 4)) {
				if (iIndx == 0) SetDlgItemText(hWndDlg, IDC_REGIONNAME, LPCSTR(pRegion->ssRegionalName));
				bncs_string sstr = bncs_string("p %1 > d %2 > s %3 (g %4)").arg(iSrcPkg).arg(iLinkedDest).arg(inTracedSrc).arg(igpioState);
				if (iLinkedDest>0)
					SetDlgItemText(hWndDlg, 2161 + iIndx, LPCSTR(sstr));
				else
					SetDlgItemText(hWndDlg, 2161 + iIndx, "");
				Debug("ProcRegTally - %s  %d - %s", LPCSTR(pRegion->ssRegionalName), iIndx + 1, LPCSTR(sstr));
				if (igpioState > 0) SetDlgItemText(hWndDlg, 2051 + iIndx, "XX"); else SetDlgItemText(hWndDlg, 2051 + iIndx, " ");
			}

		}
		ProcessNextCommand(BNCS_COMMAND_RATE);

	}
	else
		Debug("ProcRegionTally - no region for %d", iRegion);
}


void ProcessAllRegionalCameraTallies(BOOL bForce)
{
	if (cl_AllRegionals&&cl_AllGPIO) {
		for (int ii = 1; ii <= iNumberRegionals; ii++) {
			ProcessARegionTallies(ii, bForce, 0, 0);
		}
	}
}

// below current defunct 
void ProcessAllCCUCameraTallies()
{
	// go thru the ccus base records - is cam assigned, determine if on air for red 
	for (int iWhichCCU = 1; iWhichCCU <= MAX_CCU_ASSIGNMENT; iWhichCCU++) {
		char szCommand[MAX_AUTO_BUFFER_STRING] = "";
		int iRedTally = TALLY_OFF, iGrnTally = TALLY_OFF; // starting point
		CInfodriverRevs* pCCUAssigned = GetCCUAssignment(iWhichCCU);
		CInfodriverRevs* pCCURouted = GetCCURouted(iWhichCCU);
		if (pCCUAssigned&&pCCURouted) {
			// check for red and or green tallies to be lit -- only  if assigned to gallery
			if (pCCUAssigned->i_Calculated_Gallery > 0) {
				// ccu is assigned to a gallery - so see if on air anywhere

				map<int, CArea_Gallery*>::iterator itp = cl_AllGalleries->begin();
				while (itp != cl_AllGalleries->end()) {
					CArea_Gallery* pGallery = itp->second;
					if (pGallery) {
						if (pGallery->ssl_ThusSrcPackages_OnAir.find(bncs_string(pCCUAssigned->i_AssociatedSrcPackage1)) >= 0) {
							// ccu package is listed as onair -- does gallery match that calculated
							if (pGallery->iAreaGallery_Index == pCCUAssigned->i_Calculated_Gallery)
								iRedTally = TALLY_ON;   // calc gallery is in an on-air list
							else {
								iGrnTally = TALLY_ON;  // calc gallery 
							}
						}
					}
					itp++;
				}

			}
			// does new tally differ from last sent - then send new one(s) and store in record
			char szRedT[256] = "0", szGrnT[256] = "0";
			wsprintf(szRedT, "%d", iRedTally);
			wsprintf(szGrnT, "%d", iGrnTally);
			if (pCCUAssigned->i_Linked_BNCSIndexR>0) eiExtInfoId->updateslot(pCCUAssigned->i_Linked_BNCSIndexR, szRedT); 
			if (pCCUAssigned->i_Linked_BNCSIndexG>0) eiExtInfoId->updateslot(pCCUAssigned->i_Linked_BNCSIndexG, szGrnT);
			if ((pCCUAssigned->i_Calculated_RedTally != iRedTally) || (pCCUAssigned->i_Calculated_GrnTally != iGrnTally)) {
				if (bShowAllDebugMessages) 
					Debug("ProcAllCamTally CCU %d assign gall %d cam %d -- red state %d (%d)  --  green state %d (%d)", iWhichCCU, pCCUAssigned->i_Calculated_Gallery,
						pCCUAssigned->i_Calculated_PCRCamera, iRedTally, pCCUAssigned->i_Calculated_RedTally, iGrnTally, pCCUAssigned->i_Calculated_GrnTally);
			}
			pCCUAssigned->i_Calculated_RedTally = iRedTally;
			pCCUAssigned->i_Calculated_GrnTally = iGrnTally;
		}
		//else
		//	Debug("ProcessAllCCUCameraTallies -ERROR- NO CCU record for index %d ", iWhichCCU);
	}

}



//////////////////////////////////////////////////////////////////////////

BOOL isInfodriverStudioMixerDevice( int iDevNum, int iSlotNum, int *iWhichRecord )
{
	CStudioRevsData* pMixer = GetMixerByDevice( iDevNum );
	if (pMixer) {
		// mixer inputs 1-200;   outputs 621-684
		if (((iSlotNum > 0) && (iSlotNum < MAXMIXERENTRIES)) || ((iSlotNum > 620) && (iSlotNum < 685))) {
			*iWhichRecord = pMixer->getRecordKey();
			return TRUE;
		}
	}
	// else not found
	*iWhichRecord = UNKNOWNVAL;
	return FALSE;
}


BOOL StoreStudioMixerRevertive( int iStudioDev, int iGivenIndx, bncs_string ssStates, BOOL *bChanged, int *iRevType )
{
	*bChanged = FALSE;
	*iRevType = UNKNOWNVAL;

	CStudioRevsData* pMixer = GetMixerByDevice(iStudioDev);
	if (pMixer) {
		// determine if input or output -- xxx note assumes whole kahuna - device offset 0 - rework if no longer offset as 0
		int iGALLERY = 0;
		bncs_stringlist ssl = bncs_stringlist(ssStates, '|');
		int iNewState = ssl[0].toInt();
		if ((ssl.count() == 1) && (iNewState>0)) ssStates.append("|NOW");   // add NOW onair state if just a single numeric entry in list AND state is 1 - on air
		if ((iGivenIndx > 0) && (iGivenIndx<MAXMIXERENTRIES)) {
			// inputs
			*bChanged = pMixer->storeMixerState(iGivenIndx, ssStates, iNewState);
			*iRevType = pMixer->getRecordKey();
			return TRUE;
		}
		else if ((iGivenIndx > 620) && (iGivenIndx<685)) {
			// output
			int iWhichOutp = iGivenIndx - 620;
			*bChanged = pMixer->storeOutputState(iWhichOutp, ssStates, iNewState);
			*iRevType = pMixer->getRecordKey();
			return TRUE;
		}
		else
			Debug("StoreStudioMixerRev - gallery slot out of range %d ", iGivenIndx);
	}
	else
		Debug("StoreStudioMixerRev - no mixer for device %d ", iStudioDev );

	return FALSE;

}


void ProcessStudioMixerRevertive( int iWhichMixer, int iIndex )
{
	int iMxrAssociatedIndx=0, iMxrAssocType=DEST_UMD_TYPE;
	int iMxrGALLERY=0, iMxrChannel=0;
	
	CSourcePackages* pTracedSrc=NULL;
	CSourcePackages* pRoutedSrc=NULL;

	int iRoutedSrcPkg=0;
	int iTracedSrcPkg=0;
	int iRoutedSDISrc=0;
	int iTracedSDISrc=0;

	// rev type is which mixer
	CStudioRevsData* pMixer = GetMixerByKeyIndex(iWhichMixer);
	if (pMixer) {

		// now do a recalc of onair packages for this mixer, rev could have changed what is onair
		// and also determine on air sources for all areas that mixer divisions are allocated to -- calc for those areas only
		CalculateMixerOnAirSources(iWhichMixer);

		ProcessAllRegionalCameraTallies(FALSE);

		// now update all MVs assoc to areas allocated to mixer 
		// go thru all devices and update for  all  labels using the changed src
		for (int iD = 0; iD<ssl_AllUMDDevices.count(); iD++)  {
			// get device record and store rev - recalc on new src and send to hardware
			bncs_string ssCurDev = ssl_AllUMDDevices[iD];
			CUmdDevice* pUmd = GetUmdDeviceRecord(ssCurDev);
			if (pUmd) {
				// update tallies for this umd device
				ProcAllTalliesForGivenDevice(pUmd);
			}
		} // for int iD

		// do all commands to be processed now
		if (!bNextCommandTimerRunning) ProcessNextCommand(BNCS_COMMAND_RATE);

		UpdateGuiForMixerOnAir(iWhichMixer);

		// XXXXX add more into tallies - txMon, ecut, 
		// update mixer TX monitor umd label with lowest name - if in ecut or not
		// int iNewState = pMixer->isMixerOnAirNOW(iIndex); 
		// bncs_string sslbl = ProcessAndUpdateTXMonitors(pMixer, iRevAreaType);

	}
	else 
		Debug("ProcessStudioMixer - error - no class for mixer for revtype %d", iWhichMixer);
}


/////////////////////////////////////////////////////////////////////////
//
//  GPIO / SQUID devices cards for CCU tally indication -- store revertive to know that cmd has been processed
//
void StoreGPIOCardRevertive( int iGPDev, int iWhichGpi, int iNewState )
{
	int iActualIndx =0;
	// just leave as fire and forget
}


/////////////////////////////////////////////////////////////////////////////
//
// CCU Revertives - processing
//

BOOL StoreCCURevertive( int iDevNum, int iSlotNum, bncs_string ssContent, BOOL *bChanged )
{
	Debug("into StoreCCURev - from slot %d str %s", iSlotNum, LPCSTR(ssContent));
	BOOL bChangedAssigment = FALSE;
	if (cl_CCU_Assignments) {
		CInfodriverRevs* pCCU = GetCCURecordByInfoSlot(iSlotNum);
		if (pCCU) {
			bncs_string ssPrevData = pCCU->ss_SlotData;
			int iPrevAssValue = pCCU->i_SlotValue_part1;
			int iPrevMixerChannel = pCCU->i_SlotValue_part2;
			if (!AreBncsStringsEqual(ssPrevData, ssContent)) {
				bChangedAssigment = TRUE;
			}
			// store and determine new rev
			pCCU->ss_SlotData = ssContent;
			// parse content to determine gallery and mixer channel
			// what rev type is it ?? CCU or CAM part of infodriver
			if (pCCU->iInfoUseType == REV_CCU_ASS_TYPE) {
				bncs_stringlist ssl = bncs_stringlist(ssContent, '_');
				if (ssl.count() > 1) {
					pCCU->i_SlotValue_part1 = ssl[1].toInt();
					if (bShowAllDebugMessages) Debug("CCURev ass -  %s  - %d  assigned to ccu %d", LPCSTR(ssContent), pCCU->i_SlotValue_part1, pCCU->i_Calculated_CCU);
					pCCU->i_Calculated_Gallery = 0;
					pCCU->i_Calculated_PCRCamera = 0;
					if (pCCU->i_SlotValue_part1 > 100) {
						int iGallery = pCCU->i_SlotValue_part1 / 100;
						int iCamera = pCCU->i_SlotValue_part1 % 100;
						// was if ((iGallery > 0) && (iGallery <= MAX_AREAS_WITH_CAMS) && (iCamera>0) && (iCamera <= MAX_CAMS_PER_AREA)) {
						if ((iGallery > 0) && (iGallery <= MAX_AREAS_WITH_CAMS) && (iCamera>0) && (iCamera <= MAX_CCU_ASSIGNMENT)) {
							pCCU->i_Calculated_Gallery = iGallery;
							pCCU->i_Calculated_PCRCamera = iCamera;
						}
					}
				}
				else { // cleared slot ??
					pCCU->i_SlotValue_part1 = 0;
					pCCU->i_Calculated_Gallery = 0;
					pCCU->i_Calculated_PCRCamera = 0;
					if (bShowAllDebugMessages) Debug("CCURev ass -  %s  - no assignment to ccu %d", LPCSTR(ssContent), pCCU->i_Calculated_CCU);
				}
				// update gui
				//if (pCCU->i_SlotValue_part1 > 0) 
				//	SetDlgItemInt(hWndDlg, 2050 + pCCU->i_Calculated_CCU, pCCU->i_SlotValue_part1, TRUE); 
				//else  
				//	SetDlgItemText(hWndDlg, 2050 + pCCU->i_Calculated_CCU, "");
			}
			else if (pCCU->iInfoUseType == REV_CCU_PTI_TYPE) {
				if (bShowAllDebugMessages) Debug("CCURev pti - received %s - cams routed to ccu %d ", LPCSTR(ssContent), pCCU->i_Calculated_CCU);
			}
			else if (pCCU->iInfoUseType == REV_CAM_CCU_TYPE) {
				bncs_stringlist ssl = bncs_stringlist(ssContent, '_');
				if (ssl.count() > 1) {
					pCCU->i_SlotValue_part1 = ssl[1].toInt();
					if (bShowAllDebugMessages) Debug("CCURev cam ccu - %s ccu %d to cam %d", LPCSTR(ssContent), pCCU->i_SlotValue_part1, pCCU->i_Calculated_PCRCamera);
					// update gui
				}
				else {
					// cleared assignment of ccu from cam
					pCCU->i_SlotValue_part1 = 0;
					if (bShowAllDebugMessages) Debug("CCURev cam ccu - %s ccu cleared from cam %d", LPCSTR(ssContent), pCCU->i_Calculated_PCRCamera);
				}
			}
			else if (pCCU->iInfoUseType == REV_CAM_ASS_TYPE) {
				pCCU->i_SlotValue_part1 = ssContent.toInt();
				if (pCCU->i_SlotValue_part1 == 1) Debug("CCURev cam %d routed to a ccu", pCCU->i_Calculated_PCRCamera);
				else if (pCCU->i_SlotValue_part1 == 2) Debug("CCURev cam %d assigned to a ccu", pCCU->i_Calculated_PCRCamera);
				else Debug("CCURev cam %d not routed to any ccu", pCCU->i_Calculated_PCRCamera);
			}
			else
				Debug("StoreCCURev - unknown type slot %d", iSlotNum);
		}
		//
		*bChanged = bChangedAssigment;
		return TRUE;
	} // if ccu
	// error of some sort
	*bChanged = bChangedAssigment;
	return FALSE;
	
}

void CalculateCCUGalleryUmd(CInfodriverRevs* pCCU)
{
	if (pCCU) {
		// calc gallery, camera etc
		pCCU->i_Calculated_Gallery = pCCU->i_SlotValue_part1;
		pCCU->i_Calculated_CamVirtual = 0;
		pCCU->i_Calculated_PCRCamera = 0;
		pCCU->ss_Calculated_UmdLabel = "";
		// get mixer package
		if (bShowAllDebugMessages) Debug("CalcCCUGallery - gall %d mixer %d", pCCU->i_Calculated_Gallery, pCCU->i_SlotValue_part2);
		if ((pCCU->i_Calculated_Gallery > 0) && (pCCU->i_Calculated_Gallery <= iNumberAreaGalleries) && (pCCU->i_SlotValue_part2>0)) {
			//
			CArea_Gallery* pGallery = GetAreaGallery_ByKey(pCCU->i_Calculated_Gallery);
			if (pGallery) {
				if ((pCCU->i_SlotValue_part2 > 0) && (pCCU->i_SlotValue_part2 <= pGallery->ssl_CameraGalleryVirtuals.count())) {
					pCCU->i_Calculated_CamVirtual = pGallery->ssl_CameraGalleryVirtuals[pCCU->i_SlotValue_part2 - 1];
					// get pcr camera from routed package -- camera virtual - which gives umd for AR / lighting
					CDestinationPackages* pDest = GetDestinationPackage(pCCU->i_Calculated_CamVirtual);
					if (pDest) {
						int iRoutSrcPkg = pDest->iTraced_Src_Package;
						if (iRoutSrcPkg>200) pCCU->i_Calculated_PCRCamera = iRoutSrcPkg - 200;   // src pkgs 201..2224 are the ccus -- may well / should match the pCCU->iRecordIndex ie 1..24
						if ((iRoutSrcPkg > 0) && (iRoutSrcPkg<=iNumberOfUserPackages)) pCCU->ss_Calculated_UmdLabel = ExtractStringfromDevIniFile(iPackagerAuto, pDest->iTraced_Src_Package, 0).replace('|', ' ');
						if (bShowAllDebugMessages) {
							Debug("CalcCCUGallery - calc vir pkg %d traced src pkg %d calc cam %d", pCCU->i_Calculated_CamVirtual, iRoutSrcPkg, pCCU->i_Calculated_PCRCamera);
							Debug("CalcCCUGallery - calc umd %s", LPCSTR(pCCU->ss_Calculated_UmdLabel));
						}
					}
				}
			}
		}
	}
}


void ProcessCCURevertive( int iDevNum, int iSlotNum )
{
	if (cl_CCU_Assignments) {
		CInfodriverRevs* pCCU = GetCCURecordByInfoSlot(iSlotNum);
		if (pCCU) {
			// test on REV TYPE first
			switch (pCCU->iInfoUseType) {
				case REV_CCU_ASS_TYPE:
					// some change in CCU assignment so 
					ProcessAllCCUCameraTallies();  
				break;
				case REV_CCU_PTI_TYPE:
				case REV_CAM_ASS_TYPE:
				case REV_CAM_CCU_TYPE:
					// nothin to do on these revs at this time
				break;
			}

		}
	}
}


//////////////////////////////////////////////////////////////////////////
//
//  Gallery ECUT processing
//

BOOL isInfodriverECUTDevice( int iDevNum, int iSlotNum, int *iWhichGallery )
{
	///XXXX ecut redo when known
	*iWhichGallery=UNKNOWNVAL;
	return FALSE;
}


BOOL StoreECUTRevertive( int iNewState, int iWhichGallery, BOOL *bChanged )
{
	*bChanged = FALSE;

	// XXXXX redo ecut - when known in Discovery

	return FALSE;
}
	

void ProcessECUTRevertive(int iNewState, int iWhichGallery)
{

	if ((iWhichGallery >= 1) && (iWhichGallery <= iNumberAreaGalleries)) {

		// XXXXX redo ecut - when known in Discovery

	}
}



//////////////////////////////////////////////////////////////////////////

/// RM recent list manipulation
BOOL FindEntryInRecentRMList(int iDev, int iDB, int iIndex)
{
	bncs_string ssStr = bncs_string("%1,%2,%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.find(ssStr)>=0) {
		//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s found", LPCSTR(ssStr));
		return TRUE;
	}
	else{
		//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s not found", LPCSTR(ssStr));
		return FALSE;
	}
} 


void AddEntryToRecentRMList( int iDev, int iDB, int iIndex )
{
	bncs_string ssStr = bncs_string("%1,%2,%3").arg(iDev).arg(iDB).arg(iIndex);
	if (ssl_RMChanges.find(ssStr)<0) {
		//if (bShowAllDebugMessages) Debug( "FindRMList - entry %s not found so adding", LPCSTR(ssStr));
		ssl_RMChanges.append( ssStr );		
		if (bRMDatabaseTimerRunning) KillTimer( hWndMain, DATABASE_TIMER_ID );
		// reset timer
		SetTimer( hWndMain, DATABASE_TIMER_ID, 350, NULL ); 
	}
} 

////////////////////////////////////////////////////////////////////////////////////////////
//
// strip quotes from info revertives 
//
void StripOutQuotes( LPSTR szStr ) 
{
	int iI, iLen = strlen( szStr );
	char szTmp[256]="";

	wsprintf(szTmp,"",NULL);
	if (iLen > 2) {
		for ( iI=0; iI < (iLen-2); iI++)
			szTmp[iI] = szStr[iI+1];
		szTmp[iI] = NULL; // terminate string
		iLen = strlen( szTmp );
	}
	strcpy( szStr, szTmp );	
}

//
//  FUNCTION: CSIClientNotify()
//
//  PURPOSE: Callback function for incoming CSI messages for client class
//
//  COMMENTS: the message is supplied in szMsg
LRESULT CSIClientNotify(extclient* pec,LPCSTR szMsg)
{

	if (pec&&szMsg) {
		UINT iParamCount = pec->getparamcount();

		BOOL bChanged = FALSE;
		int iDevNum = 0, iDBNum = 0, iRtrDest = 0, iRtrSource = 0, iWhichGpi = 0, iGPIState = 0;
		int iWhichTypeIndex = 0, iContent = 0, iRevType = 0, iPrevTracedSrcPkg = 0, iPrevRoutedSrcPkg=0;
		int iInfoRevType = UNKNOWNVAL, iSlotNum = 0, iIndexNum = 0, iPrevSource = 0, iPrevState = UNKNOWNVAL;

		LPSTR szParamRev, szDevNum, szSlot, szIndex, szContent;

		if (iParamCount > 0) szParamRev = pec->getparam(0);
		if (iParamCount > 1)  szDevNum = pec->getparam(1);
		if (iParamCount > 2)  szSlot = pec->getparam(2);
		if (iParamCount > 3)  szIndex = pec->getparam(3);
		if (iParamCount > 4)  szContent = pec->getparam(4);

		if (szDevNum) {
			iDevNum = atoi(szDevNum);
		}

		if (szSlot)  {
			iDBNum = atoi(szSlot);
			iSlotNum = atoi(szSlot);
			iRtrDest = atoi(szSlot);
			iWhichGpi = atoi(szSlot);
		}

		if (szIndex)  {
			iRtrSource = atoi(szIndex);
			iIndexNum = atoi(szIndex);
			iGPIState = atoi(szIndex);
		}

		if ((iParamCount > 4) && (szContent)) {
			if (strlen(szContent) > 0) iContent = atoi(szContent);  // for revertives that are just numbers;
		}

		UpdateCounters();
		switch (pec->getstate())
		{
		case REVTYPE_R:

			if ((bShowAllDebugMessages) && (!bAutomaticStarting)) Debug("CSIClient - Router Rev from dev %d, dest %d, src %d ", iDevNum, iRtrDest, iRtrSource);
			StoreValidRouterRevertive(iDevNum, iRtrDest, iRtrSource, &iPrevSource);
			if (!bAutomaticStarting) ProcessValidRouterRevertive(iDevNum, iRtrDest, iPrevSource);  // standard revertive for multiviewer -- not Nevion router OR Regional SDI

			break;

		case REVTYPE_I:
			if (bValidCollediaStatetoContinue ) {
				if ((iParamCount >= 5) && (szContent)) {
					if ((bShowAllDebugMessages) && (!bAutomaticStarting))	Debug("CSIClient - Info Rev from dev %d, slot %d, state %s ", iDevNum, iSlotNum, szContent);
					StripOutQuotes(szContent);
					iContent = atoi(szContent);  // for revertives that are just numbers;
					int iWhichInfoIndx = 0;
					int iPkgrInfoDriverType = 0;
					int iActualPackageIndx = iSlotNum;
					// test for mixers first 
					if (isInfodriverStudioMixerDevice(iDevNum, iSlotNum, &iRevType)) {
						// studio mixer revs
						if (StoreStudioMixerRevertive(iDevNum, iSlotNum, bncs_string(szContent), &bChanged, &iRevType) ) {
							if ((bChanged) && (!bAutomaticStarting)) {
								if ((bShowAllDebugMessages) && (!bAutomaticStarting))	Debug("StoredMixerRev - mixer %d index %d new state %s", iRevType, iSlotNum, szContent);
								ProcessStudioMixerRevertive(iRevType, iSlotNum);
							}
						}
					}
					else if (isInfodriverAPackagerDevice(iDevNum, iSlotNum, &iPkgrInfoDriverType, &iWhichInfoIndx, &iActualPackageIndx)) {
						if ((iActualPackageIndx > 0) && (iActualPackageIndx <= iNumberOfUserPackages)) {
							//
							if (iPkgrInfoDriverType == PKGR_ROUTER_TYPE) {
								if (StorePackagerRevertive(iActualPackageIndx, bncs_string(szContent), &bChanged, &iPrevTracedSrcPkg, &iPrevRoutedSrcPkg)) {
									if ((bChanged) && (!bAutomaticStarting)) {
										// process packager rev 
										ProcessPackagerRevertive(iActualPackageIndx, iPrevTracedSrcPkg, iPrevRoutedSrcPkg);
									}
								}
								else
									Debug("Csiclient - pkgr router fail to store dev %d slot %d - calc as inf index %d pkg %d ", iDevNum, iSlotNum, iWhichInfoIndx, iActualPackageIndx);
							}
							else if (iPkgrInfoDriverType == PKGR_DEST_STAT_TYPE) {
								if (StorePackagerDestStatus(iActualPackageIndx, bncs_string(szContent), &bChanged)) {
									if ((bChanged) && (!bAutomaticStarting)) {
										// process packager destination package status 
										ProcessPackagerDestStatus(iActualPackageIndx);
									}
								}
								else
									Debug("Csiclient - pkgr dest stat fail to store dev %d slot %d - calc as inf index %d pkg %d ", iDevNum, iSlotNum, iWhichInfoIndx, iActualPackageIndx);
							}
						}
						else 
							Debug("Csiclient - pkgr infodriver invalid from dev %d slot %d - calc as inf index %d pkg %d ", iDevNum, iSlotNum, iWhichInfoIndx, iActualPackageIndx);
					}
					else if (IsNevionRouterRevertive(iDevNum)) {
						BOOL bProcessFurther = StoreNevionRouterRevertive(iIP_RouterVideoHD, iDevNum, iSlotNum, bncs_string(szContent));
						// process changes based on new rev ( not if still the same )...
						if (bProcessFurther) ProcessNevionRouterRevertive(iIP_RouterVideoHD, iDevNum, bncs_string(szContent));
					}
					else if (isInfodriverECUTDevice(iDevNum, iSlotNum, &iRevType)) {
						if (StoreECUTRevertive(iContent, iRevType, &bChanged) ) {
							if ((bChanged) && (!bAutomaticStarting)) {
								ProcessECUTRevertive(iContent, iRevType);
							}
						}
					}
					else
						Debug("CSIClient - Unexpected INFO revertive from device %d slot %d", iDevNum, iSlotNum);
				}
				else {
					if (szContent)
						Debug("CSIClient - Incorrectly formed infodriver revertive from device %d slot %d pc %d str %s", iDevNum, iSlotNum, iParamCount, szContent);
					else
						Debug("CSIClient - Incorrect infodriver rev from device %d slot %d pc %d NO string ", iDevNum, iSlotNum, iParamCount);
				}

			}
			else
				Debug("CSIClient - Automatic in INVALID or FAIL  State - info revertive ignored");
			break;

		case REVTYPE_G:
			Debug("CSIClient - Unexpected GPIO revertive from device %d gpio %d", iDevNum, iWhichGpi);
			break;

		case DATABASECHANGE:
			if (bValidCollediaStatetoContinue ) {

				if (iDevNum == iPackagerAuto) {
					if (FindEntryInRecentRMList(iDevNum, iDBNum, iIndexNum) == FALSE) {
						if ((iDBNum == 0) || (iDBNum == 2) || (iDBNum == 4) || (iDBNum == 6) || (iDBNum == 9) || (iDBNum == 10) ) {
							strcpy(szRMData, LPCSTR(ExtractStringfromDevIniFile(iDevNum, iIndexNum, iDBNum)));
							if (bShowAllDebugMessages) Debug("CSIClient - S Database change message - %s; device %d db %d indx %d content %s",
								szMsg, iDevNum, iDBNum, iIndexNum, szRMData);
							// source package changes
							if ((iIndexNum > 0) && (iIndexNum <= iNumberOfUserPackages)) {
								dbmPkgs.setName(iPackagerAuto, iDBNum, iIndexNum, szRMData, false);
								CSourcePackages* pSpkg = GetSourcePackage(iIndexNum);
								if (pSpkg) {
									bChanged = ReloadASourcePackageData(iIndexNum, pSpkg);
									if ((iDBNum == 2) || (iDBNum == 9) || (iDBNum == 10)) ProcessRouterModifyForSourcePackageChange(iIndexNum, bChanged);
								}
								// update gui
								if (iChosenSrcPkg == iIndexNum) DisplaySourcePackageData();
							}
						}
						else if ((iDBNum == 1) || (iDBNum == 3) || (iDBNum == 5) || (iDBNum == 7)) {
							// dest changes -- like the actual dest used
							// need to change / reroute for those following this package
							strcpy(szRMData, LPCSTR(ExtractStringfromDevIniFile(iDevNum, iIndexNum, iDBNum)));
							if (bShowAllDebugMessages) Debug("CSIClient - D Database change message - %s; device %d db %d indx %d content %s",
								szMsg, iDevNum, iDBNum, iIndexNum, szRMData);
							// source package changes
							if ((iIndexNum>0) && (iIndexNum <= iNumberOfUserPackages)) {
								dbmPkgs.setName(iPackagerAuto, iDBNum, iIndexNum, szRMData, false);
								CDestinationPackages* pDpkg = GetDestinationPackage(iIndexNum);
								if (pDpkg) {
									bChanged = ReloadADestinationPackageData(iIndexNum, pDpkg);
									if ((iDBNum == 1) || (iDBNum == 3)) ProcessRouterModifyForDestinationPackageChange(iIndexNum);
								}
								// update gui
								if (iChosenDestPkg == iIndexNum) DisplayDestinationPackageData();
							}
						}

						//add most recent data to list of changed indexes for 591 - to ignore the 2nd RM
						AddEntryToRecentRMList(iDevNum, iDBNum, iIndexNum);
					}// if in recent list
				}
				else {
					if (iDevNum == iDevice) {
						if (iDBNum == iFriendsDatabase) {
							strcpy(szRMData, LPCSTR(ExtractStringfromDevIniFile(iDevNum, iIndexNum, iDBNum)));
							if (bShowAllDebugMessages) Debug("CSIClient - Friends DB - %s; device %d db %d indx %d content %s",
								szMsg, iDevNum, iDBNum, iIndexNum, szRMData);
							// source package friends changes
							if ((iIndexNum > 0) && (iIndexNum <= iNumberOfUserPackages)) {
								dbmPkgs.setName(iDevice, iDBNum, iIndexNum, szRMData, false);
								CSourcePackages* pSpkg = GetSourcePackage(iIndexNum);
								if (pSpkg) {
									Debug("CSIClient - Friends DB - src pkg %d friends was %s  now  %s", iIndexNum, LPCSTR(pSpkg->ssl_Assoc_Friends.toString(',')), szRMData);
									pSpkg->ssl_Assoc_Friends = bncs_stringlist(bncs_string(szRMData), ',');
									// update gui
									if (iChosenSrcPkg == iIndexNum) DisplaySourcePackageData();
								}
							}
						}
						CRevsRouterData* pRtr = GetValidRouter(iDevNum);
						if (pRtr) {
							if (FindEntryInRecentRMList(iDevNum, iDBNum, iIndexNum) == FALSE) {
								// is index linked to device
								ProcessRouterModifyForSDIChange(iDevNum, iIndexNum);
								//add most recent data to list of changed indexes  to ignore the 2nd RM
								AddEntryToRecentRMList(iDevNum, iDBNum, iIndexNum);
							}
						}
					}
				}
			}
			else
				Debug("CSIClient - Automatic in INVALID or FAIL  State - info revertive ignored");
			break;

		case STATUS:
			Debug("CSIClient - Status message is %s", szMsg);
			break;

		case DISCONNECTED:
			Debug("CSIClient - CSI has closed down - driver in FAIL  or  ERROR  state");
			iLabelAutoStatus = 2;
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
			SetDlgItemText(hWndDlg, IDC_INFO_STATUS, " ERROR: Disconnected");
			eiExtInfoId->updateslot(COMM_STATUS_SLOT, "0");	 // error	
			bValidCollediaStatetoContinue = FALSE;
			PostMessage(hWndMain, WM_CLOSE, 0, 0);
			break;

		}
	}
	else
		Debug("CSIClient - invalid pec class or szMsg passed");

	/* always return TRUE so CSI doesn't delete your client registration */
	return TRUE;

}


///////////////////////////////////////////////////////////////////////////////////////////
//
//  Infodriver Resilience
//
///////////////////////////////////////////////////////////////////////////////////////////
// 
//  Check infodriver status for resilience
//
// 
void CheckAndSetInfoResilience(void)
{

	// having connected to driver now then getmode for redundancy
	// get state of infodriver
	char szNM[32] = "", szCM[32] = "", szStr[64] = " ??unknown??";
	int iMode = eiExtInfoId->getmode(); // main info determines the status of all others -- to prevent mix occuring
	iOverallStateJustChanged = 0;

	// determine string for driver dialog window
	if ((iMode == INFO_RXMODE) || (iMode == IFMODE_TXRXINQ)) { // if still in rx
		strcpy(szStr, "RXonly - RESERVE");
		strcpy(szNM, "RXONLY");
		//Restart_CSI_NI_NO_Messages();
	}
	if (iMode == INFO_TXRXMODE){
		strcpy(szStr, "Tx/Rx - MAIN OK");
		strcpy(szNM, "TX-RX");
	}

	if (iOverallTXRXModeStatus == INFO_RXMODE) strcpy(szCM, "RXONLY");
	if (iOverallTXRXModeStatus == INFO_TXRXMODE) strcpy(szCM, "TX-RX");

	if (iMode != iOverallTXRXModeStatus) {
		Debug("CheckInfoMode- new mode %s(%d) different to current %s(%d) -- changing status", szNM, iMode, szCM, iOverallTXRXModeStatus);
		switch (iMode) {
		case INFO_RXMODE: case IFMODE_TXRXINQ:
			iLabelAutoStatus = 2;
			ForceAutoIntoRXOnlyMode();
			iOverallTXRXModeStatus = IFMODE_RXONLY;
			Debug("RingMaster Automatic running in  RXONLY  mode ");
			SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "RXonly RESERVE Mode");
			break;
		case INFO_TXRXMODE:
			iLabelAutoStatus = 0;
			ForceAutoIntoTXRXMode();
			iOverallTXRXModeStatus = IFMODE_TXRX;
			Debug("UMD Automatic running in  TX/RX  mode ");
			SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Tx/Rx - MAIN OK");
			eiExtInfoId->setslot(4092, "UMDT txrx on workstation %d ", iWorkstation);
			break;
		default:
			SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "?unknown?");
			iOverallTXRXModeStatus = IFMODE_NONE;
			Debug("UMD Automatic in an  UNKNOWN  infodriver mode ");
		}
	}
	iNextResilienceProcessing = 57; // reset counter 
}


void Restart_CSI_NI_NO_Messages(void)
{
	if (eiExtInfoId) {
		if (iOverallTXRXModeStatus == INFO_TXRXMODE)
			eiExtInfoId->requestmode = INFO_TXRXMODE;
		else
			eiExtInfoId->requestmode = IFMODE_TXRXINQ;
	}
	iOverallModeChangedOver = 0;
}


void ForceAutoIntoRXOnlyMode(void)
{
	iOverallTXRXModeStatus = UNKNOWNVAL;
	if (eiExtInfoId) {
		eiExtInfoId->setmode(INFO_RXMODE);					// force to rxonly -- so that resilient pair can take over
		eiExtInfoId->requestmode = TO_RXONLY;            // force other driver into tx immeadiately if running ??
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "FORCE RX");
	}
	iNextResilienceProcessing = 3; // reset counter 
	iOverallModeChangedOver = 4;
	iOverallTXRXModeStatus = UNKNOWNVAL;
}


void ForceAutoIntoTXRXMode(void)
{
	iOverallTXRXModeStatus = UNKNOWNVAL;
	if (eiExtInfoId) {
		eiExtInfoId->setmode(INFO_TXRXMODE);
		eiExtInfoId->requestmode = TO_TXRX;
		SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "FORCE TXRX");
	}
	iNextResilienceProcessing = 3; // reset counter 
	iOverallModeChangedOver = 4;
	iOverallTXRXModeStatus = UNKNOWNVAL;
}

