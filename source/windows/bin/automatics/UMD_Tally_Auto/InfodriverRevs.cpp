// InfodriverRevs.cpp: implementation of the CInfodriverRevs class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "InfodriverRevs.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInfodriverRevs::CInfodriverRevs(int iIndex, bncs_string ssKey, int iDriver, int iSlot)
{

	iRecordIndex = iIndex;
	ssRecordKey = ssKey;
	iInfoUseType = 0;

	iInfoDriver = iDriver;
	iInfoSlot = iSlot;

	ss_SlotData = "";
	i_SlotValue_part1 = 0;
	i_SlotValue_part2 = 0;

	i_AssociatedDestPackage = 0;
	i_AssociatedSrcPackage1 = 0;

	i_Associated_Resource_ID = 0;
	i_Associated_Resource_Index = 0;

	i_Linked_BNCSIndexR = 0;
	i_Linked_BNCSIndexG = 0;

	i_Calculated_Gallery = 0;
	i_Calculated_CamVirtual = 0;
	i_Calculated_PCRCamera = 0;
	i_Calculated_RedTally = -1;
	i_Calculated_GrnTally = -1;
	ss_Calculated_UmdLabel = "";
}

CInfodriverRevs::~CInfodriverRevs()
{

}
