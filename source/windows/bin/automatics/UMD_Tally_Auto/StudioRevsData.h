// StudioRevsData.h: interface for the CStudioRevsData class.  -  
// Mixer driver class and revs ---- no longer holds any area / gallery specifc data - other than area assigned/using to mixer inputs
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_StudioREVSDATA_H__E91EFE9A_005D_4168_900E_54541243E119__INCLUDED_)
#define AFX_StudioREVSDATA_H__E91EFE9A_005D_4168_900E_54541243E119__INCLUDED_

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
	//
	#include <windows.h>
	#include "UMD_Tally_Common.h"


class CStudioRevsData  
{
private:

	int m_iRecordKey;
	bncs_string m_ssMixerInstance;
	int m_iStudioMixerDeviceId;     // main infodriver for mixer driver -- MIXER 1-5
	int m_iStudioMixerOffset   ;     // any device offset from instances

	int m_iHighestMixerConfigEntry;
	int i_LowestOnAir_MixerChannel;     // lowest on air index from all Studio states

	int m_iAssignedAreaToSwitcher[KAHUNA_MIXER_MAXLEVELS];  // 32 divisions max -- though no more than 6 seen so far; -- division 0 means all divisions

	struct MixerDataStates {
		bncs_string ssMixer_RevData;  // for strings from Kahuna driver if NOW, ISOnn in use etc
		int iMixer_RevData;           // integer 0 offair  1 onair  - from string 
		int iAssociatedType1;        // src or dest package or sdi_rtr type  - dest pkg as default
		int iAssociatedDevice1;    //  device number of router or packager  ---- always packager for discovery at present
		int iAssociatedIndex1;      // sdi or package index -- but considered package as default
		BOOL bOnAirNOW;
		BOOL bOnAirISO; // determined from mixer state revertive for index
		// mixer divisions
		int iMxrLogicalSwitcher;    // assigned switcher ( 0 as default )  1..n divisions, 0 means shared / not in a division-- division input is assigned into - from xml
	};

	struct MixerDataStates  stChannelRev[MAXMIXERENTRIES];    // MAIN red/green tallies - may need to revise for just sources, no bus or ouptut

	struct MixerDataStates  stOutputsRev[65];    // mixer ouptuts 

	int iAssignedAreaKey;

public:
	CStudioRevsData( int iRecordKey, bncs_string ssInstance, int iMixerBaseInfo, int iDriverOffset );
	virtual ~CStudioRevsData();

	// mixer specific
	int getRecordKey(void);
	bncs_string getMixerInstance(void);
	int getMixerDeviceId(void);
	int getMixerDeviceOffset(void);
	int getMixerHighestEntry(void);

	void storeMixerAssocIndx(int iGPI, int iRtrDev, int iIndx1, int iType);
	int getMixerAssocIndx(int iGPI, int *iType, int *iRtrDev);  // or packages

	void storeOutputAssocIndx(int iGPI, int iRtrDev, int iIndx1, int iType);
	int getOutputAssocIndx(int iGPI, int *iType, int *iRtrDev);  // or packages

	void setGalleryDivisionAssignment(int iMixerDivision, int iAreaGallery);
	int getGalleryDivisionAssignment(int iMixerDivision);

	void setMixerInputLogicalSwitcher(int iMixerInput, int iAssocDiv);
	int getMixerInputLogicalSwitcher(int iMixerInput);

	void setMixerOutputLogicalSwitcher(int iMixerOutput, int iAssocDiv);
	int getMixerOutputLogicalSwitcher(int iMixerOutput);

	BOOL storeMixerState(int iGPI, bncs_string ssData, int iState);
	int getMixerState(int iGPI);                        // onair state for whatever level
	BOOL isMixerOnAirISO(int iGPI);               // ISO level onair or not
	BOOL isMixerOnAirNOW(int iGPI);           // NOW level on air or not

	BOOL storeOutputState(int iGPI, bncs_string ssData, int iState);
	int getOutputState(int iGPI);                        // onair state for whatever level
	BOOL isOutputOnAirISO(int iGPI);               // ISO level onair or not
	BOOL isOutputOnAirNOW(int iGPI);           // NOW level on air or not

	bncs_string getAnyOnAirSources(int iSrceType, int iTallyLevel);   // NOW, ISO  etc
	bncs_string getallOnAirDestinations(int iDestType, int iTallyLevel);

	int get_Lowest_MixerTX_OnAir(void);

	// public runtime - calculated list of traced on air src packages assoc to mixer
	bncs_stringlist ssl_ListTracedSrcPackages_OnAir;        // list of just traced sources packages - for gui only
	bncs_stringlist ssl_ListTracedSrcPackages_ISO;          // iso is on air - for GRN level tallies - from pacakages
};

#endif // !defined(AFX_StudioREVSDATA_H__E91EFE9A_005D_4168_900E_54541243E119__INCLUDED_)
