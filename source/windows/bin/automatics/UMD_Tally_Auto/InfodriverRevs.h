// InfodriverRevs.h: interface for the CInfodriverRevs class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INFODRIVERREVS_H__080CB23A_40C5_426F_A25C_40CAA79D1F34__INCLUDED_)
#define AFX_INFODRIVERREVS_H__080CB23A_40C5_426F_A25C_40CAA79D1F34__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#pragma warning(disable : 4786 4996)
//
#include <windows.h>
#include "UMD_Tally_Common.h"
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include "bncs_auto_helper.h"


class CInfodriverRevs  
{
public:
	CInfodriverRevs( int iIndex, bncs_string ssKey, int iDriver, int iSlot );
	virtual ~CInfodriverRevs();

	// this class used to store infodriver revs - linked to CCU, EVS assignments etc
	int iRecordIndex; // key into map  - slot index
	bncs_string ssRecordKey;

	int iInfoDriver;
	int iInfoSlot;
	int iInfoUseType;                  // code type for usage - which ccu type rev 

	bncs_string ss_SlotData;    // raw data from revertive from infodriver

	// in event of string list data :
	int i_SlotValue_part1;
	int i_SlotValue_part2;

	int i_AssociatedDestPackage;
	int i_AssociatedSrcPackage1;    // allows for range source packages for ccus, cameras etc

	int i_Associated_Resource_ID;       // eg main router -- often this data loaded from object settings xml, or area mixer 
	int i_Associated_Resource_Index;   // eg router source, or mixer channel etc...
		
	int i_Linked_BNCSIndexR;
	int i_Linked_BNCSIndexG;

	// new fields for CCU assignment
	int i_Calculated_CCU;              // for ccu rev these are assigned at init and do not change - else calc value from cam rev 
	int i_Calculated_Gallery;          // these calculated fields are determined from rev values in slotValue and other pkger revs for mixers
	int i_Calculated_CamVirtual;
	int i_Calculated_PCRCamera;
	int i_Calculated_RedTally;            // for ccu the tally that was last sent to gpio - thus send only those that change
	int i_Calculated_GrnTally;            // for ccu the tally that was last sent to gpio - thus send only those that change
	bncs_string ss_Calculated_UmdLabel;


};

#endif // !defined(AFX_INFODRIVERREVS_H__080CB23A_40C5_426F_A25C_40CAA79D1F34__INCLUDED_)
