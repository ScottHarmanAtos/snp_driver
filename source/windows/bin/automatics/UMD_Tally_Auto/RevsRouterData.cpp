// RevsRouterData.cpp: implementation of the CRevsRouterData class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RevsRouterData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRevsRouterData::CRevsRouterData( int iDeviceNumber, int iDests, int iSrcs, int iArea, int iNumberVirtuals )
{
	// initialise structure
	m_iRouterDeviceNumber=iDeviceNumber;
	m_iMaxDestinations=iDests;
	m_iMaxSources=iSrcs;
	m_iAssocAreaGallery = iArea;
	m_iNumberVirtuals = iNumberVirtuals;
	m_iStartVirtualSource = 0;
	m_iStartVirtualDestination = 0;

	// create entries in maps for dests and sources
	for (int iS = 1; iS <= m_iMaxSources; iS++) {
		CRouterSrceData* pData = new CRouterSrceData(iS);
		if (pData) { // add to map
			cl_SrcRouterData.insert(pair<int, CRouterSrceData*>(iS, pData));
		}
	}
	// create entries in maps for dests and sources
	for (int iD = 1; iD <= m_iMaxDestinations; iD++) {
		CRouterDestData* pData = new CRouterDestData(iD);
		if (pData) { // add to map
			cl_DestRouterData.insert(pair<int, CRouterDestData*>(iD, pData));
		}
	}

	if (iNumberVirtuals > 0) {
		m_iStartVirtualSource = m_iMaxSources - iNumberVirtuals+1;
		m_iStartVirtualDestination = m_iMaxDestinations - iNumberVirtuals+1;
	}
	

}

CRevsRouterData::~CRevsRouterData()
{
	// seek and destroy
	while (cl_DestRouterData.begin() != cl_DestRouterData.end()) {
		map<int, CRouterDestData*>::iterator it = cl_DestRouterData.begin();
		if (it->second) delete it->second;
		cl_DestRouterData.erase(it);
	}
	while (cl_SrcRouterData.begin() != cl_SrcRouterData.end()) {
		map<int, CRouterSrceData*>::iterator it = cl_SrcRouterData.begin();
		if (it->second) delete it->second;
		cl_SrcRouterData.erase(it);
	}
}


////////////////////////////////////////////////////
// private internal functions for src/dest maps 
CRouterSrceData* CRevsRouterData::GetSourceData(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		map<int, CRouterSrceData*>::iterator it = cl_SrcRouterData.find(iSource);
		if (it != cl_SrcRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}

CRouterDestData* CRevsRouterData::GetDestinationData(int iDest)
{
	if ((iDest>0) && (iDest <= m_iMaxDestinations)) {
		map<int, CRouterDestData*>::iterator it = cl_DestRouterData.find(iDest);
		if (it != cl_DestRouterData.end()) {  // was the entry found
			if (it->second)
				return it->second;
		}
	}
	return NULL;
}


///////////////////////////////////////////////////////////////
BOOL CRevsRouterData::storeGRDRevertive( int iDestination, int iSource, bncs_string ssData )
{
	// store given source from actual rev; any re-enterant src/dest calc then done  if required
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			// extract source and level as there should be from ip_router as it returns a complex revertive for NV9000 protocol
			char szlvl[64] = "";
			bncs_stringlist ssl_data = bncs_stringlist(ssData, ',');
			if (pData->m_iRoutedIndex != iSource) pData->m_iPreviousIndex = pData->m_iRoutedIndex;
			pData->ss_RouterRevertive = ssl_data.toString(',');
			pData->m_iRoutedIndex = iSource;
			//
			if (pData->iOverride_SlotIndex > 0) {
				if ((pData->bOverride_Perm == FALSE) && (iSource != pData->m_iPreviousIndex)) pData->ss_Override_UMD_Name = "";
			}
			return TRUE;
		}
		else
			OutputDebugString(LPCSTR(bncs_string("CRevsRouterData::storeGRDRevertive no data class for destination %1\n").arg(iDestination)));
	}
		else
			OutputDebugString(LPCSTR(bncs_string("CRevsRouterData::storeGRDRevertive negative destination or greater than maxUsed %1\n").arg(iDestination)));
	return FALSE;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int CRevsRouterData::getRouterNumber( void )
{
	return m_iRouterDeviceNumber;
}


int CRevsRouterData::getMaximumDestinations( void )
{
	return m_iMaxDestinations;
}


int CRevsRouterData::getMaximumSources( void )
{
	return m_iMaxSources;
}


int CRevsRouterData::getRouterAreaGallery(void)
{
	return m_iAssocAreaGallery;
}

int CRevsRouterData::getRouterStartVirtualSource(void)
{
	return m_iStartVirtualSource;
}

int CRevsRouterData::getRouterStartVirtualDestination(void)
{
	return m_iStartVirtualDestination;
}


int CRevsRouterData::getRoutedSourceForDestination(int iDestination)
{
	// return revertive source as  got from grd rev
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iRoutedIndex;
	}
	return 0;
}

int CRevsRouterData::getPreviousSourceForDestination(int iDestination)
{
	// return revertive source as  got from grd rev
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// get from map
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_iPreviousIndex;
	}
	return 0;
}



//////////////////////////////////////////////////////////////////////////
// linked umd device routines
//////////////////////////////////////////////////////////////////////////

void CRevsRouterData::addLinkedUmdDeviceforDest(int iDest, bncs_string ssUmdDev, int iInput)
{
	if ((iDest>0) && (iDest <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDest);
		if (pData) {
			bncs_string sstr = bncs_string("%1,%2").arg(ssUmdDev).arg(iInput);
			pData->m_ssl_LinkedToUmdDeviceAndIndex.append(sstr); // only at startup -  so add
		}
	}
}


void CRevsRouterData::addLinkedUmdDeviceforSrc(int iSrc, bncs_string ssUmdDev, int iInput)
{
	if ((iSrc>0) && (iSrc <= m_iMaxSources)) {
		CRouterSrceData* pData = GetSourceData(iSrc);
		if (pData) {
			bncs_string sstr = bncs_string("%1,%2").arg(ssUmdDev).arg(iInput);
			pData->m_ssl_LinkedToUmdDeviceAndIndex.append(sstr); // only at startup - so add
		}
	}
}


bncs_string CRevsRouterData::getLinkedUmdDevicesforDest(int iDest)
{
	if ((iDest>0) && (iDest <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDest);
		if (pData) {
			return pData->m_ssl_LinkedToUmdDeviceAndIndex.toString('|');
		}
	}
	return "";
}


bncs_string CRevsRouterData::getLinkedUmdDevicesforSrc(int iSrc)
{
	if ((iSrc>0) && (iSrc <= m_iMaxSources)) {
		CRouterSrceData* pData = GetSourceData(iSrc);
		if (pData) {
			return pData->m_ssl_LinkedToUmdDeviceAndIndex.toString('|');
		}
	}
	return "";
}


void CRevsRouterData::setSourceLinkedToMixer(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) pData->m_bAssocToMixer = TRUE;
	}
}


void CRevsRouterData::setDestinationLinkedToMixer(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) pData->m_bAssocToMixer = TRUE;
	}
}


BOOL CRevsRouterData::isSourceLinkedToAMixer(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) return pData->m_bAssocToMixer;
	}
	return FALSE;
}

BOOL CRevsRouterData::isDestinationLinkedToAMixer(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->m_bAssocToMixer;
	}
	return FALSE;
}


void CRevsRouterData::setSourceLinkedtoHighwayOrWrap(int iSource, int iWrapDest)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) pData->m_isHighwayOrWrap = iWrapDest;
	}
}


int CRevsRouterData::getDestOfHighwayOrWrap(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) return pData->m_isHighwayOrWrap;
	}
	return 0;
}



void CRevsRouterData::setUMDOverrideSlotIndex(int iDestination, int iSlot)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->iOverride_SlotIndex = iSlot;
		}
	}
}


void CRevsRouterData::setUMDOverrideLabel(int iDestination, bncs_string ssLabel, BOOL bPerm)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			pData->ss_Override_UMD_Name=ssLabel;
			pData->bOverride_Perm = bPerm;
		}
	}
}


bncs_string CRevsRouterData::getUMDOverrideLabel(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->ss_Override_UMD_Name;
	}
	return "";
}

BOOL CRevsRouterData::isUMDOverridePermanent(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->bOverride_Perm;
	}
	return FALSE;
}

int CRevsRouterData::getUMDOverrideSlotIndex(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) return pData->iOverride_SlotIndex;
	}
	return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CRevsRouterData::addPackageToSourceList(int iSource, int iPackage)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// is package already in list
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) {
			if (pData->m_ssl_SDIIndxUsedInPackages.find(bncs_string(iPackage)) < 0) {
				pData->m_ssl_SDIIndxUsedInPackages.append(bncs_string(iPackage));
				// now sort into ascending order; 
				pData->m_ssl_SDIIndxUsedInPackages.sort(true);
			}
		}
	}
}


void CRevsRouterData::removePackageFromSourceList(int iSource, int iPackage)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		// is package in list
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) {
			int iPos = pData->m_ssl_SDIIndxUsedInPackages.find(bncs_string(iPackage));
			if (iPos >= 0) {
				pData->m_ssl_SDIIndxUsedInPackages.deleteItem(iPos);
			}
		}
	}
}


bncs_string CRevsRouterData::getAllPackagesForSource(int iSource)
{
	bncs_string ssRet = "";
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) {
			ssRet = pData->m_ssl_SDIIndxUsedInPackages.toString(',');
		}
	}
	return ssRet;
}


int CRevsRouterData::getNumberPackagesUsingSource(int iSource)
{
	if ((iSource>0) && (iSource <= m_iMaxSources)) {
		CRouterSrceData* pData = GetSourceData(iSource);
		if (pData) {
			return pData->m_ssl_SDIIndxUsedInPackages.count();
		}
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void CRevsRouterData::addPackageToDestinationList(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// is package in list
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			if (pData->m_ssl_SDIIndxUsedInPackages.find(bncs_string(iPackage)) < 0) {
				pData->m_ssl_SDIIndxUsedInPackages.append(iPackage);
				// now sort into ascending order; 
				pData->m_ssl_SDIIndxUsedInPackages.sort(true);
			}
		}
	}
}


void CRevsRouterData::removePackageFromDestinationList(int iDestination, int iPackage)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		// is package in list
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			int iPos = pData->m_ssl_SDIIndxUsedInPackages.find(bncs_string(iPackage));
			if (iPos >= 0) {
				pData->m_ssl_SDIIndxUsedInPackages.deleteItem(iPos);
			}
		}
	}
}


bncs_string CRevsRouterData::getAllPackagesForDestination(int iDestination)
{
	bncs_string ssRet = "";
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_ssl_SDIIndxUsedInPackages.toString(',');
		}
	}
	return ssRet;
}


int CRevsRouterData::getNumberPackagesUsingDestination(int iDestination)
{
	if ((iDestination>0) && (iDestination <= m_iMaxDestinations)) {
		CRouterDestData* pData = GetDestinationData(iDestination);
		if (pData) {
			return pData->m_ssl_SDIIndxUsedInPackages.count();
		}
	}
	return 0;
}



