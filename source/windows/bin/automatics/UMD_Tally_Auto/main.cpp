///////////////////////////////////////////////
//	main.cpp - core code for the application //
///////////////////////////////////////////////

/************************************
  REVISION LOG ENTRY
  Revision By: Chris Gil
  Revised on 23/07/2002 19:40:00
  Version: V
  Comments: Added SNMP 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 18/07/2001 11:29:51
  Version: V
  Comments: Modified extinfo, extclient, extdriver, to have connect() function so
			all constructors take no parameters
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: TimA
  Revised on 28/03/2001 14:42:29
  Version: V
  Comments: new comms class specifies the serial port number in the open() function
  now, rather than the constructor
 ************************************/


/************************************
  REVISION LOG ENTRY
  Revision By: PaulW
  Revised on 20/09/2006 12:12:12
  Version: V4.2 
  Comments: r_p and w_p -- will look first for 4.5 CC_ROOT and SYSTEM,   
                        then  ConfigPath defined for V3 systems 
						or as a last resort the default Windows/Winnt directory for dev ini files etc 
 ************************************/

/************************************
  REVISION LOG ENTRY
  Revision By: RichardK
  Revised on 03/08/2007
  Version: V4.3
  Comments: Debug turns off redraw of list-box while updating it.  To stop flicker problems seen on
		systems accessed using Remote Desktop.
 ************************************/


#include "stdafx.h"
#define EXT
#include "UMD_Tally_Auto.h"

//
//  FUNCTION: WinMain()
//
//  PURPOSE: 
//
//  COMMENTS: main entry point for BNCS Driver "UMD_Tally_Auto"
//
//
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	MSG msg;
	HACCEL hAccelTable;
	char acTemp[256] = "", szLeft[256] = "", szRight[256] = "";
	char szDebug[256] = "";
	ssAutoInstance = "";
	iDevice = 0;

	lstrcpyn(acTemp,lpCmdLine,255);

	// deterime system type
	getBNCS_File_Path();
	// passed in parameter an instance name id- 
	// this check is done as windows shortcuts sometimes pass in whole target path and parameters sometimes
	char *pdest = strstr(acTemp, " ");
	if (pdest != NULL) {
		// space present - as string may include full path to driver
		if (SplitAString(acTemp, ' ', szLeft, szRight)) ssAutoInstance = bncs_string(szRight);
	}
	else
		ssAutoInstance = bncs_string(acTemp);
	// now call xml parser to extract device for instance name
	iDevice = getInstanceDevice(ssAutoInstance);
	wsprintf(szDebug, "UMDT -winmain- instance %s yielded device index %d \n", LPCSTR(ssAutoInstance), iDevice);
	OutputDebugString(szDebug);


	if (iDevice == 0) {
		// if still 0 then get hardcoded instance name
		wsprintf(szDebug, "UMDT -winmain- NULL instance device - so using instance UMD_Tally_Auto \n");
		OutputDebugString(szDebug);
		ssAutoInstance = "packager_umd_tally_auto";   // LDC default
		iWhichAutoArea = AUTO_AREA_LDC;
		ssAutoAreaString = "LDC";
		iDevice = getInstanceDevice(ssAutoInstance);
		wsprintf(szDebug, "UMDT -winmain- 2nd test - instance umd t auto device index %d area %d %s\n", iDevice, iWhichAutoArea, LPCSTR(ssAutoAreaString));
		OutputDebugString(szDebug);
	}
	else {
		// determine area
		bncs_string sstr = ssAutoInstance.upper();
		if (sstr.find("NHN") >= 0) {
			iWhichAutoArea = AUTO_AREA_NHN;
			ssAutoAreaString = "NHN";
		}
		else {
			iWhichAutoArea = AUTO_AREA_LDC;
			ssAutoAreaString = "LDC";
		}
		wsprintf(szDebug, "UMDT -winmain- instance %s ==> area %d %s \n", LPCSTR(ssAutoInstance), iWhichAutoArea, LPCSTR(ssAutoAreaString));
		OutputDebugString(szDebug);
	}

	// Initialize global strings
		LoadString(hInstance, IDS_SECTION, szAppName, MAX_LOADSTRING);
		LoadString(hInstance, IDS_APP_TITLE, acTemp, MAX_LOADSTRING);
		wsprintf(szTitle,acTemp,iDevice);
		LoadString(hInstance, IDC_APPCLASS, szWindowClass, MAX_LOADSTRING);

		MyRegisterClass(hInstance);

		// Perform application initialization:
		if (!InitInstance (hInstance, nCmdShow)) return FALSE;

		hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_UMD_TALLY_AUTO);
		// Main message loop:
		while (GetMessage(&msg, NULL, 0, 0)) 
		{
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

	return msg.wParam;

}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_REG);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCSTR)IDC_UMD_TALLY_AUTO;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	hWndMain = CreateWindow(szWindowClass, szTitle, WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX|CW_USEDEFAULT,
		10,10, APP_WIDTH, APP_HEIGHT, NULL, NULL, hInstance, NULL);

	if (!hWndMain)	{
		return FALSE;
	}

	ShowWindow(hWndMain,nCmdShow);

	UpdateWindow(hWndMain);

	return TRUE;
}



///////////////////////////////////////////////////////////////////////////////
// generic method to split string into two halves around a specified character
//
BOOL SplitAString(LPCSTR szStr, char cSplit, char *szLeft, char* szRight)
{
	int result = 0;
	char szInStr[MAX_AUTO_BUFFER_STRING] = "";
	strcpy(szInStr, szStr);
	strcpy(szLeft, "");
	strcpy(szRight, "");

	char *pdest = strchr(szInStr, cSplit);
	if (pdest != NULL) {
		result = pdest - szInStr + 1;
		if (result>0) {
			if (result == strlen(szInStr)) {
				strcpy(szLeft, szInStr);
			}
			else {
				strncpy(szLeft, szInStr, result - 1);
				szLeft[result - 1] = 0; // terminate
				strncpy(szRight, szInStr + result, strlen(szInStr) - result + 1);
			}
			return TRUE;
		}
		else {
			Debug("SplitAString pdest not null, BUT result 0 or less : %d", result);
		}
	}
	// error condition
	strcpy(szLeft, szInStr);
	return FALSE;
}




//
//  FUNCTION: AssertDebugSettings(HWND)
//
//  PURPOSE:  Sorts out window size and ini file settings for the two debug
//            controls - debug and hide.
//  
//	COMMENTS:
//	
void AssertDebugSettings(HWND hWnd)
{
	if (fDebug)
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_ON, MF_CHECKED);
	}
	else
	{
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_ON, MF_UNCHECKED);
	}

	RECT rctWindow;
	GetWindowRect(hWnd, &rctWindow);
	if (fDebugHide)
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, iChildHeight + 42, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_HIDE, MF_CHECKED);
	}
	else
	{
		MoveWindow(hWnd, rctWindow.left, rctWindow.top, APP_WIDTH, APP_HEIGHT, TRUE);
		CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_HIDE, MF_UNCHECKED);
	}
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  
//	COMMENTS:
//	
//	WM_COMMAND	- process the application menu
//  WM_DESTROY	- post a quit message and return
//	
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent, i=0;
	
	switch (message) 
	{
		case WM_CREATE:
			return InitApp(hWnd);
		break;
		
		case WM_TIMER:

			switch ( wParam ) {
				case STARTUP_TIMER_ID : // go thru start up now

					if (iStartupCounter==0) {
						// get required fields from instances and object settings
						KillTimer( hWnd , STARTUP_TIMER_ID );
						iLabelAutoStatus=1;
						//SetDlgItemText(hWndDlg, IDC_AUTO_ID, szAutoId );
						// load all required data for automatic
						if (fDebug) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_ON, MF_CHECKED);
						if (fLog) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_CHECKED);
						if (fDbgView) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_CHECKED);
						if (bShowAllDebugMessages) CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_CHECKED);
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Loading Data...");
						if ( LoadAllInitialisationData() ) {					
							// register with router, panel infodrivers etc
							SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Registering..." );
							AutomaticRegistrations();
							iStartupCounter++;
							PollAutomaticDrivers();
							// restart startup timer to poll out slowly
							iOneSecondPollTimerId = SetTimer(hWnd, STARTUP_TIMER_ID, STARTUP_TIMER_INTERVAL, NULL);
						}
						else {
							// set Auto in NONRUNNING STATE
							Debug("**** UMD AUTO -- NOT RUNNING - LOAD INIT ERRORS ****");
							Debug("**** UMD AUTO -- NOT RUNNING - LOAD INIT ERRORS ****");
							Debug("**** UMD AUTO -- NOT RUNNING - LOAD INIT ERRORS ****");
							iLabelAutoStatus = 2;
							ForceAutoIntoRXOnlyMode();
							SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "RX ONLY");
							SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "INIT ERRORS" );
						}
					}
					else {
						iLabelAutoStatus=0; // ie no init errors / issues
						// startupcounter >0
						int iq = qCommands.size();
						bncs_string sstr = bncs_string("Polling...(%1/%2)").arg(iStartupCounter).arg(iq);
						SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, LPCSTR(sstr));
						Debug("Startup -  %s", LPCSTR(sstr));
						// poll routers etc and get revertives - counter increments every 4 sec - so 180 secs for startup to get all revs -- packager has 32 X 4000 revs alone

						if ((iStartupCounter>0)&&(iStartupCounter<50)) {  
							int ii = 1;
							if (iStartupCounter>40) ii = 10;
							ProcessNextCommand(ii);
							if ((iStartupCounter == 3) || (iStartupCounter == 33)) {
								// check on status - go rx if required quicker in startup process
								CheckAndSetInfoResilience();
								Restart_CSI_NI_NO_Messages();
							}
						}
						else {
							CheckAndSetInfoResilience();
							Restart_CSI_NI_NO_Messages();
							iNextResilienceProcessing = 1;
							KillTimer( hWnd , STARTUP_TIMER_ID );

							if (bAutomaticStarting) {
								// check all rtr revs in from routers -- if ok do a first pass calc  -- one time only
								bAutomaticStarting = FALSE;
								Debug( "UMDT 1st Processing pass for startup");
								SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "1st Processing" );
								// FIRST PASS AT PROCESSING 
								//
								ProcessFirstPass_Logic( );   
								// check routes expected or not
								//
								//CheckAllUmdRouting( );
								bForceUMDUpdate = TRUE;
								ProcessTalliesForAllDevices( );    // calc and output all umds 
								ProcessLabelsForAllDevices( );     // calc and output all umds 
								bForceUMDUpdate = FALSE;
								
								// now deal with command que
								ProcessNextCommand(BNCS_COMMAND_RATE);
								
								SetDlgItemText(hWndDlg , IDC_AUTO_STATUS , "Running OK");
								Debug("UMDT Normal Running now");
								// finished first pass etc - now fully running
							} 

							// set timer for device polls - to check all revertives returned
							iOneSecondPollTimerId = SetTimer( hWnd, ONE_SECOND_TIMER_ID, 1000, NULL ); 
							bOneSecondPollTimerRunning = TRUE;
						}
						// inc counter
						iStartupCounter++;
					}

				break;


				case ONE_SECOND_TIMER_ID : // used for housekeeping and cyclic polling of hardware 

					// check on infodriver status
					iNextResilienceProcessing--;
					if (iNextResilienceProcessing<=0) {
						CheckAndSetInfoResilience();
					}
	
					// 2. check if recently failed over
					if (iOverallModeChangedOver>0) {
						iOverallModeChangedOver--;
						if (iOverallModeChangedOver <= 0) {
							if (bShowAllDebugMessages) Debug("RestartCSI_NI-NO messages called");
							Restart_CSI_NI_NO_Messages();
						}
					}

					// any other actions required during housekeeping timer -- process any pending commands if command timer not already running
					if (!bNextCommandTimerRunning) {
						ProcessNextCommand(BNCS_COMMAND_RATE);
					}

				break;

				case COMMAND_TIMER_ID:
					// process next command in command queue if any 
					ProcessNextCommand(BNCS_COMMAND_RATE);
					if (qCommands.size()==0) { // queue empty so stop timer
						if (bNextCommandTimerRunning) KillTimer(hWndMain, COMMAND_TIMER_ID);
						bNextCommandTimerRunning = FALSE;
					}
				break;	
				
				case DATABASE_TIMER_ID:
					// reload all panelsets and reassert panel devices
					KillTimer( hWndMain, DATABASE_TIMER_ID );
					bRMDatabaseTimerRunning = FALSE;
					ssl_RMChanges.clear();
					if (bShowAllDebugMessages) Debug( " RMtimer - clearing list ");
				break;

				case GALLERY_EGGBOX_TIMER:
				{
					// eggbox timer for galleries
					KillTimer(hWndMain, GALLERY_EGGBOX_TIMER);
					for (int iarea = 1; iarea <= iNumberAreaGalleries; iarea++) {
						CArea_Gallery* pGall = GetAreaGallery_ByKey(iarea);
						if (pGall) {
							if (pGall->ssAssigned_MixerInstance.length() > 0) SendMixerTXUmdLabel(iarea);
						}
					}
				}
				break;

				case FORCETXRX_TIMER_ID:
					Debug("ForceTxrx timer");
					KillTimer(hWndMain, FORCETXRX_TIMER_ID);
					ForceAutoIntoTXRXMode();
				break;

			}  // switch(wparam) on timer types

		break; // wmtimer

		case WM_SIZE: // if the main window is resized, resize the listbox too
			//Don't resize window with child dialog
		break;
		
		case WM_COMMAND:
		wmId    = LOWORD(wParam); 
		wmEvent = HIWORD(wParam); 
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
			break;
			
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
			
		case ID_DEBUG_CLEAR:
			SendMessage(hWndList,LB_RESETCONTENT,0,0L);
			break;

		case ID_DEBUG_ON:
			if (fDebug)
				fDebug = FALSE;
			else	{
				fDebug = TRUE;
				fDebugHide = FALSE;
			}
			AssertDebugSettings(hWnd);
			break;

		case ID_DEBUG_HIDE:
			if (fDebugHide)	{
				fDebugHide = FALSE;
				fDebug = TRUE;
			}
			else	{
				fDebugHide = TRUE;
				fDebug = FALSE;
			}
			AssertDebugSettings(hWnd);
			break;

		case  ID_DEBUG_SHOWALLMESSAGES:
			if (bShowAllDebugMessages) {
				Debug("-- turning off most debug messages");
				bShowAllDebugMessages = FALSE;
				CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_UNCHECKED);
			}
			else {
				Debug("++ Turning on all debug messages");
				bShowAllDebugMessages = TRUE;
				CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_SHOWALLMESSAGES, MF_CHECKED);
			}
			break;

		case ID_DEBUG_LOGTOFILE:
		{
			if (fLog) {
				Debug("-- cease logging debug messages to file");
				fLog = FALSE;
				CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_UNCHECKED);
			}
			else {
				fLog = TRUE;
				CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTOFILE, MF_CHECKED);
				Debug("++ commence logging debug messages to file");
			}
		}
		break;

		case ID_DEBUG_LOGTODBGVIEW:
			if (fDbgView) {
				Debug("-- cease sending messages to dbgview app");
				fDbgView = FALSE;
				CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_UNCHECKED);
			}
			else {
				fDbgView = TRUE;
				CheckMenuItem(GetSubMenu(GetMenu(hWnd), 1), ID_DEBUG_LOGTODBGVIEW, MF_CHECKED);
				Debug("++ commence sending messages to dbgview app");
			}
			break;

		case ID_OPTIONS_FORCETXRX:
			Debug("Forcing Automatic into TXRX Mode");
			ForceAutoIntoTXRXMode();
		break;

		case ID_OPTIONS_FORCERXONLY:
			Debug(" Trying to force driver into RXONLY mode");
			ForceAutoIntoRXOnlyMode();
		break;

		case ID_OPTIONS_RECALCULATETALLIES:
			Debug( " *** Resetting all UMDS ***");
			bForceUMDUpdate = TRUE;
			ProcessTalliesForAllDevices( );
			ProcessLabelsForAllDevices( );
			bForceUMDUpdate = FALSE;
		break;
			
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		} // switch (wmId)
		break;

	case WM_DESTROY:
		CloseApp(hWnd);
		PostQuitMessage(0);
		break;
		
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}


void ClearDeviceData() 
{
	SetDlgItemText(hWndDlg, IDC_UMD_DETAILS, ""); // details
	SetDlgItemText(hWndDlg, IDC_TALLY_MASK2, "");
	SetDlgItemText(hWndDlg, IDC_DEV_NAME, ""); // name
	SetDlgItemText(hWndDlg, IDC_DEV_LOC, ""); // name
	SetDlgItemText( hWndDlg, IDC_DEV_TYPE, "" ); // type
	SetDlgItemText( hWndDlg, IDC_DEV_INFO, "" ); // driver
	SetDlgItemText( hWndDlg, IDC_DEV_SLOTS, "" ); // start slot
	SendDlgItemMessage(hWndDlg, IDC_LIST2, LB_RESETCONTENT, 0, 0);
	iChosenMVIndex=UNKNOWNVAL;
}


void DisplayUmdDeviceData()
{
	// display list of umds for chosen dev
	ClearDeviceData();
	int iDevice = (SendDlgItemMessage(hWndDlg, IDC_LIST1,  LB_GETCURSEL , 0,0))+1; // LB_GETTEXT ??
	if ((iDevice>0)&&(iDevice<=iNumberActiveUmdDevs)) {
		CUmdDevice* pDev = GetValidUmdDevice(iDevice);
		if (pDev) {
			int iUMDS, iSrcDestType, iMVRtr=0, iMVFeed=0;
			bncs_string ssName, ssLabel, ssLoc;
			iCurrentChosenDevice = iDevice;
			iChosenMVIndex = UNKNOWNVAL;
			iLeftT=0; iRightT=0, iThirdT=0;
			pDev->getUMDDevice(&ssName, &ssLabel, &ssLoc, &iUMDS);
			ssCurrentChosenDevice = ssName;
			
			SetDlgItemText( hWndDlg, IDC_DEV_NAME, LPCSTR(ssName) ); // id
			SetDlgItemText( hWndDlg, IDC_DEV_LOC, LPCSTR(ssLoc) ); // loc
			if (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10) SetDlgItemText(hWndDlg, IDC_DEV_TYPE, "TSL V5");
			else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) SetDlgItemText(hWndDlg, IDC_DEV_TYPE, "KALEIDO");
			else if (pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) SetDlgItemText(hWndDlg, IDC_DEV_TYPE, "TSL 16");
			else SetDlgItemText(hWndDlg, IDC_DEV_TYPE, "TAG MV");
			SetDlgItemInt( hWndDlg, IDC_DEV_INFO, pDev->getBNCSDriver(), TRUE ); // driver
			SetDlgItemInt( hWndDlg, IDC_DEV_SLOTS, pDev->getDriverOffset(), TRUE ); // start offset
			
			int iMask = pDev->getUMDDeviceAreaGallery(1);
			CArea_Gallery* pGallery = GetAreaGallery_ByKey(iMask);
			if (pGallery) {
				SetDlgItemText(hWndDlg, IDC_TALLY_MASK2, LPCSTR(bncs_string("%1/%2 = %3").arg(pDev->getUMDDeviceAreaGallery(1)).arg(pDev->getUMDDeviceAreaCode(1)).arg(pGallery->ssAreaGallery_Name)));
			}

			for (int iU = 1; iU <= iUMDS; iU++) {
				// get and display umd details in list2
				bncs_string ssUmd = pDev->getAllUmdDetails (iU, &iSrcDestType, &iMVRtr, &iMVFeed );
				bncs_string ssUmd2 = pDev->getSecondUmdText(iU);
				char szTmp1[64] = "", szTmp2[32] = "", szData[256] = "";
				int iRoutedSrc = 0;
				if (iSrcDestType==DEST_UMD_TYPE) { 
					CRevsRouterData* pRTR = GetValidRouter(iIP_RouterVideoHD);
					CDestinationPackages* pDest = GetDestinationPackage(iMVFeed);
					int indx = -1;
					if (pDest&&pRTR) {
						iRoutedSrc = pRTR->getRoutedSourceForDestination(pDest->i_Master_HD_Device);
						indx = pDest->i_Master_HD_Device;
						wsprintf( szTmp1, "  DP  %04d/%04d ", indx, iRoutedSrc );
						wsprintf( szData, "%02d %s T=%d,%d,%d   P=%04d/%04d   %s %s", iU, szTmp1, pDev->getTallyState(iU, LEFT_TALLY), 
							pDev->getTallyState(iU, RIGHT_TALLY), pDev->getTallyState(iU, THIRD_TALLY), iMVFeed, pDest->iTraced_Src_Package, LPCSTR(ssUmd), LPCSTR(ssUmd2));
					}
					else {
						wsprintf(szTmp1, "  DP  err/err ");
						wsprintf(szData, "%02d %s  T=%d,%d,%d   P=%04d/%04d   %s %s", iU, szTmp1, pDev->getTallyState(iU, LEFT_TALLY),
							pDev->getTallyState(iU, RIGHT_TALLY), pDev->getTallyState(iU, THIRD_TALLY), iMVFeed, 0, LPCSTR(ssUmd), LPCSTR(ssUmd2));
					}
				}
				else if (iSrcDestType==SOURCE_UMD_TYPE) {
					wsprintf( szTmp1, "  SP   - - -   %04d ",  iMVFeed );
					CSourcePackages* pSrc = GetSourcePackage(iMVFeed);
					if (pSrc) {
						wsprintf(szTmp1, "  SP   - - -   %04d ", pSrc->i_Device_Srce_HD);
					}
					wsprintf( szData, "%02d %s T=%d,%d,%d   P=%04d   %s %s", iU, szTmp1, pDev->getTallyState(iU, LEFT_TALLY), 
						pDev->getTallyState(iU, RIGHT_TALLY), pDev->getTallyState(iU, THIRD_TALLY), iMVFeed, LPCSTR(ssUmd), LPCSTR(ssUmd2));
				}
				else if (iSrcDestType==FIXED_UMD_TYPE) {
					if ((iMVRtr > 0) && (iMVFeed > 0)) {
						//CRevsRouterData* pRTR = GetValidRouter(iMVRtr);
						//iRoutedSrc = pRTR->getRoutedSourceForDestination(iMVFeed);
						wsprintf(szTmp1, "  Fix   - - - %04d ", iMVFeed);  // more likely to be a source not a dest
					}
					else
						strcpy( szTmp1, "  Fix   - - - -  - - - - " );
					wsprintf( szData, "%02d %s T=%d,%d,%d   P=%04d   %s %s", iU, szTmp1, pDev->getTallyState(iU, LEFT_TALLY),  pDev->getTallyState(iU, RIGHT_TALLY), 
						pDev->getTallyState(iU, THIRD_TALLY), iMVFeed, LPCSTR(ssUmd), LPCSTR(ssUmd2));
				}
				else if (iSrcDestType==SRCE_MAIN_SDI_INDEX) {
					wsprintf( szTmp1, "  Src   - - -  %04d ",  iMVFeed );
					wsprintf( szData, "%02d %s T=%d,%d,%d  Rtr=%03d  %s %s", iU, szTmp1, pDev->getTallyState(iU, LEFT_TALLY), pDev->getTallyState(iU, RIGHT_TALLY), 
						pDev->getTallyState(iU, THIRD_TALLY), iMVRtr, LPCSTR(ssUmd), LPCSTR(ssUmd2));
				}
				else if (iSrcDestType==DEST_MAIN_SDI_INDEX) {
					CRevsRouterData* pRTR = GetValidRouter(iMVRtr);
					if (pRTR) iRoutedSrc = pRTR->getRoutedSourceForDestination(iMVFeed);
					wsprintf( szTmp1, "  Dst  %04d/%04d",  iMVFeed, iRoutedSrc );
					wsprintf( szData, "%02d %s T=%d,%d,%d  Rtr=%03d  %s %s", iU, szTmp1, pDev->getTallyState(iU, LEFT_TALLY), pDev->getTallyState(iU, RIGHT_TALLY), 
						pDev->getTallyState(iU, THIRD_TALLY), iMVRtr, LPCSTR(ssUmd), LPCSTR(ssUmd2));
				}
				else if (iSrcDestType==UNKNOWNVAL) {
					wsprintf( szData, "%02d   --   ----  - - -  - - - ", iU );
				}
				SendDlgItemMessage(hWndDlg, IDC_LIST2, LB_INSERTSTRING, -1, (LPARAM)szData);
			}
		}
	}
}


void DisplayTallyMaskForChosenIndex( void )
{
	SetDlgItemText( hWndDlg, IDC_UMD_DETAILS, "" ); // mask
	SetDlgItemText(hWndDlg, IDC_TALLY_MASK2, "");
	CUmdDevice* pDev = GetValidUmdDevice(iCurrentChosenDevice);
	if (pDev) {
		if (iChosenMVIndex > 0) {
			int iMask = pDev->getUMDDeviceAreaGallery(iChosenMVIndex);
			int iCode = pDev->getUMDDeviceAreaCode(iChosenMVIndex);
			CArea_Gallery* pGallery = GetAreaGallery_ByKey(iMask);
			if (pGallery) SetDlgItemText(hWndDlg, IDC_TALLY_MASK2, LPCSTR(bncs_string("%1/%2 = %3").arg(iMask).arg(iCode).arg(pGallery->ssAreaGallery_Name)));
			bncs_string sstr1 = "", sstr2 = "", sstr3 = "";
			int idb1 = 0, idb2 = 0;
			bncs_string sstrfmt = pDev->getUMDFormatRules(iChosenMVIndex, &sstr1);
			bncs_string sstrrep = pDev->getUMDReplaceRules(iChosenMVIndex, &sstr2);
			sstr3 = pDev->getUmdComment(iChosenMVIndex);
			if (sstr3.length()>0)
				SetDlgItemText(hWndDlg, IDC_UMD_DETAILS, LPCSTR(bncs_string("area %1    fmt %2 %3    rep %4 %5   comm:%6").arg(iMask).arg(sstrfmt).arg(sstr1).arg(sstrrep).arg(sstr2).arg(sstr3)));
			else
				SetDlgItemText(hWndDlg, IDC_UMD_DETAILS, LPCSTR(bncs_string("area %1    fmt %2 %3    rep %4 %5   ").arg(iMask).arg(sstrfmt).arg(sstr1).arg(sstrrep).arg(sstr2)));
		}
	}
}

void SendTestText( ) 
{
	if ((iCurrentChosenDevice>0)&&(iChosenMVIndex>0)) {
		CUmdDevice* pDev = GetValidUmdDevice(iCurrentChosenDevice);
		if (pDev) { 
			int iBNCSdev = pDev->getBNCSDriver();
			int iBNCSoff = pDev->getDriverOffset();
			bncs_string ssInst="", ssLabel="", ssLoc="";
			int iNumbUmds=0;
			pDev->getUMDDevice( &ssInst, &ssLabel, &ssLoc, &iNumbUmds );
			char szText[128]="";
			int iumdoffset = 0;
			if (pDev->getUMDDeviceType() == TAG_MV) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 1;
			else if (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 1;
			else if (pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 1;
			else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iChosenMVIndex + 100;
			wsprintf( szText, "-%s-input=%02d-", LPCSTR(ssInst), iChosenMVIndex );
			Debug( "Sending mv %d test umd %d label text: %s", iCurrentChosenDevice, iChosenMVIndex, szText );
			bncs_string ssNewCmd1 = bncs_string( "IW %1 %2 %3" ).arg(iBNCSdev).arg(szText).arg(iBNCSoff+iumdoffset);
			ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
		}
	}
}

void SendAllUmdTestText( )
{
	if (iCurrentChosenDevice>0) {
		CUmdDevice* pDev = GetValidUmdDevice(iCurrentChosenDevice);
		if (pDev) { 
			int iBNCSdev = pDev->getBNCSDriver();
			int iBNCSoff = pDev->getDriverOffset();
			bncs_string ssInst="", ssLabel="", ssLoc="";
			int iNumbUmds=0;
			pDev->getUMDDevice( &ssInst, &ssLabel, &ssLoc, &iNumbUmds );
			for (int iIndex=1;iIndex<=iNumbUmds;iIndex++) {
				char szText[128]="";
				int iumdoffset = 0;
				if (pDev->getUMDDeviceType() == TAG_MV) iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 1;
				else if (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10) iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 1;
				else if (pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 1;
				else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iIndex + 100;
				wsprintf( szText, "-%s-input=%02d-", LPCSTR(ssInst), iIndex );
				Debug( "Sending mv %d test umd %d label text: %s", iCurrentChosenDevice, iIndex, szText );
				bncs_string ssNewCmd1 = bncs_string("IW %1 %2 %3").arg(iBNCSdev).arg(szText).arg(iBNCSoff + iumdoffset);
				ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
			}
		}
	}
}


void SendTestTally( int iIndex, int iTally ) 
{
	if ((iCurrentChosenDevice>0)&&(iIndex>0)) {
		CUmdDevice* pDev = GetValidUmdDevice(iCurrentChosenDevice);
		if (pDev) { 
			//  for KX, Magic, TSL V3 or V5 -- only coded for v5 at present
			int iBNCSdev = pDev->getBNCSDriver();
			int iBNCSoff = pDev->getDriverOffset();
			int iumdoffset = 0;
			bncs_string ssNewCmd1="";
			// compare prev and new commands - only send out if different or forcing an update
			if (iTally==LEFT_TALLY) {
				if (pDev->getUMDDeviceType() == TAG_MV) iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 5;
				else if (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10) iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 5;
				else if (pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 5;
				else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iIndex + 200;				
				if (iLeftT == 0) {
					Debug("Sending test left tally ON");
					if (pDev->getUMDDeviceType() == TAG_MV) 
						ssNewCmd1 = bncs_string("IW %1 '#FF0000' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
					else
						ssNewCmd1 = bncs_string("IW %1 '1' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
					iLeftT = 1;
				}
				else {
					Debug("Sending test left tally OFF");
					if (pDev->getUMDDeviceType() == TAG_MV)
						ssNewCmd1 = bncs_string("IW %1 '#000000' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
					else
						ssNewCmd1 = bncs_string("IW %1 '0' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
					iLeftT=0;
				}
				ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
			}
			else if (iTally==RIGHT_TALLY) {
				int iTallyState = 1;
				if ((pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) || (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10)) {
					iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 10;
					iTallyState = 2;
				}
				else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iIndex + 300;
				else if (pDev->getUMDDeviceType() == TAG_MV) iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 6;
				if (iRightT == 0) {
					Debug("Sending test right tally ON");
					if (pDev->getUMDDeviceType() == TAG_MV)
						ssNewCmd1 = bncs_string("IW %1 '#00FF00' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
					else
						ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(iTallyState).arg(iBNCSoff + iumdoffset);
					iRightT=1;
				}
				else {
					Debug("Sending test right tally OFF");
					if (pDev->getUMDDeviceType() == TAG_MV)
						ssNewCmd1 = bncs_string("IW %1 '#000000' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
					else
						ssNewCmd1 = bncs_string("IW %1 '0' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
					iRightT=0;
				}
				ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
			}
			else if (iTally == THIRD_TALLY) {
				int iTallyState = 1;
				if ((pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) || (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10)) {
					iumdoffset = ((iIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 4;
					iTallyState = 3;
				}
				else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iIndex;
				if (iumdoffset > 0) {
					if ((iRightT == 0)) {
						Debug("Sending test third tally ON");
						ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(iTallyState).arg(iBNCSoff + iumdoffset);
						iThirdT = 1;
					}
					else {
						Debug("Sending test third tally OFF");
						ssNewCmd1 = bncs_string("IW %1 '0' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
						iThirdT = 0;
					}
					ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
				}
			}

		}
	}
}

void ResetChosenIndex( )
{
	if ((iCurrentChosenDevice>0)&&(iChosenMVIndex>0)) {
		CUmdDevice* pDev = GetValidUmdDevice(iCurrentChosenDevice);
		if (pDev) { 
			int iBNCSdev = pDev->getBNCSDriver();
			int iBNCSoff = pDev->getDriverOffset();
			int iumdoffset = 0;
			// reset label to current one stored
			Debug( "Reset -- label is %s", LPCSTR(pDev->getPrimaryUmdText(iChosenMVIndex)));
			if (pDev->getUMDDeviceType() == TAG_MV) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 1;
			else if (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 1;
			else if (pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 1;
			else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iChosenMVIndex + 100;
			bncs_string ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pDev->getPrimaryUmdText(iChosenMVIndex)).arg(iBNCSoff + iumdoffset);
			ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
			// reset UMD 2 for MCR
			if ((pDev->getUMDDeviceAreaCode(iChosenMVIndex) == MV_AREA_MCR) && (pDev->getUMDDeviceType() == TAG_MV)) {
				iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 3;
				bncs_string ssNewCmd2 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pDev->getSecondUmdText(iChosenMVIndex)).arg(iBNCSoff + iumdoffset);
				ecCSIClient->txinfocmd(LPCSTR(ssNewCmd2));
			}
			// reset left tally to current one stored
			if (pDev->getUMDDeviceType() == TAG_MV) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 5;
			else if (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 5;
			else if (pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 5;
			else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iChosenMVIndex + 200;
			if (pDev->getUMDDeviceType() == TAG_MV) {
				if (pDev->getTallyState(iChosenMVIndex, LEFT_TALLY)>0)
					ssNewCmd1 = bncs_string("IW %1 '#FF0000' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
				else
					ssNewCmd1 = bncs_string("IW %1 '#000000' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
			}
			else
				ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pDev->getTallyState(iChosenMVIndex, LEFT_TALLY)).arg(iBNCSoff + iumdoffset);
			ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
			// reset right tally to current one stored
			iumdoffset = 0;
			if (pDev->getUMDDeviceType() == TAG_MV) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + TAG_UMD_BASE_OFFSET + 6;
			if (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 10;
			else if (pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 10;
			else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iChosenMVIndex + 300;
			if (iumdoffset > 0) {
				if (pDev->getUMDDeviceType() == TAG_MV) {
					if (pDev->getTallyState(iChosenMVIndex, RIGHT_TALLY)>0)
						ssNewCmd1 = bncs_string("IW %1 '#00FF00' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
					else
						ssNewCmd1 = bncs_string("IW %1 '#000000' %2").arg(iBNCSdev).arg(iBNCSoff + iumdoffset);
				}
				else
					ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pDev->getTallyState(iChosenMVIndex, RIGHT_TALLY)).arg(iBNCSoff + iumdoffset);
				ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
			}
			// reset third tally to current one stored -- if applicable for device 
			iumdoffset = 0;
			if (pDev->getUMDDeviceType() == TSL_V5_DRIVER_10) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 4;
			else if (pDev->getUMDDeviceType() == TSL_UMD_TYPE_16) iumdoffset = ((iChosenMVIndex - 1) * SLOT_OFFSET_TAG_TSLV5) + 4;
			else if (pDev->getUMDDeviceType() == KALEIDO_MVIP) iumdoffset = iChosenMVIndex;
			if (iumdoffset > 0) {
				ssNewCmd1 = bncs_string("IW %1 '%2' %3").arg(iBNCSdev).arg(pDev->getTallyState(iChosenMVIndex, THIRD_TALLY)).arg(iBNCSoff + iumdoffset);
				ecCSIClient->txinfocmd(LPCSTR(ssNewCmd1));
			}
		}
	}
}


//
//  FUNCTION: DlgProcChild()
//
//  PURPOSE: Message handler for child dialog.
//
//  COMMENTS: Displays the interface status
//
// 
LRESULT CALLBACK DlgProcChild(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	int iLen = 0;
	int wmId    = LOWORD(wParam); 
	int wmEvent = HIWORD(wParam); 
	char szCmd[MAX_AUTO_BUFFER_STRING] = "";
	char szNum[16]="0", szAddr[MAX_AUTO_BUFFER_STRING]= "";

	HWND hButton = (HWND)lParam;
	LONG idButton=GetDlgCtrlID(hButton);

	switch (message)	{

		case WM_COMMAND :
			switch (wmId)	{
				case IDC_LIST1:		// list of devices - display details
					DisplayUmdDeviceData();
				break;
				case IDC_BUTTON1:
					if ((iCurrentChosenDevice>0)&&(iCurrentChosenDevice<=iNumberActiveUmdDevs)) {
						CUmdDevice* pDev = GetValidUmdDevice(iCurrentChosenDevice);
						if (pDev) { 
							bForceUMDUpdate = TRUE;
							ProcAllTalliesForGivenDevice( pDev );
							ProcLabelsForGivenDevice( pDev );
							bForceUMDUpdate = FALSE;
						}
					}
					break;
				case IDC_BUTTON2:
					if ((iCurrentChosenDevice>0)&&(iChosenMVIndex>0)) {
						SendTestText( );
					}
					break;
				case IDC_BUTTON3:
					if ((iCurrentChosenDevice>0)&&(iChosenMVIndex>0)) {
						SendTestTally( iChosenMVIndex, LEFT_TALLY );
					}
					break;
				case IDC_BUTTON4:
					if ((iCurrentChosenDevice>0) && (iChosenMVIndex>0)) {
						SendTestTally(iChosenMVIndex, RIGHT_TALLY);
					}
					break;
				case IDC_BUTTON7:
					if ((iCurrentChosenDevice>0) && (iChosenMVIndex>0)) {
						SendTestTally(iChosenMVIndex, THIRD_TALLY);
					}
					break;
				case IDC_BUTTON5:
					if ((iCurrentChosenDevice>0)&&(iChosenMVIndex>0)) {
						Debug("Reseting chosen index to real label and tally state");
						bForceUMDUpdate = TRUE;
						ResetChosenIndex();
						bForceUMDUpdate = FALSE;
					}
					break;
				case IDC_BUTTON6:
					if (iCurrentChosenDevice>0) {
						Debug("Sending Test text for all umds for chosen device");
						SendAllUmdTestText();
					}
					break;

				case IDC_BUTTON8:
				{
					// goto source
					iLen = GetDlgItemText(hDlg, IDC_EDIT1, szAddr, 16);
					int iCheckSrceNumber = atoi(szAddr);
					Debug("Button 8 - srce str %s number %d", szAddr, iCheckSrceNumber);
					if ((iCheckSrceNumber > 0) && (iCheckSrceNumber <= iPackagerParkSrcePkg)) {
						iChosenSrcPkg = iCheckSrceNumber;
						DisplaySourcePackageData();
					}
					else
						SetDlgItemInt(hWndDlg, IDC_EDIT1, iChosenSrcPkg, TRUE);
				}
				break;

				case IDC_BUTTON9:
				{
					// goto destination
					iLen = GetDlgItemText(hDlg, IDC_EDIT2, szAddr, 16);
					int iCheckDestNumber = atoi(szAddr);
					Debug("Button 9 - dest str %s number %d", szAddr, iCheckDestNumber);
					if ((iCheckDestNumber > 0) && (iCheckDestNumber <= iNumberOfUserPackages)) {
						iChosenDestPkg = iCheckDestNumber;
						DisplayDestinationPackageData();
					}
					else
						SetDlgItemInt(hWndDlg, IDC_EDIT2, iChosenDestPkg, TRUE);
				}
				break;

				case IDC_BUTTON10:
					iCurrentRegion++;
					if (iCurrentRegion > iNumberRegionals) iCurrentRegion = 1;
					SetDlgItemText(hWndDlg, 2161, "");
					SetDlgItemText(hWndDlg, 2162, "");
					SetDlgItemText(hWndDlg, 2163, "");
					SetDlgItemText(hWndDlg, 2164, "");
					SetDlgItemText(hWndDlg, 2051, "");
					SetDlgItemText(hWndDlg, 2052, "");
					SetDlgItemText(hWndDlg, 2053, "");
					SetDlgItemText(hWndDlg, 2054, "");
					ProcessARegionTallies(iCurrentRegion, FALSE, 0, 0);
				break;

				case IDC_LIST2:		// list of umds for chosne device
					iChosenMVIndex = (SendDlgItemMessage(hWndDlg, IDC_LIST2,  LB_GETCURSEL , 0,0))+1; // LB_GETTEXT ??
					iLeftT=0; iRightT=0;
					if (iChosenMVIndex>0) DisplayTallyMaskForChosenIndex();
				break;

			}
		break;
	
		 case WM_INITDIALOG: 
			//This will colour the background of the child dialog on startup to defined colour
			// brushes are defined in INIT_APP function below
			SetTextColor((HDC) wParam,COL_DK_BLUE);
			SetBkColor((HDC) wParam, COL_TURQ);
			HBr = hbrTurq;
			return (LRESULT)HBr;
			//HBr = CreateSolidBrush( COL_TURQ );
			//return (LRESULT)HBr;
		break;

		case WM_CTLCOLORDLG:
			// colour all diag at startup
			SetTextColor((HDC) wParam,COL_DK_BLUE);
			SetBkColor((HDC) wParam, COL_TURQ);
			HBr = hbrTurq;
			return (LRESULT)HBr;
		break;

		case WM_CTLCOLORLISTBOX:
			SetTextColor((HDC) wParam,COL_DK_BLUE);
			SetBkColor((HDC) wParam, COL_WHITE);
			HBr = hbrWhite;
			return (LRESULT)HBr;
		break;

		case WM_CTLCOLORSTATIC:
			 // to colour labels --  this will be called on startup of the dialog by windows
			 // this section will be called every time you do a SetDlgItemText() command for a given label
			 // by testing the value of global vars this can be used to colour labels for Comms OK/Fail, TXRX/Rxonly etc
			switch (idButton)	{	
				case IDC_INFO_STATUS : 
					if (iOverallTXRXModeStatus==INFO_TXRXMODE) {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_DK_GRN);
						HBr = hbrDarkGreen;
					}
					else if (iOverallTXRXModeStatus==INFO_RXMODE) {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_LT_RED);
						HBr = hbrLightRed;
					}
					else if (iOverallTXRXModeStatus==-1) {
						SetTextColor((HDC) wParam,COL_DK_BLUE);
						SetBkColor((HDC) wParam, COL_LT_ORANGE);
						HBr = hbrLightOrange;
					}
					else {
						SetTextColor((HDC) wParam,COL_DK_BLUE);
						SetBkColor((HDC) wParam, COL_WHITE);
						HBr = hbrWhite;
					}
				break;

				case IDC_AUTO_STATUS: 
					if (iLabelAutoStatus==1) {
						SetTextColor((HDC) wParam,COL_DK_BLUE);
						SetBkColor((HDC) wParam, COL_YELLOW);
						HBr = hbrYellow;
					}
					else if (iLabelAutoStatus==2) {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_LT_RED);
						HBr = hbrLightRed;
					}
					else if (iLabelAutoStatus==3) {
						SetTextColor((HDC) wParam,COL_BLACK);
						SetBkColor((HDC) wParam, COL_LT_ORANGE);
						HBr = hbrLightOrange;
					}
					else {
						SetTextColor((HDC) wParam,COL_WHITE);
						SetBkColor((HDC) wParam, COL_DK_GRN);
						HBr = hbrDarkGreen;
					}
				break;

				case IDC_VERSION: case IDC_IDRX: case IDC_IDTX: case IDC_QUEUE: case IDC_DPKG_UMD:
				case IDC_AUTO_ID: case IDC_DEV_TYPE: case IDC_DEV_NAME: case IDC_DEV_INFO: case IDC_DEV_SLOTS:
				case IDC_SPKG_NAME: case IDC_SPKG_SRCNAME: case IDC_SPKG_SRC: case IDC_SPKG_UMD: case IDC_AUTO_PKGR: 
				case IDC_DPKG_DEST: case IDC_DPKG_MVDEV: case IDC_DPKG_SRCPKG: case IDC_DPKG_NAME: case IDC_DPKG_SRC: 
				case IDC_DEV_LOC: case IDC_AUTO_SDIRTR: case IDC_AUTO_MGNDEV: case IDC_SPKG_MVDEV: case IDC_MIXER_GPI6:
				case IDC_MIXER_GPI1: case IDC_MIXER_GPI2: case IDC_MIXER_GPI3: case IDC_MIXER_GPI4: case IDC_MIXER_GPI5:
				case IDC_MIXER_GALLERY1: case IDC_MIXER_GALLERY2: case IDC_MIXER_GALLERY3: case IDC_MIXER_GALLERY4: case IDC_MIXER_GALLERY5:
				case IDC_MIXER_GALLERY6: case IDC_MIXER_GALLERY7: case  IDC_MIXER_GALLERY8: case IDC_MIXER_GPI7: case IDC_MIXER_GPI8: 
				case IDC_DPKG_TRACED: case IDC_DPKG_AREA: case IDC_DPKG_AREA2: case IDC_TALLY_MASK2: case IDC_AUTO_AREA:
				case IDC_AUTO_INFO1: case IDC_AUTO_INFO2: case IDC_AUTO_INFO3: case IDC_DPKG_RUTEOK: case IDC_SPKG_FRIENDS:
				case IDC_ONAIR_1: case IDC_ONAIR_2:  case IDC_ONAIR_3: case IDC_ONAIR_4: case IDC_ONAIR_5: case IDC_AUTO_FRIENDDB:
				case IDC_GALLASS_MXR1: case IDC_GALLASS_MXR2: case IDC_GALLASS_MXR3: case IDC_GALLASS_MXR4: case IDC_GALLASS_MXR5:
				case IDC_GALLASS_MXR6: case IDC_GALLASS_MXR7: case IDC_GALLASS_MXR8: case IDC_ONAIR_6: case IDC_ONAIR_7:  case IDC_ONAIR_8:
					SetTextColor((HDC) wParam,COL_DK_BLUE);	 
					SetBkColor((HDC) wParam, COL_WHITE);	
					HBr = hbrWhite;
				break;

			default:
				SetTextColor((HDC) wParam,COL_DK_BLUE);
				SetBkColor((HDC) wParam, COL_TURQ);
				HBr = hbrTurq;
			}
			return (LRESULT)HBr;
		break;
	
	default:
		break;
	}
	
    return FALSE;

}


//
//  FUNCTION: Version Info()
//
void VersionInfo() {
	LPBYTE   abData;
	DWORD  handle;
	DWORD  dwSize;
	LPBYTE lpBuffer;
	LPSTR szName, szModFileName, szBuf;
	char acBuf[16];
	#define	PATH_LENGTH	256
			
	szName=(LPSTR)malloc(PATH_LENGTH);
	szModFileName=(LPSTR)malloc(PATH_LENGTH);
	szBuf=(LPSTR)malloc(PATH_LENGTH);

	/*get version info*/

	GetModuleFileName(hInst,szModFileName,PATH_LENGTH);

	dwSize = GetFileVersionInfoSize(szModFileName, &handle);
	abData=(LPBYTE)malloc(dwSize);
	GetFileVersionInfo(szModFileName, handle, dwSize, abData);
	if (dwSize) {
		/* get country translation */
		VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
		/* make country code */
		wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
		if (dwSize!=0) {
			/* get a versioninfo file version number */
			wsprintf(szName,"\\StringFileInfo\\%s\\PRODUCTVERSION",(LPSTR)acBuf);
			VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
		}
		/* copy version number from byte buffer to a string buffer */
		lstrcpyn(szModFileName,(LPSTR)lpBuffer,(int)dwSize);
		/* copy to the dialog static text box */
		wsprintf(szBuf,"%s",szModFileName);
		// replace any commas with full stops
		for (int c=0; c<int(strlen(szBuf)); c++)
			if (szBuf[c]==44 ) szBuf[c]=46; 
		SetDlgItemText(hWndDlg, IDC_VERSION, szBuf);
	}			
	free((PVOID)abData);
	free((PVOID)szName);
	free((PVOID)szModFileName);
	free((PVOID)szBuf);
}

//
//  FUNCTION: About()
//
//  PURPOSE: Message handler for about box.
//
//  COMMENTS: Displays the compile date info
//				and used to display version info from
//				a version resource
//
// 
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
	case WM_INITDIALOG:
		{
			LPBYTE   abData;
			DWORD  handle;
			DWORD  dwSize;
			LPBYTE lpBuffer;
			LPSTR szName, szModFileName, szBuf;
			char acBuf[16];
#define	PATH_LENGTH	256
			
			szName=(LPSTR)malloc(PATH_LENGTH);
			szModFileName=(LPSTR)malloc(PATH_LENGTH);
			szBuf=(LPSTR)malloc(PATH_LENGTH);
			
			/*get version info*/
			
			GetModuleFileName(hInst,szModFileName,PATH_LENGTH);
			
			dwSize = GetFileVersionInfoSize(szModFileName, &handle);
			
			abData=(LPBYTE)malloc(dwSize);
			
			GetFileVersionInfo(szModFileName, handle, dwSize, abData);
			
			if (dwSize)
			{
				LPSTR szFileVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szProductVersion=(LPSTR)alloca(PATH_LENGTH);
				LPSTR szComments=(LPSTR)alloca(PATH_LENGTH);
				
				/* get country translation */
				VerQueryValue((LPVOID)abData, "\\VarFileInfo\\Translation", (LPVOID*)&lpBuffer, (UINT *) &dwSize);
				/* make country code */
				wsprintf(acBuf,"%04X%04X",*(WORD*)lpBuffer,*(WORD*)(lpBuffer+2));
				
				if (dwSize!=0)
				{
					/* get a versioninfo file version number */
					wsprintf(szName,"\\StringFileInfo\\%s\\FileVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, (LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szFileVersion,(LPSTR)lpBuffer,(int)dwSize);
				
				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\ProductVersion",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				
				/* copy version number from byte buffer to a string buffer */
				lstrcpyn(szProductVersion,(LPSTR)lpBuffer,(int)dwSize);
				
				/* copy to the dialog static text box */
				wsprintf(szBuf,"Version %s  (%s)",szFileVersion, szProductVersion);
				SetDlgItemText (hDlg, IDC_VERSION, szBuf);
				
				if (dwSize!=0)
				{
					wsprintf(szName,"\\StringFileInfo\\%s\\Comments",(LPSTR)acBuf);
					VerQueryValue((LPVOID)abData, (LPSTR) szName, 
						(LPVOID*)&lpBuffer,(UINT *) &dwSize);
				}
				/* copy to the dialog static text box */
				lstrcpyn(szComments,(LPSTR)lpBuffer,(int)dwSize);
				SetDlgItemText (hDlg, IDC_COMMENT, szComments);			
			}
			
			wsprintf(szName,"%s - %s",(LPSTR)__DATE__,(LPSTR)__TIME__);
			SetDlgItemText(hDlg,IDC_COMPID,szName);
			
			free((PVOID)abData);
			free((PVOID)szName);
			free((PVOID)szModFileName);
			free((PVOID)szBuf);
		}
		return TRUE;
		
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			return TRUE;
		}
		break;
	}
    return FALSE;

}

//
//  FUNCTION: InitApp()
//
//  PURPOSE: Things to do when main window is created
//
//  COMMENTS: Called from WM_CREATE message
//			 We create a list box to fill the main window to use as a
//			debugging tool, and configure the serial port and infodriver 
//			connections, as well as a circular fifo buffer
//
//
LRESULT InitApp(HWND hWnd)
{

	/* return 0 if OK, or -1 if an error occurs*/
		RECT rctWindow;
	
	OutputDebugString( " \n UMD TALLY AUTO starting up \n " );

	// define brushes and colours
	CreateBrushes();	
	iLabelAutoStatus=1;
	iOverallTXRXModeStatus = 0;
	iNextResilienceProcessing = 27;
	iOverallModeChangedOver = 0;
	iOverallStateJustChanged = 0;
	iCurrentChosenDevice = -1;
	iCheckSDIDest = -1;
	iChosenMVIndex = -1;
	strcpy(szDebug, "");
	strcpy(szResult, "");

	// Init Status Dialogs
	hWndDlg = CreateDialog(hInst, (LPCTSTR)IDD_STATUS, hWnd, (DLGPROC)DlgProcChild);
	GetClientRect(hWndDlg, &rctWindow);
	iChildHeight = rctWindow.bottom - rctWindow.top + 2;
		
	GetClientRect(hWnd,&rc);
	hWndList=CreateWindow("LISTBOX","",WS_CHILD | WS_VSCROLL | LBS_NOINTEGRALHEIGHT | LBS_USETABSTOPS,
						  0,iChildHeight,rc.right-rc.left,rc.bottom-rc.top-iChildHeight,
						  hWnd,(HMENU)IDW_LIST,hInst,0);
	ShowWindow(hWndList,SW_SHOW);

	bValidCollediaStatetoContinue = TRUE;
	bAutomaticStarting = TRUE;
	bForceUMDUpdate = TRUE;
	bRMDatabaseTimerRunning = FALSE;
	bNextCommandTimerRunning = FALSE;
	bShowAllDebugMessages = FALSE;   // now as default always start off
	iWorkstation = getWS();
	iNumberOfUserPackages = 0;       // default 128000
	iNumberOfInfodriversRequired = 0;     // 196 infodrivers required at present ( 32*6 blocks of pkgr infos + 3 riedel )
	iNumberInfosInBlock = 0;              // default 32 infodrivers in a block ( 128000 )
	iInfodriverDivisor = 0;               // default is 4000 - ie pkg 4001 is next infodriver etc -- could be 4096 if push comes to shove
	i_Start_VirtualPackages = 0;
	iCurrentRegion = 0;

	LoadIni();
	VersionInfo();
	SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Initialising");

		
		// connect to all the infodrivers
		ecCSIClient = new extclient;
		// connect to all the infodrivers
		eiExtInfoId = new extinfo;

		if ((ecCSIClient==NULL)||(eiExtInfoId==NULL)) {
			OutputDebugString( " \n UMD TALLY AUTO FAILED TO CREATE CSICLIENT OR EXTINFO CLASS \n " );
			Debug( " UMD TALLY AUTO FAILED TO CREATE CSICLIENT OR EXTINFO CLASS" );
			bValidCollediaStatetoContinue = FALSE;
		}

		if (bValidCollediaStatetoContinue ) {
			OutputDebugString( " UMD TALLY AUTO 1a \n " );

			// connect to CSI first
			ecCSIClient->notify(CSIClientNotify);
			switch (ecCSIClient->connect()){
				case CONNECTED: 
					Debug("Connected OK to CSI");
					ecCSIClient->setcounters(&lTXID, &lRXID);
					break;

				case CANT_FIND_CSI:
					Debug("ERROR ERROR - Connect failed to CSI ");
					Debug("ERROR ERROR -  Automatic not functioning");
					iLabelAutoStatus=2;
					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - CSI FAIL state");
					bValidCollediaStatetoContinue	= FALSE;
				break;

				case BAD_WS:
					Debug("ERROR ERROR - Connect failed to CSI - Bas WS ?? WTF ");
					Debug("ERROR ERROR -  Automatic not functioning");
					iLabelAutoStatus=2;
					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "BAD WS - in FAIL state");
					bValidCollediaStatetoContinue	= FALSE;
				break;

			default:
				Debug("Other Error - code %d",ecCSIClient->getstate());
				Debug("ERROR ERROR - Connect failed to CSI,  code :%d",ecCSIClient->getstate() );
				Debug("ERROR ERROR -  Automatic not functioning");
				iLabelAutoStatus=2;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - CSI error");
				bValidCollediaStatetoContinue	= FALSE;
			} // switch ec
		}

		if (bValidCollediaStatetoContinue ) {
			iOverallTXRXModeStatus = UNKNOWNVAL; // unknown first time
			// connect to External Infodriver
			if (eiExtInfoId) {
				eiExtInfoId->notify(InfoNotify);
				switch (eiExtInfoId->connect(iDevice)) { // connect to infodriver
				case CONNECTED: 
					Debug("Connected OK to infodriver %d",eiExtInfoId->iDevice);
					SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Connected OK");
					break;	
				case CANT_FIND_INFODRIVER:
					Debug("ERROR ERROR - Connect failed to infodriver %d",eiExtInfoId->iDevice);
					Debug("ERROR ERROR -  Automatic not functioning");
					SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Info connect FAILED");
					iLabelAutoStatus=2;
					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
					bValidCollediaStatetoContinue	= FALSE;
					break;
				default:
					Debug("ERROR ERROR - Connect failed to infodriver, code %d",eiExtInfoId->connect(iDevice));
					Debug("ERROR ERROR -  Automatic not functioning");
					SetDlgItemText(hWndDlg, IDC_INFO_STATUS, "Info connect FAILED");
					iLabelAutoStatus=2;
					SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
					bValidCollediaStatetoContinue	= FALSE;
				}
				eiExtInfoId->setcounters(&lTXID, &lRXID);
			}
			else {
				OutputDebugString( " \n UMD TALLY AUTO NO EXTINFO CLASS \n " );
				Debug( " UMD TALLY AUTO NO EXTINFO CLASS" );
				iLabelAutoStatus=2;
				SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "ERROR - in FAIL state");
				bValidCollediaStatetoContinue	= FALSE;
				ForceAutoIntoRXOnlyMode();
			}
		}
		else 
			return 0;

		if (bValidCollediaStatetoContinue ) {
		Debug( "Determining Automatic mode for first time");
			CheckAndSetInfoResilience();
			iNextResilienceProcessing = 10;
			if (iOverallTXRXModeStatus==INFO_RXMODE) {
				// try and force here once 
				eiExtInfoId->setmode(INFO_TXRXMODE);
				eiExtInfoId->requestmode = TO_TXRX;
				CheckAndSetInfoResilience();  // check again
			}
			// continue on to initialise vars, get object settings, dev registrations and poll for revs 

			iStartupCounter = 0;
			iLabelAutoStatus=1;
			iOneSecondPollTimerId = SetTimer( hWnd, STARTUP_TIMER_ID, STARTUP_TIMER_INTERVAL , NULL );
			SetDlgItemText(hWndDlg, IDC_AUTO_STATUS, "Starting Initialisaion ");
			eiExtInfoId->updateslot( COMM_STATUS_SLOT, "1" );	 // ok	

			// end of initialisation - driver running normally now on
		}
		
		return 0;
}

//
//  FUNCTION: CloseApp()
//
//  PURPOSE: Things to do when main window is destroyed
//
//  COMMENTS: Called from WM_DESTROY message
//			The listbox debug window is a child of the main, so it will be 
//			destroyed automatically. 
//
void CloseApp(HWND hWnd)
{
	// kill any timers
	KillTimer( hWnd , COMMAND_TIMER_ID );
	KillTimer( hWnd , ONE_SECOND_TIMER_ID );
	ecCSIClient->unregtallyrange();
	if (eiExtInfoId) ForceAutoIntoRXOnlyMode();
	
	if (bValidCollediaStatetoContinue ) {
		eiExtInfoId->updateslot( COMM_STATUS_SLOT, "0" ); // currently automatic in lost fail mode

		if (cl_AllRouters) {
			while (cl_AllRouters->begin() != cl_AllRouters->end()) {
				map<int, CRevsRouterData*>::iterator it = cl_AllRouters->begin();
				if (it->second) delete it->second;
				cl_AllRouters->erase(it);
			}
			delete cl_AllRouters;
		}

		if (cl_UMDDeviceTypes) {
			if (cl_AllUmdDevices) {
				while (cl_UMDDeviceTypes->begin() != cl_UMDDeviceTypes->end()) {
					map<int, CDeviceTypeConfig*>::iterator it = cl_UMDDeviceTypes->begin();
					if (it->second) delete it->second;
					cl_UMDDeviceTypes->erase(it);
				}
				delete cl_UMDDeviceTypes;
			}
		}

		if (cl_AllUmdDevices) {
			while (cl_AllUmdDevices->begin() != cl_AllUmdDevices->end()) {
				map<bncs_string,CUmdDevice*>::iterator it = cl_AllUmdDevices->begin();
				if (it->second) delete it->second;
				cl_AllUmdDevices->erase(it);
			}
			delete cl_AllUmdDevices;
		}
		
		if (cl_CCU_Assignments) {
			while (cl_CCU_Assignments->begin() != cl_CCU_Assignments->end()) {
				map<int,CInfodriverRevs*>::iterator it = cl_CCU_Assignments->begin();
				if (it->second) delete it->second;
				cl_CCU_Assignments->erase(it);
			}
			delete cl_CCU_Assignments;
		}
						
		if (cl_SourcePackages) {
			while (cl_SourcePackages->begin() != cl_SourcePackages->end()) {
				map<int,CSourcePackages*>::iterator it = cl_SourcePackages->begin();
				if (it->second) delete it->second;
				cl_SourcePackages->erase(it);
			}
			delete cl_SourcePackages;
		}
		if (cl_DestinationPackages) {
			while (cl_DestinationPackages->begin() != cl_DestinationPackages->end()) {
				map<int,CDestinationPackages*>::iterator it = cl_DestinationPackages->begin();
				if (it->second) delete it->second;
				cl_DestinationPackages->erase(it);
			}
			delete cl_DestinationPackages;
		}

		if (cl_MixerRevertives) {
			while (cl_MixerRevertives->begin() != cl_MixerRevertives->end()) {
				map<int,CStudioRevsData*>::iterator it = cl_MixerRevertives->begin();
				if (it->second) delete it->second;
				cl_MixerRevertives->erase(it);
			}
			delete cl_MixerRevertives;  
		}

		if (cl_AllGalleries) {
			while (cl_AllGalleries->begin() != cl_AllGalleries->end()) {
				map<int, CArea_Gallery*>::iterator it = cl_AllGalleries->begin();
				if (it->second) delete it->second;
				cl_AllGalleries->erase(it);
			}
			delete cl_AllGalleries;
		}

		if (cl_AllRegionals) {
			while (cl_AllRegionals->begin() != cl_AllRegionals->end()) {
				map<int, CRegionals*>::iterator it = cl_AllRegionals->begin();
				if (it->second) delete it->second;
				cl_AllRegionals->erase(it);
			}
			delete cl_AllRegionals;
		}

		if (cl_AllGPIO) {
			while (cl_AllGPIO->begin() != cl_AllGPIO->end()) {
				map<int, CGPIODevice*>::iterator it = cl_AllGPIO->begin();
				if (it->second) delete it->second;
				cl_AllGPIO->erase(it);
			}
			delete cl_AllGPIO;
		}

		ClearCommandQueue();


	}

	////xxxxx PostMessage( eiExtInfoId->hWndInfo, WM_CLOSE, 0, 0 );

	//delete fifoBuffer;
	if (eiExtInfoId)	delete eiExtInfoId;
	if (ecCSIClient)    delete ecCSIClient;

	// tidy up colours
	ReleaseBrushes();
	DeleteObject(HBr);

}

//
//  FUNCTION: LoadIni()
//
//  PURPOSE: Assign (global) variables with values from .ini file
//
//  COMMENTS: The [section] name is gotten from a stringtable resource
//			 which defines it. It is the same name as the application, defined by the wizard
//			
//			
void LoadIni(void)
{
	fLog = FALSE;
	fDebug = FALSE;
	fDbgView = FALSE;
	fDebugHide = TRUE;

	bncs_string ssXmlConfigFile = "umd_tally_auto";
	int idbVal = getXMLItemValue(ssXmlConfigFile, ssAutoInstance, "DebugMode").toInt();
	if (idbVal > 0) {
		fDebug = TRUE;
		fDebugHide = FALSE;
		if (idbVal > 1) bShowAllDebugMessages = TRUE;
	}
	//
	int iflVal = getXMLItemValue(ssXmlConfigFile, ssAutoInstance, "LogToFile").toInt();
	if (iflVal > 0) fLog = TRUE;
	else if (iflVal >1) fDbgView = TRUE;

	wsprintf(szClientCmd, "UMDT - Loadini - db %d -> %d      fl %d -> %d \n", idbVal, fDebug, iflVal, fLog);
	OutputDebugString(szClientCmd);
}


/*!
\returns int The workstation number

\brief Function to get the workstation number

First checks to see if %CC_WORKSTATION% is defined
otherwise returns the WS stored in %WINDIR%\csi.ini

NOTE: 
In BNCSWizard apps the following change should be made to LoadINI()
- Replace:
	iWorkstation=atoi(r_p("CSI.INI","Network","Workstation","0",FALSE));
- With:
	iWorkstation=getWS();
- Also add the following to "app_name.h":
int		getWS(void);
*/
int getWS(void)
{
	char* szWS = getenv( "CC_WORKSTATION" );
	
	if(szWS)
	{
		return atoi(szWS);
	}
	else
	{
		return atoi(r_p("CSI.INI","Network","Workstation","0",FALSE));
	}

}

// new function added to version 4.2 assigns the globals    szBNCS_File_Path and szBNCS_Log_File_Path
// called from r_p and w_p functions immediately below
// could easily be called once on first start up of app

char *	getBNCS_File_Path( void  )  
{
	char* szCCRoot = getenv( "CC_ROOT" );
	char* szCCSystem = getenv( "CC_SYSTEM" );
	char szV3Path[MAX_AUTO_BUFFER_STRING] = "";
	GetPrivateProfileString("Config", "ConfigPath", "", szV3Path, sizeof(szV3Path),"C:\\bncs_config.ini" );
	char szWinDir[MAX_AUTO_BUFFER_STRING] = "";
	GetWindowsDirectory(szWinDir, MAX_AUTO_BUFFER_STRING);

	// first check for 4.5 system settings
	if(szCCRoot && szCCSystem)	{
		sprintf(szBNCS_File_Path, "%s\\%s\\config\\system", szCCRoot, szCCSystem);
		sprintf(szBNCS_Log_File_Path, "%s\\%s\\logs", szCCRoot, szCCSystem);
	}
	else if (strlen(szV3Path)>0) {
		sprintf(szBNCS_File_Path,"%s",szV3Path);
		strcpy(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	else { 
		// all other BNCS / Colledia systems - get windows or winnt directory inc v3  that are not using bncs_config.ini
		sprintf(szBNCS_File_Path,"%s",szWinDir);
		strcpy(szBNCS_Log_File_Path, "C:\\bncslogs");
	}
	return szBNCS_File_Path;

}

/*!
\returns char* Pointer to data obtained
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* defval The default value to return if not found
\param BOOL fWrite Flag - write the default value to the file

\brief Function to read one item of user data from an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
char* r_p(char* file, char* section, char* entry, char* defval, BOOL fWrite)
{
	char szFilePath[MAX_AUTO_BUFFER_STRING];	
	char szPathOnly[MAX_AUTO_BUFFER_STRING];

	strcpy( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );


	GetPrivateProfileString(section, entry, defval, szResult, sizeof(szResult), szFilePath);
	if (fWrite)
		w_p(file,section,entry,szResult);
	
	return szResult;

}

/*!
\returns void 
\param char* file The file name only to read from - path is added in this function according to Colledia system
\param char* section The section - [section]
\param char* entry The entry - entry=
\param char* setting The value to write - =value

\brief Function to write one item of user data to an INI file
Calls 	function getBNCS_File_Path  which 
checks to see if %CC_ROOT% and %CC_SYSTEM% are defined
- If so the config files in %CC_ROOT%\%CC_SYSTEM%\config\system are used
else checks for V3 bncs_config.ini used 
- Otherwise the config files in %WINDIR% are used
*/
void w_p(char* file, char* section, char* entry, char* setting)
{

	char szFilePath[MAX_AUTO_BUFFER_STRING];	
	char szPathOnly[MAX_AUTO_BUFFER_STRING];

	strcpy( szPathOnly, getBNCS_File_Path() );
	if (strlen(szPathOnly)>0)
		wsprintf( szFilePath,  "%s\\%s", szPathOnly, file );
	else
		wsprintf( szFilePath,  "%s", file );

	WritePrivateProfileString(section, entry, setting, szFilePath);

}
//
//  FUNCTION: UpdateCounters()
//
//  PURPOSE: Writes current counter values to child dialog
//
//  COMMENTS: 
//				
//			
//			
void UpdateCounters(void)
{

	char szBuf[32];
	//Update Infodriver External Message Count
	wsprintf(szBuf, "%010d", lTXID);
	SetDlgItemText(hWndDlg, IDC_IDTX, szBuf);
	wsprintf(szBuf, "%010d", lRXID);
	SetDlgItemText(hWndDlg, IDC_IDRX, szBuf);
}

//
//  FUNCTION: Debug()
//
//  PURPOSE: Writes debug information to the listbox
//
//  COMMENTS: The function works the same way as printf
//				example: Debug("Number=%d",iVal);
//			
//			
void Debug(LPCSTR szFmt, ...)
{
	if (fDebug || fLog || fDbgView)
	{
		va_list argptr;

		va_start(argptr, szFmt);
		vsprintf(szDebug, szFmt, argptr);
		va_end(argptr);

		if (strlen(szDebug) > 820) szDebug[820] = NULL; // trim very long messages

		// log to dbview app
		if (fDbgView) {
			wsprintf(szLogDbg, "UMDTallyAuto - %s \n", szDebug);
			OutputDebugString(szLogDbg);
		}

		// app debug window
		if (fDebug || fLog) {

			char tBuffer[32];
			char szDate[64];
			char szTime[64];
			struct tm *newtime;
			time_t long_time;

			time(&long_time);                // Get time as long integer. 
			newtime = localtime(&long_time); // Convert to local time. 
			_strtime(tBuffer);
			sprintf(szDate, "%04d%02d%02d", newtime->tm_year + 1900, newtime->tm_mon + 1, newtime->tm_mday);
			wsprintf(szTime, "%02d:%02d:%02d", newtime->tm_hour, newtime->tm_min, newtime->tm_sec);
			wsprintf(szLogDbg, "%s %s", szTime, szDebug);

			// log to bncs logs dir
			if (fLog) {
				Log(szDate);
			}

			if (fDebug) {
				// truncate messages to fit app dbg window
				if (strlen(szLogDbg) > 220) szLogDbg[220] = NULL;
				//
				SendMessage(hWndList, WM_SETREDRAW, FALSE, 0);	//Stop redraws.
				SendMessage(hWndList, LB_ADDSTRING, 0, (LPARAM)szLogDbg);	//Add message
				long lCount = SendMessage(hWndList, LB_GETCOUNT, 0, 0);	//Get List Count
				if (lCount > MAX_LB_ITEMS)
				{
					SendMessage(hWndList, LB_DELETESTRING, 0, 0);	//Delete item
					lCount--;
				}
				SendMessage(hWndList, LB_SETTOPINDEX, (WPARAM)lCount - 1, 0);	//Set Top Index
				SendMessage(hWndList, WM_SETREDRAW, TRUE, 0);	//Restart redraws.
			}
		}
	}
}


//
// FUNCTION CreateLogDirectory
//
// new function Version 4.2 of the Wizard - to create log file directory according to Colledia system type
//
BOOL CreateLogFileDirectory(void)
{
	static bool bFirst = true;
	//create the log directory the first time
	if (bFirst) {
		_mkdir(szBNCS_Log_File_Path);
		bFirst = false;
		return TRUE;
	}
	return FALSE;
}

//
//  FUNCTION: Log()
//
//  PURPOSE: Writes information to the log file
//
//
void Log(LPCSTR szDate)
{
	// this function must only be called from Debug - as this dir sets up time and date
	// uses szLogDbg == global debug string for message logged ( set up in Debug function )

	char szLogFile[300];
	char   c = '\n';
	FILE *fp;

	wsprintf(szLogFile, "%s\\%s_UMDTallyAuto.log", szBNCS_Log_File_Path, szDate);
	// create log file directory if it doesn't exist
	CreateLogFileDirectory();
	if (fp = fopen(szLogFile, "a"))
	{
		fprintf(fp, "%s%c", szLogDbg, c);
		fclose(fp);
	}

}

//
// Function name	: SplitString
// Description	   : Split a delimited string to an array of pointers to strings
// Return type		: int number of elements returned
//
//----------------------------------------------------------------------------//
// Takes a string and splits it up creating an array of pointers to the start //
// of the sections delimted by the array of specified char delimiters         //
// the delimter characters in "string" are overwritten with NULLs             //
// Usage:                                                                     //
// char szString[] = "1,2|3'4,5,6,7,";                                        //
// char delim[] = {',', '|', '\'', ',', ',', ',', ',',};                      //
// UINT count=7;                                                              //
// char *pElements[7];                                                        //
// int iLen;                                                                  //
//	iLen = SplitString( szString, delim, 7, pElements);                       //
//                                                                            //
// NOTE: This funcion works differently to strtok in that consecutive         //
// delimiters are not overlooked and so a NULL output string is possible      //
// i.e. a string                                                              //
//    hello,,dave                                                             //
// where ,,, is the delimiter will produce 3 output strings the 2nd of which  //
// will be NULL                                                               //
//----------------------------------------------------------------------------//
int SplitString(char *string, char *delim, UINT count, char **outarray )
{

	UINT x;
	UINT y;
	UINT len;					// length of the input string
	static char *szNull = "";	// generic NULL output string
	int delimlen;
	int dp;

	len = strlen( string );
	delimlen = strlen( delim );

	if(!len)					// if the input string is a NULL then set all the output strings to NULL
	{
		for (x = 0 ; x < count; x++ )
			outarray[x] = szNull;
		return 0;
	}

	outarray[0] = string;		// set the 1st output string to the beginning of the string

	for( x = 0,y = 1 ; (x < len) && (y < count); x++ )
	{
		if( delimlen < 2 )
			dp = 1;
		else
			dp = y;
		if(string[x] == delim[dp-1])
		{
			string[ x ] = '\0';
			if((x+1) < len)		// if there is another string following this one....
			{
				outarray[ y ] = &string[x+1];
				y++;			// increment the number of assigned buffer
			}
			else
			{
				outarray[ y ] = szNull;
			}
		}
	}

	x = y;
	while( x < count )
		outarray[x++] = szNull;
	return y;					// return the number of strings allocated
	
}





/////////////////////////////////////////////////////////////////////////
// BRUSHES FOR BUTTONS & LABELS
/////////////////////////////////////////////////////////////////////////

/*********************************************************************************************************/
/*** Function to Create Colour brushes ***/
void CreateBrushes()
{ 
	hbrWindowsGrey = CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
	hbrBlack = CreateSolidBrush(COL_BLACK);
	hbrWhite = CreateSolidBrush(COL_WHITE);

	hbrDarkRed = CreateSolidBrush(COL_DK_RED);
	hbrDarkGreen = CreateSolidBrush(COL_DK_GRN);
	hbrDarkOrange = CreateSolidBrush(COL_DK_ORANGE);
	hbrDarkBlue = CreateSolidBrush(COL_DK_BLUE);

	hbrYellow = CreateSolidBrush(COL_YELLOW);
	hbrTurq = CreateSolidBrush(COL_TURQ);

	hbrLightRed = CreateSolidBrush(COL_LT_RED);
	hbrLightGreen = CreateSolidBrush(COL_LT_GRN);
	hbrLightOrange = CreateSolidBrush(COL_LT_ORANGE);
	hbrLightBlue = CreateSolidBrush(COL_LT_BLUE);

	return;
}

 /********************************************************************************************************/
void ReleaseBrushes()
{
	if (hbrBlack) DeleteObject(hbrBlack);
	if (hbrWhite) DeleteObject(hbrWhite);
	if (hbrWindowsGrey) DeleteObject(hbrWindowsGrey);

	if (hbrDarkRed) DeleteObject(hbrDarkRed);
	if (hbrDarkGreen) DeleteObject(hbrDarkGreen);
	if (hbrDarkBlue) DeleteObject(hbrDarkBlue);
	if (hbrDarkOrange) DeleteObject(hbrDarkOrange);

	if (hbrYellow)	DeleteObject(hbrYellow);
	if (hbrTurq)	DeleteObject(hbrTurq);
	if (hbrLightRed) DeleteObject(hbrLightRed);
	if (hbrLightBlue) DeleteObject(hbrLightBlue);
	if (hbrLightGreen) DeleteObject(hbrLightGreen);
	if (hbrLightOrange) DeleteObject(hbrLightOrange);

	return;
}

/*********************************************************************************************************/

