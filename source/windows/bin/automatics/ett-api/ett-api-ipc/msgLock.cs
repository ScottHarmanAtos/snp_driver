﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_ipc
{

    [Serializable()]
    public class msgLock
    {

        public const string PipeName = "BNCS_LOCKS";

        public msgLock()
        {
        }

        public msgLock(uint index, bool locked)
        {
            Index = index;
            Locked = locked;
        }

        public uint Index { get; set; }
        public bool Locked { get; set; }

        public override string ToString()
        {
            return $"IDX {Index} LCK {Locked}";
        }

    }
}
