﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_ipc
{

    [Serializable()]
    public class msgRoute
    {

        public const string PipeName = "BNCS_ROUTE";

        public msgRoute()
        {
        }

        public msgRoute(uint sourceIndex, uint destinationIndex)
        {
            SourceIndex = sourceIndex;
            DestinationIndex = destinationIndex;
        }

        public uint SourceIndex { get; set; }
        public uint DestinationIndex { get; set; }

        public override string ToString()
        {
            return $"SRC {SourceIndex}  DST {DestinationIndex}";
        }

    }
}
