﻿using System;

namespace ett_api_ipc
{

    [Serializable()]
    public class msgDb
    {

        public const string PipeName = "BNCS_DB";

        public msgDb()
        {
        }

        public msgDb(uint dbNumber, uint dbIndex, string value, bool init = false)
        {
            DbNumber = dbNumber;
            DbIndex = dbIndex;
            Value = value;
            Init = init;
        }

        public uint DbNumber { get; set; }
        public uint DbIndex { get; set; }
        public string Value { get; set; }
        public bool Init { get; set; }

        public override string ToString()
        {
            return $"DB: {DbNumber}  IDX: {DbIndex}  VA: {Value}";
        }

    }
}
