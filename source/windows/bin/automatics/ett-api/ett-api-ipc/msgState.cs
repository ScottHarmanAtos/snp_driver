﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_ipc
{
    [Serializable()]
    public class msgState
    {

        public const string PipeName = "BNCS_STATE";

        public msgState()
        {
        }

        public msgState(bool active)
        {
            Active = active;
        }

        public bool Active { get; set; }

    }
}
