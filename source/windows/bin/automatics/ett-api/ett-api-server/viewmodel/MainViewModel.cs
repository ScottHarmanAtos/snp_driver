﻿using driver_template.helpers;
using NLog;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using driver_template.model.bncs;
using driver_template.model.interfaces;
using System.Collections.Generic;
using ett_api_logic.packages;
using System.Collections.ObjectModel;

namespace driver_template.viewmodel
{
    public sealed class MainViewModel : IDisposable, INotifyPropertyChanged
    {
        private IMainModel MainModel = null;

        public MainViewModel(IMainModel m)
        {

            MainModel = m;

            BNCSStatus = new BNCSStatusViewModel(BNCSStatusModel.Instance);
            DeviceStatus = new DeviceStatusViewModel(MainModel);
            DeviceStatus.DeviceStateChanged += DeviceStatus_DeviceStateChanged;

            IconStatus = new IconStatusViewModel();

            BNCSStatus.CommsStateChanged += (sender, args) => { IconStatus.UpdateState(BNCSStatus.CommsStatus); };
            DeviceStatus.DeviceStateChanged += (sender, status) => { IconStatus.UpdateState(status.DeviceState); };

            IconStatus.UpdateState(BNCSStatus.CommsStatus);
            IconStatus.UpdateState(DeviceStatus.DeviceStatus);

            Version = string.Format("Version {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString());

        }

        private void Packages_OnInitComplete(object sender, EventArgs e)
        {
            OnPropertyChanged(nameof(AudioPackages));
            OnPropertyChanged(nameof(SourcePackages));
            OnPropertyChanged(nameof(DestinationPackages));
        }

        /// <summary>
        /// When the state changes, update the BNCSStatus when whether or not we should be incontrol
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeviceStatus_DeviceStateChanged(object sender, DeviceStatusViewModel.DeviceStateArgs e)
        {
            if (e.DeviceState == MultipleDeviceState.Connected || e.DeviceState == MultipleDeviceState.PartiallyConnected)
            {
                BNCSStatus.OkToBeInControl(true);
            }
            else
            {
                BNCSStatus.OkToBeInControl(false);
            }
        }

        public string Version
        {
            get;
        }

        public string Title
        {
            get { return "BNCS ETT API Bridge Driver: " + BNCSConfig.StartupArguments.Instance + " - " + String.Join(", ", Infodrivers.All.Distinct().Select(x => x.Key)); }
        }

        public IconStatusViewModel IconStatus
        {
            get;
            private set;
        }

        public DeviceStatusViewModel DeviceStatus
        {
            get;
            private set;
        }

        public BNCSStatusViewModel BNCSStatus
        {
            get;
            private set;
        }

        public bool DebugAll
        {
            get { return BNCSConfig.Settings.DebugLevel.Value == DebugLevels.All; }
        }

        public bool DebugNormal
        {
            get { return BNCSConfig.Settings.DebugLevel.Value == DebugLevels.Normal; }
        }

        public bool DebugOff
        {
            get { return BNCSConfig.Settings.DebugLevel.Value == DebugLevels.Off; }
        }

        public bool LogAll
        {
            get { return BNCSConfig.Settings.LogLevel.Value == DebugLevels.All; }
        }

        public bool LogNormal
        {
            get { return BNCSConfig.Settings.LogLevel.Value == DebugLevels.Normal; }
        }

        public bool LogOff
        {
            get { return BNCSConfig.Settings.LogLevel.Value == DebugLevels.Off; }
        }

        public ICommand SetDebugLevel
        {
            get
            {
                ICommand command = new RelayCommand(
                    x =>
                    {
                        SetLevel("Debug", (string)x, BNCSConfig.Settings.DebugLevel, Logging.SetLevelDebug);
                    });
                return command;
            }
        }

        public ICommand SetLogLevel
        {
            get
            {
                ICommand command = new RelayCommand(
                    x =>
                    {
                        SetLevel("Log", (string)x, BNCSConfig.Settings.LogLevel, Logging.SetLevelLog);
                    });
                return command;
            }
        }

        private void SetLevel(string name, string debugLevel, Setting<DebugLevels> setting, Action<DebugLevels> act)
        {
            DebugLevels dbLvl = Logging.ToLevel(debugLevel);

            //loggerAlways.Info("{0} Pressed: {1}", name, dbLvl.ToString());

            if (setting.Value != dbLvl)
            {
                setting.Value = dbLvl;

                act(dbLvl);

                OnPropertyChanged($"{name}All");
                OnPropertyChanged($"{name}Normal");
                OnPropertyChanged($"{name}Off");

                setting.Save();
            }
        }

        public ICommand SaveSettings
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    if (x != null && ((string)x).ToLower() == "overwrite")
                        DriverSettings.Save(null, true);
                    else
                        DriverSettings.Save();
                });
                return command;
            }
        }

        public ICommand ForceTxRx
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    BNCSStatus.ForceTxRx();
                });
                return command;
            }
        }

        public ICommand ForceRxOnly
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    BNCSStatus.ForceRxOnly();
                });
                return command;
            }
        }

        public ICommand Exit
        {
            get
            {
                ICommand command = new RelayCommand(
                x =>
                {
                    App.Current.Shutdown();
                });
                return command;
            }
        }

        public ICommand About
        {
            get
            {
                ICommand command = new RelayCommand(
                    x =>
                    {
                        var a = new about_box_space.about_box();
                        a.Show();
                    }
                    );
                return command;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void Dispose()
        {
            if (MainModel != null)
            {
                MainModel.Dispose();
            }
        }

        public ObservableCollection<AudioPackage> AudioPackages
        {
            get
            {
                return null;
            }
        }

        public ObservableCollection<SourcePackage> SourcePackages
        {
            get
            {
                return null;
            }
        }

        public ObservableCollection<DestinationPackage> DestinationPackages
        {
            get
            {
                return null;
            }
        }

    }
}
