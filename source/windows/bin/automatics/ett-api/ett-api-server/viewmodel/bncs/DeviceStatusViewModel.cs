﻿using driver_template.helpers;
using driver_template.model.interfaces;
using Gibraltar.Agent;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace driver_template.viewmodel
{

    public class DeviceStatusViewModel : INotifyPropertyChanged
    {

        const string STR_LoggingCategory = "BNCS_ETT_API.DeviceStatus";

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public event EventHandler<DeviceStateArgs> DeviceStateChanged;

        public class DeviceStateArgs : EventArgs
        {
            public MultipleDeviceState DeviceState { get; }
            public DeviceStateArgs(MultipleDeviceState DeviceState)
            {
                this.DeviceState = DeviceState;
            }
        }

        private readonly IMainModel MainModel;

        public DeviceStatusViewModel(IMainModel mainModel)
        {
            MainModel = mainModel;

            this.MainModel = mainModel;
            foreach (var device in mainModel.Devices)
            {
                device.CommsStatusChange += Device_CommsStatusChange;
                device.TXIncrement += Device_TXIncrement;
                device.RXIncrement += Device_RXIncrement;
            }
            UpdateCommsStatus();
        }

        private void Device_RXIncrement(object sender, EventArgs e)
        {
            RXCount++;
        }

        private void Device_TXIncrement(object sender, EventArgs e)
        {
            TXCount++;
        }

        private void Device_CommsStatusChange(object sender, EventArgs e)
        {
            UpdateCommsStatus();
        }

        private void DevicesCommsStatus_CommsStatusChange(object sender, EventArgs e)
        {
            UpdateCommsStatus();
        }

        private void UpdateCommsStatus()
        {
            int comms = MainModel.Devices.Where(x => x.CommsStatus == DeviceState.Connected).Count();
            if (MainModel.Devices.Count == 0)
            {
                DeviceStatus = MultipleDeviceState.Disconnected;
            }
            else if (comms == MainModel.Devices.Count)
            {
                DeviceStatus = MultipleDeviceState.Connected;
            }
            else if (comms > 0)
            {
                DeviceStatus = MultipleDeviceState.PartiallyConnected;
            }
            else
            {
                DeviceStatus = MultipleDeviceState.Disconnected;
            }
        }

        private MultipleDeviceState deviceStatus = MultipleDeviceState.Disconnected;
        public MultipleDeviceState DeviceStatus
        {
            get { return deviceStatus; }
            private set
            {
                if (deviceStatus != value)
                {
                    Log.Information(STR_LoggingCategory, $"Device Status Changed: {value}", null);
                    deviceStatus = value;
                    OnPropertyChanged("DeviceStatus");
                    DeviceStateChanged?.Invoke(this, new DeviceStateArgs(deviceStatus));
                }
            }
        }

        private long txcount = 0;
        public long TXCount
        {
            get { return txcount; }
            private set
            {
                if (value != txcount)
                {
                    txcount = value;
                    OnPropertyChanged("TXCount");
                }
            }
        }

        private long rxcount = 0;
        public long RXCount
        {
            get { return rxcount; }
            private set
            {
                rxcount = value;
                OnPropertyChanged("RXCount");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
