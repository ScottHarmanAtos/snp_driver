﻿using System;
using System.ComponentModel;
using System.Windows.Media;
using driver_template.helpers;

namespace driver_template.viewmodel
{
    public class IconStatusViewModel : INotifyPropertyChanged
    {
        public IconStatusViewModel()
        {
            Icon = Create(driver_template.Properties.Resources.Icon);
        }

        private BncsState comms = BncsState.Error;
        private MultipleDeviceState deviceState = MultipleDeviceState.Disconnected;

        public void UpdateState(BncsState comms)
        {
            this.comms = comms;
            SetIcon(this.comms, this.deviceState);
        }

        public void UpdateState(MultipleDeviceState deviceState)
        {
            this.deviceState = deviceState;
            SetIcon(this.comms, this.deviceState);
        }

        private void SetIcon(BncsState comms, MultipleDeviceState deviceState)
        {
            System.Drawing.Icon i = driver_template.Properties.Resources.Icon;
            if (comms == BncsState.TXRX)
            {
                i = driver_template.Properties.Resources.Good;
            }
            else
            {
                i = driver_template.Properties.Resources.Warning;
            }

            App.Current.Dispatcher.BeginInvoke((Action)(() =>
            {
                Icon = Create(i);
            }));
        }

        private ImageSource Create(System.Drawing.Icon i)
        {
            ImageSource isource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(
            i.Handle,
            System.Windows.Int32Rect.Empty,
            System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            return isource;
        }


        /// <summary>
        /// Icon to display.
        /// Note if run from a Shortcut it will show the shortcut icon, ignore this (windows is crappy)
        /// </summary>
        private ImageSource icon;
        public ImageSource Icon
        {
            get { return icon; }
            private set
            {
                if (icon != value)
                {
                    icon = value;
                    OnPropertyChanged("Icon");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
