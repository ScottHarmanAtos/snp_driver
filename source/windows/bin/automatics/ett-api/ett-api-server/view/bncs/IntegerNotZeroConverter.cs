﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace driver_template.view.bncs
{
    public abstract class IntegerNotZeroConverter<T> : IValueConverter
    {
        public IntegerNotZeroConverter(T trueValue, T falseValue)
        {
            True = trueValue;
            False = falseValue;
        }

        private T True { get; }
        private T False { get; }

        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && int.TryParse(value.ToString(), out var v))
                return v > 0 ? True : False;

            return false;
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class IntegerNotZeroToVisibilityConverter : IntegerNotZeroConverter<Visibility>
    {
        public IntegerNotZeroToVisibilityConverter()
            : base(Visibility.Visible, Visibility.Collapsed)
        {
        }
    }
}