﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace driver_template.view.bncs
{
    public class EnumNotEqualConverter : IValueConverter
    {
        public virtual object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter != null && value != null && int.TryParse(parameter.ToString(), out var param) && value.GetType().IsEnum)
                return param != (int)value;

            return false;
        }

        public virtual object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
