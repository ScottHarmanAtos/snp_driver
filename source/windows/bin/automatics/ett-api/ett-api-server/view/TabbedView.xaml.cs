﻿using System;
using System.Windows.Controls;

namespace driver_template.view
{
    /// <summary>
    /// Interaction logic for TabbedView.xaml
    /// </summary>
    public partial class TabbedView : UserControl
    {
        public TabbedView()
        {
            InitializeComponent();
        }

        private void TabItem_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            // Select the first tab that is visible (ideally the TabControl items should be a collection)
            foreach (TabItem tabItem in ((TabControl)((TabItem)sender).Parent).Items)
            {
                if (tabItem.Visibility == System.Windows.Visibility.Visible)
                {
                    tabItem.IsSelected = true;
                    return;
                }
            }
        }
    }
}
