﻿using BncsInfodriverLibrary;
using instances_and_devicetypes;
using System;
using System.Collections.Generic;

namespace driver_template.model.bncs
{
    public class ManagedInstances : ControlledInstances<Slot>
    {
        protected override string Name => "managed";

        /// <summary>
        /// Load the Slot
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="Slot">The Device Slot, without the offset added</param>
        /// <returns></returns>
        protected override (bool Success, (Slot Slot, Action<String> Update) SlotAndUpdate) LoadSlot(Instance instance, uint slotIndex, Parameter parameter)
        {

            //Check if the device is a managed one
            if (!Infodrivers.All.TryGetValue(instance.Device, out var i))
            {
                logger.Error($"Failed to get Instance:{instance.Id}, device:{instance.Device} is not managed.");
                return (false, (null, null));
            }

            uint SlotWithOffset = (uint)(slotIndex + instance.Offset);
            //Check if the index is within range
            if (i.maxIndexes() < SlotWithOffset)
            {
                if (parameter == null)
                    logger.Error($"Failed to get Instance:{instance.Id} ({instance.Device}:{SlotWithOffset}), it is higher than the max indexes ({i.maxIndexes()}) on this infodriver.");
                else
                    logger.Error($"Failed to get Instance:{instance.Id}:{parameter.Name} ({instance.Device}:{SlotWithOffset}), it is higher than the max indexes ({i.maxIndexes()}) on this infodriver.");
                return (false, (null, null));
            }

            i.slot(SlotWithOffset, out var v); //I assume we can ignore the return? 

            var s = Slot.Create((uint)instance.Device, SlotWithOffset, v, (str) =>
            {
                if (str == null)
                {
                    if (parameter == null)
                        logger.Error($"Attempt to set a {instance.Id} ({instance.Device}:{SlotWithOffset}) to Null, this has been ignored use String.Empty instead.");
                    else
                        logger.Error($"Attempt to set a {instance.Id}:{parameter.Name} ({instance.Device}:{SlotWithOffset}) to Null, this has been ignored use String.Empty instead.");
                    return;
                }

                if (parameter == null)
                    logger.Info($"Setting Slot {instance.Id} ({instance.Device}:{SlotWithOffset}) to \"{str}\"");
                else
                    logger.Info($"Setting Slot {instance.Id}:{parameter.Name} ({instance.Device}:{SlotWithOffset}) to \"{str}\"");

                i.setSlot(SlotWithOffset, str);
            }, BNCSStatusModel.Instance.TXCountIncrement, BNCSStatusModel.Instance.RXCountIncrement);

            return (true, s);
        }

        HashSet<uint> InfodriversLinked = new HashSet<uint>();

        /// <summary>
        /// Add a managed instance
        /// </summary>
        /// <param name="instance"></param>
        protected override bool _Add(Instance instance)
        {
            //See if the infodriver already exists.
            if (!Infodrivers.All.TryGetValue(instance.Device, out var info))
            {
                //We only go through here if the Infodriver has not already been added
                //If we are here it doesn't exist, see if we can link to it.
                if (!Infodrivers.Add(instance))
                {
                    return false;
                }

                //This is the first time we have seen this infodriver.
                //Get the infodriver and link to it.
                if (!Infodrivers.All.TryGetValue(instance.Device, out var i))
                {
                    //This shouldn't happen, as we exit above if we fail to create the infodriver
                    return false;
                }

                i.SlotRequestEvent += I_SlotRequestEvent;
                InfodriversLinked.Add(instance.Device);
            }

            //If not linked link now
            if (!InfodriversLinked.Contains(instance.Device))
            {
                info.SlotRequestEvent += I_SlotRequestEvent;
                InfodriversLinked.Add(instance.Device);
            }

            Instances.Add(instance.Id, instance);

            //This is used to load all the param/slots when the instance is added
            this.GetSlots(instance);
            return true;
        }

        private void I_SlotRequestEvent(uint device, uint index, string value)
        {
            if (!Update(device, index, value))
            {
                logger.Warn($"Received a Message for a slot we don't manage. Device: {device} Slot: {index} Value: \"{value}\". It will be ignored.");
            }
        }

    }
}