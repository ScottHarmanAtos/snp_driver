﻿namespace driver_template.model.bncs
{
    public struct StartupArguments
    {
        public string Instance;
        public bool Simulation;
        public bool Development;

        public StartupArguments(
            string instance,
            bool simulation,
            bool development
            )
        {
            Instance = instance;
            Simulation = simulation;
            Development = development;
        }
    }
}
