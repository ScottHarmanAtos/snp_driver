﻿using BncsClientLibrary;
using instances_and_devicetypes;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace driver_template.model.bncs
{
    public class ClientInstances : ControlledInstances<SlotClient>
    {
        protected override string Name => "client";
        protected override bool CanAddInstanceAtRuntime => true;

        protected BncsClient Client { get; }

        public ClientInstances(BncsClient client) : base()
        {
            Client = client;
            client.InfoRevertiveEvent += BncsClient_InfoRevertiveEvent;
            client.RouterRevertiveEvent += BncsClient_RouterRevertiveEvent;
        }

        private void BncsClient_RouterRevertiveEvent(uint device, uint index, int info)
        {
            Update(device, index, info.ToString());
        }

        private void BncsClient_InfoRevertiveEvent(uint device, uint index, string value)
        {
            Update(device, index, value);
        }

        public static Predicate<Instance> IsRouter
        {
            get;
            set;
        }

        /// <summary>
        /// Load the Slot
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="slotIndex">The Device Slot, without the offset added</param>
        /// <returns></returns>
        protected override (bool Success, (SlotClient Slot, Action<String> Update) SlotAndUpdate) LoadSlot(Instance instance, uint slotIndex, Parameter parameter)
        {
            //Register with the slot
            uint SlotWithOffset = (uint)(slotIndex + instance.Offset);
            Client.revertiveRegister((uint)instance.Device, SlotWithOffset, SlotWithOffset, false);

            //Check if the index is within range
            if (Client.maxIndexes() < SlotWithOffset)
            {
                if (parameter == null)
                    logger.Error($"Failed to get Instance:{instance.Id} ({instance.Device}:{SlotWithOffset}), it is higher than the max indexes ({Client.maxIndexes()}) on this infodriver.");
                else
                    logger.Error($"Failed to get Instance:{instance.Id}:{parameter.Name} ({instance.Device}:{SlotWithOffset}), it is higher than the max indexes ({Client.maxIndexes()}) on this infodriver.");
                return (false, (null, null));
            }

            bool isRouter = IsRouter?.Invoke(instance) ?? false;

            (SlotClient Slot, Action<string> Update) s = (null, null);
            if (isRouter)
            {
                Client.routerPoll((uint)instance.Device, SlotWithOffset, SlotWithOffset);
                BNCSStatusModel.Instance.TXCountIncrement();

                s = SlotRouter.Create((uint)instance.Device, SlotWithOffset, 0, (i) =>
                {
                    if (parameter == null)
                        logger.Info($"Setting RouterCrosspoint {instance.Id} ({instance.Device}:{SlotWithOffset}) to \"{i}\"");
                    else
                        logger.Info($"Setting RouterCrosspoint {instance.Id}:{parameter.Name} ({instance.Device}:{SlotWithOffset}) to \"{i}\"");

                    Client.routerCrosspoint((uint)instance.Device, SlotWithOffset, i);
                }, BNCSStatusModel.Instance.TXCountIncrement, BNCSStatusModel.Instance.RXCountIncrement, () =>
                {
                    Client.routerPoll((uint)instance.Device, SlotWithOffset, SlotWithOffset);
                });
            }
            else
            {
                //Poll now
                Client.infoPoll((uint)instance.Device, SlotWithOffset, SlotWithOffset);
                BNCSStatusModel.Instance.TXCountIncrement();

                s = SlotClient.Create((uint)instance.Device, SlotWithOffset, null, (str) =>
                {
                    if (str == null)
                    {
                        if (parameter == null)
                            logger.Error($"Attempt to set a {instance.Id} ({instance.Device}:{SlotWithOffset}) to Null, this has been ignored use String.Empty instead.");
                        else
                            logger.Error($"Attempt to set a {instance.Id}:{parameter.Name} ({instance.Device}:{SlotWithOffset}) to Null, this has been ignored use String.Empty instead.");
                        return;
                    }

                    if (parameter == null)
                        logger.Info($"Setting InfoWrite {instance.Id} ({instance.Device}:{SlotWithOffset}) to \"{str}\"");
                    else
                        logger.Info($"Setting InfoWrite {instance.Id}:{parameter.Name} ({instance.Device}:{SlotWithOffset}) to \"{str}\"");

                    Client.infoWrite((uint)instance.Device, SlotWithOffset, str, true);
                }, BNCSStatusModel.Instance.TXCountIncrement, BNCSStatusModel.Instance.RXCountIncrement, () =>
                {
                    Client.infoPoll((uint)instance.Device, SlotWithOffset, SlotWithOffset);
                });

            }
            return (true, s);
        }

        /// <summary>
        /// Add a managed instance
        /// </summary>
        /// <param name="instance"></param>
        protected override bool _Add(Instance instance)
        {
            Instances.Add(instance.Id, instance);

            return true;
        }
    }
}
