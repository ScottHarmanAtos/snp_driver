﻿using driver_template.helpers;
using NLog;
using System;
using System.ComponentModel;
using System.Threading;

namespace driver_template.model.bncs
{

    public enum SlotType
    {
        Infodriver,
        RouterClient,
        InfodriverClient,
        Database
    }

    public class Slot : IDisposable
    {
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        public static (Slot Slot, Action<String> Update) Create(uint device, uint index, string initialValue, Action<string> set, Action updateTxCount, Action updateRxCount)
        {
            var s = new Slot(device, index, initialValue, set, updateTxCount, updateRxCount);
            return (s, (str) => { s.Changed(str); });
        }

        protected Slot(uint device, uint index, string initialValue, Action<string> set, Action updateTxCount, Action updateRxCount)
        {
            Device = device;
            Index = index;
            this.value = initialValue;
            this.set = set;
            this.updateTxCount = updateTxCount;
            this.updateRxCount = updateRxCount;
        }

        public SlotType Type { get; protected set; } = SlotType.Infodriver;
        protected string value = null;
        private readonly Action<string> set;
        private readonly Action updateTxCount;
        private readonly Action updateRxCount;
        public uint Device { get; }
        public uint Index { get; }

        /// <summary>
        /// A Value of "---" may mean the db slot does not exist
        /// </summary>
        public virtual string Value
        {
            get { return value; }
            private set
            {
                this.value = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Value"));
            }
        }

        Timer NagleDelay = null;

        string NextValueToSet = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="ifChanged">If Trye will only send the change if it is different than the current value</param>
        public void Set(string value, bool ifChanged = true)
        {
            if (ifChanged == false || this.value != value)
            {
                SetValue(value);
                updateTxCount();
            }
        }

        protected void Set(String value)
        {
            this.set(value);
            ValueSent?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// Send revertive onto the network
        /// </summary>
        /// <param name="value"></param>
        protected virtual void SetValue(String value)
        {
            this.Set(value);
            this.Value = value;
        }

        /// <summary>
        /// Infowrite received
        /// </summary>
        /// <param name="value"></param>
        protected virtual void Changed(string value)
        {
            updateRxCount();
            OnChange?.Invoke(this, new EventSlotChange(this, value));
        }

        private bool disposed = false;
        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                if (NagleDelay != null)
                {
                    NagleDelay.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        public event EventHandler<EventSlotChange> OnChange;

        public event EventHandler ValueSent;

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class EventSlotChange : EventArgs
    {
        public Slot Slot { get; }
        public String Value { get; }
        public EventSlotChange(Slot slot, String value)
        {
            Slot = slot;
            Value = value;
        }
    }
}
