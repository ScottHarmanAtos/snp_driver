﻿using driver_template.model.bncs;
using ett_api_ipc;
using ett_api_logic;
using Gibraltar.Agent;
using IniParser;
using IniParser.Model;
using instances_and_devicetypes;
using NamedPipeWrapper;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.model
{
    public class DatabaseHandler : IDisposable
    {

        const string STR_LoggingCategory = "BNCS_ETT_API.DatabaseHandler";

        private FileIniDataParser iniParser;
        private Instance instance;
        private NamedPipeClient<msgDb> dbClient;

        public DatabaseHandler(Instance packagerMainInstance)
        {

            instance = packagerMainInstance;

            iniParser = new FileIniDataParser();
            BNCSConfig.BncsClient.DatabaseChangeEvent += BncsClient_DatabaseChangeEvent;

            dbClient = new NamedPipeClient<msgDb>(msgDb.PipeName);
            dbClient.AutoReconnect = true;
            dbClient.Disconnected += DbClient_Disconnected;
            dbClient.ServerMessage += DbClient_ServerMessage;
            dbClient.Start();

        }

        private void DbClient_ServerMessage(NamedPipeConnection<msgDb, msgDb> connection, msgDb message)
        {
            try
            {
                //we have received a request for an RM from the engine
                Log.Verbose(STR_LoggingCategory, "RM SENT", "Device: {0}  DB: {1} IDX {2} Value: {3}", instance.Device, message.DbIndex, message.DbNumber, message.Value);
                BNCSConfig.BncsClient.databaseModify(instance.Device, message.DbIndex, message.DbNumber, message.Value);
            }
            catch (Exception ex)
            {
                Log.Error(ex,STR_LoggingCategory, "Error sending RM", "Device: {0}  DB: {1} IDX {2} Value: {3}", instance.Device, message.DbIndex, message.DbNumber, message.Value);
            }
        }

        private void DbClient_Disconnected(NamedPipeConnection<msgDb, msgDb> connection)
        {
            Log.Warning(STR_LoggingCategory, "Named Pipe Connection lost", null, null);
        }

        private void BncsClient_DatabaseChangeEvent(uint device, uint index, int database)
        {
            try
            {
                if (device == instance.Device)
                {

                    if (!DbConstants.monitoredDbs.Contains((uint)database)) return;  //ignore any database not explicitly in the monitoring list

                    //this is for me
                    var value = ReadDbValue((uint)database, index);

                    Log.Verbose(STR_LoggingCategory, "RM RECEIVED", "Device: {0}  DB: {1} IDX {2} Value: {3}", instance.Device, database, index, value);

                    try
                    {
                        //Send the new value to the engine
                        dbClient.PushMessage(new msgDb((uint)database, index, value));
                    }
                    catch (Exception ex)
                    {
                        //capture error
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex,STR_LoggingCategory, "Error receiving RM", "Device: {0}  DB: {1} IDX {2}", instance.Device, database, index);
            }
        }

        private string ReadDbValue(uint dbNumber, uint index)
        {

            //db file name extensions might or might not be padded!
            string dbName = $"{instances_and_devicetypes.Helper.ConfigPath}/system/dev_{instance.Device}.db{dbNumber}";
            string dbAltName = $"{instances_and_devicetypes.Helper.ConfigPath}/system/dev_{instance.Device}.db{dbNumber.ToString("D4")}";

            IniData data = null;

            if (System.IO.File.Exists(dbName))
            {
                data = iniParser.ReadFile(dbName);
            }
            else if (System.IO.File.Exists(dbAltName))
            {
                data = iniParser.ReadFile(dbAltName);
            }
            else
            {
                //Log.Critical(STR_LoggingCategory, $"Unable to find database file for index {dbIndex}", null);
                return null;  //can't continue
            }

            return data[$"Database_{dbNumber}"][index.ToString("D4")];

        }

        public void Dispose()
        {
            dbClient.Stop();
        }

    }
}
