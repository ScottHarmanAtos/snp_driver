﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using driver_template.model.bncs;
using driver_template.model.interfaces;
using Gibraltar.Agent;
using System.Diagnostics;
using ett_api_ipc;
using NamedPipeWrapper;

namespace driver_template.model
{
    //Driver Template Version:0.0.14.1

    public sealed class MainModel : IDisposable, IMainModel
    {

        const string STR_LoggingCategory = "BNCS_ETT_API.MainModel";

        private static DatabaseHandler databaseHandler;
        private static InfodriverHandler infodriverHandler;
        private static NamedPipeClient<msgState> stateClient;

        /// <summary>
        /// This is a list of all the devices that this driver controls
        /// </summary>
        public List<IDeviceStatus> Devices { get; } = new List<IDeviceStatus>();

        public MainModel()
        {

            //get the packager setting from the main instance
            if (!BNCSConfig.MainInstance.TryGetSetting("packager", out var packagerInstanceName)) throw new Exception("Could not locate packager name in configuration");

            Log.Information(STR_LoggingCategory, $"Packager instance name: {packagerInstanceName}", null, null);

            if (!instances_and_devicetypes.Instance.TryGetInstance(packagerInstanceName, out var packagerInstance)) throw new Exception("Packager instance count not be found");

            if (!packagerInstance.TryGetChildInstance("router", out var routerInstance)) throw new Exception("Router instance not found");
            if (!packagerInstance.TryGetChildInstance("dest_status", out var destStatusInstance)) throw new Exception("Destination status instance not found");

            //the main config lives under the first device id of the routerInstance
            var routerInstances = routerInstance.GetAllChildInstances();
            var mainInstance = routerInstances.Values.FirstOrDefault();

            if (mainInstance == null) throw new Exception("Couldn't identify main device config instance");

            Log.Information(STR_LoggingCategory, "Starting model build", null, null);

            //start the loading process on a background thread
            Task.Run(() =>
            {

                var sw = new Stopwatch();
                sw.Start();

                databaseHandler = new DatabaseHandler(mainInstance);
                infodriverHandler = new InfodriverHandler(routerInstance, destStatusInstance);

                sw.Stop();

                Log.Information(STR_LoggingCategory, "Completed model build", "Build time: {0}", sw.Elapsed);

                //Start the state client and send initial state
                stateClient = new NamedPipeClient<msgState>(msgState.PipeName);
                stateClient.AutoReconnect = true;
                stateClient.ServerMessage += StateClient_ServerMessage;
                stateClient.Disconnected += StateClient_Disconnected;
                stateClient.Start();

                BNCSStatusModel.Instance.CommsStateChanged += Instance_CommsStateChanged;

            });

        }

        private void StateClient_Disconnected(NamedPipeConnection<msgState, msgState> connection)
        {
            //Host has gone down
            Log.Critical(STR_LoggingCategory, "Host connection lost", null);
            BNCSStatusModel.Instance.OkToBeInControl(false);
        }

        private static void SendCurrentState()
        {
            var active = false;

            if (BNCSStatusModel.Instance.CommsStatus == helpers.BncsState.TXRX) active = true;
            stateClient.PushMessage(new msgState(active));
        }

        private void StateClient_ServerMessage(NamedPipeConnection<msgState, msgState> connection, msgState message)
        {
            //respond to a state message by sending the current state - also acts as proof of life for Host
            BNCSStatusModel.Instance.OkToBeInControl(true);
            SendCurrentState();
        }

        private void Instance_CommsStateChanged(object sender, EventArgs e)
        {
            SendCurrentState();
        }

        public void Dispose()
        {
            stateClient.Stop();
            databaseHandler.Dispose();
            infodriverHandler.Dispose();
        }

    }
}
