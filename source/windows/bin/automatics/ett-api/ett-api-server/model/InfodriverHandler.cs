﻿using driver_template.model.bncs;
using ett_api_ipc;
using Gibraltar.Agent;
using instances_and_devicetypes;
using NamedPipeWrapper;
using SharedMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace driver_template.model
{
    public class InfodriverHandler : IDisposable
    {

        const string STR_LoggingCategory = "BNCS_ETT_API.InfodriverHandler";
        const int PackagerParkSourceIndex = 64002;

        private Dictionary<uint, SlotClient> routerSlots;
        private Dictionary<uint, SlotClient> destInfoSlots;
        SharedArray<int> destinationIndexes;
        SharedArray<bool> destinationLocks;
        private NamedPipeClient<msgRoute> routeClient;
        private NamedPipeClient<msgLock> lockClient;
        private bool initialized = false;

        public InfodriverHandler(Instance packagerRouterInstance, Instance packagerDestinationStatusInstance)
        {

            try
            {

                initialized = false;
                routerSlots = new Dictionary<uint, SlotClient>();
                destInfoSlots = new Dictionary<uint, SlotClient>();

                //setup IPC clients
                routeClient = new NamedPipeClient<msgRoute>(msgRoute.PipeName);
                routeClient.AutoReconnect = true;
                routeClient.ServerMessage += RouteClient_ServerMessage;
                routeClient.Start();

                lockClient = new NamedPipeClient<msgLock>(msgLock.PipeName);
                lockClient.AutoReconnect = true;
                lockClient.ServerMessage += LockClient_ServerMessage;
                lockClient.Start();

                var children = packagerRouterInstance.GetAllChildInstances();

                uint slotCounter = 0;

                //get the destination slots
                foreach (var child in children)
                {

                    //Get a clientInstance slot by index
                    var (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(child.Value, 1, 4000);
                    if (Success)
                    {

                        foreach (var slot in Slots)
                        {

                            slotCounter += 1;
                            slot.Slot.GlobalIndex = slotCounter;
                            slot.Slot.OnChange += DestSlot_OnChange;

                            routerSlots.Add(slotCounter, slot.Slot);

                        }

                    }

                }

                var destInfoChildren = packagerDestinationStatusInstance.GetAllChildInstances();

                slotCounter = 0;

                //get the destination slots
                foreach (var child in destInfoChildren)
                {

                    //Get a clientInstance slot by index
                    var (Success, Slots) = BNCSConfig.ClientInstances.GetSlots(child.Value, 1, 4000);
                    if (Success)
                    {

                        foreach (var slot in Slots)
                        {

                            slotCounter += 1;
                            slot.Slot.GlobalIndex = slotCounter;
                            slot.Slot.OnChange += DestInfo_OnChange;

                            destInfoSlots.Add(slotCounter, slot.Slot);

                        }

                    }

                }

                //Infodriver SLots are now initiated - we can setup our shared memory file
                destinationIndexes = new SharedArray<int>("PackagerDestinationIndexes", routerSlots.Count);

                for (uint i = 0; i < routerSlots.Count; i++)
                {
                    var val = (int)ReadSlotIndex(routerSlots[i + 1].Value);
                    if (val == PackagerParkSourceIndex) val = 0;  
                    //TODO:  Make this configurable!
                    destinationIndexes[(int)i] = val;
                }

                destinationLocks = new SharedArray<bool>("PackagerDestinationLocks", destInfoSlots.Count);

                for (uint i = 0; i < destInfoSlots.Count; i++)
                {
                    destinationLocks[(int)i] = ReadSlotLock(destInfoSlots[i + 1].Value);
                }

                //send a read all locks message to the server
                lockClient.PushMessage(new msgLock(0, false));

            }
            catch (Exception ex)
            {
                Log.Error(ex, STR_LoggingCategory, "Error in initialization", null, null);
            }
            finally
            {
                initialized = true;
            }

        }

        private void DestSlot_OnChange(object sender, EventSlotChange e)
        {

            SlotClient slotClient = (SlotClient)e.Slot;

            try
            {
                Log.Verbose(STR_LoggingCategory, "DestSlot_OnChange", "IDX: {0}  VAL: {1}  MEM: {2}  INIT: {3}", slotClient.GlobalIndex, e.Value, (destinationIndexes != null), initialized);
                var newSource = ReadSlotIndex(e.Value);
                if (newSource == PackagerParkSourceIndex) newSource = 0;//check for magic packager park index

                if (destinationIndexes != null && destinationIndexes[(int)slotClient.GlobalIndex - 1] != (int)newSource) destinationIndexes[(int)slotClient.GlobalIndex - 1] = (int)newSource;
                if (initialized) routeClient.PushMessage(new msgRoute(newSource, slotClient.GlobalIndex));

            }
            catch (Exception ex)
            {
                Log.Error(ex, STR_LoggingCategory, "ERROR DestSlot_OnChange", "IDX: {0}  VAL: {1}", slotClient.GlobalIndex, e.Value);
            }

        }

        private void DestInfo_OnChange(object sender, EventSlotChange e)
        {
            SlotClient slotClient = (SlotClient)e.Slot;

            try
            {
                Log.Verbose(STR_LoggingCategory, "DestInfo_OnChange", "IDX: {0}  VAL: {1}  MEM: {2}  INIT: {3}", slotClient.GlobalIndex, e.Value, (destinationLocks != null), initialized);
                var newLock = ReadSlotLock(e.Value);

                if (destinationLocks != null) destinationLocks[(int)slotClient.GlobalIndex - 1] = newLock;
                if (initialized) lockClient.PushMessage(new msgLock(slotClient.GlobalIndex, newLock));

            }
            catch (Exception ex)
            {
                Log.Error(ex, STR_LoggingCategory, "ERROR DestInfo_OnChange", "IDX: {0}  VAL: {1}", slotClient.GlobalIndex, e.Value);
            }
        }

        private void LockClient_ServerMessage(NamedPipeConnection<msgLock, msgLock> connection, msgLock message)
        {
            //not currently supporting locking via the API
        }

        private void RouteClient_ServerMessage(NamedPipeConnection<msgRoute, msgRoute> connection, msgRoute message)
        {

            //route request received
            Log.Verbose(STR_LoggingCategory, "Route Request Received", "Source Package: {0}  Dest Package: {1}", message.SourceIndex, message.DestinationIndex);

            if (routerSlots == null) Log.Error(STR_LoggingCategory, "Request received before slots are available", null, null);
            if (!routerSlots.ContainsKey(message.DestinationIndex)) Log.Error(STR_LoggingCategory, "Request received for unknown slot", null, null);

            var clientSlot = routerSlots[message.DestinationIndex];
            clientSlot.Set($"index={message.SourceIndex}");

        }

        private static uint ReadSlotIndex(string value)
        {

            if (string.IsNullOrWhiteSpace(value)) return 0;

            try
            {
                var resultString = Regex.Match(value, @"(?i)(index=(?<index>\d*))", RegexOptions.ExplicitCapture).Groups["index"].Value;

                if (uint.TryParse(resultString, out uint result)) return result;
                else return 0;

            }
            catch (ArgumentException ex)
            {
                // Syntax error in the regular expression
                return 0;
            }
        }

        private static bool ReadSlotLock(string value)
        {

            if (string.IsNullOrWhiteSpace(value)) return false;

            try
            {
                var resultString = Regex.Match(value, @"(?i)(lock=(?<lock>\d))", RegexOptions.ExplicitCapture).Groups["lock"].Value;

                if (uint.TryParse(resultString, out uint result))
                {
                    if (result == 1) return true;
                    else return false;
                }
                else return false;

            }
            catch (ArgumentException ex)
            {
                // Syntax error in the regular expression
                return false;
            }
        }

        public void Dispose()
        {
            routeClient.Stop();
            lockClient.Stop();

            destinationIndexes.Close();
            destinationIndexes.Dispose();
        }
    }
}
