﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace driver_template.model.interfaces
{
    public interface IMainModel : IDisposable
    {
        List<IDeviceStatus> Devices { get; }
    }
}
