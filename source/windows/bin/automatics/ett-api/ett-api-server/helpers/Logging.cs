using NLog;
using System;
using System.Linq;

namespace driver_template.helpers
{
    public enum DebugLevels
    {
        Off,
        Normal,
        All
    }

    /// <summary>
    /// Class to manage the logging settings
    /// </summary>
    public static class Logging
    {
        private static readonly string LogToFile = "File";
        private static readonly string LogToDebug = "DebugWindow";

        public static DebugLevels ToLevel(String s)
        {
            if (!Enum.TryParse<DebugLevels>(s, out DebugLevels d))
                return DebugLevels.Normal;
            return d;
        }

        public static void SetLevelLog(DebugLevels level)
        {
            SetLevel(LogToFile, level);
        }

        public static void SetLevelDebug(DebugLevels level)
        {
            SetLevel(LogToDebug, level);
        }

        private static void SetLevel(string logName, DebugLevels dbLvl)
        {
            var rules = LogManager.Configuration.LoggingRules.Where(y => y.Targets.Any(z => z.Name == logName) && y.LoggerNamePattern == "*").ToList();
            LogLevel.AllLoggingLevels.ToList().ForEach(z => rules.ForEach(y => y.DisableLoggingForLevel(z)));

            LogLevel l = dbLvl == DebugLevels.Off ? LogLevel.Off : dbLvl == DebugLevels.All ? LogLevel.Trace : LogLevel.Info;

            rules.ForEach(y => y.EnableLoggingForLevels(l, LogLevel.Off));

            LogManager.ReconfigExistingLoggers();
        }
    }
}
