﻿using System;
using System.IO;
using System.Windows;
using System.Reflection;
using System.Windows.Threading;
using NLog;
using NLog.Targets;
using NlogViewer;
using driver_template.helpers;
using driver_template.model.bncs;
using driver_template.view;
using driver_template.viewmodel;
using driver_template.model.interfaces;
using System.Linq;
using Gibraltar.Agent;
using Gibraltar.Agent.Configuration;

namespace driver_template
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        const string STR_LoggingCategory = "BNCS_ETT_API.ApplicationControl";
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static MainWindow mw = null;
        private static MainViewModel vm = null;
        private static IMainModel m = null;

        /// <summary>
        /// Application entry point
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void AppStartup(object sender, StartupEventArgs e)
        {
            Type mainModelType = null;
            Type settingsType = null;
            MethodInfo getManagedAndClientInstancesMethod = null;

            //Check the arguments we have been supplied
            if (e.Args.Length == 0)
            {
                CriticalError("No argument(s) supplied", "Error", logger, ErrorCode.MissingArgument);
                return;
            }

            //Get the first argument, this should be the instance usually
            string argument = e.Args[0];

            LoadNlogConfig(argument);

            //startup logging
            Log.Initializing += Log_Initializing;
            Log.MessageAlert += Log_MessageAlert;

            Log.StartSession();

            //Test to see if a driver with this argument is already running
            if (IsAlreadyRunning(argument))
            {
                CriticalError($"There is already a Driver with this argument({argument}) running on the machine.", "Error", logger, ErrorCode.DriverAlreadyRunning);
                return;
            }

            Log.Information(STR_LoggingCategory, "Startup", "Starting:{0} Version:{1} Arguments:{2} DriverTemplateVersion:0.0.13.6", Assembly.GetExecutingAssembly().GetName().Name, Assembly.GetExecutingAssembly().GetName().Version.ToString(), String.Join(",", e.Args));

            //Don't catch unhandled excecptions whilst being run from Visual Studio Debugger
            if (!AppDomain.CurrentDomain.FriendlyName.EndsWith("vshost.exe")) //Don't catch unhandled excecptions whilst being run from Visual Studio Debugger
            {
                AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException;
                DispatcherUnhandledException += DispatchUnhandledException;
            }

            // Optionally allow for segregation of generic code from driver-specific code
            // NB: This may be expanded at a later date to remove all common/generic code into a separate library
            var types = typeof(App).Assembly.GetTypes()
                .Where(t => t.Namespace == typeof(App).Namespace + ".model");

            foreach (var type in types)
            {
                if (type.IsAbstract && type.IsSealed)
                {
                    var method = type.GetMethod(nameof(model.bncs.Startup.GetManagedAndClientInstances), BindingFlags.Public | BindingFlags.Static);
                    if (method != null)
                    {
                        // Override the default GetManagedAndClientInstances method in model.bncs.Startup
                        getManagedAndClientInstancesMethod = method;
                        continue;
                    }
                }
                else if (typeof(IMainModel).IsAssignableFrom(type))
                {
                    // Allow any class that implements IMainModel to be used
                    mainModelType = type;
                }
                else if (typeof(Settings).IsAssignableFrom(type))
                {
                    // Expand the Settings class with custom settings for this project in a separate class
                    settingsType = type;
                }
            }

            if (mainModelType == null)
            {
                CriticalError("No class implementing IMainModel could be found.", "Error", logger, ErrorCode.Error);
                return;
            }

            // Create main application window
            try
            {
                BNCSConfig.Settings = settingsType == null ? new Settings() : (Settings)Activator.CreateInstance(settingsType);

                //Create the window
                mw = new MainWindow();

                //Load startup info
                var startup = model.bncs.Startup.Load(getManagedAndClientInstancesMethod);

                if (startup.Success == false)
                    throw new CloseException(startup.ErrorMessage, "Startup Error", logger, ErrorCode.StartupError);

                m = (IMainModel)Activator.CreateInstance(mainModelType);

                vm = new MainViewModel(m);
                mw.DataContext = vm;

                mw.Show();
            }
            catch (CloseException exception)
            {
                CriticalError(exception);
            }
            catch (Exception exception)
            {
                CritcalException(exception, logger);
            }
        }

        private static void LoadNlogConfig(string argument)
        {
            if (LogManager.Configuration == null)
            {
                LogManager.Configuration = new NLog.Config.LoggingConfiguration();

                var OutputDebug = new OutputDebugStringTarget("DebugView")
                {
                    Layout = "${var:instance}|${level:uppercase=true}|${logger:shortName=false}|${message}"
                };
                LogManager.Configuration.AddTarget(OutputDebug);

                var File = new FileTarget("File")
                {
                    Layout = "${date} :+: ${callsite} :+: ${message}",
                    DeleteOldFileOnStartup = false,
                    FileName = "${environment:variable=CC_ROOT}\\${environment:variable=CC_SYSTEM}\\logs\\${processname}\\${processname}_${var:instance:default=no_instance_supplied}_${date:format=yyyyMMdd-HH}.log",
                    ArchiveFileName = "${environment:variable=CC_ROOT}\\${environment:variable=CC_SYSTEM}\\logs\\${processname}\\archive\\${processname}_${var:instance:default=no_instance_supplied}_{#}.log",
                    ArchiveEvery = FileArchivePeriod.Hour,
                    ArchiveNumbering = ArchiveNumberingMode.Date,
                    ArchiveDateFormat = "yyyyMMdd-HH",
                    ArchiveOldFileOnStartup = true,
                    MaxArchiveFiles = 168,
                    KeepFileOpen = true,
                    Encoding = System.Text.Encoding.GetEncoding("iso-8859-2")
                };
                LogManager.Configuration.AddTarget(File);

                var DebugWindow = new NlogViewerTarget();
                DebugWindow.Name = "DebugWindow";
                LogManager.Configuration.AddTarget(DebugWindow);

                LogManager.Configuration.AddRule(LogLevel.Trace, LogLevel.Off, OutputDebug, "*");
                LogManager.Configuration.AddRule(LogLevel.Trace, LogLevel.Off, File, "*");
                LogManager.Configuration.AddRule(LogLevel.Trace, LogLevel.Off, DebugWindow, "*");
            }

            LogManager.Configuration.Variables["instance"] = argument;
            LogManager.ReconfigExistingLoggers();
        }

        private static System.Threading.Mutex mutex;
        private static bool IsAlreadyRunning(string id)
        {
            string Guid = System.Reflection.Assembly.GetExecutingAssembly().GetType().GUID.ToString();
            mutex = new System.Threading.Mutex(false, @"Global\" + Guid + "-" + id);
            if (!mutex.WaitOne(0, false))
            {
                return true;
            }
            return false;
        }

        public static void CritcalException(Exception e, ILogger l)
        {
            CriticalError(e.ToString() + "\n\n" + e.StackTrace, "Exception " + e.GetType().ToString(), l, ErrorCode.Exception);
        }

        public static void CriticalError(CloseException e)
        {
            CriticalError(e.Message, e.Title, e.Logger, e.ErrorCode);
        }

        private static bool HasCriticalError = false;
        public static void CriticalError(string message, string caption, ILogger l, ErrorCode ErrorCode = ErrorCode.Error)
        {
            if (!HasCriticalError)
            {
                HasCriticalError = true;
                if (l == null)
                    logger.Fatal(message + " Additional Error No Logger Passed");
                else
                    l.Fatal(message);

                Log.Critical(STR_LoggingCategory, caption, message, null);

                helper.PopupMessageBox.Show(
                    String.Format(
    @"Driver: {0}
Config Path:{1}
Version: {2}              
Error:
{3}",
                    Assembly.GetExecutingAssembly().GetName().Name,
                    instances_and_devicetypes.Helper.ConfigPath,
                    Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    message
                    )

                    , $"Driver:{Assembly.GetExecutingAssembly().GetName().Name} - {caption}"
                    );

                try
                {
                    App.Current.Dispatcher.Invoke(new Action(() =>
                    {
                        ExitErrorCode = (int)ErrorCode;
                        App.Current.Shutdown(ExitErrorCode);
                    }));
                }
                catch (Exception)
                {
                    // Ignore errors here that can occur when Workstation Manager is used to close this app (and the associated infodriver)
                }
            }
        }
        private static int ExitErrorCode = 0;

        private void Cleanup()
        {
            if (vm != null)
            {
                vm.Dispose();
            }
            if (mw != null)
            {
                mw.Close();
            }
            if (m != null)
            {
                m.Dispose();
            }
            // The following is a call to a static class so it may actually cause the contructor to be called if the class has not yet been used
            Infodrivers.Dispose();
            BNCSConfig.Dispose();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            e.ApplicationExitCode = ExitErrorCode; //@Fudge wouldn't accept it from the Shutdown event
            logger.Trace("Exiting Application Tidy Up");
            //Tidy up on exit           
            Cleanup();

            base.OnExit(e);

            Log.Information(STR_LoggingCategory, "Exiting Application", "Error Code:{0}", e.ApplicationExitCode);

            Log.EndSession();

            logger.Info("Exiting Application Error Code:{0}", e.ApplicationExitCode);
        }


        #region Private event handlers
        /// <summary>
        /// Fires when a domain level exception occurs
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs ue)
        {
            if (ue.ExceptionObject is CloseException c)
                CriticalError(c);
            else
                CritcalException((Exception)ue.ExceptionObject, logger);
        }

        /// <summary>
        /// Fires when any other unhandled exception occurs
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        private void DispatchUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs due)
        {
            CritcalException(due.Exception, logger);
        }

        /// <summary>
        /// This handler is called only when the common language runtime tries to bind to the assembly and fails. This is our opportunity to resolve it from the system path.
        /// </summary>
        /// <param name="sender">Calling object</param>
        /// <param name="e">Parameters</param>
        /// <returns>An object of type Assembly if resolved else null</returns>
        //private Assembly CurrentDomainAssemblyResolve(object sender, ResolveEventArgs args)
        //{
        /*
         * This code will look for the assembly on the paths specified in assembly resolver.
        AssemblyWithFilename assembly = AssemblyResolver.ResolveFromPath(args.Name);
        if (assembly.Loaded)
        {
            return assembly.Assembly;
        }
        return null;
        */
        //}
        #endregion

        static void Log_Initializing(object sender, LogInitializingEventArgs e)
        {

            e.Configuration.Publisher.ProductName = "BNCS";
            e.Configuration.Publisher.ApplicationName = "ETT API Server";
            e.Configuration.Publisher.ApplicationType = ApplicationType.Windows;

#if DEBUG
            e.Configuration.Publisher.PromotionLevelName = "Debug";
#else
            e.Configuration.Publisher.PromotionLevelName = "Release";
#endif

            var strVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            DateTime buildDate = driver_template.helpers.Constants.CompilationTimestampUtc;
            var strBuildUser = driver_template.helpers.Constants.CompilationUser;

            e.Configuration.Publisher.ApplicationDescription = string.Format("Version: {0}  Built: {1} {2} (UTC)  by {3}", strVersion, buildDate.ToShortDateString(), buildDate.ToShortTimeString(), strBuildUser);

            e.Configuration.Listener.AutoTraceRegistration = true;
            e.Configuration.Listener.CatchApplicationExceptions = true;
            e.Configuration.Listener.CatchUnhandledExceptions = true;
            e.Configuration.Listener.EnableAssemblyEvents = true;
            e.Configuration.Listener.EnableAssemblyLoadFailureEvents = false;
            e.Configuration.Listener.EnableCommandLine = true;
            e.Configuration.Listener.EnableConsole = false;
            e.Configuration.Listener.EnableDiskPerformance = true;
            e.Configuration.Listener.EnableMemoryPerformance = true;
            e.Configuration.Listener.EnableNetworkEvents = true;
            e.Configuration.Listener.EnableNetworkPerformance = true;
            e.Configuration.Listener.EnablePowerEvents = true;
            e.Configuration.Listener.EnableProcessPerformance = true;
            e.Configuration.Listener.EnableSystemPerformance = true;
            e.Configuration.Listener.EnableUserEvents = true;

            e.Configuration.Packager.ProductName = e.Configuration.Publisher.ProductName;
            e.Configuration.Packager.ApplicationName = e.Configuration.Publisher.ApplicationName;
            e.Configuration.Packager.AllowEmail = false;
            e.Configuration.Packager.AllowFile = true;
            e.Configuration.Packager.AllowRemovableMedia = true;
            e.Configuration.Packager.AllowServer = false;
            e.Configuration.Packager.HotKey = PackagerConfiguration.DefaultHotKey;

            e.Configuration.SessionFile.EnableFilePruning = true;
            e.Configuration.SessionFile.MaxLocalFileAge = 90;
            e.Configuration.SessionFile.MaxLocalDiskUsage = 10000;
            e.Configuration.SessionFile.AutoFlushInterval = 60;
            e.Configuration.SessionFile.MaxFileDuration = 15;

            e.Configuration.Viewer.Enabled = true;
            e.Configuration.Viewer.HotKey = ViewerConfiguration.DefaultHotKey;

            e.Configuration.Server.Enabled = false;

        }

        private void Log_MessageAlert(object sender, LogMessageAlertEventArgs e)
        {

            foreach (var msg in e.Messages)
            {
                switch (msg.Severity)
                {
                    case LogMessageSeverity.None:
                        break;
                    case LogMessageSeverity.Critical:
                        logger.Fatal(msg.Caption + " - " + msg.Description);
                        break;
                    case LogMessageSeverity.Error:
                        logger.Error(msg.Caption + " - " + msg.Description);
                        break;
                    case LogMessageSeverity.Warning:
                        logger.Warn(msg.Caption + " - " + msg.Description);
                        break;
                    case LogMessageSeverity.Information:
                        logger.Info(msg.Caption + " - " + msg.Description);
                        break;
                    case LogMessageSeverity.Verbose:
                        logger.Trace(msg.Caption + " - " + msg.Description);
                        break;
                    default:
                        break;
                }
            }
        }

    }
}
