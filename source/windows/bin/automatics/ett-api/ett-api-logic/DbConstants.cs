﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic
{
    public static class DbConstants
    {

        public const uint dbSourceButtonName = 0;
        public const uint dbDestButtonName = 1;
        public const uint dbSourcePackageTitle = 2;
        public const uint dbDestPackageTitle = 3;
        public const uint dbSourcePackageTags = 4;
        public const uint dbDestPackageTags = 5;
        public const uint dbAudioSourcePackages = 6;
        public const uint dbDestLevels = 7;
        //db8 appears to be page layouts
        public const uint dbMCRUmd = 9;

        public const uint dbProductionUmd = 10;
        public const uint dbSourceVideo = 11;
        public const uint dbAudioDestinationPairIndex = 15;
        public const uint dbAudioDestinationPairTagType = 16;
        public const uint dbAudioDestinationPairTagTypeSubst1 = 17;
        public const uint dbAudioDestinationPairTagTypeSubst2 = 18;
        public const uint dbAudioReturn = 19;

        public const uint dbDefaultSourcePackage = 20;
        public const uint dbFunctionalSourceArea = 24;
        public const uint dbFunctionalDestArea = 25;
        public const uint dbFunctionalSourceName = 26;
        public const uint dbFunctionalDestName = 27;

        public const uint dbSourceEventName = 30;
        public const uint dbSourceSpevList1 = 31;
        public const uint dbSourceSpevList2 = 132;
        public const uint dbSourceSpevList3 = 133;
        public const uint dbSourceSpevList4 = 134;
        public const uint dbSourceRecordId = 32;
        public const uint dbSourceClassId = 33;
        public const uint dbSourceDates = 34;
        public const uint dbSourceTeams = 35;
        public const uint dbSourceStatus = 36;

        public const uint dbMCRNotes = 37;

        public const uint dbVideoHubTags = 41;
        public const uint dbAudioHubTags = 42;
        public const uint dbLanguageUseHubTags = 43;
        public const uint dbAudioTypeTags = 44;
        public const uint dbOwnerTags = 45;
        public const uint dbDelegateTags = 46;
        public const uint dbRiedelTags = 47;

        public const uint dbAudioSourcePackageButtonName = 60;
        public const uint dbAudioSourcePackageLongName = 61;
        public const uint dbAudioSourcePackageTagFields1 = 62;
        public const uint dbAudioSourcePackageTagFields2 = 63;
        public const uint dbAudioSourcePackagePairs = 64;
        public const uint dbAudioSourcePackageReturnPairs = 65;
        public const uint dbAudioSourcePackageVideoLinkIndex = 66;
        public const uint dbAudioSourcePackageCFLabels = 67;
        public const uint dbAudioSourcePackageConfLabels = 68;

        public const uint dbAudioPresetName = 71;
        public const uint dbAudioPresetTypeTag = 72;
        public const uint dbAudioPresetTypeTagSubst1 = 73;
        public const uint dbAudioPresetTypeTagSubst2 = 74;

        public const uint db0081 = 81;

        public const uint dbSourcePackageLocks = 91;
        public const uint dbAudioPackageLocks = 92;
        public const uint dbDestPackageEditLocks = 93;
        public const uint dbDestPackageRouteLocks = 94;

        public static readonly uint[] monitoredDbs = {
            dbSourceButtonName,
            dbDestButtonName,
            dbSourcePackageTitle,
            dbDestPackageTitle,
            dbSourcePackageTags,
            dbDestPackageTags,
            dbAudioSourcePackages,
            dbDestLevels,
            dbMCRUmd,
            dbProductionUmd,
            dbSourceVideo,
            dbAudioDestinationPairIndex,
            dbAudioDestinationPairTagType,
            dbAudioDestinationPairTagTypeSubst1,
            dbAudioDestinationPairTagTypeSubst2,
            dbAudioReturn,
            dbDefaultSourcePackage,
            dbFunctionalSourceArea,
            dbFunctionalDestArea,
            dbFunctionalSourceName,
            dbFunctionalDestName,
            dbSourceEventName,
            dbSourceSpevList1,
            dbSourceSpevList2,
            dbSourceSpevList3,
            dbSourceSpevList4,
            dbSourceRecordId,
            dbSourceClassId,
            dbSourceDates,
            dbSourceTeams,
            dbSourceStatus,
            dbMCRNotes,
            dbVideoHubTags,
            dbAudioHubTags,
            dbLanguageUseHubTags,
            dbAudioTypeTags,
            dbOwnerTags,
            dbDelegateTags,
            dbRiedelTags,
            dbAudioSourcePackageButtonName,
            dbAudioSourcePackageLongName,
            dbAudioSourcePackageTagFields1,
            dbAudioSourcePackageTagFields2,
            dbAudioSourcePackagePairs,
            dbAudioSourcePackageReturnPairs,
            dbAudioSourcePackageVideoLinkIndex,
            dbAudioSourcePackageCFLabels,
            dbAudioSourcePackageConfLabels,
            dbAudioPresetName,
            dbAudioPresetTypeTag,
            dbAudioPresetTypeTagSubst1,
            dbAudioPresetTypeTagSubst2,
            db0081,
            dbSourcePackageLocks,
            dbAudioPackageLocks,
            dbDestPackageEditLocks,
            dbDestPackageRouteLocks,
        };

    }
}
