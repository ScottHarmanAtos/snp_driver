﻿using ett_api_logic.packages;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ett_api_logic
{
    public class PackageGroup<T> where T : BasePackage
    {

        private Dictionary<uint, T> packages;
        private ObservableCollection<T> packageList;
        private uint groupSize;

        public PackageGroup(uint groupSize)
        {

            this.groupSize = groupSize;
            packages = new Dictionary<uint, T>();
            packageList = new ObservableCollection<T>();

        }

        private T CreatePackage(uint i)
        {
            BasePackage packageInstance = (BasePackage)Activator.CreateInstance(typeof(T), new object[] { i });
            packages.Add(i, (T)packageInstance);
            packageList.Add((T)packageInstance);
            return (T)packageInstance;
        }

        public T GetPackage(uint index)
        {
            if (index < 1 || index > groupSize) throw new IndexOutOfRangeException("Package index out of range");  //package out of range
            if (!packages.ContainsKey(index)) return CreatePackage(index);
            return packages[index];
        }

        public bool InitDbValue(uint dbNumber, uint index, string value)
        {
            var pkg = GetPackage(index);
            if (pkg == null && string.IsNullOrWhiteSpace(value)) return false;
            if (pkg == null) pkg = CreatePackage(index);
            return pkg.InitDbValue(dbNumber, value);
        }

        public bool ChangeDbValue(uint dbNumber, uint index, string value)
        {
            var pkg = GetPackage(index);
            if (pkg == null && string.IsNullOrWhiteSpace(value)) return false;
            if (pkg == null) pkg = CreatePackage(index);
            return pkg.ChangeDbValue(dbNumber, value);
        }

        public string GetDbValue(uint dbNumber, uint index)
        {
            var pkg = GetPackage(index);
            if (pkg == null) return null;
            return pkg.GetDbValue(dbNumber);
        }

        public ObservableCollection<T> Values
        {
            get
            {
                return packageList;
            }
        }

        public IEnumerable<T> AsQueryable()
        {
            return packages.Values.AsEnumerable();
        }

    }
}
