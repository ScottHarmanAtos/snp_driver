﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.packages
{
    public class AudioPackage : BasePackage
    {

        /// <summary>Initializes a new instance of the <a onclick="return false;" href="T:AudioPackage" originaltag="see">T:AudioPackage</a> class.</summary>
        /// <param name="Index">The BNCS index of the package</param>
        public AudioPackage(uint Index) : base(Index)
        {
            PackageName.AffectedDb = (int)DbConstants.dbAudioSourcePackageLongName;
            ButtonName.AffectedDb = (int)DbConstants.dbAudioSourcePackageButtonName;
            OwnerTag.AffectedDb = (int)DbConstants.dbAudioSourcePackageTagFields1;
            DelegateTag.AffectedDb = (int)DbConstants.dbAudioSourcePackageTagFields1;
            UniqueId.AffectedDb = (int)DbConstants.dbAudioSourcePackageTagFields1;
        }

        public override string PackageType => "audio";
        public TrackedParameter<int> LanguageTag { get => languageTag; set => languageTag = value; }
        public TrackedParameter<int> VideoLink { get => videoLink; set => videoLink = value; }
        public TrackedParameter<int> AudioLevel01 { get => audioLevel01; set => audioLevel01 = value; }
        public TrackedParameter<int> AudioLevel02 { get => audioLevel02; set => audioLevel02 = value; }
        public TrackedParameter<int> AudioLevel03 { get => audioLevel03; set => audioLevel03 = value; }
        public TrackedParameter<int> AudioLevel04 { get => audioLevel04; set => audioLevel04 = value; }
        public TrackedParameter<int> AudioLevel05 { get => audioLevel05; set => audioLevel05 = value; }
        public TrackedParameter<int> AudioLevel06 { get => audioLevel06; set => audioLevel06 = value; }
        public TrackedParameter<int> AudioLevel07 { get => audioLevel07; set => audioLevel07 = value; }
        public TrackedParameter<int> AudioLevel08 { get => audioLevel08; set => audioLevel08 = value; }
        public TrackedParameter<int> AudioLevel09 { get => audioLevel09; set => audioLevel09 = value; }
        public TrackedParameter<int> AudioLevel10 { get => audioLevel10; set => audioLevel10 = value; }
        public TrackedParameter<int> AudioLevel11 { get => audioLevel11; set => audioLevel11 = value; }
        public TrackedParameter<int> AudioLevel12 { get => audioLevel12; set => audioLevel12 = value; }
        public TrackedParameter<int> AudioLevel13 { get => audioLevel13; set => audioLevel13 = value; }
        public TrackedParameter<int> AudioLevel14 { get => audioLevel14; set => audioLevel14 = value; }
        public TrackedParameter<int> AudioLevel15 { get => audioLevel15; set => audioLevel15 = value; }
        public TrackedParameter<int> AudioLevel16 { get => audioLevel16; set => audioLevel16 = value; }
        public TrackedParameter<int> AudioLevelTypeTag01 { get => audioLevelTypeTag01; set => audioLevelTypeTag01 = value; }
        public TrackedParameter<int> AudioLevelTypeTag02 { get => audioLevelTypeTag02; set => audioLevelTypeTag02 = value; }
        public TrackedParameter<int> AudioLevelTypeTag03 { get => audioLevelTypeTag03; set => audioLevelTypeTag03 = value; }
        public TrackedParameter<int> AudioLevelTypeTag04 { get => audioLevelTypeTag04; set => audioLevelTypeTag04 = value; }
        public TrackedParameter<int> AudioLevelTypeTag05 { get => audioLevelTypeTag05; set => audioLevelTypeTag05 = value; }
        public TrackedParameter<int> AudioLevelTypeTag06 { get => audioLevelTypeTag06; set => audioLevelTypeTag06 = value; }
        public TrackedParameter<int> AudioLevelTypeTag07 { get => audioLevelTypeTag07; set => audioLevelTypeTag07 = value; }
        public TrackedParameter<int> AudioLevelTypeTag08 { get => audioLevelTypeTag08; set => audioLevelTypeTag08 = value; }
        public TrackedParameter<int> AudioLevelTypeTag09 { get => audioLevelTypeTag09; set => audioLevelTypeTag09 = value; }
        public TrackedParameter<int> AudioLevelTypeTag10 { get => audioLevelTypeTag10; set => audioLevelTypeTag10 = value; }
        public TrackedParameter<int> AudioLevelTypeTag11 { get => audioLevelTypeTag11; set => audioLevelTypeTag11 = value; }
        public TrackedParameter<int> AudioLevelTypeTag12 { get => audioLevelTypeTag12; set => audioLevelTypeTag12 = value; }
        public TrackedParameter<int> AudioLevelTypeTag13 { get => audioLevelTypeTag13; set => audioLevelTypeTag13 = value; }
        public TrackedParameter<int> AudioLevelTypeTag14 { get => audioLevelTypeTag14; set => audioLevelTypeTag14 = value; }
        public TrackedParameter<int> AudioLevelTypeTag15 { get => audioLevelTypeTag15; set => audioLevelTypeTag15 = value; }
        public TrackedParameter<int> AudioLevelTypeTag16 { get => audioLevelTypeTag16; set => audioLevelTypeTag16 = value; }
        public TrackedParameter<int> AudioReturnLevel01 { get => audioReturnLevel01; set => audioReturnLevel01 = value; }
        public TrackedParameter<int> AudioReturnLevel02 { get => audioReturnLevel02; set => audioReturnLevel02 = value; }
        public TrackedParameter<int> AudioReturnLevel03 { get => audioReturnLevel03; set => audioReturnLevel03 = value; }
        public TrackedParameter<int> AudioReturnLevel04 { get => audioReturnLevel04; set => audioReturnLevel04 = value; }
        public TrackedParameter<int> AudioReturnLevelLangTag01 { get => audioReturnLevelLangTag01; set => audioReturnLevelLangTag01 = value; }
        public TrackedParameter<int> AudioReturnLevelLangTag02 { get => audioReturnLevelLangTag02; set => audioReturnLevelLangTag02 = value; }
        public TrackedParameter<int> AudioReturnLevelLangTag03 { get => audioReturnLevelLangTag03; set => audioReturnLevelLangTag03 = value; }
        public TrackedParameter<int> AudioReturnLevelLangTag04 { get => audioReturnLevelLangTag04; set => audioReturnLevelLangTag04 = value; }
        public TrackedParameter<int> AudioReturnLevelTypeTag01 { get => audioReturnLevelTypeTag01; set => audioReturnLevelTypeTag01 = value; }
        public TrackedParameter<int> AudioReturnLevelTypeTag02 { get => audioReturnLevelTypeTag02; set => audioReturnLevelTypeTag02 = value; }
        public TrackedParameter<int> AudioReturnLevelTypeTag03 { get => audioReturnLevelTypeTag03; set => audioReturnLevelTypeTag03 = value; }
        public TrackedParameter<int> AudioReturnLevelTypeTag04 { get => audioReturnLevelTypeTag04; set => audioReturnLevelTypeTag04 = value; }

        private TrackedParameter<int> languageTag = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageTagFields2);
        private TrackedParameter<int> videoLink = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageVideoLinkIndex);

        private TrackedParameter<int> audioLevel01 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel02 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel03 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel04 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel05 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel06 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel07 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel08 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel09 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel10 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel11 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel12 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel13 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel14 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel15 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevel16 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);

        private TrackedParameter<int> audioLevelTypeTag01 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag02 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag03 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag04 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag05 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag06 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag07 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag08 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag09 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag10 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag11 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag12 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag13 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag14 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag15 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);
        private TrackedParameter<int> audioLevelTypeTag16 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackagePairs);

        private TrackedParameter<int> audioReturnLevel01 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevel02 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevel03 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevel04 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);

        private TrackedParameter<int> audioReturnLevelLangTag01 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevelLangTag02 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevelLangTag03 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevelLangTag04 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);

        private TrackedParameter<int> audioReturnLevelTypeTag01 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevelTypeTag02 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevelTypeTag03 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);
        private TrackedParameter<int> audioReturnLevelTypeTag04 = new TrackedParameter<int>(DbConstants.dbAudioSourcePackageReturnPairs);

        protected override bool ProcessDbValue(uint dbNumber, string value, bool initMode)
        {

            switch (dbNumber)
            {
                case DbConstants.dbAudioSourcePackageButtonName:

                    ButtonName.Set(value, Enums.ChangeSources.BNCS, initMode);
                    
                    return true;

                case DbConstants.dbAudioSourcePackageLongName:
                    
                    PackageName.Set(value, Enums.ChangeSources.BNCS, initMode);
                    
                    return true;

                case DbConstants.dbAudioSourcePackageTagFields1:
                    
                    //field contains UniqueId, owner & delegate fields
                    UniqueId.Set(ReadTaggedValue(value, "unique_id"), Enums.ChangeSources.BNCS, initMode);
                    OwnerTag.Set(GetTaggedIntValue(ReadTaggedValue(value, "owner")), Enums.ChangeSources.BNCS, initMode);
                    DelegateTag.Set(GetTaggedIntValue(ReadTaggedValue(value, "delegate")), Enums.ChangeSources.BNCS, initMode);
                    
                    return true;

                case DbConstants.dbAudioSourcePackageTagFields2:
                    
                    //Video Link index,Reverse Vision index,Riedel 
                    LanguageTag.Set(GetTaggedIntValue(ReadTaggedValue(value, "language_tag")), Enums.ChangeSources.BNCS, initMode);
                    
                    return true;

                case DbConstants.dbAudioSourcePackagePairs:

                    //audio pairs, where present, are in the form index#tag

                    var pair01 = GetTuple(ReadTaggedValue(value, "a1"));
                    var pair02 = GetTuple(ReadTaggedValue(value, "a2"));
                    var pair03 = GetTuple(ReadTaggedValue(value, "a3"));
                    var pair04 = GetTuple(ReadTaggedValue(value, "a4"));
                    var pair05 = GetTuple(ReadTaggedValue(value, "a5"));
                    var pair06 = GetTuple(ReadTaggedValue(value, "a6"));
                    var pair07 = GetTuple(ReadTaggedValue(value, "a7"));
                    var pair08 = GetTuple(ReadTaggedValue(value, "a8"));
                    var pair09 = GetTuple(ReadTaggedValue(value, "a9"));
                    var pair10 = GetTuple(ReadTaggedValue(value, "a10"));
                    var pair11 = GetTuple(ReadTaggedValue(value, "a11"));
                    var pair12 = GetTuple(ReadTaggedValue(value, "a12"));
                    var pair13 = GetTuple(ReadTaggedValue(value, "a13"));
                    var pair14 = GetTuple(ReadTaggedValue(value, "a14"));
                    var pair15 = GetTuple(ReadTaggedValue(value, "a15"));
                    var pair16 = GetTuple(ReadTaggedValue(value, "a16"));

                    AudioLevel01.Set(GetTaggedIntValue(pair01.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02.Set(GetTaggedIntValue(pair02.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03.Set(GetTaggedIntValue(pair03.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04.Set(GetTaggedIntValue(pair04.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05.Set(GetTaggedIntValue(pair05.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06.Set(GetTaggedIntValue(pair06.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07.Set(GetTaggedIntValue(pair07.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08.Set(GetTaggedIntValue(pair08.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09.Set(GetTaggedIntValue(pair09.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10.Set(GetTaggedIntValue(pair10.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel11.Set(GetTaggedIntValue(pair11.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12.Set(GetTaggedIntValue(pair12.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13.Set(GetTaggedIntValue(pair13.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14.Set(GetTaggedIntValue(pair14.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15.Set(GetTaggedIntValue(pair15.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16.Set(GetTaggedIntValue(pair16.Item1), Enums.ChangeSources.BNCS, initMode);

                    AudioLevelTypeTag01.Set(GetTaggedIntValue(pair01.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag02.Set(GetTaggedIntValue(pair02.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag03.Set(GetTaggedIntValue(pair03.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag04.Set(GetTaggedIntValue(pair04.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag05.Set(GetTaggedIntValue(pair05.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag06.Set(GetTaggedIntValue(pair06.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag07.Set(GetTaggedIntValue(pair07.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag08.Set(GetTaggedIntValue(pair08.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag09.Set(GetTaggedIntValue(pair09.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag10.Set(GetTaggedIntValue(pair10.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag11.Set(GetTaggedIntValue(pair11.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag12.Set(GetTaggedIntValue(pair12.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag13.Set(GetTaggedIntValue(pair13.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag14.Set(GetTaggedIntValue(pair14.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag15.Set(GetTaggedIntValue(pair15.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevelTypeTag16.Set(GetTaggedIntValue(pair16.Item2), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioSourcePackageReturnPairs:

                    var rpair01 = GetTuple(ReadTaggedValue(value, "a1"));
                    var rpair02 = GetTuple(ReadTaggedValue(value, "a2"));
                    var rpair03 = GetTuple(ReadTaggedValue(value, "a3"));
                    var rpair04 = GetTuple(ReadTaggedValue(value, "a4"));

                    AudioReturnLevel01.Set(GetTaggedIntValue(rpair01.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevel02.Set(GetTaggedIntValue(rpair02.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevel03.Set(GetTaggedIntValue(rpair03.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevel04.Set(GetTaggedIntValue(rpair04.Item1), Enums.ChangeSources.BNCS, initMode);

                    AudioReturnLevelLangTag01.Set(GetTaggedIntValue(rpair01.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevelLangTag02.Set(GetTaggedIntValue(rpair02.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevelLangTag03.Set(GetTaggedIntValue(rpair03.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevelLangTag04.Set(GetTaggedIntValue(rpair04.Item2), Enums.ChangeSources.BNCS, initMode);

                    AudioReturnLevelTypeTag01.Set(GetTaggedIntValue(rpair01.Item3), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevelTypeTag02.Set(GetTaggedIntValue(rpair02.Item3), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevelTypeTag03.Set(GetTaggedIntValue(rpair03.Item3), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevelTypeTag04.Set(GetTaggedIntValue(rpair04.Item3), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioSourcePackageVideoLinkIndex:

                    VideoLink.Set(GetTaggedIntValue(value), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioPackageLocks:

                    EditLocked = !String.IsNullOrWhiteSpace(value);
                    return true;

                case DbConstants.dbAudioSourcePackageCFLabels:
                case DbConstants.dbAudioSourcePackageConfLabels:
                default:
                    return false;
            }
        }

        protected override void ProcessDbChange(uint dbNumber)
        {

            switch (dbNumber)
            {
                case DbConstants.dbAudioSourcePackageButtonName:

                    UpdateDbEntry(dbNumber, ButtonName.CurrentValue);
                    break;

                case DbConstants.dbAudioSourcePackageLongName:

                    UpdateDbEntry(dbNumber, PackageName.CurrentValue);
                    break;

                case DbConstants.dbAudioSourcePackageTagFields1:

                    SetTaggedValue(dbNumber, "unique_id", UniqueId.CurrentValue);
                    SetTaggedValue(dbNumber, "owner", GetTaggedIntValue(OwnerTag.CurrentValue));
                    SetTaggedValue(dbNumber, "delegate", GetTaggedIntValue(DelegateTag.CurrentValue));
                    break;

                case DbConstants.dbAudioSourcePackageTagFields2:

                    SetTaggedValue(dbNumber, "language_tag", GetTaggedIntValue(LanguageTag.CurrentValue));
                    break;

                case DbConstants.dbAudioSourcePackagePairs:

                    var a01 = $"{GetTaggedIntValue(AudioLevel01.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag01.CurrentValue)}";
                    var a02 = $"{GetTaggedIntValue(AudioLevel02.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag02.CurrentValue)}";
                    var a03 = $"{GetTaggedIntValue(AudioLevel03.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag03.CurrentValue)}";
                    var a04 = $"{GetTaggedIntValue(AudioLevel04.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag04.CurrentValue)}";
                    var a05 = $"{GetTaggedIntValue(AudioLevel05.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag05.CurrentValue)}";
                    var a06 = $"{GetTaggedIntValue(AudioLevel06.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag06.CurrentValue)}";
                    var a07 = $"{GetTaggedIntValue(AudioLevel07.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag07.CurrentValue)}";
                    var a08 = $"{GetTaggedIntValue(AudioLevel08.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag08.CurrentValue)}";
                    var a09 = $"{GetTaggedIntValue(AudioLevel09.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag09.CurrentValue)}";
                    var a10 = $"{GetTaggedIntValue(AudioLevel10.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag10.CurrentValue)}";
                    var a11 = $"{GetTaggedIntValue(AudioLevel11.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag11.CurrentValue)}";
                    var a12 = $"{GetTaggedIntValue(AudioLevel12.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag12.CurrentValue)}";
                    var a13 = $"{GetTaggedIntValue(AudioLevel13.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag13.CurrentValue)}";
                    var a14 = $"{GetTaggedIntValue(AudioLevel14.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag14.CurrentValue)}";
                    var a15 = $"{GetTaggedIntValue(AudioLevel15.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag15.CurrentValue)}";
                    var a16 = $"{GetTaggedIntValue(AudioLevel16.CurrentValue)}#{GetTaggedIntValue(AudioLevelTypeTag16.CurrentValue)}";

                    SetTaggedValue(dbNumber, "a1", a01);
                    SetTaggedValue(dbNumber, "a2", a02);
                    SetTaggedValue(dbNumber, "a3", a03);
                    SetTaggedValue(dbNumber, "a4", a04);
                    SetTaggedValue(dbNumber, "a5", a05);
                    SetTaggedValue(dbNumber, "a6", a06);
                    SetTaggedValue(dbNumber, "a7", a07);
                    SetTaggedValue(dbNumber, "a8", a08);
                    SetTaggedValue(dbNumber, "a9", a09);
                    SetTaggedValue(dbNumber, "a10", a10);
                    SetTaggedValue(dbNumber, "a11", a11);
                    SetTaggedValue(dbNumber, "a12", a12);
                    SetTaggedValue(dbNumber, "a13", a13);
                    SetTaggedValue(dbNumber, "a14", a14);
                    SetTaggedValue(dbNumber, "a15", a15);
                    SetTaggedValue(dbNumber, "a16", a16);

                    break;

                case DbConstants.dbAudioSourcePackageReturnPairs:

                    var r01 = $"{GetTaggedIntValue(AudioReturnLevel01.CurrentValue)}#{GetTaggedIntValue(AudioReturnLevelLangTag01.CurrentValue)}:{GetTaggedIntValue(AudioReturnLevelTypeTag01.CurrentValue)}";
                    var r02 = $"{GetTaggedIntValue(AudioReturnLevel02.CurrentValue)}#{GetTaggedIntValue(AudioReturnLevelLangTag02.CurrentValue)}:{GetTaggedIntValue(AudioReturnLevelTypeTag02.CurrentValue)}";
                    var r03 = $"{GetTaggedIntValue(AudioReturnLevel03.CurrentValue)}#{GetTaggedIntValue(AudioReturnLevelLangTag03.CurrentValue)}:{GetTaggedIntValue(AudioReturnLevelTypeTag03.CurrentValue)}";
                    var r04 = $"{GetTaggedIntValue(AudioReturnLevel04.CurrentValue)}#{GetTaggedIntValue(AudioReturnLevelLangTag04.CurrentValue)}:{GetTaggedIntValue(AudioReturnLevelTypeTag04.CurrentValue)}";

                    SetTaggedValue(dbNumber, "a1", r01);
                    SetTaggedValue(dbNumber, "a2", r02);
                    SetTaggedValue(dbNumber, "a3", r03);
                    SetTaggedValue(dbNumber, "a4", r04);

                    break;

                case DbConstants.dbAudioSourcePackageVideoLinkIndex:

                    UpdateDbEntry(dbNumber, GetTaggedIntValue(VideoLink.CurrentValue));
                    break;

                case DbConstants.dbAudioSourcePackageCFLabels:
                case DbConstants.dbAudioSourcePackageConfLabels:
                default:
                    break;
            }

        }

        public ett_api_domain.AudioPackage GetDocument()
        {

            var result = new ett_api_domain.AudioPackage();

            result.Index = (int)Index;

            result.OwnerTag = new ett_api_domain.OwnerTag();
            result.OwnerTag.Index = NullableInt(OwnerTag.CurrentValue);
            result.OwnerTag.Name = Lookup(DbConstants.dbOwnerTags, OwnerTag.CurrentValue);

            result.DelegateTag = new ett_api_domain.DelegateTag();
            result.DelegateTag.Index = NullableInt(DelegateTag.CurrentValue);
            result.DelegateTag.Name = Lookup(DbConstants.dbDelegateTags, DelegateTag.CurrentValue);

            result.ButtonName = ButtonName.CurrentValue;
            result.PackageName = PackageName.CurrentValue;
            result.PackageType = PackageType;
            result.LanguageTag = new ett_api_domain.LanguageTag();
            result.LanguageTag.Index = LanguageTag.CurrentValue;
            result.LanguageTag.Name = Lookup(DbConstants.dbLanguageUseHubTags, LanguageTag.CurrentValue);
            result.VideoLink = NullableInt(VideoLink.CurrentValue);

            result.AudioLevels = new ett_api_domain.AudioPackageAudioLevels();

            result.AudioLevels._01 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._01.Index = NullableInt(AudioLevel01.CurrentValue);
            result.AudioLevels._01.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._01.TypeTag.Index = NullableInt(AudioLevelTypeTag01.CurrentValue);
            result.AudioLevels._01.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag01.CurrentValue);

            result.AudioLevels._02 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._02.Index = NullableInt(AudioLevel02.CurrentValue);
            result.AudioLevels._02.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._02.TypeTag.Index = NullableInt(AudioLevelTypeTag02.CurrentValue);
            result.AudioLevels._02.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag02.CurrentValue);

            result.AudioLevels._03 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._03.Index = NullableInt(AudioLevel03.CurrentValue);
            result.AudioLevels._03.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._03.TypeTag.Index = NullableInt(AudioLevelTypeTag03.CurrentValue);
            result.AudioLevels._03.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag03.CurrentValue);

            result.AudioLevels._04 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._04.Index = NullableInt(AudioLevel04.CurrentValue);
            result.AudioLevels._04.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._04.TypeTag.Index = NullableInt(AudioLevelTypeTag04.CurrentValue);
            result.AudioLevels._04.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag04.CurrentValue);

            result.AudioLevels._05 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._05.Index = NullableInt(AudioLevel05.CurrentValue);
            result.AudioLevels._05.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._05.TypeTag.Index = NullableInt(AudioLevelTypeTag05.CurrentValue);
            result.AudioLevels._05.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag05.CurrentValue);

            result.AudioLevels._06 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._06.Index = NullableInt(AudioLevel06.CurrentValue);
            result.AudioLevels._06.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._06.TypeTag.Index = NullableInt(AudioLevelTypeTag06.CurrentValue);
            result.AudioLevels._06.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag06.CurrentValue);

            result.AudioLevels._07 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._07.Index = NullableInt(AudioLevel07.CurrentValue);
            result.AudioLevels._07.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._07.TypeTag.Index = NullableInt(AudioLevelTypeTag07.CurrentValue);
            result.AudioLevels._07.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag07.CurrentValue);

            result.AudioLevels._08 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._08.Index = NullableInt(AudioLevel08.CurrentValue);
            result.AudioLevels._08.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._08.TypeTag.Index = NullableInt(AudioLevelTypeTag08.CurrentValue);
            result.AudioLevels._08.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag08.CurrentValue);

            result.AudioLevels._09 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._09.Index = NullableInt(AudioLevel09.CurrentValue);
            result.AudioLevels._09.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._09.TypeTag.Index = NullableInt(AudioLevelTypeTag09.CurrentValue);
            result.AudioLevels._09.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag09.CurrentValue);

            result.AudioLevels._10 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._10.Index = NullableInt(AudioLevel10.CurrentValue);
            result.AudioLevels._10.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._10.TypeTag.Index = NullableInt(AudioLevelTypeTag10.CurrentValue);
            result.AudioLevels._10.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag10.CurrentValue);

            result.AudioLevels._11 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._11.Index = NullableInt(AudioLevel11.CurrentValue);
            result.AudioLevels._11.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._11.TypeTag.Index = NullableInt(AudioLevelTypeTag11.CurrentValue);
            result.AudioLevels._11.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag11.CurrentValue);

            result.AudioLevels._12 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._12.Index = NullableInt(AudioLevel12.CurrentValue);
            result.AudioLevels._12.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._12.TypeTag.Index = NullableInt(AudioLevelTypeTag12.CurrentValue);
            result.AudioLevels._12.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag12.CurrentValue);

            result.AudioLevels._13 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._13.Index = NullableInt(AudioLevel13.CurrentValue);
            result.AudioLevels._13.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._13.TypeTag.Index = NullableInt(AudioLevelTypeTag13.CurrentValue);
            result.AudioLevels._13.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag13.CurrentValue);

            result.AudioLevels._14 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._14.Index = NullableInt(AudioLevel14.CurrentValue);
            result.AudioLevels._14.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._14.TypeTag.Index = NullableInt(AudioLevelTypeTag14.CurrentValue);
            result.AudioLevels._14.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag14.CurrentValue);

            result.AudioLevels._15 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._15.Index = NullableInt(AudioLevel15.CurrentValue);
            result.AudioLevels._15.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._15.TypeTag.Index = NullableInt(AudioLevelTypeTag15.CurrentValue);
            result.AudioLevels._15.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag15.CurrentValue);

            result.AudioLevels._16 = new ett_api_domain.AudioPackageAudioLevel();
            result.AudioLevels._16.Index = NullableInt(AudioLevel16.CurrentValue);
            result.AudioLevels._16.TypeTag = new ett_api_domain.TypeTag();
            result.AudioLevels._16.TypeTag.Index = NullableInt(AudioLevelTypeTag16.CurrentValue);
            result.AudioLevels._16.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevelTypeTag16.CurrentValue);

            result.ReturnAudioLevels = new ett_api_domain.AudioPackageReturnAudioLevels();

            result.ReturnAudioLevels._1 = new ett_api_domain.AudioPackageReturnAudioLevel();
            result.ReturnAudioLevels._1.Index = NullableInt(AudioReturnLevel01.CurrentValue);
            result.ReturnAudioLevels._1.LangTag = new ett_api_domain.LanguageTag();
            result.ReturnAudioLevels._1.LangTag.Index = NullableInt(AudioReturnLevelLangTag01.CurrentValue);
            result.ReturnAudioLevels._1.LangTag.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioReturnLevelLangTag01.CurrentValue);
            result.ReturnAudioLevels._1.TypeTag = new ett_api_domain.TypeTag();
            result.ReturnAudioLevels._1.TypeTag.Index = NullableInt(AudioReturnLevelTypeTag01.CurrentValue);
            result.ReturnAudioLevels._1.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioReturnLevelTypeTag01.CurrentValue);

            result.ReturnAudioLevels._2 = new ett_api_domain.AudioPackageReturnAudioLevel();
            result.ReturnAudioLevels._2.Index = NullableInt(AudioReturnLevel02.CurrentValue);
            result.ReturnAudioLevels._2.LangTag = new ett_api_domain.LanguageTag();
            result.ReturnAudioLevels._2.LangTag.Index = NullableInt(AudioReturnLevelLangTag02.CurrentValue);
            result.ReturnAudioLevels._2.LangTag.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioReturnLevelLangTag02.CurrentValue);
            result.ReturnAudioLevels._2.TypeTag = new ett_api_domain.TypeTag();
            result.ReturnAudioLevels._2.TypeTag.Index = NullableInt(AudioReturnLevelTypeTag02.CurrentValue);
            result.ReturnAudioLevels._2.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioReturnLevelTypeTag02.CurrentValue);

            result.ReturnAudioLevels._3 = new ett_api_domain.AudioPackageReturnAudioLevel();
            result.ReturnAudioLevels._3.Index = NullableInt(AudioReturnLevel03.CurrentValue);
            result.ReturnAudioLevels._3.LangTag = new ett_api_domain.LanguageTag();
            result.ReturnAudioLevels._3.LangTag.Index = NullableInt(AudioReturnLevelLangTag03.CurrentValue);
            result.ReturnAudioLevels._3.LangTag.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioReturnLevelLangTag03.CurrentValue);
            result.ReturnAudioLevels._3.TypeTag = new ett_api_domain.TypeTag();
            result.ReturnAudioLevels._3.TypeTag.Index = NullableInt(AudioReturnLevelTypeTag03.CurrentValue);
            result.ReturnAudioLevels._3.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioReturnLevelTypeTag03.CurrentValue);

            result.ReturnAudioLevels._4 = new ett_api_domain.AudioPackageReturnAudioLevel();
            result.ReturnAudioLevels._4.Index = NullableInt(AudioReturnLevel04.CurrentValue);
            result.ReturnAudioLevels._4.LangTag = new ett_api_domain.LanguageTag();
            result.ReturnAudioLevels._4.LangTag.Index = NullableInt(AudioReturnLevelLangTag04.CurrentValue);
            result.ReturnAudioLevels._4.LangTag.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioReturnLevelLangTag04.CurrentValue);
            result.ReturnAudioLevels._4.TypeTag = new ett_api_domain.TypeTag();
            result.ReturnAudioLevels._4.TypeTag.Index = NullableInt(AudioReturnLevelTypeTag04.CurrentValue);
            result.ReturnAudioLevels._4.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioReturnLevelTypeTag04.CurrentValue);

            return result;

        }

        public void SetDocument(ett_api_domain.AudioPackage package)
        {

            if (package == null) throw new ArgumentNullException();
            if (package.Index != Index) throw new Exception("Index mismatch");

            PackageName.Change(package.PackageName, Enums.ChangeSources.API);
            ButtonName.Change(package.ButtonName, Enums.ChangeSources.API);

            if (package.OwnerTag != null) OwnerTag.Change(NullableInt(package.OwnerTag.Index), Enums.ChangeSources.API);
            else OwnerTag.Change(-1, Enums.ChangeSources.API);

            if (package.DelegateTag != null) DelegateTag.Change(NullableInt(package.DelegateTag.Index), Enums.ChangeSources.API);
            else DelegateTag.Change(-1, Enums.ChangeSources.API);

            if (package.LanguageTag != null) LanguageTag.Change(NullableInt(package.LanguageTag.Index), Enums.ChangeSources.API);
            else LanguageTag.Change(-1, Enums.ChangeSources.API);

            VideoLink.Change(NullableInt(package.VideoLink), Enums.ChangeSources.API);

            if (package.AudioLevels != null)
            {
                //process the audio levels

                if (package.AudioLevels._01 != null)
                {
                    AudioLevel01.Change(NullableInt(package.AudioLevels._01.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._01.TypeTag != null) AudioLevelTypeTag01.Change(NullableInt(package.AudioLevels._01.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag01.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel01.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag01.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._02 != null)
                {
                    AudioLevel02.Change(NullableInt(package.AudioLevels._02.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._02.TypeTag != null) AudioLevelTypeTag02.Change(NullableInt(package.AudioLevels._02.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag02.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel02.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag02.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._03 != null)
                {
                    AudioLevel03.Change(NullableInt(package.AudioLevels._03.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._03.TypeTag != null) AudioLevelTypeTag03.Change(NullableInt(package.AudioLevels._03.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag03.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel03.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag03.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._04 != null)
                {
                    AudioLevel04.Change(NullableInt(package.AudioLevels._04.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._04.TypeTag != null) AudioLevelTypeTag04.Change(NullableInt(package.AudioLevels._04.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag04.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel04.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag04.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._05 != null)
                {
                    AudioLevel05.Change(NullableInt(package.AudioLevels._05.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._05.TypeTag != null) AudioLevelTypeTag05.Change(NullableInt(package.AudioLevels._05.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag05.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel05.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag05.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._06 != null)
                {
                    AudioLevel06.Change(NullableInt(package.AudioLevels._06.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._06.TypeTag != null) AudioLevelTypeTag06.Change(NullableInt(package.AudioLevels._06.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag06.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel06.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag06.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._07 != null)
                {
                    AudioLevel07.Change(NullableInt(package.AudioLevels._07.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._07.TypeTag != null) AudioLevelTypeTag07.Change(NullableInt(package.AudioLevels._07.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag07.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel07.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag07.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._08 != null)
                {
                    AudioLevel08.Change(NullableInt(package.AudioLevels._08.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._08.TypeTag != null) AudioLevelTypeTag08.Change(NullableInt(package.AudioLevels._08.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag08.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel08.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag08.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._09 != null)
                {
                    AudioLevel09.Change(NullableInt(package.AudioLevels._09.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._09.TypeTag != null) AudioLevelTypeTag09.Change(NullableInt(package.AudioLevels._09.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag09.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel09.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag09.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._10 != null)
                {
                    AudioLevel10.Change(NullableInt(package.AudioLevels._10.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._10.TypeTag != null) AudioLevelTypeTag10.Change(NullableInt(package.AudioLevels._10.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag10.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel10.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag10.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._11 != null)
                {
                    AudioLevel11.Change(NullableInt(package.AudioLevels._11.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._11.TypeTag != null) AudioLevelTypeTag11.Change(NullableInt(package.AudioLevels._11.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag11.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel11.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag11.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._12 != null)
                {
                    AudioLevel12.Change(NullableInt(package.AudioLevels._12.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._12.TypeTag != null) AudioLevelTypeTag12.Change(NullableInt(package.AudioLevels._12.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag12.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel12.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag12.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._13 != null)
                {
                    AudioLevel13.Change(NullableInt(package.AudioLevels._13.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._13.TypeTag != null) AudioLevelTypeTag13.Change(NullableInt(package.AudioLevels._13.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag13.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel13.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag13.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._14 != null)
                {
                    AudioLevel14.Change(NullableInt(package.AudioLevels._14.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._14.TypeTag != null) AudioLevelTypeTag14.Change(NullableInt(package.AudioLevels._14.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag14.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel14.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag14.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._15 != null)
                {
                    AudioLevel15.Change(NullableInt(package.AudioLevels._15.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._15.TypeTag != null) AudioLevelTypeTag15.Change(NullableInt(package.AudioLevels._15.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag15.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel15.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag15.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AudioLevels._16 != null)
                {
                    AudioLevel16.Change(NullableInt(package.AudioLevels._16.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._16.TypeTag != null) AudioLevelTypeTag16.Change(NullableInt(package.AudioLevels._16.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioLevelTypeTag16.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel16.Change(-1, Enums.ChangeSources.API);
                    AudioLevelTypeTag16.Change(-1, Enums.ChangeSources.API);
                }

            }
            else
            {
                AudioLevel01.Change(-1, Enums.ChangeSources.API);
                AudioLevel02.Change(-1, Enums.ChangeSources.API);
                AudioLevel03.Change(-1, Enums.ChangeSources.API);
                AudioLevel04.Change(-1, Enums.ChangeSources.API);
                AudioLevel05.Change(-1, Enums.ChangeSources.API);
                AudioLevel06.Change(-1, Enums.ChangeSources.API);
                AudioLevel07.Change(-1, Enums.ChangeSources.API);
                AudioLevel08.Change(-1, Enums.ChangeSources.API);
                AudioLevel09.Change(-1, Enums.ChangeSources.API);
                AudioLevel10.Change(-1, Enums.ChangeSources.API);
                AudioLevel11.Change(-1, Enums.ChangeSources.API);
                AudioLevel12.Change(-1, Enums.ChangeSources.API);
                AudioLevel13.Change(-1, Enums.ChangeSources.API);
                AudioLevel14.Change(-1, Enums.ChangeSources.API);
                AudioLevel15.Change(-1, Enums.ChangeSources.API);
                AudioLevel16.Change(-1, Enums.ChangeSources.API);

                AudioLevelTypeTag01.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag02.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag03.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag04.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag05.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag06.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag07.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag08.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag09.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag10.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag11.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag12.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag13.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag14.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag15.Change(-1, Enums.ChangeSources.API);
                AudioLevelTypeTag16.Change(-1, Enums.ChangeSources.API);
            }

            if (package.ReturnAudioLevels != null)
            {

                if (package.ReturnAudioLevels._1 != null)
                {
                    AudioReturnLevel01.Change(NullableInt(package.ReturnAudioLevels._1.Index), Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._1.TypeTag != null) AudioReturnLevelTypeTag01.Change(NullableInt(package.ReturnAudioLevels._1.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevelTypeTag01.Change(-1, Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._1.LangTag != null) AudioReturnLevelLangTag01.Change(NullableInt(package.ReturnAudioLevels._1.LangTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevelLangTag01.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioReturnLevel01.Change(-1, Enums.ChangeSources.API);
                    AudioReturnLevelLangTag01.Change(-1, Enums.ChangeSources.API);
                    audioReturnLevelTypeTag01.Change(-1, Enums.ChangeSources.API);
                }

                if (package.ReturnAudioLevels._2 != null)
                {
                    AudioReturnLevel02.Change(NullableInt(package.ReturnAudioLevels._2.Index), Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._2.TypeTag != null) AudioReturnLevelTypeTag02.Change(NullableInt(package.ReturnAudioLevels._2.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevelTypeTag02.Change(-1, Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._2.LangTag != null) AudioReturnLevelLangTag02.Change(NullableInt(package.ReturnAudioLevels._2.LangTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevelLangTag02.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioReturnLevel02.Change(-1, Enums.ChangeSources.API);
                    AudioReturnLevelLangTag02.Change(-1, Enums.ChangeSources.API);
                    audioReturnLevelTypeTag02.Change(-1, Enums.ChangeSources.API);
                }

                if (package.ReturnAudioLevels._3 != null)
                {
                    AudioReturnLevel03.Change(NullableInt(package.ReturnAudioLevels._3.Index), Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._3.TypeTag != null) AudioReturnLevelTypeTag03.Change(NullableInt(package.ReturnAudioLevels._3.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevelTypeTag03.Change(-1, Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._3.LangTag != null) AudioReturnLevelLangTag03.Change(NullableInt(package.ReturnAudioLevels._3.LangTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevelLangTag03.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioReturnLevel03.Change(-1, Enums.ChangeSources.API);
                    AudioReturnLevelLangTag03.Change(-1, Enums.ChangeSources.API);
                    audioReturnLevelTypeTag03.Change(-1, Enums.ChangeSources.API);
                }

                if (package.ReturnAudioLevels._4 != null)
                {
                    AudioReturnLevel04.Change(NullableInt(package.ReturnAudioLevels._4.Index), Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._4.TypeTag != null) AudioReturnLevelTypeTag04.Change(NullableInt(package.ReturnAudioLevels._4.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevelTypeTag04.Change(-1, Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._4.LangTag != null) AudioReturnLevelLangTag04.Change(NullableInt(package.ReturnAudioLevels._4.LangTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevelLangTag04.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioReturnLevel04.Change(-1, Enums.ChangeSources.API);
                    AudioReturnLevelLangTag04.Change(-1, Enums.ChangeSources.API);
                    audioReturnLevelTypeTag04.Change(-1, Enums.ChangeSources.API);
                }

            }
            else
            {
                AudioReturnLevel01.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevel02.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevel03.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevel04.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevelLangTag01.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevelLangTag02.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevelLangTag03.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevelLangTag04.Change(-1, Enums.ChangeSources.API);
                audioReturnLevelTypeTag01.Change(-1, Enums.ChangeSources.API);
                audioReturnLevelTypeTag02.Change(-1, Enums.ChangeSources.API);
                audioReturnLevelTypeTag03.Change(-1, Enums.ChangeSources.API);
                audioReturnLevelTypeTag04.Change(-1, Enums.ChangeSources.API);

            }
        }

    }
}
