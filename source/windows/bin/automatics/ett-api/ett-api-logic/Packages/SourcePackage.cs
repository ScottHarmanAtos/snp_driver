﻿using Gibraltar.Agent;
using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.packages
{
    public class SourcePackage : BasePackage
    {

        const string STR_LoggingCategory = "BNCS_ETT_API.Packages.SourcePackage";

        /// <summary>Initializes a new instance of the <a onclick="return false;" href="T:MasterSourcePackage" originaltag="see">T:MasterSourcePackage</a> class.</summary>
        /// <param name="Index">The BNCS index of the package</param>
        public SourcePackage(uint Index) : base(Index)
        {
            PackageName.AffectedDb = (int)DbConstants.dbSourcePackageTitle;
            ButtonName.AffectedDb = (int)DbConstants.dbSourceButtonName;
            OwnerTag.AffectedDb = (int)DbConstants.dbSourcePackageTags;
            DelegateTag.AffectedDb = (int)DbConstants.dbSourcePackageTags;
            UniqueId.AffectedDb = (int)DbConstants.dbSourcePackageTags;
        }

        private TrackedParameter<string> mcrNotes = new TrackedParameter<string>(DbConstants.dbMCRNotes);

        private TrackedParameter<int> videoLevel01VideoIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02VideoIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03VideoIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04VideoIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);

        private TrackedParameter<int> videoLevel01AudioPackageIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02AudioPackageIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03AudioPackageIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04AudioPackageIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);

        private TrackedParameter<int> videoLevel01VideoHubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02VideoHubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03VideoHubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04VideoHubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);

        private TrackedParameter<int> videoLevel01AudioHubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel01Anc1Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel01Anc1HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel01Anc2Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel01Anc2HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel01Anc3Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel01Anc3HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel01Anc4Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel01Anc4HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02AudioHubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02Anc1Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02Anc1HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02Anc2Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02Anc2HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02Anc3Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02Anc3HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02Anc4Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel02Anc4HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03AudioHubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03Anc1Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03Anc1HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03Anc2Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03Anc2HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03Anc3Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03Anc3HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03Anc4Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel03Anc4HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04AudioHubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04Anc1Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04Anc1HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04Anc2Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04Anc2HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04Anc3Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04Anc3HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04Anc4Index = new TrackedParameter<int>(DbConstants.dbSourceVideo);
        private TrackedParameter<int> videoLevel04Anc4HubTagIndex = new TrackedParameter<int>(DbConstants.dbSourceVideo);

        private TrackedParameter<int> audioLevel01Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel02Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel03Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel04Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel05Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel06Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel07Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel08Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel09Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel10Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel11Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel12Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel13Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel14Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel15Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel16Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel17Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel18Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel19Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel20Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel21Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel22Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel23Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel24Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel25Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel26Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel27Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel28Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel29Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel30Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel31Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);
        private TrackedParameter<int> audioLevel32Index = new TrackedParameter<int>(DbConstants.dbAudioSourcePackages);

        private TrackedParameter<string> mcrUmd = new TrackedParameter<string>(DbConstants.dbMCRUmd);

        private TrackedParameter<string> productionUmd01 = new TrackedParameter<string>(DbConstants.dbProductionUmd);
        private TrackedParameter<string> productionUmd02 = new TrackedParameter<string>(DbConstants.dbProductionUmd);
        private TrackedParameter<string> productionUmd03 = new TrackedParameter<string>(DbConstants.dbProductionUmd);
        private TrackedParameter<string> productionUmd04 = new TrackedParameter<string>(DbConstants.dbProductionUmd);

        private TrackedParameter<string> eventName = new TrackedParameter<string>(DbConstants.dbSourceEventName);
        private TrackedParameter<string> spevList = new TrackedParameter<string>(DbConstants.dbSourceSpevList1);
        private TrackedParameter<string> recordId = new TrackedParameter<string>(DbConstants.dbSourceRecordId);
        private TrackedParameter<string> classId = new TrackedParameter<string>(DbConstants.dbSourceClassId);
        private TrackedParameter<DateTime> eventStart = new TrackedParameter<DateTime>(DbConstants.dbSourceDates);
        private TrackedParameter<DateTime> eventEnd = new TrackedParameter<DateTime>(DbConstants.dbSourceDates);
        private TrackedParameter<string> team1 = new TrackedParameter<string>(DbConstants.dbSourceTeams);
        private TrackedParameter<string> team2 = new TrackedParameter<string>(DbConstants.dbSourceTeams);

        public override string PackageType => "sp";

        public TrackedParameter<string> McrNotes { get => mcrNotes; set => mcrNotes = value; }
        public string FunctionalArea { get; private set; }
        public string FunctionalName { get; private set; }
        public TrackedParameter<int> VideoLevel01VideoIndex { get => videoLevel01VideoIndex; set => videoLevel01VideoIndex = value; }
        public TrackedParameter<int> VideoLevel02VideoIndex { get => videoLevel02VideoIndex; set => videoLevel02VideoIndex = value; }
        public TrackedParameter<int> VideoLevel03VideoIndex { get => videoLevel03VideoIndex; set => videoLevel03VideoIndex = value; }
        public TrackedParameter<int> VideoLevel04VideoIndex { get => videoLevel04VideoIndex; set => videoLevel04VideoIndex = value; }
        public TrackedParameter<int> VideoLevel01AudioPackageIndex { get => videoLevel01AudioPackageIndex; set => videoLevel01AudioPackageIndex = value; }
        public TrackedParameter<int> VideoLevel02AudioPackageIndex { get => videoLevel02AudioPackageIndex; set => videoLevel02AudioPackageIndex = value; }
        public TrackedParameter<int> VideoLevel03AudioPackageIndex { get => videoLevel03AudioPackageIndex; set => videoLevel03AudioPackageIndex = value; }
        public TrackedParameter<int> VideoLevel04AudioPackageIndex { get => videoLevel04AudioPackageIndex; set => videoLevel04AudioPackageIndex = value; }
        public TrackedParameter<int> VideoLevel01VideoHubTagIndex { get => videoLevel01VideoHubTagIndex; set => videoLevel01VideoHubTagIndex = value; }
        public TrackedParameter<int> VideoLevel02VideoHubTagIndex { get => videoLevel02VideoHubTagIndex; set => videoLevel02VideoHubTagIndex = value; }
        public TrackedParameter<int> VideoLevel03VideoHubTagIndex { get => videoLevel03VideoHubTagIndex; set => videoLevel03VideoHubTagIndex = value; }
        public TrackedParameter<int> VideoLevel04VideoHubTagIndex { get => videoLevel04VideoHubTagIndex; set => videoLevel04VideoHubTagIndex = value; }
        public TrackedParameter<int> AudioLevel01Index { get => audioLevel01Index; set => audioLevel01Index = value; }
        public TrackedParameter<int> AudioLevel02Index { get => audioLevel02Index; set => audioLevel02Index = value; }
        public TrackedParameter<int> AudioLevel03Index { get => audioLevel03Index; set => audioLevel03Index = value; }
        public TrackedParameter<int> AudioLevel04Index { get => audioLevel04Index; set => audioLevel04Index = value; }
        public TrackedParameter<int> AudioLevel05Index { get => audioLevel05Index; set => audioLevel05Index = value; }
        public TrackedParameter<int> AudioLevel06Index { get => audioLevel06Index; set => audioLevel06Index = value; }
        public TrackedParameter<int> AudioLevel07Index { get => audioLevel07Index; set => audioLevel07Index = value; }
        public TrackedParameter<int> AudioLevel08Index { get => audioLevel08Index; set => audioLevel08Index = value; }
        public TrackedParameter<int> AudioLevel09Index { get => audioLevel09Index; set => audioLevel09Index = value; }
        public TrackedParameter<int> AudioLevel10Index { get => audioLevel10Index; set => audioLevel10Index = value; }
        public TrackedParameter<int> AudioLevel11Index { get => audioLevel11Index; set => audioLevel11Index = value; }
        public TrackedParameter<int> AudioLevel12Index { get => audioLevel12Index; set => audioLevel12Index = value; }
        public TrackedParameter<int> AudioLevel13Index { get => audioLevel13Index; set => audioLevel13Index = value; }
        public TrackedParameter<int> AudioLevel14Index { get => audioLevel14Index; set => audioLevel14Index = value; }
        public TrackedParameter<int> AudioLevel15Index { get => audioLevel15Index; set => audioLevel15Index = value; }
        public TrackedParameter<int> AudioLevel16Index { get => audioLevel16Index; set => audioLevel16Index = value; }
        public TrackedParameter<int> AudioLevel17Index { get => audioLevel17Index; set => audioLevel17Index = value; }
        public TrackedParameter<int> AudioLevel18Index { get => audioLevel18Index; set => audioLevel18Index = value; }
        public TrackedParameter<int> AudioLevel19Index { get => audioLevel19Index; set => audioLevel19Index = value; }
        public TrackedParameter<int> AudioLevel20Index { get => audioLevel20Index; set => audioLevel20Index = value; }
        public TrackedParameter<int> AudioLevel21Index { get => audioLevel21Index; set => audioLevel21Index = value; }
        public TrackedParameter<int> AudioLevel22Index { get => audioLevel22Index; set => audioLevel22Index = value; }
        public TrackedParameter<int> AudioLevel23Index { get => audioLevel23Index; set => audioLevel23Index = value; }
        public TrackedParameter<int> AudioLevel24Index { get => audioLevel24Index; set => audioLevel24Index = value; }
        public TrackedParameter<int> AudioLevel25Index { get => audioLevel25Index; set => audioLevel25Index = value; }
        public TrackedParameter<int> AudioLevel26Index { get => audioLevel26Index; set => audioLevel26Index = value; }
        public TrackedParameter<int> AudioLevel27Index { get => audioLevel27Index; set => audioLevel27Index = value; }
        public TrackedParameter<int> AudioLevel28Index { get => audioLevel28Index; set => audioLevel28Index = value; }
        public TrackedParameter<int> AudioLevel29Index { get => audioLevel29Index; set => audioLevel29Index = value; }
        public TrackedParameter<int> AudioLevel30Index { get => audioLevel30Index; set => audioLevel30Index = value; }
        public TrackedParameter<int> AudioLevel31Index { get => audioLevel31Index; set => audioLevel31Index = value; }
        public TrackedParameter<int> AudioLevel32Index { get => audioLevel32Index; set => audioLevel32Index = value; }
        public TrackedParameter<string> McrUmd { get => mcrUmd; set => mcrUmd = value; }
        public TrackedParameter<string> ProductionUmd01 { get => productionUmd01; set => productionUmd01 = value; }
        public TrackedParameter<string> ProductionUmd02 { get => productionUmd02; set => productionUmd02 = value; }
        public TrackedParameter<string> ProductionUmd03 { get => productionUmd03; set => productionUmd03 = value; }
        public TrackedParameter<string> ProductionUmd04 { get => productionUmd04; set => productionUmd04 = value; }
        public TrackedParameter<int> VideoLevel01AudioHubTagIndex { get => videoLevel01AudioHubTagIndex; set => videoLevel01AudioHubTagIndex = value; }
        public TrackedParameter<int> VideoLevel01Anc1Index { get => videoLevel01Anc1Index; set => videoLevel01Anc1Index = value; }
        public TrackedParameter<int> VideoLevel01Anc1HubTagIndex { get => videoLevel01Anc1HubTagIndex; set => videoLevel01Anc1HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel01Anc2Index { get => videoLevel01Anc2Index; set => videoLevel01Anc2Index = value; }
        public TrackedParameter<int> VideoLevel01Anc2HubTagIndex { get => videoLevel01Anc2HubTagIndex; set => videoLevel01Anc2HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel01Anc3Index { get => videoLevel01Anc3Index; set => videoLevel01Anc3Index = value; }
        public TrackedParameter<int> VideoLevel01Anc3HubTagIndex { get => videoLevel01Anc3HubTagIndex; set => videoLevel01Anc3HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel01Anc4Index { get => videoLevel01Anc4Index; set => videoLevel01Anc4Index = value; }
        public TrackedParameter<int> VideoLevel01Anc4HubTagIndex { get => videoLevel01Anc4HubTagIndex; set => videoLevel01Anc4HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel02AudioHubTagIndex { get => videoLevel02AudioHubTagIndex; set => videoLevel02AudioHubTagIndex = value; }
        public TrackedParameter<int> VideoLevel02Anc1Index { get => videoLevel02Anc1Index; set => videoLevel02Anc1Index = value; }
        public TrackedParameter<int> VideoLevel02Anc1HubTagIndex { get => videoLevel02Anc1HubTagIndex; set => videoLevel02Anc1HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel02Anc2Index { get => videoLevel02Anc2Index; set => videoLevel02Anc2Index = value; }
        public TrackedParameter<int> VideoLevel02Anc2HubTagIndex { get => videoLevel02Anc2HubTagIndex; set => videoLevel02Anc2HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel02Anc3Index { get => videoLevel02Anc3Index; set => videoLevel02Anc3Index = value; }
        public TrackedParameter<int> VideoLevel02Anc3HubTagIndex { get => videoLevel02Anc3HubTagIndex; set => videoLevel02Anc3HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel02Anc4Index { get => videoLevel02Anc4Index; set => videoLevel02Anc4Index = value; }
        public TrackedParameter<int> VideoLevel02Anc4HubTagIndex { get => videoLevel02Anc4HubTagIndex; set => videoLevel02Anc4HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel03AudioHubTagIndex { get => videoLevel03AudioHubTagIndex; set => videoLevel03AudioHubTagIndex = value; }
        public TrackedParameter<int> VideoLevel03Anc1Index { get => videoLevel03Anc1Index; set => videoLevel03Anc1Index = value; }
        public TrackedParameter<int> VideoLevel03Anc1HubTagIndex { get => videoLevel03Anc1HubTagIndex; set => videoLevel03Anc1HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel03Anc2Index { get => videoLevel03Anc2Index; set => videoLevel03Anc2Index = value; }
        public TrackedParameter<int> VideoLevel03Anc2HubTagIndex { get => videoLevel03Anc2HubTagIndex; set => videoLevel03Anc2HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel03Anc3Index { get => videoLevel03Anc3Index; set => videoLevel03Anc3Index = value; }
        public TrackedParameter<int> VideoLevel03Anc3HubTagIndex { get => videoLevel03Anc3HubTagIndex; set => videoLevel03Anc3HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel03Anc4Index { get => videoLevel03Anc4Index; set => videoLevel03Anc4Index = value; }
        public TrackedParameter<int> VideoLevel03Anc4HubTagIndex { get => videoLevel03Anc4HubTagIndex; set => videoLevel03Anc4HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel04AudioHubTagIndex { get => videoLevel04AudioHubTagIndex; set => videoLevel04AudioHubTagIndex = value; }
        public TrackedParameter<int> VideoLevel04Anc1Index { get => videoLevel04Anc1Index; set => videoLevel04Anc1Index = value; }
        public TrackedParameter<int> VideoLevel04Anc1HubTagIndex { get => videoLevel04Anc1HubTagIndex; set => videoLevel04Anc1HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel04Anc2Index { get => videoLevel04Anc2Index; set => videoLevel04Anc2Index = value; }
        public TrackedParameter<int> VideoLevel04Anc2HubTagIndex { get => videoLevel04Anc2HubTagIndex; set => videoLevel04Anc2HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel04Anc3Index { get => videoLevel04Anc3Index; set => videoLevel04Anc3Index = value; }
        public TrackedParameter<int> VideoLevel04Anc3HubTagIndex { get => videoLevel04Anc3HubTagIndex; set => videoLevel04Anc3HubTagIndex = value; }
        public TrackedParameter<int> VideoLevel04Anc4Index { get => videoLevel04Anc4Index; set => videoLevel04Anc4Index = value; }
        public TrackedParameter<int> VideoLevel04Anc4HubTagIndex { get => videoLevel04Anc4HubTagIndex; set => videoLevel04Anc4HubTagIndex = value; }
        public TrackedParameter<string> EventName { get => eventName; set => eventName = value; }
        public TrackedParameter<string> SpevList { get => spevList; set => spevList = value; }
        public TrackedParameter<string> RecordId { get => recordId; set => recordId = value; }
        public TrackedParameter<string> ClassId { get => classId; set => classId = value; }
        public TrackedParameter<DateTime> EventStart { get => eventStart; set => eventStart = value; }
        public TrackedParameter<DateTime> EventEnd { get => eventEnd; set => eventEnd = value; }
        public TrackedParameter<string> Team1 { get => team1; set => team1 = value; }
        public TrackedParameter<string> Team2 { get => team2; set => team2 = value; }

        private string[] spevdb = new string[4];

        protected override void ProcessDbChange(uint dbNumber)
        {
            switch (dbNumber)
            {
                case DbConstants.dbSourcePackageTitle:

                    UpdateDbEntry(dbNumber, PackageName.CurrentValue);
                    break;

                case DbConstants.dbSourceButtonName:

                    UpdateDbEntry(dbNumber, ButtonName.CurrentValue);
                    break;

                case DbConstants.dbSourcePackageTags:

                    SetTaggedValue(dbNumber, "unique_id", UniqueId.CurrentValue);
                    SetTaggedValue(dbNumber, "owner", GetTaggedIntValue(OwnerTag.CurrentValue));
                    SetTaggedValue(dbNumber, "delegate", GetTaggedIntValue(DelegateTag.CurrentValue));
                    break;

                case DbConstants.dbMCRNotes:

                    UpdateDbEntry(dbNumber, McrNotes.CurrentValue);
                    break;

                case DbConstants.dbSourceVideo:

                    var level1 = $"{GetTaggedIntValue(VideoLevel01VideoIndex.CurrentValue)}#{GetTaggedIntValue(VideoLevel01VideoHubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel01Anc1Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel01Anc1HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel01Anc2Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel01Anc2HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel01Anc3Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel01Anc3HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel01Anc4Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel01Anc4HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel01AudioPackageIndex.CurrentValue)}#{GetTaggedIntValue(VideoLevel01AudioHubTagIndex.CurrentValue)}";

                    var level2 = $"{GetTaggedIntValue(VideoLevel02VideoIndex.CurrentValue)}#{GetTaggedIntValue(VideoLevel02VideoHubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel02Anc1Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel02Anc1HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel02Anc2Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel02Anc2HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel02Anc3Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel02Anc3HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel02Anc4Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel02Anc4HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel02AudioPackageIndex.CurrentValue)}#{GetTaggedIntValue(VideoLevel02AudioHubTagIndex.CurrentValue)}";

                    var level3 = $"{GetTaggedIntValue(VideoLevel03VideoIndex.CurrentValue)}#{GetTaggedIntValue(VideoLevel03VideoHubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel03Anc1Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel03Anc1HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel03Anc2Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel03Anc2HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel03Anc3Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel03Anc3HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel03Anc4Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel03Anc4HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel03AudioPackageIndex.CurrentValue)}#{GetTaggedIntValue(VideoLevel03AudioHubTagIndex.CurrentValue)}";

                    var level4 = $"{GetTaggedIntValue(VideoLevel04VideoIndex.CurrentValue)}#{GetTaggedIntValue(VideoLevel04VideoHubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel04Anc1Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel04Anc1HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel04Anc2Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel04Anc2HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel04Anc3Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel04Anc3HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel04Anc4Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel04Anc4HubTagIndex.CurrentValue)}|" +
                                 $"{GetTaggedIntValue(VideoLevel04AudioPackageIndex.CurrentValue)}#{GetTaggedIntValue(VideoLevel04AudioHubTagIndex.CurrentValue)}";

                    var srcVideoValue = $"L1={level1},L2={level2},L3={level3},L4={level4}";

                    UpdateDbEntry(dbNumber, srcVideoValue);

                    break;

                case DbConstants.dbAudioSourcePackages:

                    var valList = new List<string>();

                    valList.Add(GetTaggedIntValue(AudioLevel01Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel02Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel03Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel04Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel05Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel06Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel07Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel08Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel09Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel10Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel11Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel12Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel13Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel14Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel15Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel16Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel17Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel18Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel19Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel20Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel21Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel22Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel23Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel24Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel25Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel26Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel27Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel28Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel29Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel30Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel31Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel32Index.CurrentValue));

                    var srcAudioValue = String.Join("|", valList);

                    UpdateDbEntry(dbNumber, srcAudioValue);

                    break;

                case DbConstants.dbMCRUmd:

                    UpdateDbEntry(dbNumber, McrUmd.CurrentValue);

                    break;

                case DbConstants.dbProductionUmd:

                    SetTaggedValue(dbNumber, "umd1", productionUmd01.CurrentValue);
                    SetTaggedValue(dbNumber, "umd2", productionUmd02.CurrentValue);
                    SetTaggedValue(dbNumber, "umd3", productionUmd03.CurrentValue);
                    SetTaggedValue(dbNumber, "umd4", productionUmd04.CurrentValue);

                    break;

                case DbConstants.dbSourceEventName:

                    UpdateDbEntry(dbNumber, EventName.CurrentValue);

                    break;

                case DbConstants.dbSourceSpevList1:

                    //split the spev list into its 4 databases
                    string[] list = SpevList.CurrentValue.Split(',');

                    //build dbstrings
                    spevdb[0] = string.Empty;
                    spevdb[1] = string.Empty;
                    spevdb[2] = string.Empty;
                    spevdb[3] = string.Empty;

                    int entryCount = 0;
                    int dbCount = 0;

                    for (int i = 0; i < list.Length; i++)
                    {

                        if (entryCount == 0) spevdb[dbCount] = list[i]; //first entry in this db list
                        else spevdb[dbCount] = $"{spevdb[dbCount]},{list[i]}";  //append entry

                        entryCount += 1;

                        //check for db rollover
                        if (entryCount == 8)
                        {

                            //if this isn't the last entry in the list. append a +
                            if (i < (list.Length - 1)) spevdb[dbCount] = $"{spevdb[dbCount]}+";

                            dbCount += 1;
                            entryCount = 0;

                        }

                    }

                    UpdateDbEntry(DbConstants.dbSourceSpevList1, spevdb[0]);
                    UpdateDbEntry(DbConstants.dbSourceSpevList2, spevdb[1]);
                    UpdateDbEntry(DbConstants.dbSourceSpevList3, spevdb[2]);
                    UpdateDbEntry(DbConstants.dbSourceSpevList4, spevdb[3]);

                    break;

                case DbConstants.dbSourceRecordId:

                    UpdateDbEntry(dbNumber, RecordId.CurrentValue);

                    break;

                case DbConstants.dbSourceClassId:

                    UpdateDbEntry(dbNumber, ClassId.CurrentValue);

                    break;

                case DbConstants.dbSourceTeams:

                    SetTaggedValue(dbNumber, "team_1", Team1.CurrentValue);
                    SetTaggedValue(dbNumber, "team_2", Team2.CurrentValue);

                    break;

                case DbConstants.dbSourceDates:

                    var startDate = EventStart.CurrentValue.ToString("dd/MM/yyyy");
                    var startTime = EventStart.CurrentValue.ToString("HH:mm:ss");
                    var endDate = startDate;
                    var endTime = EventEnd.CurrentValue.ToString("HH:mm:ss");

                    SetTaggedValue(dbNumber, "start_date", startDate);
                    SetTaggedValue(dbNumber, "start_time", startTime);
                    SetTaggedValue(dbNumber, "end_date", endDate);
                    SetTaggedValue(dbNumber, "end_time", endTime);

                    break;

                default:
                    break;
            }
        }

        protected override bool ProcessDbValue(uint dbNumber, string value, bool initMode)
        {
            switch (dbNumber)
            {
                case DbConstants.dbSourcePackageTitle:

                    PackageName.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbSourceButtonName:

                    ButtonName.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbSourcePackageTags:

                    //field contains UniqueId, owner & delegate fields
                    UniqueId.Set(ReadTaggedValue(value, "unique_id"), Enums.ChangeSources.BNCS, initMode);
                    OwnerTag.Set(GetTaggedIntValue(ReadTaggedValue(value, "owner")), Enums.ChangeSources.BNCS, initMode);
                    DelegateTag.Set(GetTaggedIntValue(ReadTaggedValue(value, "delegate")), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbFunctionalSourceArea:

                    FunctionalArea = value;

                    return true;

                case DbConstants.dbFunctionalSourceName:

                    FunctionalName = value;

                    return true;

                case DbConstants.dbMCRNotes:

                    McrNotes.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbSourceVideo:

                    if (String.IsNullOrWhiteSpace(value))
                    {
                        //Log.Warning(STR_LoggingCategory, "Audio Source Package null value", "Index: {0}", Index);
                        return false;
                    }

                    var level01 = ReadTaggedValue(value, "L1");
                    var split01 = level01.Split('|'); //array now contains 5 tuples
                    var tuple01 = new List<(string, string, string, string)>();

                    foreach (var split in split01)
                    {
                        tuple01.Add(GetTuple(split));
                    }

                    var level02 = ReadTaggedValue(value, "L2");
                    var split02 = level02.Split('|'); //array now contains 5 tuples
                    var tuple02 = new List<(string, string, string, string)>();

                    foreach (var split in split02)
                    {
                        tuple02.Add(GetTuple(split));
                    }

                    var level03 = ReadTaggedValue(value, "L3");
                    var split03 = level03.Split('|'); //array now contains 5 tuples
                    var tuple03 = new List<(string, string, string, string)>();

                    foreach (var split in split03)
                    {
                        tuple03.Add(GetTuple(split));
                    }

                    var level04 = ReadTaggedValue(value, "L4");
                    var split04 = level04.Split('|'); //array now contains 5 tuples
                    var tuple04 = new List<(string, string, string, string)>();

                    foreach (var split in split04)
                    {
                        tuple04.Add(GetTuple(split));
                    }

                    if (tuple01.Count < 6 || tuple02.Count < 6 || tuple03.Count < 6 || tuple04.Count < 6)
                    {
                        Log.Warning(STR_LoggingCategory, "Error decoding destination package source video", "Index: {0}  Value: {1}", Index, value);
                    }

                    VideoLevel01VideoIndex.Set(GetTaggedIntValue(tuple01[00].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01VideoHubTagIndex.Set(GetTaggedIntValue(tuple01[00].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01Anc1Index.Set(GetTaggedIntValue(tuple01[01].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01Anc1HubTagIndex.Set(GetTaggedIntValue(tuple01[01].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01Anc2Index.Set(GetTaggedIntValue(tuple01[02].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01Anc2HubTagIndex.Set(GetTaggedIntValue(tuple01[02].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01Anc3Index.Set(GetTaggedIntValue(tuple01[03].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01Anc3HubTagIndex.Set(GetTaggedIntValue(tuple01[03].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01Anc4Index.Set(GetTaggedIntValue(tuple01[04].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01Anc4HubTagIndex.Set(GetTaggedIntValue(tuple01[04].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01AudioPackageIndex.Set(GetTaggedIntValue(tuple01[05].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01AudioHubTagIndex.Set(GetTaggedIntValue(tuple01[05].Item2), Enums.ChangeSources.BNCS, initMode);

                    VideoLevel02VideoIndex.Set(GetTaggedIntValue(tuple02[00].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02VideoHubTagIndex.Set(GetTaggedIntValue(tuple02[00].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02Anc1Index.Set(GetTaggedIntValue(tuple02[01].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02Anc1HubTagIndex.Set(GetTaggedIntValue(tuple02[01].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02Anc2Index.Set(GetTaggedIntValue(tuple02[02].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02Anc2HubTagIndex.Set(GetTaggedIntValue(tuple02[02].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02Anc3Index.Set(GetTaggedIntValue(tuple02[03].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02Anc3HubTagIndex.Set(GetTaggedIntValue(tuple02[03].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02Anc4Index.Set(GetTaggedIntValue(tuple02[04].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02Anc4HubTagIndex.Set(GetTaggedIntValue(tuple02[04].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02AudioPackageIndex.Set(GetTaggedIntValue(tuple02[05].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel02AudioHubTagIndex.Set(GetTaggedIntValue(tuple02[05].Item2), Enums.ChangeSources.BNCS, initMode);

                    VideoLevel03VideoIndex.Set(GetTaggedIntValue(tuple03[00].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03VideoHubTagIndex.Set(GetTaggedIntValue(tuple03[00].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03Anc1Index.Set(GetTaggedIntValue(tuple03[01].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03Anc1HubTagIndex.Set(GetTaggedIntValue(tuple03[01].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03Anc2Index.Set(GetTaggedIntValue(tuple03[02].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03Anc2HubTagIndex.Set(GetTaggedIntValue(tuple03[02].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03Anc3Index.Set(GetTaggedIntValue(tuple03[03].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03Anc3HubTagIndex.Set(GetTaggedIntValue(tuple03[03].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03Anc4Index.Set(GetTaggedIntValue(tuple03[04].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03Anc4HubTagIndex.Set(GetTaggedIntValue(tuple03[04].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03AudioPackageIndex.Set(GetTaggedIntValue(tuple03[05].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel03AudioHubTagIndex.Set(GetTaggedIntValue(tuple03[05].Item2), Enums.ChangeSources.BNCS, initMode);

                    VideoLevel04VideoIndex.Set(GetTaggedIntValue(tuple04[00].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04VideoHubTagIndex.Set(GetTaggedIntValue(tuple04[00].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04Anc1Index.Set(GetTaggedIntValue(tuple04[01].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04Anc1HubTagIndex.Set(GetTaggedIntValue(tuple04[01].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04Anc2Index.Set(GetTaggedIntValue(tuple04[02].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04Anc2HubTagIndex.Set(GetTaggedIntValue(tuple04[02].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04Anc3Index.Set(GetTaggedIntValue(tuple04[03].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04Anc3HubTagIndex.Set(GetTaggedIntValue(tuple04[03].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04Anc4Index.Set(GetTaggedIntValue(tuple04[04].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04Anc4HubTagIndex.Set(GetTaggedIntValue(tuple04[04].Item2), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04AudioPackageIndex.Set(GetTaggedIntValue(tuple04[05].Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel04AudioHubTagIndex.Set(GetTaggedIntValue(tuple04[05].Item2), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioSourcePackages:

                    if (String.IsNullOrWhiteSpace(value))
                    {
                        //Log.Warning(STR_LoggingCategory, "Audio Source Package null value", "Index: {0}", Index);
                        return false;
                    }

                    //source should be a pipe delimited array of 32 integers
                    var strVals = value.Split('|');

                    //at this stage strVals should contain 32 entries
                    if (strVals.Length < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Audio Source Package length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, strVals.Length);
                        return false;
                    }

                    var intVals = new List<int>();

                    //convert each value into an integer and put it in the list
                    foreach (var val in strVals)
                    {
                        if (string.IsNullOrWhiteSpace(val)) intVals.Add(0);
                        else if (int.TryParse(val, out int result)) intVals.Add(result);
                        else 
                        {
                            //invalid value!
                            Log.Warning(STR_LoggingCategory, "Invalid Audio Source Package entry", "Index: {0}  Value: {1}", Index, value);
                            intVals.Add(0);
                        }
                    }

                    //at this stage intValue should contain 32 entries
                    if (intVals.Count < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Audio Source Package length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, intVals.Count);
                        return false;
                    }

                    AudioLevel01Index.Set(intVals[00], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02Index.Set(intVals[01], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03Index.Set(intVals[02], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04Index.Set(intVals[03], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05Index.Set(intVals[04], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06Index.Set(intVals[05], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07Index.Set(intVals[06], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08Index.Set(intVals[07], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09Index.Set(intVals[08], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10Index.Set(intVals[09], Enums.ChangeSources.BNCS, initMode);

                    AudioLevel11Index.Set(intVals[10], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12Index.Set(intVals[11], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13Index.Set(intVals[12], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14Index.Set(intVals[13], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15Index.Set(intVals[14], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16Index.Set(intVals[15], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel17Index.Set(intVals[16], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel18Index.Set(intVals[17], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel19Index.Set(intVals[18], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel20Index.Set(intVals[19], Enums.ChangeSources.BNCS, initMode);

                    AudioLevel21Index.Set(intVals[20], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel22Index.Set(intVals[21], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel23Index.Set(intVals[22], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel24Index.Set(intVals[23], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel25Index.Set(intVals[24], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel26Index.Set(intVals[25], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel27Index.Set(intVals[26], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel28Index.Set(intVals[27], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel29Index.Set(intVals[28], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel30Index.Set(intVals[29], Enums.ChangeSources.BNCS, initMode);

                    AudioLevel31Index.Set(intVals[30], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel32Index.Set(intVals[31], Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbMCRUmd:

                    McrUmd.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbProductionUmd:

                    ProductionUmd01.Set(ReadTaggedValue(value, "umd1"), Enums.ChangeSources.BNCS, initMode);
                    ProductionUmd02.Set(ReadTaggedValue(value, "umd2"), Enums.ChangeSources.BNCS, initMode);
                    ProductionUmd03.Set(ReadTaggedValue(value, "umd3"), Enums.ChangeSources.BNCS, initMode);
                    ProductionUmd04.Set(ReadTaggedValue(value, "umd4"), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbSourceEventName:

                    EventName.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbSourceSpevList1:

                    spevdb[0] = value;
                    UpdateSpevList(Enums.ChangeSources.BNCS, initMode);
                    return true;

                case DbConstants.dbSourceSpevList2:

                    spevdb[1] = value;
                    UpdateSpevList(Enums.ChangeSources.BNCS, initMode);
                    return true;

                case DbConstants.dbSourceSpevList3:

                    spevdb[2] = value;
                    UpdateSpevList(Enums.ChangeSources.BNCS, initMode);
                    return true;

                case DbConstants.dbSourceSpevList4:

                    spevdb[3] = value;
                    UpdateSpevList(Enums.ChangeSources.BNCS, initMode);
                    return true;

                case DbConstants.dbSourceRecordId:

                    RecordId.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbSourceClassId:

                    ClassId.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbSourceTeams:


                    Team1.Set(ReadTaggedValue(value, "team_1"), Enums.ChangeSources.BNCS, initMode);
                    Team2.Set(ReadTaggedValue(value, "team_2"), Enums.ChangeSources.BNCS, initMode);

                    return true;
                   
                case DbConstants.dbSourceDates:

                    var strStartDate = ReadTaggedValue(value, "start_date");
                    var strStartTime = ReadTaggedValue(value, "start_time");
                    var strEndDate = ReadTaggedValue(value, "end_date");
                    var strEndTime = ReadTaggedValue(value, "end_time");

                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MinValue;

                    DateTime.TryParse(strStartDate, out startDate);
                    DateTime.TryParse(strEndDate, out endDate);

                    if (TimeSpan.TryParse(strStartTime, out var timeStart)) startDate = startDate.Add(timeStart);
                    if (TimeSpan.TryParse(strEndTime, out var timeEnd)) endDate = endDate.Add(timeEnd);

                    EventStart.Set(startDate, Enums.ChangeSources.BNCS, initMode);
                    EventEnd.Set(endDate, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbSourcePackageLocks:

                    EditLocked = !String.IsNullOrWhiteSpace(value);
                    return true;

                default:
                    return false;
            }
        }

        private void UpdateSpevList(Enums.ChangeSources changeSource, bool initMode)
        {

            string value = spevdb[0].Trim('+');
            if (!string.IsNullOrWhiteSpace(spevdb[1])) value = $"{value},{spevdb[1].Trim('+')}";
            if (!string.IsNullOrWhiteSpace(spevdb[2])) value = $"{value},{spevdb[2].Trim('+')}";
            if (!string.IsNullOrWhiteSpace(spevdb[3])) value = $"{value},{spevdb[3].Trim('+')}";

            SpevList.Set(value, Enums.ChangeSources.BNCS, initMode);

        }

        public ett_api_domain.SourcePackage GetDocument()
        {

            var result = new ett_api_domain.SourcePackage();

            result.Index = (int)Index;

            result.OwnerTag = new ett_api_domain.OwnerTag();
            result.OwnerTag.Index = NullableInt(OwnerTag.CurrentValue);
            result.OwnerTag.Name = Lookup(DbConstants.dbOwnerTags, OwnerTag.CurrentValue);

            result.ButtonName = ButtonName.CurrentValue;
            result.PackageName = PackageName.CurrentValue;
            result.PackageType = PackageType;

            result.SymbolicName = SymbolicName;
            result.McrNotes = McrNotes.CurrentValue;

            result.UmdLabels = new ett_api_domain.SourcePackageUmdLabels();
            result.UmdLabels.McrUmd = McrUmd.CurrentValue;
            result.UmdLabels.ProdUmd1 = ProductionUmd01.CurrentValue;
            result.UmdLabels.ProdUmd2 = ProductionUmd02.CurrentValue;
            result.UmdLabels.ProdUmd3 = ProductionUmd03.CurrentValue;
            result.UmdLabels.ProdUmd4 = ProductionUmd04.CurrentValue;

            result.VideoLevels = new ett_api_domain.SourcePackageVideoLevels();

            result.VideoLevels._1 = new ett_api_domain.SourcePackageVideoLevel();
            result.VideoLevels._1.Index = NullableInt(VideoLevel01VideoIndex.CurrentValue);
            result.VideoLevels._1.AudioPackageIndex = NullableInt(VideoLevel01AudioPackageIndex.CurrentValue);
            result.VideoLevels._1.VideoHubTag = new ett_api_domain.VideoHubTag();
            result.VideoLevels._1.VideoHubTag.Index = NullableInt(VideoLevel01VideoHubTagIndex.CurrentValue);
            result.VideoLevels._1.VideoHubTag.Name = Lookup(DbConstants.dbVideoHubTags, VideoLevel01VideoHubTagIndex.CurrentValue);

            result.VideoLevels._2 = new ett_api_domain.SourcePackageVideoLevel();
            result.VideoLevels._2.Index = NullableInt(VideoLevel02VideoIndex.CurrentValue);
            result.VideoLevels._2.AudioPackageIndex = NullableInt(VideoLevel02AudioPackageIndex.CurrentValue);
            result.VideoLevels._2.VideoHubTag = new ett_api_domain.VideoHubTag();
            result.VideoLevels._2.VideoHubTag.Index = NullableInt(VideoLevel02VideoHubTagIndex.CurrentValue);
            result.VideoLevels._2.VideoHubTag.Name = Lookup(DbConstants.dbVideoHubTags, VideoLevel02VideoHubTagIndex.CurrentValue);

            result.VideoLevels._3 = new ett_api_domain.SourcePackageVideoLevel();
            result.VideoLevels._3.Index = NullableInt(VideoLevel03VideoIndex.CurrentValue);
            result.VideoLevels._3.AudioPackageIndex = NullableInt(VideoLevel03AudioPackageIndex.CurrentValue);
            result.VideoLevels._3.VideoHubTag = new ett_api_domain.VideoHubTag();
            result.VideoLevels._3.VideoHubTag.Index = NullableInt(VideoLevel03VideoHubTagIndex.CurrentValue);
            result.VideoLevels._3.VideoHubTag.Name = Lookup(DbConstants.dbVideoHubTags, VideoLevel03VideoHubTagIndex.CurrentValue);

            result.VideoLevels._4 = new ett_api_domain.SourcePackageVideoLevel();
            result.VideoLevels._4.Index = NullableInt(VideoLevel04VideoIndex.CurrentValue);
            result.VideoLevels._4.AudioPackageIndex = NullableInt(VideoLevel04AudioPackageIndex.CurrentValue);
            result.VideoLevels._4.VideoHubTag = new ett_api_domain.VideoHubTag();
            result.VideoLevels._4.VideoHubTag.Index = NullableInt(VideoLevel04VideoHubTagIndex.CurrentValue);
            result.VideoLevels._4.VideoHubTag.Name = Lookup(DbConstants.dbVideoHubTags, VideoLevel04VideoHubTagIndex.CurrentValue);

            result.AudioPackageLevels = new ett_api_domain.SourcePackageAudioLevels();

            result.AudioPackageLevels._01 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._02 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._03 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._04 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._05 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._06 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._07 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._08 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._09 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._10 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._11 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._12 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._13 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._14 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._15 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._16 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._17 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._18 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._19 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._20 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._21 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._22 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._23 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._24 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._25 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._26 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._27 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._28 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._29 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._30 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._31 = new ett_api_domain.SourcePackageAudioLevel();
            result.AudioPackageLevels._32 = new ett_api_domain.SourcePackageAudioLevel();

            result.AudioPackageLevels._01.Index = NullableInt(AudioLevel01Index.CurrentValue);
            result.AudioPackageLevels._02.Index = NullableInt(AudioLevel02Index.CurrentValue);
            result.AudioPackageLevels._03.Index = NullableInt(AudioLevel03Index.CurrentValue);
            result.AudioPackageLevels._04.Index = NullableInt(AudioLevel04Index.CurrentValue);
            result.AudioPackageLevels._05.Index = NullableInt(AudioLevel05Index.CurrentValue);
            result.AudioPackageLevels._06.Index = NullableInt(AudioLevel06Index.CurrentValue);
            result.AudioPackageLevels._07.Index = NullableInt(AudioLevel07Index.CurrentValue);
            result.AudioPackageLevels._08.Index = NullableInt(AudioLevel08Index.CurrentValue);
            result.AudioPackageLevels._09.Index = NullableInt(AudioLevel09Index.CurrentValue);
            result.AudioPackageLevels._10.Index = NullableInt(AudioLevel10Index.CurrentValue);
            result.AudioPackageLevels._11.Index = NullableInt(AudioLevel11Index.CurrentValue);
            result.AudioPackageLevels._12.Index = NullableInt(AudioLevel12Index.CurrentValue);
            result.AudioPackageLevels._13.Index = NullableInt(AudioLevel13Index.CurrentValue);
            result.AudioPackageLevels._14.Index = NullableInt(AudioLevel14Index.CurrentValue);
            result.AudioPackageLevels._15.Index = NullableInt(AudioLevel15Index.CurrentValue);
            result.AudioPackageLevels._16.Index = NullableInt(AudioLevel16Index.CurrentValue);
            result.AudioPackageLevels._17.Index = NullableInt(AudioLevel17Index.CurrentValue);
            result.AudioPackageLevels._18.Index = NullableInt(AudioLevel18Index.CurrentValue);
            result.AudioPackageLevels._19.Index = NullableInt(AudioLevel19Index.CurrentValue);
            result.AudioPackageLevels._20.Index = NullableInt(AudioLevel20Index.CurrentValue);
            result.AudioPackageLevels._21.Index = NullableInt(AudioLevel21Index.CurrentValue);
            result.AudioPackageLevels._22.Index = NullableInt(AudioLevel22Index.CurrentValue);
            result.AudioPackageLevels._23.Index = NullableInt(AudioLevel23Index.CurrentValue);
            result.AudioPackageLevels._24.Index = NullableInt(AudioLevel24Index.CurrentValue);
            result.AudioPackageLevels._25.Index = NullableInt(AudioLevel25Index.CurrentValue);
            result.AudioPackageLevels._26.Index = NullableInt(AudioLevel26Index.CurrentValue);
            result.AudioPackageLevels._27.Index = NullableInt(AudioLevel27Index.CurrentValue);
            result.AudioPackageLevels._28.Index = NullableInt(AudioLevel28Index.CurrentValue);
            result.AudioPackageLevels._29.Index = NullableInt(AudioLevel29Index.CurrentValue);
            result.AudioPackageLevels._30.Index = NullableInt(AudioLevel30Index.CurrentValue);
            result.AudioPackageLevels._31.Index = NullableInt(AudioLevel31Index.CurrentValue);
            result.AudioPackageLevels._32.Index = NullableInt(AudioLevel32Index.CurrentValue);

            result.Metadata = new ett_api_domain.SourcePackageMetadata();
            result.Metadata.SpevList = new List<string>();
            if (SpevList.CurrentValue != null) result.Metadata.SpevList.AddRange(SpevList.CurrentValue.Split(','));

            result.Metadata.EventDate = EventStart.CurrentValue.Date;
            result.Metadata.StartTime = EventStart.CurrentValue.TimeOfDay.ToString("c");
            result.Metadata.EndTime = EventEnd.CurrentValue.TimeOfDay.ToString("c");

            result.Metadata.ClassificationId = ClassId.CurrentValue;
            result.Metadata.RecordId = RecordId.CurrentValue;

            return result;

        }

        public void SetDocument(ett_api_domain.SourcePackage package)
        {

            if (package == null) throw new ArgumentNullException();
            if (package.Index != Index) throw new Exception("Index mismatch");

            PackageName.Change(package.PackageName, Enums.ChangeSources.API);
            ButtonName.Change(package.ButtonName, Enums.ChangeSources.API);

            if (package.OwnerTag != null) OwnerTag.Change(NullableInt(package.OwnerTag.Index), Enums.ChangeSources.API);
            else OwnerTag.Change(-1, Enums.ChangeSources.API);

            McrNotes.Change(package.McrNotes, Enums.ChangeSources.API);

            if (package.UmdLabels != null)
            {
                McrUmd.Change(package.UmdLabels.McrUmd, Enums.ChangeSources.API);
                ProductionUmd01.Change(package.UmdLabels.ProdUmd1, Enums.ChangeSources.API);
                ProductionUmd02.Change(package.UmdLabels.ProdUmd2, Enums.ChangeSources.API);
                ProductionUmd03.Change(package.UmdLabels.ProdUmd3, Enums.ChangeSources.API);
                ProductionUmd04.Change(package.UmdLabels.ProdUmd4, Enums.ChangeSources.API);
            }
            else
            {
                McrUmd.Change(null, Enums.ChangeSources.API);
                ProductionUmd01.Change(null, Enums.ChangeSources.API);
                ProductionUmd02.Change(null, Enums.ChangeSources.API);
                ProductionUmd03.Change(null, Enums.ChangeSources.API);
                ProductionUmd04.Change(null, Enums.ChangeSources.API);
            }

            if (package.VideoLevels != null)
            {

                if (package.VideoLevels._1 != null)
                {

                    if (package.VideoLevels._1.Index != null) VideoLevel01VideoIndex.Change(NullableInt(package.VideoLevels._1.Index), Enums.ChangeSources.API);
                    else VideoLevel01VideoIndex.Change(-1, Enums.ChangeSources.API);

                    if (package.VideoLevels._1.AudioPackageIndex != null) VideoLevel01AudioPackageIndex.Change(NullableInt(package.VideoLevels._1.AudioPackageIndex), Enums.ChangeSources.API);
                    else VideoLevel01AudioPackageIndex.Change(-1, Enums.ChangeSources.API);

                    if (package.VideoLevels._1.VideoHubTag != null) VideoLevel01VideoHubTagIndex.Change(NullableInt(package.VideoLevels._1.VideoHubTag.Index), Enums.ChangeSources.API);
                    else VideoLevel01VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    VideoLevel01VideoIndex.Change(-1, Enums.ChangeSources.API);
                    VideoLevel01AudioPackageIndex.Change(-1, Enums.ChangeSources.API);
                    VideoLevel01VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);
                }

                if (package.VideoLevels._2 != null)
                {

                    if (package.VideoLevels._2.Index != null) VideoLevel02VideoIndex.Change(NullableInt(package.VideoLevels._2.Index), Enums.ChangeSources.API);
                    else VideoLevel02VideoIndex.Change(-1, Enums.ChangeSources.API);

                    if (package.VideoLevels._2.AudioPackageIndex != null) VideoLevel02AudioPackageIndex.Change(NullableInt(package.VideoLevels._2.AudioPackageIndex), Enums.ChangeSources.API);
                    else VideoLevel02AudioPackageIndex.Change(-1, Enums.ChangeSources.API);

                    if (package.VideoLevels._2.VideoHubTag != null) VideoLevel02VideoHubTagIndex.Change(NullableInt(package.VideoLevels._2.VideoHubTag.Index), Enums.ChangeSources.API);
                    else VideoLevel02VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    VideoLevel02VideoIndex.Change(-1, Enums.ChangeSources.API);
                    VideoLevel02AudioPackageIndex.Change(-1, Enums.ChangeSources.API);
                    VideoLevel02VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);
                }

                if (package.VideoLevels._3 != null)
                {

                    if (package.VideoLevels._3.Index != null) VideoLevel03VideoIndex.Change(NullableInt(package.VideoLevels._3.Index), Enums.ChangeSources.API);
                    else VideoLevel03VideoIndex.Change(-1, Enums.ChangeSources.API);

                    if (package.VideoLevels._3.AudioPackageIndex != null) VideoLevel03AudioPackageIndex.Change(NullableInt(package.VideoLevels._3.AudioPackageIndex), Enums.ChangeSources.API);
                    else VideoLevel03AudioPackageIndex.Change(-1, Enums.ChangeSources.API);

                    if (package.VideoLevels._3.VideoHubTag != null) VideoLevel03VideoHubTagIndex.Change(NullableInt(package.VideoLevels._3.VideoHubTag.Index), Enums.ChangeSources.API);
                    else VideoLevel03VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    VideoLevel03VideoIndex.Change(-1, Enums.ChangeSources.API);
                    VideoLevel03AudioPackageIndex.Change(-1, Enums.ChangeSources.API);
                    VideoLevel03VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);
                }

                if (package.VideoLevels._4 != null)
                {

                    if (package.VideoLevels._4.Index != null) VideoLevel04VideoIndex.Change(NullableInt(package.VideoLevels._4.Index), Enums.ChangeSources.API);
                    else VideoLevel04VideoIndex.Change(-1, Enums.ChangeSources.API);

                    if (package.VideoLevels._4.AudioPackageIndex != null) VideoLevel04AudioPackageIndex.Change(NullableInt(package.VideoLevels._4.AudioPackageIndex), Enums.ChangeSources.API);
                    else VideoLevel04AudioPackageIndex.Change(-1, Enums.ChangeSources.API);

                    if (package.VideoLevels._4.VideoHubTag != null) VideoLevel04VideoHubTagIndex.Change(NullableInt(package.VideoLevels._4.VideoHubTag.Index), Enums.ChangeSources.API);
                    else VideoLevel04VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    VideoLevel04VideoIndex.Change(-1, Enums.ChangeSources.API);
                    VideoLevel04AudioPackageIndex.Change(-1, Enums.ChangeSources.API);
                    VideoLevel04VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);
                }

            }
            else
            {
                VideoLevel01VideoIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel01AudioPackageIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel01VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel02VideoIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel02AudioPackageIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel02VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel03VideoIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel03AudioPackageIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel03VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel04VideoIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel04AudioPackageIndex.Change(-1, Enums.ChangeSources.API);
                VideoLevel04VideoHubTagIndex.Change(-1, Enums.ChangeSources.API);
            }

            if (package.AudioPackageLevels != null)
            {

                if (package.AudioPackageLevels._01 != null) AudioLevel01Index.Change(NullableInt(package.AudioPackageLevels._01.Index), Enums.ChangeSources.API);
                else AudioLevel01Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._02 != null) AudioLevel02Index.Change(NullableInt(package.AudioPackageLevels._02.Index), Enums.ChangeSources.API);
                else AudioLevel02Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._03 != null) AudioLevel03Index.Change(NullableInt(package.AudioPackageLevels._03.Index), Enums.ChangeSources.API);
                else AudioLevel03Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._04 != null) AudioLevel04Index.Change(NullableInt(package.AudioPackageLevels._04.Index), Enums.ChangeSources.API);
                else AudioLevel04Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._05 != null) AudioLevel05Index.Change(NullableInt(package.AudioPackageLevels._05.Index), Enums.ChangeSources.API);
                else AudioLevel05Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._06 != null) AudioLevel06Index.Change(NullableInt(package.AudioPackageLevels._06.Index), Enums.ChangeSources.API);
                else AudioLevel06Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._07 != null) AudioLevel07Index.Change(NullableInt(package.AudioPackageLevels._07.Index), Enums.ChangeSources.API);
                else AudioLevel07Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._08 != null) AudioLevel08Index.Change(NullableInt(package.AudioPackageLevels._08.Index), Enums.ChangeSources.API);
                else AudioLevel08Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._09 != null) AudioLevel09Index.Change(NullableInt(package.AudioPackageLevels._09.Index), Enums.ChangeSources.API);
                else AudioLevel09Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._10 != null) AudioLevel10Index.Change(NullableInt(package.AudioPackageLevels._10.Index), Enums.ChangeSources.API);
                else AudioLevel10Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._11 != null) AudioLevel11Index.Change(NullableInt(package.AudioPackageLevels._11.Index), Enums.ChangeSources.API);
                else AudioLevel11Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._12 != null) AudioLevel12Index.Change(NullableInt(package.AudioPackageLevels._12.Index), Enums.ChangeSources.API);
                else AudioLevel12Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._13 != null) AudioLevel13Index.Change(NullableInt(package.AudioPackageLevels._13.Index), Enums.ChangeSources.API);
                else AudioLevel13Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._14 != null) AudioLevel14Index.Change(NullableInt(package.AudioPackageLevels._14.Index), Enums.ChangeSources.API);
                else AudioLevel14Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._15 != null) AudioLevel15Index.Change(NullableInt(package.AudioPackageLevels._15.Index), Enums.ChangeSources.API);
                else AudioLevel15Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._16 != null) AudioLevel16Index.Change(NullableInt(package.AudioPackageLevels._16.Index), Enums.ChangeSources.API);
                else AudioLevel16Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._17 != null) AudioLevel17Index.Change(NullableInt(package.AudioPackageLevels._17.Index), Enums.ChangeSources.API);
                else AudioLevel17Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._18 != null) AudioLevel18Index.Change(NullableInt(package.AudioPackageLevels._18.Index), Enums.ChangeSources.API);
                else AudioLevel18Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._19 != null) AudioLevel19Index.Change(NullableInt(package.AudioPackageLevels._19.Index), Enums.ChangeSources.API);
                else AudioLevel19Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._20 != null) AudioLevel20Index.Change(NullableInt(package.AudioPackageLevels._20.Index), Enums.ChangeSources.API);
                else AudioLevel20Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._21 != null) AudioLevel21Index.Change(NullableInt(package.AudioPackageLevels._21.Index), Enums.ChangeSources.API);
                else AudioLevel21Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._22 != null) AudioLevel22Index.Change(NullableInt(package.AudioPackageLevels._22.Index), Enums.ChangeSources.API);
                else AudioLevel22Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._23 != null) AudioLevel23Index.Change(NullableInt(package.AudioPackageLevels._23.Index), Enums.ChangeSources.API);
                else AudioLevel23Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._24 != null) AudioLevel24Index.Change(NullableInt(package.AudioPackageLevels._24.Index), Enums.ChangeSources.API);
                else AudioLevel24Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._25 != null) AudioLevel25Index.Change(NullableInt(package.AudioPackageLevels._25.Index), Enums.ChangeSources.API);
                else AudioLevel25Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._26 != null) AudioLevel26Index.Change(NullableInt(package.AudioPackageLevels._26.Index), Enums.ChangeSources.API);
                else AudioLevel26Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._27 != null) AudioLevel27Index.Change(NullableInt(package.AudioPackageLevels._27.Index), Enums.ChangeSources.API);
                else AudioLevel27Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._28 != null) AudioLevel28Index.Change(NullableInt(package.AudioPackageLevels._28.Index), Enums.ChangeSources.API);
                else AudioLevel28Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._29 != null) AudioLevel29Index.Change(NullableInt(package.AudioPackageLevels._29.Index), Enums.ChangeSources.API);
                else AudioLevel29Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._30 != null) AudioLevel30Index.Change(NullableInt(package.AudioPackageLevels._30.Index), Enums.ChangeSources.API);
                else AudioLevel30Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._31 != null) AudioLevel31Index.Change(NullableInt(package.AudioPackageLevels._31.Index), Enums.ChangeSources.API);
                else AudioLevel31Index.Change(-1, Enums.ChangeSources.API);

                if (package.AudioPackageLevels._32 != null) AudioLevel32Index.Change(NullableInt(package.AudioPackageLevels._32.Index), Enums.ChangeSources.API);
                else AudioLevel32Index.Change(-1, Enums.ChangeSources.API);

            }
            else
            {
                AudioLevel01Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel02Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel03Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel04Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel05Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel06Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel07Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel08Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel09Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel10Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel11Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel12Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel13Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel14Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel15Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel16Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel17Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel18Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel19Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel20Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel21Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel22Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel23Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel24Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel25Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel26Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel27Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel28Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel29Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel30Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel31Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel32Index.Change(-1, Enums.ChangeSources.API);
            }

            if (package.Metadata != null)
            {

                if (package.Metadata.SpevList != null) SpevList.Change(String.Join(",", package.Metadata.SpevList), Enums.ChangeSources.API);
                else SpevList.Clear(Enums.ChangeSources.API);

                ClassId.Change(package.Metadata.ClassificationId, Enums.ChangeSources.API);
                RecordId.Change(package.Metadata.RecordId, Enums.ChangeSources.API);

                var startTime = DateTime.MinValue;
                TimeSpan.TryParse(package.Metadata.StartTime, out var st);

                if (package.Metadata.EventDate != null) startTime = package.Metadata.EventDate.Value.Date;
                if (st != null) startTime = startTime.Add(st);

                EventStart.Change(startTime, Enums.ChangeSources.API);

                var endTime = DateTime.MaxValue;
                TimeSpan.TryParse(package.Metadata.EndTime, out var et);

                if (et != null) endTime = startTime.Date.Add(et);

                EventEnd.Change(endTime, Enums.ChangeSources.API);

            }
            else
            {
                EventName.Clear(Enums.ChangeSources.API);
                SpevList.Clear(Enums.ChangeSources.API);
                ClassId.Clear(Enums.ChangeSources.API);
                RecordId.Clear(Enums.ChangeSources.API);
                EventStart.Clear(Enums.ChangeSources.API);
                EventEnd.Clear(Enums.ChangeSources.API);
            }

        }

        public string SymbolicName
        {
            get
            {
                return $"{FunctionalArea}.{FunctionalName}";
            }
        }

    }
}
