﻿using Gibraltar.Agent;
using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.packages
{
    public class DestinationPackage : BasePackage
    {

        const string STR_LoggingCategory = "BNCS_ETT_API.Packages.DestinationPackage";

        /// <summary>Initializes a new instance of the <a onclick="return false;" href="T:DestinationPackage" originaltag="see">T:DestinationPackage</a> class.</summary>
        /// <param name="Index">The BNCS index of the package</param>
        public DestinationPackage(uint Index) : base(Index)
        {
            PackageName.AffectedDb = (int)DbConstants.dbDestPackageTitle;
            ButtonName.AffectedDb = (int)DbConstants.dbDestButtonName;
            OwnerTag.AffectedDb = (int)DbConstants.dbDestPackageTags;
            DelegateTag.AffectedDb = (int)DbConstants.dbDestPackageTags;
            UniqueId.AffectedDb = (int)DbConstants.dbDestPackageTags;
        }

        public override string PackageType => "destination";

        public bool RouteLocked { get; protected set; }

        public string FunctionalArea { get; private set; }
        public string FunctionalName { get; private set; }
        public TrackedParameter<bool> LockStatus { get => lockStatus; set => lockStatus = value; }
        public TrackedParameter<int> VideoLevel01Index { get => videoLevel01Index; set => videoLevel01Index = value; }
        public TrackedParameter<int> VideoLevel01HubTag { get => videoLevel01HubTag; set => videoLevel01HubTag = value; }
        public TrackedParameter<int> AudioLevel01Index { get => audioLevel01Index; set => audioLevel01Index = value; }
        public TrackedParameter<int> AudioLevel01LangTag { get => audioLevel01LangTag; set => audioLevel01LangTag = value; }
        public TrackedParameter<int> AudioLevel01TypeTag { get => audioLevel01TypeTag; set => audioLevel01TypeTag = value; }
        public TrackedParameter<int> AudioLevel01LangTagSub1 { get => audioLevel01LangTagSub1; set => audioLevel01LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel01TypeTagSub1 { get => audioLevel01TypeTagSub1; set => audioLevel01TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel01LangTagSub2 { get => audioLevel01LangTagSub2; set => audioLevel01LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel01TypeTagSub2 { get => audioLevel01TypeTagSub2; set => audioLevel01TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel02Index { get => audioLevel02Index; set => audioLevel02Index = value; }
        public TrackedParameter<int> AudioLevel02LangTag { get => audioLevel02LangTag; set => audioLevel02LangTag = value; }
        public TrackedParameter<int> AudioLevel02TypeTag { get => audioLevel02TypeTag; set => audioLevel02TypeTag = value; }
        public TrackedParameter<int> AudioLevel02LangTagSub1 { get => audioLevel02LangTagSub1; set => audioLevel02LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel02TypeTagSub1 { get => audioLevel02TypeTagSub1; set => audioLevel02TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel02LangTagSub2 { get => audioLevel02LangTagSub2; set => audioLevel02LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel02TypeTagSub2 { get => audioLevel02TypeTagSub2; set => audioLevel02TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel03Index { get => audioLevel03Index; set => audioLevel03Index = value; }
        public TrackedParameter<int> AudioLevel03LangTag { get => audioLevel03LangTag; set => audioLevel03LangTag = value; }
        public TrackedParameter<int> AudioLevel03TypeTag { get => audioLevel03TypeTag; set => audioLevel03TypeTag = value; }
        public TrackedParameter<int> AudioLevel03LangTagSub1 { get => audioLevel03LangTagSub1; set => audioLevel03LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel03TypeTagSub1 { get => audioLevel03TypeTagSub1; set => audioLevel03TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel03LangTagSub2 { get => audioLevel03LangTagSub2; set => audioLevel03LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel03TypeTagSub2 { get => audioLevel03TypeTagSub2; set => audioLevel03TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel04Index { get => audioLevel04Index; set => audioLevel04Index = value; }
        public TrackedParameter<int> AudioLevel04LangTag { get => audioLevel04LangTag; set => audioLevel04LangTag = value; }
        public TrackedParameter<int> AudioLevel04TypeTag { get => audioLevel04TypeTag; set => audioLevel04TypeTag = value; }
        public TrackedParameter<int> AudioLevel04LangTagSub1 { get => audioLevel04LangTagSub1; set => audioLevel04LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel04TypeTagSub1 { get => audioLevel04TypeTagSub1; set => audioLevel04TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel04LangTagSub2 { get => audioLevel04LangTagSub2; set => audioLevel04LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel04TypeTagSub2 { get => audioLevel04TypeTagSub2; set => audioLevel04TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel05Index { get => audioLevel05Index; set => audioLevel05Index = value; }
        public TrackedParameter<int> AudioLevel05LangTag { get => audioLevel05LangTag; set => audioLevel05LangTag = value; }
        public TrackedParameter<int> AudioLevel05TypeTag { get => audioLevel05TypeTag; set => audioLevel05TypeTag = value; }
        public TrackedParameter<int> AudioLevel05LangTagSub1 { get => audioLevel05LangTagSub1; set => audioLevel05LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel05TypeTagSub1 { get => audioLevel05TypeTagSub1; set => audioLevel05TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel05LangTagSub2 { get => audioLevel05LangTagSub2; set => audioLevel05LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel05TypeTagSub2 { get => audioLevel05TypeTagSub2; set => audioLevel05TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel06Index { get => audioLevel06Index; set => audioLevel06Index = value; }
        public TrackedParameter<int> AudioLevel06LangTag { get => audioLevel06LangTag; set => audioLevel06LangTag = value; }
        public TrackedParameter<int> AudioLevel06TypeTag { get => audioLevel06TypeTag; set => audioLevel06TypeTag = value; }
        public TrackedParameter<int> AudioLevel06LangTagSub1 { get => audioLevel06LangTagSub1; set => audioLevel06LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel06TypeTagSub1 { get => audioLevel06TypeTagSub1; set => audioLevel06TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel06LangTagSub2 { get => audioLevel06LangTagSub2; set => audioLevel06LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel06TypeTagSub2 { get => audioLevel06TypeTagSub2; set => audioLevel06TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel07Index { get => audioLevel07Index; set => audioLevel07Index = value; }
        public TrackedParameter<int> AudioLevel07LangTag { get => audioLevel07LangTag; set => audioLevel07LangTag = value; }
        public TrackedParameter<int> AudioLevel07TypeTag { get => audioLevel07TypeTag; set => audioLevel07TypeTag = value; }
        public TrackedParameter<int> AudioLevel07LangTagSub1 { get => audioLevel07LangTagSub1; set => audioLevel07LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel07TypeTagSub1 { get => audioLevel07TypeTagSub1; set => audioLevel07TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel07LangTagSub2 { get => audioLevel07LangTagSub2; set => audioLevel07LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel07TypeTagSub2 { get => audioLevel07TypeTagSub2; set => audioLevel07TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel08Index { get => audioLevel08Index; set => audioLevel08Index = value; }
        public TrackedParameter<int> AudioLevel08LangTag { get => audioLevel08LangTag; set => audioLevel08LangTag = value; }
        public TrackedParameter<int> AudioLevel08TypeTag { get => audioLevel08TypeTag; set => audioLevel08TypeTag = value; }
        public TrackedParameter<int> AudioLevel08LangTagSub1 { get => audioLevel08LangTagSub1; set => audioLevel08LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel08TypeTagSub1 { get => audioLevel08TypeTagSub1; set => audioLevel08TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel08LangTagSub2 { get => audioLevel08LangTagSub2; set => audioLevel08LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel08TypeTagSub2 { get => audioLevel08TypeTagSub2; set => audioLevel08TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel09Index { get => audioLevel09Index; set => audioLevel09Index = value; }
        public TrackedParameter<int> AudioLevel09LangTag { get => audioLevel09LangTag; set => audioLevel09LangTag = value; }
        public TrackedParameter<int> AudioLevel09TypeTag { get => audioLevel09TypeTag; set => audioLevel09TypeTag = value; }
        public TrackedParameter<int> AudioLevel09LangTagSub1 { get => audioLevel09LangTagSub1; set => audioLevel09LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel09TypeTagSub1 { get => audioLevel09TypeTagSub1; set => audioLevel09TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel09LangTagSub2 { get => audioLevel09LangTagSub2; set => audioLevel09LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel09TypeTagSub2 { get => audioLevel09TypeTagSub2; set => audioLevel09TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel10Index { get => audioLevel10Index; set => audioLevel10Index = value; }
        public TrackedParameter<int> AudioLevel10LangTag { get => audioLevel10LangTag; set => audioLevel10LangTag = value; }
        public TrackedParameter<int> AudioLevel10TypeTag { get => audioLevel10TypeTag; set => audioLevel10TypeTag = value; }
        public TrackedParameter<int> AudioLevel10LangTagSub1 { get => audioLevel10LangTagSub1; set => audioLevel10LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel10TypeTagSub1 { get => audioLevel10TypeTagSub1; set => audioLevel10TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel10LangTagSub2 { get => audioLevel10LangTagSub2; set => audioLevel10LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel10TypeTagSub2 { get => audioLevel10TypeTagSub2; set => audioLevel10TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel11Index { get => audioLevel11Index; set => audioLevel11Index = value; }
        public TrackedParameter<int> AudioLevel11LangTag { get => audioLevel11LangTag; set => audioLevel11LangTag = value; }
        public TrackedParameter<int> AudioLevel11TypeTag { get => audioLevel11TypeTag; set => audioLevel11TypeTag = value; }
        public TrackedParameter<int> AudioLevel11LangTagSub1 { get => audioLevel11LangTagSub1; set => audioLevel11LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel11TypeTagSub1 { get => audioLevel11TypeTagSub1; set => audioLevel11TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel11LangTagSub2 { get => audioLevel11LangTagSub2; set => audioLevel11LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel11TypeTagSub2 { get => audioLevel11TypeTagSub2; set => audioLevel11TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel12Index { get => audioLevel12Index; set => audioLevel12Index = value; }
        public TrackedParameter<int> AudioLevel12LangTag { get => audioLevel12LangTag; set => audioLevel12LangTag = value; }
        public TrackedParameter<int> AudioLevel12TypeTag { get => audioLevel12TypeTag; set => audioLevel12TypeTag = value; }
        public TrackedParameter<int> AudioLevel12LangTagSub1 { get => audioLevel12LangTagSub1; set => audioLevel12LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel12TypeTagSub1 { get => audioLevel12TypeTagSub1; set => audioLevel12TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel12LangTagSub2 { get => audioLevel12LangTagSub2; set => audioLevel12LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel12TypeTagSub2 { get => audioLevel12TypeTagSub2; set => audioLevel12TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel13Index { get => audioLevel13Index; set => audioLevel13Index = value; }
        public TrackedParameter<int> AudioLevel13LangTag { get => audioLevel13LangTag; set => audioLevel13LangTag = value; }
        public TrackedParameter<int> AudioLevel13TypeTag { get => audioLevel13TypeTag; set => audioLevel13TypeTag = value; }
        public TrackedParameter<int> AudioLevel13LangTagSub1 { get => audioLevel13LangTagSub1; set => audioLevel13LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel13TypeTagSub1 { get => audioLevel13TypeTagSub1; set => audioLevel13TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel13LangTagSub2 { get => audioLevel13LangTagSub2; set => audioLevel13LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel13TypeTagSub2 { get => audioLevel13TypeTagSub2; set => audioLevel13TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel14Index { get => audioLevel14Index; set => audioLevel14Index = value; }
        public TrackedParameter<int> AudioLevel14LangTag { get => audioLevel14LangTag; set => audioLevel14LangTag = value; }
        public TrackedParameter<int> AudioLevel14TypeTag { get => audioLevel14TypeTag; set => audioLevel14TypeTag = value; }
        public TrackedParameter<int> AudioLevel14LangTagSub1 { get => audioLevel14LangTagSub1; set => audioLevel14LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel14TypeTagSub1 { get => audioLevel14TypeTagSub1; set => audioLevel14TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel14LangTagSub2 { get => audioLevel14LangTagSub2; set => audioLevel14LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel14TypeTagSub2 { get => audioLevel14TypeTagSub2; set => audioLevel14TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel15Index { get => audioLevel15Index; set => audioLevel15Index = value; }
        public TrackedParameter<int> AudioLevel15LangTag { get => audioLevel15LangTag; set => audioLevel15LangTag = value; }
        public TrackedParameter<int> AudioLevel15TypeTag { get => audioLevel15TypeTag; set => audioLevel15TypeTag = value; }
        public TrackedParameter<int> AudioLevel15LangTagSub1 { get => audioLevel15LangTagSub1; set => audioLevel15LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel15TypeTagSub1 { get => audioLevel15TypeTagSub1; set => audioLevel15TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel15LangTagSub2 { get => audioLevel15LangTagSub2; set => audioLevel15LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel15TypeTagSub2 { get => audioLevel15TypeTagSub2; set => audioLevel15TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel16Index { get => audioLevel16Index; set => audioLevel16Index = value; }
        public TrackedParameter<int> AudioLevel16LangTag { get => audioLevel16LangTag; set => audioLevel16LangTag = value; }
        public TrackedParameter<int> AudioLevel16TypeTag { get => audioLevel16TypeTag; set => audioLevel16TypeTag = value; }
        public TrackedParameter<int> AudioLevel16LangTagSub1 { get => audioLevel16LangTagSub1; set => audioLevel16LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel16TypeTagSub1 { get => audioLevel16TypeTagSub1; set => audioLevel16TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel16LangTagSub2 { get => audioLevel16LangTagSub2; set => audioLevel16LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel16TypeTagSub2 { get => audioLevel16TypeTagSub2; set => audioLevel16TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel17Index { get => audioLevel17Index; set => audioLevel17Index = value; }
        public TrackedParameter<int> AudioLevel17LangTag { get => audioLevel17LangTag; set => audioLevel17LangTag = value; }
        public TrackedParameter<int> AudioLevel17TypeTag { get => audioLevel17TypeTag; set => audioLevel17TypeTag = value; }
        public TrackedParameter<int> AudioLevel17LangTagSub1 { get => audioLevel17LangTagSub1; set => audioLevel17LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel17TypeTagSub1 { get => audioLevel17TypeTagSub1; set => audioLevel17TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel17LangTagSub2 { get => audioLevel17LangTagSub2; set => audioLevel17LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel17TypeTagSub2 { get => audioLevel17TypeTagSub2; set => audioLevel17TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel18Index { get => audioLevel18Index; set => audioLevel18Index = value; }
        public TrackedParameter<int> AudioLevel18LangTag { get => audioLevel18LangTag; set => audioLevel18LangTag = value; }
        public TrackedParameter<int> AudioLevel18TypeTag { get => audioLevel18TypeTag; set => audioLevel18TypeTag = value; }
        public TrackedParameter<int> AudioLevel18LangTagSub1 { get => audioLevel18LangTagSub1; set => audioLevel18LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel18TypeTagSub1 { get => audioLevel18TypeTagSub1; set => audioLevel18TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel18LangTagSub2 { get => audioLevel18LangTagSub2; set => audioLevel18LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel18TypeTagSub2 { get => audioLevel18TypeTagSub2; set => audioLevel18TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel19Index { get => audioLevel19Index; set => audioLevel19Index = value; }
        public TrackedParameter<int> AudioLevel19LangTag { get => audioLevel19LangTag; set => audioLevel19LangTag = value; }
        public TrackedParameter<int> AudioLevel19TypeTag { get => audioLevel19TypeTag; set => audioLevel19TypeTag = value; }
        public TrackedParameter<int> AudioLevel19LangTagSub1 { get => audioLevel19LangTagSub1; set => audioLevel19LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel19TypeTagSub1 { get => audioLevel19TypeTagSub1; set => audioLevel19TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel19LangTagSub2 { get => audioLevel19LangTagSub2; set => audioLevel19LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel19TypeTagSub2 { get => audioLevel19TypeTagSub2; set => audioLevel19TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel20Index { get => audioLevel20Index; set => audioLevel20Index = value; }
        public TrackedParameter<int> AudioLevel20LangTag { get => audioLevel20LangTag; set => audioLevel20LangTag = value; }
        public TrackedParameter<int> AudioLevel20TypeTag { get => audioLevel20TypeTag; set => audioLevel20TypeTag = value; }
        public TrackedParameter<int> AudioLevel20LangTagSub1 { get => audioLevel20LangTagSub1; set => audioLevel20LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel20TypeTagSub1 { get => audioLevel20TypeTagSub1; set => audioLevel20TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel20LangTagSub2 { get => audioLevel20LangTagSub2; set => audioLevel20LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel20TypeTagSub2 { get => audioLevel20TypeTagSub2; set => audioLevel20TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel21Index { get => audioLevel21Index; set => audioLevel21Index = value; }
        public TrackedParameter<int> AudioLevel21LangTag { get => audioLevel21LangTag; set => audioLevel21LangTag = value; }
        public TrackedParameter<int> AudioLevel21TypeTag { get => audioLevel21TypeTag; set => audioLevel21TypeTag = value; }
        public TrackedParameter<int> AudioLevel21LangTagSub1 { get => audioLevel21LangTagSub1; set => audioLevel21LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel21TypeTagSub1 { get => audioLevel21TypeTagSub1; set => audioLevel21TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel21LangTagSub2 { get => audioLevel21LangTagSub2; set => audioLevel21LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel21TypeTagSub2 { get => audioLevel21TypeTagSub2; set => audioLevel21TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel22Index { get => audioLevel22Index; set => audioLevel22Index = value; }
        public TrackedParameter<int> AudioLevel22LangTag { get => audioLevel22LangTag; set => audioLevel22LangTag = value; }
        public TrackedParameter<int> AudioLevel22TypeTag { get => audioLevel22TypeTag; set => audioLevel22TypeTag = value; }
        public TrackedParameter<int> AudioLevel22LangTagSub1 { get => audioLevel22LangTagSub1; set => audioLevel22LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel22TypeTagSub1 { get => audioLevel22TypeTagSub1; set => audioLevel22TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel22LangTagSub2 { get => audioLevel22LangTagSub2; set => audioLevel22LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel22TypeTagSub2 { get => audioLevel22TypeTagSub2; set => audioLevel22TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel23Index { get => audioLevel23Index; set => audioLevel23Index = value; }
        public TrackedParameter<int> AudioLevel23LangTag { get => audioLevel23LangTag; set => audioLevel23LangTag = value; }
        public TrackedParameter<int> AudioLevel23TypeTag { get => audioLevel23TypeTag; set => audioLevel23TypeTag = value; }
        public TrackedParameter<int> AudioLevel23LangTagSub1 { get => audioLevel23LangTagSub1; set => audioLevel23LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel23TypeTagSub1 { get => audioLevel23TypeTagSub1; set => audioLevel23TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel23LangTagSub2 { get => audioLevel23LangTagSub2; set => audioLevel23LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel23TypeTagSub2 { get => audioLevel23TypeTagSub2; set => audioLevel23TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel24Index { get => audioLevel24Index; set => audioLevel24Index = value; }
        public TrackedParameter<int> AudioLevel24LangTag { get => audioLevel24LangTag; set => audioLevel24LangTag = value; }
        public TrackedParameter<int> AudioLevel24TypeTag { get => audioLevel24TypeTag; set => audioLevel24TypeTag = value; }
        public TrackedParameter<int> AudioLevel24LangTagSub1 { get => audioLevel24LangTagSub1; set => audioLevel24LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel24TypeTagSub1 { get => audioLevel24TypeTagSub1; set => audioLevel24TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel24LangTagSub2 { get => audioLevel24LangTagSub2; set => audioLevel24LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel24TypeTagSub2 { get => audioLevel24TypeTagSub2; set => audioLevel24TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel25Index { get => audioLevel25Index; set => audioLevel25Index = value; }
        public TrackedParameter<int> AudioLevel25LangTag { get => audioLevel25LangTag; set => audioLevel25LangTag = value; }
        public TrackedParameter<int> AudioLevel25TypeTag { get => audioLevel25TypeTag; set => audioLevel25TypeTag = value; }
        public TrackedParameter<int> AudioLevel25LangTagSub1 { get => audioLevel25LangTagSub1; set => audioLevel25LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel25TypeTagSub1 { get => audioLevel25TypeTagSub1; set => audioLevel25TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel25LangTagSub2 { get => audioLevel25LangTagSub2; set => audioLevel25LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel25TypeTagSub2 { get => audioLevel25TypeTagSub2; set => audioLevel25TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel26Index { get => audioLevel26Index; set => audioLevel26Index = value; }
        public TrackedParameter<int> AudioLevel26LangTag { get => audioLevel26LangTag; set => audioLevel26LangTag = value; }
        public TrackedParameter<int> AudioLevel26TypeTag { get => audioLevel26TypeTag; set => audioLevel26TypeTag = value; }
        public TrackedParameter<int> AudioLevel26LangTagSub1 { get => audioLevel26LangTagSub1; set => audioLevel26LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel26TypeTagSub1 { get => audioLevel26TypeTagSub1; set => audioLevel26TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel26LangTagSub2 { get => audioLevel26LangTagSub2; set => audioLevel26LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel26TypeTagSub2 { get => audioLevel26TypeTagSub2; set => audioLevel26TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel27Index { get => audioLevel27Index; set => audioLevel27Index = value; }
        public TrackedParameter<int> AudioLevel27LangTag { get => audioLevel27LangTag; set => audioLevel27LangTag = value; }
        public TrackedParameter<int> AudioLevel27TypeTag { get => audioLevel27TypeTag; set => audioLevel27TypeTag = value; }
        public TrackedParameter<int> AudioLevel27LangTagSub1 { get => audioLevel27LangTagSub1; set => audioLevel27LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel27TypeTagSub1 { get => audioLevel27TypeTagSub1; set => audioLevel27TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel27LangTagSub2 { get => audioLevel27LangTagSub2; set => audioLevel27LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel27TypeTagSub2 { get => audioLevel27TypeTagSub2; set => audioLevel27TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel28Index { get => audioLevel28Index; set => audioLevel28Index = value; }
        public TrackedParameter<int> AudioLevel28LangTag { get => audioLevel28LangTag; set => audioLevel28LangTag = value; }
        public TrackedParameter<int> AudioLevel28TypeTag { get => audioLevel28TypeTag; set => audioLevel28TypeTag = value; }
        public TrackedParameter<int> AudioLevel28LangTagSub1 { get => audioLevel28LangTagSub1; set => audioLevel28LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel28TypeTagSub1 { get => audioLevel28TypeTagSub1; set => audioLevel28TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel28LangTagSub2 { get => audioLevel28LangTagSub2; set => audioLevel28LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel28TypeTagSub2 { get => audioLevel28TypeTagSub2; set => audioLevel28TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel29Index { get => audioLevel29Index; set => audioLevel29Index = value; }
        public TrackedParameter<int> AudioLevel29LangTag { get => audioLevel29LangTag; set => audioLevel29LangTag = value; }
        public TrackedParameter<int> AudioLevel29TypeTag { get => audioLevel29TypeTag; set => audioLevel29TypeTag = value; }
        public TrackedParameter<int> AudioLevel29LangTagSub1 { get => audioLevel29LangTagSub1; set => audioLevel29LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel29TypeTagSub1 { get => audioLevel29TypeTagSub1; set => audioLevel29TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel29LangTagSub2 { get => audioLevel29LangTagSub2; set => audioLevel29LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel29TypeTagSub2 { get => audioLevel29TypeTagSub2; set => audioLevel29TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel30Index { get => audioLevel30Index; set => audioLevel30Index = value; }
        public TrackedParameter<int> AudioLevel30LangTag { get => audioLevel30LangTag; set => audioLevel30LangTag = value; }
        public TrackedParameter<int> AudioLevel30TypeTag { get => audioLevel30TypeTag; set => audioLevel30TypeTag = value; }
        public TrackedParameter<int> AudioLevel30LangTagSub1 { get => audioLevel30LangTagSub1; set => audioLevel30LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel30TypeTagSub1 { get => audioLevel30TypeTagSub1; set => audioLevel30TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel30LangTagSub2 { get => audioLevel30LangTagSub2; set => audioLevel30LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel30TypeTagSub2 { get => audioLevel30TypeTagSub2; set => audioLevel30TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel31Index { get => audioLevel31Index; set => audioLevel31Index = value; }
        public TrackedParameter<int> AudioLevel31LangTag { get => audioLevel31LangTag; set => audioLevel31LangTag = value; }
        public TrackedParameter<int> AudioLevel31TypeTag { get => audioLevel31TypeTag; set => audioLevel31TypeTag = value; }
        public TrackedParameter<int> AudioLevel31LangTagSub1 { get => audioLevel31LangTagSub1; set => audioLevel31LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel31TypeTagSub1 { get => audioLevel31TypeTagSub1; set => audioLevel31TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel31LangTagSub2 { get => audioLevel31LangTagSub2; set => audioLevel31LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel31TypeTagSub2 { get => audioLevel31TypeTagSub2; set => audioLevel31TypeTagSub2 = value; }
        public TrackedParameter<int> AudioLevel32Index { get => audioLevel32Index; set => audioLevel32Index = value; }
        public TrackedParameter<int> AudioLevel32LangTag { get => audioLevel32LangTag; set => audioLevel32LangTag = value; }
        public TrackedParameter<int> AudioLevel32TypeTag { get => audioLevel32TypeTag; set => audioLevel32TypeTag = value; }
        public TrackedParameter<int> AudioLevel32LangTagSub1 { get => audioLevel32LangTagSub1; set => audioLevel32LangTagSub1 = value; }
        public TrackedParameter<int> AudioLevel32TypeTagSub1 { get => audioLevel32TypeTagSub1; set => audioLevel32TypeTagSub1 = value; }
        public TrackedParameter<int> AudioLevel32LangTagSub2 { get => audioLevel32LangTagSub2; set => audioLevel32LangTagSub2 = value; }
        public TrackedParameter<int> AudioLevel32TypeTagSub2 { get => audioLevel32TypeTagSub2; set => audioLevel32TypeTagSub2 = value; }
        public TrackedParameter<int> AncLevel01Index { get => ancLevel01Index; set => ancLevel01Index = value; }
        public TrackedParameter<int> AncLevel01HubTag { get => ancLevel01HubTag; set => ancLevel01HubTag = value; }
        public TrackedParameter<int> AncLevel02Index { get => ancLevel02Index; set => ancLevel02Index = value; }
        public TrackedParameter<int> AncLevel02HubTag { get => ancLevel02HubTag; set => ancLevel02HubTag = value; }
        public TrackedParameter<int> AncLevel03Index { get => ancLevel03Index; set => ancLevel03Index = value; }
        public TrackedParameter<int> AncLevel03HubTag { get => ancLevel03HubTag; set => ancLevel03HubTag = value; }
        public TrackedParameter<int> AncLevel04Index { get => ancLevel04Index; set => ancLevel04Index = value; }
        public TrackedParameter<int> AncLevel04HubTag { get => ancLevel04HubTag; set => ancLevel04HubTag = value; }
        public TrackedParameter<int> AudioReturnLevel01Index { get => audioReturnLevel01Index; set => audioReturnLevel01Index = value; }
        public TrackedParameter<int> AudioReturnLevel01LangTag { get => audioReturnLevel01LangTag; set => audioReturnLevel01LangTag = value; }
        public TrackedParameter<int> AudioReturnLevel01TypeTag { get => audioReturnLevel01TypeTag; set => audioReturnLevel01TypeTag = value; }
        public TrackedParameter<int> AudioReturnLevel02Index { get => audioReturnLevel02Index; set => audioReturnLevel02Index = value; }
        public TrackedParameter<int> AudioReturnLevel02LangTag { get => audioReturnLevel02LangTag; set => audioReturnLevel02LangTag = value; }
        public TrackedParameter<int> AudioReturnLevel02TypeTag { get => audioReturnLevel02TypeTag; set => audioReturnLevel02TypeTag = value; }
        public TrackedParameter<int> ReturnVideoIndex { get => returnVideoIndex; set => returnVideoIndex = value; }
        public TrackedParameter<int> ReturnVideoHubTag { get => returnVideoHubTag; set => returnVideoHubTag = value; }

        private TrackedParameter<bool> lockStatus = new TrackedParameter<bool>(DbConstants.dbDestPackageTags);

        private TrackedParameter<int> videoLevel01Index = new TrackedParameter<int>(DbConstants.dbDestLevels);
        private TrackedParameter<int> videoLevel01HubTag = new TrackedParameter<int>(DbConstants.dbDestLevels);

        private TrackedParameter<int> audioLevel01Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel01LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel01TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel01LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel01TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel01LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel01TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel02Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel02LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel02TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel02LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel02TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel02LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel02TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel03Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel03LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel03TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel03LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel03TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel03LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel03TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel04Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel04LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel04TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel04LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel04TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel04LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel04TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel05Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel05LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel05TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel05LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel05TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel05LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel05TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel06Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel06LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel06TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel06LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel06TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel06LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel06TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel07Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel07LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel07TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel07LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel07TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel07LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel07TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel08Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel08LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel08TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel08LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel08TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel08LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel08TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel09Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel09LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel09TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel09LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel09TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel09LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel09TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel10Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel10LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel10TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel10LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel10TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel10LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel10TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel11Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel11LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel11TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel11LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel11TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel11LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel11TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel12Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel12LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel12TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel12LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel12TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel12LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel12TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel13Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel13LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel13TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel13LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel13TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel13LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel13TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel14Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel14LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel14TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel14LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel14TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel14LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel14TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel15Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel15LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel15TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel15LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel15TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel15LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel15TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel16Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel16LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel16TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel16LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel16TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel16LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel16TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel17Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel17LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel17TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel17LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel17TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel17LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel17TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel18Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel18LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel18TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel18LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel18TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel18LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel18TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel19Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel19LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel19TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel19LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel19TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel19LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel19TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel20Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel20LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel20TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel20LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel20TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel20LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel20TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel21Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel21LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel21TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel21LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel21TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel21LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel21TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel22Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel22LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel22TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel22LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel22TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel22LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel22TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel23Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel23LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel23TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel23LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel23TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel23LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel23TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel24Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel24LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel24TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel24LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel24TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel24LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel24TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel25Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel25LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel25TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel25LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel25TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel25LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel25TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel26Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel26LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel26TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel26LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel26TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel26LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel26TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel27Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel27LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel27TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel27LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel27TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel27LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel27TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel28Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel28LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel28TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel28LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel28TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel28LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel28TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel29Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel29LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel29TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel29LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel29TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel29LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel29TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel30Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel30LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel30TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel30LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel30TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel30LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel30TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel31Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel31LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel31TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel31LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel31TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel31LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel31TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> audioLevel32Index = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairIndex);
        private TrackedParameter<int> audioLevel32LangTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel32TypeTag = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagType);
        private TrackedParameter<int> audioLevel32LangTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel32TypeTagSub1 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst1);
        private TrackedParameter<int> audioLevel32LangTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);
        private TrackedParameter<int> audioLevel32TypeTagSub2 = new TrackedParameter<int>(DbConstants.dbAudioDestinationPairTagTypeSubst2);

        private TrackedParameter<int> ancLevel01Index = new TrackedParameter<int>(DbConstants.dbDestLevels);
        private TrackedParameter<int> ancLevel01HubTag = new TrackedParameter<int>(DbConstants.dbDestLevels);

        private TrackedParameter<int> ancLevel02Index = new TrackedParameter<int>(DbConstants.dbDestLevels);
        private TrackedParameter<int> ancLevel02HubTag = new TrackedParameter<int>(DbConstants.dbDestLevels);

        private TrackedParameter<int> ancLevel03Index = new TrackedParameter<int>(DbConstants.dbDestLevels);
        private TrackedParameter<int> ancLevel03HubTag = new TrackedParameter<int>(DbConstants.dbDestLevels);

        private TrackedParameter<int> ancLevel04Index = new TrackedParameter<int>(DbConstants.dbDestLevels);
        private TrackedParameter<int> ancLevel04HubTag = new TrackedParameter<int>(DbConstants.dbDestLevels);

        private TrackedParameter<int> audioReturnLevel01Index = new TrackedParameter<int>(DbConstants.dbAudioReturn);
        private TrackedParameter<int> audioReturnLevel01LangTag = new TrackedParameter<int>(DbConstants.dbAudioReturn);
        private TrackedParameter<int> audioReturnLevel01TypeTag = new TrackedParameter<int>(DbConstants.dbAudioReturn);

        private TrackedParameter<int> audioReturnLevel02Index = new TrackedParameter<int>(DbConstants.dbAudioReturn);
        private TrackedParameter<int> audioReturnLevel02LangTag = new TrackedParameter<int>(DbConstants.dbAudioReturn);
        private TrackedParameter<int> audioReturnLevel02TypeTag = new TrackedParameter<int>(DbConstants.dbAudioReturn);

        private TrackedParameter<int> returnVideoIndex = new TrackedParameter<int>(DbConstants.dbDestLevels);
        private TrackedParameter<int> returnVideoHubTag = new TrackedParameter<int>(DbConstants.dbDestLevels);

        protected override void ProcessDbChange(uint dbNumber)
        {

            switch (dbNumber)
            {
                case DbConstants.dbDestPackageTitle:

                    UpdateDbEntry(dbNumber, PackageName.CurrentValue);
                    break;

                case DbConstants.dbDestButtonName:

                    UpdateDbEntry(dbNumber, ButtonName.CurrentValue);
                    break;

                case DbConstants.dbDestPackageTags:

                    SetTaggedValue(dbNumber, "unique_id", UniqueId.CurrentValue);
                    SetTaggedValue(dbNumber, "owner", GetTaggedIntValue(OwnerTag.CurrentValue));
                    SetTaggedValue(dbNumber, "delegate", GetTaggedIntValue(DelegateTag.CurrentValue));
                    break;

                case DbConstants.dbDestLevels:

                    var videoValue = $"{GetTaggedIntValue(VideoLevel01Index.CurrentValue)}#{GetTaggedIntValue(VideoLevel01HubTag.CurrentValue)}";

                    var anc1Value = $"{GetTaggedIntValue(AncLevel01Index.CurrentValue)}#{GetTaggedIntValue(AncLevel01HubTag.CurrentValue)}";
                    var anc2Value = $"{GetTaggedIntValue(AncLevel02Index.CurrentValue)}#{GetTaggedIntValue(AncLevel02HubTag.CurrentValue)}";
                    var anc3Value = $"{GetTaggedIntValue(AncLevel03Index.CurrentValue)}#{GetTaggedIntValue(AncLevel03HubTag.CurrentValue)}";
                    var anc4Value = $"{GetTaggedIntValue(AncLevel04Index.CurrentValue)}#{GetTaggedIntValue(AncLevel04HubTag.CurrentValue)}";

                    var revValue = $"{GetTaggedIntValue(ReturnVideoIndex.CurrentValue)}#{GetTaggedIntValue(ReturnVideoHubTag.CurrentValue)}";

                    SetTaggedValue(dbNumber, "video", videoValue);
                    SetTaggedValue(dbNumber, "anc1", anc1Value);
                    SetTaggedValue(dbNumber, "anc2", anc2Value);
                    SetTaggedValue(dbNumber, "anc3", anc3Value);
                    SetTaggedValue(dbNumber, "anc4", anc4Value);
                    SetTaggedValue(dbNumber, "rev_vision", revValue);

                    break;

                case DbConstants.dbAudioDestinationPairIndex:

                    var valList = new List<string>();

                    valList.Add(GetTaggedIntValue(AudioLevel01Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel02Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel03Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel04Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel05Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel06Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel07Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel08Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel09Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel10Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel11Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel12Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel13Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel14Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel15Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel16Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel17Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel18Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel19Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel20Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel21Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel22Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel23Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel24Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel25Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel26Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel27Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel28Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel29Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel30Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel31Index.CurrentValue));
                    valList.Add(GetTaggedIntValue(AudioLevel32Index.CurrentValue));

                    var srcAudioValue = String.Join(",", valList);

                    UpdateDbEntry(dbNumber, srcAudioValue);

                    break;

                case DbConstants.dbAudioDestinationPairTagType:

                    var tagList = new List<string>();

                    tagList.Add($"{GetTaggedIntValue(AudioLevel01LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel01TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel02LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel02TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel03LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel03TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel04LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel04TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel05LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel05TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel06LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel06TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel07LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel07TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel08LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel08TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel09LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel09TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel10LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel10TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel11LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel11TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel12LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel12TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel13LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel13TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel14LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel14TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel15LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel15TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel16LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel16TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel17LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel17TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel18LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel18TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel19LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel19TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel20LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel20TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel21LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel21TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel22LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel22TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel23LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel23TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel24LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel24TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel25LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel25TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel26LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel26TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel27LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel27TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel28LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel28TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel29LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel29TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel30LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel30TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel31LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel31TypeTag.CurrentValue)}");
                    tagList.Add($"{GetTaggedIntValue(AudioLevel32LangTag.CurrentValue)}:{GetTaggedIntValue(AudioLevel32TypeTag.CurrentValue)}");

                    var tagsValue = String.Join(",", tagList);

                    UpdateDbEntry(dbNumber, tagsValue);

                    break;

                case DbConstants.dbAudioDestinationPairTagTypeSubst1:

                    var subst1List = new List<string>();

                    subst1List.Add($"{GetTaggedIntValue(AudioLevel01LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel01TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel02LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel02TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel03LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel03TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel04LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel04TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel05LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel05TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel06LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel06TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel07LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel07TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel08LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel08TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel09LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel09TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel10LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel10TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel11LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel11TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel12LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel12TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel13LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel13TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel14LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel14TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel15LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel15TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel16LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel16TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel17LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel17TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel18LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel18TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel19LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel19TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel20LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel20TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel21LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel21TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel22LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel22TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel23LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel23TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel24LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel24TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel25LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel25TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel26LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel26TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel27LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel27TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel28LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel28TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel29LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel29TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel30LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel30TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel31LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel31TypeTagSub1.CurrentValue)}");
                    subst1List.Add($"{GetTaggedIntValue(AudioLevel32LangTagSub1.CurrentValue)}:{GetTaggedIntValue(AudioLevel32TypeTagSub1.CurrentValue)}");

                    var subst1Value = String.Join(",", subst1List);

                    UpdateDbEntry(dbNumber, subst1Value);

                    break;

                case DbConstants.dbAudioDestinationPairTagTypeSubst2:

                    var subst2List = new List<string>();

                    subst2List.Add($"{GetTaggedIntValue(AudioLevel01LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel01TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel02LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel02TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel03LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel03TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel04LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel04TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel05LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel05TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel06LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel06TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel07LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel07TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel08LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel08TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel09LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel09TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel10LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel10TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel11LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel11TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel12LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel12TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel13LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel13TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel14LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel14TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel15LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel15TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel16LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel16TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel17LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel17TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel18LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel18TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel19LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel19TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel20LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel20TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel21LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel21TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel22LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel22TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel23LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel23TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel24LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel24TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel25LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel25TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel26LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel26TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel27LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel27TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel28LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel28TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel29LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel29TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel30LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel30TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel31LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel31TypeTagSub2.CurrentValue)}");
                    subst2List.Add($"{GetTaggedIntValue(AudioLevel32LangTagSub2.CurrentValue)}:{GetTaggedIntValue(AudioLevel32TypeTagSub2.CurrentValue)}");

                    var subst2Value = String.Join(",", subst2List);

                    UpdateDbEntry(dbNumber, subst2Value);

                    break;

                case DbConstants.dbAudioReturn:

                    var a1 = $"{GetTaggedIntValue(AudioReturnLevel01Index.CurrentValue)}#{GetTaggedIntValue(AudioReturnLevel01LangTag.CurrentValue)}:{GetTaggedIntValue(AudioReturnLevel01TypeTag.CurrentValue)}";
                    var a2 = $"{GetTaggedIntValue(AudioReturnLevel02Index.CurrentValue)}#{GetTaggedIntValue(AudioReturnLevel02LangTag.CurrentValue)}:{GetTaggedIntValue(AudioReturnLevel02TypeTag.CurrentValue)}";

                    SetTaggedValue(dbNumber, "a1", a1);
                    SetTaggedValue(dbNumber, "a2", a2);

                    break;

                default:
                    break;
            }
        }

        protected override bool ProcessDbValue(uint dbNumber, string value, bool initMode)
        {

            switch (dbNumber)
            {

                case DbConstants.dbDestPackageTitle:

                    PackageName.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbDestButtonName:

                    ButtonName.Set(value, Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbDestPackageTags:

                    //field contains UniqueId, owner & delegate fields
                    UniqueId.Set(ReadTaggedValue(value, "unique_id"), Enums.ChangeSources.BNCS, initMode);
                    OwnerTag.Set(GetTaggedIntValue(ReadTaggedValue(value, "owner")), Enums.ChangeSources.BNCS, initMode);
                    DelegateTag.Set(GetTaggedIntValue(ReadTaggedValue(value, "delegate")), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbFunctionalDestArea:

                    FunctionalArea = value;

                    return true;

                case DbConstants.dbFunctionalDestName:

                    FunctionalName = value;

                    return true;

                case DbConstants.dbDestLevels:

                    var pairVideo = GetTuple(ReadTaggedValue(value, "video"));

                    var pairAnc01 = GetTuple(ReadTaggedValue(value, "anc1"));
                    var pairAnc02 = GetTuple(ReadTaggedValue(value, "anc2"));
                    var pairAnc03 = GetTuple(ReadTaggedValue(value, "anc3"));
                    var pairAnc04 = GetTuple(ReadTaggedValue(value, "anc4"));

                    var pairReverse = GetTuple(ReadTaggedValue(value, "rev_vision"));

                    VideoLevel01Index.Set(GetTaggedIntValue(pairVideo.Item1), Enums.ChangeSources.BNCS, initMode);
                    VideoLevel01HubTag.Set(GetTaggedIntValue(pairVideo.Item2), Enums.ChangeSources.BNCS, initMode);

                    AncLevel01Index.Set(GetTaggedIntValue(pairAnc01.Item1), Enums.ChangeSources.BNCS, initMode);
                    AncLevel02Index.Set(GetTaggedIntValue(pairAnc02.Item1), Enums.ChangeSources.BNCS, initMode);
                    AncLevel03Index.Set(GetTaggedIntValue(pairAnc03.Item1), Enums.ChangeSources.BNCS, initMode);
                    AncLevel04Index.Set(GetTaggedIntValue(pairAnc04.Item1), Enums.ChangeSources.BNCS, initMode);

                    AncLevel01HubTag.Set(GetTaggedIntValue(pairAnc01.Item2), Enums.ChangeSources.BNCS, initMode);
                    AncLevel02HubTag.Set(GetTaggedIntValue(pairAnc02.Item2), Enums.ChangeSources.BNCS, initMode);
                    AncLevel03HubTag.Set(GetTaggedIntValue(pairAnc03.Item2), Enums.ChangeSources.BNCS, initMode);
                    AncLevel04HubTag.Set(GetTaggedIntValue(pairAnc04.Item2), Enums.ChangeSources.BNCS, initMode);

                    ReturnVideoIndex.Set(GetTaggedIntValue(pairReverse.Item1), Enums.ChangeSources.BNCS, initMode);
                    ReturnVideoHubTag.Set(GetTaggedIntValue(pairReverse.Item2), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioDestinationPairIndex:

                    if (String.IsNullOrWhiteSpace(value))
                    {
                        //Log.Warning(STR_LoggingCategory, "Destination Package Audio null value", "Index: {0}", Index);
                        return false;
                    }

                    //source should be a pipe delimited array of 32 integers
                    var strVals = value.Split(',');

                    //at this stage strVals should contain 32 entries
                    if (strVals.Length < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Destination Package Audio length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, strVals.Length);
                        return false;
                    }

                    var intVals = new List<int>();

                    //convert each value into an integer and put it in the list
                    foreach (var val in strVals)
                    {
                        if (string.IsNullOrWhiteSpace(val)) intVals.Add(0);
                        else if (int.TryParse(val, out int result)) intVals.Add(result);
                        else
                        {
                            //invalid value!
                            Log.Warning(STR_LoggingCategory, "Invalid Destination Package Audio entry", "Index: {0}  Value: {1}", Index, value);
                            intVals.Add(0);
                        }
                    }

                    //at this stage intValue should contain 32 entries
                    if (intVals.Count < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Destination Package Audio length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, intVals.Count);
                        return false;
                    }

                    AudioLevel01Index.Set(intVals[00], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02Index.Set(intVals[01], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03Index.Set(intVals[02], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04Index.Set(intVals[03], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05Index.Set(intVals[04], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06Index.Set(intVals[05], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07Index.Set(intVals[06], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08Index.Set(intVals[07], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09Index.Set(intVals[08], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10Index.Set(intVals[09], Enums.ChangeSources.BNCS, initMode);

                    AudioLevel11Index.Set(intVals[10], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12Index.Set(intVals[11], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13Index.Set(intVals[12], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14Index.Set(intVals[13], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15Index.Set(intVals[14], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16Index.Set(intVals[15], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel17Index.Set(intVals[16], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel18Index.Set(intVals[17], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel19Index.Set(intVals[18], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel20Index.Set(intVals[19], Enums.ChangeSources.BNCS, initMode);

                    AudioLevel21Index.Set(intVals[20], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel22Index.Set(intVals[21], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel23Index.Set(intVals[22], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel24Index.Set(intVals[23], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel25Index.Set(intVals[24], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel26Index.Set(intVals[25], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel27Index.Set(intVals[26], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel28Index.Set(intVals[27], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel29Index.Set(intVals[28], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel30Index.Set(intVals[29], Enums.ChangeSources.BNCS, initMode);

                    AudioLevel31Index.Set(intVals[30], Enums.ChangeSources.BNCS, initMode);
                    AudioLevel32Index.Set(intVals[31], Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioDestinationPairTagType:

                    if (String.IsNullOrWhiteSpace(value))
                    {
                        //Log.Warning(STR_LoggingCategory, "Destination Package Audio null value", "Index: {0}", Index);
                        return false;
                    }

                    //source should be a pipe delimited array of 32 integers
                    var strTagVals = value.Split(',');

                    //at this stage strVals should contain 32 entries
                    if (strTagVals.Length < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Destination Audio Pair Type length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, strTagVals.Length);
                        return false;
                    }

                    var tagTuples = new List<(string, string, string, string)>();

                    //convert each value into an integer and put it in the list
                    foreach (var val in strTagVals)
                    {
                        if (string.IsNullOrWhiteSpace(val)) tagTuples.Add((null, null, null, null));

                        tagTuples.Add(GetTuple(val));
                    }

                    //at this stage intValue should contain 32 entries
                    if (tagTuples.Count < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Destination Audio Pair Type length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, tagTuples.Count);
                        return false;
                    }

                    AudioLevel01LangTag.Set(GetTaggedIntValue(tagTuples[00].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02LangTag.Set(GetTaggedIntValue(tagTuples[01].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03LangTag.Set(GetTaggedIntValue(tagTuples[02].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04LangTag.Set(GetTaggedIntValue(tagTuples[03].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05LangTag.Set(GetTaggedIntValue(tagTuples[04].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06LangTag.Set(GetTaggedIntValue(tagTuples[05].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07LangTag.Set(GetTaggedIntValue(tagTuples[06].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08LangTag.Set(GetTaggedIntValue(tagTuples[07].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09LangTag.Set(GetTaggedIntValue(tagTuples[08].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10LangTag.Set(GetTaggedIntValue(tagTuples[09].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel11LangTag.Set(GetTaggedIntValue(tagTuples[10].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12LangTag.Set(GetTaggedIntValue(tagTuples[11].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13LangTag.Set(GetTaggedIntValue(tagTuples[12].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14LangTag.Set(GetTaggedIntValue(tagTuples[13].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15LangTag.Set(GetTaggedIntValue(tagTuples[14].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16LangTag.Set(GetTaggedIntValue(tagTuples[15].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel17LangTag.Set(GetTaggedIntValue(tagTuples[16].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel18LangTag.Set(GetTaggedIntValue(tagTuples[17].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel19LangTag.Set(GetTaggedIntValue(tagTuples[18].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel20LangTag.Set(GetTaggedIntValue(tagTuples[19].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel21LangTag.Set(GetTaggedIntValue(tagTuples[20].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel22LangTag.Set(GetTaggedIntValue(tagTuples[21].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel23LangTag.Set(GetTaggedIntValue(tagTuples[22].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel24LangTag.Set(GetTaggedIntValue(tagTuples[23].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel25LangTag.Set(GetTaggedIntValue(tagTuples[24].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel26LangTag.Set(GetTaggedIntValue(tagTuples[25].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel27LangTag.Set(GetTaggedIntValue(tagTuples[26].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel28LangTag.Set(GetTaggedIntValue(tagTuples[27].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel29LangTag.Set(GetTaggedIntValue(tagTuples[28].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel30LangTag.Set(GetTaggedIntValue(tagTuples[29].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel31LangTag.Set(GetTaggedIntValue(tagTuples[30].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel32LangTag.Set(GetTaggedIntValue(tagTuples[31].Item1), Enums.ChangeSources.BNCS, initMode);

                    AudioLevel01TypeTag.Set(GetTaggedIntValue(tagTuples[00].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02TypeTag.Set(GetTaggedIntValue(tagTuples[01].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03TypeTag.Set(GetTaggedIntValue(tagTuples[02].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04TypeTag.Set(GetTaggedIntValue(tagTuples[03].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05TypeTag.Set(GetTaggedIntValue(tagTuples[04].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06TypeTag.Set(GetTaggedIntValue(tagTuples[05].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07TypeTag.Set(GetTaggedIntValue(tagTuples[06].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08TypeTag.Set(GetTaggedIntValue(tagTuples[07].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09TypeTag.Set(GetTaggedIntValue(tagTuples[08].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10TypeTag.Set(GetTaggedIntValue(tagTuples[09].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel11TypeTag.Set(GetTaggedIntValue(tagTuples[10].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12TypeTag.Set(GetTaggedIntValue(tagTuples[11].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13TypeTag.Set(GetTaggedIntValue(tagTuples[12].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14TypeTag.Set(GetTaggedIntValue(tagTuples[13].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15TypeTag.Set(GetTaggedIntValue(tagTuples[14].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16TypeTag.Set(GetTaggedIntValue(tagTuples[15].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel17TypeTag.Set(GetTaggedIntValue(tagTuples[16].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel18TypeTag.Set(GetTaggedIntValue(tagTuples[17].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel19TypeTag.Set(GetTaggedIntValue(tagTuples[18].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel20TypeTag.Set(GetTaggedIntValue(tagTuples[19].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel21TypeTag.Set(GetTaggedIntValue(tagTuples[20].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel22TypeTag.Set(GetTaggedIntValue(tagTuples[21].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel23TypeTag.Set(GetTaggedIntValue(tagTuples[22].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel24TypeTag.Set(GetTaggedIntValue(tagTuples[23].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel25TypeTag.Set(GetTaggedIntValue(tagTuples[24].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel26TypeTag.Set(GetTaggedIntValue(tagTuples[25].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel27TypeTag.Set(GetTaggedIntValue(tagTuples[26].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel28TypeTag.Set(GetTaggedIntValue(tagTuples[27].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel29TypeTag.Set(GetTaggedIntValue(tagTuples[28].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel30TypeTag.Set(GetTaggedIntValue(tagTuples[29].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel31TypeTag.Set(GetTaggedIntValue(tagTuples[30].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel32TypeTag.Set(GetTaggedIntValue(tagTuples[31].Item2), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioDestinationPairTagTypeSubst1:

                    if (String.IsNullOrWhiteSpace(value))
                    {
                        //Log.Warning(STR_LoggingCategory, "Destination Package Audio null value", "Index: {0}", Index);
                        return false;
                    }

                    //source should be a pipe delimited array of 32 integers
                    var strSub1Vals = value.Split(',');

                    //at this stage strVals should contain 32 entries
                    if (strSub1Vals.Length < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Destination Audio Pair Subst 1 length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, strSub1Vals.Length);
                        return false;
                    }

                    var tagSub1Tuples = new List<(string, string, string, string)>();

                    //convert each value into an integer and put it in the list
                    foreach (var val in strSub1Vals)
                    {
                        if (string.IsNullOrWhiteSpace(val)) tagSub1Tuples.Add((null, null, null, null));

                        tagSub1Tuples.Add(GetTuple(val));
                    }

                    //at this stage intValue should contain 32 entries
                    if (tagSub1Tuples.Count < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Destination Audio Pair Subst 1 length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, tagSub1Tuples.Count);
                        return false;
                    }

                    AudioLevel01LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[00].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[01].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[02].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[03].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[04].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[05].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[06].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[07].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[08].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[09].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel11LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[10].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[11].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[12].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[13].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[14].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[15].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel17LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[16].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel18LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[17].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel19LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[18].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel20LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[19].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel21LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[20].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel22LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[21].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel23LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[22].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel24LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[23].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel25LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[24].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel26LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[25].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel27LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[26].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel28LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[27].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel29LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[28].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel30LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[29].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel31LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[30].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel32LangTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[31].Item1), Enums.ChangeSources.BNCS, initMode);

                    AudioLevel01TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[00].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[01].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[02].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[03].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[04].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[05].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[06].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[07].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[08].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[09].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel11TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[10].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[11].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[12].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[13].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[14].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[15].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel17TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[16].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel18TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[17].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel19TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[18].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel20TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[19].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel21TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[20].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel22TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[21].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel23TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[22].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel24TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[23].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel25TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[24].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel26TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[25].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel27TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[26].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel28TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[27].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel29TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[28].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel30TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[29].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel31TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[30].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel32TypeTagSub1.Set(GetTaggedIntValue(tagSub1Tuples[31].Item2), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioDestinationPairTagTypeSubst2:

                    if (String.IsNullOrWhiteSpace(value))
                    {
                        //Log.Warning(STR_LoggingCategory, "Destination Package Audio null value", "Index: {0}", Index);
                        return false;
                    }

                    //source should be a pipe delimited array of 32 integers
                    var strSub2Vals = value.Split(',');

                    //at this stage strVals should contain 32 entries
                    if (strSub2Vals.Length < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Destination Audio Pair Subst 2 length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, strSub2Vals.Length);
                        return false;
                    }

                    var tagSub2Tuples = new List<(string, string, string, string)>();

                    //convert each value into an integer and put it in the list
                    foreach (var val in strSub2Vals)
                    {
                        if (string.IsNullOrWhiteSpace(val)) tagSub2Tuples.Add((null, null, null, null));

                        tagSub2Tuples.Add(GetTuple(val));
                    }

                    //at this stage intValue should contain 32 entries
                    if (tagSub2Tuples.Count < 32)
                    {
                        Log.Warning(STR_LoggingCategory, "Destination Audio Pair Subst 2 length mismatch - should be 32", "Index: {0}  Length: {2} Value: {1}", Index, value, tagSub2Tuples.Count);
                        return false;
                    }

                    AudioLevel01LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[00].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[01].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[02].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[03].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[04].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[05].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[06].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[07].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[08].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[09].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel11LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[10].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[11].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[12].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[13].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[14].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[15].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel17LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[16].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel18LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[17].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel19LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[18].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel20LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[19].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel21LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[20].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel22LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[21].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel23LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[22].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel24LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[23].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel25LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[24].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel26LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[25].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel27LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[26].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel28LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[27].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel29LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[28].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel30LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[29].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel31LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[30].Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel32LangTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[31].Item1), Enums.ChangeSources.BNCS, initMode);

                    AudioLevel01TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[00].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel02TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[01].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel03TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[02].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel04TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[03].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel05TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[04].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel06TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[05].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel07TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[06].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel08TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[07].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel09TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[08].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel10TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[09].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel11TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[10].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel12TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[11].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel13TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[12].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel14TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[13].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel15TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[14].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel16TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[15].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel17TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[16].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel18TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[17].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel19TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[18].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel20TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[19].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel21TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[20].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel22TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[21].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel23TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[22].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel24TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[23].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel25TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[24].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel26TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[25].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel27TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[26].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel28TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[27].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel29TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[28].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel30TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[29].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel31TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[30].Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioLevel32TypeTagSub2.Set(GetTaggedIntValue(tagSub2Tuples[31].Item2), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbAudioReturn:

                    var rpair01 = GetTuple(ReadTaggedValue(value, "a1"));
                    var rpair02 = GetTuple(ReadTaggedValue(value, "a2"));

                    AudioReturnLevel01Index.Set(GetTaggedIntValue(rpair01.Item1), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevel02Index.Set(GetTaggedIntValue(rpair02.Item1), Enums.ChangeSources.BNCS, initMode);

                    AudioReturnLevel01LangTag.Set(GetTaggedIntValue(rpair01.Item2), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevel02LangTag.Set(GetTaggedIntValue(rpair02.Item2), Enums.ChangeSources.BNCS, initMode);

                    AudioReturnLevel01TypeTag.Set(GetTaggedIntValue(rpair01.Item3), Enums.ChangeSources.BNCS, initMode);
                    AudioReturnLevel02TypeTag.Set(GetTaggedIntValue(rpair02.Item3), Enums.ChangeSources.BNCS, initMode);

                    return true;

                case DbConstants.dbDestPackageEditLocks:

                    EditLocked = !String.IsNullOrWhiteSpace(value);
                    return true;

                case DbConstants.dbDestPackageRouteLocks:

                    RouteLocked = !String.IsNullOrWhiteSpace(value);
                    return true;

                default:
                    return false;
            }
        }

        public ett_api_domain.DestinationPackage GetDocument()
        {

            var result = new ett_api_domain.DestinationPackage();

            result.Index = (int)Index;

            result.OwnerTag = new ett_api_domain.OwnerTag();
            result.OwnerTag.Index = NullableInt(OwnerTag.CurrentValue);
            result.OwnerTag.Name = Lookup(DbConstants.dbOwnerTags, OwnerTag.CurrentValue);

            result.DelegateTag = new ett_api_domain.DelegateTag();
            result.DelegateTag.Index = NullableInt(DelegateTag.CurrentValue);
            result.DelegateTag.Name = Lookup(DbConstants.dbDelegateTags, DelegateTag.CurrentValue);

            result.ButtonName = ButtonName.CurrentValue;
            result.PackageName = PackageName.CurrentValue;
            result.PackageType = PackageType;

            result.SymbolicName = SymbolicName;
            result.LockStatus = LockStatus.CurrentValue;

            result.VideoLevels = new ett_api_domain.DestinationPackageVideoLevels();

            result.VideoLevels._1 = new ett_api_domain.DestinationPackageVideoLevel();

            result.VideoLevels._1.Index = NullableInt(VideoLevel01Index.CurrentValue);
            result.VideoLevels._1.VideoHubTagIndex = NullableInt(VideoLevel01HubTag.CurrentValue);

            result.AudioLevels = new ett_api_domain.DestinationPackageAudioLevels();

            //AUDIO LEVEL 01
            result.AudioLevels._1 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._1.Index = NullableInt(AudioLevel01Index.CurrentValue);

            result.AudioLevels._1.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._1.LangTagPrimary.Index = NullableInt(AudioLevel01LangTag.CurrentValue);
            result.AudioLevels._1.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel01LangTag.CurrentValue);
            result.AudioLevels._1.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._1.LangTagSubstitute1.Index = NullableInt(AudioLevel01LangTagSub1.CurrentValue);
            result.AudioLevels._1.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel01LangTagSub1.CurrentValue);
            result.AudioLevels._1.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._1.LangTagSubstitute2.Index = NullableInt(AudioLevel01LangTagSub2.CurrentValue);
            result.AudioLevels._1.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel01LangTagSub2.CurrentValue);

            result.AudioLevels._1.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._1.TypeTagPrimary.Index = NullableInt(AudioLevel01TypeTag.CurrentValue);
            result.AudioLevels._1.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel01TypeTag.CurrentValue);
            result.AudioLevels._1.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._1.TypeTagSubstitute1.Index = NullableInt(AudioLevel01TypeTagSub1.CurrentValue);
            result.AudioLevels._1.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel01TypeTagSub1.CurrentValue);
            result.AudioLevels._1.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._1.TypeTagSubstitute2.Index = NullableInt(AudioLevel01TypeTagSub2.CurrentValue);
            result.AudioLevels._1.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel01TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 02
            result.AudioLevels._2 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._2.Index = NullableInt(AudioLevel02Index.CurrentValue);

            result.AudioLevels._2.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._2.LangTagPrimary.Index = NullableInt(AudioLevel02LangTag.CurrentValue);
            result.AudioLevels._2.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel02LangTag.CurrentValue);
            result.AudioLevels._2.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._2.LangTagSubstitute1.Index = NullableInt(AudioLevel02LangTagSub1.CurrentValue);
            result.AudioLevels._2.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel02LangTagSub1.CurrentValue);
            result.AudioLevels._2.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._2.LangTagSubstitute2.Index = NullableInt(AudioLevel02LangTagSub2.CurrentValue);
            result.AudioLevels._2.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel02LangTagSub2.CurrentValue);

            result.AudioLevels._2.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._2.TypeTagPrimary.Index = NullableInt(AudioLevel02TypeTag.CurrentValue);
            result.AudioLevels._2.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel02TypeTag.CurrentValue);
            result.AudioLevels._2.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._2.TypeTagSubstitute1.Index = NullableInt(AudioLevel02TypeTagSub1.CurrentValue);
            result.AudioLevels._2.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel02TypeTagSub1.CurrentValue);
            result.AudioLevels._2.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._2.TypeTagSubstitute2.Index = NullableInt(AudioLevel02TypeTagSub2.CurrentValue);
            result.AudioLevels._2.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel02TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 03
            result.AudioLevels._3 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._3.Index = NullableInt(AudioLevel03Index.CurrentValue);

            result.AudioLevels._3.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._3.LangTagPrimary.Index = NullableInt(AudioLevel03LangTag.CurrentValue);
            result.AudioLevels._3.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel03LangTag.CurrentValue);
            result.AudioLevels._3.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._3.LangTagSubstitute1.Index = NullableInt(AudioLevel03LangTagSub1.CurrentValue);
            result.AudioLevels._3.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel03LangTagSub1.CurrentValue);
            result.AudioLevels._3.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._3.LangTagSubstitute2.Index = NullableInt(AudioLevel03LangTagSub2.CurrentValue);
            result.AudioLevels._3.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel03LangTagSub2.CurrentValue);

            result.AudioLevels._3.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._3.TypeTagPrimary.Index = NullableInt(AudioLevel03TypeTag.CurrentValue);
            result.AudioLevels._3.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel03TypeTag.CurrentValue);
            result.AudioLevels._3.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._3.TypeTagSubstitute1.Index = NullableInt(AudioLevel03TypeTagSub1.CurrentValue);
            result.AudioLevels._3.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel03TypeTagSub1.CurrentValue);
            result.AudioLevels._3.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._3.TypeTagSubstitute2.Index = NullableInt(AudioLevel03TypeTagSub2.CurrentValue);
            result.AudioLevels._3.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel03TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 04
            result.AudioLevels._4 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._4.Index = NullableInt(AudioLevel04Index.CurrentValue);

            result.AudioLevels._4.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._4.LangTagPrimary.Index = NullableInt(AudioLevel04LangTag.CurrentValue);
            result.AudioLevels._4.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel04LangTag.CurrentValue);
            result.AudioLevels._4.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._4.LangTagSubstitute1.Index = NullableInt(AudioLevel04LangTagSub1.CurrentValue);
            result.AudioLevels._4.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel04LangTagSub1.CurrentValue);
            result.AudioLevels._4.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._4.LangTagSubstitute2.Index = NullableInt(AudioLevel04LangTagSub2.CurrentValue);
            result.AudioLevels._4.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel04LangTagSub2.CurrentValue);

            result.AudioLevels._4.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._4.TypeTagPrimary.Index = NullableInt(AudioLevel04TypeTag.CurrentValue);
            result.AudioLevels._4.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel04TypeTag.CurrentValue);
            result.AudioLevels._4.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._4.TypeTagSubstitute1.Index = NullableInt(AudioLevel04TypeTagSub1.CurrentValue);
            result.AudioLevels._4.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel04TypeTagSub1.CurrentValue);
            result.AudioLevels._4.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._4.TypeTagSubstitute2.Index = NullableInt(AudioLevel04TypeTagSub2.CurrentValue);
            result.AudioLevels._4.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel04TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 05
            result.AudioLevels._5 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._5.Index = NullableInt(AudioLevel05Index.CurrentValue);

            result.AudioLevels._5.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._5.LangTagPrimary.Index = NullableInt(AudioLevel05LangTag.CurrentValue);
            result.AudioLevels._5.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel05LangTag.CurrentValue);
            result.AudioLevels._5.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._5.LangTagSubstitute1.Index = NullableInt(AudioLevel05LangTagSub1.CurrentValue);
            result.AudioLevels._5.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel05LangTagSub1.CurrentValue);
            result.AudioLevels._5.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._5.LangTagSubstitute2.Index = NullableInt(AudioLevel05LangTagSub2.CurrentValue);
            result.AudioLevels._5.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel05LangTagSub2.CurrentValue);

            result.AudioLevels._5.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._5.TypeTagPrimary.Index = NullableInt(AudioLevel05TypeTag.CurrentValue);
            result.AudioLevels._5.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel05TypeTag.CurrentValue);
            result.AudioLevels._5.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._5.TypeTagSubstitute1.Index = NullableInt(AudioLevel05TypeTagSub1.CurrentValue);
            result.AudioLevels._5.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel05TypeTagSub1.CurrentValue);
            result.AudioLevels._5.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._5.TypeTagSubstitute2.Index = NullableInt(AudioLevel05TypeTagSub2.CurrentValue);
            result.AudioLevels._5.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel05TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 06
            result.AudioLevels._6 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._6.Index = NullableInt(AudioLevel06Index.CurrentValue);

            result.AudioLevels._6.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._6.LangTagPrimary.Index = NullableInt(AudioLevel06LangTag.CurrentValue);
            result.AudioLevels._6.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel06LangTag.CurrentValue);
            result.AudioLevels._6.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._6.LangTagSubstitute1.Index = NullableInt(AudioLevel06LangTagSub1.CurrentValue);
            result.AudioLevels._6.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel06LangTagSub1.CurrentValue);
            result.AudioLevels._6.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._6.LangTagSubstitute2.Index = NullableInt(AudioLevel06LangTagSub2.CurrentValue);
            result.AudioLevels._6.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel06LangTagSub2.CurrentValue);

            result.AudioLevels._6.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._6.TypeTagPrimary.Index = NullableInt(AudioLevel06TypeTag.CurrentValue);
            result.AudioLevels._6.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel06TypeTag.CurrentValue);
            result.AudioLevels._6.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._6.TypeTagSubstitute1.Index = NullableInt(AudioLevel06TypeTagSub1.CurrentValue);
            result.AudioLevels._6.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel06TypeTagSub1.CurrentValue);
            result.AudioLevels._6.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._6.TypeTagSubstitute2.Index = NullableInt(AudioLevel06TypeTagSub2.CurrentValue);
            result.AudioLevels._6.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel06TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 07
            result.AudioLevels._7 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._7.Index = NullableInt(AudioLevel07Index.CurrentValue);

            result.AudioLevels._7.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._7.LangTagPrimary.Index = NullableInt(AudioLevel07LangTag.CurrentValue);
            result.AudioLevels._7.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel07LangTag.CurrentValue);
            result.AudioLevels._7.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._7.LangTagSubstitute1.Index = NullableInt(AudioLevel07LangTagSub1.CurrentValue);
            result.AudioLevels._7.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel07LangTagSub1.CurrentValue);
            result.AudioLevels._7.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._7.LangTagSubstitute2.Index = NullableInt(AudioLevel07LangTagSub2.CurrentValue);
            result.AudioLevels._7.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel07LangTagSub2.CurrentValue);

            result.AudioLevels._7.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._7.TypeTagPrimary.Index = NullableInt(AudioLevel07TypeTag.CurrentValue);
            result.AudioLevels._7.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel07TypeTag.CurrentValue);
            result.AudioLevels._7.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._7.TypeTagSubstitute1.Index = NullableInt(AudioLevel07TypeTagSub1.CurrentValue);
            result.AudioLevels._7.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel07TypeTagSub1.CurrentValue);
            result.AudioLevels._7.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._7.TypeTagSubstitute2.Index = NullableInt(AudioLevel07TypeTagSub2.CurrentValue);
            result.AudioLevels._7.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel07TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 08
            result.AudioLevels._8 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._8.Index = NullableInt(AudioLevel08Index.CurrentValue);

            result.AudioLevels._8.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._8.LangTagPrimary.Index = NullableInt(AudioLevel08LangTag.CurrentValue);
            result.AudioLevels._8.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel08LangTag.CurrentValue);
            result.AudioLevels._8.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._8.LangTagSubstitute1.Index = NullableInt(AudioLevel08LangTagSub1.CurrentValue);
            result.AudioLevels._8.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel08LangTagSub1.CurrentValue);
            result.AudioLevels._8.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._8.LangTagSubstitute2.Index = NullableInt(AudioLevel08LangTagSub2.CurrentValue);
            result.AudioLevels._8.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel08LangTagSub2.CurrentValue);

            result.AudioLevels._8.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._8.TypeTagPrimary.Index = NullableInt(AudioLevel08TypeTag.CurrentValue);
            result.AudioLevels._8.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel08TypeTag.CurrentValue);
            result.AudioLevels._8.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._8.TypeTagSubstitute1.Index = NullableInt(AudioLevel08TypeTagSub1.CurrentValue);
            result.AudioLevels._8.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel08TypeTagSub1.CurrentValue);
            result.AudioLevels._8.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._8.TypeTagSubstitute2.Index = NullableInt(AudioLevel08TypeTagSub2.CurrentValue);
            result.AudioLevels._8.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel08TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 09
            result.AudioLevels._9 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._9.Index = NullableInt(AudioLevel09Index.CurrentValue);

            result.AudioLevels._9.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._9.LangTagPrimary.Index = NullableInt(AudioLevel09LangTag.CurrentValue);
            result.AudioLevels._9.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel09LangTag.CurrentValue);
            result.AudioLevels._9.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._9.LangTagSubstitute1.Index = NullableInt(AudioLevel09LangTagSub1.CurrentValue);
            result.AudioLevels._9.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel09LangTagSub1.CurrentValue);
            result.AudioLevels._9.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._9.LangTagSubstitute2.Index = NullableInt(AudioLevel09LangTagSub2.CurrentValue);
            result.AudioLevels._9.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel09LangTagSub2.CurrentValue);

            result.AudioLevels._9.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._9.TypeTagPrimary.Index = NullableInt(AudioLevel09TypeTag.CurrentValue);
            result.AudioLevels._9.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel09TypeTag.CurrentValue);
            result.AudioLevels._9.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._9.TypeTagSubstitute1.Index = NullableInt(AudioLevel09TypeTagSub1.CurrentValue);
            result.AudioLevels._9.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel09TypeTagSub1.CurrentValue);
            result.AudioLevels._9.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._9.TypeTagSubstitute2.Index = NullableInt(AudioLevel09TypeTagSub2.CurrentValue);
            result.AudioLevels._9.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel09TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 10
            result.AudioLevels._10 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._10.Index = NullableInt(AudioLevel10Index.CurrentValue);

            result.AudioLevels._10.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._10.LangTagPrimary.Index = NullableInt(AudioLevel10LangTag.CurrentValue);
            result.AudioLevels._10.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel10LangTag.CurrentValue);
            result.AudioLevels._10.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._10.LangTagSubstitute1.Index = NullableInt(AudioLevel10LangTagSub1.CurrentValue);
            result.AudioLevels._10.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel10LangTagSub1.CurrentValue);
            result.AudioLevels._10.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._10.LangTagSubstitute2.Index = NullableInt(AudioLevel10LangTagSub2.CurrentValue);
            result.AudioLevels._10.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel10LangTagSub2.CurrentValue);

            result.AudioLevels._10.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._10.TypeTagPrimary.Index = NullableInt(AudioLevel10TypeTag.CurrentValue);
            result.AudioLevels._10.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel10TypeTag.CurrentValue);
            result.AudioLevels._10.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._10.TypeTagSubstitute1.Index = NullableInt(AudioLevel10TypeTagSub1.CurrentValue);
            result.AudioLevels._10.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel10TypeTagSub1.CurrentValue);
            result.AudioLevels._10.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._10.TypeTagSubstitute2.Index = NullableInt(AudioLevel10TypeTagSub2.CurrentValue);
            result.AudioLevels._10.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel10TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 11
            result.AudioLevels._11 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._11.Index = NullableInt(AudioLevel11Index.CurrentValue);

            result.AudioLevels._11.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._11.LangTagPrimary.Index = NullableInt(AudioLevel11LangTag.CurrentValue);
            result.AudioLevels._11.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel11LangTag.CurrentValue);
            result.AudioLevels._11.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._11.LangTagSubstitute1.Index = NullableInt(AudioLevel11LangTagSub1.CurrentValue);
            result.AudioLevels._11.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel11LangTagSub1.CurrentValue);
            result.AudioLevels._11.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._11.LangTagSubstitute2.Index = NullableInt(AudioLevel11LangTagSub2.CurrentValue);
            result.AudioLevels._11.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel11LangTagSub2.CurrentValue);

            result.AudioLevels._11.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._11.TypeTagPrimary.Index = NullableInt(AudioLevel11TypeTag.CurrentValue);
            result.AudioLevels._11.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel11TypeTag.CurrentValue);
            result.AudioLevels._11.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._11.TypeTagSubstitute1.Index = NullableInt(AudioLevel11TypeTagSub1.CurrentValue);
            result.AudioLevels._11.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel11TypeTagSub1.CurrentValue);
            result.AudioLevels._11.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._11.TypeTagSubstitute2.Index = NullableInt(AudioLevel11TypeTagSub2.CurrentValue);
            result.AudioLevels._11.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel11TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 12
            result.AudioLevels._12 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._12.Index = NullableInt(AudioLevel12Index.CurrentValue);

            result.AudioLevels._12.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._12.LangTagPrimary.Index = NullableInt(AudioLevel12LangTag.CurrentValue);
            result.AudioLevels._12.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel12LangTag.CurrentValue);
            result.AudioLevels._12.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._12.LangTagSubstitute1.Index = NullableInt(AudioLevel12LangTagSub1.CurrentValue);
            result.AudioLevels._12.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel12LangTagSub1.CurrentValue);
            result.AudioLevels._12.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._12.LangTagSubstitute2.Index = NullableInt(AudioLevel12LangTagSub2.CurrentValue);
            result.AudioLevels._12.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel12LangTagSub2.CurrentValue);

            result.AudioLevels._12.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._12.TypeTagPrimary.Index = NullableInt(AudioLevel12TypeTag.CurrentValue);
            result.AudioLevels._12.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel12TypeTag.CurrentValue);
            result.AudioLevels._12.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._12.TypeTagSubstitute1.Index = NullableInt(AudioLevel12TypeTagSub1.CurrentValue);
            result.AudioLevels._12.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel12TypeTagSub1.CurrentValue);
            result.AudioLevels._12.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._12.TypeTagSubstitute2.Index = NullableInt(AudioLevel12TypeTagSub2.CurrentValue);
            result.AudioLevels._12.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel12TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 13
            result.AudioLevels._13 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._13.Index = NullableInt(AudioLevel13Index.CurrentValue);

            result.AudioLevels._13.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._13.LangTagPrimary.Index = NullableInt(AudioLevel13LangTag.CurrentValue);
            result.AudioLevels._13.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel13LangTag.CurrentValue);
            result.AudioLevels._13.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._13.LangTagSubstitute1.Index = NullableInt(AudioLevel13LangTagSub1.CurrentValue);
            result.AudioLevels._13.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel13LangTagSub1.CurrentValue);
            result.AudioLevels._13.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._13.LangTagSubstitute2.Index = NullableInt(AudioLevel13LangTagSub2.CurrentValue);
            result.AudioLevels._13.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel13LangTagSub2.CurrentValue);

            result.AudioLevels._13.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._13.TypeTagPrimary.Index = NullableInt(AudioLevel13TypeTag.CurrentValue);
            result.AudioLevels._13.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel13TypeTag.CurrentValue);
            result.AudioLevels._13.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._13.TypeTagSubstitute1.Index = NullableInt(AudioLevel13TypeTagSub1.CurrentValue);
            result.AudioLevels._13.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel13TypeTagSub1.CurrentValue);
            result.AudioLevels._13.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._13.TypeTagSubstitute2.Index = NullableInt(AudioLevel13TypeTagSub2.CurrentValue);
            result.AudioLevels._13.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel13TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 14
            result.AudioLevels._14 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._14.Index = NullableInt(AudioLevel14Index.CurrentValue);

            result.AudioLevels._14.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._14.LangTagPrimary.Index = NullableInt(AudioLevel14LangTag.CurrentValue);
            result.AudioLevels._14.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel14LangTag.CurrentValue);
            result.AudioLevels._14.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._14.LangTagSubstitute1.Index = NullableInt(AudioLevel14LangTagSub1.CurrentValue);
            result.AudioLevels._14.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel14LangTagSub1.CurrentValue);
            result.AudioLevels._14.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._14.LangTagSubstitute2.Index = NullableInt(AudioLevel14LangTagSub2.CurrentValue);
            result.AudioLevels._14.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel14LangTagSub2.CurrentValue);

            result.AudioLevels._14.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._14.TypeTagPrimary.Index = NullableInt(AudioLevel14TypeTag.CurrentValue);
            result.AudioLevels._14.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel14TypeTag.CurrentValue);
            result.AudioLevels._14.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._14.TypeTagSubstitute1.Index = NullableInt(AudioLevel14TypeTagSub1.CurrentValue);
            result.AudioLevels._14.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel14TypeTagSub1.CurrentValue);
            result.AudioLevels._14.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._14.TypeTagSubstitute2.Index = NullableInt(AudioLevel14TypeTagSub2.CurrentValue);
            result.AudioLevels._14.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel14TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 15
            result.AudioLevels._15 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._15.Index = NullableInt(AudioLevel15Index.CurrentValue);

            result.AudioLevels._15.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._15.LangTagPrimary.Index = NullableInt(AudioLevel15LangTag.CurrentValue);
            result.AudioLevels._15.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel15LangTag.CurrentValue);
            result.AudioLevels._15.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._15.LangTagSubstitute1.Index = NullableInt(AudioLevel15LangTagSub1.CurrentValue);
            result.AudioLevels._15.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel15LangTagSub1.CurrentValue);
            result.AudioLevels._15.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._15.LangTagSubstitute2.Index = NullableInt(AudioLevel15LangTagSub2.CurrentValue);
            result.AudioLevels._15.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel15LangTagSub2.CurrentValue);

            result.AudioLevels._15.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._15.TypeTagPrimary.Index = NullableInt(AudioLevel15TypeTag.CurrentValue);
            result.AudioLevels._15.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel15TypeTag.CurrentValue);
            result.AudioLevels._15.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._15.TypeTagSubstitute1.Index = NullableInt(AudioLevel15TypeTagSub1.CurrentValue);
            result.AudioLevels._15.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel15TypeTagSub1.CurrentValue);
            result.AudioLevels._15.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._15.TypeTagSubstitute2.Index = NullableInt(AudioLevel15TypeTagSub2.CurrentValue);
            result.AudioLevels._15.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel15TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 16
            result.AudioLevels._16 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._16.Index = NullableInt(AudioLevel16Index.CurrentValue);

            result.AudioLevels._16.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._16.LangTagPrimary.Index = NullableInt(AudioLevel16LangTag.CurrentValue);
            result.AudioLevels._16.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel16LangTag.CurrentValue);
            result.AudioLevels._16.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._16.LangTagSubstitute1.Index = NullableInt(AudioLevel16LangTagSub1.CurrentValue);
            result.AudioLevels._16.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel16LangTagSub1.CurrentValue);
            result.AudioLevels._16.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._16.LangTagSubstitute2.Index = NullableInt(AudioLevel16LangTagSub2.CurrentValue);
            result.AudioLevels._16.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel16LangTagSub2.CurrentValue);

            result.AudioLevels._16.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._16.TypeTagPrimary.Index = NullableInt(AudioLevel16TypeTag.CurrentValue);
            result.AudioLevels._16.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel16TypeTag.CurrentValue);
            result.AudioLevels._16.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._16.TypeTagSubstitute1.Index = NullableInt(AudioLevel16TypeTagSub1.CurrentValue);
            result.AudioLevels._16.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel16TypeTagSub1.CurrentValue);
            result.AudioLevels._16.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._16.TypeTagSubstitute2.Index = NullableInt(AudioLevel16TypeTagSub2.CurrentValue);
            result.AudioLevels._16.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel16TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 17
            result.AudioLevels._17 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._17.Index = NullableInt(AudioLevel17Index.CurrentValue);

            result.AudioLevels._17.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._17.LangTagPrimary.Index = NullableInt(AudioLevel17LangTag.CurrentValue);
            result.AudioLevels._17.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel17LangTag.CurrentValue);
            result.AudioLevels._17.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._17.LangTagSubstitute1.Index = NullableInt(AudioLevel17LangTagSub1.CurrentValue);
            result.AudioLevels._17.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel17LangTagSub1.CurrentValue);
            result.AudioLevels._17.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._17.LangTagSubstitute2.Index = NullableInt(AudioLevel17LangTagSub2.CurrentValue);
            result.AudioLevels._17.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel17LangTagSub2.CurrentValue);

            result.AudioLevels._17.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._17.TypeTagPrimary.Index = NullableInt(AudioLevel17TypeTag.CurrentValue);
            result.AudioLevels._17.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel17TypeTag.CurrentValue);
            result.AudioLevels._17.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._17.TypeTagSubstitute1.Index = NullableInt(AudioLevel17TypeTagSub1.CurrentValue);
            result.AudioLevels._17.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel17TypeTagSub1.CurrentValue);
            result.AudioLevels._17.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._17.TypeTagSubstitute2.Index = NullableInt(AudioLevel17TypeTagSub2.CurrentValue);
            result.AudioLevels._17.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel17TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 18
            result.AudioLevels._18 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._18.Index = NullableInt(AudioLevel18Index.CurrentValue);

            result.AudioLevels._18.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._18.LangTagPrimary.Index = NullableInt(AudioLevel18LangTag.CurrentValue);
            result.AudioLevels._18.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel18LangTag.CurrentValue);
            result.AudioLevels._18.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._18.LangTagSubstitute1.Index = NullableInt(AudioLevel18LangTagSub1.CurrentValue);
            result.AudioLevels._18.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel18LangTagSub1.CurrentValue);
            result.AudioLevels._18.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._18.LangTagSubstitute2.Index = NullableInt(AudioLevel18LangTagSub2.CurrentValue);
            result.AudioLevels._18.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel18LangTagSub2.CurrentValue);

            result.AudioLevels._18.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._18.TypeTagPrimary.Index = NullableInt(AudioLevel18TypeTag.CurrentValue);
            result.AudioLevels._18.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel18TypeTag.CurrentValue);
            result.AudioLevels._18.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._18.TypeTagSubstitute1.Index = NullableInt(AudioLevel18TypeTagSub1.CurrentValue);
            result.AudioLevels._18.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel18TypeTagSub1.CurrentValue);
            result.AudioLevels._18.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._18.TypeTagSubstitute2.Index = NullableInt(AudioLevel18TypeTagSub2.CurrentValue);
            result.AudioLevels._18.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel18TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 19
            result.AudioLevels._19 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._19.Index = NullableInt(AudioLevel19Index.CurrentValue);

            result.AudioLevels._19.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._19.LangTagPrimary.Index = NullableInt(AudioLevel19LangTag.CurrentValue);
            result.AudioLevels._19.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel19LangTag.CurrentValue);
            result.AudioLevels._19.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._19.LangTagSubstitute1.Index = NullableInt(AudioLevel19LangTagSub1.CurrentValue);
            result.AudioLevels._19.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel19LangTagSub1.CurrentValue);
            result.AudioLevels._19.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._19.LangTagSubstitute2.Index = NullableInt(AudioLevel19LangTagSub2.CurrentValue);
            result.AudioLevels._19.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel19LangTagSub2.CurrentValue);

            result.AudioLevels._19.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._19.TypeTagPrimary.Index = NullableInt(AudioLevel19TypeTag.CurrentValue);
            result.AudioLevels._19.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel19TypeTag.CurrentValue);
            result.AudioLevels._19.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._19.TypeTagSubstitute1.Index = NullableInt(AudioLevel19TypeTagSub1.CurrentValue);
            result.AudioLevels._19.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel19TypeTagSub1.CurrentValue);
            result.AudioLevels._19.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._19.TypeTagSubstitute2.Index = NullableInt(AudioLevel19TypeTagSub2.CurrentValue);
            result.AudioLevels._19.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel19TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 20
            result.AudioLevels._20 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._20.Index = NullableInt(AudioLevel20Index.CurrentValue);

            result.AudioLevels._20.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._20.LangTagPrimary.Index = NullableInt(AudioLevel20LangTag.CurrentValue);
            result.AudioLevels._20.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel20LangTag.CurrentValue);
            result.AudioLevels._20.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._20.LangTagSubstitute1.Index = NullableInt(AudioLevel20LangTagSub1.CurrentValue);
            result.AudioLevels._20.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel20LangTagSub1.CurrentValue);
            result.AudioLevels._20.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._20.LangTagSubstitute2.Index = NullableInt(AudioLevel20LangTagSub2.CurrentValue);
            result.AudioLevels._20.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel20LangTagSub2.CurrentValue);

            result.AudioLevels._20.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._20.TypeTagPrimary.Index = NullableInt(AudioLevel20TypeTag.CurrentValue);
            result.AudioLevels._20.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel20TypeTag.CurrentValue);
            result.AudioLevels._20.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._20.TypeTagSubstitute1.Index = NullableInt(AudioLevel20TypeTagSub1.CurrentValue);
            result.AudioLevels._20.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel20TypeTagSub1.CurrentValue);
            result.AudioLevels._20.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._20.TypeTagSubstitute2.Index = NullableInt(AudioLevel20TypeTagSub2.CurrentValue);
            result.AudioLevels._20.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel20TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 21
            result.AudioLevels._21 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._21.Index = NullableInt(AudioLevel21Index.CurrentValue);

            result.AudioLevels._21.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._21.LangTagPrimary.Index = NullableInt(AudioLevel21LangTag.CurrentValue);
            result.AudioLevels._21.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel21LangTag.CurrentValue);
            result.AudioLevels._21.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._21.LangTagSubstitute1.Index = NullableInt(AudioLevel21LangTagSub1.CurrentValue);
            result.AudioLevels._21.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel21LangTagSub1.CurrentValue);
            result.AudioLevels._21.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._21.LangTagSubstitute2.Index = NullableInt(AudioLevel21LangTagSub2.CurrentValue);
            result.AudioLevels._21.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel21LangTagSub2.CurrentValue);

            result.AudioLevels._21.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._21.TypeTagPrimary.Index = NullableInt(AudioLevel21TypeTag.CurrentValue);
            result.AudioLevels._21.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel21TypeTag.CurrentValue);
            result.AudioLevels._21.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._21.TypeTagSubstitute1.Index = NullableInt(AudioLevel21TypeTagSub1.CurrentValue);
            result.AudioLevels._21.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel21TypeTagSub1.CurrentValue);
            result.AudioLevels._21.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._21.TypeTagSubstitute2.Index = NullableInt(AudioLevel21TypeTagSub2.CurrentValue);
            result.AudioLevels._21.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel21TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 22
            result.AudioLevels._22 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._22.Index = NullableInt(AudioLevel22Index.CurrentValue);

            result.AudioLevels._22.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._22.LangTagPrimary.Index = NullableInt(AudioLevel22LangTag.CurrentValue);
            result.AudioLevels._22.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel22LangTag.CurrentValue);
            result.AudioLevels._22.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._22.LangTagSubstitute1.Index = NullableInt(AudioLevel22LangTagSub1.CurrentValue);
            result.AudioLevels._22.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel22LangTagSub1.CurrentValue);
            result.AudioLevels._22.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._22.LangTagSubstitute2.Index = NullableInt(AudioLevel22LangTagSub2.CurrentValue);
            result.AudioLevels._22.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel22LangTagSub2.CurrentValue);

            result.AudioLevels._22.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._22.TypeTagPrimary.Index = NullableInt(AudioLevel22TypeTag.CurrentValue);
            result.AudioLevels._22.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel22TypeTag.CurrentValue);
            result.AudioLevels._22.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._22.TypeTagSubstitute1.Index = NullableInt(AudioLevel22TypeTagSub1.CurrentValue);
            result.AudioLevels._22.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel22TypeTagSub1.CurrentValue);
            result.AudioLevels._22.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._22.TypeTagSubstitute2.Index = NullableInt(AudioLevel22TypeTagSub2.CurrentValue);
            result.AudioLevels._22.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel22TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 23
            result.AudioLevels._23 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._23.Index = NullableInt(AudioLevel23Index.CurrentValue);

            result.AudioLevels._23.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._23.LangTagPrimary.Index = NullableInt(AudioLevel23LangTag.CurrentValue);
            result.AudioLevels._23.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel23LangTag.CurrentValue);
            result.AudioLevels._23.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._23.LangTagSubstitute1.Index = NullableInt(AudioLevel23LangTagSub1.CurrentValue);
            result.AudioLevels._23.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel23LangTagSub1.CurrentValue);
            result.AudioLevels._23.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._23.LangTagSubstitute2.Index = NullableInt(AudioLevel23LangTagSub2.CurrentValue);
            result.AudioLevels._23.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel23LangTagSub2.CurrentValue);

            result.AudioLevels._23.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._23.TypeTagPrimary.Index = NullableInt(AudioLevel23TypeTag.CurrentValue);
            result.AudioLevels._23.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel23TypeTag.CurrentValue);
            result.AudioLevels._23.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._23.TypeTagSubstitute1.Index = NullableInt(AudioLevel23TypeTagSub1.CurrentValue);
            result.AudioLevels._23.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel23TypeTagSub1.CurrentValue);
            result.AudioLevels._23.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._23.TypeTagSubstitute2.Index = NullableInt(AudioLevel23TypeTagSub2.CurrentValue);
            result.AudioLevels._23.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel23TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 24
            result.AudioLevels._24 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._24.Index = NullableInt(AudioLevel24Index.CurrentValue);

            result.AudioLevels._24.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._24.LangTagPrimary.Index = NullableInt(AudioLevel24LangTag.CurrentValue);
            result.AudioLevels._24.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel24LangTag.CurrentValue);
            result.AudioLevels._24.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._24.LangTagSubstitute1.Index = NullableInt(AudioLevel24LangTagSub1.CurrentValue);
            result.AudioLevels._24.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel24LangTagSub1.CurrentValue);
            result.AudioLevels._24.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._24.LangTagSubstitute2.Index = NullableInt(AudioLevel24LangTagSub2.CurrentValue);
            result.AudioLevels._24.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel24LangTagSub2.CurrentValue);

            result.AudioLevels._24.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._24.TypeTagPrimary.Index = NullableInt(AudioLevel24TypeTag.CurrentValue);
            result.AudioLevels._24.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel24TypeTag.CurrentValue);
            result.AudioLevels._24.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._24.TypeTagSubstitute1.Index = NullableInt(AudioLevel24TypeTagSub1.CurrentValue);
            result.AudioLevels._24.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel24TypeTagSub1.CurrentValue);
            result.AudioLevels._24.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._24.TypeTagSubstitute2.Index = NullableInt(AudioLevel24TypeTagSub2.CurrentValue);
            result.AudioLevels._24.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel24TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 25
            result.AudioLevels._25 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._25.Index = NullableInt(AudioLevel25Index.CurrentValue);

            result.AudioLevels._25.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._25.LangTagPrimary.Index = NullableInt(AudioLevel25LangTag.CurrentValue);
            result.AudioLevels._25.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel25LangTag.CurrentValue);
            result.AudioLevels._25.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._25.LangTagSubstitute1.Index = NullableInt(AudioLevel25LangTagSub1.CurrentValue);
            result.AudioLevels._25.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel25LangTagSub1.CurrentValue);
            result.AudioLevels._25.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._25.LangTagSubstitute2.Index = NullableInt(AudioLevel25LangTagSub2.CurrentValue);
            result.AudioLevels._25.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel25LangTagSub2.CurrentValue);

            result.AudioLevels._25.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._25.TypeTagPrimary.Index = NullableInt(AudioLevel25TypeTag.CurrentValue);
            result.AudioLevels._25.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel25TypeTag.CurrentValue);
            result.AudioLevels._25.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._25.TypeTagSubstitute1.Index = NullableInt(AudioLevel25TypeTagSub1.CurrentValue);
            result.AudioLevels._25.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel25TypeTagSub1.CurrentValue);
            result.AudioLevels._25.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._25.TypeTagSubstitute2.Index = NullableInt(AudioLevel25TypeTagSub2.CurrentValue);
            result.AudioLevels._25.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel25TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 26
            result.AudioLevels._26 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._26.Index = NullableInt(AudioLevel26Index.CurrentValue);

            result.AudioLevels._26.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._26.LangTagPrimary.Index = NullableInt(AudioLevel26LangTag.CurrentValue);
            result.AudioLevels._26.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel26LangTag.CurrentValue);
            result.AudioLevels._26.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._26.LangTagSubstitute1.Index = NullableInt(AudioLevel26LangTagSub1.CurrentValue);
            result.AudioLevels._26.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel26LangTagSub1.CurrentValue);
            result.AudioLevels._26.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._26.LangTagSubstitute2.Index = NullableInt(AudioLevel26LangTagSub2.CurrentValue);
            result.AudioLevels._26.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel26LangTagSub2.CurrentValue);

            result.AudioLevels._26.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._26.TypeTagPrimary.Index = NullableInt(AudioLevel26TypeTag.CurrentValue);
            result.AudioLevels._26.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel26TypeTag.CurrentValue);
            result.AudioLevels._26.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._26.TypeTagSubstitute1.Index = NullableInt(AudioLevel26TypeTagSub1.CurrentValue);
            result.AudioLevels._26.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel26TypeTagSub1.CurrentValue);
            result.AudioLevels._26.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._26.TypeTagSubstitute2.Index = NullableInt(AudioLevel26TypeTagSub2.CurrentValue);
            result.AudioLevels._26.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel26TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 27
            result.AudioLevels._27 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._27.Index = NullableInt(AudioLevel27Index.CurrentValue);

            result.AudioLevels._27.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._27.LangTagPrimary.Index = NullableInt(AudioLevel27LangTag.CurrentValue);
            result.AudioLevels._27.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel27LangTag.CurrentValue);
            result.AudioLevels._27.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._27.LangTagSubstitute1.Index = NullableInt(AudioLevel27LangTagSub1.CurrentValue);
            result.AudioLevels._27.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel27LangTagSub1.CurrentValue);
            result.AudioLevels._27.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._27.LangTagSubstitute2.Index = NullableInt(AudioLevel27LangTagSub2.CurrentValue);
            result.AudioLevels._27.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel27LangTagSub2.CurrentValue);

            result.AudioLevels._27.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._27.TypeTagPrimary.Index = NullableInt(AudioLevel27TypeTag.CurrentValue);
            result.AudioLevels._27.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel27TypeTag.CurrentValue);
            result.AudioLevels._27.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._27.TypeTagSubstitute1.Index = NullableInt(AudioLevel27TypeTagSub1.CurrentValue);
            result.AudioLevels._27.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel27TypeTagSub1.CurrentValue);
            result.AudioLevels._27.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._27.TypeTagSubstitute2.Index = NullableInt(AudioLevel27TypeTagSub2.CurrentValue);
            result.AudioLevels._27.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel27TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 28
            result.AudioLevels._28 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._28.Index = NullableInt(AudioLevel28Index.CurrentValue);

            result.AudioLevels._28.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._28.LangTagPrimary.Index = NullableInt(AudioLevel28LangTag.CurrentValue);
            result.AudioLevels._28.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel28LangTag.CurrentValue);
            result.AudioLevels._28.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._28.LangTagSubstitute1.Index = NullableInt(AudioLevel28LangTagSub1.CurrentValue);
            result.AudioLevels._28.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel28LangTagSub1.CurrentValue);
            result.AudioLevels._28.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._28.LangTagSubstitute2.Index = NullableInt(AudioLevel28LangTagSub2.CurrentValue);
            result.AudioLevels._28.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel28LangTagSub2.CurrentValue);

            result.AudioLevels._28.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._28.TypeTagPrimary.Index = NullableInt(AudioLevel28TypeTag.CurrentValue);
            result.AudioLevels._28.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel28TypeTag.CurrentValue);
            result.AudioLevels._28.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._28.TypeTagSubstitute1.Index = NullableInt(AudioLevel28TypeTagSub1.CurrentValue);
            result.AudioLevels._28.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel28TypeTagSub1.CurrentValue);
            result.AudioLevels._28.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._28.TypeTagSubstitute2.Index = NullableInt(AudioLevel28TypeTagSub2.CurrentValue);
            result.AudioLevels._28.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel28TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 29
            result.AudioLevels._29 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._29.Index = NullableInt(AudioLevel29Index.CurrentValue);

            result.AudioLevels._29.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._29.LangTagPrimary.Index = NullableInt(AudioLevel29LangTag.CurrentValue);
            result.AudioLevels._29.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel29LangTag.CurrentValue);
            result.AudioLevels._29.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._29.LangTagSubstitute1.Index = NullableInt(AudioLevel29LangTagSub1.CurrentValue);
            result.AudioLevels._29.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel29LangTagSub1.CurrentValue);
            result.AudioLevels._29.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._29.LangTagSubstitute2.Index = NullableInt(AudioLevel29LangTagSub2.CurrentValue);
            result.AudioLevels._29.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel29LangTagSub2.CurrentValue);

            result.AudioLevels._29.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._29.TypeTagPrimary.Index = NullableInt(AudioLevel29TypeTag.CurrentValue);
            result.AudioLevels._29.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel29TypeTag.CurrentValue);
            result.AudioLevels._29.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._29.TypeTagSubstitute1.Index = NullableInt(AudioLevel29TypeTagSub1.CurrentValue);
            result.AudioLevels._29.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel29TypeTagSub1.CurrentValue);
            result.AudioLevels._29.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._29.TypeTagSubstitute2.Index = NullableInt(AudioLevel29TypeTagSub2.CurrentValue);
            result.AudioLevels._29.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel29TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 30
            result.AudioLevels._30 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._30.Index = NullableInt(AudioLevel30Index.CurrentValue);

            result.AudioLevels._30.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._30.LangTagPrimary.Index = NullableInt(AudioLevel30LangTag.CurrentValue);
            result.AudioLevels._30.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel30LangTag.CurrentValue);
            result.AudioLevels._30.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._30.LangTagSubstitute1.Index = NullableInt(AudioLevel30LangTagSub1.CurrentValue);
            result.AudioLevels._30.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel30LangTagSub1.CurrentValue);
            result.AudioLevels._30.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._30.LangTagSubstitute2.Index = NullableInt(AudioLevel30LangTagSub2.CurrentValue);
            result.AudioLevels._30.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel30LangTagSub2.CurrentValue);

            result.AudioLevels._30.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._30.TypeTagPrimary.Index = NullableInt(AudioLevel30TypeTag.CurrentValue);
            result.AudioLevels._30.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel30TypeTag.CurrentValue);
            result.AudioLevels._30.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._30.TypeTagSubstitute1.Index = NullableInt(AudioLevel30TypeTagSub1.CurrentValue);
            result.AudioLevels._30.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel30TypeTagSub1.CurrentValue);
            result.AudioLevels._30.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._30.TypeTagSubstitute2.Index = NullableInt(AudioLevel30TypeTagSub2.CurrentValue);
            result.AudioLevels._30.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel30TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 31
            result.AudioLevels._31 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._31.Index = NullableInt(AudioLevel31Index.CurrentValue);

            result.AudioLevels._31.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._31.LangTagPrimary.Index = NullableInt(AudioLevel31LangTag.CurrentValue);
            result.AudioLevels._31.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel31LangTag.CurrentValue);
            result.AudioLevels._31.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._31.LangTagSubstitute1.Index = NullableInt(AudioLevel31LangTagSub1.CurrentValue);
            result.AudioLevels._31.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel31LangTagSub1.CurrentValue);
            result.AudioLevels._31.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._31.LangTagSubstitute2.Index = NullableInt(AudioLevel31LangTagSub2.CurrentValue);
            result.AudioLevels._31.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel31LangTagSub2.CurrentValue);

            result.AudioLevels._31.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._31.TypeTagPrimary.Index = NullableInt(AudioLevel31TypeTag.CurrentValue);
            result.AudioLevels._31.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel31TypeTag.CurrentValue);
            result.AudioLevels._31.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._31.TypeTagSubstitute1.Index = NullableInt(AudioLevel31TypeTagSub1.CurrentValue);
            result.AudioLevels._31.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel31TypeTagSub1.CurrentValue);
            result.AudioLevels._31.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._31.TypeTagSubstitute2.Index = NullableInt(AudioLevel31TypeTagSub2.CurrentValue);
            result.AudioLevels._31.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel31TypeTagSub2.CurrentValue);

            //AUDIO LEVEL 32
            result.AudioLevels._32 = new ett_api_domain.DestinationPackageAudioLevel();
            result.AudioLevels._32.Index = NullableInt(AudioLevel32Index.CurrentValue);

            result.AudioLevels._32.LangTagPrimary = new ett_api_domain.LanguageTag();
            result.AudioLevels._32.LangTagPrimary.Index = NullableInt(AudioLevel32LangTag.CurrentValue);
            result.AudioLevels._32.LangTagPrimary.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel32LangTag.CurrentValue);
            result.AudioLevels._32.LangTagSubstitute1 = new ett_api_domain.LanguageTag();
            result.AudioLevels._32.LangTagSubstitute1.Index = NullableInt(AudioLevel32LangTagSub1.CurrentValue);
            result.AudioLevels._32.LangTagSubstitute1.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel32LangTagSub1.CurrentValue);
            result.AudioLevels._32.LangTagSubstitute2 = new ett_api_domain.LanguageTag();
            result.AudioLevels._32.LangTagSubstitute2.Index = NullableInt(AudioLevel32LangTagSub2.CurrentValue);
            result.AudioLevels._32.LangTagSubstitute2.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioLevel32LangTagSub2.CurrentValue);

            result.AudioLevels._32.TypeTagPrimary = new ett_api_domain.TypeTag();
            result.AudioLevels._32.TypeTagPrimary.Index = NullableInt(AudioLevel32TypeTag.CurrentValue);
            result.AudioLevels._32.TypeTagPrimary.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel32TypeTag.CurrentValue);
            result.AudioLevels._32.TypeTagSubstitute1 = new ett_api_domain.TypeTag();
            result.AudioLevels._32.TypeTagSubstitute1.Index = NullableInt(AudioLevel32TypeTagSub1.CurrentValue);
            result.AudioLevels._32.TypeTagSubstitute1.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel32TypeTagSub1.CurrentValue);
            result.AudioLevels._32.TypeTagSubstitute2 = new ett_api_domain.TypeTag();
            result.AudioLevels._32.TypeTagSubstitute2.Index = NullableInt(AudioLevel32TypeTagSub2.CurrentValue);
            result.AudioLevels._32.TypeTagSubstitute2.Name = Lookup(DbConstants.dbAudioTypeTags, AudioLevel32TypeTagSub2.CurrentValue);

            result.AncLevels = new ett_api_domain.DestinationPackageANCLevels();

            result.AncLevels._1 = new ett_api_domain.DestinationPackageANCLevel();
            result.AncLevels._1.Index = NullableInt(AncLevel01Index.CurrentValue);
            result.AncLevels._1.VideoHubTag = new ett_api_domain.VideoHubTag();
            result.AncLevels._1.VideoHubTag.Index = NullableInt(AncLevel01HubTag.CurrentValue);
            result.AncLevels._1.VideoHubTag.Name = Lookup(DbConstants.dbVideoHubTags, AncLevel01HubTag.CurrentValue);

            result.AncLevels._2 = new ett_api_domain.DestinationPackageANCLevel();
            result.AncLevels._2.Index = NullableInt(AncLevel02Index.CurrentValue);
            result.AncLevels._2.VideoHubTag = new ett_api_domain.VideoHubTag();
            result.AncLevels._2.VideoHubTag.Index = NullableInt(AncLevel02HubTag.CurrentValue);
            result.AncLevels._2.VideoHubTag.Name = Lookup(DbConstants.dbVideoHubTags, AncLevel02HubTag.CurrentValue);

            result.AncLevels._3 = new ett_api_domain.DestinationPackageANCLevel();
            result.AncLevels._3.Index = NullableInt(AncLevel03Index.CurrentValue);
            result.AncLevels._3.VideoHubTag = new ett_api_domain.VideoHubTag();
            result.AncLevels._3.VideoHubTag.Index = NullableInt(AncLevel03HubTag.CurrentValue);
            result.AncLevels._3.VideoHubTag.Name = Lookup(DbConstants.dbVideoHubTags, AncLevel03HubTag.CurrentValue);

            result.AncLevels._4 = new ett_api_domain.DestinationPackageANCLevel();
            result.AncLevels._4.Index = NullableInt(AncLevel04Index.CurrentValue);
            result.AncLevels._4.VideoHubTag = new ett_api_domain.VideoHubTag();
            result.AncLevels._4.VideoHubTag.Index = NullableInt(AncLevel04HubTag.CurrentValue);
            result.AncLevels._4.VideoHubTag.Name = Lookup(DbConstants.dbVideoHubTags, AncLevel04HubTag.CurrentValue);

            result.ReturnAudioLevels = new ett_api_domain.DestinationPackageReturnAudioLevels();

            result.ReturnAudioLevels._1 = new ett_api_domain.DestinationPackageReturnAudioLevel();
            result.ReturnAudioLevels._1.Index = NullableInt(AudioReturnLevel01Index.CurrentValue);
            result.ReturnAudioLevels._1.LangTag = new ett_api_domain.LanguageTag();
            result.ReturnAudioLevels._1.LangTag.Index = NullableInt(AudioReturnLevel01LangTag.CurrentValue);
            result.ReturnAudioLevels._1.LangTag.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioReturnLevel01LangTag.CurrentValue);
            result.ReturnAudioLevels._1.TypeTag = new ett_api_domain.TypeTag();
            result.ReturnAudioLevels._1.TypeTag.Index = NullableInt(AudioReturnLevel01TypeTag.CurrentValue);
            result.ReturnAudioLevels._1.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioReturnLevel01TypeTag.CurrentValue);

            result.ReturnAudioLevels._2 = new ett_api_domain.DestinationPackageReturnAudioLevel();
            result.ReturnAudioLevels._2.Index = NullableInt(AudioReturnLevel02Index.CurrentValue);
            result.ReturnAudioLevels._2.LangTag = new ett_api_domain.LanguageTag();
            result.ReturnAudioLevels._2.LangTag.Index = NullableInt(AudioReturnLevel02LangTag.CurrentValue);
            result.ReturnAudioLevels._2.LangTag.Name = Lookup(DbConstants.dbLanguageUseHubTags, AudioReturnLevel02LangTag.CurrentValue);
            result.ReturnAudioLevels._2.TypeTag = new ett_api_domain.TypeTag();
            result.ReturnAudioLevels._2.TypeTag.Index = NullableInt(AudioReturnLevel02TypeTag.CurrentValue);
            result.ReturnAudioLevels._2.TypeTag.Name = Lookup(DbConstants.dbAudioTypeTags, AudioReturnLevel02TypeTag.CurrentValue);

            result.ReturnVideo = new ett_api_domain.DestinationPackageReverseVideoLevel();
            result.ReturnVideo.Index = NullableInt(ReturnVideoIndex.CurrentValue);

            return result;

        }

        public void SetDocument(ett_api_domain.DestinationPackage package)
        {

            if (package == null) throw new ArgumentNullException();
            if (package.Index != Index) throw new Exception("Index mismatch");

            PackageName.Change(package.PackageName, Enums.ChangeSources.API);
            ButtonName.Change(package.ButtonName, Enums.ChangeSources.API);

            if (package.DelegateTag != null) DelegateTag.Change(NullableInt(package.DelegateTag.Index), Enums.ChangeSources.API);
            else DelegateTag.Change(-1, Enums.ChangeSources.API);

            if (package.OwnerTag != null) OwnerTag.Change(NullableInt(package.OwnerTag.Index), Enums.ChangeSources.API);
            else OwnerTag.Change(-1, Enums.ChangeSources.API);

            if (package.VideoLevels != null && package.VideoLevels._1 != null)
            {
                VideoLevel01Index.Change(NullableInt(package.VideoLevels._1.Index), Enums.ChangeSources.API);
                VideoLevel01HubTag.Change(NullableInt(package.VideoLevels._1.VideoHubTagIndex), Enums.ChangeSources.API);
            }
            else
            {
                VideoLevel01Index.Change(-1, Enums.ChangeSources.API);
                VideoLevel01HubTag.Change(-1, Enums.ChangeSources.API);
            }

            if (package.AudioLevels != null)
            {

                //AUDIO LEVEL 01
                if (package.AudioLevels._1 != null)
                {
                    AudioLevel01Index.Change(NullableInt(package.AudioLevels._1.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._1.LangTagPrimary != null) AudioLevel01LangTag.Change(NullableInt(package.AudioLevels._1.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel01LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._1.LangTagSubstitute1 != null) AudioLevel01LangTagSub1.Change(NullableInt(package.AudioLevels._1.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel01LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._1.LangTagSubstitute2 != null) AudioLevel01LangTagSub2.Change(NullableInt(package.AudioLevels._1.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel01LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._1.TypeTagPrimary != null) AudioLevel01TypeTag.Change(NullableInt(package.AudioLevels._1.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel01TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._1.TypeTagSubstitute1 != null) AudioLevel01TypeTagSub1.Change(NullableInt(package.AudioLevels._1.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel01TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._1.TypeTagSubstitute2 != null) AudioLevel01TypeTagSub2.Change(NullableInt(package.AudioLevels._1.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel01TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel01Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel01LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel01LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel01LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel01TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel01TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel01TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 02
                if (package.AudioLevels._2 != null)
                {
                    AudioLevel02Index.Change(NullableInt(package.AudioLevels._2.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._2.LangTagPrimary != null) AudioLevel02LangTag.Change(NullableInt(package.AudioLevels._2.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel02LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._2.LangTagSubstitute1 != null) AudioLevel02LangTagSub1.Change(NullableInt(package.AudioLevels._2.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel02LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._2.LangTagSubstitute2 != null) AudioLevel02LangTagSub2.Change(NullableInt(package.AudioLevels._2.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel02LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._2.TypeTagPrimary != null) AudioLevel02TypeTag.Change(NullableInt(package.AudioLevels._2.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel02TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._2.TypeTagSubstitute1 != null) AudioLevel02TypeTagSub1.Change(NullableInt(package.AudioLevels._2.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel02TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._2.TypeTagSubstitute2 != null) AudioLevel02TypeTagSub2.Change(NullableInt(package.AudioLevels._2.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel02TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel02Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel02LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel02LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel02LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel02TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel02TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel02TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 03
                if (package.AudioLevels._3 != null)
                {
                    AudioLevel03Index.Change(NullableInt(package.AudioLevels._3.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._3.LangTagPrimary != null) AudioLevel03LangTag.Change(NullableInt(package.AudioLevels._3.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel03LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._3.LangTagSubstitute1 != null) AudioLevel03LangTagSub1.Change(NullableInt(package.AudioLevels._3.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel03LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._3.LangTagSubstitute2 != null) AudioLevel03LangTagSub2.Change(NullableInt(package.AudioLevels._3.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel03LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._3.TypeTagPrimary != null) AudioLevel03TypeTag.Change(NullableInt(package.AudioLevels._3.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel03TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._3.TypeTagSubstitute1 != null) AudioLevel03TypeTagSub1.Change(NullableInt(package.AudioLevels._3.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel03TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._3.TypeTagSubstitute2 != null) AudioLevel03TypeTagSub2.Change(NullableInt(package.AudioLevels._3.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel03TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel03Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel03LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel03LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel03LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel03TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel03TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel03TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 04
                if (package.AudioLevels._4 != null)
                {
                    AudioLevel04Index.Change(NullableInt(package.AudioLevels._4.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._4.LangTagPrimary != null) AudioLevel04LangTag.Change(NullableInt(package.AudioLevels._4.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel04LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._4.LangTagSubstitute1 != null) AudioLevel04LangTagSub1.Change(NullableInt(package.AudioLevels._4.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel04LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._4.LangTagSubstitute2 != null) AudioLevel04LangTagSub2.Change(NullableInt(package.AudioLevels._4.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel04LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._4.TypeTagPrimary != null) AudioLevel04TypeTag.Change(NullableInt(package.AudioLevels._4.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel04TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._4.TypeTagSubstitute1 != null) AudioLevel04TypeTagSub1.Change(NullableInt(package.AudioLevels._4.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel04TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._4.TypeTagSubstitute2 != null) AudioLevel04TypeTagSub2.Change(NullableInt(package.AudioLevels._4.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel04TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel04Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel04LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel04LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel04LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel04TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel04TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel04TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 05
                if (package.AudioLevels._5 != null)
                {
                    AudioLevel05Index.Change(NullableInt(package.AudioLevels._5.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._5.LangTagPrimary != null) AudioLevel05LangTag.Change(NullableInt(package.AudioLevels._5.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel05LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._5.LangTagSubstitute1 != null) AudioLevel05LangTagSub1.Change(NullableInt(package.AudioLevels._5.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel05LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._5.LangTagSubstitute2 != null) AudioLevel05LangTagSub2.Change(NullableInt(package.AudioLevels._5.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel05LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._5.TypeTagPrimary != null) AudioLevel05TypeTag.Change(NullableInt(package.AudioLevels._5.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel05TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._5.TypeTagSubstitute1 != null) AudioLevel05TypeTagSub1.Change(NullableInt(package.AudioLevels._5.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel05TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._5.TypeTagSubstitute2 != null) AudioLevel05TypeTagSub2.Change(NullableInt(package.AudioLevels._5.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel05TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel05Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel05LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel05LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel05LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel05TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel05TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel05TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 06
                if (package.AudioLevels._6 != null)
                {
                    AudioLevel06Index.Change(NullableInt(package.AudioLevels._6.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._6.LangTagPrimary != null) AudioLevel06LangTag.Change(NullableInt(package.AudioLevels._6.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel06LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._6.LangTagSubstitute1 != null) AudioLevel06LangTagSub1.Change(NullableInt(package.AudioLevels._6.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel06LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._6.LangTagSubstitute2 != null) AudioLevel06LangTagSub2.Change(NullableInt(package.AudioLevels._6.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel06LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._6.TypeTagPrimary != null) AudioLevel06TypeTag.Change(NullableInt(package.AudioLevels._6.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel06TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._6.TypeTagSubstitute1 != null) AudioLevel06TypeTagSub1.Change(NullableInt(package.AudioLevels._6.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel06TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._6.TypeTagSubstitute2 != null) AudioLevel06TypeTagSub2.Change(NullableInt(package.AudioLevels._6.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel06TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel06Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel06LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel06LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel06LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel06TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel06TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel06TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 07
                if (package.AudioLevels._7 != null)
                {
                    AudioLevel07Index.Change(NullableInt(package.AudioLevels._7.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._7.LangTagPrimary != null) AudioLevel07LangTag.Change(NullableInt(package.AudioLevels._7.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel07LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._7.LangTagSubstitute1 != null) AudioLevel07LangTagSub1.Change(NullableInt(package.AudioLevels._7.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel07LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._7.LangTagSubstitute2 != null) AudioLevel07LangTagSub2.Change(NullableInt(package.AudioLevels._7.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel07LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._7.TypeTagPrimary != null) AudioLevel07TypeTag.Change(NullableInt(package.AudioLevels._7.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel07TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._7.TypeTagSubstitute1 != null) AudioLevel07TypeTagSub1.Change(NullableInt(package.AudioLevels._7.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel07TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._7.TypeTagSubstitute2 != null) AudioLevel07TypeTagSub2.Change(NullableInt(package.AudioLevels._7.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel07TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel07Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel07LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel07LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel07LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel07TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel07TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel07TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 08
                if (package.AudioLevels._8 != null)
                {
                    AudioLevel08Index.Change(NullableInt(package.AudioLevels._8.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._8.LangTagPrimary != null) AudioLevel08LangTag.Change(NullableInt(package.AudioLevels._8.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel08LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._8.LangTagSubstitute1 != null) AudioLevel08LangTagSub1.Change(NullableInt(package.AudioLevels._8.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel08LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._8.LangTagSubstitute2 != null) AudioLevel08LangTagSub2.Change(NullableInt(package.AudioLevels._8.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel08LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._8.TypeTagPrimary != null) AudioLevel08TypeTag.Change(NullableInt(package.AudioLevels._8.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel08TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._8.TypeTagSubstitute1 != null) AudioLevel08TypeTagSub1.Change(NullableInt(package.AudioLevels._8.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel08TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._8.TypeTagSubstitute2 != null) AudioLevel08TypeTagSub2.Change(NullableInt(package.AudioLevels._8.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel08TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel08Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel08LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel08LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel08LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel08TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel08TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel08TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 09
                if (package.AudioLevels._9 != null)
                {
                    AudioLevel09Index.Change(NullableInt(package.AudioLevels._9.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._9.LangTagPrimary != null) AudioLevel09LangTag.Change(NullableInt(package.AudioLevels._9.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel09LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._9.LangTagSubstitute1 != null) AudioLevel09LangTagSub1.Change(NullableInt(package.AudioLevels._9.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel09LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._9.LangTagSubstitute2 != null) AudioLevel09LangTagSub2.Change(NullableInt(package.AudioLevels._9.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel09LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._9.TypeTagPrimary != null) AudioLevel09TypeTag.Change(NullableInt(package.AudioLevels._9.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel09TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._9.TypeTagSubstitute1 != null) AudioLevel09TypeTagSub1.Change(NullableInt(package.AudioLevels._9.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel09TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._9.TypeTagSubstitute2 != null) AudioLevel09TypeTagSub2.Change(NullableInt(package.AudioLevels._9.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel09TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel09Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel09LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel09LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel09LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel09TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel09TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel09TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 10
                if (package.AudioLevels._10 != null)
                {
                    AudioLevel10Index.Change(NullableInt(package.AudioLevels._10.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._10.LangTagPrimary != null) AudioLevel10LangTag.Change(NullableInt(package.AudioLevels._10.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel10LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._10.LangTagSubstitute1 != null) AudioLevel10LangTagSub1.Change(NullableInt(package.AudioLevels._10.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel10LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._10.LangTagSubstitute2 != null) AudioLevel10LangTagSub2.Change(NullableInt(package.AudioLevels._10.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel10LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._10.TypeTagPrimary != null) AudioLevel10TypeTag.Change(NullableInt(package.AudioLevels._10.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel10TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._10.TypeTagSubstitute1 != null) AudioLevel10TypeTagSub1.Change(NullableInt(package.AudioLevels._10.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel10TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._10.TypeTagSubstitute2 != null) AudioLevel10TypeTagSub2.Change(NullableInt(package.AudioLevels._10.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel10TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel10Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel10LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel10LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel10LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel10TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel10TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel10TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 11
                if (package.AudioLevels._11 != null)
                {
                    AudioLevel11Index.Change(NullableInt(package.AudioLevels._11.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._11.LangTagPrimary != null) AudioLevel11LangTag.Change(NullableInt(package.AudioLevels._11.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel11LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._11.LangTagSubstitute1 != null) AudioLevel11LangTagSub1.Change(NullableInt(package.AudioLevels._11.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel11LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._11.LangTagSubstitute2 != null) AudioLevel11LangTagSub2.Change(NullableInt(package.AudioLevels._11.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel11LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._11.TypeTagPrimary != null) AudioLevel11TypeTag.Change(NullableInt(package.AudioLevels._11.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel11TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._11.TypeTagSubstitute1 != null) AudioLevel11TypeTagSub1.Change(NullableInt(package.AudioLevels._11.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel11TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._11.TypeTagSubstitute2 != null) AudioLevel11TypeTagSub2.Change(NullableInt(package.AudioLevels._11.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel11TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel11Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel11LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel11LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel11LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel11TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel11TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel11TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 12
                if (package.AudioLevels._12 != null)
                {
                    AudioLevel12Index.Change(NullableInt(package.AudioLevels._12.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._12.LangTagPrimary != null) AudioLevel12LangTag.Change(NullableInt(package.AudioLevels._12.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel12LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._12.LangTagSubstitute1 != null) AudioLevel12LangTagSub1.Change(NullableInt(package.AudioLevels._12.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel12LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._12.LangTagSubstitute2 != null) AudioLevel12LangTagSub2.Change(NullableInt(package.AudioLevels._12.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel12LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._12.TypeTagPrimary != null) AudioLevel12TypeTag.Change(NullableInt(package.AudioLevels._12.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel12TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._12.TypeTagSubstitute1 != null) AudioLevel12TypeTagSub1.Change(NullableInt(package.AudioLevels._12.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel12TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._12.TypeTagSubstitute2 != null) AudioLevel12TypeTagSub2.Change(NullableInt(package.AudioLevels._12.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel12TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel12Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel12LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel12LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel12LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel12TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel12TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel12TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 13
                if (package.AudioLevels._13 != null)
                {
                    AudioLevel13Index.Change(NullableInt(package.AudioLevels._13.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._13.LangTagPrimary != null) AudioLevel13LangTag.Change(NullableInt(package.AudioLevels._13.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel13LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._13.LangTagSubstitute1 != null) AudioLevel13LangTagSub1.Change(NullableInt(package.AudioLevels._13.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel13LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._13.LangTagSubstitute2 != null) AudioLevel13LangTagSub2.Change(NullableInt(package.AudioLevels._13.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel13LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._13.TypeTagPrimary != null) AudioLevel13TypeTag.Change(NullableInt(package.AudioLevels._13.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel13TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._13.TypeTagSubstitute1 != null) AudioLevel13TypeTagSub1.Change(NullableInt(package.AudioLevels._13.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel13TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._13.TypeTagSubstitute2 != null) AudioLevel13TypeTagSub2.Change(NullableInt(package.AudioLevels._13.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel13TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel13Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel13LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel13LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel13LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel13TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel13TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel13TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 14
                if (package.AudioLevels._14 != null)
                {
                    AudioLevel14Index.Change(NullableInt(package.AudioLevels._14.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._14.LangTagPrimary != null) AudioLevel14LangTag.Change(NullableInt(package.AudioLevels._14.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel14LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._14.LangTagSubstitute1 != null) AudioLevel14LangTagSub1.Change(NullableInt(package.AudioLevels._14.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel14LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._14.LangTagSubstitute2 != null) AudioLevel14LangTagSub2.Change(NullableInt(package.AudioLevels._14.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel14LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._14.TypeTagPrimary != null) AudioLevel14TypeTag.Change(NullableInt(package.AudioLevels._14.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel14TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._14.TypeTagSubstitute1 != null) AudioLevel14TypeTagSub1.Change(NullableInt(package.AudioLevels._14.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel14TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._14.TypeTagSubstitute2 != null) AudioLevel14TypeTagSub2.Change(NullableInt(package.AudioLevels._14.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel14TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel14Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel14LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel14LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel14LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel14TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel14TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel14TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 15
                if (package.AudioLevels._15 != null)
                {
                    AudioLevel15Index.Change(NullableInt(package.AudioLevels._15.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._15.LangTagPrimary != null) AudioLevel15LangTag.Change(NullableInt(package.AudioLevels._15.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel15LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._15.LangTagSubstitute1 != null) AudioLevel15LangTagSub1.Change(NullableInt(package.AudioLevels._15.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel15LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._15.LangTagSubstitute2 != null) AudioLevel15LangTagSub2.Change(NullableInt(package.AudioLevels._15.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel15LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._15.TypeTagPrimary != null) AudioLevel15TypeTag.Change(NullableInt(package.AudioLevels._15.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel15TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._15.TypeTagSubstitute1 != null) AudioLevel15TypeTagSub1.Change(NullableInt(package.AudioLevels._15.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel15TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._15.TypeTagSubstitute2 != null) AudioLevel15TypeTagSub2.Change(NullableInt(package.AudioLevels._15.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel15TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel15Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel15LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel15LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel15LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel15TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel15TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel15TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 16
                if (package.AudioLevels._16 != null)
                {
                    AudioLevel16Index.Change(NullableInt(package.AudioLevels._16.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._16.LangTagPrimary != null) AudioLevel16LangTag.Change(NullableInt(package.AudioLevels._16.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel16LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._16.LangTagSubstitute1 != null) AudioLevel16LangTagSub1.Change(NullableInt(package.AudioLevels._16.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel16LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._16.LangTagSubstitute2 != null) AudioLevel16LangTagSub2.Change(NullableInt(package.AudioLevels._16.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel16LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._16.TypeTagPrimary != null) AudioLevel16TypeTag.Change(NullableInt(package.AudioLevels._16.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel16TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._16.TypeTagSubstitute1 != null) AudioLevel16TypeTagSub1.Change(NullableInt(package.AudioLevels._16.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel16TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._16.TypeTagSubstitute2 != null) AudioLevel16TypeTagSub2.Change(NullableInt(package.AudioLevels._16.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel16TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel16Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel16LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel16LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel16LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel16TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel16TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel16TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 17
                if (package.AudioLevels._17 != null)
                {
                    AudioLevel17Index.Change(NullableInt(package.AudioLevels._17.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._17.LangTagPrimary != null) AudioLevel17LangTag.Change(NullableInt(package.AudioLevels._17.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel17LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._17.LangTagSubstitute1 != null) AudioLevel17LangTagSub1.Change(NullableInt(package.AudioLevels._17.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel17LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._17.LangTagSubstitute2 != null) AudioLevel17LangTagSub2.Change(NullableInt(package.AudioLevels._17.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel17LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._17.TypeTagPrimary != null) AudioLevel17TypeTag.Change(NullableInt(package.AudioLevels._17.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel17TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._17.TypeTagSubstitute1 != null) AudioLevel17TypeTagSub1.Change(NullableInt(package.AudioLevels._17.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel17TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._17.TypeTagSubstitute2 != null) AudioLevel17TypeTagSub2.Change(NullableInt(package.AudioLevels._17.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel17TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel17Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel17LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel17LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel17LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel17TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel17TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel17TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 18
                if (package.AudioLevels._18 != null)
                {
                    AudioLevel18Index.Change(NullableInt(package.AudioLevels._18.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._18.LangTagPrimary != null) AudioLevel18LangTag.Change(NullableInt(package.AudioLevels._18.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel18LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._18.LangTagSubstitute1 != null) AudioLevel18LangTagSub1.Change(NullableInt(package.AudioLevels._18.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel18LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._18.LangTagSubstitute2 != null) AudioLevel18LangTagSub2.Change(NullableInt(package.AudioLevels._18.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel18LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._18.TypeTagPrimary != null) AudioLevel18TypeTag.Change(NullableInt(package.AudioLevels._18.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel18TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._18.TypeTagSubstitute1 != null) AudioLevel18TypeTagSub1.Change(NullableInt(package.AudioLevels._18.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel18TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._18.TypeTagSubstitute2 != null) AudioLevel18TypeTagSub2.Change(NullableInt(package.AudioLevels._18.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel18TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel18Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel18LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel18LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel18LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel18TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel18TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel18TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 19
                if (package.AudioLevels._19 != null)
                {
                    AudioLevel19Index.Change(NullableInt(package.AudioLevels._19.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._19.LangTagPrimary != null) AudioLevel19LangTag.Change(NullableInt(package.AudioLevels._19.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel19LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._19.LangTagSubstitute1 != null) AudioLevel19LangTagSub1.Change(NullableInt(package.AudioLevels._19.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel19LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._19.LangTagSubstitute2 != null) AudioLevel19LangTagSub2.Change(NullableInt(package.AudioLevels._19.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel19LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._19.TypeTagPrimary != null) AudioLevel19TypeTag.Change(NullableInt(package.AudioLevels._19.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel19TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._19.TypeTagSubstitute1 != null) AudioLevel19TypeTagSub1.Change(NullableInt(package.AudioLevels._19.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel19TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._19.TypeTagSubstitute2 != null) AudioLevel19TypeTagSub2.Change(NullableInt(package.AudioLevels._19.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel19TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel19Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel19LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel19LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel19LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel19TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel19TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel19TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 20
                if (package.AudioLevels._20 != null)
                {
                    AudioLevel20Index.Change(NullableInt(package.AudioLevels._20.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._20.LangTagPrimary != null) AudioLevel20LangTag.Change(NullableInt(package.AudioLevels._20.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel20LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._20.LangTagSubstitute1 != null) AudioLevel20LangTagSub1.Change(NullableInt(package.AudioLevels._20.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel20LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._20.LangTagSubstitute2 != null) AudioLevel20LangTagSub2.Change(NullableInt(package.AudioLevels._20.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel20LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._20.TypeTagPrimary != null) AudioLevel20TypeTag.Change(NullableInt(package.AudioLevels._20.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel20TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._20.TypeTagSubstitute1 != null) AudioLevel20TypeTagSub1.Change(NullableInt(package.AudioLevels._20.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel20TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._20.TypeTagSubstitute2 != null) AudioLevel20TypeTagSub2.Change(NullableInt(package.AudioLevels._20.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel20TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel20Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel20LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel20LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel20LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel20TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel20TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel20TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 21
                if (package.AudioLevels._21 != null)
                {
                    AudioLevel21Index.Change(NullableInt(package.AudioLevels._21.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._21.LangTagPrimary != null) AudioLevel21LangTag.Change(NullableInt(package.AudioLevels._21.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel21LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._21.LangTagSubstitute1 != null) AudioLevel21LangTagSub1.Change(NullableInt(package.AudioLevels._21.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel21LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._21.LangTagSubstitute2 != null) AudioLevel21LangTagSub2.Change(NullableInt(package.AudioLevels._21.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel21LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._21.TypeTagPrimary != null) AudioLevel21TypeTag.Change(NullableInt(package.AudioLevels._21.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel21TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._21.TypeTagSubstitute1 != null) AudioLevel21TypeTagSub1.Change(NullableInt(package.AudioLevels._21.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel21TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._21.TypeTagSubstitute2 != null) AudioLevel21TypeTagSub2.Change(NullableInt(package.AudioLevels._21.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel21TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel21Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel21LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel21LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel21LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel21TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel21TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel21TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 22
                if (package.AudioLevels._22 != null)
                {
                    AudioLevel22Index.Change(NullableInt(package.AudioLevels._22.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._22.LangTagPrimary != null) AudioLevel22LangTag.Change(NullableInt(package.AudioLevels._22.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel22LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._22.LangTagSubstitute1 != null) AudioLevel22LangTagSub1.Change(NullableInt(package.AudioLevels._22.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel22LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._22.LangTagSubstitute2 != null) AudioLevel22LangTagSub2.Change(NullableInt(package.AudioLevels._22.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel22LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._22.TypeTagPrimary != null) AudioLevel22TypeTag.Change(NullableInt(package.AudioLevels._22.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel22TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._22.TypeTagSubstitute1 != null) AudioLevel22TypeTagSub1.Change(NullableInt(package.AudioLevels._22.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel22TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._22.TypeTagSubstitute2 != null) AudioLevel22TypeTagSub2.Change(NullableInt(package.AudioLevels._22.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel22TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel22Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel22LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel22LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel22LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel22TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel22TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel22TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 23
                if (package.AudioLevels._23 != null)
                {
                    AudioLevel23Index.Change(NullableInt(package.AudioLevels._23.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._23.LangTagPrimary != null) AudioLevel23LangTag.Change(NullableInt(package.AudioLevels._23.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel23LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._23.LangTagSubstitute1 != null) AudioLevel23LangTagSub1.Change(NullableInt(package.AudioLevels._23.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel23LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._23.LangTagSubstitute2 != null) AudioLevel23LangTagSub2.Change(NullableInt(package.AudioLevels._23.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel23LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._23.TypeTagPrimary != null) AudioLevel23TypeTag.Change(NullableInt(package.AudioLevels._23.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel23TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._23.TypeTagSubstitute1 != null) AudioLevel23TypeTagSub1.Change(NullableInt(package.AudioLevels._23.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel23TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._23.TypeTagSubstitute2 != null) AudioLevel23TypeTagSub2.Change(NullableInt(package.AudioLevels._23.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel23TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel23Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel23LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel23LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel23LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel23TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel23TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel23TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 24
                if (package.AudioLevels._24 != null)
                {
                    AudioLevel24Index.Change(NullableInt(package.AudioLevels._24.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._24.LangTagPrimary != null) AudioLevel24LangTag.Change(NullableInt(package.AudioLevels._24.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel24LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._24.LangTagSubstitute1 != null) AudioLevel24LangTagSub1.Change(NullableInt(package.AudioLevels._24.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel24LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._24.LangTagSubstitute2 != null) AudioLevel24LangTagSub2.Change(NullableInt(package.AudioLevels._24.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel24LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._24.TypeTagPrimary != null) AudioLevel24TypeTag.Change(NullableInt(package.AudioLevels._24.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel24TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._24.TypeTagSubstitute1 != null) AudioLevel24TypeTagSub1.Change(NullableInt(package.AudioLevels._24.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel24TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._24.TypeTagSubstitute2 != null) AudioLevel24TypeTagSub2.Change(NullableInt(package.AudioLevels._24.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel24TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel24Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel24LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel24LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel24LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel24TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel24TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel24TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 25
                if (package.AudioLevels._25 != null)
                {
                    AudioLevel25Index.Change(NullableInt(package.AudioLevels._25.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._25.LangTagPrimary != null) AudioLevel25LangTag.Change(NullableInt(package.AudioLevels._25.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel25LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._25.LangTagSubstitute1 != null) AudioLevel25LangTagSub1.Change(NullableInt(package.AudioLevels._25.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel25LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._25.LangTagSubstitute2 != null) AudioLevel25LangTagSub2.Change(NullableInt(package.AudioLevels._25.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel25LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._25.TypeTagPrimary != null) AudioLevel25TypeTag.Change(NullableInt(package.AudioLevels._25.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel25TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._25.TypeTagSubstitute1 != null) AudioLevel25TypeTagSub1.Change(NullableInt(package.AudioLevels._25.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel25TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._25.TypeTagSubstitute2 != null) AudioLevel25TypeTagSub2.Change(NullableInt(package.AudioLevels._25.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel25TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel25Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel25LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel25LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel25LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel25TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel25TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel25TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 26
                if (package.AudioLevels._26 != null)
                {
                    AudioLevel26Index.Change(NullableInt(package.AudioLevels._26.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._26.LangTagPrimary != null) AudioLevel26LangTag.Change(NullableInt(package.AudioLevels._26.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel26LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._26.LangTagSubstitute1 != null) AudioLevel26LangTagSub1.Change(NullableInt(package.AudioLevels._26.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel26LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._26.LangTagSubstitute2 != null) AudioLevel26LangTagSub2.Change(NullableInt(package.AudioLevels._26.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel26LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._26.TypeTagPrimary != null) AudioLevel26TypeTag.Change(NullableInt(package.AudioLevels._26.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel26TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._26.TypeTagSubstitute1 != null) AudioLevel26TypeTagSub1.Change(NullableInt(package.AudioLevels._26.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel26TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._26.TypeTagSubstitute2 != null) AudioLevel26TypeTagSub2.Change(NullableInt(package.AudioLevels._26.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel26TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel26Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel26LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel26LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel26LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel26TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel26TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel26TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 27
                if (package.AudioLevels._27 != null)
                {
                    AudioLevel27Index.Change(NullableInt(package.AudioLevels._27.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._27.LangTagPrimary != null) AudioLevel27LangTag.Change(NullableInt(package.AudioLevels._27.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel27LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._27.LangTagSubstitute1 != null) AudioLevel27LangTagSub1.Change(NullableInt(package.AudioLevels._27.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel27LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._27.LangTagSubstitute2 != null) AudioLevel27LangTagSub2.Change(NullableInt(package.AudioLevels._27.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel27LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._27.TypeTagPrimary != null) AudioLevel27TypeTag.Change(NullableInt(package.AudioLevels._27.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel27TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._27.TypeTagSubstitute1 != null) AudioLevel27TypeTagSub1.Change(NullableInt(package.AudioLevels._27.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel27TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._27.TypeTagSubstitute2 != null) AudioLevel27TypeTagSub2.Change(NullableInt(package.AudioLevels._27.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel27TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel27Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel27LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel27LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel27LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel27TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel27TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel27TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 28
                if (package.AudioLevels._28 != null)
                {
                    AudioLevel28Index.Change(NullableInt(package.AudioLevels._28.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._28.LangTagPrimary != null) AudioLevel28LangTag.Change(NullableInt(package.AudioLevels._28.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel28LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._28.LangTagSubstitute1 != null) AudioLevel28LangTagSub1.Change(NullableInt(package.AudioLevels._28.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel28LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._28.LangTagSubstitute2 != null) AudioLevel28LangTagSub2.Change(NullableInt(package.AudioLevels._28.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel28LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._28.TypeTagPrimary != null) AudioLevel28TypeTag.Change(NullableInt(package.AudioLevels._28.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel28TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._28.TypeTagSubstitute1 != null) AudioLevel28TypeTagSub1.Change(NullableInt(package.AudioLevels._28.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel28TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._28.TypeTagSubstitute2 != null) AudioLevel28TypeTagSub2.Change(NullableInt(package.AudioLevels._28.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel28TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel28Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel28LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel28LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel28LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel28TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel28TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel28TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 29
                if (package.AudioLevels._29 != null)
                {
                    AudioLevel29Index.Change(NullableInt(package.AudioLevels._29.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._29.LangTagPrimary != null) AudioLevel29LangTag.Change(NullableInt(package.AudioLevels._29.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel29LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._29.LangTagSubstitute1 != null) AudioLevel29LangTagSub1.Change(NullableInt(package.AudioLevels._29.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel29LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._29.LangTagSubstitute2 != null) AudioLevel29LangTagSub2.Change(NullableInt(package.AudioLevels._29.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel29LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._29.TypeTagPrimary != null) AudioLevel29TypeTag.Change(NullableInt(package.AudioLevels._29.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel29TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._29.TypeTagSubstitute1 != null) AudioLevel29TypeTagSub1.Change(NullableInt(package.AudioLevels._29.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel29TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._29.TypeTagSubstitute2 != null) AudioLevel29TypeTagSub2.Change(NullableInt(package.AudioLevels._29.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel29TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel29Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel29LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel29LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel29LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel29TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel29TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel29TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 30
                if (package.AudioLevels._30 != null)
                {
                    AudioLevel30Index.Change(NullableInt(package.AudioLevels._30.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._30.LangTagPrimary != null) AudioLevel30LangTag.Change(NullableInt(package.AudioLevels._30.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel30LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._30.LangTagSubstitute1 != null) AudioLevel30LangTagSub1.Change(NullableInt(package.AudioLevels._30.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel30LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._30.LangTagSubstitute2 != null) AudioLevel30LangTagSub2.Change(NullableInt(package.AudioLevels._30.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel30LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._30.TypeTagPrimary != null) AudioLevel30TypeTag.Change(NullableInt(package.AudioLevels._30.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel30TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._30.TypeTagSubstitute1 != null) AudioLevel30TypeTagSub1.Change(NullableInt(package.AudioLevels._30.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel30TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._30.TypeTagSubstitute2 != null) AudioLevel30TypeTagSub2.Change(NullableInt(package.AudioLevels._30.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel30TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel30Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel30LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel30LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel30LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel30TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel30TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel30TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 31
                if (package.AudioLevels._31 != null)
                {
                    AudioLevel31Index.Change(NullableInt(package.AudioLevels._31.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._31.LangTagPrimary != null) AudioLevel31LangTag.Change(NullableInt(package.AudioLevels._31.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel31LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._31.LangTagSubstitute1 != null) AudioLevel31LangTagSub1.Change(NullableInt(package.AudioLevels._31.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel31LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._31.LangTagSubstitute2 != null) AudioLevel31LangTagSub2.Change(NullableInt(package.AudioLevels._31.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel31LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._31.TypeTagPrimary != null) AudioLevel31TypeTag.Change(NullableInt(package.AudioLevels._31.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel31TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._31.TypeTagSubstitute1 != null) AudioLevel31TypeTagSub1.Change(NullableInt(package.AudioLevels._31.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel31TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._31.TypeTagSubstitute2 != null) AudioLevel31TypeTagSub2.Change(NullableInt(package.AudioLevels._31.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel31TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel31Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel31LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel31LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel31LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel31TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel31TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel31TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

                //AUDIO LEVEL 32
                if (package.AudioLevels._32 != null)
                {
                    AudioLevel32Index.Change(NullableInt(package.AudioLevels._32.Index), Enums.ChangeSources.API);

                    if (package.AudioLevels._32.LangTagPrimary != null) AudioLevel32LangTag.Change(NullableInt(package.AudioLevels._32.LangTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel32LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._32.LangTagSubstitute1 != null) AudioLevel32LangTagSub1.Change(NullableInt(package.AudioLevels._32.LangTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel32LangTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._32.LangTagSubstitute2 != null) AudioLevel32LangTagSub2.Change(NullableInt(package.AudioLevels._32.LangTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel32LangTagSub2.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._32.TypeTagPrimary != null) AudioLevel32TypeTag.Change(NullableInt(package.AudioLevels._32.TypeTagPrimary.Index), Enums.ChangeSources.API);
                    else AudioLevel32TypeTag.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._32.TypeTagSubstitute1 != null) AudioLevel32TypeTagSub1.Change(NullableInt(package.AudioLevels._32.TypeTagSubstitute1.Index), Enums.ChangeSources.API);
                    else AudioLevel32TypeTagSub1.Change(-1, Enums.ChangeSources.API);

                    if (package.AudioLevels._32.TypeTagSubstitute2 != null) AudioLevel32TypeTagSub2.Change(NullableInt(package.AudioLevels._32.TypeTagSubstitute2.Index), Enums.ChangeSources.API);
                    else AudioLevel32TypeTagSub2.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioLevel32Index.Change(-1, Enums.ChangeSources.API);
                    AudioLevel32LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel32LangTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel32LangTagSub2.Change(-1, Enums.ChangeSources.API);
                    AudioLevel32TypeTag.Change(-1, Enums.ChangeSources.API);
                    AudioLevel32TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                    AudioLevel32TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                }

            }
            else
            {
                AudioLevel01Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel01LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel01LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel01LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel01TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel01TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel01TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel02Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel02LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel02LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel02LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel02TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel02TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel02TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel03Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel03LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel03LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel03LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel03TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel03TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel03TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel04Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel04LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel04LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel04LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel04TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel04TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel04TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel05Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel05LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel05LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel05LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel05TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel05TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel05TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel06Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel06LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel06LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel06LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel06TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel06TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel06TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel07Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel07LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel07LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel07LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel07TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel07TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel07TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel08Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel08LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel08LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel08LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel08TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel08TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel08TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel09Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel09LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel09LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel09LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel09TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel09TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel09TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel10Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel10LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel10LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel10LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel10TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel10TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel10TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel11Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel11LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel11LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel11LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel11TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel11TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel11TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel12Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel12LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel12LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel12LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel12TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel12TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel12TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel13Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel13LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel13LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel13LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel13TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel13TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel13TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel14Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel14LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel14LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel14LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel14TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel14TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel14TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel15Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel15LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel15LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel15LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel15TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel15TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel15TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel16Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel16LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel16LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel16LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel16TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel16TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel16TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel17Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel17LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel17LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel17LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel17TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel17TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel17TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel18Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel18LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel18LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel18LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel18TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel18TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel18TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel19Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel19LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel19LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel19LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel19TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel19TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel19TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel20Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel20LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel20LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel20LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel20TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel20TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel20TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel21Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel21LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel21LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel21LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel21TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel21TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel21TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel22Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel22LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel22LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel22LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel22TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel22TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel22TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel23Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel23LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel23LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel23LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel23TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel23TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel23TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel24Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel24LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel24LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel24LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel24TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel24TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel24TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel25Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel25LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel25LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel25LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel25TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel25TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel25TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel26Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel26LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel26LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel26LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel26TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel26TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel26TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel27Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel27LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel27LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel27LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel27TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel27TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel27TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel28Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel28LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel28LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel28LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel28TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel28TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel28TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel29Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel29LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel29LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel29LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel29TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel29TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel29TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel30Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel30LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel30LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel30LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel30TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel30TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel30TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel31Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel31LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel31LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel31LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel31TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel31TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel31TypeTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel32Index.Change(-1, Enums.ChangeSources.API);
                AudioLevel32LangTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel32LangTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel32LangTagSub2.Change(-1, Enums.ChangeSources.API);
                AudioLevel32TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioLevel32TypeTagSub1.Change(-1, Enums.ChangeSources.API);
                AudioLevel32TypeTagSub2.Change(-1, Enums.ChangeSources.API);

            }

            if (package.AncLevels != null)
            {

                if (package.AncLevels._1 != null)
                {
                    AncLevel01Index.Change(NullableInt(package.AncLevels._1.Index), Enums.ChangeSources.API);

                    if (package.AncLevels._1.VideoHubTag != null) AncLevel01HubTag.Change(NullableInt(package.AncLevels._1.VideoHubTag.Index), Enums.ChangeSources.API);
                    else AncLevel01HubTag.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AncLevel01Index.Change(-1, Enums.ChangeSources.API);
                    AncLevel01HubTag.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AncLevels._2 != null)
                {
                    AncLevel02Index.Change(NullableInt(package.AncLevels._2.Index), Enums.ChangeSources.API);

                    if (package.AncLevels._2.VideoHubTag != null) AncLevel02HubTag.Change(NullableInt(package.AncLevels._2.VideoHubTag.Index), Enums.ChangeSources.API);
                    else AncLevel02HubTag.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AncLevel02Index.Change(-1, Enums.ChangeSources.API);
                    AncLevel02HubTag.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AncLevels._3 != null)
                {
                    AncLevel03Index.Change(NullableInt(package.AncLevels._3.Index), Enums.ChangeSources.API);

                    if (package.AncLevels._3.VideoHubTag != null) AncLevel03HubTag.Change(NullableInt(package.AncLevels._3.VideoHubTag.Index), Enums.ChangeSources.API);
                    else AncLevel03HubTag.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AncLevel03Index.Change(-1, Enums.ChangeSources.API);
                    AncLevel03HubTag.Change(-1, Enums.ChangeSources.API);
                }

                if (package.AncLevels._4 != null)
                {
                    AncLevel04Index.Change(NullableInt(package.AncLevels._4.Index), Enums.ChangeSources.API);

                    if (package.AncLevels._4.VideoHubTag != null) AncLevel04HubTag.Change(NullableInt(package.AncLevels._4.VideoHubTag.Index), Enums.ChangeSources.API);
                    else AncLevel04HubTag.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AncLevel04Index.Change(-1, Enums.ChangeSources.API);
                    AncLevel04HubTag.Change(-1, Enums.ChangeSources.API);
                }

            }
            else
            {
                AncLevel01Index.Change(-1, Enums.ChangeSources.API);
                AncLevel01HubTag.Change(-1, Enums.ChangeSources.API);
                AncLevel02Index.Change(-1, Enums.ChangeSources.API);
                AncLevel02HubTag.Change(-1, Enums.ChangeSources.API);
                AncLevel03Index.Change(-1, Enums.ChangeSources.API);
                AncLevel03HubTag.Change(-1, Enums.ChangeSources.API);
                AncLevel04Index.Change(-1, Enums.ChangeSources.API);
                AncLevel04HubTag.Change(-1, Enums.ChangeSources.API);
            }

            if (package.ReturnAudioLevels != null)
            {
                
                if (package.ReturnAudioLevels._1 != null)
                {

                    AudioReturnLevel01Index.Change(NullableInt(package.ReturnAudioLevels._1.Index), Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._1.LangTag != null) AudioReturnLevel01LangTag.Change(NullableInt(package.ReturnAudioLevels._1.LangTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevel01LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._1.TypeTag != null) AudioReturnLevel01TypeTag.Change(NullableInt(package.ReturnAudioLevels._1.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevel01TypeTag.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioReturnLevel01Index.Change(-1, Enums.ChangeSources.API);
                    AudioReturnLevel01LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioReturnLevel01TypeTag.Change(-1, Enums.ChangeSources.API);
                }

                if (package.ReturnAudioLevels._2 != null)
                {

                    AudioReturnLevel02Index.Change(NullableInt(package.ReturnAudioLevels._2.Index), Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._2.LangTag != null) AudioReturnLevel02LangTag.Change(NullableInt(package.ReturnAudioLevels._2.LangTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevel02LangTag.Change(-1, Enums.ChangeSources.API);

                    if (package.ReturnAudioLevels._2.TypeTag != null) AudioReturnLevel02TypeTag.Change(NullableInt(package.ReturnAudioLevels._2.TypeTag.Index), Enums.ChangeSources.API);
                    else AudioReturnLevel02TypeTag.Change(-1, Enums.ChangeSources.API);

                }
                else
                {
                    AudioReturnLevel02Index.Change(-1, Enums.ChangeSources.API);
                    AudioReturnLevel02LangTag.Change(-1, Enums.ChangeSources.API);
                    AudioReturnLevel02TypeTag.Change(-1, Enums.ChangeSources.API);
                }

            }
            else
            {
                AudioReturnLevel01Index.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevel01LangTag.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevel01TypeTag.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevel02Index.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevel02LangTag.Change(-1, Enums.ChangeSources.API);
                AudioReturnLevel02TypeTag.Change(-1, Enums.ChangeSources.API);
            }

            if (package.ReturnVideo != null)
            {
                ReturnVideoIndex.Change(NullableInt(package.ReturnVideo.Index), Enums.ChangeSources.API);
            }
            else
            {
                ReturnVideoIndex.Change(-1, Enums.ChangeSources.API);
            }

        }

        public bool BncsChangeLockValue(bool Locked)
        {

            LockStatus.Change(Locked, Enums.ChangeSources.BNCS);

            //a db change should process a notification 5 seconds after the last change is received
            if (dbChangeTimer.Enabled) dbChangeTimer.Stop();
            AcceptAllBncs();  //call this now so that the local object is always upto date with the BNCS network
            dbChangeTimer.Start();

            return true;
        }

        public bool LoadAudioPreset(uint PresetIndex)
        {

            //get the audio preset details
            var presetName = Lookup(DbConstants.dbAudioPresetName, (int)PresetIndex);
            var presetTags = Lookup(DbConstants.dbAudioPresetTypeTag, (int)PresetIndex);
            var presetSubst1 = Lookup(DbConstants.dbAudioPresetTypeTagSubst1, (int)PresetIndex);
            var presetSubst2 = Lookup(DbConstants.dbAudioPresetTypeTagSubst2, (int)PresetIndex);

            if (string.IsNullOrWhiteSpace(presetName))
            {
                Log.Warning(STR_LoggingCategory, "Attempted to load empty Audio Preset", "Package Index: {0}  Preset Index: {1}", Index, PresetIndex);
                return false;
            }

            Log.Information(STR_LoggingCategory, "Applying Audio Preset", "Package Index: {0}  Preset Index: {1}  Preset Name: {2}", Index, PresetIndex, presetName);

            ChangeDbValue(DbConstants.dbAudioDestinationPairTagType, presetTags);
            ChangeDbValue(DbConstants.dbAudioDestinationPairTagTypeSubst1, presetSubst1);
            ChangeDbValue(DbConstants.dbAudioDestinationPairTagTypeSubst2, presetSubst2);

            //special habdling required on LoadAudioPreset - need to force an RM of the preset fields
            UpdateDbEntry(DbConstants.dbAudioDestinationPairTagType, presetTags);
            UpdateDbEntry(DbConstants.dbAudioDestinationPairTagTypeSubst1, presetSubst1);
            UpdateDbEntry(DbConstants.dbAudioDestinationPairTagTypeSubst2, presetSubst2);

            AcceptAllBncs();
            AcceptAll();
            CommitDbChanges();

            return true;

        }

        public string SymbolicName
        {
            get
            {
                return $"{FunctionalArea}.{FunctionalName}";
            }
        }

    }
}
