﻿using ett_api_logic.events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Timers;
using static ett_api_logic.Enums;

namespace ett_api_logic.packages
{
    /// <summary>
    ///   <para>
    /// Class BasePackage.
    /// This is the abstract base class for all the package defenition classes</para>
    /// </summary>
    public abstract class BasePackage : INotifyPropertyChanged
    {

        /// <summary>Internal store of the database values for this package</summary>
        private ConcurrentDictionary<uint, string> dbValues;
        private ConcurrentDictionary<uint, string> dbPendingChanges;
        protected Timer dbChangeTimer;

        /// <summary>Initializes a new instance of the <a onclick="return false;" href="T:ett_api_logic.BasePackage" originaltag="see">T:ett_api_logic.BasePackage</a> class.</summary>
        /// <param name="Index">The BNCS index of the package</param>
        protected BasePackage(uint Index)
        {

            dbChangeTimer = new Timer(5000);
            dbChangeTimer.AutoReset = false;
            dbChangeTimer.Elapsed += DbChangeTimer_Elapsed;

            dbValues = new ConcurrentDictionary<uint, string>();
            dbPendingChanges = new ConcurrentDictionary<uint, string>();

            this.Index = Index;

            ConnectHandlers();

        }

        private void DbChangeTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            AcceptAllBncs();
            Packages.SendPackageUpdate(PackageType, Index);
        }

        private void OnTrackedPropertyValueChanging(object sender, ValueChangedEventArgs e)
        {
            var paramName = MatchParameterName(sender);
            var paramPath = $"{paramName}.NewValue";
            TrackedPropertyValueChanging?.Invoke(this, new TrackedPropertyValueChangedEventArgs(paramPath, paramName, e.ChangeSource));
        }

        private void OnTrackedPropertyValueChanged(object sender, ValueChangedEventArgs e)
        {
            var paramName = MatchParameterName(sender);
            var paramPath = $"{paramName}.CurrentValue";
            TrackedPropertyValueChanged?.Invoke(this, new TrackedPropertyValueChangedEventArgs(paramPath, paramName, e.ChangeSource));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(paramName));
        }

        private void OnTrackedPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

            var paramName = MatchParameterName(sender);
            var paramPath = $"{paramName}.{e.PropertyName}";
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(paramPath));

            if (e.PropertyName == "Changed")
            {
                Changed = AnyChanges();
            }

        }

        bool changed;
        /// <summary>
        ///   <para>
        /// Gets a value indicating whether any Tracked Parameter field has a pending change</para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if changed; otherwise, <c>false</c>.</value>
        public bool Changed
        {
            get => changed; private set
            {
                if (changed == value)
                {
                    return;
                }

                changed = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Changed)));
            }
        }

        public abstract string PackageType { get; }
        public uint Index { get; private set; }

        private TrackedParameter<string> packageName = new TrackedParameter<string>(clearProtect: true);
        private TrackedParameter<string> buttonName = new TrackedParameter<string>(clearProtect: true);
        private TrackedParameter<int> owner = new TrackedParameter<int>();
        private TrackedParameter<int> _delegate = new TrackedParameter<int>();
        private TrackedParameter<string> uniqueId = new TrackedParameter<string>();

        public TrackedParameter<string> PackageName { get => packageName; }
        public TrackedParameter<string> ButtonName { get => buttonName; }
        public TrackedParameter<int> OwnerTag { get => owner; }
        public TrackedParameter<int> DelegateTag { get => _delegate; }
        public TrackedParameter<string> UniqueId { get => uniqueId; }

        public bool EditLocked { get; protected set; }

        /// <summary>Occurs when a property value changes.</summary>
        /// <returns>
        ///   <br />
        /// </returns>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>Fires when the Current Value of any Tracked Parameter field of the package changes</summary>
        public event TrackedPropertyChangedEventHandler TrackedPropertyValueChanged;
        /// <summary>Fires when the New Value of any Tracked Parameter field of the package changes</summary>
        public event TrackedPropertyChangedEventHandler TrackedPropertyValueChanging;

        /// <summary>Checks all Tracked Parameter fields for any changes</summary>
        /// <returns>
        ///   <c>true</c> if any Tracked Parameter field has a pending change, <c>false</c> otherwise.</returns>
        private bool AnyChanges()
        {

            //use reflection to look for any TrackedParameters that are in the changed state
            PropertyInfo[] props = this.GetType().GetProperties();

            foreach (var pi in props)
            {
                //check if the property is a TrackedParameter
                if (typeof(ITrackedParameter).IsAssignableFrom(pi.PropertyType))
                {
                    var trackedParameter = (ITrackedParameter)pi.GetValue(this);
                    if (trackedParameter.Changed) return true;  //don't bother looking any further
                }
            }

            return false; //no changes found

        }

        /// <summary>
        ///   <para>
        /// Accepts all pending changes to Tracked Parameter fields that have been initiated via the API</para>
        /// </summary>
        public void AcceptAll()
        {

            //use reflection to look for any TrackedParameters that are in the changed state
            PropertyInfo[] props = this.GetType().GetProperties();

            foreach (var pi in props)
            {
                //check if the property is a TrackedParameter
                if (typeof(ITrackedParameter).IsAssignableFrom(pi.PropertyType))
                {
                    var trackedParameter = (ITrackedParameter)pi.GetValue(this);
                    if (trackedParameter.ChangeSource == ChangeSources.API) trackedParameter.Accept(true); //call in bulk change mode
                }
            }

            //because we are in bulk change mode we need to rescan for any changes
            Changed = AnyChanges();

        }

        /// <summary>
        ///   <para>
        /// Accepts all pending changes to Tracked Parameter fields that have been initiated by BNCS</para>
        /// </summary>
        protected void AcceptAllBncs()
        {

            //use reflection to look for any TrackedParameters that are in the changed state
            PropertyInfo[] props = this.GetType().GetProperties();

            foreach (var pi in props)
            {
                //check if the property is a TrackedParameter
                if (typeof(ITrackedParameter).IsAssignableFrom(pi.PropertyType))
                {
                    var trackedParameter = (ITrackedParameter)pi.GetValue(this);
                    if (trackedParameter.ChangeSource == ChangeSources.BNCS) trackedParameter.Accept(true); //call in bulk change mode
                }
            }

            //because we are in bulk change mode we need to rescan for any changes
            Changed = AnyChanges();

        }

        /// <summary>Rejects all pending changes to Tracked Parameter fields</summary>
        public void RejectAll()
        {

            //use reflection to look for any TrackedParameters that are in the changed state
            PropertyInfo[] props = this.GetType().GetProperties();

            foreach (var pi in props)
            {
                //check if the property is a TrackedParameter
                if (typeof(ITrackedParameter).IsAssignableFrom(pi.PropertyType))
                {
                    var trackedParameter = (ITrackedParameter)pi.GetValue(this);
                    if (trackedParameter.ChangeSource == ChangeSources.API) trackedParameter.Reject(true); //call in bulk change mode
                }
            }

            CancelDbChanges();

            //because we are in bulk change mode we need to rescan for any changes
            Changed = AnyChanges();

        }

        public void CommitDbChanges()
        {

            if (dbPendingChanges.Count > 0)
            {
                foreach (var item in dbPendingChanges)
                {
                    var value = item.Value;
                    if (value == null) value = string.Empty; //not allowed to send nulls - must send empty steing instead
                    Packages.SendDbWrite(item.Key, Index, value);
                }
            }

            dbPendingChanges.Clear();

        }

        public void CancelDbChanges()
        {
            //clear any pending db changes
            dbPendingChanges.Clear();
        }

        /// <summary>FInds the field name of a Tracked Parameter</summary>
        /// <param name="sender">The Tracked Parameter instance to lookup</param>
        /// <returns>System.String.</returns>
        private string MatchParameterName(object sender)
        {
            if (typeof(ITrackedParameter).IsAssignableFrom(sender.GetType()))
            {
                //sender implements ITrackedParameter - match the Parameter ID
                PropertyInfo[] props = this.GetType().GetProperties();

                foreach (var pi in props)
                {
                    //check if the property is a TrackedParameter
                    if (typeof(ITrackedParameter).IsAssignableFrom(pi.PropertyType))
                    {
                        var trackedParameter = (ITrackedParameter)pi.GetValue(this);
                        if (trackedParameter.ParamId == ((ITrackedParameter)sender).ParamId) return pi.Name;
                    }
                }

            }

            //if we get this far then nothing matched
            return nameof(sender);

        }

        /// <summary>
        ///   <para>
        /// Wire up all the event handlers to all Tracked Parameters defined in the child classes</para>
        /// </summary>
        private void ConnectHandlers()
        {

            //use reflection to look for any TrackedParameters that are in the changed state
            PropertyInfo[] props = this.GetType().GetProperties();

            foreach (var pi in props)
            {
                //check if the property is a TrackedParameter
                if (typeof(ITrackedParameter).IsAssignableFrom(pi.PropertyType))
                {
                    var trackedParameter = (ITrackedParameter)pi.GetValue(this);
                    trackedParameter.PropertyChanged += OnTrackedPropertyChanged;
                    trackedParameter.ValueChanged += OnTrackedPropertyValueChanged;
                    trackedParameter.ValueChanging += OnTrackedPropertyValueChanging;
                    trackedParameter.DatabaseChanged += OnTrackedPropertyDatabaseValueChanged;
                }
            }

        }

        private void OnTrackedPropertyDatabaseValueChanged(object sender, DatabaseValueChangedEventArgs e)
        {
            ProcessDbChange(e.DbNumber);
        }

        /// <summary>Returns the current value of a Tracked Parameter field based on a dotted notation path</summary>
        /// <param name="parameterPath">The parameter path.</param>
        /// <returns>System.Object.</returns>
        public object GetValue(string parameterPath)
        {
            if (parameterPath.Contains("."))
            {
                //dotted path - look for a TrackedParameter and then return its property
                var pathParts = parameterPath.Split('.');
                PropertyInfo pi = this.GetType().GetProperty(pathParts[0]);
                
                if (pi == null) return null;

                if (typeof(ITrackedParameter).IsAssignableFrom(pi.PropertyType))
                {
                    var trackedParameter = pi.GetValue(this);

                    PropertyInfo pi2 = trackedParameter.GetType().GetProperty(pathParts[1]);

                    if (pi2 != null)
                    {
                        return pi2.GetValue((object)trackedParameter);
                    }
                    else return null;

                }
                else return null;
            }
            else
            {
                //non dotted parameter- just read it directly
                PropertyInfo pi = this.GetType().GetProperty(parameterPath);
                if (pi != null)
                {
                    return pi.GetValue(this);
                }
                else return null;

            }
        }

        /// <summary>Internal method that processes an incomming db change and maps it to the packager fields</summary>
        /// <param name="dbNumber">The database number - indicates the type of data</param>
        /// <param name="value">The database contents</param>
        /// <param name="initMode">if set to <c>true</c> tuns in initialization mode (no change events generated)</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        protected abstract bool ProcessDbValue(uint dbNumber, string value, bool initMode);

        /// <summary>Implemented in a concrete package to map DB changes to the correct database</summary>
        /// <param name="dbNumber">The AffectedDB number of the changed parameter</param>
        protected abstract void ProcessDbChange(uint dbNumber);

        /// <summary>Processes initialization of the database value - no change events generated</summary>
        /// <param name="dbNumber">The database number.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool InitDbValue(uint dbNumber, string value)
        {
            var result = ProcessDbValue(dbNumber, value, true);
            if (result)
            {
                if (dbValues.ContainsKey(dbNumber)) dbValues[dbNumber] = value;
                else dbValues.TryAdd(dbNumber, value);
            }
            return result;
        }

        /// <summary>Processes a change in the database value - with change events if required</summary>
        /// <param name="dbNumber">The database number.</param>
        /// <param name="value">The value.</param>
        /// <returns>
        ///   <c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public bool ChangeDbValue(uint dbNumber, string value)
        {
            var result = ProcessDbValue(dbNumber, value, false);
            if (result)
            {

                if (dbValues.ContainsKey(dbNumber)) dbValues[dbNumber] = value;
                else dbValues.TryAdd(dbNumber, value);

                //a db change should process a notification 5 seconds after the last change is received
                if (dbChangeTimer.Enabled) dbChangeTimer.Stop();
                AcceptAllBncs();  //call this now so that the local object is always upto date with the BNCS network
                dbChangeTimer.Start();

            }
            return result;
        }

        /// <summary>Retrieves a database value</summary>
        /// <param name="dbNumber">The database number.</param>
        /// <returns>System.String.</returns>
        public string GetDbValue(uint dbNumber)
        {
            if (dbValues.ContainsKey(dbNumber)) return dbValues[dbNumber];
            else return null;
        }

        protected static string ReadTaggedValue(string value, string tag)
        {
            if (string.IsNullOrWhiteSpace(value)) return null;

            //split comma deliminted field
            var pairs = value.Split(',');

            //now look for a pair that starts with the tag
            foreach (var pair in pairs)
            {
                if (pair.StartsWith(tag))
                {
                    //this is our value, now split around the equals sign
                    var parts = pair.Split('=');
                    if (parts.Length > 1)
                    {
                        //return the value
                        return parts[1];
                    }
                }
            }

            return null;
        }

        protected string SetTaggedValue(uint dbNumber, string tag, string value)
        {

            string currentValue = string.Empty;

            if (dbValues.ContainsKey(dbNumber)) currentValue = dbValues[dbNumber];

            if (string.IsNullOrWhiteSpace(value)) value = string.Empty;
            if (string.IsNullOrWhiteSpace(currentValue)) currentValue = string.Empty;

            //split comma deliminted field
            var pairs = new List<string>();
            pairs.AddRange(currentValue.Split(','));
            bool found = false;

            //now look for a pair that starts with the tag
            for (int i = 0; i < pairs.Count; i++)
            {
                if (pairs[i].StartsWith($"{tag}="))
                {
                    //this is our value, overwrite it
                    pairs[i] = $"{tag}={value}";
                    found = true;
                }
            }

            //if we never found a match on the tag then we need to add it
            if (!found) pairs.Add($"{tag}={value}");

            var newValue = String.Join(",", pairs);
            newValue = newValue.Trim(',');

            UpdateDbEntry(dbNumber, newValue);

            return newValue;

        }

        protected void UpdateDbEntry(uint dbNumber, string value)
        {

            if (dbValues.ContainsKey(dbNumber)) dbValues[dbNumber] = value;
            else dbValues.TryAdd(dbNumber, value);

            if (dbPendingChanges.ContainsKey(dbNumber)) dbPendingChanges[dbNumber] = value;
            else dbPendingChanges.TryAdd(dbNumber, value);

        }

        protected static int GetTaggedIntValue(string value)
        {

            if (string.IsNullOrWhiteSpace(value)) return -1; //this is used to indicate blank or null

            if (int.TryParse(value, out var result))
            {
                return result;
            }

            //TODO:  deal with # - find out what they mean!

            return -1;

        }

        protected static string GetTaggedIntValue(int value)
        {
            if (value < 0) return null;
            else return value.ToString();
        }

        protected static (string, string, string, string) GetTuple(string value)
        {

            if (value == null) return (null, null, null, null);

            var parts = value.Split('#',':');

            if (parts.Length <= 1) return (value, null, null, null); //not a tuple
            else if (parts.Length == 2) return (parts[0], parts[1], null, null); 
            else if (parts.Length == 3) return (parts[0], parts[1], parts[2], null); 
            else return (parts[0], parts[1], parts[2], parts[3]);
        }

        protected static string Lookup(uint dbNumber, int index)
        {
            if (index < 0) return null;
            return Packages.Lookups.GetDbValue(dbNumber, (uint)index);
        }

        protected static int Lookup(uint dbNumber, string value)
        {
            return (int)Packages.Lookups.GetDbValueIndex(dbNumber, value);
        }

        protected static int? NullableInt(int value)
        {
            if (value < 0) return null;
            return value;
        }

        protected static int NullableInt(int? value)
        {
            if (value == null) return -1;
            return (int)value;
        }

        public void ErasePackage(ChangeSources changeSource)
        {
            //use reflection to look for any TrackedParameters that are in the changed state
            PropertyInfo[] props = this.GetType().GetProperties();

            foreach (var pi in props)
            {
                //check if the property is a TrackedParameter
                if (typeof(ITrackedParameter).IsAssignableFrom(pi.PropertyType))
                {
                    var trackedParameter = (ITrackedParameter)pi.GetValue(this);
                    trackedParameter.Clear(changeSource);
                }
            }

        }

        public List<KeyValuePair<uint, string>> GetDbValues()
        {

            var changes = new List<KeyValuePair<uint, string>>();

            foreach (var item in dbValues)
            {
                changes.Add(item);
            }

            return changes;

        }

        public List<KeyValuePair<uint, string>> GetDbChanges()
        {

            var changes = new List<KeyValuePair<uint, string>>();

            foreach (var item in dbPendingChanges)
            {
                changes.Add(item);
            }

            return changes;

        }


    }
}
