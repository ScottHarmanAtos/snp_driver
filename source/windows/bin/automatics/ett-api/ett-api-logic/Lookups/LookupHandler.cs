﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.lookups
{
    public class LookupHandler
    {

        private Dictionary<uint, LookupDictionary> dictionaries;

        public LookupHandler()
        {
            dictionaries = new Dictionary<uint, LookupDictionary>();
        }

        private LookupDictionary GetDictionary(uint dbNumber)
        {
            //if the dictionary doesn't exist then create it
            if (!dictionaries.ContainsKey(dbNumber)) dictionaries.Add(dbNumber, new LookupDictionary());
            return dictionaries[dbNumber];
        }

        public void SetDbValue(uint dbNumber, uint index, string value)
        {
            var dict = GetDictionary(dbNumber);
            dict.SetDbValue(index, value);
        }

        public string GetDbValue(uint dbNumber, uint index)
        {
            var dict = GetDictionary(dbNumber);
            return dict.GetDbValue(index);
        }

        public int GetDbValueIndex(uint dbNumber, string value)
        {
            var dict = GetDictionary(dbNumber);
            return dict.GetDbValueIndex(value);
        }

    }
}
