﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ett_api_logic.lookups
{
    public class LookupDictionary
    {

        private Dictionary<uint, string> dbValues;

        public LookupDictionary()
        {
            dbValues = new Dictionary<uint, string>();
        }

        public void SetDbValue(uint index, string value)
        {

            if (value != null && value.Contains('|'))
            {
                //only use upto the pipe
                var values = value.Split('|');
                value = values[0];
            }

            if (dbValues.ContainsKey(index)) dbValues[index] = value;
            else dbValues.Add(index, value);
        }

        public string GetDbValue(uint index)
        {
            if (dbValues.ContainsKey(index)) return dbValues[index];
            else return null;
        }

        public int GetDbValueIndex(string value)
        {

            if (value == null) return -1;

            try
            {
                var myKey = dbValues.FirstOrDefault(x => x.Value == value).Key;
                return (int)myKey;
            }
            catch (Exception ex)
            {
                return -1;
            }

       
        }

    }
}
