﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic
{
    public static class Enums
    {

        /// <summary>Indicates the source of a tracked change request</summary>
        public enum ChangeSources
        {
            /// <summary>No change source</summary>
            NONE = 0,
            /// <summary>the change has been requested by the API</summary>
            API = 1,
            /// <summary>the change has originated from elsewhere in BNCS</summary>
            BNCS = 2  
        }

    }
}
