﻿using ett_api_logic.events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;
using static ett_api_logic.Enums;

namespace ett_api_logic
{

    /// <summary>
    ///   <para>
    /// A generic class with change tracking</para>
    /// </summary>
    /// <typeparam name="T">
    ///   <br />
    /// </typeparam>
    public class TrackedParameter<T> : INotifyPropertyChanged, ITrackedParameter where T : IEquatable<T>
    { 

        private T currentValue;
        private T newValue;
        private bool changed;
        private Guid paramId;

        public TrackedParameter(uint? affectedDb = null, bool clearProtect = false)
        {
            paramId = Guid.NewGuid();
            if (affectedDb == null) this.AffectedDb = -1;
            else this.AffectedDb = (int)affectedDb;
            this.ClearProtect = clearProtect;
        }

        public TrackedParameter(T InitialValue, uint? affectedDb = null, bool clearProtect = false)
        {
            paramId = Guid.NewGuid();
            if (affectedDb == null) this.AffectedDb = -1;
            else this.AffectedDb = (int)affectedDb;
            this.Init(InitialValue, false);
            this.ClearProtect = clearProtect;
        }

        public Guid ParamId => paramId;

        /// <summary>The current accepted value of the parameter</summary>
        /// <value>The current value.</value>
        public T CurrentValue
        {         
            get
            {
                return currentValue;
            }
            private set
            {

                //no change conditions
                if (currentValue == null && value == null)
                {
                    return;
                }

                if (currentValue != null && currentValue.Equals(value))
                {
                    return;
                }

                currentValue = value;
                ValueChanged?.Invoke(this, new ValueChangedEventArgs(ChangeSource));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(CurrentValue)));
            }
        }

        /// <summary>The proposed new value for the parameter</summary>
        /// <value>The new value.</value>
        public T NewValue
        {
            get
            {
                return newValue;
            }
            private set
            {

                //no change conditions
                if (newValue == null && value == null)
                {
                    return;
                }

                if (newValue != null && newValue.Equals(value))
                {
                    //value already matches
                    return;
                }

                if (currentValue != null && currentValue.Equals(value))
                {
                    //reset condition
                    newValue = value;
                    Changed = false;
                }
                else
                {
                    //change the value
                    newValue = value;
                    Changed = true;
                }

                ValueChanging?.Invoke(this, new ValueChangedEventArgs(ChangeSource));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(NewValue)));

            }
        }

        /// <summary>Indicates that the value of the parameter has been changed (i.e. CurrentValue &amp; NewValue are different)</summary>
        /// <value>
        ///   <c>true</c> if changed; otherwise, <c>false</c>.</value>
        public bool Changed
        {
            get
            {
                return changed;
            }
            private set
            {
                if (changed == value)
                {
                    return;
                }

                changed = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Changed)));
            }
        }

        ChangeSources changeSource;
        /// <summary>Indicates the source (API side or BNCS side) for the current change</summary>
        /// <value>The change source.</value>
        public ChangeSources ChangeSource
        {
            get => changeSource; set
            {
                if (changeSource == value)
                {
                    return;
                }

                changeSource = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(ChangeSource)));
            }
        }

        public int AffectedDb { get; set; }

        public bool ClearProtect { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public event ValueChangedEventHandler ValueChanged;
        public event ValueChangedEventHandler ValueChanging;
        public event DatabaseValueChangedEventHandler DatabaseChanged;

        /// <summary>Accepts the change</summary>
        /// <param name="bulkChange">
        /// if set to <c>true</c> this is treated as a bulk change - i.e. no PropertyChanged event handlers will be fired for the Changed field - it is the responsibility of the calling method to scan for changes
        /// </param>
        public void Accept(bool bulkChange = false)
        {

            if (!Changed) return;  //don't do anything if nothing has changed

            CurrentValue = NewValue;

            //if in bulkChange mode don't trigger a change notification on the Changed parameter
            if (bulkChange) changed = false;
            else Changed = false;

            if (ChangeSource == ChangeSources.API && AffectedDb >= 0)
            {
                //fire a database change event
                DatabaseChanged?.Invoke(this, new DatabaseValueChangedEventArgs((uint)AffectedDb));
            }
            
        }

        /// <summary>Rejects the change</summary>
        /// <param name="bulkChange">
        /// if set to <c>true</c> this is treated as a bulk change - i.e. no PropertyChanged event handlers will be fired for the Changed field - it is the responsibility of the calling method to scan for changes
        public void Reject(bool bulkChange = false)
        {
            NewValue = CurrentValue;

            //if in bulkChange mode don't trigger a change notification on the Changed parameter
            if (bulkChange) changed = false;
            else Changed = false;

        }

        /// <summary>Initializes the value of the parameter bypassing the change tracking mechanism</summary>
        /// <param name="value">The value.</param>
        /// <param name="notifyChange">if set to <c>true</c> fire change events.</param>
        public void Init(T value, bool notifyChange = false)
        {
            if (notifyChange)
            {
                //If change notification is requested change the property (which will trigger the change notification
                CurrentValue = value;
                NewValue = value;
                Changed = false;
            }
            else
            {
                //if no change notification, update the underlying variables
                currentValue = value;
                newValue = value;
                changed = false;
            }
        }

        /// <summary>Initiates a tracked change to the parameter</summary>
        /// <param name="newValue">The new value.</param>
        /// <param name="changeSource">The source of the change request</param>
        public void Change(T newValue, ChangeSources changeSource)
        {
            ChangeSource = changeSource;
            NewValue = newValue;
        }

        public void Set(T value, ChangeSources changeSource, bool init, bool notifyChange = false)
        {
            if (init) Init(value, notifyChange);
            else Change(value, changeSource);
        }

        public override string ToString()
        {
            if (CurrentValue == null) return string.Empty;
            return CurrentValue.ToString();
        }

        public void Clear(ChangeSources changeSource)
        {
            if (ClearProtect) return; //don't allow a change 
            Change(default(T), changeSource);
        }
    }
}
