﻿using ett_api_logic.events;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using static ett_api_logic.Enums;

namespace ett_api_logic
{
    public interface ITrackedParameter
    {
        ChangeSources ChangeSource { get; set; }

        /// <summary>Occurs when the Changed field value changes for the Tracked Parameter</summary>
        event PropertyChangedEventHandler PropertyChanged;
        /// <summary>Occurs when the CurrentValue of the Tracked Parameter changes (either via an Init or Accept)</summary>
        event ValueChangedEventHandler ValueChanged;
        /// <summary>Occurs when the New Value of the Tracked Parameter changes</summary>
        event ValueChangedEventHandler ValueChanging;
        /// <summary>Occurs when a BNCS database needs to be changed</summary>
        event DatabaseValueChangedEventHandler DatabaseChanged;

        /// <summary>
        ///   <para>
        /// The BNCS database that stores this parameter</para>
        /// </summary>
        /// <value>The affected database.</value>
        int AffectedDb { get; set; }

        /// <summary>Internal tracking GUID - used to match TrackedParameters</summary>
        /// <value>The parameter identifier.</value>
        Guid ParamId { get; }

        /// <summary>Indicates that the value of the parameter has been changed (i.e. CurrentValue &amp; NewValue are different)</summary>
        /// <value>
        ///   <c>true</c> if changed; otherwise, <c>false</c>.</value>
        bool Changed { get; }

        /// <summary>Accepts the change</summary>
        /// <param name="bulkChange">
        /// if set to <c>true</c> this is treated as a bulk change - i.e. no PropertyChanged event handlers will be fired for the Changed field - it is the responsibility of the calling method to scan for changes
        /// </param>
        void Accept(bool bulkChange = false);

        /// <summary>Rejects the change</summary>
        /// <param name="bulkChange">
        /// if set to <c>true</c> this is treated as a bulk change - i.e. no PropertyChanged event handlers will be fired for the Changed field - it is the responsibility of the calling method to scan for changes
        void Reject(bool bulkChange = false);

        /// <summary>Sets the parameter to its default based on type</summary>
        /// <param name="changedSource">The changed source.</param>
        void Clear(ChangeSources changedSource);

        /// <summary>Gets a value indicating whether the parameter is protected during a Clear operation</summary>
        /// <value>
        ///   <c>true</c> if Clear has no effect otherwise, <c>false</c>.</value>
        bool ClearProtect { get; }

    }
}
