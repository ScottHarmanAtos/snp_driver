﻿using ett_api_logic.events;
using ett_api_logic.Events;
using ett_api_logic.lookups;
using ett_api_logic.packages;
using Gibraltar.Agent;
using Newtonsoft.Json;
using SharedMemory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ett_api_logic
{
    public static class Packages
    {
        const string STR_LoggingCategory = "BNCS_ETT_API.Packages";

        private static uint intPackageRange = 64000;

        private enum PackageTypes
        {
            Unmapped = 0,
            Lookup = 1,
            SourcePackageType = 2,
            AudioPackageType = 3,
            DestinationPackageType = 4
        }

        public enum DriverStates
        {
            Disconnected = 0,
            RxOnly = 1,
            Connected = 2
        }

        private static PackageGroup<SourcePackage> sourcePackages;
        private static PackageGroup<AudioPackage> audioPackages;
        private static PackageGroup<DestinationPackage> destinationPackages;

        private static LookupHandler lookups;

        static Packages()
        {
        }

        public static void InitPackages(uint PackageRange)
        {
            intPackageRange = PackageRange;
            DriverState = DriverStates.Disconnected;
            sourcePackages = new PackageGroup<SourcePackage>(IntPackageRange);
            audioPackages = new PackageGroup<AudioPackage>(IntPackageRange);
            destinationPackages = new PackageGroup<DestinationPackage>(IntPackageRange);
            lookups = new LookupHandler();
        }

        public static PackageGroup<SourcePackage> SourcePackages { get => sourcePackages; }
        public static PackageGroup<AudioPackage> AudioPackages { get => audioPackages; }
        public static PackageGroup<DestinationPackage> DestinationPackages { get => destinationPackages; }

        public static LookupHandler Lookups { get => lookups; }

        private static PackageTypes MapDbToPackageGroup(uint dbNumber)
        {
            switch (dbNumber)
            {
                case DbConstants.dbVideoHubTags:
                case DbConstants.dbAudioHubTags:
                case DbConstants.dbLanguageUseHubTags:
                case DbConstants.dbAudioTypeTags:
                case DbConstants.dbOwnerTags:
                case DbConstants.dbDelegateTags:
                case DbConstants.dbRiedelTags:
                case DbConstants.dbAudioPresetName:
                case DbConstants.dbAudioPresetTypeTag:
                case DbConstants.dbAudioPresetTypeTagSubst1:
                case DbConstants.dbAudioPresetTypeTagSubst2:
                    return PackageTypes.Lookup;
                case DbConstants.dbAudioSourcePackageButtonName:
                case DbConstants.dbAudioSourcePackageLongName:
                case DbConstants.dbAudioSourcePackageTagFields1:
                case DbConstants.dbAudioSourcePackageTagFields2:
                case DbConstants.dbAudioSourcePackagePairs:
                case DbConstants.dbAudioSourcePackageReturnPairs:
                case DbConstants.dbAudioSourcePackageVideoLinkIndex:
                case DbConstants.dbAudioSourcePackageCFLabels:
                case DbConstants.dbAudioSourcePackageConfLabels:
                case DbConstants.dbAudioPackageLocks:
                    return PackageTypes.AudioPackageType;
                case DbConstants.dbSourcePackageTitle:
                case DbConstants.dbSourceButtonName:
                case DbConstants.dbSourcePackageTags:
                case DbConstants.dbFunctionalSourceArea:
                case DbConstants.dbFunctionalSourceName:
                case DbConstants.dbMCRNotes:
                case DbConstants.dbSourceVideo:
                case DbConstants.dbAudioSourcePackages:
                case DbConstants.dbMCRUmd:
                case DbConstants.dbProductionUmd:
                case DbConstants.dbSourceEventName:
                case DbConstants.dbSourceSpevList1:
                case DbConstants.dbSourceSpevList2:
                case DbConstants.dbSourceSpevList3:
                case DbConstants.dbSourceSpevList4:
                case DbConstants.dbSourceRecordId:
                case DbConstants.dbSourceClassId:
                case DbConstants.dbSourceDates:
                case DbConstants.dbSourceStatus:
                case DbConstants.dbSourcePackageLocks:
                    return PackageTypes.SourcePackageType;
                case DbConstants.dbDestPackageTitle:
                case DbConstants.dbDestButtonName:
                case DbConstants.dbDestPackageTags:
                case DbConstants.dbFunctionalDestArea:
                case DbConstants.dbFunctionalDestName:
                case DbConstants.dbDestLevels:
                case DbConstants.dbAudioDestinationPairIndex:
                case DbConstants.dbAudioDestinationPairTagType:
                case DbConstants.dbAudioDestinationPairTagTypeSubst1:
                case DbConstants.dbAudioDestinationPairTagTypeSubst2:
                case DbConstants.dbAudioReturn:
                case DbConstants.dbDestPackageEditLocks:
                case DbConstants.dbDestPackageRouteLocks:
                    return PackageTypes.DestinationPackageType;
                default:
                    return PackageTypes.Unmapped;
            }
        }

        public static bool InitDbValue(uint dbNumber, uint index, string value)
        {
            switch (MapDbToPackageGroup(dbNumber))
            {
                case PackageTypes.Unmapped:
                    return false;
                case PackageTypes.Lookup:
                    Lookups.SetDbValue(dbNumber, index, value);
                    return true;
                case PackageTypes.SourcePackageType:
                    return SourcePackages.InitDbValue(dbNumber, index, value);
                case PackageTypes.AudioPackageType:
                    return AudioPackages.InitDbValue(dbNumber, index, value);
                case PackageTypes.DestinationPackageType:
                    return DestinationPackages.InitDbValue(dbNumber, index, value);
                default:
                    return false;
            }
        }

        public static bool ChangeDbValue(uint dbNumber, uint index, string value)
        {
            switch (MapDbToPackageGroup(dbNumber))
            {
                case PackageTypes.Unmapped:
                    return false;
                case PackageTypes.Lookup:
                    Lookups.SetDbValue(dbNumber, index, value);
                    return true;
                case PackageTypes.SourcePackageType:
                    return SourcePackages.ChangeDbValue(dbNumber, index, value);
                case PackageTypes.AudioPackageType:
                    return AudioPackages.ChangeDbValue(dbNumber, index, value);
                case PackageTypes.DestinationPackageType:
                    return DestinationPackages.ChangeDbValue(dbNumber, index, value);
                default:
                    return false;
            }
        }

        public static string GetDbValue(uint dbNumber, uint index)
        {
            switch (MapDbToPackageGroup(dbNumber))
            {
                case PackageTypes.Unmapped:
                    return null;
                case PackageTypes.SourcePackageType:
                    return SourcePackages.GetDbValue(dbNumber, index);
                case PackageTypes.AudioPackageType:
                    return AudioPackages.GetDbValue(dbNumber, index);
                case PackageTypes.DestinationPackageType:
                    return DestinationPackages.GetDbValue(dbNumber, index);
                default:
                    return null;
            }
        }

        internal static void SendDbWrite(uint dbNumber, uint dbIndex, string value)
        {
            OnDbWrite?.Invoke(null, new DbWriteEventArgs(dbNumber, dbIndex, value));
        }

        static bool initComplete;
        public static bool InitComplete
        { 
            get => initComplete;
            set
            {
                initComplete = value;
                OnInitComplete?.Invoke(null, new EventArgs());
            }
        }

        public static event DbWriteEventHandler OnDbWrite;
        public static event RouteRequestEventHandler OnRouteRequested;
        public static event EventHandler OnInitComplete;
        public static event PackageUpdateEventHandler OnPackageUpdate;
        public static event RouteRequestEventHandler OnRouteRevertive;

        internal static void SendPackageUpdate(string packageType, uint dbIndex)
        {

            if (DriverState != DriverStates.Connected) return; //only send when in a connected state

            switch (packageType)
            {
                case "audio":
                    
                    var audioPackage = AudioPackages.GetPackage(dbIndex);
                    if (audioPackage == null) break;
                    var audioPackageDoc = audioPackage.GetDocument();
                    if (audioPackageDoc == null) break;

                    OnPackageUpdate?.Invoke(null, new PackageUpdateEventArgs(audioPackage.PackageType, audioPackage.Index, audioPackageDoc));

                    break;

                case "sp":

                    var sourcePackage = SourcePackages.GetPackage(dbIndex);
                    if (sourcePackage == null) break;
                    var sourcePackageDoc = sourcePackage.GetDocument();
                    if (sourcePackageDoc == null) break;

                    OnPackageUpdate?.Invoke(null, new PackageUpdateEventArgs(sourcePackage.PackageType, sourcePackage.Index, sourcePackageDoc));

                    break;

                case "destination":

                    var destPackage = DestinationPackages.GetPackage(dbIndex);
                    if (destPackage == null) break;
                    var destPackageDoc = destPackage.GetDocument();
                    if (destPackageDoc == null) break;

                    OnPackageUpdate?.Invoke(null, new PackageUpdateEventArgs(destPackage.PackageType, destPackage.Index, destPackageDoc));

                    break;

                default:
                    break;
            }
        }

        public static void SetAllDestinationLocks()
        {

            Log.Warning(STR_LoggingCategory, "Started lock reload", null, null);

            try
            {
                using (var destinationLocks = new SharedArray<bool>("PackagerDestinationLocks"))
                {
                    for (int i = 0; i < destinationLocks.Count; i++)
                    {

                        var package = DestinationPackages.GetPackage((uint)i + 1);

                        if (package != null)
                        {
                            package.LockStatus.Init(destinationLocks[i]);
                        }

                    }
                }
            }
            catch
            {
            }
            finally
            {
                Log.Warning(STR_LoggingCategory, "Completed lock reload", null, null);
            }

        }

        public static void SetDestinationLock(uint Index, bool Locked)
        {

            Log.Warning(STR_LoggingCategory, "Setting lock state for package", "Package Index {0}, Lock {1}", Index, Locked);
            var package = DestinationPackages.GetPackage(Index);

            if (package != null)
            {
                package.BncsChangeLockValue(Locked);
            }
            else
            {
                Log.Error(STR_LoggingCategory, "Package not found when setting lock state", "Package Index {0}, Lock {1}", Index, Locked);
            }

        }

        public static void RequestRoute(uint SourcePackage, uint DestPackage)
        {
            OnRouteRequested?.Invoke(null, new RouteRequestEventArgs(SourcePackage, DestPackage));
        }

        public static void HandleRouteRevertive(uint sourcePackage, uint destPackage)
        {
            if (DriverState != DriverStates.Connected) return; //only send when in a connected state
            OnRouteRevertive?.Invoke(null, new RouteRequestEventArgs(sourcePackage, destPackage));
        }

        public static DriverStates DriverState { get; private set; }

        public static uint IntPackageRange => intPackageRange;

        public static void SetDriverState(DriverStates state)
        {
            switch (state)
            {
                case DriverStates.Disconnected:
                    Log.Critical(STR_LoggingCategory, "Driver IS DISCONNECTED", null);
                    break;
                case DriverStates.RxOnly:
                    Log.Error(STR_LoggingCategory, "Driver IS RXONLY", null);
                    break;
                case DriverStates.Connected:
                    Log.Warning(STR_LoggingCategory, "Driver IS CONNECTED", null);
                    break;
            }
            DriverState = state;
        }

        public static SourcePackage[] GetSourcePackages(string symbolicName)
        {

            var pkgs = SourcePackages.AsQueryable();
            pkgs = pkgs.Where(a => a.SymbolicName == symbolicName);
            return pkgs.ToArray();

        }

        public static DestinationPackage[] GetDestinationPackages(string symbolicName)
        {

            var pkgs = DestinationPackages.AsQueryable();
            pkgs = pkgs.Where(a => a.SymbolicName == symbolicName);
            return pkgs.ToArray();

        }

    }
}
