﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.Events
{
    public class PackageUpdateEventArgs : EventArgs
    {
        public PackageUpdateEventArgs(string packageType, uint packageIndex, object packageDoc)
        {
            PackageType = packageType;
            PackageIndex = packageIndex;
            PackageDoc = packageDoc;
        }

        public PackageUpdateEventArgs()
        {

        }

        public string PackageType { get; set; }
        public uint PackageIndex { get; set; }
        public object PackageDoc { get; set; }

    }
}
