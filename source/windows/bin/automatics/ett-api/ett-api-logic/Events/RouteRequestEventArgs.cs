﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.Events
{
    public class RouteRequestEventArgs : EventArgs
    {

        public RouteRequestEventArgs(uint sourcePackage, uint destPackage)
        {
            SourcePackage = sourcePackage;
            DestPackage = destPackage;
        }

        public uint SourcePackage { get; set; }
        public uint DestPackage { get; set; }

        public override string ToString()
        {
            return $"Source: {SourcePackage}  Dest: {DestPackage}";
        }

    }
}
