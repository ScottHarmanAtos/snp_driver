﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.events
{
    public class DbWriteEventArgs : EventArgs
    {
        private uint dbNumber;
        private uint dbIndex;
        private string value;

        public DbWriteEventArgs(uint dbNumber, uint dbIndex, string value)
        {
            this.dbNumber = dbNumber;
            this.dbIndex = dbIndex;
            this.value = value;
        }

        public uint DbNumber { get => dbNumber; }
        public uint DbIndex { get => dbIndex; }
        public string Value { get => value; }
    }
}
