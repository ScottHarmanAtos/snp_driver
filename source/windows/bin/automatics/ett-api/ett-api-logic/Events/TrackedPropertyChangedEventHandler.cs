﻿namespace ett_api_logic.events
{
    public delegate void TrackedPropertyChangedEventHandler(object sender, TrackedPropertyValueChangedEventArgs e);
}