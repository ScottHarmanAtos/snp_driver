﻿using System;
using static ett_api_logic.Enums;

namespace ett_api_logic.events
{
    public class TrackedPropertyValueChangedEventArgs : EventArgs
    {

        public TrackedPropertyValueChangedEventArgs(string trackedPropertyPath, string trackedPropertyName, ChangeSources changeSource)
        {
            TrackedPropertyPath = trackedPropertyPath;
            TrackedPropertyName = trackedPropertyName;
            ChangeSource = changeSource;
        }

        public string TrackedPropertyPath { get; private set; }
        public string TrackedPropertyName { get; private set; }
        public ChangeSources ChangeSource { get; private set; }

    }
}