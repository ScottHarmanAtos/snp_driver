﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.events
{
    public class DatabaseValueChangedEventArgs : EventArgs
    {

        private uint dbNumber;

        public DatabaseValueChangedEventArgs(uint dbNumber)
        {
            this.dbNumber = dbNumber;
        }

        public uint DbNumber { get => dbNumber; }

    }
}
