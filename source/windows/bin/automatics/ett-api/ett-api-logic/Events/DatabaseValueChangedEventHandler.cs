﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ett_api_logic.events
{
    public delegate void DatabaseValueChangedEventHandler(object sender, DatabaseValueChangedEventArgs e);
}
