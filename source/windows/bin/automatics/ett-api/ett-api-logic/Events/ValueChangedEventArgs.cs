﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using static ett_api_logic.Enums;

namespace ett_api_logic.events
{
    public class ValueChangedEventArgs : EventArgs
    {

        /// <summary>Initializes a new instance of the <see cref="ValueChangedEventArgs"></see> class.</summary>
        /// <param name="propertyName">The name of the property that changed.</param>
        public ValueChangedEventArgs(ChangeSources changeSource) 
        {
            this.ChangeSource = changeSource;           
        }

        public ChangeSources ChangeSource { get; private set; }

    }
}
