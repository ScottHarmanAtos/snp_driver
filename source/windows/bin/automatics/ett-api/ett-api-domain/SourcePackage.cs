/*
 * BNCS/ETT Control Message API
 *
 * API for control messages from ETT (BNCS is SERVER)
 *
 * OpenAPI spec version: 1.0.0
 * Contact: kenneth.munro.external@atos.net
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */
using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace ett_api_domain
{ 
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public partial class SourcePackage : IEquatable<SourcePackage>
    { 
        /// <summary>
        /// always sp for a source package
        /// </summary>
        /// <value>always sp for a source package</value>
        [DataMember(Name="packageType")]
        public string PackageType { get; set; }

        /// <summary>
        /// the index of the package to be retrieved
        /// </summary>
        /// <value>the index of the package to be retrieved</value>
        [DataMember(Name="index")]
        public int? Index { get; set; }

        /// <summary>
        /// resource based name e.g.: ldc_pcr_01.os_lines.01
        /// </summary>
        /// <value>resource based name e.g.: ldc_pcr_01.os_lines.01</value>
        [DataMember(Name="symbolicName")]
        public string SymbolicName { get; set; }

        /// <summary>
        /// the current name of the package
        /// </summary>
        /// <value>the current name of the package</value>
        [DataMember(Name="packageName")]
        public string PackageName { get; set; }

        /// <summary>
        /// the button label for the package with | (pipe) line break
        /// </summary>
        /// <value>the button label for the package with | (pipe) line break</value>
        [DataMember(Name="buttonName")]
        public string ButtonName { get; set; }

        /// <summary>
        /// the tag for the owner of the package
        /// </summary>
        /// <value>the tag for the owner of the package</value>
        [DataMember(Name="ownerTag")]
        public OwnerTag OwnerTag { get; set; }

        /// <summary>
        /// Gets or Sets Metadata
        /// </summary>
        [DataMember(Name="metadata")]
        public SourcePackageMetadata Metadata { get; set; }

        /// <summary>
        /// Notes for MCR operators
        /// </summary>
        /// <value>Notes for MCR operators</value>
        [DataMember(Name="mcrNotes")]
        public string McrNotes { get; set; }

        /// <summary>
        /// Gets or Sets UmdLabels
        /// </summary>
        [DataMember(Name="umdLabels")]
        public SourcePackageUmdLabels UmdLabels { get; set; }

        /// <summary>
        /// Gets or Sets VideoLevels
        /// </summary>
        [DataMember(Name="videoLevels")]
        public SourcePackageVideoLevels VideoLevels { get; set; }

        /// <summary>
        /// Gets or Sets AudioPackageLevels
        /// </summary>
        [DataMember(Name="audioPackageLevels")]
        public SourcePackageAudioLevels AudioPackageLevels { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class SourcePackage {\n");
            sb.Append("  PackageType: ").Append(PackageType).Append("\n");
            sb.Append("  Index: ").Append(Index).Append("\n");
            sb.Append("  SymbolicName: ").Append(SymbolicName).Append("\n");
            sb.Append("  PackageName: ").Append(PackageName).Append("\n");
            sb.Append("  ButtonName: ").Append(ButtonName).Append("\n");
            sb.Append("  OwnerTag: ").Append(OwnerTag).Append("\n");
            sb.Append("  Metadata: ").Append(Metadata).Append("\n");
            sb.Append("  McrNotes: ").Append(McrNotes).Append("\n");
            sb.Append("  UmdLabels: ").Append(UmdLabels).Append("\n");
            sb.Append("  VideoLevels: ").Append(VideoLevels).Append("\n");
            sb.Append("  AudioPackageLevels: ").Append(AudioPackageLevels).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((SourcePackage)obj);
        }

        /// <summary>
        /// Returns true if SourcePackage instances are equal
        /// </summary>
        /// <param name="other">Instance of SourcePackage to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(SourcePackage other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    PackageType == other.PackageType ||
                    PackageType != null &&
                    PackageType.Equals(other.PackageType)
                ) && 
                (
                    Index == other.Index ||
                    Index != null &&
                    Index.Equals(other.Index)
                ) && 
                (
                    SymbolicName == other.SymbolicName ||
                    SymbolicName != null &&
                    SymbolicName.Equals(other.SymbolicName)
                ) && 
                (
                    PackageName == other.PackageName ||
                    PackageName != null &&
                    PackageName.Equals(other.PackageName)
                ) && 
                (
                    ButtonName == other.ButtonName ||
                    ButtonName != null &&
                    ButtonName.Equals(other.ButtonName)
                ) && 
                (
                    OwnerTag == other.OwnerTag ||
                    OwnerTag != null &&
                    OwnerTag.Equals(other.OwnerTag)
                ) && 
                (
                    Metadata == other.Metadata ||
                    Metadata != null &&
                    Metadata.Equals(other.Metadata)
                ) && 
                (
                    McrNotes == other.McrNotes ||
                    McrNotes != null &&
                    McrNotes.Equals(other.McrNotes)
                ) && 
                (
                    UmdLabels == other.UmdLabels ||
                    UmdLabels != null &&
                    UmdLabels.Equals(other.UmdLabels)
                ) && 
                (
                    VideoLevels == other.VideoLevels ||
                    VideoLevels != null &&
                    VideoLevels.Equals(other.VideoLevels)
                ) && 
                (
                    AudioPackageLevels == other.AudioPackageLevels ||
                    AudioPackageLevels != null &&
                    AudioPackageLevels.Equals(other.AudioPackageLevels)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                    if (PackageType != null)
                    hashCode = hashCode * 59 + PackageType.GetHashCode();
                    if (Index != null)
                    hashCode = hashCode * 59 + Index.GetHashCode();
                    if (SymbolicName != null)
                    hashCode = hashCode * 59 + SymbolicName.GetHashCode();
                    if (PackageName != null)
                    hashCode = hashCode * 59 + PackageName.GetHashCode();
                    if (ButtonName != null)
                    hashCode = hashCode * 59 + ButtonName.GetHashCode();
                    if (OwnerTag != null)
                    hashCode = hashCode * 59 + OwnerTag.GetHashCode();
                    if (Metadata != null)
                    hashCode = hashCode * 59 + Metadata.GetHashCode();
                    if (McrNotes != null)
                    hashCode = hashCode * 59 + McrNotes.GetHashCode();
                    if (UmdLabels != null)
                    hashCode = hashCode * 59 + UmdLabels.GetHashCode();
                    if (VideoLevels != null)
                    hashCode = hashCode * 59 + VideoLevels.GetHashCode();
                    if (AudioPackageLevels != null)
                    hashCode = hashCode * 59 + AudioPackageLevels.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(SourcePackage left, SourcePackage right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SourcePackage left, SourcePackage right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
