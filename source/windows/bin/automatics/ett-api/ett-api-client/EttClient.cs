﻿using ett_api_logic;
using Gibraltar.Agent;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers.NewtonsoftJson;
using System;
using System.Threading.Tasks;

namespace ett_api_client
{
    public class EttClient
    {

        const string STR_LoggingCategory = "BNCS_ETT_CLIENT.EttClient";

        private string clientEndpoint;
        private string clientApiKey;

        public EttClient(string clientEndpoint, string clientApiKey)
        {

            this.clientEndpoint = clientEndpoint;
            this.clientApiKey = clientApiKey;

            Log.Information(STR_LoggingCategory, "ETT Client Initialized", "Endpoint URL: {0}", clientEndpoint);

            Packages.OnPackageUpdate += Packages_OnPackageUpdate;
            Packages.OnRouteRevertive += Packages_OnRouteRevertive;

        }

        private void Packages_OnRouteRevertive(object sender, ett_api_logic.Events.RouteRequestEventArgs e)
        {

            Log.Verbose(STR_LoggingCategory, "Package Route Changed", "Source: {0}  Destination: {1}", e.SourcePackage, e.DestPackage);

            var message = new ett_api_domain.PackageRouteResult();
            message.SourcePackage = (int)e.SourcePackage;
            message.DestPackage = (int)e.DestPackage;

            var request = new RestRequest();
            request.Method = Method.POST;
            request.AddJsonBody(message);
            request.Resource = "packageRouteChanged";

            _ = SendMessage(request);

        }

        private void Packages_OnPackageUpdate(object sender, ett_api_logic.Events.PackageUpdateEventArgs e)
        {

            //send the updated package to the client endpoint
            var jsonDoc = JsonConvert.SerializeObject(e.PackageDoc, Formatting.Indented);
            Log.Verbose(STR_LoggingCategory, $"Package Update Type: {e.PackageType}  Index: {e.PackageIndex}", jsonDoc, null);

            var request = new RestRequest();
            request.Method = Method.POST;
            request.AddJsonBody(e.PackageDoc);

            switch (e.PackageType)
            {
                case "audio":
                    request.Resource = "audioPackageChanged";
                    break;
                case "sp":
                    request.Resource = "sourcePackageChanged";
                    break;
                case "destination":
                    request.Resource = "destinationPackageChanged";
                    break;
                default:
                    break;
            }

            _ = SendMessage(request);

        }

        private async Task<bool> SendMessage(RestRequest request)
        {

            try
            {

                //if no endpoint is set don't send the message!
                if (string.IsNullOrWhiteSpace(clientEndpoint)) return true;

                var client = new RestClient(clientEndpoint);
                client.UseNewtonsoftJson();

                if (!String.IsNullOrWhiteSpace(clientApiKey)) request.AddHeader("Authorization", $"ApiKey {clientApiKey}");

                var response = await client.ExecuteAsync(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK) return true;

                //if we get here we have an issue
                Log.Error(STR_LoggingCategory, "Error processing message", "Status Code: {0}  Message: {1}", response.StatusCode, response.ErrorMessage);
                return false;
            }
            catch (Exception ex)
            {
                Log.Error(ex, STR_LoggingCategory, "Exception processing message", null, null);
                return false;
            }

        }
    }
}
