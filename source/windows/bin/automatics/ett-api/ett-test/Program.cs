﻿using ett_api_logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ett_api_logic.Enums;

namespace ett_test
{
    class Program
    {
        static void Main(string[] args)
        {

            var grop = Packages.AudioPackages;

            var testPackage = new TestPackage(1);
            testPackage.PropertyChanged += TestPackage_PropertyChanged;
            testPackage.TrackedPropertyValueChanging += TestPackage_TrackedPropertyValueChanging;
            testPackage.TrackedPropertyValueChanged += TestPackage_TrackedPropertyValueChanged;

            testPackage.ButtonName.Init("Test Button");
            testPackage.TestStringParameter.Init("Test String");
            testPackage.TestIntParameter.Init(10);
            testPackage.TestBoolParameter.Init(false);

            Console.WriteLine("Press any key to make changes");
            Console.ReadLine();

            testPackage.ButtonName.Change("New Button Name", ChangeSources.API);
            testPackage.TestStringParameter.Change("New Test String", ChangeSources.API);
            testPackage.TestIntParameter.Change(20,  ChangeSources.API);
            testPackage.TestBoolParameter.Change(false, ChangeSources.API);  //no change!

            Console.WriteLine("Press any key to accept changes");
            Console.ReadLine();

            testPackage.AcceptAll();

            Console.WriteLine("Press any key to make changes");
            Console.ReadLine();

            testPackage.ButtonName.Change("New Button Name 2", ChangeSources.BNCS);
            testPackage.TestStringParameter.Change("New Test String 2", ChangeSources.BNCS);
            testPackage.TestIntParameter.Change(30, ChangeSources.BNCS);
            testPackage.TestBoolParameter.Change(false, ChangeSources.BNCS);  //no change!

            Console.WriteLine("Press any key to reject changes");
            Console.ReadLine();

            testPackage.RejectAll();

            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }

        private static void TestPackage_TrackedPropertyValueChanged(object sender, TrackedPropertyValueChangedEventArgs e)
        {
            var value = ((BasePackage)sender).GetValue(e.TrackedPropertyPath);
            Console.WriteLine($"Value changed: {e.TrackedPropertyName} value: {value}  source: {e.ChangeSource}");
        }

        private static void TestPackage_TrackedPropertyValueChanging(object sender, TrackedPropertyValueChangedEventArgs e)
        {
            var value = ((BasePackage)sender).GetValue(e.TrackedPropertyPath);
            Console.WriteLine($"Value changing: {e.TrackedPropertyName} value: {value}  source: {e.ChangeSource}");
        }

        private static void TestPackage_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var value = ((BasePackage)sender).GetValue(e.PropertyName);
            Console.WriteLine($"Property changed: {e.PropertyName} value: {value}");
        }
    }
}
