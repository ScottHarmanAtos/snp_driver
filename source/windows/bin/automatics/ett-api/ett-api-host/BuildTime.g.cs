﻿using System;
namespace driver_template.helpers
{
    public static partial class Constants
    {
        public static DateTime CompilationTimestampUtc { get { return new DateTime(637485748069472219L, DateTimeKind.Utc); } }
        public static String CompilationUser { get { return "AzureAD\\KennyMunro"; } }
    }
}