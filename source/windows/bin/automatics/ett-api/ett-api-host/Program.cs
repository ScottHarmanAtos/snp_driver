﻿using ett_api_client;
using ett_api_ipc;
using ett_api_logic;
using ett_api_web;
using Gibraltar.Agent;
using IniParser;
using IniParser.Model;
using NamedPipeWrapper;
using Pastel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static BNCS.Config;

namespace ett_api_host
{
    class Program
    {
        
        const string STR_LoggingCategory = "ett-api-host";

        private static uint packageRange;
        private static uint deviceId;

        private static FileIniDataParser iniParser;
        static NamedPipeServer<msgDb> dbServer;
        static NamedPipeServer<msgRoute> routeServer;
        static NamedPipeServer<msgLock> lockServer;
        static NamedPipeServer<msgState> stateServer;

        private static EttClient ettClient;
        static string serverBinding;
        static string serverApiKey;
        static bool autoCommit;

        static void Main(string[] args)
        {

            try
            {

                WriteHeaderLine($"");
                WriteHeaderLine($"        BNCS ETT API HOST");
                WriteHeaderLine($"        Version: {Assembly.GetEntryAssembly().GetName().Version}");
                WriteHeaderLine($"        Built: {driver_template.helpers.Constants.CompilationTimestampUtc}");
                WriteHeaderLine($"        by {driver_template.helpers.Constants.CompilationUser}");
                WriteHeaderLine($"        Copyright (c) 2020 Atos|Syntel");
                WriteHeaderLine($"");

                Log.Initializing += Log_Initializing;
                Log.MessageAlert += Log_MessageAlert;
                Log.StartSession();

                Log.Information(STR_LoggingCategory, "Starting model build", null, null);

                //read in the InstanceId from the arguments
                if (args.Length > 0)
                {

                    var instanceId = args[0];

                    //lookup that device instance
                    var mainInstance = GetInstance(instanceId);
                    if (mainInstance == null) throw new Exception($"Instance {instanceId} could not be located");

                    //look for the linked packager
                    var strPackagerName = mainInstance.GetSetting("packager");
                    if (string.IsNullOrWhiteSpace(strPackagerName)) throw new Exception("No packager setting found for instance");

                    //retrieve the packager instance
                    var packagerInstance = GetInstance(strPackagerName);
                    if (packagerInstance == null) throw new Exception($"Packager instance {strPackagerName} could not be located");

                    //get the packager router child
                    var routerInstance = packagerInstance.GetChildInstance("router").Instance;
                    if (routerInstance == null) throw new Exception($"Packager router instance could not be located");

                    //get the first device in the packager router
                    var routerChild = routerInstance.GetChildInstance("packager_router_1").Instance;
                    if (routerChild == null) throw new Exception($"Packager router first child could not be located");

                    deviceId = routerChild.DeviceId;

                    packageRange = mainInstance.GetSetting<uint>("package_range", 64000);
                    Log.Information(STR_LoggingCategory, "Package Range", packageRange.ToString(), null);

                    serverBinding = mainInstance.GetSetting("server_binding", "http://+:8080");
                    Log.Information(STR_LoggingCategory, "Server Binding", serverBinding, null);

                    serverApiKey = mainInstance.GetSetting("server_api_key", "321");
                    Log.Information(STR_LoggingCategory, "Server API Key", serverApiKey, null);

                    autoCommit = mainInstance.GetSetting<bool>("auto_commit", false);
                    Log.Information(STR_LoggingCategory, "Auto Commit", autoCommit.ToString(), null);

                }
                else throw new Exception("InstanceID missing from the command line");

                iniParser = new FileIniDataParser();

                var sw = new Stopwatch();
                sw.Start();

                Packages.InitPackages(packageRange);
                Packages.OnDbWrite += Packages_OnDbWrite;
                Packages.OnRouteRequested += Packages_OnRouteRequested;

                dbServer = new NamedPipeServer<msgDb>(msgDb.PipeName);
                dbServer.ClientConnected += DbServer_ClientConnected;
                dbServer.ClientDisconnected += DbServer_ClientDisconnected;
                dbServer.ClientMessage += DbServer_ClientMessage;

                routeServer = new NamedPipeServer<msgRoute>(msgRoute.PipeName);
                routeServer.ClientMessage += IwServer_ClientMessage;

                lockServer = new NamedPipeServer<msgLock>(msgLock.PipeName);
                lockServer.ClientMessage += LockServer_ClientMessage;

                stateServer = new NamedPipeServer<msgState>(msgState.PipeName);
                stateServer.ClientMessage += StateServer_ClientMessage;
                stateServer.ClientConnected += StateServer_ClientConnected;
                stateServer.ClientDisconnected += StateServer_ClientDisconnected;

                dbServer.Start();
                routeServer.Start();
                lockServer.Start();
                stateServer.Start();

                var clientEndpoint = GetThisWorkstation().GetSetting("client_endpoint", "https://virtserver.swaggerhub.com/Pixsel-Limited/BNCSClientApi/1.0.0/");
                Log.Information(STR_LoggingCategory, "Client Endpoint", clientEndpoint, null);

                var clientApiKey = GetThisWorkstation().GetSetting("client_api_key", null);
                Log.Information(STR_LoggingCategory, "Client API Key", clientApiKey, null);

                ettClient = new EttClient(clientEndpoint, clientApiKey);

                Log.Warning(STR_LoggingCategory, "Starting Database Load", "Packager Device Id: {0}  Index Range: {1}", deviceId, packageRange);

                LoadDatabase(DbConstants.dbSourceButtonName);
                LoadDatabase(DbConstants.dbDestButtonName);
                LoadDatabase(DbConstants.dbSourcePackageTitle);
                LoadDatabase(DbConstants.dbDestPackageTitle);
                LoadDatabase(DbConstants.dbSourcePackageTags);
                LoadDatabase(DbConstants.dbDestPackageTags);
                LoadDatabase(DbConstants.dbAudioSourcePackages);
                LoadDatabase(DbConstants.dbDestLevels);
                LoadDatabase(DbConstants.dbMCRUmd);

                LoadDatabase(DbConstants.dbProductionUmd);
                LoadDatabase(DbConstants.dbSourceVideo);
                LoadDatabase(DbConstants.dbAudioDestinationPairIndex);
                LoadDatabase(DbConstants.dbAudioDestinationPairTagType);
                LoadDatabase(DbConstants.dbAudioDestinationPairTagTypeSubst1);
                LoadDatabase(DbConstants.dbAudioDestinationPairTagTypeSubst2);
                LoadDatabase(DbConstants.dbAudioReturn);

                LoadDatabase(DbConstants.dbDefaultSourcePackage);
                LoadDatabase(DbConstants.dbFunctionalSourceArea);
                LoadDatabase(DbConstants.dbFunctionalDestArea);
                LoadDatabase(DbConstants.dbFunctionalSourceName);
                LoadDatabase(DbConstants.dbFunctionalDestName);

                LoadDatabase(DbConstants.dbSourceEventName);
                LoadDatabase(DbConstants.dbSourceSpevList1);
                LoadDatabase(DbConstants.dbSourceSpevList2);
                LoadDatabase(DbConstants.dbSourceSpevList3);
                LoadDatabase(DbConstants.dbSourceSpevList4);
                LoadDatabase(DbConstants.dbSourceClassId);
                LoadDatabase(DbConstants.dbSourceRecordId);
                LoadDatabase(DbConstants.dbSourceDates);
                LoadDatabase(DbConstants.dbSourceTeams);
                LoadDatabase(DbConstants.dbSourceStatus);
                LoadDatabase(DbConstants.dbMCRNotes);

                LoadDatabase(DbConstants.dbVideoHubTags);
                LoadDatabase(DbConstants.dbAudioHubTags);
                LoadDatabase(DbConstants.dbLanguageUseHubTags);
                LoadDatabase(DbConstants.dbAudioTypeTags);
                LoadDatabase(DbConstants.dbOwnerTags);
                LoadDatabase(DbConstants.dbDelegateTags);
                LoadDatabase(DbConstants.dbRiedelTags);

                LoadDatabase(DbConstants.dbAudioSourcePackageButtonName);
                LoadDatabase(DbConstants.dbAudioSourcePackageLongName);
                LoadDatabase(DbConstants.dbAudioSourcePackageTagFields1);
                LoadDatabase(DbConstants.dbAudioSourcePackageTagFields2);
                LoadDatabase(DbConstants.dbAudioSourcePackagePairs);
                LoadDatabase(DbConstants.dbAudioSourcePackageReturnPairs);
                LoadDatabase(DbConstants.dbAudioSourcePackageVideoLinkIndex);
                LoadDatabase(DbConstants.dbAudioSourcePackageCFLabels);
                LoadDatabase(DbConstants.dbAudioSourcePackageConfLabels);

                LoadDatabase(DbConstants.dbAudioPresetName);
                LoadDatabase(DbConstants.dbAudioPresetTypeTag);
                LoadDatabase(DbConstants.dbAudioPresetTypeTagSubst1);
                LoadDatabase(DbConstants.dbAudioPresetTypeTagSubst2);

                LoadDatabase(DbConstants.db0081);

                LoadDatabase(DbConstants.dbSourcePackageLocks);
                LoadDatabase(DbConstants.dbAudioPackageLocks);
                LoadDatabase(DbConstants.dbDestPackageEditLocks);
                LoadDatabase(DbConstants.dbDestPackageRouteLocks);

                sw.Stop();

                Log.Warning(STR_LoggingCategory, "Completed model build", "Build time: {0}", sw.Elapsed);

                Task.Delay(2500).Wait();

                var webServer = new WebServer();
                webServer.Start(serverBinding, serverApiKey, autoCommit);

                WriteHeaderLine("     Web Server Started");
                WriteHeaderLine($"     Binding: {serverBinding}");

                ett_api_logic.Packages.InitComplete = true;

                WriteHeaderLine("     Type EXIT<return> to shutdown");
                string returnString = string.Empty;

                do
                {
                    returnString = Console.ReadLine();
                } while (returnString.ToUpper() != "EXIT");

                WriteHeaderLine("     SHUTTING DOWN");

                dbServer.Stop();
                routeServer.Stop();
                lockServer.Stop();
                stateServer.Stop();

                webServer.Dispose();

                Log.EndSession();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        private static void StateServer_ClientConnected(NamedPipeConnection<msgState, msgState> connection)
        {
            //when a client connects, send an empty message to trigger a state update
            stateServer.PushMessage(new msgState());
        }

        private static void StateServer_ClientDisconnected(NamedPipeConnection<msgState, msgState> connection)
        {
            //client disconnection means the driver has gone down so set the state to false
            Packages.SetDriverState(Packages.DriverStates.Disconnected);
        }

        private static void StateServer_ClientMessage(NamedPipeConnection<msgState, msgState> connection, msgState message)
        {
            if (message.Active)
            {
                //we are fully connected & txRx
                Packages.SetDriverState(Packages.DriverStates.Connected);
            }
            else
            {
                //driver connected but in RxOnly mode
                Packages.SetDriverState(Packages.DriverStates.RxOnly);
            }
        }

        private static void Packages_OnRouteRequested(object sender, ett_api_logic.Events.RouteRequestEventArgs e)
        {

            var message = new msgRoute(e.SourcePackage, e.DestPackage);
            Log.Information(STR_LoggingCategory, "Routing Pipe Client Outgoing Message", "Message: {0}", message);
            Console.WriteLine($"RTE MSG OUT {message}");

            try
            {
                routeServer.PushMessage(message);
            }
            catch (Exception ex)
            {
                Log.Error(ex, STR_LoggingCategory, "Error sending Routing Pipe Client Outgoing Message", "Message: {0}", message);
            }

        }

        private static void LockServer_ClientMessage(NamedPipeConnection<msgLock, msgLock> connection, msgLock message)
        {

            Log.Information(STR_LoggingCategory, "Locking Pipe Client Incomming Message", "Message: {0}", message);
            Console.WriteLine($"LOCK MSG IN {message}");

            if (message.Index == 0)
            {
                //special case, reload all locks
                Packages.SetAllDestinationLocks();
            }
            else
            {
                //set specific lock state
                Packages.SetDestinationLock(message.Index, message.Locked);
            }
        }

        private static void IwServer_ClientMessage(NamedPipeConnection<msgRoute, msgRoute> connection, msgRoute message)
        {
            Packages.HandleRouteRevertive(message.SourceIndex, message.DestinationIndex);
        }

        private static void Log_MessageAlert(object sender, LogMessageAlertEventArgs e)
        {

            foreach (var message in e.Messages)
            {

                var fgColor = Color.White;
                var bgColor = Color.Black;

                switch (message.Severity)
                {
                    case LogMessageSeverity.None:
                        break;
                    case LogMessageSeverity.Critical:
                        fgColor = Color.White;
                        bgColor = Color.Red;
                        break;
                    case LogMessageSeverity.Error:
                        fgColor = Color.Red;
                        bgColor = Color.Black;
                        break;
                    case LogMessageSeverity.Warning:
                        fgColor = Color.Orange;
                        bgColor = Color.Black;
                        break;
                    case LogMessageSeverity.Information:
                        break;
                    case LogMessageSeverity.Verbose:
                        break;
                    default:
                        break;
                }

                Console.WriteLine($"{message.Timestamp} {message.Severity.ToString().ToUpper()}".Pastel(fgColor).PastelBg(bgColor));
                Console.WriteLine(message.CategoryName.Pastel(fgColor).PastelBg(bgColor));
                Console.WriteLine(message.Caption.Pastel(fgColor).PastelBg(bgColor));
                Console.WriteLine(message.Description.Pastel(fgColor).PastelBg(bgColor));
                if (message.Exception != null) Console.WriteLine(message.Exception.ToString().Pastel(fgColor).PastelBg(bgColor));

            }
        }

        private static void Packages_OnDbWrite(object sender, ett_api_logic.events.DbWriteEventArgs e)
        {
            var message = new msgDb(e.DbNumber, e.DbIndex, e.Value);
            Log.Information(STR_LoggingCategory, "Database Pipe Client Outgoing Message", "Message: {0}", message);
            Console.WriteLine($"DB MSG OUT {message}");

            try
            {
                dbServer.PushMessage(message);
            }
            catch (Exception ex)
            {
                Log.Error(ex,STR_LoggingCategory, "Error sending Database Pipe Client Outgoing Message", "Message: {0}", message);
            }
        }

        private static void DbServer_ClientMessage(NamedPipeConnection<msgDb, msgDb> connection, msgDb message)
        {
            Log.Information(STR_LoggingCategory, "Database Pipe Client Incomming Message", "Connection name: {0}  Message: {1}", connection.Name, message);
            Console.WriteLine($"DB MSG IN  {message}");
            if (message.Init) Packages.InitDbValue(message.DbNumber, message.DbIndex, message.Value);
            else Packages.ChangeDbValue(message.DbNumber, message.DbIndex, message.Value);
        }

        private static void DbServer_ClientDisconnected(NamedPipeConnection<msgDb, msgDb> connection)
        {
            Log.Warning(STR_LoggingCategory, "Database Pipe Client Disconnected", "Connection name: {0}", connection.Name);
            Console.WriteLine($"Client Disconnected {connection.Name}");
        }

        private static void DbServer_ClientConnected(NamedPipeConnection<msgDb, msgDb> connection)
        {
            Log.Warning(STR_LoggingCategory, "Database Pipe Client Connected", "Connection name: {0}", connection.Name);
            Console.WriteLine($"Client Connected {connection.Name}");
        }

        static void Log_Initializing(object sender, LogInitializingEventArgs e)
        {
            e.Configuration.Viewer.Enabled = true;
            e.Configuration.Viewer.HotKey = "CTRL + F12";
            e.Configuration.Publisher.ProductName = "BNCS";
            e.Configuration.Publisher.ApplicationName = "ETT API Host";
#if (!DEBUG)
    //this is not a debug compile, disable the agent.
    e.Cancel = true;
#endif
        }

        private static void LoadDatabase(uint dbNumber)
        {

            Log.Warning(STR_LoggingCategory, "Loading Database", "DBNumber : {0}", dbNumber);
            var sw = new Stopwatch();
            sw.Start();

            try
            {

                //db file name extensions might or might not be padded!
                string dbName = $"{BNCSConfigPath}/system/dev_{deviceId}.db{dbNumber}";
                string dbAltName = $"{BNCSConfigPath}/system/dev_{deviceId}.db{dbNumber.ToString("D4")}";

                IniData data = null;

                if (System.IO.File.Exists(dbName))
                {
                    data = iniParser.ReadFile(dbName);
                }
                else if (System.IO.File.Exists(dbAltName))
                {
                    data = iniParser.ReadFile(dbAltName);
                }
                else
                {
                    //Log.Critical(STR_LoggingCategory, $"Unable to find database file for index {dbIndex}", null);
                    return;  //can't continue
                }

                for (uint i = 1; i <= packageRange; i++)
                {
                    var value = (data[$"Database_{dbNumber}"][i.ToString("D4")]);

                    try
                    {
                        //Set the value in the package model
                        Packages.InitDbValue(dbNumber, i, value);
                        //dbClient.PushMessage(new msgDb(dbNumber, i, value, true));
                    }
                    catch (Exception ex)
                    {
                        //capture error
                    }

                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, STR_LoggingCategory, "ERROR Loading Database", "DBNumber : {0}", dbNumber);
            }
            finally
            {
                sw.Stop();
                Log.Warning(STR_LoggingCategory, "DB Load Complete", "DBNumber : {0}  Duration : {1}", dbNumber, sw.Elapsed);
            }

        }

        private static void WriteHeaderLine(string line)
        {
            Console.WriteLine(line.PadRight(50).Pastel(Color.Black).PastelBg(Color.LightGray));
        }

    }
}
