﻿using ett_api_domain;
using ett_api_logic;
using ett_api_logic.packages;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace ett_api_web.Controllers
{

    [Authorize()]
    public class ControlController : ApiController
    {

        public enum PackageTypes
        {
            AudioPackage = 0,
            SourcePackage = 1,
            DestinationPackage = 2
        }

        [HttpGet]
        [Route("acceptChanges")]
        [SwaggerOperation("AcceptChanges")]
        public object AcceptChanges(PackageTypes PackageType, uint PackageIndex)
        {

            var package = GetPackage(PackageType, PackageIndex);

            if (package == null) return NotFound();

            package.AcceptAll();

            return package.GetDbChanges();

        }

        [HttpPost]
        [Route("commitChanges")]
        [SwaggerOperation("CommitChanges")]
        public object CommitChanges(PackageTypes PackageType, uint PackageIndex)
        {

            var package = GetPackage(PackageType, PackageIndex);

            if (package == null) return NotFound();

            package.CommitDbChanges();

            return Ok();

        }


        [HttpPost]
        [Route("rejectChanges")]
        [SwaggerOperation("RejectChanges")]
        public object RejectChanges(PackageTypes PackageType, uint PackageIndex)
        {

            var package = GetPackage(PackageType, PackageIndex);

            if (package == null) return NotFound();

            package.RejectAll();

            return Ok();

        }

        private BasePackage GetPackage(PackageTypes PackageType, uint PackageIndex)
        {

            BasePackage package = null;

            switch (PackageType)
            {
                case PackageTypes.AudioPackage:
                    package = Packages.AudioPackages.GetPackage(PackageIndex);
                    break;
                case PackageTypes.SourcePackage:
                    package = Packages.SourcePackages.GetPackage(PackageIndex);
                    break;
                case PackageTypes.DestinationPackage:
                    package = Packages.DestinationPackages.GetPackage(PackageIndex);
                    break;
                default:
                    break;
            }

            return package;
        }

    }
}
