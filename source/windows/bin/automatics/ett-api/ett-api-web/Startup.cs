﻿using Gibraltar.Agent.Web.Mvc.Filters;
using Microsoft.Owin.Security.ApiKey;
using Microsoft.Owin.Security.ApiKey.Contexts;
using Owin;
using Patcheetah.JsonNET;
using Patcheetah.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace ett_api_web
{
    public class Startup
    {

        private static string apiKey = "123";

        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {

            appBuilder.UseApiKeyAuthentication(new ApiKeyAuthenticationOptions()
            {
                Provider = new ApiKeyAuthenticationProvider()
                {
                    OnValidateIdentity = ValidateIdentity
                }
            });

            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();

            //Register the three filters (Order is not important)
            GlobalConfiguration.Configuration.Filters.Add(new WebApiRequestMonitorAttribute());
            GlobalFilters.Filters.Add(new MvcRequestMonitorAttribute());
            GlobalFilters.Filters.Add(new UnhandledExceptionAttribute());


            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "BNCS ETT API",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            SwaggerConfig.Register(config);

            PatchEngine.Init(cfg =>
            {
                cfg.EnableNestedPatching();
            });

            appBuilder.UseWebApi(config);

            config.EnsureInitialized();

        }

        public static void SetApiKey(string ApiKey)
        {
            apiKey = ApiKey;
        }

        private async Task ValidateIdentity(ApiKeyValidateIdentityContext context)
        {
            if (context.ApiKey == apiKey)
            {
                context.Validate();
            }
        }

    }
}
