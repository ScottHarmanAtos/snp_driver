﻿using Gibraltar.Agent;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ett_api_web
{
    public class WebServer : IDisposable
    {

        public static bool AutoCommit = false;
        const string STR_LoggingCategory = "BNCS_ETT_API.WebServer";

        IDisposable restServer;

        public void Start(string baseAddress, string apiKey, bool autoCommit)
        {

            Log.Information(STR_LoggingCategory, "Web Server Starting", "Address: {0}", baseAddress);
            AutoCommit = autoCommit;
            
            // Start OWIN host 
            restServer = WebApp.Start<Startup>(url: baseAddress);
            Startup.SetApiKey(apiKey);
        }

        public void Dispose()
        {
            restServer.Dispose();
        }

    }
}
