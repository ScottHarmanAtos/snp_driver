/*************************************/
/* Written by David Yates            */
/* Copyright Siemens IT Systems 2007 */
/*************************************/
#define timermain

#include "timer.h"

// our global map of running timers
map< int, client > timer::timers;



timer::timer( ItimerCallback * c )
{
	callback = c;
}

timer::~timer()
{

}

int timer::timerStart( int id, int duration )
{
	timerStop( id );


	int ret = SetTimer( 0, 0, duration, (TIMERPROC) timerProc );

	client c( callback, id );

	timers[ ret ] = c;
	
	return ret;
}


int timer::timerStop( int id )
{
	map< int, client >::iterator it;

	for( it = timers.begin() ; it != timers.end() ; ++it )
	{
		if(( it->second.m_callback == callback ) && (it->second.m_id == id ))
		{
			KillTimer( 0, it->first );
			timers.erase( it );
			return 0;
		}
	}
	return 1;
}



VOID CALLBACK timerProc( HWND hwnd,  UINT uMsg, UINT_PTR idEvent, DWORD dwTime )
{
	client c = timer::timers[ idEvent ];

	c.m_callback->timerCallback( c.m_id );
}
