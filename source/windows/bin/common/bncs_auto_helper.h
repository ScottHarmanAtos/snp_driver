	#if !defined(AFX_BNCS_HELPER_H)
	#define AFX_BNCS_HELPER_H

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000
	#pragma warning(disable : 4786 4996)
//
	// colledia includes
	#include <bncs_string.h>
	#include <bncs_stringlist.h>

//TODO include bncs_string and bncs_config
int getInstanceDevice(const bncs_string &strInstance);
int getInstanceOffset(const bncs_string &strInstance);
bncs_string getInstanceType(const bncs_string& strInstance);
bncs_string getInstanceAltId(const bncs_string& strInstance);
bool getDevSlot(bncs_string instance, bncs_string param, int* dev, int* slot, char* type);
bool getInstanceDeviceSettings( const bncs_string &strInstance, int* idev, int* ioffset, char* szlocation, char* szType, char* szAltId );

//int getInstanceSlot(const bncs_string& strInstance, bncs_string& strParam );
bncs_string getObjectSetting(const bncs_string& strObject, const bncs_string& strSetting);
int getRouterSize(int intDevice, int intSwitch);
bncs_string getWorkstationSetting(const bncs_string& strSetting);

//void gpiPoll(int intDevice, int intMin, int intMax);
//void gpiQuery(int intDevice, int intMin, int intMax);
//void gpiSet(int intDevice, int intIndex, int intValue);

void infoPoll(int intDevice, int intMin, int intMax);
//void infoQuery(int intDevice, int intMin, int intMax);
void infoWriteInt(int intDevice, int intValue, int intIndex);
void infoWriteString(int intDevice, const bncs_string& strValue, int intIndex);

void routerCrosspoint(int intDevice, int intSource, int intDest, const bncs_string& strMask);
void routerPoll(int intDevice, int intMin, int intMax);
//void routerQuery(int intDevice, int intMin, int intMax);

// more generic xml entries -- for CCM 
bncs_string getXMLItemIdSetting(const bncs_string& strFileName, const bncs_string& strItemId, const bncs_string& strSettingId);
bncs_string getXMLItemValue(const bncs_string& strFileName, const bncs_string& strItemId, const bncs_string& strSettingId, const bncs_string& strAttr = "value");
bncs_string getXMLListOfItems( const bncs_string& ssFileName, const bncs_string& strItemId="id"  );

#endif // !defined(BNCS_HELPER_H)
