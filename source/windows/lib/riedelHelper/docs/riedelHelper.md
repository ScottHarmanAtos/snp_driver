# Riedel Helper Dll

## Description

This is a helper dll for ring master panels

The purpose of this is to load files and do the heavy lifting to remove complexity from the panels.
It will load when the first dll that is using it is called and stay loaded as long as a dll that needs it exists.

It also defines some structs for max numbers and consistent naming.

There are also some enums and hash defines that were in riedelCommon.h before.

This loads config files:

* comms_ringmaster_outputs
* comms_rings

And Database 0,1,2,3 and 9 for each ring.

## How to use

Include the riedelHelper.h and reidelHelper.lib

Create a pointer to the ridelHelper.

```riedelHelper *rh;```

In the constructor call

```rh = riedelHelper::getriedelHelperObject();```

This loads the dll.


## Helpers

### riedelHelpers - struct containing

* databases - struct with the names and number of databases.
* max - struct with the max numbers
* offset - struct with the offsets in infodrivers
* icons - struct with icons - this might not be the best place for it?
* default - Default string names

## ringmaster_devices

Object created holding ringmaster devices.

* Main - int device
* Ifbs - int device
* Conference - int device
* Ports - map<int,int> Map of device to ports
* Ifb_pti - int device

## Devices

Object created holding all the device numbers and instance names for a ring.

* Grd
* Monitor
* Pti
* Alias
* Gpio
* Gain
* Conference
* Ifb
* Extras
* Lock

Also holds all the Instance string names as well.

## getPortType

Object that parses string and enums and vis versa, so compile time errors can't happen with string names.

## comms_ring

Object holds information about a ring. Loading from comms_rings.xml

* ring_id - Ring int
* ID
* label
* site
* instance
* **Devices** device object
* vaild - bool true if valid

* toString() gives all info about the object

## comms_ringmaster_port 

Object holds information about a single port on a ring.

* ring_port - RING.PORT
* ring_port_leading_zero - RING.PORT <-- 6 Chars
* ring_id - Ring int
* port - Port int
* instance - composite instance of ring
* slot_index - Slot Index within the Riedel it belongs to
* sourceName_short - Short source name
* sourceName_long - Long source name
* destName_short - Short dest name
* destName_long - Long dest name
* portType - Port type as an enum (ENUM_RIEDEL_PORT_TYPES)
* rrcs_address - RRCS Address of the port
* valid - bool true if valid
* **Devices** device object

* == and != operator overloaded to compare against ring_port

* toString() ives all info about the object 

### comms_ringmaster_output

Object holds information about a single output port on a ring. Plus information from **comms_ringmaster_outputs.outputs**

* **comms_ringmaster_port** port object
* instance - ringmaster instance
* slot_index - ringmaster slot for instance
* device - ringmaster device for instance
* valid - bool true if valid

* == and != operator overloaded to compare against ring_port

* toString() ives all info about the object 



### riedelHelper

Object that holds the loaded state and allows interaction with the model.

* ```AllPorts()``` - Gets all ports, returns ```map<bncs_string, comms_ringmaster_port>```
* ```SourcePorts()``` - Gets all source ports, returns ```map<bncs_string, comms_ringmaster_port>```
* ```OutputPorts()``` - Gets all output ports, returns ```map<bncs_string, comms_ringmaster_port>```
* ```GetRings()``` - Gets all rings, returns ```map<int, comms_rings>```
* ```GetPort(int ring, int port)``` - Gets ports, returns ```comms_ringmaster_port```
* ```GetPort(const bncs_string& ringport)``` - Gets ports, returns ```comms_ringmaster_port```
* ```GetOutputPort(int ring, int port)``` - Gets output port, returns ```comms_ringmaster_output```
* ```GetOutputPort(const bncs_string& ringport)``` - Gets output port, returns ```comms_ringmaster_output```
* ```GetRingmasterDevices()``` - Gets ringmaster devices, returns ```ringmaster_devices```
* ```GetRing(int ring)``` - Gets ring, returns ```comms_rings```
* ```GetRing(bncs_string comositeInstance)``` - Gets ring, returns ```comms_rings```
* ```GetPortFromRRCSAddress(int ring, bncs_string rrcsAddress)``` - Gets Port from ring and rrcsAddress, returns ```comms_ringmaster_port```
* ```GetPortFromRRCSAddress(bncs_string ringAndRRCSAddress)``` - Gets Port from ring and rrcsAddress, returns ```comms_ringmaster_port```