#pragma warning( disable: 4786 )
#include <windows.h>
#include "riedelHelper.h"

BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved )
{
    switch (ul_reason_for_call)
	{ 
		// on process attach if there are no styles loaded then go off and load them!
		case DLL_PROCESS_ATTACH:
//			OutputDebugString( "DLL_PROCESS_ATTACH\n" );
			if( !riedelHelper::m_riedelHelperObj )
			{
				riedelHelper::m_riedelHelperObj =  new riedelHelper;
			}	
			break;

		case DLL_PROCESS_DETACH:
//			OutputDebugString( "DLL_PROCESS_DETACH\n" );
			if( riedelHelper::m_riedelHelperObj )
			{
				delete riedelHelper::m_riedelHelperObj;
				riedelHelper::m_riedelHelperObj=0;
			}
			break;

		case DLL_THREAD_ATTACH:
//			OutputDebugString( "DLL_THREAD_ATTACH\n" );
			break;

		case DLL_THREAD_DETACH:
//			OutputDebugString( "DLL_THREAD_DETACH\n" );
			break;
    }
    return TRUE;
}
