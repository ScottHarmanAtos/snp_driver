#pragma warning( disable: 4786 )

#include "riedelHelper.h"
#include "bncs_config.h"
#include "bncs_shim_loader.h"
#include "bncs_shim.h"
#include "bncs_string.h"
#include "ccConfig.h"

//This could be passed in as an argument when the first component calls it, that is a little messy though.
//As is requires all components that call this to know the composite.
//Also this isn't going to change and if it does it will affect everything so it might as well go here.


//Create Empty reference.
comms_rings comms_rings::Empty = comms_rings();

const bncs_string riedelHelpers::defaults::KEY_MODE("KMD1");
const bncs_string riedelHelpers::defaults::TALK_LISTEN("TL");
const bncs_string riedelHelpers::defaults::DIM_MODE("D1");

const bncs_string riedelHelpers::icons::CONF("/images/comms/icon_conf.png");
const bncs_string riedelHelpers::icons::IFB("/images/comms/icon_ifb.png");
const bncs_string riedelHelpers::icons::LOGIC("/images/comms/icon_logic.png");
const bncs_string riedelHelpers::icons::PANEL("/images/comms/icon_panel.png");


const bncs_string riedelHelpers::icons::_ERROR("/images/comms/icon_error.png");
const bncs_string riedelHelpers::icons::_4WIRE("/images/comms/icon_4wire.png");
const bncs_string riedelHelpers::icons::_4WIRE_SPLIT("/images/comms/icon_4wire_split.png");
const bncs_string riedelHelpers::icons::DUAL("/images/comms/icon_dual.png");
const bncs_string riedelHelpers::icons::LOCAL_CH2("/images/comms/icon_local_ch2.png");
const bncs_string riedelHelpers::icons::REMOTE_CH2("/images/comms/icon_remote_ch2.png");
const bncs_string riedelHelpers::icons::NONE("");

const bncs_string riedelHelpers::icons::PIXMAP_LOCATION_CONF("pixmap.bottomRight");
const bncs_string riedelHelpers::icons::PIXMAP_LOCATION_IFB("pixmap.bottomLeft");

const bncs_string riedelHelpers::icons::PIXMAP_CONF(PIXMAP_LOCATION_CONF + "=" + CONF);
const bncs_string riedelHelpers::icons::PIXMAP_IFB(PIXMAP_LOCATION_IFB + "=" + IFB);


const bncs_string getPortType::Input("input");
const bncs_string getPortType::Output("output");
const bncs_string getPortType::Split("split");
const bncs_string getPortType::Wire4("4wire");
const bncs_string getPortType::Panel("panel");
const bncs_string getPortType::Dual("dual");
const bncs_string getPortType::Codec("codec");
const bncs_string getPortType::PoolPort("poolport");
const bncs_string getPortType::Mapping_Input("mapping_input");
const bncs_string getPortType::Mapping_Output("mapping_output");
const bncs_string getPortType::None("none");

const bncs_string getPortType::Long_NA("Not Assigned");
const bncs_string getPortType::Long_Split("Split");
const bncs_string getPortType::Long_4Wire("4W");
const bncs_string getPortType::Long_Panel("Panel");
const bncs_string getPortType::Long_Dual_Panel("Dual Panel");
const bncs_string getPortType::Long_Not_used("Not Used");
const bncs_string getPortType::Long_Codec("Codec");
const bncs_string getPortType::Long_PoolPort("PoolPort");

const bncs_string getPortType::Short_NA("NA");
const bncs_string getPortType::Short_Split("Split");
const bncs_string getPortType::Short_4Wire("4W");
const bncs_string getPortType::Short_Panel("Panel");
const bncs_string getPortType::Short_Dual_Panel("Panel");
const bncs_string getPortType::Short_Not_used("NU");
const bncs_string getPortType::Short_Codec("Codec");
const bncs_string getPortType::Short_PoolPort("PoolPort");


bool getPortType::IsValid(int portType)
{
	switch (portType)
	{
	case 0:
		return false;
	case 3:
		return true;
	case 4:
		return true;
	case 5:
		return true;
	case 6:
		return true;
	case 7:
		return false;   // you can't do anything with a Codec port
	case 8:
		return true;
	default:
		return false;
	}
}

const list<int> getPortType::ValidPorts()
{
	return list<int>{3, 4, 5, 6, 8};
}

const ENUM_PORT_VIEWS getPortType::view_port_type(const bncs_string& s)
{
	bncs_string ss = s.lower();
	if (ss == Input)
	{
		return VIEW_PORT_TYPE_INPUT;
	}
	else if (ss == Output)
	{
		return VIEW_PORT_TYPE_OUTPUT;
	}
	else  if (ss == Split)
	{
		return VIEW_PORT_TYPE_SPLIT;
	}
	else if (ss == Wire4)
	{
		return VIEW_PORT_TYPE_4WIRE;
	}
	else if (ss == Panel)
	{
		return VIEW_PORT_TYPE_PANEL;
	}
	else if (ss == Dual)
	{
		return VIEW_PORT_TYPE_PANEL_DUAL_CHANNEL;
	}
	else if (ss == Codec)
	{
		return VIEW_PORT_TYPE_CODEC;
	}
	else if (ss == PoolPort)
	{
		return VIEW_PORT_TYPE_POOL_PORT;
	}
	else if (ss == Mapping_Input)
	{
		return VIEW_PORT_MAPPING_INPUT;
	}
	else if (ss == Mapping_Output)
	{
		return VIEW_PORT_MAPPING_OUTPUT;
	}
	else
	{
		return VIEW_PORT_TYPE_NONE;
	}
}

const bncs_string& getPortType::view_port_type(ENUM_PORT_VIEWS i)
{
	switch (i)
	{
	case VIEW_PORT_TYPE_INPUT:
		return Input;
	case VIEW_PORT_TYPE_OUTPUT:
		return Output;
	case VIEW_PORT_TYPE_SPLIT:
		return Split;
	case VIEW_PORT_TYPE_4WIRE:
		return Wire4;
	case VIEW_PORT_TYPE_PANEL:
		return Panel;
	case VIEW_PORT_TYPE_PANEL_DUAL_CHANNEL:
		return Dual;
	case VIEW_PORT_MAPPING_INPUT:
		return Mapping_Input;
	case VIEW_PORT_MAPPING_OUTPUT:
		return Mapping_Output;
	default:
		return None;
	}
}

const ENUM_RIEDEL_PORT_TYPES getPortType::port_type(const bncs_string& s)
{
	bncs_string ss = s.lower();
	if (ss == Input)
	{
		return PORT_TYPE_INPUT;
	}
	else if (ss == Output)
	{
		return PORT_TYPE_OUTPUT;
	}
	else  if (ss == Split)
	{
		return PORT_TYPE_SPLIT;
	}
	else if (ss == Wire4)
	{
		return PORT_TYPE_4WIRE;
	}
	else if (ss == Panel)
	{
		return PORT_TYPE_PANEL;
	}
	else if (ss == Dual)
	{
		return PORT_TYPE_PANEL_DUAL_CHANNEL;
	}
	else if (ss == Codec)
	{
		return PORT_TYPE_CODEC;
	}
	else if (ss == PoolPort)
	{
		return PORT_TYPE_POOL_PORT;
	}
	else
	{
		return PORT_TYPE_NONE;
	}
}

const bncs_string& getPortType::port_type(ENUM_RIEDEL_PORT_TYPES portType)
{
	switch (portType)
	{
	case PORT_TYPE_INPUT:
		return Input;
	case PORT_TYPE_OUTPUT:
		return Output;
	case PORT_TYPE_SPLIT:
		return Split;
	case PORT_TYPE_4WIRE:
		return Wire4;
	case PORT_TYPE_PANEL:
		return Panel;
	case PORT_TYPE_PANEL_DUAL_CHANNEL:
		return Dual;
	case PORT_TYPE_CODEC:
		return Codec;
	case PORT_TYPE_POOL_PORT:
		return PoolPort;
	default:
		return None;
	}
}


const bncs_string& getPortType::Name_Long(ENUM_RIEDEL_PORT_TYPES portType)
{
	switch (portType)
	{
	case PORT_TYPE_INPUT:
		return Long_NA;
	case PORT_TYPE_SPLIT:
		return Long_Split;
	case PORT_TYPE_4WIRE:
		return Long_4Wire;
	case PORT_TYPE_PANEL:
		return Long_Panel;
	case PORT_TYPE_PANEL_DUAL_CHANNEL:
		return Long_Dual_Panel;
	case PORT_TYPE_CODEC:
		return Long_Codec;
	case PORT_TYPE_POOL_PORT:
		return Long_PoolPort;
	default:
		return Long_Not_used;
	}
}

const bncs_string& getPortType::Name_Type(ENUM_RIEDEL_PORT_TYPES portType)
{
	switch (portType)
	{
	case PORT_TYPE_NONE:
		return Short_NA;
	case PORT_TYPE_SPLIT:
		return Short_Split;
	case PORT_TYPE_4WIRE:
		return Short_4Wire;
	case PORT_TYPE_PANEL:
		return Short_Panel;
	case PORT_TYPE_PANEL_DUAL_CHANNEL:
		return Short_Dual_Panel;
	case PORT_TYPE_CODEC:
		return Short_Codec;
	case PORT_TYPE_POOL_PORT:
		return Short_PoolPort;
	default:
		return Short_Not_used;
	}
}


riedelHelper *riedelHelper::m_riedelHelperObj;

riedelHelper::riedelHelper()
{
	//m_shim = bncs_shim_loader::csiShim();	

	load();
}

riedelHelper::~riedelHelper()
{
}

riedelHelper * riedelHelper::getriedelHelperObject( void )
{
	return m_riedelHelperObj;
}

COMMS_RINGMASTER_OUTPUT_LOOKUP riedelHelper::load_comms_ringmaster_output(const COMMS_RINGMASTER_PORT_LOOKUP& ports)
{
	bncs_config c("comms_ringmaster_outputs.outputs");

	COMMS_RINGMASTER_OUTPUT_LOOKUP comms_ringmaster_outputs_lookup;

	while (c.isChildValid())
	{
		COMMS_RINGMASTER_PORT_LOOKUP::const_iterator it = ports.find(combineRingPort(c.childAttr("ring"),c.childAttr("port")));

		if (it != ports.end())
		{
			comms_ringmaster_output r(it->second, c.childAttr("instance"), GetDevice(c.childAttr("instance")), c.childAttr("slot_index"));

			comms_ringmaster_outputs_lookup[r.port.ring_port] = r;
		}

		c.nextChild();
	}

	return comms_ringmaster_outputs_lookup;
}

COMMS_RINGS_LOOPKUP riedelHelper::load_comms_rings(void)
{
	bncs_config c("comms_rings");

	COMMS_RINGS_LOOPKUP comms_rings_lookup;

	while (c.isChildValid())
	{
		bncs_config in(c);
		in.drillDown();

		comms_rings r(c.childAttr("id"));
		while (in.isChildValid())
		{
			if (in.childAttr("id") == "ID")
			{
				r.ID = in.childAttr("value");
			}
			else if (in.childAttr("id") == "label")
			{
				r.label = in.childAttr("value");
			}
			else if (in.childAttr("id") == "site")
			{
				r.site = in.childAttr("value");
			}
			else if (in.childAttr("id") == "instance")
			{
				r.instance = in.childAttr("value");

				r.devices = GetDeviceComposite(r.instance);
			}
			in.nextChild();
		}
		comms_rings_lookup[r.ring_id] = r;

		r.print();

		c.nextChild();
	}
	return comms_rings_lookup;
}

void riedelHelper::load_comms_ports(const COMMS_RINGS_LOOPKUP& rings)
{
	print(bncs_string("riedelHelper::load_comms_ports"));
	for (COMMS_RINGS_LOOPKUP::const_iterator r = rings.begin(); rings.end() != r; ++r)
	{	
	
		//Create Park Sources
		int i = 0; //For Park

		comms_ringmaster_port park(r->second.ring_id, i, r->second.instance, i, r->second.devices);
		park.sourceName_long = "PARKED";
		park.sourceName_short = "...";
		park.destName_long = "PARKED";
		park.destName_short = "PARKED";
		comms_ringmaster_all_port_lookup[park.ring_port] = park;

		//Increment to a useful number
		i++;
		//Get the Device	
		while (true)
		{
			comms_ringmaster_port p = comms_ringmaster_port(r->second.ring_id, i, r->second.instance, i, r->second.devices);

			//print(bncs_string("riedelHelper::load_comms_ports ring:%1 inst:%2 dev:%3").arg(r->second.ring_id).arg(r->second.instance).arg(r->second.devices.Grd));
			bncs_string port = dbm.getName(r->second.devices.Grd, riedelHelpers::databases::Port_Type, i);
			p.portType = (ENUM_RIEDEL_PORT_TYPES)port.toInt();

			//If its 0 or nothing its not a port
			if (p.portType > 0)
			{
				//Loop Here
				p.sourceName_long = sanitize(dbm.getName(r->second.devices.Grd, riedelHelpers::databases::Input_Long_Name, i));
				p.sourceName_short = sanitize(dbm.getName(r->second.devices.Grd, riedelHelpers::databases::Input_Port_Button_Name, i));
				p.destName_long = sanitize(dbm.getName(r->second.devices.Grd, riedelHelpers::databases::Output_Long_Name, i));
				p.destName_short = sanitize(dbm.getName(r->second.devices.Grd, riedelHelpers::databases::Output_Port_Button_Name, i));
				bncs_string assignGroup = sanitize(dbm.getName(r->second.devices.Grd, riedelHelpers::databases::TB_Assign_Groups, i));
				p.rrcs_address = dbm.getName(r->second.devices.Grd, riedelHelpers::databases::Port_Address,i);
				p.source_8_char = sanitize(dbm.getName(r->second.devices.Grd, riedelHelpers::databases::Input_8_Char,i));
				p.dest_8_char = sanitize(dbm.getName(r->second.devices.Grd, riedelHelpers::databases::Output_8_Char,i));

				comms_ringmaster_rrcs_lookup[bncs_string(r->second.ring_id) + "#" + p.rrcs_address] = p;
				comms_ringmaster_all_port_lookup[p.ring_port] = p;

				//Setup assign groups
				if (assignGroup.length() > 0)
				{
					assign_group_to_port_lookup[assignGroup].push_back(&comms_ringmaster_all_port_lookup[p.ring_port]);
					port_to_assign_group_lookup[RingPort(p.ring_id, p.port)] = assignGroup;
				}
			}
			else
			{
				if (port == "!!!")
				{
					print(bncs_string("riedelHelper::port %1 BREAK! (ring_port:%2)").arg(port).arg(p.ring_port));
					break;
				}
			}
			i++;
		}
	}
}

ringmaster_devices riedelHelper::load_ringmaster_devices(bncs_string ringamsterComposite)
{
	ringmaster_devices rmaster;
	bncs_string compositeInstance;

	rmaster.Composite = ringamsterComposite;

	if (instanceLookupComposite(ringamsterComposite, "main", compositeInstance))
	{
		rmaster.Main = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(ringamsterComposite, "ifbs", compositeInstance))
	{
		rmaster.Ifbs = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(ringamsterComposite, "conferences", compositeInstance))
	{
		rmaster.Conferences = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(ringamsterComposite, "ifb_pti", compositeInstance))
	{
		rmaster.Ifb_pti = GetDevice(compositeInstance);
	}

	int port = 0;
	map<int, int> Ports;
	while (true)
	{
		port++;
		if (instanceLookupComposite(ringamsterComposite, bncs_string("ports_%1").arg(port), compositeInstance))
		{
			Ports[port] = GetDevice(compositeInstance);
		}
		else
			break;
	}
	rmaster.Ports = Ports;
	return rmaster;
}

static const vector<comms_ringmaster_port*> EmptyCommsRingmasterPort;
static const vector<PanelInfo> EmptyPanelInfo;

const vector<comms_ringmaster_port*>& riedelHelper::GetAssignGroup(int ring, int port)
{
	auto n = port_to_assign_group_lookup.find(RingPort(ring, port));
	if (n != port_to_assign_group_lookup.end())
	{
		return GetAssignGroup(n->second);
	}
	return EmptyCommsRingmasterPort;
}

const vector<comms_ringmaster_port*>& riedelHelper::GetAssignGroup(bncs_string assignGroupName)
{
	auto v = assign_group_to_port_lookup.find(assignGroupName);
	if (v != assign_group_to_port_lookup.end())
	{
		return v->second;
	}
	return EmptyCommsRingmasterPort;
}

const bool riedelHelper::CanAccessPanel(int ring, int port)
{
	//If it is the local panel then yes
	if (ring == m_local_port.ring_id && port == m_local_port.port) return true;

	if (CanAccessAssignGroup(ring, port)) return true;

	//If we are not in the tb assign group, check if it is in the linking group.
	//It is possible to not in be an assign group but in the linking group?
	//If not the case remove this bit of code
	return CanAccessLocalPanels(ring, port);
}

const bool riedelHelper::CanAccessAssignGroup(int ring, int port)
{
	//Can access any port
	if (m_tb_assign_access[0] == "*") return true;

	//Other wise check if port ops position is in our access list
	auto n = port_to_assign_group_lookup.find(RingPort(ring, port));
	if (n != port_to_assign_group_lookup.end())
	{
		//Can access that workstation has the tb assign group
		if (m_tb_assign_access.find(n->second) >= 0)
			return true;
	}

	return false;
}

const bool riedelHelper::CanAccessLocalPanels(int ring, int port)
{
	//Go through all panels in the ops position, check if any appear in the 
	//can assign group or in are local group if any do then all panels should be accessable.
	auto ops = port_to_ops_postion_lookup.find(RingPort(ring, port));
	if (ops == port_to_ops_postion_lookup.end())
	{
		return false;
	}

	auto it = ops_position_to_panel_info.find(ops->second);
	if (it != ops_position_to_panel_info.end())
	{
		for (auto i = it->second.begin(); i != it->second.end(); ++i)
		{
			if (i->Ring == m_local_port.ring_id && i->Port == m_local_port.port) return true;

			if (CanAccessAssignGroup(i->Ring, i->Port)) return true;
		}
	}
	return false;
}

COMMS_RINGMASTER_PORT_LOOKUP riedelHelper::set_output_port_lookup(const COMMS_RINGMASTER_OUTPUT_LOOKUP& l)
{
	COMMS_RINGMASTER_PORT_LOOKUP p;
	for (COMMS_RINGMASTER_OUTPUT_LOOKUP::const_iterator it = l.begin(); it != l.end(); ++it)
	{
		p[it->second.port.ring_port] = it->second.port;
	}
	return p;
}

const comms_ringmaster_port& riedelHelper::GetPort(int ring, int port)
{

	COMMS_RINGMASTER_PORT_LOOKUP::const_iterator it = comms_ringmaster_all_port_lookup.find(combineRingPort(ring, port));

	if (it != comms_ringmaster_all_port_lookup.end())
	{
		return it->second;
	}
	return m_EMPTY_PORT;
}

const comms_ringmaster_port& riedelHelper::GetPort(const bncs_string& ringport)
{
	pair<int, int> rp = splitRingPort(ringport);
	return GetPort(rp.first,rp.second);
}

const comms_ringmaster_output& riedelHelper::GetOutputPort(int ring, int port)
{
	COMMS_RINGMASTER_OUTPUT_LOOKUP::const_iterator it = comms_ringmaster_output_lookup.find(combineRingPort(ring,port));

	if (it != comms_ringmaster_output_lookup.end())
	{
		return it->second;
	}
	return m_EMPTY_OUTPUT_PORT;
}

const ringmaster_devices& riedelHelper::GetRingmasterDevices()
{
	return m_ringmaster_devices;
}

const vector<ringmaster_devices>& riedelHelper::GetOtherRingmasterDevices()
{
	return m_other_ringmasters;
}

const comms_ringmaster_port& riedelHelper::GetPortFromRRCSAddress(int ring, bncs_string rrcsAddress)
{
	return GetPortFromRRCSAddress(bncs_string(ring) + "#" + rrcsAddress);
}

//Ring and RRCS Address format is  Ring#RRCS Address 10#1.2.0
const comms_ringmaster_port& riedelHelper::GetPortFromRRCSAddress(bncs_string ringAndRRCSAddress)
{
	COMMS_RINGMASTER_PORT_LOOKUP::const_iterator it = comms_ringmaster_rrcs_lookup.find(ringAndRRCSAddress);

	if (it != comms_ringmaster_rrcs_lookup.end())
	{
		return it->second;
	}
	return m_EMPTY_PORT;
}

const comms_ringmaster_output& riedelHelper::GetOutputPort(const bncs_string& ringport)
{
	pair<int,int> rp = splitRingPort(ringport);
	return GetOutputPort(rp.first, rp.second);
}


int riedelHelper::GetDevice(const bncs_string& instance)
{
	int dev = 0;
	int slot;
	int offset;
	char t;
	bncs_string altid;
	if (instanceLookup(instance, "", altid, &dev, &offset, &slot, &t))
		return dev;
	return 0;
}

Devices riedelHelper::GetDeviceComposite(const bncs_string& instance)
{
	Devices d;
	bncs_string compositeInstance;
	if (instanceLookupComposite(instance, "grd", compositeInstance))
	{
		d.Grd_Instance = compositeInstance;
		d.Grd = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "monitors", compositeInstance))
	{
		d.Monitor_Instance = compositeInstance;
		d.Monitor = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "pti", compositeInstance))
	{
		d.Pti_Instance = compositeInstance;
		d.Pti = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "alias", compositeInstance))
	{
		d.Alias_Instance = compositeInstance;
		d.Alias = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "gpio", compositeInstance))
	{
		d.GPIO_Instance = compositeInstance;
		d.Gpio = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "gain", compositeInstance))
	{
		d.Gain_Instance = compositeInstance;
		d.Gain = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "conference", compositeInstance))
	{
		d.Conference_Instance = compositeInstance;
		d.Conference = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "ifbs", compositeInstance))
	{
		d.Ifb_Instance = compositeInstance;
		d.Ifb = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "extras", compositeInstance))
	{
		d.Extra_Instance = compositeInstance;
		d.Extras = GetDevice(compositeInstance);
	}
	if (instanceLookupComposite(instance, "lock", compositeInstance))
	{
		d.Lock_Instance = compositeInstance;
		d.Lock = GetDevice(compositeInstance);
	}
	return d;
}

bncs_string riedelHelper::sanitize(const bncs_string& s)
{
	if (s == "!!!")
		return "";
	return s;
}

void riedelHelper::load( void )
{
	comms_rings_lookup = load_comms_rings();
	load_comms_ports(comms_rings_lookup);
	comms_ringmaster_output_lookup = load_comms_ringmaster_output(comms_ringmaster_all_port_lookup);
	comms_ringmaster_output_port_lookup = set_output_port_lookup(comms_ringmaster_output_lookup);

	//Get local settings
	auto workstation = ccConfig::workstation();
	auto ops_position = bncs_config(bncs_string("workstation_settings.%1.ops_position").arg(workstation)).attr("value");
	auto packager_instance = bncs_config(bncs_string("workstation_settings.%1.packager_instance").arg(workstation)).attr("value");
	auto ringmaster = bncs_config(bncs_string("packager_config.%1.ring_master_auto").arg(packager_instance)).attr("value");

	//Ringmaster config, used to find all other ringmaster instances
	auto rc = bncs_config("ring_master_config");
	while (rc.isChildValid())
	{
		auto child = rc.childAttr("id");
		//NOTE: 2 ASSUMPTIONSare being made here:
		//1) We don't want c_ringmaster <- This is a special case for Discovery so if used elsewhere you may want to remove this. As this is a legacy composite
		//2) The ringmaster tags are data type, this is what is currently define in the xml there are other pieces on config with different tag type, this is no other "clean" way to find the ringmaster instances.
		if (child != ringmaster && child != "c_ringmaster" && rc.childTag() == "data")
		{
			auto rm = load_ringmaster_devices(child);
			m_other_ringmasters.push_back(rm);
		}
		rc.nextChild();
	}

	m_ringmaster_devices = load_ringmaster_devices(ringmaster);

	m_local_port = comms_ringmaster_port();

	//Load panel linking
	bncs_config c("riedel_panel_linking");
	while (c.isChildValid())
	{
		bncs_config d = bncs_config(c);
		auto ops_position = c.childAttr("id");
		d.drillDown();
		vector<PanelInfo> panels;
		while (d.isChildValid())
		{
			auto address = d.childAttr("address");
			auto uid = d.childAttr("uid");
			auto type = d.childAttr("type");

			bncs_stringlist sl(address, '.');
			PanelInfo i;
			i.Ring = sl[0];
			i.Port = sl[1];

			comms_ringmaster_port* port = &comms_ringmaster_all_port_lookup[combineRingPort(i.Ring, i.Port)];
			if (!port->valid || !(port->portType == ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL || port->portType == PORT_TYPE_PANEL_DUAL_CHANNEL))
			{
				//Ignore ones which are not panels.
				d.nextChild();
			}

			i.Expansion = sl[2];
			i.Uid = uid;
			i.Type = type;
			i.CommsPort = port;

			port_to_ops_postion_lookup[RingPort(i.Ring, i.Port)] = ops_position;
			panels.push_back(i);
			d.nextChild();
		}
		if (!panels.empty())
		{
			ops_position_to_panel_info[ops_position] = panels;

		}

		c.nextChild();
	}

	if (ops_position.length() > 0)
	{
		m_ops_position = ops_position;
		auto ops_settings = bncs_config(bncs_string("ops_positions.%1").arg(ops_position));
		while (ops_settings.isChildValid())
		{
			auto id = ops_settings.childAttr("id");
			auto value = ops_settings.childAttr("value");
			if (id == "comms_panel_port")
			{
				bncs_stringlist s(value,'.');
				m_local_port = GetPort(s[0], s[1]);
			}
			else if (id == "tb_assign")
			{
				m_tb_assign_access = bncs_stringlist(value);
			}
			ops_settings.nextChild();
		}
	}
}

const COMMS_RINGMASTER_PORT_LOOKUP& riedelHelper::AllPorts()
{
	return 	comms_ringmaster_all_port_lookup;
}

const COMMS_RINGMASTER_PORT_LOOKUP& riedelHelper::SourcePorts()
{
	return 	comms_ringmaster_all_port_lookup;
}

const COMMS_RINGMASTER_PORT_LOOKUP& riedelHelper::OutputPorts()
{
	return comms_ringmaster_output_port_lookup;
}

const COMMS_RINGS_LOOPKUP& riedelHelper::GetRings()
{
	return comms_rings_lookup;
}

const comms_rings& riedelHelper::GetRing(int ring)
{
	COMMS_RINGS_LOOPKUP::const_iterator it = comms_rings_lookup.find(ring);

	if (it != comms_rings_lookup.end())
		return it->second;
	return comms_rings::Empty;
}

const comms_rings& riedelHelper::GetRing(bncs_string compositeInstance)
{
	for (COMMS_RINGS_LOOPKUP::const_iterator it = comms_rings_lookup.begin(); it != comms_rings_lookup.end(); ++it)
	{
		if (it->second.instance == compositeInstance)
		{
			return it->second;
		}
	}
	return comms_rings::Empty;
}


void riedelHelper::print(const bncs_string& s)
{
	OutputDebugString(s);
}

bncs_string riedelHelper::combineRingPort(int ring, int port)
{
	return bncs_string("%1.%2").arg(ring).arg(port);
}

pair<int, int> riedelHelper::splitRingPort(const bncs_string& ringport)
{
	bncs_string sring, sport;
	ringport.split('.', sring, sport);
	return pair<int, int>(sring.toInt(), sport.toInt());
}

const comms_ringmaster_port& riedelHelper::GetLocalPanel()
{
	return m_local_port;
}

const bncs_string& riedelHelper::Ops_Position()
{
	return m_ops_position;
}

const vector<PanelInfo> riedelHelper::GetLocalPorts(int ring, int port)
{
	auto ops = port_to_ops_postion_lookup.find(RingPort(ring, port));

	if (ops == port_to_ops_postion_lookup.end())
	{
		return EmptyPanelInfo;
	}

	auto it = ops_position_to_panel_info.find(ops->second);

	if (it != ops_position_to_panel_info.end())
	{
		return it->second;
	}
	return EmptyPanelInfo;
}