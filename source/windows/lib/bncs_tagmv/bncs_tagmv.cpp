#include "bncs_tagmv.h"

/*		Example Instance Map :
	<instance id="c_tag_driver" alt_id="test tag MV" type="c_tag_mcm9000" composite="yes">
		<group id="driver" instance="tag_driver" />
		<group id="inputs" instance="c_tag_driver_inputs" />
		<group id="outputs" instance="c_tag_driver_outputs" />
	</instance>
	<instance id="tag_driver" alt_id="test tag MV driver" type="tag_driver" location="" ref="device=xxx,offset=0" composite="no">
		<setting id="device_address" value="{device_ip}" />
		<setting id="device_port" value="{device_port}" />
		<setting id="driver_address" value="{driver_ip}" />
		<setting id="driver_port" value="{driver_port}" />
		<setting id="user" value="{username}" />
		<setting id="password" value="{password}" />
	</instance>
	<instance id="c_tag_driver_inputs" alt_id="test tag MV inputs" type="tag_mcm9000" composite="yes">
		<group id="input_01" instance="tag_driver_input_01" />
			...
		<group id="input_32" instance="tag_driver_input_32" />
	</instance>
	<instance id="c_tag_driver_outputs" alt_id="test tag MV outputs" type="tag_mcm9000" composite="yes">
		<group id="output_01" instance="tag_driver_output_01" />
			...
		<group id="output_04" instance="tag_driver_output_04" />
	</instance>
	<instance id="tag_driver_input_01" alt_id="test tag MV input_01" type="tag_input" location="" ref="device=xxx,offset=400" composite="no">
		<setting id="aux_interface_id" value="input_01" />
	</instance>
		...
	<instance id="tag_driver_input_32" alt_id="test tag MV input_32" type="tag_input" location="" ref="device=xxx,offset=710" composite="no">
	<setting id="aux_interface_id" value="input_32" />
	</instance>
	<instance id="tag_driver_output_01" alt_id="test tag MV output_01" type="tag_output" location="" ref="device=xxx,offset=10" composite="no">
		<setting id="id" value="6E4BA1CF-0CD0-4663-8D5D-74C5D87B036B" />
	</instance>
		...
	<instance id="tag_driver_output_04" alt_id="test tag MV output_04" type="tag_output" location="" ref="device=xxx,offset=40" composite="no">
		<setting id="id" value="538EF0C3-3225-48C5-AD57-77000B46FFCB" />
	</instance>
*/

bncs_tagmv::bncs_tagmv()
{
	initValues();
}

bncs_tagmv::bncs_tagmv(bncs_string cinstance, int head)
{
	initValues();
	m_cinstance = cinstance;
	m_head = head;
	m_caltid = bncs_config(bncs_string("instances.%1").arg(m_cinstance)).attr("alt_id");

	m_isValid = true;	// assmume we are good

	m_driver_instance = bncs_config(bncs_string("instances.%1.%2").arg(m_cinstance).arg(CONFIG_GROUP_DRIVER)).attr("instance");
	m_driver_address = bncs_config(bncs_string("instances.%1.%2").arg(m_driver_instance).arg(CONFIG_SETTING_DRIVER_ADDRESS)).attr("value");
	m_driver_port = bncs_config(bncs_string("instances.%1.%2").arg(m_driver_instance).arg(CONFIG_SETTING_DRIVER_PORT)).attr("value");
	ODS(bncs_string("bncs_tagmv::Constructor::Driver instance <%1>, Driver address <%2>, Driver port <%3>").arg(m_driver_instance).arg(m_driver_address).arg(m_driver_port));
	if (!m_driver_instance.length() || !m_driver_address.length() || !m_driver_port.length())
	{
		m_isValid = false;
	}

	m_output_cinstance = bncs_config(bncs_string("instances.%1.%2").arg(cinstance).arg(CONFIG_GROUP_OUTPUTS)).attr("instance");

	// now try to load the composite output groups
	bncs_config c_oi = bncs_config(bncs_string("instances.%1").arg(m_output_cinstance));
	if (c_oi.isValid())
	{
		int count = 0;
		while (c_oi.isChildValid())
		{
			if (c_oi.childTag() == "group")
			{
				count++;
				bncs_string group = c_oi.childAttr("id");

				//ODS(bncs_string("bncs_tagmv::Found output group %1").arg(group));

				if (group.startsWith(CONFIG_GROUP_PREFIX_OUTPUTS) && group.firstInt() == count)
				{
					tagOutputInstance gInstance;
					gInstance.group = group;
					gInstance.instance = c_oi.childAttr("instance");
					gInstance.type = bncs_config(bncs_string("instances.%1").arg(gInstance.instance)).attr("type");
					gInstance.altid = bncs_config(bncs_string("instances.%1").arg(gInstance.instance)).attr("alt_id");
					bncs_stringlist ref = bncs_config(bncs_string("instances.%1").arg(gInstance.instance)).attr("ref");
					gInstance.device = ref.getNamedParam("device");
					gInstance.offset = ref.getNamedParam("offset");
					gInstance.id = bncs_config(bncs_string("instances.%1.id").arg(gInstance.instance)).attr("value");
					bncs_config c_dt = bncs_config(bncs_string("devicetypes/%1").arg(gInstance.type));
					if (c_dt.isValid())
					{
						while (c_dt.isChildValid())
						{
							if (c_dt.childAttr("name") == CONFIG_TAG_OUTPUT_GEOMETRY_PARAM)
							{
								gInstance.geometry = c_dt.childAttr("slot").toInt();
							}
							else if (c_dt.childAttr("name") == CONFIG_TAG_OUTPUT_LAYOUT_ID_PARAM)
							{
								gInstance.layout_id = c_dt.childAttr("slot").toInt();
							}
							c_dt.nextChild();
						}
					}
					//ODS(bncs_string("bncs_tagmv::found %1 instance %2 id %3 type %4 geometry %5").arg(group).arg(gInstance.instance).arg(gInstance.id).arg(gInstance.type).arg(gInstance.geometry));

					if (gInstance.isValid())
					{
						m_output_instance_map[gInstance.index()] = gInstance;

						//ODS(bncs_string("bncs_tagmv::output device %1/%2 allocated to entry %3").arg(gInstance.device).arg(gInstance.offset).arg(gInstance.index()));
					}
					else
					{	// output instance invalid
						m_isValid = false;
					}
				}
				else
				{	// count out of sequence
					m_isValid = false;
				}
			}

			c_oi.nextChild();
		}


		// then check if our headcount or requested head is out of bounds
		if ((int)m_output_instance_map.size() < 1 || head < 1 || head >(int)m_output_instance_map.size())
		{
			m_isValid = false;
		}

		m_input_cinstance = bncs_config(bncs_string("instances.%1.%2").arg(cinstance).arg(CONFIG_GROUP_INPUTS)).attr("instance");

		//try to load the input groups
		bncs_config c_ii = bncs_config(bncs_string("instances.%1").arg(m_input_cinstance));
		if (c_ii.isValid())
		{
			int count = 0;
			while (c_ii.isChildValid())
			{
				if (c_ii.childTag() == "group")
				{
					count++;
					bncs_string group = c_ii.childAttr("id");

					//ODS(bncs_string("bncs_tagmv::Found input group %1").arg(group));

					if (group.startsWith(CONFIG_GROUP_PREFIX_INPUTS) && group.firstInt() == count)
					{
						tagInputInstance gInstance;
						gInstance.group = group;
						gInstance.instance = c_ii.childAttr("instance");
						bncs_stringlist ref = bncs_config(bncs_string("instances.%1").arg(gInstance.instance)).attr("ref");
						gInstance.device = ref.getNamedParam("device");
						gInstance.offset = ref.getNamedParam("offset");

						bncs_string cfgstr = bncs_string("%1.%2.%3").arg(CONFIG_TAG_INPUT_MAPPING_GROUP).arg(cinstance).arg(count);
						gInstance.dest_pkg = bncs_config(cfgstr).attr(CONFIG_TAG_INPUT_MAPPING_DEST_PKG);

						//ODS(bncs_string("bncs_tagmv::found %1 instance %2 / %3 from %4").arg(group).arg(gInstance.instance).arg(gInstance.dest_pkg).arg(cfgstr));

						if (gInstance.isValid())
						{
							m_input_instance_map[gInstance.index()] = gInstance;

							//ODS(bncs_string("bncs_tagmv::input device %1/%2 allocated to entry %3 and dest_pkg %4 from %5").arg(gInstance.device).arg(gInstance.offset).arg(gInstance.index()).arg(gInstance.dest_pkg).arg(cfgstr));
						}
						else
						{
							m_isValid = false;
						}
					}
					else
					{	// count out of sequence
						m_isValid = false;
					}
				}

				c_ii.nextChild();
			}
		}

		// check if we have inputs
		if ((int)m_input_instance_map.size() < 1)
		{
			m_isValid = false;
		}
	}
	else
	{
		m_isValid = false;
	}

	ODS(bncs_string("bncs_tagmv::Constructor::We are %1, found %2 output groups, %3 input groups, selected head %3").arg(m_isValid ? "valid" : "NOT valid").arg((int)m_output_instance_map.size()).arg((int)m_input_instance_map.size()).arg(m_head));

	// finally, if we have been tripped false then reset all values
	if (!m_isValid)
	{
		initValues();
	}
}

bncs_tagmv::~bncs_tagmv()
{
}

bool bncs_tagmv::isValid()
{
	return m_isValid;
}

// requestURI()
// ============
//
// formats the http request for obtaining the layout on an output
// this is in the format
// http://{driver_ip}:{driver_port}/outputs/{head_id}/layout
//
bncs_string bncs_tagmv::requestURI(void)
{
	bncs_string ret = "";

	if (m_isValid)
	{
		ret = bncs_string("http://%1:%2/outputs/%3/layout").arg(m_driver_address).arg(m_driver_port).arg(m_output_instance_map[m_head].id);
		ODS(bncs_string("bncs_tagmv::requestURI::%1").arg(ret));
	}

	return ret;
}

// layoutsURI()
// ============
//
// formats the http request for obtaining the layouts on a tag
// this is in the format
// http://{driver_ip}:{driver_port}/layouts
//
bncs_string bncs_tagmv::layoutsURI(void)
{
	bncs_string ret = "";

	if (m_isValid)
	{
		ret = bncs_string("http://%1:%2/layouts").arg(m_driver_address).arg(m_driver_port);
		ODS(bncs_string("bncs_tagmv::layoutsURI::%1").arg(ret));
	}

	return ret;
}

// layoutURI()
// ============
//
// formats the http request for obtaining a layout on a tag
// this is in the format
// http://{driver_ip}:{driver_port}/layout/{layout_id}
//
bncs_string bncs_tagmv::layoutURI(bncs_string layout_id)
{
	bncs_string ret = "";

	if (m_isValid)
	{
		ret = bncs_string("http://%1:%2/layout/%3").arg(m_driver_address).arg(m_driver_port).arg(layout_id);
		ODS(bncs_string("bncs_tagmv::layoutURI::%1").arg(ret));
	}

	return ret;
}

bncs_string bncs_tagmv::instance()
{
	bncs_string ret = "";

	if (m_isValid)
	{
		ret = m_cinstance;
		ODS(bncs_string("bncs_tagmv::composite instance::%1").arg(ret));
	}

	return ret;
}

bncs_string bncs_tagmv::altId()
{
	bncs_string ret = "";

	if (m_isValid)
	{
		ret = m_caltid;
		ODS(bncs_string("bncs_tagmv::composite altId::%1").arg(ret));
	}

	return ret;
}

bncs_string bncs_tagmv::headInstance()
{
	bncs_string ret = "";

	if (m_isValid)
	{
		ret = m_output_instance_map[m_head].instance;
		ODS(bncs_string("bncs_tagmv::head instance::%1").arg(ret));
	}

	return ret;
}

bncs_string bncs_tagmv::headId()
{
	bncs_string ret = "";

	if (m_isValid)
	{
		ret = m_output_instance_map[m_head].id;
		ODS(bncs_string("bncs_tagmv::headId::%1").arg(ret));
	}

	return ret;
}

bncs_string bncs_tagmv::headAltId()
{
	bncs_string ret = "";

	if (m_isValid)
	{
		ret = m_output_instance_map[m_head].altid;
		ODS(bncs_string("bncs_tagmv::head AltId::%1").arg(ret));
	}

	return ret;
}

int bncs_tagmv::head()
{
	int ret = -1;

	if (m_isValid)
	{
		ret = m_head;
		ODS(bncs_string("bncs_tagmv::head::%1").arg(ret));
	}

	return ret;
}

int bncs_tagmv::headDevice()
{
	int ret = 0;

	if (m_isValid)
	{
		ret = m_output_instance_map[m_head].device;
		ODS(bncs_string("bncs_tagmv::head device::%1").arg(ret));
	}

	return ret;
}

int bncs_tagmv::geometryIndex()
{
	int ret = 0;

	if (m_isValid)
	{
		ret = m_output_instance_map[m_head].geometry + m_output_instance_map[m_head].offset;
		ODS(bncs_string("bncs_tagmv::geometry index::%1").arg(ret));
	}

	return ret;
}

int bncs_tagmv::layoutIdIndex()
{
	int ret = 0;

	if (m_isValid)
	{
		ret = m_output_instance_map[m_head].layout_id + m_output_instance_map[m_head].offset;
		ODS(bncs_string("bncs_tagmv::layout id index::%1").arg(ret));
	}

	return ret;
}

int bncs_tagmv::inputDestPkg(int input)
{
	int ret = BNCS_TAGMV_ERROR;

	if (m_isValid && input > 0 && input <= (int)m_input_instance_map.size())
	{
		ret = m_input_instance_map[input].dest_pkg;
	}

	return ret;
}

void bncs_tagmv::initValues()
{
	m_output_cinstance = "";
	m_caltid = "";
	m_input_cinstance = "";
	m_output_instance_map.clear();
	m_input_instance_map.clear();
	m_isValid = false;
	m_head = BNCS_TAGMV_ERROR;
	m_driver_address = "";
	m_driver_port = "";
}
