#include "local_cache.h"
#include "bncs_config.h"

local_cache *local_cache::local_cacheObj;

local_cache::local_cache()
{
	load();
}

local_cache::~local_cache()
{
}

local_cache * local_cache::getLocalCache(void)
{
	return local_cacheObj;
}

void local_cache::load( void )
{
	char *zErrMsg = 0;
	bncs_string filename = bncs_string("%1\\bncs_local_cache.dat(sqlite)").arg(getenv("TEMP"));
	OutputDebugString((const char*)filename);
	int rc = sqlite3_open((const char*)filename, &m_db);
	sqlite3_exec(m_db, "PRAGMA synchronous = OFF", NULL, NULL, &zErrMsg);

	rc = sqlite3_exec(m_db, "CREATE TABLE IF NOT EXISTS KEYVALUE (key TEXT, value TEXT,PRIMARY KEY( key))", 0, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		char msg[256];
		sprintf(msg, "SQL error1: %s\n", zErrMsg);
		OutputDebugString(zErrMsg);
		sqlite3_free(zErrMsg);
	}
	else
	{
		char sql[256];

		sprintf(sql, "INSERT OR REPLACE INTO keyvalue (key, value) values ( @key, @value)");

		rc = sqlite3_prepare_v2(m_db, sql, strlen(sql) + 1, &m_setStmt, 0);

		if (rc != SQLITE_OK)
			OutputDebugString("SET Statement Prepare Failed");

		sprintf(sql, "SELECT key, value FROM keyvalue WHERE key=@key");


		rc = sqlite3_prepare_v2(m_db, sql, strlen(sql) + 1, &m_getStmt, 0);

		if (rc != SQLITE_OK)
			OutputDebugString("GET Statement Prepare Failed");


		sprintf(sql, "DELETE FROM keyvalue WHERE key=@key");

		rc = sqlite3_prepare_v2(m_db, sql, strlen(sql) + 1, &m_deleteStmt, 0);

		if (rc != SQLITE_OK)
			OutputDebugString("DELETE Statement Prepare Failed");
	}

}

bool local_cache::setValue(bncs_string key, bncs_string value)
{
	bncs_string k = key;
	bncs_string v = value;
	sqlite3_bind_text(m_setStmt, 1, (const char*)k, -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(m_setStmt, 2, (const char*)v, -1, SQLITE_TRANSIENT);

	int rc = sqlite3_step(m_setStmt);

	if (rc != SQLITE_DONE)
	{
		char msg[256];
		sprintf(msg, "SQL error2: %d\n", rc);
		OutputDebugString(msg);
	}
	sqlite3_clear_bindings(m_setStmt);
	sqlite3_reset(m_setStmt);
	return false;
}

bool local_cache::getValue(bncs_string key, bncs_string &value)
{
	sqlite3_bind_text(m_getStmt, 1, (const char*)key, -1, SQLITE_TRANSIENT);

	while (sqlite3_step(m_getStmt) == SQLITE_ROW)
	{
		const unsigned char* k = sqlite3_column_text(m_getStmt, 0);
		const unsigned char* v = sqlite3_column_text(m_getStmt, 1);

		value = (char*)v;
		//qDebug("inserting %d %d '%s' %d", s.device, s.index, s.value.latin1(), s.workstation);
	}

	sqlite3_clear_bindings(m_getStmt);
	sqlite3_reset(m_getStmt);
	return false;
}

bool local_cache::deleteValue(bncs_string key, bncs_string &value)
{
	getValue(key, value);

	sqlite3_bind_text(m_deleteStmt, 1, (const char*)key, -1, SQLITE_TRANSIENT);

	int rc = sqlite3_step(m_deleteStmt);

	if (rc != SQLITE_DONE)
	{
		char msg[256];
		sprintf(msg, "SQL error2: %d\n", rc);
		OutputDebugString(msg);
	}

	sqlite3_clear_bindings(m_deleteStmt);
	sqlite3_reset(m_deleteStmt);

	return false;
}