# ![alt text](header.jpg "Header")

# local_cache
## Description
This dll can be used to store data locally.
This should only be used to store transient/volatile data that does not matter if it is lost.

Sometimes it is useful to store data locally, that persists longer than a panel might. For instance the last group page and page selected on a router grid. This data does not needed to be stored for long but can be used to load the panel in the same position it was left if the dll was reloaded.

## How to use

### Create a pointer to the local cache.
```cpp
local_cache *lc;
```

### Get the pointer to the localcache
```cpp
lc = local_cache::getLocalCache();
```

### Get, Set and Delete parameters
```cpp
lc->setValue("MyKey", "MyValue");

bncs_string getVal;
lc->getValue("MyKey", getVal);

bncs_string deleteVal;
lc->deleteValue("MyKey", deleteVal);
```

## Commands
Each of the commands returns a boolean to say whether it has been successful or not. True on success.
```cpp
//Store a value
bool setValue(bncs_string key, bncs_string value);

//Get a value
bool getValue(bncs_string key, bncs_string &value);

//Remove a value
bool deleteValue(bncs_string key, bncs_string &value);
```

## Notifications
No notifications are used.


## Developer Nodes
The data is stored in %temp% in an sqlite database
