#include <windows.h>
#include "local_cache.h"

BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved )
{
    switch (ul_reason_for_call)
	{ 
		// on process attach if there are no styles loaded then go off and load them!
		case DLL_PROCESS_ATTACH:
		{
			OutputDebugString( "DLL_PROCESS_ATTACH\n" );
			if( !local_cache::local_cacheObj )
			{
				OutputDebugString( "local_cache: DLL_PROCESS_ATTACH - creating object\n" );
				local_cache::local_cacheObj =  new local_cache;
			}
			break;
		}

		case DLL_PROCESS_DETACH:
		{
			OutputDebugString( "DLL_PROCESS_DETACH\n" );
			if( local_cache::local_cacheObj )
			{
				OutputDebugString( "local_cache: DLL_PROCESS_DETACH - deleting object\n" );
				delete local_cache::local_cacheObj;
				local_cache::local_cacheObj=0;
			}
			break;
		}
    }
    return TRUE;
}


