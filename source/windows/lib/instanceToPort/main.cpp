#pragma warning( disable: 4786 )
#include <windows.h>
#include "instanceToPort.h"

BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved )
{
    switch (ul_reason_for_call)
	{ 
		// on process attach if there are no styles loaded then go off and load them!
		case DLL_PROCESS_ATTACH:
//			OutputDebugString( "DLL_PROCESS_ATTACH\n" );
			if( !instanceToPort::m_instanceToPortObj )
			{
				instanceToPort::m_instanceToPortObj =  new instanceToPort;
			}	
			break;

		case DLL_PROCESS_DETACH:
//			OutputDebugString( "DLL_PROCESS_DETACH\n" );
			if( instanceToPort::m_instanceToPortObj )
			{
				delete instanceToPort::m_instanceToPortObj;
				instanceToPort::m_instanceToPortObj=0;
			}
			break;

		case DLL_THREAD_ATTACH:
//			OutputDebugString( "DLL_THREAD_ATTACH\n" );
			break;

		case DLL_THREAD_DETACH:
//			OutputDebugString( "DLL_THREAD_DETACH\n" );
			break;
    }
    return TRUE;
}
