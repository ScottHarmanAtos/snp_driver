#pragma warning( disable: 4786 )

#include "instanceToPort.h"
#include "bncs_config.h"
#include "bncs_shim_loader.h"
#include "bncs_shim.h"
#include "bncs_string.h"
#include "ccConfig.h"

#define DB7_DEST_ADJUST 7

instanceToPort *instanceToPort::m_instanceToPortObj;

instanceToPort::instanceToPort()
{
	m_shim = bncs_shim_loader::csiShim();

	load();
}

instanceToPort::~instanceToPort()
{
}

instanceToPort * instanceToPort::getinstanceToPortObject( void )
{
	return m_instanceToPortObj;
}

void instanceToPort::load( void )
{
	b_validObjectSetting = false;
	string ret = "";
	bncs_string retValue;

	int i_rtr_number=1;
	//Access object settings and get the router name list
	bncs_config c_rtrName("object_settings.instanceToPort");
	while(c_rtrName.isChildValid())
	{
		if(c_rtrName.childAttr("id") == bncs_string("rtr"))
		{
			b_validObjectSetting = true;
			OutputDebugString(bncs_string("instanceToPort::load Rtr Found getting instance ID:%1").arg(c_rtrName.childAttr("value")));
			//Access instances and get the device ID using the router name
			bncs_config c_instanceID("instances." + c_rtrName.childAttr("value"));
			if(c_instanceID.isValid()) 
			{
				int adjustDatabase = DB7_DEST_ADJUST;

				//See if we have been told the adjust are in a different database
				bncs_string sDatabase = c_rtrName.childAttr("database");
				if (sDatabase.length() > 0)
				{
					adjustDatabase = sDatabase.toInt();
				}

				int i_device = getDeviceID(c_instanceID);
				int i_databaseSize;	
				//Get the database size from the dev router
				//OutputDebugString(bncs_string("device ID is %1").arg(s_device));
				//Get router size
				i_databaseSize = ccConfig::getInt(i_device,"Database","DatabaseSize_1",0);

				//OutputDebugString(bncs_string("database size is %1").arg(i_databaseSize));
					
				//Loop Though Database 7 to get device IDs
				bncs_string s_instance, s_name;
					for(int i = 1; i < i_databaseSize; i++)
					{
						bool r = m_shim->databaseName( i_device, i, adjustDatabase, ret );
						
						if(ret != "")
						{
							//char msg[1024];
							//sprintf( msg, "instanceToPort::load adding %s %d %d\n", ret.c_str(), i, r );
							//OutputDebugString( msg);
							retValue = ret.c_str();
							if(retValue.contains('#'))
							{
								retValue.split('#',s_instance,s_name);
								//OutputDebugString(bncs_string("instanceToPort::retValue Instance=%1 Name=%2").arg(s_instance).arg(s_name));
							}
							else
							{
								s_instance = retValue;
								s_name = "";
							}

							m_values.insert( pair< bncs_string, portEntry >( s_instance, portEntry( s_instance, s_name ,i_device, i )));
							//OutputDebugString(bncs_string("instanceToPort::Load Dev:%1 Slot:%2 Instance:%3").arg(i_device).arg(i).arg(s_instance));
							//Get group instances within composites
							if(s_instance.left(2)=="c_")
							{
								bncs_config c_compositeInstance("instances." + s_instance);
								while (c_compositeInstance.isChildValid())
								{	
									m_values_in_composite.insert( make_pair< bncs_string, portEntry >( c_compositeInstance.childAttr("instance"), portEntry( c_compositeInstance.childAttr("instance"), s_name ,i_device, i )));
									//OutputDebugString(bncs_string("instanceToPort:: create composite instance enteries instance[%1] slot[%2]").arg(c_compositeInstance.childAttr("instance")).arg( i));
									c_compositeInstance.nextChild();
								}
							}
							
							ret = "";
						}
					}
			}
		
		}
		c_rtrName.nextChild();
	}
	//bncs_string s =static m_values.size();
	OutputDebugString(bncs_string("instanceToPort::load Number of stored instances is %1").arg(static_cast<int>(m_values.size())));
	OutputDebugString(bncs_string("instanceToPort::loadNumber of stored instances within composites is %1").arg(static_cast<int>(m_values_in_composite.size())));
}

bool instanceToPort::getValue( const bncs_string & instance, const bncs_string & name, portEntry & ret)
{
	//OutputDebugString( bncs_string("instanceToPort::getValue Overload values: instance:%1, name:%2, ret, ").arg(instance).arg(name) );
	return getMapValue(m_values,instance, name, ret);
}

bool instanceToPort::getValue( const bncs_string & instance, const bncs_string & name, portEntry & ret , const bncs_string & routerInstance )
{
	//OutputDebugString( bncs_string("instanceToPort::getValue Overload values: instance:%1, name:%2, ret, routerInstance:%3").arg(instance).arg(name).arg(routerInstance));
	return getMapValue_with_device_id(m_values,instance, name, ret, routerInstance);
}

void instanceToPort::setValue( int device, int index, int database, const bncs_string & name )
{
	char msg[1024];
	sprintf( msg, ("instanceToPort::setValue: %d %d %d " + name + "\n"), device, index, database );
	OutputDebugString( msg );
}

bool instanceToPort::getStatus(bncs_string & s_errMsg)
{
	if(!b_validObjectSetting)
	{
		s_errMsg = "No object setting is set|or object setting is broken";
		return true;
	}
	else if(m_values.size() < 1)
	{
		s_errMsg = "No Adjust have been found in DB" + DB7_DEST_ADJUST;
		return true;
	}
	return false;
	
}


int instanceToPort::getDeviceID(bncs_config & c_instanceID)
{
	//Get the device ID from the Ref string
	bncs_string s_deviceAndRef(c_instanceID.attr("ref"));
	if(s_deviceAndRef != "")
	{
		//OutputDebugString(bncs_string("instance value is %1").arg(s_deviceAndRef));
		bncs_string s_temp1, s_temp2, s_device;
			s_deviceAndRef.split(',', s_temp1, s_temp2);
			s_temp1.split('=',s_temp2, s_device);
			if(s_device.toInt() > 0)
				return s_device.toInt();
	}
	return 0;
}

bool instanceToPort::getValue(const bncs_string &instance, const bncs_string & name, portEntry & ret, const bncs_string & routerInstance,  const bool & checkInstancesWithinComposite)
{
	//OutputDebugString( bncs_string("instanceToPort::getValue Overload values: instance:%1, name:%2, ret, routerInstance:%3 checkInstancesWithinComposite:%4").arg(instance).arg(name).arg(routerInstance).arg(checkInstancesWithinComposite));
	
	if(getMapValue_with_device_id(m_values,instance, name, ret , routerInstance) && checkInstancesWithinComposite)
	{
		return getMapValue_with_device_id(m_values_in_composite, instance, name, ret, routerInstance);
	}
	else
	{
		return false;
	}
	return true;
	
}

bool instanceToPort::getValue(const bncs_string & instance, const bncs_string & name, portEntry & ret, const bool & checkInstancesWithinComposite)
{
	//OutputDebugString( bncs_string("instanceToPort::getValue Overload values: instance:%1, name:%2, ret, checkInstancesWithinComposite:%3").arg(instance).arg(name).arg(checkInstancesWithinComposite));	
	if(getMapValue(m_values,instance, name, ret) && checkInstancesWithinComposite)
	{
		return getMapValue(m_values_in_composite, instance, name, ret);
	}
	else
	{
		return false;
	}
return true;
}



bool instanceToPort::getMapValue(const multimap< bncs_string, portEntry > & m_map, const bncs_string & instance, const bncs_string & name, portEntry & ret)
{
	//OutputDebugString( bncs_string("instanceToPort::getMapValue instance:%1, name:%2").arg(instance).arg(name) );
	//OutputDebugString(bncs_string("instanceToPort::getValue instance:%1 name:%2").arg(instance).arg(name));
	multimap< bncs_string, portEntry >::const_iterator it = m_map.find( instance );
	if( it != m_map.end() )
	{
		do
		{
			if( it->second.m_instance != instance )
				break;
			//OutputDebugString(bncs_string("instanceToPort::getValue PORT instance:%1 name:%2 device:%3 index:%4").arg(it->second.m_instance).arg(it->second.m_name).arg(it->second.m_device).arg(it->second.m_index));
			if( it->second.m_name == name )
			{
				ret = it->second;
				return false;
			}
		} while( ++it != m_map.end() );
	}
	return true;
}

bool instanceToPort::getMapValue_with_device_id(const multimap< bncs_string, portEntry > & m_map, const bncs_string & instance, const bncs_string & name, portEntry & ret , const bncs_string & routerInstance )
{
	//OutputDebugString( bncs_string("instanceToPort::getMapValue_with_device_id instance:%1, name:%2 routerInstance:%3").arg(instance).arg(name).arg(routerInstance) );
	bncs_config c_instanceID("instances." + routerInstance);
	int i_device = getDeviceID(c_instanceID);
	//OutputDebugString(bncs_string("instanceToPort::getValue instance:%1 name:%2 routerInstance:%3").arg(instance).arg(name).arg(routerInstance));
	multimap< bncs_string, portEntry >::const_iterator it = m_map.find( instance );
	if( it != m_map.end() )
	{
		do
		{
			//OutputDebugString(bncs_string("instanceToPort::getValue PORT instance:%1 name:%2 device:%3 index:%4").arg(it->second.m_instance).arg(it->second.m_name).arg(it->second.m_device).arg(it->second.m_index));
			if( it->second.m_instance != instance )
				break;			
			if( it->second.m_name == name )
			{
				if(i_device != 0)
				{
					if(it->second.m_device == i_device)
					{
						ret = it->second;
						return false;
					}
				}
				else
				{
					ret = it->second;
					return false;
				}
			}
			
		} while( ++it != m_map.end() );
	}
	return true;
}