#include "bncs_packager.h"

bncs_packager::bncs_packager()
{
	initValues();
	m_config_packager_auto = "";
}

bncs_packager::bncs_packager(bncs_string pInstance, bncs_string pGroup)
{	// bncs_packager
	initValues();
	m_isValid = true;	// lets assume success and if *anything* fails set to false

	// resolve packager_config.xml entry section, assume default to start with
	m_config_packager_auto = CONFIG_PACKAGER_AUTO;
	// check for a config section entry held as the incomming composite instance name
	if (bncs_config(bncs_string("%1.%2").arg(CONFIG_PACKAGER_CONFIG).arg(pInstance)).isValid())
	{ // ...and use that if it is valid
		m_config_packager_auto = pInstance;
	}

	m_numberOfPackages = getConfigItem(CONFIG_PACKAGER_CONFIG, m_config_packager_auto, CONFIG_NUMBER_OF_PACKAGES);
	if (m_numberOfPackages < 1)
	{
		m_isValid = false;
	}

	m_packagerDivisor = getConfigItem(CONFIG_PACKAGER_CONFIG, m_config_packager_auto, CONFIG_PACKAGER_DIVISOR);
	if (m_packagerDivisor < 1)
	{
		m_isValid = false;
	}

	m_firstVirtualPackage = getConfigItem(CONFIG_PACKAGER_CONFIG, m_config_packager_auto, CONFIG_PACKAGER_FIRST_VIRTUAL);
	if (m_firstVirtualPackage < 1)
	{
		m_isValid = false;
	}

	if (m_isValid)
	{
		// try to find the composite instance group
		m_group_instance = bncs_config(bncs_string("instances.%1.%2").arg(pInstance).arg(pGroup)).attr("instance");

		if (!m_group_instance.length())
		{	// if not valid then use the instance given directly
			m_group_instance = pInstance;
		}

		// now try to load the composite groups
		m_deviceCount = 0;
		bncs_config c_pr = bncs_config(bncs_string("instances.%1").arg(m_group_instance));
		if (c_pr.isValid())
		{
			while (c_pr.isChildValid())
			{
				if (c_pr.childTag() == "group")
				{
					m_deviceCount++;
					bncs_string group = c_pr.childAttr("id");
					if (group.find(pGroup) && group.firstInt() == m_deviceCount)
					{
						pgInstance gInstance;
						gInstance.group = group;
						gInstance.instance = c_pr.childAttr("instance");
						bncs_stringlist ref = bncs_config(bncs_string("instances.%1").arg(gInstance.instance)).attr("ref");
						gInstance.device = ref.getNamedParam("device");
						if (gInstance.isValid())
						{
							m_packager_instance_map[gInstance.index()] = gInstance;
						}
						else
						{
							m_isValid = false;
						}
					}
				}

				c_pr.nextChild();
			}
		}
		else
		{
			m_isValid = false;
		}

		if ((m_deviceCount * m_packagerDivisor) < m_numberOfPackages)
		{	// sanity check to make sure number of packages doesn't overrun
			m_isValid = false;
		}
	}

	// finally, if we have been tripped false then reset all values
	if (!m_isValid)
	{
		initValues();
	}
}

bncs_packager::~bncs_packager()
{
}

bool bncs_packager::isValid()
{
	return m_isValid;
}

bncs_string bncs_packager::destInstance(int dest)
{
	if (!m_isValid || dest < 1 || dest > m_numberOfPackages)
	{
		return BNCS_PACKAGER_ERROR_STRING;
	}
	else
	{
		return m_packager_instance_map[((dest - 1) / m_packagerDivisor) + 1].instance;
	}
}

int bncs_packager::destDev(int dest)
{
	if (!m_isValid || dest < 1 || dest > m_numberOfPackages)
	{
		return BNCS_PACKAGER_ERROR;
	}
	else
	{
		return m_packager_instance_map[((dest - 1) / m_packagerDivisor) + 1].device;
	}
}

int bncs_packager::destSlot(int dest)
{
	if (!m_isValid || dest < 1 || dest > m_numberOfPackages)
	{
		return BNCS_PACKAGER_ERROR;
	}
	else
	{
		return ((dest - 1) % m_packagerDivisor) + 1;
	}
}

int bncs_packager::deviceCount()
{
	return m_deviceCount;
}

int bncs_packager::numberOfPackages()
{
	return m_numberOfPackages;
}

int bncs_packager::packagerDivisor()
{
	return m_packagerDivisor;
}

int bncs_packager::firstVirtual()
{
	return m_firstVirtualPackage;
}

bncs_string bncs_packager::instance(void)
{
	if (!m_isValid)
	{
		return BNCS_PACKAGER_ERROR_STRING;
	}
	else
	{
		return m_group_instance;
	}
}

bncs_string bncs_packager::instance(int router)
{
	if (!m_isValid || router < 1 || router > m_deviceCount)
	{
		return BNCS_PACKAGER_ERROR_STRING;
	}
	else
	{
		return m_packager_instance_map[router].instance;
	}
}

int bncs_packager::device(int router)
{
	if (!m_isValid || router < 1 || router > m_deviceCount)
	{
		return BNCS_PACKAGER_ERROR;
	}
	else
	{
		return m_packager_instance_map[router].device;
	}
}

int bncs_packager::device(void)
{
	return device(BNCS_PACKAGER_BASE_DEVICE);
}

int bncs_packager::deviceNames(int database)
{
	return device(BNCS_PACKAGER_NAMES_DEVICE);
}

int bncs_packager::toDest(int dev, int slot)
{
	int ret = BNCS_PACKAGER_ERROR;

	if (slot > 0 && slot <= m_packagerDivisor)
	{
		for (map<int, pgInstance>::iterator mit = m_packager_instance_map.begin(); mit != m_packager_instance_map.end(); mit++)
		{
			if (mit->second.device == dev)
			{
				ret = (mit->first - 1) * m_packagerDivisor + slot;
			}
		}
	}
	return ret;
}

bool bncs_packager::isDevice(int dev)
{
	bool ret = false;

	if (m_isValid)
	{
		for (map<int, pgInstance>::iterator mit = m_packager_instance_map.begin(); mit != m_packager_instance_map.end(); mit++)
		{
			if (mit->second.device == dev)
			{
				ret = true;
			}
		}
	}
	return ret;
}

bncs_string bncs_packager::config()
{
	return m_config_packager_auto;
}

bncs_string bncs_packager::getConfigItem(bncs_string pItem)
{
	return getConfigItem(CONFIG_PACKAGER_CONFIG, m_config_packager_auto, pItem);
}


/**************************************/
/*      private methods               */
/**************************************/

void bncs_packager::initValues()
{
	m_group_instance = "";
	m_numberOfPackages = BNCS_PACKAGER_ERROR;
	m_packagerDivisor = BNCS_PACKAGER_ERROR;
	m_firstVirtualPackage = BNCS_PACKAGER_ERROR;
	m_deviceCount = BNCS_PACKAGER_ERROR;
	m_packager_instance_map.clear();
	m_isValid = false;
}

bncs_string bncs_packager::getConfigItem(bncs_string pConfig, bncs_string pAuto, bncs_string pItem)
{
	return bncs_config(bncs_string("%1.%2.%3").arg(pConfig).arg(pAuto).arg(pItem)).attr("value");
}

