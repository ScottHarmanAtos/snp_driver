echo off

REM =================================================

REM Copy a specific  dds OSPL.xml to the working folder

IF EXIST "D:\BNCS\%CC_SYSTEM%\CONFIG\WS\%CC_WORKSTATION%\ospl.xml" (
echo "Copying ws specific ospl.xml (DDS Tech Hub multicast specific address)"
xcopy "D:\BNCS\%CC_SYSTEM%\CONFIG\WS\%CC_WORKSTATION%\ospl.xml" "D:\BNCS\%CC_SYSTEM%\CONFIG\dds" /c/i/f/y
) 

REM =================================================

echo "Copying xDS section  of local settings ini"
xcopy "%CC_ROOT%\%CC_SYSTEM%\config\system\local_settings.ini" "%CC_ROOT%%\" /c/i/f/y

REM =================================================

REM WRITE A BACKGROUND BITMAP
desktopbmp.exe

REM =================================================

