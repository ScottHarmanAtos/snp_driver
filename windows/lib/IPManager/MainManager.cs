using IPManager;
using BNCS;
using System.Collections.Generic;
using System;
using System.Linq;

namespace MainManager
{
	public class MainManager : IScriptedBase
	{
        private string scriptVersionTag = "1.0.0.18";

		private uint maxItems = 4096;
		
		private BncsContext bncs;
		
		private IScriptedBase parent;	
		
		private SetCollection<ViewParameter> viewModel;
		
		private ViewParameter vmExternalsList;
		private ViewParameter vmBaseInstance;
        private ViewParameter vmTxRxState;
        private ViewParameter vmCsiState;

        private bool initialise_once = false;
		
		private Dictionary<string, IInterComponentMessaging> registeredTopics = new Dictionary<string, IInterComponentMessaging>();
		
		private Dictionary<uint, ManagedReceiver> managedReceivers = new Dictionary<uint, ManagedReceiver>();
		
		private Dictionary<uint, ManagedSender> managedSenders = new Dictionary<uint, ManagedSender>();

        private Dictionary<SenderProfileIGMPv3, IScriptedBase> senderProfilesIGMPv3 = new Dictionary<SenderProfileIGMPv3, IScriptedBase>();

        private Dictionary<SenderProfileIGMPv2, IScriptedBase> senderProfilesIGMPv2 = new Dictionary<SenderProfileIGMPv2, IScriptedBase>();
		
		private BNCSDeviceInstance baseInstance;

        private ViewCategory nameIDTypeInstance;
		
		public void SetContext(BncsContext bncs, SetCollection<ViewParameter> viewModel)
		{
			this.bncs = bncs;
			this.viewModel = viewModel;
		}
		
		public SetCollection<ViewParameter> GetViewModel()
		{
			return viewModel;
		}
		
		public void Main(IScriptedBase parent, Dictionary<string, string> parameters)
		{
			this.parent = parent;

            nameIDTypeInstance = bncs.CreateViewCategory("Main", "Main", "Main", "Main", "Main");
			
			//Prepare view model		            
			vmExternalsList = bncs.CreateViewParameter("External", "1, 2, 3");            

			vmBaseInstance = bncs.CreateViewParameter("Base instance", "");            

            vmTxRxState = bncs.CreateViewParameter("ExternalState", bncs.GetTxRxState());

            vmCsiState = bncs.CreateViewParameter("CsiState", bncs.GetCsiState());
			
			viewModel.Add(vmExternalsList);
			viewModel.Add(vmBaseInstance);
            viewModel.Add(vmTxRxState);
            viewModel.Add(vmCsiState);

            if(bncs.GetTxRxState() == "Connected")
			{
				bncs.SetExternalTopStrap("Connected", "Green");
			}
			else if(bncs.GetTxRxState() == "Disconnected")
			{
				bncs.SetExternalTopStrap("Disconnected", "Red");
			}
			else if(bncs.GetTxRxState() == "RxOnly")
			{
				bncs.SetExternalTopStrap("RxOnly", "Orange");
			}
			else if(bncs.GetTxRxState() == "TxRx")
			{
				bncs.SetExternalTopStrap("TxRx", "Green");
			}

            if(bncs.GetCsiState() == "Connected")
			{
				bncs.SetCsiTopStrap("Connected", "Green");
			}
			else if(bncs.GetCsiState() == "Disconnected")
			{
				bncs.SetCsiTopStrap("Disconnected", "Red");
			}
			else if(bncs.GetCsiState() == "Connecting")
			{
				bncs.SetCsiTopStrap("Connecting", "Yellow");
			}			

            string strExternals;
			if (parameters.TryGetValue("externals", out strExternals))
			{
				vmExternalsList.Value = strExternals;
			}

			//Get parameters
			string strBaseInstance;
			if (parameters.TryGetValue("instance", out strBaseInstance))
			{
				baseInstance = bncs.ResolveInstance(strBaseInstance);
				
				vmBaseInstance.Value = strBaseInstance;
				
				bncs.Debug(string.Format("Base instance: {0}", strBaseInstance), this);

                if(initialise_once == false && (bncs.GetTxRxState() == "Connected" || bncs.GetTxRxState() == "TxRx" || bncs.GetTxRxState() == "RxOnly") && 
                    bncs.GetCsiState() == "Connected")
                    {
                        initialise_once = true;
                        Initialise();
                    }
			}		
			else
			{	
				vmBaseInstance.Value = "Base instance not resolved";
				
				bncs.Debug("Base instance not resolved", this);
			}            
		}

        public ViewCategory GetNameIDTypeInstance()
		{
            return nameIDTypeInstance;
		}

        public string GetMappedIndex()
        {
            return "0";
        }
		
		public void Initialise()
		{			
            Dictionary<uint, DatabasesCache> databasesCacheDict = new Dictionary<uint, DatabasesCache>();

			//Register for DB changes and read initial values
			List<DevSlotDB> db0RegistrationList = new List<DevSlotDB>();
			List<DevSlotDB> db1RegistrationList = new List<DevSlotDB>();
			List<DevSlotDB> db4RegistrationList = new List<DevSlotDB>();
			List<DevSlotDB> db6RegistrationList = new List<DevSlotDB>();
			List<DevSlotDB> db7RegistrationList = new List<DevSlotDB>();

			for(uint index = 1; index <= maxItems; index++)
			{
				DevSlotDB db0Interest = new DevSlotDB((uint)baseInstance.Reference.DeviceNumber, index, 0, ResourceType.DB);
				DevSlotDB db1Interest = new DevSlotDB((uint)baseInstance.Reference.DeviceNumber, index, 1, ResourceType.DB);
				DevSlotDB db4Interest = new DevSlotDB((uint)baseInstance.Reference.DeviceNumber, index, 4, ResourceType.DB);
				DevSlotDB db6Interest = new DevSlotDB((uint)baseInstance.Reference.DeviceNumber, index, 6, ResourceType.DB);
				DevSlotDB db7Interest = new DevSlotDB((uint)baseInstance.Reference.DeviceNumber, index, 7, ResourceType.DB);
				
				db0RegistrationList.Add(db0Interest);                
				db0RegistrationList.Add(db1Interest);                
				db4RegistrationList.Add(db4Interest);                
				db6RegistrationList.Add(db6Interest);                
				db6RegistrationList.Add(db7Interest);                
				
				string valueDB0 = bncs.ReadDBValue(db0Interest);
				string valueDB1 = bncs.ReadDBValue(db1Interest);
				string valueDB4 = bncs.ReadDBValue(db4Interest);
				string valueDB6 = bncs.ReadDBValue(db6Interest);
				string valueDB7 = bncs.ReadDBValue(db7Interest);

                databasesCacheDict.Add(index, new DatabasesCache(valueDB0, valueDB1, valueDB4, valueDB6, valueDB7));
			}

            foreach(var kvpCacheS in databasesCacheDict)
            {
				Dictionary<string, string> parametersSender = new Dictionary<string, string>(){
					{ "name", kvpCacheS.Value.valueDB0},
					{ "profile", kvpCacheS.Value.valueDB4},
					{ "instance", kvpCacheS.Value.valueDB6},
                    { "index", kvpCacheS.Key.ToString()}
				};
				
				if((string.IsNullOrEmpty(kvpCacheS.Value.valueDB0) == false) && (string.IsNullOrEmpty(kvpCacheS.Value.valueDB4) == false) && (string.IsNullOrEmpty(kvpCacheS.Value.valueDB6) == false) &&
                    kvpCacheS.Value.valueDB0 != "!!!" && kvpCacheS.Value.valueDB4 != "!!!" && kvpCacheS.Value.valueDB6 != "!!!")
				{
					IScriptedBase script = bncs.CreateScriptedComponent("SenderManager", parametersSender, this);
				
					ManagedSender sender;
					if (managedSenders.TryGetValue(kvpCacheS.Key, out sender))
					{
						sender.Name = kvpCacheS.Value.valueDB0;
						sender.InstanceStr = kvpCacheS.Value.valueDB6;
						sender.Instance = bncs.ResolveInstance(kvpCacheS.Value.valueDB6);
						sender.Profile = kvpCacheS.Value.valueDB4;
						sender.Script = script;
					}
					else
					{
						sender = new ManagedSender(kvpCacheS.Value.valueDB0, kvpCacheS.Value.valueDB6, bncs.ResolveInstance(kvpCacheS.Value.valueDB6), kvpCacheS.Value.valueDB4, script);
						managedSenders.Add(kvpCacheS.Key, sender);
					}
				}
            }

            foreach(var kvpCacheR in databasesCacheDict)
            {
                Dictionary<string, string> parametersReceiver = new Dictionary<string, string>(){
					{ "name", kvpCacheR.Value.valueDB1},
					{ "instance", kvpCacheR.Value.valueDB7},
                    { "index", kvpCacheR.Key.ToString()}
				};
				
				if((string.IsNullOrEmpty(kvpCacheR.Value.valueDB1) == false) && (string.IsNullOrEmpty(kvpCacheR.Value.valueDB7) == false) && 
                    kvpCacheR.Value.valueDB1 != "!!!" && kvpCacheR.Value.valueDB7 != "!!!")
				{
					IScriptedBase script = bncs.CreateScriptedComponent("ReceiverManager", parametersReceiver, this);
				
					ManagedReceiver receiver;
					if (managedReceivers.TryGetValue(kvpCacheR.Key, out receiver))
					{
						receiver.Name = kvpCacheR.Value.valueDB1;
						receiver.InstanceStr = kvpCacheR.Value.valueDB7;
						receiver.Instance = bncs.ResolveInstance(kvpCacheR.Value.valueDB7);
						receiver.Script = script;
					}
					else
					{
						receiver = new ManagedReceiver(kvpCacheR.Value.valueDB1, kvpCacheR.Value.valueDB7, bncs.ResolveInstance(kvpCacheR.Value.valueDB7), script);
						managedReceivers.Add(kvpCacheR.Key, receiver);
					}
				}
            }
			
			bncs.RegisterDB(db0RegistrationList, this);
			bncs.RegisterDB(db1RegistrationList, this);
			bncs.RegisterDB(db4RegistrationList, this);
			bncs.RegisterDB(db6RegistrationList, this);
			bncs.RegisterDB(db7RegistrationList, this);
		}

		public void RevertiveFromExternal(string message)
		{

		}

        public void TopicSettingsFromComponent(string topic, Dictionary<string, string> settings, IInterComponentMessaging originator)
        {
            if (topic == "Add sender profile") 
            {
                SenderProfileIGMPv3 spIGMPv3 = new SenderProfileIGMPv3();
                SenderProfileIGMPv2 spIGMPv2 = new SenderProfileIGMPv2();

                string grpAddr1 = string.Empty;
                if (settings.TryGetValue("group_address_1", out grpAddr1))
                {
                    spIGMPv3.GrpAddr1 = grpAddr1;
                    spIGMPv2.GrpAddr1 = grpAddr1;
                }

                string grpAddr2 = string.Empty;
                if (settings.TryGetValue("group_address_2", out grpAddr2))
                {
                    spIGMPv3.GrpAddr2 = grpAddr2;
                    spIGMPv2.GrpAddr2 = grpAddr2;
                }

                string srcAddr1 = string.Empty;
                if (settings.TryGetValue("source_address_1", out srcAddr1))
                {
                    spIGMPv3.SrcAddr1 = srcAddr1;
                }

                string srcAddr2 = string.Empty;
                if (settings.TryGetValue("source_address_2", out srcAddr2))
                {
                    spIGMPv3.SrcAddr2 = srcAddr2;
                }

                string udpPort1 = string.Empty;
                if (settings.TryGetValue("udp_port_1", out udpPort1))
                {
                    spIGMPv3.UdpPort1 = udpPort1;
                    spIGMPv2.UdpPort1 = udpPort1;
                }

                string udpPort2 = string.Empty;
                if (settings.TryGetValue("udp_port_2", out udpPort2))
                {
                    spIGMPv3.UdpPort2 = udpPort2;
                    spIGMPv2.UdpPort2 = udpPort2;
                }

                if(!senderProfilesIGMPv3.ContainsKey(spIGMPv3))
                    senderProfilesIGMPv3.Add(spIGMPv3, (IScriptedBase) originator);

                if(!senderProfilesIGMPv2.ContainsKey(spIGMPv2))
                    senderProfilesIGMPv2.Add(spIGMPv2, (IScriptedBase) originator);

                TriggerReceiversRetally();

				/*bncs.Debug(string.Format("SENDER ADD - GrpAddr1: {0}, GrpAddr2: {1}, SrcAddr1: {2}, SrcAddr2: {3}, UdpPort1: {4}, UdpPort2: {5} - current no of senders: {6}",
								sp.GrpAddr1, sp.GrpAddr2, sp.SrcAddr1, sp.SrcAddr2, sp.UdpPort1, sp.UdpPort2, senderProfilesIGMPv3.Count), this);*/
            }
            else if (topic == "Remove sender profile") 
            {
                SenderProfileIGMPv3 spIGMPv3 = new SenderProfileIGMPv3();
                SenderProfileIGMPv2 spIGMPv2 = new SenderProfileIGMPv2();

                string grpAddr1 = string.Empty;
                if (settings.TryGetValue("group_address_1", out grpAddr1))
                {
                    spIGMPv3.GrpAddr1 = grpAddr1;
                    spIGMPv2.GrpAddr1 = grpAddr1;
                }

                string grpAddr2 = string.Empty;
                if (settings.TryGetValue("group_address_2", out grpAddr2))
                {
                    spIGMPv3.GrpAddr2 = grpAddr2;
                    spIGMPv2.GrpAddr2 = grpAddr2;
                }

                string srcAddr1 = string.Empty;
                if (settings.TryGetValue("source_address_1", out srcAddr1))
                {
                    spIGMPv3.SrcAddr1 = srcAddr1;
                }

                string srcAddr2 = string.Empty;
                if (settings.TryGetValue("source_address_2", out srcAddr2))
                {
                    spIGMPv3.SrcAddr2 = srcAddr2;
                }

                string udpPort1 = string.Empty;
                if (settings.TryGetValue("udp_port_1", out udpPort1))
                {
                    spIGMPv3.UdpPort1 = udpPort1;
                    spIGMPv2.UdpPort1 = udpPort1;
                }

                string udpPort2 = string.Empty;
                if (settings.TryGetValue("udp_port_2", out udpPort2))
                {
                    spIGMPv3.UdpPort2 = udpPort2;
                    spIGMPv2.UdpPort2 = udpPort2;
                }

                if(senderProfilesIGMPv3.ContainsKey(spIGMPv3))
                    senderProfilesIGMPv3.Remove(spIGMPv3);

                if(senderProfilesIGMPv2.ContainsKey(spIGMPv2))
                    senderProfilesIGMPv2.Remove(spIGMPv2);

                TriggerReceiversRetally();

				/*bncs.Debug(string.Format("SENDER ADD - GrpAddr1: {0}, GrpAddr2: {1}, SrcAddr1: {2}, SrcAddr2: {3}, UdpPort1: {4}, UdpPort2: {5} - current no of senders: {6}",
								sp.GrpAddr1, sp.GrpAddr2, sp.SrcAddr1, sp.SrcAddr2, sp.UdpPort1, sp.UdpPort2, senderProfilesIGMPv3.Count), this);*/
            }
            else if (topic == "Get receiver tally")
            {
                bool is2022_6 = false;
                string is2022_6_str = string.Empty;
                if(settings.TryGetValue("is2022_6", out is2022_6_str))
                {
                    if(is2022_6_str == "true")
                        is2022_6 = true;
                }

                SenderProfileIGMPv3 spIGMPv3 = new SenderProfileIGMPv3();
                SenderProfileIGMPv2 spIGMPv2 = new SenderProfileIGMPv2();

                string grpAddr1 = string.Empty;
                if (settings.TryGetValue("group_address_1", out grpAddr1))
                {
                    spIGMPv3.GrpAddr1 = grpAddr1;
                    spIGMPv2.GrpAddr1 = grpAddr1;
                }

                string grpAddr2 = string.Empty;
                if (settings.TryGetValue("group_address_2", out grpAddr2))
                {
                    spIGMPv3.GrpAddr2 = grpAddr2;
                    spIGMPv2.GrpAddr2 = grpAddr2;
                }

                string srcAddr1 = string.Empty;
                if (settings.TryGetValue("source_address_1", out srcAddr1))
                {
                    spIGMPv3.SrcAddr1 = srcAddr1;
                }

                string srcAddr2 = string.Empty;
                if (settings.TryGetValue("source_address_2", out srcAddr2))
                {
                    spIGMPv3.SrcAddr2 = srcAddr2;
                }

                string udpPort1 = string.Empty;
                if (settings.TryGetValue("udp_port_1", out udpPort1))
                {
                    spIGMPv3.UdpPort1 = udpPort1;
                    spIGMPv2.UdpPort1 = udpPort1;
                }

                string udpPort2 = string.Empty;
                if (settings.TryGetValue("udp_port_2", out udpPort2))
                {
                    spIGMPv3.UdpPort2 = udpPort2;
                    spIGMPv2.UdpPort2 = udpPort2;
                }

                bncs.Debug(string.Format("Tally question for - GrpAddr1: {0}, GrpAddr2: {1}, SrcAddr1: {2}, SrcAddr2: {3}, UdpPort1: {4}, UdpPort2: {5}",
								spIGMPv3.GrpAddr1, spIGMPv3.GrpAddr2, spIGMPv3.SrcAddr1, spIGMPv3.SrcAddr2, spIGMPv3.UdpPort1, spIGMPv3.UdpPort2), this);

				/*bncs.Debug(string.Format("TALLY REQ - GrpAddr1: {0}, GrpAddr2: {1}, SrcAddr1: {2}, SrcAddr2: {3}, UdpPort1: {4}, UdpPort2: {5} - current no of senders: {6}",
								sp.GrpAddr1, sp.GrpAddr2, sp.SrcAddr1, sp.SrcAddr2, sp.UdpPort1, sp.UdpPort2, senderProfilesIGMPv3.Count), this);*/
                if(is2022_6)
                {
                    var senderScripts = senderProfilesIGMPv3.Where(w => w.Key.GrpAddr1 == spIGMPv3.GrpAddr1 && w.Key.SrcAddr1 == spIGMPv3.SrcAddr1 && w.Key.UdpPort1 == spIGMPv3.UdpPort1);

                    if(senderScripts.Count() > 0)
                    {
                        IScriptedBase senderScript = senderScripts.First().Value;

                        Dictionary<string, string> tallyNameIndexInstance = new Dictionary<string, string>() {
                                {"senderIndex", senderScript.GetMappedIndex()},
                                {"senderName", senderScript.GetNameIDTypeInstance().Name},
                                {"senderInstance", senderScript.GetNameIDTypeInstance().Instance},
                            };			

                            bncs.Debug(string.Format("Pushing new tally to Receiver - Index: {0}, Name: {1}, Instance: {2}",
                                    senderScript.GetMappedIndex(), senderScript.GetNameIDTypeInstance().Name, senderScript.GetNameIDTypeInstance().Instance), this);		

                            originator.TopicSettingsFromComponent(topic, tallyNameIndexInstance, this);
                    }
                }
                else
                {
                    if(settings.Count == 6)
                    {
                        IScriptedBase senderScript;
                        if (senderProfilesIGMPv3.TryGetValue(spIGMPv3, out senderScript))
                        {
                            Dictionary<string, string> tallyNameIndexInstance = new Dictionary<string, string>() {
                                {"senderIndex", senderScript.GetMappedIndex()},
                                {"senderName", senderScript.GetNameIDTypeInstance().Name},
                                {"senderInstance", senderScript.GetNameIDTypeInstance().Instance},
                            };			

                            bncs.Debug(string.Format("Pushing new tally to Receiver - Index: {0}, Name: {1}, Instance: {2}",
                                    senderScript.GetMappedIndex(), senderScript.GetNameIDTypeInstance().Name, senderScript.GetNameIDTypeInstance().Instance), this);		

                            originator.TopicSettingsFromComponent(topic, tallyNameIndexInstance, this);                    
                        }
                        else
                        {
                            Dictionary<string, string> tallyNameIndexInstance = new Dictionary<string, string>() {
                                {"senderIndex", "0"},
                                {"senderName", "---"},
                                {"senderInstance", "---"},
                            };					

                            originator.TopicSettingsFromComponent(topic, tallyNameIndexInstance, this);
                        }      
                    }
                    else
                    {
                        IScriptedBase senderScript;
                        if (senderProfilesIGMPv2.TryGetValue(spIGMPv2, out senderScript))
                        {
                            Dictionary<string, string> tallyNameIndexInstance = new Dictionary<string, string>() {
                                {"senderIndex", senderScript.GetMappedIndex()},
                                {"senderName", senderScript.GetNameIDTypeInstance().Name},
                                {"senderInstance", senderScript.GetNameIDTypeInstance().Instance},
                            };	

                            //bncs.Debug(string.Format("Pushing new tally to Receiver - Index: {0}, Name: {1}, Instance: {2}", senderScript.GetMappedIndex(), senderScript.GetNameIDTypeInstance().Name, senderScript.GetNameIDTypeInstance().Instance), this);				

                            originator.TopicSettingsFromComponent(topic, tallyNameIndexInstance, this);                    
                        }
                        else
                        {
                            Dictionary<string, string> tallyNameIndexInstance = new Dictionary<string, string>() {
                                {"senderIndex", "0"},
                                {"senderName", "---"},
                                {"senderInstance", "---"},
                            };					

                            originator.TopicSettingsFromComponent(topic, tallyNameIndexInstance, this);
                        }
                    }
                }                         
            }
			else if (topic == "Find sender by index")
			{
				UInt32 senderIndex;
				if(UInt32.TryParse(settings.First().Value, out senderIndex))
				{
					ManagedSender sender;
					if(managedSenders.TryGetValue(senderIndex, out sender))
					{
                        sender.Script.TopicSettingsFromComponent("Find sender by index", null, originator);
					}
				}
			}
        } 

        private void TriggerReceiversRetally()
        {
            foreach (var kvpR in managedReceivers)
            {
                kvpR.Value.Script.TopicSettingsFromComponent("Retally", null, this);
            }
        }
		
		public void RegisterForTopic(string topic, IInterComponentMessaging originator)
		{

		}	
		
		public void InfoDriverRevertiveCallback(uint device, uint index, string value)
		{
			
		}
		
		public void RouterRevertiveCallback(uint device, uint destination, uint source, string sourceName)
		{
			
			
		}

		public void DatabaseCallback(uint device, uint db, uint index, string value)
		{
			//bncs.Debug(string.Format("DatabaseCallback device: {0} db: {1} index: {2} value: {3}", device, db, index, value), this);
			
			if(device == (uint)baseInstance.Reference.DeviceNumber)
			{
				switch(db)
				{
					case 0:
						{
                            ManagedSender sender;
							if (managedSenders.TryGetValue(index, out sender))
							{
								sender.Name = value;

                                TriggerReceiversRetally();
							}							
						}
					break;
					
					case 1:
						{
							ManagedReceiver receiver;
							if (managedReceivers.TryGetValue(index, out receiver))
							{
								receiver.Name = value;
							}
						}
					break;
					
					case 4:
						{
							ManagedSender sender;
							if (managedSenders.TryGetValue(index, out sender))
							{
								sender.Profile = value;

                                TriggerReceiversRetally();
							}	
						}
					break;		
					
					break;
				}
			}
		}

		public string GenerateGUID()
		{
			return Guid.NewGuid().ToString();
		}

        public void TxRxState(string state)
		{
			vmTxRxState.Value = state;

            if(bncs.GetTxRxState() == "Connected")
			{
				bncs.SetExternalTopStrap("Connected", "Green");
			}
			else if(bncs.GetTxRxState() == "Disconnected")
			{
				bncs.SetExternalTopStrap("Disconnected", "Red");
			}
			else if(bncs.GetTxRxState() == "RxOnly")
			{
				bncs.SetExternalTopStrap("RxOnly", "Orange");
			}
			else if(bncs.GetTxRxState() == "TxRx")
			{
				bncs.SetExternalTopStrap("TxRx", "Green");
			}

            if(initialise_once == false && (bncs.GetTxRxState() == "Connected" || bncs.GetTxRxState() == "TxRx" || bncs.GetTxRxState() == "RxOnly") && 
                bncs.GetCsiState() == "Connected")
            {
                initialise_once = true;
                Initialise();
            }
		} 

        public void CsiState(string state)
        {
            vmCsiState.Value = state;

            if(bncs.GetCsiState() == "Connected")
			{
				bncs.SetCsiTopStrap("Connected", "Green");
			}
			else if(bncs.GetCsiState() == "Disconnected")
			{
				bncs.SetCsiTopStrap("Disconnected", "Red");
			}
			else if(bncs.GetCsiState() == "Connecting")
			{
				bncs.SetCsiTopStrap("Connecting", "Yellow");
			}	

            if(initialise_once == false && (bncs.GetTxRxState() == "Connected" || bncs.GetTxRxState() == "TxRx" || bncs.GetTxRxState() == "RxOnly") && 
                bncs.GetCsiState() == "Connected")
            {
                initialise_once = true;
                Initialise();
            }
        }
	}
	
	public class ManagedReceiver 
    {
        private string name = string.Empty;

        public string Name
        {
            get { return name; }
            set { 
                name = value; 

                if(this.script != null)
                    this.script.TopicSettingsFromComponent("NewName", new Dictionary<string, string>(){{"name", value}}, null);
                }
        }

        private string instanceStr = string.Empty;

        public string InstanceStr
        {
            get { return instanceStr; }
            set { instanceStr = value; }
        }

        private BNCSDeviceInstance instance;

        public BNCSDeviceInstance Instance
        {
            get { return instance; }
            set { instance = value; }
        }

        private IScriptedBase script;

        public IScriptedBase Script
        {
            get { return script; }
            set { script = value; }
        }

        public ManagedReceiver(string name, string instanceStr, BNCSDeviceInstance instance, IScriptedBase script)
        {
            this.name = name;
            this.instanceStr = instanceStr;
            this.instance = instance;
            this.script = script;
        }
    }

	public class ManagedSender
    {
        private string name = string.Empty;

        public string Name
        {
            get { return name; }
            set { 
                name = value;
                if(this.script != null)
                    this.script.TopicSettingsFromComponent("NewName", new Dictionary<string, string>(){{"name", value}}, null);
             }
        }

        private string instanceStr = string.Empty;

        public string InstanceStr
        {
            get { return instanceStr; }
            set { instanceStr = value; }
        }

        private BNCSDeviceInstance instance;

        public BNCSDeviceInstance Instance
        {
            get { return instance; }
            set { instance = value; }
        }
		
		private string profile = string.Empty;

        public string Profile
        {
            get { return profile; }
            set { 
                profile = value; 
                if(this.script != null)
                    this.script.TopicSettingsFromComponent("NewProfile", new Dictionary<string, string>(){{"profile", value}}, null);
                }
        }

        private IScriptedBase script;

        public IScriptedBase Script
        {
            get { return script; }
            set { script = value; }
        }

        public ManagedSender(string name, string instanceStr, BNCSDeviceInstance instance, string profile, IScriptedBase script)
        {
            this.name = name;
            this.instanceStr = instanceStr;
            this.instance = instance;
			this.profile = profile;
            this.script = script;
        }
    }
	
	public class SenderProfileIGMPv3
    {
        private string grpAddr1 = string.Empty;

        public string GrpAddr1
        {
            get { return grpAddr1; }
            set { grpAddr1 = value; }
        }

        private string grpAddr2 = string.Empty;

        public string GrpAddr2
        {
            get { return grpAddr2; }
            set { grpAddr2 = value; }
        }

        private string srcAddr1 = string.Empty;

        public string SrcAddr1
        {
            get { return srcAddr1; }
            set { srcAddr1 = value; }
        }

        private string srcAddr2 = string.Empty;

        public string SrcAddr2
        {
            get { return srcAddr2; }
            set { srcAddr2 = value; }
        }

        private string udpPort1 = string.Empty;

        public string UdpPort1
        {
            get { return udpPort1; }
            set { udpPort1 = value; }
        }

        private string udpPort2 = string.Empty;

        public string UdpPort2
        {
            get { return udpPort2; }
            set { udpPort2 = value; }
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            SenderProfileIGMPv3 sp = obj as SenderProfileIGMPv3;
            if (sp == null)
                return false;

            return ((this.GrpAddr1 == sp.GrpAddr1) && (this.GrpAddr2 == sp.GrpAddr2) &&
                (this.SrcAddr1 == sp.SrcAddr1) && (this.SrcAddr2 == sp.SrcAddr2) &&
                (this.UdpPort1 == sp.UdpPort1) && (this.UdpPort2 == sp.UdpPort2));
        }

        public bool Equals(SenderProfileIGMPv3 sp)
        {
            if (sp == null)
                return false;

            return ((this.GrpAddr1 == sp.GrpAddr1) && (this.GrpAddr2 == sp.GrpAddr2) &&
                (this.SrcAddr1 == sp.SrcAddr1) && (this.SrcAddr2 == sp.SrcAddr2) &&
                (this.UdpPort1 == sp.UdpPort1) && (this.UdpPort2 == sp.UdpPort2));
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;                
                hash = hash * 23 + GrpAddr1.GetHashCode();
                hash = hash * 23 + GrpAddr2.GetHashCode();
                hash = hash * 23 + SrcAddr1.GetHashCode();
                hash = hash * 23 + SrcAddr2.GetHashCode();
                hash = hash * 23 + UdpPort1.GetHashCode();
                hash = hash * 23 + UdpPort2.GetHashCode();
                return hash;
            }
        }       
    }

    public class SenderProfileIGMPv2
    {
        private string grpAddr1 = string.Empty;

        public string GrpAddr1
        {
            get { return grpAddr1; }
            set { grpAddr1 = value; }
        }

        private string grpAddr2 = string.Empty;

        public string GrpAddr2
        {
            get { return grpAddr2; }
            set { grpAddr2 = value; }
        }

        private string udpPort1 = string.Empty;

        public string UdpPort1
        {
            get { return udpPort1; }
            set { udpPort1 = value; }
        }

        private string udpPort2 = string.Empty;

        public string UdpPort2
        {
            get { return udpPort2; }
            set { udpPort2 = value; }
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            SenderProfileIGMPv2 sp = obj as SenderProfileIGMPv2;
            if (sp == null)
                return false;

            return ((this.GrpAddr1 == sp.GrpAddr1) && (this.GrpAddr2 == sp.GrpAddr2) &&                
                (this.UdpPort1 == sp.UdpPort1) && (this.UdpPort2 == sp.UdpPort2));
        }

        public bool Equals(SenderProfileIGMPv2 sp)
        {
            if (sp == null)
                return false;

            return ((this.GrpAddr1 == sp.GrpAddr1) && (this.GrpAddr2 == sp.GrpAddr2) &&                
                (this.UdpPort1 == sp.UdpPort1) && (this.UdpPort2 == sp.UdpPort2));
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;                
                hash = hash * 23 + GrpAddr1.GetHashCode();
                hash = hash * 23 + GrpAddr2.GetHashCode();                
                hash = hash * 23 + UdpPort1.GetHashCode();
                hash = hash * 23 + UdpPort2.GetHashCode();
                return hash;
            }
        }       
    }
}

public class DatabasesCache
{
    public string valueDB0 { get; set; }
    public string valueDB1 { get; set; }
    public string valueDB4 { get; set; }
    public string valueDB6 { get; set; }
    public string valueDB7 { get; set; }

    public DatabasesCache(string db0, string db1, string db4, string db6, string db7)
    {
        this.valueDB0 = db0;
        this.valueDB1 = db1;
        this.valueDB4 = db4;
        this.valueDB6 = db6;
        this.valueDB7 = db7;
    }
}

