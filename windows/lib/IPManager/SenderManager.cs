using IPManager;
using BNCS;
using System.Collections.Generic;
using System;
using System.Linq;

namespace SenderManager
{
	public class SenderManager : IScriptedBase
	{	
		private string scriptVersionTag = "1.0.0.11";

		private BncsContext bncs;
		
		private IScriptedBase parent;	
		
		private SetCollection<ViewParameter> viewModel;

        private ViewParameter vmName;

		private ViewParameter vmBaseInstance;
		
		private ViewParameter vmDeviceType;
		
		private Dictionary<string, IInterComponentMessaging> registeredTopics = new Dictionary<string, IInterComponentMessaging>();

        private Dictionary<string, string> configuredProfile = new Dictionary<string, string>();

        private Dictionary<string, string> actualProfile = new Dictionary<string, string>();
		
		private BNCSDeviceInstance baseInstance;

        private ViewCategory nameIDTypeInstance;

		private string mappedIndex = "0";
		
		public void SetContext(BncsContext bncs, SetCollection<ViewParameter> viewModel)
		{
			this.bncs = bncs;
			this.viewModel = viewModel;
		}
		
		public SetCollection<ViewParameter> GetViewModel()
		{
			return viewModel;
		}
		
		public void Main(IScriptedBase parent, Dictionary<string, string> parameters)
		{
			this.parent = parent;
			
            nameIDTypeInstance = bncs.CreateViewCategory(GenerateGUID(), "Unknown", "Sender", "Unknown", "Unknown");
			
			//Prepare view model					
            vmName = bncs.CreateViewParameter("Name", "Unknown");
			vmBaseInstance = bncs.CreateViewParameter("Base instance", "Unknown");
			vmDeviceType = bncs.CreateViewParameter("Device type", "Unknown");

            viewModel.Add(vmName);
			viewModel.Add(vmBaseInstance);
			viewModel.Add(vmDeviceType);

			//Get parameters
			if (parameters.TryGetValue("index", out mappedIndex))
			{
				bncs.Debug(string.Format("Sender Script got index {0}", mappedIndex), this);
			}

            string nameStr;
            if (parameters.TryGetValue("name", out nameStr))
			{
                vmName.Value = nameStr;

				nameIDTypeInstance.Name = nameStr;

                nameIDTypeInstance.ListName = string.Format("{0} {1}", mappedIndex.ToString().PadLeft(4, '0'), nameStr);
			}
			else
			{	
				bncs.Debug("Name not defined", this);
			}            				
			
			string profileStr;
			if (parameters.TryGetValue("profile", out profileStr))
			{
				List<string> settingsPairs = profileStr.Split('|').ToList();
				
				if(settingsPairs.Count > 0)
				{
					foreach(var pair in settingsPairs)
					{
						List<string> settingsAndValue = pair.Split('=').ToList();
						
						if(settingsAndValue.Count == 2)
						{
							string header = settingsAndValue.First();
							string value = settingsAndValue.Last();
							
							ViewParameter vmSetting = bncs.CreateViewParameter(header, value);																						
						
							viewModel.Add(vmSetting);

							configuredProfile.Add(header, value);
						}
					}
				}
			}		
			else
			{	
				bncs.Debug("Profile not defined", this);
			}
			
			string strBaseInstance;
			if (parameters.TryGetValue("instance", out strBaseInstance))
			{
                nameIDTypeInstance.Instance = strBaseInstance;

				baseInstance = bncs.ResolveInstance(strBaseInstance);
				
				vmBaseInstance.Value = strBaseInstance;
				
				if(baseInstance != null)
				{
					vmDeviceType.Value = baseInstance.DeviceTypeName;
				}
				
				bncs.Debug(string.Format("Base instance: {0}", strBaseInstance), this);

				//bncs.Debug(string.Format("my nameIDTypeInstance is name: {0} instance: {1}", nameIDTypeInstance.Name, nameIDTypeInstance.Instance), this);
				
				Initialise();
			}		
			else
			{	
				bncs.Debug("Base instance not resolved", this);
			}
		}

        public ViewCategory GetNameIDTypeInstance()
        {
			//bncs.Debug(string.Format("GetNameIDTypeInstance() name: {0} instance: {1}", nameIDTypeInstance.Name, nameIDTypeInstance.Instance), this);
            return nameIDTypeInstance;
        }

		public string GetMappedIndex()
        {
            return mappedIndex;
        }
		
		public void Initialise()
		{			
			parent.TopicSettingsFromComponent("Add sender profile", configuredProfile, this);
		}

		public void RevertiveFromExternal(string message)
		{

		}

        public void TopicSettingsFromComponent(string topic, Dictionary<string, string> settings, IInterComponentMessaging originator)
        {
			if(topic == "Find sender by index")
			{
				originator.TopicSettingsFromComponent(topic, configuredProfile, this);
			}
			else if(topic == "NewName")
			{
				string newName = string.Empty;
                if(settings.TryGetValue("name", out newName))
                {
                    vmName.Value = newName;

					nameIDTypeInstance.Name = newName;

                    nameIDTypeInstance.ListName = string.Format("{0} {1}", mappedIndex.ToString().PadLeft(4, '0'), newName);
                }
			}
			else if(topic == "NewProfile")
			{
				string profileStr = string.Empty;
                if(settings.TryGetValue("profile", out profileStr))
                {
					parent.TopicSettingsFromComponent("Remove sender profile", configuredProfile, this);

					configuredProfile.Clear();

                    List<string> settingsPairs = profileStr.Split('|').ToList();
				
					if(settingsPairs.Count > 0)
					{
						foreach(var pair in settingsPairs)
						{
							List<string> settingsAndValue = pair.Split('=').ToList();
							
							if(settingsAndValue.Count == 2)
							{
								string header = settingsAndValue.First();
								string value = settingsAndValue.Last();

								var filter = viewModel.Where(f => f.Header == header);

								if(filter.Count() > 0)
                					filter.First().Value = value;
								else
								{
									ViewParameter vmSetting = bncs.CreateViewParameter(header, value);																						
							
									viewModel.Add(vmSetting);
								}

								configuredProfile.Add(header, value);
							}
						}
					}

					parent.TopicSettingsFromComponent("Add sender profile", configuredProfile, this);
                }
			}
        } 
		
		public void RegisterForTopic(string topic, IInterComponentMessaging originator)
		{
			
		}	
		
		public void InfoDriverRevertiveCallback(uint device, uint index, string value)
		{
			
		}
		
		public void RouterRevertiveCallback(uint device, uint destination, uint source, string sourceName)
		{
			
			
		}

		public void DatabaseCallback(uint device, uint db, uint index, string value)
		{
			
		}

		public string GenerateGUID()
		{
			return Guid.NewGuid().ToString();
		}

		public void TxRxState(string state)
		{
			
		}

		public void CsiState(string state)
        {
         
        }
	}
}

