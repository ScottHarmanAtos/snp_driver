using IPManager;
using BNCS;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using Receivers;

namespace ReceiverManager
{
	public class ReceiverManager : IScriptedBase
	{	
        private string scriptVersionTag = "1.0.0.19";

		private BncsContext bncs;
		
		private IScriptedBase parent;	
		
		private SetCollection<ViewParameter> viewModel;

        private ViewParameter vmName;
		
		private ViewParameter vmBaseInstance;
		
		private ViewParameter vmDeviceType;

        private ViewParameter vmExternalIndex;
		
		private Dictionary<string, IInterComponentMessaging> registeredTopics = new Dictionary<string, IInterComponentMessaging>();
		
		private BNCSDeviceInstance baseInstance;
		
		private ReceiverProfile receiverProfile;

        private List<LiveParamAndInst> realProfileList = new List<LiveParamAndInst>();

        private Dictionary<string, LiveParamAndInst> realProfileDict = new Dictionary<string, LiveParamAndInst>();

        private Dictionary<string, ViewParameter> realValues = new Dictionary<string, ViewParameter>();

        private ViewCategory nameIDTypeInstance;

        private ReceiverEntity specificReceiver;

        private uint externalIndexMapping;

        private string mappedIndex = "0";
        
        public void SetContext(BncsContext bncs, SetCollection<ViewParameter> viewModel)
		{
			this.bncs = bncs;
            this.viewModel = viewModel;
		}        
		
		public SetCollection<ViewParameter> GetViewModel()
		{
			return viewModel;
		}
		
		public void Main(IScriptedBase parent, Dictionary<string, string> parameters)
		{
			this.parent = parent;

            nameIDTypeInstance = bncs.CreateViewCategory(GenerateGUID(), "Unknown", "Receiver", "Unknown", "Unknown");
			
			//Prepare view model					
            vmName = bncs.CreateViewParameter("Name", "Unknown");	
			vmBaseInstance = bncs.CreateViewParameter("Base instance", "Unknown");			
			vmDeviceType = bncs.CreateViewParameter("Device type", "Unknown");
            vmExternalIndex = bncs.CreateViewParameter("External mapping index", "Unknown");

            viewModel.Add(vmName);
			viewModel.Add(vmBaseInstance);
			viewModel.Add(vmDeviceType);
            viewModel.Add(vmExternalIndex);

			//Get parameters
            if (parameters.TryGetValue("index", out mappedIndex))
			{
				bncs.Debug(string.Format("Receiver Script got index {0}", mappedIndex), this);
			}

            string nameStr;
            if (parameters.TryGetValue("name", out nameStr))
			{
                vmName.Value = nameStr;

                nameIDTypeInstance.Name = nameStr;

                nameIDTypeInstance.ListName = string.Format("{0} {1}", mappedIndex.ToString().PadLeft(4, '0'), nameStr);
			}		
			else
			{	
				bncs.Debug("Name not defined", this);
			}            	
			
			string strBaseInstance;
			if (parameters.TryGetValue("instance", out strBaseInstance))
			{
                nameIDTypeInstance.Instance = strBaseInstance;

				baseInstance = bncs.ResolveInstance(strBaseInstance);
				
				vmBaseInstance.Value = strBaseInstance;
				
				if(baseInstance != null)
				{
                    bncs.Debug(string.Format("Base instance: {0}", strBaseInstance), this);

					vmDeviceType.Value = baseInstance.DeviceTypeName;
				
					receiverProfile = bncs.GetReceiverProfile(baseInstance.DeviceTypeName);
					
					if(receiverProfile != null)
					{
						if(receiverProfile.settings != null)
						{
                            ViewParameter vmSettingTallyName = bncs.CreateViewParameter("Tally - name", "");
                            ViewParameter vmSettingTallyInst = bncs.CreateViewParameter("Tally - instance", "");
                            
                            viewModel.Add(vmSettingTallyName);
                            viewModel.Add(vmSettingTallyInst);

                            realValues.Add("tallyName", vmSettingTallyName);
                            realValues.Add("tallyInstance", vmSettingTallyInst);       

							foreach(var setting in receiverProfile.settings)
							{
								ViewParameter vmSettingP = bncs.CreateViewParameter(string.Format("{0} - param", setting.id), setting.parameter);
								
								viewModel.Add(vmSettingP);

                                ViewParameter vmSettingLiveValue = bncs.CreateViewParameter(string.Format("{0} - live", setting.id), "");
        
                                viewModel.Add(vmSettingLiveValue);
                                realValues.Add(setting.id, vmSettingLiveValue);                               

                                if (string.IsNullOrEmpty(setting.group))
                                {
                                    //is main dev type

                                    BNCSDeviceTypeParameter devTParam = bncs.GetDeviceTypeParameter(baseInstance.DeviceTypeName, setting.parameter);

                                    if (devTParam != null)                                     
                                    {
                                        if (realProfileDict.ContainsKey(setting.id) == false)
                                        {
                                            LiveParamAndInst tempLiveParam = new LiveParamAndInst(setting.id, devTParam, baseInstance, devTParam.Slot + baseInstance.Reference.SlotOffset);

                                            realProfileDict.Add(setting.id, tempLiveParam);

                                            realProfileList.Add(tempLiveParam);

                                            ViewParameter vmSettingDevIndex = bncs.CreateViewParameter(string.Format("{0} - location", setting.id), string.Format("dev: {0} index: {1}", tempLiveParam.Instance.Reference.DeviceNumber, tempLiveParam.LiveIndex));
								
								            viewModel.Add(vmSettingDevIndex);

                                            bncs.Debug(string.Format("### Setting: {0} resolved to Param: {1} from Instance: {2}, DevType: {3}, TargetIndex: {4}", setting.id, setting.parameter, tempLiveParam.Instance.Id, tempLiveParam.Instance.DeviceTypeName, tempLiveParam.LiveIndex), this);
                                        }
                                    }                                    
                                }
                                else 
                                {
                                    //is composite so need to find the right device type  

                                    if (baseInstance.Composite == true)
                                    {
                                        BNCSCompositeInstanceGroupItem grpItem = baseInstance.GroupItems.Find(p => p.Id == setting.group);

                                        if (grpItem != null) 
                                        {
                                            BNCSDeviceInstance tempInstance = bncs.ResolveInstance(grpItem.Instance);

                                            if (tempInstance != null) 
                                            {
                                                BNCSDeviceTypeParameter devTParam = bncs.GetDeviceTypeParameter(tempInstance.DeviceTypeName, setting.parameter);

                                                if (devTParam != null)
                                                {
                                                    if (realProfileDict.ContainsKey(setting.id) == false)
                                                    {
                                                        LiveParamAndInst tempLiveParam = new LiveParamAndInst(setting.id, devTParam, tempInstance, devTParam.Slot + tempInstance.Reference.SlotOffset);

                                                        realProfileDict.Add(setting.id, tempLiveParam);

                                                        realProfileList.Add(tempLiveParam);

                                                        ViewParameter vmSettingDevIndex = bncs.CreateViewParameter(string.Format("{0} - location", setting.id), string.Format("dev: {0} index: {1}", tempLiveParam.Instance.Reference.DeviceNumber, tempLiveParam.LiveIndex));
								
								                        viewModel.Add(vmSettingDevIndex);

                                                        bncs.Debug(string.Format("### Setting: {0} resolved to Param: {1} from Instance: {2}, DevType: {3}, TargetIndex: {4}", setting.id, setting.parameter, tempLiveParam.Instance.Id, tempLiveParam.Instance.DeviceTypeName, tempLiveParam.LiveIndex), this);                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else 
                                    {
                                        bncs.Debug(string.Format("Params have a group but the instance provided is not composite, profile: {0}, instance: {1}", baseInstance.DeviceTypeName, baseInstance.Id), this);
                                    }                                   
                                }
							}                                     
						}

                        Initialise();
					}					
				}
			}		
			else
			{	
				bncs.Debug("Base instance not resolved", this);
			}
		}

        public ViewCategory GetNameIDTypeInstance()
        {
            return nameIDTypeInstance;
        }

        public string GetMappedIndex()
        {
            return mappedIndex;
        }
		
		public void Initialise()
		{
            switch (baseInstance.DeviceTypeName) 
            {
                case "imagine_ucip_rx":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new Selenio_Receiver();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "IQMIX40_spigot_ch":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new Iqmix40_Receiver();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "IQMIX25_spigot_ch":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new Iqmix25_Receiver();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "IQMIX10_spigot_ch":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new Iqmix10_Receiver();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "em_eb22_sfp":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new Embrionix_Receiver();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "tag_mv_ssm_tile":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new TagTileSSM();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "obe_channel":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new OBE_Channel();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "sdc01_flow":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new SDC01_Receiver();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "imagine_ccs_ucip1_rx":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new Imagine_ccs_ucip1_Receiver();

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;

                    case "NEURON_CARD_ch":
                    {
                        uint index;
                        if(UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);                        

                            vmExternalIndex.Value = externalIndexMapping.ToString();                        

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            specificReceiver = new Neuron_Card_Receiver();

                            specificReceiver.myStreamSettings.Add("is2022_6", "true");

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);                        

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict) 
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint) kvp.Value.LiveIndex, ResourceType.InfoDriver));
                                
                            }

                            bncs.Register(registrationList, this);
                        }                        
                    }                    
                    break;
                case "imagine_snp_ipVidRx":
                    {
                        //bncs.Debug(string.Format("Attmpting mapping to receiver type: {0}", baseInstance.DeviceTypeName), this);
                        uint index;
                        if (UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);

                            vmExternalIndex.Value = externalIndexMapping.ToString();

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            //
                            specificReceiver = new Selenio_Receiver();

                            //specificReceiver.myStreamSettings.Add("is2022_6", "true");

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict)
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint)kvp.Value.LiveIndex, ResourceType.InfoDriver));

                            }

                            bncs.Register(registrationList, this);
                        }
                    }
                    break;
                case "dec7882h264mini-mib":
                    {
                        //bncs.Debug(string.Format("Attmpting mapping to receiver type: {0}", baseInstance.DeviceTypeName), this);
                        uint index;
                        if (UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);

                            vmExternalIndex.Value = externalIndexMapping.ToString();

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            //
                            specificReceiver = new Evertz_7882DM_Receiver();

                            //specificReceiver.myStreamSettings.Add("is2022_6", "true");

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict)
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint)kvp.Value.LiveIndex, ResourceType.InfoDriver));

                            }

                            bncs.Register(registrationList, this);
                        }
                    }
                    break;
                    case "hitless_dec7882h264mini-mib":
                    {
                        //bncs.Debug(string.Format("Attmpting mapping to receiver type: {0}", baseInstance.DeviceTypeName), this);
                        uint index;
                        if (UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);

                            vmExternalIndex.Value = externalIndexMapping.ToString();

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            //
                            specificReceiver = new Evertz_7882DM_Receiver();

                            //specificReceiver.myStreamSettings.Add("is2022_6", "true");

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict)
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint)kvp.Value.LiveIndex, ResourceType.InfoDriver));

                            }

                            bncs.Register(registrationList, this);
                        }
                    }
                    break;
                default: 
                    {
                        bncs.Debug(string.Format("Unrecognised receiver type: {0}", baseInstance.DeviceTypeName), this);
						uint index;
                        if (UInt32.TryParse(mappedIndex, out index))
                        {
                            externalIndexMapping = bncs.RegisterWithExternal(this, index);

                            vmExternalIndex.Value = externalIndexMapping.ToString();

                            bncs.SendRevertive(externalIndexMapping, string.Empty);

                            //
                            specificReceiver = new Generic_Receiver();

                            //specificReceiver.myStreamSettings.Add("is2022_6", "true");

                            specificReceiver.Main(realProfileDict, realValues, Debug, TopicSettingsFromNestedComponent, NewTally, ChangeOwnParameter);

                            List<DevSlot> registrationList = new List<DevSlot>();

                            foreach (var kvp in realProfileDict)
                            {
                                bncs.Debug(string.Format("Registering for {0} device {1} index {2}", kvp.Key, kvp.Value.Instance.Reference.DeviceNumber, kvp.Value.LiveIndex), this);
                                registrationList.Add(new DevSlot((uint)kvp.Value.Instance.Reference.DeviceNumber, (uint)kvp.Value.LiveIndex, ResourceType.InfoDriver));

                            }

                            bncs.Register(registrationList, this);
                        }
                    }
                    break;
            }
		}

        public void Debug(string message) 
        {
            bncs.Debug(message, this);
        }

        public void NewTally(string tally)
        {
            nameIDTypeInstance.TallyName = tally;

            bncs.SendRevertive(externalIndexMapping, tally);
        }

        public void ChangeOwnParameter(string name, string value)
        {
            LiveParamAndInst liveParam;
            if(realProfileDict.TryGetValue(name, out liveParam))
            {
                bncs.InfoWrite((uint) liveParam.Instance.Reference.DeviceNumber, (uint) liveParam.LiveIndex, value);                
            }
        }

        public void TopicSettingsFromNestedComponent(string topic, Dictionary<string, string> myDetails, IInterComponentMessaging originator)
        {
            parent.TopicSettingsFromComponent(topic, myDetails, originator);
        }

		public void RevertiveFromExternal(string message)
		{
            bncs.Debug("RevertiveFromExternal: " + message, this);
			
			if(baseInstance != null)
			{
				if(message.Contains("index="))
				{
					var equalsSplit = message.Split('=').ToList();                

					UInt32 senderIndex;
					if(UInt32.TryParse(equalsSplit.ElementAt(1), out senderIndex))
					{
						parent.TopicSettingsFromComponent("Find sender by index", new Dictionary<string, string>(){{"sender", equalsSplit.ElementAt(1)}}, this);
					}
				}
				else
				{
					UInt32 senderIndex;
					if(UInt32.TryParse(message, out senderIndex))
					{
						parent.TopicSettingsFromComponent("Find sender by index", new Dictionary<string, string>(){{"sender", message}}, this);
					}
				}				
			}
			else
			{
				bncs.Debug("RevertiveFromExternal: " + message + " my receiver instance is null, no action take", this);
			}            
		}

        public void TopicSettingsFromComponent(string topic, Dictionary<string, string> settings, IInterComponentMessaging originator)
        {
            if(topic == "Find sender by index")
            {
                specificReceiver.Connect(settings);
            }
            else if(topic == "Retally")
            {
                if(specificReceiver != null)
                    specificReceiver.Retally();
            }
            else if (topic == "NewName")
            {
                string newName = string.Empty;
                if(settings.TryGetValue("name", out newName))
                {
                    vmName.Value = newName;

                    nameIDTypeInstance.Name = newName;

                    nameIDTypeInstance.ListName = string.Format("{0} {1}", mappedIndex.ToString().PadLeft(4, '0'), newName);
                }
            }
        } 
		
		public void RegisterForTopic(string topic, IInterComponentMessaging originator)
		{
			
		}	
		
		public void InfoDriverRevertiveCallback(uint device, uint index, string value)
		{
            bncs.Debug(string.Format("Script InfoDriverRevertiveCallback dev: {0} index: {1} value: {2}", device, index, value), this);

            foreach (var item in realProfileList.Where(p => ((uint)p.Instance.Reference.DeviceNumber == device) && (p.LiveIndex == index)))
            {
                specificReceiver.ParameterNotification(item.SettingID, value);
            }              
		}
		
		public void RouterRevertiveCallback(uint device, uint destination, uint source, string sourceName)
		{
			
			
		}

		public void DatabaseCallback(uint device, uint db, uint index, string value)
		{
			
		}

		public string GenerateGUID()
		{
			return Guid.NewGuid().ToString();
		}

        public void TxRxState(string state)
		{
			
		}         

        public void CsiState(string state)
        {
         
        }
	}
}