﻿function GenerateTextSettings()
    local dummy = {"Font name", "Font size", "Style", "Max chars", coordinates}

    local coordinates_line1_1 = { -1,12}
    local coordinates_line1 = { coordinates_line1_1 }
    local one_line_p = { "Lucida Console", 11, "Bold", 7, coordinates_line1 }

    local coordinates_line2_1 = { -1, 2 }
    local coordinates_line2_2 = { -1, 16 }
    local coordinates_line2 = { coordinates_line2_1, coordinates_line2_2 }
    local two_line_p = { "Lucida Console", 11, "Bold", 7, coordinates_line2 }

    local coordinates_line3_1 = { 1, 0 }
    local coordinates_line3_2 = { 1, 11 }
    local coordinates_line3_3 = { 1, 23 }
    local coordinates_line3 = { coordinates_line3_1, coordinates_line3_2, coordinates_line3_3 }
    local three_line_p = { "Press Start 2P", 8, "Regular", 7, coordinates_line3 }

    local coordinates_line2r_1 = { 1, 4 }
    local coordinates_line2r_2 = { 1, 20 }
    local coordinates_line2r = { coordinates_line2r_1, coordinates_line2r_2 }
    local two_line_reversed_p = { "Press Start 2P", 8, "Regular", 7, coordinates_line2r }

    local designer_definition = { one_line_p, two_line_p, three_line_p, two_line_reversed_p }

    return designer_definition
end
