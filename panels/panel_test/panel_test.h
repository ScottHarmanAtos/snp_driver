#ifndef panel_test_INCLUDED
	#define panel_test_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class panel_test : public bncs_script_helper
{
public:
	panel_test( bncs_client_callback * parent, const char* path );
	virtual ~panel_test();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_device;
	bncs_string m_instance;

	int m_index;
	int m_row;
	int m_column;
	int m_x, m_y;
	int m_count;
	bool m_inOrder;
	bncs_dateTime m_startTime;
	bncs_dateTime m_endTime;
	__int64 m_duration;
	bncs_string m_value;

	void enable(void);
	void disable(void);
	void clear(void);
	void update(void);
	void launch(void);
	
};


#endif // panel_test_INCLUDED