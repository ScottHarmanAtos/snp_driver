# tallyCpp C++ tally component

## Description 

This is sibling of a similarly named control written in lua. It is designed to replicate the operation of the lua control

![tally](example.png)

The component registers with an destination index on a router and displays the source name routed.

This tally component implements some standard features often used in tally components.

## Parameters

### instance

The instance of the router

### index

The destination index to watch

### single_line

When true pipes will be removed from the source name before being displayed.

## Version History

Version | Notes
--------|--------------
1.0.0.0 | First release
