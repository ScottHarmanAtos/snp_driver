#ifndef tallyCpp_INCLUDED
	#define tallyCpp_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class tallyCpp : public bncs_script_helper
{
public:
	tallyCpp( bncs_client_callback * parent, const char* path );
	virtual ~tallyCpp();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	void checkReg(void);

	bool m_singleLine;
	int m_index;
	int m_device;
	bncs_string m_instance;
	
};


#endif // tallyCpp_INCLUDED