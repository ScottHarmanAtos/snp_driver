#Tally component

PANEL_MAIN = 1
device = 0
slot = 0
single_line = "false"

function Main(us, path, pointer)
	bncs:panelShow(1, "tally.bncs_ui", true)
  bncs:textPut("text","No Revertive",PANEL_MAIN,"tally")
end

function ParentCallback(p)
	if p.command == "instance" then
		r = bncs:getDev(p.value)
		if r.result == true then
      device = r.device
      Init()
    end
  elseif p.command == "index" then
    slot = tonumber(p.value)
    Init()
  elseif p.command == "single_line" then
    single_line = p.value == "true" and "true" or "false"
	elseif p.command == "return" then
		return "index=" .. slot .."\n" .. "single_line=" .. single_line
	end
end

function RevertiveCallback(r)
  local s = r.sinfo
  if single_line == "true" then
    s = string.gsub(s,"|", " ")
  end
  bncs:textPut("text",s,PANEL_MAIN,"tally")
end

function Init()
  if device > 0 and slot > 0 then
    bncs:register(device, slot, slot, false)
    bncs:routerPoll(device, slot, slot)
  end
end
