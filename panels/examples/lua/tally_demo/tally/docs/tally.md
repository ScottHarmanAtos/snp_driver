# tally Lua host component

## Description 

This is a technology demonstration rather than fully working system.

![tally](example.png)

This is a simple tally component written in lua, to demonstrate the kind of component that is often written in c++.

The component registers with an destination index on a router and displays the source name routed.

This tally component implements some standard features often used in tally components.

## Parameters

### instance

The instance of the router

### index

The destination index to watch

### single_line

When true pipes will be removed from the source name before being displayed.

## Version History

Version | Notes
--------|--------------
1.0.0.0 | First release
