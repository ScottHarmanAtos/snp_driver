# Tally Demo/Comparison

## Description 

This UI shows a lua component along side a C++ equivalent of the same thing.

They are functionally (almost!) the same. I hope you can compare and contrast the different approaches in this simple application.

[lua tally](../tally/docs/tally.html)
[c++ tally](../tallyCpp/docs/tallyCpp.html)

## Version History

Version | Notes
--------|--------------
1.0.0.0 | First release
