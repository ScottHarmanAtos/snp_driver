# Complex Mapped Router Example

## Description

This is a more complex code-free panel that just uses smart buttons & standard components to produce a fully functioning mapped router XY panel.

## Configuration

This example uses instance of name "router" which must have the following databases set up

Database | Use 
---------|-------------------
0        | Source Names
1        | Dest Names
4        | Source pixmaps
5        | Dest pixmaps
6        | Source Instance Names
7        | Dest Instance Names
11       | Comma delimited list of sources
12       | Comma delimited list of source pages
13       | Source page names
14       | Comma delimited list of dests
15       | Comma delimited list of dest pages
16       | Dest page names

Previously these names and lists would have been offset by index all crammed into database 8, but CSI's extended database addressing capability means they can be all sensibly split out all indexed from 1

## Dependencies

This panel requires the following components:

* _core_components\take
* _core_components\adjust
* _core_components\tracebar
