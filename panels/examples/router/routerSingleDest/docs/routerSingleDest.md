# Router with single dest button

## Description

This is a simple code-free panel that just uses smart buttons & standard components to produce a fully functioning mapped router XY panel with single dest buttons.

This kind of panel is useful for stacks, or monitoring where the destinations are fixed for a particular position.

Dest. buttons control their own selection status by notifying of button press and if the current dest is theirs showing as selected. This has the added advantage that two buttons of the same destination will both illuminate.

## Dependencies

This panel requires the following components:

* _core_components\take
* _core_components\router_dest