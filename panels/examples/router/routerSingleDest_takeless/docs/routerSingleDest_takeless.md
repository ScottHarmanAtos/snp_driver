# Router with single dest buttons - takeless!

## Description

This is a simple code-free panel that just uses smart buttons & standard components to produce a fully functioning mapped router XY panel with single dest buttons.

This kind of panel is useful for stacks, or monitoring where the destinations are fixed for a particular position.

This panel is nearly the same as the routerSingleDest panel except there is no take button. The change in orientation reflects the need to select the destination first.

## Dependencies

This panel requires the following components:

* _core_components\take
* _core_components\router_dest