# Tabbed Panel Example

## Description

This is a simple code-free panel shows "tabs" - pages of separate UI or DLLs.

![example](example.png)

## How it works

The panel is created with all the UI elements in place. So if you have a three-tab panel like this example you'll have all three tabs visible and the three pages you want to use stacked on top of each other.

The key to the panel's operation is that the tabs and pages follow a convention:

Button Name    | Use
---------------|------------------------------------
tab_button_*n*   | The tab button used to select the required page
tab_selected_*n* | The tab selection indicator used to show current page
tab_host_*n*     | The UI element to show/hide for each page

where *n* is the tab number. If you copy and paste existing buttons the numeric suffix will get automatically incremented by the visual editor.

The panel then uses some connections to show/hide the relevant elements.

## Further thoughts on how to extend this example

It doesn't matter what the elements are - merely that the follow the naming convention.

The page (host) elements can be anything - an external UI file loaded from file (like this example) a DLL/lua script, or even a single button.

The selection buttons can be smart too - so if you want to show an aggregate alarm for that page then make it a smart button. This can derive its own state or be connected to a value generated from one of the host components