#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_overview.h"

#define PANEL_MAIN                "main"
#define PANEL_LAYOUT              "layout"

#define DATABASE_ASSIGNABLE_KEYS  5
#define DATABASE_SOURCE_NAME      0


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_overview )

// constructor - equivalent to ApplCore STARTUP
comms_overview::comms_overview( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	m_strLayout = "default";	

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );
	panelShow(PANEL_LAYOUT , "\\layouts\\default");
	
	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_overview::~comms_overview()
{
}

// all button pushes and notifications come here
void comms_overview::buttonCallback( buttonNotify *b )
{
	b->dump("comms_overview::buttonCallback()");
	
	bncs_stringlist sltControlName(b->id(), '_');
	bncs_string strControlPrefix = sltControlName[0];
	int intControlIndex = sltControlName[1].toInt();	
	
	if( b->panel() == PANEL_LAYOUT )
	{
		if (strControlPrefix == "panel")
		{
			map<bncs_string, comms_ringmaster_port>::iterator it = m_buttons.find(b->id());

			if (it != m_buttons.end() && it->second.valid)
			{
				bncs_string strExecute = bncs_string("adjust/adjust_intercom/assign_keys,%1:%2").arg(it->second.sourceName_short.replace('|', ' ')).arg(it->second.ring_port);
				debug("comms_overview:: strExecute=%1", strExecute);
				navigateExecute(strExecute);
			}
		}
	}
}

// all revertives come here
int comms_overview::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void comms_overview::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_overview::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		bncs_stringlist sl;
		
		sl << bncs_string( "layout=%1" ).arg( m_strLayout );
		return sl.toString( '\n' );
	}
	else if( p->command() == "layout" )
	{
		m_strLayout = p->value();
		panelDestroy(PANEL_LAYOUT);
		panelShow(PANEL_LAYOUT , bncs_string("\\layouts\\%1.bncs_ui").arg(m_strLayout));
		init();
	}
	return "";
}

// timer events come here
void comms_overview::timerCallback( int id )
{
}

void comms_overview::init()
{
	m_buttons.clear();
	bncs_stringlist sltControls = getIdList(PANEL_LAYOUT, "panel");
	for (int i=0; i<sltControls.count();i++)
	{
		bncs_string strRrcsAddress, strName;
		textGet("text", PANEL_LAYOUT,sltControls[i], strRrcsAddress);

		comms_ringmaster_port p = rh->GetPortFromRRCSAddress(strRrcsAddress);
		
		m_buttons[sltControls[i]] = p;
		if ( p.valid ) 
		{
			bncs_string strAssignableKeys, strPanelName;
			routerName(p.devices.Grd, riedelHelpers::databases::Assignable_Keys, p.slot_index, strAssignableKeys);
			textPut("text", p.sourceName_short, PANEL_LAYOUT, sltControls[i]);
			
			if (isAssignableKeyPresent(strAssignableKeys))
			{
				controlEnable(PANEL_LAYOUT, sltControls[i]);
			}
			else
			{
				controlDisable(PANEL_LAYOUT, sltControls[i]);
			}
		}
		else
		{
			controlDisable(PANEL_LAYOUT, sltControls[i]);
		}
	}

}

bool comms_overview::isAssignableKeyPresent( bncs_string keylist )
{
	bool flag= false;
	bncs_stringlist sltKeyList(keylist);
	for (int i=0; i<sltKeyList.count();i++)
	{
		if (sltKeyList[i].length()>0)
		{
			flag = true;
			break;
		}
	}
	return  flag;
}
