#ifndef comms_overview_INCLUDED
	#define comms_overview_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_overview : public bncs_script_helper
{
public:
	comms_overview( bncs_client_callback * parent, const char* path );
	virtual ~comms_overview();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void init();
	bool isAssignableKeyPresent( bncs_string keylist );
	
private:
	bncs_string m_strLayout;
	riedelHelper *rh;
	map<bncs_string, comms_ringmaster_port> m_buttons;

};


#endif // comms_overview_INCLUDED