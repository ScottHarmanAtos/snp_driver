#ifndef assign_keys_INCLUDED
	#define assign_keys_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"
#include "local_cache.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class assign_keys : public bncs_script_helper
{
public:
	assign_keys( bncs_client_callback * parent, const char* path );
	virtual ~assign_keys();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void init();
	
private:
	enum tab
	{
		TabPort,
		TabFunction
	};

	tab m_tabSelected;

	void SetTab();

	local_cache * lc;
	riedelHelper * rh;
	void loadPanel();
	int m_intShiftPage;
	int m_intCurrentKey;
	comms_ringmaster_port m_panelPort;
	int m_intSelectedFunctionIndex;
	int m_intSelectedIFB;
	int m_intPortType;
	bncs_string m_initialPageFunction;
	bncs_string m_strKeyMode;
	bncs_string m_strTalkListenMode;
	bncs_string m_strFunction;
	bncs_string m_opsPosition;
	int m_expansion;

	bncs_stringlist m_sltKeyMode;
	bncs_stringlist m_sltTalkListenModes;
	vector<pair<const comms_ringmaster_port*, int>> m_assignGroup;
	vector<pair<const comms_ringmaster_port*, int>> m_localPorts;
	vector<pair<const comms_ringmaster_port*, int>> m_popupPorts;
	void createPopup(const vector<pair<const comms_ringmaster_port*, int>>& ports, bool ignoreExpansion);

};


#endif // assign_keys_INCLUDED