#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "persist.h"
#include "assign_keys.h"
// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(assign_keys)

#define STORE_FUNC bncs_string("ringmaster.function.")

#define PANEL_MAIN	"panel_main"
#define PANEL_ACCESS_DENIED	"access_denied"
#define PANEL_NOT_FOUND	"panel_not_found"
#define POPUP_NAVIGATE "navigate_popup"

#define SHIFT_OFF	1
#define SHIFT_ON	2

#define TIMER_GROUP  1
#define TIMEOUT_GROUP  5

#define MSG_PORT_IS_PANEL  "Port is a|PANEL|Autolisten|OFF"

// constructor - equivalent to ApplCore STARTUP
assign_keys::assign_keys( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_tabSelected = TabPort;

	lc = local_cache::getLocalCache();
	rh = riedelHelper::getriedelHelperObject();

	auto other = rh->GetOtherRingmasterDevices();
	for (auto it = other.begin(); it != other.end(); ++it)
	{
		debug("Riedel Other: %1", it->Composite);
	}

	//init vars
	m_panelPort = comms_ringmaster_port();
	m_intCurrentKey = 0;
	m_intSelectedIFB=-1;
	m_intSelectedFunctionIndex =-1;
	m_intShiftPage = 1;

	m_sltKeyMode = bncs_stringlist(KEY_MODE_LIST);
	m_strKeyMode= m_sltKeyMode[0];

	m_sltTalkListenModes = bncs_stringlist(TALKLISTEN_MODE_LIST);
	m_strTalkListenMode = DEFAULT_TALK_LISTEN_MODE;
	m_intPortType = -1;


	init();
}

// destructor - equivalent to ApplCore CLOSEDOWN
assign_keys::~assign_keys()
{
}

// all button pushes and notifications come here
void assign_keys::buttonCallback( buttonNotify *b )
{
	b->dump("assign_keys::buttonCallback()");
	
	if (b->panel() == PANEL_MAIN)
	{
		bncs_stringlist sltControlName(b->id(), '_');
		bncs_string strControlPrefix = sltControlName[0];
		bncs_string strControlSuffix = sltControlName[1];
		int intControlIndex = sltControlName[1].toInt();

		if (b->id() == "grid")
		{
			if (b->command() == "index")
			{
				textPut("index", b->value(), PANEL_MAIN, "properties");
			}
		}
		else if (b->id() == "function_grid")
		{
			if (b->command() == "function")
			{
				textPut("function", b->value(), PANEL_MAIN, "properties");
			}
		}
		else if (b->id() == "function_groups")
		{
			int val = b->value().toInt();
			textPut("page", b->value().toInt(), PANEL_MAIN, "function_pages");
			lc->setValue(STORE_FUNC + "group", b->value());
		}
		else if (b->id() == "function_pages")
		{
			int val = b->value().toInt();
			textPut("page", b->value().toInt(), PANEL_MAIN, "function_grid");
			textPut("deselect","", PANEL_MAIN, "function_grid");
			textPut("function", "", PANEL_MAIN, "properties");

			lc->setValue(STORE_FUNC + "page", b->value());
		}
		else if (b->id() == "tab_port")
		{
			m_tabSelected = TabPort;
			SetTab();
		}
		else if (b->id() == "tab_function")
		{
			m_tabSelected = TabFunction;
			SetTab();
		}
		else if (b->id() == "mimic")
		{
			if (b->command() == "key")
			{
				textPut("key", b->value(), PANEL_MAIN, "properties");
				if (b->value().length() > 0 && b->value() != "0")
				{
					controlEnable(PANEL_MAIN, "properties");
				}
				else
				{
					controlDisable(PANEL_MAIN, "properties");
				}
			}
		}
		else if (b->id() == "assign_group")
		{
			createPopup(m_assignGroup, true);
		}
		else if (b->id() == "local_panels")
		{
			createPopup(m_localPorts, false);
		}
	}
	else if (b->panel() == POPUP_NAVIGATE)
	{
		if (b->value() == "released")
		{
			auto panel = m_popupPorts[b->id()].first;
			auto expansion = m_popupPorts[b->id()].second;

			bncs_string s = panel->sourceName_short;
			//Navigate to panel
			bncs_string strExecute = bncs_string("comms/assign_keys,%1:%2.%3").arg(s.replace('|', ' ')).arg(panel->ring_port).arg(expansion);
			navigateExecute(strExecute);
		}
	}

}

// all revertives come here
int assign_keys::revertiveCallback( revertiveNotify * r )
{
	if ( r->device() == rh->GetRingmasterDevices().Ifbs)
	{
		if(m_intSelectedIFB > 0 &&  r->index() == m_intSelectedIFB)
		{
			bncs_stringlist sltRevertive(r->sInfo(), '|') ;
			bncs_string strLabel = sltRevertive[3];
			strLabel = strLabel.length() >0?strLabel:"<NO BNCS LABEL>";
			textPut("text", strLabel, PANEL_MAIN, "label");
		}
	}
	return 0;
}

// all database name changes come back here
void assign_keys::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string assign_keys::parentCallback( parentNotify *p )
{
	p->dump("assign_keys");
	if( p->command() == "raw_parameters" || (p->command() == "instance" && p->value().length() == 0) )
	{
		//Possibly Some text:ring.port.expansion or Some text:ring.port 
		bncs_string value = p->value();

		bool isLocalPanel = false;
		m_opsPosition = "";
		//No value provided load the local panel if one is specified.
		if (p->value().length() == 0)
		{
			isLocalPanel = true;
			auto v = rh->GetLocalPanel();
			value = v.ring_port;

			//@NOTE: This does not write a nice title like Navigate Adjust does.
			//It is possible to do this by calling Navigate Adjust here with a value.
			//But that will screw up the back button.
		}		

		bncs_stringlist sl(value, ':');

		//Possibly ring.port.expansion or ring.port
		bncs_string addressAsString = sl[sl.count() - 1];
		bncs_stringlist address(addressAsString,'.');
		auto oldExpansion = m_expansion;
		if (address.count() == 3)
		{
			m_expansion = address[2];
		}
		else
		{
			m_expansion = 0; //If no expansion, use default 0
		}

		auto oldPort = m_panelPort;
		m_panelPort = rh->GetPort(address[0], address[1]);

		//Check we are allowed to load the panel.
		if (!m_panelPort.valid || !(m_panelPort.portType == ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL || m_panelPort.portType == PORT_TYPE_PANEL_DUAL_CHANNEL))
		{
			panelRemove(PANEL_MAIN);
			panelShow(PANEL_NOT_FOUND, "panel_not_found.bncs_ui");
			bncs_string text;
			textGet("text", PANEL_NOT_FOUND, "Replace", text);
			textPut("text", text.replace("%panel%", m_panelPort.ring_port), PANEL_NOT_FOUND, "Replace");
		}
		else if (!rh->CanAccessPanel(m_panelPort.ring_id, m_panelPort.port))
		{
			//Error panel can not be access by this machine.
			panelRemove(PANEL_MAIN);
			panelShow(PANEL_ACCESS_DENIED, "access_denied.bncs_ui");
			bncs_string text;
			textGet("text", PANEL_ACCESS_DENIED, "Replace", text);
			textPut("text",text.replace("%ops_position%",rh->Ops_Position()).replace("%panel%", m_panelPort.ring_port), PANEL_ACCESS_DENIED, "Replace");
			return "";
		}
		else
		{
			panelDestroy(PANEL_NOT_FOUND);
			panelDestroy(PANEL_ACCESS_DENIED);
			panelShow(PANEL_MAIN, "main.bncs_ui");
		}

		//NOTE: It would be possible to change the panels without navigating, would be faster and less flashy.
		//BUT... this would mean the title bar is incorrect and the 

		if (oldPort.ring_port_leading_zero != m_panelPort.ring_port_leading_zero || m_expansion != oldExpansion)
		{
			textPut("ringport", value, PANEL_MAIN, "mimic");

			if (isLocalPanel == false)
			{
				bncs_string s;
				routerName(m_panelPort.devices.Grd, 4, m_panelPort.slot_index, s);
				bncs_stringlist sl(s);
				m_opsPosition = sl.getNamedParam("ops_position");
			}

			loadPanel();
		}
		//Clear last selected key
		controlDisable(PANEL_MAIN, "properties");
		textPut("key","", PANEL_MAIN, "properties");
	}
	return "";
}

// timer events come here
void assign_keys::timerCallback( int id )
{
	timerStop(id);
	if (id == TIMER_GROUP)
	{
		textPut(bncs_string("button.") + m_initialPageFunction, "released", PANEL_MAIN, "function_pages");
	}

}

void assign_keys::SetTab()
{
	lc->setValue("ringmaster.key_assign_mode", (int)m_tabSelected);
	controlHide(PANEL_MAIN, "function_grid");
	controlHide(PANEL_MAIN, "function_groups");
	controlHide(PANEL_MAIN, "function_pages");
	controlHide(PANEL_MAIN, "function_groupbox");
	controlHide(PANEL_MAIN, "grid");

	textPut("statesheet", "enum_deselected", PANEL_MAIN, "tab_port");
	textPut("statesheet", "enum_deselected", PANEL_MAIN, "tab_function");

	textPut("deselect", "", PANEL_MAIN, "function_grid");
	textPut("deselect", "", PANEL_MAIN, "grid");
	textPut("function", "", PANEL_MAIN, "properties");

	switch (m_tabSelected)
	{
	case assign_keys::TabPort:
		controlShow(PANEL_MAIN, "grid");
		textPut("statesheet", "enum_selected", PANEL_MAIN, "tab_port");
		break;
	case assign_keys::TabFunction:
		controlShow(PANEL_MAIN, "function_grid");
		controlShow(PANEL_MAIN, "function_groups");
		controlShow(PANEL_MAIN, "function_pages");
		controlShow(PANEL_MAIN, "function_groupbox");
		textPut("statesheet", "enum_selected", PANEL_MAIN, "tab_function");
		break;
	default:
		break;
	}

}

void assign_keys::loadPanel()
{
	//Chose the correct function panel
	debug("assign_keys::loadPanel() inst:%1 ring:%2", m_panelPort.devices.Monitor_Instance, m_panelPort.ring_id);
	
	textPut("text", m_panelPort.sourceName_short, PANEL_MAIN, "grpPanel");
	m_assignGroup.clear();
	if (rh->CanAccessAssignGroup(m_panelPort.ring_id, m_panelPort.port))
	{
		auto assignGroups = rh->GetAssignGroup(m_panelPort.ring_id, m_panelPort.port);

		for (auto a = assignGroups.begin(); a != assignGroups.end(); ++a)
		{
			m_assignGroup.push_back(make_pair(*a, 0));
		}
	}

	if (m_assignGroup.empty())
	{
		controlDisable(PANEL_MAIN, "assign_group");
	}
	else
	{
		controlEnable(PANEL_MAIN, "assign_group");
	}

	//Load all the local panels
	m_localPorts.clear();
	auto ports = rh->GetLocalPorts(m_panelPort.ring_id, m_panelPort.port);
	for (auto i = ports.begin(); i != ports.end(); ++i)
	{
		m_localPorts.push_back(make_pair(i->CommsPort, i->Expansion));
	}

	if (m_localPorts.empty())
	{
		controlDisable(PANEL_MAIN, "local_panels");
	}
	else
	{
		controlEnable(PANEL_MAIN, "local_panels");
	}

}

void assign_keys::init()
{
	panelShow(PANEL_MAIN, "main.bncs_ui");

	textPut("instance", rh->GetRingmasterDevices().Composite, PANEL_MAIN, "function_grid");
	textPut("device", rh->GetRingmasterDevices().Main, PANEL_MAIN, "function_groups");
	textPut("device", rh->GetRingmasterDevices().Main, PANEL_MAIN, "function_pages");
	textPut("device.map", rh->GetRingmasterDevices().Main, PANEL_MAIN, "function_groups");
	textPut("device.map", rh->GetRingmasterDevices().Main, PANEL_MAIN, "function_pages");

	bncs_string stab;
	lc->getValue("ringmaster.key_assign_mode", stab);

	m_tabSelected = (tab)stab.toInt();
	SetTab();

	//Get the current groups
	bncs_string fgroup, fpage;
	//Can add instance to the function a different cache for each function
	lc->getValue(STORE_FUNC + "group", fgroup);
	lc->getValue(STORE_FUNC + "page", fpage);
	m_initialPageFunction = fpage; 

	if(fgroup.toInt() > 0)
		textPut("button." + fgroup, "released", PANEL_MAIN, "function_groups");

	if (m_initialPageFunction.toInt() > 0)
	{
		timerStart(TIMER_GROUP, TIMEOUT_GROUP);
	}
}

void assign_keys::createPopup(const vector<pair<const comms_ringmaster_port*, int>>& ports, bool ignoreExpansion)
{
	m_popupPorts = ports;

	if (ports.size() == 0) return;

	const int maxButton = 18;
	const int height = 40;
	const int width = 200;
	int x = 0;
	int y = 0;

	auto fullWidth = ((int)(ports.size() / maxButton) + 1) * width;
	auto fullheight = ((ports.size() >= maxButton) ? maxButton : ports.size()) * height;


	panelPopup(POPUP_NAVIGATE, "", fullWidth, fullheight);
	int i = 0;
	for (vector<pair<const comms_ringmaster_port*, int>>::const_iterator it = ports.begin(); it != ports.end(); ++it, i++)
	{
		if ((i + 1) % (maxButton + 1) == 0)
		{
			x += width;
			y = 0;
		}

		bncs_string state = "enum_deselected";
		if (it->first->ring_port == m_panelPort.ring_port && (ignoreExpansion || it->second == m_expansion))
		{
			state = "enum_selected";
		}

		bncs_string text = it->first->sourceName_short;
		if (ignoreExpansion == false && it->second > 0)
		{
			text.append(" Exp ").append(it->second);
		}
		controlCreate(POPUP_NAVIGATE, i, "bncs_control", x, y * height, width, height, bncs_string("text=%1\nstatesheet=%2").arg(text).arg(state));

		y++;
	}
}