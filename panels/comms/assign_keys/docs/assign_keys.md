# assign_keys Component

This component allows the assignment of functions and port onto comms panels.

There are 2 modes of operation of this panel.
- Navigate Adjusting - From XY panel
- Opening from App Bar - Using **ops_positions.xml** **comms_panel_port=ring.port**


## Commands

## Notifications

## Stylesheets
None