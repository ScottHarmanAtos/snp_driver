#ifndef button_mapping_comms_INCLUDED
	#define button_mapping_comms_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class button_mapping_comms : public bncs_script_helper
{
public:
	button_mapping_comms( bncs_client_callback * parent, const char* path );
	virtual ~button_mapping_comms();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );

	riedelHelper *rh;
	
private:
	bncs_string m_panel;

	void selectMapping(bncs_string s = "");
};


#endif // button_mapping_comms_INCLUDED