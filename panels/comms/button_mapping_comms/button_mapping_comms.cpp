#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "button_mapping_comms.h"

const int MAIN(1);
const int SOURCES(0);
const int DEST(1);

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( button_mapping_comms )

// constructor - equivalent to ApplCore STARTUP
button_mapping_comms::button_mapping_comms( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();
	panelShow( MAIN, "button_mapping_comms.bncs_ui" );

	selectMapping();
}

// destructor - equivalent to ApplCore CLOSEDOWN
button_mapping_comms::~button_mapping_comms()
{
}

// all button pushes and notifications come here
void button_mapping_comms::buttonCallback( buttonNotify *b )
{
	//debug("CRIS btnCall id=%1 com=%2 val=%3", b->id(),b->command(),b->value());
	if( b->panel() == MAIN && b->id() != "mapping" )
	{
		if(m_panel != b->id())
			selectMapping(b->id());
	}
}

// all revertives come here
int button_mapping_comms::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void button_mapping_comms::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string button_mapping_comms::parentCallback( parentNotify *p )
{
	
	return "";
}

// timer events come here
void button_mapping_comms::timerCallback( int id )
{
}


void button_mapping_comms::selectMapping(bncs_string s)
{
	textPut("statesheet", "enum_deselected", MAIN, m_panel);
	m_panel = s;
	if (s == "dest_ports")
	{
		textPut("panel", "dest_ports", MAIN, "mapping");
	}
	else if ( s == "functions")
	{
		textPut("panel", "functions", MAIN, "mapping");
	}
	else
	{
		textPut("panel", "source_ports", MAIN, "mapping");
	}

	textPut("statesheet", "enum_selected", MAIN, m_panel);

}