#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_push_ifb_labels.h"

#define PNL_MAIN	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_push_ifb_labels )

// constructor - equivalent to ApplCore STARTUP
comms_push_ifb_labels::comms_push_ifb_labels(bncs_client_callback* parent, const char* path) : bncs_script_helper(parent, path)
{
	rh = riedelHelper::getriedelHelperObject();
	// show a panel from file comms_push_ifb_labels.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PNL_MAIN, "comms_push_ifb_labels.bncs_ui");

	m_onlyEmpty = false;

	auto rings = rh->GetRings();
	for (auto it = rings.cbegin(); it != rings.cend(); ++it)
	{
		infoRegister(it->second.devices.Ifb, 1, riedelHelpers::max::IFBs);
		infoPoll(it->second.devices.Ifb, 1, riedelHelpers::max::IFBs);
	}
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_push_ifb_labels::~comms_push_ifb_labels()
{
}

// all button pushes and notifications come here
void comms_push_ifb_labels::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if (b->id() == "OnlyEmpty")
		{
			m_onlyEmpty = !m_onlyEmpty;
			textPut("statesheet", m_onlyEmpty? "enum_selected":"enum_deselected", PNL_MAIN, "OnlyEmpty");
		}
		else
		{
			//Button pressed do something
			auto rings = rh->GetRings();
			auto it = rings.find(b->id());
			if (it != rings.end())
			{
				for (int i = 1; i <= riedelHelpers::max::IFBs; ++i)
				{
					if (!m_onlyEmpty || m_ifbs[make_pair(it->second.devices.Ifb, i)].length() == 0)
					{
						bncs_string name;
						routerName(it->second.devices.Ifb, riedelHelpers::databases::IFB_Name, i, name);
						if (m_ifbs[make_pair(it->second.devices.Ifb, i)] != name)
						{
							infoWrite(it->second.devices.Ifb, bncs_string("&LABEL=%1").arg(name), i);
						}
					}
				}	
			}
		}
	}
}

// all revertives come here
int comms_push_ifb_labels::revertiveCallback( revertiveNotify * r )
{
	bncs_stringlist sl(r->sInfo(), '|');
	if (sl.count() == 4)
	{
		auto p = make_pair(r->device(), r->index());
		auto it = m_ifbs.find(p);
		if (it != m_ifbs.end())
		{
			m_ifbs[p] = sl[3];
		}
	}

	return 0;
}

// all database name changes come back here
void comms_push_ifb_labels::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_push_ifb_labels::parentCallback( parentNotify *p )
{

	return "";
}

// timer events come here
void comms_push_ifb_labels::timerCallback( int id )
{
}
