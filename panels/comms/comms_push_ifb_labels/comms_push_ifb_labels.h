#ifndef comms_push_ifb_labels_INCLUDED
	#define comms_push_ifb_labels_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_push_ifb_labels : public bncs_script_helper
{
public:
	comms_push_ifb_labels( bncs_client_callback * parent, const char* path );
	virtual ~comms_push_ifb_labels();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	riedelHelper* rh;
	map<pair<int, int>, bncs_string> m_ifbs;
	bool m_onlyEmpty;
};


#endif // comms_push_ifb_labels_INCLUDED