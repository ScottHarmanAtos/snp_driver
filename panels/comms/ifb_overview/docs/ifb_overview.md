# ifb_overview Component

IFB overview display either all IFBs in the system or the IFBs that the workstation is allowed to view.

The panel loads IFB data from **comms_ifbs.xml**, each ifb in this file holds an **edit_permissions** attribute
this will a comma delimited list of ops_areas this have permission to edit the ifb.

If the panel is in **local** mode, it will only show the IFBs that the ops_position has permission too.

If run from a panelset the behavior will be different depending on what is set in the param.
The possible options are **all**, **local** or another ops_area, if blank the local ops_area will be shown.

If run from bncs_vis_ed the panel will show the local values.

The panel views information from the ringmaster, rather then the riedel drivers.

![Example](example.png)

## Stylesheets
None