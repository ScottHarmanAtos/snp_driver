#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "ifb_overview.h"

#define PNL_MAIN	1
#define PNL_FILTER	2

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( ifb_overview )

// constructor - equivalent to ApplCore STARTUP
ifb_overview::ifb_overview( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	// show a panel from file ifb_overview.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "ifb_overview.bncs_ui" );

	controlDisable(PNL_MAIN, "edit_selected");

	//Wait for the raw parameter, if we don't get one, show call this
	timerStart(1, 10);
}

// destructor - equivalent to ApplCore CLOSEDOWN
ifb_overview::~ifb_overview()
{
}

// all button pushes and notifications come here
void ifb_overview::buttonCallback( buttonNotify *b )
{
	if (b->panel() == PNL_MAIN)
	{
		if (b->id() == "list")
		{
			if (m_selectedTag.length() == 0)
			{
				controlEnable(PNL_MAIN, "edit_selected");
			}

			auto ifb = m_ifbs.find(b->value());
			if (ifb != m_ifbs.end())
			{
				m_selectedTag = ifb->second.RingPort;
				//navigateExecute(bncs_string("_components/comms/comms_edit_ifb,%1").arg(ifb->second.Ring));
			}		
		}
		else if (b->id() == "edit_selected")
		{
			navigateExecute(bncs_string("_components/comms/comms_edit_ifb,%1").arg(m_selectedTag));
		}
		else if (b->id() == "edit_filter")
		{
			panelPopup(PNL_FILTER, "popup_keyboard.bncs_ui");
			bncs_string s;
			textGet("text", PNL_MAIN, "filter_text", s);
			textPut("text", s, PNL_FILTER, "keyboard");
		}
	}
	else if (b->panel() == PNL_FILTER)
	{
		bncs_string s;
		if (b->id() == "close")
		{
			s = "";
		}
		else if (b->id() == "keyboard")
		{
			textGet("text", PNL_FILTER, "keyboard", s);
		}
		panelDestroy(PNL_FILTER);

		if (s.length() > 0)
		{
			textPut("statesheet", "enum_selected", PNL_MAIN, "edit_filter");		
		}
		else
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, "edit_filter");
		}
		textPut("text", s, PNL_MAIN, "filter_text");
		filter(s);
	}
}

// all revertives come here
int ifb_overview::revertiveCallback( revertiveNotify * r )
{
	if (r->index() <= riedelHelpers::max::IFBs)
	{
		auto it = m_device_ring.find(r->device());
		if (it != m_device_ring.end())
		{
			bncs_string ringport = bncs_string("%1_%2").arg(it->second).arg(r->index());
			auto ifb = m_ifbs.find(ringport);
			if (ifb != m_ifbs.end())
			{
				bncs_stringlist sl(r->sInfo(), '|');
				bncs_stringlist inputsL(sl[0], ',');
				bncs_stringlist mixminusL(sl[1], ',');
				bncs_stringlist outputsL(sl[2], ',');
				bncs_string label = sl[3];

				bncs_stringlist inputsSL;
				for (int i = 0; i < inputsL.count(); ++i)
				{
					auto p = rh->GetPort(inputsL[0]);
					inputsSL.append(bncs_string("%1 %2 ").arg(inputsL[0]).arg(p.sourceName_long));
				}
				bncs_string inputs = inputsSL.toString();

				if (ifb->second.Input != inputs)
				{
					ifb->second.Input = inputs;
					if(ifb->second.Shown) textPut(bncs_string("cellTextTag.%1.%2").arg(ringport).arg((int)Inputs), ifb->second.Input, PNL_MAIN, "list");
				}

				bncs_stringlist mixminusSL;
				for (int i = 0; i < mixminusL.count(); ++i)
				{
					auto p = rh->GetPort(mixminusL[0]);
					mixminusSL.append(bncs_string("%1 %2 ").arg(mixminusL[0]).arg(p.sourceName_long));
				}
				bncs_string mixminus = mixminusSL.toString();

				if (ifb->second.Mixminus != mixminus)
				{
					ifb->second.Mixminus = mixminus;
					if (ifb->second.Shown) textPut(bncs_string("cellTextTag.%1.%2").arg(ringport).arg((int)MixMinus), ifb->second.Mixminus, PNL_MAIN, "list");
				}

				bncs_stringlist outputsSL;
				for (int i = 0; i < outputsL.count(); ++i)
				{
					auto p = rh->GetPort(outputsL[0]);
					outputsSL.append(bncs_string("%1 %2 ").arg(outputsL[0]).arg(p.destName_long));
				}
				bncs_string outputs = outputsSL.toString();

				if (ifb->second.Output != outputs)
				{
					ifb->second.Output = outputs;
					if (ifb->second.Shown) textPut(bncs_string("cellTextTag.%1.%2").arg(ringport).arg((int)Outputs), ifb->second.Output, PNL_MAIN, "list");
				}
				if (ifb->second.Label != label)
				{
					ifb->second.Label = label;
					if (ifb->second.Shown) textPut(bncs_string("cellTextTag.%1.%2").arg(ringport).arg((int)Label), ifb->second.Label, PNL_MAIN, "list");
				}
			}
		}
	}

	return 0;
}

// all database name changes come back here
void ifb_overview::databaseCallback(revertiveNotify* r)
{
	if (r->index() <= riedelHelpers::max::IFBs)
	{
		auto it = m_device_ring.find(r->device());
		if (it != m_device_ring.end())
		{
			bncs_string ringport = bncs_string("%1_%2").arg(it->second).arg(r->index());
			auto ifb = m_ifbs.find(ringport);
			if (ifb != m_ifbs.end())
			{
				if (r->database() == riedelHelpers::databases::Subtitle_Input)
				{
					ifb->second.Subtitle = r->sInfo();
					if (ifb->second.Shown) textPut(bncs_string("cellTextTag.%1.%2").arg(ringport).arg((int)Subtitle), ifb->second.Subtitle, PNL_MAIN, "list");
				}
				else if (r->database() == riedelHelpers::databases::Input_Long_Name)
				{
					ifb->second.Name = r->sInfo();
					if (ifb->second.Shown) textPut(bncs_string("cellTextTag.%1.%2").arg(ringport).arg((int)Name), ifb->second.Name, PNL_MAIN, "list");
				}
			}
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string ifb_overview::parentCallback( parentNotify *p )
{
	if (p->command() == "raw_parameters")
	{
		timerStop(1);
		if (p->value() == "all") init(true, "all");
		else if (p->value() == "local" || p->value().length() == 0)
		{
			auto area = bncs_config(bncs_string("workstation_settings.%1.ops_area").arg(workstation())).attr("value");
			init(false, area);
		}
		else
		{
			//Assume an area has been passed in:
			init(false, p->value());
		}
	}
	return "";
}

// timer events come here
void ifb_overview::timerCallback( int id )
{
	if (id == 1)
	{
		auto area = bncs_config(bncs_string("workstation_settings.%1.ops_area").arg(workstation())).attr("value");
		init(false, area);
	}
	timerStop(id);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
void ifb_overview::init(bool showAll, bncs_string area)
{
	textPut("clear", PNL_MAIN, "list");
	m_device_ring.clear();
	m_ifbs.clear();

	infoUnregister(0);

	textPut("text", area, PNL_MAIN, "lbl_ops_area");

	//Get the Ringmaster IFB Devices
	auto rmd = rh->GetRingmasterDevices();
	bncs_config rs("ring_master_config.RIEDEL_SYSTEMS");
	while (rs.isChildValid())
	{
		//Riedel_1
		bncs_string _, num;
		rs.childAttr("id").split('_', _, num);

		auto id = rs.childAttr("id").toInt();
		//"trunk_address=11,base_instance=c_riedel_london_01,name=LDC_01" 
		bncs_stringlist sl(rs.childAttr("value"));
		auto trunk = sl.getNamedParam("trunk_address");
		bncs_string inst;
		instanceLookupComposite((const char*)rmd.Composite, (const char*)bncs_string("ifbs_%1").arg(num), inst);
		int device;
		getDev(inst, &device);
		m_device_ring[device] = trunk;
		rs.nextChild();
	}

	bncs_config c("comms_ifbs");
	//Loop through ring
	while (c.isChildValid())
	{
		bncs_config r(c);
		r.drillDown();

		bncs_string ring = r.attr("id");

		int device = 0;
		for (auto it = m_device_ring.begin(); it != m_device_ring.end(); ++it)
		{
			if (it->second == ring)
			{
				device = it->first;
				break;
			}
		}

		auto rg = rh->GetRing(ring.toInt());

		textPut("enableSorting", "false", PNL_MAIN, "list");

		bool atLeastOneItem = false;
		while (r.isChildValid())
		{
			bncs_string ifb = r.childAttr("id");
			bncs_string edit_permissions = r.childAttr("edit_permissions");

			if (showAll || edit_permissions.find(area) > -1)
			{
				bncs_string sub;
				routerName(rg.devices.Ifb, riedelHelpers::databases::Subtitle_Input, ifb, sub);
				bncs_string name;
				routerName(rg.devices.Ifb, riedelHelpers::databases::Input_Long_Name, ifb, name);
				auto ringport = bncs_string("%1_%2").arg(ring).arg(ifb);
				m_ifbs[ringport].Subtitle = sub;
				m_ifbs[ringport].Name = name;
				m_ifbs[ringport].RingPort = bncs_string("%1.%2").arg(ring).arg(ifb);
				m_ifbs[ringport].Shown = true;

				textPut("add",bncs_string("%1;;;;;%2;%3;#TAG=%4").arg(m_ifbs[ringport].RingPort).arg(sub).arg(name).arg(ringport), PNL_MAIN, "list");

				if (!showAll)
				{
					infoRegister(device, ifb, ifb, true);
					infoPoll(device, ifb, ifb);
				}
				atLeastOneItem = true;
			}

			r.nextChild();
		}

		textPut("enableSorting", "true", PNL_MAIN, "list");

		//For subtitles database callbacks
		if (atLeastOneItem)
		{
			infoRegister(rg.devices.Ifb, 4096, 4096);
		}

		if (showAll)
		{
			//Poll all of them
			infoRegister(device, 1, riedelHelpers::max::IFBs);
			infoPoll(device, 1, riedelHelpers::max::IFBs);
		}

		c.nextChild();
	}
}

void ifb_overview::filter(bncs_string s)
{
	bool isEmpty = s.length() == 0;

	for (auto it = m_ifbs.begin(); it != m_ifbs.end(); ++it)
	{
		IFB& a = it->second;

		if (isEmpty || a.RingPort.find(s) > -1 || a.Label.find(s) > -1 || a.Name.find(s) > -1 || a.Mixminus.find(s) > -1 || a.Output.find(s) > -1 || a.Input.find(s) > -1 || a.Subtitle.find(s) > -1)
		{
			if (a.Shown == true) continue;
			//Add if not shown
			debug("selected: show %1", a.RingPort);
			textPut("add", bncs_string("%1;%2;%3;%4;%5;%6;%7;#TAG=%8").arg(a.RingPort).arg(a.Label).arg(a.Input).arg(a.Mixminus).arg(a.Output).arg(a.Subtitle).arg(a.Name).arg(it->first), PNL_MAIN, "list");
			a.Shown = true;
		}
		else
		{
			if (a.Shown == true)
			{
				debug("selected: remove %1", a.RingPort);
				a.Shown = false;
				textPut("removeTag", it->first, PNL_MAIN, "list");
			}
		}
	}
}