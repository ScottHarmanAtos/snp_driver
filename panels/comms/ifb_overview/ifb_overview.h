#ifndef ifb_overview_INCLUDED
	#define ifb_overview_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h";
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class ifb_overview : public bncs_script_helper
{
public:
	ifb_overview( bncs_client_callback * parent, const char* path );
	virtual ~ifb_overview();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	
	riedelHelper* rh;
	void init(bool, bncs_string area);
	void filter(bncs_string s);

	enum Position
	{
		RingPort,
		Label,
		Inputs,
		MixMinus,
		Outputs,
		Subtitle,
		Name
	};

	struct IFB
	{
		bncs_string RingPort;
		bncs_string Name;
		bncs_string Subtitle;
		bncs_string Input;
		bncs_string Mixminus;
		bncs_string Output;
		bncs_string Label;
		bool Shown;
	};

	map<int, int> m_device_ring;
	map<bncs_string, IFB> m_ifbs;

	bncs_string m_selectedTag;
};


#endif // ifb_overview_INCLUDED