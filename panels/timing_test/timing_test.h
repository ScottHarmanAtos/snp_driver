#ifndef timing_test_INCLUDED
	#define timing_test_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class timing_test : public bncs_script_helper
{
public:
	timing_test( bncs_client_callback * parent, const char* path );
	virtual ~timing_test();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	struct device
	{
		int dev;
		bncs_string instance;
		bncs_dateTime pollStart;
		bncs_dateTime pollEnd;
		__int64 pollDur;
		bncs_dateTime writeStart;
		bncs_dateTime writeEnd;
		__int64 writeDur;
	};

	bncs_string m_instance;
	device info;
	device router;

};


#endif // timing_test_INCLUDED