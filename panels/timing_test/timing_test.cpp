#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <ctime>
#include <bncs_datetime.h>

#include "timing_test.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( timing_test )

// constructor - equivalent to ApplCore STARTUP
timing_test::timing_test( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "default.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
timing_test::~timing_test()
{
}

// all button pushes and notifications come here
void timing_test::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		bncs_string t, c;
		b->id().split('_', t, c);
		//debug("timing_test::t %1, c %2", t, c);
		if (t == "info")
		{
			if (c == "poll")
			{
				textPut("text", "", PNL_MAIN, "info_poll_start");
				textPut("text", "", PNL_MAIN, "info_poll_end");
				textPut("text", "", PNL_MAIN, "info_poll_dur");
				info.pollStart = bncs_dateTime::nowUTC();
				infoPoll(info.dev, 1, 1);
			}
			else if (c == "write")
			{
				textPut("text", "", PNL_MAIN, "info_write_start");
				textPut("text", "", PNL_MAIN, "info_write_end");
				textPut("text", "", PNL_MAIN, "info_write_dur");
				info.writeStart = bncs_dateTime::nowUTC();
				infoWrite(info.dev, info.writeStart.toString(), 2, true);
			}
		}
		else if (t == "router")
		{
			if (c == "poll")
			{
				textPut("text", "", PNL_MAIN, "router_poll_start");
				textPut("text", "", PNL_MAIN, "router_poll_end");
				textPut("text", "", PNL_MAIN, "router_poll_dur");
				router.pollStart = bncs_dateTime::nowUTC();
				routerPoll(router.dev, 1, 1);
			}
			else if (c == "write")
			{
				textPut("text", "", PNL_MAIN, "router_write_start");
				textPut("text", "", PNL_MAIN, "router_write_end");
				textPut("text", "", PNL_MAIN, "router_write_dur");
				router.writeStart = bncs_dateTime::nowUTC();
				routerCrosspoint(router.dev, bncs_string(router.writeStart.toString("xxx")).firstInt(), 2, true);
			}
		}
	}
}

// all revertives come here
int timing_test::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == info.dev)
	{
		switch (r->index())
		{
		case 1:
			info.pollEnd = bncs_dateTime::nowUTC();
			info.pollDur = info.pollEnd.difference(info.pollStart) / 10;
			textPut("text", info.pollStart.toString("ss.zzz"), PNL_MAIN, "info_poll_start");
			textPut("text", info.pollEnd.toString("ss.zzz"), PNL_MAIN, "info_poll_end");
			textPut("text", bncs_string("%1us").arg((long)info.pollDur), PNL_MAIN, "info_poll_dur");
			break;

		case 2:
			info.writeEnd = bncs_dateTime::nowUTC();
			info.writeDur = info.writeEnd.difference(info.writeStart) / 10;
			textPut("text", info.writeStart.toString("ss.zzz"), PNL_MAIN, "info_write_start");
			textPut("text", info.writeEnd.toString("ss.zzz"), PNL_MAIN, "info_write_end");
			textPut("text", bncs_string("%1us").arg((long)info.writeDur), PNL_MAIN, "info_write_dur");
			break;

		default:
			break;
		}
	}
	else if (r->device() == router.dev)
	{
		switch (r->index())
		{
		case 1:
			router.pollEnd = bncs_dateTime::nowUTC();
			router.pollDur = router.pollEnd.difference(router.pollStart) / 10;
			textPut("text", router.pollStart.toString("ss.zzz"), PNL_MAIN, "router_poll_start");
			textPut("text", router.pollEnd.toString("ss.zzz"), PNL_MAIN, "router_poll_end");
			textPut("text", bncs_string("%1us").arg((long)router.pollDur), PNL_MAIN, "router_poll_dur");
			break;

		case 2:
			router.writeEnd = bncs_dateTime::nowUTC();
			router.writeDur = router.writeEnd.difference(router.writeStart) / 10;
			textPut("text", router.writeStart.toString("ss.zzz"), PNL_MAIN, "router_write_start");
			textPut("text", router.writeEnd.toString("ss.zzz"), PNL_MAIN, "router_write_end");
			textPut("text", bncs_string("%1us").arg((long)router.writeDur), PNL_MAIN, "router_write_dur");
			break;

		default:
			break;
		}
	}

	return 0;
}

// all database name changes come back here
void timing_test::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string timing_test::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			return sl.toString( '\n' );
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
		info.instance = bncs_config(bncs_string("instances.%1.info").arg(m_instance)).attr("instance");
		getDev(info.instance, &info.dev);
		debug("timing_test::info dev %1", info.dev);
		infoRegister(info.dev, 1, 2);

		router.instance = bncs_config(bncs_string("instances.%1.router").arg(m_instance)).attr("instance");
		getDev(router.instance, &router.dev);
		debug("timing_test::info dev %1", router.dev);
		routerRegister(router.dev, 1, 2);
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void timing_test::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


