#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "status_bar.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( status_bar )

// constructor - equivalent to ApplCore STARTUP
status_bar::status_bar( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{

	m_status = getWorkstationSetting("status_bar");
	LoadLayout(m_status);
}

// destructor - equivalent to ApplCore CLOSEDOWN
status_bar::~status_bar()
{
}

// all button pushes and notifications come here
void status_bar::buttonCallback( buttonNotify *b )
{

}

// all revertives come here
int status_bar::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void status_bar::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string status_bar::parentCallback( parentNotify *p )
{
	p->dump("status_bar::parentCallback()");
	if (p->command() == "return")
	{
		bncs_stringlist sl;
		sl << bncs_string("workstation=%1").arg(m_status);
		return sl.toString('\n');
	}
	else if( p->command() == "workstation")
	{	// Our instance is being set/changed
		if (m_status != p->value())
		{
			m_status = p->value();
			LoadLayout(p->value());
		}
	}

	return "";
}

// timer events come here
void status_bar::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void status_bar::LoadLayout(bncs_string statusbar)
{
	debug("status_bar::LoadLayout() statusbar=%1", statusbar);
	if (panelShow(1, bncs_string("layout/%1.bncs_ui").arg(statusbar)))
	{
		panelShow(1, bncs_string("unknown.bncs_ui"));
	}
}
