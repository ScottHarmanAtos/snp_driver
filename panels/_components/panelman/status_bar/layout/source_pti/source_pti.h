#ifndef source_pti_INCLUDED
	#define source_pti_INCLUDED

#include <bncs_script_helper.h>
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class source_pti : public bncs_script_helper
{
public:
	source_pti( bncs_client_callback * parent, const char* path );
	virtual ~source_pti();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	int m_iDevice;
	int m_iDestRouterSize;
	bncs_string m_sCurrentSource;

	bncs_string m_sDatabaseSourceNames;
	bncs_string m_sDatabaseDestNames;

	map<int, int> m_mapDestSource;
	map<int, int>::iterator m_mapITDestSource;

	int m_sPopupSourceSelection_source;

	void init(void);

	void updateListView(bncs_string);

	
};


#endif // source_pti_INCLUDED