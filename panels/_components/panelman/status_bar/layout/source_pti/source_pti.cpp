#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "source_pti.h"


#define PANEL_MAIN					1
#define PANEL_POPUP_SELECT_SOURCE	2

#define TIMER_SETUP					1
#define TIMER_DISPLAY				2
#define TIMER_DISPLAY_DURATION		100

#define CTRL_LIST_VIEW				"listView"
#define CTRL_BUTTON					"button"

#define DEFAULT_DB_SOURCE_NAMES		"0"
#define DEFAULT_DB_DEST_NAMES		"1"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( source_pti )

// constructor - equivalent to ApplCore STARTUP
source_pti::source_pti( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file source_pti.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PANEL_MAIN, "source_pti.bncs_ui");

	// Defaults
	m_sDatabaseSourceNames = DEFAULT_DB_SOURCE_NAMES;
	m_sDatabaseDestNames = DEFAULT_DB_DEST_NAMES;
	m_iDevice = -1;
	m_iDestRouterSize = 0;
	m_sCurrentSource = 1;

	m_sPopupSourceSelection_source = m_sCurrentSource;

	// Sort the list view
	textPut("sort", "1;true", PANEL_MAIN, CTRL_LIST_VIEW);

}

// destructor - equivalent to ApplCore CLOSEDOWN
source_pti::~source_pti()
{
}

// all button pushes and notifications come here
void source_pti::buttonCallback(buttonNotify *b)
{
	if (b->panel() == PANEL_MAIN)
	{
		debug("CNTRL=%1", b->id());
		if (b->id() == "button")
		{
			// Popup the source grid
			panelPopup(PANEL_POPUP_SELECT_SOURCE, "popup_select_source.bncs_ui");

			// Set the instance for the pop up
			textPut("instance", m_instance, PANEL_POPUP_SELECT_SOURCE, "sourceGroup");
			textPut("instance.map", m_instance, PANEL_POPUP_SELECT_SOURCE, "sourceGroup");

			textPut("instance", m_instance, PANEL_POPUP_SELECT_SOURCE, "sourceGrid");
			textPut("instance.map", m_instance, PANEL_POPUP_SELECT_SOURCE, "sourceGrid");

			textPut("instance", m_instance, PANEL_POPUP_SELECT_SOURCE, "sourcePage");
			textPut("instance.map", m_instance, PANEL_POPUP_SELECT_SOURCE, "sourcePage");
		}
	}
	else if (b->panel() == PANEL_POPUP_SELECT_SOURCE)
	{
		if (b->command() == "source_index")
		{
			m_sPopupSourceSelection_source = b->value();
		}

		// Look for close/accept/select button
		else if (b->command() == "select")
		{
			if (b->value() == "released")
			{
				panelDestroy(PANEL_POPUP_SELECT_SOURCE);

				m_sCurrentSource = m_sPopupSourceSelection_source;
				updateListView(m_sCurrentSource);
			}
		}
	}
}

// all revertives come here
int source_pti::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_iDevice)
	{
		if (r->index() <= m_iDestRouterSize)
		{

			//r->dump("\nsource_pti::");

			bncs_stringlist slstsData = bncs_stringlist(r->sInfo(), ',');
			bncs_string sRoutedSource = slstsData.getNamedParam("index").toInt();
			bncs_string sBackupSource = slstsData.getNamedParam("buindex").toInt();
			bncs_string sStatusSource = slstsData.getNamedParam("status").toInt();

			bncs_string sRoutedDest = r->index();

			// Check to see if this dest is in the map !
			if (m_mapDestSource.count(sRoutedDest) == 0)
			{
				// Add dest/source pair
				m_mapDestSource.insert(std::pair<int, int>(sRoutedDest, sRoutedSource));
			}
			else
			{
				// update dest/source pair
				m_mapDestSource[sRoutedDest] = sRoutedSource;
			}

			// Restart timer for list box update
			timerStop(TIMER_DISPLAY);
			timerStart(TIMER_DISPLAY, TIMER_DISPLAY_DURATION);
		}
	}

	return 0;
}

// all database name changes come back here
void source_pti::databaseCallback( revertiveNotify * r )
{
	// Database has changed so update the list view box - potentially changed dest names
	if ((r->device() == m_iDevice) && (r->database() == m_sDatabaseDestNames))
	{
		// Restart timer for list box update
		timerStop(TIMER_DISPLAY);
		timerStart(TIMER_DISPLAY, TIMER_DISPLAY_DURATION);
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string source_pti::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("database_source_names=%1").arg(m_sDatabaseSourceNames);
			sl << bncs_string("database_dest_names=%1").arg(m_sDatabaseDestNames);

			return sl.toString( '\n' );
		}

		else if (p->value() == "database_source_names")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_sDatabaseSourceNames));
		}
		else if (p->value() == "database_dest_names")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_sDatabaseDestNames));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		init();
		//Do something instance-change related here
	}

	else if (p->command() == "database_source_names")
	{	// Persisted value or 'Command' being set here
		m_sDatabaseSourceNames = p->value();
	}
	else if (p->command() == "database_dest_names")
	{	// Persisted value or 'Command' being set here
		m_sDatabaseDestNames = p->value();
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "database_source_names=[value]";
		sl << "database_dest_names=[value]";

		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void source_pti::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;
	case TIMER_DISPLAY:
		timerStop(id);
		updateListView(m_sCurrentSource);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void source_pti::init(void)
{
	// Get source device 
	getDev(m_instance, &m_iDevice);
	
	// get size of database
	m_iDestRouterSize = getRouterSize(m_iDevice, m_sDatabaseDestNames);

	// Unregister previous registrations
	infoUnregister(m_iDevice);

	// Register with all the router destinations for callbacks
	infoRegister(m_iDevice, 1, m_iDestRouterSize, true);
	infoPoll(m_iDevice, 1, m_iDestRouterSize);


}

void source_pti::updateListView(bncs_string sSelectedSource)
{
	// Start updating the source list when we have all the destinations covered
	bncs_stringlist slstCollection;

	// Clear the listView as source has changed
	textPut("clear", "", PANEL_MAIN, CTRL_LIST_VIEW);
	textPut("sort", "false", PANEL_MAIN, CTRL_LIST_VIEW);

	// Get all dests related to the current source
	bncs_string sDestName = "";

	// Iterate over the map using Iterator till end.
	m_mapITDestSource = m_mapDestSource.begin();

	while (m_mapITDestSource != m_mapDestSource.end())
	{
		if (m_mapITDestSource->second == sSelectedSource.toInt())
		{

			routerName(m_iDevice, m_sDatabaseDestNames, m_mapITDestSource->first, sDestName);
			textPut("add", bncs_string("%1").arg(sDestName), PANEL_MAIN, CTRL_LIST_VIEW);
		}
		++m_mapITDestSource;
	}

	// Update caption on button
	// Clear the listView as source has changed
	bncs_string sSourceName = "";
	routerName(m_iDevice, m_sDatabaseSourceNames, sSelectedSource, sSourceName);


	textPut("text", sSourceName, PANEL_MAIN, CTRL_BUTTON);
}