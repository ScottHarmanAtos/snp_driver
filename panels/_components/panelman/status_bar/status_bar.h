#ifndef status_bar_INCLUDED
	#define status_bar_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class status_bar : public bncs_script_helper
{
public:
	status_bar( bncs_client_callback * parent, const char* path );
	virtual ~status_bar();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	void LoadLayout(bncs_string workstation);
	bncs_string m_status;
	
};


#endif // status_bar_INCLUDED