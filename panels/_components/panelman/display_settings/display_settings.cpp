#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "display_settings.h"

#define PANEL_MAIN 1

#define PARAMCOUNT 2

#define FIELD_1  0
#define FIELD_2  1

#define TIMER_INIT  1
#define TIMEOUT_INIT 10

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( display_settings )

/*******************************************************
Setting options:

The "file" parameter is decoded 3 ways
	1. Begins with 'workstation'
		Used for 'workstations.xml' or 'workstation_settings.xml'
		The 'setting' parameter is accessed directly as its value

			<workstation id="5">
				<setting id="status_bar" value="tx_05_playout_interim" />
			</workstation>

		with 'setting=status_bar' the display would read 'tx_05_playout_interim'

	2. Begins with 'object'
		Used for 'object_settings.xml'
		The 'setting' parameter required two comma dliminated values
		The first value is the 'object' id, the second value is the 'setting' id

			<object id="TraceBar">
				<setting id="Index_001" value="1" />
			</object>

		with 'setting=TraceBar,Index_001' the display would read '1'

	3. Begins with 'ops'
		Used for 'ops_areas.xml' and 'ops_positions.xml'
		The child tag is the singular of the filename so the last character is dropped to locate the workstation area or position in workstation_settings.xml

			ops_areas > ops_area
			ops_positions > ops_position

		The 'setting' parameter requires two comma deliminated values
		The first value is the setting 'id', the second value is the tag to be identified

			<position id="ingest_desk_2">
				<setting id="mon_dest" name="mon" from="17052/RTR/001" dest="464" />
				<value id="comms_panel_port" value="11.3"/>
			</position>

		Given the workstation is in position 'ingest_desk_2', with 'setting=mon_dest,dest' the display would read '464'

*******************************************************/


// constructor - equivalent to ApplCore STARTUP
display_settings::display_settings( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "display_settings.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
	setSize( PANEL_MAIN );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
display_settings::~display_settings()
{
}

// all button pushes and notifications come here
void display_settings::buttonCallback( buttonNotify *b )
{
}

// all revertives come here
int display_settings::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void display_settings::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string display_settings::parentCallback( parentNotify *p )
{
	if (p->command() == "file")
	{
		m_strFileName = p->value().lower();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "setting")
	{
		m_strSetting = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "style")
	{
		m_strStyle = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "return")
	{
		bncs_stringlist slt;
		slt << bncs_string("file=%1").arg(m_strFileName);
		slt << bncs_string("setting=%1").arg(m_strSetting);
		slt << bncs_string("style=%1").arg(m_strStyle);
		return slt.toString('\n');

	}
	return "";
}

// timer events come here
void display_settings::timerCallback( int id )
{
	if (id == TIMER_INIT)
	{
		timerStop(TIMER_INIT);
		init();

	}

}

void display_settings::init()
{

	debug("display_settings::init()::m_strFileName=%1 m_strSetting=%2",m_strFileName, m_strSetting);

	m_strDisplay = "BAD CONFIG";

	if (m_strFileName.startsWith("workstation"))
	{
		m_strDisplay = getWorkstationSetting(m_strSetting);
	}
	else if (m_strFileName.startsWith("object"))
	{
		bncs_stringlist sltObjectSetting = bncs_stringlist(m_strSetting);
		if (sltObjectSetting.count() == PARAMCOUNT)
		{
			m_strDisplay = getObjectSetting(sltObjectSetting[FIELD_1], sltObjectSetting[FIELD_2]);	
		}
	}
	else if (m_strFileName.startsWith("ops"))
	{
		bncs_string strOpsFilename = getWorkstationSetting(m_strFileName.left(m_strFileName.length()-1));
		if (bncs_config(bncs_string("%1.%2").arg(m_strFileName).arg(strOpsFilename)).isValid())
		{
			bncs_stringlist sltOpsSetting = bncs_stringlist(m_strSetting);
			if (sltOpsSetting.count() == PARAMCOUNT)
			{
				debug("display_settings::init()::ops_*:looking for <%1>", bncs_string("%1.%2.%3.%4").arg(m_strFileName).arg(strOpsFilename).arg(sltOpsSetting[FIELD_1]).arg(sltOpsSetting[FIELD_2]));

				m_strDisplay = bncs_config(bncs_string("%1.%2.%3").arg(m_strFileName).arg(strOpsFilename).arg(sltOpsSetting[FIELD_1])).attr(sltOpsSetting[FIELD_2]);
			}
		}
	}

	textPut("text", m_strDisplay, PANEL_MAIN, "label");
	textPut("stylesheet", m_strStyle, PANEL_MAIN, "label");
}
