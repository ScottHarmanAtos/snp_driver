#ifndef display_settings_INCLUDED
	#define display_settings_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class display_settings : public bncs_script_helper
{
public:
	display_settings( bncs_client_callback * parent, const char* path );
	virtual ~display_settings();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void init();

	bncs_string m_strObjectSetting;
	bncs_string m_strFileName;
	bncs_string m_sltObjectSetting;
	bncs_string m_strSetting;
	bncs_string m_strDisplay;
	bncs_string m_strStyle;
};


#endif // display_settings_INCLUDED