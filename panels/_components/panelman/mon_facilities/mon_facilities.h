#ifndef mon_facilities_INCLUDED
	#define mon_facilities_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class mon_facilities : public bncs_script_helper
{
public:
	mon_facilities( bncs_client_callback * parent, const char* path );
	virtual ~mon_facilities();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_layout;
	bncs_string m_instance;

	void loadLayout();
	
};


#endif // mon_facilities_INCLUDED