#ifndef chain_mfr_assign_INCLUDED
	#define chain_mfr_assign_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class chain_mfr_assign : public bncs_script_helper
{
public:
	chain_mfr_assign( bncs_client_callback * parent, const char* path );
	virtual ~chain_mfr_assign();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;

	void initChain();
};


#endif // chain_mfr_assign_INCLUDED