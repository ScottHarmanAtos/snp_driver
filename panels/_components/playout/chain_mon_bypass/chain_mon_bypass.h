#ifndef chain_mon_bypass_INCLUDED
	#define chain_mon_bypass_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class chain_mon_bypass : public bncs_script_helper
{
public:
	chain_mon_bypass( bncs_client_callback * parent, const char* path );
	virtual ~chain_mon_bypass();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_chainDevice;
	bncs_string m_instance;
	bncs_string m_deviceLabel;
	bncs_string m_layout;

	int m_deviceChainRouter;
	int m_deviceTielineGRD;
	int m_deviceTielineInfo;
	int m_role;
	int m_monDestPvw;
	int m_monSource;
	int m_monSourceHD;
	int m_monSourceSD;
	int m_monDestTally;
	int m_extDest;

	int m_sourceActiveMain;
	int m_destChainOutMain;
	int m_destChainBypassMain;
	
	int m_sourceActiveBackup;
	int m_destChainOutBackup;
	int m_destChainBypassBackup;

	int m_destMixPGM;

	int m_sourceActive;
	bool m_isSourcePreset;
	int m_sourceBypass;
	int m_destChainOut;
	int m_destChainOut1;
	int m_destChainOut2;
	int m_destChainBypass;
	int m_chainDestTally;
	int m_bypassDestTally;

	bool m_devicePair;

	void backup();
	void bypass();
	void loadChain();
	void loadDevice();	
	void loadDevicePair();
	void main();
	void monitor();
	void monitorHD();
	void monitorSD();
	void select();
	void updateMonDest(bncs_string monDestTally);
	void updateBypass();
	void updateSelect(int source);
	
};


#endif // chain_mon_bypass_INCLUDED