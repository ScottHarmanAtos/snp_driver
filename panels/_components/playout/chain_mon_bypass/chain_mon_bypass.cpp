#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "chain_mon_bypass.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define LAYOUT_DEFAULT	"main.bncs_ui"

#define BTN_MON		"mon"
#define BTN_MON_HD	"mon_hd"
#define BTN_MON_SD	"mon_sd"
#define BTN_BYPASS	"bypass"
#define BTN_MAIN	"main"
#define BTN_BACKUP	"backup"
#define BTN_SELECT	"select"


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( chain_mon_bypass )

// constructor - equivalent to ApplCore STARTUP
chain_mon_bypass::chain_mon_bypass( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_role = 0;
	m_monDestPvw = 0;
	m_monSource = 0;
	m_monDestTally = 0;
	m_chainDestTally = 0;
	m_bypassDestTally = 0;
	m_extDest = 0;
	m_monSourceHD = 0;
	m_monSourceSD = 0;

	m_sourceActive = 0;
	m_sourceBypass = 0;
	m_destChainOut = 0;
	m_destChainOut1 = 0;
	m_destChainOut2 = 0;
	m_destChainBypass = 0;

	m_sourceActiveMain = 0;
	m_destChainOutMain = 0;
	m_destChainBypassMain = 0;
	m_sourceActiveBackup = 0;
	m_destChainOutBackup = 0;
	m_destChainBypassBackup = 0;

	m_destMixPGM = 0;

	m_devicePair = false;

	getDev("tieline_manager_grd", &m_deviceTielineGRD);
	getDev("tieline_manager_info", &m_deviceTielineInfo);

	m_deviceChainRouter = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PNL_MAIN, LAYOUT_DEFAULT);

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
chain_mon_bypass::~chain_mon_bypass()
{
}

// all button pushes and notifications come here
void chain_mon_bypass::buttonCallback( buttonNotify *b )
{
	if (b->panel() == PNL_MAIN)
	{
		if (b->id() == BTN_MON)
		{
			monitor();
		}
		if (b->id() == BTN_MON_HD)
		{
			monitorHD();
		}
		if (b->id() == BTN_MON_SD)
		{
			monitorSD();
		}
		else if (b->id() == BTN_BYPASS)
		{
			bypass();
		}
		else if (b->id() == BTN_MAIN)
		{
			main();
		}
		else if (b->id() == BTN_BACKUP)
		{
			backup();
		}
		else if (b->id() == BTN_SELECT)
		{
			select();
		}

	}
}

// all revertives come here
int chain_mon_bypass::revertiveCallback( revertiveNotify * r )
{
	//r->dump("chain_mon_bypass::revertiveCallback()");	
	if (r->device() == m_deviceTielineInfo && r->index() == m_monDestPvw)
	{
		updateMonDest(r->sInfo());
	}
	else if (r->device() == m_deviceChainRouter)
	{
		if (r->index() == m_destChainOut)
		{
			m_chainDestTally = r->info();
			updateBypass();
		}
		else if (r->index() == m_destChainBypass)
		{
			m_bypassDestTally = r->info();
			updateBypass();
		}
		else if (r->index() == m_destMixPGM)
		{
			updateSelect(r->info());
		}
	}
	return 0;
}

// all database name changes come back here
void chain_mon_bypass::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string chain_mon_bypass::parentCallback( parentNotify *p )
{
	p->dump("chain_mon_bypass::parentCallback");
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "chain_device=%1" ).arg( m_chainDevice );
			sl << bncs_string( "label=%1" ).arg(m_deviceLabel);
			sl << bncs_string( "layout=%1" ).arg(m_layout);

			return sl.toString( '\n' );
		}

		else if( p->value() == "chain_device" )
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_chainDevice));
		}
		else if (p->value() == "label")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_deviceLabel));
		}
		else if (p->value() == "layout")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_layout));
		}
	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
		loadChain();

		if (m_chainDevice.contains(','))
		{
			loadDevicePair();
		}
		else
		{
			loadDevice();
		}
	}

	else if( p->command() == "chain_device" )
	{	// Persisted value or 'Command' being set here
		m_chainDevice = p->value();
	}

	else if (p->command() == "label")
	{	// Persisted value or 'Command' being set here
		m_deviceLabel = p->value();
		textPut("text", m_deviceLabel, PNL_MAIN, "label");
	}
	else if (p->command() == "layout")
	{	// Persisted value or 'Command' being set here
		m_layout = p->value();
		panelDestroy(PNL_MAIN);
		if (m_layout == "")
		{
			panelShow(PNL_MAIN, LAYOUT_DEFAULT);
		}
		else
		{
			panelShow(PNL_MAIN, m_layout + ".bncs_ui");
			setSize(1);
		}
		textPut("text", m_deviceLabel, PNL_MAIN, "label");
		textPut("text", m_deviceLabel, PNL_MAIN, BTN_SELECT);
		textPut("statesheet", "primary_deselected", PNL_MAIN, BTN_SELECT);
	}
	// ***** run time commands *****
	else if (p->command() == "role")
	{
		m_role = p->value().toInt();
		//textPut("text", bncs_string("%1 [%2/%3]").arg(m_deviceLabel).arg(m_role).arg(m_monDestPvw), PNL_MAIN, "label");
	}
	else if (p->command() == "mon_dest_pvw")
	{
		m_monDestPvw = p->value().toInt();
		//textPut("text", bncs_string("%1 [%2/%3]").arg(m_deviceLabel).arg(m_role).arg(m_monDestPvw), PNL_MAIN, "label");

		infoRegister(m_deviceTielineInfo, m_monDestPvw, m_monDestPvw);
		infoPoll(m_deviceTielineInfo, m_monDestPvw, m_monDestPvw);
	}
	else if (p->command() == "external_dest")
	{
		m_extDest = p->value().toInt();
		textPut("index", m_extDest, PNL_MAIN, "external_dest");
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		//sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		//sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void chain_mon_bypass::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void chain_mon_bypass::loadChain()
{
	//textPut("text", m_instance, PNL_MAIN, "chain");

	/*
	<item id="chain_tx_11">
		<setting id="name" value="Kanal 5" />
		<setting id="territory" value="Sweden" />
		<setting id="icon" value="Kanal_5_Sweden.png" />
		<setting id="chain_router" value="17011/RTR/001" />
		<setting id="ap_router" value="17011/RTR/002" />
		<setting id="tieline_dst_cmr_1" value="64" />
		<setting id="tieline_dst_cmr_2" value="65" />
		<setting id="tieline_dst_cmr_3" value="66" />
		<setting id="tieline_dst_cmr_4" value="67" />
		<setting id="tieline_dst_cmr_mv_chain" value="68" />
		<setting id="tieline_dst_cmr_mv_live" value="69" />
		<setting id="tieline_src_cmr_1" value="470" />
		<setting id="tieline_src_cmr_2" value="488" />
		<setting id="tieline_src_cmr_3" value="506" />
		<setting id="tieline_src_cmr_4" value="524" />
		<setting id="tieline_src_cmr_mv_chain" value="542" />
		<setting id="tieline_src_cmr_mv_live" value="560" />
	</item>
	*/

	/*
	bncs_config pc(bncs_string("playout_chains.%1").arg(m_instance));
	if (pc.isValid())
	{
		while (pc.isChildValid())
		{
			debug("chain_mon_bypass::LoadChain()    child id=%1 value=%2", pc.childAttr("id"), pc.childAttr("value"));

			bncs_string id = pc.childAttr("id");
			bncs_string value = pc.childAttr("value");

			if (pc.childAttr("id").startsWith("tl_dst_cmr_"))
			{
				textPut("text", value, PNL_MAIN, id);
			}

			pc.nextChild();
		}
	}
	*/

	/*
	<instance composite="yes" id="chain_tx_11" type="chain_fta_hd_sd_quad_srv" alt_id="Kanal 5 Sweden">
		<group id="chain_router" instance="17011/RTR/001" />
		<group id="ap_router" instance="17011/RTR/002" />
		<group id="chain_arc" instance="17011/ARC/001" />
		<group id="ap_arc" instance="17011/ARC/002" />
		<group id="co_1" instance="17011/COSW/001" />
		<group id="co_2" instance="17011/COSW/002" />
		<group id="co_3" instance="" />
	</instance>
	*/
	bncs_config inst(bncs_string("instances.%1").arg(m_instance));
	if (inst.isValid())
	{
		while (inst.isChildValid())
		{
			bncs_string groupId = inst.childAttr("id");
			bncs_string groupInstance = inst.childAttr("instance");
			debug("chain_mon_bypass::LoadChain()    child group id=%1 instance=%2", groupId, groupInstance);

			if (groupId == "chain_router")
			{
				//textPut("text", groupInstance, PNL_MAIN, "label");
				getDev(groupInstance, &m_deviceChainRouter);
			}

			inst.nextChild();
		}
	}


}

void chain_mon_bypass::loadDevicePair()
{
	/*
	[chain_tx_11.xml]
	<playout_chain>
		<item id="device_main_downconverter">
			<setting id="mon_source" value="694" />
			<setting id="active_out_source" value="44" />
			<setting id="bypass_out_source" value="0" />
			<setting id="chain_out_dest" value="49" />
			<setting id="chain_bypass_dest" value="50" />
		</item>
		<item id="device_backup_downconverter">
			<setting id="mon_source" value="696" />
			<setting id="active_out_source" value="46" />
			<setting id="bypass_out_source" value="" />
			<setting id="chain_out_dest" value="49" />
			<setting id="chain_bypass_dest" value="50" />
		</item>
	*/

	bncs_stringlist sltMainBackup(m_chainDevice, ',');
	if (sltMainBackup.count() == 2)
	{
		bncs_string chainDeviceMain = sltMainBackup[0];
		bncs_string chainDeviceBackup = sltMainBackup[1];

		bncs_string pathMain = bncs_string("%1.device_%2").arg(m_instance).arg(chainDeviceMain);
		bncs_string pathBackup = bncs_string("%1.device_%2").arg(m_instance).arg(chainDeviceBackup);

		m_devicePair = true;

		bncs_config cfgMain(pathMain);
		if (cfgMain.isValid())
		{
			while (cfgMain.isChildValid())
			{
				bncs_string setting = cfgMain.childAttr("id");
				bncs_string value = cfgMain.childAttr("value");
				debug("chain_mon_bypass::loadDevicePair()  path=%1  child id=%2 value=%3", chainDeviceMain, setting, value);

				if (setting == "active_out_source")
				{
					m_sourceActiveMain = value.toInt();
				}
				else if (setting == "chain_out_dest")
				{
					m_destChainOutMain = value.toInt();
					m_destChainOut = m_destChainOutMain;
				}
				else if (setting == "chain_bypass_dest")
				{
					m_destChainBypassMain = value.toInt();
					m_destChainBypass = m_destChainBypassMain;
				}

				cfgMain.nextChild();
			}
		}

		bncs_config cfgBackup(pathBackup);
		if (cfgBackup.isValid())
		{
			while (cfgBackup.isChildValid())
			{
				bncs_string setting = cfgBackup.childAttr("id");
				bncs_string value = cfgBackup.childAttr("value");
				debug("chain_mon_bypass::loadDevicePair()  path=%1  child id=%2 value=%3", chainDeviceBackup, setting, value);

				if (setting == "active_out_source")
				{
					m_sourceActiveBackup = value.toInt();
				}
				else if (setting == "chain_out_dest")
				{
					m_destChainOutBackup = value.toInt();
					//Note the backup chain destination will be the same as for the main
					if (m_destChainOutBackup != m_destChainOutMain)
					{
						debug("chain_mon_bypass::loadDevicePair()  warning %1 chain - m_destChainOutBackup[%2] != destChainOutMain[%3]", chainDeviceBackup, m_destChainOutBackup, m_destChainOutMain);
					}
				}
				else if (setting == "chain_bypass_dest")
				{
					m_destChainBypassBackup = value.toInt();
					//Note the backup bypass destination will be the same as for the main
					if (m_destChainBypassBackup != m_destChainBypassMain)
					{
						debug("chain_mon_bypass::loadDevicePair()  warning %1 chain - destChainBypassBackup[%2] != m_destChainBypassMain[%3]", chainDeviceBackup, m_destChainBypassBackup, m_destChainBypassMain);
					}
				}

				cfgBackup.nextChild();
			}
		}
	}

	//textPut("text", bncs_string("%1 - %2|%3 - %4").arg(m_sourceActiveMain).arg(m_destChainBypassMain).arg(m_sourceActiveBackup).arg(m_destChainOutMain), PNL_MAIN, "debug");

	//register for bypass
	routerRegister(m_deviceChainRouter, m_destChainOut, m_destChainOut);
	routerRegister(m_deviceChainRouter, m_destChainBypass, m_destChainBypass, true);
	routerPoll(m_deviceChainRouter, m_destChainOut, m_destChainOut);
	routerPoll(m_deviceChainRouter, m_destChainBypass, m_destChainBypass);
}

void chain_mon_bypass::loadDevice()
{
	textPut("text", m_chainDevice, PNL_MAIN, "name");

	/*
	[chain_tx_11.xml]
	<playout_chain>
		<item id = "device_chain_arc">
			<setting id = "instance" value = "17011/ARC/001" /><!--also in composite instance-->
			<setting id = "valid_active_source" value = "7" />
			<setting id = "valid_bypass_source" value = "60" />
			<setting id = "active_in_dest" value = "11" />
			<setting id = "active_out_source" value = "11" />
			<setting id = "bypass_in_dest" value = "12" />
			<setting id = "bypass_out_source" value = "12" />
			<setting id = "chain_out_dest" value = "14" />
			<setting id = "chain_bypass_dest" value = "15" />
		</item>

		<item id="device_air_protect_arc">
			<setting id="instance" value="17011/ARC/002" />
			<setting id="active_out_source_hd" value="43" />
			<setting id="active_out_source_sd" value="59" />
			<setting id="mon_source_hd" value="693" />
			<setting id="mon_source_sd" value="693" />
		</item>

	*/


	//m_instance	=	chain_tx_11
	//m_chainDevice	=	chain_arc
	//path			=	chain_tx_11.device_chain_arc
	bncs_string path = bncs_string("%1.device_%2").arg(m_instance).arg(m_chainDevice);
	debug("chain_mon_bypass::loadDevice() path=%1", path);
	bncs_config cd(path);
	if (cd.isValid())
	{
		m_devicePair = false;

		while (cd.isChildValid())
		{
			bncs_string setting = cd.childAttr("id");
			bncs_string value = cd.childAttr("value");
			debug("chain_mon_bypass::LoadChain()    child id=%1 value=%2", setting, value);

			if (setting == "mon_source")
			{
				m_monSource = value.toInt();
			}
			else if (setting == "mon_source_hd")
			{
				m_monSourceHD = value.toInt();
			}
			else if (setting == "mon_source_sd")
			{
				m_monSourceSD = value.toInt();
			}
			else if (setting == "active_out_source")
			{
				m_sourceActive = value.toInt();
				m_isSourcePreset = false;
			}
			else if (setting == "active_out_preset_source")
			{
				m_sourceActive = value.toInt();
				m_isSourcePreset = true;
			}
			else if (setting == "bypass_out_source")
			{
				m_sourceBypass = value.toInt();
			}
			else if (setting == "chain_out_dest")
			{
				bncs_stringlist sltDestList(value);
				if (sltDestList.count() == 1)
				{
					m_destChainOut = value.toInt();
				}
				else if (sltDestList.count() == 2)
				{
					m_destChainOut = sltDestList[0].toInt();
					m_destChainOut1 = sltDestList[1].toInt();
					debug("chain_mon_bypass::loadDevicePair() ### m_destChainOut=%1 m_destChainOut1=%1 m_destChainOut2=%1 ", m_destChainOut, m_destChainOut1, m_destChainOut2);
				}
				else if (sltDestList.count() == 3)
				{
					m_destChainOut = sltDestList[0].toInt();
					m_destChainOut1 = sltDestList[1].toInt();
					m_destChainOut2 = sltDestList[2].toInt();
					debug("chain_mon_bypass::loadDevicePair() ### m_destChainOut=%1 m_destChainOut1=%1 m_destChainOut2=%1 ", m_destChainOut, m_destChainOut1, m_destChainOut2);
				}


				m_destChainOut = value.toInt();
			}
			else if (setting == "chain_bypass_dest")
			{
				m_destChainBypass = value.toInt();
			}

			cd.nextChild();
		}
	}
	else
	{
		textPut("text", "device not found", PNL_MAIN, "mon_source");
	}

	if (m_monSource > 0)
	{
		controlEnable(PNL_MAIN, BTN_MON);
	}
	else
	{
		controlDisable(PNL_MAIN, BTN_MON);
	}

	if (m_destChainOut > 0)
	{
		controlEnable(PNL_MAIN, BTN_BYPASS);

		//register for bypass
		routerRegister(m_deviceChainRouter, m_destChainOut, m_destChainOut);
		routerPoll(m_deviceChainRouter, m_destChainOut, m_destChainOut);

		if (m_destChainBypass > 0)
		{
			routerRegister(m_deviceChainRouter, m_destChainBypass, m_destChainBypass, true);
			routerPoll(m_deviceChainRouter, m_destChainBypass, m_destChainBypass);
		}
	}
	else
	{
		controlDisable(PNL_MAIN, BTN_BYPASS);
	}

	if (m_sourceActive > 0)
	{
		bncs_config cfgMixPGM(bncs_string("%1.device_mix_%2").arg(m_instance).arg(m_isSourcePreset ? "pst" : "pgm"));
		if (cfgMixPGM.isValid())
		{
			while (cfgMixPGM.isChildValid())
			{
				bncs_string setting = cfgMixPGM.childAttr("id");
				bncs_string value = cfgMixPGM.childAttr("value");

				if (setting == "active_in_dest")
				{
					m_destMixPGM = value.toInt();
					//textPut("text", m_destMixPGM, PNL_MAIN, BTN_MON);
					break;
				}

				cfgMixPGM.nextChild();
			}
		}

		if (m_destMixPGM > 0)
		{
			//register for primary input select
			routerRegister(m_deviceChainRouter, m_destMixPGM, m_destMixPGM, true);
			routerPoll(m_deviceChainRouter, m_destMixPGM, m_destMixPGM);
		}
		else
		{
			controlDisable(PNL_MAIN, BTN_SELECT);
		}
	}

	textPut("text", bncs_string("%1 - %2|%3 - %4").arg(m_sourceBypass).arg(m_destChainBypass).arg(m_sourceActive).arg(m_destChainOut), PNL_MAIN, "debug");

}

void chain_mon_bypass::bypass()
{
	debug("chain_mon_bypass::bypass()");

	//not all devices have a backup output path e.g. device_hd_teletext_bridge
	if (m_destChainBypass > 0)
	{
		if (m_chainDestTally == m_sourceActive && m_bypassDestTally == m_sourceActive)
		{
			//Active - route bypass
			routerCrosspoint(m_deviceChainRouter, m_sourceBypass, m_destChainOut);
			if (m_destChainBypass > 0)
			{
				//not all devices have a backup output path e.g. device_hd_teletext_bridge
				routerCrosspoint(m_deviceChainRouter, m_sourceBypass, m_destChainBypass);
			}
			if (m_destChainOut1 > 0)
			{
				//not all devices have a first additional destination e.g. device_scte_inserter
				routerCrosspoint(m_deviceChainRouter, m_sourceBypass, m_destChainOut1);
			}
			if (m_destChainOut2 > 0)
			{
				//not all devices have a second additional destination e.g. device_scte_inserter
				routerCrosspoint(m_deviceChainRouter, m_sourceBypass, m_destChainOut2);
			}
		}
		else
		{
			//Other - route active
			routerCrosspoint(m_deviceChainRouter, m_sourceActive, m_destChainOut);
			if (m_destChainBypass > 0)
			{
				//not all devices have a backup output path e.g. device_hd_teletext_bridge
				routerCrosspoint(m_deviceChainRouter, m_sourceActive, m_destChainBypass);
			}
			if (m_destChainOut1 > 0)
			{
				//not all devices have a first additional destination e.g. device_scte_inserter
				routerCrosspoint(m_deviceChainRouter, m_sourceActive, m_destChainOut1);
			}
			if (m_destChainOut2 > 0)
			{
				//not all devices have a second additional destination e.g. device_scte_inserter
				routerCrosspoint(m_deviceChainRouter, m_sourceActive, m_destChainOut2);
			}
		}
	}
	else
	{
		if (m_chainDestTally == m_sourceActive)
		{
			//Active - route bypass
			routerCrosspoint(m_deviceChainRouter, m_sourceBypass, m_destChainOut);

			if (m_destChainOut1 > 0)
			{
				//not all devices have a first additional destination e.g. device_scte_inserter
				routerCrosspoint(m_deviceChainRouter, m_sourceBypass, m_destChainOut1);
			}
			if (m_destChainOut2 > 0)
			{
				//not all devices have a second additional destination e.g. device_scte_inserter
				routerCrosspoint(m_deviceChainRouter, m_sourceBypass, m_destChainOut2);
			}
		}
		else
		{
			//Other - route active
			routerCrosspoint(m_deviceChainRouter, m_sourceActive, m_destChainOut);

			if (m_destChainOut1 > 0)
			{
				//not all devices have a first additional destination e.g. device_scte_inserter
				routerCrosspoint(m_deviceChainRouter, m_sourceActive, m_destChainOut1);
			}
			if (m_destChainOut2 > 0)
			{
				//not all devices have a second additional destination e.g. device_scte_inserter
				routerCrosspoint(m_deviceChainRouter, m_sourceActive, m_destChainOut2);
			}
		}
	}
}

void chain_mon_bypass::monitor()
{
	if (m_role > 0 && m_monDestPvw > 0 && m_monSource > 0)
	{
		int device = 912;	//TODO init properly
		infoWrite(device, bncs_string("%1,%2").arg(m_monSource).arg(m_role), m_monDestPvw);
	}
}

void chain_mon_bypass::monitorHD()
{
	if (m_role > 0 && m_monDestPvw > 0 && m_monSourceHD > 0)
	{
		int device = 912;	//TODO init properly
		infoWrite(device, bncs_string("%1,%2").arg(m_monSourceHD).arg(m_role), m_monDestPvw);
	}
}

void chain_mon_bypass::monitorSD()
{
	if (m_role > 0 && m_monDestPvw > 0 && m_monSourceSD > 0)
	{
		int device = 912;	//TODO init properly
		infoWrite(device, bncs_string("%1,%2").arg(m_monSourceSD).arg(m_role), m_monDestPvw);
	}
}

void chain_mon_bypass::updateMonDest(bncs_string monDestTally)
{
	bncs_stringlist sltSourceTieline(monDestTally, ',');
	if (sltSourceTieline.count() == 2)
	{
		m_monDestTally = sltSourceTieline[0].toInt();
		if (m_monDestTally == m_monSource)
		{
			textPut("statesheet", "mon_point_active", PNL_MAIN, BTN_MON);
		}
		else if (m_monDestTally == m_monSourceHD)
		{
			textPut("statesheet", "mon_point_active", PNL_MAIN, BTN_MON_HD);
			textPut("statesheet", "mon_point_inactive", PNL_MAIN, BTN_MON_SD);
		}
		else if (m_monDestTally == m_monSourceSD)
		{
			textPut("statesheet", "mon_point_inactive", PNL_MAIN, BTN_MON_HD);
			textPut("statesheet", "mon_point_active", PNL_MAIN, BTN_MON_SD);
		}
		else
		{
			textPut("statesheet", "mon_point_inactive", PNL_MAIN, BTN_MON);
			textPut("statesheet", "mon_point_inactive", PNL_MAIN, BTN_MON_HD);
			textPut("statesheet", "mon_point_inactive", PNL_MAIN, BTN_MON_SD);
		}

		/*
		bncs_string sourceName;
		routerName(m_deviceTielineGRD, 0, m_monDestTally, sourceName);
		textPut("text", sourceName, PNL_MAIN, BTN_MON);
		*/
	}
}

void chain_mon_bypass::updateBypass()
{
	if (m_devicePair)
	{
		//Different handling for main/backup
		if (m_chainDestTally == m_sourceActiveMain && m_bypassDestTally == m_sourceActiveMain)
		{
			textPut("statesheet", "enum_normal", PNL_MAIN, BTN_MAIN);
			textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_BACKUP);
		}
		else if (m_chainDestTally == m_sourceActiveBackup && m_bypassDestTally == m_sourceActiveBackup)
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_MAIN);
			textPut("statesheet", "enum_normal", PNL_MAIN, BTN_BACKUP);
		}
		else
		{
			textPut("statesheet", "enum_warning", PNL_MAIN, BTN_MAIN);
			textPut("statesheet", "enum_warning", PNL_MAIN, BTN_BACKUP);
			//textPut("text", bncs_string("%1/%2").arg(m_chainDestTally).arg(m_bypassDestTally), PNL_MAIN, "debug");
		}
	}
	else
	{
		//not all devices have a backup output path e.g. device_hd_teletext_bridge
		if (m_destChainBypass > 0)
		{
			if (m_chainDestTally == m_sourceActive && m_bypassDestTally == m_sourceActive)
			{
				textPut("statesheet", "enum_normal", PNL_MAIN, BTN_BYPASS);
			}
			else if (m_chainDestTally == m_sourceBypass && m_bypassDestTally == m_sourceBypass)
			{
				textPut("statesheet", "enum_warning", PNL_MAIN, BTN_BYPASS);
			}
			else
			{
				textPut("statesheet", "enum_alarm", PNL_MAIN, BTN_BYPASS);
			}
		}
		else
		{
			if (m_chainDestTally == m_sourceActive)
			{
				textPut("statesheet", "enum_normal", PNL_MAIN, BTN_BYPASS);
			}
			else if (m_chainDestTally == m_sourceBypass)
			{
				textPut("statesheet", "enum_warning", PNL_MAIN, BTN_BYPASS);
			}
			else
			{
				textPut("statesheet", "enum_alarm", PNL_MAIN, BTN_BYPASS);
			}
		}
	}
}

void chain_mon_bypass::main()
{
	debug("chain_mon_bypass::main()");

	if (m_sourceActiveMain > 0)
	{
		if (m_destChainOut > 0)
		{
			routerCrosspoint(m_deviceChainRouter, m_sourceActiveMain, m_destChainOut);
		}
		if (m_destChainBypass > 0)
		{
			routerCrosspoint(m_deviceChainRouter, m_sourceActiveMain, m_destChainBypass);
		}
	}
}

void chain_mon_bypass::backup()
{
	debug("chain_mon_bypass::backup()");

	if (m_sourceActiveBackup > 0)
	{
		if (m_destChainOut > 0)
		{
			routerCrosspoint(m_deviceChainRouter, m_sourceActiveBackup, m_destChainOut);
		}
		if (m_destChainBypass > 0)
		{
			routerCrosspoint(m_deviceChainRouter, m_sourceActiveBackup, m_destChainBypass);
		}
	}
}

void chain_mon_bypass::select()
{
	routerCrosspoint(m_deviceChainRouter, m_sourceActive, m_destMixPGM);
}

void chain_mon_bypass::updateSelect(int source)
{
	if (source == m_sourceActive)
	{
		textPut("statesheet", "primary_selected", PNL_MAIN, BTN_SELECT);
	}
	else
	{
		textPut("statesheet", "primary_deselected", PNL_MAIN, BTN_SELECT);
	}
}