#ifndef region_mon_mv_INCLUDED
	#define region_mon_mv_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class region_mon_mv : public bncs_script_helper
{
public:
	region_mon_mv( bncs_client_callback * parent, const char* path );
	virtual ~region_mon_mv();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_layout;
	bncs_string m_currentLayout;

	int m_deviceTielineInfo;
	int m_monSource;
	int m_monDestPvw;
	int m_monDestTally;

	int m_deviceMV;
	int m_offset;
	int m_param;
	int m_slot;

	void initMV();
	void initPvw();
	void monitor();
	void updateMonDest(bncs_string monDestTally);
	void updateTally();
	
};


#endif // region_mon_mv_INCLUDED