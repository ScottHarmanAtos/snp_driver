#ifndef chain_monitor_status_INCLUDED
	#define chain_monitor_status_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class chain_monitor_status : public bncs_script_helper
{
public:
	chain_monitor_status( bncs_client_callback * parent, const char* path );
	virtual ~chain_monitor_status();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	int m_devMFR;
	int m_slotChannel;
	int m_slotRole;
	int m_slotFunction;

	bncs_string m_opsAreaName;

	bncs_string m_opsArea;
	bncs_string m_opsPosition;
	bncs_string m_playoutMFR;
	bncs_string m_currentChannel;

	bncs_string m_monDestMvLive;
	bncs_string m_monDestMvChain;
	bncs_string m_monDestPvw;

	bncs_string m_myParam;
	bncs_string m_instance;

	void init();
	void updateChannel(bncs_string channel);
	void updateRole(bncs_string role);

	
};


#endif // chain_monitor_status_INCLUDED