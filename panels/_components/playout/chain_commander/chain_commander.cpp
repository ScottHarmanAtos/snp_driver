#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "chain_commander.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1
#define TIMER_SETUP_INSTANCE	2

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( chain_commander )

// constructor - equivalent to ApplCore STARTUP
chain_commander::chain_commander( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file chain_commander.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "chain_commander.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
	m_xmlfile = "playout_chains";
}

// destructor - equivalent to ApplCore CLOSEDOWN
chain_commander::~chain_commander()
{
}

// all button pushes and notifications come here
void chain_commander::buttonCallback( buttonNotify *b )
{
}

// all revertives come here
int chain_commander::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void chain_commander::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string chain_commander::parentCallback( parentNotify *p )
{
	debug("chain_commander::parentCallback() cmd=%1 value=%2", p->command(), p->value());
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("chain_id=%1").arg(m_chain_id);
			sl << bncs_string("xml_file=%1").arg(m_xmlfile);

			return sl.toString( '\n' );
		}

		else if( p->value() == "chain_id" )
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_chain_id));
		}

	}
	else if (p->command() == "xml_file")
	{
		//This is the xml file to use
		if (p->value().length() == 0)
		{
			m_xmlfile = "playout_chains";
		}
		else
		{
			m_xmlfile = p->value();
		}
	}
	else if( p->command() == "instance" && p->value() != m_chain_id )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//timerStart(TIMER_SETUP_INSTANCE, 100);
		LoadSettings(m_instance);

		//Do something instance-change related here
	}
	else if (p->command() == "raw_parameters" && p->value() != m_chain_id)
	{	// Our instance is being set/changed
		m_chain_id = p->value();
		timerStart(TIMER_SETUP, 100);

		//Do something instance-change related here
	}
	else if (p->command() == "role")
	{
		//textPut("text", p->value(), PNL_MAIN, "label");
		hostNotify("role=" + p->value());
	}
	else if (p->command() == "mon_dest_pvw")
	{
		//textPut("text", p->value(), PNL_MAIN, "label");
		hostNotify("mon_dest_pvw=" + p->value());
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "channel_icon=*";
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "chain_id=[value]";

		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void chain_commander::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		LoadChain(m_chain_id);
		break;
	case TIMER_SETUP_INSTANCE:
		timerStop(id);
		LoadSettings(m_instance);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void chain_commander::LoadSettings(bncs_string instance)
{
	debug("chain_commander::LoadSettings() instance=%1", instance);
	hostNotify(bncs_string("instance=").append(instance));
	bncs_config c(bncs_string("instances.%1").arg(instance));
	if (c.isValid() == false)
	{
		textPut("text", bncs_string("%1 - Error").arg(instance), PNL_MAIN, "label");
		textPut("stylesheet", "enum_alarm", PNL_MAIN, "label");
		return;
	}
	else
	{
		bncs_string altID = c.attr("alt_id");
		textPut("text", altID, PNL_MAIN, "label");
		//textPut("stylesheet", "enum_normal", PNL_MAIN, "label");

		/*
		<item id="chain_tx_41">
			<setting id="tx" value="41" />
			<setting id="name" value="Eurosport 1" />
			<setting id="territory" value="Denmark" />
			<setting id="icon" value="Eurosport_1.png" />

			<setting id="cmr_dst_ext_1" value="577" />
			<setting id="cmr_dst_ext_2" value="578" />
			<setting id="cmr_dst_ext_3" value="579" />
			<setting id="cmr_dst_ext_4" value="580" />

			<setting id="cmr_dst_ac_1_itx_a" value="1" />
			<setting id="cmr_dst_ac_1_itx_b" value="2" />
			<setting id="cmr_dst_ac_2_itx_a" value="37" />
			<setting id="cmr_dst_ac_2_itx_b" value="38" />

			<setting id="mon_src_mv_chain" value="55" />
			<setting id="mon_src_mv_live" value="73" />
		</item>
		*/


		bncs_config pc(bncs_string("playout_chains.%1").arg(m_instance));
		if (pc.isValid())
		{
			while (pc.isChildValid())
			{

				bncs_string id = pc.childAttr("id");
				bncs_string value = pc.childAttr("value");
				debug("chain_commander::LoadChain() %1=%2", id, value);

				//send channel_icon to parent
				if (id == "icon")
				{
					bncs_string path = "/images/channels/" + value;
					//debug("chain_commander::LoadChain() channel_icon=%1", path);
					hostNotify(bncs_string("%1=%2").arg("channel_icon").arg(path));
				}
				else if (id == "cmr_dst_ext_1") 
				{
					hostNotify(bncs_string("external_1=%1").arg(value));
				}
				else if (id == "cmr_dst_ext_2")
				{
					hostNotify(bncs_string("external_2=%1").arg(value));
				}
				else if (id == "cmr_dst_ext_3")
				{
					hostNotify(bncs_string("external_3=%1").arg(value));
				}
				else if (id == "cmr_dst_ext_4")
				{
					hostNotify(bncs_string("external_4=%1").arg(value));
				}
				else if (id == "cmr_dst_ac_1_itx_a")
				{
					hostNotify(bncs_string("dst_ac_1_itx_a=%1").arg(value));
				}
				else if (id == "cmr_dst_ac_1_itx_b")
				{
					hostNotify(bncs_string("dst_ac_1_itx_b=%1").arg(value));
				}
				else if (id == "cmr_dst_ac_2_itx_a")
				{
					hostNotify(bncs_string("dst_ac_2_itx_a=%1").arg(value));
				}
				else if (id == "cmr_dst_ac_2_itx_b")
				{
					hostNotify(bncs_string("dst_ac_2_itx_b=%1").arg(value));
				}
				pc.nextChild();
			}
		}

		while (c.isChildValid())
		{
			debug("chain_commander::LoadSettings()    child id=%1 instance=%2 settings=%3", c.childAttr("id"), c.childAttr("instance"), c.childAttr("settings"));

			bncs_stringlist sltSettings = bncs_stringlist(c.childAttr("settings"));
			for (int setting = 0; setting < sltSettings.count(); setting++)
			{
				debug("chain_commander::LoadSettings() sltSettings[setting]=%1", sltSettings[setting]);
				bncs_string cmd = "";
				bncs_string target = c.childAttr("id");
				bncs_string value = "";
				int split = sltSettings[setting].split('=', cmd, value);
				debug("chain_commander::LoadSettings() cmd=%1 sub0=%2 value=%3", cmd, target, value);
				hostNotify(bncs_string("%1.%2=%3").arg(cmd).arg(target).arg(value));
			}
			c.nextChild();
		}
	}
}


void chain_commander::LoadChain(bncs_string chain_id)
{
	debug("chain_commander::LoadChain() chain_id=%1", chain_id);
	bncs_config c(bncs_string("%1.%2").arg(m_xmlfile).arg(chain_id));
	if (c.isValid() == false)
	{
		textPut("text", bncs_string("%1 - Error").arg(chain_id), PNL_MAIN, "label");
		textPut("stylesheet", "enum_alarm" , PNL_MAIN, "label");
		return;
	}
	else
	{
		textPut("text",chain_id, PNL_MAIN, "label");
		textPut("stylesheet", "enum_normal", PNL_MAIN, "label");
	}

	while (c.isChildValid())
	{
		//Instance is just a place holder, we should be getting this information from the xml too.
		hostNotify(bncs_string("%1.%2=%3").arg(c.childAttr("type")).arg(c.childAttr("id")).arg(c.childAttr("value")));

		c.nextChild();
	}
}
