#ifndef dual_preset_load_INCLUDED
	#define dual_preset_load_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dual_preset_load : public bncs_script_helper
{
public:
	dual_preset_load( bncs_client_callback * parent, const char* path );
	virtual ~dual_preset_load();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	
	void loadPreset(int preset);

	void updateCurrentPreset(int preset);
};


#endif // dual_preset_load_INCLUDED