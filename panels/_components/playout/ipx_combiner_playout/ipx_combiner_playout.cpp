#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include "ipx_combiner_playout.h"

#define PANEL_MAIN						1

#define TIMER_SETUP						1

// Disable button revertive for a while
#define TIMER_DISABLE_BUTTONS			2
#define DISABLE_BUTTONS_DURATION		500

#define TIMER_SELECT_DEST_PAGE			3
#define TIMER_SELECT_DEST_PAGE_DURATION 100

#define GROUP_COUNT						10
#define PAGE_COUNT						16
#define DEST_COUNT						32

#define BTN_GROUP						"group"
#define BTN_PAGE						"page"
#define BTN_DEST						"dest_grid"

#define PRESELECT_MONITOR_TEST "preselect_monitor_test"
#define PRESELECT_PARK	"preselect_park"
#define PRESELECT_LOCK	"preselect_lock"


#define PRESELECT_MON_1	"preselect_mon_1"
#define PRESELECT_MON_2	"preselect_mon_2"
#define PRESELECT_MON_3	"preselect_mon_3"
#define PRESELECT_MON_4	"preselect_mon_4"

#define DEST_MON_1	"dest_mon_1"
#define DEST_MON_2	"dest_mon_2"
#define DEST_MON_3	"dest_mon_3"
#define DEST_MON_4	"dest_mon_4"



// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( ipx_combiner_playout )

// constructor - equivalent to ApplCore STARTUP
ipx_combiner_playout::ipx_combiner_playout( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file player.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PANEL_MAIN, "ipx_combiner_playout.bncs_ui" );

	// default
	m_iSelectedTabIndex = -1;
	m_iSelectedPage = 1;
	m_iSelectedPageIndex = 1;
	m_iSelectedDestPage = 1;
	m_iSelectedDestIndex = 1;
	m_bDisableButtonRevertiveHandling = true;
}

// destructor - equivalent to ApplCore CLOSEDOWN
ipx_combiner_playout::~ipx_combiner_playout()
{
}

// all button pushes and notifications come here
void ipx_combiner_playout::buttonCallback( buttonNotify *b )
{
	if (m_bDisableButtonRevertiveHandling) return;

	b->dump("ipx_combiner_playout component");
	if (b->panel() == PANEL_MAIN)
	{
		if (b->value() == "released")
		{

			if (b->id() == BTN_GROUP)
			{
				const auto btnIndex = b->sub(0).toInt();

				if ((btnIndex > 0) && (btnIndex <= GROUP_COUNT))
				{
					if (m_iSelectedTabIndex != btnIndex)
					{
						m_iSelectedTabIndex = btnIndex;

						m_iSelectedPage = m_iSelectedTabIndex;
						m_iSelectedPageIndex = 1;

						m_iSelectedDestPage = bncs_string((m_iSelectedPage * PAGE_COUNT) - PAGE_COUNT + m_iSelectedPageIndex);
						m_iSelectedDestIndex = 1;

						UpdateTabs();
						UpdateGrids();
					}
				}
			}
			else if (b->id() == BTN_PAGE)
			{
				const auto btnIndex = b->sub(0).toInt();

				if ((btnIndex > 0) && (btnIndex <= PAGE_COUNT))
				{
					if (m_iSelectedPageIndex != btnIndex)
					{
						m_iSelectedPageIndex = btnIndex;

						m_iSelectedDestPage = bncs_string(((m_iSelectedPage)* PAGE_COUNT) - PAGE_COUNT + m_iSelectedPageIndex);

						m_iSelectedDestIndex = 1;

						UpdateGrids();
					}
				}
			}
			else if (b->id() == BTN_DEST)
			{
				const auto btnIndex = b->sub(0).toInt();

				if ((btnIndex > 0) && (btnIndex < DEST_COUNT))
				{
					if (m_iSelectedDestIndex != btnIndex)
					{
						m_iSelectedDestIndex = btnIndex;
					}
				}
			}
		}

		else if (b->id() == PRESELECT_MONITOR_TEST)
		{
//			textPut("preselect", "monitor_test", PANEL_MAIN, "dest");
		}
		else if (b->id() == PRESELECT_LOCK)
		{
//			textPut("preselect", "lock", PANEL_MAIN, "dest");
		}
		else if (b->id() == PRESELECT_PARK)
		{
//			textPut("preselect", "park", PANEL_MAIN, "dest");
		}
		else if (b->id().startsWith("preselect_mon_"))
		{
//			textPut("preselect", "monitor", PANEL_MAIN, "dest");
		}
	}
}

// all revertives come here
int ipx_combiner_playout::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void ipx_combiner_playout::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string ipx_combiner_playout::parentCallback( parentNotify *p )
{
	p->dump("ipx_combiner_playout component");
	if (p->command() == "return")
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("selectedtabindex=%1").arg(m_iSelectedTabIndex);
			sl << bncs_string("selectedpageindex=%1").arg(m_iSelectedPageIndex);

			return sl.toString('\n');
		}
		else if (p->value() == "selectedtabindex")
		{
			bncs_stringlist sl;

			// Specific value being asked for by a textGet
			sl << bncs_string("selectedtabindex=%1").arg(m_iSelectedTabIndex);
			return sl.toString('\n');
		}
		else if (p->value() == "selectedpageindex")
		{
			bncs_stringlist sl;

			// Specific value being asked for by a textGet
			sl << bncs_string("selectedpageindex=%1").arg(m_iSelectedPageIndex);
			return sl.toString('\n');
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();

		textPut("instance", m_instance, PANEL_MAIN, BTN_DEST);
		InitialiseGui();


		// Pass this on to all the other controls on the panel
	}

	else if( p->command() == "selectedtabindex" )
	{

		if (m_iSelectedTabIndex != p->value().toInt())
		{
			m_iSelectedTabIndex = p->value().toInt();
			InitialiseGui();
		}

	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void ipx_combiner_playout::timerCallback( int id )
{
	timerStop(id);

	switch (id)
	{
	case TIMER_SETUP:
		break;
	case TIMER_DISABLE_BUTTONS:
		m_bDisableButtonRevertiveHandling = false;
		break;
	case TIMER_SELECT_DEST_PAGE:
		// Switch to dest page selected by the page button index
		textPut("page", m_iSelectedDestPage, PANEL_MAIN, BTN_DEST);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void ipx_combiner_playout::InitialiseGui(void)
{


	/////////////////
	// Groups - Tabs
	/////////////////
	UpdateTabs();

	/////////////////
	// Pages/Dests
	/////////////////
	UpdateGrids();

}

void ipx_combiner_playout::UpdateTabs()
{
	timerStart(TIMER_DISABLE_BUTTONS, DISABLE_BUTTONS_DURATION);
	m_bDisableButtonRevertiveHandling = true;

	for (int i = 1; i <= GROUP_COUNT; i++)
	{
		controlHide(PANEL_MAIN, bncs_string("tab_%1_selected").arg(i));
	}

	controlShow(PANEL_MAIN, bncs_string("tab_%1_selected").arg(m_iSelectedTabIndex));

	// Update the group button selected
	textPut(bncs_string("button.%1=released").arg(m_iSelectedTabIndex), PANEL_MAIN, BTN_GROUP);
}

void ipx_combiner_playout::UpdateGrids()
{
	timerStart(TIMER_DISABLE_BUTTONS, DISABLE_BUTTONS_DURATION);
	m_bDisableButtonRevertiveHandling = true;

	// Switch page selected by the page button index
	textPut("page", m_iSelectedPage, PANEL_MAIN, BTN_PAGE);

	// select first entry on the page selected
	textPut(bncs_string("button.%1=released").arg(m_iSelectedPageIndex), PANEL_MAIN, BTN_PAGE);


	timerStart(TIMER_SELECT_DEST_PAGE, TIMER_SELECT_DEST_PAGE_DURATION);

}