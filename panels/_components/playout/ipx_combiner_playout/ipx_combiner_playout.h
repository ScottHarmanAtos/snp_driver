#ifndef ipx_combiner_playout_INCLUDED
	#define ipx_combiner_playout_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class ipx_combiner_playout : public bncs_script_helper
{
public:
	ipx_combiner_playout( bncs_client_callback * parent, const char* path );
	virtual ~ipx_combiner_playout();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	int m_iSelectedTabIndex;
	int m_iSelectedPage;
	int m_iSelectedPageIndex;
	int m_iSelectedDestPage;
	int m_iSelectedDestIndex;

	bool m_bDisableButtonRevertiveHandling;

	void InitialiseGui();
	void UpdateTabs();
	void UpdateGrids();


	
};


#endif // ipx_combiner_playout_INCLUDED