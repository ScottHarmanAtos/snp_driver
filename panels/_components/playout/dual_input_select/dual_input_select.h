#ifndef dual_input_select_INCLUDED
	#define dual_input_select_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dual_input_select : public bncs_script_helper
{
public:
	dual_input_select( bncs_client_callback * parent, const char* path );
	virtual ~dual_input_select();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bool m_single;
	bncs_string m_instance;

	bncs_string m_outputCO1;
	bncs_string m_outputCO2;
	void updateOutputs();

	
};


#endif // dual_input_select_INCLUDED