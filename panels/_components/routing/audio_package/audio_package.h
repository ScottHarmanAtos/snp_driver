#ifndef audio_package_INCLUDED
	#define audio_package_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_package : public bncs_script_helper
{
public:
	audio_package( bncs_client_callback * parent, const char* path );
	virtual ~audio_package();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_displayLevel;
	int m_ap;
	int m_packagerRouterMainDevice;

	bncs_string m_instance;

	void showAP();
	
};


#endif // audio_package_INCLUDED