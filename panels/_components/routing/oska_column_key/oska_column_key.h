#ifndef oska_column_key_INCLUDED
	#define oska_column_key_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class oska_column_key : public bncs_script_helper
{
public:
	oska_column_key( bncs_client_callback * parent, const char* path );
	virtual ~oska_column_key();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_key;
	bncs_string m_instance;

	void init();

	void updateFunctionLanguage(bncs_string functionLanguage);
	void updateLabelSubtitle(bncs_string labelSubtitle);
	
};


#endif // oska_column_key_INCLUDED