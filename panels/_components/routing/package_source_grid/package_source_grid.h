#ifndef package_source_grid_INCLUDED
	#define package_source_grid_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"
#include "local_cache.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class package_source_grid : public bncs_script_helper
{
public:
	package_source_grid( bncs_client_callback * parent, const char* path );
	virtual ~package_source_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	/*
	<instance composite="yes" id="c_packager_router" ref="" type="">
	<group id="packager_router_1" instance="packager_router_main" />
	*/

	bncs_string m_compositePackager;			//c_packager_nhn or c_packager_ldc read from workstation_settings.xml

	bncs_string m_instance;						//c_packager_router
	bncs_string m_instancePackagerRouterMain;	//packager_router_nhn_main or packager_router_main 
	bncs_string m_workspace;
	bncs_string m_currentWorkspace;

	bncs_string m_layout;

	local_cache *lc;

	bncs_packager m_packagerRouter;

	int m_presetDest;
	int m_presetSource;
	int m_devicePackagerRouterMain;
	bool m_presetMode;
	bool m_localWorkspace;

	void select(int tab);
	void initWorkspace();
	void initPackager();
	void initView();
	void setSource();
};


#endif // package_source_grid_INCLUDED