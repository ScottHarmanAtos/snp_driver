#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "package_source_grid.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define SOURCE_GRID				"source_grid"
#define WORKSPACE_SOURCE_PAGES	"workspace_source_pages"
#define WORKSPACE_SELECT		"workspace_select"

#define BTN_LOCAL			"local_workspace"
#define LBL_PRESET_SOURCE	"preset_source"
#define	LBL_SOURCE			"source"
#define TAKE_COMPONENT		"take"

#define TAB_ALL			1
#define TAB_WORKSPACE	2
#define TAB_SELECT		3

#define DATABASE_SOURCE_NAME	0	//dev_1001 packager_router_main

#define LAYOUT_MAIN	"main.bncs_ui"
#define LAYOUT_WIDE	"wide.bncs_ui"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( package_source_grid )

// constructor - equivalent to ApplCore STARTUP
package_source_grid::package_source_grid( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_presetSource = 0;
	m_presetDest = 0;
	m_devicePackagerRouterMain = 0;
	m_localWorkspace = false;
	m_layout = LAYOUT_MAIN;
	lc = local_cache::getLocalCache();

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PNL_MAIN, LAYOUT_MAIN);
	controlHide(PNL_MAIN, BTN_LOCAL);

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel

	initPackager();
	initView();
}

// destructor - equivalent to ApplCore CLOSEDOWN
package_source_grid::~package_source_grid()
{
}

// all button pushes and notifications come here
void package_source_grid::buttonCallback( buttonNotify *b )
{
	b->dump("package_source_grid::buttonCallback()");
	if( b->panel() == PNL_MAIN )
	{
		if (b->id().startsWith("tab"))
		{
			bncs_stringlist sltTabIndex(b->id(), '_');
			int tab =  sltTabIndex[1].toInt();
			select(tab);

			if (tab != TAB_SELECT)
			{
				//store selected tab in cache
				lc->setValue("package_source_grid.tab", tab);
			}
		}
		else if (b->id() == SOURCE_GRID)
		{
			if (b->command() == "index")
			{
				m_presetSource = b->value().toInt();
				setSource();
			}
		}
		else if (b->id() == WORKSPACE_SOURCE_PAGES)
		{
			if (b->command() == "index")
			{
				m_presetSource = b->value().toInt();
				setSource();
			}
			else if (b->command() == "workspace_name")
			{
				if (m_currentWorkspace == m_workspace)
				{
					textPut("text", "LOCAL", PNL_MAIN, "tab_2");
				}
				else
				{
					bncs_string name = b->value();
					if (name != "?")
					{
						textPut("text", name, PNL_MAIN, "tab_2");
					}
				}
			}
		}
		else if (b->id() == WORKSPACE_SELECT)
		{
			if (b->command() == "workspace")
			{
				m_currentWorkspace = b->value();
				textPut("workspace", m_currentWorkspace, PNL_MAIN, WORKSPACE_SOURCE_PAGES);
				select(TAB_WORKSPACE);
			}
		}
		else if (b->id() == BTN_LOCAL)
		{
			if (m_currentWorkspace != m_workspace)
			{
				m_currentWorkspace = m_workspace;
				textPut("workspace", m_currentWorkspace, PNL_MAIN, WORKSPACE_SOURCE_PAGES);
			}
			controlHide(PNL_MAIN, BTN_LOCAL);
		}
		else if (b->id() == TAKE_COMPONENT)
		{
			if (b->command() == "take")
			{
				if (b->value().endsWith("&special"))
				{
					hostNotify(bncs_string("index=%1&special").arg(m_presetSource));
				}
				else
				{
					hostNotify(bncs_string("index=%1").arg(m_presetSource));
				}
			}
		}
	}
}

// all revertives come here
int package_source_grid::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void package_source_grid::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string package_source_grid::parentCallback( parentNotify *p )
{
	p->dump("package_source_grid::parentCallback()");
	if (p->command() == "return")
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("local_workspace=%1").arg(m_localWorkspace ? "true" : "false");
			sl << bncs_string("preset_mode=%1").arg(m_presetMode ? "true" : "false");
			sl << bncs_string("wide_layout=%1").arg(m_layout == LAYOUT_WIDE ? "true" : "false");
			
			return sl.toString( '\n' );
		}

		else if (p->value() == "local_workspace")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_localWorkspace ? "true" : "false"));
		}

		else if (p->value() == "preset_mode")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_presetMode ? "true" : "false"));
		}

		else if (p->value() == "wide_layout")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_layout == LAYOUT_WIDE ? "true" : "false"));
		}
	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
		textPut("instance", m_instance, PNL_MAIN, TAKE_COMPONENT);
	}

	else if( p->command() == "preset_mode" )
	{	// Persisted value or 'Command' being set here
		if (p->value().lower() == "true")
		{
			m_presetMode = true;
		}
		else
		{
			m_presetMode = false;
		}
	}

	else if (p->command() == "local_workspace")
	{
		if (p->value().lower() == "true")
		{
			m_localWorkspace = true;
			initWorkspace();
		}
		else
		{
			m_localWorkspace = false;
		}
	}

	else if (p->command() == "tab")
	{	// Persisted value or 'Command' being set here
		select(p->value().toInt());
	}

	else if (p->command() == "dest")
	{
		m_presetDest = p->value().toInt();
		textPut("dest", m_presetDest, PNL_MAIN, TAKE_COMPONENT);
	}

	else if (p->command() == "index")
	{
		//index=0 will be received when PARK is preselected on take component
		//source grid should deselect - send deselect command to component_grid of source_pacakge components
		if (p->value().toInt() == 0)
		{
			textPut(p->command(), p->value(), PNL_MAIN, SOURCE_GRID);
		}
	}

	else if (p->command() == "workspace")
	{
		m_workspace = p->value();
		initWorkspace();
	}

	else if (p->command() == "wide_layout")
	{
		if (p->value().lower() == "true")
		{
			m_layout = LAYOUT_WIDE;
			panelDestroy(PNL_MAIN);
			panelShow(PNL_MAIN, LAYOUT_WIDE);
			initView();
		}
		else
		{
			if (m_layout != LAYOUT_MAIN)
			{
				m_layout = LAYOUT_MAIN;
				panelDestroy(PNL_MAIN);
				panelShow(PNL_MAIN, LAYOUT_MAIN);
				initView();
			}
		}
	}


	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "index=*";		
		sl << "preset_source=*";

		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "dest=[value]";
		sl << "preset_mode=[value]";
		sl << "tab=[value]";
		sl << "workspace=[value]";

		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void package_source_grid::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void package_source_grid::initPackager()
{
	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager, "router");
	m_instancePackagerRouterMain = m_packagerRouter.instance(1);
	debug("package_source_grid::initPackager() m_compositePackager [%1] router is %2 - %3",  
		m_compositePackager, m_packagerRouter.isValid() ? "valid" : "NOT valid", m_instancePackagerRouterMain);

	//Get main router device
	m_devicePackagerRouterMain = m_packagerRouter.destDev(1);

	/*
	bncs_config cPackagerMain(bncs_string("instances.%1.packager_router_1").arg(m_instance));
	if (cPackagerMain.isValid())
	{
		m_packagerMainInstance = cPackagerMain.attr("instance");
		getDev(m_packagerMainInstance, &m_devicePackagerRouterMain);
	}
	*/

}

void package_source_grid::initView()
{
	bncs_string cacheTab;
	lc->getValue("package_source_grid.tab", cacheTab);
	int tab = cacheTab.toInt();

	if (tab > 0)
	{
		select(tab);
	}
	else
	{
		select(TAB_ALL);	//value not set
	}

	textPut("instance", m_instancePackagerRouterMain, PNL_MAIN, SOURCE_GRID);
}

void package_source_grid::initWorkspace()
{
	//Set workspace source pages component to local workspace
	if (m_localWorkspace == true)
	{
		m_workspace = getWorkstationSetting("workspace");
	}
	m_currentWorkspace = m_workspace;
	textPut("workspace", m_currentWorkspace, PNL_MAIN, WORKSPACE_SOURCE_PAGES);
}

void package_source_grid::select(int tab)
{
	if (tab == TAB_ALL)
	{
		//All
		controlShow(PNL_MAIN, "selected_1");
		controlHide(PNL_MAIN, "selected_2");
		controlHide(PNL_MAIN, "selected_3");

		controlShow(PNL_MAIN, SOURCE_GRID);
		controlHide(PNL_MAIN, WORKSPACE_SOURCE_PAGES);
		controlHide(PNL_MAIN, WORKSPACE_SELECT);
	}
	else if (tab == TAB_WORKSPACE)
	{
		//Local workspace
		controlHide(PNL_MAIN, "selected_1");
		controlShow(PNL_MAIN, "selected_2");
		controlHide(PNL_MAIN, "selected_3");

		controlHide(PNL_MAIN, SOURCE_GRID);
		controlShow(PNL_MAIN, WORKSPACE_SOURCE_PAGES);
		controlHide(PNL_MAIN, WORKSPACE_SELECT);

		if (m_currentWorkspace == m_workspace)
		{
			//local
			controlHide(PNL_MAIN, BTN_LOCAL);
		}
		else
		{
			controlShow(PNL_MAIN, BTN_LOCAL);
		}

	}
	else if (tab == TAB_SELECT)
	{
		//Select workspace
		controlHide(PNL_MAIN, "selected_1");
		controlHide(PNL_MAIN, "selected_2");
		controlShow(PNL_MAIN, "selected_3");

		controlHide(PNL_MAIN, SOURCE_GRID);
		controlHide(PNL_MAIN, WORKSPACE_SOURCE_PAGES);
		controlShow(PNL_MAIN, WORKSPACE_SELECT);
	}
}

void package_source_grid::setSource()
{
	if (m_presetMode == true)
	{
		hostNotify(bncs_string("preset_source=%1").arg(m_presetSource));
		textPut("source", m_presetSource, PNL_MAIN, TAKE_COMPONENT);
	}
	else
	{
		hostNotify(bncs_string("index=%1").arg(m_presetSource));
	}
}