#ifndef take_special_dest_audio_INCLUDED
	#define take_special_dest_audio_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"
#include "packager_common.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class take_special_dest_audio : public bncs_script_helper
{
public:
	take_special_dest_audio( bncs_client_callback * parent, const char* path );
	virtual ~take_special_dest_audio();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_packager m_packagerRouter;
	bncs_string m_compositePackager;				//c_packager_nhn or c_packager_ldc

	bncs_string m_instance;

	bncs_string m_instanceRouterAudio;
	bncs_string m_instanceRouterAudio_section_01;


	int m_dp;
	int m_displayLevel;
	int m_packagerRouterMainDevice;
	int m_deviceRouterAudio;

	void init();

	void showDP(bool init);

	void showDestAudio(bool init);
	void showDestTags(bool init);


};


#endif // take_special_dest_audio_INCLUDED