# ![alt text](../../../../../docs/common/header.jpg "Header")

# router_mapping

## Description
router_mapping is a single panel component used for button mapping.
It is actually made up of a number of components, that the router_mapping panel
brings together.

Mapping panels have 3 parts to them
* Select buttons - These are Source and Destination they switch between source
and dest views
* Router list - This is a list of source or dest to mapping
* Router Grid - This is router grid to map too

### Panel Layout
The router_mapping layouts looks like this.

![alt text](router_mapping_panel.png "router_mapping_panel")

The panel expects buttons called sources and destinations, these are used to switch between the source mapping and dest mapping.

The panel also expects 2 **mapping components** called mapping_sources and mapping_destinations

The routing panel can load different layouts, there are 2 reason for this:
* It allows the positioning of buttons to be configurable.
* It allows different parameters to be set on the mapping components.
  - Different mapping layouts can be chosen  
  - Different database can be set for the router list  

## Parameters
Name        | Type      | default           |  used
-------     | --------  | ----------------- |---
instance    | mandatory | none              | The router instance to be used
layout      | mandatory | standard_portrait | The layout to show
sourcePanel | optinal   |                   | The source panel to map
destPanel   | optinal   |                   | The dest panel to map
