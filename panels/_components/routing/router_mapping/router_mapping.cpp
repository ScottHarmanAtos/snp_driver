#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "router_mapping.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(router_mapping)

const bncs_string PNL_MAIN(1);

const bncs_string DEFAULT_LAYOUT("standard_portrait");

// constructor - equivalent to ApplCore STARTUP
router_mapping::router_mapping( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	setSize( 1 );
}

// destructor - equivalent to ApplCore CLOSEDOWN
router_mapping::~router_mapping()
{
}

// all button pushes and notifications come here
void router_mapping::buttonCallback( buttonNotify *b )
{
	if( b->panel() == 1 )
	{
		if( b->id() == "sources" )
		{
			controlHide(PNL_MAIN, "mapping_destinations");
			controlShow(PNL_MAIN, "mapping_sources");

			textPut( "statesheet=enum_selected", b->panel(), b->id() );
			textPut( "statesheet=enum_deselected", b->panel(), "destinations");
		}
		else if( b->id() == "destinations" )
		{
			controlHide(PNL_MAIN, "mapping_sources");
			controlShow(PNL_MAIN, "mapping_destinations");

			textPut( "statesheet=enum_selected", b->panel(), b->id() );
			textPut( "statesheet=enum_deselected", b->panel(), "sources");
		}
	}
}

// all revertives come here
int router_mapping::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void router_mapping::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string router_mapping::parentCallback( parentNotify *p )
{
	if( p->command() == "instance" )
	{
		m_instance = p->value();
		timerStart( 1, 1 );
	}
	else if (p->command() == "layout")
	{
		if (p->value() == "")
			m_layout = DEFAULT_LAYOUT;
		else
			m_layout = p->value();
		panelShow(PNL_MAIN, bncs_string("layouts/%1.bncs_ui").arg(m_layout));
	}
	else if( p->command() == "sourcePanel" )
	{
		m_sourcePanel = p->value();
		timerStart( 1, 1 );
	}
	else if( p->command() == "destPanel" )
	{
		m_destPanel = p->value();
		if(m_destPanel != "")
			controlShow(PNL_MAIN, "destinations");
		else
			controlHide(PNL_MAIN, "destinations");
		timerStart( 1, 1 );

	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << "layout=" + m_layout;
		sl << "sourcePanel=" + m_sourcePanel;
		sl << "destPanel=" + m_destPanel;

		return sl.toString( '\n' );
	}
	else if (p->command() == "_show")
	{
		debug("router_mapping::parentCallback _show");
		textPut("_show", PNL_MAIN, "mapping_sources"); // _show is used my the mutli mapping panel, it is set when the panel is being displayed.
		textPut("_show", PNL_MAIN, "mapping_destinations");
	}
	return "";
}

// timer events come here
void router_mapping::timerCallback( int id )
{
	timerStop( id );

	textPut("panel", m_sourcePanel, PNL_MAIN, "mapping_sources");
	textPut("panel", m_destPanel, PNL_MAIN, "mapping_destinations");

	panelTarget(PNL_MAIN, m_instance);
	//textPut("instance", m_instance, PNL_MAIN, "mapping_sources");
	//textPut("instance", m_instance, PNL_MAIN, "mapping_destinations");

	controlHide(PNL_MAIN, "mapping_destinations");
	controlShow(PNL_MAIN, "mapping_sources");

	textPut("statesheet=enum_selected", PNL_MAIN, "sources");
}
