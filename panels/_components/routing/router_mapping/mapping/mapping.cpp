#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "mapping.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(mapping)

const int PNL_MAIN(1);
const int PNL_POPUP(2);

const bncs_string DEFAULT("portrait");

// constructor - equivalent to ApplCore STARTUP
mapping::mapping( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_panel = "";
	m_pasteMode = false;
	m_database = 0;
	m_copyPaste = false;
	m_renamePage = false;
}

// destructor - equivalent to ApplCore CLOSEDOWN
mapping::~mapping()
{
}

// all button pushes and notifications come here
void mapping::buttonCallback( buttonNotify *b )
{
	if (b->panel() == PNL_MAIN)
	{
		if( b->id() == "list" )
		{
			textPut(b->command(), b->value(), PNL_MAIN, "map");
		}
		else if( b->id() == "map" )
		{
			b->dump("map");
			if (b->command() == "index")
			{
				//if (b->value() != "released"  && !m_pasteMode)
				{
					textPut("selected=next", PNL_MAIN, "list");
				}
				textPut("dirty", PNL_MAIN, "map");

				controlEnable(PNL_MAIN, "save");
				controlEnable(PNL_MAIN, "cancel");
			}
			else if (b->command() == "page")
			{
				if (b->sub(0) == "name")
				{
					m_pageName = b->value();

					if (m_copyPaste)
					{
						textPut("page_name", b->value(), PNL_MAIN, "copypaste");
					}
				}
				else if (b->sub(0) == "index")
				{
					if (m_copyPaste)
					{
						textPut("index", b->value(), PNL_MAIN, "copypaste");
					}
				}
			}
			else if (b->command() == "group")
			{
				if (b->sub(0) == "name")
				{
					m_groupName = b->value();
				}
			}
			else if (b->command() == "dirty")
			{
				controlEnable(PNL_MAIN, "save");
				controlEnable(PNL_MAIN, "cancel");
			}
			else if (b->command() == "mapping")
			{
				//if (m_copyPaste)
				{
					//Setup for the copypaste button if on the panel.
					//bncs_stringlist sl = bncs_stringlist(b->value());

					//textPut("Mapping_DB_Number", sl.getNamedParam("mapdb"), PNL_MAIN, "copypaste");
					//textPut("Mapping_DB_Offset", sl.getNamedParam("mapoffset"), PNL_MAIN, "copypaste");
					//debug("mapping::mapping = %1", sl.toString());
				}
			}
		}
		else if( b->id() == "save" )
		{
			controlDisable(PNL_MAIN, "save");
			controlDisable(PNL_MAIN, "cancel");
			controlEnable(PNL_MAIN, "renamegroups");
			controlEnable(PNL_MAIN, "rename");

			textPut("save", PNL_MAIN, "map");

			textPut( "selected=none", 1, "list" );
		}
		else if( b->id() == "cancel" )
		{
			controlDisable(PNL_MAIN, "save");
			controlDisable(PNL_MAIN, "cancel");
			controlEnable(PNL_MAIN, "renamegroups");
			controlEnable(PNL_MAIN, "rename");

			textPut("cancel", PNL_MAIN, "map");

			textPut("selected=none", PNL_MAIN, "list");
		}
		else if( b->id() == "rename" )
		{
			panelPopup(PNL_POPUP, "keyboard.bncs_ui");
			textPut("text", m_pageName, PNL_POPUP, "keyboard");
			m_renamePage = true;
			textPut("dirty.group", PNL_MAIN, "map");
			textPut("text", "Rename Page", PNL_POPUP, "rename");
		}
		else if( b->id() == "renamegroups" )
		{
			panelPopup(PNL_POPUP, "keyboard.bncs_ui");
			textPut("text", m_groupName, PNL_POPUP, "keyboard");
			m_renamePage = false;
			textPut("dirty.page", PNL_MAIN, "map");
			textPut("text", "Rename Group", PNL_POPUP, "rename");
		}
		else if(b->id() == "copypaste")
		{
			if(b->command() == "paste")
			{

				textPut("mass_mapping", b->value(), PNL_MAIN, "map");
				/*
				m_pasteMode = true;
				bncs_stringlist slMapping(b->value());
				for(int i = 0; i < slMapping.count(); ++i)
				{
					textPut(bncs_string("button.%1=released").arg(i),1,"grid");
					textPut("index",slMapping[i],1,"grid");
				}
				m_pasteMode = false;
				*/
			}
		}
	}
	else if( b->panel() == 2 )
	{
		if( b->id() == "keyboard" )
		{
			bncs_string ret;

			textGet("text", PNL_POPUP, "keyboard", ret);
			if(m_renamePage)
			{
				textPut("rename.page", ret, PNL_MAIN, "map");
				controlDisable(PNL_MAIN, "renamegroups");
			}
			else
			{
				textPut("rename.group", ret, PNL_MAIN, "map");
				controlDisable(PNL_MAIN, "rename");
			}
		}
		else
		{
			textPut("cancel", PNL_MAIN, "map");			
			controlEnable(PNL_MAIN, "renamegroups");
			controlEnable(PNL_MAIN, "rename");
		}
		panelDestroy(PNL_POPUP);
	}
}

// all revertives come here
int mapping::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void mapping::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string mapping::parentCallback( parentNotify *p )
{
	if( p->command() == "instance" )
	{
		m_instance = p->value();

		timerStart( 1, 1 );
	}
	else if (p->command() == "panelType")
	{
		if (p->value().length() == 0 && m_panelType.length() == 0)
			m_panelType = DEFAULT;
		else
			m_panelType = p->value();
		m_panelType = m_panelType.lower().replace(".bncs_ui", "");

		timerStart(1, 1);
	}
	else if( p->command() == "panel" )
	{
		if (p->value().length() == 0 && m_panel.length() == 0)
			m_panel = DEFAULT;
		else
			m_panel = p->value();
		m_panel = m_panel.lower().replace(".bncs_ui", "");

		timerStart( 1, 1 );
	}
	else if (p->command() == "database")
	{
		m_database = p->value().toInt();

		timerStart(1, 1);
	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl += "panelType=" + m_panelType;
		sl += "panel=" + m_panel;
		sl += bncs_string("database=%1").arg(m_database);

		return sl.toString( '\n' );
	}
	else if (p->command() == "_show")
	{
		debug("mapping::parentCallback _show");
		textPut("_show", PNL_MAIN, "copypaste");
	}
	return "";
}

// timer events come here
void mapping::timerCallback( int id )
{
	timerStop( id );
	init();

}

void mapping::init( void )
{
	panelDestroy(PNL_MAIN);

	panelShow(PNL_MAIN, bncs_string("layouts/%1.bncs_ui").arg(m_panelType));

	bncs_stringlist sl = getIdList(PNL_MAIN,"copypaste");

	//Check if copy and paste button is on the panel.
	if (sl.count() > 0)
		m_copyPaste = true;
	else
		m_copyPaste = false;

	//Set the name of the listbox
	if (m_database % 2)
	{
		textPut("text", "Select Dest", PNL_MAIN, "grp");
		textPut("Mapping_DB_Offset", 1000, PNL_MAIN, "copypaste");
	}
	else
	{
		textPut("text", "Select Source", PNL_MAIN, "grp");
		textPut("Mapping_DB_Offset", 0, PNL_MAIN, "copypaste");
	}

	textPut("layout", m_panel, PNL_MAIN, "map");
	textPut("database", m_database, PNL_MAIN, "list");
	setSize(PNL_MAIN);

	controlDisable(PNL_MAIN, "save");
	controlDisable(PNL_MAIN, "cancel");

	if (m_copyPaste)
		textPut("instance", m_instance, PNL_MAIN, "copypaste");

	textPut("instance", m_instance, PNL_MAIN, "map");
	textPut("instance", m_instance, PNL_MAIN, "list");

	textPut("button_mapping", PNL_MAIN, "map");
	
	textPut("statesheet=enum_selected", PNL_MAIN, "sources");
	textPut("statesheet=enum_deselected", PNL_MAIN, "destinations");
}


