#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "copy_paste.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(copy_paste)
#define TIMER_LOAD 1
#define TIMER_LOAD_DURATION 1
bncs_string LOCAL_CACHE_NAME("router_mapping_copy_paste_page");

// constructor - equivalent to ApplCore STARTUP
copy_paste::copy_paste( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( 1, "copy_paste.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel

	lc = local_cache::getLocalCache();

	m_instance = "";
	m_mapDB = 8;
	m_mapDBOffset = 0;
	m_pageIndex = -1;
	m_copyPageIndex = -1;
	m_iDevice = 0;
	controlDisable(1,"paste");

	copyLoad();
}

// destructor - equivalent to ApplCore CLOSEDOWN
copy_paste::~copy_paste()
{
}

// all button pushes and notifications come here
void copy_paste::buttonCallback( buttonNotify *b )
{
	if( b->panel() == 1 )
	{
		if(b->id() == "copy")
		{
			pageCopy();
		}
		else if(b->id() == "paste")
		{
			pagePaste();
		}
	}
}

// all revertives come here
int copy_paste::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void copy_paste::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string copy_paste::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		bncs_stringlist sl;
		
		sl << bncs_string("Mapping_DB_Number=%1").arg(m_mapDB);
		sl << bncs_string("Mapping_DB_Offset=%1").arg(m_mapDBOffset);
		
		return sl.toString( '\n' );
	}
	else if( p->command() == "Mapping_DB_Number" )
	{
		if(p->value().toInt() != m_mapDB)
		{
			m_mapDB = p->value().toInt();
			timerStart(TIMER_LOAD,TIMER_LOAD_DURATION);
		}
	}
	else if(p->command() == "Mapping_DB_Offset" )
	{
		if(p->value().toInt() != m_mapDBOffset)
		{
			m_mapDBOffset = p->value().toInt();
			timerStart(TIMER_LOAD,TIMER_LOAD_DURATION);
		}
	}
	else if(p->command() == "index")
	{
		if(p->value().toInt() != m_pageIndex)
		{
			m_pageIndex = p->value().toInt();
		}
	}
	else if (p->command() == "_show")
	{
		debug("copy_paste::parentCallback _show");
		copyLoad();
	}
	else if (p->command() == "page_name")
	{
		m_pageName = p->value();
	}
	else if(p->command() == "instance")
	{
		if(p->value() != m_instance)
		{
			m_instance = p->value();
			getDev(m_instance, &m_iThisDevice);

			int dev;
			int slot;
			char c;
			getDevSlot(m_instance, "", m_altid, &dev, &slot, &c);

			timerStart(TIMER_LOAD,TIMER_LOAD_DURATION);
		}
	}
	return "";
}

// timer events come here
void copy_paste::timerCallback( int id )
{
	if(id == TIMER_LOAD)
	{
		timerStop(TIMER_LOAD);
		if (m_iThisDevice != 0 && m_mapDBOffset != -1 && m_mapDB != -1)
			controlEnable(1,"copy");
		else
			controlDisable(1,"copy");
	}
}

void copy_paste::pageCopy()
{
	if(m_pageIndex != -1)
	{
		bncs_string s = bncs_string("instance=%1,altid=%2,pageName=%3,index=%4").arg(m_instance).arg(m_altid).arg(m_pageName).arg(m_pageIndex);
		lc->setValue(LOCAL_CACHE_NAME, s);
		copyLoad(s);
	}
	else
	{
		lc->setValue(LOCAL_CACHE_NAME, "");
	}
}

void copy_paste::copyLoad(bncs_string s)
{
	bncs_string paste = s;
	//See if there is a page stored
	if (s.length() == 0)
		lc->getValue(LOCAL_CACHE_NAME, paste);


	bncs_stringlist sl = bncs_stringlist(paste);
	bncs_string instance;
	bncs_string altid;
	bncs_string pagename;
	instance = sl.getNamedParam("instance");
	altid = sl.getNamedParam("instance");
	pagename = sl.getNamedParam("pageName");
	m_copyPageIndex = sl.getNamedParam("index");

	if (paste.length() > 0)
	{
		textPut("text", bncs_string("Copied|%1|%2").arg(altid).arg(pagename), 1, "text");
		controlEnable(1, "paste");
	}
	else
	{
		textPut("text", "", 1, "text");
		controlDisable(1, "paste");
	}

	getDev(instance, &m_iDevice);
}

void copy_paste::pagePaste()
{
	if(m_copyPageIndex != -1)
	{
	
		debug(bncs_string("copy_paste::pagePaste() iDevice:%1  iMapDB:%2 iMapOffset:%3 + m_copyPageIndex:%4").arg(m_iDevice).arg(m_mapDB).arg(m_mapDBOffset).arg(m_copyPageIndex));
		
		bncs_string mapping;
		routerName(m_iDevice,m_mapDB,m_mapDBOffset + m_copyPageIndex ,mapping);
		
		debug(bncs_string("copy_paste::pagePaste() Mapping:%1").arg(mapping));
		
		lc->setValue(LOCAL_CACHE_NAME, "");
		copyLoad();

		hostNotify(bncs_string("paste=%1").arg(mapping));
	}
}
