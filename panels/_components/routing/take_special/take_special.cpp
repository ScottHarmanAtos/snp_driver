#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "take_special.h"
#include "packager_common.h"
#include "instanceLookup.h"

#define PNL_MAIN				1
#define POPUP_VIDEO_HUB_TAG		2

#define TIMER_SETUP				1

#define	VIDEO_LEVEL_COUNT		4

#define LBL_SOURCE_PACKAGE_NAME		"source_package_name"
#define LBL_SOURCE_PACKAGE_TITLE	"source_package_title"

#define LBL_DEST_PACKAGE_NAME		"dest_package_name"
#define LBL_DEST_PACKAGE_TITLE		"dest_package_title"

#define	LBL_DEST_LEVEL_V			"video_dest"
#define	LBL_DEST_HUB_TAG_V			"dest_hub_tag_v"

#define LBL_SELECTED_TAG			"selected_tag"
#define LBL_SELECTED_NAME			"selected_name"

#define BTN_SELECT					"select"
#define BTN_CANCEL					"cancel"
#define BTN_TAKE_V					"take_v"

#define	AUDIO_PRESET_RECALL			"audio_preset_recall"

/*
#define DATABASE_SOURCE_NAME	0	//source:	button name
#define DATABASE_DEST_NAME		1	//dest:		button name
#define DATABASE_SOURCE_TITLE	2	//source:	title/long name
#define DATABASE_DEST_TITLE		3	//dest:		title/long name
#define DATABASE_AP_LEVELS		6	//source:	list of audio packages
#define DATABASE_DEST_VIDEO		7	//dest:		video#vid-hub-tag, 4x ANC#vid-hub-tag, rev_vision#vid-hub-tag, 2x IFB
#define DATABASE_SOURCE_VIDEO	11	//source:	4x (video#vid-hub-tag|4x ANC#vid-hub-tag|audio#aud-hub-tag)
#define DATABASE_DEST_LANG_TYPE	16
#define DATABASE_DEST_AUDIO_1	17	//dest:		audio  1-16 16x audio#lang-tag:type-tag,
#define DATABASE_DEST_AUDIO_2	18	//dest:		audio 17-32 16x audio#lang-tag:type-tag,

#define DATABASE_TAG_VID_HUB	41	//lookup:	vid-hub-tag
#define DATABASE_TAG_AUD_HUB	42	//lookup:	aud-hub-tag
#define DATABASE_TAG_LANGUAGE	43	//lookup:	lang-tag
#define DATABASE_TAG_AUD_TYPE	44	//lookup:	type-tag

#define DATABASE_AP_NAME		60	//ap:		name
#define DATABASE_AP_VIDEO_LANG	63	//ap:		video level and lang-tag
#define DATABASE_AP_SOURCE_PAIR	64	//ap:		12x index#tag (source pairs)
*/


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( take_special )

// constructor - equivalent to ApplCore STARTUP
take_special::take_special( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_audioPackagePage = 0;
	m_destPackageAudioPage = 0;
	m_destVideoHubTag = 0;

	for (int level = 0; level < VIDEO_LEVEL_COUNT; level++)
	{
		m_sourceVideoHubTags[level] = 0;
	}

	m_sourcePackageIndex = 0;
	m_destPackageIndex = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	init();

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
take_special::~take_special()
{
}

// all button pushes and notifications come here
void take_special::buttonCallback( buttonNotify *b )
{
	if (b->panel() == PNL_MAIN)
	{
		bncs_stringlist controlIndex(b->id(), '_');
		int index = controlIndex[1].toInt();

		if (b->id() == LBL_DEST_HUB_TAG_V)
		{
			panelPopup(POPUP_VIDEO_HUB_TAG, "popup_video_hub_tag.bncs_ui");
			initVideoHubTagPopup();

		}
		else if (b->id() == BTN_TAKE_V)
		{
			takeVideoHubTag();
		}
		else if (controlIndex[0] == "ap-page")
		{
			showAudioPackagePage(index);
		}
		else if (controlIndex[0] == "dest-audio-page")
		{
			showDestPackageAudioPage(index);
		}
		else if (controlIndex[0] == "preset")
		{
			audioPreset(index);
		}
		else if (controlIndex[0] == "video")
		{
			if (b->command() == "video_hub_tag")
			{
				m_sourceVideoHubTags[index - 1] = b->value().toInt();
				checkVideoHubTagPresent();
			}
		}
	}
	else if (b->panel() == POPUP_VIDEO_HUB_TAG)
	{
		b->dump("take_special::buttonCallback - POPUP_VIDEO_HUB_TAG");
		if (b->id() == "lvw_tags")
		{
			if (b->command() == "cell_selection")
			{
				bncs_string selectedName = b->value();
				textPut("text", selectedName, POPUP_VIDEO_HUB_TAG, LBL_SELECTED_NAME);
				//[lvw_tags: cell_selection.0.0=LDC Main
			}
			else if (b->command() == "tag")
			{
				m_selectedVideoTag = b->value().toInt();
				if (m_selectedVideoTag > 0)
				{
					textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedVideoTag), POPUP_VIDEO_HUB_TAG, LBL_SELECTED_TAG);
					controlEnable(POPUP_VIDEO_HUB_TAG, BTN_SELECT);
				}
				else
				{
					textPut("text", "", POPUP_VIDEO_HUB_TAG, LBL_SELECTED_TAG);
					controlDisable(POPUP_VIDEO_HUB_TAG, BTN_SELECT);
				}
			}
		}
		else if (b->id() == BTN_SELECT)
		{
			panelDestroy(POPUP_VIDEO_HUB_TAG);
			setEditVideoTag(m_selectedVideoTag);
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_VIDEO_HUB_TAG);
		}
	}
}

// all revertives come here
int take_special::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void take_special::databaseCallback( revertiveNotify * r )
{
	r->dump("take_special::databaseCallback()");
	if (r->device() == m_devicePackageRouter)
	{
		//Note only the primary database is visible on the child components
		if (r->database() == DATABASE_DEST_LANG_TYPE)
		{
			//Force a refresh of the audio tags
			//showAudioPackagePage(m_destPackageAudioPage);
		}
		else if (r->database() == DATABASE_DEST_LEVELS && r->index() == m_destPackageIndex)
		{
			updateVideoDestLevels(r->sInfo());
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string take_special::parentCallback( parentNotify *p )
{
	p->dump("take_special::parentCallback()");
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "myParam=%1" ).arg( m_myParam );
			
			return sl.toString( '\n' );
		}

		else if( p->value() == "myParam" )
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_myParam ) );
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if( p->command() == "myParam" )
	{	// Persisted value or 'Command' being set here
		m_myParam = p->value();
	}

	else if (p->command() == "packager_router_main_device")
	{	// Persisted value or 'Command' being set here
		//m_devicePackageRouter = p->value().toInt();
	}

	else if (p->command() == "source")
	{	// Persisted value or 'Command' being set here
		setSource(p->value());
	}

	else if (p->command() == "dest")
	{	// Persisted value or 'Command' being set here
		setDestination(p->value());
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void take_special::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void take_special::init()
{
	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);

	//Get main router device
	m_devicePackageRouter = m_packagerRouter.destDev(1);
	debug("take_special::init() m_devicePackageRouter=%1", m_devicePackageRouter);

	//Get main router instance
	m_instancePackagerRouterMain = m_packagerRouter.instance(1);

	//getDev("vip_router_video_hd_01", &m_videoRouterMainDevice);
	m_instanceVideoHD = m_packagerRouter.getConfigItem("video_router_hd");
	bncs_string instanceVideoHD_section_01 = "";
	if (instanceLookupComposite(m_instanceVideoHD, "section_01", instanceVideoHD_section_01))
	{
		getDev(instanceVideoHD_section_01, &m_deviceVideoHD);
		textPut("instance", instanceVideoHD_section_01, PNL_MAIN, LBL_DEST_LEVEL_V);
	}
	else
	{
		debug("take_special::init() instanceVideoHD.section_01 NOT FOUND");
	}
	


}

void take_special::setDestination(bncs_string dest)
{
	debug("take_special::setDestination() dest=%1 packagerRouterMainDevice=%2", dest, m_devicePackageRouter);
	m_destPackageIndex = dest.toInt();
	m_destVideoHubTag = 0;

	bncs_string destName = "---";
	bncs_string destTitle = "---";

	bncs_string destVideoLevels;
	if (m_destPackageIndex > 0)
	{
		routerName(m_devicePackageRouter, DATABASE_DEST_NAME, m_destPackageIndex, destName);
		routerName(m_devicePackageRouter, DATABASE_DEST_TITLE, m_destPackageIndex, destTitle);
		routerName(m_devicePackageRouter, DATABASE_DEST_LEVELS, m_destPackageIndex, destVideoLevels);
		routerName(m_devicePackageRouter, DATABASE_DEST_AUDIO_1, m_destPackageIndex, m_destAudioLevels1);
		routerName(m_devicePackageRouter, DATABASE_DEST_AUDIO_2, m_destPackageIndex, m_destAudioLevels2);
	}
	textPut("text", destName.replace('|', ' '), PNL_MAIN, LBL_DEST_PACKAGE_NAME);
	textPut("text", destTitle.replace('|', ' '), PNL_MAIN, LBL_DEST_PACKAGE_TITLE);

	//Video/ANC/Rev levels
	//17501=video=501#1,anc1=#1,anc2=#1,anc3=#1,anc4=#1,rev_vision=#1,ifb1=,ifb2=
	updateVideoDestLevels(destVideoLevels);

	//Temp show audio levels
	textPut("text", m_destAudioLevels1.replace(',', '|'), PNL_MAIN, "dest_audio_1_16");
	textPut("text", m_destAudioLevels2.replace(',', '|'), PNL_MAIN, "dest_audio_17_32");

	showDestPackageAudioPage(1);

	//Select destination for audio preset recall
	textPut("dest_index", m_destPackageIndex, PNL_MAIN, AUDIO_PRESET_RECALL);

	//register for database changes
	infoRegister(m_devicePackageRouter, m_destPackageIndex, m_destPackageIndex);

}


void take_special::setSource(bncs_string source)
{
	debug("take_special::setSource() source=%1 packagerRouterMainDevice=%2", source, m_devicePackageRouter);
	m_sourcePackageIndex = source.toInt();
	m_apLevels = "";

	bncs_string sourceName = "---";
	bncs_string sourceTitle = "---";

	bncs_string sourceVideoLevels;
	if (m_sourcePackageIndex > 0)
	{
		routerName(m_devicePackageRouter, DATABASE_SOURCE_NAME, m_sourcePackageIndex, sourceName);
		routerName(m_devicePackageRouter, DATABASE_SOURCE_TITLE, m_sourcePackageIndex, sourceTitle);
		routerName(m_devicePackageRouter, DATABASE_SOURCE_VIDEO, m_sourcePackageIndex, sourceVideoLevels);
		routerName(m_devicePackageRouter, DATABASE_SOURCE_AP, m_sourcePackageIndex, m_apLevels);
	}
	textPut("text", sourceName.replace('|', ' '), PNL_MAIN, LBL_SOURCE_PACKAGE_NAME);
	textPut("text", sourceTitle.replace('|', ' '), PNL_MAIN, LBL_SOURCE_PACKAGE_TITLE);

	//Video levels
	//0001=L1=0#0|0#0|0#0|0#0|0#0|0#0,L2=0#0|0#0|0#0|0#0|0#0|0#0,L3=0#0|0#0|0#0|0#0|0#0|0#0,L4=0#0|0#0|0#0|0#0|0#0|0#0
	bncs_stringlist sltSourceVideoLevels(sourceVideoLevels, ',');

	//L1=0#0|0#0|0#0|0#0|0#0|0#0
	bncs_stringlist sltL1(sltSourceVideoLevels[0], '=');
	bncs_stringlist sltL2(sltSourceVideoLevels[1], '=');
	bncs_stringlist sltL3(sltSourceVideoLevels[2], '=');
	bncs_stringlist sltL4(sltSourceVideoLevels[3], '=');

	bncs_string videoIndex;
	bncs_string videoTag;
	bncs_string videoName;

	//V#tag|ANC1#tag|ANC2#tag|ANC3#tag|ANC4#tag
	bncs_stringlist sltL1Fields(sltL1[1], '|');
	sltL1Fields[0].split('#', videoIndex, videoTag);
	routerName(m_deviceVideoHD, DATABASE_SOURCE_NAME, videoIndex.toInt(), videoName);
	textPut("text", bncs_string("[%1] %2").arg(videoIndex).arg(videoName.replace('|', ' ')), PNL_MAIN, "video_name_1");

	bncs_stringlist sltL2Fields(sltL2[1], '|');
	sltL2Fields[0].split('#', videoIndex, videoTag);
	routerName(m_deviceVideoHD, DATABASE_SOURCE_NAME, videoIndex.toInt(), videoName);
	textPut("text", bncs_string("[%1] %2").arg(videoIndex).arg(videoName.replace('|', ' ')), PNL_MAIN, "video_name_2");

	bncs_stringlist sltL3Fields(sltL3[1], '|');
	sltL3Fields[0].split('#', videoIndex, videoTag);
	routerName(m_deviceVideoHD, DATABASE_SOURCE_NAME, videoIndex.toInt(), videoName);
	textPut("text", bncs_string("[%1] %2").arg(videoIndex).arg(videoName.replace('|', ' ')), PNL_MAIN, "video_name_3");

	bncs_stringlist sltL4Fields(sltL4[1], '|');
	sltL4Fields[0].split('#', videoIndex, videoTag);
	routerName(m_deviceVideoHD, DATABASE_SOURCE_NAME, videoIndex.toInt(), videoName);
	textPut("text", bncs_string("[%1] %2").arg(videoIndex).arg(videoName.replace('|', ' ')), PNL_MAIN, "video_name_4");

	textPut("instance", m_instancePackagerRouterMain, PNL_MAIN, "video_1");
	textPut("instance", m_instancePackagerRouterMain, PNL_MAIN, "video_2");
	textPut("instance", m_instancePackagerRouterMain, PNL_MAIN, "video_3");
	textPut("instance", m_instancePackagerRouterMain, PNL_MAIN, "video_4");

	textPut("sp", m_sourcePackageIndex, PNL_MAIN, "video_1");
	textPut("sp", m_sourcePackageIndex, PNL_MAIN, "video_2");
	textPut("sp", m_sourcePackageIndex, PNL_MAIN, "video_3");
	textPut("sp", m_sourcePackageIndex, PNL_MAIN, "video_4");

	showAudioPackagePage(1);

}

void take_special::showAudioPackagePage(int page)
{
	if (m_audioPackagePage != page)
	{
		//highlight buttons
		textPut("statesheet", "enum_deselected", PNL_MAIN, bncs_string("ap-page_%1").arg(m_audioPackagePage));
	}
	m_audioPackagePage = page;
	textPut("statesheet", "enum_selected", PNL_MAIN, bncs_string("ap-page_%1").arg(m_audioPackagePage));

	//init AP
	bncs_stringlist sltApLevels(m_apLevels, '|');

	int levelOffset = (m_audioPackagePage - 1) * 8;

	debug("take_special::showAudioPackagePage() page=%1 levelOffset=%2", m_audioPackagePage, levelOffset);


	for (int level = levelOffset + 1; level <= levelOffset + 8; level++)
	{
		int ap = sltApLevels[level - 1].toInt();

		textPut("display_level", level, PNL_MAIN, bncs_string("ap_%1").arg(level - levelOffset));
		textPut("ap", ap, PNL_MAIN, bncs_string("ap_%1").arg(level - levelOffset));
		debug("take_special::setSource() audio packages: level=%1 ap=%2", level, ap);

	}
}

void take_special::showDestPackageAudioPage(int page)
{
	if (m_destPackageAudioPage != page)
	{
		//highlight buttons
		textPut("statesheet", "enum_deselected", PNL_MAIN, bncs_string("dest-audio-page_%1").arg(m_destPackageAudioPage));
	}
	m_destPackageAudioPage = page;
	textPut("statesheet", "enum_selected", PNL_MAIN, bncs_string("dest-audio-page_%1").arg(m_destPackageAudioPage));

	int levelOffset = (m_destPackageAudioPage - 1) * 8;

	debug("take_special::showDestPackageAudioPage() page=%1 levelOffset=%2", m_destPackageAudioPage, levelOffset);

	for (int level = levelOffset + 1; level <= levelOffset + 8; level++)
	{
		textPut("display_level", level, PNL_MAIN, bncs_string("dest_audio_%1").arg(level - levelOffset));
		textPut("dp", m_destPackageIndex, PNL_MAIN, bncs_string("dest_audio_%1").arg(level - levelOffset));
	}
}

void take_special::audioPreset(int preset)
{
	
	//routerName(m_packagerRouterMainDevice, DATABASE_DEST_AUDIO_1, m_destPackageIndex, m_destAudioLevels1);

	//31071=a1=30513#2:151,a2=30514#2:152,a3=30515#2:153,a4=30516#2:154,a5=30517#2:155,a6=30518#2:156,a7=30519#2:157,a8=30520#2:158,a9=0#0:0,a10=0#0:0,a11=0#0:0,a12=0#0:0,a13=0#0:0,a14=0#0:0,a15=0#0:0,a16=0#0:0
	bncs_string levels1;
	routerName(m_devicePackageRouter, DATABASE_DEST_AUDIO_1, m_destPackageIndex, levels1);
	bncs_stringlist sltLevels1(levels1, ',');
	bncs_stringlist sltNewlevels;
	for (int level = 1; level <= 16; level++)
	{
		bncs_string indexLangType = sltLevels1.getNamedParam(bncs_string("a%1").arg(level));
		bncs_stringlist sltIndexLangType(indexLangType, '#');
		int index = sltIndexLangType[0].toInt();
		bncs_stringlist sltLangType(sltIndexLangType[1], ':');
		int lang = sltLangType[0].toInt();
		int type = sltLangType[1].toInt();

		bncs_string newLevel = bncs_string("a%1=%2#%3:%4").arg(level).arg(index).arg(preset).arg(type);

		sltNewlevels.append(newLevel);
	}

	textPut("text", sltNewlevels.toString(), PNL_MAIN, "newLevels");
	routerModify(m_devicePackageRouter, DATABASE_DEST_AUDIO_1, m_destPackageIndex, sltNewlevels.toString(), false);
}

void take_special::initVideoHubTagPopup()
{
	textPut("text", "  Select Video Hub Tag from Source Package Levels", POPUP_VIDEO_HUB_TAG, "title");
	textPut("clear", POPUP_VIDEO_HUB_TAG, "lvw_tags");

	//textPut("add", bncs_string("%1;#TAG=%2").arg("[NONE]").arg(0), POPUP_VIDEO_HUB_TAG, "lvw_tags");
	for (int level = 0; level < VIDEO_LEVEL_COUNT; level++)
	{
		int tag = m_sourceVideoHubTags[level];
		if (tag > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, tag, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			int status = tagStatus.toInt();
			if (status == 1)
			{
				textPut("add", bncs_string("%1;%2;#TAG=%3").arg(level + 1).arg(tagValue).arg(tag), POPUP_VIDEO_HUB_TAG, "lvw_tags");
			}
		}
		else
		{
			textPut("add", bncs_string("%1;%2;#TAG=%3").arg(level + 1).arg("---").arg(0), POPUP_VIDEO_HUB_TAG, "lvw_tags");
		}
	}

	controlDisable(POPUP_VIDEO_HUB_TAG, BTN_SELECT);
}

void take_special::setEditVideoTag(int videoTag)
{
	debug("take_special::setEditVideoTag videoTag=%1", videoTag);

	controlEnable(PNL_MAIN, BTN_TAKE_V);
	textPut("statesheet", "enum_selected", PNL_MAIN, BTN_TAKE_V);
}

void take_special::takeVideoHubTag()
{
	debug("take_special::takeVideoHubTag() dest=%1 selectedVideoTag=%2", m_destPackageIndex, m_selectedVideoTag);
	controlDisable(PNL_MAIN, BTN_TAKE_V);
	textPut("statesheet", "enum_normal", PNL_MAIN, BTN_TAKE_V);

	bncs_string destVideoLevels;
	routerName(m_devicePackageRouter, DATABASE_DEST_LEVELS, m_destPackageIndex, destVideoLevels);
	//Video/ANC/Rev levels
	//17501=video=501#1,anc1=#1,anc2=#1,anc3=#1,anc4=#1,rev_vision=#1,ifb1=,ifb2=
	bncs_stringlist sltDestVideoLevels(destVideoLevels, ',');
	bncs_stringlist sltVideoHub(sltDestVideoLevels.getNamedParam("video"), '#');
	int destVideoIndex = sltVideoHub[0].toInt();
	textPut("index", destVideoIndex, PNL_MAIN, LBL_DEST_LEVEL_V);

	sltDestVideoLevels[0] = bncs_string("video=%1#%2").arg(destVideoIndex).arg(m_selectedVideoTag);

	//debug("take_special::takeVideoHubTag() RM device=%1 database=%2 index=%3 levels=%4", m_packagerRouterMainDevice, DATABASE_DEST_LEVELS, m_destPackageIndex, sltDestVideoLevels.toString());
	routerModify(m_devicePackageRouter, DATABASE_DEST_LEVELS, m_destPackageIndex, sltDestVideoLevels.toString(), false);
}

void take_special::updateVideoDestLevels(bncs_string levels)
{
	bncs_stringlist sltDestVideoLevels(levels, ',');
	bncs_stringlist sltVideoHub(sltDestVideoLevels.getNamedParam("video"), '#');
	int destVideoIndex = sltVideoHub[0].toInt();
	textPut("index", destVideoIndex, PNL_MAIN, LBL_DEST_LEVEL_V);

	bncs_string destVideoHubTagName = "-";
	int destVideoHubTag = sltVideoHub[1].toInt();
	if (destVideoHubTag > 0)
	{
		bncs_string tagValueStatus;
		routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, destVideoHubTag, tagValueStatus);

		bncs_string tagValue;
		bncs_string tagStatus;
		tagValueStatus.split('|', tagValue, tagStatus);

		destVideoHubTagName = tagValue;
	}
	textPut("text", destVideoHubTagName, PNL_MAIN, LBL_DEST_HUB_TAG_V);

	m_destVideoHubTag = destVideoHubTag;

	checkVideoHubTagPresent();
}

void take_special::checkVideoHubTagPresent()
{
	bool tagPresent = false;
	for (int level = 0; level < VIDEO_LEVEL_COUNT; level++)
	{
		if (m_sourceVideoHubTags[level] == m_destVideoHubTag)
		{
			tagPresent = true;
		}
	}

	if (tagPresent)
	{
		textPut("statesheet", "field_unmodified", PNL_MAIN, LBL_DEST_HUB_TAG_V);
		//textPut("colour.background", "#f0f0f0", PNL_MAIN, LBL_DEST_HUB_TAG_V);
		//textPut("colour.text", "#000000", PNL_MAIN, LBL_DEST_HUB_TAG_V);
	}
	else
	{
		textPut("statesheet", "enum_alarm", PNL_MAIN, LBL_DEST_HUB_TAG_V);
		//textPut("colour.background", "#ff0000", PNL_MAIN, LBL_DEST_HUB_TAG_V);
		//textPut("colour.text", "#ffffff", PNL_MAIN, LBL_DEST_HUB_TAG_V);
	}
}