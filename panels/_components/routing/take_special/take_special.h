#ifndef take_special_INCLUDED
	#define take_special_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class take_special : public bncs_script_helper
{
public:
	take_special( bncs_client_callback * parent, const char* path );
	virtual ~take_special();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );

	
private:
	bncs_packager m_packagerRouter;

	bncs_string m_compositePackager;				//c_packager_nhn or c_packager_ldc
	bncs_string m_instancePackagerRouterMain;		//packager_router_nhn_main or packager_router_main
	bncs_string m_instanceVideoHD;					//c_vip_router_video_hd_nhn or c_vip_router_video_hd (ldc)
	bncs_string m_instanceAudio2ch;					//

	bncs_string m_myParam;
	bncs_string m_instance;
	bncs_string m_apLevels;
	bncs_string m_destAudioLevels1;
	bncs_string m_destAudioLevels2;

	int m_audioPackagePage;
	int m_destPackageAudioPage;

	int m_deviceVideoHD;
	int m_devicePackageRouter;		//dev_1001 or dev_2001
	int m_sourcePackageIndex;
	int m_destPackageIndex;

	int m_sourceVideoHubTags[4];
	int m_selectedVideoTag;
	int m_destVideoHubTag;


	void init();
	void takeVideoHubTag();
	void setSource(bncs_string source);
	void setDestination(bncs_string dest);
	void setEditVideoTag(int videoTag);
	void showAudioPackagePage(int page);
	void showDestPackageAudioPage(int page);
	void audioPreset(int preset);
	void initVideoHubTagPopup();

	void updateVideoDestLevels(bncs_string levels);
	void checkVideoHubTagPresent();

};


#endif // take_special_INCLUDED