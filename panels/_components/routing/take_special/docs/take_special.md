# take_special Component

Headline Description here

![Example](example.png)
Example of what it should look like

## Commands

### instance
Instance of the device to take router names from

### index
The index number indication the source or destination that this button will be assigned to<br>Index must have database and instance configured too


## Notifications

### something=&lt;value&gt;
When something interesting happens this is what we're told

## Stylesheets
None