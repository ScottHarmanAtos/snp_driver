#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dest_package_audio.h"
#include "instanceLookup.h"

#define PNL_MAIN	1

#define BTN_DISPLAY_LEVEL	"display_level"
#define LBL_NAME			"name"
#define	LBL_LANGUAGE_TAG	"language"
#define	LBL_TYPE_TAG		"type"
#define	LBL_LANGUAGE_TAG_1	"language_1"
#define	LBL_TYPE_TAG_1		"type_1"
#define	LBL_LANGUAGE_TAG_2	"language_2"
#define	LBL_TYPE_TAG_2		"type_2"

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( dest_package_audio )

// constructor - equivalent to ApplCore STARTUP
dest_package_audio::dest_package_audio( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_diffAudio = 0;
	m_editAudio = 0;

	m_diffTypeTag = 0;
	m_editTypeTag = 0;
	m_diffTypeTag_1 = 0;
	m_editTypeTag_1 = 0;
	m_diffTypeTag_2 = 0;
	m_editTypeTag_2 = 0;

	m_diffLanguageTag = 0;
	m_editLanguageTag = 0;
	m_diffLanguageTag_1 = 0;
	m_editLanguageTag_1 = 0;
	m_diffLanguageTag_2 = 0;
	m_editLanguageTag_2 = 0;

	m_level = 0;

	m_devicePackageRouter = 0;
	m_deviceRouterAudio = 0;

	init();

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel


}

// destructor - equivalent to ApplCore CLOSEDOWN
dest_package_audio::~dest_package_audio()
{
}

// all button pushes and notifications come here
void dest_package_audio::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == BTN_DISPLAY_LEVEL )
		{
			hostNotify(bncs_string("display_level=%1").arg(m_level));
		}
		else if (b->id() == LBL_LANGUAGE_TAG)
		{
			hostNotify(bncs_string("language_tag=%1,%2").arg(m_level).arg(m_editLanguageTag));
		}
		else if (b->id() == LBL_LANGUAGE_TAG_1)
		{
			hostNotify(bncs_string("language_tag_1=%1,%2").arg(m_level).arg(m_editLanguageTag_1));
		}
		else if (b->id() == LBL_LANGUAGE_TAG_2)
		{
			hostNotify(bncs_string("language_tag_2=%1,%2").arg(m_level).arg(m_editLanguageTag_2));
		}
		else if (b->id() == LBL_TYPE_TAG)
		{
			hostNotify(bncs_string("type_tag=%1,%2").arg(m_level).arg(m_editTypeTag));
		}
		else if (b->id() == LBL_TYPE_TAG_1)
		{
			hostNotify(bncs_string("type_tag_1=%1,%2").arg(m_level).arg(m_editTypeTag_1));
		}
		else if (b->id() == LBL_TYPE_TAG_2)
		{
			hostNotify(bncs_string("type_tag_2=%1,%2").arg(m_level).arg(m_editTypeTag_2));
		}
	}
}

// all revertives come here
int dest_package_audio::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void dest_package_audio::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dest_package_audio::parentCallback( parentNotify *p )
{
	p->dump(bncs_string("dest_package_audio::parentCallback[%1]").arg(m_level));
	if( p->command() == "return" )
	{
		if (p->value() == "all")
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;

			sl << bncs_string("display_level=%1").arg(m_level);

			return sl.toString('\n');
		}

		else if (p->value() == "display_level")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_level));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "display_level")
	{	// Persisted value or 'Command' being set here
		m_level = p->value().toInt();
		//textPut("text", bncs_string("Aud|Pair %1").arg(m_level), PNL_MAIN, BTN_DISPLAY_LEVEL);
		int levelA = (m_level * 2) - 1;
		int levelB = levelA + 1;
		textPut("text", bncs_string("A|%1-%2").arg(levelA).arg(levelB), PNL_MAIN, BTN_DISPLAY_LEVEL);
	}
	else if (p->command() == "select_level")
	{	// Persisted value or 'Command' being set here
		int selectLevel = p->value().toInt();
		if (selectLevel > 0)
		{
			if (m_level == selectLevel)
			{
				textPut("statesheet", "enum_selected", PNL_MAIN, BTN_DISPLAY_LEVEL);
				controlEnable(PNL_MAIN, LBL_LANGUAGE_TAG);
				controlEnable(PNL_MAIN, LBL_TYPE_TAG);
			}
			else
			{
				textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
				controlDisable(PNL_MAIN, LBL_LANGUAGE_TAG);
				controlDisable(PNL_MAIN, LBL_TYPE_TAG);
			}
		}
		else
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
			controlEnable(PNL_MAIN, LBL_LANGUAGE_TAG);
			controlEnable(PNL_MAIN, LBL_TYPE_TAG);
		}
	}
	else if (p->command() == "diff_data")
	{
		updateDiffData(p->value());
	}
	else if (p->command() == "edit_data")
	{
		updateEditData(p->value());
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void dest_package_audio::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void dest_package_audio::init()
{
	/* packager_config.xml
	<packager_config>
	<data id="packager_auto">
	<setting id="audio_router" value="c_vip_router_audio_2ch_nhn" />
	*/

	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);

	//Get audio router device
	m_instanceRouterAudio = m_packagerRouter.getConfigItem("audio_router");

	debug("dest_package_editor::initEditor() m_instanceRouterAudio=%1", m_instanceRouterAudio);

	//Check if we have been passed a composite instance.
	if (instanceLookupComposite(m_instanceRouterAudio, "section_01", m_instanceRouterAudio_section_01))
	{
		getDev(m_instanceRouterAudio_section_01, &m_deviceRouterAudio);
		debug("dest_package_editor::initEditor() m_instanceRouterAudio_section_01=%1 device=%2", m_instanceRouterAudio_section_01, m_deviceRouterAudio);
	}
	else
	{
		debug("dest_package_editor::initEditor() m_instanceRouterAudio_section_01 NOT FOUND");
	}

	m_instancePackagerRouterMain = m_packagerRouter.instance(1);
	m_devicePackageRouter = m_packagerRouter.destDev(1);

	//getDev(m_instancePackagerRouterMain, &m_devicePackageRouter);


	/*
	bncs_config cfgPackagerAudioRouter(bncs_string("packager_config.packager_auto.audio_router"));
	if (cfgPackagerAudioRouter.isValid())
	{
	bncs_stringlist sltPackagerRouterAudio(cfgPackagerAudioRouter.attr("value"));
	m_instanceAudioRouter = sltPackagerRouterAudio.getNamedParam("instance");
	getDev(m_instanceAudioRouter, &m_deviceAudioRouter);
	}
	else
	{
	m_deviceAudioRouter = -1;
	}
	*/
}

void dest_package_audio::updateDiffData(bncs_string levelData)
{
	bncs_stringlist sltFields(levelData);
	debug("dest_package_audio::updateDiffData() level=%1 levelData=%2", m_level, levelData);

	if (levelData.find("audio") > -1)
	{
		m_diffAudio = sltFields.getNamedParam("audio").toInt();
		//textPut("text", bncs_string("audio=%1").arg(m_diffAudio), PNL_MAIN, LBL_NAME);
		if (m_diffAudio > 0)
		{
			bncs_string audioName;
			routerName(m_deviceRouterAudio, DATABASE_DEST_NAME, m_diffAudio, audioName);
			textPut("text", bncs_string("%1").arg(audioName.replace('|', ' ')), PNL_MAIN, LBL_NAME);
			textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_NAME);
			textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME);
		}
	}

	if (levelData.find("language_tag_0") > -1)
	{
		m_diffLanguageTag = sltFields.getNamedParam("language_tag_0").toInt();

		if (m_editLanguageTag > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_LANG_TAG, m_diffLanguageTag, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_LANGUAGE_TAG);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_LANGUAGE_TAG);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG);
		}
	}

	if (levelData.find("language_tag_1") > -1)
	{
		m_diffLanguageTag_1 = sltFields.getNamedParam("language_tag_1").toInt();

		if (m_editLanguageTag_1 > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_LANG_TAG, m_diffLanguageTag_1, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_LANGUAGE_TAG_1);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG_1);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_LANGUAGE_TAG_1);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG_1);
		}
	}

	if (levelData.find("language_tag_2") > -1)
	{
		m_diffLanguageTag_2 = sltFields.getNamedParam("language_tag_2").toInt();

		if (m_editLanguageTag_2 > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_LANG_TAG, m_diffLanguageTag_2, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_LANGUAGE_TAG_2);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG_2);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_LANGUAGE_TAG_2);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG_2);
		}
	}

	if (levelData.find("type_tag_0") > -1)
	{
		m_diffTypeTag = sltFields.getNamedParam("type_tag_0").toInt();

		if (m_diffTypeTag > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_TYPE_TAG, m_diffTypeTag, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_TYPE_TAG);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_TYPE_TAG);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG);
		}
	}

	if (levelData.find("type_tag_1") > -1)
	{
		m_diffTypeTag_1 = sltFields.getNamedParam("type_tag_1").toInt();

		if (m_diffTypeTag_1 > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_TYPE_TAG, m_diffTypeTag_1, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_TYPE_TAG_1);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG_1);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_TYPE_TAG_1);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG_1);
		}
	}

	if (levelData.find("type_tag_2") > -1)
	{
		m_diffTypeTag_2 = sltFields.getNamedParam("type_tag_2").toInt();

		if (m_diffTypeTag_2 > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_TYPE_TAG, m_diffTypeTag_2, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_TYPE_TAG_2);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG_2);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_TYPE_TAG_2);
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG_2);
		}
	}
}

void dest_package_audio::updateEditData(bncs_string levelData)
{
	debug("dest_package_audio::updateEditData() level=%1 levelData=%2", m_level, levelData);

	bncs_stringlist sltFields(levelData);

	if (levelData.find("audio") > -1)
	{
		m_editAudio = sltFields.getNamedParam("audio").toInt();
		bncs_string audioName;
		routerName(m_deviceRouterAudio, DATABASE_DEST_NAME, m_editAudio, audioName);
		textPut("text", bncs_string("%1").arg(audioName.replace('|', ' ')), PNL_MAIN, LBL_NAME);

		if (m_editAudio == m_diffAudio)
		{
			textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME);
		}
		else
		{
			textPut("statesheet", "level_dirty", PNL_MAIN, LBL_NAME);
		}
	}

	if (levelData.find("language_tag_0") > -1)
	{
		m_editLanguageTag = sltFields.getNamedParam("language_tag_0").toInt();

		if (m_editLanguageTag > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_LANG_TAG, m_editLanguageTag, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_LANGUAGE_TAG);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_LANGUAGE_TAG);
		}

		if (m_editLanguageTag == m_diffLanguageTag)
		{
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG);		
		}
		else
		{
			textPut("statesheet", "tag_dirty", PNL_MAIN, LBL_LANGUAGE_TAG);
		}
	}

	if (levelData.find("language_tag_1") > -1)
	{
		m_editLanguageTag_1 = sltFields.getNamedParam("language_tag_1").toInt();

		if (m_editLanguageTag_1 > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_LANG_TAG, m_editLanguageTag_1, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_LANGUAGE_TAG_1);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_LANGUAGE_TAG_1);
		}

		if (m_editLanguageTag_1 == m_diffLanguageTag_1)
		{
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG_1);
		}
		else
		{
			textPut("statesheet", "tag_dirty", PNL_MAIN, LBL_LANGUAGE_TAG_1);
		}
	}

	if (levelData.find("language_tag_2") > -1)
	{
		m_editLanguageTag_2 = sltFields.getNamedParam("language_tag_2").toInt();

		if (m_editLanguageTag_2 > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_LANG_TAG, m_editLanguageTag_2, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_LANGUAGE_TAG_2);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_LANGUAGE_TAG_2);
		}

		if (m_editLanguageTag_2 == m_diffLanguageTag_2)
		{
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_LANGUAGE_TAG_2);
		}
		else
		{
			textPut("statesheet", "tag_dirty", PNL_MAIN, LBL_LANGUAGE_TAG_2);
		}
	}

	if (levelData.find("type_tag_0") > -1)
	{
		m_editTypeTag = sltFields.getNamedParam("type_tag_0").toInt();

		if (m_editTypeTag > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_TYPE_TAG, m_editTypeTag, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_TYPE_TAG);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_TYPE_TAG);
		}

		if (m_editTypeTag == m_diffTypeTag)
		{
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG);
		}
		else
		{
			textPut("statesheet", "tag_dirty", PNL_MAIN, LBL_TYPE_TAG);
		}
	}

	if (levelData.find("type_tag_1") > -1)
	{
		m_editTypeTag_1 = sltFields.getNamedParam("type_tag_1").toInt();

		if (m_editTypeTag_1 > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_TYPE_TAG, m_editTypeTag_1, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_TYPE_TAG_1);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_TYPE_TAG_1);
		}

		if (m_editTypeTag_1 == m_diffTypeTag_1)
		{
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG_1);
		}
		else
		{
			textPut("statesheet", "tag_dirty", PNL_MAIN, LBL_TYPE_TAG_1);
		}
	}

	if (levelData.find("type_tag_2") > -1)
	{
		m_editTypeTag_2 = sltFields.getNamedParam("type_tag_2").toInt();

		if (m_editTypeTag_2 > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_TYPE_TAG, m_editTypeTag_2, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_TYPE_TAG_2);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_TYPE_TAG_2);
		}

		if (m_editTypeTag_2 == m_diffTypeTag_2)
		{
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_TYPE_TAG_2);
		}
		else
		{
			textPut("statesheet", "tag_dirty", PNL_MAIN, LBL_TYPE_TAG_2);
		}
	}
}

