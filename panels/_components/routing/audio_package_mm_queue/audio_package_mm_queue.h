#ifndef audio_package_mm_queue_INCLUDED
	#define audio_package_mm_queue_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_package_mm_queue : public bncs_script_helper
{
public:
	audio_package_mm_queue( bncs_client_callback * parent, const char* path );
	virtual ~audio_package_mm_queue();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	
	bncs_string m_compositePackager;	//c_packager_nhn or c_packager_ldc

	bncs_string m_instancePackagerRouterMain;	//packager_router_nhn_main or packager_router_main

	bncs_string m_queue;

	bncs_packager m_packagerRouter;
	bncs_packager m_packagerMMStack;

	int m_devicePackageRouter;			//dev_1001 or dev_2001
	int m_devicePackageMMStack;			//dev_1021-1036 or dev_2021-2036
	int m_slotPackageMMStack;

	int m_AP;

	int m_selected;

	void init();
	void initAP();
	void sendCommand(bncs_string command);
	void updateQueue();

};


#endif // audio_package_mm_queue_INCLUDED