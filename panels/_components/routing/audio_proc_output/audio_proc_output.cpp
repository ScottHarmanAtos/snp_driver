#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "audio_proc_output.h"

#define PNL_MAIN				1
#define POPUP_AUDIO_TYPE_TAG	2
#define POPUP_CONFIRM_RESTORE	3

#define TIMER_SETUP	1

#define AP_INDEX			"ap_index"
#define AP_NAME				"ap_name"

#define OUTPUT_SOURCE		"output_source"
#define OUTPUT_LINE			"output_line"
#define AUDIO_PAIR			"audio_pair"
#define AUDIO_SOURCE		"audio_source"
#define TYPE_TAG			"type_tag"

#define BTN_CANCEL			"cancel"
#define BTN_RESTORE			"restore"
#define BTN_SET				"set"
#define BTN_SELECT_DEFAULT	"select_default"

#define AUDIO_PACKAGE_PAIR_COUNT	12

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( audio_proc_output )

// constructor - equivalent to ApplCore STARTUP
audio_proc_output::audio_proc_output( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_deviceRouterAudio = 0;
	m_devicePackagerRouterMain = 0;
	m_output = 0;
	m_audioSourceIndex = 0;
	m_audioPackageIndex = 0;
	m_audioPackagePair = 0;
	m_audioPackageProc = 0;
	m_currentTypeTag = 0;
	m_selectedTypeTag = 0;
	m_defaultTypeTag = 0;
	m_audioSourceRestore = false;

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
audio_proc_output::~audio_proc_output()
{
}

// all button pushes and notifications come here
void audio_proc_output::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == TYPE_TAG )
		{
			panelPopup(POPUP_AUDIO_TYPE_TAG, "popup_audio_type_tag.bncs_ui");
			initAudioTypeTagPopup(m_audioPackagePair, m_currentTypeTag);
		}
		else if (b->id() == AUDIO_SOURCE || b->id() == AUDIO_PAIR)
		{
			if (m_audioSourceRestore)
			{
				panelPopup(POPUP_CONFIRM_RESTORE, "popup_confirm_restore.bncs_ui");
			}
		}
	}
	else if (b->panel() == POPUP_CONFIRM_RESTORE)
	{
		if (b->id() == BTN_RESTORE)
		{
			restoreSource();
			panelDestroy(POPUP_CONFIRM_RESTORE);
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_CONFIRM_RESTORE);
		}
	}
	else if (b->panel() == POPUP_AUDIO_TYPE_TAG)
	{
		b->dump("audio_proc_output::buttonCallback - POPUP_AUDIO_TYPE_TAG");
		if (b->id() == "lvw_tags")
		{
			if (b->command() == "cell_selection")
			{
				bncs_string selectedName = b->value();
				textPut("text", selectedName, POPUP_AUDIO_TYPE_TAG, "selected_name");
				//[lvw_tags: cell_selection.0.0=LDC Main
			}
			else if (b->command() == "tag")
			{
				m_selectedTypeTag = b->value().toInt();
				textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedTypeTag), POPUP_AUDIO_TYPE_TAG, "selected_tag");
				controlEnable(POPUP_AUDIO_TYPE_TAG, BTN_SET);
			}
		}
		else if (b->id() == BTN_SELECT_DEFAULT)
		{
			textPut("selectTag", m_defaultTypeTag, POPUP_AUDIO_TYPE_TAG, "lvw_tags");
			m_selectedTypeTag = m_defaultTypeTag;
			controlEnable(POPUP_AUDIO_TYPE_TAG, BTN_SET);

			bncs_string selectedName = getTypeTagName(m_selectedTypeTag);
			textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedTypeTag), POPUP_AUDIO_TYPE_TAG, "selected_tag");
			textPut("text", selectedName, POPUP_AUDIO_TYPE_TAG, "selected_name");
		}
		else if (b->id() == BTN_SET)
		{
			panelDestroy(POPUP_AUDIO_TYPE_TAG);
			setTypeTag();
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_AUDIO_TYPE_TAG);
		}

	}
}

// all revertives come here
int audio_proc_output::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void audio_proc_output::databaseCallback( revertiveNotify * r )
{
	if (r->device() == m_devicePackagerRouterMain)
	{
		if (r->index() == m_audioPackageIndex)
		{
			if (r->database() == DATABASE_AP_PAIRS)
			{
				updateAPLevel(r->sInfo());
			}
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string audio_proc_output::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "output=%1" ).arg( m_output );
			
			return sl.toString( '\n' );
		}

		else if( p->value() == "output" )
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_output));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if( p->command() == "output" )
	{	// Persisted value or 'Command' being set here
		m_output = p->value().toInt();
	}

	else if (p->command() == "output_description")
	{
		m_outputDescription = p->value();
		textPut("text", m_outputDescription, PNL_MAIN, "output_description");
	}

	else if (p->command() == "instance_router_audio")
	{
		m_instanceRouterAudio_section_01 = p->value();
		getDev(m_instanceRouterAudio_section_01, &m_deviceRouterAudio);
	}

	else if (p->command() == "instance_packager_router_main")
	{
		m_instancePackagerRouterMain = p->value();
		getDev(m_instancePackagerRouterMain, &m_devicePackagerRouterMain);
	}

	else if (p->command() == "audio_source_index")
	{
		m_audioSourceIndex = p->value().toInt();
		
		if (m_audioSourceIndex > 0)
		{
			textPut("text", getAudioSourceName(m_audioSourceIndex), PNL_MAIN, OUTPUT_SOURCE);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, OUTPUT_SOURCE);

		}

		initOutputView();
	}

	else if (p->command() == "audio_package_index")
	{
		m_audioPackageIndex = p->value().toInt();
		textPut("text", getAudioPackageName(m_audioPackageIndex), PNL_MAIN, AP_NAME);
		textPut("text", bncs_string("AP %1").arg(m_audioPackageIndex), PNL_MAIN, AP_INDEX);
	}

	else if (p->command() == "output_type_default")
	{
		m_defaultTypeTag = p->value().toInt();
	}

	else if (p->command() == "audio_package_proc")
	{
		m_audioPackageProc = p->value().toInt();
		showProcAudioPair();
		
		bncs_string apLevels;
		routerName(m_devicePackagerRouterMain, DATABASE_AP_PAIRS, m_audioPackageIndex, apLevels);
		updateAPLevel(apLevels);
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void audio_proc_output::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


bncs_string audio_proc_output::getAudioPackageName(int index)
{
	bncs_string name;
	routerName(m_devicePackagerRouterMain, DATABASE_AP_NAME, index, name);

	return name.replace('|', ' ');
}

bncs_string audio_proc_output::getAudioSourceName(int index)
{
	bncs_string name;
	routerName(m_deviceRouterAudio, DATABASE_SOURCE_NAME, index, name);

	return name;
}

bncs_string audio_proc_output::getTypeTagName(int tag)
{
	bncs_string typeNameStatus;
	routerName(m_devicePackagerRouterMain, DATABASE_TYPE_TAG, tag, typeNameStatus);

	bncs_stringlist sltNameStatus(typeNameStatus, '|');
	return sltNameStatus[0];
}

void audio_proc_output::initOutputView()
{
	m_audioSourceRestore = false;

	if (m_audioSourceIndex > 0)
	{
		controlShow(PNL_MAIN, AP_INDEX);
		controlShow(PNL_MAIN, AP_NAME);
		controlShow(PNL_MAIN, TYPE_TAG);
		controlShow(PNL_MAIN, OUTPUT_LINE);
		controlShow(PNL_MAIN, AUDIO_PAIR);
		controlShow(PNL_MAIN, AUDIO_SOURCE);
		controlEnable(PNL_MAIN, OUTPUT_SOURCE);
		//textPut("text", bncs_string("Input %1 DP %2 Level %3").arg(m_input).arg(m_destPackageIndex).arg(m_destPackageLevel), PNL_MAIN, "input_level");
	}
	else
	{
		controlHide(PNL_MAIN, AP_INDEX);
		controlHide(PNL_MAIN, AP_NAME);
		controlHide(PNL_MAIN, TYPE_TAG);
		controlHide(PNL_MAIN, OUTPUT_LINE);
		controlHide(PNL_MAIN, AUDIO_PAIR);
		controlHide(PNL_MAIN, AUDIO_SOURCE);
		controlDisable(PNL_MAIN, OUTPUT_SOURCE);
		//textPut("text", "", PNL_MAIN, "input_level");
	}

	bncs_string audioDestTags;
	//routerName(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_destPackageIndex, audioDestTags);
	//showDestPackageTags(audioDestTags);

	//register for database changes
	infoRegister(m_devicePackagerRouterMain, 1, 1);
}

void audio_proc_output::initAudioTypeTagPopup(int level, int typeTag)
{
	textPut("text", bncs_string("  Set Audio Proc Output Type Tag - Audio Package Pair: %1").arg(level), POPUP_AUDIO_TYPE_TAG, "title");

	bncs_string defaultTypeName = "-";
	if (m_defaultTypeTag > 0)
	{
		defaultTypeName = getTypeTagName(m_defaultTypeTag);
	}
	else
	{
		controlDisable(POPUP_AUDIO_TYPE_TAG, BTN_SELECT_DEFAULT);
	}
	textPut("text", defaultTypeName, POPUP_AUDIO_TYPE_TAG, "default_type");

	textPut("add", bncs_string("%1;#TAG=%2").arg("[NONE]").arg(0), POPUP_AUDIO_TYPE_TAG, "lvw_tags");
	for (int tag = 1; tag <= 500; tag++)
	{
		bncs_string tagValueStatus;
		routerName(m_devicePackagerRouterMain, DATABASE_TYPE_TAG, tag, tagValueStatus);

		bncs_string tagValue;
		bncs_string tagStatus;
		tagValueStatus.split('|', tagValue, tagStatus);

		int status = tagStatus.toInt();
		if (status == 1)
		{
			textPut("add", bncs_string("%1;#TAG=%2").arg(tagValue).arg(tag), POPUP_AUDIO_TYPE_TAG, "lvw_tags");
		}
	}

	textPut("selectTag", typeTag, POPUP_AUDIO_TYPE_TAG, "lvw_tags");
	m_selectedTypeTag = 0;
	controlDisable(POPUP_AUDIO_TYPE_TAG, BTN_SET);
}

void audio_proc_output::showProcAudioPair()
{
	bncs_string pairLabel = "";

	//Get audio proc 1 & 2 and usage on levels 1-12 from database 69
	//2501=proc_1=ldc_proc_001,proc_2=ldc_proc_002,a1=,a2=,a3=,a4=,a5=1.1,a6=1.2,a7=,a8=,a9=,a10=,a11=,a12=
	bncs_string apAudioProcDatabase;
	routerName(m_devicePackagerRouterMain, DATABASE_AP_AUDIO_PROC, m_audioPackageIndex, apAudioProcDatabase);
	bncs_stringlist apAudioProcFields(apAudioProcDatabase);

	for (int pair = 1; pair <= AUDIO_PACKAGE_PAIR_COUNT; pair++)
	{
		bncs_string usage = apAudioProcFields.getNamedParam(bncs_string("a%1").arg(pair));
		bncs_string splitProc;
		bncs_string splitOutput;
		usage.split('.', splitProc, splitOutput);
		int proc = splitProc.toInt();
		int output = splitOutput.toInt();
		if (proc == m_audioPackageProc && output == m_output)
		{
			m_audioPackagePair = pair;
			pairLabel = bncs_string("Aud Pair %1").arg(pair);
		}
	}

	textPut("text", pairLabel, PNL_MAIN, AUDIO_PAIR);
}

void audio_proc_output::updateAPLevel(bncs_string apLevels)
{
	//2561=a1=981#151,a2=982#152,a3=983#153,a4=984#154,a5=985#155,a6=986#156,a7=987#157,a8=988#158,a9=0#0,a10=0#0,a11=0#0,a12=0#0
	bncs_stringlist sltAPLevels(apLevels);
	bncs_string pairIndexType = sltAPLevels.getNamedParam(bncs_string("a%1").arg(m_audioPackagePair));
	bncs_string splitIndex;
	bncs_string splitType;
	pairIndexType.split('#', splitIndex, splitType);
	int sourceIndex = splitIndex.toInt();
	int typeTagNum = splitType.toInt();

	bncs_string sourceName = "---";
	if (sourceIndex > 0)
	{
		sourceName = getAudioSourceName(sourceIndex);
	}
	textPut("text", sourceName.replace('|', ' '), PNL_MAIN, AUDIO_SOURCE);

	if (sourceIndex == m_audioSourceIndex)
	{
		textPut("colour.background", "#000000", PNL_MAIN, AUDIO_SOURCE);
		m_audioSourceRestore = false;
	}
	else
	{
		textPut("colour.background", "#800000", PNL_MAIN, AUDIO_SOURCE);
		m_audioSourceRestore = true;
	}

	bncs_string typeTagName = "-";
	if (typeTagNum > 0)
	{
		typeTagName = getTypeTagName(typeTagNum);
	}
	textPut("text", typeTagName, PNL_MAIN, TYPE_TAG);

	m_currentTypeTag = typeTagNum;
}

void audio_proc_output::setTypeTag()
{
	bncs_string apLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_AP_PAIRS, m_audioPackageIndex, apLevels);
	//2561=a1=981#151,a2=982#152,a3=983#153,a4=984#154,a5=985#155,a6=986#156,a7=987#157,a8=988#158,a9=0#0,a10=0#0,a11=0#0,a12=0#0
	bncs_stringlist sltAPLevels(apLevels);

	for (int level = 0; level < sltAPLevels.count(); level++)
	{
		bncs_string levelData = sltAPLevels[level];
		//a1=981#151
		bncs_string levelKey;
		bncs_string pairIndexType;
		levelData.split('=', levelKey, pairIndexType);

		//check if this is the level to be modified
		if (levelKey == bncs_string("a%1").arg(m_audioPackagePair))
		{
			bncs_string splitIndex;
			bncs_string splitType;
			pairIndexType.split('#', splitIndex, splitType);
			int sourceIndex = splitIndex.toInt();
			int typeTagNum = splitType.toInt();

			//Check if a different type tag needs to be set
			if (typeTagNum != m_selectedTypeTag)
			{
				bncs_string newLevelData = bncs_string("%1=%2#%3").arg(levelKey).arg(sourceIndex).arg(m_selectedTypeTag);
				sltAPLevels[level] = newLevelData;
				routerModify(m_devicePackagerRouterMain, DATABASE_AP_PAIRS, m_audioPackageIndex, sltAPLevels.toString(), false);
				break;
			}
		}
	}
}

void audio_proc_output::restoreSource()
{
	bncs_string apLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_AP_PAIRS, m_audioPackageIndex, apLevels);
	//2561=a1=981#151,a2=982#152,a3=983#153,a4=984#154,a5=985#155,a6=986#156,a7=987#157,a8=988#158,a9=0#0,a10=0#0,a11=0#0,a12=0#0
	bncs_stringlist sltAPLevels(apLevels);

	for (int level = 0; level < sltAPLevels.count(); level++)
	{
		bncs_string levelData = sltAPLevels[level];
		//a1=981#151
		bncs_string levelKey;
		bncs_string pairIndexType;
		levelData.split('=', levelKey, pairIndexType);

		//check if this is the level to be modified
		if (levelKey == bncs_string("a%1").arg(m_audioPackagePair))
		{
			bncs_string splitIndex;
			bncs_string splitType;
			pairIndexType.split('#', splitIndex, splitType);
			int sourceIndex = splitIndex.toInt();
			int typeTagNum = splitType.toInt();

			//Check if a different type tag needs to be set
			if (sourceIndex != m_audioSourceIndex)
			{
				bncs_string newLevelData = bncs_string("%1=%2#%3").arg(levelKey).arg(m_audioSourceIndex).arg(typeTagNum);
				sltAPLevels[level] = newLevelData;
				routerModify(m_devicePackagerRouterMain, DATABASE_AP_PAIRS, m_audioPackageIndex, sltAPLevels.toString(), false);
				break;
			}
		}
	}
}