#ifndef audio_proc_output_INCLUDED
	#define audio_proc_output_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_proc_output : public bncs_script_helper
{
public:
	audio_proc_output( bncs_client_callback * parent, const char* path );
	virtual ~audio_proc_output();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_instancePackagerRouterMain;
	bncs_string m_instanceRouterAudio_section_01;

	bncs_string m_outputDescription;

	int m_deviceRouterAudio;
	int m_devicePackagerRouterMain;
	int m_output;
	int m_audioSourceIndex;
	int m_audioPackageIndex;
	int m_audioPackagePair;
	int m_audioPackageProc;
	int m_currentTypeTag;
	int m_selectedTypeTag;
	int m_defaultTypeTag;

	bool m_audioSourceRestore;

	bncs_string getAudioPackageName(int index);
	bncs_string getAudioSourceName(int index);
	bncs_string getTypeTagName(int tag);

	void initAudioTypeTagPopup(int level, int typeTag);
	void initOutputView();
	void showProcAudioPair();
	void updateAPLevel(bncs_string apLevels);
	void setTypeTag();
	void restoreSource();
};


#endif // audio_proc_output_INCLUDED