#ifndef oska_mode_INCLUDED
	#define oska_mode_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class oska_mode : public bncs_script_helper
{
public:
	oska_mode( bncs_client_callback * parent, const char* path );
	virtual ~oska_mode();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	bncs_string m_workspace;
	
	bncs_string m_compositePackager;				//c_packager
	bncs_packager m_packagerRouter;

	int m_devicePackagerRouterMain;
	int m_selectedLanguageTag;

	void init();
	void setMasterLanguage(int languageTag);
	void updateMasterLanguage(int languageTag);
};


#endif // oska_mode_INCLUDED