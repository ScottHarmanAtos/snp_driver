# lock Component

This is a simple control to set and show lock.

The control takes instance and index and displays the current lock state (using the stylesheet entries below). On pressing the button on this control the lock state is toggled..

## Commands
| Name | Use |
|------|------|
| instance | Normal instance targeting |
| index | The index of on the lock device |
| dest_with_instance | Instance and index set in one. instance,index |

## Notifications
None

Stylesheets
| Name | Use |
|------|------|
| enum_unknown | When a control is targeted but the value has not been returned from the device
| dest_locked | When the destination is locked (1) |
| dest_unlocked | When the destination is not locked (0) |

 