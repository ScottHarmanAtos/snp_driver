#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "lock.h"


#define TIMER_START 1
#define TIMEOUT_START 20
#define PANEL_MAIN 1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( lock )

// constructor - equivalent to ApplCore STARTUP
lock::lock( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_device = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow(PANEL_MAIN, "main.bncs_ui");
	controlDisable( PANEL_MAIN, "button" );
}

// destructor - equivalent to ApplCore CLOSEDOWN
lock::~lock()
{ 
}
 
// all button pushes and notifications come here
void lock::buttonCallback( buttonNotify *b )
{
	if( m_device && (m_index > 0 ))
	{
		if( m_state )
			infoWrite( m_device, "0", m_index );
		else
			infoWrite( m_device, "1", m_index );
	}
}

// all revertives come here
int lock::revertiveCallback( revertiveNotify * r )
{
	if(( r->index() == m_index ) && (r->device() == m_device ))
	{
		m_state = atoi( r->sInfo() );

		if( m_state )
			textPut("statesheet", "dest_locked", PANEL_MAIN, "button");
		else
		 	textPut( "statesheet", "dest_unlocked", PANEL_MAIN, "button" );
	}
	return 0;
}
 
// all database name changes come back here
void lock::databaseCallback( revertiveNotify * r )
{
}



// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string lock::parentCallback( parentNotify *p )
{
	if( p->command() == "instance" )
	{
		m_instance = p->value();
		timerStart(TIMER_START, TIMEOUT_START);

	}
	else if (p->command() == "index")
	{
		m_index = p->value().toInt();
		timerStart(TIMER_START, TIMEOUT_START);
	}
	else if (p->command() == "dest_with_instance")
	{
		bncs_stringlist slt = bncs_stringlist(p->value(), ',');
		m_instance = bncs_string("%1%2").arg(slt[0]).arg(m_strSuffix);
		m_index = slt[1].toInt();
		timerStart(TIMER_START, TIMEOUT_START);
	}
	else if (p->command() == "suffix") // WHAT DOES THIS DO - ADP
	{
		m_strSuffix = p->value();
	}
	else if (p->command() == "_commands")
	{
		bncs_stringlist sl;

		sl << "instance=<value>";
		sl << "index=<value>";
		sl << "dest_with_instance=<instance,index>";

		return sl.toString('\n');
	}
	else if (p->command() == "return")
	{
		bncs_stringlist slt;
		slt << bncs_string("suffix=%1").arg(m_strSuffix);
		return slt.toString('\n');
	}

	return "";
}

// timer events come here
void lock::timerCallback( int id )
{
	if ( id== TIMER_START)
	{
		
		timerStop(id);
		init();
	}
}



void lock::init()
{
	getDev(m_instance, &m_device);
	debug("lock::init - inst=%1, dev=%2, index=%3", m_instance, m_device, m_index);
	if (m_device && (m_index > 0))
	{
		controlEnable(PANEL_MAIN, "button");

		// put a value to the button just to indicate this is the unknown state
		textPut("statesheet", "enum_unknown", PANEL_MAIN, "button");

		// go off and get the lock state
		infoRegister(m_device, m_index, m_index);
		infoPoll(m_device, m_index, m_index);
	}
	else
		controlDisable(PANEL_MAIN, "button");
}

