#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dest_package_rev_audio.h"
#include "instanceLookup.h"

#define PNL_MAIN	1

#define BTN_DISPLAY_LEVEL	"display_level"
#define LBL_NAME			"name"


#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( dest_package_rev_audio )

// constructor - equivalent to ApplCore STARTUP
dest_package_rev_audio::dest_package_rev_audio( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_diffAudio = 0;
	m_editAudio = 0;
	m_level = 0;

	init();

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
dest_package_rev_audio::~dest_package_rev_audio()
{
}

// all button pushes and notifications come here
void dest_package_rev_audio::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == BTN_DISPLAY_LEVEL )
		{
			hostNotify(bncs_string("display_level=%1").arg(m_level));
		}
	}
}

// all revertives come here
int dest_package_rev_audio::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void dest_package_rev_audio::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dest_package_rev_audio::parentCallback( parentNotify *p )
{
	p->dump(bncs_string("dest_package_rev_audio::parentCallback[%1]").arg(m_level));
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("display_level=%1").arg(m_level);
			
			return sl.toString( '\n' );
		}

		else if (p->value() == "display_level")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_level));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "display_level")
	{	// Persisted value or 'Command' being set here
		m_level = p->value().toInt();
		textPut("text", bncs_string("Rev|A %1").arg(m_level), PNL_MAIN, BTN_DISPLAY_LEVEL);
	}
	else if (p->command() == "select_level")
	{	// Persisted value or 'Command' being set here
		int selectLevel = p->value().toInt();
		if (selectLevel > 0)
		{
			if (m_level == selectLevel)
			{
				textPut("statesheet", "enum_selected", PNL_MAIN, BTN_DISPLAY_LEVEL);
			}
			else
			{
				textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
			}
		}
		else
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
		}
	}
	else if (p->command() == "diff_data")
	{
		updateDiffData(p->value());
	}
	else if (p->command() == "edit_data")
	{
		updateEditData(p->value());
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void dest_package_rev_audio::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void dest_package_rev_audio::init()
{
	/* packager_config.xml
	<packager_config>
	<data id="packager_auto">
	<setting id="audio_router" value="c_vip_router_audio_2ch_nhn" />
	*/

	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);

	//Get audio router device
	m_instanceRouterAudio = m_packagerRouter.getConfigItem("audio_router");

	debug("dest_package_editor::initEditor() m_instanceRouterAudio=%1", m_instanceRouterAudio);

	//Check if we have been passed a composite instance.
	if (instanceLookupComposite(m_instanceRouterAudio, "section_01", m_instanceRouterAudio_section_01))
	{
		getDev(m_instanceRouterAudio_section_01, &m_deviceRouterAudio);
		debug("dest_package_editor::initEditor() m_instanceRouterAudio_section_01=%1 device=%2", m_instanceRouterAudio_section_01, m_deviceRouterAudio);
	}
	else
	{
		debug("dest_package_editor::initEditor() m_instanceRouterAudio_section_01 NOT FOUND");
	}

	m_instancePackagerRouterMain = m_packagerRouter.instance(1);
	m_devicePackageRouter = m_packagerRouter.destDev(1);
	/*
	getDev(INSTANCE_PACKAGE_ROUTER, &m_devicePackageRouter);

	bncs_config cfgPackagerAudioRouter(bncs_string("packager_config.packager_auto.audio_router"));
	if (cfgPackagerAudioRouter.isValid())
	{
		bncs_stringlist sltPackagerRouterAudio(cfgPackagerAudioRouter.attr("value"));
		m_instanceAudioRouter = sltPackagerRouterAudio.getNamedParam("instance");
		getDev(m_instanceAudioRouter, &m_deviceAudioRouter);
	}
	else
	{
		m_deviceAudioRouter = -1;
	}
	*/

}

void dest_package_rev_audio::updateDiffData(bncs_string levelData)
{
	bncs_stringlist sltFields(levelData);
	debug("dest_package_rev_audio::updateDiffData() level=%1 levelData=%2", m_level, levelData);

	if (levelData.find("audio") > -1)
	{
		m_diffAudio = sltFields.getNamedParam("audio").toInt();
		//textPut("text", bncs_string("audio=%1").arg(m_diffAudio), PNL_MAIN, LBL_NAME);
		if (m_diffAudio > 0)
		{
			bncs_string audioName;
			routerName(m_deviceRouterAudio, DATABASE_DEST_NAME, m_diffAudio, audioName);
			textPut("text", bncs_string("%1").arg(audioName.replace('|', ' ')), PNL_MAIN, LBL_NAME);
			textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_NAME);
			textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME);
		}
	}


}

void dest_package_rev_audio::updateEditData(bncs_string levelData)
{
	debug("dest_package_rev_audio::updateEditData() level=%1 levelData=%2", m_level, levelData);

	bncs_stringlist sltFields(levelData);

	if (levelData.find("audio") > -1)
	{
		m_editAudio = sltFields.getNamedParam("audio").toInt();
		bncs_string audioName;
		routerName(m_deviceRouterAudio, DATABASE_SOURCE_NAME, m_editAudio, audioName);
		textPut("text", bncs_string("%1").arg(audioName.replace('|', ' ')), PNL_MAIN, LBL_NAME);

		if (m_editAudio == m_diffAudio)
		{
			textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME);
		}
		else
		{
			textPut("statesheet", "level_dirty", PNL_MAIN, LBL_NAME);
		}
	}


}

