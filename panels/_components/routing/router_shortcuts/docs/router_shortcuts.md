# router_shortcuts Component

## Description
This connections component provides fast selection of a configurable number of router sources
It dynamically adjusts the number of indices is is expecting, based on the number of source buttons it finds on the configured UI.
It will either calculate the port name, based on instance, source_index (hard coded to database 0), or you may set an "alias" for the button at design time

![as_used_on_panel](as_used_on_panel.png)

![config](config.png)

## Static Configuration

Configuration is held the UI, and as indicated above, modified by the selected UI layout.
			
						
## Commands
| Type | Scope | Type | When set | Example | Use |
|------|------|------|------|------|------|
|instance | mandatory |bncs_string | Design time |rtr_sdi_a | router instance |
| layout | mandatory | bncs_string | Design time | two_by_two | layout mimic from layouts folder. omit folder and .bncs_ui bit |
| background colour | mandatory | bncs_string | Design time | 1 or 2 | html or valid QT colour; used to set the colour of background between buttons to match the parent background colour |
| source_index_1 | optional | int | Design time | 12 | optional, in that if you don't set it nothing will happen |
| source_alias_1 | optional | bncs_string | Design time | Fred | manually override the calculated port name (leave blank for auto) |
| source_index_2 | optional | int | Design time | 45 | second source..
| source_alias_2 | optional | bncs_string | Design time | second manual override..
| source | optional | int | run time | 45 | inbound connection message -used to highlight the source if matched (i.e. in sympathy with grid selection) |

## Notifications

| Command | Example / Value | Use |
|------|------|------|
| index | \<router index> | router index associated with selected button |

## Stylesheets
None

## Developers Notes 1
If you choose a layout with less buttons than the previous. you will lose data. Sorry

## Developers Notes 2
Is it worth it<br>
A new winter coat and shoes for the wife<br>
And a bicycle on the boy's birthday<br>
It's just a rumour that was spread around town

Tim Hall
October 2016
