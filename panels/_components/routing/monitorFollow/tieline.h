// tieline.h: interface for the tieline class.
//
//////////////////////////////////////////////////////////////////////
#include "bncs_script_helper.h"
#include "point.h"
#if !defined(AFX_TIELINE_H__927AE807_C1B0_4ADC_B385_2AC240140FB5__INCLUDED_)
#define AFX_TIELINE_H__927AE807_C1B0_4ADC_B385_2AC240140FB5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000s

class tieline  
{
public:
	tieline();
	tieline(point Dest, point Source, point DestSlave);
	tieline(point Dest, point Source);
	tieline(point Dest);
	virtual ~tieline();

	bncs_string RoutedSourceName;
	int RoutedSourceIndex;
	point Dest;
	point Source;
	point DestSlave;

	bool IsUnderPackage;
	int PackagerDevice;
};

#endif // !defined(AFX_TIELINE_H__927AE807_C1B0_4ADC_B385_2AC240140FB5__INCLUDED_)
