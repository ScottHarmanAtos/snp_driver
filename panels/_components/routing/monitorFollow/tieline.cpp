// tieline.cpp: implementation of the tieline class.
//
//////////////////////////////////////////////////////////////////////

#include "tieline.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

tieline::tieline()
{
	IsUnderPackage = false;
	PackagerDevice = 0;
	RoutedSourceIndex = 0;
}

tieline::tieline(point Dest, point Source, point DestSlave) :tieline(Dest, Source)
{
	this->DestSlave = DestSlave;
}

tieline::tieline(point Dest, point Source):tieline(Dest)
{
	this->Source = Source;
}

tieline::tieline(point Dest) :tieline()
{
	this->Dest = Dest;
}

tieline::~tieline()
{

}

