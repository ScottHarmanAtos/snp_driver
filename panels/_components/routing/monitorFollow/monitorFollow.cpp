#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "monitorFollow.h"


#define PNL_MONITOR_FOLLOW 1

#define TIMER_PACKAGE_PARK 1

#define PACKAGE_PARK_DELAY 200		//200ms delay between package park and RC

const int DATABASE_PACKAGER_SOURCE(0);
const int DATABASE_PACKAGER_DEST(7);


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( monitorFollow )

// constructor - equivalent to ApplCore STARTUP
monitorFollow::monitorFollow( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_tally_packager_underpackager = "both";

	m_mon_fixed_follow = false;

	m_source = 0;
	m_bMonFollowReady = false;


	m_dest_follow_continuous = false;

	m_monFollowing=false;


	m_bEnableOnLoad = false;

	m_sTally = "";
	m_sRouterName = "";

	m_bParking = false;

	m_tally_device = 0;
	m_tally_instance = "";

	m_tally_level = "";
	m_tally_level_int = 0;

	m_instance = "";

	m_default = "100x80_with_tally";
	//m_layout = m_default;

	m_tally_level_bool = false;

	m_DestIndex = 0;
	m_SourceIndex = 0;

	m_tally_instance_clear_if_not_in_trace = false;

	m_lastFollow = none;

	m_show_source_dest_statesheets = true;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	//panelShow(PNL_MONITOR_FOLLOW, bncs_string("layouts/%1.bncs_ui").arg(m_layout));
	//textPut("statesheet", "mon_notfollowing", PNL_MONITOR_FOLLOW, "button");

	bncs_string name;
	routerName(121, 16, 201, name);
	debug("monitorFollow::monitorFollow() name[16, 201]='%1'", name);

	init();
}

// destructor - equivalent to ApplCore CLOSEDOWN
monitorFollow::~monitorFollow()
{
}

// all button pushes and notifications come here
void monitorFollow::buttonCallback( buttonNotify *b )
{
	//Toggle our state
	m_monFollowing = !m_monFollowing;

	if( m_monFollowing )
	{
		textPut( "statesheet", "mon_following", PNL_MONITOR_FOLLOW , "button" );
	}
	else
	{
		textPut( "statesheet", "mon_notfollowing", PNL_MONITOR_FOLLOW , "button" );
	}

	setFollow(m_lastFollow);
}

// all revertives come here
int monitorFollow::revertiveCallback( revertiveNotify * r )
{

	//See if we are following a dest

	if (m_destFollow.Dest.Type.Device == r->device() && m_destFollow.Dest.Index == r->index())
	{
		debug("monitorFollow::revertiveCallback Following Checking if a dest follow has been set");

		if (m_destFollow.Dest.Type.IsPackager)
		{
			bncs_string value = bncs_stringlist(r->sInfo()).getNamedParam("index");
			if (value.upper() == "PARK")
			{
				m_source = 0;
			}
			else
			{
				m_source = value.toInt();
			}
		}
		else
		{
			debug("monitorFollow::revertiveCallback %1 Source", r->info());
			m_source = r->info();
		}
		if (m_dest_follow_continuous == false)
		{
			tieline empty;
			m_destFollow = empty;
		}
		setFollow(dest);
	}
	
	//Get instance for router
	//router router = findRouter(r->device()); //No longer needed
	//debug("monitorFollow::revertiveCallback Router found: instance:%1", router.Instance);

	//See if its something we are interested in
	map<int, tieline>::iterator it = m_Tielines.find(r->device());

	//r->dump("monitorFollow::revertiveCallback");
	if (m_Tielines.end() != it)
	{
		//The revertive is from a device we are interested in. (This should always be the case but its good to check.)

		if (it->second.Dest.Index == r->index())
		{
			int dest = r->index();
			int source;
			bncs_string sourceName;

			//What type is the router
			if (it->second.Dest.Type.IsPackager == true)
			{
				//debug("monitorFollow:: Is a Packager #### Device:%1 Device:%2", it->second.Dest.Instance, it->second.Dest.Type.Device);
				//Its an infodriver talking to us.
				bncs_string value = bncs_stringlist(r->sInfo()).getNamedParam("index");
				if (value.upper() == "PARK")
				{
					source = 0;
					sourceName = "PARKED";
				}
				else
				{
					source = value.toInt();
					routerName(r->device(), DATABASE_PACKAGER_SOURCE, source, sourceName);
				}
			}
			else
			{
				//Its a router talking to us
				source = r->info();
				sourceName = r->sInfo();
			}
			//debug("monitorFollow:: Source:%1 Name:%2 d:%3,index:%4,info:%5,sInfo:%6", source, sourceName, r->device(), r->index(), r->info(), r->sInfo());
			it->second.RoutedSourceIndex = source;
			it->second.RoutedSourceName = sourceName;

			//debug("monitorFollow::A Dest we are intested in has changed: Dest:%1 Index:%2, Source:%3 SourceName:%4", it->second.Dest.Type.Device, it->second.Dest.Index, it->second.RoutedSourceIndex, it->second.RoutedSourceName);
			tally();
		}
	}

	if (m_bParking == true && m_dest.Dest.Type.Device == r->device() && m_dest.Dest.Index == r->index())
	{
		//We are parking; lets see if its has now parked
		setFollow();
	}

	return 0;
}

// all database name changes come back here
void monitorFollow::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string monitorFollow::parentCallback( parentNotify *p )
{
	if( p->command() == "instance" )
	{
		if (m_mon_fixed_follow == false)
		{
			m_instance = p->value();
			getDev(m_instance, &m_device);
			debug("monitorFollow::parentCallback Instance Changed:%1 Dev:%2", m_instance, m_device);
		}
	}
	else if (p->command() == "name")
	{
		//The name of the monfollow to use.
		m_mon_follow_name = p->value();
		init();
	}
	else if (p->command() == "dest_name")
	{
		//The name of the monfollow to use.
		m_mon_dest_name = p->value();
		init();
	}
	else if (p->command() == "dest_follow_continuous")
	{
		if (m_mon_fixed_follow == false)
		{
			m_dest_follow_continuous = p->value().lower() == "true" ? true : false;
		}
	}
	else if( p->command() == "source" )
	{
		if (m_mon_fixed_follow == false)
		{
			m_lastFollow = source;
			m_source = p->value().toInt();
			debug("monitorFollow::parentCallback Source Changed:%1", m_source);
			setFollow();
		}
	}
	else if( p->command() == "dest" )
	{
		if (m_mon_fixed_follow == false)
		{
			m_lastFollow = dest;
			//For a Dest Index find the source that is routed to it
			setDestFollow(p->value().toInt());
		}
	}
	else if(p->command() == "EnableOnLoad")
	{
		if(p->value().lower() == "true")
		{
			m_bEnableOnLoad = true;
			m_monFollowing = true;
			textPut( "statesheet", "mon_following", PNL_MONITOR_FOLLOW , "button" );
		}
	}
	else if (p->command() == "tally_instance")
	{
		//This shows the tally on a specific router, rather than the traced through tally
		if (p->value() != m_tally_instance)
		{
			m_tally_instance = p->value();
			getDev(m_tally_instance, &m_tally_device);

			tally();
		}
	}
	else if (p->command() == "tally_instance_clear_if_not_in_trace")
	{
		//If a tally instance is being used, but the instance is not part of the tally trace it won't be shown.
		bool b = p->value().lower() == "true";
		if (b != m_tally_instance_clear_if_not_in_trace)
		{
			m_tally_instance_clear_if_not_in_trace = b;

			tally();
		}
	}
	else if (p->command() == "tally_level")
	{
		//Only shows the tally up to the level defined here.
		//1 would only show the tally from the first router
		if (p->value() != m_tally_level)
		{
			m_tally_level = p->value();
			//debug("monitorFollow::parentCallback m_tally_level changed:%1", m_tally_level);
			if (m_tally_level.length() > 0)
				m_tally_level_int = m_tally_level.toInt();
			else
				m_tally_level_int = 0;
			//debug("monitorFollow::parentCallback m_tally_level_int changed:%1", m_tally_level_int);
		}
	}
	else if (p->command() == "tally_level_only")
	{
		bool b = p->value() == "true" ? true : false;
		if (b != m_tally_level_bool)
		{
			m_tally_level_bool = b;
		}
	}
	else if (p->command() == "tally_packager_underpackager")
	{
		//Allows the user to choose to show only the underpackage or Packager if the level being shown is a packager level.
		if (p->value() != m_tally_packager_underpackager)
		{
			m_tally_packager_underpackager = p->value();
			if (m_tally_packager_underpackager == "both")
			{

			}
			else if (m_tally_packager_underpackager == "packager")
			{

			}
			else if (m_tally_packager_underpackager == "underpackager")
			{

			}
			else
			{
				m_tally_packager_underpackager = "both";
			}
		}
	}
	else if (p->command() == "layout")
	{
		if (m_layout != p->value())
		{
			if (p->value().length() == 0)
				m_layout = m_default;

			//If its empty, set it to the default
			bncs_string s = p->value();
			if (s.length() == 0)
				s = m_default;

			if (s.endsWith(".bncs_ui"))
				s = s.left(s.length() - 8);
			m_layout = s;
			panelDestroy(PNL_MONITOR_FOLLOW);

			panelShow(PNL_MONITOR_FOLLOW, bncs_string("layouts/%1.bncs_ui").arg(m_layout));

			m_slIds = getIdList(PNL_MONITOR_FOLLOW);

			if (m_bMonFollowReady)
				controlEnable(PNL_MONITOR_FOLLOW, "button");
			else
				controlDisable(PNL_MONITOR_FOLLOW, "button");

			if (m_monFollowing)
				textPut("statesheet", "mon_following", PNL_MONITOR_FOLLOW, "button");
			else
				textPut("statesheet", "mon_notfollowing", PNL_MONITOR_FOLLOW, "button");
		}
	}
	else if (p->command() == "show_source_dest_statesheets")
	{
		m_show_source_dest_statesheets = p->value() == "true" ? true : false;
	}
	else if( p->command() == "_events" )
	{
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;
		sl << "source=<source index>";
		sl << "dest=<dest_index,source_index>";
		return sl.toString( '\n' );
	}
	
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << bncs_string("name=%1").arg(m_mon_follow_name);
		sl << bncs_string("dest_name=%1").arg(m_mon_dest_name);
		sl << bncs_string("EnableOnLoad=%1").arg(bncs_string(m_bEnableOnLoad==true?"true":"false"));
		sl << bncs_string("layout=%1").arg(m_layout);
		sl << bncs_string("tally_instance=%1").arg(m_tally_instance);
		sl << bncs_string("tally_instance_clear_if_not_in_trace=%1").arg(bncs_string(m_tally_instance_clear_if_not_in_trace == true ? "true" : "false"));
		sl << bncs_string("tally_level=%1").arg(m_tally_level);
		sl << bncs_string("tally_level_only=%1").arg(bncs_string(m_tally_level_bool==true?"true":"false"));
		sl << bncs_string("tally_packager_underpackager=%1").arg(m_tally_packager_underpackager);
		sl << bncs_string("dest_follow_continuous=%1").arg(m_dest_follow_continuous ? "true" : "false");
		sl << bncs_string("show_source_dest_statesheets=%1").arg(m_show_source_dest_statesheets == true ? "true" : "false");

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void monitorFollow::timerCallback( int id )
{
	if (id == TIMER_PACKAGE_PARK)
	{
		m_bParking = false;
		timerStop(TIMER_PACKAGE_PARK);
		setFollow();
	}
}


router monitorFollow::getRouter(bncs_string instance)
{
	router r;

	if (instance.length() > 0)
	{
		int dev;
		if (getDev(instance, &dev))
		{
			r = m_routers[dev];

			//We haven't loaded this one yet do it now
			if (r.Instance.length() == 0)
			{
				r.Instance = instance;

				bncs_config c = bncs_config(bncs_string("instances.%1").arg(instance));

				if (c.isValid())
				{
					r.AltId = c.attr("alt_id");
				}

				//debug("monitorFollow::getRouter Loading Router: Instance:%1 Dev:%2", instance, dev);
				if (instance.find("packager") > -1)
				{
					//debug("monitorFollow::getRouter Loading Router: Instance:%1 Dev:%2 IS PACKAGER", instance, dev);
					r.IsPackager = true;
				}
				r.Device = dev;
				debug("monitorFollow::getRouter Adding Router:%1, Instance:%2", dev, r.Instance);
				m_routers[r.Device] = r;
			}
		}
	}
	return r;
}

tieline monitorFollow::createTieline(bncs_config &c)
{
	//Get all the information about the tieline
	bncs_string from = c.childAttr("from");
	point dest(from, c.childAttr("dest"), getRouter(from));

	point source;
	bncs_string to = c.childAttr("to");
	if (to.length() > 0)
		source = point(to, c.childAttr("source"), getRouter(to));

	point destSlave;
	int slaveIndex = c.childAttr("dest_slave");
	if (slaveIndex > 0)
	{
		destSlave.Instance = dest.Instance;
		destSlave.Type = dest.Type;
		destSlave.Index = slaveIndex;
	}

	tieline t(dest, source, destSlave);

	m_Tielines[t.Dest.Type.Device] = t;

	return t;
}

tieline monitorFollow::createUnderPackagerTieline(bncs_config &c, tieline &packager)
{
	tieline t;

	bncs_string underpackageInstance = c.childAttr("underpackage");
	bncs_string level = c.childAttr("underpackage_level");


	debug("monitorFollow::underpackageInstance:%1 level:%2", underpackageInstance, level);
	if (underpackageInstance.length() > 0 && level.length() > 0)
	{
		point dest(underpackageInstance, getIndexFromDestPackager(packager.Dest.Type.Device, packager.Dest.Index, level), getRouter(underpackageInstance));

		point destSlave;
		if (packager.DestSlave.Index > 0)
		{
			destSlave.Instance = underpackageInstance;
			destSlave.Type = dest.Type;
			destSlave.Index = getIndexFromDestPackager(packager.DestSlave.Type.Device, packager.DestSlave.Index, level);
		}

		t = tieline(dest, packager.Source, destSlave);

		t.IsUnderPackage = true;
		t.PackagerDevice = packager.Dest.Type.Device;

		//Add to tieline list
		m_Tielines[t.Dest.Type.Device] = t;
	}
	return t;
}

void monitorFollow::init()
{
	debug("monitorFollow::init Loading Mon follow");
	m_bMonFollowReady = false;
	controlDisable( PNL_MONITOR_FOLLOW , "button" );

	bool bRouteCrosspointOnMonRouter = false;

	//Load all of the routers and there types

	bncs_string opsPosition = getWorkstationSetting( "ops_position" );

	bncs_config c;

	//Check if we have been given a name to use
	if (m_mon_follow_name.length() > 0)
	{
		c = bncs_config(bncs_string("ops_positions.%1.%2").arg(opsPosition).arg(m_mon_follow_name));
	}
	else
	{
		c = bncs_config(bncs_string("ops_positions.%1").arg(opsPosition));
	}

	debug("monitorFollow::init Loading opsPosition:\"%1\"", opsPosition);

	//If we haven't had a m_mon_dest_name supplied, the first mon_dest we find we should use.
	bool monDestFound = false;

	while (c.isChildValid())
	{
		bncs_string id = c.childAttr("id");

		if (id.startsWith("mon_tieline"))
		{
			tieline t = createTieline(c);
				//Now check if we are a packager
				if (t.Dest.Type.IsPackager)
				{
					tieline under = createUnderPackagerTieline(c, t);

					//Link Packager to under packager router
					m_Tielines[t.Dest.Type.Device].PackagerDevice = under.Dest.Type.Device;
				}
		}
		else if (monDestFound == false && id == "mon_dest")
		{
			//See if this mon dest is the one we are looking for.
			bncs_string name = c.childAttr("name");
			if (name == m_mon_dest_name || m_mon_dest_name.length() == 0)
			{
				monDestFound = true;
				m_dest = createTieline(c);

				//Now check the type
				if (m_dest.Dest.Type.IsPackager)
				{
					m_underPackageDest = createUnderPackagerTieline(c, m_dest);

					//Link Packager to under packager router
					m_Tielines[m_dest.Dest.Type.Device].PackagerDevice = m_underPackageDest.Dest.Type.Device;
				}

				//Check for fixed Source or Dest Following.
				bncs_string mon_fixed_dest_follow;
				bncs_string mon_fixed_source_follow;

				bncs_string fixed_instance = c.childAttr("fixed_instance");
				bncs_string fixed_dest = c.childAttr("fixed_dest");
				bncs_string fixed_source = c.childAttr("fixed_source");

				if (fixed_instance.length() > 0 && (fixed_dest.length() > 0 || fixed_source.length() > 0))
				{
					m_instance = fixed_instance;
					getDev(m_instance, &m_device);
					m_mon_fixed_follow = true;

					if (fixed_source.length() > 0)
					{
						m_source = fixed_source.toInt();
						setFollow();
					}
					else
					{
						setDestFollow(fixed_dest.toInt());
						m_dest_follow_continuous = true;
					}
				}


			}
		}
		c.nextChild();
	}
	//Now we have loaded everything lets join up all the routes
	//We will start from the dest and create links between Tielines
	list<tieline> ties;
	ties.push_back(m_dest);
	int rep = 0;
	traceTielines(ties, rep);


	//Add Dest and Underpackage if it exists, these are single routes
	list<tieline> tieDest;
	tieDest.push_back(m_dest);
	m_linkedTielines[tieDest.back().Dest.Type.Device] = tieDest;
	if (m_dest.Dest.Type.IsPackager)
	{
		list<tieline> tieUnderpackage;
		tieUnderpackage.push_back(m_underPackageDest);
		m_linkedTielines[tieUnderpackage.back().Dest.Type.Device] = tieUnderpackage;
	}

	//Debug Print all tieline routes found
	debug("monitorFollow::init --------- All TieLine Routes Found");
	for (map<int, list<tieline>>::iterator it = m_linkedTielines.begin(); it != m_linkedTielines.end(); it++)
	{
		list<tieline> t = it->second;
		bncs_string sTieline = "";
		for (list<tieline>::reverse_iterator itt = t.rbegin(); itt != t.rend(); ++itt)
			sTieline += bncs_string("monitorFollow::init Tie:Dest:%1:%2 Source:%3:%4 IsPackager:%5 IsUnderPackage:%6|").arg(itt->Dest.Type.Device).arg(itt->Dest.Index).arg(itt->Source.Type.Device).arg(itt->Source.Index).arg(itt->Dest.Type.IsPackager == true ? "true" : "false").arg(itt->IsUnderPackage == true ? "true" : "false");
		debug(sTieline);
	}
	debug("monitorFollow::init --------- End All TieLine Routes Found");

	//Register for each tieline
	map<int, tieline>::iterator it;
	for (it = m_Tielines.begin(); it != m_Tielines.end(); ++it)
	{
		tieline t = it->second;
		
		registerForTieline(t);
	}

	if (m_dest.Dest.Instance.length() > 0)
	{
		controlEnable(PNL_MONITOR_FOLLOW, "button");
		m_bMonFollowReady = true;
	}
	
	hostNotify(bncs_string("monFollowReady=%1").arg(m_bMonFollowReady==true?"true":"false"));
}

void monitorFollow::registerForTieline(tieline t)
{
	if (t.Dest.Type.IsPackager == false)
	{
		routerRegister(t.Dest.Type.Device, t.Dest.Index, t.Dest.Index, true);
		routerPoll(t.Dest.Type.Device, t.Dest.Index, t.Dest.Index);
	}
	else
	{
		infoRegister(t.Dest.Type.Device, t.Dest.Index, t.Dest.Index, true);
		infoPoll(t.Dest.Type.Device, t.Dest.Index, t.Dest.Index);
	}
	debug("monitorFollow:Register: Dest:%1 Index:%2", t.Dest.Type.Device, t.Dest.Index);
}

bool monitorFollow::traceTielines(list<tieline> currentTies, int &rep)
{
	rep++;
	if (rep > 10)
	{
		//Loop lets stop
		return false;
	}
	
	map<int, tieline>::iterator it;
	for (it = m_Tielines.begin(); it != m_Tielines.end(); ++it)
	{
		//debug(monitorFollow::traceTielines "Dest:%1 Source:%2", it->second.Dest.Instance, it->second.Source.Instance);

		if (it->second.Source.Type.Device == currentTies.back().Dest.Type.Device)
		{
			//debug("Match");
			list<tieline> t = currentTies;
			t.push_back(it->second);
			m_linkedTielines[t.back().Dest.Type.Device] = t;

			//Debug
			//bncs_string sTieline = "";
			//for (list<tieline>::reverse_iterator itt = t.rbegin(); itt != t.rend(); ++itt)
			//	sTieline += bncs_string("Tie:Dest:%1:%2 Source:%3:%4 IsPackager:%5|").arg(itt->Dest.Instance).arg(itt->Dest.Index).arg(itt->Source.Instance).arg(itt->Source.Index).arg(itt->Dest.Type.IsPackager==true?"true":"false");
			//debug("monitorFollow::traceTielines:%1", sTieline);

			traceTielines(t, rep);
		}
	}
	rep--;
	return true;
}

int monitorFollow::getIndexFromDestPackager(bncs_string packager_instance, int packager_index, bncs_string routeType)
{
	int intRouterDevice = 0;
	getDev(packager_instance, &intRouterDevice);

	return getIndexFromDestPackager(intRouterDevice, packager_index, routeType);
}

int monitorFollow::getIndexFromDestPackager(int packager_device, int packager_index, bncs_string routeType)
{
	//debug("monitorFollow::getIndexFromDestPackager dev:%1 index:%2 level:%3", packager_device, packager_index, routeType);
	bncs_string sDBPackager = "";
	routerName(packager_device, DATABASE_PACKAGER_DEST, packager_index, sDBPackager);
	//Look through all the items in the returned list and see if any match what we are looking for
	int i = bncs_stringlist(sDBPackager, ',').getNamedParam(routeType).toInt();
	//debug("monitorFollow::getIndexFromDestPackager Index found:%1", i);
	return i;
}


void monitorFollow::traceTally(tieline current, bncs_string &tally, bncs_string &routerName, int &dest, int source, bncs_stringlist &fullTally, bncs_stringlist &fullRouterTally, bncs_stringlist &fullDest, bncs_stringlist &fullSource, int &rep)
{
	rep++;
	if (rep > 10)
	{
		//Loop lets stop
		return;
	}

	bncs_string level_tally;
	bncs_string level_router;
	int level_dest;
	int level_source;
	bool specialPackaging = false;

	//Get the tally
	tally = current.RoutedSourceName;
	routerName = current.Dest.Type.AltId;
	dest = current.Dest.Index;
	source = current.RoutedSourceIndex;

	//If the router is a Packager Type and its Parked, check if its under routed
	if (current.Dest.Type.IsPackager == true)
	{
		//Some special work with the packager, so we can choose which level to show 

		if (m_tally_packager_underpackager == "packager")
		{
			level_tally = tally;
			level_router = routerName;
			level_dest = dest;
			level_source = source;
			specialPackaging = true;
		}
		else if (m_tally_packager_underpackager == "underpackager")
		{
			level_tally = m_Tielines[current.PackagerDevice].RoutedSourceName;
			level_router = m_Tielines[current.PackagerDevice].Dest.Type.AltId;
			level_dest = m_Tielines[current.PackagerDevice].Dest.Index;
			level_source = m_Tielines[current.PackagerDevice].RoutedSourceIndex;
			specialPackaging = true;
		}


		if (current.RoutedSourceIndex == 0)
		{
			//debug("monitorfollow::traceTally Is Packager");
			current = m_Tielines[current.PackagerDevice];

			tally = current.RoutedSourceName;
			routerName = current.Dest.Type.AltId;
			dest = current.Dest.Index;
			source = current.RoutedSourceIndex;
		}
	}

	if (m_tally_packager_underpackager != "both" && specialPackaging)
	{
		tally = level_tally;
		routerName = level_router;
		dest = level_dest;
		source = level_source;
	}
	
	fullTally << tally.replace('|', ' ');
	fullRouterTally << routerName.replace('|', ' ');
	fullDest << dest;
	fullSource << source;



	//break if we are at the level requested in the parent callback.
	if (m_tally_level_int != 0 && rep >= m_tally_level_int)
	{
		return;
	}


	//We are routed somewhere
	//Is it one of our tielines
	for (map<int, tieline>::iterator it = m_Tielines.begin(); it != m_Tielines.end(); it++)
	{
		//debug("monitorFollow::traceTally Looping through tielines %1-%2 Looking For: %3-%4", it->second.Source.Instance, it->second.Source.Index, current.Dest.Instance, current.Dest.Index);

		if (it->second.Source.Type.Device == current.Dest.Type.Device && it->second.Source.Index == current.RoutedSourceIndex)
		{
			current = m_Tielines[it->second.Dest.Type.Device];
			
			//debug("monitorFollow::traceTally Found a Trace:%1-%2 Source:%3", current.Dest.Type.Device, current.Dest.Index, m_Tielines[it->second.Dest.Type.Device].RoutedSourceName);
			traceTally(current, tally, routerName, dest, source, fullTally, fullRouterTally, fullDest, fullSource, rep);
			return;
		}
		else if (m_tally_level_bool && m_tally_level_int != 0 && rep < m_tally_level_int)
		{
			//We have not reach out level so set every thing to blank
			tally = "";
			routerName = "";
		}
	}

	rep--;
	return;
}

void monitorFollow::tally()
{
	//A route has changed lets see if that effects our tally

	bncs_string tally;
	bncs_string routerName;

	bncs_stringlist fullTally;
	bncs_stringlist fullRouterTally;

	bncs_stringlist fullDestIndexes;
	bncs_stringlist fullSourceIndexes;

	int dest = 0;
	int source = 0;

	tieline current = m_Tielines[m_dest.Dest.Type.Device];
	int rep = 0;
	traceTally(current, tally, routerName,dest, source, fullTally, fullRouterTally, fullDestIndexes, fullSourceIndexes,  rep);

	//If its a device tally lets check if we should show the tally.
	if (m_tally_device > 0)
	{
		map<int, tieline>::iterator it = m_Tielines.find(m_tally_device);
		if (it != m_Tielines.end())
		{
			if (m_tally_instance_clear_if_not_in_trace && fullDestIndexes.find(it->second.Dest.Index) == -1)
			{
				if (it->second.RoutedSourceName == "PARKED")
					tally = "PARKED";
				else
					tally = "";
				routerName = it->second.Dest.Type.AltId;
			}
			else
			{
				tally = it->second.RoutedSourceName;
				routerName = it->second.Dest.Type.AltId;
			}
		}
		else
		{
			tally = "Tally Instance Error";
			routerName = "Tally Instance Error";
		}
	}
	//debug("monitorFollow::tally() tally:%1 routerName:%2", tally, routerName);
	setTally(tally, routerName, dest, source, fullTally, fullRouterTally, fullDestIndexes, fullSourceIndexes);
}

void monitorFollow::setTally(bncs_string Tally, bncs_string RouterName, int DestIndex, int SourceIndex, bncs_stringlist FullTally, bncs_stringlist FullRouterTally, bncs_stringlist fullDestIndexes, bncs_stringlist fullSourceIndexes)
{
	if (m_slIds.find("tally", true)  > -1 && m_sTally != Tally)
	{
		textPut("text", Tally.replace('|', ' '), PNL_MONITOR_FOLLOW, "tally");
		m_sTally = Tally;
	}
	if (m_slIds.find("router", true)  > -1 && m_sRouterName != RouterName)
	{
		textPut("text", RouterName.replace('|', ' '), PNL_MONITOR_FOLLOW, "router");
		m_sRouterName = RouterName;
	}
	if (m_slIds.find("dest", true)  > -1 && m_DestIndex != DestIndex)
	{
		if (DestIndex == 0)
			textPut("text", "", PNL_MONITOR_FOLLOW, "dest");
		else
			textPut("text", DestIndex, PNL_MONITOR_FOLLOW, "dest");
		m_DestIndex = DestIndex;
	}
	if (m_slIds.find("source", true)  > -1 && m_SourceIndex != SourceIndex)
	{
		if (SourceIndex == 0)
			textPut("text", "", PNL_MONITOR_FOLLOW, "source");
		else
			textPut("text", SourceIndex, PNL_MONITOR_FOLLOW, "source");
		m_DestIndex = SourceIndex;
	}
	if (m_sFullTally != FullTally)
	{

		for (int i = FullTally.count(); i > -1; i--)
		{
			if (m_slIds.find(bncs_string("fullTally_%1").arg(i), true) > -1)
				textPut("text", FullTally[i], PNL_MONITOR_FOLLOW, bncs_string("fullTally_%1").arg(i));
		}
		for (int i = FullTally.count(); i < m_sFullTally.count(); i++)
		{
			if (m_slIds.find(bncs_string("fullTally_%1").arg(i), true) > -1)
				textPut("text", "", PNL_MONITOR_FOLLOW, bncs_string("fullTally_%1").arg(i));
		}
		m_sFullTally = FullTally;
	}
	if (m_sFullRouterTally != FullRouterTally)
	{
		for (int i = FullRouterTally.count(); i > -1; i--)
		{
			if (m_slIds.find(bncs_string("fullRouter_%1").arg(i), true) > -1)
				textPut("text", FullRouterTally[i], PNL_MONITOR_FOLLOW, bncs_string("fullRouter_%1").arg(i));
		}
		for (int i = FullRouterTally.count(); i < m_sFullRouterTally.count(); i++)
		{
			if (m_slIds.find(bncs_string("fullRouter_%1").arg(i), true) > -1)
				textPut("text", "", PNL_MONITOR_FOLLOW, bncs_string("fullRouter_%1").arg(i));
		}
		m_sFullRouterTally = FullRouterTally;
	}
	if (m_FullDest != fullDestIndexes)
	{
		for (int i = fullDestIndexes.count(); i > -1; i--)
		{
			if (m_slIds.find(bncs_string("fullDest_%1").arg(i), true) > -1)
				textPut("text", fullDestIndexes[i], PNL_MONITOR_FOLLOW, bncs_string("fullDest_%1").arg(i));
		}
		for (int i = fullDestIndexes.count(); i < m_FullDest.count(); i++)
		{
			if (m_slIds.find(bncs_string("fullDest_%1").arg(i), true) > -1)
				textPut("text", "", PNL_MONITOR_FOLLOW, bncs_string("fullDest__%1").arg(i));
		}
		m_FullDest = fullDestIndexes;
	}
	if (m_FullSource != fullSourceIndexes)
	{
		for (int i = fullSourceIndexes.count(); i > -1; i--)
		{
			if (m_slIds.find(bncs_string("fullSource_%1").arg(i), true) > -1)
				textPut("text", fullSourceIndexes[i], PNL_MONITOR_FOLLOW, bncs_string("fullSource_%1").arg(i));
		}
		for (int i = fullSourceIndexes.count(); i < m_FullSource.count(); i++)
		{
			if (m_slIds.find(bncs_string("fullSource_%1").arg(i), true) > -1)
				textPut("text", "", PNL_MONITOR_FOLLOW, bncs_string("fullSource_%1").arg(i));
		}
		m_FullSource = fullSourceIndexes;
	}
}

void monitorFollow::setDestFollow(int iDestIndex)
{
	//Get the source routed to this.
	router r = getRouter(m_instance);

	debug("monitorFollow::Register for DestToFollow device:%1 index:%2", r.Device, iDestIndex);
	if (r.Device > 0)
	{
		m_destFollow.Dest.Index = iDestIndex;
		m_destFollow.Dest.Instance = m_instance;
		m_destFollow.Dest.Type = r;


		if (r.IsPackager == true)
		{
			infoRegister(m_destFollow.Dest.Type.Device, m_destFollow.Dest.Index, m_destFollow.Dest.Index, true);
			infoPoll(m_destFollow.Dest.Type.Device, m_destFollow.Dest.Index, m_destFollow.Dest.Index);
		}
		else
		{
			routerRegister(m_destFollow.Dest.Type.Device, m_destFollow.Dest.Index, m_destFollow.Dest.Index, true);
			routerPoll(m_destFollow.Dest.Type.Device, m_destFollow.Dest.Index, m_destFollow.Dest.Index);
		}
	}
}

void monitorFollow::setFollow(following Following)
{
	//debug("monitorFollow::setFollow");
	if (m_monFollowing)
	{
		if (Following == source && m_mon_fixed_follow == false)
		{
			tieline empty;
			m_destFollow = empty;
		}

		//debug("monitorFollow::Mon Follow On");
		map<int, list<tieline>>::iterator it = m_linkedTielines.find(m_device);

		if (it != m_linkedTielines.end())
		{
			//debug("monitorFollow::Linked");
			//The First Dest we route the source we have been supplied to
			list<tieline>::reverse_iterator tDest = it->second.rbegin();

			int source = m_source;
			if (source > 0)
			{
				for (tDest; tDest != it->second.rend(); tDest++)
				{
					debug("monitorFollow::setFollow  Dest:%1:%2  underpackage:%3", tDest->Dest.Type.Device, tDest->Dest.Index, tDest->IsUnderPackage);
					//Check if the dest is an underpackage
					if (tDest->IsUnderPackage == true)
					{
						debug("monitorFollow::setFollow is under package");
						//If it is we have to do some special treatment
						//Check if it packager dest is parked
						if (m_Tielines[tDest->PackagerDevice].RoutedSourceIndex != 0)
						{
							tieline packager = m_Tielines[tDest->PackagerDevice];
							m_bParking = true;
							infoWrite(packager.Dest.Type.Device, "index=PARK", packager.Dest.Index);
							if (packager.DestSlave.Index > 0)
								infoWrite(packager.DestSlave.Type.Device, "index=PARK", packager.DestSlave.Index);
							timerStart(TIMER_PACKAGE_PARK, PACKAGE_PARK_DELAY);
							break; //Break as we need this set before we do anything else
						}
						else
						{
							m_bParking = false;
							timerStop(TIMER_PACKAGE_PARK);
							route(m_Tielines[tDest->Dest.Type.Device], source);
						}
					}
					else
					{
						route(m_Tielines[tDest->Dest.Type.Device], source);
					}

					//Store the source as we will use this with the next dest
					source = tDest->Source.Index;
				}

				SetFollowTallyFlag(Following);
			}
		}
	}
}

void monitorFollow::route(tieline &t, int source)
{
	//Check if the route is already made, don't do it again if it is
	if (t.RoutedSourceIndex != source)
	{
		route(t.Dest, source);
		if (t.DestSlave.Instance.length() > 0)
			route(t.DestSlave, source);
	}
}

void monitorFollow::route(point p, int source)
{
	
	if (p.Type.IsPackager)
	{
		debug("monitorFollow::route Device:%1 Source:%2 Dest:%3", p.Type.Device, bncs_string("index=%1").arg(source), p.Index);
		infoWrite(p.Type.Device, bncs_string("index=%1").arg(source), p.Index, true);
	}
	else
	{
		debug("monitorFollow::route Device:%1 Source:%2 Dest:%3", p.Type.Device, source, p.Index);
		routerCrosspoint(p.Type.Device, source, p.Index, true);
	}

}

void monitorFollow::underPackageRoute()
{
	//This is already parked, route under the package
	route(m_underPackageDest.Dest, m_source);
	if (m_underPackageDest.DestSlave.Instance.length() > 0)
		route(m_underPackageDest.DestSlave, m_source);
}

void monitorFollow::SetFollowTallyFlag(following Following)
{
	if (m_show_source_dest_statesheets)
	{
		if (Following == source)
		{
			textPut("statesheet", "mon_following_source", PNL_MONITOR_FOLLOW, "button");
		}
		else if (Following == dest)
		{
			textPut("statesheet", "mon_following_dest", PNL_MONITOR_FOLLOW, "button");
		}
		else
		{
			//textPut("statesheet", "mon_following", PNL_MONITOR_FOLLOW, "button");
		}
	}
}

