# monitorFollow Component

This is a simple control to optionally follow the selected source to a monitoring destination.

## Assumptions
There are a few assumptions that the monitor follow component makes to work.
1. There is only 1 monitoring position end point per ops position Named monitor follow elements means this is no longer the case.
2. There is only a single tieline between each router per monitoring position, dest slave can route the same source to 2 destinations
3. Any instance with "package" within it is assumed to be a package router

## Configuration
This device requires the following configuration:

**workstation_settings.xml**

We need to know which what operational position this workstation is and in which area it is used so that we can tie this up with the monitoring destination. This is given in the example below.

```xml
<?xml version="1.0" encoding="utf-8"?>
<workstation_settings>
    <workstation id="211">
        <setting id="area" value="studio_1" />
        <setting id="ops_position" value="studio_1_td" />
    </workstation>
</workstation_settings>
```

**ops_positions.xml**

Once we've determined where this operational position is then we can work out what destinations it uses.

There are 2 examples below:

1) In this version the Monitor follow settings are contained within a settings element, the setting id is the name (name parameter) the monitor follow will look for with in xml. This allows multiple monitor follows to be associated with one ops position.
```xml
<?xml version="1.0" encoding="utf-8"?>
<ops_positions>
   <position id="mcr_1">
      <setting id="monitor_follow_video" >
          <mointor_follow id="mon_tieline" from="rtr_sdi_lines_a" dest="185" to="packager_router" source="909"/>
          <mointor_follow id="mon_tieline" from="rtr_sdi_lines_b" dest="185" to="packager_router" source="949"/>
          <mointor_follow id="mon_tieline" from="rtr_sdi_blackmagic" dest="185" to="rtr_sdi_lines_a" source="909"/>
          <mointor_follow id="mon_dest" from="packager_router" dest="1" underpackage="rtr_sdi_main" underpackage_level="hd"/>
      </setting>
   </position>
</ops_positions>
```
2) In this version the Monitor follow settings are not in there own element but within the position element. This only allows 1 monitoring position to be defined, no name should be specified in the name parameter.
```xml
<?xml version="1.0" encoding="utf-8"?>
<ops_positions>
   <position id="mcr_1">
      <setting id="mon_tieline" from="rtr_sdi_lines_a" dest="185" to="packager_router" source="909"/>
      <setting id="mon_tieline" from="rtr_sdi_lines_b" dest="185" to="packager_router" source="949"/>
      <setting id="mon_tieline" from="rtr_sdi_blackmagic" dest="185" to="rtr_sdi_lines_a" source="909"/>
      <setting id="mon_dest" from="packager_router" dest="1" underpackage="rtr_sdi_main" underpackage_level="hd"/>
   </position>
</ops_positions>
```
<position id="mcr_1"> is the operation position it applies to

Another Example showing fixed mons:
```xml
<position id="mc_mcr_1">
    <monitor_follow id="mon_tieline" from="mc_rtr_sdi_playout_x" dest="287" to="mc_rtr_sdi_infrastructure" source="131"/>
    <monitor_follow id="mon_tieline" from="mc_rtr_sdi_playout_y" dest="288" to="mc_rtr_sdi_infrastructure" source="143"/>
    <monitor_follow id="mon_dest" name="mon" from="mc_rtr_sdi_infrastructure" dest="23"/>
    <monitor_follow id="mon_dest" name="wfm" from="mc_rtr_sdi_infrastructure" dest="21" fixed_instance="mc_rtr_sdi_infrastructure" fixed_dest="23"/>
    <monitor_follow id="mon_dest" name="amu" from="mc_rtr_sdi_infrastructure" dest="25" fixed_instance="mc_rtr_sdi_infrastructure" fixed_dest="23"/>
</position> 
```
**<setting id="mon_dest"**

The monitoring destination, there can be multiple name monitoring dests. Use the **dest_name** parameter to look for names in the mon_dest's. If no dest_name is specified, this first mon_dest found in the xml will be used.
Multiple mon_dest allow the same monitor follow setting to be used with multiple destinations, this stops duplicate xml and settings.

**name** - the name of the mon_dest to use specified in **dest_name** parameter

**from** - router instance for mon destination
**dest** -  the dest index for the monitor dest, if a packager this is the dest package index

**dest_slave** - Optional the dest index if a dest slave is used

**underpackage - Optional** router instance for underpackage routing if packager, if under package routing is set, it park the source and then routes underneath it.

**underpackage_level - Optional** level defined in the db, used to find the real router index from the packager index, this is used for both dest and the dest_slave if one has been set.

It is possible to set a mon follow to be fixed, in this case it will only ever follow the source or dest set here. It will not respond to instance, source or dest events.
This is useful in setting on mon points that follow other monitor points.

**fixed_instance** - The instance to follow

**fixed_source** - The Source index to follow, if source is set dest will be ignored.

**fixed_dest** - The Dest index to follow, if source is set dest will be ignored. If dest if set, dest_follow_continuous will be set to true.

**\<setting id="mon_tieline"/>**

Tielines can be specified between routers. Multiple routes can be linked to each other using tielines.

**from** - instance of the router the tieline is coming from.

**dest** -  destination on the from router the tieline is connected too.

**to** -  instance of the router the tieline is connecting too.

**source** - source on the to router the tieline is connected too.

**dest_slave - Optional** the dest index if a dest slave is used
**underpackage - Optional** router instance for underpackage routing if packager

**underpackage_level - Optional** level defined in the db, used to find the real router index from the packager index, this is used for both dest and the dest_slave if one has been set.

## Layouts
Information about the layouts

The mon follow will toggle state if any of the objects on the gui have a button callback event.

**button** - This is the id for the button used to display the state of the mon follow component if on statesheet mon_following, if off statesheet mon_notfollowing is set.

**tally** - This shows the current tally.

**router** - This shows the current router the tally is coming from.

**fullTally_n** - Will show one step of the tally where "n" is the step starting from 0, 0 is the Dest router, Shows nothing if there aren't that many steps.

**fullRouter_n** - Will show one step of the tally "The Router Name" where "n" is the step starting from 0, 0 is the Dest router, Shows nothing if there aren't that many steps.

**dest** - dest index of the current tally

**source** - source index of the current tally

**fullDest_n** - Will show one step of the tally "The Dest index" where "n" is the step starting from 0, 0 is the Dest router, Shows nothing if there aren't that many steps.

**fullDest_n** - Will show one step of the tally "The Source  index" where "n" is the step starting from 0, 0 is the Dest router, Shows nothing if there aren't that many steps.

## Commands
| Name | Use |
|------|------|
|instance | Instance of the router to mon follow from. This can change at runtime
| name | The name of the monitor follow settings to look for in ops position, if blank the monitor follow setting should be in the main ops position element |
|dest_name | The name of the mon_dest to use, if blank the first mon_dest found in the xml will be used |
| EnableOnLoad | True/False if mon follow is active when the panel starts
| layout | This is the layout to display, looks in layouts/ folder. Default is "100x80_with_tally", ".bncs_ui" is not needed |
|tally_instance | Optional: If set, the tally shown will be that of the router instance specified rather than the traced tally |
|tally_instance_clear_if_not_in_trace | Default false: If set to true, if the tally_instance is set, this will blank the tally, if the router instance is not part of the tally traced. tally_level, tally_level_only and tally_packager_underpackager should all be left with there default values if using this.
| tally_level | Optional: If set, the tally will only trace through the number of routers set. e.g. if 1 the tally will be shown for the first router only. This does not work when tally_instance is set |
| tally_level_only | Default false: If set, the tally level will only show the level selected. E.g. If tally_level is 2, it will only show 2nd level routes.
| tally_packager_underpackager | This is a special option, the default is both. It can be set to either packager or underpackager this tells the tally to show only one or the other. E.g. if set to packager, the tally will show the Packager tally even if the packager is parked. If set to underpackager it will show the underpackage tally even if a package route is being made. If set to both, the packager route will be shown unless the packager is parked. This only affects tallies that are on a Packager Router |
| dest_follow_continuous | If true the monitor follow will continue following a dest, so when the source changes it will follow that. Default if false |
| show_source_dest_statesheets | If true, the mon_following_source and mon_following_dest statehseets are used, otherwise they are not |

## Events

| Name | Use |
|------|------|
| instance | Instance of the router to mon follow from. This can change at runtime |
| source | The source index - if we're monitor following then this is routed to our monitoring destination<br>If we're not monitor following then the value is stored in case the user hits the button to monitor follow
| dest | The dest index - if mon following this will monitor the source that is routed to the destination |

## Notifications
| Name | Use |
|------|------|
| monFollowReady | True/False wether mon follow is ready when first loaded

## Stylesheets
These are the statesheest used.
| Name | Use |
|------|------|
| mon_notfollowing | Shown if the monitor follow function is inactive |
| mon_following | Shown if the monitor follow function is active |
| mon_following_source | Shown when following sources - on the button |
| mon_following_dest | Shown when follwing dest- on the button|
