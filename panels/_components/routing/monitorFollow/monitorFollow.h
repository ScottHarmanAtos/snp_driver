#ifndef monitorFollow_INCLUDED
	#define monitorFollow_INCLUDED

#include <bncs_script_helper.h>
#include <map>
#include "tieline.h"
#include "router.h"
#include "point.h"
#include <list>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class monitorFollow : public bncs_script_helper
{
public:
	monitorFollow( bncs_client_callback * parent, const char* path );
	
	void init() ;
	
	virtual ~monitorFollow();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:

	enum following
	{
		source,
		dest,
		none
	};

	bool m_bEnableOnLoad;
	void setFollow(following Following = source);
	void setTally(bncs_string tally, bncs_string routerName, int DestIndex, int SourceIndex, bncs_stringlist fullTally, bncs_stringlist fullRouter, bncs_stringlist fullDestIndexes, bncs_stringlist fullSourceIndexes);
	void tally();
	int getIndexFromDestPackager(bncs_string packager_instance, int packager_index, bncs_string routeType);
	int getIndexFromDestPackager(int packager_device, int packager_index, bncs_string routeType);




	//These are the Routing Functions

	bncs_string m_mon_follow_name;
	bncs_string m_mon_dest_name;
	bool m_mon_fixed_follow;
	bool m_dest_follow_continuous;

	bncs_string m_sRouterName;
	bncs_string m_sTally;
	void setDestFollow(int iDestIndex);

	int m_source;

	bool m_bMonFollowReady;

	bool m_monFollowing;

	bncs_string m_default;
	bncs_string m_layout;

	bncs_string m_tally_level;
	int m_tally_level_int;

	bncs_string m_tally_instance;
	int m_tally_device;

	bncs_string m_instance;
	int m_device;

	//Assuming making an ass out of you and me
	//that there will only ever be 1 instance of a tieline per router per monitor, any thing else would seem stupid. If this is not the case Multimap should be used and some tweaks to revertive are necessary

	//These are all for the Tallying
	 // Stores device of router, index of tieline dest, index of source package related too, index of SDI under source package just incase

	//route m_direct; //There can only be one direct route. because of how the config is currently set
	//tieline m_tieline;	//This is to store the tieline if we are supposed to do tieline monfollow

	tieline m_destFollow;

	router getRouter(bncs_string instance);

	tieline m_underPackageDest;
	tieline m_dest;
	map<int, router>m_routers;
	
	map<int, tieline> m_Tielines; //Stores Dest Instance and Tieline
	bool traceTielines(list<tieline> currentTies, int &rep);

	map<int, list<tieline>> m_linkedTielines;

	void registerForTieline(tieline t);

	void traceTally(tieline current, bncs_string &tally, bncs_string &routerName, int &dest, int source, bncs_stringlist &fullTally, bncs_stringlist &fullRouterTally, bncs_stringlist &fullDest, bncs_stringlist &fullSource, int &rep);

	void route(point p, int source);
	void route(tieline &t, int source);

	void underPackageRoute();

	bool m_bParking;

	tieline createTieline(bncs_config &c);
	tieline createUnderPackagerTieline(bncs_config &c, tieline &packager);

	bncs_string m_tally_packager_underpackager;

	bncs_stringlist m_sFullTally;
	bncs_stringlist m_sFullRouterTally;

	bool m_tally_level_bool;

	int m_DestIndex;
	int m_SourceIndex;

	bncs_stringlist m_FullDest;
	bncs_stringlist m_FullSource;

	bncs_stringlist m_slIds;

	bool m_tally_instance_clear_if_not_in_trace;

	void SetFollowTallyFlag(following Following);

	following m_lastFollow;

	bool m_show_source_dest_statesheets; 
};


#endif // monitorFollow_INCLUDED