#ifndef ipx_combiner_source_grid_INCLUDED
	#define ipx_combiner_source_grid_INCLUDED

#include <bncs_script_helper.h>
#include "local_cache.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class ipx_combiner_source_grid : public bncs_script_helper
{
public:
	ipx_combiner_source_grid( bncs_client_callback * parent, const char* path );
	virtual ~ipx_combiner_source_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	/*
	<instance composite="yes" id="c_packager_router" ref="" type="">
	<group id="packager_router_1" instance="packager_router_main" />
	*/
	bncs_string m_instance;					//c_packager_router
	bncs_string m_packagerMainInstance;		//packager_router_main dev_1001 - 
	bncs_string m_workspace;
	bncs_string m_currentWorkspace;

	local_cache *lc;

	int m_presetDest;
	int m_presetSource;
	int m_devicePackagerRouterMain;
	bool m_presetMode;

	void setSource();
};


#endif // ipx_combiner_source_grid_INCLUDED