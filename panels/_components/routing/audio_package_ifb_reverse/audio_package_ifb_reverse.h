#ifndef audio_package_ifb_reverse_INCLUDED
	#define audio_package_ifb_reverse_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_package_ifb_reverse : public bncs_script_helper
{
public:
	audio_package_ifb_reverse( bncs_client_callback * parent, const char* path );
	virtual ~audio_package_ifb_reverse();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_displayLevel;
	int m_ap;
	int m_destIndex;
	int m_devicePackagerRouterMain;
	int m_devicePackagerMMStack;
	int m_slotPackagerMMStack;
	int m_queuePosition;

	bncs_string m_instance;

	bncs_string m_compositePackager;
	bncs_packager m_packagerRouter;
	bncs_packager m_packagerMMStack;

	void init();
	void showAP();
	void updateQueue(bncs_string queueData);
	
};


#endif // audio_package_ifb_reverse_INCLUDED