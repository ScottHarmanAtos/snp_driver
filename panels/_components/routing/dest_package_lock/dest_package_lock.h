#ifndef dest_package_lock_INCLUDED
	#define dest_package_lock_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dest_package_lock : public bncs_script_helper
{
public:
	dest_package_lock( bncs_client_callback * parent, const char* path );
	virtual ~dest_package_lock();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	
	bncs_string m_compositePackager;				//c_packager or c_packager_nhn, c_packager_ldc

	bncs_packager m_packagerDestStatus;

	int m_index;
	int m_deviceDestStatus;
	int m_slotDestStatus;

	bool m_lock;

	void init();
	void setDestPackage(int index);
	void updateStatus(bncs_string status);
};


#endif // dest_package_lock_INCLUDED