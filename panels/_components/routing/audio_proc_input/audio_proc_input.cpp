#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "audio_proc_input.h"

#define PNL_MAIN					1
#define POPUP_SOURCE_PACKAGE_LEVELS	2
#define POPUP_AUDIO_TYPE_TAG		3

#define TIMER_SETUP	1

#define LANGUAGE_GRID		"language_grid"
#define LANGUAGE_TAG		"language_tag"
#define TYPE_TAG			"type_tag"
#define INPUT_DEST			"input_dest"
#define INPUT_TALLY			"input_tally"

#define AP_INDEX			"ap_index"
#define AP_NAME				"ap_name"

#define BTN_CANCEL			"cancel"
#define BTN_NONE			"none"
#define BTN_SET				"set"
#define BTN_SELECT_DEFAULT	"select_default"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( audio_proc_input )

// constructor - equivalent to ApplCore STARTUP
audio_proc_input::audio_proc_input( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_input = 0;
	m_currentLanguageTag = 0;
	m_currentTypeTag = 0;

	m_defaultLanguageTag = 0;
	m_defaultTypeTag = 0;
	
	m_selectedLanguageTag = 0;
	m_selectedTypeTag = 0;

	m_sourcePackageIndex = 0;
	m_destPackageIndex = 0;
	m_destPackageLevel = 0;
	m_destAudioIndex = 0;
	m_destPackageVideoHubTag = 0;
	m_devicePackagerRouterMain = 0;
	m_deviceRouterAudio = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
audio_proc_input::~audio_proc_input()
{
}

// all button pushes and notifications come here
void audio_proc_input::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == LANGUAGE_TAG )
		{
			//hostNotify( bncs_string("%1=%2").arg(LANGUAGE_TAG).arg(m_languageTag) );
			panelPopup(POPUP_SOURCE_PACKAGE_LEVELS, "popup_source_package_levels.bncs_ui");

			bncs_string title = bncs_string(" Select Language from Source Package AP Levels for Audio Proc Input %1").arg(m_input);
			textPut("text", title, POPUP_SOURCE_PACKAGE_LEVELS, "title");

			initSourcePackageLevelPopup();
		}
		else if (b->id() == TYPE_TAG)
		{
			//hostNotify(bncs_string("%1=%2").arg(TYPE_TAG).arg(m_typeTag));
			panelPopup(POPUP_AUDIO_TYPE_TAG, "popup_audio_type_tag.bncs_ui");

			bncs_string title = bncs_string(" Set Audio Type Tag For Audio Proc Input %1").arg(m_input);
			textPut("text", title, POPUP_AUDIO_TYPE_TAG, "title");

			initAudioTypeTagPopup();
		}
	}
	else if (b->panel() == POPUP_AUDIO_TYPE_TAG)
	{
		popupAudioTypeTag(b);
	}
	else if (b->panel() == POPUP_SOURCE_PACKAGE_LEVELS)
	{
		popupSourcePackageLevel(b);
	}
}

// all revertives come here
int audio_proc_input::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void audio_proc_input::databaseCallback(revertiveNotify * r)
{
	//TODO handle database callback after writing to dest package tag database
	if (r->device() == m_devicePackagerRouterMain && r->index() == m_destPackageIndex)
	{
		if (r->database() == DATABASE_DEST_LEVELS)
		{
			updateDestPackageVideoHubTag();
			updateInputAudioPackage();
		}
		else if (r->database() == DATABASE_DEST_LANG_TYPE)
		{
			showDestPackageTags(r->sInfo());
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string audio_proc_input::parentCallback( parentNotify *p )
{
	p->dump("audio_proc_input::parentCallback()");
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "input=%1" ).arg( m_input );
			
			return sl.toString( '\n' );
		}

		else if( p->value() == "input" )
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_myParam ) );
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if( p->command() == "input" )
	{	// Persisted value or 'Command' being set here
		m_input = p->value().toInt();
	}

	else if (p->command() == "input_description")
	{
		m_inputDescription = p->value();
		textPut("text", m_inputDescription, PNL_MAIN, "input_description");
	}

	else if (p->command() == "input_language_default")
	{	
		m_defaultLanguageTag = p->value().toInt();
		if (m_defaultLanguageTag > 0)
		{
			textPut("text", bncs_string("Default:%1").arg(m_defaultLanguageTag), PNL_MAIN, "default_language");
		}
	}

	else if (p->command() == "input_type_default")
	{
		m_defaultTypeTag = p->value().toInt();
		if (m_defaultTypeTag > 0)
		{
			textPut("text", bncs_string("Default:%1").arg(m_defaultTypeTag), PNL_MAIN, "default_type");
		}
	}

	else if (p->command() == "instance_packager_router_main")
	{	
		m_instancePackagerRouterMain = p->value();
		getDev(m_instancePackagerRouterMain, &m_devicePackagerRouterMain);
	}

	else if (p->command() == "source_package_index")
	{
		m_sourcePackageIndex = p->value().toInt();
	}

	else if (p->command() == "dest_package_index")
	{
		m_destPackageIndex = p->value().toInt();
		updateDestPackageVideoHubTag();
	}

	else if (p->command() == "dest_package_level")
	{	
		m_destPackageLevel = p->value().toInt();
		initInputView();
	}

	else if (p->command() == "instance_router_audio")
	{
		textPut("instance", p->value(), PNL_MAIN, INPUT_TALLY);
		m_instanceRouterAudio_section_01 = p->value();
		getDev(m_instanceRouterAudio_section_01, &m_deviceRouterAudio);
	}

	else if (p->command() == "audio_dest_index")
	{	
		m_destAudioIndex = p->value().toInt();
		textPut("index", m_destAudioIndex, PNL_MAIN, INPUT_TALLY);

		bncs_string destName = "---";
		
		if (m_destAudioIndex > 0)
		{
			routerName(m_deviceRouterAudio, DATABASE_DEST_NAME, m_destAudioIndex, destName);
		}
		textPut("text", destName, PNL_MAIN, INPUT_DEST);

		textPut("index", m_destAudioIndex, PNL_MAIN, INPUT_TALLY);
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void audio_proc_input::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

bncs_string audio_proc_input::getLanguageTagName(int tag)
{
	bncs_string name = "[NONE]";

	if (tag > 0)
	{
		bncs_string languageNameStatus;
		routerName(m_devicePackagerRouterMain, DATABASE_LANG_TAG, tag, languageNameStatus);

		bncs_stringlist sltNameStatus(languageNameStatus, '|');
		name = sltNameStatus[0];
	}
	return name;
}

bncs_string audio_proc_input::getTypeTagName(int tag)
{
	bncs_string typeNameStatus;
	routerName(m_devicePackagerRouterMain, DATABASE_TYPE_TAG, tag, typeNameStatus);

	bncs_stringlist sltNameStatus(typeNameStatus, '|');
	return sltNameStatus[0];
}

bncs_string audio_proc_input::getAudioPackageName(int index)
{
	bncs_string name;
	routerName(m_devicePackagerRouterMain, DATABASE_AP_NAME, index, name);

	return name.replace('|', ' ');
}

bncs_string audio_proc_input::getAudioSourceName(int index)
{
	bncs_string name;
	routerName(m_deviceRouterAudio, DATABASE_SOURCE_NAME, index, name);

	return name.replace('|', ' ');
}

bncs_string audio_proc_input::getSourcePackageName(int index)
{
	bncs_string name;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_NAME, index, name);

	return name.replace('|', ' ');
}

void audio_proc_input::initInputView()
{
	if (m_destPackageLevel > 0)
	{
		controlShow(PNL_MAIN, AP_INDEX);
		controlShow(PNL_MAIN, AP_NAME);
		controlShow(PNL_MAIN, "default_language");
		controlShow(PNL_MAIN, "default_type");
		controlShow(PNL_MAIN, LANGUAGE_TAG);
		controlShow(PNL_MAIN, TYPE_TAG);
		controlShow(PNL_MAIN, "input_tally");
		controlShow(PNL_MAIN, "input_line");
		controlEnable(PNL_MAIN, "input_dest");
		textPut("text", bncs_string("Input %1 DP %2 Level %3").arg(m_input).arg(m_destPackageIndex).arg(m_destPackageLevel), PNL_MAIN, "input_level");
	}
	else
	{
		controlHide(PNL_MAIN, AP_INDEX);
		controlHide(PNL_MAIN, AP_NAME);
		controlHide(PNL_MAIN, "default_language");
		controlHide(PNL_MAIN, "default_type");
		controlHide(PNL_MAIN, LANGUAGE_TAG);
		controlHide(PNL_MAIN, TYPE_TAG);
		controlHide(PNL_MAIN, "input_tally");
		controlHide(PNL_MAIN, "input_line");
		controlDisable(PNL_MAIN, "input_dest");
		textPut("text", "", PNL_MAIN, "input_level");
	}

	bncs_string audioDestTags;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_destPackageIndex, audioDestTags);
	showDestPackageTags(audioDestTags);

	//register for database changes
	infoRegister(m_devicePackagerRouterMain, 1, 1);
}

void audio_proc_input::showDestPackageTags(bncs_string tagData)
{
	debug("audio_proc_input::showDestPackageTags() tagData=%1", tagData);

	bncs_string languageName = "";
	if (m_destPackageIndex > 0)
	{
		//db0016: Audio lang:type 1-32 (1/2 - 63/64)
		//19001=2:151,2:152,2:153,2:154,2:155,2:156,2:157,2:158,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0

		bncs_string langName = "-";
		bncs_string typeName = "-";

		//indexLangType: "1001#2:151"
		bncs_stringlist sltTags(tagData);
		if (sltTags.count() > 0)
		{
			bncs_stringlist sltLangType(sltTags[m_destPackageLevel - 1], ':');
			m_currentLanguageTag = sltLangType[0].toInt();
			m_currentTypeTag = sltLangType[1].toInt();
		}
		else
		{
			m_currentLanguageTag = -1;
			m_currentTypeTag = -1;
			langName = "LANG TAG|NOT SET";
			typeName = "TYPE TAG|NOT SET";
		}

		if (m_currentLanguageTag > 0)
		{
			langName = getLanguageTagName(m_currentLanguageTag);
		}
		textPut("text", langName, PNL_MAIN, LANGUAGE_TAG);

		if (m_currentTypeTag > 0)
		{
			typeName = getTypeTagName(m_currentTypeTag);
		}
		textPut("text", typeName, PNL_MAIN, TYPE_TAG);

	}
	else
	{
		m_currentLanguageTag = 0;
		m_currentTypeTag = 0;
	}

	updateInputAudioPackage();
}

void audio_proc_input::updateDestPackageVideoHubTag()
{
	bncs_string strDestLevels;
	//12001=video=0#3,anc1=0#3,anc2=0#3,anc3=0#3,anc4=0#3,rev_vision=0#3,ifb1=0.0,ifb2=0.0
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LEVELS, m_destPackageIndex, strDestLevels);
	bncs_stringlist sltDestLevels(strDestLevels);
	bncs_string destVideoLevelIndexTag = sltDestLevels.getNamedParam("video");
	bncs_string destVideoIndex;
	bncs_string destVideoHubTag;
	destVideoLevelIndexTag.split('#', destVideoIndex, destVideoHubTag);
	m_destPackageVideoHubTag = destVideoHubTag.toInt();
}

void audio_proc_input::updateInputAudioPackage()
{
	//Get Audio Package Levels to find which AP level is on the current language for this input

	//Check Video Levels
	int videoLevelMatchingLanguage = 0;
	int apIndexMatchingLanguage = 0;
	bncs_string strVideoLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_VIDEO, m_sourcePackageIndex, strVideoLevels);
	bncs_stringlist sltVideoLevels(strVideoLevels);

	//debug("audio_proc_input::updateInputAudioPackage() m_destPackageIndex=%1 strDestLevels=%2", m_destPackageIndex, strDestLevels);
	//debug("audio_proc_input::updateInputAudioPackage() sltDestLevels[0]=%1 destVideoHubTagNum=%2", sltDestLevels[0], destVideoHubTagNum);

	for (int videoLevel = 1; videoLevel <= 4; videoLevel++)
	{
		bncs_string index;
		bncs_string tag;
		bncs_stringlist sltVideo(sltVideoLevels.getNamedParam(bncs_string("L%1").arg(videoLevel)), '|');

		//TODO also check video hub tag for each level to handle multiple levels with *SRC language tag
		bncs_string videoTag = sltVideo[0];
		videoTag.split('#', index, tag);
		int video = index.toInt();
		int videoHubTag = tag.toInt();

		//find source audio for this video - check if [5] is useful
		bncs_string apTag = sltVideo[5];
		apTag.split('#', index, tag);
		int ap = index.toInt();

		if (ap > 0)
		{
			bncs_string apTags;
			routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);
			bncs_stringlist sltAPTags(apTags);

			//AP Language Tag
			int languageTag = sltAPTags.getNamedParam("language_tag").toInt();
			bncs_string languageName = getLanguageTagName(languageTag);

			if (languageTag == m_currentLanguageTag && videoHubTag == m_destPackageVideoHubTag)
			{
				//debug("audio_proc_input::initAudioTypeTagPopup videoLevel=%1 ap=%2", videoLevel, ap);
				videoLevelMatchingLanguage = videoLevel;
				apIndexMatchingLanguage = ap;
				break;	//for(apLevel)
			}
		}
	}

	//Also check AP levels if the language tag was not found on a video level
	if (videoLevelMatchingLanguage == 0)
	{
		//Check AP levels
		int apLevelMatchingLanguage = 0;
		bncs_string strAPLevels;
		routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_AP, m_sourcePackageIndex, strAPLevels);
		bncs_stringlist sltApLevels(strAPLevels, '|');
		for (int apLevel = 1; apLevel <= AP_LEVEL_COUNT; apLevel++)
		{
			int ap = sltApLevels[apLevel - 1].toInt();
			//SP Level;Audio Package;Language Tag;#TAG

			if (ap > 0)
			{
				bncs_string apTags;
				routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);

				bncs_stringlist sltAPTags(apTags);

				//AP Language Tag
				int languageTag = sltAPTags.getNamedParam("language_tag").toInt();
				bncs_string languageName = getLanguageTagName(languageTag);

				if (languageTag == m_currentLanguageTag)
				{
					apLevelMatchingLanguage = apLevel;
					apIndexMatchingLanguage = ap;

					break;	//for(apLevel)
				}
			}
		}
	}

	if (apIndexMatchingLanguage > 0)
	{
		textPut("text", bncs_string("AP %1").arg(apIndexMatchingLanguage), PNL_MAIN, AP_INDEX);
		textPut("text", getAudioPackageName(apIndexMatchingLanguage), PNL_MAIN, AP_NAME);
	}
	else
	{
		textPut("text", "-", PNL_MAIN, AP_INDEX);
		textPut("text", "-", PNL_MAIN, AP_NAME);
	}
}

void audio_proc_input::initSourcePackageLevelPopup()
{
	//			textPut("instance", m_instancePackagerRouterMain, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
	//	textPut("selected", m_currentLanguageTag, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);

	//Get Source Package Video Levels
	bncs_string strVideoLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_VIDEO, m_sourcePackageIndex, strVideoLevels);
	bncs_stringlist sltVideoLevels(strVideoLevels);

	//Get Dest Package Video Levels
	bncs_string strDestLevels;
	//12001=video=0#3,anc1=0#3,anc2=0#3,anc3=0#3,anc4=0#3,rev_vision=0#3,ifb1=0.0,ifb2=0.0
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LEVELS, m_destPackageIndex, strDestLevels);
	bncs_stringlist sltDestLevels(strDestLevels);
	bncs_string destVideoLevelIndexTag = sltDestLevels.getNamedParam("video");
	bncs_string destVideoIndex;
	bncs_string destVideoHubTag;
	destVideoLevelIndexTag.split('#', destVideoIndex, destVideoHubTag);
	int destVideoHubTagNum = destVideoHubTag.toInt();

	for (int videoLevel = 1; videoLevel <= 4; videoLevel++)
	{
		bncs_string index;
		bncs_string tag;
		bncs_stringlist sltVideo(sltVideoLevels.getNamedParam(bncs_string("L%1").arg(videoLevel)), '|');

		//TODO also check video hub tag for each level to 
		bncs_string videoTag = sltVideo[0];
		videoTag.split('#', index, tag);
		int video = index.toInt();
		int videoHubTag = tag.toInt();

		//find source audio for this video - check if [5] is useful
		bncs_string apTag = sltVideo[5];
		apTag.split('#', index, tag);
		int ap = index.toInt();

		if (ap > 0 && videoHubTag == m_destPackageVideoHubTag)
		{
			bncs_string apTags;
			routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);
			bncs_stringlist sltAPTags(apTags);

			//AP Language Tag
			int languageTag = sltAPTags.getNamedParam("language_tag").toInt();
			bncs_string languageName = getLanguageTagName(languageTag);

			textPut("add", bncs_string("Vid %1;%2 [%3];%4;#TAG=%5")
				.arg(videoLevel)
				.arg(getAudioPackageName(ap))
				.arg(ap)
				.arg(languageName)
				.arg(languageTag), 
				POPUP_SOURCE_PACKAGE_LEVELS, "lvw_levels");
		}
	}

	//Get Audio Package Levels
	bncs_string strAPLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_AP, m_sourcePackageIndex, strAPLevels);
	bncs_stringlist sltApLevels(strAPLevels, '|');
	for (int apLevel = 1; apLevel <= AP_LEVEL_COUNT; apLevel++)
	{
		int ap = sltApLevels[apLevel - 1].toInt();
		//SP Level;Audio Package;Language Tag;#TAG

		if (ap > 0)
		{
			bncs_string apTags;
			routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);

			bncs_stringlist sltAPTags(apTags);

			//AP Language Tag
			int languageTag = sltAPTags.getNamedParam("language_tag").toInt();
			bncs_string languageName = getLanguageTagName(languageTag);

			textPut("add", bncs_string("AP %1;%2 [%3];%4;#TAG=%5")
				.arg(apLevel)
				.arg(getAudioPackageName(ap))
				.arg(ap)
				.arg(languageName)
				.arg(languageTag),
				POPUP_SOURCE_PACKAGE_LEVELS, "lvw_levels");
 		}
	}
}

void audio_proc_input::initAudioTypeTagPopup()
{
	bncs_string defaultTypeName = "-";
	if (m_defaultTypeTag > 0)
	{
		defaultTypeName = getTypeTagName(m_defaultTypeTag);
	}
	else
	{
		controlDisable(POPUP_AUDIO_TYPE_TAG, BTN_SELECT_DEFAULT);
	}
	textPut("text", defaultTypeName, POPUP_AUDIO_TYPE_TAG, "default_type");

	//Show the tags present in the audio package of the source package that matches the current language
	bncs_string listCaption = bncs_string("No language set for Audio Proc Input|Unable to resolve AP level of SP %1 [%2]")
		.arg(getSourcePackageName(m_sourcePackageIndex)).arg(m_sourcePackageIndex);

	//Get Audio Package Levels to find which AP level is on the current language for this input
	
	//Check Video Levels
	int videoLevelMatchingLanguage = 0;
	int apIndexMatchingLanguage = 0;
	bncs_string strVideoLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_VIDEO, m_sourcePackageIndex, strVideoLevels);
	bncs_stringlist sltVideoLevels(strVideoLevels);

	for (int videoLevel = 1; videoLevel <= 4; videoLevel++)
	{
		bncs_string index;
		bncs_string tag;
		bncs_stringlist sltVideo(sltVideoLevels.getNamedParam(bncs_string("L%1").arg(videoLevel)), '|');

		//TODO also check video hub tag for each level to handle multiple levels with *SRC language tag
		bncs_string videoTag = sltVideo[0];
		videoTag.split('#', index, tag);
		int video = index.toInt();
		int videoHubTag = tag.toInt();

		//find source audio for this video - check if [5] is useful
		bncs_string apTag = sltVideo[5];
		apTag.split('#', index, tag);
		int ap = index.toInt();

		if (ap > 0 && videoHubTag == m_destPackageVideoHubTag)
		{
			bncs_string apTags;
			routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);
			bncs_stringlist sltAPTags(apTags);

			//AP Language Tag
			int languageTag = sltAPTags.getNamedParam("language_tag").toInt();
			bncs_string languageName = getLanguageTagName(languageTag);

			if (languageTag == m_currentLanguageTag)
			{
				//debug("audio_proc_input::initAudioTypeTagPopup videoLevel=%1 ap=%2", videoLevel, ap);
				videoLevelMatchingLanguage = videoLevel;
				apIndexMatchingLanguage = ap;

				listCaption = bncs_string("Showing Audio Pairs from AP: `%1` [%2]|with language %3 on Level Vid %4 of SP: %5 [%6]")
					.arg(getAudioPackageName(apIndexMatchingLanguage))
					.arg(apIndexMatchingLanguage)
					.arg(getLanguageTagName(m_currentLanguageTag))
					.arg(videoLevelMatchingLanguage)
					.arg(getSourcePackageName(m_sourcePackageIndex))
					.arg(m_sourcePackageIndex);

				break;	//for(apLevel)
			}
		}
	}

	//Also check AP levels if the language tag was not found on a video level
	if (videoLevelMatchingLanguage == 0)
	{
		//Check AP levels
		int apLevelMatchingLanguage = 0;
		bncs_string strAPLevels;
		routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_AP, m_sourcePackageIndex, strAPLevels);
		bncs_stringlist sltApLevels(strAPLevels, '|');
		for (int apLevel = 1; apLevel <= AP_LEVEL_COUNT; apLevel++)
		{
			int ap = sltApLevels[apLevel - 1].toInt();
			//SP Level;Audio Package;Language Tag;#TAG

			if (ap > 0)
			{
				bncs_string apTags;
				routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);

				bncs_stringlist sltAPTags(apTags);

				//AP Language Tag
				int languageTag = sltAPTags.getNamedParam("language_tag").toInt();
				bncs_string languageName = getLanguageTagName(languageTag);

				if (languageTag == m_currentLanguageTag)
				{
					//debug("audio_proc_input::initAudioTypeTagPopup apLevel=%1 ap=%2", apLevel, ap);
					apLevelMatchingLanguage = apLevel;
					apIndexMatchingLanguage = ap;

					listCaption = bncs_string("Showing Audio Pairs from AP: `%1` [%2]|with language %3 on Level A%4 of SP: `%5` [%6]")
						.arg(getAudioPackageName(apIndexMatchingLanguage))
						.arg(apIndexMatchingLanguage)
						.arg(getLanguageTagName(m_currentLanguageTag))
						.arg(apLevelMatchingLanguage)
						.arg(getSourcePackageName(m_sourcePackageIndex))
						.arg(m_sourcePackageIndex);
					break;	//for(apLevel)
				}
			}
		}
	}

	if (apIndexMatchingLanguage > 0)
	{
		//TODO - find pairs and tags of matching AP
		//Column 1: Audio Pair
		//Column 2: Audio Source Name
		//Column 3: Audio Type Tag
		//Column 4: #TAG

		bncs_string pairs;
		routerName(m_devicePackagerRouterMain, DATABASE_AP_PAIRS, apIndexMatchingLanguage, pairs);
		bncs_stringlist sltAudioPairs(pairs);

		for (int pair = 1; pair <= PAIR_COUNT; pair++)
		{
			bncs_string pairTag = sltAudioPairs.getNamedParam(bncs_string("a%1").arg(pair));

			bncs_stringlist sltIndexTypeTag(pairTag, '#');
			int index = sltIndexTypeTag[0].toInt();
			int typeTag = sltIndexTypeTag[1].toInt();
			
			if (typeTag > 0)
			{
				bncs_string typeName = getTypeTagName(typeTag);
				bncs_string sourceName = getAudioSourceName(index);
				textPut("add", bncs_string("Aud Pair %1;%2;%3;#TAG=%4").arg(pair).arg(sourceName).arg(typeName).arg(typeTag), POPUP_AUDIO_TYPE_TAG, "lvw_tags");
			}
		}

		textPut("selectTag", m_currentTypeTag, POPUP_AUDIO_TYPE_TAG, "lvw_tags");
	}
	textPut("text", listCaption, POPUP_AUDIO_TYPE_TAG, "list_caption");

}

void audio_proc_input::popupSourcePackageLevel(buttonNotify *b)
{
	if (b->id() == BTN_SET)
	{
		panelDestroy(POPUP_SOURCE_PACKAGE_LEVELS);
		setLanguageTag(m_selectedLanguageTag);
	}
	if (b->id() == "lvw_levels")
	{
		if (b->command() == "tag")
		{
			m_selectedLanguageTag = b->value().toInt();
			textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedLanguageTag), POPUP_SOURCE_PACKAGE_LEVELS, "selected_tag");

			bncs_string selectedName = getLanguageTagName(m_selectedLanguageTag);
			textPut("text", selectedName, POPUP_SOURCE_PACKAGE_LEVELS, "selected_name");
		}
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPUP_SOURCE_PACKAGE_LEVELS);
	}

}

void audio_proc_input::popupAudioTypeTag(buttonNotify *b)
{
	b->dump("audio_package_edit_view::popupAudioTypeTag");
	if (b->id() == "lvw_tags")
	{
		if (b->command() == "tag")
		{
			m_selectedTypeTag = b->value().toInt();
			bncs_string selectedName = getTypeTagName(m_selectedTypeTag);
			textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedTypeTag), POPUP_AUDIO_TYPE_TAG, "selected_tag");
			textPut("text", selectedName, POPUP_AUDIO_TYPE_TAG, "selected_name");
		}
	}
	else if (b->id() == BTN_SELECT_DEFAULT)
	{
		textPut("selectTag", m_defaultTypeTag, POPUP_AUDIO_TYPE_TAG, "lvw_tags");
		m_selectedTypeTag = m_defaultTypeTag;
		bncs_string selectedName = getTypeTagName(m_selectedTypeTag);
		textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedTypeTag), POPUP_AUDIO_TYPE_TAG, "selected_tag");
		textPut("text", selectedName, POPUP_AUDIO_TYPE_TAG, "selected_name");
	}
	else if (b->id() == BTN_SET)
	{
		panelDestroy(POPUP_AUDIO_TYPE_TAG);
		setTypeTag(m_selectedTypeTag);
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPUP_AUDIO_TYPE_TAG);
	}
}

void audio_proc_input::setTypeTag(int newTag)
{
	//read and modify type tag in dest tag database for m_destPackageLevel
	bncs_string audioDestTags;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_destPackageIndex, audioDestTags);

	bncs_stringlist sltTags(audioDestTags);

	if (sltTags.count() > 0)
	{
		bncs_stringlist sltLangType(sltTags[m_destPackageLevel - 1], ':');
		int lang = sltLangType[0].toInt();
		int type = sltLangType[1].toInt();
		sltTags[m_destPackageLevel - 1] = bncs_string("%1:%2").arg(lang).arg(newTag);
		routerModify(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_destPackageIndex, sltTags.toString(), false);
	}
	else
	{
		debug("audio_proc_input::setTypeTag newTag=%1 dp=%2 - Error unable to apply new newTag, existing tags are blank", newTag, m_destPackageIndex);

		//re-initialise all tags and apply new tag
		bncs_stringlist sltLangTypeLevels;
		for (int level = 1; level <= DEST_PACKAGE_AUDIO_COUNT; level++)
		{
			if (level == m_destPackageLevel)
			{
				sltLangTypeLevels.append(bncs_string("0:%1").arg(newTag));
			}
			else
			{
				sltLangTypeLevels.append("0:0");
			}
		}
		routerModify(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_destPackageIndex, sltLangTypeLevels.toString(), false);
	}
}

void audio_proc_input::setLanguageTag(int newTag)
{
	//read and modify type tag in dest tag database for m_destPackageLevel
	bncs_string audioDestTags;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_destPackageIndex, audioDestTags);

	bncs_stringlist sltTags(audioDestTags);
	if (sltTags.count() > 0)
	{
		bncs_stringlist sltLangType(sltTags[m_destPackageLevel - 1], ':');
		int lang = sltLangType[0].toInt();
		int type = sltLangType[1].toInt();
		sltTags[m_destPackageLevel - 1] = bncs_string("%1:%2").arg(newTag).arg(type);
		routerModify(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_destPackageIndex, sltTags.toString(), false);
	}
	else
	{
		debug("audio_proc_input::setLanguageTag newTag=%1 dp=%2 - Error unable to apply new newTag, existing tags are blank", newTag, m_destPackageIndex);
		
		//re-initialise all tags and apply new tag
		bncs_stringlist sltLangTypeLevels;
		for (int level = 1; level <= DEST_PACKAGE_AUDIO_COUNT; level++)
		{
			if (level == m_destPackageLevel)
			{
				sltLangTypeLevels.append(bncs_string("%1:0").arg(newTag));
			}
			else
			{
				sltLangTypeLevels.append("0:0");
			}
		}
		routerModify(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_destPackageIndex, sltLangTypeLevels.toString(), false);
	}
}