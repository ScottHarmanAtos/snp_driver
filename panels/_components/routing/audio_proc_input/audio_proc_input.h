#ifndef audio_proc_input_INCLUDED
	#define audio_proc_input_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
const int DEST_PACKAGE_AUDIO_COUNT(32);
const int PAIR_COUNT(12);

class audio_proc_input : public bncs_script_helper
{
public:
	audio_proc_input( bncs_client_callback * parent, const char* path );
	virtual ~audio_proc_input();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	bncs_string m_instancePackagerRouterMain;
	bncs_string m_instanceRouterAudio_section_01;

	bncs_string m_inputDescription;

	int m_deviceRouterAudio;
	int m_devicePackagerRouterMain;
	int m_input;
	int m_destPackageIndex;
	int m_destPackageLevel;
	int m_destAudioIndex;
	int m_sourcePackageIndex;
	int m_destPackageVideoHubTag;

	int m_currentLanguageTag;
	int m_currentTypeTag;

	int m_selectedLanguageTag;
	int m_selectedTypeTag;
	
	int m_defaultLanguageTag;
	int m_defaultTypeTag;

	bncs_string getLanguageTagName(int tag);
	bncs_string getTypeTagName(int tag);
	bncs_string getSourcePackageName(int index);
	bncs_string getAudioPackageName(int index);
	bncs_string getAudioSourceName(int index);

	void initInputView();
	void showDestPackageTags(bncs_string tagData);
	void initSourcePackageLevelPopup();
	void initAudioTypeTagPopup();
	void popupAudioTypeTag(buttonNotify *b);
	void popupSourcePackageLevel(buttonNotify *b);
	void setLanguageTag(int newTag);
	void setTypeTag(int newTag);
	void updateInputAudioPackage();
	void updateDestPackageVideoHubTag();
};


#endif // audio_proc_input_INCLUDED