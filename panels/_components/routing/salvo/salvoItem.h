#pragma once
#include <bncs_script_helper.h>

class salvoItem
{
public:
	salvoItem(bncs_string Instance, int Device, bncs_string Type, bncs_string Content, int Target)
	{
		this->Instance = Instance;
		this->Device = Device;
		this->Type = Type.lower();
		this->Content = Content;
		this->Target = Target;
		Active = false;
	}
	~salvoItem()
	{

	}

	bncs_string GetContent()
	{
		if (this->Type == "packager")
		{
			return bncs_string("index=%1").arg(this->Content);
		}
		else
			return this->Content;
	}

	bncs_string Instance;
	int Device;
	bncs_string Type;
	bncs_string Content;
	int Target;

	///This bool tells us if the salvo is active
	bool Active;
};

