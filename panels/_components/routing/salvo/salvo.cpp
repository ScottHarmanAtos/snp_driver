#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <bncs_packager.h>
#include "salvo.h"
#include <queue>

//#define DEBUG_ON
#ifndef  DEBUG_ON
#define debug //debug
#endif

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( salvo )

#define PANEL_MAIN	1

#define BTN_SALVO	"salvo"
#define TIMER_LOAD  1
#define TIMEOUT_LOAD  10
#define TIMER_EXECUTE  2
#define TIMEOUT_EXECUTE 10
#define DEFAULT_TYPE "packager"	// the type of route that gets set if the salvo config type does not resolve fully

const int PRI_TOP(1);
const int PRI_HIGH(2);
const int PRI_DEFAULT(3);
const int PRI_LOW(4);
const int PRI_BOTTOM(5);

const bncs_string DEFAULT_FILENAME("salvoes");

// constructor - equivalent to ApplCore STARTUP
salvo::salvo( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_currentActiveState = false;

	m_strDesignTimeLabel = "";
	m_strXmlLabel = "";
	m_strSalvoID = "";
	m_intExecutionFrequency = TIMEOUT_EXECUTE;
	m_priority = PRI_BOTTOM;
	m_isValid = false;
	m_defaultType = DEFAULT_TYPE;
	m_isRunning = false;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );

	// catchall for first run with this param
	if ( m_strFileName.length()==0)
	{
		m_strFileName = DEFAULT_FILENAME;
	}	
}

// destructor - equivalent to ApplCore CLOSEDOWN
salvo::~salvo()
{
}

// all button pushes and notifications come here
void salvo::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == BTN_SALVO)
		{
			if (m_isValid && !m_isRunning)
			{ // only fire if we are valid and not already running
				fire();
			}
		}
	}
}

// all revertives come here
int salvo::revertiveCallback( revertiveNotify * r )
{
	debug("salvo::%1::revertiveCallback() dev=%2 index=%3 value=%4", id() , r->device(), r->index(), r->info());

	// Check if we are valid
	if (m_isValid)
	{
		//Check if we are interested in the revertive
		for (list<salvoItem>::iterator it = m_salvo.begin(); it != m_salvo.end(); it++)
		{
			if (it->Device == r->device() && r->index() == it->Target)
			{
				//It was one of our revertives, check if the value is the same as the value we are.
				bncs_string val;
				if (it->Type == "rtr" || it->Type == "router")
				{
					val = r->info();
				}
				else
				{ // changed to ensure it picks up the index=nn part and ignores the rest of the packager message
					val = r->sInfo();
					bncs_string x;
					r->sInfo().split(',', val, x);
				}

				if (val == it->GetContent())
					it->Active = true;
				else
					it->Active = false;

				//Check if we are active
				checkTally();
				break;
			}
		}
	}

	return 0;
}

// all database name changes come back here
void salvo::databaseCallback( revertiveNotify * r )
{
}

void salvo::setButtonLabel()
{
	if (m_strDesignTimeLabel.length()> 0)
	{
		textPut("text", m_strDesignTimeLabel, PANEL_MAIN, BTN_SALVO);
	}
	else
	{
		if (m_strXmlLabel.length() >0)
		{
			textPut("text", m_strXmlLabel, PANEL_MAIN, BTN_SALVO);
		}
		else
		{
			textPut("text", "No Label|Defined", PANEL_MAIN, BTN_SALVO);
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string salvo::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		bncs_stringlist sl;
		
		sl << "filename=" + m_strFileName;
		sl << "label=" + m_strDesignTimeLabel;
		sl << "salvo=" + m_strSalvoID;
		sl << "default_type=" + m_defaultType;

		return sl.toString( '\n' );
	}
	else if( p->command() == "label" )
	{
		// deisgn time label trumps label in XML file
		m_strDesignTimeLabel = p->value();

		setButtonLabel();
			
	}
	else if (p->command() == "filename")
	{
		if ( p->value().length()==0)
		{
			m_strFileName = DEFAULT_FILENAME;
		}
		else
		{
			if (p->value().endsWith(".xml"))
			{
				m_strFileName = p->value().left(p->value().length() - 4);
			}
			else
			{
				m_strFileName = p->value();
			}
		}
		timerStart(TIMER_LOAD, TIMEOUT_LOAD);
	}
	else if (p->command() == "salvo")
	{
		m_strSalvoID = p->value();
		timerStart(TIMER_LOAD, TIMEOUT_LOAD);
	}
	else if (p->command() == "default_type")
	{
		m_defaultType = p->value();
		timerStart(TIMER_LOAD, TIMEOUT_LOAD);
	}
	else if (p->command() == "fire")
	{
		fire();
	}
	else if( p->command() == "_events" )
	{
		bncs_stringlist sl;
		
		sl << "fired";
		sl << "salvo_status=*";
		sl << "salvo_config=*";
		
		return sl.toString( '\n' );
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;
		
		sl << "fire";
		
		return sl.toString( '\n' );
	}	return "";
}

// timer events come here
void salvo::timerCallback( int id )
{
	if (id == TIMER_EXECUTE)
	{
		if (!m_queue.empty())
		{
			debug("salvo::%1::issue item in queue", bncs_script_helper::id());
			salvoItem si = m_queue.front();
			m_queue.pop();
			debug("salvo::%1::salvo item type %2", bncs_script_helper::id(), si.Type);

			// mark the salvo item as false until we get a revertive
			si.Active = false;

			if (si.Type == "info" || si.Type == "packager")
			{
				if (si.Instance.startsWith("vip"))
				{
					//VIP route
					bncs_string wsName = "";
					bncs_config cfgWorkstation(bncs_string("workstations.%1").arg(workstation()));
					if (cfgWorkstation.isValid())
					{
						wsName = cfgWorkstation.attr("name");
					}
					bncs_string tags = bncs_string("App: SALVO, Workstation: %1 - %2, Priority: %3").arg(workstation()).arg(wsName).arg(m_priority);
					infoWrite(si.Device, bncs_string("%1,pri=%2,%3").arg(si.GetContent()).arg(m_priority).arg(tags), si.Target);
					debug("salvo::%1::vip IW %2 %3 %4", bncs_script_helper::id(), si.Device, bncs_string("%1,pri=%2,%3").arg(si.GetContent()).arg(m_priority).arg(tags), si.Target);
				}
				else if ( si.Type == "packager")
				{
					//Package route
					infoWrite(si.Device, bncs_string("%1,pri=%2").arg(si.GetContent()).arg(m_priority), si.Target);
					debug("salvo::%1::packager IW %2 %3 %4", bncs_script_helper::id(), si.Device, bncs_string("%1,pri=%2").arg(si.GetContent()).arg(m_priority), si.Target);
				}
				else 
				{
					infoWrite(si.Device, si.GetContent(), si.Target);
					debug("salvo::%1::info IW %2 %3 %4", bncs_script_helper::id(), si.Device, si.GetContent(), si.Target);
				}
			}
			else if (si.Type == "rtr" || si.Type == "router")
			{
				routerCrosspoint(si.Device, si.GetContent(), si.Target);
				debug("salvo::%1::router RC %2 %3 %4", bncs_script_helper::id(), si.Device, si.GetContent(), si.Target);
			}

			if (m_queue.empty())
			{
				debug("salvo::%1::queue empty", bncs_script_helper::id());
				hostNotify(bncs_string("fired=%1").arg(m_strSalvoID));
				textPut("pixmap.bottom", "", PANEL_MAIN, BTN_SALVO);
				timerStop(TIMER_EXECUTE);
				m_isRunning = false;
			}
		}
	}
	else if ( id == TIMER_LOAD)
	{
		timerStop(id);
		load();		
	}
}

void salvo::fire()
{
	textPut("pixmap.bottom", "/images/icon_running.png", PANEL_MAIN, BTN_SALVO);
	for (list<salvoItem>::iterator it = m_salvo.begin(); it != m_salvo.end(); it++)
	{
		m_queue.push(*it);
	}
	debug("salvo::%1::executing %2 items, starting timer (%2ms)", id(), (int)m_queue.size(), m_intExecutionFrequency);

	// indicate we are running to stop further button presses until queue is empty
	m_isRunning = true;

	timerStart(TIMER_EXECUTE, m_intExecutionFrequency);

}

void salvo::load()
{
	//ClearOld Registrations
	infoUnregister(0);
	routerUnregister(0);
	//Clear our salvo
	m_salvo.clear();

	// isValid flag set to true, if any thing in the loops go wrong this will be set to false
	m_isValid = true;
	bncs_config cfgSalvo = bncs_config(bncs_string("%1.%2").arg(m_strFileName).arg(m_strSalvoID));

	debug("salvo::load::%1::start::xml %2.%3 - %4 parent config", id(), m_strFileName, m_strSalvoID, cfgSalvo.isValid() && m_strSalvoID.length() ? "valid" : "invalid");

	if (cfgSalvo.isValid() && m_strSalvoID.length() > 0)
	{
		// we have navigated to a salvo parent block
		// the xml parent in that block is in this format
		// where the entries have been replaced with the variable that is used to store them
		// <salvo id="m_strSalvoID" name="m_strDefaultName" label="m_strXmlLabel" instance="strMainInstance" type="strMainType" frequency="intExecutionFrequency">

		bncs_string strParentInstance = cfgSalvo.attr("instance");

		bncs_string strMainType = cfgSalvo.attr("type");
		if (!strMainType.length())
		{ // if the main type is not set then override it with the default type - this ensures a route will always have a type
			strMainType = m_defaultType;
		}

		bncs_string strDefaultName = cfgSalvo.attr("name");
		if (!strDefaultName.length())
		{ // if the default name is not set then override it with the default ID
			strDefaultName = m_strSalvoID;
		}

		m_strXmlLabel = cfgSalvo.attr("label");
		if (!m_strXmlLabel.length())
		{ // if the label is not set then use the name
			m_strXmlLabel = strDefaultName;
		}

		int m_intExecutionFrequency = cfgSalvo.attr("frequency").toInt();
		if (!m_intExecutionFrequency)
		{ // if we do not have an execution frequency specified then use the default
			m_intExecutionFrequency = TIMEOUT_EXECUTE;
		}

		debug("salvo::load::%1::parent config - id<%2>, name<%3>, label<%4>, %5<%6>, type<%7>, frequency(%8)<%9>", 
			id(), 
			m_strSalvoID, 
			strDefaultName,
			m_strXmlLabel, 
			bncs_packager(strParentInstance).isValid() ? "PACKAGER" : "instance", 
			strParentInstance, 
			strMainType, 
			m_intExecutionFrequency == TIMEOUT_EXECUTE ? "default" : "OVERRIDE",
			m_intExecutionFrequency);

		setButtonLabel();
		
		while (cfgSalvo.isChildValid())
		{
			bncs_string strContent = cfgSalvo.childAttr("content");
			bncs_string childInstance = cfgSalvo.childAttr("instance");

			bncs_string childType = cfgSalvo.childAttr("type");
			//Check if we have a type for this item, if we don't use the main one
			if (!childType.length())
			{
				childType = strMainType;
			}

			int childTarget = cfgSalvo.childAttr("target").toInt();
			int intDevice = 0;
			int intOffest = 0; // <<< this *should* always be zero !
			int intTarget = childTarget;

			//Check if the instance is a packager
			bncs_packager packager(childInstance.length() == 0 ? strParentInstance : childInstance);
			if (packager.isValid())
			{
				// find the device for the target
				intDevice = packager.destDev(childTarget);
				// and the target
				intTarget = packager.destSlot(childTarget);
				debug("salvo::load::%1::packager salvo, content %2 > %3/%4 resolved instance %5, device %6, slot %7", id(), strContent, childInstance.length() == 0 ? strParentInstance : childInstance, childTarget, packager.destInstance(childTarget), intDevice, intTarget);
			}
			else
			{
				getDev(childInstance.length() == 0 ? strParentInstance : childInstance, &intDevice, &intOffest);
				debug("salvo::load::%1::generic salvo, content %2 > instance %3, device %4, slot %5", id(), strContent, childInstance.length() == 0 ? strParentInstance : childInstance, intDevice, intTarget);
			}

			//Not Adding the offset at this point, as this may make an invalid dest id look like a valid one.
			//eg. Dest of 0 plus an offset of 100 makes the Dest 100, which looks like a valid value when it probably isn't

			if (intDevice == 0)
			{
				//Error our device in 0
				textPut("text", bncs_string("Instance:%1|No device").arg(childInstance), PANEL_MAIN, BTN_SALVO);
				//hostNotify("salvo_config=INCORRECT");
				m_isValid = false;
			}
			else if (intTarget == 0)
			{
				//Error our target is 0
				textPut("text", bncs_string("Target:%1|No target").arg(intTarget), PANEL_MAIN, BTN_SALVO);
				//hostNotify("salvo_config=INCORRECT");
				m_isValid = false;
			}
			else if ((intTarget + intOffest) > 4096)
			{
				//Error our target is 0
				textPut("text", bncs_string("Target:%1|Out of range").arg(intTarget), PANEL_MAIN, BTN_SALVO);
				//hostNotify("salvo_config=INCORRECT");
				m_isValid = false;
			}
			else
			{
				//Salvo values are good, create it and save it in our array.
				//Add item to salvo
				m_salvo.push_back(salvoItem(childInstance, intDevice, childType, strContent, intTarget + intOffest));
			}

			// rinse and repeat
			cfgSalvo.nextChild();
		}

		// Check for no salvoes in list
		if (!m_salvo.size())
		{
			//Error our salvo quantity is 0
			textPut("text", bncs_string("%1|Salvo empty").arg(m_strSalvoID), PANEL_MAIN, BTN_SALVO);
			//hostNotify("salvo_config=INCORRECT");
			m_isValid = false;
		}

		// Check for overall valid
		if (m_isValid == true)
		{
			//Register for the info
			for (list<salvoItem>::iterator it = m_salvo.begin(); it != m_salvo.end(); it++)
			{
				if (it->Type == "info" || it->Type == "packager")
				{
					infoRegister(it->Device, it->Target, it->Target, true);
					infoPoll(it->Device, it->Target, it->Target);
				}
				else if (it->Type == "rtr" || it->Type == "router")
				{
					routerRegister(it->Device, it->Target, it->Target, true);
					routerPoll(it->Device, it->Target, it->Target);
				}
				else
				{
					m_isValid = false;
					textPut("text", bncs_string("%1|Bad type").arg(it->Type), PANEL_MAIN, BTN_SALVO);
				}
			}
		}
	}
	else
	{
		m_isValid = false;
		textPut("text", bncs_string("%1|Not found").arg(m_strSalvoID), PANEL_MAIN, BTN_SALVO);
	}

	if (m_isValid)
	{
		debug("salvo::load::result::%1::Good config %2.%3", id(), m_strFileName, m_strSalvoID);
		hostNotify("salvo_config=CORRECT");
	}
	else
	{
		debug("salvo::load::result::%1::Bad config %2.%3", id(), m_strFileName, m_strSalvoID);
		infoUnregister(0);
		routerUnregister(0);
		hostNotify("salvo_config=INCORRECT");
	}
}

void salvo::checkTally()
{

	bool blnActive = true;
	for (list<salvoItem>::iterator it = m_salvo.begin(); it != m_salvo.end(); it++)
	{
		if (!it->Active)
		{
			blnActive = false;
			break;
		}
	}

	//Only fire this if our state changes
	if (m_currentActiveState != blnActive)
	{
		m_currentActiveState = blnActive;
		textPut("statesheet", blnActive ? "salvo_active" : "salvo_inactive", PANEL_MAIN, BTN_SALVO);
		if (blnActive)
		{
			hostNotify("salvo_status=ACTIVE");
		}
		else
		{
			hostNotify("salvo_status=INACTIVE");
		}
	}

	debug("salvo::%1::checkTally(), salvo is %1", id(), blnActive ? "active" : "not active");
}
