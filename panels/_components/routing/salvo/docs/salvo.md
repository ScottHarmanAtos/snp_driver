# salvo Component

## Description
This component is used to send a sequence of values to 1 or more devices, commands are defined in the config file /config/salvoes.xml

![as_used_on_panel](as_used_on_panel.png)

![config](config.png)

Example of the format of the XML file is as follows:

```xml
<salvoes>
    <salvo id="jf_misc_1-1_2-2" instance="rtr_audio_analogue" type="rtr" name="defaultName">
        <item content="172" target="66" instance="rtr_audio_analogue" type="rtr"/>
        <item content="173" target="67" />
    </salvo>
    <salvo id="jf_misc_3-1_4-2" instance="rtr_audio_analogue">
        <item content="174" target="66" />
        <item content="175" target="67" />
    </salvo>
    <salvo id="jf_misc_1-3_2-4" instance="rtr_audio_analogue" >
        <item content="172" target="68" />
        <item content="173" target="69" />
    </salvo>
    <salvo id="jf_misc_3-3_4-4" instance="rtr_audio_analogue">
        <item content="174" target="68" />
        <item content="175" target="69" />
    </salvo>
</salvoes>
```

Each item using its own instance

```xml

    <salvo id="jf_misc_3-3_4-4" >
        <item content="174" target="1" instance="rtr_audio_analogue_01" type="rtr"/>
        <item content="175" target="1" instance="rtr_audio_analogue_02" type="rtr"/>
    </salvo>
```

Each item using its the main 
```xml

    <salvo id="jf_misc_3-3_4-4" instance="rtr_audio_analogue_01" type="rtr">
        <item content="174" target="1" />
        <item content="175" target="1" />
    </salvo>
```

Mixed instance and type use, first item uses the main instance, second item uses its own instance
```xml

    <salvo id="jf_misc_3-3_4-4" instance="rtr_audio_analogue_01" type="rtr">
        <item content="174" target="1" />
        <item content="175" target="1" instance="rtr_audio_analogue_02" type="rtr"/>
    </salvo>
```


* Each salvo element must have a unique id attribute and this is used by the component as a reference
* The name is the default name to show on a button if its not overriden by a lable
* The instance attribute defines which router/infodriver device instance the commands should be sent to. This can be set in the attribute of the main salvo or each item, if not set in the item the item will use the main instance
* The type specifies how the salvo item should be treated, options are "packager", "rtr", "info" , packager prefixes "index=" to the content and uses infowrites, rtr sends router crosspoints, info sends infowrites.This can be set in the attribute of the main salvo or each item, if not set in the item the item will use the main instance
* The item child elements have two to four attributes that contain the content and targetination for the command, its also possible to set the instance and the type per item, if they don't use the same instances and type as the master instance and type
* The target indexes use the offset from the instance, so if its the first slot of the instance then the target will be 1, even if the instance offset is 1000.

## Types
- packager
- rtr
- info

## Commands
| Name | Type | Use |
|------|------|------|
|label | optional | The text to be shown on the salvo button, will use the name supplied in the xml if this is not present |
| salvo | mandatory | The ID of the salvo as defined in salvoes.xml
| fire | run-time method | This command may be issued via connections to cause the salvo to fire <br> e.g. when a slave salvo should fire after a master salvo has fired - see Developers Notes below

## Notifications
| Name | Use |
|------|------|
| fired=<salvo> | notification that the salvo has been fired

## Stylesheets
| Name | Use |
|------|------|
| salvo_active | appearance when active<br>- indicates that all target destinations have the content defined in the salvo currently routed 
| salvo_inactive | appearance when inactive<br>- indicates that one or more target destinations do not have the content defined in the salvo

##Developers Notes
To fire a salvo when another salvo fires the recommended connection is shown below:

**Sender:** another_salvo    **Receiver:** this_salvo    **Event:** fired=*    **Command:** fire=[value] 

In addition to the salvo_active/salvo_inactive states are applied to the button it may also be useful to show the current content on each of the target destinations that are routed by the salvo.
The tally component can be used to show the content that is currently routed to any target destination and these can be added to the panel then arranged and labelled as required.

22/11/2019 - revertive routine modified to remove extra route info from the packager revertive in order to just process the 'index=nn' section