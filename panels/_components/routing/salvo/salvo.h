#ifndef salvo_INCLUDED
	#define salvo_INCLUDED

#include <list>
#include <bncs_script_helper.h>
#include "salvoItem.h"
#include <queue>


#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class salvo : public bncs_script_helper
{
public:
	salvo( bncs_client_callback * parent, const char* path );
	virtual ~salvo();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	void setButtonLabel();
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	void checkTally();
	bncs_string m_strSalvoID;
	bncs_string m_strDesignTimeLabel;
	bncs_string m_strXmlLabel;
	bncs_string m_strFileName;
	bool m_isValid;
	bncs_string m_defaultType;
	bool m_isRunning;

	void load();
	void fire();

	bool m_currentActiveState;

	list<salvoItem> m_salvo;
	queue<salvoItem> m_queue;
	int m_intExecutionFrequency;
	int m_priority;
};


#endif // salvo_INCLUDED