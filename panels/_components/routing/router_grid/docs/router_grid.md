# ![alt text](../../../../../docs/common/header.jpg "Header")

# router_gird

## Description
Router grid can be used to display router grid, pages and groups.

Router grid has a built in ability to remember what group and page where last selected, so when returning to the panel the last group and page are selected.

Router grid can be used for both source and dest grids.

An attempt has also been made to simplify the use of the panel connections.
The only notification is:
index = which identifies the index selected.

The commands it can receive are:
take - take.[dest_index].[mask]=[source_index]
undo - undoes the last action
index - select an grid index
select_group - select a group
select_page - select a page
mapping - "page_mapoffset=0,page_nameoffset=0,grid_mapoffset=0,grid_namesdb=0"

Parameters:
instance - The instance to target. The lock instance is assumed to be instance_lock
layout - The layout to show, these are found in the layouts folder. e.g. source/grid_5x15x40

## NOTE

The router_name_grid does not had groups, so targetting with composite ids is an issue.

There is a special case, where if the router_grid is targetted with a compsite instance, it will look for a group called mapping,
if this group exists it will target the group and pages with this instance.

## Layouts

Router grid layouts need fixed names.
![alt text](router_grid.png "router grid")

The Grid is named "grid" - This can be a "Router Name Grid" or a "Router Dest Grid" or any other component that satisfies this.

Callbacks passed to grid:
instance
instance.lock - This is assumed to be instance_lock
mapoffset
instance.map
namesdb
take = take.[dest_index].[mask]=[source_index] - Dest Grids Only
undo - Dest Grids Only
index
button_mapping = changes the grid to button mapping mode

Notifications Expected:
index

The Group is named "group" - This should be a router name grid

The Page is name "page" - This should be a router name grid

Both Group and Page need this notifications.

![alt text](notifications.png "notifications")

## Button Mapping Mode
Button mapping mode allows router grids to be used in router mapping panels.

When first setting button mapping mode a notification with the mapping info of
the grid is set, this is for the copy and paste component.

## Parameters
Name     | Type      | used
-------  | --------  | ---
instance | mandatory | The instance to used
layout   | mandatory | The layout to use

## Commands
Name          | Use
-----------   | ---
take          | take.[dest_index].[mask]=[source_index]
undo          | undoes the last action
index         | select an grid index
select_group  | select a group
select_page   | select a page
mapping       | "page_mapoffset=0,page_nameoffset=0,grid_mapoffset=0,grid_namesdb=0"
button_mapping| This sets the grid into button mapping mode

Whilst in button mapping mode:

Name          | Use
-----------   | ---
save          | save the mapping
cancel        | cancel the mapping
dirty         | Sets page and group to dirty
dirty.page    | Sets page to dirty
dirty.group   | Sets group to dirty
rename.page   | Rename page
rename.group  | Rename group

## Notifications
Name          | Use
------------- | ---
index=`<index>` | The index that has been selected

When the mode is changed to mapping

Name                                        | Use
------------------------------------------- | ---
mapping=mapdb=`<db>`,mapoffset=`<offset>` | Notification when changed to mapping mode
dirty                                       | Notifies the router grid is dirty
page.name                                   | Notifies the page name selected
group.name                                  | Notifies the group name selected
page.index                                  | Page index
## Developer Nodes
