#ifndef router_grid_INCLUDED
	#define router_grid_INCLUDED

#include <bncs_script_helper.h>
#include "local_cache.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class router_grid : public bncs_script_helper
{
public:
	router_grid( bncs_client_callback * parent, const char* path );
	virtual ~router_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
	void NotifyIndex(bncs_string value, bncs_string subs = "");

private:
	bncs_string m_strInstance;

	bncs_string m_strLayout;
	int m_currentPage;

	local_cache *lc;
	bncs_string m_grid;
	bncs_string m_group;
	bncs_string m_page;

	bool m_buttonMapping;

	bool m_mass_mapping;

};


#endif // router_grid_INCLUDED