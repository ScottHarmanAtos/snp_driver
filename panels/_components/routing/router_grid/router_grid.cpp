#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "router_grid.h"
#include "instanceLookup.h"

#define PNL_MAIN	1

#define TIMER_SET_PAGE_DELAY 1
#define TIMER_SET_PAGE_DELAY_DURATION 100

#define TIMER_INIT   2
#define TIMEOUT_INIT 10

#define LAYOUT_DEFAULT	"sources/grid_5x15x40"

#define RESET_INDEX  -1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( router_grid )

// constructor - equivalent to ApplCore STARTUP
router_grid::router_grid( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	lc = local_cache::getLocalCache();

	m_buttonMapping = false;
	m_mass_mapping = false;

	m_currentPage = 0;
	m_strLayout = LAYOUT_DEFAULT;

	// show a panel from file router_grid.bncs_ui and we'll know it as our panel PNL_MAIN
	//panelShow(PNL_MAIN, bncs_string("%1.bncs_ui").arg(m_strLayout));
	timerStart(TIMER_INIT, TIMEOUT_INIT);
}

// destructor - equivalent to ApplCore CLOSEDOWN
router_grid::~router_grid()
{
}

// all button pushes and notifications come here
void router_grid::buttonCallback( buttonNotify *b )
{
	debug("router_grid::buttonCallback id=%1 command=%2 value=%3 sub(0)=%4 sub(1)=%5", b->id(), b->command(), b->value(), b->sub(0), b->sub(1));

	//b->dump("router_grid::buttonCallback");
	if (b->panel() == PNL_MAIN)
	{
		b->dump("router_grid");
		if (b->id() == "grid" && b->value() == "released")
		{
			if (m_buttonMapping == true)
			{
				NotifyIndex(b->sub(0));
				hostNotify("dirty");
			}
		}
		else if (b->id() == "grid" && (b->command() == "index" || b->command() == "dest"))
		{
			if (m_buttonMapping == false)
			{
				bncs_string subs;
				for (int i = 0; i < b->subs(); i++)
				{
					subs.append(".");
					subs.append(b->sub(i));
				}
				NotifyIndex(b->value(), subs);
			}

			bncs_string s = bncs_string("%1.%2.grid=%3").arg(m_strInstance).arg(m_strLayout).arg(b->value());
			debug("router_grid: Store %1", s);
			lc->setValue(bncs_string("%1.%2.grid").arg(m_strInstance).arg(m_strLayout), b->value());
		}
		else if (b->id() == "grid")
		{
			//Pass any other message straigth through
			bncs_string sub;
			for (int i = 0; i < b->subs(); i++)
			{
				sub.append(".");
				sub.append(b->sub(i));
			}

			//Send all other stuff over
			hostNotify(bncs_string("%1%2=%3").arg(b->command()).arg(sub).arg(b->value()));
		}
		else if (b->id() == "group" && b->command() == "index")
		{
			//debug("router_grid::buttonCallback ##GROUP## textPut(`page`, %1, PNL_MAIN, `grid`)", b->value());

			m_group = b->value();

			textPut("page", b->value(), PNL_MAIN, "page");
			if (m_buttonMapping == false)
			{
				NotifyIndex(RESET_INDEX);
			}

		}
		else if (b->id() == "group" && b->command() == "name")
		{
			if (m_buttonMapping == true)
			{
				hostNotify(bncs_string("group.name=%1").arg(b->value()));
			}
		}
		else if (b->id() == "group" && b->value() == "released")
		{
			lc->setValue(bncs_string("%1.%2.group").arg(m_strInstance).arg(m_strLayout), b->sub(0));
		}
		else if (b->id() == "page" && b->command() == "button" && b->value() == "released")
		{
			int page = b->sub(0).toInt();
			int group = m_group.toInt() - 1;

			int pageRows = 3;		//default rows = 3
			int pageColumns = 5;	//default columns = 5
			bncs_string retRows;
			bncs_string retColumns;
			textGet("rows", PNL_MAIN, "page", retRows);
			if (retRows.toInt() > 0)
				pageRows = retRows.toInt();
			textGet( "columns", PNL_MAIN, "page",retColumns);
			if (retColumns.toInt() > 0)
				pageColumns = retColumns.toInt();

			page += group * pageRows * pageColumns;
			//debug("router_grid::buttonCallback ##PAGE## textPut(`page`, %1, PNL_MAIN, `grid`) - current group=%2", page, m_group);
			textPut("page", page, PNL_MAIN, "grid");
			if (m_buttonMapping == false)
			{
				NotifyIndex(RESET_INDEX);
			}
			else
			{
				hostNotify(bncs_string("page.index=%1").arg(page));
			}
			lc->setValue(bncs_string("%1.%2.page").arg(m_strInstance).arg(m_strLayout), b->sub(0));
		}
		else if (b->id() == "page" && b->command() == "name")
		{
			if (m_buttonMapping == true)
			{
				hostNotify(bncs_string("page.name=%1").arg(b->value()));
			}
		}
		else if (b->command() == "dirty")
		{
			if (m_buttonMapping == true)
			{
				hostNotify("dirty");
			}
		}
	}
}

void router_grid::NotifyIndex(bncs_string s, bncs_string subs)
{
	if (m_mass_mapping == false)
	{
		hostNotify(bncs_string("index%1=%2").arg(subs).arg(s));
	}
}

// all revertives come here
int router_grid::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void router_grid::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string router_grid::parentCallback( parentNotify *p )
{
	debug("router_grid::parentCallback()  command=%1 value=%2", p->command(), p->value());

	if (p->command() == "instance")
	{
		m_strInstance = p->value();

		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "return")
	{
		bncs_stringlist sl;
		sl << "layout=" + m_strLayout;

		return sl.toString('\n');
	}
	else if (p->command() == "layout")
	{
		if (m_strLayout != p->value())
		{
			m_strLayout = p->value();
			panelDestroy(PNL_MAIN);
			panelShow(PNL_MAIN, bncs_string("layouts/%1.bncs_ui").arg(m_strLayout));
		}

		//Refresh instance targeting
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "mapping")
	{
		bncs_stringlist sltDatabases(p->value());

		//Refresh mapping
		textPut("mapoffset", sltDatabases.getNamedParam("page_mapoffset"), PNL_MAIN, "page");
		textPut("namesoffset", sltDatabases.getNamedParam("page_nameoffset"), PNL_MAIN, "page");
		textPut("mapoffset", sltDatabases.getNamedParam("grid_mapoffset"), PNL_MAIN, "grid");
		textPut("namesdb", sltDatabases.getNamedParam("grid_namesdb"), PNL_MAIN, "grid");
	}
	else if (p->command() == "select_group")
	{
		textPut(bncs_string("button.%1=released").arg(p->value()), PNL_MAIN, "group");
	}
	else if (p->command() == "select_page")
	{
		//This is done to allow times for the group to be set before the page is set, as setting the group reset the page number
		timerStart(TIMER_SET_PAGE_DELAY, TIMER_SET_PAGE_DELAY_DURATION);
		m_currentPage = p->value();
	}
	else if (p->command() == "take")
	{
		textPut(bncs_string("take.%1.%2=%3").arg(p->sub(0)).arg(p->sub(1)).arg(p->value()), PNL_MAIN, "grid");
	}
	else if (p->command() == "undo")
	{
		textPut("undo", p->value(), PNL_MAIN, "grid");
	}
	else if (p->command() == "index")
	{
		textPut("index", p->value(), PNL_MAIN, "grid");

		//additional case for layout using component_grid
		if (p->value().toInt() == 0)
		{
			textPut("deselect", PNL_MAIN, "grid");
		}
	}
	else if (p->command() == "button_mapping")
	{
		m_buttonMapping = true;
		debug("router_grid::parentCallback button_mapping");
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "save")
	{
		if (m_buttonMapping)
		{
			textPut("save", PNL_MAIN, "grid");
			textPut("save", PNL_MAIN, "page");
			textPut("save", PNL_MAIN, "group");
		}
	}
	else if (p->command() == "cancel")
	{
		if (m_buttonMapping)
		{
			textPut("cancel", PNL_MAIN, "grid");
			textPut("cancel", PNL_MAIN, "page");
			textPut("cancel", PNL_MAIN, "group");
		}
	}
	else if (p->command() == "dirty")
	{
		if (m_buttonMapping)
		{
			if (p->subs() > 0)
			{
				if (p->sub(0) == "page")
				{
					textPut("dirty", PNL_MAIN, "page");
				}
				else if (p->sub(0) == "group")
				{
					textPut("dirty", PNL_MAIN, "group");
				}
			}
			else
			{
				textPut("dirty", PNL_MAIN, "page");
				textPut("dirty", PNL_MAIN, "group");
			}
		}
	}
	else if (p->command() == "rename")
	{
		if (m_buttonMapping)
		{
			if (p->subs() > 0)
			{
				if (p->sub(0) == "page")
				{
					textPut("rename", p->value(), PNL_MAIN, "page");
				}
				else if (p->sub(0) == "group")
				{
					textPut("rename", p->value(), PNL_MAIN, "group");
				}
			}
		}
	}
	else if (p->command() == "mass_mapping")
	{
		if (m_buttonMapping == true)
		{
			m_mass_mapping = true;
			bncs_stringlist slMapping(p->value());
			for (int i = 0; i < slMapping.count(); ++i)
			{
				textPut(bncs_string("button.%1=released").arg(i), 1, "grid");
				textPut("index", slMapping[i], 1, "grid");
			}
			textPut(bncs_string("button.%1=released").arg(slMapping.count()), 1, "grid");
			m_mass_mapping = false;
			textPut("dirty", PNL_MAIN, "page");
			textPut("dirty", PNL_MAIN, "group");
			hostNotify("dirty");
		}
	}
	return "";
}

// timer events come here
void router_grid::timerCallback( int id )
{
	timerStop(id);

	if (id == TIMER_SET_PAGE_DELAY)
	{
		textPut(bncs_string("button.%1=released").arg(m_currentPage), PNL_MAIN, "page");
	}
	else if (id == TIMER_INIT)
	{
		//See if there is a lock instance

		int lockDev = 0;
		bncs_string lockInstance = bncs_string("%1_lock").arg(m_strInstance);
		if (getDev(lockInstance, &lockDev))
		{
			textPut("instance.lock", lockInstance, PNL_MAIN, "grid");
		}

		if (m_buttonMapping)
		{
			debug("router_grid::timerCallback Set button_mapping");
			textPut("map_mode", "true", PNL_MAIN, "grid");
			textPut("rename_mode", "true", PNL_MAIN, "page");
			textPut("rename_mode", "true", PNL_MAIN, "group");

			textPut("notify.name", "true", PNL_MAIN, "page");
			textPut("notify.name", "true", PNL_MAIN, "group");

			bncs_string mapdb;
			bncs_string mapoffset;
			textGet("mapdb", PNL_MAIN, "grid", mapdb);
			textGet("mapoffset", PNL_MAIN, "grid", mapoffset);

			bncs_string mapping = bncs_string("mapping=mapdb=%1,mapoffset=%2").arg(mapdb).arg(mapoffset);
			hostNotify(mapping);
			debug("router_grid::timerCallback mapping notify:%1", mapping);
		}

		//Check if we have been passed a composite instance.
		bncs_string mappingInstance = "";
		bncs_string gridInstance = m_strInstance;
		if (!instanceLookupComposite(m_strInstance, "mapping", mappingInstance))
		{
			mappingInstance = m_strInstance; //If it is not a composite use the normal instance
		}
		else
		{
			bncs_string scriptname;
			textGet("all", PNL_MAIN, "grid", scriptname);
			
			debug("router_grid::timerCallback scriptname=%1", scriptname);
			//If its a composite and its not a scriptname it is probably a router name grid, so target this with the non composite
			if (scriptname.find("scriptname=") < 0)
				gridInstance = mappingInstance;
		}

		textPut("instance", mappingInstance, PNL_MAIN, "group");
		textPut("instance", mappingInstance, PNL_MAIN, "page");
		textPut("instance", gridInstance, PNL_MAIN, "grid");

		textPut("instance.map", mappingInstance, PNL_MAIN, "group");
		textPut("instance.map", mappingInstance, PNL_MAIN, "page");
		textPut("instance.map", gridInstance, PNL_MAIN, "grid");

		//lc->getValue(bncs_string("%1.%2.grid").arg(m_strInstance).arg(m_strLayout), m_grid);
		lc->getValue(bncs_string("%1.%2.group").arg(m_strInstance).arg(m_strLayout), m_group);
		lc->getValue(bncs_string("%1.%2.page").arg(m_strInstance).arg(m_strLayout), m_page);

		debug("router_grid::Get %1 source_grid::Group:%2 Page:%3 Grid:%4 ", bncs_string("%1.%2.grid").arg(m_strInstance).arg(m_strLayout), m_group, m_page, m_grid);

		if (m_group.length() == 0)
		{
			m_group = 1;
		}
		textPut(bncs_string("button.%1=released").arg(m_group), PNL_MAIN, "group");
		

		if (m_page.length() == 0)
		{
			m_page = 1;
		}
		timerStart(TIMER_SET_PAGE_DELAY, TIMER_SET_PAGE_DELAY_DURATION);
		m_currentPage = m_page;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


