#ifndef vip_dest_details_INCLUDED
	#define vip_dest_details_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class vip_dest_details : public bncs_script_helper
{
public:
	vip_dest_details( bncs_client_callback * parent, const char* path );
	virtual ~vip_dest_details();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	bncs_string m_serviceDetails;

	int m_globalDest;
	int m_deviceSource;
	int m_deviceDest;
	int m_destDeviceOffset;
	int m_dest;

	void showDetails();
	void showPopup();
	void showServiceDetails();

};


#endif // vip_dest_details_INCLUDED