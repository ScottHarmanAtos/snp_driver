#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dest_package.h"

#define PNL_MAIN			1
#define POPUP_TAKE_SPECIAL	2
#define POPUP_CONFIRM_PARK	3

#define TIMER_SETUP	1

#define DATABASE_SOURCE_NAME	0
#define DATABASE_DEST_NAME		1
#define DATABASE_PACKAGE_NAME	2  //ADP
#define	DATABASE_MAPPING		8

#define LBL_TALLY		"tally"
#define BTN_DEST		"dest"
#define LBL_PACKAGE		"package_tally"  //ADP

#define TAKE_SPECIAL	"take_special"

#define MANAGE_IFB_REVERSE "manage_ifb_reverse"
#define UPSTREAM_PRESET	"upstream_preset"	//required for PCR OS lines to indicate the audio preset in use on upstream SCR OS line
#define RECALL_PRESET	"recall_preset"

#define LAYOUT_DEFAULT	"main"

#define PRESELECT_NONE		"none"
#define	PRESELECT_DETAILS	"details"
#define PRESELECT_PARK		"park"
#define PRESELECT_LOCK		"lock"
#define PRESELECT_MONITOR	"monitor"
#define PRESELECT_INSPECT	"inspect"

const int PRI_TOP(1);
const int PRI_HIGH(2);
const int PRI_DEFAULT(3);
const int PRI_LOW(4);
const int PRI_BOTTOM(5);

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( dest_package )

// constructor - equivalent to ApplCore STARTUP
dest_package::dest_package( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_index = 0;
	m_devicePackagerRouterMain = 0;
	m_deviceDestRouter = 0;
	m_deviceDestStatus = 0;
	m_slotDestRouter = 0;
	m_slotDestStatus = 0;
	m_currentSource = 0;
	m_undoSource = -1;
	m_showHighlight = true;
	m_selected = false;
	m_layout = LAYOUT_DEFAULT;
	m_preselect = PRESELECT_NONE;
	m_priority = PRI_DEFAULT;

	m_manageIFBReverse = false;
	m_recallPreset = false;
	m_upstreamPreset = false;
	m_lock = false;

	m_firstVirtualPackage = 0;
	m_masterLanguage = 0;
	
	/*
	bncs_config cFirstVirtual("packager_config.packager_auto.first_virtual_package");
	if (cFirstVirtual.isValid())
	{
		m_firstVirtualPackage = cFirstVirtual.attr("value").toInt();
	}
	*/

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, m_layout + ".bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
dest_package::~dest_package()
{
}

// all button pushes and notifications come here
void dest_package::buttonCallback( buttonNotify *b )
{
	b->dump(bncs_string("dest_package::buttonCallback() [%1]").arg(m_index));
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == BTN_DEST)
		{
			if (m_preselect == PRESELECT_NONE)
			{
				hostNotify(bncs_string("dest=%1").arg(m_index));
				hostNotify("button=released");
				hostNotify(bncs_string("current_source=%1").arg(m_currentSource));
				hostNotify(bncs_string("current_function=%1").arg(m_function));


				if (m_showHighlight == true)
				{
					textPut("statesheet", "dest_selected", PNL_MAIN, BTN_DEST);
				}

				m_selected = true;
			}
			else if (m_preselect == PRESELECT_PARK)
			{
				/*
				take("park");
				hostNotify(bncs_string("completed=%1").arg(m_preselect));
				m_preselect = PRESELECT_NONE;
				*/

				if (m_currentSource > 0)
				{
					//Are you sure you want to Park?
					panelPopup(POPUP_CONFIRM_PARK, "popup_confirm_park.bncs_ui");
					bncs_string sourceName;
					routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_NAME, m_currentSource, sourceName);
					textPut("text", bncs_string("Are you sure you want to Park?| |Destination - %1| |Current Source - %2")
						.arg(m_destName.replace('|', ' ')).arg(sourceName.replace('|', ' ')), POPUP_CONFIRM_PARK, "message");
				}
			}
			else if (m_preselect == PRESELECT_LOCK)
			{
				debug("dest_package::buttonCallback() [%1] set lock to %2 on slot %3 of device %4", 
					m_index, m_lock ? "0" : "1", m_slotDestStatus, m_deviceDestStatus);
				infoWrite(m_deviceDestStatus, bncs_string("lock=%1").arg(m_lock ? "0" : "1"), m_slotDestStatus);	//invert lock
				hostNotify(bncs_string("completed=%1").arg(m_preselect));
				m_preselect = PRESELECT_NONE;
			}
			else if (m_preselect == PRESELECT_MONITOR)
			{
				
				hostNotify(bncs_string("monitor_source_dest=%1.%2").arg(m_currentSource).arg(m_index));
				hostNotify(bncs_string("completed=%1").arg(m_preselect));
				m_preselect = PRESELECT_NONE;
			}
			else if (m_preselect == PRESELECT_INSPECT)
			{
				takeSpecial(bncs_string("%1&special").arg(m_currentSource));
				hostNotify(bncs_string("completed=%1").arg(m_preselect));
				m_preselect = PRESELECT_NONE;
			}
		}
	}
	else if (b->panel() == POPUP_CONFIRM_PARK)
	{
		if (b->id() == "confirm")
		{
			take("park");

			panelRemove(POPUP_CONFIRM_PARK);
			hostNotify(bncs_string("completed=%1").arg(m_preselect));
			m_preselect = PRESELECT_NONE;
		}
		else if (b->id() == "cancel")
		{
			panelRemove(POPUP_CONFIRM_PARK);
			hostNotify(bncs_string("completed=%1").arg(m_preselect));
			m_preselect = PRESELECT_NONE;
		}
	}
	else if (b->panel() == POPUP_TAKE_SPECIAL)
	{
		if (b->id() == "close")
		{
			panelDestroy(POPUP_TAKE_SPECIAL);
		}
	}

}

// all revertives come here
int dest_package::revertiveCallback( revertiveNotify * r )
{
	r->dump("dest_package::revertiveCallback()");
	if( r->device() == m_deviceDestRouter && r->index() == m_slotDestRouter)
	{
		updateTally(r->sInfo());
		

	}
	else if (r->device() == m_deviceDestStatus && r->index() == m_slotDestStatus)
	{
		updateStatus(r->sInfo());
	}
	else
	{
		debug("dest_package::revertiveCallback() m_deviceDestRouter=%1 m_slotDestRouter=%2");
	}


	return 0;
}

// all database name changes come back here
void dest_package::databaseCallback( revertiveNotify * r )
{
	//r->dump("dest_package::databaseCallback()");
	if (r->device() == m_devicePackagerRouterMain)
	{
		if (r->database() == DATABASE_SOURCE_NAME && r->index() == m_currentSource)
		{
			bncs_string sourceName = r->sInfo();
			textPut("text", sourceName.replace('|', ' '), PNL_MAIN, LBL_TALLY);
		}
		else if (r->database() == DATABASE_PACKAGE_NAME && r->index() == m_currentSource)
		{
			bncs_string packageName = r->sInfo();
			textPut("text", packageName.replace('|', ' '), PNL_MAIN, LBL_PACKAGE);
		}
		else if (r->database() == DATABASE_DEST_NAME && r->index() == m_index)
		{
			m_destName = r->sInfo();
			textPut("text", m_destName, PNL_MAIN, BTN_DEST);
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dest_package::parentCallback( parentNotify *p )
{
	p->dump(bncs_string("dest_package::parentCallback() dest[%1]").arg(m_index));

	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("layout=%1").arg(m_layout);
			sl << bncs_string("function=%1").arg(m_function);
			sl << bncs_string( "show_highlight=%1").arg(m_showHighlight ? "true" : "false");
			sl << bncs_string("priority=%1").arg(m_priority);
			
			return sl.toString( '\n' );
		}

		else if (p->value() == "layout")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_layout));
		}
		else if (p->value() == "function")
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_function ) );
		}
		else if (p->value() == "show_highlight")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_showHighlight ? "true" : "false"));
		}
		else if (p->value() == "priority")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_priority));
		}
		else if (p->value().startsWith("getIndex"))
		{
			bncs_stringlist sl(p->value(), '.');

			if (sl.count() == 4)
			{
				bncs_string instance = sl[1];
				bncs_string offset = sl[2];
				bncs_string page = sl[3];

				int device = 0;
				if (instance.length() != 0 && getDev(instance, &device))
				{
					bncs_string value;
					routerName(device, DATABASE_MAPPING, offset.toInt() + page.toInt(), value);

					return bncs_string("%1=%2").arg(p->value()).arg(value);
				}
				else
				{
					return bncs_string("%1=").arg(p->value());
				}
			}
		}
	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if( p->command() == "workspace" )
	{	// Persisted value or 'Command' being set here
		m_workspaceId = p->value();
		textPut("text", m_workspaceId, PNL_MAIN, "workspace");

		if (m_manageIFBReverse)
		{
			textPut("workspace", p->value(), PNL_MAIN, MANAGE_IFB_REVERSE);
		}

		initWorkspaceFunction();
	}

	else if (p->command() == "layout")
	{	// Persisted value or 'Command' being set here
		m_layout = p->value();
		setLayout();
	}

	else if (p->command() == "function")
	{	// Persisted value or 'Command' being set here
		m_function = p->value();
		textPut("text", m_function, PNL_MAIN, "function");
		initWorkspaceFunction();
	}

	else if (p->command() == "show_highlight")
	{
		if (p->value().upper() == "FALSE")
		{
			m_showHighlight = false;
		}
		else
		{
			m_showHighlight = true;
		}
	}
	else if (p->command() == "priority")
	{	// Persisted value or 'Command' being set here
		int value = p->value().toInt();
		if (value == PRI_BOTTOM || value == PRI_LOW || value == PRI_HIGH || value == PRI_TOP)
		{
			m_priority = value;
		}
		else
		{ 
			m_priority = PRI_DEFAULT;
		}
	}
	else if (p->command() == "index")
	{	// Persisted value or 'Command' being set here
		m_index = p->value().toInt();
		initDestPackage();
	}
	else if (p->command() == "dest" && m_index > 0)
	{
		int intSelect = p->value().toInt();
		m_selected = (intSelect == m_index) ? true : false;

		if (m_showHighlight)
		{
			if (m_selected)
			{
				textPut("statesheet", "dest_selected", PNL_MAIN, BTN_DEST);
			}
			else
			{
				textPut("statesheet", "dest_deselected", PNL_MAIN, BTN_DEST);
			}
		}
	}
	else if (p->command() == "take")
	{

		if (m_lock == false)
		{
			if (m_preselect == PRESELECT_MONITOR)
			{
				if (p->value().endsWith("&special"))
				{
					takeSpecial(p->value());
				}
				else
				{
					// find the destination
					// passed is source, dest

					bncs_stringlist slstData = bncs_stringlist(p->value(), '.');

					if (slstData.count() == 2)
					{

						bncs_string sSource = slstData[0];;
						bncs_string sDest = slstData[1];

						take(sSource);

						// set the preset and dest of 'That' using 'This' details
						textPut("set_preset_dest_index", bncs_string("%1").arg(sDest), PNL_MAIN, RECALL_PRESET);
					}
				}
			}
			else
			{
				if (m_selected)
				{

					if (p->value().endsWith("&special"))
					{
						takeSpecial(p->value());
					}
					else
					{
						// find the destination
						take(p->value());
					}
				}
			}
		}
	}
	else if (p->command() == "undo")
	{
		if (m_lock == false)
		{
			if (m_selected)
			{
				undo();
			}
		}

		//reset undo source
		//m_undoSource = -1;
	}
	else if (p->command() == "preselect" && m_index > 0)
	{
		if (p->value() == PRESELECT_PARK)
		{
			m_preselect = PRESELECT_PARK;
		}
		else if (p->value() == PRESELECT_DETAILS)
		{
			m_preselect = PRESELECT_DETAILS;
		}
		else if (p->value() == PRESELECT_NONE)
		{
			m_preselect = PRESELECT_NONE;
		}
		else if (p->value() == PRESELECT_LOCK)
		{
			m_preselect = PRESELECT_LOCK;
		}
		else if (p->value() == PRESELECT_MONITOR)
		{
			m_preselect = PRESELECT_MONITOR;
		}
		else if (p->value() == PRESELECT_INSPECT)
		{
			m_preselect = PRESELECT_INSPECT;
		}
		else
		{
			//backstop
			m_preselect = PRESELECT_NONE;
		}
	}
	else if (p->command() == "deselect")
	{
		m_preselect = PRESELECT_NONE;
		if (m_showHighlight && m_selected)
		{
			textPut("statesheet", "dest_deselected", PNL_MAIN, BTN_DEST);
		}
	}
	else if (p->command() == "master_language")
	{
		if (m_manageIFBReverse)
		{
			textPut("master_language", p->value(), PNL_MAIN, MANAGE_IFB_REVERSE);
		}
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void dest_package::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void dest_package::initWorkspaceFunction()
{
	bncs_string section;
	bncs_string function;
	m_function.split('.', section, function);

	bncs_config c(bncs_string("workspace_resources/%1.%2").arg(m_workspaceId).arg(section));
	bool init = false;
	if (c.isValid())
	{
		while (c.isChildValid())
		{
			bncs_string id = c.childAttr("id");
			bncs_string value = c.childAttr("value");
			debug("dest_package::initWorkspaceFunction() %1=%2", id, value);
			if (id == function)
			{
				init = true;
				m_index = value.toInt();
				controlEnable(PNL_MAIN, BTN_DEST);

				initDestPackage();
				return;
			}

			c.nextChild();
		}
		//Workspace function not found
		if (init == false)
		{
			//textPut("text", bncs_string("%1|Not Found").arg(m_function), PNL_MAIN, BTN_DEST);
			controlDisable(PNL_MAIN, BTN_DEST);
		}
	}
	else
	{
		textPut("text", "-", PNL_MAIN, BTN_DEST);
	}
}

void dest_package::initDestPackage()
{
	/*
	<instance composite="yes" id="c_packager" >
	  <group id="router" instance="c_packager_router" />
	  <group id="mm_stack" instance="c_packager_mm_stack" />
	  <group id="audio_package_returns" instance="c_packager_audio_package" />
	  <group id="dest_status" instance="c_packager_dest_status" />
	  <group id="source_pti" instance="c_packager_source_pti" />
	  <group id="nevion_dest_use" instance="c_packager_nevion_dest" />
	  <group id="riedel " instance="c_packager_riedel" />
	  <group id="video_router" instance="vip_router_video_hd_01" /> <!-- test group for adjust -->
	</instance>
	*/

	if (m_showHighlight && m_selected)
	{
		//Clear any existing highlight
		textPut("statesheet", "dest_deselected", PNL_MAIN, BTN_DEST);
		m_selected = false;
	}


	if (m_index > 0)
	{
		//Reset destination
		m_currentSource = 0;
		m_undoSource = -1;

		//Get top level packager instance of the tech hub for this workstation
		m_compositePackager = getWorkstationSetting("packager_instance");
		m_packagerRouter = bncs_packager(m_compositePackager);
		//debug("dest_package::initDestPackage() m_packager [%1] is %2", m_compositePackager, m_packagerRouter.isValid() ? "valid" : "NOT valid");

		m_packagerDestStatus = bncs_packager(m_compositePackager, "dest_status");
		//debug("dest_package::initDestPackage() dest_status is %1", m_packagerDestStatus.isValid() ? "valid" : "NOT valid");

		m_firstVirtualPackage = m_packagerRouter.firstVirtual();
		//debug("dest_package::initDestPackage() firstVirtual is %1", m_firstVirtualPackage);

		//Get main router device
		m_devicePackagerRouterMain = m_packagerRouter.destDev(1);
		routerName(m_devicePackagerRouterMain, DATABASE_DEST_NAME, m_index, m_destName);
		textPut("text", m_destName, PNL_MAIN, BTN_DEST);

		//register for database changes
		infoRegister(m_devicePackagerRouterMain, 1, 1);

		if (m_index >= m_firstVirtualPackage)
		{
			textPut("pixmap.topLeft", "/images/icon_virtual.png", PNL_MAIN, BTN_DEST);
		}
		else
		{
			textPut("pixmap.topLeft", "", PNL_MAIN, BTN_DEST);
		}

		//Get device for destination
		m_deviceDestRouter = m_packagerRouter.destDev(m_index);
		m_slotDestRouter = m_packagerRouter.destSlot(m_index);
		infoRegister(m_deviceDestRouter, m_slotDestRouter, m_slotDestRouter);
		infoPoll(m_deviceDestRouter, m_slotDestRouter, m_slotDestRouter);
		textPut("text", bncs_string("%1/%2").arg(m_deviceDestRouter).arg(m_slotDestRouter), PNL_MAIN, LBL_TALLY);
		textPut("text", bncs_string("%1/%2").arg(m_deviceDestRouter).arg(m_slotDestRouter), PNL_MAIN, LBL_PACKAGE); //ADP

		//Dest status
		m_deviceDestStatus = m_packagerDestStatus.destDev(m_index);
		m_slotDestStatus = m_packagerDestStatus.destSlot(m_index);
		infoRegister(m_deviceDestStatus, m_slotDestStatus, m_slotDestStatus);
		infoPoll(m_deviceDestStatus, m_slotDestStatus, m_slotDestStatus);

		//Init recall preset
		if (m_recallPreset)
		{
			textPut("dest_index", m_index, PNL_MAIN, RECALL_PRESET);
		}

		if (m_manageIFBReverse)
		{
			textPut("dest_index", m_index, PNL_MAIN, MANAGE_IFB_REVERSE);
		}

		debug("dest_package::initDestPackage() m_deviceDestRouter=%1 m_deviceDestStatus=%2", m_deviceDestRouter, m_deviceDestStatus);
	}
	else
	{
		m_destName = "";
		textPut("text", m_destName, PNL_MAIN, BTN_DEST);
		textPut("pixmap.topLeft", "", PNL_MAIN, BTN_DEST);
		textPut("pixmap.topRight", "", PNL_MAIN, BTN_DEST);
		textPut("text", "", PNL_MAIN, LBL_TALLY);
		textPut("text", "", PNL_MAIN, LBL_PACKAGE);
	}
}

void dest_package::take(bncs_string source)
{
	if (source == "park")
	{
		m_undoSource = m_currentSource;

		bncs_string command = bncs_string("index=%1,pri=%2").arg(0).arg(m_priority);
		infoWrite(m_deviceDestRouter, command, m_slotDestRouter);
	}
	else
	{
		int sourceIndex = source.toInt();
		if (sourceIndex > 0)
		{
			m_undoSource = m_currentSource;
			bncs_string command = bncs_string("index=%1,pri=%2").arg(sourceIndex).arg(m_priority);
			infoWrite(m_deviceDestRouter, command, m_slotDestRouter);
		}
	}

	textPut("text", bncs_string("take %1").arg(source), PNL_MAIN, "on_air");
}

void dest_package::takeSpecial(bncs_string source)
{
	bncs_string index, special;
	source.split('&', index, special);
	int sourceIndex = index.toInt();
	if (sourceIndex > 0)
	{
		panelPopup(POPUP_TAKE_SPECIAL, "popup_take_special.bncs_ui");
		textPut("text", bncs_string("Take Special: source %1 to dest %2").arg(sourceIndex).arg(m_index), POPUP_TAKE_SPECIAL, "title");
		textPut("packager_router_main_device", m_devicePackagerRouterMain, POPUP_TAKE_SPECIAL, TAKE_SPECIAL);
		textPut("source", sourceIndex, POPUP_TAKE_SPECIAL, TAKE_SPECIAL);
		textPut("dest", m_index, POPUP_TAKE_SPECIAL, TAKE_SPECIAL);
	}
	textPut("text", bncs_string("take %1").arg(source), PNL_MAIN, "on_air");

}

void dest_package::undo()
{
	debug("dest_package::undo() [%1] selected=%2 m_currentSource=%3 m_undoSource=%4 ", m_index, m_selected?"true":"false", m_currentSource, m_undoSource);

	//Check if an undo source is set - note: 0 means park is the undo source
	if (m_undoSource > -1)
	{
		bncs_string command = bncs_string("index=%1,pri=%2").arg(m_undoSource).arg(m_priority);
		infoWrite(m_deviceDestRouter, command, m_slotDestRouter);
	}
}

void dest_package::setLayout()
{
	panelDestroy(PNL_MAIN);
	bool error = panelShow(PNL_MAIN, m_layout + ".bncs_ui");
	if (error == true)
	{ 
		m_layout = LAYOUT_DEFAULT;
		panelShow(PNL_MAIN, m_layout + ".bncs_ui");
	}

	//Check for layout features
	m_manageIFBReverse = getIdList(PNL_MAIN, MANAGE_IFB_REVERSE).count() > 0;
	if (m_manageIFBReverse)
	{
		textPut("dest_index", 0, PNL_MAIN, MANAGE_IFB_REVERSE);
	}	
	
	m_recallPreset = getIdList(PNL_MAIN, RECALL_PRESET).count() > 0;
	if (m_recallPreset)
	{
		textPut("dest_index", 0, PNL_MAIN, RECALL_PRESET);
	}

	m_upstreamPreset = getIdList(PNL_MAIN, UPSTREAM_PRESET).count() > 0;
	if (m_upstreamPreset)
	{
		textPut("dest_index", 0, PNL_MAIN, UPSTREAM_PRESET);
	}



	debug("dest_package::setLayout() dest[%1] layout=%2 recallPreset=%3", m_index, m_layout, m_recallPreset?"true":"false");
}

void dest_package::updateStatus(bncs_string status)
{
	//video=1,audio=1,anc=1,trace=2001,lock=1

	bncs_stringlist sltStatus(status);
	int lock = sltStatus.getNamedParam("lock").toInt();

	if (lock == 1)
	{
		textPut("pixmap.topRight", "/images/icon_padlock.png", PNL_MAIN, BTN_DEST);
		m_lock = true;
	}
	else
	{
		textPut("pixmap.topRight", "", PNL_MAIN, BTN_DEST);
		m_lock = false;
	}
}

void dest_package::updateTally(bncs_string tally)
{
	bncs_stringlist sltTally(tally);
	m_currentSource = sltTally.getNamedParam("index").toInt();

	if (m_undoSource == -1)
	{
		//Set initial value for undo
		m_undoSource = m_currentSource;
	}

	bncs_string sourceName = "---";
	bncs_string packageName = "---";
	if (m_currentSource > 0)
	{
		routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_NAME, m_currentSource, sourceName);
		routerName(m_devicePackagerRouterMain, DATABASE_PACKAGE_NAME, m_currentSource, packageName);

		if (m_upstreamPreset)
		{
			//check if upstream source is a virtual e.g. SCR OS
			if (m_currentSource >= m_firstVirtualPackage)
			{
				textPut("dest_index", m_currentSource, PNL_MAIN, UPSTREAM_PRESET);
			}
			else
			{
				textPut("dest_index", 0, PNL_MAIN, UPSTREAM_PRESET);
			}
		}
	}
	textPut("text", sourceName.replace('|', ' '), PNL_MAIN, LBL_TALLY);
	textPut("text", packageName.replace('|', ' '), PNL_MAIN, LBL_PACKAGE);
}