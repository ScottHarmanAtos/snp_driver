#ifndef dest_package_INCLUDED
	#define dest_package_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dest_package : public bncs_script_helper
{
public:
	dest_package( bncs_client_callback * parent, const char* path );
	virtual ~dest_package();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;

	bncs_string m_workspaceId;
	bncs_string m_function;

	bncs_string m_compositePackager;				//c_packager
	//bncs_string m_compositePackagerRouter;			//c_packager_router
	//bncs_string m_compositePackagerDestStatus;		//c_packager_dest_status

	//bncs_string m_instancePackagerRouterMain;		//packager_router_main		dev=1000
	//bncs_string m_instancePackagerRouter;			//packager_router_n			dev=1001-1016
	//bncs_string m_instancePackagerDestStatus;		//packager_dest_status_n	dev=1061-1076

	bncs_string m_destName;
	bncs_string m_layout;

	bncs_string m_preselect;

	bncs_packager m_packagerRouter;
	bncs_packager m_packagerDestStatus;

	bool m_showHighlight;
	bool m_selected;
	bool m_manageIFBReverse;
	bool m_recallPreset;
	bool m_upstreamPreset;
	bool m_lock;

	int m_index;
	int m_devicePackagerRouterMain;
	int m_deviceDestRouter;
	int m_deviceDestStatus;
	int m_slotDestRouter;
	int m_slotDestStatus;
	int m_currentSource;
	int m_priority;
	int m_undoSource;

	int m_firstVirtualPackage;
	int m_masterLanguage;

	void initWorkspaceFunction();
	void initDestPackage();
	void setLayout();
	void take(bncs_string source);
	void takeSpecial(bncs_string source);
	void undo();
	void updateTally(bncs_string tally);
	void updateStatus(bncs_string status);

};


#endif // dest_package_INCLUDED