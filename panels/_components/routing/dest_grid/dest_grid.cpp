#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dest_grid.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( dest_grid )

#define PANEL_MAIN					1
#define DATABASE_MAPPING			8
#define OFFSET_DEST_PAGE_MAPPING	1000
#define PAGE_SIZE					32
#define DEFAULT_LAYOUT				"grid_5x18x24"

// constructor - equivalent to ApplCore STARTUP
dest_grid::dest_grid( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_strInstance = "";
	m_intDeviceRouter = 0;
	m_intSelected = 0;

	m_strLayout = DEFAULT_LAYOUT;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, bncs_string("%1.bncs_ui").arg(m_strLayout) );

	//Init destinations
	for(int intDest = 0; intDest < PAGE_SIZE; intDest++)
	{
		textPut( "dest_index", "0", PANEL_MAIN, bncs_string("dest_%1").arg(intDest + 1) );
	}

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
dest_grid::~dest_grid()
{
}

// all button pushes and notifications come here
void dest_grid::buttonCallback( buttonNotify *b )
{
	bncs_stringlist sltControlName = bncs_stringlist(b->id(), '_');
	bncs_string strControlPrefix = sltControlName[0];
	int intNewControlIndex = sltControlName[1].toInt();

	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == "destPage" && b->command() == "index")
		{
			int intPage = b->value().toInt();
			if (intPage > 0)
			{
				textPut("deselect","deselect", PANEL_MAIN, bncs_string("dest_%1").arg(m_intSelected));
				m_intSelected = 0;

				bncs_string strMapping;
				routerName(m_intDeviceRouter, DATABASE_MAPPING, OFFSET_DEST_PAGE_MAPPING + intPage, strMapping);
				//textPut("text", bncs_string("dev=%1 page=%2 mapping=%3")
				//	.arg(m_intDeviceRouter).arg(intPage).arg(strMapping), PANEL_MAIN, "grpDestgrid");

				m_sltDestPage = bncs_stringlist().fromString(strMapping);
				for(int intDest = 0; intDest < PAGE_SIZE; intDest++)
				{
					textPut( "dest_index", m_sltDestPage[intDest], PANEL_MAIN, bncs_string("dest_%1").arg(intDest + 1) );
				}
				hostNotify(bncs_string("dest=%1").arg(-1));
			}
			else
			{
				textPut("deselect","deselect", PANEL_MAIN, bncs_string("dest_%1").arg(m_intSelected));
				m_intSelected = 0;

				m_sltDestPage.clear();
				for(int intDest = 0; intDest < PAGE_SIZE; intDest++)
				{
					textPut( "dest_index", "0", PANEL_MAIN, bncs_string("dest_%1").arg(intDest + 1) );
				}
				hostNotify(bncs_string("dest=%1").arg(-1));
			}
		}
		else if(strControlPrefix == "dest")
		{
			debug("dest_grid::buttonCallback() command=%1 value=%2", b->command(), b->value());
			if(b->command() == "completed")
			{
				hostNotify(bncs_string("%1=%2").arg(b->command()).arg(b->value()));
				for(int intDest = 0; intDest < PAGE_SIZE; intDest++)
				{
					textPut( "preselect", "none", PANEL_MAIN, bncs_string("dest_%1").arg(intDest + 1) );
				}
			}
			textPut("deselect","deselect", PANEL_MAIN, bncs_string("dest_%1").arg(m_intSelected));
			m_intSelected = intNewControlIndex;
			hostNotify(bncs_string("dest=%1").arg(m_sltDestPage[m_intSelected - 1]));
			
			m_intCurrentDest = (m_sltDestPage[m_intSelected - 1]).toInt();
			textPut("dest", m_intCurrentDest, PANEL_MAIN, bncs_string("dest_%1").arg(m_intSelected));
		}
	}
}

// all revertives come here
int dest_grid::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void dest_grid::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dest_grid::parentCallback( parentNotify *p )
{
	debug("dest_grid::parentCallback() command=%1 value=%2 sub(0)=%3", 
		p->command(), p->value(), p->sub(0));
	if( p->command() == "instance" )
	{
		m_strInstance = p->value();
		getDev(m_strInstance, &m_intDeviceRouter);
	}
	else if(p->command() == "take")
	{
		int intSource = p->value().toInt();
		//infoWrite(m_intDeviceRouter, bncs_string("index=%1,level=1").arg(intSource), m_intSelected);
		textPut("route",bncs_string("%1,%2").arg(intSource).arg(m_intCurrentDest), PANEL_MAIN, bncs_string("dest_%1").arg(m_intSelected));
	}
	else if(p->command() == "undo")
	{
		int intUndoDest = p->value();
		if(intUndoDest == m_intCurrentDest)
		{
			textPut("undo", p->value(), PANEL_MAIN, bncs_string("dest_%1").arg(m_intSelected));
		}
	}
	else if(p->command() == "preselect")
	{
		for(int intDest = 0; intDest < PAGE_SIZE; intDest++)
		{
			textPut( "preselect", p->value(), PANEL_MAIN, bncs_string("dest_%1").arg(intDest + 1) );
		}
	}
	else if(p->command() == "layout")
	{
		m_strLayout = p->value();
		panelDestroy( PANEL_MAIN );
		panelShow( PANEL_MAIN, bncs_string("%1.bncs_ui").arg(m_strLayout) );

		//Refresh instance targeting
		textPut("instance", m_strInstance, PANEL_MAIN, "destGroup");
		textPut("instance", m_strInstance, PANEL_MAIN, "destPage");
		textPut("instance", m_strInstance, PANEL_MAIN, "destGrid");
		
		textPut("instance.map", m_strInstance, PANEL_MAIN, "destGroup");
		textPut("instance.map", m_strInstance, PANEL_MAIN, "destPage");
		textPut("instance.map", m_strInstance, PANEL_MAIN, "destGrid");

		for(int intDest = 0; intDest < PAGE_SIZE; intDest++)
		{
			textPut( "dest_index", "0", PANEL_MAIN, bncs_string("dest_%1").arg(intDest + 1) );
		}

	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;
		sl << "layout=" + m_strLayout;
		
		return sl.toString( '\n' );
	}
	return "";
}

// timer events come here
void dest_grid::timerCallback( int id )
{
}
