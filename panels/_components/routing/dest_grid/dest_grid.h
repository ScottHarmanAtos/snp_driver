#ifndef dest_grid_INCLUDED
	#define dest_grid_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dest_grid : public bncs_script_helper
{
public:
	dest_grid( bncs_client_callback * parent, const char* path );
	virtual ~dest_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_intCurrentDest;
	bncs_stringlist m_sltDestPage;
	int m_intSelected;
	int m_intDeviceRouter;
	bncs_string m_strInstance;
	bncs_string m_strLayout;
	
};


#endif // dest_grid_INCLUDED