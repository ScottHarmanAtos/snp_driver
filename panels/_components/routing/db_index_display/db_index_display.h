#ifndef db_index_display_INCLUDED
	#define db_index_display_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class db_index_display : public bncs_script_helper
{
public:
	db_index_display( bncs_client_callback * parent, const char* path );
	virtual ~db_index_display();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );

	void displayIndex();
	void changePanel();
	int m_db;
	int m_index;
	int m_offset;
	bncs_string m_instance;
	int m_dev;
	bncs_string m_display_text;
	bncs_string m_skin;
	
	//TH to strip pipes
	bncs_string m_ssingleline;
	bool m_bsingleline;
	bncs_string m_strOldText;
	bncs_string m_strNewText;

};


#endif // db_index_display_INCLUDED