#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "db_index_display.h"

#define PANEL_MAIN			1
#define POPUP_KEYBOARD		2

#define TIMER_INIT			1
#define TIMER_INIT_TIMEOUT  10

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( db_index_display )

// constructor - equivalent to ApplCore STARTUP
db_index_display::db_index_display( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_skin = "300x30";
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "skins/" + m_skin +".bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel

	m_instance = "";
	m_db = -1;
	m_index = 0;
	m_offset = 0;
	m_dev = 0;
	m_display_text = "";
	controlDisable(PANEL_MAIN, "btnEdit");
//	timerStart(TIMER_INIT, TIMER_INIT_TIMEOUT);
	displayIndex();	
}

// destructor - equivalent to ApplCore CLOSEDOWN
db_index_display::~db_index_display()
{
}

// all button pushes and notifications come here
void db_index_display::buttonCallback( buttonNotify *b )
{
//	bncs_string strOldText;
	if ( b->panel() == PANEL_MAIN)
	{
		if ( b->id() == "btnEdit")
		{
			m_strOldText = m_display_text;
			panelPopup(POPUP_KEYBOARD,"keyboard.bncs_ui");
			textPut("text", m_strOldText, POPUP_KEYBOARD, "keyboard");
			textPut("text", m_strOldText, POPUP_KEYBOARD, "preview");
		}
		else if (b->id() == "label")
		{
			hostNotify(bncs_string("%1=%2").arg(b->command()).arg(b->value()));
		}
	}
	else if ( b->panel() == POPUP_KEYBOARD)
	{
		if (b->id() == "keyboard")
		{
//			bncs_string strNewText;
			debug("command=%1 sub_0=%2 sub_1=%3", b->command(), b->sub(0), b->sub(1));
			if ( b->sub(0) == "return" )
			{
				textGet("text",POPUP_KEYBOARD, "keyboard", m_strNewText );
				panelDestroy(POPUP_KEYBOARD);
				if ( m_strNewText != m_strOldText)
				{
					routerModify(m_dev, m_db,m_index, m_strNewText,true);
				}
			}
			else
			{
				textGet("text",POPUP_KEYBOARD, "keyboard", m_strNewText );
				textPut("text", m_strNewText, POPUP_KEYBOARD, "preview");
				int intCharCount = m_strNewText.length();
				textPut("text", bncs_string("%1 characters left").arg(255-intCharCount), POPUP_KEYBOARD, "charcount");
			}
		}
		else if (b->id() == "close")
		{
			panelDestroy(POPUP_KEYBOARD);
		}

	}
}

// all revertives come here
int db_index_display::revertiveCallback( revertiveNotify * r )
{

	return 0;
}

// all database name changes come back here
void db_index_display::databaseCallback( revertiveNotify * r )
{
	if(r->device() == m_dev && r->index() == m_index && r->database() == m_db)
	{
		m_display_text = r->sInfo();
		textPut("text",m_display_text, PANEL_MAIN, "label");
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string db_index_display::parentCallback( parentNotify *p )
{
	if(p->command() == "instance")
	{
		m_instance = p->value();
		timerStart(TIMER_INIT, TIMER_INIT_TIMEOUT);
	}
	else if(p->command() == "skin")
	{
		m_skin = p->value();
		changePanel();
	}
	else if (p->command() == "db")
	{
		if(p->value() != bncs_string(m_db))
		{
			m_db = p->value();
			timerStart(TIMER_INIT, TIMER_INIT_TIMEOUT);
		}
	}
	else if( p->command() == "singleline" )
	{

		bncs_string tempString = p->value().upper();
		if(tempString =="TRUE"||tempString =="YES"||tempString =="1")
		{
			m_bsingleline=true;
		}
		else
		{
			m_bsingleline=false;
		}
		timerStart(TIMER_INIT, TIMER_INIT_TIMEOUT);
	}
	else if(p->command() == "index")
	{
		if(p->value() != bncs_string(m_index))
		{
			m_index = p->value().toInt();
			timerStart(TIMER_INIT, TIMER_INIT_TIMEOUT);

		}
	}
	else if (p->command() == "offset")
	{
		if (p->value() != bncs_string(m_offset))
		{
			m_offset = p->value().toInt();
			timerStart(TIMER_INIT, TIMER_INIT_TIMEOUT);

		}
	}
	else if (p->command() == "return")
	{
		bncs_stringlist sl;
		sl << "skin=" + bncs_string(m_skin);
		sl << "db=" + bncs_string(m_db);
		sl << "index=" + bncs_string(m_index);
		sl << "offset=" + bncs_string(m_offset);
		sl << "singleline=" + bncs_string(m_bsingleline == true ? "true" : "false");
		return sl.toString('\n');
	}
	else if(p->command() == "_commands")
	{
		bncs_stringlist sl;
		
		sl << "index=[value]";
		sl << "offset=[value]";

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void db_index_display::timerCallback( int id )
{
	if (id = TIMER_INIT)
	{
		debug("db_index_display::init timer running");
		getDev(m_instance,&m_dev);
		displayIndex();
		timerStop(TIMER_INIT);
	}
	
}

void db_index_display::displayIndex()
{
	debug("db_index_display::displayIndex m_dev:%1 m_db:%2 m_index:%3 m_offset: %4 m_display_text:%5", m_dev, m_db, m_index, m_offset, m_display_text);
	if(m_dev > 0 && m_db > -1 && m_index > 0)
	{
		routerRegister(m_dev,m_index,m_index);
		routerName(m_dev,m_db,m_index + m_offset,m_display_text);
		
		if ( m_display_text == "!!!" )
		{
			m_display_text = "";
		}
		if ( m_bsingleline == true)
		{
			m_display_text.replace("|", " ");
		}
		textPut("text",m_display_text, PANEL_MAIN, "label");
		controlEnable(PANEL_MAIN, "btnEdit");
	
		hostNotify("name=" + m_display_text);
	}
	else
	{
		controlDisable(PANEL_MAIN, "btnEdit");
		textPut("text","", PANEL_MAIN, "label");

	}
}

void db_index_display::changePanel()
{
				
	if(m_skin.right(8) == ".bncs_ui")
	{
		m_skin = m_skin.left(m_skin.length() - 8);
	}
	
	panelDestroy(PANEL_MAIN);
	panelShow(PANEL_MAIN,"skins/" + m_skin + ".bncs_ui");
	timerStart(TIMER_INIT, TIMER_INIT_TIMEOUT);
	//		displayIndex();
}