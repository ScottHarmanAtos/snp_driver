#ifndef package_take_INCLUDED
	#define package_take_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class package_take : public bncs_script_helper
{
public:
	package_take( bncs_client_callback * parent, const char* path );
	virtual ~package_take();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_layout;

	/*
	<instance composite="yes" id="c_packager_router" ref="" type="">
	<group id="packager_router_1" instance="packager_router_main" />
	*/
	bncs_string m_instance;					//c_packager_router
	bncs_string m_packagerMainInstance;		//packager_router_main dev_1001 - 

	bncs_string m_compositePackager;				//c_packager or c_packager_nhn, c_packager_ldc
	bncs_packager m_packagerRouter;
	bncs_packager m_packagerDestStatus;

	bool m_locked;
	bool m_parkSelected;
	bool m_undoPossible;

	int m_presetSource;
	int m_presetDest;
	int m_devicePackagerRouterMain;
	int m_deviceDestStatus;
	int m_slotDestStatus;

	void initPackager();
	void showPresetSource();
	void showPresetDest();
	void setLayout();
	void checkEnableState();
	void updateStatus(bncs_string status);

};


#endif // package_take_INCLUDED