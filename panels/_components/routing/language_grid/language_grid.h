#ifndef language_grid_INCLUDED
	#define language_grid_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class language_grid : public bncs_script_helper
{
public:
	language_grid( bncs_client_callback * parent, const char* path );
	virtual ~language_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	
	int m_devicePackageRouter;
	int m_selected;

	void init();
	void setSelection(int select);
};


#endif // language_grid_INCLUDED