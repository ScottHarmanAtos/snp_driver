#ifndef take_special_source_video_INCLUDED
	#define take_special_source_video_INCLUDED

#include "bncs_packager.h"
#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class take_special_source_video : public bncs_script_helper
{
public:
	take_special_source_video( bncs_client_callback * parent, const char* path );
	virtual ~take_special_source_video();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_compositePackager;				//c_packager

	int m_sp;
	int m_displayLevel;
	int m_devicePackagerRouterMain;
	int m_deviceVideoRouterHD;

	bncs_packager m_packagerRouter;

	void init();
	void showSourceVideo(bool init);

};


#endif // take_special_source_video_INCLUDED