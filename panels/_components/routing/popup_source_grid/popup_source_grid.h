#ifndef popup_source_grid_INCLUDED
	#define popup_source_grid_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class popup_source_grid : public bncs_script_helper
{
public:
	popup_source_grid( bncs_client_callback * parent, const char* path );
	virtual ~popup_source_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_layout;
	bncs_string m_workspace;

	int m_dest;

	void showPopup();
};


#endif // popup_source_grid_INCLUDED