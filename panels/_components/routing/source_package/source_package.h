#ifndef source_package_INCLUDED
	#define source_package_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class source_package : public bncs_script_helper
{
public:
	source_package( bncs_client_callback * parent, const char* path );
	virtual ~source_package();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );

	void showSourceName();
	void showSourceTitle();
	
private:
	bncs_string m_layout;
	bncs_string m_instance;
	bncs_string m_sourceName;
	bncs_string m_sourceTitle;

	bncs_string m_compositePackager;				//c_packager_nhn or c_packager_ldc
	bncs_packager m_packagerRouter;

	int m_packageIndex;
	int m_videoIndex;
	int m_firstVirtualPackage;

	int m_devicePackagerRouter;
	int m_deviceVideoRouter;

	bool m_selected;

	bool m_showHighlight;
	
};


#endif // source_package_INCLUDED