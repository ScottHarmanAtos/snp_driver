#ifndef vip_source_details_INCLUDED
	#define vip_source_details_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class vip_source_details : public bncs_script_helper
{
public:
	vip_source_details( bncs_client_callback * parent, const char* path );
	virtual ~vip_source_details();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	
	int m_deviceSource;
	int m_source;

	void showDetails();
	void showPopup();
};


#endif // vip_source_details_INCLUDED