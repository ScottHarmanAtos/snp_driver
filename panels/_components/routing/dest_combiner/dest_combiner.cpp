#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dest_combiner.h"

#define PNL_MAIN				1
#define POPUP_TAKE_SPECIAL		2
#define POPUP_CONFIRM_PARK		3

#define TIMER_SETUP				1

#define DATABASE_SOURCE_NAME	0
#define DATABASE_DEST_NAME		1
#define	DATABASE_MAPPING		8

#define LBL_TALLY_1			"tally_1"
#define LBL_TALLY_2			"tally_2"
#define BTN_DEST			"button"

#define TAKE_SPECIAL		"take_special"
#define RECALL_PRESET		"recall_preset"
#define LAYOUT_DEFAULT		"combiner_2" // 2 Tallies

#define PRESELECT_NONE		"none"
#define	PRESELECT_DETAILS	"details"
#define PRESELECT_PARK		"park"
#define PRESELECT_LOCK		"lock"
#define PRESELECT_MONITOR	"monitor"
// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(dest_combiner)

// constructor - equivalent to ApplCore STARTUP
dest_combiner::dest_combiner(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path)
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	m_device = 0;
	m_index = 0;
	m_dest_database = 1;
	m_source_database = 0;

	m_sMainRouterInstance = "";
	m_sBackupRouterInstance = "";
	m_iMainRouterDevice = 0;
	m_iBackupRouterDevice = 0;

	m_iSourceMain = 0;
	m_iSourceBackup = 0;
	m_iStatus = 0;
	m_lastSource = 0;

	m_sPark = "0";
	m_layout = LAYOUT_DEFAULT;
	m_showHighlight = true;
	m_instance = "";

	m_selected = false;
	m_preselect = PRESELECT_NONE;
	m_map_mode = false;
	m_isEnabled = true;
	m_isLocked = false;
	m_selectedOnStart = false;
	m_singleLine = false;

	m_startupInit.clear();
	m_startupInit << "instance";
	m_startupInit << "index";
	m_startupInit << "dest_database";
	m_startupInit << "source_database";	
	m_startupInit << "layout";

	panelShow(PNL_MAIN, m_layout + ".bncs_ui");
	m_startupInit << "selected_on_start";
}

// destructor - equivalent to ApplCore CLOSEDOWN
dest_combiner::~dest_combiner()
{
}

// all button pushes and notifications come here
void dest_combiner::buttonCallback(buttonNotify *b)
{
	b->dump(bncs_string("dest_combiner::buttonCallback() [%1]").arg(m_index));
	if( b->panel() == PNL_MAIN )
	{                                                                                                                                                                                                                     
		if( b->id() == BTN_DEST)
		{
		
			if (m_preselect == PRESELECT_NONE && m_isLocked == false)
			{
				
				m_selected = true;
				textPut("statesheet", "dest_selected", PNL_MAIN, BTN_DEST);

				hostNotify(bncs_string("dest=%1").arg(m_index));
				hostNotify("button=released");

				hostNotify(bncs_string("lock_instance_index=%1,%2").arg(bncs_string("%1_lock").arg(m_instance)).arg(m_index));
				hostNotify(bncs_string("instance=%1").arg(m_instance));
				hostNotify("index." + bncs_string("%1").arg(m_destName) + "=" + m_index);
				hostNotify(bncs_string("index_only=").append(m_index));
//				hostNotify(bncs_string("current_source=%1").arg(m_source));

			}
			else if (m_preselect == PRESELECT_PARK)
			{
				/*
				take("park");
				hostNotify(bncs_string("completed=%1").arg(m_preselect));
				m_preselect = PRESELECT_NONE;
				*/

				if (m_iSourceMain > 0)
				{
					//Are you sure you want to Park?
					panelPopup(POPUP_CONFIRM_PARK, "popup_confirm_park.bncs_ui");
					bncs_string sourceName;
					routerName(m_device, DATABASE_SOURCE_NAME, m_iSourceMain, sourceName);
					textPut("text", bncs_string("Are you sure you want to Park?| |Destination - %1| |Current Source - %2")
						.arg(m_destName.replace('|', ' ')).arg(sourceName.replace('|', ' ')), POPUP_CONFIRM_PARK, "message");
					

				}
			}
			else if (m_preselect == PRESELECT_LOCK)
			{
				debug("dest_combiner::buttonCallback() [%1] set lock to %2 on slot %3 of device %4",
					m_index, m_deviceLock ? "0" : "1", m_index, m_deviceLock);
				infoWrite(m_deviceLock, bncs_string("lock=%1").arg(m_deviceLock ? "0" : "1"), m_deviceLock);	//invert lock
				hostNotify(bncs_string("completed=%1").arg(m_preselect));
				m_preselect = PRESELECT_NONE;
			}
			else if (m_preselect == PRESELECT_MONITOR)
			{

				hostNotify(bncs_string("monitor_source_dest=%1.%2").arg(m_iSourceMain).arg(m_index));
				hostNotify(bncs_string("completed=%1").arg(m_preselect));
				m_preselect = PRESELECT_NONE;

			}
		}
	}
	else if (b->panel() == POPUP_CONFIRM_PARK)
	{
		if (b->id() == "confirm")
		{
			Take("park");

			panelRemove(POPUP_CONFIRM_PARK);
			hostNotify(bncs_string("completed=%1").arg(m_preselect));
			m_preselect = PRESELECT_NONE;
		}
		else if (b->id() == "cancel")
		{
			panelRemove(POPUP_CONFIRM_PARK);
			hostNotify(bncs_string("completed=%1").arg(m_preselect));
			m_preselect = PRESELECT_NONE;
		}
	}
	else if (b->panel() == POPUP_TAKE_SPECIAL)
	{
		if (b->id() == "close")
		{
			panelDestroy(POPUP_TAKE_SPECIAL);
		}
	}

}

// all revertives come here
int dest_combiner::revertiveCallback(revertiveNotify * r)
{
	r->dump("");

	if (r->device() == m_device && r->index() == m_index)
	{
		// szInfo = 0x0053f3fc "index=1,buindex=1,status=1"

		bncs_string sTallyText_1 = "";
		bncs_string sTallyText_2 = "";

		bncs_string sTallyState_1 = "destgrid_tally";
		bncs_string sTallyState_2 = "destgrid_tally";

		bool bErrorTally_1 = false;
		bool bErrorTally_2 = false;

		m_iSourceMain = -1;
		m_iSourceBackup = -1;
		m_iStatus = -1;

		bncs_stringlist slstAutoValue = bncs_stringlist(r->sInfo());
		if (slstAutoValue.count() != 3)
		{
			// Error
			debug("dest_combiner::ERROR::revertiveCallback device:%1 Index:%2 Revertive:%3 Not providing 3 parameters", m_device, m_index, r->sInfo());
		}

		m_iSourceMain = slstAutoValue.getNamedParam("index").toInt();
		m_iSourceBackup = slstAutoValue.getNamedParam("buindex").toInt();
		m_iStatus = slstAutoValue.getNamedParam("status").toInt();

		if (m_iSourceMain < 1)
		{
			bErrorTally_1 = true;
		}
		else
		{
			routerName(m_device, m_source_database, m_iSourceMain, sTallyText_1);
		}

		if (m_iSourceBackup < 1)
		{
			bErrorTally_2 = true;
		}
		else
		{
			routerName(m_device, m_source_database, m_iSourceBackup, sTallyText_2);
		}

		debug("dest_combiner::revertiveCallback device:%1 DB:%2 Source:%3 Name:%4 Status:%5", m_device, m_source_database, m_iSourceMain, sTallyText_1, m_iStatus);
		debug("dest_combiner::revertiveCallback device:%1 DB:%2 Source:%3 Name:%4 Status:%5", m_device, m_source_database, m_iSourceBackup, sTallyText_2, m_iStatus);


		// Status 1 = London Matches backup, 2 it doesn't !
		if (m_iStatus == 1)
		{
			// London matched Backup
		}
		else if (m_iStatus == 2)
		{
			// Error:London does not match backup
			// Set colours to Alarm on 2nd row !
			sTallyState_2 = "destgrid_tally_alarm";
			// This is the only error that does not show the status !
		}
		else
		{
			// Error we should not get this
			// Set colours to Alarm on both rows!
			sTallyState_1 = "destgrid_tally_alarm";
			sTallyState_2 = "destgrid_tally_alarm";
			bErrorTally_1 = true;
			bErrorTally_2 = true;
			
		}


		// Any alarms set ?
		if (bErrorTally_1)
		{
			sTallyText_1 = bncs_string("ERR.%1.%2").arg(m_iSourceMain).arg(m_iStatus);
			sTallyState_1 = "destgrid_tally_alarm";
		}

		if (bErrorTally_2)
		{
			sTallyText_2 = bncs_string("ERR.%1.%2").arg(m_iSourceBackup).arg(m_iStatus);
			sTallyState_2 = "destgrid_tally_alarm";

		}

		// Update Tallys
		if (m_singleLine)
		{
			textPut("text", sTallyText_1.replace('|', ' '), PNL_MAIN, LBL_TALLY_1);
			textPut("text", sTallyText_2.replace('|', ' '), PNL_MAIN, LBL_TALLY_2);
		}
		else
		{
			textPut("text", sTallyText_1, PNL_MAIN, LBL_TALLY_1);
			textPut("text", sTallyText_2, PNL_MAIN, LBL_TALLY_2);
		}


		textPut("statesheet", sTallyState_1, PNL_MAIN, LBL_TALLY_1);
		textPut("statesheet", sTallyState_2, PNL_MAIN, LBL_TALLY_2);

	}
	else if (r->device() == m_deviceLock && r->index() == m_index)
	{
		if (r->sInfo() == "1")
		{
			m_isLocked = true;
		}
		else
		{
			m_isLocked = false;
		}
	}
	return 0;
}

// all database name changes come back here
void dest_combiner::databaseCallback(revertiveNotify * r)
{
	if (r->device() == m_device)
	{
		if (r->database() == DATABASE_SOURCE_NAME && r->index() == m_iSourceMain)
		{
			bncs_string sourceName = r->sInfo();
			textPut("text", sourceName.replace('|', ' '), PNL_MAIN, LBL_TALLY_1);
		}
		else if (r->database() == DATABASE_SOURCE_NAME && r->index() == m_iSourceBackup)
		{
			bncs_string sourceName = r->sInfo();
			textPut("text", sourceName.replace('|', ' '), PNL_MAIN, LBL_TALLY_2);
		}
		else if (r->database() == DATABASE_DEST_NAME && r->index() == m_index)
		{
			m_destName = r->sInfo();
			textPut("text", m_destName, PNL_MAIN, BTN_DEST);
		}
	}

}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dest_combiner::parentCallback(parentNotify *p)
{
 	RemoveFromStartupInit(p->command());
	p->dump("\n parent callback");
	if( p->command() == "instance" && p->value() != m_instance )
	{
		m_instance = p->value();
		getDev(m_instance, &m_device);
		
		//Assume lock is lock
		bncs_string lock = bncs_string("%1_lock").arg(m_instance);

		getDev(lock, &m_deviceLock);

		bncs_config instanceLookup = bncs_config(bncs_string("instances.%1").arg(m_instance));// .arg(m_instance));
		if (instanceLookup.isValid())
		{
			while (instanceLookup.isChildValid())            // are we sitting on a valid entry?
			{
				bncs_string sID = instanceLookup.childAttr("id");

				if (sID == "main_router")
				{
					m_sMainRouterInstance = instanceLookup.childAttr("value");
				}
				else if (sID == "backup_router")
				{
					m_sBackupRouterInstance = instanceLookup.childAttr("value");
				}

				instanceLookup.nextChild();
			}

			getDev(m_sMainRouterInstance, &m_iMainRouterDevice);
			getDev(m_sBackupRouterInstance, &m_iBackupRouterDevice);
		}

		Init(true);
	}

	else if (p->command() == "dest_database")
	{
		m_dest_database = p->value().toInt();

		Init(true);
	}

	else if (p->command() == "source_database")
	{
		m_source_database = p->value().toInt();

		Init(true);
	}

	else if (p->command() == "single_line")
	{
		m_singleLine = p->value() == "true" ? true : false;
		//debug("dest_combiner::assignsingleline: to be %1", m_singleLine);
	}

	else if (p->command() == "index")
	{

		Deselect();

		if (p->value() != "map_mode")
		{
			m_index = p->value().toInt();

			if (m_index > 0)
			{
				int i = 1;
			}
		}
		else
		{
			Enable();
			textPut("text", "", PNL_MAIN, BTN_DEST);
			textPut("text", "", PNL_MAIN, LBL_TALLY_1);
			textPut("text", "", PNL_MAIN, LBL_TALLY_2);
		}

		Init(true);
	}
	else if (p->command() == "layout")
	{
		bncs_string layout = p->value();
		if (p->value().length() == 0)
		{
			layout = "dest";
		}

		if (layout != m_layout)
		{
			m_layout = layout;

			panelDestroy(PNL_MAIN);
			panelShow(PNL_MAIN, bncs_string("%1.bncs_ui").arg(m_layout));
			//textPut("text", bncs_string("layout=%1").arg(layout), PNL_MAIN, "dest_grid");
			Init(true);
		}
	}

	//Runtime
	else if (p->command() == "map_mode")
	{
		if (p->value() == "true")
		{
			if (m_map_mode == false)
			{				
				m_map_mode = true;
				//This is runtime so s
				panelDestroy(PNL_MAIN);
				panelShow(PNL_MAIN, "mapping.bncs_ui");
				m_isEnabled = true;
			}
		}
		else
			m_map_mode = false;
	}

	//Runtime
	else if (p->command() == "deselect")
	{
		if (p->value().length() == 0)
		{
			Deselect();
		}
		else
		{
			if (m_index != p->value())
			{
				Deselect();
			}
		}
	}
	else if (p->command() == "dest" && m_index > 0)
	{
		int intSelect = p->value().toInt();
		m_selected = (intSelect == m_index) ? true : false;


		if (m_showHighlight)
		{
			if (m_selected)
			{
				textPut("statesheet", "dest_selected", PNL_MAIN, BTN_DEST);
			}
			else
			{
				textPut("statesheet", "dest_deselected", PNL_MAIN, BTN_DEST);
			}
		}
	}
	else if (p->command() == "take")
	{
		if (m_selected || m_preselect == PRESELECT_MONITOR)
		{
			if (p->value().endsWith("&special"))
			{
				//takeSpecial(p->value());
			}
			else
			{
				// find the destination
				Take(p->value());
			}
		}
	}

	else if (p->command() == "park")
	{
		m_sPark = p->value();
	}
	else if (p->command() == "selected_on_start")
	{
		m_selectedOnStart = p->value() == "true" ? true : false;
		Init(true);
	}
	else if (p->value() == "show_highlight")
	{
		// Specific value being asked for by a textGet
		return(bncs_string("%1=%2").arg(p->value()).arg(m_showHighlight ? "true" : "false"));
	}
	else if (p->command() == "undo")
	{
		int last = m_iSourceMain;
		Take(m_lastSource);
		m_lastSource = last;
	}
	else if (p->command() == "save")
	{
		if (p->subs() == 3)
		{
			bncs_string instance = p->sub(0);
			int offset = p->sub(1).toInt();
			int page = p->sub(2).toInt();

			int device = 0;
			if (instance.length() != 0 && getDev(instance, &device))
			{
				routerModify(device, DATABASE_MAPPING, offset + page, p->value(), true);
			}
		}
	}

	if (p->command() == "return" )
	{
		if (p->value().startsWith("getIndex"))
		{
			bncs_stringlist sl(p->value(), '.');

			if (sl.count() == 4)
			{
				bncs_string instance = sl[1];
				bncs_string offset = sl[2];
				bncs_string page = sl[3];

				int device = 0;
				if (instance.length() != 0 && getDev(instance, &device))
				{
					bncs_string value;
					routerName(device, DATABASE_MAPPING, offset.toInt() + page.toInt(), value);

					return bncs_string("%1=%2").arg(p->value()).arg(value);
				}
				else
				{
					return bncs_string("%1=").arg(p->value());
				}
			}
		}
		else if (p->command() == "show_highlight")
		{
			if (p->value().upper() == "FALSE")
			{
				m_showHighlight = false;
			}
			else
			{
				m_showHighlight = true;
			}
		}
		else
		{
			bncs_stringlist sl;

			sl << bncs_string("index=").append(m_index);
			sl << bncs_string("dest_database=").append(m_dest_database);
			sl << bncs_string("source_database=").append(m_source_database);
			sl << bncs_string("layout=").append(m_layout);
			sl << bncs_string("show_highlight=%1").arg(m_showHighlight ? "true" : "false");
			sl << bncs_string("selected_on_start=").append(m_selectedOnStart ? "true" : "false");
			sl << bncs_string("preselect=").append(m_preselect);
			sl << bncs_string("single_line=").append(m_singleLine ? "true" : "false");
			sl << bncs_string("park=").append(m_sPark);

			return sl.toString('\n');
		}
	}
	else if (p->command() == "preselect" && m_index > 0)
	{
		if (p->value() == PRESELECT_PARK)
		{
			m_preselect = PRESELECT_PARK;
			
		}
		else if (p->value() == PRESELECT_DETAILS)
		{
			m_preselect = PRESELECT_DETAILS;
		}
		else if (p->value() == PRESELECT_NONE)
		{
			m_preselect = PRESELECT_NONE;
		}
		else if (p->value() == PRESELECT_LOCK)
		{
			m_preselect = PRESELECT_LOCK;
		}
		else if (p->value() == PRESELECT_MONITOR)
		{
			m_preselect = PRESELECT_MONITOR;
		}
		else
		{
			//backstop
			m_preselect = PRESELECT_NONE;
		}


	}
	else if (p->command() == "deselect")
	{
		m_preselect = PRESELECT_NONE;
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "button=released";
		sl << "index.<destName>=<value>";
		sl << "instance=<value>";
		sl << "lock_instance_index=<value>";
		sl << "source_index=<value>";

		return sl.toString('\n');
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;

		sl << "deselect";
		sl << "deselect=<index>";
		sl << "save.instance.offset.page=<mapping>";
		sl << "index=<value>";
		sl << "undo";
		sl << "take=<value>";

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void dest_combiner::timerCallback(int id)
{
	switch( id )
	{
	case TIMER_SETUP:
		Init(true);
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void dest_combiner::Take(bncs_string sSourceRouting)
{

	if (!m_isLocked)
	{
		if (sSourceRouting == "park")
		{
			sSourceRouting = m_sPark;
		}

		// If take is in the format of ss.dd we are getting a source take that is routed to another dest.
		// So split it and route the source to this dest also


		int iSource = 0;
		int iOriginalDest = 0;

		bncs_stringlist slstRoute = bncs_stringlist(sSourceRouting, '.');
		if (slstRoute.count() == 1)
		{
			iSource = slstRoute[0].toInt();
		}
		else if (slstRoute.count() == 2)
		{
			iSource = slstRoute[0].toInt();
			iOriginalDest = slstRoute[1].toInt();
		}
		else
		{
			debug("dest_combiner::ERROR:Take Invalid data: %1", sSourceRouting);
		}


		// szInfo = 0x0053f3fc "index=1,buindex=1,status=1"

		if (m_device > 0 && iSource > 0 && m_index > 0)
		{
			//			bncs_string sData = bncs_string("index=%1,buindex=%2,status=%3").arg(iSource).arg(iSource).arg(1); // 1 is valid, 2 is invalid
			bncs_string sData = bncs_string("index=%1").arg(iSource);

			// We always get revertives from the ipx_combiner auto, and write to the same device/infodriver
			infoWrite(m_device, sData, m_index);
			infoPoll(m_device, m_index, m_index);
		}
	}
}

void dest_combiner::RemoveFromStartupInit(bncs_string name)
{
	if (m_startupInit.count() > 0)
	{
		int i = m_startupInit.find(name);
		if (i >= 0)
			m_startupInit.deleteItem(i);
	}
}

void dest_combiner::Init(bool Reload)
{
	m_selected = false;

	//Don't bother trying to start before we have all our parameters
	if (m_startupInit.count() > 0)
		return;

	if (m_device == 0 || m_index == 0)
	{
		//Error
		textPut("text", "", PNL_MAIN, BTN_DEST);
		textPut("text", "", PNL_MAIN, LBL_TALLY_1);
		textPut("text", "", PNL_MAIN, LBL_TALLY_2);
		return;
	}

	if (Reload)
	{

		infoUnregister(m_device);
		routerUnregister(m_iMainRouterDevice);
		routerUnregister(m_iBackupRouterDevice);

		if (m_deviceLock > 0)
		{
			infoUnregister(m_deviceLock);
		}
	}
	
	routerRegister(m_iMainRouterDevice, m_index, m_index);
	routerRegister(m_iBackupRouterDevice, m_index, m_index, false);

	infoRegister(m_device, m_index, m_index, false);
	infoPoll(m_device, m_index, m_index);

	routerName(m_device, m_dest_database, m_index, m_destName);
	textPut("text", m_destName, PNL_MAIN, BTN_DEST);

	if (m_selectedOnStart)
	{
		m_selected = true;
		textPut("statesheet", "dest_selected", PNL_MAIN, BTN_DEST);
	}

	if (m_deviceLock > 0)
	{
		infoRegister(m_deviceLock, m_index, m_index, false);
		infoPoll(m_deviceLock, m_index, m_index);
	}
}

void dest_combiner::Enable(bool enable)
{
	if (enable && !m_isEnabled)
	{
		controlEnable(PNL_MAIN, BTN_DEST);
		controlEnable(PNL_MAIN, LBL_TALLY_1);
		controlEnable(PNL_MAIN, LBL_TALLY_2);
		m_isEnabled = true;
	}
	else if (!enable && m_isEnabled)
	{
		controlDisable(PNL_MAIN, BTN_DEST);
		controlDisable(PNL_MAIN, LBL_TALLY_1);
		controlDisable(PNL_MAIN, LBL_TALLY_2);
		m_isEnabled = false;
	}
}

void dest_combiner::Deselect()
{
	if (m_selected)
	{
		textPut("statesheet", "dest_deselected", PNL_MAIN, BTN_DEST);
		m_selected = false;
	}
}