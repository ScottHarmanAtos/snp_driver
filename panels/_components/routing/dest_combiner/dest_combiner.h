#ifndef dest_combiner_INCLUDED
#define dest_combiner_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dest_combiner : public bncs_script_helper
{
public:
	dest_combiner( bncs_client_callback * parent, const char* path );
	virtual ~dest_combiner();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	void RemoveFromStartupInit(bncs_string name);
	void Init(bool Reload = false);
	void Take(bncs_string Index);
	void Enable(bool enable = true);
	void Deselect();

	bncs_string m_destName;
	bncs_string m_instance;

	int m_index;
	int m_device;
	int m_deviceLock;
	bncs_string m_sPark;

	bncs_string m_sMainRouterInstance;
	bncs_string m_sBackupRouterInstance;
	int m_iMainRouterDevice;
	int m_iBackupRouterDevice;

	int m_dest_database;
	int m_source_database;
	int m_iSourceMain;
	int m_iSourceBackup;
	int m_iStatus;
	bncs_stringlist m_startupInit;
	bncs_string m_layout;
	bool m_selected;
	bncs_string m_preselect;
	bool m_map_mode;
	bool m_isEnabled;
	int m_lastSource;
	bool m_isLocked;
	bool m_selectedOnStart;
	bool m_singleLine;
	bool m_showHighlight;
};


#endif // dest_combiner_INCLUDED