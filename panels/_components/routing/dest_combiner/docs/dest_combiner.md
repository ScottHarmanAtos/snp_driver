# dest Component

This is a generic dest component, that can work with infowrite or crosspoints.

It can work with the component_grid.

## Commands

### instance
Instance of the device to take router names from

### dest_database
Dest database number

### source_database
Source database number

### index
The dest index to display

### message_type
The type of messages we need to deal with:
* crosspoint
* infowrite

### layout
The layout to display.

The buttons names set in the layout are:
*button
*tally

### take_when_not_selected
When true, will take even if it is not selected.

### selected_on_start
When true will start highlighted.


### RUNTIME Only Commands

### map_mode
Bool, sets whether we are in map mode or not.

### deselect
Deselects the button, an index can be passed as the value, if the index is the same as the dest
it will not deselect

### take
Take the source index passed.

### undo
Undoes the take and puts it back to the previous index.

### save
In Map mode used to save to the database

### return=getIndex
When used with the component grid will return the mapping

## Notifications

### Events
* button=released
* index.<destName>=<value>
* instance=<value>
* lock_instance_index=<value>
* source_index=<value>

### Commands
* deselect
* deselect=<index>
* save.instance.offset.page=<mapping>
* index=<value>
* undo
* take=<value>



## Stylesheets
None