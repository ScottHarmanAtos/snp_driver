#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "tally.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( tally )

#define TIMER_INIT		1
#define TIMEOUT_INIT	100

// constructor - equivalent to ApplCore STARTUP
tally::tally( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( 1, "p1.bncs_ui" );
	m_idatabase=0;
	m_iCurrentSource = 0;
	m_packageTally = false;
}

// destructor - equivalent to ApplCore CLOSEDOWN
tally::~tally()
{
}

// all button pushes and notifications come here
void tally::buttonCallback( buttonNotify *b )
{
}

// all revertives come here
int tally::revertiveCallback( revertiveNotify * r )
{
	if (m_packageTally)
	{
		bncs_stringlist sltIndex = bncs_stringlist().fromString(r->sInfo());
		m_iCurrentSource = sltIndex.getNamedParam("index").toInt();
	}
	else
	{
		m_iCurrentSource = r->info();
	}

	bncs_string tally;
	routerName(m_device, m_idatabase, m_iCurrentSource, tally);

	ShowTally(tally);

	return 0;
}

// all database name changes come back here
void tally::databaseCallback( revertiveNotify * r )
{
	if (r->device() == m_device && r->database() == m_sdatabase)
	{
		if (r->index() == m_iCurrentSource)
		{
			ShowTally(r->sInfo());
		}
	}

}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string tally::parentCallback( parentNotify *p )
{
	p->dump("tally::parentCallback()");
	debug("tally::parentCallback() cmd=%1 value=%2", p->command(), p->value());
	if( p->command() == "panel" )
	{
		m_spanelname=p->value();
		panelDestroy(1);
		panelShow(1,m_spanelname);
		
	}
	else if( p->command() == "instance" )
	{
		m_sinstance = p->value();
		
		int offset; // not needed
		getDev(m_sinstance, &m_device, &offset);
		
		if (!m_device)
			textPut("text","---",1,"tally");
		
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	 else if( p->command() == "database" )
	 {
		 m_sdatabase = p->value();
		 m_idatabase = p->value().toInt();
		 
		 
	 }
	 else if( p->command() == "singleline" )
	 {
		 m_ssingleline = p->value();
		 if(m_ssingleline=="TRUE"|| m_ssingleline=="true"||m_ssingleline=="yes"||m_ssingleline=="1"){
			 m_bsingleline=true;
		 }
		 else (	m_bsingleline=false);
	 }
	 
	 else if( p->command() == "destindex" )
	 {
		 //your specialsource is set!!!!!!!!arh
		 m_sdestindex = p->value();
		 /*	m_iscsindex=m_scsindex.toInt();
		 
		   bncs_string sName;
		   routerName(m_iinstance,0, m_iscsindex, sName);
		   textPut("text",sName,1,"source");
		   //what on earth does it think it is connected to on the other router 
		   //and what is routed to these destinations
		 Register_with_the_SourceRouter(m_iscsindex);*/
		 
		 if(m_sdestindex > 0)
		 {
			 timerStart(TIMER_INIT, TIMEOUT_INIT);
		 }
		 else
		 {
			 textPut("text","-", 1,"tally");
		 }
	 }
	 else if( p->command() == "return" )
	 {
		 bncs_stringlist sl;		// a means of returning the string
		 
		 //	sl= "destindex=" + m_sdestindex;
		 sl= "panel=" + m_spanelname;
		 sl+= "destindex=" + m_sdestindex;
		 sl+= "database=" + m_sdatabase;
		 sl+= "singleline=" + m_ssingleline;
		 return sl.toString( '\n' );	// return our stringlist as newline delimited single string
	 }
	 else if(p->command() == "text")
	 {
		textPut("text",p->value(), 1,"tally");
	 }
	 
	 return "";
}

// timer events come here
void tally::timerCallback( int id )
{
	if(id == TIMER_INIT)
	{
		timerStop(TIMER_INIT);

		bncs_config cfgInstance(bncs_string("instances.%1").arg(m_sinstance));
		bncs_string type = cfgInstance.attr("type");
		if (type.lower().find("package") > -1)
		{
			m_packageTally = true;
		}
		else
		{
			m_packageTally = false;
		}
		int dest = m_sdestindex.toInt();
		if (m_device && dest){
			if (m_packageTally)
			{
				infoRegister(m_device, dest, dest);
				infoPoll(m_device, dest, dest);
			}
			else
			{
				routerRegister(m_device, dest, dest);
				routerPoll(m_device, dest, dest);
			}
		}
	}
}

void tally::ShowTally(bncs_string t)
{

	m_sName = t;
	if (m_bsingleline)
		m_sName.replace('|', ' ');

	// 	debug("tally::revertiveCallback()dest=%1 tally=%2 - this.dest=%3 this.database=%4 this.iinstance=%5 name=%6", 
	// 		r->index(), r->info(), m_sdestindex, m_sdatabase, m_iinstance, sname);

	if (!m_device)
		m_sName = "-";

	textPut("text", m_sName.replace('\'', '`'), 1, "tally");
	hostNotify(bncs_string("tally=%1").arg(m_sName));
	hostNotify(bncs_string("source_index=%1").arg(m_iCurrentSource));
}