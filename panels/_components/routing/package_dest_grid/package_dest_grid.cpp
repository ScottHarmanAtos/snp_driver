#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "package_dest_grid.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define DEST_GRID				"dest_grid"
#define WORKSPACE_DEST_PAGES	"workspace_dest_pages"
#define WORKSPACE_GRID			"workspace_grid"

#define BTN_LOCAL			"local_workspace"
#define LBL_PRESET_SOURCE	"preset_source"
#define	LBL_SOURCE			"source"

#define TAB_ALL			1
#define TAB_WORKSPACE	2
#define TAB_SELECT		3

#define DATABASE_SOURCE_NAME	0	//dev_1001 packager_router_main

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( package_dest_grid )

// constructor - equivalent to ApplCore STARTUP
package_dest_grid::package_dest_grid( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_devicePackagerRouterMain = 0;
	m_presetDest = 0;
	m_currentTab = 0;
	lc = local_cache::getLocalCache();

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );
	controlHide(PNL_MAIN, BTN_LOCAL);

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel

	bncs_string cacheTab;
	lc->getValue("package_dest_grid.tab", cacheTab);
	int tab = cacheTab.toInt();

	//always open ALL tab
	tab = TAB_ALL;

	if (tab > 0)
	{
		select(tab);
	}
	else
	{
		select(TAB_ALL);	//value not set
	}

	initPackager();

	if (m_currentTab == TAB_WORKSPACE)
	{
		debug("package_dest_grid::package_dest_grid() tab=%1 (TAB_WORKSPACE)", m_currentTab);
		lc->getValue("package_dest_grid.workspace", m_workspace);
		initWorkspace();
	}
}

// destructor - equivalent to ApplCore CLOSEDOWN
package_dest_grid::~package_dest_grid()
{
}

// all button pushes and notifications come here
void package_dest_grid::buttonCallback( buttonNotify *b )
{
	debug("package_dest_grid::buttonCallback() id=%1 command=%2 value=%3", b->id(),  b->command(), b->value());
	b->dump("package_dest_grid::buttonCallback()");
	if( b->panel() == PNL_MAIN )
	{
		if (b->id().startsWith("tab"))
		{
			bncs_stringlist sltTabIndex(b->id(), '_');
			int tab =  sltTabIndex[1].toInt();
			select(tab);

			if (tab != TAB_SELECT)
			{
				//store selected tab in cache
				lc->setValue("package_dest_grid.tab", tab);
			}
		}
		else if (b->id() == DEST_GRID)
		{
			if (b->command() == "index")
			{
				m_presetDest = b->value().toInt();
				setDest();
			}
		}
		else if (b->id() == WORKSPACE_DEST_PAGES)
		{
			if (b->command() == "dest")
			{
				m_presetDest = b->value().toInt();
				setDest();
			}
			else if (b->command() == "workspace_name")
			{
				if (m_currentWorkspace == "")
				{
					textPut("text", "NONE", PNL_MAIN, "tab_2");
				}
				else
				{
					bncs_string name = b->value();
					if (name != "?")
					{
						textPut("text", name, PNL_MAIN, "tab_2");
					}
				}
				debug("package_dest_grid::buttonCallback() workspace_name=%1", b->value());
			}
		}
		else if (b->id() == WORKSPACE_GRID)
		{
			if (b->command() == "workspace")
			{
				m_currentWorkspace = b->value();
				lc->setValue("package_dest_grid.workspace", m_currentWorkspace);
				textPut("workspace", m_currentWorkspace, PNL_MAIN, WORKSPACE_DEST_PAGES);
				select(TAB_WORKSPACE);
			}
		}
		else if (b->id() == "select_workspace")
		{
			select(TAB_SELECT);
		}
	}
}

// all revertives come here
int package_dest_grid::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void package_dest_grid::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string package_dest_grid::parentCallback( parentNotify *p )
{
	p->dump("package_dest_grid::parentCallback()");
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			return sl.toString( '\n' );
		}
	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
	}

	else if (p->command() == "tab")
	{	// Persisted value or 'Command' being set here
		select(p->value().toInt());
	}

	else if (p->command() == "take")
	{
		//command()="take" sub(0)=dest value=source
		if (m_currentTab == TAB_ALL)
		{
			//Send take to dest_grid
			textPut(bncs_string("%1.%2").arg(p->command()).arg(p->sub(0)), p->value(), PNL_MAIN, DEST_GRID);
		}
		else if (m_currentTab == TAB_WORKSPACE)
		{
			//Send take to workspace_dest_pages
			textPut(bncs_string("%1.%2").arg(p->command()).arg(p->sub(0)), p->value(), PNL_MAIN, WORKSPACE_DEST_PAGES);
		}
	}
	else if (p->command() == "undo")
	{
		//command()="take" sub(0)=dest value=source
		if (m_currentTab == TAB_ALL)
		{
			//Send undo to dest_grid
			textPut(p->command(), p->value(), PNL_MAIN, DEST_GRID);
		}
		else if (m_currentTab == TAB_WORKSPACE)
		{
			//Send undo to workspace_dest_pages
			textPut(p->command(), p->value(), PNL_MAIN, WORKSPACE_DEST_PAGES);
		}
	}

	else if (p->command() == "index")
	{
		//index=0 will be received when PARK is preselected on take component
		//source grid should deselect - send deselect command to component_grid of source_pacakge components
		if (p->value().toInt() == 0)
		{
			textPut(p->command(), p->value(), PNL_MAIN, DEST_GRID);
		}
	}

	else if (p->command() == "workspace")
	{
		m_workspace = p->value();
		initWorkspace();
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "index=*";		
		sl << "preset_source=*";

		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "dest=[value]";
		sl << "tab=[value]";
		sl << "workspace=[value]";

		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void package_dest_grid::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void package_dest_grid::initPackager()
{
	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager, "router");
	m_instancePackagerRouterMain = m_packagerRouter.instance(1);
	debug("package_dest_grid::initPackager() m_compositePackager [%1] router is %2 - %3",
		m_compositePackager, m_packagerRouter.isValid() ? "valid" : "NOT valid", m_instancePackagerRouterMain);

	//Get main router device
	m_devicePackagerRouterMain = m_packagerRouter.destDev(1);

	textPut("instance", m_instancePackagerRouterMain, PNL_MAIN, DEST_GRID);

	/*
	bncs_config cPackagerMain(bncs_string("instances.%1.packager_router_1").arg(m_instance));
	if (cPackagerMain.isValid())
	{
		m_packagerMainInstance = cPackagerMain.attr("instance");
		getDev(m_packagerMainInstance, &m_devicePackagerRouterMain);
	}
	*/
}


void package_dest_grid::initWorkspace()
{
	debug("package_dest_grid::initWorkspace() m_currentWorkspace=%1 m_workspace=%2", m_currentWorkspace, m_workspace);

	m_currentWorkspace = m_workspace;
	textPut("workspace", m_currentWorkspace, PNL_MAIN, WORKSPACE_DEST_PAGES);
}

void package_dest_grid::select(int tab)
{
	debug("package_dest_grid::select() tab=%1", tab);
	if (tab == TAB_ALL)
	{
		//All
		controlShow(PNL_MAIN, "selected_1");
		controlHide(PNL_MAIN, "selected_2");
		controlHide(PNL_MAIN, "selected_3");

		controlShow(PNL_MAIN, DEST_GRID);
		controlHide(PNL_MAIN, WORKSPACE_DEST_PAGES);
		controlHide(PNL_MAIN, WORKSPACE_GRID);

		controlShow(PNL_MAIN, "select_workspace");
		controlHide(PNL_MAIN, "title_select_workspace");
		controlHide(PNL_MAIN, "tab_1");	//show tab_1: "ALL"
		controlHide(PNL_MAIN, "tab_3");	//show tab_3: "Select|Workspace"
	}
	else if (tab == TAB_WORKSPACE)
	{
		//Local workspace
		controlHide(PNL_MAIN, "selected_1");
		controlShow(PNL_MAIN, "selected_2");
		controlHide(PNL_MAIN, "selected_3");

		controlHide(PNL_MAIN, DEST_GRID);
		controlShow(PNL_MAIN, WORKSPACE_DEST_PAGES);
		controlHide(PNL_MAIN, WORKSPACE_GRID);

		controlHide(PNL_MAIN, "select_workspace");
		controlHide(PNL_MAIN, "title_select_workspace");
		controlShow(PNL_MAIN, "tab_1");	//show tab_1: "ALL"
		controlShow(PNL_MAIN, "tab_3");	//show tab_3: "Select|Workspace"
		//controlEnable(PNL_MAIN, "tab_3");
	}
	else if (tab == TAB_SELECT)
	{
		//Select workspace
		controlHide(PNL_MAIN, "selected_1");
		controlHide(PNL_MAIN, "selected_2");
		controlShow(PNL_MAIN, "selected_3");

		controlHide(PNL_MAIN, DEST_GRID);
		controlHide(PNL_MAIN, WORKSPACE_DEST_PAGES);
		controlShow(PNL_MAIN, WORKSPACE_GRID);

		controlHide(PNL_MAIN, "select_workspace");
		controlShow(PNL_MAIN, "title_select_workspace");
		controlShow(PNL_MAIN, "tab_1");		//show tab_1: "ALL"
		controlShow(PNL_MAIN, "tab_3");		//hide tab_3: "Select|Workspace"
		//controlDisable(PNL_MAIN, "tab_3");
	}

	m_currentTab = tab;
}

void package_dest_grid::setDest()
{
	debug("package_dest_grid::setDest()");
	hostNotify(bncs_string("index=%1").arg(m_presetDest));
}