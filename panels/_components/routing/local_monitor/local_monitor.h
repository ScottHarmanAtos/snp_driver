#ifndef local_monitor_INCLUDED
	#define local_monitor_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class local_monitor : public bncs_script_helper
{
public:
	local_monitor( bncs_client_callback * parent, const char* path );
	virtual ~local_monitor();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	
	bncs_packager m_packagerRouter;
	bncs_packager m_packagerDestStatus;

	bncs_stringlist m_localMonitors;

	int m_localDestIndex;
	int m_selectedSourceIndex;
	int m_selectedDestIndex;

	int m_selectedLocalMon;
	int m_selectedVideoTag;
	int m_localMonVideoHubTag;

	int m_selectedDestCurrentSource;

	int m_devicePackagerRouterMain;
	int m_deviceDestRouter;
	int m_slotDestRouter;

	void init();
	void updateVideoTag(bncs_string levels);
	void showLocalMonitors();
	void setLocalDest();
	void initVideoHubTagPopup();
	void setVideoTag(int videoHubTag);
	void applySelectedDestTags();
};


#endif // local_monitor_INCLUDED