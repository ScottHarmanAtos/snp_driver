
#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "bncs_packager.h"
#include "packager_common.h"
#include "local_monitor.h"

#define PNL_MAIN			1
#define POPUP_SELECT_MON	2

#define POPUP_VIDEO_HUB_TAG	4

#define MON_LOCAL	"mon_local"
#define MON_SOURCE	"mon_source"
#define MON_DEST	"mon_dest"
#define SELECT_MON	"select_mon"
#define EDIT_MON	"edit_mon"
#define VIDEO_TAG	"video_tag"
#define	AUDIO_PRESET_RECALL	"audio_preset_recall"

#define BTN_CLOSE	"close"
#define BTN_SELECT	"select"
#define LBL_SELECT  "selected"
#define LOCAL_MONS	"local_monitors"

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( local_monitor )

// constructor - equivalent to ApplCore STARTUP
local_monitor::local_monitor( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_localDestIndex = 0;
	m_selectedSourceIndex = 0;
	m_selectedDestIndex = 0;
	m_selectedDestCurrentSource = 0;
	m_selectedLocalMon = 0;
	m_selectedVideoTag = 0;
	m_localMonVideoHubTag = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	init();

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel

}

// destructor - equivalent to ApplCore CLOSEDOWN
local_monitor::~local_monitor()
{
}

// all button pushes and notifications come here
void local_monitor::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if (b->id() == MON_SOURCE)
		{
			//debug("local_monitor::buttonCallback() take source %1", m_selectedSourceIndex);
			textPut("take", m_selectedSourceIndex, PNL_MAIN, MON_LOCAL);
		}
		else if (b->id() == MON_DEST)
		{
			if (m_selectedDestCurrentSource > 0)
			{
				textPut("take", m_selectedDestCurrentSource, PNL_MAIN, MON_LOCAL);
			}

			//Set local monitor video hub tag and audio tags to match selected destination
			applySelectedDestTags();
		}
		else if (b->id() == EDIT_MON)
		{
			navigateExecute(bncs_string("%1,%2").arg("packager/dest_package_editor").arg(m_localDestIndex));
		}
		else if (b->id() == SELECT_MON)
		{
			panelPopup(POPUP_SELECT_MON, "popup_select_mon.bncs_ui");
			showLocalMonitors();
		}
		else if (b->id() == VIDEO_TAG)
		{
			panelPopup(POPUP_VIDEO_HUB_TAG, "popup_video_hub_tag.bncs_ui");
			initVideoHubTagPopup();
		}
	}
	else if (b->panel() == POPUP_SELECT_MON)
	{
		b->dump("local_monitor::buttonCallback() POPUP_SELECT_MON ");

		if (b->id() == BTN_CLOSE)
		{
			panelDestroy(POPUP_SELECT_MON);
		}
		else if (b->id() == LOCAL_MONS)
		{
			bncs_string index;
			bncs_string name;

			b->value().split(';', index, name);
			textPut("text", name, POPUP_SELECT_MON, LBL_SELECT);
			m_selectedLocalMon = index.toInt();

			if (m_selectedLocalMon > 0)
			{
				controlEnable(POPUP_SELECT_MON, BTN_SELECT);
			}
			else
			{
				controlDisable(POPUP_SELECT_MON, BTN_SELECT);
			}
		}
		else if (b->id() == BTN_SELECT)
		{
			panelDestroy(POPUP_SELECT_MON);
			m_localDestIndex = m_selectedLocalMon;
			setLocalDest();
		}
	}
	else if (b->panel() == POPUP_VIDEO_HUB_TAG)
	{
		b->dump("local_monitor::buttonCallback - POPUP_VIDEO_HUB_TAG");
		if (b->id() == "lvw_tags")
		{
			if (b->command() == "cell_selection")
			{
				bncs_string selectedName = b->value();
				textPut("text", selectedName, POPUP_VIDEO_HUB_TAG, "selected_name");
				//[lvw_tags: cell_selection.0.0=LDC Main
			}
			else if (b->command() == "tag")
			{
				m_selectedVideoTag = b->value().toInt();
				textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedVideoTag), POPUP_VIDEO_HUB_TAG, "selected_tag");
			}
		}
		else if (b->id() == "select")
		{
			panelDestroy(POPUP_VIDEO_HUB_TAG);
			setVideoTag(m_selectedVideoTag);
		}
		else if (b->id() == "cancel")
		{
			panelDestroy(POPUP_VIDEO_HUB_TAG);
		}
	}
}

// all revertives come here
int local_monitor::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_deviceDestRouter && r->index() == m_slotDestRouter)
	{
		bncs_stringlist sltTally(r->sInfo());
		m_selectedDestCurrentSource = sltTally.getNamedParam("index").toInt();
	}

	return 0;
}

// all database name changes come back here
void local_monitor::databaseCallback( revertiveNotify * r )
{
	r->dump("local_monitor::databaseCallback");
	if (r->device() == m_devicePackagerRouterMain && r->database() == DATABASE_DEST_LEVELS && r->index() == m_localDestIndex)
	{
		updateVideoTag(r->sInfo());
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string local_monitor::parentCallback( parentNotify *p )
{
	p->dump("local_monitor::parentCallback");

	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			
			return sl.toString( '\n' );
		}

		/*
		else if( p->value() == "myParam" )
		{	// Specific value being asked for by a textGet
		return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_myParam ) );
		}
		*/
	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "source")
	{	
		m_selectedSourceIndex = p->value().toInt();
		if (m_selectedSourceIndex > 0 && m_localDestIndex > 0)
		{
			controlEnable(PNL_MAIN, MON_SOURCE);
		}
		else
		{
			controlDisable(PNL_MAIN, MON_SOURCE);
		}
	}
	else if (p->command() == "dest")
	{
		m_selectedDestIndex = p->value().toInt();
		if (m_selectedDestIndex > 0 && m_localDestIndex > 0)
		{
			controlEnable(PNL_MAIN, MON_DEST);

			//Get device for destination
			m_deviceDestRouter = m_packagerRouter.destDev(m_selectedDestIndex);
			m_slotDestRouter = m_packagerRouter.destSlot(m_selectedDestIndex);
			infoRegister(m_deviceDestRouter, m_slotDestRouter, m_slotDestRouter);
			infoPoll(m_deviceDestRouter, m_slotDestRouter, m_slotDestRouter);
		}
		else
		{
			controlDisable(PNL_MAIN, MON_DEST);
			infoUnregister(m_deviceDestRouter);
		}

		//re-register to ensure database changes are received
		infoRegister(m_devicePackagerRouterMain, 1, 1, (m_devicePackagerRouterMain==m_deviceDestRouter));

		m_selectedDestCurrentSource = 0;
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		sl << "source=[value]";
		sl << "dest=[value]";

		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void local_monitor::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void local_monitor::init()
{

	controlDisable(PNL_MAIN, MON_SOURCE);
	controlDisable(PNL_MAIN, MON_DEST);

	// get local monitors
	bncs_string opsPosition = getWorkstationSetting("ops_position");
	debug("local_monitor::init() opsPosition=%1", opsPosition);

	bncs_config cfgLocalMonitors = bncs_config(bncs_string("ops_positions.%1.local_monitors").arg(opsPosition));
	
	m_localMonitors = bncs_stringlist(cfgLocalMonitors.attr("selection"));

	debug("local_monitor::init() local monitors %1", m_localMonitors.toString('-'));

	if (m_localMonitors.count() > 0)
	{
		controlEnable(PNL_MAIN, SELECT_MON);
	}
	else
	{
		controlDisable(PNL_MAIN, SELECT_MON);
	}

	m_localDestIndex = cfgLocalMonitors.attr("primary").toInt();
	setLocalDest();

	//prepare for selected dest tally handling
	bncs_string compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(compositePackager);
	m_packagerDestStatus = bncs_packager(compositePackager, "dest_status");
	debug("dest_package::initDestPackage() dest_status is %1", m_packagerDestStatus.isValid() ? "valid" : "NOT valid");

	//Get video tag
	m_devicePackagerRouterMain = m_packagerRouter.destDev(1);

	//Register for database changes
	infoRegister(m_devicePackagerRouterMain, 1, 1);

	//Get initial video tag
	bncs_string destLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LEVELS, m_localDestIndex, destLevels);
	updateVideoTag(destLevels);
}

void local_monitor::setLocalDest()
{

	//Set index of destination package
	textPut("index", m_localDestIndex, PNL_MAIN, MON_LOCAL);

	//Select destination package
	textPut("dest", m_localDestIndex, PNL_MAIN, MON_LOCAL);

	//Select destination for audio preset recall
	textPut("dest_index", m_localDestIndex, PNL_MAIN, AUDIO_PRESET_RECALL);
	
	if (m_localDestIndex > 0)
	{
		controlEnable(PNL_MAIN, EDIT_MON);
		controlEnable(PNL_MAIN, VIDEO_TAG);
		controlEnable(PNL_MAIN, MON_LOCAL);

		//Get initial video tag
		bncs_string destLevels;
		routerName(m_devicePackagerRouterMain, DATABASE_DEST_LEVELS, m_localDestIndex, destLevels);
		updateVideoTag(destLevels);
	}
	else
	{
		controlDisable(PNL_MAIN, EDIT_MON);
		controlDisable(PNL_MAIN, VIDEO_TAG);
		controlDisable(PNL_MAIN, MON_LOCAL);

		m_localMonVideoHubTag = 0;
		textPut("text", "---", PNL_MAIN, VIDEO_TAG);
	}

}

void local_monitor::updateVideoTag(bncs_string levels)
{
	bncs_stringlist sltDestLevels = bncs_stringlist().fromString(levels);
	bncs_stringlist sltVideoHub(sltDestLevels.getNamedParam("video"), '#');
	m_localMonVideoHubTag = sltVideoHub[1].toInt();

	bncs_string tagValueStatus;
	routerName(m_devicePackagerRouterMain, DATABASE_VIDEO_HUB_TAG, m_localMonVideoHubTag, tagValueStatus);

	bncs_string tagValue;
	bncs_string tagStatus;
	tagValueStatus.split('|', tagValue, tagStatus);

	textPut("text", tagValue, PNL_MAIN, VIDEO_TAG);
}

void local_monitor::showLocalMonitors()
{
	m_selectedLocalMon = 0;
	controlDisable(POPUP_SELECT_MON, BTN_SELECT);

	for (int mon = 0; mon < m_localMonitors.count(); mon++)
	{
		int dest = m_localMonitors[mon].toInt();
		bncs_string name;
		routerName(m_devicePackagerRouterMain, DATABASE_DEST_NAME, dest, name);
		textPut("add", bncs_string("%1;%2;#TAG=%3").arg(dest).arg(name.replace('|', ' ').arg(mon)), POPUP_SELECT_MON, LOCAL_MONS);
	}
}

void local_monitor::initVideoHubTagPopup()
{
	bncs_string name;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_NAME, m_localDestIndex, name);
	textPut("text", bncs_string("Set Video Hub Tag: %1").arg(name.replace('|', ' ')), POPUP_VIDEO_HUB_TAG, "title");

	textPut("add", bncs_string("%1;#TAG=%2").arg("[NONE]").arg(0), POPUP_VIDEO_HUB_TAG, "lvw_tags");
	for (int tag = 1; tag <= 30; tag++)
	{
		bncs_string tagValueStatus;
		routerName(m_devicePackagerRouterMain, DATABASE_VIDEO_HUB_TAG, tag, tagValueStatus);

		bncs_string tagValue;
		bncs_string tagStatus;
		tagValueStatus.split('|', tagValue, tagStatus);

		int status = tagStatus.toInt();
		if (status == 1)
		{
			textPut("add", bncs_string("%1;#TAG=%2").arg(tagValue).arg(tag), POPUP_VIDEO_HUB_TAG, "lvw_tags");
		}
	}

	textPut("selectTag", m_localMonVideoHubTag, POPUP_VIDEO_HUB_TAG, "lvw_tags");
}

void local_monitor::setVideoTag(int videoHubTag)
{
	//Get current dest levels
	bncs_string destLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LEVELS, m_localDestIndex, destLevels);
	bncs_stringlist sltDestLevels = bncs_stringlist().fromString(destLevels);

	int videoField = -1;

	for (int field = 0; field < sltDestLevels.count(); field++)
	{
		if (sltDestLevels[field].startsWith("video="))
		{
			videoField = field;
		}
	}

	if (videoField > -1)
	{
		bncs_stringlist sltVideoHub(sltDestLevels.getNamedParam("video"), '#');
		int videoDest = sltVideoHub[0].toInt();

		bncs_string data = bncs_string("video=%1#%2").arg(videoDest).arg(videoHubTag);
		sltDestLevels[videoField] = data;

		routerModify(m_devicePackagerRouterMain, DATABASE_DEST_LEVELS, m_localDestIndex, sltDestLevels.toString(), false);
	}
}

void local_monitor::applySelectedDestTags()
{
	//Set Video Hub Tag & Audio preset of monitor to match selected DP

	//Get hub tag for selected dest package
	bncs_string destLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LEVELS, m_selectedDestIndex, destLevels);
	bncs_stringlist sltDestLevels = bncs_stringlist().fromString(destLevels);
	bncs_stringlist sltVideoHub(sltDestLevels.getNamedParam("video"), '#');
	int destHubTag = sltVideoHub[1].toInt();

	//Set Video Hub Tag
	setVideoTag(destHubTag);

	//Get audio tags (db 16)
	bncs_string audioLangType;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_selectedDestIndex, audioLangType);

	//Apply to local monitor dest audio tags
	routerModify(m_devicePackagerRouterMain, DATABASE_DEST_LANG_TYPE, m_localDestIndex, audioLangType, false);
}
