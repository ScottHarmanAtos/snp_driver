#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "source_package_audio.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define BTN_DISPLAY_LEVEL	"display_level"
#define LBL_NAME_1			"name_1"
#define LBL_NAME_2			"name_2"
#define	LBL_LANG_TAG		"lang_tag"
#define BTN_EDIT_AP			"edit_ap"

#define DATABASE_LANG_TAG		43
#define DATABASE_AP_NAME		60
#define DATABASE_AP_LONG_NAME	61
#define DATABASE_AP_ID_OWNER	62
#define DATABASE_AP_TAGS		63


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( source_package_audio )

// constructor - equivalent to ApplCore STARTUP
source_package_audio::source_package_audio( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_diffAP = 0;
	m_editAP = 0;
	m_level = 0;
	m_devicePackageRouter = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	init();

	//Register for database changes
	infoRegister(m_devicePackageRouter, 1, 1);

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
source_package_audio::~source_package_audio()
{
}

// all button pushes and notifications come here
void source_package_audio::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if (b->id() == BTN_DISPLAY_LEVEL)
		{
			hostNotify(bncs_string("display_level=%1").arg(m_level));
		}
		else if (b->id() == BTN_EDIT_AP)
		{
			hostNotify(bncs_string("edit_ap=%1").arg(m_editAP));
		}
	}
}

// all revertives come here
int source_package_audio::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void source_package_audio::databaseCallback( revertiveNotify * r )
{
	if (r->database() == DATABASE_AP_TAGS)
	{
		if (r->index() == m_editAP)
		{
			bncs_stringlist sltAPTags(r->sInfo());
			int languageTag = sltAPTags.getNamedParam("language_tag").toInt();

			bncs_string languageNameStatus;
			routerName(m_devicePackageRouter, DATABASE_LANG_TAG, languageTag, languageNameStatus);

			bncs_stringlist sltNameStatus(languageNameStatus, '|');
			bncs_string languageName = sltNameStatus[0];

			if (languageTag > 0)
			{
				textPut("text", languageName, PNL_MAIN, LBL_LANG_TAG);
			}
			else
			{
				textPut("text", "---", PNL_MAIN, LBL_LANG_TAG);
			}
		}
	}
	else if (r->database() == DATABASE_AP_NAME)
	{
		if (r->index() == m_editAP)
		{
			bncs_string name = r->sInfo();
			textPut("text", name.replace('|', ' '), PNL_MAIN, LBL_NAME_1);
		}
	}
	else if (r->database() == DATABASE_AP_LONG_NAME)
	{
		if (r->index() == m_editAP)
		{
			bncs_string name = r->sInfo();
			textPut("text", bncs_string("AP%1: %2").arg(m_editAP).arg(name.replace('|', ' ')), PNL_MAIN, LBL_NAME_2);
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string source_package_audio::parentCallback( parentNotify *p )
{
	p->dump(bncs_string("source_package_audio::parentCallback()[level:%1]").arg(m_level));
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("display_level=%1").arg(m_level);

			return sl.toString( '\n' );
		}

		else if (p->value() == "display_level")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_level));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "display_level")
	{	// Persisted value or 'Command' being set here
		m_level = p->value().toInt();
		textPut("text", bncs_string("AP|%1").arg(m_level), PNL_MAIN, BTN_DISPLAY_LEVEL);
	}
	else if (p->command() == "select_level")
	{	// Persisted value or 'Command' being set here
		int selectLevel = p->value().toInt();
		if (selectLevel > 0)
		{
			if (m_level == selectLevel)
			{
				textPut("statesheet", "enum_selected", PNL_MAIN, BTN_DISPLAY_LEVEL);
				//controlEnable(PNL_MAIN, LBL_LANG_TAG);
			}
			else
			{
				textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
				//controlDisable(PNL_MAIN, LBL_LANG_TAG);
			}
		}
		else
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
			//controlEnable(PNL_MAIN, LBL_LANG_TAG);
		}
	}

	else if (p->command() == "diff_data")
	{	
		updateDiffData(p->value());
	}
	else if (p->command() == "edit_data")
	{	
		updateEditData(p->value());
	}


	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void source_package_audio::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void source_package_audio::init()
{
	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);

	m_instancePackagerRouterMain = m_packagerRouter.instance(1);
	getDev(m_instancePackagerRouterMain, &m_devicePackageRouter);
}

void source_package_audio::updateDiffData(bncs_string levelData)
{

	m_diffAP = levelData.toInt();
	bncs_string apName;
	routerName(m_devicePackageRouter, DATABASE_AP_NAME, m_diffAP, apName);
	bncs_string apLongName;
	routerName(m_devicePackageRouter, DATABASE_AP_LONG_NAME, m_diffAP, apLongName);

	//debug("source_package_audio::updateDiffData() levelData=%1 device=%2 name=%3 longName=%4", levelData, m_devicePackageRouter, apName, apLongName);

	if (m_diffAP > 0)
	{
		textPut("text", apName.replace('|', ' '), PNL_MAIN, LBL_NAME_1);
		textPut("text", bncs_string("AP%1: %2").arg(m_diffAP).arg(apLongName.replace('|', ' ')), PNL_MAIN, LBL_NAME_2);
		controlEnable(PNL_MAIN, BTN_EDIT_AP);
		//controlDisable(PNL_MAIN, BTN_EDIT_AP);
	}
	else
	{
		textPut("text", "---", PNL_MAIN, LBL_NAME_1);
		textPut("text", "", PNL_MAIN, LBL_NAME_2);
		controlDisable(PNL_MAIN, BTN_EDIT_AP);
	}

	//lookup language tag for ap
	bncs_string apTags;
	routerName(m_devicePackageRouter, DATABASE_AP_TAGS, m_diffAP, apTags);

	bncs_stringlist sltAPTags(apTags);
	int languageTag = sltAPTags.getNamedParam("language_tag").toInt();

	bncs_string languageNameStatus;
	routerName(m_devicePackageRouter, DATABASE_LANG_TAG, languageTag, languageNameStatus);

	bncs_stringlist sltNameStatus(languageNameStatus, '|');
	bncs_string languageName = sltNameStatus[0];

	if (languageTag > 0)
	{
		textPut("text", languageName, PNL_MAIN, LBL_LANG_TAG);
	}
	else
	{
		textPut("text", "---", PNL_MAIN, LBL_LANG_TAG);
	}

}

void source_package_audio::updateEditData(bncs_string levelData)
{
	m_editAP = levelData.toInt();
	bncs_string apName;
	routerName(m_devicePackageRouter, DATABASE_AP_NAME, m_editAP, apName);
	bncs_string apLongName;
	routerName(m_devicePackageRouter, DATABASE_AP_LONG_NAME, m_editAP, apLongName);
	if (m_editAP > 0)
	{
		textPut("text", apName.replace('|', ' '), PNL_MAIN, LBL_NAME_1);
		textPut("text", bncs_string("AP%1: %2").arg(m_editAP).arg(apLongName.replace('|', ' ')), PNL_MAIN, LBL_NAME_2);
		controlEnable(PNL_MAIN, BTN_EDIT_AP);
		//controlDisable(PNL_MAIN, BTN_EDIT_AP);
	}
	else
	{
		textPut("text", "---", PNL_MAIN, LBL_NAME_1);
		textPut("text", "", PNL_MAIN, LBL_NAME_2);
		controlDisable(PNL_MAIN, BTN_EDIT_AP);
	}

	//lookup language tag for ap
	bncs_string apTags;
	routerName(m_devicePackageRouter, DATABASE_AP_TAGS, m_editAP, apTags);

	bncs_stringlist sltAPTags(apTags);
	int languageTag = sltAPTags.getNamedParam("language_tag").toInt();

	bncs_string languageNameStatus;
	routerName(m_devicePackageRouter, DATABASE_LANG_TAG, languageTag, languageNameStatus);

	bncs_stringlist sltNameStatus(languageNameStatus, '|');
	bncs_string languageName = sltNameStatus[0];

	if (languageTag > 0)
	{
		textPut("text", languageName, PNL_MAIN, LBL_LANG_TAG);
	}
	else
	{
		textPut("text", "---", PNL_MAIN, LBL_LANG_TAG);
	}

	//TEMP check difference
	if (m_editAP == m_diffAP)
	{
		textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME_1);
		textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME_2);
	}
	else
	{
		textPut("statesheet", "level_dirty", PNL_MAIN, LBL_NAME_1);
		textPut("statesheet", "level_dirty", PNL_MAIN, LBL_NAME_2);
	}

}
