#ifndef audio_package_audio_return_INCLUDED
	#define audio_package_audio_return_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_package_audio_return : public bncs_script_helper
{
public:
	audio_package_audio_return( bncs_client_callback * parent, const char* path );
	virtual ~audio_package_audio_return();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_instanceAudioRouter;
	
	bncs_string m_compositePackager;				//c_packager_nhn or c_packager_ldc
	bncs_string m_instancePackagerRouterMain;		//packager_router_nhn_main or packager_router_main 

	bncs_packager m_packagerRouter;

	int m_level;
	int m_devicePackageRouter;		//dev_1001
	int m_deviceAudioRouter;		//dev_1261

	int m_editIndex;
	int m_diffIndex;

	int	m_editTypeTag;
	int	m_diffTypeTag;

	int m_editLanguageTag;
	int m_diffLanguageTag;

	void init();

	void updateDiffData(bncs_string levelData);
	void updateEditData(bncs_string levelData);
};


#endif // audio_package_audio_return_INCLUDED