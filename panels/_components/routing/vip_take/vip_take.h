#ifndef vip_take_INCLUDED
	#define vip_take_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class vip_take : public bncs_script_helper
{
public:
	vip_take( bncs_client_callback * parent, const char* path );
	virtual ~vip_take();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;

	int m_source;
	int m_currentSource;
	int m_undoSource;
	int m_dest;
	int m_globalDest;
	int m_deviceDest;
	int m_deviceLock;
	int m_deviceLockBase;
	int m_destDeviceOffset;
	int m_deviceSource;

	bool m_showHighlight;
	bool m_selected;
	bool m_locked;
	bool m_undoPossible;
	bool m_parkSelected;

	void checkEnableState(bncs_string context);
	void globaDestSet(bncs_string value);

	void setSource(bncs_string value);

};


#endif // vip_take_INCLUDED