#ifndef source_package_users_INCLUDED
	#define source_package_users_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class source_package_users : public bncs_script_helper
{
public:
	source_package_users( bncs_client_callback * parent, const char* path );
	virtual ~source_package_users();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;

	bncs_string m_instanceBlockPTI;

	bncs_string m_compositePackager;				//c_packager

	bncs_packager m_packagerRouter;
	bncs_packager m_packagerSourcePTI;

	int m_devicePackagerRouterMain;
	int m_devicePackagerSourcePTI;
	int m_sourceIndex;
	int m_slot;

	void init();
	void setSourceIndex();
	void showCurrentUsers(bncs_string userSlotData);
};


#endif // source_package_users_INCLUDED