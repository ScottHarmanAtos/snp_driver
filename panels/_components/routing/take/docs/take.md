# take Component

This is a simple control to show take sources and destinations and provide take and undo events for another component to make routes. It doesn't make any routes itself.
There is "lock" inhibit - which disables the lock button when the destination is locked. Lock is simply a 0 or 1 in a slot directly corresponding to the destination number. When 			targeted with instance and index this control goes off and polls for the lock state.
Undo is only available immediately after pressing the take button and disables when the next index value is set.

![as_used_on_panel](as_used_on_panel.png)

![config](config.png)

## Commands
| Name | Use |
|------|------|
| instance | Instance of the device to take router names from
| dest | The destination index - this is used to put the router dest name in the pre-select label. If sub(0) is set this is the name to display rather then the looked up name<br> NOTE: If the name is passed in this way it is assumed the dest passed is not a normal index, therefore the dest will not be used for the lock. Use lock_instance_index to pass the information for the lock. <br/>e.g. dest.test=1 |
| source | The source index - this is used to put the router source name in the pre-select label. <br> If sub(0) is set this is the name to display rather then the looked up name<br/> If sub(1) is set this is the value displays rather then the index<br/> 	e.g. source.test=1<br/> e.g. source.park.park=1
| lock_instance | The instance of the device to get lock information from |
| lock_instance_index | The lock instance and index in one. instance,index |

## Notifications
| Name | Use |
|------|------|
| take.<dest_index>=<source_index> | when the take button is pressed |
| undo | when the undo button is pressed |

## Stylesheets
| Name | Use |
|------|------|
dest_selected | Shown on the dest pre-select tally when the 						destination is set |
| dest_deselected | When the destination is not set |
| source_selected | Shown on the source pre-select tally when 						the source is set |
| source_deselected | When the source is not set |
| takeBackground | Background to the take button |
| groupbox | background to the control |
