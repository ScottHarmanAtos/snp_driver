#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "take.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( take )

// constructor - equivalent to ApplCore STARTUP
take::take( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_device = 0;
	m_lockDevice = 0;
	m_dest = -1;
	m_source = -1;
	m_undoPossible = false;
	m_state = false;


	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( 1, "p1.bncs_ui" );
	setSize( 1 );

	controlDisable( 1, "take" );
	controlDisable( 1, "undo" );
}

// destructor - equivalent to ApplCore CLOSEDOWN
take::~take()
{ 
}
 
// all button pushes and notifications come here
void take::buttonCallback( buttonNotify *b )
{
	if( b->id() == "take" )
	{
		hostNotify( bncs_string( "take.%1=%2").arg( m_dest ).arg( m_source ) );
		m_undoPossible = true;
		checkEnableState();
	}
	if( b->id() == "undo" )
	{
		hostNotify( bncs_string( "undo=%1").arg( m_dest ) );
		m_undoPossible = false;
		checkEnableState();
	}
}

// all revertives come here
int take::revertiveCallback( revertiveNotify * r )
{
//	debug( "lock device %1 dest %2 %3", m_lockDevice, m_dest, r->sInfo() );

	if (r->index() == m_lockIndex && (r->device() == m_lockDevice))
	{
		m_state = atoi( r->sInfo() );

		checkEnableState();
	}
	return 0;
}
 
// all database name changes come back here
void take::databaseCallback( revertiveNotify * r )
{
} 


// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string take::parentCallback( parentNotify *p )
{
	if( p->command() == "instance" )
	{
		m_instance = p->value();
		getDev( m_instance, &m_device );
	}
	else if( p->command() == "lock_instance" )
	{
		m_lockInstance = p->value();
		getDev( m_lockInstance, &m_lockDevice );
	}
	else if (p->command() == "lock_instance_index")
	{
		//Expecting instance,index
		bncs_stringlist sl(p->value());

		m_lockInstance = sl[0];
		m_lockIndex = sl[1];
		getDev(m_lockInstance, &m_lockDevice);

		m_state = false;
		if (m_lockDevice && m_lockIndex > 0)
		{
			// go off and get the take state
			infoRegister(m_lockDevice, m_lockIndex, m_lockIndex);
			infoPoll(m_lockDevice, m_lockIndex, m_lockIndex);
		}

		checkEnableState();
	}
	else if( p->command() == "dest" )
	{
		m_dest = p->value();

		bncs_string name = "---";
		//A name can be passed in, rather than having the take composite look for the name
		if (p->subs() > 0)
		{
			name = p->sub(0);//The name to use
		}
		else if (p->value() > 0)
		{			
			routerName(m_device, 1, p->value(), name);
			
			if (m_lockDevice && (m_dest.length() > 0 && (m_dest != "0" || m_dest != "-1")))
			{
				m_lockIndex = m_dest;
				// go off and get the take state
				infoRegister(m_lockDevice, m_dest, m_dest);
				infoPoll(m_lockDevice, m_dest, m_dest);
			}
		}

		if( name == "---" )
		{
			textPut( "text=", 1, "dest" );
			textPut( "stylesheet=dest_deselected", 1, "dest" );
		}
		else
		{
			//name.replace( "|", " " );
			textPut( "text", name, 1, "dest" );
			textPut( "stylesheet=dest_selected", 1, "dest" );
		}


		textPut("text", (m_dest.length() > 0 && (m_dest != "0" || m_dest != "-1")) ? bncs_string("Dest|%1").arg(m_dest) : "Dest|-", 1, "preset_dest");

		m_undoPossible = false;
		checkEnableState();

	}
	else if( p->command() == "source" )
	{
		p->dump("take::parentCallback source");
		m_source = p->value();

		int sourceIndex = m_source.toInt();

		bncs_string name = "---";
		//A name can be passed in, rather than having the take composite look for the name
		if (p->subs() > 0)
		{
			name = p->sub(0);//The name to use
		}
		else 
		{
			routerName(m_device, 0, sourceIndex, name);
		}

		if (m_source == "park")
		{
			textPut("text", "Park", 1, "source");
			textPut("stylesheet=source_selected", 1, "source");
		}
		else
		{
			if (name == "---")
			{
				textPut("text=", 1, "source");
				textPut("stylesheet=source_deselected", 1, "source");
			}
			else
			{
				//name.replace( "|", " " );
				textPut("text", name, 1, "source");
				textPut("stylesheet=source_selected", 1, "source");
			}
		}

		if (p->subs() > 1)
			textPut("text", bncs_string("Source|%1").arg(p->sub(1)), 1,"preset_source");
		else
			textPut("text", (sourceIndex>0)?bncs_string("Source|%1").arg(sourceIndex):"Source|-", 1, "preset_source");

		checkEnableState();
	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << "lock_instance=" + m_lockInstance;

		return sl.toString( '\n' );
	}
	else if( p->command() == "_events" )
	{
		bncs_stringlist sl;

		sl << "take";
		sl << "undo";

		return sl.toString( '\n' );
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;

		sl << "dest=<dest index>";
		sl << "source=<source index>";
		sl << "lock_instance=<instance>";
		sl << "lock_instance_index=<instance,index>";

		return sl.toString( '\n' );
	}
	return "";
}

// timer events come here
void take::timerCallback( int id )
{
}


void take::checkEnableState( void )
{
	if( m_source.length() == 0 || m_source == "-1" )
	{
		controlDisable( 1, "take" );
		controlDisable( 1, "undo" );
		return;
	}
	if (m_dest.length() == 0 || m_dest == "0" || m_dest == "-1")
	{
		controlDisable( 1, "take" );
		controlDisable( 1, "undo" );
		return;
	}
	if( m_state )
	{
		controlDisable( 1, "take" );
		controlDisable( 1, "undo" );
		return;
	}

	controlEnable( 1, "take" );
	
	if(	m_undoPossible )
		controlEnable( 1, "undo" );
	else
		controlDisable( 1, "undo" );
}