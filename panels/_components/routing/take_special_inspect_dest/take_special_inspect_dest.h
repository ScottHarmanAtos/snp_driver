#ifndef take_special_inspect_dest_INCLUDED
	#define take_special_inspect_dest_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class take_special_inspect_dest : public bncs_script_helper
{
public:
	take_special_inspect_dest( bncs_client_callback * parent, const char* path );
	virtual ~take_special_inspect_dest();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_sInstance;
	bncs_string m_sWorkspace;
	bncs_string m_sFunction;
	bool m_bDisable;

	int m_iSource;

	void showTakeSpecial();
	void showPopupError();
	void checkEnableButton();
};


#endif // take_special_inspect_dest_INCLUDED