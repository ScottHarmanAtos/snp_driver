#ifndef manage_ifb_reverse_INCLUDED
	#define manage_ifb_reverse_INCLUDED

#include <map>
#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class manage_ifb_reverse : public bncs_script_helper
{
public:
	manage_ifb_reverse( bncs_client_callback * parent, const char* path );
	virtual ~manage_ifb_reverse();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_compositePackager;
	bncs_string m_workspace;
	bncs_string m_instanceColumn;
	bncs_string m_columnFields;

	bncs_packager m_packagerMMStack;
	bncs_packager m_packagerRouter;
	bncs_packager m_packagerDestStatus;
	bncs_string m_destStatusFields;
	bncs_string m_trace;

	map<pair<int, int>, pair<int, int>> m_mapDevSlotLevelAP;
	map<int, int> m_mapQueuePositions;

	int m_devicePackagerRouterMain;
	int m_destIndex;
	int m_deviceDestRouter;
	int m_slotDestRouter;
	int m_deviceDestStatus;
	int m_slotDestStatus;
	int m_deviceColumnKey;
	int m_slotColumnKey;
	int m_sourceIndex;
	int m_audioPackagePage;
	int m_masterLanguage;
	int m_column;
	int m_selectedLanguage;

	int m_masterLanguageLevel;
	int m_masterLanguageQueueDevice;
	int m_masterLanguageQueueSlot;
	bncs_string m_masterLanguageQueueAction;	//ADD=, REMOVE=

	bncs_string m_setColumnKey;
	bncs_string m_setColumnCommand;

	bool m_manageComms;

	void init();
	void initDest();
	void initColumn();
	void initPopupManageComms();
	void initPopupManageIFB();
	void initPopupSelectKeyLanguage();
	void setColumnFunctionLanguage(bncs_string key, bncs_string command, bncs_string value);
	void showAudioPackagePage(int page);
	void updateTally(bncs_string tally);
	void updateTrace(bncs_string status);
	void initSource();
	void updateQueue(revertiveNotify * r);
	void updateColumn();
	void displayDestRevVision();

	bncs_string getLanguageTagName(int tag);
};


#endif // manage_ifb_reverse_INCLUDED