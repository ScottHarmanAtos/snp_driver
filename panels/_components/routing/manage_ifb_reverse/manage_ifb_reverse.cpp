#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "packager_common.h"
#include "bncs_packager.h"
#include "manage_ifb_reverse.h"

#define PNL_MAIN			1
#define POPUP_MANAGE_COMMS	2
#define POPUP_MANAGE_IFB	3
#define POPUP_KEY_FUNCTION	4
#define POPUP_KEY_LANGUAGE	5

#define BTN_COMMS			"comms"
#define BTN_REV_VISION		"rev_vision"
#define BTN_CLOSE			"close"
#define BTN_ADVANCED		"advanced"

#define BTN_QUEUE			"queue"

#define LVW_TAGS			"lvw_tags"
#define	BTN_SELECT			"select"
#define	BTN_CANCEL			"cancel"

#define AP_LEVEL			"ap_level"
#define AP_NAME				"ap_name"
#define AP_LANGUAGE			"ap_language"

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( manage_ifb_reverse )

// constructor - equivalent to ApplCore STARTUP
manage_ifb_reverse::manage_ifb_reverse( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_destIndex = 0;
	m_deviceDestRouter = 0;
	m_slotDestRouter = 0;
	m_deviceDestStatus = 0;
	m_slotDestStatus = 0;
	m_deviceColumnKey = 0;
	m_slotColumnKey = 0;
	m_sourceIndex = 0;
	m_audioPackagePage = 0;
	m_masterLanguage = 0;
	m_masterLanguageLevel = 0;
	m_masterLanguageQueueDevice = 0;
	m_masterLanguageQueueSlot = 0;
	m_column = 0;
	m_selectedLanguage = 0;
	m_manageComms = false;	//true when POPUP_MANAGE_COMMS is active

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	init();

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
manage_ifb_reverse::~manage_ifb_reverse()
{
}

// all button pushes and notifications come here
void manage_ifb_reverse::buttonCallback( buttonNotify *b )
{
	b->dump("manage_ifb_reverse::buttonCallback");
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == BTN_COMMS )
		{
			panelPopup(POPUP_MANAGE_COMMS, "popup_manage_comms.bncs_ui");
			m_manageComms = true;
			initPopupManageComms();
		}
	}
	else if (b->panel() == POPUP_MANAGE_COMMS)
	{
		if (b->id() == BTN_CLOSE)
		{
			panelDestroy(POPUP_MANAGE_COMMS);
			m_manageComms = false;
		}
		else if (b->id() == BTN_ADVANCED)
		{
			panelDestroy(POPUP_MANAGE_COMMS);
			m_manageComms = false;
			panelPopup(POPUP_MANAGE_IFB, "popup_manage_ifb.bncs_ui");
			initPopupManageIFB();
		}
		else if (b->id() == BTN_QUEUE)
		{
			debug("manage_ifb_reverse::buttonCallback action=%1 device=%2 slot=%3", 
				m_masterLanguageQueueAction, m_masterLanguageQueueDevice, m_masterLanguageQueueSlot);
			if (m_masterLanguageQueueDevice > 0 && m_masterLanguageQueueSlot > 0)
			{
				if (m_masterLanguageQueueAction == "ADD")
				{
					infoWrite(m_masterLanguageQueueDevice, bncs_string("ADD=%1").arg(m_destIndex), m_masterLanguageQueueSlot);
				}
				else if (m_masterLanguageQueueAction == "REMOVE")
				{
					infoWrite(m_masterLanguageQueueDevice, bncs_string("REMOVE=%1").arg(m_destIndex), m_masterLanguageQueueSlot);
				}
			}
		}
		else if (b->id() == "key_1" || b->id() == "key_2" || b->id() == "key_3")
		{
			setColumnFunctionLanguage(b->id(), b->command(), "");
		}
		else if (b->command() == "dismiss")
		{
			m_manageComms = false;
		}
	}
	else if (b->panel() == POPUP_MANAGE_IFB)
	{
		if (b->id() == BTN_CLOSE)
		{
			panelDestroy(POPUP_MANAGE_IFB);
		}
		else if (b->id().startsWith("ap-page_"))
		{
			bncs_stringlist controlIndex(b->id(), '_');
			int index = controlIndex[1].toInt();
			showAudioPackagePage(index);
		}
	}
	else if (b->panel() == POPUP_KEY_FUNCTION)
	{
		if (b->id() == BTN_CLOSE)
		{
			panelDestroy(POPUP_KEY_FUNCTION);
		}
		else
		{
			panelDestroy(POPUP_KEY_FUNCTION);
			bncs_stringlist controlIndex(b->id(), '_');
			int index = controlIndex[1].toInt();
			if (index > 0)
			{
				setColumnFunctionLanguage(m_setColumnKey, m_setColumnCommand, b->id());
			}
		}
	}
	else if (b->panel() == POPUP_KEY_LANGUAGE)
	{
		if (b->id() == BTN_SELECT)
		{
			panelDestroy(POPUP_KEY_LANGUAGE);
			if (m_selectedLanguage > 0)
			{
				setColumnFunctionLanguage(m_setColumnKey, m_setColumnCommand, m_selectedLanguage);
			}
		}
		else if (b->id() == LVW_TAGS)
		{
			m_selectedLanguage = b->value().toInt();
			textPut("text", m_selectedLanguage, POPUP_KEY_LANGUAGE, "selected_tag");
			textPut("text", getLanguageTagName(m_selectedLanguage), POPUP_KEY_LANGUAGE, "selected_name");
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_KEY_LANGUAGE);
		}
	}
}

// all revertives come here
int manage_ifb_reverse::revertiveCallback( revertiveNotify * r )
{
	r->dump("manage_ifb_reverse::revertiveCallback()");
	if (r->device() == m_deviceDestRouter && r->index() == m_slotDestRouter)
	{
		updateTally(r->sInfo());
	}	
	else if (r->device() == m_deviceDestStatus && r->index() == m_slotDestStatus)
	{
		updateTrace(r->sInfo());
	}
	else if (r->device() == m_deviceColumnKey && r->index() == m_slotColumnKey)
	{
		//debug("manage_ifb_reverse::revertiveCallback() column=%1", m_column);
		m_columnFields = r->sInfo();
		updateColumn();
	}
	else
	{
		//Other device/slot will be an update from an AP MM Stack
		updateQueue(r);
	}
	return 0;
}

// all database name changes come back here
void manage_ifb_reverse::databaseCallback( revertiveNotify * r )
{
	r->dump("manage_ifb_reverse::databaseCallback()");
	if (r->device() == m_devicePackagerRouterMain && r->database() == DATABASE_SOURCE_AP && r->index() == m_sourceIndex)
	{
		//TODO handle case where audio packages of the current source package are modified
		initSource();
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string manage_ifb_reverse::parentCallback( parentNotify *p )
{
	p->dump("manage_ifb_reverse::parentCallback()");

	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
				
			return sl.toString( '\n' );
		}
	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	//Run time events
	else if (p->command() == "dest_index")
	{
		m_destIndex = p->value().toInt();
		initDest();
		initColumn();
	}

	else if (p->command() == "workspace")
	{
		m_workspace = p->value();
		initColumn();
	}

	else if (p->command() == "master_language")
	{
		m_masterLanguage = p->value().toInt();
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "dest_index=[value]";
		sl << "master_language=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void manage_ifb_reverse::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void manage_ifb_reverse::init()
{
	//prepare for selected dest tally handling
	m_compositePackager = getWorkstationSetting("packager_instance");

	m_packagerMMStack = bncs_packager(m_compositePackager, "mm_stack");
	m_packagerRouter = bncs_packager(m_compositePackager);
	m_packagerDestStatus = bncs_packager(m_compositePackager, "dest_status");

	m_devicePackagerRouterMain = m_packagerRouter.destDev(1);

	//register for database callbacks
	infoRegister(m_devicePackagerRouterMain, 1, 1);
}

void manage_ifb_reverse::initColumn()
{
	debug("manage_ifb_reverse::initColumn() m_destIndex=%1 m_workspace=%2", m_destIndex, m_workspace);

	if (m_destIndex > 0 && m_workspace != "")
	{
		bncs_string site = m_compositePackager.right(3);
		m_instanceColumn = bncs_string("%1_oska_%2").arg(site).arg(m_workspace);

		bncs_config cfgRiedelColumn(bncs_string("riedel_columns.%1.%2").arg(m_workspace).arg(m_destIndex));
		if (cfgRiedelColumn.isValid())
		{
			m_column = cfgRiedelColumn.attr("column").toInt();

			char type;
			bncs_string param = bncs_string("column_%1").arg(m_column, '0', 2);
			getDevSlot(m_instanceColumn, param, &m_deviceColumnKey, &m_slotColumnKey, &type);
			infoRegister(m_deviceColumnKey, m_slotColumnKey, m_slotColumnKey);
			infoPoll(m_deviceColumnKey, m_slotColumnKey, m_slotColumnKey);
		}
		else
		{
			m_column = -1;
		}
	}
}

void manage_ifb_reverse::initSource()
{
	//Get audio packages for the source
	bncs_string strAPLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_AP, m_sourceIndex, strAPLevels);

	//debug("manage_ifb_reverse::initSource() [%1] source=%2 levels=%3", m_destIndex, m_sourceIndex, strAPLevels);

	int devicePackagerMMStack = 0;
	int slotPackagerMMStack = 0;
	m_mapDevSlotLevelAP.clear();
	m_mapQueuePositions.clear();
	textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_COMMS);

	bncs_stringlist sltApLevels(strAPLevels, '|');
	for (int level = 0; level < AP_LEVEL_COUNT; level++)
	{
		m_mapQueuePositions[level] = -1;
		int ap = sltApLevels[level].toInt();
		if (ap > 0)
		{
			//register for audio package
			devicePackagerMMStack = m_packagerMMStack.destDev(ap);
			slotPackagerMMStack = m_packagerMMStack.destSlot(ap);
			//map key: device,slot	value: level,ap
			m_mapDevSlotLevelAP[pair<int, int>(devicePackagerMMStack, slotPackagerMMStack)] = pair<int, int>(level, ap);
			infoRegister(devicePackagerMMStack, slotPackagerMMStack, slotPackagerMMStack, true);
			infoPoll(devicePackagerMMStack, slotPackagerMMStack, slotPackagerMMStack);
		}
	}
}

void manage_ifb_reverse::initDest()
{
	//Get device for destination
	m_deviceDestRouter = m_packagerRouter.destDev(m_destIndex);
	m_slotDestRouter = m_packagerRouter.destSlot(m_destIndex);
	bool add = m_deviceDestRouter == m_devicePackagerRouterMain;
	infoRegister(m_deviceDestRouter, m_slotDestRouter, m_slotDestRouter, add);
	infoPoll(m_deviceDestRouter, m_slotDestRouter, m_slotDestRouter);

	//Register for dest status fields
	m_destStatusFields = "";
	m_trace = "";
	m_deviceDestStatus = m_packagerDestStatus.destDev(m_destIndex);
	m_slotDestStatus = m_packagerDestStatus.destSlot(m_destIndex);
	infoRegister(m_deviceDestStatus, m_slotDestStatus, m_slotDestStatus);
	infoPoll(m_deviceDestStatus, m_slotDestStatus, m_slotDestStatus);

	displayDestRevVision();
}

void manage_ifb_reverse::initPopupManageComms()
{
	//debug("manage_ifb_reverse::initPopupManageComms() device=%1", m_devicePackagerRouterMain);

	bncs_string destName;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_NAME, m_destIndex, destName);

	bncs_string sourceName;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_NAME, m_sourceIndex, sourceName);
	textPut("text", sourceName.replace('|', ' '), POPUP_MANAGE_COMMS, "package_name");

	bncs_string sourceTitle;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_TITLE, m_sourceIndex, sourceTitle);
	textPut("text", sourceTitle.replace('|', ' '), POPUP_MANAGE_COMMS, "package_title");

	bncs_string languageName = "None|[0]";

	bncs_string title = bncs_string(" Manage Comms - %1")
		.arg(destName.replace('|', ' '));

	if (m_masterLanguage > 0)
	{
		languageName = getLanguageTagName(m_masterLanguage);

		title = bncs_string(" Manage Comms - %1 - Master Language: %2 [%3]")
			.arg(destName.replace('|', ' '))
			.arg(languageName)
			.arg(m_masterLanguage);
	}

	textPut("text", title, POPUP_MANAGE_COMMS, "title");

	bncs_string strAPLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_AP, m_sourceIndex, strAPLevels);

	//debug("manage_ifb_reverse::initSource() [%1] source=%2 levels=%3", m_destIndex, m_sourceIndex, strAPLevels);

	textPut("text", "---", POPUP_MANAGE_COMMS, AP_LEVEL);
	textPut("text", "", POPUP_MANAGE_COMMS, AP_NAME);
	textPut("text", "", POPUP_MANAGE_COMMS, AP_LANGUAGE);
	textPut("text", "---", POPUP_MANAGE_COMMS, BTN_QUEUE);
	controlDisable(POPUP_MANAGE_COMMS, BTN_QUEUE);

	m_masterLanguageQueueAction = "";
	m_masterLanguageQueueDevice = 0;
	m_masterLanguageQueueSlot = 0;
	m_masterLanguageLevel = 0;

	if (m_masterLanguage > 0)
	{
		bncs_stringlist sltApLevels(strAPLevels, '|');
		for (int level = 0; level < AP_LEVEL_COUNT; level++)
		{
			int ap = sltApLevels[level].toInt();

			bncs_string apTags;
			routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);

			bncs_stringlist sltApTags(apTags);

			int languageTag = sltApTags.getNamedParam("language_tag").toInt();

			//Check if AP level language is a match for the current master language
			if (languageTag == m_masterLanguage)
			{
				bncs_string apName;
				routerName(m_devicePackagerRouterMain, DATABASE_AP_NAME, ap, apName);
				bncs_string apLongName;
				routerName(m_devicePackagerRouterMain, DATABASE_AP_LONG_NAME, ap, apLongName);

				bncs_string apDetails = bncs_string("%1 [%2]|%3")
					.arg(apName.replace('|', ' '))
					.arg(ap)
					.arg(apLongName.replace('|', ' '));

				textPut("text", bncs_string("AP%1").arg(level + 1), POPUP_MANAGE_COMMS, AP_LEVEL);
				textPut("text", apDetails, POPUP_MANAGE_COMMS, AP_NAME);
				textPut("text", languageName, POPUP_MANAGE_COMMS, AP_LANGUAGE);

				controlEnable(POPUP_MANAGE_COMMS, BTN_QUEUE);

				bool top = false;
				bool queued = false;
				int position = m_mapQueuePositions[level];
				if (position == 0)
				{
					queued = true;
					top = true;
					textPut("statesheet", "enum_selected", POPUP_MANAGE_COMMS, BTN_QUEUE);

					//Show dest name and leave on button
					textPut("text", bncs_string("%1|Leave Queue").arg(destName.replace('|', ' ')), POPUP_MANAGE_COMMS, BTN_QUEUE);
					m_masterLanguageQueueAction = "REMOVE";
				}
				else if (position > 0)
				{
					queued = true;
					textPut("statesheet", "enum_warning", POPUP_MANAGE_COMMS, BTN_QUEUE);
					textPut("text", "Leave Queue", POPUP_MANAGE_COMMS, BTN_QUEUE);
					m_masterLanguageQueueAction = "REMOVE";
				}
				else
				{
					textPut("statesheet", "enum_deselected", POPUP_MANAGE_COMMS, BTN_QUEUE);
					textPut("text", "Join Queue", POPUP_MANAGE_COMMS, BTN_QUEUE);
					m_masterLanguageQueueAction = "ADD";
				}

				for (map<pair<int, int>, pair<int, int>>::iterator it = m_mapDevSlotLevelAP.begin();
					it != m_mapDevSlotLevelAP.end(); it++)
				{
					if (it->second.first == level && it->second.second == ap)
					{
						m_masterLanguageQueueDevice = it->first.first;
						m_masterLanguageQueueSlot = it->first.second;
						m_masterLanguageLevel = level;

						debug("manage_ifb_reverse::initPopupManageComms() master device=%1 slot=%2",
							m_masterLanguageQueueDevice, m_masterLanguageQueueSlot);
					}
				}

				break;
			}
		}

		if (m_masterLanguageLevel == 0)
		{
			textPut("text", "[No AP found for Master Language]", POPUP_MANAGE_COMMS, AP_NAME);
		}
	}
	else
	{
		textPut("text", "[Master Language Not Set]", POPUP_MANAGE_COMMS, AP_NAME);
		textPut("text", "-", POPUP_MANAGE_COMMS, AP_LANGUAGE);
	}

	/*
	<package_comms>
	  <tb_assign id="ldc_pcr_01">
		<virtual_package id="36501" name="PCR 1|OS 01" column="1">
	*/

	//textPut("text", bncs_string("lblRiedelButton [workspace=%1 dest=%2 column=%3 device=%4 slot=%5]")
	//	.arg(m_workspace).arg(m_destIndex).arg(m_column).arg(m_deviceColumnKey).arg(m_slotColumnKey),
	//	POPUP_MANAGE_COMMS, "lblRiedelButton");

	updateColumn();
}

void manage_ifb_reverse::initPopupManageIFB()
{
	bncs_string name;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_NAME, m_destIndex, name);

	textPut("text", bncs_string(" Advanced Options IFB - %1").arg(name.replace('|', ' ')), POPUP_MANAGE_IFB, "title");

	debug("manage_ifb_reverse::initPopupManageIFB() device=%1", m_devicePackagerRouterMain);

	bncs_string sourceName;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_NAME, m_sourceIndex, sourceName);
	textPut("text", sourceName.replace('|', ' '), POPUP_MANAGE_IFB, "package_name");

	bncs_string sourceTitle;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_TITLE, m_sourceIndex, sourceTitle);
	textPut("text", sourceTitle.replace('|', ' '), POPUP_MANAGE_IFB, "package_title");

	bncs_string sourceSPEV;
	routerName(m_devicePackagerRouterMain, DATABASE_SPEV_ID, m_sourceIndex, sourceSPEV);
	textPut("text", sourceSPEV, POPUP_MANAGE_IFB, "spev");


	//Get Audio Package Levels
	bncs_string strAPLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_AP, m_sourceIndex, strAPLevels);

	bncs_stringlist sltApLevels(strAPLevels, '|');
	for (int level = 1; level <= AP_LEVEL_COUNT; level++)
	{
		int ap = sltApLevels[level - 1].toInt();
		//textPut("diff_data", ap, POPUP_MANAGE_IFB, bncs_string("ap_%1").arg(level));
		textPut("ap", ap, POPUP_MANAGE_IFB, bncs_string("ap_%1").arg(level));
		textPut("dest_index", m_destIndex, POPUP_MANAGE_IFB, bncs_string("ap_%1").arg(level));
	}

	showAudioPackagePage(1);

}

void manage_ifb_reverse::updateQueue(revertiveNotify * r)
{
	//check MM Queue map
	auto pair = make_pair(r->device(), r->index());
	auto it = m_mapDevSlotLevelAP.find(pair);

	if (it != m_mapDevSlotLevelAP.end())
	{
		int level = it->second.first;
		int ap = it->second.second;
		bncs_stringlist mmQueue(r->sInfo());

		int position = -1;
		if (mmQueue.count() >= 0)
		{
			position = mmQueue.find(m_destIndex);
		}
		m_mapQueuePositions[level] = position;
		//debug("manage_ifb_reverse::revertiveCallback() [%1] level=%2 ap=%3 queue=%4 position=%5", m_destIndex, level, ap, r->sInfo(), position);
	}

	//Check all levels
	int max = -1;
	bool top = false;
	for (int level = 0; level < AP_LEVEL_COUNT; level++)
	{
		int position = m_mapQueuePositions[level];
		if (position == 0)
		{
			top = true;
		}
		if (position > max)
		{
			max = position;
		}

		bncs_string destName;

		if (level == m_masterLanguageLevel)
		{
			if (position == 0)
			{
				routerName(m_devicePackagerRouterMain, DATABASE_DEST_NAME, m_destIndex, destName);
				textPut("text", bncs_string("%1|Leave Queue").arg(destName.replace('|', ' ')), POPUP_MANAGE_COMMS, BTN_QUEUE);
				textPut("statesheet", "enum_selected", POPUP_MANAGE_COMMS, BTN_QUEUE);
				m_masterLanguageQueueAction = "REMOVE";
			}
			else if (position > 0)
			{
				destName = " ";
				textPut("text", bncs_string("%1|Leave Queue").arg(destName), POPUP_MANAGE_COMMS, BTN_QUEUE);
				textPut("statesheet", "enum_warning", POPUP_MANAGE_COMMS, BTN_QUEUE);
				m_masterLanguageQueueAction = "REMOVE";
			}
			else
			{
				destName = " ";
				textPut("text", bncs_string("%1|Join Queue").arg(destName), POPUP_MANAGE_COMMS, BTN_QUEUE);
				textPut("statesheet", "enum_deselected", POPUP_MANAGE_COMMS, BTN_QUEUE);
				m_masterLanguageQueueAction = "ADD";
			}
		}


		//debug("manage_ifb_reverse::updateQueue() [%1] level=%2 position=%3 max=%4", m_destIndex, level, position, max);
	}

	debug("manage_ifb_reverse::updateQueue() [%1] max=%2 top=%3", m_destIndex, max, top?"true":"false");

	if (top)
	{
		textPut("statesheet", "enum_selected", PNL_MAIN, BTN_COMMS);
	}
	else
	{
		if (max > 0)
		{
			textPut("statesheet", "enum_warning", PNL_MAIN, BTN_COMMS);
		}
		else
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_COMMS);
		}
	}
}

void manage_ifb_reverse::updateTally(bncs_string tally)
{
	bncs_stringlist sltTally(tally);
	//m_sourceIndex = sltTally.getNamedParam("index").toInt();

	//initSource();
}

void manage_ifb_reverse::updateTrace(bncs_string status)
{
	m_destStatusFields = status;

	bncs_stringlist sltDestStatus(m_destStatusFields);
	m_trace = sltDestStatus.getNamedParam("trace");

	debug("manage_ifb_reverse::updateTrace m_trace=%1", m_trace);

	m_sourceIndex = m_trace.toInt();
	initSource();
}

void manage_ifb_reverse::showAudioPackagePage(int page)
{
	if (m_audioPackagePage != page)
	{
		//highlight buttons
		textPut("statesheet", "enum_deselected", POPUP_MANAGE_IFB, bncs_string("ap-page_%1").arg(m_audioPackagePage));
	}
	m_audioPackagePage = page;
	textPut("statesheet", "enum_selected", POPUP_MANAGE_IFB, bncs_string("ap-page_%1").arg(m_audioPackagePage));

	//first hide all
	for (int l = 1; l <= AP_LEVEL_COUNT; l++)
	{

		controlHide(POPUP_MANAGE_IFB, bncs_string("ap_%1").arg(l));
	}

	//show range of 8 for current page
	int levelOffset = (m_audioPackagePage - 1) * 8;
	for (int level = levelOffset + 1; level <= levelOffset + 8; level++)
	{
		controlShow(POPUP_MANAGE_IFB, bncs_string("ap_%1").arg(level));
	}
}

void manage_ifb_reverse::displayDestRevVision()
{
	//video=2987#1,anc1=#1,anc2=#1,anc3=#1,anc4=#1,rev_vision=#1,ifb1=r.p,ifb2=r.p
	bncs_string destLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_DEST_LEVELS, m_destIndex, destLevels);

	bncs_stringlist sltDestLevels(destLevels);


	//Update Reverse Video
	bncs_stringlist sltRevVideoHub(sltDestLevels.getNamedParam("rev_vision"), '#');
	int revVideo = sltRevVideoHub[0].toInt();

	if (revVideo > 0)
	{
		bncs_string name;
		routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_NAME, revVideo, name);

		textPut("text", bncs_string("Rev Vision: %1").arg(name.replace('|', ' ')), PNL_MAIN, BTN_REV_VISION);
	}
	else
	{
		textPut("text", "Rev Vision: -", PNL_MAIN, BTN_REV_VISION);
	}
}

void manage_ifb_reverse::updateColumn()
{
	if (m_manageComms)
	{
		debug("manage_ifb_reverse::updateColumn()");
		//"key_1=cf_1#master,key_2=cf_3#41,key_3=conf_2#56"
		bncs_stringlist keys(m_columnFields);
		for (int key = 1; key <= 3; key++)
		{
			bncs_string id = bncs_string("key_%1").arg(key);
			bncs_string functionLanguage = keys.getNamedParam(id);
			
			bncs_string function;
			bncs_string language;
			functionLanguage.split('#', function, language);
			int languageTag = language.toInt();
			bncs_string languageName = "---";
			bncs_string labelSubtitle = "label=---,subtitle=---";
			if (languageTag > 0)
			{
				languageName = getLanguageTagName(languageTag);

				//Update label and subtitle for each key of the column
				
				//Get Audio Package Levels
				bncs_string strAPLevels;
				routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_AP, m_sourceIndex, strAPLevels);

				bncs_stringlist sltApLevels(strAPLevels, '|');
				for (int level = 1; level <= AP_LEVEL_COUNT; level++)
				{
					int ap = sltApLevels[level - 1].toInt();
					if (ap > 0)
					{
						bncs_string apTags;
						routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);
						bncs_stringlist sltApTags(apTags);
						int apLanguageTag = sltApTags.getNamedParam("language_tag").toInt();

						if (apLanguageTag == languageTag)
						{
							bncs_string functionType;
							bncs_string functionIndex;
							function.split('_', functionType, functionIndex);

							//get the labels and subtitles for the AP
							bncs_string labelSubtitleFields;
							if (functionType == "cf")
							{
								routerName(m_devicePackagerRouterMain, DATABASE_AP_CF_LABELS, ap, labelSubtitleFields);
							}
							else if (functionType == "conf")
							{
								routerName(m_devicePackagerRouterMain, DATABASE_AP_CONF_LABELS, ap, labelSubtitleFields);
							}

							bncs_stringlist fields(labelSubtitleFields);
							bncs_string label = fields.getNamedParam(bncs_string("label_%1").arg(functionIndex));
							bncs_string subtitle = fields.getNamedParam(bncs_string("sub_%1").arg(functionIndex));

							labelSubtitle = bncs_string("label=%1,subtitle=%2").arg(label).arg(subtitle);

							break; //AP Level
						}
					}
				}
			}

			textPut("function_language", bncs_string("%1#%2").arg(function).arg(languageName), POPUP_MANAGE_COMMS, id);
			textPut("label_subtitle", labelSubtitle, POPUP_MANAGE_COMMS, id);
			//debug("manage_ifb_reverse::updateColumn() function_language=%1", functionLanguage);
		}
	}
}

void manage_ifb_reverse::setColumnFunctionLanguage(bncs_string key, bncs_string command, bncs_string value)
{
	bncs_stringlist keys(m_columnFields);
	bool set = false;
	for (int item = 0; item < 3; item++)
	{
		bncs_string field = keys[item];
		bncs_string keyId;
		bncs_string functionLanguage;
		field.split('=', keyId, functionLanguage);
		debug("manage_ifb_reverse::setColumnFunctionLanguage() field=%1 keyId=%2", field, keyId);
		if (keyId == key)
		{
			debug("manage_ifb_reverse::setColumnFunctionLanguage() key=%1 command=%2", keyId, command);

			//textPut("text", bncs_string("%1 - %2").arg(key).arg(command), POPUP_MANAGE_COMMS, "lblRiedelButton");

			bncs_string function;
			bncs_string language;
			functionLanguage.split('#', function, language);

			int languageTag = language.toInt();
			bncs_string languageName = "---";

			if (command == "language")
			{
				if (value == "")
				{
					m_setColumnKey = keyId;
					m_setColumnCommand = command;
					panelPopup(POPUP_KEY_LANGUAGE, "popup_column_key_language.bncs_ui");
					textPut("text", " Select Key Language", POPUP_KEY_LANGUAGE, "title");
					initPopupSelectKeyLanguage();
				}
				else
				{
					set = true;
					languageTag = value.toInt();
				}
			}
			else if (command == "function")
			{
				if (value == "")
				{
					m_setColumnKey = keyId;
					m_setColumnCommand = command;
					panelPopup(POPUP_KEY_FUNCTION, "popup_column_key_function.bncs_ui");
					textPut("text", " Select Key Function", POPUP_KEY_FUNCTION, "title");
				}
				else
				{
					set = true;
					function = value;
				}
			}
			else if (command == "default")
			{
				m_setColumnKey = "";
				m_setColumnCommand = "";
				set = true;
				if (keyId == "key_1")
				{
					function = "cf_1";
				}
				else if (keyId == "key_2")
				{
					function = "cf_2";
				}
				else if (keyId == "key_3")
				{
					function = "conf_1";
				}
				languageTag = m_masterLanguage;
			}
			keys[item] = bncs_string("%1=%2#%3").arg(keyId).arg(function).arg(languageTag);
		}
	}
	
	if (set)
	{
		debug("manage_ifb_reverse::setColumnFunctionLanguage() IW(m_deviceColumnKey=%1, keys.toString()=%2, m_slotColumnKey=%3",
			m_deviceColumnKey, keys.toString(), m_slotColumnKey);
		infoWrite(m_deviceColumnKey, keys.toString(), m_slotColumnKey);
	}
}

void manage_ifb_reverse::initPopupSelectKeyLanguage()
{
	m_selectedLanguage = 0;
	//Get Audio Package Levels
	bncs_string strAPLevels;
	routerName(m_devicePackagerRouterMain, DATABASE_SOURCE_AP, m_sourceIndex, strAPLevels);

	bncs_stringlist sltApLevels(strAPLevels, '|');
	for (int level = 1; level <= AP_LEVEL_COUNT; level++)
	{
		int ap = sltApLevels[level - 1].toInt();
		if (ap > 0)
		{
			bncs_string apTags;
			routerName(m_devicePackagerRouterMain, DATABASE_AP_TAGS, ap, apTags);
			bncs_stringlist sltApTags(apTags);
			int languageTag = sltApTags.getNamedParam("language_tag").toInt();
			
			bncs_string apName;
			routerName(m_devicePackagerRouterMain, DATABASE_AP_NAME, ap, apName);

			if (languageTag > 0)
			{
				bncs_string languageName = getLanguageTagName(languageTag);

				bncs_string line = bncs_string("AP%1 - %2 [%3]; %4;#TAG=%5")
					.arg(level)
					.arg(apName.replace('|', ' '))
					.arg(ap)
					.arg(languageName)
					.arg(languageTag);

				textPut("add", line, POPUP_KEY_LANGUAGE, LVW_TAGS);
			}
		}
	}
}

bncs_string manage_ifb_reverse::getLanguageTagName(int tag)
{
	bncs_string name = "[NONE]";

	if (tag > 0)
	{
		bncs_string languageNameStatus;
		routerName(m_devicePackagerRouterMain, DATABASE_LANG_TAG, tag, languageNameStatus);

		bncs_stringlist sltNameStatus(languageNameStatus, '|');
		name = sltNameStatus[0];
	}
	return name;
}
