#ifndef audio_package_cf_INCLUDED
	#define audio_package_cf_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_package_cf : public bncs_script_helper
{
public:
	audio_package_cf( bncs_client_callback * parent, const char* path );
	virtual ~audio_package_cf();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper *rh;

	bncs_string m_instance;
	bncs_string m_instanceRiedel1;
	bncs_string m_instanceRiedel2;

	bncs_string m_diffLabel;
	bncs_string m_diffSubtitle;
	bncs_string m_editLabel;
	bncs_string m_editSubtitle;

	int m_level;
	//int m_devicePackageRouter;		//dev_1001
	int m_ifbRing;
	int m_ifbNumber;
	int m_confNumber;

	int m_diffInRing;
	int m_diffInPort;
	int m_diffOutRing;
	int m_diffOutPort;
	int m_diffRouting;
	int m_editInRing;
	int m_editInPort;
	int m_editOutRing;
	int m_editOutPort;
	int m_editRouting;

	void updateDiffData(bncs_string levelData);
	void updateEditData(bncs_string levelData);
	void updateAllocationData(bncs_string allocationData);
};


#endif // audio_package_cf_INCLUDED