#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dest_package_ifb.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define BTN_DISPLAY_LEVEL	"display_level"
#define LBL_NAME			"name"

#define DATABASE_SUB_IN		12

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( dest_package_ifb )

// constructor - equivalent to ApplCore STARTUP
dest_package_ifb::dest_package_ifb( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_diffInRing = 0;
	m_diffInPort = 0;
	m_editInRing = 0;
	m_editInPort = 0;
	m_level = 0;

	rh = riedelHelper::getriedelHelperObject();

	getDev(INSTANCE_PACKAGE_ROUTER, &m_devicePackageRouter);
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
dest_package_ifb::~dest_package_ifb()
{
}

// all button pushes and notifications come here
void dest_package_ifb::buttonCallback( buttonNotify *b )
{
	b->dump("dest_package_ifb::buttonCallback");
	if( b->panel() == PNL_MAIN )
	{
		if (b->id() == BTN_DISPLAY_LEVEL)
		{
			hostNotify(bncs_string("display_level=%1").arg(m_level));
			debug("audio_package_cf::buttonCallback() level=%1", m_level);

		}
		switch( b->id() )
		{
			case 1:			textPut( "text", "you pressed|control 1", PNL_MAIN, 4 );		break;
			case 2:			textPut( "text", "you pressed|control 2", PNL_MAIN, 4 );		break;
			case 3:			textPut( "text", "you pressed|control 3", PNL_MAIN, 4 );		break;
		}
	}
}

// all revertives come here
int dest_package_ifb::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void dest_package_ifb::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dest_package_ifb::parentCallback( parentNotify *p )
{
	p->dump("dest_package_ifb::parentCallback");
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("display_level=%1").arg(m_level);

			return sl.toString( '\n' );
		}

		else if( p->value() == "display_level" )
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_level));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "display_level")
	{	// Persisted value or 'Command' being set here
		m_level = p->value().toInt();
		textPut("text", bncs_string("IFB %1").arg(m_level), PNL_MAIN, BTN_DISPLAY_LEVEL);
	}
	else if (p->command() == "select_level")
	{	// Persisted value or 'Command' being set here
		int selectLevel = p->value().toInt();
		if (selectLevel > 0)
		{
			if (m_level == selectLevel)
			{
				textPut("statesheet", "enum_selected", PNL_MAIN, BTN_DISPLAY_LEVEL);
			}
			else
			{
				textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
			}
		}
		else
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
		}
	}
	else if (p->command() == "diff_data")
	{
		updateDiffData(p->value());
	}
	else if (p->command() == "edit_data")
	{
		updateEditData(p->value());
	}


	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void dest_package_ifb::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void dest_package_ifb::updateDiffData(bncs_string levelData)
{
	bncs_stringlist sltFields(levelData);

	if (m_level == 1)
	{
		debug("dest_package_ifb::updateDiffData() level=%1 levelData=%2", m_level, levelData);
	}

	bool updatePort = false;
	//"in_ring=%1,in_port=%2,out_ring=%3,out_port=%4"

	if (levelData.find("in_ring") > -1)
	{
		updatePort = true;
		m_diffInRing = sltFields.getNamedParam("in_ring").toInt();
	}
	
	if (levelData.find("in_port") > -1)
	{
		updatePort = true;
		m_diffInPort = sltFields.getNamedParam("in_port").toInt();
	}
	
	if (updatePort)
	{
		bncs_string nameIn;
		bncs_string subIn;
		comms_ringmaster_port portIn = rh->GetPort(m_diffInRing, m_diffInPort);
		if (portIn.valid == true)
		{
			nameIn = portIn.sourceName_short;
			routerName(portIn.devices.Grd, riedelHelpers::databases::Subtitle_Input, m_diffInPort, subIn);
		}

		bncs_string displayName = "---";
		if (m_diffInRing > 0 && m_diffInPort > 0)
		{
			displayName = bncs_string("%1|%2")
				.arg(nameIn.replace('|', ' ')).arg(subIn);
		}
		textPut("text", displayName, PNL_MAIN, LBL_NAME);
	}
}

void dest_package_ifb::updateEditData(bncs_string levelData)
{
	debug("dest_package_ifb::updateEditData() level=%1 levelData=%2", m_level, levelData);
	bncs_stringlist sltFields(levelData);

	bool updatePort = false;
	//"in_ring=%1,in_port=%2,out_ring=%3,out_port=%4"

	if (levelData.find("in_ring") > -1)
	{
		updatePort = true;
		m_editInRing = sltFields.getNamedParam("in_ring").toInt();
	}

	if (levelData.find("in_port") > -1)
	{
		updatePort = true;
		m_editInPort = sltFields.getNamedParam("in_port").toInt();
	}

	if (updatePort)
	{
		bncs_string nameIn;
		bncs_string subIn;
		comms_ringmaster_port portIn = rh->GetPort(m_editInRing, m_editInPort);
		if (portIn.valid == true)
		{
			nameIn = portIn.sourceName_short;
			routerName(portIn.devices.Grd, riedelHelpers::databases::Subtitle_Input, m_editInPort, subIn);
		}

		bncs_string displayName = "---";

		if (m_editInRing > 0 && m_editInPort > 0)
		{
			displayName = bncs_string("%1|%2")
				.arg(nameIn.replace('|', ' ')).arg(subIn);
		}
		textPut("text", displayName, PNL_MAIN, LBL_NAME);

		if (m_editInRing != m_diffInRing || m_editInPort != m_diffInPort)
		{
			textPut("statesheet", "level_dirty", PNL_MAIN, LBL_NAME);
		}
		else
		{
			textPut("statesheet", "level_clean", PNL_MAIN, LBL_NAME);
		}
	}
}
