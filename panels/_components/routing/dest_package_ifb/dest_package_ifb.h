#ifndef dest_package_ifb_INCLUDED
	#define dest_package_ifb_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dest_package_ifb : public bncs_script_helper
{
public:
	dest_package_ifb( bncs_client_callback * parent, const char* path );
	virtual ~dest_package_ifb();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper *rh;

	bncs_string m_myParam;
	bncs_string m_instance;
	bncs_string m_instanceRiedel1;
	bncs_string m_instanceRiedel2;

	int m_level;
	int m_devicePackageRouter;		//dev_1001

	int m_diffInRing;
	int m_diffInPort;
	int m_editInRing;
	int m_editInPort;

	void updateDiffData(bncs_string levelData);
	void updateEditData(bncs_string levelData);
};


#endif // dest_package_ifb_INCLUDED