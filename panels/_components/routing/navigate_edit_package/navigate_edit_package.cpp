#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "navigate_edit_package.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( navigate_edit_package )

#define PANEL_MAIN	1

// constructor - equivalent to ApplCore STARTUP
navigate_edit_package::navigate_edit_package( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_intIndex = 0;
	m_strLabel = "Edit|Package";
	m_blnEditDest = false;
	m_blnReadonly = false;
	m_strEditor = "packager/source_package_editor";

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );
	controlDisable(PANEL_MAIN, "edit");
	textPut("text", m_strLabel, PANEL_MAIN, "edit");

	init();

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//  setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
navigate_edit_package::~navigate_edit_package()
{
}

// all button pushes and notifications come here
void navigate_edit_package::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == "edit")
		{
			if(m_blnReadonly)
			{
				navigateExecute(bncs_string("%1,%2 Read-only").arg(m_strEditor).arg(m_intIndex));
			}
			else
			{
				navigateExecute(bncs_string("%1,%2").arg(m_strEditor).arg(m_intIndex));
			}
		}
	}
}

// all revertives come here
int navigate_edit_package::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void navigate_edit_package::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string navigate_edit_package::parentCallback( parentNotify *p )
{
	p->dump("navigate_edit_package::parentCallback()");
	if( p->command() == "return" )
	{
		bncs_stringlist sl;
		sl << "editor=" + m_strEditor;
		sl << "label=" + m_strLabel;
		if(m_blnEditDest)
		{
			sl << "source-dest=dest";
		}
		else
		{
			sl << "source-dest=source";
		}
		return sl.toString( '\n' );
	}
	else if( p->command() == "label" )
	{
		m_strLabel = p->value();
		textPut("text", m_strLabel, PANEL_MAIN, "edit");
	}
	else if( p->command() == "editor" )
	{
		m_strEditor = p->value();
	}
	else if( p->command() == "source-dest" )
	{
		if(p->value().upper() == "DEST")
		{
			m_blnEditDest = true;
		}
		else
		{
			m_blnEditDest = false;
		}
	}
	else if( p->command() == "index" )
	{
		m_intIndex = p->value().toInt();
		if(m_intIndex > 0)
		{
			controlEnable(PANEL_MAIN, "edit");
		}
		else
		{
			controlDisable(PANEL_MAIN, "edit");
		}
	}
	return "";
}

// timer events come here
void navigate_edit_package::timerCallback( int id )
{
}

void navigate_edit_package::init()
{
	//read only feature not required
	/*
	if( getWorkstationSetting("ops_area") == "mcr" )
	{
	m_blnReadonly = false;
	}
	else
	{
	m_blnReadonly = true;
	textPut("pixmap", "/images/icon_read_only.png", PANEL_MAIN, "edit");
	}
	*/
}
