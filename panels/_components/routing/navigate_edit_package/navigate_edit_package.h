#ifndef navigate_edit_package_INCLUDED
	#define navigate_edit_package_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class navigate_edit_package : public bncs_script_helper
{
public:
	navigate_edit_package( bncs_client_callback * parent, const char* path );
	virtual ~navigate_edit_package();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	void init();
	bool m_blnEditDest;
	bncs_string m_strLabel;
	bncs_string m_strEditor;
	int m_intIndex;
	bool m_blnReadonly;

};


#endif // navigate_edit_package_INCLUDED