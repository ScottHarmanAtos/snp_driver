#ifndef vip_lock_INCLUDED
	#define vip_lock_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class vip_lock : public bncs_script_helper
{
public:
	vip_lock( bncs_client_callback * parent, const char* path );
	virtual ~vip_lock();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	
	int m_dest;
	int m_globalDest;
	int m_deviceDest;
	int m_destDeviceOffset;
	int m_deviceSource;

	bool m_locked;

	void globaDestSet(bncs_string value);

	void setLock();
};


#endif // vip_lock_INCLUDED