#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "vip_lock.h"

#define PNL_MAIN	1

#define BTN_LOCK	"lock"

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( vip_lock )

// constructor - equivalent to ApplCore STARTUP
vip_lock::vip_lock( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_dest = -1;
	m_globalDest = -1;
	m_deviceDest = 0;
	m_destDeviceOffset = 0;
	m_deviceSource = 0;
	m_locked = false;

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
vip_lock::~vip_lock()
{
}

// all button pushes and notifications come here
void vip_lock::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if ( b->id() == BTN_LOCK)
		{
			setLock();
		}
	}
}

// all revertives come here
int vip_lock::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_deviceDest)
	{
		bncs_stringlist fields = bncs_stringlist(r->sInfo());
		int lock = fields.getNamedParam("lock").toInt();
		m_locked = (lock == 1) ? true : false;

		if (m_locked)
		{
			textPut("statesheet", "dest_locked", PNL_MAIN, BTN_LOCK);
		}
		else
		{
			textPut("statesheet", "dest_unlocked", PNL_MAIN, BTN_LOCK);
		}
	}
	return 0;
}

// all database name changes come back here
void vip_lock::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string vip_lock::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
						
			return sl.toString( '\n' );
		}

		else if( p->value() == "myParam" )
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_myParam ) );
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		getDev(m_instance, &m_deviceSource);
	}
	else if (p->command() == "index")
	{
		globaDestSet(p->value());
	}

	else if( p->command() == "myParam" )
	{	// Persisted value or 'Command' being set here
		m_myParam = p->value();
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void vip_lock::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void vip_lock::globaDestSet(bncs_string value)
{
	if (m_instance.length() > 0)
	{
		if (m_deviceDest)
			infoUnregister(m_deviceDest);
		m_deviceDest = 0;

		if (value.length())
		{
			m_globalDest = value;

			m_destDeviceOffset = (m_globalDest - 1) / 4096;
			m_dest = ((m_globalDest - 1) % 4096) + 1;

			m_deviceDest = m_deviceSource + m_destDeviceOffset;

			infoRegister(m_deviceDest, m_dest, m_dest);
			infoPoll(m_deviceDest, m_dest, m_dest);
		}
		else
		{
			m_deviceDest = 0;
			m_globalDest = 0;

			m_destDeviceOffset = 0;
			m_dest = -1;
		}
	}
}

void vip_lock::setLock()
{
	if (m_deviceDest > 0 && m_dest > 0)
	{
		if (m_locked)
		{
			infoWrite(m_deviceDest, "lock=0", m_dest);
		}
		else
		{
			infoWrite(m_deviceDest, "lock=1", m_dest);
		}
	}
}
