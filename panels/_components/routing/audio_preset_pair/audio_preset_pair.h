#ifndef audio_preset_pair_INCLUDED
	#define audio_preset_pair_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"
#include "bncs_packager.h"


#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_preset_pair : public bncs_script_helper
{
public:
	audio_preset_pair( bncs_client_callback * parent, const char* path );
	virtual ~audio_preset_pair();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	
	bncs_string m_compositePackager;

	bncs_packager m_packagerRouter;


	int m_preset;
	int m_level;
	int m_devicePackageRouter;		//dev_1001

	void init();

	void showPreset();
	void showPresetSub(int sub);
};


#endif // audio_preset_pair_INCLUDED