#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dest.h"

#define PNL_MAIN	1
#define DATABASE_MAPPING 8

#define BTN_DEST "button"
#define LBL_TALLY "tally"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( dest )

// constructor - equivalent to ApplCore STARTUP
dest::dest( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	m_msgType = INFODRIVER;
	m_device = 0;
	m_index = 0;
	m_dest_database = 1;
	m_source_database = 0;

	m_source = 0;
	m_lastSource = 0;

	m_layout = "dest";
	panelShow(PNL_MAIN, bncs_string("%1.bncs_ui").arg(m_layout));

	m_instance = "";

	m_isSelected = false;
	m_takeWhenNotSelected = false;
	m_map_mode = false;
	m_isEnabled = true;
	m_isLocked = false;
	m_selectedOnStart = false;
	m_singleLine = false;

	m_startupInit.clear();
	m_startupInit << "instance";
	m_startupInit << "index";
	m_startupInit << "dest_database";
	m_startupInit << "source_database";	
	m_startupInit << "message_type";
	m_startupInit << "layout";
	m_startupInit << "selected_on_start";
}

// destructor - equivalent to ApplCore CLOSEDOWN
dest::~dest()
{
}

// all button pushes and notifications come here
void dest::buttonCallback( buttonNotify *b )
{
	if (b->panel() == PNL_MAIN)
	{
		m_isSelected = true;
		textPut("statesheet", "dest_selected", PNL_MAIN, BTN_DEST);

		hostNotify("button=released");
		hostNotify(bncs_string("lock_instance_index=%1,%2").arg(bncs_string("%1_lock").arg(m_instance)).arg(m_index));
		hostNotify(bncs_string("instance=%1").arg(m_instance));
		hostNotify("index." + bncs_string("%1").arg(m_destName) + "=" + m_index);
		hostNotify(bncs_string("index_only=").append(m_index));
	}
}

// all revertives come here
int dest::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_device && r->index() == m_index)
	{
		switch (m_msgType)
		{
		case INFODRIVER:
			m_source = r->sInfo();
			break;
		case CROSSPOINT:
			m_source = r->info();
			break;
		}
		bncs_string sourceName = "";
		routerName(m_device, m_source_database, m_source, sourceName);
		debug("dest::revertiveCallback device:%1 DB:%2 Source:%3 Name:%4", m_device, m_source_database, m_source, sourceName);
		//debug("dest::assignsingleline used as :%1", m_singleLine);
		if (m_singleLine)
			textPut("text", sourceName.replace('|', ' '), PNL_MAIN, "tally");
		else
			textPut("text", sourceName, PNL_MAIN, "tally");
	}
	else if (r->device() == m_deviceLock && r->index() == m_index)
	{
		if (r->sInfo() == "1")
		{
			m_isLocked = true;
		}
		else
		{
			m_isLocked = false;
		}
	}
	return 0;
}

// all database name changes come back here
void dest::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dest::parentCallback( parentNotify *p )
{
	RemoveFromStartupInit(p->command());
	if( p->command() == "instance" && p->value() != m_instance )
	{
		m_instance = p->value();
		getDev(m_instance, &m_device);
		
		//Assume lock is lock
		bncs_string lock = bncs_string("%1_lock").arg(m_instance);

		getDev(lock, &m_deviceLock);

		Init(true);
	}

	else if (p->command() == "dest_database")
	{
		m_dest_database = p->value().toInt();

		Init(true);
	}

	else if (p->command() == "source_database")
	{
		m_source_database = p->value().toInt();

		Init(true);
	}

	else if (p->command() == "single_line")
	{
		m_singleLine = p->value() == "true" ? true : false;
		//debug("dest::assignsingleline: to be %1", m_singleLine);
	}

	else if (p->command() == "index")
	{

		Deselect();

		if (p->value() != "map_mode")
		{
			m_index = p->value().toInt();
		}
		else
		{
			Enable();
			textPut("text", "", PNL_MAIN, BTN_DEST);
			textPut("text", "", PNL_MAIN, LBL_TALLY);
		}

		Init(true);
	}

	else if (p->command() == "message_type")
	{
		MSG_TYPE old = m_msgType;
		if (p->value() == "crosspoint")
		{
			m_msgType = CROSSPOINT;
		}
		else
		{
			m_msgType = INFODRIVER;
		}

		if (old != m_msgType)
		{
			switch (old)
			{
			case INFODRIVER:
				infoUnregister(m_device);
				break;
			case CROSSPOINT:
				routerUnregister(m_device);
				break;
			}
		}


		Init(true);
	}
	/* Maybe a little to advanced for this component, would need a message_parsing function too.
	else if (p->command() == "message_format")
	{
		//This is the formatting we will use to set the string if it is an infodriver.

		if (m_msgType == INFODRIVER)
		{
			if (p->value().length() == 0 || p->value().find("%1") == -1)
			{
				m_msgFormat = "%1";
			}
			else
			{
				m_msgFormat = p->value();
			}
		}
		//Used for Masks
		else if (m_msgType == CROSSPOINT)
		{
			if (m_msgFormat == "%1")
				m_msgFormat = "";
			else
				m_msgFormat = p->value();
		}

		Init();
	}
	*/
	else if (p->command() == "layout")
	{
		bncs_string layout = p->value();
		if (p->value().length() == 0)
		{
			layout = "dest";
		}
		if (layout != m_layout)
		{
			panelDestroy(PNL_MAIN);
			panelShow(PNL_MAIN, bncs_string("%1.bncs_ui").arg(layout));
			m_layout = layout;
			Init(true);
		}
	}

	//Runtime
	else if (p->command() == "map_mode")
	{
		if (p->value() == "true")
		{
			if (m_map_mode == false)
			{				
				m_map_mode = true;
				//This is runtime so s
				panelDestroy(PNL_MAIN);
				panelShow(PNL_MAIN, "mapping.bncs_ui");
				m_isEnabled = true;
			}
		}
		else
			m_map_mode = false;
	}

	//Runtime
	else if (p->command() == "deselect")
	{
		if (p->value().length() == 0)
		{
			Deselect();
		}
		else
		{
			if (m_index != p->value())
			{
				Deselect();
			}
		}
	}

	else if (p->command() == "take")
	{
		m_lastSource = p->value().toInt();
		Take(p->value());
	}

	else if (p->command() == "take_when_not_selected")
	{
		m_takeWhenNotSelected = p->value() == "true" ? true : false;
	}

	else if (p->command() == "selected_on_start")
	{
		m_selectedOnStart = p->value() == "true" ? true : false;
		Init(true);
	}

	else if (p->command() == "undo")
	{
		int last = m_source;
		Take(m_lastSource);
		m_lastSource = last;
	}
	else if (p->command() == "save")
	{
		if (p->subs() == 3)
		{
			bncs_string instance = p->sub(0);
			int offset = p->sub(1).toInt();
			int page = p->sub(2).toInt();

			int device = 0;
			if (instance.length() != 0 && getDev(instance, &device))
			{
				routerModify(device, DATABASE_MAPPING, offset + page, p->value(), true);
			}
		}
	}

	if (p->command() == "return" )
	{
		if (p->value().startsWith("getIndex"))
		{
			bncs_stringlist sl(p->value(), '.');

			if (sl.count() == 4)
			{
				bncs_string instance = sl[1];
				bncs_string offset = sl[2];
				bncs_string page = sl[3];

				int device = 0;
				if (instance.length() != 0 && getDev(instance, &device))
				{
					bncs_string value;
					routerName(device, DATABASE_MAPPING, offset.toInt() + page.toInt(), value);

					return bncs_string("%1=%2").arg(p->value()).arg(value);
				}
				else
				{
					return bncs_string("%1=").arg(p->value());
				}
			}
		}
		else
		{
			bncs_stringlist sl;

			sl << bncs_string("index=").append(m_index);
			sl << bncs_string("dest_database=").append(m_dest_database);
			sl << bncs_string("source_database=").append(m_source_database);
			sl << bncs_string("message_type=").append(m_msgType == INFODRIVER ? "infodriver" : "crosspoint");
			sl << bncs_string("layout=").append(m_layout);
			sl << bncs_string("take_when_not_selected=").append(m_takeWhenNotSelected ? "true" : "false");
			sl << bncs_string("selected_on_start=").append(m_selectedOnStart ? "true" : "false");
			sl << bncs_string("single_line=").append(m_singleLine ? "true" : "false");

			return sl.toString('\n');
		}
	}

	else if (p->command() == "_events")
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "button=released";
		sl << "index.<destName>=<value>";
		sl << "instance=<value>";
		sl << "lock_instance_index=<value>";
		sl << "source_index=<value>";

		return sl.toString('\n');
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;

		sl << "deselect";
		sl << "deselect=<index>";
		sl << "save.instance.offset.page=<mapping>";
		sl << "index=<value>";
		sl << "undo";
		sl << "take=<value>";

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void dest::timerCallback( int id )
{
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void dest::Take(bncs_string Source)
{
	if (m_takeWhenNotSelected == true || m_isSelected)
	{
		if (!m_isLocked)
		{
			switch (m_msgType)
			{
				case CROSSPOINT:
				{
					routerCrosspoint(m_device, Source.toInt(), m_index);
				}
				break;

				case INFODRIVER:
				{
					infoWrite(m_device, Source, m_index);					
				}
				break;					
			}
		}
	}
}

void dest::RemoveFromStartupInit(bncs_string name)
{
	if (m_startupInit.count() > 0)
	{
		int i = m_startupInit.find(name);
		if (i >= 0)
			m_startupInit.deleteItem(i);
	}
}

void dest::Init(bool Reload)
{
	m_isSelected = false;

	//Don't bother trying to start before we have all our parameters
	if (m_startupInit.count() > 0)
		return;

	if (m_device == 0 || m_index == 0)
	{
		//Error
		textPut("text", "", PNL_MAIN, "button");
		textPut("text", "", PNL_MAIN, "tally");
		return;
	}

	if (Reload)
	{		
		routerName(m_device, m_dest_database, m_index, m_destName);
		textPut("text", m_destName, PNL_MAIN, "button");

		if (m_selectedOnStart)
		{
			m_isSelected = true;
			textPut("statesheet", "dest_selected", PNL_MAIN, BTN_DEST);
		}
	}
	
	switch (m_msgType)
	{
	case CROSSPOINT:
		routerRegister(m_device, m_index, m_index, false);
		routerPoll(m_device, m_index, m_index);
		break;
	case INFODRIVER:
		infoRegister(m_device, m_index, m_index, false);
		infoPoll(m_device, m_index, m_index);
		break;
	}

	if (m_deviceLock > 0)
	{
		infoRegister(m_deviceLock, m_index, m_index, false);
		infoPoll(m_deviceLock, m_index, m_index);
	}
}

void dest::Enable(bool enable)
{
	if (enable && !m_isEnabled)
	{
		controlEnable(PNL_MAIN, BTN_DEST);
		controlEnable(PNL_MAIN, LBL_TALLY);
		m_isEnabled = true;
	}
	else if (!enable && m_isEnabled)
	{
		controlDisable(PNL_MAIN, BTN_DEST);
		controlDisable(PNL_MAIN, LBL_TALLY);
		m_isEnabled = false;
	}
}

void dest::Deselect()
{
	if (m_isSelected)
	{
		textPut("statesheet", "dest_deselected", PNL_MAIN, BTN_DEST);
		m_isSelected = false;
	}
}