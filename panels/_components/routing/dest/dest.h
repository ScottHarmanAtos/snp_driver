#ifndef dest_INCLUDED
	#define dest_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
enum MSG_TYPE
{
	CROSSPOINT,
	INFODRIVER
};

class dest : public bncs_script_helper
{
public:
	dest( bncs_client_callback * parent, const char* path );
	virtual ~dest();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	void RemoveFromStartupInit(bncs_string name);
	void Init(bool Reload = false);
	void Take(bncs_string Index);
	void Enable(bool enable = true);
	void Deselect();

	bncs_string m_destName;
	bncs_string m_instance;
	MSG_TYPE m_msgType;
	int m_index;
	int m_device;
	int m_deviceLock;
	int m_dest_database;
	int m_source_database;
	int m_source;
	bncs_stringlist m_startupInit;
	bncs_string m_layout;
	bool m_isSelected;
	bool m_takeWhenNotSelected;
	bool m_map_mode;
	bool m_isEnabled;
	int m_lastSource;
	bool m_isLocked;
	bool m_selectedOnStart;
	bool m_singleLine;
};


#endif // dest_INCLUDED