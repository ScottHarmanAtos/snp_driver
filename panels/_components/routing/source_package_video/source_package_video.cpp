#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "source_package_video.h"
#include "instanceLookup.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define DATABASE_SOURCE_NAME	0
#define DATABASE_VIDEO_HUB_TAG	41
#define DATABASE_AP_NAME		60

#define	INSTANCE_PACKAGE_ROUTER	"packager_router_main"

#define BTN_DISPLAY_LEVEL	"display_level"
#define LBL_VIDEO_NAME		"video_name"
#define LBL_AP_NAME			"ap_name"
#define LBL_HUB_TAG			"hub_tag"
#define BTN_EDIT_AP			"edit_ap"
#define BTN_SET_ANC			"anc"


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( source_package_video )

// constructor - equivalent to ApplCore STARTUP
source_package_video::source_package_video( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_level = 0;
	m_deviceVideoHD = 0;
	m_deviceANC = 0;

	m_diffVideo = 0;
	m_editVideo = 0;

	m_diffVideoTag = 0;
	m_editVideoTag = 0;

	m_editANC1 = 0;
	m_diffANC1 = 0;
	m_editANC2 = 0;
	m_diffANC2 = 0;
	m_editANC3 = 0;
	m_diffANC3 = 0;
	m_editANC4 = 0;
	m_diffANC4 = 0;

	m_diffSourceAudio = 0;
	m_editSourceAudio = 0;

	//getDev(INSTANCE_PACKAGE_ROUTER, &m_devicePackageRouter);

	//debug("source_package_video::source_package_video m_instanceVideoHD=%1 m_deviceVideoHD=%2", m_instanceVideoHD, m_deviceVideoHD);

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	init();

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
source_package_video::~source_package_video()
{
}

// all button pushes and notifications come here
void source_package_video::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == BTN_DISPLAY_LEVEL)
		{
			hostNotify(bncs_string("display_level=%1").arg(m_level));
		}
		else if (b->id() == LBL_HUB_TAG)
		{
			hostNotify(bncs_string("hub_tag_level=%1,%2").arg(m_level).arg(m_editVideoTag));
		}
		else if (b->id() == BTN_EDIT_AP)
		{
			hostNotify(bncs_string("edit_ap=%1,%2").arg(m_level).arg(m_editSourceAudio));
		}
		else if (b->id() == BTN_SET_ANC)
		{
			hostNotify(bncs_string("set_anc=%1").arg(m_level));
		}
	}
}

// all revertives come here
int source_package_video::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void source_package_video::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string source_package_video::parentCallback( parentNotify *p )
{
	p->dump("source_package_video::parentCallback");
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "display_level=%1" ).arg( m_level );
			
			return sl.toString( '\n' );
		}

		else if( p->value() == "display_level" )
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_level ) );
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if( p->command() == "display_level" )
	{	// Persisted value or 'Command' being set here
		m_level = p->value().toInt();
		textPut("text", bncs_string("Vid %1").arg(m_level), PNL_MAIN, BTN_DISPLAY_LEVEL);
	}
	else if (p->command() == "select_level")
	{	// Persisted value or 'Command' being set here
		int selectLevel = p->value().toInt();
		if (selectLevel > 0)
		{
			if (m_level == selectLevel)
			{
				textPut("statesheet", "enum_selected", PNL_MAIN, BTN_DISPLAY_LEVEL);
				controlEnable(PNL_MAIN, LBL_HUB_TAG);
				if (m_editSourceAudio > 0)
				{
					controlEnable(PNL_MAIN, BTN_EDIT_AP);
				}
				else
				{
					controlDisable(PNL_MAIN, BTN_EDIT_AP);
				}
			}
			else
			{
				textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
				controlDisable(PNL_MAIN, LBL_HUB_TAG);
				controlDisable(PNL_MAIN, BTN_EDIT_AP);
			}
		}
		else
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, BTN_DISPLAY_LEVEL);
			controlEnable(PNL_MAIN, LBL_HUB_TAG);
			if (m_editSourceAudio > 0)
			{
				controlEnable(PNL_MAIN, BTN_EDIT_AP);
			}
			else
			{
				controlDisable(PNL_MAIN, BTN_EDIT_AP);
			}
		}
	}

	else if (p->command() == "diff_data")
	{	// Persisted value or 'Command' being set here
		updateDiffData(p->value());
	}
	else if (p->command() == "edit_data")
	{	// Persisted value or 'Command' being set here
		updateEditData(p->value());
	}


	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void source_package_video::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void source_package_video::init()
{



	/* packager_config.xml
	<packager_config>
 	  <data id="packager_auto">
	    <setting id="video_router_hd" value="vip_router_video_hd_01" />
	*/

	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);
	m_instanceVideoHD = m_packagerRouter.getConfigItem("video_router_hd");

	//Check if we have been passed a composite instance.
	bncs_string instanceVideoHD_section_01 = "";
	if (instanceLookupComposite(m_instanceVideoHD, "section_01", instanceVideoHD_section_01))
	{
		getDev(instanceVideoHD_section_01, &m_deviceVideoHD);
	}
	else
	{
		debug("source_package_video::init() instanceVideoHD.section_01 NOT FOUND");
	}

	m_instancePackagerRouterMain = m_packagerRouter.instance(1);
	getDev(m_instancePackagerRouterMain, &m_devicePackageRouter);


	//ANC device
	m_instanceANC = m_packagerRouter.getConfigItem("anc_router");

	//Check if we have been passed a composite instance.
	bncs_string instanceANC_section_01 = "";
	if (instanceLookupComposite(m_instanceANC, "section_01", instanceANC_section_01))
	{
		getDev(instanceANC_section_01, &m_deviceANC);
	}
	else
	{
		debug("source_package_video::init() instanceANC.section_01 NOT FOUND");
	}
}

void source_package_video::updateDiffData(bncs_string levelData)
{
	//bncs_string levelData = bncs_string("video=%1,videoTag=%2,ap=%3,apTag=%4")
	//	.arg(vEdit.video).arg(vEdit.videoTag).arg(vEdit.ap).arg(vEdit.apTag);

	bncs_stringlist sltFields(levelData);

	m_diffVideo = sltFields.getNamedParam("video").toInt();
	bncs_string videoName;
	routerName(m_deviceVideoHD, DATABASE_SOURCE_NAME, m_diffVideo, videoName);
	textPut("text", bncs_string("%1").arg(videoName.replace('|', ' ')), PNL_MAIN, LBL_VIDEO_NAME);
	textPut("statesheet", "level_clean", PNL_MAIN, LBL_VIDEO_NAME);

	m_diffANC1 = sltFields.getNamedParam("anc_1").toInt();
	m_diffANC2 = sltFields.getNamedParam("anc_2").toInt();
	m_diffANC3 = sltFields.getNamedParam("anc_3").toInt();
	m_diffANC4 = sltFields.getNamedParam("anc_4").toInt();
	//textPut("text", bncs_string("Set ANC|%1").arg(m_diffANC1), PNL_MAIN, BTN_SET_ANC);
	textPut("statesheet", "", PNL_MAIN, BTN_SET_ANC);

	m_diffSourceAudio = sltFields.getNamedParam("ap").toInt();
	bncs_string apName;
	routerName(m_devicePackageRouter, DATABASE_AP_NAME, m_diffSourceAudio, apName);

	if (m_diffSourceAudio > 0)
	{
		textPut("text", bncs_string("AP%1: %2").arg(m_diffSourceAudio).arg(apName.replace('|', ' ')), PNL_MAIN, LBL_AP_NAME);
	}
	else
	{
		textPut("text", "---", PNL_MAIN, LBL_AP_NAME);
	}

	textPut("statesheet", "source_audio_clean", PNL_MAIN, LBL_AP_NAME);

	m_diffVideoTag = sltFields.getNamedParam("hubTag").toInt();
	bncs_string tagValueStatus;
	routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, m_diffVideoTag, tagValueStatus);
	
	bncs_string tagValue;
	bncs_string tagStatus;
	tagValueStatus.split('|', tagValue, tagStatus);

	textPut("text", tagValue, PNL_MAIN, LBL_HUB_TAG);

}

void source_package_video::updateEditData(bncs_string levelData)
{
	//bncs_string levelData = bncs_string("video=%1,hubTag=%2,ap=%3,apTag=%4")
	//	.arg(vEdit.video).arg(vEdit.videoTag).arg(vEdit.ap).arg(vEdit.apTag);
	
	//debug("source_package_video::updateEditData() find video=%1 ap=%2 hubTag=%3", levelData.find("video"), levelData.find("ap"), levelData.find("hubTag"));
	debug("source_package_video::updateEditData() %1", levelData);

	bncs_stringlist sltFields(levelData);

	bool ancDirty = false;

	if (levelData.find("video") > -1)
	{
		m_editVideo = sltFields.getNamedParam("video").toInt();
		bncs_string videoName;
		routerName(m_deviceVideoHD, DATABASE_SOURCE_NAME, m_editVideo, videoName);
		textPut("text", bncs_string("%1").arg(videoName.replace('|', ' ')), PNL_MAIN, LBL_VIDEO_NAME);

		if (m_editVideo == m_diffVideo)
		{
			textPut("statesheet", "level_clean", PNL_MAIN, LBL_VIDEO_NAME);
		}
		else
		{
			textPut("statesheet", "level_dirty", PNL_MAIN, LBL_VIDEO_NAME);
		}
	}

	if (levelData.find("ap") > -1)
	{
		m_editSourceAudio = sltFields.getNamedParam("ap").toInt();
		bncs_string apName;
		routerName(m_devicePackageRouter, DATABASE_AP_NAME, m_editSourceAudio, apName);

		if (m_editSourceAudio > 0)
		{
			textPut("text", bncs_string("AP%1: %2").arg(m_editSourceAudio).arg(apName.replace('|', ' ')), PNL_MAIN, LBL_AP_NAME);
			controlEnable(PNL_MAIN, BTN_EDIT_AP);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_AP_NAME);
			controlDisable(PNL_MAIN, BTN_EDIT_AP);
		}

		if (m_editSourceAudio == m_diffSourceAudio)
		{
			textPut("statesheet", "source_audio_clean", PNL_MAIN, LBL_AP_NAME);
		}
		else
		{
			textPut("statesheet", "source_audio_dirty", PNL_MAIN, LBL_AP_NAME);
		}
	}

	if (levelData.find("hubTag") > -1)
	{
		m_editVideoTag = sltFields.getNamedParam("hubTag").toInt();

		if (m_editVideoTag > 0)
		{
			bncs_string tagValueStatus;
			routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, m_editVideoTag, tagValueStatus);

			bncs_string tagValue;
			bncs_string tagStatus;
			tagValueStatus.split('|', tagValue, tagStatus);

			textPut("text", tagValue, PNL_MAIN, LBL_HUB_TAG);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, LBL_HUB_TAG);
		}

		if (m_editVideoTag == m_diffVideoTag)
		{
			textPut("statesheet", "tag_clean", PNL_MAIN, LBL_HUB_TAG);		//TODO tag_clean state
		}
		else
		{
			textPut("statesheet", "tag_dirty", PNL_MAIN, LBL_HUB_TAG);
		}
	}


	if (levelData.find("anc_1") > -1)
	{
		m_editANC1 = sltFields.getNamedParam("anc_1").toInt();
	}

	if (levelData.find("anc_2") > -1)
	{
		m_editANC2 = sltFields.getNamedParam("anc_2").toInt();
	}

	if (levelData.find("anc_3") > -1)
	{
		m_editANC3 = sltFields.getNamedParam("anc_3").toInt();
	}

	if (levelData.find("anc_4") > -1)
	{
		m_editANC4 = sltFields.getNamedParam("anc_4").toInt();

	}

	if (m_editANC1 == m_diffANC1 &&
		m_editANC2 == m_diffANC2 &&
		m_editANC3 == m_diffANC3 &&
		m_editANC4 == m_diffANC4)
	{
		textPut("statesheet", "level_clean", PNL_MAIN, BTN_SET_ANC);
	}
	else
	{
		textPut("statesheet", "level_dirty", PNL_MAIN, BTN_SET_ANC);
	}

}
