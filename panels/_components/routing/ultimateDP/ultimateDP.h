#ifndef ultimateDP_INCLUDED
	#define ultimateDP_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class ultimateDP : public bncs_script_helper
{
public:
	ultimateDP( bncs_client_callback * parent, const char* path );
	virtual ~ultimateDP();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_dest_use_group;
	bncs_string m_router_level;
	bncs_string m_instance;
	int m_instance_dev;
	bncs_string m_router_dest;
	bncs_string m_packager_dest;
	bncs_string m_packager_trace;
	bool m_isValid;
	bncs_packager m_packager;
	bncs_packager m_packager_status;
	bncs_packager m_packager_group;

	void init(void);
	void controlPut(bncs_string text, bncs_string fgnd, bncs_string bgnd, bncs_string led, bncs_string pnl, bncs_string ctrl);
};


#endif // ultimateDP_INCLUDED