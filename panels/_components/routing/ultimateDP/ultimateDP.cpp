#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <bncs_packager.h>
#include "ultimateDP.h"

#define PNL_MAIN	1

#define TIMER_SETUP				1
#define TIMER_SETUP_DURATION	1

#define BUTTON	"button"
#define LABEL	"label"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(ultimateDP)

// constructor - equivalent to ApplCore STARTUP
ultimateDP::ultimateDP(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path)
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PNL_MAIN, "main.bncs_ui");

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
	//	setSize( 1024,668 );		// set the size explicitly
	//	setSize( PNL_MAIN );		// set the size to the same as the specified panel

	m_router_dest = -1;
	m_instance_dev = -1;
	init();
}

// destructor - equivalent to ApplCore CLOSEDOWN
ultimateDP::~ultimateDP()
{
}

// all button pushes and notifications come here
void ultimateDP::buttonCallback(buttonNotify *b)
{
	b->dump("ultimateDP::BCB::Dump():");
	if (b->panel() == PNL_MAIN)
	{

	}
}

// all revertives come here
int ultimateDP::revertiveCallback(revertiveNotify * r)
{
	r->dump("ultimateDP::RCB::Dump():");
	if (m_packager_group.isDevice(r->device()))
	{
		bncs_string new_packager_dest = bncs_stringlist(r->sInfo()).getNamedParam(m_router_level);
		debug("ultimateDP::RCB::group revertive, looking for %1=%2", m_router_level, new_packager_dest);

		if (bncs_stringlist(r->sInfo()).find(bncs_string("%1=%2").arg(m_router_level).arg(new_packager_dest)) + 1 && new_packager_dest.firstInt())
		{
			debug("ultimateDP::RCB::group revertive, found dest index %1, polling for status info", new_packager_dest.firstInt());
			infoUnregister(m_packager_status.destDev(m_packager_dest));
			m_packager_dest = new_packager_dest;
			controlPut("#", "#", "#", "#", PNL_MAIN, LABEL);
			controlPut("~", "#", "#", "orange", PNL_MAIN, BUTTON);
			infoRegister(m_packager_status.destDev(m_packager_dest.firstInt()), m_packager_status.destSlot(m_packager_dest.firstInt()), m_packager_status.destSlot(m_packager_dest.firstInt()));
			infoPoll(m_packager_status.destDev(m_packager_dest.firstInt()), m_packager_status.destSlot(m_packager_dest.firstInt()), m_packager_status.destSlot(m_packager_dest.firstInt()));
		}
		else
		{
			debug("ultimateDP::RCB::group revertive, fail packager_dest index %1, unregistering", new_packager_dest.firstInt());
			infoUnregister(m_packager_status.destDev(m_packager_dest));
			m_packager_dest = -1;
			controlPut("~", "", "", "red", PNL_MAIN, BUTTON);
		}
	}

	else if (m_packager_status.isDevice(r->device()))
	{
		m_packager_trace = bncs_stringlist(r->sInfo()).getNamedParam("trace");
		debug("ultimateDP::RCB::status revertive, found trace=%1, resolved to index %2", m_packager_trace, m_packager_trace.firstInt());

		bncs_string name;
		routerName(m_packager.deviceNames(), 0, m_packager_trace.firstInt(), name);
		controlPut("#", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut(name, "", "", "green", PNL_MAIN, BUTTON);
	}



	return 0;
}

// all database name changes come back here
void ultimateDP::databaseCallback(revertiveNotify * r)
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string ultimateDP::parentCallback(parentNotify *p)
{
	p->dump("ultimateDP::PCB::Dump():");
	if (p->command() == "return")
	{
		if (p->value() == "all")
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;

			sl << bncs_string("dest_use_group=%1").arg(m_dest_use_group);
			sl << bncs_string("router_level=%1").arg(m_router_level);

			return sl.toString('\n');
		}

		else if (p->value() == "dest_use_group")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_dest_use_group));
		}

		else if (p->value() == "router_level")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_router_level));
		}

	}
	else if (p->command() == "instance")
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
		getDev(m_instance, &m_instance_dev);
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}

	else if (p->command() == "dest_use_group")
	{	// Persisted value or 'Command' being set here
		m_dest_use_group = p->value();
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}

	else if (p->command() == "router_level")
	{	// Persisted value or 'Command' being set here
		m_router_level = p->value();
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}

	else if (p->command() == "router_dest")
	{	// Persisted value or 'Command' being set here
		m_router_dest = p->value().firstInt();
		init();
		infoUnregister(0);

		if (m_isValid && m_router_dest.firstInt() > 0)	// only go if we are valid and have a sensible destination number
		{
			debug("ultimateDP::PCB::router_dest index %1, polling for packager info", m_router_dest.firstInt());

			if (m_instance_dev > 0)
			{
				bncs_string name;
				routerName(m_instance_dev, 1, m_router_dest.firstInt(), name);
				controlPut(name, "", "", "", PNL_MAIN, LABEL);
			}
			else
			{
				controlPut(bncs_string("d%1").arg(m_router_dest.firstInt()), "", "", "", PNL_MAIN, LABEL);
			}
			controlPut("~", "", "", "red", PNL_MAIN, BUTTON);

			infoRegister(m_packager_group.destDev(m_router_dest), m_packager_group.destSlot(m_router_dest), m_packager_group.destSlot(m_router_dest));
			infoPoll(m_packager_group.destDev(m_router_dest), m_packager_group.destSlot(m_router_dest), m_packager_group.destSlot(m_router_dest));
		}
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if (p->command() == "_events")
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "ultimate_source=*";

		return sl.toString('\n');
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if (p->command() == "_commands")
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;

		sl << "router_dest=[value]";

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void ultimateDP::timerCallback(int id)
{
	switch (id)
	{
	case TIMER_SETUP:
		timerStop(id);
		init();
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void ultimateDP::init(void)
{
	m_packager_dest = -1;
	m_packager_trace = -1;
	m_isValid = false;

	// get our packager stuff
	if (!m_packager.isValid())
	{
		m_packager = bncs_packager(getWorkstationSetting("packager_instance"));
	}
	if (m_packager.isValid())
	{
		controlPut("Packager", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut("?", "", "", "red", PNL_MAIN, BUTTON);
	}
	else
	{
		controlPut("Packager?", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut(m_packager.instance(), "red", "#", "grey", PNL_MAIN, BUTTON);
		return;
	}

	if (!m_packager_status.isValid())
	{
		m_packager_status = bncs_packager(getWorkstationSetting("packager_instance"), "dest_status");
	}
	if (m_packager_status.isValid())
	{
		controlPut("Status", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut("?", "#", "#", "#", PNL_MAIN, BUTTON);
	}
	else
	{
		controlPut("Status?", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut(m_packager_status.instance(), "#", "#", "#", PNL_MAIN, BUTTON);
		return;
	}

	if (!m_packager_group.isValid())
	{
		m_packager_group = bncs_packager(getWorkstationSetting("packager_instance"), m_dest_use_group);
	}
	if (m_packager_group.isValid())
	{
		controlPut("Group", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut("?", "#", "#", "#", PNL_MAIN, BUTTON);
	}
	else
	{
		controlPut("Group?", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut(m_packager_group.instance(), "#", "#", "#", PNL_MAIN, BUTTON);
		return;
	}

	if (m_router_level.length())
	{
		controlPut("Ultimate DP", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut("?", "", "", "darkred", PNL_MAIN, BUTTON);
	}
	else
	{
		controlPut("Level?", "#", "#", "#", PNL_MAIN, LABEL);
		controlPut("?", "#", "#", "#", PNL_MAIN, BUTTON);
		return;
	}

	debug("ultimateDP::init()::Good config, packager %1, packager_group %2, packager_status %3, router_level %4", m_packager.instance(), m_packager_group.instance(), m_packager_status.instance(), m_router_level);
	m_isValid = true;
}

void ultimateDP::controlPut(bncs_string text, bncs_string fgnd, bncs_string bgnd, bncs_string led, bncs_string pnl, bncs_string ctrl)
{
	if (text != "#") textPut("text", text, pnl, ctrl);
	if (fgnd != "#") textPut("colour.text", fgnd, pnl, ctrl);
	if (bgnd != "#") textPut("colour.background", bgnd, pnl, ctrl);
	if (led != "#") textPut("colour.led", led, pnl, ctrl);
}