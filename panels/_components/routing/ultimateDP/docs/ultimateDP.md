# ultimateDP Component

Traces a real router destination to a source package on the packager

## Commands

### instance
Instance of the real destination router device to take router names from

### dest_use_group
The group name in the site packager composite instance that resolves to the destination use instance<br>
This is what the component uses to evaluate what package the destination is in<br> 
So in the following typical config, the entry would be "nevion_dest_use":<br>
```xml
<instance composite="yes" id="c_packager_ldc">
	<group id="router" instance="c_packager_router" />
	<group id="mm_stack" instance="c_packager_mm_stack" />
	<group id="audio_package_returns" instance="c_packager_audio_package" />
	<group id="dest_status" instance="c_packager_dest_status" />
	<group id="source_pti" instance="c_packager_source_pti" />
	<group id="nevion_dest_use" instance="c_packager_nevion_dest" />
	<group id="riedel" instance="c_packager_riedel" />
</instance>
```
### router_level
The level parameter to use when extracting the destination contents<br>
So in the following typical revertive, the entry to extract the video use in the dest package would be "hd":<br>
```
 "hd=21241,audio=19406,anc=,revvis=,audret="
```

### router_dest
Run-time parameter of the destination to use for resolving

## Notifications
None currently but we can notify ultimate package info if required

## Stylesheets
None