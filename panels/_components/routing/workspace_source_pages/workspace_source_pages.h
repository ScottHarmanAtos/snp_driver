#ifndef workspace_source_pages_INCLUDED
	#define workspace_source_pages_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"
#include "local_cache.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class workspace_source_pages : public bncs_script_helper
{
public:
	workspace_source_pages( bncs_client_callback * parent, const char* path );
	virtual ~workspace_source_pages();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
	void init();
	void setWorkspace();
	void setSourcePage(int page);
	void setLayout();

private:
	bncs_string m_myParam;
	bncs_string m_instance;
	bncs_string m_workspaceName;
	bncs_string m_layout;

	bncs_string m_compositePackager;			//c_packager_nhn or c_packager_ldc read from workstation_settings.xml
	bncs_string m_instancePackagerRouterMain;	//packager_router_nhn_main or packager_router_main 

	bncs_packager m_packagerRouter;

	local_cache *lc;

	bncs_string m_selectedWorkspace;
	bncs_string m_sourcePagesTemplate;
	int m_pageButton;
	int m_sourceButton;
	int m_sourceIndex;

};


#endif // workspace_source_pages_INCLUDED