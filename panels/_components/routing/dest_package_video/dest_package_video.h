#ifndef dest_package_video_INCLUDED
	#define dest_package_video_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dest_package_video : public bncs_script_helper
{
public:
	dest_package_video( bncs_client_callback * parent, const char* path );
	virtual ~dest_package_video();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;

	bncs_string m_compositePackager;				//c_packager_nhn or c_packager_ldc
	bncs_string m_instancePackagerRouterMain;		//packager_router_nhn_main or packager_router_main 

	bncs_string m_instanceVideoHD;
	bncs_string m_instanceVideoHD_section_01;

	bncs_packager m_packagerRouter;

	int m_level;
	int m_devicePackageRouter;		//dev_1001 or dev_2001
	int m_deviceVideoHD;			//dev_1201 or dev_2201

	int m_editVideo;
	int m_diffVideo;

	int m_editVideoTag;
	int m_diffVideoTag;

	int m_editANC1;
	int m_diffANC1;

	int m_editANC2;
	int m_diffANC2;

	int m_editANC3;
	int m_diffANC3;

	int m_editANC4;
	int m_diffANC4;

	void init();

	void updateDiffData(bncs_string levelData);
	void updateEditData(bncs_string levelData);
};


#endif // dest_package_video_INCLUDED