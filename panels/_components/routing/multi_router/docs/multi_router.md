# multi_router component

## Description
This panel is for use in an mcr. The purpose is to have access to multiple routers in multiple suites from one panel.

![as_used_on_panel](as_used_on_panel.png)

![config](config.png)

A **Suite** would be something like a playout that is monitored.

The panel shows a list of available suites and the routers that are available in each when a suite is selected.

The configuration is stored in multi_router.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<multi_router>
	<area id="mc_mcr">
		<suite id="playout_5" >
			<router id="Video A" value="/playout_5/router/router_xy_grid_a/router_xy_grid_a.bncs_ui" />
			<router id="Video B" value="/playout_5/router/router_xy_grid_b/router_xy_grid_b.bncs_ui" />
			<router id="Audio" value="/playout_5/router/router_xy_grid_aes/router_xy_grid_aes.bncs_ui" />
		</suite>
	</area>
</multi_router>
```

## Commands
Name | Type      | Use
---- | --------- | ---
area | mandatory | the area to load

## Notifications
Name                     | Use
------------------------ | ---
notification=<something> | some notification  XXXXXXXXXXXXX

## Stylesheets
Name     | Use
-------- | ---
groupbox | background to the control XXXXXXXXXXXXX


## Developer Nodes
The panel has been set to use XML configuration, though the panel can also work with dbs

The panel creates a new ui_host for each router it loads and hides and shows them once loaded. In an attempt to stop redrawing flashing, not sure if its any better though.
