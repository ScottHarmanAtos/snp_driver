#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "source_package_vob.h"

#define PNL_MAIN	1

#define BTN_SOURCE	"source"
#define LBL_TITLE	"title"

#define TIMER_SETUP	1

#define DATABASE_SOURCE_NAME	0	//dev_1001 packager_router_main
#define DATABASE_SOURCE_TITLE	2	//dev_1001 packager_router_main
#define DATABASE_SOURCE_LABELS	4	//dev_1001 packager_router_main
#define DATABASE_SOURCE_LEVELS	6	//dev_1001 packager_router_main
#define DATABASE_MAPPING    	8	//dev_1001 packager_router_main


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( source_package_vob )

// constructor - equivalent to ApplCore STARTUP
source_package_vob::source_package_vob( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_packageIndex = 0;
	m_selected = false;
	m_showHighlight = true;
	m_devicePackagerRouter = 0;	
	m_deviceVideoRouter = 0;
	m_videoIndex = 0;

	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);
	m_firstVirtualPackage = m_packagerRouter.firstVirtual();

	/*
	//Check for discovery config model
	m_firstVirtualPackage = 0;
	bncs_config cFirstVirtual("packager_config.packager_auto.first_virtual_package");
	if (cFirstVirtual.isValid())
	{
		m_firstVirtualPackage = cFirstVirtual.attr("value").toInt();
	}
	*/





	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
source_package_vob::~source_package_vob()
{
}

// all button pushes and notifications come here
void source_package_vob::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		//if (b->id() == BTN_SOURCE && m_packageIndex > 0)
			if (b->id() == BTN_SOURCE)
			{
			hostNotify(bncs_string("index=%1").arg(m_packageIndex));
			hostNotify("button=released");
			//if (m_blnShowHighlight)
			//{
				textPut("statesheet", "source_selected", PNL_MAIN, BTN_SOURCE);
				m_selected = true;
			//}
		}
	}
}

// all revertives come here
int source_package_vob::revertiveCallback( revertiveNotify * r )
{
	//not required

	return 0;
}

// all database name changes come back here
void source_package_vob::databaseCallback( revertiveNotify * r )
{
	//r->dump("source_package::databaseCallback()");
	if (r->device() == m_devicePackagerRouter && r->index() == m_packageIndex)
	{
		if (r->database() == DATABASE_SOURCE_TITLE)
		{
			m_sourceTitle = r->sInfo();
			textPut("text", m_sourceTitle.replace('|', ' '), PNL_MAIN, LBL_TITLE);
		}
		else if (r->database() == DATABASE_SOURCE_NAME)
		{
			m_sourceName = r->sInfo();
			if (m_sourceName == "---")
				m_sourceName = "";
			textPut("text", m_sourceName, PNL_MAIN, BTN_SOURCE);
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string source_package_vob::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "layout=%1" ).arg( m_layout );
			
			return sl.toString( '\n' );
		}

		else if( p->value() == "layout" )
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_layout));
		}

		else if (p->value().startsWith("getIndex"))
		{
			bncs_stringlist sl(p->value(), '.');

			if (sl.count() == 4)
			{
				bncs_string instance = sl[1];
				bncs_string offset = sl[2];
				bncs_string page = sl[3];

				int device = 0;
				if (instance.length() != 0 && getDev(instance, &device))
				{
					bncs_string value;
					routerName(device, DATABASE_MAPPING, offset.toInt() + page.toInt(), value);

					return bncs_string("%1=%2").arg(p->value()).arg(value);
				}
				else
				{
					return bncs_string("%1=").arg(p->value());
				}
			}
		}


	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		getDev(m_instance, &m_devicePackagerRouter);

		//register for database changes
		infoRegister(m_devicePackagerRouter, 1, 1);

	}

	else if( p->command() == "layout" )
	{	// Persisted value or 'Command' being set here
		m_layout = p->value();
	}

	else if (p->command() == "index")
	{	// Persisted value or 'Command' being set here
		debug("source_package::parentCallback() command=%1 value=%2", p->command(), p->value());
		
		m_packageIndex = p->value().toInt();
		showSourceName();
		showSourceTitle();

		if (m_showHighlight)
		{
			textPut("statesheet", "source_deselected", PNL_MAIN, BTN_SOURCE);
			m_selected = false;
		}

		//Set icon if virtual
		if (m_packageIndex >= m_firstVirtualPackage)
		{
			textPut("pixmap.topLeft", "/images/icon_virtual.png", PNL_MAIN, BTN_SOURCE);
		}
		else
		{
			textPut("pixmap.topLeft", "", PNL_MAIN, BTN_SOURCE);
		}
	}

	else if (p->command() == "source" && m_packageIndex > 0)
	{
		int intSelect = p->value().toInt();
		if (m_showHighlight)
		{
			if (intSelect == m_packageIndex)
			{
				textPut("statesheet", "source_selected", PNL_MAIN, BTN_SOURCE);
				m_selected = true;
			}
			else
			{
				textPut("statesheet", "source_deselected", PNL_MAIN, BTN_SOURCE);
				m_selected = false;
			}
		}
	}

	else if (p->command().startsWith("deselect") && m_packageIndex > 0)
	{
		m_selected = false;
		textPut("statesheet", "source_deselected", PNL_MAIN, BTN_SOURCE);
	}


	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "layout=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void source_package_vob::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void source_package_vob::showSourceName()
{
	routerName(m_devicePackagerRouter, DATABASE_SOURCE_NAME, m_packageIndex, m_sourceName);
	if (m_sourceName == "---")
		m_sourceName = "No Preset|Selected";
	textPut("text", m_sourceName, PNL_MAIN, BTN_SOURCE);
}


void source_package_vob::showSourceTitle()
{
	routerName(m_devicePackagerRouter, DATABASE_SOURCE_TITLE, m_packageIndex, m_sourceTitle);
	textPut("text", (m_sourceTitle=="!!!")?"---":m_sourceTitle.replace('|', ' '), PNL_MAIN, LBL_TITLE);
}



