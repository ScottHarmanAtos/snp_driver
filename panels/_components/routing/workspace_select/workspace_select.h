#ifndef workspace_select_INCLUDED
	#define workspace_select_INCLUDED

#include <bncs_script_helper.h>
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif

class workspace_select : public bncs_script_helper
{
public:
	workspace_select( bncs_client_callback * parent, const char* path );
	virtual ~workspace_select();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	
	bncs_string m_selectedWorkspace;

	map<int, bncs_string> m_groupWorkspaces;

	void initGroups();
	void showGroup(int group);
};


#endif // workspace_select_INCLUDED