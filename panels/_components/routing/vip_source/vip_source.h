#ifndef vip_source_INCLUDED
	#define vip_source_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class vip_source : public bncs_script_helper
{
public:
	vip_source( bncs_client_callback * parent, const char* path );
	virtual ~vip_source();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_layout;

	int m_source;
	int m_deviceSource;
	int m_deviceSourceAlarm;
	int m_slotSourceAlarm;
	int m_sourceDeviceOffset;
	int m_sourceAlarmBase;

	bool m_showHighlight;
	bool m_selected;

	void deselect();
	void loadLayout(bncs_string layout);
	void setSource(int source);
	void updateAlarmState(bncs_string sourceAlarms);

};


#endif // vip_source_INCLUDED