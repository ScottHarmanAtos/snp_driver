#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "latching_inspect_button.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( latching_inspect_button )

#define PANEL_MAIN	1

#define DEFAULT_LABEL		"Inspect|Destination"
#define DEFAULT_SELECTED	"enum_ok"
#define DEFAULT_DESELECTED	"enum_deselected"

#define TIMER_SELF_CANCEL	1
#define TIMEOUT_SELF_CANCEL	2000

// constructor - equivalent to ApplCore STARTUP
latching_inspect_button::latching_inspect_button( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_blnSelected = false;
	m_strEvent = "";
	m_strValue = "";
	m_strLabel = DEFAULT_LABEL;
	m_strStatesheetSelected = DEFAULT_SELECTED;
	m_strStatesheetDeselected = DEFAULT_DESELECTED;
	m_intCancelTimeout = TIMEOUT_SELF_CANCEL;
	m_intAngle = 0;
	m_btnPressed = false;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );

	textPut("text", m_strLabel, PANEL_MAIN, "button");

	
}

// destructor - equivalent to ApplCore CLOSEDOWN
latching_inspect_button::~latching_inspect_button()
{
}

// all button pushes and notifications come here
void latching_inspect_button::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if ( b->id() == "button")
		{
			bncs_string strState;
			textGet("statesheet", PANEL_MAIN, "button", strState);
			hostNotify(bncs_string("button=released"));

			if(strState == m_strStatesheetSelected)
			{
				hostNotify( "deselect=deselect" );
				hostNotify( bncs_string("%1=none").arg(m_strEvent) );
				timerStop(TIMER_SELF_CANCEL);
			}
			else
			{
				// No remote feedback at this point to tell us this button
				// has been selected, so set directly
				// 
				m_btnPressed = true;
				
				textPut("statesheet", m_strStatesheetSelected, PANEL_MAIN, "button");
				hostNotify( bncs_string("%1=%2").arg(m_strEvent).arg(m_strValue) );
				timerStart(TIMER_SELF_CANCEL, m_intCancelTimeout);
			}
		}
	}
}

// all revertives come here
int latching_inspect_button::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void latching_inspect_button::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string latching_inspect_button::parentCallback( parentNotify *p )
{
	debug("dest_test::parentCallback() command=%1 value=%2 event/value=%3/%4",
		p->command(), p->value(), m_strEvent, m_strValue);

	if( p->command() == "return" )
	{
		bncs_stringlist sl;
		
		sl << "label=" + m_strLabel;
		sl << "event=" + m_strEvent;
		sl << "value=" + m_strValue;
		sl << "statesheet_selected=" + m_strStatesheetSelected;
		sl << "statesheet_deselected=" + m_strStatesheetDeselected;
		sl << "self_cancel_timeout=" + bncs_string(m_intCancelTimeout);
		sl << "text_angle=" + bncs_string(m_intAngle);
		
		return sl.toString( '\n' );
	}
	else if( p->command() == "label" )
	{
		if(p->value().stripWhiteSpace().length() > 0)
		{
			m_strLabel = p->value();
		}
		else
		{
			m_strLabel = "Button";
		}
		textPut("text", m_strLabel, PANEL_MAIN, "button");
	}
	else if( p->command() == "event" )
	{
		m_strEvent = p->value();
	}
	else if( p->command() == "value" )
	{
		m_strValue = p->value();
	}
	else if( p->command() == "statesheet_selected" )
	{
		m_strStatesheetSelected = p->value();
	}
	else if( p->command() == "statesheet_deselected" )
	{
		m_strStatesheetDeselected = p->value();
	}
	else if( p->command() == "self_cancel_timeout" )
	{
		m_intCancelTimeout = p->value().toInt();
	}
	else if( p->command() == "deselect" )
	{
		textPut("statesheet", m_strStatesheetDeselected, PANEL_MAIN, "button");
		m_blnSelected = false;
		timerStop(TIMER_SELF_CANCEL);
	}
/*	else if (p->command() == "inspect_source_dest")
	{
		if (m_blnSelected)
		{
			textPut("statesheet", m_strStatesheetDeselected, PANEL_MAIN, "button");
			m_blnSelected = false;
			hostNotify(bncs_string("source_index=%1").arg(p->value()));
			timerStop(TIMER_SELF_CANCEL);
		}
	}*/
	else if (p->command() == m_strEvent)
	{
		if (p->value() == m_strValue)
		{
			
			if (p->value() == "inspect")
			{
				if (m_btnPressed)
				{
					textPut("statesheet", m_strStatesheetSelected, PANEL_MAIN, "button");
					m_blnSelected = true;
				}
				else
				{
					textPut("statesheet", m_strStatesheetDeselected, PANEL_MAIN, "button");
					m_blnSelected = false;
				}
				// Clear as only valid for action when button is first pressed on this instance
				// Next time in call may have come from another instance - so reset this one
				m_btnPressed = false;
			}
			else
			{
				textPut("statesheet", m_strStatesheetSelected, PANEL_MAIN, "button");
				m_blnSelected = true;
			}
		}
		else
		{
			textPut("statesheet", m_strStatesheetDeselected, PANEL_MAIN, "button");
			m_blnSelected = false;
		}
		timerStop(TIMER_SELF_CANCEL);
	}
	else if ( p->command() == "text_angle")
	{
		if ( p->value().length() > 0)
		{
			m_intAngle = p->value().toInt();
		}
		textPut("textOrientation", m_intAngle, PANEL_MAIN, "button");
	}
	return "";
}

// timer events come here
void latching_inspect_button::timerCallback( int id )
{
	hostNotify( "deselect=deselect" );
	hostNotify( bncs_string("%1=none").arg(m_strEvent) );
	timerStop(TIMER_SELF_CANCEL);
}
