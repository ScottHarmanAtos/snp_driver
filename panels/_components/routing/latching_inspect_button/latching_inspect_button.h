#ifndef latching_inspect_button_INCLUDED
	#define latching_inspect_button_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class latching_inspect_button : public bncs_script_helper
{
public:
	latching_inspect_button( bncs_client_callback * parent, const char* path );
	virtual ~latching_inspect_button();

	void buttonCallback( buttonNotify *b ) override;
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_intCancelTimeout;
	bool m_blnSelected;
	bool m_btnPressed;
	bncs_string m_strLabel;
	bncs_string m_strEvent;
	bncs_string m_strValue;
	bncs_string m_strStatesheetSelected;
	bncs_string m_strStatesheetDeselected;

	int m_intAngle;
};


#endif // latching_inspect_button_INCLUDED