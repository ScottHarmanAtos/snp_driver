#ifndef audio_package_edit_view_INCLUDED
	#define audio_package_edit_view_INCLUDED

#include <bncs_script_helper.h>
#include "AudioPackage.h"
#include "AudioProc.h"
#include "bncs_packager.h"

#include <map>
typedef map<bncs_string, CAudioProc*> MAP_STRING_PROC;
typedef map<bncs_string, CAudioProc*>::iterator MAP_STRING_PROC_ITERATOR;

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_package_edit_view : public bncs_script_helper
{
public:
	audio_package_edit_view( bncs_client_callback * parent, const char* path );
	virtual ~audio_package_edit_view();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	bncs_string m_instance;
	bncs_string m_instanceRouterVideoHD;
	bncs_string m_instanceRouterVideoHD_section_01;
	bncs_string m_instanceRouterAudio;
	bncs_string m_instanceRouterAudio_section_01;

	//bncs_string m_editLabelField;

	bncs_string m_contextLanguageTag;	//Used by AP, audio returns
	bncs_string m_contextAudioTypeTag;	//Used by pairs, audio returns
	bncs_string m_contextCommsPort;		//Used by CF, Conf
	bncs_string m_contextEditLabel;		//Used by AP, CF, Conf
	bncs_string m_contextVideoHubTag;	//Used by AP Proc, Rev Vision

	bncs_string m_compositePackager;	//c_packager_nhn or c_packager_ldc

	bncs_string m_instancePackagerRouterMain;	//packager_router_nhn_main or packager_router_main

	bncs_packager m_packagerRouter;
	bncs_packager m_packagerAudioPackageReturns;

	CAudioPackage m_diffAP;
	CAudioPackage m_editAP;

	MAP_STRING_PROC m_mapProcs;

	int m_manageProcNum;
	int m_manageProcInputDestPackageIndex;
	int m_manageProcInputDestPackageVideoHubTag;

	bncs_string m_selectedProcId;
	bncs_string m_manageProcId;
	bncs_string m_proc1;
	bncs_string m_proc2;
	bncs_string m_spLevel;

	bool m_applyEnabled;
	bool m_manageProcEnabled;
	bool m_manageConferenceView;

	int m_devicePackageRouter;			//dev_1001 or dev_2001
	int m_devicePackageAudioReturns;	//dev_1041-1056 or dev_2041-2056
	int m_deviceRouterVideoHD;			//dev_1201 or dev_2201
	int m_deviceRouterAudio;			//dev_1261 or dev_2261

	int m_slotPackageAudioReturns;

	int m_AP;
	int m_audioPairPage;
	int m_sourcePackageIndex;

	int m_selectedAudioLevel;
	int m_selectedAudioReturnLevel;
	int m_selectedAudioSource;
	int m_selectedAudioDest;
	int m_selectedVideoDest;
	int m_selectedLanguageTag;
	int m_selectedTypeTag;
	int m_selectedVideoTag;
	int m_selectedCommsRing;
	int m_selectedCommsPort;
	int m_selectedCFLevel;
	int m_selectedConfLevel;
	int m_selectedConfManage;		//0=no conference managed, 1=conf_1 managed, 2=conf_2 managed

	int m_sourcePackageCount;
	int m_sourcePackageUseNext;

	int m_diffIndex[32];
	int m_diffTypeTag[32];

	int m_editIndex[32];
	int m_editTypeTag[32];

	void applyChanges();
	void applyChangesAudioPairs();
	
	void popoverManageConference(buttonNotify *b);
	void popoverAudioProc(buttonNotify *b);
	void popoverSelectAudioProc(buttonNotify *b);
	void popupEditLabel(buttonNotify *b);
	void popupSelectCommsPort(buttonNotify *b);
	void popupAudioTypeTag(buttonNotify *b);
	void popupVideoHubTag(buttonNotify *b);
	void popupIFBRouting(buttonNotify *b);
	void popoverSelectAudioSource(buttonNotify *b);
	void popoverSelectAudioDest(buttonNotify *b);
	void popoverSelectVideoDest(buttonNotify *b);

	void checkApply();

	void editLabel(bncs_string field);
	void editLevelLabel(bncs_string levelField, int level, bncs_string caption);
	void editPackage();
	void refreshAPTags(bncs_string newTags);
	void refreshCFLabels(bncs_string newFields);
	void refreshConfLabels(bncs_string newFields);
	void refreshAudioPairs(bncs_string newLevels);
	void refreshAudioReturns(bncs_string newLevels);
	void refreshName(bncs_string newName);
	void refreshTitle(bncs_string newTitle);
	void refreshAudioProcAssignment(bncs_string fields);

	void setEditAudio(int level, int source);
	void setEditAudioLanguageTag(int languageTag);
	void setEditAudioReturn(int level, int dest);
	void setEditAudioTypeTag(int typeTag);
	void setEditCFCircuit(int level, int ring, int port, bncs_string inOut);
	void setEditCFIFBRouting(int level, int ifb);
	void setEditConfMainPort(int level, int ring, int port, bncs_string inOut);
	void setConfEnable(int level, int confNumber);
	void setCFConfEnable(int level, int confNumber);
	void initEditor();

	void initAudioTypeTagPopup(int level, int typeTag, bncs_string title);
	void initVideoHubTagPopup(int videoHubTag);
	void initManageConfMembers();
	void initAudioProc(int proc);
	void initSelectAudioProc(int proc);
	
	void insertAudioProc();
	void removeAudioProc();

	void loadAudioProcs();
	void showAudioProcLabels();

	void showAudioPairPage(int page);
	void showSelectedAudioLevel();
	void showSelectedAudioReturnLevel();
	void showSelectedCFLevel();
	void showSelectedAudioSource();
	void showSelectedAudioDest();
	void showSelectedVideoDest();
	void showUse();
	void showUseNext();
	void updateLabel(bncs_string label);
	void updateAudioPackageReturnSlot(bncs_string fields);
	void updateProcInputDestPackageVideoHubTag();
	void setProcInputDestPackageVideoHubTag(int newTag);

	CleanFeed* getDiffCF(int level);
	CleanFeed* getEditCF(int level);

	Conference* getDiffConf(int level);
	Conference* getEditConf(int level);
};


#endif // audio_package_edit_view_INCLUDED