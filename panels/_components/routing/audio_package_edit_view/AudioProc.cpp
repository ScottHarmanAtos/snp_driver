#include "AudioProc.h"


CAudioProc::CAudioProc()
{
	for (int input = 0; input < INPUT_COUNT; input++)
	{
		destPackageLevelIn[input] = 0;
		audioTypeIn[input] = 0;
		audioLanguageIn[input] = 0;
	}

	for (int output = 0; output < OUTPUT_COUNT; output++)
	{
		destPackageLevelIn[output] = 0;
		audioIndexOut[output] = 0;
		audioTypeOut[output] = 0;
	}
}


CAudioProc::~CAudioProc()
{
}
