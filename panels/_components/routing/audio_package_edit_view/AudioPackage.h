#pragma once

#include <bncs_string.h>

const int PAIR_COUNT(12);
const int AR_COUNT(4);

struct CleanFeed
{
	int inRing = 0;
	int inPort = 0;
	int outRing = 0;
	int outPort = 0;
	int number = 0; //The cf number
	int routing = 0;
	bncs_string label;
	bncs_string subtitle;

	explicit CleanFeed(int number = 0)
	{
		this->number = number;
	}

	static bool Compare(const CleanFeed& one, const CleanFeed& two)
	{
		return
			one.inRing != two.inRing ||
			one.inPort != two.inPort ||
			one.outRing != two.outRing ||
			one.outPort != two.outPort ||
			one.routing != two.routing ||
			one.label != two.label ||
			one.subtitle != two.subtitle;
	}

	static bool CompareTL(const CleanFeed& one, const CleanFeed& two)
	{
		return
			one.inRing != two.inRing ||
			one.inPort != two.inPort ||
			one.outRing != two.outRing ||
			one.outPort != two.outPort ||
			one.routing != two.routing ||
			one.label != two.label ||
			one.subtitle != two.subtitle;
	}

	void Reset()
	{
		inRing = 0;
		inPort = 0;
		outRing = 0;
		outPort = 0;
		routing = 0;
		label = "";
		subtitle = "";
	}
};

struct Conference
{
	int inRing = 0;
	int inPort = 0;
	int outRing = 0;
	int outPort = 0;
	int number = 0; //The conf number 
	int allocated = 0;
	bncs_string label;
	bncs_string subtitle;

	explicit Conference(int number = 0)
	{
		this->number = number;
	}

	static bool Compare(const Conference& one, const Conference& two)
	{
		return
			one.inRing != two.inRing ||
			one.inPort != two.inPort ||
			one.outRing != two.outRing ||
			one.outPort != two.outPort ||
			one.label != two.label ||
			one.subtitle != two.subtitle;
	}

	static bool CompareTL(const CleanFeed& one, const CleanFeed& two)
	{
		return
			one.inRing != two.inRing ||
			one.inPort != two.inPort ||
			one.outRing != two.outRing ||
			one.outPort != two.outPort;
	}

	void Reset()
	{
		inRing = 0;
		inPort = 0;
		outRing = 0;
		outPort = 0;
		label = "";
		subtitle = "";
	}
};

class CAudioPackage
{

public:
	CAudioPackage();
	~CAudioPackage();
	void reset();

	int index[PAIR_COUNT];
	int typeTag[PAIR_COUNT];
	int language_tag;
	int rev_vision;
	int rev_vision_hub_tag;

	int arIndex[AR_COUNT];
	int arTypeTag[AR_COUNT];
	int arLanguageTag[AR_COUNT];

	bncs_string name;
	bncs_string package_title;

	CleanFeed cf_1;
	CleanFeed cf_2;
	CleanFeed cf_3;
	CleanFeed cf_4;

	Conference conf_1;
	Conference conf_2;
};

