#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "audio_package_edit_view.h"
#include "packager_common.h"
#include "instanceLookup.h"

#define PNL_MAIN					1
#define POPOVER_SELECT_AUDIO_SOURCE	2
#define POPOVER_SELECT_AUDIO_DEST	3
#define POPOVER_SELECT_VIDEO_DEST	4
#define POPUP_AUDIO_TYPE_TAG		5
#define POPUP_LANGUAGE_TAG			6
#define	POPUP_VIDEO_HUB_TAG			7
#define POPUP_EDIT_LABEL			8
#define	POPUP_SELECT_COMMS_PORT		9
#define	POPUP_IFB_ROUTING			10
#define POPOVER_MANAGE_CONFERENCE	11
#define POPOVER_AUDIO_PROC			12
#define POPOVER_SELECT_AUDIO_PROC	13

#define TIMER_SETUP			1
#define TIMER_USAGE_NEXT	2
#define TIMEOUT_USAGE_NEXT	10

#define AR_COUNT					4
#define CF_COUNT					4
#define CONF_COUNT					2

#define LBL_TITLE			"title"
#define BTN_LANGUAGE		"language"

#define VIDEO_RETURN		"video_return"
#define VIDEO_RETURN_NAME	"video_return_name"
#define VIDEO_RETURN_HUB_TAG "video_return_hub_tag"

#define BTN_APPLY			"apply"
#define BTN_CANCEL			"cancel"
#define BTN_CLOSE			"close"
#define BTN_NONE			"none"
#define BTN_RESET			"reset"
#define BTN_SELECT			"select"
#define BTN_SET				"set"
#define BTN_SET_IN			"set_in"
#define BTN_SET_OUT			"set_out"

#define BTN_INSERT_AUTOMIX		"insert_automix"
#define BTN_SHOW_USE			"show_use"
#define LVW_CURRENT_USE			"current_use"
#define LBL_USAGE_STATUS		"usage_status"

#define MANAGE_CONF_MEMBERS		"manage_conf_members"

#define FIELD_PACKAGE_NAME		"package_name"
#define FIELD_PACKAGE_TITLE		"package_title"

#define IFB_LABEL				"ifb_label"
#define IFB_ROUTING				"ifb_routing"
#define IFB_SUBTITLE			"ifb_subtitle"
#define CONF_ENABLE				"conf_enable"
#define CONF_LABEL				"conf_label"
#define CONF_SUBTITLE			"conf_subtitle"
#define LANGUAGE_TAG			"language_tag"
#define TYPE_TAG				"type_tag"

#define PROC_INPUT_HUB_TAG		"input_video_hub_tag"
#define PROC_INPUT_DEST_PACKAGE	"input_dest_package"

#define SOURCE_GRID				"source_grid"
#define DEST_GRID				"dest_grid"
#define LANGUAGE_GRID			"language_grid"
#define DISMISS_POPUP			"dismiss"

#define IN_USE					"@IN_USE@"

/*
#define DATABASE_SOURCE_NAME	0
#define DATABASE_DEST_NAME		1
#define DATABASE_SOURCE_AP		6

#define DATABASE_LANG_TAG		43
#define DATABASE_TYPE_TAG		44

#define DATABASE_AP_NAME		60
#define DATABASE_AP_LONG_NAME	61
#define DATABASE_AP_ID_OWNER	62
#define DATABASE_AP_TAGS		63
#define DATABASE_AP_PAIRS		64
#define DATABASE_AP_RETURN		65
#define DATABASE_AP_VIDEO_LINK	66
#define DATABASE_AP_CF_LABELS	67
#define DATABASE_AP_CONF_LABELS	68

#define	INSTANCE_PACKAGE_ROUTER	"packager_router_main"
*/


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( audio_package_edit_view )

// constructor - equivalent to ApplCore STARTUP
audio_package_edit_view::audio_package_edit_view( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_AP = 0;
	m_sourcePackageIndex = 0;
	m_audioPairPage = 0;
	m_selectedCFLevel = 0;
	m_selectedConfLevel = 0;
	m_selectedAudioLevel = 0;
	m_selectedAudioReturnLevel = 0;
	m_selectedAudioSource = 0;
	m_selectedAudioDest = 0;
	m_selectedVideoDest = 0;
	m_selectedCommsRing = 0;
	m_selectedCommsPort = 0;
	m_selectedLanguageTag = 0;
	m_selectedTypeTag = 0;
	m_sourcePackageCount = 0;
	m_sourcePackageUseNext = 0;
	m_applyEnabled = false;
	m_manageConferenceView = false;
	m_manageProcEnabled = false;
	m_selectedConfManage = 0;

	m_deviceRouterAudio = 0;
	m_deviceRouterVideoHD = 0;
	m_devicePackageRouter = 0;
	m_devicePackageAudioReturns = 0;
	m_slotPackageAudioReturns = 0;

	m_diffAP.reset();
	m_editAP.reset();

	m_manageProcInputDestPackageIndex = 0;
	m_manageProcInputDestPackageVideoHubTag = 0;
	m_manageProcNum = 0;
	m_manageProcId = "";
	m_proc1 = "";
	m_proc2 = "";

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );
	setSize(PNL_MAIN);

	controlDisable(PNL_MAIN, BTN_INSERT_AUTOMIX);
	//controlDisable(PNL_MAIN, BTN_SHOW_USE);

	initEditor();
}

// destructor - equivalent to ApplCore CLOSEDOWN
audio_package_edit_view::~audio_package_edit_view()
{
}

// all button pushes and notifications come here
void audio_package_edit_view::buttonCallback( buttonNotify *b )
{
	b->dump("audio_package_edit_view::buttonCallback");
	if( b->panel() == PNL_MAIN )
	{
		bncs_stringlist controlIndex(b->id(), '_');
		int index = controlIndex[1].toInt();
		if (b->id().startsWith("pair-page_"))
		{
			showAudioPairPage(index);
		}
		else if (b->id().startsWith("pair_"))
		{
			if (b->command() == "display_level")
			{
				debug("audio_package_edit_view::buttonCallback() m_instanceRouterAudio_section_01=%1", m_instanceRouterAudio_section_01);

				m_selectedAudioLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_AUDIO_SOURCE, "popover_select_audio_source.bncs_ui");
				textPut("text", bncs_string("  Select Audio Source - Level %1").arg(b->value()), POPOVER_SELECT_AUDIO_SOURCE, "title");
				textPut("instance", m_instanceRouterAudio_section_01, POPOVER_SELECT_AUDIO_SOURCE, SOURCE_GRID);
				textPut("index", 0, POPOVER_SELECT_AUDIO_SOURCE, SOURCE_GRID);
				m_selectedAudioSource = 0;
				showSelectedAudioSource();

				controlDisable(POPOVER_SELECT_AUDIO_SOURCE, BTN_SET);

				//deselect Audio Return level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_DEST);
				panelRemove(POPOVER_SELECT_VIDEO_DEST);
				m_selectedAudioReturnLevel = 0;
				showSelectedAudioReturnLevel();
				textPut("statesheet", "enum_deselected", PNL_MAIN, VIDEO_RETURN);

				//Show selected audiolevel
				showSelectedAudioLevel();

				//deselect AP level and hide popover
				//panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
				//m_intSelectedAPLevel = 0;
				//showSelectedAPLevel();
			}
			else if (b->command() == TYPE_TAG)
			{
				panelPopup(POPUP_AUDIO_TYPE_TAG, "popup_audio_type_tag.bncs_ui");

				bncs_string level;
				bncs_string tag;
				b->value().split(',', level, tag);

				m_selectedAudioLevel = level.toInt();
				m_contextAudioTypeTag = b->id().append(".").append(TYPE_TAG);
				bncs_string title = bncs_string(" Set Audio Pair Type Tag - Level: %1").arg(level.toInt());
				initAudioTypeTagPopup(level, tag.toInt(), title);
			}
		}
		else if (b->id().startsWith("ar_"))
		{
			if (b->command() == "display_level")
			{
				m_selectedAudioReturnLevel = b->value().toInt();
				//Note the only context for Audio Destination Selection is for Audio Return
				panelShow(POPOVER_SELECT_AUDIO_DEST, "popover_select_audio_dest.bncs_ui");
				textPut("text", bncs_string("  Select Audio Dest - Audio Return Level %1").arg(b->value()), POPOVER_SELECT_AUDIO_DEST, "title");
				textPut("instance", m_instanceRouterAudio_section_01, POPOVER_SELECT_AUDIO_DEST, DEST_GRID);
				textPut("index", 0, POPOVER_SELECT_AUDIO_DEST, DEST_GRID);
				m_selectedAudioDest = 0;
				showSelectedAudioDest();

				controlDisable(POPOVER_SELECT_AUDIO_DEST, BTN_SET);

				//deselect Audio Pair level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
				panelRemove(POPOVER_SELECT_VIDEO_DEST);
				m_selectedAudioLevel = 0;
				showSelectedAudioLevel();
				textPut("statesheet", "enum_deselected", PNL_MAIN, VIDEO_RETURN);

				//Show selected audio return level
				showSelectedAudioReturnLevel();

				//deselect AP level and hide popover
				//panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
				//m_intSelectedAPLevel = 0;
				//showSelectedAPLevel();
			}
			else if (b->command() == LANGUAGE_TAG)
			{
				m_selectedAudioReturnLevel = index;
				m_contextLanguageTag = b->id().append(".").append(LANGUAGE_TAG);
				panelPopup(POPUP_LANGUAGE_TAG, "popup_language_tag.bncs_ui");

				bncs_string title = bncs_string(" Set Audio Return Language Tag - Level: %1").arg(m_selectedAudioReturnLevel);
				textPut("text", title, POPUP_LANGUAGE_TAG, "title");
				textPut("instance", m_instancePackagerRouterMain, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
				textPut("selected", m_editAP.arLanguageTag[m_selectedAudioReturnLevel - 1], POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
			}
			else if (b->command() == TYPE_TAG)
			{
				m_selectedAudioReturnLevel = index;
				m_contextAudioTypeTag = b->id().append(".").append(TYPE_TAG);
				panelPopup(POPUP_AUDIO_TYPE_TAG, "popup_audio_type_tag.bncs_ui");

				bncs_string title = bncs_string(" Set Audio Return Type Tag - Level: %1").arg(m_selectedAudioReturnLevel);
				bncs_string level;
				bncs_string tag;
				b->value().split(',', level, tag);

				initAudioTypeTagPopup(level, tag.toInt(), title);
			}
		}
		else if (b->id().startsWith("cf_"))
		{
			if (b->command() == "display_level" || b->command() == "cf_circuit")
			{
				m_selectedCFLevel = index;
				panelPopup(POPUP_SELECT_COMMS_PORT, "popup_select_comms_port.bncs_ui");
				textPut("text", bncs_string("Set CF%1 Circuit port").arg(m_selectedCFLevel), POPUP_SELECT_COMMS_PORT, "title");

				m_selectedCommsRing = 0;
				m_selectedCommsPort = 0;
			}
			else if (b->command() == IFB_LABEL)
			{
				m_selectedCFLevel = index;
				editLevelLabel(b->id().append(".").append(IFB_LABEL), m_selectedCFLevel, 
					bncs_string("Edit CF%1 IFB Label").arg(m_selectedCFLevel));
			}
			else if (b->command() == IFB_SUBTITLE)
			{
				m_selectedCFLevel = index;
				editLevelLabel(b->id().append(".").append(IFB_SUBTITLE), m_selectedCFLevel,
					bncs_string("Edit CF%1 IFB Subtitle").arg(m_selectedCFLevel));
			}
			else if (b->command() == IFB_ROUTING)
			{
				m_selectedCFLevel = index;
				panelPopup(POPUP_IFB_ROUTING, "popup_ifb_routing.bncs_ui");
				textPut("text", bncs_string("Set CF%1 IFB Routing").arg(m_selectedCFLevel), POPUP_IFB_ROUTING, "title");

				CleanFeed* edit = getEditCF(m_selectedCFLevel);

				int ifbRouting = edit->routing;
				if (ifbRouting == 1)
				{
					textPut("statesheet", "enum_selected", POPUP_IFB_ROUTING, "ifb_1");
				}
				else if (ifbRouting == 2)
				{
					textPut("statesheet", "enum_selected", POPUP_IFB_ROUTING, "ifb_2");
				}
			}
			else if (b->command() == CONF_ENABLE)
			{
				bncs_string level;
				bncs_string confNumber;
				b->value().split(',', level, confNumber);
				setCFConfEnable(index, confNumber.toInt());
			}

			showSelectedCFLevel();
		}
		else if (b->id().startsWith("conf_"))
		{
			if (b->command() == "conf_enable")
			{
				bncs_string level;
				bncs_string confNumber;
				b->value().split(',', level, confNumber);
				setConfEnable(index, confNumber.toInt());
			}
			else if (b->command() == "main_port")
			{
				m_selectedConfLevel = index;
				panelPopup(POPUP_SELECT_COMMS_PORT, "popup_select_comms_port.bncs_ui");
				textPut("text", bncs_string("Set Conference %1 Main Port").arg(m_selectedConfLevel), POPUP_SELECT_COMMS_PORT, "title");

				m_selectedCommsRing = 0;
				m_selectedCommsPort = 0;
			}
			else if (b->command() == CONF_LABEL)
			{
				m_selectedConfLevel = index;
				editLevelLabel(b->id().append(".").append(CONF_LABEL), m_selectedConfLevel,
					bncs_string("Edit Conference %1 Label").arg(m_selectedConfLevel));
			}
			else if (b->command() == CONF_SUBTITLE)
			{
				m_selectedConfLevel = index;
				editLevelLabel(b->id().append(".").append(CONF_SUBTITLE), m_selectedConfLevel,
					bncs_string("Edit Conference %1 Subtitle").arg(m_selectedConfLevel));
			}

			//showSelectedConfLevel();
		}
		else if (b->id() == MANAGE_CONF_MEMBERS)
		{
			initManageConfMembers();
		}
		else if (b->id() == "insert_audio_proc_1" || b->id() == "tally_audio_proc_1")
		{
			if (m_manageProcEnabled)
			{
				if (m_proc1 == "")
				{
					panelShow(POPOVER_SELECT_AUDIO_PROC, "popover_select_audio_proc.bncs_ui");
					initSelectAudioProc(1);
				}
				else
				{
					initAudioProc(1);
				}
			}
		}
		else if (b->id() == "insert_audio_proc_2" || b->id() == "tally_audio_proc_2")
		{
			if (m_manageProcEnabled)
			{
				if (m_proc2 == "")
				{
					panelShow(POPOVER_SELECT_AUDIO_PROC, "popover_select_audio_proc.bncs_ui");
					initSelectAudioProc(2);
				}
				else
				{
					initAudioProc(2);
				}
			}
		}
		else if (b->id() == FIELD_PACKAGE_NAME)
		{
			editLabel(b->id());
		}
		else if (b->id() == FIELD_PACKAGE_TITLE)
		{
			editLabel(b->id());
		}
		else if (b->id() == BTN_LANGUAGE)
		{
			m_contextLanguageTag = BTN_LANGUAGE;
			panelPopup(POPUP_LANGUAGE_TAG, "popup_language_tag.bncs_ui");
			textPut("text", " Set Audio Package Language Tag", POPUP_LANGUAGE_TAG, LBL_TITLE);
			textPut("instance", m_instancePackagerRouterMain, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
			textPut("selected", m_editAP.language_tag, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
		}
		else if (b->id() == VIDEO_RETURN)
		{
			//Note the only context for Video Destination Selection is for Return Video
			panelShow(POPOVER_SELECT_VIDEO_DEST, "popover_select_video_dest.bncs_ui");
			textPut("text", "  Select Video Return Destination", POPOVER_SELECT_VIDEO_DEST, "title");
			textPut("instance", m_instanceRouterVideoHD_section_01, POPOVER_SELECT_VIDEO_DEST, DEST_GRID);
			textPut("index", 0, POPOVER_SELECT_VIDEO_DEST, DEST_GRID);
			m_selectedVideoDest = 0;
	
			controlDisable(POPOVER_SELECT_VIDEO_DEST, BTN_SET);

			//deselect Audio Pair level and hide popover
			panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
			panelRemove(POPOVER_SELECT_AUDIO_DEST);
			m_selectedAudioLevel = 0;
			m_selectedAudioReturnLevel = 0;
			showSelectedAudioLevel();
			showSelectedAudioReturnLevel();
			textPut("statesheet", "enum_selected", PNL_MAIN, VIDEO_RETURN);
		}
		else if (b->id() == VIDEO_RETURN_HUB_TAG)
		{
			panelPopup(POPUP_VIDEO_HUB_TAG, "popup_video_hub_tag.bncs_ui");
			textPut("text", " Set Video Return Hub Tag", POPUP_VIDEO_HUB_TAG, LBL_TITLE);
			initVideoHubTagPopup(m_editAP.rev_vision_hub_tag);
			
			//set context for hub tag Proc Input
			m_contextVideoHubTag = VIDEO_RETURN_HUB_TAG;
		}
		else if (b->id() == BTN_SHOW_USE)
		{
			showUse();
		}
		else if (b->id() == BTN_CLOSE)
		{
			hostNotify("action=close");
		}
		else if (b->id() == BTN_APPLY)
		{
			applyChanges();
		}
	}
	else if (b->panel() == POPOVER_SELECT_AUDIO_SOURCE)
	{	
		popoverSelectAudioSource(b);
	}
	else if (b->panel() == POPOVER_SELECT_AUDIO_DEST)
	{
		popoverSelectAudioDest(b);
	}
	else if (b->panel() == POPOVER_SELECT_VIDEO_DEST)
	{
		popoverSelectVideoDest(b);
	}
	else if (b->panel() == POPUP_VIDEO_HUB_TAG)
	{
		popupVideoHubTag(b);
	}
	else if (b->panel() == POPUP_AUDIO_TYPE_TAG)
	{
		popupAudioTypeTag(b);
	}
	else if (b->panel() == POPUP_LANGUAGE_TAG)
	{
		if (b->id() == BTN_SET)
		{
			panelDestroy(POPUP_LANGUAGE_TAG);
			setEditAudioLanguageTag(m_selectedLanguageTag);
		}
		else if (b->id() == LANGUAGE_GRID)
		{
			if (b->command() == "select")
			{
				m_selectedLanguageTag = b->value().toInt();
				textPut("selected", m_selectedLanguageTag, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
			}
		}
		if (b->id() == BTN_NONE)
		{
			m_selectedLanguageTag = 0;
			textPut("selected", m_selectedLanguageTag, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_LANGUAGE_TAG);
		}
	}
	else if (b->panel() == POPUP_EDIT_LABEL)
	{
		popupEditLabel(b);
	}
	else if (b->panel() == POPUP_IFB_ROUTING)
	{
		popupIFBRouting(b);
	}
	else if (b->panel() == POPUP_SELECT_COMMS_PORT)
	{
		popupSelectCommsPort(b);
		/*
		if (b->id() == "grid" && b->command() == "index")
		{
			if (b->value().contains('.'))
			{
				bncs_string ring;
				bncs_string port;
				b->value().split('.', ring, port);

				m_selectedCommsRing = ring.toInt();
				m_selectedCommsPort = port.toInt();

				textPut("text", bncs_string("Ring: %1|Port: %2").arg(m_selectedCommsRing).arg(m_selectedCommsPort), POPUP_SELECT_COMMS_PORT, "selected_port");
			}
			else
			{
				m_selectedCommsRing = 0;
				m_selectedCommsPort = 0;
				textPut("text", "---", POPUP_SELECT_COMMS_PORT, "selected_port");
			}
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("deselect", POPUP_SELECT_COMMS_PORT, "grid");
			m_selectedCommsRing = 0;
			m_selectedCommsPort = 0;
			textPut("text", "---", POPUP_SELECT_COMMS_PORT, "selected_port");
		}
		else if (b->id() == BTN_SET)
		{
			panelDestroy(POPUP_SELECT_COMMS_PORT);
			setEditCFCircuit(m_selectedCFLevel, m_selectedCommsRing, m_selectedCommsPort, "both");	//in & out
		}
		else if (b->id() == BTN_SET_IN)
		{
			panelDestroy(POPUP_SELECT_COMMS_PORT);
			setEditCFCircuit(m_selectedCFLevel, m_selectedCommsRing, m_selectedCommsPort, "in");		//in only
		}
		else if (b->id() == BTN_SET_OUT)
		{
			panelDestroy(POPUP_SELECT_COMMS_PORT);
			setEditCFCircuit(m_selectedCFLevel, m_selectedCommsRing, m_selectedCommsPort, "out");	//out only
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_SELECT_COMMS_PORT);
		}
		*/
	}
	else if (b->panel() == POPOVER_MANAGE_CONFERENCE)
	{
		popoverManageConference(b);
	}
	else if (b->panel() == POPOVER_AUDIO_PROC)
	{
		popoverAudioProc(b);
	}
	else if (b->panel() == POPOVER_SELECT_AUDIO_PROC)
	{
		popoverSelectAudioProc(b);
	}
}

// all revertives come here
int audio_package_edit_view::revertiveCallback( revertiveNotify * r )
{
	r->dump("audio_package_edit_view::revertiveCallback");
	if (r->device() == m_devicePackageAudioReturns && r->index() == m_slotPackageAudioReturns)
	{
		updateAudioPackageReturnSlot(r->sInfo());
	}
	return 0;
}

// all database name changes come back here
void audio_package_edit_view::databaseCallback( revertiveNotify * r )
{
	debug("audio_package_edit_view::databaseCallback() dev=%1 database=%2 index=%3 value=%4", r->device(), r->database(), r->index(), r->sInfo());

	if (r->device() == m_devicePackageRouter)
	{
		if (r->index() == m_AP)
		{
			if (r->database() == DATABASE_AP_NAME)
			{
				refreshName(r->sInfo());
			}
			else if (r->database() == DATABASE_AP_LONG_NAME)
			{
				refreshTitle(r->sInfo());
			}
			else if (r->database() == DATABASE_AP_TAGS)
			{
				refreshAPTags(r->sInfo());
			}
			else if (r->database() == DATABASE_AP_PAIRS)
			{
				refreshAudioPairs(r->sInfo());
			}
			else if (r->database() == DATABASE_AP_RETURN)
			{
				refreshAudioReturns(r->sInfo());
			}
			else if (r->database() == DATABASE_AP_CF_LABELS)
			{
				refreshCFLabels(r->sInfo());
			}
			else if (r->database() == DATABASE_AP_CONF_LABELS)
			{
				refreshConfLabels(r->sInfo());
			}
			else if (r->database() == DATABASE_AP_AUDIO_PROC)
			{
				refreshAudioProcAssignment(r->sInfo());
			}

		}
		else if (r->index() == m_manageProcInputDestPackageIndex)
		{
			if (r->database() == DATABASE_DEST_LEVELS)
			{
				updateProcInputDestPackageVideoHubTag();
			}
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string audio_package_edit_view::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "ap=%1" ).arg( m_AP );
			
			return sl.toString( '\n' );
		}

		else if( p->value() == "ap" )
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_AP ) );
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "sp_level")
	{	// Persisted value or 'Command' being set here
		m_spLevel = p->value();
	}

	else if( p->command() == "ap" )
	{	// Persisted value or 'Command' being set here
		m_AP = p->value().toInt();
		editPackage();
	}

	else if (p->command() == "sp")
	{	// Persisted value or 'Command' being set here
		m_sourcePackageIndex = p->value().toInt();
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "ap=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void audio_package_edit_view::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;
	case TIMER_USAGE_NEXT:
		showUseNext();
		break;
	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void audio_package_edit_view::checkApply()
{
	//check pairs
	bool dirtyPair = false;
	for (int pair = 0; pair < PAIR_COUNT; pair++)
	{
		if (m_editAP.index[pair] != m_diffAP.index[pair] || m_editAP.typeTag[pair] != m_diffAP.typeTag[pair])
		{
			dirtyPair = true;
		}
	}

	//check audio returns
	bool dirtyAudioReturn = false;
	for (int audioReturn = 0; audioReturn < AR_COUNT; audioReturn++)
	{
		if (m_editAP.arIndex[audioReturn] != m_diffAP.arIndex[audioReturn] || 
			m_editAP.arLanguageTag[audioReturn] != m_diffAP.arLanguageTag[audioReturn] ||
			m_editAP.arTypeTag[audioReturn] != m_diffAP.arTypeTag[audioReturn])
		{
			dirtyAudioReturn = true;
		}
	}

	//check clean feeds
	bool dirtyCF = false;
	if (CleanFeed::Compare(m_editAP.cf_1, m_diffAP.cf_1) || 
		CleanFeed::Compare(m_editAP.cf_2, m_diffAP.cf_2) ||
		CleanFeed::Compare(m_editAP.cf_3, m_diffAP.cf_3) ||
		CleanFeed::Compare(m_editAP.cf_4, m_diffAP.cf_4)
		)
	{
		dirtyCF = true;
	}

	//check conferences
	bool dirtyConf = false;
	if (Conference::Compare(m_editAP.conf_1, m_diffAP.conf_1) ||
		Conference::Compare(m_editAP.conf_2, m_diffAP.conf_2)
		)
	{
		dirtyConf = true;
	}


	if (m_editAP.name != m_diffAP.name ||
		m_editAP.package_title != m_diffAP.package_title ||
		m_editAP.language_tag != m_diffAP.language_tag ||
		m_editAP.rev_vision != m_diffAP.rev_vision ||
		m_editAP.rev_vision_hub_tag != m_diffAP.rev_vision_hub_tag ||
		dirtyPair == true || dirtyAudioReturn ==true || dirtyCF == true || dirtyConf == true)
	{
		controlEnable(PNL_MAIN, BTN_APPLY);
		m_applyEnabled = true;
	}
	else
	{
		controlDisable(PNL_MAIN, BTN_APPLY);
		m_applyEnabled = false;
	}

	debug("audio_package_edit_view::checkApply() dirtyPair=%1 dirtyAudioReturn=%2 dirtyCF=%3 dirtyConf=%4", 
		dirtyPair ? "true" : "false", dirtyAudioReturn ? "true" : "false", 
		dirtyCF ? "true" : "false", dirtyConf ? "true" : "false");

	//Conditional enable of audio proc buttons - only enabled if the language is 
	if (m_editAP.language_tag != m_diffAP.language_tag || dirtyPair == true)
	{
		m_manageProcEnabled = false;
		controlDisable(PNL_MAIN, "insert_audio_proc_1");
		controlDisable(PNL_MAIN, "insert_audio_proc_2");
		//controlDisable(PNL_MAIN, "tally_audio_proc_1");
		//controlDisable(PNL_MAIN, "tally_audio_proc_2");
	}
	else
	{
		m_manageProcEnabled = true;
		controlEnable(PNL_MAIN, "insert_audio_proc_1");
		controlEnable(PNL_MAIN, "insert_audio_proc_2");
		//controlEnable(PNL_MAIN, "tally_audio_proc_1");
		//controlEnable(PNL_MAIN, "tally_audio_proc_2");
	}
}

void audio_package_edit_view::applyChanges()
{
	debug("audio_package_edit_view::applyChanges()");
	m_applyEnabled = false;

	//Update Name
	if (m_editAP.name != m_diffAP.name)
	{
		routerModify(m_devicePackageRouter, DATABASE_AP_NAME, m_AP, m_editAP.name, false);
	}

	//Update Title
	if (m_editAP.package_title != m_diffAP.package_title)
	{
		routerModify(m_devicePackageRouter, DATABASE_AP_LONG_NAME, m_AP, m_editAP.package_title, false);
	}

	applyChangesAudioPairs();

	//check audio returns
	bncs_stringlist sltAudioReturns;
	bool dirtyAudioReturn = false;
	for (int audioReturn = 0; audioReturn < AR_COUNT; audioReturn++)
	{
		sltAudioReturns.append(bncs_string("a%1=%2#%3:%4")
			.arg(audioReturn + 1)
			.arg(m_editAP.arIndex[audioReturn])
			.arg(m_editAP.arLanguageTag[audioReturn])
			.arg(m_editAP.arTypeTag[audioReturn]));
		if (m_editAP.arIndex[audioReturn] != m_diffAP.arIndex[audioReturn] ||
			m_editAP.arLanguageTag[audioReturn] != m_diffAP.arLanguageTag[audioReturn] ||
			m_editAP.arTypeTag[audioReturn] != m_diffAP.arTypeTag[audioReturn])
		{
			dirtyAudioReturn = true;
		}
	}

	//31801=a1=12345#41:291,a2=12345#51:327,a3=12345#56:345,a4=12345#106:524
	if (dirtyAudioReturn == true)
	{
		routerModify(m_devicePackageRouter, DATABASE_AP_RETURN, m_AP, sltAudioReturns.toString(), false);
	}

	//check clean feeds
	bncs_stringlist sltCFCircuits;
	bncs_stringlist sltCFIFBRouting;
	bncs_stringlist sltCFLabelSubtitle;
	bool dirtyCFCircuit = false;
	bool dirtyCFIFBRouting = false;
	bool dirtyCFLabelSubtitle = false;

	for (int level = 1; level <= CF_COUNT; level++)
	{
		CleanFeed* diff = getDiffCF(level);
		CleanFeed* edit = getEditCF(level);

		//Check circuits
		sltCFCircuits.append(bncs_string("cf%1=%2.%3/%4.%5").arg(level)
			.arg(edit->inRing).arg(edit->inPort).arg(edit->outRing).arg(edit->outPort));
		if (edit->inRing != diff->inRing ||
			edit->inPort != diff->inPort ||
			edit->outRing != diff->outRing || 
			edit->outPort != diff->outPort)
		{
			dirtyCFCircuit = true;
		}

		sltCFIFBRouting.append(bncs_string("cf%1_ifb=%2").arg(level).arg(edit->routing));
		if (edit->routing != diff->routing)
		{
			dirtyCFIFBRouting = true;
		}

		//Check labels and subtitles 
		//l1=label1,s1=subtitle1,l2=label2,s2=subtitle2,l3=,s3=,l4=,s4=
		sltCFLabelSubtitle.append(bncs_string("label_%1=%2").arg(level).arg(edit->label));
		sltCFLabelSubtitle.append(bncs_string("sub_%1=%2").arg(level).arg(edit->subtitle));
		if (edit->label != diff->label || edit->subtitle != diff->subtitle)
		{
			dirtyCFLabelSubtitle = true;
		}
	}

	//check conferences
	bncs_stringlist sltConfMainPorts;
	bncs_stringlist sltConfLabelSubtitle;
	bool dirtyConfMainPort = false;
	bool dirtyConfLabelSubtitle = false;

	for (int level = 1; level <= CONF_COUNT; level++)
	{
		Conference* diff = getDiffConf(level);
		Conference* edit = getEditConf(level);

		//Check circuits
		sltConfMainPorts.append(bncs_string("conf%1=%2.%3/%4.%5").arg(level)
			.arg(edit->inRing).arg(edit->inPort).arg(edit->outRing).arg(edit->outPort));
		if (edit->inRing != diff->inRing ||
			edit->inPort != diff->inPort ||
			edit->outRing != diff->outRing ||
			edit->outPort != diff->outPort)
		{
			dirtyConfMainPort = true;
		}

		//Check labels and subtitles 
		//l1=label1,s1=subtitle1,l2=label2,s2=subtitle2,l3=,s3=,l4=,s4=
		sltConfLabelSubtitle.append(bncs_string("label_%1=%2").arg(level).arg(edit->label));
		sltConfLabelSubtitle.append(bncs_string("sub_%1=%2").arg(level).arg(edit->subtitle));
		if (edit->label != diff->label || edit->subtitle != diff->subtitle)
		{
			dirtyConfLabelSubtitle = true;
		}
	}


	//Update tags/CleanFeed/Conf
	if (m_editAP.language_tag != m_diffAP.language_tag || 
		m_editAP.rev_vision != m_diffAP.rev_vision ||
		m_editAP.rev_vision_hub_tag != m_diffAP.rev_vision_hub_tag ||
		dirtyCFCircuit || dirtyCFIFBRouting || dirtyConfMainPort)
	{
		//db0063: rev_vision=#,language_tag=21,ifb1=,ifb2=,4w1=#0,4w2=#0

		bncs_string apTags = bncs_string("rev_vision=%1#%2,language_tag=%3,%4,%5,%6")
			.arg(m_editAP.rev_vision)				//rev vision index
			.arg(m_editAP.rev_vision_hub_tag)		//rev vision hub tag
			.arg(m_editAP.language_tag)				//language tag
			.arg(sltCFCircuits.toString())			//cf1,cf2,cf3,cf4
			.arg(sltCFIFBRouting.toString())		//cf1_ifb,cf2_ifb,cf3_ifb,cf4_ifb
			.arg(sltConfMainPorts.toString());		//conf1,conf2

		//31801=video=201#0,rev_vision=#,language_tag=21,ifb1=11.457/11.601,ifb2=11.514/11.514,ifb3=,ifb4=,4w1=#0,4w2=#0
		//31801=video=201#0,rev_vision=#,language_tag=21,cf1=11.457/11.601,cf2=11.514/11.514,cf3=,cfb4=,conf1=,conf2=
		routerModify(m_devicePackageRouter, DATABASE_AP_TAGS, m_AP, apTags, false);
	}

	//Update CF Labels/Subtitles
	if (dirtyCFLabelSubtitle)
	{
		routerModify(m_devicePackageRouter, DATABASE_AP_CF_LABELS, m_AP, sltCFLabelSubtitle.toString(), false);
	}

	//Update Conf Labels/Subtitles
	if (dirtyConfLabelSubtitle)
	{
		routerModify(m_devicePackageRouter, DATABASE_AP_CONF_LABELS, m_AP, sltConfLabelSubtitle.toString(), false);
	}

	//Do this check here as sometimes we don't get any callbacks but everything is correct.
	checkApply();
}

void audio_package_edit_view::applyChangesAudioPairs()
{
	//Update audio pairs
	bool dirtyPair = false;
	bncs_stringlist sltPairs;
	for (int pair = 0; pair < PAIR_COUNT; pair++)
	{
		bncs_string indexTypeTag = bncs_string("a%1=%2#%3")
			.arg(pair + 1)
			.arg(m_editAP.index[pair])
			.arg(m_editAP.typeTag[pair]);
		sltPairs.append(indexTypeTag);

		if (m_editAP.index[pair] != m_diffAP.index[pair] || m_editAP.typeTag[pair] != m_diffAP.typeTag[pair])
		{
			dirtyPair = true;
		}
	}

	//31801=a1=12081#21,a2=12082#22,a3=12083#24,a4=12084#25,a5=0#,a6=0#,a7=0#,a8=0#,a9=#,a10=#,a11=#,a12=#,a13=#,a14=#,a15=#,a16=#
	if (dirtyPair == true)
	{
		routerModify(m_devicePackageRouter, DATABASE_AP_PAIRS, m_AP, sltPairs.toString(), false);
	}
}

void audio_package_edit_view::editPackage()
{
	controlDisable(PNL_MAIN, BTN_APPLY);
	textPut("clear", PNL_MAIN, LVW_CURRENT_USE);

	//reset vars
	m_selectedAudioLevel = 0;
	m_selectedAudioSource = 0;
	m_selectedTypeTag = 0;
	m_manageProcNum = 0;
	m_manageProcInputDestPackageIndex = 0;
	m_manageProcInputDestPackageVideoHubTag = 0;

	//Show index and current name of package
	textPut("text", m_AP, PNL_MAIN, "package_index");

	bncs_string apName;
	routerName(m_devicePackageRouter, DATABASE_AP_NAME, m_AP, apName);
	m_diffAP.name = apName;
	m_editAP.name = apName;

	textPut("text", "  " + apName.replace("|", "|  "), PNL_MAIN, FIELD_PACKAGE_NAME);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_PACKAGE_NAME);

	bncs_string apLongName;
	routerName(m_devicePackageRouter, DATABASE_AP_LONG_NAME, m_AP, apLongName);
	m_diffAP.package_title = apLongName;
	m_editAP.package_title = apLongName;

	textPut("text", "  " + apLongName.replace("|", "|  "), PNL_MAIN, FIELD_PACKAGE_TITLE);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_PACKAGE_TITLE);

	//Update title
	if (m_AP > 0)
	{
		textPut("text", bncs_string("  Editing Audio Package: %1 / %2").arg(apLongName.replace('|', ' ')).arg(apName.replace('|', ' ')), PNL_MAIN, LBL_TITLE);
	}
	else
	{
		textPut("text", bncs_string("  Editing Audio Package: %1").arg("-"), PNL_MAIN, LBL_TITLE);
	}

	//Get Video Link
	bncs_string videoLink;
	routerName(m_devicePackageRouter, DATABASE_AP_VIDEO_LINK, m_AP, videoLink);
	int videoLinkIndex = videoLink.toInt();
	bncs_string videoLinkName;
	routerName(m_deviceRouterVideoHD, DATABASE_SOURCE_NAME, videoLinkIndex, videoLinkName);
	textPut("text", videoLinkName.replace('|', ' '), PNL_MAIN, "video_link_name");

	//Get Audio Pairs
	bncs_string pairs;
	routerName(m_devicePackageRouter, DATABASE_AP_PAIRS, m_AP, pairs);
	refreshAudioPairs(pairs);
	showAudioPairPage(1);

	//Get Audio Returns
	bncs_string returns;
	routerName(m_devicePackageRouter, DATABASE_AP_RETURN, m_AP, returns);
	refreshAudioReturns(returns);

	//lookup language tag for ap
	//db_0063: rev_vision=#,language_tag=21,ifb1=,ifb2=,4w1=#0,4w2=#0
	bncs_string apTags;
	routerName(m_devicePackageRouter, DATABASE_AP_TAGS, m_AP, apTags);
	refreshAPTags(apTags);

	bncs_string apCFLabels;
	routerName(m_devicePackageRouter, DATABASE_AP_CF_LABELS, m_AP, apCFLabels);
	refreshCFLabels(apCFLabels);

	bncs_string apConfLabels;
	routerName(m_devicePackageRouter, DATABASE_AP_CONF_LABELS, m_AP, apConfLabels);
	refreshConfLabels(apConfLabels);

	/*
	<instance composite="yes" id="c_packager" ref="" type="">
		<group id="audio_package_returns" instance="c_packager_audio_package" />
	</instance>

	<instance composite="yes" id="c_packager_audio_package" ref="" type="">
		<group id="packager_audio_package_1" instance="packager_audio_package_returns_main" />
		...
		<group id="packager_audio_package_2" instance="packager_audio_package_returns_2" />
		<group id="packager_audio_package_16" instance="packager_audio_package_returns_16" />
	</instance>

	<instance ref="device=1041,offset=0" id="packager_audio_package_returns_main" alt_id="packager_audio_package_main" type="PackagerAuto" composite="no" />
	<instance ref="device=1042,offset=0" id="packager_audio_package_returns_2" alt_id="packager_audio_package_2" type="PackagerAuto" composite="no" />
	...
	<instance ref="device=1048,offset=0" id="packager_audio_package_returns_8" alt_id="packager_audio_package_8" type="PackagerAuto" composite="no" />
	...
	<instance ref="device=1056,offset=0" id="packager_audio_package_returns_16" alt_id="packager_audio_package_16" type="PackagerAuto" composite="no" />
	*/

	//bncs_string compositePackager = "c_packager";		//instance:"c_packager"
	bncs_string compositePackagerAudioPackageReturns;	//instance:"c_packager_audio_package"
	bncs_string packagerAudioPackageReturnsMain;		//instance:"packager_audio_package_returns_main" - dev_1041
	bncs_string instanceAudioPackageReturns;			//instance:"packager_audio_package_returns_main/2-16" - dev_1041-1056

	//Get main audio package returns device
	m_devicePackageAudioReturns = m_packagerAudioPackageReturns.destDev(m_AP);
	m_slotPackageAudioReturns = m_packagerAudioPackageReturns.destSlot(m_AP);

	//Register for Audio Package Returns slot
	infoRegister(m_devicePackageAudioReturns, m_slotPackageAudioReturns, m_slotPackageAudioReturns);
	infoPoll(m_devicePackageAudioReturns, m_slotPackageAudioReturns, m_slotPackageAudioReturns);

	bncs_string apAudioProcAssignment;
	routerName(m_devicePackageRouter, DATABASE_AP_AUDIO_PROC, m_AP, apAudioProcAssignment);
	refreshAudioProcAssignment(apAudioProcAssignment);

	textPut("audio_package_index", m_AP, PNL_MAIN, "mm_queue");
}

void audio_package_edit_view::refreshAudioProcAssignment(bncs_string fields)
{
	//Get audio proc 1 & 2 and usage on levels 1-12 from database 69
	//2501=proc_1=ldc_proc_001,proc_2=ldc_proc_002,a1=,a2=,a3=,a4=,a5=1.1,a6=1.2,a7=,a8=,a9=,a10=,a11=,a12=
	bncs_stringlist apAudioProcFields(fields);

	m_proc1 = apAudioProcFields.getNamedParam("proc_1");
	m_proc2 = apAudioProcFields.getNamedParam("proc_2");
	bncs_string procName1 = "---";
	bncs_string procName2 = "---";

	MAP_STRING_PROC_ITERATOR it1 = m_mapProcs.find(m_proc1);
	if (it1 != m_mapProcs.end())
	{
		procName1 = it1->second->name;
	}

	MAP_STRING_PROC_ITERATOR it2 = m_mapProcs.find(m_proc2);
	if (it2 != m_mapProcs.end())
	{
		procName2 = it2->second->name;
	}

	textPut("text", procName1, PNL_MAIN, "tally_audio_proc_1");
	textPut("text", procName2, PNL_MAIN, "tally_audio_proc_2");


	//update pairs
	for (int pair = 1; pair <= PAIR_COUNT; pair++)
	{
		bncs_string procOutput = apAudioProcFields.getNamedParam(bncs_string("a%1").arg(pair));

		bncs_stringlist sltProcOutput(procOutput, '.');
		int proc = sltProcOutput[0].toInt();
		int output = sltProcOutput[1].toInt();

		if (proc > 0 && output > 0)
		{
			textPut("audio_proc_usage", bncs_string("Proc %1|Output %2").arg(proc).arg(output), PNL_MAIN, bncs_string("pair_%1").arg(pair, '0', 2));
		}
		else
		{
			textPut("audio_proc_usage", "", PNL_MAIN, bncs_string("pair_%1").arg(pair, '0', 2));
		}
	}




	//TODO Register for Audio Proc routes
}

void audio_package_edit_view::editLevelLabel(bncs_string levelField, int level, bncs_string caption)
{
	debug("audio_package_edit_view::editLevelLabel() levelField=%1 level=%2 title=%3", levelField, level, caption);

	bncs_string currentLabel = "";
	int length = 0;

	if (levelField.startsWith("cf_"))
	{
		CleanFeed* edit = getEditCF(level);

		bncs_string cf;
		bncs_string field;
		levelField.split('.', cf, field);

		if (field == "ifb_label")
		{
			currentLabel = edit->label;
			length = 8;

		}
		else if (field == "ifb_subtitle")
		{
			currentLabel = edit->subtitle;
			length = 16;
		}
	}
	else if (levelField.startsWith("conf_"))
	{
		Conference* edit = getEditConf(level);

		bncs_string conf;
		bncs_string field;
		levelField.split('.', conf, field);

		if (field == "conf_label")
		{
			currentLabel = edit->label;
			length = 8;

		}
		else if (field == "conf_subtitle")
		{
			currentLabel = edit->subtitle;
			length = 16;
		}
	}

	m_contextEditLabel = levelField;
	panelPopup(POPUP_EDIT_LABEL, "popup_edit_label.bncs_ui");
	textPut("setFocus", "setFocus", POPUP_EDIT_LABEL, "keyboard");

	//Set keyboard label length limits
	if (length > 0)
	{
		textPut("numpadValidation", "Length", POPUP_EDIT_LABEL, "keyboard");
		textPut("minLength", "0", POPUP_EDIT_LABEL, "keyboard");
		textPut("maxLength", length, POPUP_EDIT_LABEL, "keyboard");
	}
	else
	{
		textPut("numpadValidation", "", POPUP_EDIT_LABEL, "keyboard");
	}

	textPut("text", currentLabel, POPUP_EDIT_LABEL, "keyboard");
	textPut("text", caption, POPUP_EDIT_LABEL, "grpEditLabel");
}

void audio_package_edit_view::editLabel(bncs_string field)
{
	bncs_string caption = "Edit Label";
	bncs_string currentLabel = "";

	int intLabelLength = 0;

	m_contextEditLabel = field;
	panelPopup(POPUP_EDIT_LABEL, "popup_edit_label.bncs_ui");
	textPut("setFocus", "setFocus", POPUP_EDIT_LABEL, "keyboard");

	if (m_contextEditLabel == FIELD_PACKAGE_NAME)
	{
		currentLabel = m_editAP.name;
		caption = "Edit Button Name";
	}
	else if (m_contextEditLabel == FIELD_PACKAGE_TITLE)
	{
		currentLabel = m_editAP.package_title;
		caption = "Edit Package Name";
	}

	//Set keyboard label length limits
	if (intLabelLength > 0)
	{
		textPut("numpadValidation", "Length", POPUP_EDIT_LABEL, "keyboard");
		textPut("minLength", "0", POPUP_EDIT_LABEL, "keyboard");
		textPut("maxLength", intLabelLength, POPUP_EDIT_LABEL, "keyboard");
	}
	else
	{
		textPut("numpadValidation", "", POPUP_EDIT_LABEL, "keyboard");
	}

	textPut("text", currentLabel, POPUP_EDIT_LABEL, "keyboard");
	textPut("text", caption, POPUP_EDIT_LABEL, "grpEditLabel");

	debug("audio_package_edit_view::editLabel() Field=%1 Caption='%2' Label='%3'", field, caption, currentLabel);
}

void audio_package_edit_view::popoverAudioProc(buttonNotify *b)
{
	bncs_stringlist controlIndex(b->id(), '_');
	int index = controlIndex[1].toInt();

	if (b->id() == BTN_CLOSE)
	{
		panelDestroy(POPOVER_AUDIO_PROC);
	}
	else if (b->id() == "assign")
	{
		//panelShow(POPOVER_SELECT_AUDIO_PROC, "popover_select_audio_proc.bncs_ui");
		//initSelectAudioProc();
	}
	else if (b->id() == "remove_audio_proc")
	{
		removeAudioProc();
		panelDestroy(POPOVER_AUDIO_PROC);
	}
	else if (b->id() == PROC_INPUT_HUB_TAG)
	{
		panelPopup(POPUP_VIDEO_HUB_TAG, "popup_video_hub_tag.bncs_ui");
		bncs_string title = bncs_string(" Set Audio Proc %1 Input Video Hub Tag").arg(m_manageProcNum);
		textPut("text", title, POPUP_VIDEO_HUB_TAG, LBL_TITLE);

		initVideoHubTagPopup(m_manageProcInputDestPackageVideoHubTag);

		//set context for hub tag Proc Input
		m_contextVideoHubTag = PROC_INPUT_HUB_TAG;
	}
}

void audio_package_edit_view::popoverSelectAudioProc(buttonNotify *b)
{
	if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPOVER_SELECT_AUDIO_PROC);
	}
	else if (b->id() == BTN_SELECT)
	{
		panelDestroy(POPOVER_SELECT_AUDIO_PROC);
		insertAudioProc();
	}
	else if (b->id() == "lvw_proc")
	{
		if (b->command() == "tag")
		{
			m_selectedProcId = b->value();

			if (m_selectedProcId == IN_USE)
			{
				textPut("text", "Not Available|  |Audio Proc In Use", POPOVER_SELECT_AUDIO_PROC, "selected_audio_proc");
				controlDisable(POPOVER_SELECT_AUDIO_PROC, BTN_SELECT);
			}
			else
			{
				bncs_string procName = "-";
				MAP_STRING_PROC_ITERATOR it = m_mapProcs.find(m_selectedProcId);
				if (it != m_mapProcs.end())
				{
					CAudioProc* p = it->second;
					procName = p->name;
				}

				textPut("text", bncs_string("Selected Proc:| |%1").arg(procName), POPOVER_SELECT_AUDIO_PROC, "selected_audio_proc");
				controlEnable(POPOVER_SELECT_AUDIO_PROC, BTN_SELECT);
			}
		}
	}
}

void audio_package_edit_view::popoverManageConference(buttonNotify *b)
{
	if (b->id().startsWith("manage_"))
	{
		bncs_stringlist sltManageConf(b->id(), '_');
		int level = sltManageConf[1].toInt();

		if (m_selectedConfManage > 0)
		{
			textPut("statesheet", "enum_deselected", POPOVER_MANAGE_CONFERENCE, bncs_string("manage_%1").arg(m_selectedConfManage));
		}
		m_selectedConfManage = level;
		textPut("statesheet", "enum_selected", POPOVER_MANAGE_CONFERENCE, bncs_string("manage_%1").arg(m_selectedConfManage));

		Conference* diff = getDiffConf(m_selectedConfManage);
		int confNumber = diff->allocated;
		textPut("embedded", confNumber, POPOVER_MANAGE_CONFERENCE, "edit_conference");

		bncs_string name = m_editAP.name;
		textPut("text", bncs_string("  Editing Audio Package:  %1 - Manage Conference Level %2 Members")
			.arg(name.replace('|', ' ')).arg(m_selectedConfManage), POPOVER_MANAGE_CONFERENCE, "title");
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPOVER_MANAGE_CONFERENCE);
		m_manageConferenceView = false;
		m_selectedConfManage = 0;
	}

}

void audio_package_edit_view::popoverSelectAudioSource(buttonNotify *b)
{
	if (b->id() == SOURCE_GRID)
	{
		m_selectedAudioSource = b->value().toInt();
		showSelectedAudioSource();
	}
	else if (b->id() == BTN_SET)
	{
		//TODO - why are these only setting video 1?
		//m_spEdit.video_1.video = m_intSelectedVideoSource;
		//m_spEdit.video_1.ap = m_intSelectedVideoSourceAudio;
		panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
		setEditAudio(m_selectedAudioLevel, m_selectedAudioSource);
		m_selectedAudioLevel = 0;
		showSelectedAudioLevel();
	}
	else if (b->id() == BTN_NONE)
	{
		textPut("index", 0, POPOVER_SELECT_AUDIO_SOURCE, SOURCE_GRID);
		m_selectedAudioSource = 0;
		textPut("text", "None", POPOVER_SELECT_AUDIO_SOURCE, "selected_ap");
		showSelectedAudioSource();

		//Must be able to set an audio pair to none
		controlEnable(POPOVER_SELECT_AUDIO_SOURCE, BTN_SET);
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
		m_selectedAudioLevel = 0;
		showSelectedAudioLevel();
	}

}

void audio_package_edit_view::popoverSelectAudioDest(buttonNotify *b)
{
	if (b->id() == DEST_GRID)
	{
		m_selectedAudioDest = b->value().toInt();
		showSelectedAudioDest();
	}
	else if (b->id() == BTN_SET)
	{
		panelRemove(POPOVER_SELECT_AUDIO_DEST);
		setEditAudioReturn(m_selectedAudioReturnLevel, m_selectedAudioDest);
		m_selectedAudioReturnLevel = 0;
		showSelectedAudioReturnLevel();
	}
	else if (b->id() == BTN_NONE)
	{
		textPut("index", 0, POPOVER_SELECT_AUDIO_DEST, DEST_GRID);
		m_selectedAudioDest = 0;
		textPut("text", "None", POPOVER_SELECT_AUDIO_DEST, "selected_dest");
		showSelectedAudioDest();

		//Must be able to set an audio pair to none
		controlEnable(POPOVER_SELECT_AUDIO_DEST, BTN_SET);
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelRemove(POPOVER_SELECT_AUDIO_DEST);
		m_selectedAudioReturnLevel = 0;
		showSelectedAudioReturnLevel();
	}
}

void audio_package_edit_view::popoverSelectVideoDest(buttonNotify *b)
{
	bool selected = true;
	if (b->id() == DEST_GRID)
	{
		m_selectedVideoDest = b->value().toInt();
		showSelectedVideoDest();
	}
	else if (b->id() == BTN_SET)
	{
		panelRemove(POPOVER_SELECT_VIDEO_DEST);
	
		m_editAP.rev_vision = m_selectedVideoDest;
		
		bncs_string videoReturnName = "---";
		if (m_editAP.rev_vision > 0)
		{
			routerName(m_deviceRouterVideoHD, DATABASE_DEST_NAME, m_editAP.rev_vision, videoReturnName);
			textPut("text", videoReturnName.replace('|', ' '), PNL_MAIN, VIDEO_RETURN_NAME);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, VIDEO_RETURN_NAME);
		}

		if (m_editAP.rev_vision == m_diffAP.rev_vision)
		{
			textPut("statesheet", "level_clean", PNL_MAIN, VIDEO_RETURN_NAME);
		}
		else
		{
			textPut("statesheet", "level_dirty", PNL_MAIN, VIDEO_RETURN_NAME);
		}
		selected = false;

		checkApply();
	}
	else if (b->id() == BTN_NONE)
	{
		textPut("index", 0, POPOVER_SELECT_VIDEO_DEST, SOURCE_GRID);
		m_selectedAudioSource = 0;
		textPut("text", "None", POPOVER_SELECT_VIDEO_DEST, "selected_dest");
		showSelectedVideoDest();

		//Must be able to set an audio pair to none
		controlEnable(POPOVER_SELECT_VIDEO_DEST, BTN_SET);
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelRemove(POPOVER_SELECT_VIDEO_DEST);
		selected = false;
	}

	if (selected)
	{
		textPut("statesheet", "enum_selected", PNL_MAIN, VIDEO_RETURN);
	}
	else
	{
		textPut("statesheet", "enum_deselected", PNL_MAIN, VIDEO_RETURN);
	}
}

void audio_package_edit_view::popupEditLabel(buttonNotify *b)
{
	debug("audio_package_edit_view::popupEditLabel b->id=%1", b->id());
	if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPUP_EDIT_LABEL);
		m_selectedCFLevel = 0;
		showSelectedCFLevel();
	}
	else if (b->id() == "keyboard")
	{
		bncs_string strKeyboard;
		textGet("text", POPUP_EDIT_LABEL, "keyboard", strKeyboard);
		panelDestroy(POPUP_EDIT_LABEL);
		debug("audio_package_edit_view::buttonCallback() field=%1 edit value=%2", m_contextEditLabel, strKeyboard);
		updateLabel(strKeyboard);
	}
	else if (b->id() == "" && b->command() == DISMISS_POPUP)
	{
		//Clear CF level selection
		m_selectedCFLevel = 0;
		showSelectedCFLevel();
	}
}

void audio_package_edit_view::popupIFBRouting(buttonNotify *b)
{
	debug("audio_package_edit_view::popupIFBRouting");
	bool clearLevel = false;

	bncs_stringlist controlIndex(b->id(), '_');
	int index = controlIndex[1].toInt();
	if (b->id().startsWith("ifb_"))
	{
		setEditCFIFBRouting(m_selectedCFLevel, index);
		clearLevel = true;
		panelDestroy(POPUP_IFB_ROUTING);
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPUP_IFB_ROUTING);
		clearLevel = true;
	}
	else if (b->id() == "" && b->command() == DISMISS_POPUP)
	{
		clearLevel = true;
	}

	if (clearLevel)
	{
		if (m_selectedCFLevel > 0)
		{
			//Clear CF level selection
			m_selectedCFLevel = 0;
			showSelectedCFLevel();
		}
	}
}

void audio_package_edit_view::popupSelectCommsPort(buttonNotify *b)
{
	bool clearLevel = false;

	debug("audio_package_edit_view::popupSelectCommsPort");
	if (b->id() == "grid" && b->command() == "index")
	{
		if (b->value().contains('.'))
		{
			bncs_string ring;
			bncs_string port;
			b->value().split('.', ring, port);

			m_selectedCommsRing = ring.toInt();
			m_selectedCommsPort = port.toInt();

			textPut("text", bncs_string("Ring: %1|Port: %2").arg(m_selectedCommsRing).arg(m_selectedCommsPort), POPUP_SELECT_COMMS_PORT, "selected_port");
		}
		else
		{
			m_selectedCommsRing = 0;
			m_selectedCommsPort = 0;
			textPut("text", "---", POPUP_SELECT_COMMS_PORT, "selected_port");
		}
	}
	else if (b->id() == BTN_NONE)
	{
		textPut("deselect", POPUP_SELECT_COMMS_PORT, "grid");
		m_selectedCommsRing = 0;
		m_selectedCommsPort = 0;
		textPut("text", "---", POPUP_SELECT_COMMS_PORT, "selected_port");
	}
	else if (b->id() == BTN_SET)
	{
		panelDestroy(POPUP_SELECT_COMMS_PORT);


		if (m_selectedCFLevel > 0)
		{
			setEditCFCircuit(m_selectedCFLevel, m_selectedCommsRing, m_selectedCommsPort, "both");	//in & out
		}
		else if (m_selectedConfLevel > 0)
		{
			setEditConfMainPort(m_selectedConfLevel, m_selectedCommsRing, m_selectedCommsPort, "both");	//in & out
		}
		clearLevel = true;
	}
	else if (b->id() == BTN_SET_IN)
	{
		panelDestroy(POPUP_SELECT_COMMS_PORT);

		if (m_selectedCFLevel > 0)
		{
			setEditCFCircuit(m_selectedCFLevel, m_selectedCommsRing, m_selectedCommsPort, "in");		//in only
		}
		else if (m_selectedConfLevel > 0)
		{
			setEditConfMainPort(m_selectedConfLevel, m_selectedCommsRing, m_selectedCommsPort, "in");	//in only
		}
		clearLevel = true;
	}
	else if (b->id() == BTN_SET_OUT)
	{
		panelDestroy(POPUP_SELECT_COMMS_PORT);
		if (m_selectedCFLevel > 0)
		{
			setEditCFCircuit(m_selectedCFLevel, m_selectedCommsRing, m_selectedCommsPort, "out");	//out only
		}
		else if (m_selectedConfLevel > 0)
		{
			setEditConfMainPort(m_selectedConfLevel, m_selectedCommsRing, m_selectedCommsPort, "out");	//out only
		}
		clearLevel = true;
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPUP_SELECT_COMMS_PORT);
		clearLevel = true;
	}
	else if (b->id() == "" && b->command() == DISMISS_POPUP)
	{
		clearLevel = true;
	}

	if (clearLevel)
	{
		if (m_selectedCFLevel > 0)
		{
			//Clear CF level selection
			m_selectedCFLevel = 0;
			showSelectedCFLevel();
		}
		if (m_selectedConfLevel > 0)
		{
			//Clear Conference level selection
			m_selectedConfLevel = 0;
		}
	}
}

void audio_package_edit_view::popupAudioTypeTag(buttonNotify *b)
{
	b->dump("audio_package_edit_view::popupAudioTypeTag");
	if (b->id() == "lvw_tags")
	{
		if (b->command() == "cell_selection")
		{
			bncs_string selectedName = b->value();
			textPut("text", selectedName, POPUP_AUDIO_TYPE_TAG, "selected_name");
		}
		else if (b->command() == "tag")
		{
			m_selectedTypeTag = b->value().toInt();
			textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedTypeTag), POPUP_AUDIO_TYPE_TAG, "selected_tag");
		}
	}
	else if (b->id() == BTN_SET)
	{
		panelDestroy(POPUP_AUDIO_TYPE_TAG);
		setEditAudioTypeTag(m_selectedTypeTag);
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPUP_AUDIO_TYPE_TAG);
	}
}

void audio_package_edit_view::popupVideoHubTag(buttonNotify *b)
{
	b->dump("audio_package_edit_view::popupVideoHubTag");
	if (b->id() == "lvw_tags")
	{
		if (b->command() == "cell_selection")
		{
			bncs_string selectedName = b->value();
			textPut("text", selectedName, POPUP_VIDEO_HUB_TAG, "selected_name");
			//[lvw_tags: cell_selection.0.0=LDC Main
		}
		else if (b->command() == "tag")
		{
			m_selectedVideoTag = b->value().toInt();
			textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedVideoTag), POPUP_VIDEO_HUB_TAG, "selected_tag");

			textPut("text", m_selectedVideoTag, PNL_MAIN, VIDEO_RETURN_HUB_TAG);

		}
	}
	else if (b->id() == "select")
	{
		panelDestroy(POPUP_VIDEO_HUB_TAG);
		
		if (m_contextVideoHubTag == VIDEO_RETURN_HUB_TAG)
		{
			m_editAP.rev_vision_hub_tag = m_selectedVideoTag;

			if (m_editAP.rev_vision_hub_tag > 0)
			{
				bncs_string hubTagStatus;
				routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, m_editAP.rev_vision_hub_tag, hubTagStatus);

				bncs_stringlist sltHubNameStatus(hubTagStatus, '|');
				bncs_string hubName = sltHubNameStatus[0];

				textPut("text", hubName, PNL_MAIN, VIDEO_RETURN_HUB_TAG);
			}
			else
			{
				textPut("text", "---", PNL_MAIN, VIDEO_RETURN_HUB_TAG);
			}

			if (m_editAP.rev_vision_hub_tag == m_diffAP.rev_vision_hub_tag)
			{
				textPut("statesheet", "field_unmodified", PNL_MAIN, VIDEO_RETURN_HUB_TAG);
			}
			else
			{
				textPut("statesheet", "field_modified", PNL_MAIN, VIDEO_RETURN_HUB_TAG);
			}

			checkApply();
		}
		else if (m_contextVideoHubTag == PROC_INPUT_HUB_TAG)
		{
			//set proc input dest package hub tag
			setProcInputDestPackageVideoHubTag(m_selectedVideoTag);
		}
		
	}
	else if (b->id() == "cancel")
	{
		panelDestroy(POPUP_VIDEO_HUB_TAG);
	}
}

void audio_package_edit_view::updateLabel(bncs_string label)
{
	debug("audio_package_edit_view::updateLabel() label=%1 cfLevel=%2 confLevel=%3", label, m_selectedCFLevel, m_selectedConfLevel);
	if (m_contextEditLabel.startsWith("cf_") && m_selectedCFLevel > 0)
	{
		CleanFeed* edit = getEditCF(m_selectedCFLevel);

		bncs_string cf;
		bncs_string field;
		m_contextEditLabel.split('.', cf, field);
		
		if (field == IFB_LABEL)
		{
			edit->label = label;
		}
		else if (field == IFB_SUBTITLE)
		{
			edit->subtitle = label;
		}
		
		debug("audio_package_edit_view::updateLabel() send to %1 edit_data[%2=%3]", cf, field, label);
		textPut("edit_data", bncs_string("%1=%2").arg(field).arg(label), PNL_MAIN, cf);

		m_selectedCFLevel = 0;
		showSelectedCFLevel();
	}
	else if (m_contextEditLabel.startsWith("conf_") && m_selectedConfLevel > 0)
	{
		Conference* edit = getEditConf(m_selectedConfLevel);

		bncs_string conf;
		bncs_string field;
		m_contextEditLabel.split('.', conf, field);

		if (field == CONF_LABEL)
		{
			edit->label = label;
		}
		else if (field == CONF_SUBTITLE)
		{
			edit->subtitle = label;
		}

		debug("audio_package_edit_view::updateLabel() send to %1 edit_data[%2=%3]", conf, field, label);
		textPut("edit_data", bncs_string("%1=%2").arg(field).arg(label), PNL_MAIN, conf);
		m_selectedConfLevel = 0;
	}
	else
	{
		if (m_contextEditLabel == FIELD_PACKAGE_NAME)
		{
			m_editAP.name = label;
		}
		else if (m_contextEditLabel == FIELD_PACKAGE_TITLE)
		{
			m_editAP.package_title = label;
		}
		textPut("text", "  " + label, PNL_MAIN, m_contextEditLabel);
		textPut("statesheet", "field_modified", PNL_MAIN, m_contextEditLabel);
	}

	m_contextEditLabel = "";

	checkApply();
}

void audio_package_edit_view::initEditor()
{
	//getDev(INSTANCE_PACKAGE_ROUTER, &m_devicePackageRouter);

	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);
	m_packagerAudioPackageReturns = bncs_packager(m_compositePackager, "audio_package_returns");

	//Get main router device
	m_devicePackageRouter = m_packagerRouter.destDev(1);
	debug("audio_package_edit_view::initEditor() m_devicePackageRouter=%1", m_devicePackageRouter);

	//Get main router instance
	m_instancePackagerRouterMain = m_packagerRouter.instance(1);

	//Register for database changes
	infoRegister(m_devicePackageRouter, 1, 1);

	/* packager_config.xml
	<packager_config>
	<data id="packager_auto">
	<setting id="audio_router" value="instance=vip_router_audio_2ch_01" />
	<setting id="video_router_hd" value="instance=vip_router_video_hd_01" />
	*/

	//Get audio router device
	m_instanceRouterAudio = m_packagerRouter.getConfigItem("audio_router");

	debug("audio_package_edit_view::initEditor() m_instanceRouterAudio=%1", m_instanceRouterAudio);

	//Check if we have been passed a composite instance.
	if (instanceLookupComposite(m_instanceRouterAudio, "section_01", m_instanceRouterAudio_section_01))
	{
		getDev(m_instanceRouterAudio_section_01, &m_deviceRouterAudio);
		debug("audio_package_edit_view::initEditor() m_instanceRouterAudio_section_01=%1 device=%2", m_instanceRouterAudio_section_01, m_deviceRouterAudio);
	}
	else
	{
		debug("audio_package_edit_view::initEditor() m_instanceRouterAudio_section_01 NOT FOUND");
	}

	//Get video router device
	m_instanceRouterVideoHD = m_packagerRouter.getConfigItem("video_router_hd");

	//Check if we have been passed a composite instance.
	if (instanceLookupComposite(m_instanceRouterVideoHD, "section_01", m_instanceRouterVideoHD_section_01))
	{
		getDev(m_instanceRouterVideoHD_section_01, &m_deviceRouterVideoHD);
		debug("audio_package_edit_view::initEditor() m_instanceRouterVideoHD_section_01=%1 device=%2", m_instanceRouterVideoHD_section_01, m_deviceRouterVideoHD);
	}
	else
	{
		debug("audio_package_edit_view::initEditor() m_instanceRouterVideoHD_section_01 NOT FOUND");
	}

	loadAudioProcs();
}

void audio_package_edit_view::initAudioTypeTagPopup(int level, int typeTag, bncs_string title)
{
	textPut("text", title, POPUP_AUDIO_TYPE_TAG, "title");

	textPut("add", bncs_string("%1;#TAG=%2").arg("[NONE]").arg(0), POPUP_AUDIO_TYPE_TAG, "lvw_tags");
	for (int tag = 1; tag <= 500; tag++)
	{
		bncs_string tagValueStatus;
		routerName(m_devicePackageRouter, DATABASE_TYPE_TAG, tag, tagValueStatus);

		bncs_string tagValue;
		bncs_string tagStatus;
		tagValueStatus.split('|', tagValue, tagStatus);

		int status = tagStatus.toInt();
		if (status == 1)
		{
			textPut("add", bncs_string("%1;#TAG=%2").arg(tagValue).arg(tag), POPUP_AUDIO_TYPE_TAG, "lvw_tags");
		}
	}

	textPut("selectTag", typeTag, POPUP_AUDIO_TYPE_TAG, "lvw_tags");

}

void audio_package_edit_view::initVideoHubTagPopup(int videoHubTag)
{

	textPut("add", bncs_string("%1;#TAG=%2").arg("[NONE]").arg(0), POPUP_VIDEO_HUB_TAG, "lvw_tags");
	for (int tag = 1; tag <= 30; tag++)
	{
		bncs_string tagValueStatus;
		routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, tag, tagValueStatus);

		bncs_string tagValue;
		bncs_string tagStatus;
		tagValueStatus.split('|', tagValue, tagStatus);

		int status = tagStatus.toInt();
		if (status == 1)
		{
			textPut("add", bncs_string("%1;#TAG=%2").arg(tagValue).arg(tag), POPUP_VIDEO_HUB_TAG, "lvw_tags");
		}
	}

	textPut("selectTag", videoHubTag, POPUP_VIDEO_HUB_TAG, "lvw_tags");
}

void audio_package_edit_view::refreshName(bncs_string newName)
{
	m_diffAP.name = newName;
	m_editAP.name = newName;
	textPut("text", "  " + newName.replace("|", "|  "), PNL_MAIN, FIELD_PACKAGE_NAME);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_PACKAGE_NAME);
	checkApply();
}

void audio_package_edit_view::refreshTitle(bncs_string newTitle)
{
	m_diffAP.package_title = newTitle;
	m_editAP.package_title = newTitle;
	textPut("text", "  " + newTitle, PNL_MAIN, FIELD_PACKAGE_TITLE);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_PACKAGE_TITLE);
	checkApply();
}

void audio_package_edit_view::refreshAudioPairs(bncs_string newLevels)
{
	debug("audio_package_edit_view::refreshAudioPairs strNewLevels=%1", newLevels);

	//Note there are 12 pairs in the specification but there are 16 pairs in the configuration
	//31801=a1=12081#21,a2=12082#22,a3=12083#24,a4=12084#25,a5=0#,a6=0#,a7=0#,a8=0#,a9=#,a10=#,a11=#,a12=#,a13=#,a14=#,a15=#,a16=#

	bncs_stringlist sltAudioPairs(newLevels);

	//update pairs
	for (int pair = 1; pair <= PAIR_COUNT; pair++)
	{
		bncs_string pairTag = sltAudioPairs.getNamedParam(bncs_string("a%1").arg(pair));
		
		bncs_stringlist sltIndexTypeTag(pairTag, '#');
		int index = sltIndexTypeTag[0].toInt();
		int typeTag = sltIndexTypeTag[1].toInt();

		bncs_string diffData = bncs_string("index=%1,%2=%3").arg(index).arg(TYPE_TAG).arg(typeTag);
		bncs_string editData = bncs_string("index=%1,%2=%3").arg(index).arg(TYPE_TAG).arg(typeTag);

		textPut("diff_data", diffData, PNL_MAIN, bncs_string("pair_%1").arg(pair, '0', 2));
		textPut("edit_data", editData, PNL_MAIN, bncs_string("pair_%1").arg(pair, '0', 2));

		m_diffAP.index[pair - 1] = index;
		m_editAP.index[pair - 1] = index;
		m_diffAP.typeTag[pair - 1] = typeTag;
		m_editAP.typeTag[pair - 1] = typeTag;

	}

	checkApply();

}

void audio_package_edit_view::refreshAudioReturns(bncs_string newLevels)
{
	debug("audio_package_edit_view::refreshAudioReturns strNewLevels=%1", newLevels);

	//Database 65

	//31801=a1=12345#41:291,a2=12345#51:327,a3=12345#56:345,a4=12345#106:524

	bncs_stringlist sltAudioReturns(newLevels);

	for (int level = 1; level <= AR_COUNT; level++)
	{
		bncs_string arIndexTags = sltAudioReturns.getNamedParam(bncs_string("a%1").arg(level));

		bncs_string destIndex;
		bncs_string languageType;
		bncs_string language;
		bncs_string type;
		arIndexTags.split('#', destIndex, languageType);
		languageType.split(':', language, type);

		m_diffAP.arIndex[level - 1] = destIndex.toInt();
		m_diffAP.arLanguageTag[level - 1] = language.toInt();
		m_diffAP.arTypeTag[level - 1] = type.toInt();

		m_editAP.arIndex[level - 1] = destIndex.toInt();
		m_editAP.arLanguageTag[level - 1] = language.toInt();
		m_editAP.arTypeTag[level - 1] = type.toInt();

		//Query - what is apTag? is this valid or is the same as video hubTag?
		bncs_string diffData = bncs_string("index=%1,language_tag=%2,type_tag=%3")
			.arg(m_diffAP.arIndex[level - 1])
			.arg(m_diffAP.arLanguageTag[level - 1])
			.arg(m_diffAP.arTypeTag[level - 1]);

		bncs_string editData = bncs_string("index=%1,language_tag=%2,type_tag=%3")
			.arg(m_editAP.arIndex[level - 1])
			.arg(m_editAP.arLanguageTag[level - 1])
			.arg(m_editAP.arTypeTag[level - 1]);

		textPut("diff_data", diffData, PNL_MAIN, bncs_string("ar_%1").arg(level));
		textPut("edit_data", editData, PNL_MAIN, bncs_string("ar_%1").arg(level));
	}

	checkApply();
}

void audio_package_edit_view::refreshAPTags(bncs_string newTags)
{
	//lookup language tag for ap
	//db_0063: rev_vision=#,language_tag=21,ifb1=,ifb2=,4w1=#0,4w2=#0
	// ifb1=ring.port/ring.port
	// 4w1=ring.port/ring.port
	bncs_stringlist sltAPTags(newTags);

	//AP Language Tag
	int languageTag = sltAPTags.getNamedParam("language_tag").toInt();
	m_diffAP.language_tag = languageTag;
	m_editAP.language_tag = languageTag;

	bncs_string languageNameStatus;
	routerName(m_devicePackageRouter, DATABASE_LANG_TAG, languageTag, languageNameStatus);

	bncs_stringlist sltNameStatus(languageNameStatus, '|');
	bncs_string languageName = sltNameStatus[0];

	textPut("text", languageName, PNL_MAIN, BTN_LANGUAGE);
	textPut("statesheet", "level_clean", PNL_MAIN, BTN_LANGUAGE);

	//AP Reverse Vision
	bncs_string revVision = sltAPTags.getNamedParam("rev_vision");

	bncs_string videoReturnIndex;
	bncs_string videoReturnHubTag;
	revVision.split('#', videoReturnIndex, videoReturnHubTag);
	m_diffAP.rev_vision = videoReturnIndex.toInt();
	m_editAP.rev_vision = videoReturnIndex.toInt();
	m_diffAP.rev_vision_hub_tag = videoReturnHubTag.toInt();
	m_editAP.rev_vision_hub_tag = videoReturnHubTag.toInt();

	bncs_string videoReturnName = "---";
	if (revVision > 0)
	{
		routerName(m_deviceRouterVideoHD, DATABASE_DEST_NAME, m_diffAP.rev_vision, videoReturnName);
	}
	textPut("text", videoReturnName.replace('|', ' '), PNL_MAIN, VIDEO_RETURN_NAME);
	textPut("statesheet", "level_clean", PNL_MAIN, VIDEO_RETURN_NAME);

	if (m_diffAP.rev_vision_hub_tag > 0)
	{
		bncs_string hubTagStatus;
		routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, m_diffAP.rev_vision_hub_tag, hubTagStatus);


		bncs_stringlist sltHubNameStatus(hubTagStatus, '|');
		bncs_string hubName = sltHubNameStatus[0];

		textPut("text", hubName, PNL_MAIN, VIDEO_RETURN_HUB_TAG);
	}
	else
	{
		textPut("text", "---", PNL_MAIN, VIDEO_RETURN_HUB_TAG);
	}
	textPut("statesheet", "field_unmodified", PNL_MAIN, VIDEO_RETURN_HUB_TAG);

	//Clean Feed Levels
	for (int level = 1; level <= CF_COUNT; level++)
	{
		CleanFeed* diff = getDiffCF(level);
		CleanFeed* edit = getEditCF(level);

		bncs_string cfCircuit = sltAPTags.getNamedParam(bncs_string("cf%1").arg(level));

		bncs_string in;
		bncs_string out;
		cfCircuit.split('/', in, out);

		bncs_string inRing;
		bncs_string inPort;
		in.split('.', inRing, inPort);
		diff->inRing = inRing.toInt();
		diff->inPort = inPort.toInt();
		edit->inRing = inRing.toInt();
		edit->inPort = inPort.toInt();

		bncs_string outRing;
		bncs_string outPort;
		out.split('.', outRing, outPort);
		diff->outRing = outRing.toInt();
		diff->outPort = outPort.toInt();
		edit->outRing = outRing.toInt();
		edit->outPort = outPort.toInt();

		bncs_string cfIFBRouting = sltAPTags.getNamedParam(bncs_string("cf%1_ifb").arg(level));
		diff->routing = cfIFBRouting.toInt();
		edit->routing = cfIFBRouting.toInt();

		//Query - what is apTag? is this valid or is the same as video hubTag?
		bncs_string diffData = bncs_string("in_ring=%1,in_port=%2,out_ring=%3,out_port=%4,ifb_routing=%5")
			.arg(diff->inRing)
			.arg(diff->inPort)
			.arg(diff->outRing)
			.arg(diff->outPort)
			.arg(diff->routing);

		bncs_string editData = bncs_string("in_ring=%1,in_port=%2,out_ring=%3,out_port=%4,ifb_routing=%5")
			.arg(edit->inRing)
			.arg(edit->inPort)
			.arg(edit->outRing)
			.arg(edit->outPort)
			.arg(edit->routing);

		textPut("diff_data", diffData, PNL_MAIN, bncs_string("cf_%1").arg(level));
		textPut("edit_data", editData, PNL_MAIN, bncs_string("cf_%1").arg(level));
	}

	for (int level = 1; level <= CONF_COUNT; level++)
	{
		Conference* diff = getDiffConf(level);
		Conference* edit = getEditConf(level);

		bncs_string mainPort = sltAPTags.getNamedParam(bncs_string("conf%1").arg(level));

		bncs_string in;
		bncs_string out;
		mainPort.split('/', in, out);

		bncs_string inRing;
		bncs_string inPort;
		in.split('.', inRing, inPort);
		diff->inRing = inRing.toInt();
		diff->inPort = inPort.toInt();
		edit->inRing = inRing.toInt();
		edit->inPort = inPort.toInt();

		bncs_string outRing;
		bncs_string outPort;
		out.split('.', outRing, outPort);
		diff->outRing = outRing.toInt();
		diff->outPort = outPort.toInt();
		edit->outRing = outRing.toInt();
		edit->outPort = outPort.toInt();

		bncs_string diffData = bncs_string("in_ring=%1,in_port=%2,out_ring=%3,out_port=%4")
			.arg(diff->inRing)
			.arg(diff->inPort)
			.arg(diff->outRing)
			.arg(diff->outPort);

		bncs_string editData = bncs_string("in_ring=%1,in_port=%2,out_ring=%3,out_port=%4")
			.arg(edit->inRing)
			.arg(edit->inPort)
			.arg(edit->outRing)
			.arg(edit->outPort);

		if (m_manageConferenceView)
		{
			//update the conference main port on the manage conference view
			textPut("edit_data", diffData, POPOVER_MANAGE_CONFERENCE, bncs_string("conf_%1").arg(level));
			textPut("edit_data", editData, POPOVER_MANAGE_CONFERENCE, bncs_string("conf_%1").arg(level));
		}

		textPut("diff_data", diffData, PNL_MAIN, bncs_string("conf_%1").arg(level));
		textPut("edit_data", editData, PNL_MAIN, bncs_string("conf_%1").arg(level));
	}

	checkApply();
}

void audio_package_edit_view::refreshCFLabels(bncs_string newFields)
{
	//db_0067: l1=label-cf1,l2=label-cf2,l3=label-cf3,l4=label-cf4,
	//         s1=label-cf1,s2=label-cf2,s3=label-cf3,s4=label-cf4
	bncs_stringlist sltFields(newFields);

	for (int level = 1; level <= CF_COUNT; level++)
	{

		bncs_string label = sltFields.getNamedParam(bncs_string("label_%1").arg(level));
		bncs_string subtitle = sltFields.getNamedParam(bncs_string("sub_%1").arg(level));

		CleanFeed* diff = getDiffCF(level);
		CleanFeed* edit = getEditCF(level);

		diff->label = label;
		edit->label = label;

		diff->subtitle = subtitle;
		edit->subtitle = subtitle;

		bncs_string diffData = bncs_string("ifb_label=%1,ifb_subtitle=%2").arg(diff->label).arg(diff->subtitle);
		bncs_string editData = bncs_string("ifb_label=%1,ifb_subtitle=%2").arg(edit->label).arg(edit->subtitle);

		textPut("diff_data", diffData, PNL_MAIN, bncs_string("cf_%1").arg(level));
		textPut("edit_data", editData, PNL_MAIN, bncs_string("cf_%1").arg(level));

		//debug("audio_package_edit_view::refreshCFLabels level=%1 data=%2", level, diffData);
	}

	checkApply();
}

void audio_package_edit_view::refreshConfLabels(bncs_string newFields)
{
	//db_0068: label_conf1=label-conf1,label_conf2=label-conf2,sub_conf1=subtitle-conf1,sub_conf2=subtitle-conf2
	bncs_stringlist sltFields(newFields);

	for (int level = 1; level <= 2; level++)
	{
		Conference* diff;
		Conference* edit;
		if (level == 1)
		{
			diff = &m_diffAP.conf_1;
			edit = &m_editAP.conf_1;
		}
		else if (level == 2)
		{
			diff = &m_diffAP.conf_2;
			edit = &m_editAP.conf_2;
		}

		bncs_string label = sltFields.getNamedParam(bncs_string("label_%1").arg(level));
		bncs_string subtitle = sltFields.getNamedParam(bncs_string("sub_%1").arg(level));

		diff->label = label;
		edit->label = label;

		diff->subtitle = subtitle;
		edit->subtitle = subtitle;

		bncs_string diffData = bncs_string("conf_label=%1,conf_subtitle=%2").arg(diff->label).arg(diff->subtitle);
		bncs_string editData = bncs_string("conf_label=%1,conf_subtitle=%2").arg(edit->label).arg(edit->subtitle);

		textPut("diff_data", diffData, PNL_MAIN, bncs_string("conf_%1").arg(level));
		textPut("edit_data", editData, PNL_MAIN, bncs_string("conf_%1").arg(level));
	}

	checkApply();
}

void audio_package_edit_view::showAudioPairPage(int page)
{
	if (m_audioPairPage != page)
	{
		//highlight buttons
		textPut("statesheet", "enum_deselected", PNL_MAIN, bncs_string("pair-page_%1").arg(m_audioPairPage));
	}
	m_audioPairPage = page;
	textPut("statesheet", "enum_selected", PNL_MAIN, bncs_string("pair-page_%1").arg(m_audioPairPage));

	//first hide all
	for (int l = 1; l <= PAIR_COUNT; l++)
	{

		controlHide(PNL_MAIN, bncs_string("pair_%1").arg(l, '0', 2));
	}

	//show range of 6 for current page
	int offset = (m_audioPairPage - 1) * 6;
	for (int pair = offset + 1; pair <= offset + 6; pair++)
	{
		controlShow(PNL_MAIN, bncs_string("pair_%1").arg(pair, '0', 2));
	}
	debug("audio_package_edit_view::showAudioPairPage() page=%1 offset=%2", m_audioPairPage, offset);
}

void audio_package_edit_view::showSelectedAudioLevel()
{
	bncs_stringlist sltPairs = getIdList(PNL_MAIN, "pair_");
	for (int level = 0; level < sltPairs.count(); level++)
	{
		textPut("select_level", m_selectedAudioLevel, PNL_MAIN, sltPairs[level]);
	}
}

void audio_package_edit_view::showSelectedAudioReturnLevel()
{
	bncs_stringlist sltAudioReturns = getIdList(PNL_MAIN, "ar_");
	for (int level = 0; level < sltAudioReturns.count(); level++)
	{
		textPut("select_level", m_selectedAudioReturnLevel, PNL_MAIN, sltAudioReturns[level]);
	}
}

void audio_package_edit_view::showSelectedCFLevel()
{
	bncs_stringlist sltCF = getIdList(PNL_MAIN, "cf_");
	for (int level = 0; level < sltCF.count(); level++)
	{
		textPut("select_level", m_selectedCFLevel, PNL_MAIN, sltCF[level]);
	}
}

void audio_package_edit_view::showSelectedAudioSource()
{
	if (m_selectedAudioSource > 0)
	{
		bncs_string selectedSourceName;
		routerName(m_deviceRouterAudio, DATABASE_SOURCE_NAME, m_selectedAudioSource, selectedSourceName);

		textPut("text", bncs_string("Audio: %1| |%2").arg(m_selectedAudioSource).arg(selectedSourceName), POPOVER_SELECT_AUDIO_SOURCE, "selected_source");

		controlEnable(POPOVER_SELECT_AUDIO_SOURCE, BTN_SET);
	}
	else
	{
		textPut("text", bncs_string("Audio: None|Selected"), POPOVER_SELECT_AUDIO_SOURCE, "selected_source");
	}
}

void audio_package_edit_view::showSelectedAudioDest()
{
	if (m_selectedAudioDest > 0)
	{
		bncs_string selectedDestName;
		routerName(m_deviceRouterAudio, DATABASE_DEST_NAME, m_selectedAudioDest, selectedDestName);

		textPut("text", bncs_string("Audio: %1| |%2").arg(m_selectedAudioDest).arg(selectedDestName), POPOVER_SELECT_AUDIO_DEST, "selected_dest");

		controlEnable(POPOVER_SELECT_AUDIO_DEST, BTN_SET);
	}
	else
	{
		textPut("text", bncs_string("Audio: None|Selected"), POPOVER_SELECT_AUDIO_DEST, "selected_dest");
	}
}

void audio_package_edit_view::showSelectedVideoDest()
{
	if (m_selectedVideoDest > 0)
	{
		bncs_string selectedDestName;
		routerName(m_deviceRouterVideoHD, DATABASE_DEST_NAME, m_selectedVideoDest, selectedDestName);

		textPut("text", bncs_string("Video: %1| |%2").arg(m_selectedVideoDest).arg(selectedDestName), POPOVER_SELECT_VIDEO_DEST, "selected_dest");

		controlEnable(POPOVER_SELECT_VIDEO_DEST, BTN_SET);
	}
	else
	{
		textPut("text", bncs_string("Video: None|Selected"), POPOVER_SELECT_VIDEO_DEST, "selected_dest");
	}
}

void audio_package_edit_view::setEditAudio(int level, int source)
{
	m_editAP.index[level - 1] = source;
	//m_editIndex[level - 1] = source;

	bncs_string editData = bncs_string("index=%1").arg(source);

	debug("audio_package_edit_view::setEditAudio editData=%1", editData);

	textPut("edit_data", editData, PNL_MAIN, bncs_string("pair_%1").arg(level, '0', 2));

	checkApply();
}

void audio_package_edit_view::setEditAudioReturn(int level, int dest)
{
	m_editAP.arIndex[level - 1] = dest;

	bncs_string editData = bncs_string("index=%1").arg(dest);

	debug("audio_package_edit_view::setEditAudioReturn editData=%1", editData);

	textPut("edit_data", editData, PNL_MAIN, bncs_string("ar_%1").arg(level));

	checkApply();
}

void audio_package_edit_view::setEditAudioTypeTag(int typeTag)
{
	debug("audio_package_edit_view::setEditTypeTag typeTag=%21 context=%2",  typeTag, m_contextAudioTypeTag);

	if (m_contextAudioTypeTag.startsWith("pair_") && m_selectedAudioLevel > 0)
	{
		m_editAP.typeTag[m_selectedAudioLevel - 1] = typeTag;

		bncs_string editData = bncs_string("%1=%2").arg(TYPE_TAG).arg(typeTag);
		debug("source_package_editor::setEditTypeTag editData=%1", editData);
		textPut("edit_data", editData, PNL_MAIN, bncs_string("pair_%1").arg(m_selectedAudioLevel, '0', 2));
	}
	
	if (m_contextAudioTypeTag.startsWith("ar_"))
	{
		m_editAP.arTypeTag[m_selectedAudioReturnLevel - 1] = typeTag;

		bncs_string editData = bncs_string("%1=%2").arg(TYPE_TAG).arg(typeTag);
		debug("source_package_editor::setEditTypeTag editData=%1", editData);
		textPut("edit_data", editData, PNL_MAIN, bncs_string("ar_%1").arg(m_selectedAudioReturnLevel));
	}

	checkApply();
}

void audio_package_edit_view::setEditCFCircuit(int level, int ring, int port, bncs_string inOut)
{
	debug("audio_package_edit_view::setEditCFCircuit() level=%1 ring=%2 port=%3 inOut=%4", level, ring, port, inOut);

	CleanFeed* edit = getEditCF(level);

	bncs_string editData;

	if (inOut == "in")
	{
		edit->inRing = ring;
		edit->inPort = port;

		editData = bncs_string("in_ring=%1,in_port=%2")
			.arg(edit->inRing)
			.arg(edit->inPort);
	}
	else if (inOut == "out")
	{
		edit->outRing = ring;
		edit->outPort = port;

		editData = bncs_string("out_ring=%1,out_port=%2")
			.arg(edit->outRing)
			.arg(edit->outPort);
	}
	else
	{
		edit->inRing = ring;
		edit->inPort = port;
		edit->outRing = ring;
		edit->outPort = port;

		editData = bncs_string("in_ring=%1,in_port=%2,out_ring=%3,out_port=%4")
			.arg(edit->inRing)
			.arg(edit->inPort)
			.arg(edit->outRing)
			.arg(edit->outPort);
	}


	textPut("edit_data", editData, PNL_MAIN, bncs_string("cf_%1").arg(level));

	debug("audio_package_edit_view::setEditCFCircuit() edit_data=%1", editData);

	checkApply();
}

void audio_package_edit_view::setEditCFIFBRouting(int level, int ifb)
{
	getEditCF(level)->routing = ifb;

	bncs_string editData = bncs_string("%1=%2").arg(IFB_ROUTING).arg(ifb);
	debug("audio_package_edit_view::setEditCFIFBRouting level=%1 editData: %2", level, editData);
	textPut("edit_data", editData, PNL_MAIN, bncs_string("cf_%1").arg(level));

	checkApply();
}

void audio_package_edit_view::setEditConfMainPort(int level, int ring, int port, bncs_string inOut)
{
	debug("audio_package_edit_view::setEditConfMainPort() level=%1 ring=%2 port=%3 inOut=%4", level, ring, port, inOut);

	Conference* edit = getEditConf(level);

	bncs_string editData;

	if (inOut == "in")
	{
		edit->inRing = ring;
		edit->inPort = port;

		editData = bncs_string("in_ring=%1,in_port=%2")
			.arg(edit->inRing)
			.arg(edit->inPort);
	}
	else if (inOut == "out")
	{
		edit->outRing = ring;
		edit->outPort = port;

		editData = bncs_string("out_ring=%1,out_port=%2")
			.arg(edit->outRing)
			.arg(edit->outPort);
	}
	else
	{
		edit->inRing = ring;
		edit->inPort = port;
		edit->outRing = ring;
		edit->outPort = port;

		editData = bncs_string("in_ring=%1,in_port=%2,out_ring=%3,out_port=%4")
			.arg(edit->inRing)
			.arg(edit->inPort)
			.arg(edit->outRing)
			.arg(edit->outPort);
	}


	textPut("edit_data", editData, PNL_MAIN, bncs_string("conf_%1").arg(level));

	debug("audio_package_edit_view::setEditConfMainPort() edit_data=%1", editData);

	checkApply();
}

void audio_package_edit_view::setEditAudioLanguageTag(int languageTag)
{
	debug("audio_package_edit_view::setEditAudioLanguageTag languageTag=%1 context=%2", languageTag, m_contextLanguageTag);
	if (m_contextLanguageTag == BTN_LANGUAGE)
	{
		//Audio Package Language tag 
		m_editAP.language_tag = m_selectedLanguageTag;

		bncs_string languageNameStatus;
		routerName(m_devicePackageRouter, DATABASE_LANG_TAG, languageTag, languageNameStatus);

		bncs_stringlist sltNameStatus(languageNameStatus, '|');
		bncs_string languageName = sltNameStatus[0];

		if (languageTag > 0)
		{
			textPut("text", languageName, PNL_MAIN, BTN_LANGUAGE);
		}
		else
		{
			textPut("text", "---", PNL_MAIN, BTN_LANGUAGE);
		}

		if (m_editAP.language_tag != m_diffAP.language_tag)
		{
			textPut("statesheet", "level_dirty", PNL_MAIN, BTN_LANGUAGE);
		}
		else
		{
			textPut("statesheet", "level_clean", PNL_MAIN, BTN_LANGUAGE);
		}
	}
	else if (m_contextLanguageTag.startsWith("ar_") && m_selectedAudioReturnLevel > 0)
	{
		bncs_string ar;
		bncs_string field;
		m_contextLanguageTag.split('.', ar, field);

		m_editAP.arLanguageTag[m_selectedAudioReturnLevel - 1] = m_selectedLanguageTag;

		bncs_string editData = bncs_string("%1=%2").arg(LANGUAGE_TAG).arg(m_selectedLanguageTag);
		textPut("edit_data", editData, PNL_MAIN, bncs_string("ar_%1").arg(m_selectedAudioReturnLevel));
	}

	checkApply();
}

void audio_package_edit_view::setConfEnable(int level, int confNumber)
{
	if (confNumber > 0)
	{
		//Currently enabled - send FREE
		infoWrite(m_devicePackageAudioReturns, bncs_string("FREE=conf%1").arg(level), m_slotPackageAudioReturns);
	}
	else
	{
		//Currently disabled - send GET
		infoWrite(m_devicePackageAudioReturns, bncs_string("GET=conf%1").arg(level), m_slotPackageAudioReturns);
	}
}

void audio_package_edit_view::setCFConfEnable(int level, int confNumber)
{
	if (confNumber > 0)
	{
		//Currently enabled - send FREE
		infoWrite(m_devicePackageAudioReturns, bncs_string("FREE=cf%1_conf").arg(level), m_slotPackageAudioReturns);
	}
	else
	{
		//Currently disabled - send GET
		infoWrite(m_devicePackageAudioReturns, bncs_string("GET=cf%1_conf").arg(level), m_slotPackageAudioReturns);
	}
}

void audio_package_edit_view::updateAudioPackageReturnSlot(bncs_string fields)
{
	debug("audio_package_edit_view::updateAudioPackageReturnSlot fields=%1", fields);

	bncs_stringlist sltFields(fields);

	for (int cf = 1; cf <= CF_COUNT; cf++)
	{
		bncs_string ifb = sltFields.getNamedParam(bncs_string("cf%1_ifb").arg(cf));
		bncs_string conf = sltFields.getNamedParam(bncs_string("cf%1_conf").arg(cf));

		bncs_string ifbRing;
		bncs_string ifbNumber;
		ifb.split('.', ifbRing, ifbNumber);

		bncs_string allocation = bncs_string("ifb_ring=%1,ifb_number=%2,conf_number=%3").arg(ifbRing).arg(ifbNumber).arg(conf);
		textPut("allocation_data", allocation, PNL_MAIN, bncs_string("cf_%1").arg(cf));	

		if (cf == 0)
		{
			textPut("text", bncs_string(" [%1] CF%2 allocation_data: %3").arg(fields).arg(cf).arg(allocation), PNL_MAIN, "title");
		}
	}

	bool manageConferences = false;
	for (int conf = 1; conf <= CONF_COUNT; conf++)
	{
		bncs_string confNumber = sltFields.getNamedParam(bncs_string("conf%1").arg(conf));

		//Store allocated conference in both diff and edit
		Conference* diff = getDiffConf(conf);
		diff->allocated = confNumber;
		Conference* edit = getEditConf(conf);
		edit->allocated = confNumber;

		if (confNumber > 0)
		{
			manageConferences = true;
		}

		bncs_string allocation = bncs_string("conf_number=%1").arg(confNumber);
		textPut("allocation_data", allocation, PNL_MAIN, bncs_string("conf_%1").arg(conf));

		if (conf == 0)
		{
			textPut("text", bncs_string(" [%1] Conf%2 allocation_data: %3").arg(fields).arg(conf).arg(allocation), PNL_MAIN, "title");
		}

		if (m_manageConferenceView)
		{
			//update the conference main port on the manage conference view
			if (confNumber > 0)
			{
				textPut("text", bncs_string("CNF|%1").arg(confNumber), POPOVER_MANAGE_CONFERENCE, bncs_string("allocated_conf_%1").arg(conf));
				controlEnable(POPOVER_MANAGE_CONFERENCE, bncs_string("manage_%1").arg(conf));
			}
			else
			{
				textPut("text", "---", POPOVER_MANAGE_CONFERENCE, bncs_string("allocated_conf_%1").arg(conf));
				textPut("statesheet", "enum_deselected", POPOVER_MANAGE_CONFERENCE, bncs_string("manage_%1").arg(conf));
				controlDisable(POPOVER_MANAGE_CONFERENCE, bncs_string("manage_%1").arg(conf));

				//Check if the currently managed conference level has just been de-allocated
				if (m_selectedConfManage == conf)
				{
					textPut("embedded", "0", POPOVER_MANAGE_CONFERENCE, "edit_conference");
					//deselect the conf level - can be reselected if conference is reallocated
					m_selectedConfManage = 0;

					bncs_string name = m_editAP.name;
					textPut("text", bncs_string("  Editing Audio Package: %1 - Manage Conference Level Members")
						.arg(name.replace('|', ' ')), POPOVER_MANAGE_CONFERENCE, "title");
				}
			}
		}
	}

	if (manageConferences == true)
	{
		controlEnable(PNL_MAIN, MANAGE_CONF_MEMBERS);
	}
	else
	{
		controlDisable(PNL_MAIN, MANAGE_CONF_MEMBERS);
	}
}

void audio_package_edit_view::updateProcInputDestPackageVideoHubTag()
{
	bncs_string strDestLevels;
	//12001=video=0#3,anc1=0#3,anc2=0#3,anc3=0#3,anc4=0#3,rev_vision=0#3,ifb1=0.0,ifb2=0.0
	routerName(m_devicePackageRouter, DATABASE_DEST_LEVELS, m_manageProcInputDestPackageIndex, strDestLevels);
	bncs_stringlist sltDestLevels(strDestLevels);
	bncs_string destVideoLevelIndexTag = sltDestLevels.getNamedParam("video");
	bncs_string destVideoIndex;
	bncs_string destVideoHubTag;
	destVideoLevelIndexTag.split('#', destVideoIndex, destVideoHubTag);
	m_manageProcInputDestPackageVideoHubTag = destVideoHubTag.toInt();

	//Update label - only needed if audio proc view is active
	bncs_string destPackageVideoHubTagName = "---";
	if (m_manageProcInputDestPackageVideoHubTag > 0)
	{
		bncs_string destPackageVideoHubTag;
		routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, m_manageProcInputDestPackageVideoHubTag, destPackageVideoHubTag);
		bncs_string destPackageVideoHubTagFlag;
		destPackageVideoHubTag.split('|', destPackageVideoHubTagName, destPackageVideoHubTagFlag);
	}
	textPut("text", destPackageVideoHubTagName, POPOVER_AUDIO_PROC, PROC_INPUT_HUB_TAG);
}

void audio_package_edit_view::setProcInputDestPackageVideoHubTag(int newTag)
{
	bncs_string strDestLevels;
	//12001=video=0#3,anc1=0#3,anc2=0#3,anc3=0#3,anc4=0#3,rev_vision=0#3,ifb1=0.0,ifb2=0.0
	routerName(m_devicePackageRouter, DATABASE_DEST_LEVELS, m_manageProcInputDestPackageIndex, strDestLevels);
	bncs_stringlist sltDestLevels(strDestLevels);

	bool updateVideoHubTag = false;

	for (int field = 0; field < sltDestLevels.count(); field++)
	{
		bncs_string key;
		bncs_string value;
		bncs_string keyValue = sltDestLevels[field];
		keyValue.split('=', key, value);

		//clear proc_x field if x == m_manageProcNum
		if (key == "video")
		{
			bncs_string destVideoIndex;
			bncs_string destVideoHubTag;
			value.split('#', destVideoIndex, destVideoHubTag);
			sltDestLevels[field] = bncs_string("video=%1#%2").arg(destVideoIndex).arg(newTag);
			updateVideoHubTag = true;
		}
	}

	if (updateVideoHubTag)
	{
		debug("audio_package_edit_view::setProcInputDestPackageVideoHubTag() newTag: %1", newTag);
		routerModify(m_devicePackageRouter, DATABASE_DEST_LEVELS, m_manageProcInputDestPackageIndex, sltDestLevels.toString(), false);
	}
}

void audio_package_edit_view::showUse()
{
	textPut("clear", PNL_MAIN, LVW_CURRENT_USE);
	controlDisable(PNL_MAIN, BTN_SHOW_USE);

	//For each source package
	//	for each AP level
	//		is this AP used?
	m_sourcePackageCount = getRouterSize(m_devicePackageRouter, DATABASE_SOURCE_NAME);

	textPut("text", bncs_string("Checking %1 Source Packages").arg(m_sourcePackageCount), PNL_MAIN, LBL_USAGE_STATUS);

	m_sourcePackageUseNext = 1;
	timerStart(TIMER_USAGE_NEXT, TIMEOUT_USAGE_NEXT);
}

void audio_package_edit_view::showUseNext()
{
	int block = 100;

	for (int index = m_sourcePackageUseNext; index < m_sourcePackageUseNext + block; index++)
	{
		//get levels
		bncs_string sourcePackageAPLevels;
		routerName(m_devicePackageRouter, DATABASE_SOURCE_AP, index, sourcePackageAPLevels);
		bncs_stringlist sltApLevels(sourcePackageAPLevels, '|');

		bncs_stringlist usedOnLevels;

		for (int level = 1; level <= 32; level++)
		{
			int ap = sltApLevels[level - 1].toInt();
			if (ap == m_AP)
			{
				usedOnLevels.append(level);
			}
		}

		if (usedOnLevels.count() > 0)
		{
			//get name
			bncs_string sourcePackageName;
			routerName(m_devicePackageRouter, DATABASE_SOURCE_NAME, index, sourcePackageName);
			textPut("add", bncs_string("%1;%2;%3").arg(index).arg(sourcePackageName.replace('|', ' ')).arg(usedOnLevels.toString('/')), PNL_MAIN, LVW_CURRENT_USE);
		}
	}
	m_sourcePackageUseNext += block;

	if (m_sourcePackageUseNext >= m_sourcePackageCount)
	{
		timerStop(TIMER_USAGE_NEXT);

		bncs_string count;
		textGet("count", PNL_MAIN, LVW_CURRENT_USE, count);

		textPut("text", bncs_string("AP used in %1 Source Packages").arg(count), PNL_MAIN, LBL_USAGE_STATUS);
		controlEnable(PNL_MAIN, BTN_SHOW_USE);
	}
}

CleanFeed* audio_package_edit_view::getDiffCF(int level)
{
	CleanFeed* cf = 0;
	if (level == 1)
	{
		cf = &m_diffAP.cf_1;
	}
	else if (level == 2)
	{
		cf = &m_diffAP.cf_2;
	}
	else if (level == 3)
	{
		cf = &m_diffAP.cf_3;
	}
	else if (level == 4)
	{
		cf = &m_diffAP.cf_4;
	}
	return cf;
}

CleanFeed* audio_package_edit_view::getEditCF(int level)
{
	CleanFeed* cf = 0;
	if (level == 1)
	{
		cf = &m_editAP.cf_1;
	}
	else if (level == 2)
	{
		cf = &m_editAP.cf_2;
	}
	else if (level == 3)
	{
		cf = &m_editAP.cf_3;
	}
	else if (level == 4)
	{
		cf = &m_editAP.cf_4;
	}
	return cf;
}

Conference* audio_package_edit_view::getDiffConf(int level)
{
	Conference* conf = 0;
	if (level == 1)
	{
		conf = &m_diffAP.conf_1;
	}
	else if (level == 2)
	{
		conf = &m_diffAP.conf_2;
	}
	return conf;
}

Conference* audio_package_edit_view::getEditConf(int level)
{
	Conference* conf = 0;
	if (level == 1)
	{
		conf = &m_editAP.conf_1;
	}
	else if (level == 2)
	{
		conf = &m_editAP.conf_2;
	}
	return conf;
}

void audio_package_edit_view::initManageConfMembers()
{
	panelShow(POPOVER_MANAGE_CONFERENCE, "popover_manage_conference.bncs_ui");

	m_manageConferenceView = true;
	m_selectedConfManage = 0;

	bool targetEmbedded = false;
	for (int level = 1; level <= CONF_COUNT; level++)
	{
		Conference* diff = getDiffConf(level);
		Conference* edit = getEditConf(level);

		bncs_string diffData = bncs_string("in_ring=%1,in_port=%2,out_ring=%3,out_port=%4")
			.arg(diff->inRing)
			.arg(diff->inPort)
			.arg(diff->outRing)
			.arg(diff->outPort);

		bncs_string editData = bncs_string("in_ring=%1,in_port=%2,out_ring=%3,out_port=%4")
			.arg(edit->inRing)
			.arg(edit->inPort)
			.arg(edit->outRing)
			.arg(edit->outPort);

		//Note: send both diff+edit to the conference component to show the Main Port with dirty statesheet if not applied yet
		textPut("diff_data", diffData, POPOVER_MANAGE_CONFERENCE, bncs_string("conf_%1").arg(level));
		textPut("edit_data", editData, POPOVER_MANAGE_CONFERENCE, bncs_string("conf_%1").arg(level));

		int confNumber = diff->allocated;
		if (confNumber > 0)
		{
			textPut("text", bncs_string("CNF|%1").arg(confNumber), POPOVER_MANAGE_CONFERENCE, bncs_string("allocated_conf_%1").arg(level));
			controlEnable(POPOVER_MANAGE_CONFERENCE, bncs_string("manage_%1").arg(level));

			//Enable Conf 1 (or Conf 2 if Conf 1 not allocated)
			if (targetEmbedded == false)
			{
				textPut("embedded", confNumber, POPOVER_MANAGE_CONFERENCE, "edit_conference");
				targetEmbedded = true;

				m_selectedConfManage = level;
				textPut("statesheet", "enum_selected", POPOVER_MANAGE_CONFERENCE, bncs_string("manage_%1").arg(m_selectedConfManage));

				bncs_string name = m_editAP.name;
				textPut("text", bncs_string("  Editing Audio Package: %1 - Manage Conference Level %2 Members")
					.arg(name.replace('|', ' ')).arg(m_selectedConfManage), POPOVER_MANAGE_CONFERENCE, "title");
			}
		}
		else
		{
			textPut("text", "---", POPOVER_MANAGE_CONFERENCE, bncs_string("allocated_conf_%1").arg(level));
			controlDisable(POPOVER_MANAGE_CONFERENCE, bncs_string("manage_%1").arg(level));
		}
	}
}

void audio_package_edit_view::initAudioProc(int proc)
{
	panelShow(POPOVER_AUDIO_PROC, "popover_audio_proc.bncs_ui");

	m_manageProcNum = proc;
	if (m_manageProcNum == 1)
	{
		m_manageProcId = m_proc1;
	}
	else if (m_manageProcNum == 2)
	{
		m_manageProcId = m_proc2;
	}

	bncs_string spLevelName = m_spLevel;
	bncs_stringlist sltLevelName(spLevelName, '_');
	if (sltLevelName[0] == "v")
	{
		spLevelName = bncs_string("Video %1").arg(sltLevelName[1]);
	}
	else if (sltLevelName[0] == "a")
	{
		spLevelName = bncs_string("AP %1").arg(sltLevelName[1]);
	}

	bncs_string languageNameStatus;
	routerName(m_devicePackageRouter, DATABASE_LANG_TAG, m_editAP.language_tag, languageNameStatus);

	bncs_stringlist sltNameStatus(languageNameStatus, '|');
	bncs_string languageName = sltNameStatus[0];

	bncs_string spName;
	routerName(m_devicePackageRouter, DATABASE_SOURCE_NAME, m_sourcePackageIndex, spName);
	bncs_string title = bncs_string("  Manage Audio Processor %1 of Audio Package %2 [%3] with language %4 on Level %5 of Source Package %6 [%7]")
		.arg(m_manageProcNum)			//%1
		.arg(m_editAP.name)				//%2
		.arg(m_AP)						//%3
		.arg(languageName)				//%4
		.arg(spLevelName)				//%5
		.arg(spName.replace('|', ' '))	//%6
		.arg(m_sourcePackageIndex);		//%7
	textPut("text", title.replace('|', ' '), POPOVER_AUDIO_PROC, "title");

	MAP_STRING_PROC_ITERATOR it = m_mapProcs.find(m_manageProcId);
	if (it != m_mapProcs.end())
	{
		CAudioProc* p = it->second;
		controlDisable(POPOVER_AUDIO_PROC, "assign");

		//TODO enable remove proc button
	}
	else
	{
		//No Audio Proc currently assigned
		controlEnable(POPOVER_AUDIO_PROC, "assign");
	}

	textPut("text", bncs_string("Assigned Audio Proc %1").arg(m_manageProcNum), POPOVER_AUDIO_PROC, "assigned");

	showAudioProcLabels();

}

void audio_package_edit_view::showAudioProcLabels()
{

	MAP_STRING_PROC_ITERATOR it = m_mapProcs.find(m_manageProcId);
	if (it != m_mapProcs.end() )
	{
		CAudioProc* p = it->second;

		textPut("text", p->name, POPOVER_AUDIO_PROC, "assigned_proc");
		textPut("text", p->description, POPOVER_AUDIO_PROC, "proc_description");

		m_manageProcInputDestPackageIndex = p->destPackage;

		//Target the dest_package component to show the input dest name and current source package
		textPut("instance", m_instancePackagerRouterMain, POPOVER_AUDIO_PROC, PROC_INPUT_DEST_PACKAGE);
		textPut("index", m_manageProcInputDestPackageIndex, POPOVER_AUDIO_PROC, PROC_INPUT_DEST_PACKAGE);

		updateProcInputDestPackageVideoHubTag();

		//Get the name of the destination package
		bncs_string destPackageName;
		routerName(m_devicePackageRouter, DATABASE_DEST_NAME, m_manageProcInputDestPackageIndex, destPackageName);
		//textPut("text", destPackageName, POPOVER_AUDIO_PROC, PROC_INPUT_DEST_PACKAGE);

		bncs_string destPackageAudioLevels;
		routerName(m_devicePackageRouter, DATABASE_DEST_AUDIO, m_manageProcInputDestPackageIndex, destPackageAudioLevels);

		bncs_stringlist audioDestList(destPackageAudioLevels);

		//show inputs
		for (int input = 1; input <= 5; input++)
		{
			bncs_string audioDestName = "---";
			bncs_string inputDescription = "";
			int audioDest = 0;
			if (p->destPackageLevelIn[input] > 0)
			{
				//get audio dest index for dest package level
				audioDest = audioDestList[p->destPackageLevelIn[input] - 1];
				routerName(m_deviceRouterAudio, DATABASE_DEST_NAME, audioDest, audioDestName);
				inputDescription = p->labelIn[input];
			}

			//Initialise Input Components
			textPut("instance_packager_router_main", m_instancePackagerRouterMain, POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
			textPut("source_package_index", m_sourcePackageIndex, POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
			textPut("dest_package_index", m_manageProcInputDestPackageIndex, POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
			textPut("dest_package_level", p->destPackageLevelIn[input], POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
			textPut("instance_router_audio", m_instanceRouterAudio_section_01, POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
			textPut("audio_dest_index", audioDest, POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
			textPut("input_description", inputDescription, POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
			textPut("input_language_default", p->audioLanguageIn[input], POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
			textPut("input_type_default", p->audioTypeIn[input], POPOVER_AUDIO_PROC, bncs_string("input_%1").arg(input));
		}

		//show outputs
		for (int output = 1; output <= 5; output++)
		{
			int audioSource = 0;
			bncs_string audioSourceName = "---";
			bncs_string outputDescription = "";
			if (p->audioIndexOut[output] > 0)
			{
				audioSource = p->audioIndexOut[output];
				routerName(m_deviceRouterAudio, DATABASE_SOURCE_NAME, audioSource, audioSourceName);
				outputDescription = p->labelOut[output];
			}

			//Initialise output Components
			textPut("instance_packager_router_main", m_instancePackagerRouterMain, POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
			textPut("instance_router_audio", m_instanceRouterAudio_section_01, POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
			textPut("source_package_index", m_sourcePackageIndex, POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
			textPut("audio_package_index", m_AP, POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
			textPut("audio_source_index", audioSource, POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
			textPut("output_description", outputDescription, POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
			textPut("output_type_default", p->audioTypeOut[output], POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
			//TODO get pair from db0069 usage
			//textPut("audio_package_pair", output, POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
			textPut("audio_package_proc", m_manageProcNum, POPOVER_AUDIO_PROC, bncs_string("output_%1").arg(output));
		}
	}
	else
	{
		textPut("text", "", POPOVER_AUDIO_PROC, "assigned_proc");
		textPut("text", "", POPOVER_AUDIO_PROC, "proc_description");

		m_manageProcInputDestPackageIndex = 0;
		textPut("index", m_manageProcInputDestPackageIndex, POPOVER_AUDIO_PROC, PROC_INPUT_DEST_PACKAGE);
		textPut("text", "---", POPOVER_AUDIO_PROC, PROC_INPUT_HUB_TAG);
	}
}

void audio_package_edit_view::initSelectAudioProc(int proc)
{
	textPut("text", bncs_string("  Audio Processor %1 - Select Device").arg(proc), POPOVER_SELECT_AUDIO_PROC, "title");
	controlDisable(POPOVER_SELECT_AUDIO_PROC, BTN_SELECT);

	m_manageProcNum = proc;

	for (MAP_STRING_PROC_ITERATOR it = m_mapProcs.begin(); it != m_mapProcs.end(); it++)
	{
		CAudioProc* p = it->second;
		bncs_string id = it->first;

		int procInputDestPackageIndex = p->destPackage;

		//Get the name of the destination package
		bncs_string destPackageName;
		routerName(m_devicePackageRouter, DATABASE_DEST_NAME, procInputDestPackageIndex, destPackageName);

		//Get the usage of the audio proc
		bncs_string inputDestPackageUsage;
		routerName(m_devicePackageRouter, DATABASE_DEST_PACKAGE_AUDIO_PROC_USAGE, procInputDestPackageIndex, inputDestPackageUsage);

		bncs_stringlist sltUsage(inputDestPackageUsage);
		int apIndex = sltUsage.getNamedParam("ap").toInt();
		int apProcNum = sltUsage.getNamedParam("proc").toInt();
		bncs_string apName = "-";
		bncs_string tag = id;
		bncs_string usage = "-";
		if (apIndex > 0)
		{
			routerName(m_devicePackageRouter, DATABASE_AP_NAME, apIndex, apName);
			tag = IN_USE;
			usage = bncs_string("%1: Proc %2").arg(apName.replace('|', ' ')).arg(apProcNum);
		}

		textPut("add", bncs_string("%1  ;%2  ;%3  ;%4 ;%5 [%6] ;%7 ;#TAG=%8")
			.arg(id)
			.arg(p->name)
			.arg(p->description)
			.arg(p->category)
			.arg(destPackageName.replace('|', ' '))
			.arg(procInputDestPackageIndex)
			.arg(usage)
			.arg(tag),
			POPOVER_SELECT_AUDIO_PROC, "lvw_proc");
	}
}

void audio_package_edit_view::loadAudioProcs()
{
	bncs_config audioProcs("audio_procs");
	if (audioProcs.isValid())
	{
		while (audioProcs.isChildValid())
		{
			/*
			<audio_proc_devices>
				<audio_proc id="ldc_proc_001">
					<setting id="name" value="LDC Split 01" />
					<setting id="packager_instance" value="c_packager_ldc" />
					<setting id="description" value="L+R to L+L and R+R" />
					<setting id="category" value="Shuffle" />
					<setting id="dest_package" value="23001" />
					<setting id="label_in_1" value="L+R Stream in" />
					<setting id="label_in_2" value="" />
					<setting id="label_in_3" value="" />
					<setting id="label_in_4" value="" />
					<setting id="label_in_5" value="" />
					<setting id="dest_package_level_in_1" value="1" />
					<setting id="dest_package_level_in_2" value="0" />
					<setting id="dest_package_level_in_3" value="0" />
					<setting id="dest_package_level_in_4" value="0" />
					<setting id="dest_package_level_in_5" value="0" />
			*/

			bncs_string id = audioProcs.childAttr("id");
			bncs_config proc(audioProcs);
			proc.drillDown();

			CAudioProc *p = new CAudioProc();

			bncs_string instance;

			while (proc.isChildValid())
			{
				bncs_string settingId = proc.childAttr("id");
				bncs_string settingValue = proc.childAttr("value");

				if (settingId == "name") p->name = settingValue;
				else if (settingId == "packager_instance") p->packagerInstance = settingValue;
				else if (settingId == "description") p->description = settingValue;
				else if (settingId == "category") p->category = settingValue;
				else if (settingId == "dest_package") p->destPackage = settingValue.toInt();
				else if (settingId == "label_in_1") p->labelIn[1] = settingValue;
				else if (settingId == "label_in_2") p->labelIn[2] = settingValue;
				else if (settingId == "label_in_3") p->labelIn[3] = settingValue;
				else if (settingId == "label_in_4") p->labelIn[4] = settingValue;
				else if (settingId == "label_in_5") p->labelIn[5] = settingValue;
				else if (settingId == "dest_package_level_in_1") p->destPackageLevelIn[1] = settingValue.toInt();
				else if (settingId == "dest_package_level_in_2") p->destPackageLevelIn[2] = settingValue.toInt();
				else if (settingId == "dest_package_level_in_3") p->destPackageLevelIn[3] = settingValue.toInt();
				else if (settingId == "dest_package_level_in_4") p->destPackageLevelIn[4] = settingValue.toInt();
				else if (settingId == "dest_package_level_in_5") p->destPackageLevelIn[5] = settingValue.toInt();
				else if (settingId == "audiolanguage_in_1") p->audioLanguageIn[1] = settingValue.toInt();
				else if (settingId == "audiolanguage_in_2") p->audioLanguageIn[2] = settingValue.toInt();
				else if (settingId == "audiolanguage_in_3") p->audioLanguageIn[3] = settingValue.toInt();
				else if (settingId == "audiolanguage_in_4") p->audioLanguageIn[4] = settingValue.toInt();
				else if (settingId == "audiolanguage_in_5") p->audioLanguageIn[5] = settingValue.toInt();
				else if (settingId == "audio_type_in_1") p->audioTypeIn[1] = settingValue.toInt();
				else if (settingId == "audio_type_in_2") p->audioTypeIn[2] = settingValue.toInt();
				else if (settingId == "audio_type_in_3") p->audioTypeIn[3] = settingValue.toInt();
				else if (settingId == "audio_type_in_4") p->audioTypeIn[4] = settingValue.toInt();
				else if (settingId == "audio_type_in_5") p->audioTypeIn[5] = settingValue.toInt();
				else if (settingId == "label_out_1") p->labelOut[1] = settingValue;
				else if (settingId == "label_out_2") p->labelOut[2] = settingValue;
				else if (settingId == "label_out_3") p->labelOut[3] = settingValue;
				else if (settingId == "label_out_4") p->labelOut[4] = settingValue;
				else if (settingId == "label_out_5") p->labelOut[5] = settingValue;
				else if (settingId == "audio_index_out_1") p->audioIndexOut[1] = settingValue.toInt();
				else if (settingId == "audio_index_out_2") p->audioIndexOut[2] = settingValue.toInt();
				else if (settingId == "audio_index_out_3") p->audioIndexOut[3] = settingValue.toInt();
				else if (settingId == "audio_index_out_4") p->audioIndexOut[4] = settingValue.toInt();
				else if (settingId == "audio_index_out_5") p->audioIndexOut[5] = settingValue.toInt();
				else if (settingId == "audio_type_out_1") p->audioTypeOut[1] = settingValue.toInt();
				else if (settingId == "audio_type_out_2") p->audioTypeOut[2] = settingValue.toInt();
				else if (settingId == "audio_type_out_3") p->audioTypeOut[3] = settingValue.toInt();
				else if (settingId == "audio_type_out_4") p->audioTypeOut[4] = settingValue.toInt();
				else if (settingId == "audio_type_out_5") p->audioTypeOut[5] = settingValue.toInt();
				proc.nextChild();
			}

			//textPut("add", bncs_string("%1;%2;%3;%4").arg(id).arg(name).arg(description).arg(category), POPOVER_SELECT_AUDIO_PROC, "lvw_proc");
			if (p->packagerInstance == m_compositePackager)
			{
				m_mapProcs.insert(make_pair(id, p));
			}

			audioProcs.nextChild();
		}
	}
}

void audio_package_edit_view::insertAudioProc()
{
	bool applyPairs = false;

	//First find free pairs available in AP
	bncs_stringlist freePairs;
	bncs_stringlist sltPairs;
	for (int pair = 0; pair < PAIR_COUNT; pair++)
	{
		if(m_editAP.index[pair] == 0)
		{
			freePairs.append(pair + 1);
		}
	}

	debug("audio_package_edit_view::insertAudioProc() freePairs=%1", freePairs.toString());

	//Get Selected Proc Details
	int procInputDestPackageIndex = 0;
	int procOutputCount = 0;
	MAP_STRING_PROC_ITERATOR it = m_mapProcs.find(m_selectedProcId);
	if (it != m_mapProcs.end())
	{
		CAudioProc* p = it->second;
		debug("audio_package_edit_view::insertAudioProc() m_selectedProcId=%1 name=%2", m_selectedProcId, p->name);

		//textPut("text", p->name, POPOVER_AUDIO_PROC, "assigned_proc");
		//textPut("text", p->description, POPOVER_AUDIO_PROC, "proc_description");
		procInputDestPackageIndex = p->destPackage;

		debug("audio_package_edit_view::insertAudioProc() procInputDestPackageIndex=%1", procInputDestPackageIndex);

		//Note there may not be enough free pairs - count the number for this proc

		//First pass - count pairs required for proc outputs
		for (int output = 1; output <= 5; output++)
		{
			if (p->audioIndexOut[output] > 0)
			{
				procOutputCount++;
			}
		}
		debug("audio_package_edit_view::insertAudioProc() procOutputCount=%1", procOutputCount);

		//check if enough AP pairs are free
		if (procOutputCount <= freePairs.count())
		{
			// Get current AP proc assignment database - db0069
			// 31811=proc_1=ldc_proc_001,proc_2=ldc_proc_011,a1=,a2=,a3=,a4=,a5=,a6=,a7=,a8=,a9=1.1,a10=1.2,a11=2.1,a12=2.2
			bncs_string apAudioProcDatabase;
			routerName(m_devicePackageRouter, DATABASE_AP_AUDIO_PROC, m_AP, apAudioProcDatabase);
			
			bncs_stringlist apAudioProcFields(apAudioProcDatabase);

			//handle blank entry
			if (apAudioProcFields.count() == 0)
			{
				apAudioProcFields.append("proc_1=");
				apAudioProcFields.append("proc_2=");
				apAudioProcFields.append("a1=");
				apAudioProcFields.append("a2=");
				apAudioProcFields.append("a3=");
				apAudioProcFields.append("a4=");
				apAudioProcFields.append("a5=");
				apAudioProcFields.append("a6=");
				apAudioProcFields.append("a7=");
				apAudioProcFields.append("a8=");
				apAudioProcFields.append("a9=");
				apAudioProcFields.append("a10=");
				apAudioProcFields.append("a11=");
				apAudioProcFields.append("a12=");
				debug("audio_package_edit_view::insertAudioProc() initialise apAudioProcFields: %1", apAudioProcFields.toString());
			}

			bool updateProcAssignment = false;
			int usedPairs = 0;
			
			//Second pass update pairs with proc output indexes
			for (int output = 1; output <= 5; output++)
			{
				int audioIndexOut = p->audioIndexOut[output];
				int audioTypeOut = p->audioTypeOut[output];
				bncs_string audioSourceName = "---";
				if (audioIndexOut > 0)
				{
					//get next free AP pair
					int insertPair = freePairs[usedPairs].toInt();
					debug("audio_package_edit_view::insertAudioProc() setting index=%1 type=%2 on insertPair=%3", audioIndexOut, audioTypeOut, insertPair);
					m_editAP.index[insertPair - 1] = audioIndexOut;
					m_editAP.typeTag[insertPair - 1] = audioTypeOut;
					applyPairs = true;

					//update pair in the proc assign fields
					for (int field = 0; field < apAudioProcFields.count(); field++)
					{
						bncs_string key;
						bncs_string value;
						bncs_string keyValue = apAudioProcFields[field];
						keyValue.split('=', key, value);

						//Update AP proc assignment database field for insertPair
						if (key.startsWith("a"))
						{
							int pair = key.mid(1).toInt();
							if (pair == insertPair)
							{
								//set field aX=Y.Z  where X=insertPair, Y=m_manageProcNum, Z=output
								apAudioProcFields[field] = bncs_string("%1=%2.%3").arg(key).arg(m_manageProcNum).arg(output);
								debug("audio_package_edit_view::insertAudioProc() set field %1", apAudioProcFields[field]);
								break;
							}
						}
					}
					usedPairs++;
				}
			}

			//modify proc_1/2 field
			for (int field = 0; field < apAudioProcFields.count(); field++)
			{
				bncs_string key;
				bncs_string value;
				bncs_string keyValue = apAudioProcFields[field];
				keyValue.split('=', key, value);

				//clear proc_x field if x == m_manageProcNum
				if (key == bncs_string("proc_%1").arg(m_manageProcNum))
				{
					apAudioProcFields[field] = bncs_string("proc_%1=%2").arg(m_manageProcNum).arg(m_selectedProcId);
					updateProcAssignment = true;
				}
			}

			if (updateProcAssignment)
			{
				debug("audio_package_edit_view::insertAudioProc() updateProcAssignment fields=%1", apAudioProcFields.toString());
				routerModify(m_devicePackageRouter, DATABASE_AP_AUDIO_PROC, m_AP, apAudioProcFields.toString(), false);
			}

			//Apply changes to Audio Package -
			if (applyPairs)
			{
				applyChangesAudioPairs();
			}

			//Update Audio Proc Input DP usage database - db0013
			bncs_string usage = bncs_string("sp=%1,ap=%2,proc=%3").arg(m_sourcePackageIndex).arg(m_AP).arg(m_manageProcNum);
			routerModify(m_devicePackageRouter, DATABASE_DEST_PACKAGE_AUDIO_PROC_USAGE, procInputDestPackageIndex, usage, false);

			//Route Source Package to Audio Proc Input Dest Package
			int deviceInputDestPackage = m_packagerRouter.destDev(procInputDestPackageIndex);
			int slotInputDestPackage = m_packagerRouter.destSlot(procInputDestPackageIndex);
			infoWrite(deviceInputDestPackage, bncs_string("index=%1,pri=1").arg(m_sourcePackageIndex), slotInputDestPackage);
		}
		else
		{
			//Not enough free pairs
			//TODO popup error
		}
	}
}

void audio_package_edit_view::removeAudioProc()
{
	//Find levels in AP that carry outputs of m_manageProcNum
	// Look in Audio Proc AP assignment database - db0069
	// 31811=proc_1=ldc_proc_001,proc_2=ldc_proc_011,a1=,a2=,a3=,a4=,a5=,a6=,a7=,a8=,a9=1.1,a10=1.2,a11=2.1,a12=2.2
	// Clear audio source index and type tag

	bncs_string apAudioProcDatabase;
	routerName(m_devicePackageRouter, DATABASE_AP_AUDIO_PROC, m_AP, apAudioProcDatabase);
	bncs_stringlist apAudioProcFields(apAudioProcDatabase);

	bool apply = false;
	for (int pair = 1; pair <= PAIR_COUNT; pair++)
	{
		bncs_string usage = apAudioProcFields.getNamedParam(bncs_string("a%1").arg(pair));
		bncs_string splitProc;
		bncs_string splitOutput;
		usage.split('.', splitProc, splitOutput);
		int proc = splitProc.toInt();
		int output = splitOutput.toInt();
		if (proc == m_manageProcNum)
		{
			debug("audio_package_edit_view::removeAudioProc() remove proc %1 output %2 from pair %3", m_manageProcNum, output, pair);
			m_editAP.index[pair - 1] = 0;
			m_editAP.typeTag[pair - 1] = 0;
			apply = true;
		}
	}

	if (apply)
	{
		debug("audio_package_edit_view::removeAudioProc() apply changes to AP %1", m_AP);
		applyChangesAudioPairs();
	}

	//Send dest select to Audio Proc Input Dest Package component
	textPut("dest", m_manageProcInputDestPackageIndex, POPOVER_AUDIO_PROC, PROC_INPUT_DEST_PACKAGE);

	//Park Audio Proc Input DP
	textPut(bncs_string("take.%1").arg(m_manageProcInputDestPackageIndex), "park", POPOVER_AUDIO_PROC, PROC_INPUT_DEST_PACKAGE);

	//Clear Audio Proc Input DP usage database - db0013
	routerModify(m_devicePackageRouter, DATABASE_DEST_PACKAGE_AUDIO_PROC_USAGE, m_manageProcInputDestPackageIndex, "", false);

	//Update Audio Proc AP assignment database - db0069
	// Remove references and levels for m_manageProcNum

	//31811=proc_1=ldc_proc_001,proc_2=,a1=,a2=,a3=,a4=,a5=1.1,a6=1.2,a7=,a8=,a9=,a10=,a11=,a12=

	bool updateProcAssignment = false;

	for (int field = 0; field < apAudioProcFields.count(); field++)
	{
		bncs_string key;
		bncs_string value;
		bncs_string keyValue = apAudioProcFields[field];
		keyValue.split('=', key, value);

		//clear proc_x field if x == m_manageProcNum
		if (key == bncs_string("proc_%1").arg(m_manageProcNum))
		{
			apAudioProcFields[field] = key + "=";
			updateProcAssignment = true;
		}

		//clear a#=x.y field if x == m_manageProcNum
		if (key.startsWith("a"))
		{
			int pair = key.mid(1).toInt();

			bncs_string splitProc;
			bncs_string splitOutput;
			value.split('.', splitProc, splitOutput);
			if (splitProc.toInt() == m_manageProcNum)
			{
				debug("audio_package_edit_view::removeAudioProc() clear pair a%1 key=%2 value=%3 proc=%4 out=%5", pair, key, value, splitProc, splitOutput);
				apAudioProcFields[field] = key + "=";
				updateProcAssignment = true;
			}
		}
	}

	if (updateProcAssignment)
	{
		debug("audio_package_edit_view::removeAudioProc() updateProcAssignment: %1", apAudioProcFields.toString());
		routerModify(m_devicePackageRouter, DATABASE_AP_AUDIO_PROC, m_AP, apAudioProcFields.toString(), false);
	}

	//clear m_manageProcNum and m_manageProcId
	m_manageProcNum = 0;
	m_manageProcId = "";
}