#pragma once

#include <bncs_string.h>

const int INPUT_COUNT(6);	//1-based
const int OUTPUT_COUNT(6);	//1-based

class CAudioProc
{
public:
	CAudioProc();
	~CAudioProc();

	bncs_string name;
	bncs_string packagerInstance;
	bncs_string description;
	bncs_string category;
	int destPackage;

	bncs_string labelIn[INPUT_COUNT];
	int destPackageLevelIn[INPUT_COUNT];
	int audioTypeIn[INPUT_COUNT];
	int audioLanguageIn[INPUT_COUNT];

	bncs_string labelOut[OUTPUT_COUNT];
	int audioIndexOut[OUTPUT_COUNT];
	int audioTypeOut[OUTPUT_COUNT];
};

