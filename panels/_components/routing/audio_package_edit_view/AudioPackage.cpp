#include "AudioPackage.h"


CAudioPackage::CAudioPackage()
{
}


CAudioPackage::~CAudioPackage()
{
}

void CAudioPackage::reset()
{
	for (int pair = 0; pair < PAIR_COUNT; pair++)
	{
		index[pair] = 0;
		typeTag[pair] = 0;
	}

	for (int audioReturn = 0; audioReturn < AR_COUNT; audioReturn++)
	{
		arIndex[audioReturn] = 0;
		arTypeTag[audioReturn] = 0;
		arLanguageTag[audioReturn] = 0;
	}

	rev_vision = 0;
	rev_vision_hub_tag = 0;
	language_tag = 0;
	name = "";
	package_title = "";
}