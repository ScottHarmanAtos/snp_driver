#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "vip_dest.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define BTN_DEST	"dest"
#define LBL_TALLY	"tally"

#define DATABASE_SOURCE_NAME	0	
#define DATABASE_DEST_NAME		1	
#define DATABASE_MAPPING    	8	

#define DATABASE_DEST_ID		11
#define DATABASE_DEST_LABEL		15	

const int PRI_TOP(1);
const int PRI_HIGH(2);
const int PRI_DEFAULT(3);
const int PRI_LOW(4);
const int PRI_BOTTOM(5);

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( vip_dest )

// constructor - equivalent to ApplCore STARTUP
vip_dest::vip_dest( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_undoSource = -1;
	m_source = -1;
	m_dest = -1;
	m_globalDest = -1;
	m_deviceDest = 0;
	m_deviceLock = 0;
	m_deviceLockBase = 0;
	m_destDeviceOffset = 0;
	m_deviceSource = 0;
	m_priority = PRI_TOP;

	m_showHighlight = true;
	m_selected = false;
	m_locked = false;
	m_destSingleLine = false;
	m_layout = "main";

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, bncs_string("%1.bncs_ui").arg(m_layout));

	textPut("statesheet", "background_discrete", PNL_MAIN, LBL_TALLY);

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
vip_dest::~vip_dest()
{
}

// all button pushes and notifications come here
void vip_dest::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if (b->id() == BTN_DEST && m_globalDest > 0)
		{
			hostNotify(bncs_string("dest=%1").arg(m_globalDest));
			hostNotify("button=released");
			if (m_showHighlight)
			{
				textPut("statesheet", "dest_selected", PNL_MAIN, BTN_DEST);
				m_selected = true;
				hostNotify(bncs_string("current_source=%1").arg(m_source));
			}
		}

	}
}

// all revertives come here
int vip_dest::revertiveCallback( revertiveNotify * r )
{
	r->dump("vip_dest::revertiveCallback");
	if (r->device() == m_deviceDest)
	{
		bncs_stringlist fields = bncs_stringlist(r->sInfo());
		m_source = fields.getNamedParam("index").toInt();
		int status = fields.getNamedParam("status").toInt();
		int lock = fields.getNamedParam("lock").toInt();
		m_locked = (lock == 1) ? true : false;

		// get the name of this port from the source device database
		bncs_string srcName;
		
		if (m_source == 0)
		{
			srcName = "Disconnected";
		}
		else
		{
			routerName(m_deviceSource, DATABASE_SOURCE_NAME, m_source, srcName);
			// make a single line of it
			srcName.replace("|", " ");
		}

		if (m_undoSource == -1)
		{
			//Set initial value for undo
			m_undoSource = m_source;
		}
		//textPut("text", bncs_string("undo=%1|status=%2").arg(m_undoSource).arg(status), PNL_MAIN, BTN_DEST);

		// stick on our single tally component (may or may not be shown...)
		textPut("text", srcName, PNL_MAIN, LBL_TALLY);

		if (m_locked)
		{
			textPut("pixmap.topRight", "/images/icon_padlock.png", PNL_MAIN, BTN_DEST);
		}
		else
		{
			textPut("pixmap.topRight", "", PNL_MAIN, BTN_DEST);
		}

		if (status == 0)		//Disconnected
		{
			textPut("statesheet", "background_discrete", PNL_MAIN, LBL_TALLY);
			textPut("pixmap.left", "", PNL_MAIN, LBL_TALLY);
		}
		else if (status == 1)	//Scheduled
		{
			textPut("statesheet", "destgrid_tally", PNL_MAIN, LBL_TALLY);
			textPut("pixmap.left", "/images/icon_time_small.png", PNL_MAIN, LBL_TALLY);
		}
		else if (status == 2)	//Active
		{
			textPut("statesheet", "destgrid_tally", PNL_MAIN, LBL_TALLY);
			textPut("pixmap.left", "", PNL_MAIN, LBL_TALLY);
		}
		else if (status == 3)	//Pending
		{
			textPut("statesheet", "destgrid_tally", PNL_MAIN, LBL_TALLY);
			textPut("pixmap.left", "/images/icon_hourglass_small.png", PNL_MAIN, LBL_TALLY);
		}

		// work out the offset to our local tally cache
		//int offset = r->index() - m_dest;
		//debug("vip_dest::revertiveCallback Calc Offset:%1", offset);
	}
	else if (r->device() == m_deviceLock)
	{
		//		debug( "%1 %2 %3", r->device(), r->index(), r->sInfo() );

		map<int, int>::iterator it = m_lockStates.find(r->index());

		// atoi allows for unset (null entry) as well as explicitly set 0 or 1...dunno if this is a good thing or not
		if (it != m_lockStates.end())
			it->second = atoi(r->sInfo()) ? 1 : 0;

	}
	return 0;

}

// all database name changes come back here
void vip_dest::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string vip_dest::parentCallback( parentNotify *p )
{
	p->dump(bncs_string("vip_dest::parentCallback() [%1]").arg(m_globalDest));
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("layout=%1").arg(m_layout);
			sl << bncs_string("dest_single_line=%1").arg(m_destSingleLine?"true":"false");
			sl << bncs_string("index=%1").arg(m_globalDest);

			return sl.toString( '\n' );
		}
		else if (p->command() == "return" && p->value().startsWith("getIndex"))
		{
			bncs_stringlist sl(p->value(), '.');

			if (sl.count() == 4)
			{
				bncs_string instance = sl[1];
				bncs_string offset = sl[2];
				bncs_string page = sl[3];

				int device = 0;
				if (instance.length() != 0 && getDev(instance, &device))
				{
					bncs_string value;
					routerName(device, DATABASE_MAPPING, offset.toInt() + page.toInt(), value);

					return bncs_string("%1=%2").arg(p->value()).arg(value);
				}
				else
				{
					return bncs_string("%1=").arg(p->value());
				}
			}
		}

		else if (p->value() == "layout")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_layout));
		}

		else if (p->value() == "dest_single_line")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg((m_destSingleLine ? "true" : "false")));
		}

		else if (p->value() == "index")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_globalDest));
		}
	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		getDev(m_instance, &m_deviceSource);
	}
	else if (p->command() == "index")
	{
		globaDestSet(p->value());
	}
	else if (p->command() == "take")
	{
		int source = p->value().toInt();
		if (source > -1)
		{
			routeSource(p->value().toInt());
		}
	}
	else if (p->command() == "undo")
	{
		int dest = p->value().toInt();
		if (m_undoSource > -1 && dest == m_globalDest)
		{
			routeSource(m_undoSource);
		}
	}
	else if (p->command().startsWith("deselect") && m_dest > 0)
	{
		deselect();
	}
	else if (p->command() == "layout")
	{	// Persisted value or 'Command' being set here
		loadLayout(p->value());
	}

	else if (p->command() == "dest_single_line")
	{
		if (p->value().lower() == "true")
		{
			m_destSingleLine = true;
		}
		else
		{
			m_destSingleLine = false;
		}
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void vip_dest::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void vip_dest::globaDestSet(bncs_string value)
{
	if (m_instance.length() > 0)
	{
		if (m_deviceDest)
			infoUnregister(m_deviceDest);
		m_deviceDest = 0;

		if (m_deviceLock)
			infoUnregister(m_deviceLock);
		m_deviceLock = 0;

		if (value.length())
		{
			m_globalDest = value;

			m_destDeviceOffset = (m_globalDest - 1) / 4096;
			m_dest = ((m_globalDest - 1) % 4096) +1;

			m_deviceDest = m_deviceSource + m_destDeviceOffset;
			m_deviceLock = m_deviceLockBase + m_destDeviceOffset;
		}
		else
		{
			m_deviceDest = 0;
			m_deviceLock = 0;
			m_globalDest = 0;

			m_destDeviceOffset = 0;
			m_dest = -1;
		}

		bncs_string destName;
		routerName(m_deviceSource, DATABASE_DEST_NAME, m_globalDest, destName);

		bncs_string destLabel;
		routerName(m_deviceSource, DATABASE_DEST_LABEL, m_globalDest, destLabel);

		if (destName == "---")
		{
			textPut("text", "", PNL_MAIN, BTN_DEST);
		}
		else
		{
			//bncs_string buttonText = bncs_string("%1|[%2]|%3/%4").arg(destName.replace('|', ' ')).arg(destLabel).arg(m_deviceDest).arg(m_dest);
			if (m_destSingleLine)
			{
				textPut("text", destName.replace('|', ' '), PNL_MAIN, BTN_DEST);

			}
			else
			{
				textPut("text", destName, PNL_MAIN, BTN_DEST);
			}
		}


		textPut("text", "", PNL_MAIN, LBL_TALLY);

		debug( "vip_dest::globaDestSet() base device/actual lock base/actual %1 %2 %3 %4 - global/local %5 %6", m_deviceSource, m_deviceDest, m_deviceLockBase, m_deviceLock, m_globalDest, m_dest );

		m_lockStates.clear();

		if (m_selected)
		{
			deselect();
		}

		infoRegister(m_deviceDest, m_dest, m_dest);
		infoPoll(m_deviceDest, m_dest, m_dest);

		if (m_deviceLock)
		{
			m_lockStates.insert(make_pair(m_dest, -1));

			infoRegister(m_deviceLock, m_dest, m_dest);
			infoPoll(m_deviceLock, m_dest, m_dest);
		}

		hostNotify(bncs_string("global_dest=%1").arg(m_globalDest));
	}
}

void vip_dest::routeSource(int source)
{
	debug("vip_dest::routeSource source:%1 selected:%2 locked:%3 dest:%4", source, m_selected, m_locked, m_dest);

	if (m_selected == true && m_locked == false)
	{
		if (source > -1)
		{
			m_undoSource = m_source;
			debug("vip_dest::parentCallback() dest %1 routing source %2", m_dest, source);
			bncs_string command = bncs_string("index=%1,pri=%2,App: XY,Workstation: %2")
				.arg(source).arg(m_priority).arg(workstation());
			infoWrite(m_deviceDest, command, m_dest);
		}
	}
}

void vip_dest::deselect()
{
	m_selected = false;
	textPut("statesheet", "dest_deselected", PNL_MAIN, BTN_DEST);
}

void vip_dest::loadLayout(bncs_string layout)
{
	if (layout != m_layout)
	{
		panelDestroy(PNL_MAIN);
	}
	m_layout = layout;

	bool show = panelShow(PNL_MAIN, bncs_string("%1.bncs_ui").arg(m_layout));
	if (show)
	{
		m_layout = "main";

		panelShow(PNL_MAIN, bncs_string("%1.bncs_ui").arg(m_layout));
	}

}