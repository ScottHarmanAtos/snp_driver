#ifndef vip_dest_INCLUDED
	#define vip_dest_INCLUDED

#pragma warning(disable:4786)
#include <bncs_script_helper.h>
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class vip_dest : public bncs_script_helper
{
public:
	vip_dest( bncs_client_callback * parent, const char* path );
	virtual ~vip_dest();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_layout;

	map< int, int > m_lockStates;

	int m_source;
	int m_undoSource;
	int m_dest; 
	int m_globalDest;
	int m_deviceDest;
	int m_deviceLock;
	int m_deviceLockBase;
	int m_destDeviceOffset;
	int m_deviceSource;
	int m_priority;

	bool m_showHighlight;
	bool m_selected;
	bool m_locked;
	bool m_destSingleLine;

	void globaDestSet(bncs_string value);
	void routeSource(int source);
	void deselect();
	void loadLayout(bncs_string layout);

};


#endif // vip_dest_INCLUDED