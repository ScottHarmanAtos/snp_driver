#ifndef audio_preset_recall_INCLUDED
	#define audio_preset_recall_INCLUDED

#include <bncs_script_helper.h>
#include "packager_common.h"
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class audio_preset_recall : public bncs_script_helper
{
public:
	audio_preset_recall( bncs_client_callback * parent, const char* path );
	virtual ~audio_preset_recall();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_destName;
	bncs_string m_instance;
	bncs_string m_compositePackager;	//c_packager_nhn or c_packager_ldc
	bncs_string m_instancePackagerRouter;

	bncs_packager m_packagerRouter;

	bool m_apply;

	int m_devicePackageRouter;		//dev_1001 or dev_2001

	int m_preset;
	int m_presetPage;
	int m_presetCat;
	int m_levelPage;
	int m_destIndex;
	int m_selectedCategory;
	int m_selectedPage;
	int m_pageOffset;

	//void initCategories();
	//void showGridCategory();
	//void showGridPage();

	void init();
	void showPreset();
	void showLevelPage(int index);
	void applyPreset();
	void getCurrentPreset();


};


#endif // audio_preset_recall_INCLUDED