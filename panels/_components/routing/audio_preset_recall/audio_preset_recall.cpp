#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "audio_preset_recall.h"

#define PNL_MAIN		1
#define POPUP_GRID		2
#define POPUP_CONFIRM	3

#define BTN_APPLY			"apply"
#define BTN_CLOSE			"close"
#define BTN_RECALL_PRESET	"recall_preset"
#define LBL_CURRENT_PRESET	"current_preset"
#define PRESET_GRID			"grid"

#define LBL_NAME			"preset_name"
#define LBL_NUM				"preset_num"
#define LBL_CAT				"category"
#define LBL_PAGE			"page"

#define MAP_OFFSET_PAGENAME	2600
#define MAP_OFFSET_CATNAME	6000
#define PAGES_PER_CATEGORY	18

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( audio_preset_recall )

// constructor - equivalent to ApplCore STARTUP
audio_preset_recall::audio_preset_recall( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_preset = 0;
	m_presetPage = 1;
	m_presetCat = 1;
	m_levelPage = 1;
	m_destIndex = 0;
	m_destName = "---";
	m_apply = false;
	m_selectedCategory = 0;
	m_selectedPage = 0;
	m_pageOffset = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );
	textPut("text", "", PNL_MAIN, LBL_CURRENT_PRESET);
	controlDisable(PNL_MAIN, BTN_RECALL_PRESET);

	init();

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
audio_preset_recall::~audio_preset_recall()
{
}

// all button pushes and notifications come here
void audio_preset_recall::buttonCallback( buttonNotify *b )
{
	b->dump("audio_preset_recall::buttonCallback()");
	bncs_stringlist controlIndex(b->id(), '_');
	int index = controlIndex[1].toInt();

	if (b->panel() == PNL_MAIN)
	{
		if (b->id() == BTN_RECALL_PRESET)
		{
			panelPopup(POPUP_GRID, "popup_grid.bncs_ui");
			m_presetCat = 1;
			m_presetPage = 1;
			textPut("instance", m_instancePackagerRouter, POPUP_GRID, PRESET_GRID);
			textPut("group_index", m_presetCat, POPUP_GRID, PRESET_GRID);
			textPut("page_index", m_presetPage, POPUP_GRID, PRESET_GRID);
		}
	}
	else if (b->panel() == POPUP_GRID)
	{
		if (b->id() == PRESET_GRID)
		{
			if (b->command() == "group_index")
			{
				m_presetCat = b->value().toInt();
			}
			else if (b->command() == "page_index")
			{
				m_presetPage = b->value().toInt();
			}
			else if (b->command() == "index")
			{
				m_preset = b->value().toInt();
				if (m_preset > 0)
				{ 
					showPreset();
				}
			}
		}
		/*
		if (b->id().startsWith("preset_"))
		{
			m_preset = index + m_pageOffset; //only on page 1
			//TODO show confirm
			showPreset();
		}
		else if (b->id().startsWith("page_"))
		{
			m_presetPage = index;	//only on cat 1
			showGridPage();
		}
		else if (b->id().startsWith("cat_"))
		{
			m_presetCat = index;
			//TODO show pages of the category
		}
		*/
	}
	else if (b->panel() == POPUP_CONFIRM)
	{
		if (b->id().startsWith("audio-page_"))
		{
			showLevelPage(index);
		}
		else if (b->id() == BTN_APPLY)
		{
			panelDestroy(POPUP_CONFIRM);
			applyPreset();
		}
		else if (b->id() == BTN_CLOSE)
		{
			panelDestroy(POPUP_CONFIRM);
		}
	}

}

// all revertives come here
int audio_preset_recall::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void audio_preset_recall::databaseCallback( revertiveNotify * r )
{
	r->dump("audio_preset_recall::databaseCallback()");
	if (r->device() == m_devicePackageRouter && r->database() == DATABASE_DEST_LANG_TYPE)
	{
		if (r->index() == m_destIndex)
		{
			bncs_string name = "---";
			bncs_string destAudioTags = r->sInfo();

			int preset = routerIndex(m_devicePackageRouter, DATABASE_PRESET_PRIMARY, destAudioTags);

			if (preset > 0)
			{
				m_preset = preset;
				routerName(m_devicePackageRouter, DATABASE_PRESET_NAME, m_preset, name);
			}
			textPut("text", name, PNL_MAIN, LBL_CURRENT_PRESET);
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string audio_preset_recall::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("apply_preset=%1").arg(m_apply?"true":"false");
			sl << bncs_string("dest_index=%1").arg(m_destIndex);

			return sl.toString( '\n' );
		}

		else if (p->value() == "apply_preset")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_apply ? "true" : "false"));
		}
		else if (p->value() == "dest_index")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_destIndex));
		}
		else if (p->value() == "current_preset")
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_preset));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "apply_preset")
	{	// Persisted value or 'Command' being set here
		if (p->value().upper() == "TRUE")
		{
			m_apply = true;
		}
		else
		{
			m_apply = false;
		}
	}

	else if( p->command() == "dest_name" )
	{	// Persisted value or 'Command' being set here
		m_destName = p->value();
	}

	else if (p->command() == "dest_index")
	{	// Persisted value or 'Command' being set here
		m_destIndex = p->value().toInt();

		if (m_destIndex > 0)
		{
			routerName(m_devicePackageRouter, DATABASE_DEST_NAME, m_destIndex, m_destName);
			controlEnable(PNL_MAIN, BTN_RECALL_PRESET);
		}
		else
		{
			m_destName = "---";
			controlDisable(PNL_MAIN, BTN_RECALL_PRESET);
		}
		getCurrentPreset();
	}
	else if (p->command() == "set_preset_dest_index")
	{
		// passed as "set_preset_dest = dest_index"

		bncs_string sPreset = "";
		const auto sDestIndex = p->value();
		
		bncs_string destAudioTags = "";
		routerName(m_devicePackageRouter, DATABASE_DEST_LANG_TYPE, sDestIndex.toInt(), destAudioTags);

		m_preset = routerIndex(m_devicePackageRouter, DATABASE_PRESET_PRIMARY, destAudioTags);
		debug("audio_preset_recall::set_preset_dest_index:: dest_index=%1 preset=%2 tags=%3", sDestIndex.toInt(), m_preset, destAudioTags);

		if ((m_preset > 0) && (m_destIndex > 0))
		{
			routerName(m_devicePackageRouter, DATABASE_DEST_NAME, m_destIndex, m_destName);
			controlEnable(PNL_MAIN, BTN_RECALL_PRESET);
		}
		else
		{
			m_destName = "---";
			controlDisable(PNL_MAIN, BTN_RECALL_PRESET);
		}
		applyPreset();
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "dest_index=[value]";
		sl << "apply_preset=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void audio_preset_recall::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
void audio_preset_recall::showGridPage()
{
	debug("audio_preset_recall::showGridPage()");

	bncs_stringlist sltPreset = getIdList(POPUP_GRID, "preset_");
	bncs_stringlist sltPage = getIdList(POPUP_GRID, "page_");
	for (int index = 0; index < sltPreset.count(); index++)
	{
		bncs_string name;
		m_pageOffset = (m_presetPage - 1) * sltPage.count();
		int preset = index + 1 + m_pageOffset;

		routerName(m_devicePackageRouter, DATABASE_PRESET_NAME, preset, name);

		textPut("text", name, POPUP_GRID, sltPreset[index]);
		debug("audio_preset_recall::initGrid() preset[%1] name=%2 control=%3", preset, name, sltPreset[index]);
	}
}

void audio_preset_recall::showGridCategory()
{
	debug("audio_preset_recall::showGridCategory()");

	bncs_stringlist sltPage = getIdList(POPUP_GRID, "page_");
	for (int index = 0; index < sltPage.count(); index++)
	{
		bncs_string name;
		int categoryOffset = (m_selectedCategory - 1) * sltPage.count();
		int page = index + 1 + categoryOffset;
		name = bncs_string("Cat %1|Page %2").arg(m_selectedCategory).arg(page);
		textPut("text", name, POPUP_GRID, sltPage[index]);
	}
}

void audio_preset_recall::initCategories()
{
	bncs_stringlist sltCat = getIdList(POPUP_GRID, "cat_");
	for (int index = 0; index < sltCat.count(); index++)
	{
		bncs_string name;
		name = bncs_string("Category|%1").arg(index + 1);
		textPut("text", name, POPUP_GRID, sltCat[index]);

		if (m_selectedCategory == index + 1)
		{
			textPut("statesheet", "sourcegroup_selected", POPUP_GRID, sltCat[index]);
		}
		else
		{
			textPut("statesheet", "sourcegroup_deselected", POPUP_GRID, sltCat[index]);
		}
	}
}
*/

void audio_preset_recall::init()
{
	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);
	m_instancePackagerRouter = m_packagerRouter.instance(1);

	//Get main router device
	m_devicePackageRouter = m_packagerRouter.destDev(1);
	debug("audio_preset_recall::init() m_instancePackagerRouter=%1 m_devicePackageRouter=%2", m_instancePackagerRouter, m_devicePackageRouter);

	//register for RM events
	infoRegister(m_devicePackageRouter, 1, 1);
}

void audio_preset_recall::showPreset()
{
	panelDestroy(POPUP_GRID);
	panelPopup(POPUP_CONFIRM, "popup_confirm.bncs_ui");

	textPut("text", bncs_string("Apply to|%1").arg(m_destName.replace('|',' ')), POPUP_CONFIRM, BTN_APPLY);

	bncs_string name;
	routerName(m_devicePackageRouter, DATABASE_PRESET_NAME, m_preset, name);
	textPut("text", name, POPUP_CONFIRM, LBL_NAME);
	textPut("text", m_preset, POPUP_CONFIRM, LBL_NUM);

	int catPage = ((m_presetCat - 1) * PAGES_PER_CATEGORY) + m_presetPage;

	bncs_string catName;
	routerName(m_devicePackageRouter, DATABASE_PRESET_MAPPING, MAP_OFFSET_CATNAME + m_presetCat, catName);

	bncs_string pageName;
	routerName(m_devicePackageRouter, DATABASE_PRESET_MAPPING, MAP_OFFSET_PAGENAME + catPage, pageName);

	textPut("text", catName.replace('|', ' '), POPUP_CONFIRM, LBL_CAT);
	textPut("text", pageName.replace('|', ' '), POPUP_CONFIRM, LBL_PAGE);

	bncs_stringlist controls = getIdList(POPUP_CONFIRM, "audio_");
	for (int index = 0; index < controls.count(); index++)
	{
		textPut("preset", m_preset, POPUP_CONFIRM, controls[index]);
	}

	showLevelPage(1);
}

void audio_preset_recall::showLevelPage(int page)
{
	int pageItems = 8;

	if (m_levelPage != page)
	{
		//highlight buttons
		textPut("statesheet", "enum_deselected", POPUP_CONFIRM, bncs_string("audio-page_%1").arg(m_levelPage));
	}
	m_levelPage = page;
	textPut("statesheet", "enum_selected", POPUP_CONFIRM, bncs_string("audio-page_%1").arg(m_levelPage));

	bncs_stringlist controls = getIdList(POPUP_CONFIRM, "audio_");
	//first hide all
	for (int l = 1; l <= controls.count(); l++)
	{
		controlHide(POPUP_CONFIRM, bncs_string("audio_%1").arg(l, '0', 2));
	}

	//show range of 8 for current page
	int offset = (m_levelPage - 1) * pageItems;
	for (int level = offset + 1; level <= offset + pageItems; level++)
	{
		controlShow(POPUP_CONFIRM, bncs_string("audio_%1").arg(level, '0', 2));
	}
}

void audio_preset_recall::applyPreset()
{
	bncs_string name;
	routerName(m_devicePackageRouter, DATABASE_PRESET_NAME, m_preset, name);
	bncs_string presetPrimary;
	routerName(m_devicePackageRouter, DATABASE_PRESET_PRIMARY, m_preset, presetPrimary);
	bncs_string presetSub1;
	routerName(m_devicePackageRouter, DATABASE_PRESET_SUB1, m_preset, presetSub1);
	bncs_string presetSub2;
	routerName(m_devicePackageRouter, DATABASE_PRESET_SUB2, m_preset, presetSub2);

	if (m_apply == true)
	{
		//Apply directly to destination package
		routerModify(m_devicePackageRouter, DATABASE_DEST_LANG_TYPE, m_destIndex, presetPrimary, false);

		//Apply to Sub1
		routerModify(m_devicePackageRouter, DATABASE_DEST_AUDIO_1, m_destIndex, presetSub1, false);

		//Apply to Sub2
		routerModify(m_devicePackageRouter, DATABASE_DEST_AUDIO_2, m_destIndex, presetSub2, false);
	}
	else
	{
		//Send notification with audio tags for use by Destination Package Editor
		bncs_string apply = bncs_string("%1|%2|%3").arg(presetPrimary).arg(presetSub1).arg(presetSub2);
		hostNotify("apply_preset=" + apply);
	}

	//textPut("text", name, PNL_MAIN, LBL_CURRENT_PRESET);
}

void audio_preset_recall::getCurrentPreset()
{

	bncs_string name = "---";
	bncs_string destAudioTags;
	routerName(m_devicePackageRouter, DATABASE_DEST_LANG_TYPE, m_destIndex, destAudioTags);

	int presetIndex = routerIndex(m_devicePackageRouter, DATABASE_PRESET_PRIMARY, destAudioTags);
	debug("audio_preset_recall::getCurrentPreset() dest_index=%1 preset=%2 tags=%3", m_destIndex, presetIndex, destAudioTags);

	if (presetIndex > 0)
	{
		routerName(m_devicePackageRouter, DATABASE_PRESET_NAME, presetIndex, name);

		//TODO highlight preset in grid?
		//m_preset = presetIndex;
	}
	textPut("text", name, PNL_MAIN, LBL_CURRENT_PRESET);
}

