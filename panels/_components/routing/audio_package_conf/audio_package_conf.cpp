#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "audio_package_conf.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define BTN_CONF_ENABLE		"conf_enable"
#define ALLOCATED_CONF		"allocated_conf"
#define MAIN_PORT			"main_port"
#define CONF_LABEL			"conf_label"
#define CONF_SUBTITLE		"conf_subtitle"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( audio_package_conf )

// constructor - equivalent to ApplCore STARTUP
audio_package_conf::audio_package_conf( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_diffInRing = 0;
	m_diffInPort = 0;
	m_diffOutRing = 0;
	m_diffOutPort = 0;

	m_editInRing = 0;
	m_editInPort = 0;
	m_editOutRing = 0;
	m_editOutPort = 0;

	m_confNumber = 0;
	m_level = 0;

	rh = riedelHelper::getriedelHelperObject();
	
	//getDev(INSTANCE_PACKAGE_ROUTER, &m_devicePackageRouter);

	/*
	<packager_config>
	  <data id="packager_auto">
	    <setting id="packager_router" value="packager_router_main" />
	    
	    <setting id="packager_riedel_1" value="packager_riedel_1_main" />
	    <setting id="packager_riedel_2" value="packager_riedel_2_main" />	
	    
	    <!-- Ring master defaults used when no ring info passed into packages definitions etc -->
	    <setting id="ring_master_auto" value="instance=ring_master_main" />
	    <setting id="default_conf_ring" value="30" />
	    <setting id="default_ifb_ring"  value="30" /> <!-- this missing from export added by hand-->
	    <setting id="riedel_ring_ifb_hold_sources"  value="589,589,589,589" />
	    <setting id="riedel_ring_conference_pool"  value="1,400" />
	*/
	
	bncs_config cfgPackagerRiedel1(bncs_string("packager_config.packager_auto.packager_riedel_1"));
	if (cfgPackagerRiedel1.isValid())
	{
		//bncs_stringlist sltPackagerRiedel1(cfgPackagerRiedel1.attr("value"));
		//m_instanceAudioRouter = sltPackagerRouterAudio.getNamedParam("instance");
		//getDev(m_instanceAudioRouter, &m_deviceAudioRouter);
	}
	else
	{
		//m_deviceAudioRouter = -1;
	}

	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
audio_package_conf::~audio_package_conf()
{
}

// all button pushes and notifications come here
void audio_package_conf::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if (b->id() == BTN_CONF_ENABLE)
		{
			hostNotify(bncs_string("conf_enable=%1,%2").arg(m_level).arg(m_confNumber));
		}
		else if (b->id() == MAIN_PORT)
		{
			hostNotify(bncs_string("main_port=%1,%2.%3/%4.%5")
				.arg(m_level)
				.arg(m_editInRing)
				.arg(m_editInPort)
				.arg(m_editOutRing)
				.arg(m_editOutPort));
		}
		else if (b->id() == CONF_LABEL)
		{
			hostNotify(bncs_string("conf_label=%1,%2").arg(m_level).arg(m_editLabel));
		}
		else if (b->id() == CONF_SUBTITLE)
		{
			hostNotify(bncs_string("conf_subtitle=%1,%2").arg(m_level).arg(m_editSubtitle));
		}
	}
}

// all revertives come here
int audio_package_conf::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), PNL_MAIN, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void audio_package_conf::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string audio_package_conf::parentCallback( parentNotify *p )
{
	p->dump("audio_package_conf::parentCallback");

	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string("display_level=%1").arg(m_level);

			return sl.toString( '\n' );
		}

		else if( p->value() == "display_level" )
		{	// Specific value being asked for by a textGet
			return(bncs_string("%1=%2").arg(p->value()).arg(m_level));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "display_level")
	{	// Persisted value or 'Command' being set here
		m_level = p->value().toInt();
		textPut("text", bncs_string("Conf %1 Enable").arg(m_level), PNL_MAIN, BTN_CONF_ENABLE);
	}
	else if (p->command() == "diff_data")
	{
		updateDiffData(p->value());
	}
	else if (p->command() == "edit_data")
	{
		updateEditData(p->value());
	}
	else if (p->command() == "allocation_data")
	{
		updateAllocationData(p->value());
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void audio_package_conf::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void audio_package_conf::updateDiffData(bncs_string levelData)
{
	bncs_stringlist sltFields(levelData);

	if (m_level == 1)
	{
		debug("audio_package_conf::updateDiffData() level=%1 levelData=%2", m_level, levelData);
	}

	if (levelData.find(CONF_LABEL) > -1)
	{
		m_diffLabel = sltFields.getNamedParam(CONF_LABEL);
		textPut("text", m_diffLabel, PNL_MAIN, CONF_LABEL);
	}

	if (levelData.find(CONF_SUBTITLE) > -1)
	{
		m_diffSubtitle = sltFields.getNamedParam(CONF_SUBTITLE);
		textPut("text", m_diffSubtitle, PNL_MAIN, CONF_SUBTITLE);
	}

	bool updateMainPort = false;
	//"in_ring=%1,in_port=%2,out_ring=%3,out_port=%4"

	if (levelData.find("in_ring") > -1)
	{
		updateMainPort = true;
		m_diffInRing = sltFields.getNamedParam("in_ring").toInt();
	}
	
	if (levelData.find("in_port") > -1)
	{
		updateMainPort = true;
		m_diffInPort = sltFields.getNamedParam("in_port").toInt();
	}
	
	if (levelData.find("out_ring") > -1)
	{
		updateMainPort = true;
		m_diffOutRing = sltFields.getNamedParam("out_ring").toInt();
	}
	
	if (levelData.find("out_port") > -1)
	{
		updateMainPort = true;
		m_diffOutPort = sltFields.getNamedParam("out_port").toInt();
	}

	if (updateMainPort)
	{
		bncs_string nameIn;
		comms_ringmaster_port portIn = rh->GetPort(m_diffInRing, m_diffInPort);
		if (portIn.valid == true)
		{
			nameIn = portIn.sourceName_short;
		}

		bncs_string nameOut;
		comms_ringmaster_port portOut = rh->GetPort(m_diffOutRing, m_diffOutPort);
		if (portOut.valid == true)
		{
			nameOut = portOut.destName_short;
		}

		//nameIn = bncs_string("%1.%2").arg(m_diffInRing).arg(m_diffInPort);
		//nameOut = bncs_string("%1.%2").arg(m_diffOutRing).arg(m_diffOutPort);

		bncs_string cfCircuit = bncs_string("%1.%2/%3.%4").arg(m_diffInRing).arg(m_diffInPort).arg(m_diffOutRing).arg(m_diffOutPort);
		bncs_string cfNames = bncs_string("%1/%2").arg(nameIn).arg(nameOut);
		debug("audio_package_cf::updateDiffData() level=%1 cfCircuit=%2", m_level, cfCircuit);
		//textPut("text", cfCircuit, PNL_MAIN, CF_CIRCUIT);

		bncs_string splitter;
		unsigned int length = (nameIn.length() > nameOut.length()) ? nameIn.length() : nameOut.length();
		for (unsigned int c = 0; c < length; c++)
		{
			splitter.append("�");
		}

		bool sameName = (nameOut == nameIn);
		bool sameRing = (m_diffInRing == m_diffOutRing);

		bncs_string displayName = "---";

		if (sameRing)
		{
			if (sameName)
			{
				//Simple case: in & out have the same name and are on the same ring
				displayName = nameOut;
			}
			else
			{
				displayName = bncs_string("%1|%2|%3").arg(nameIn.replace('|', ' '))
					.arg(splitter).arg(nameOut.replace('|', ' '));
			}
		}
		else
		{
			displayName = bncs_string("%1:%2|%3|%4:%5")
				.arg(m_diffInRing).arg(nameIn.replace('|', ' ')).arg(splitter)
				.arg(m_diffOutRing).arg(nameOut.replace('|', ' '));
		}
		textPut("text", displayName, PNL_MAIN, MAIN_PORT);
	}
}

void audio_package_conf::updateEditData(bncs_string levelData)
{
	debug("audio_package_cf::updateEditData() level=%1 levelData=%2", m_level, levelData);
	bncs_stringlist sltFields(levelData);

	if (levelData.find(CONF_LABEL) > -1)
	{
		m_editLabel = sltFields.getNamedParam(CONF_LABEL);
		textPut("text", m_editLabel, PNL_MAIN, CONF_LABEL);
		textPut("statesheet", m_editLabel == m_diffLabel ? "tag_clean" : "tag_dirty", PNL_MAIN, CONF_LABEL);
	}

	if (levelData.find(CONF_SUBTITLE) > -1)
	{
		m_editSubtitle = sltFields.getNamedParam(CONF_SUBTITLE);
		textPut("text", m_editSubtitle, PNL_MAIN, CONF_SUBTITLE);
		textPut("statesheet", m_editSubtitle == m_diffSubtitle ? "tag_clean" : "tag_dirty", PNL_MAIN, CONF_SUBTITLE);
	}

	bool updateMainPort = false;
	//"in_ring=%1,in_port=%2,out_ring=%3,out_port=%4"

	if (levelData.find("in_ring") > -1)
	{
		updateMainPort = true;
		m_editInRing = sltFields.getNamedParam("in_ring").toInt();
	}

	if (levelData.find("in_port") > -1)
	{
		updateMainPort = true;
		m_editInPort = sltFields.getNamedParam("in_port").toInt();
	}

	if (levelData.find("out_ring") > -1)
	{
		updateMainPort = true;
		m_editOutRing = sltFields.getNamedParam("out_ring").toInt();
	}

	if (levelData.find("out_port") > -1)
	{
		updateMainPort = true;
		m_editOutPort = sltFields.getNamedParam("out_port").toInt();
	}

	if (updateMainPort)
	{
		bncs_string nameIn;

		comms_ringmaster_port portIn = rh->GetPort(m_editInRing, m_editInPort);
		if (portIn.valid == true)
		{
			nameIn = portIn.sourceName_short;
		}

		bncs_string nameOut;

		comms_ringmaster_port portOut = rh->GetPort(m_editOutRing, m_editOutPort);
		if (portOut.valid == true)
		{
			nameOut = portOut.destName_short;
		}

		//nameIn = bncs_string("%1.%2").arg(m_editInRing).arg(m_editInPort);
		//nameOut = bncs_string("%1.%2").arg(m_editOutRing).arg(m_editOutPort);

		bncs_string mainPort = bncs_string("%1.%2/%3.%4").arg(m_editInRing).arg(m_editInPort).arg(m_editOutRing).arg(m_editOutPort);
		bncs_string portNames = bncs_string("%1/%2").arg(nameIn).arg(nameOut);
		debug("audio_package_conf::updateEditData() level=%1 mainPort=%2", m_level, mainPort);
		//textPut("text", cfCircuit, PNL_MAIN, CF_CIRCUIT);

		bncs_string splitter;
		unsigned int length = (nameIn.length() > nameOut.length()) ? nameIn.length() : nameOut.length();
		for (unsigned int c = 0; c < length; c++)
		{
			splitter.append("�");
		}

		bool sameName = (nameOut == nameIn);
		bool sameRing = (m_editInRing == m_editOutRing);

		bncs_string displayName = "---";

		if (sameRing)
		{
			if (sameName)
			{
				//Simple case: in & out have the same name and are on the same ring
				displayName = nameOut;
			}
			else
			{
				displayName = bncs_string("%1|%2|%3").arg(nameIn.replace('|', ' '))
					.arg(splitter).arg(nameOut.replace('|', ' '));
			}
		}
		else
		{
			displayName = bncs_string("%1:%2|%3|%4:%5")
					.arg(m_editInRing).arg(nameIn.replace('|', ' ')).arg(splitter)
					.arg(m_editOutRing).arg(nameOut.replace('|', ' '));
		}
		textPut("text", displayName, PNL_MAIN, MAIN_PORT);

		if (m_editInRing != m_diffInRing || m_editInPort != m_diffInPort ||
			m_editOutRing != m_diffOutRing || m_editOutPort != m_diffOutPort)
		{
			textPut("statesheet", "level_dirty", PNL_MAIN, MAIN_PORT);
		}
		else
		{
			textPut("statesheet", "level_clean", PNL_MAIN, MAIN_PORT);
		}
	}
}

void audio_package_conf::updateAllocationData(bncs_string data)
{
	debug("audio_package_conf::updateAllocationData() level=%1 data=%2", m_level, data);
	bncs_stringlist sltFields(data);

	if (data.find("conf_number") > -1)
	{
		m_confNumber = sltFields.getNamedParam("conf_number").toInt();
	}

	bncs_string confName = "---";
	if (m_confNumber > 0)
	{
		confName = bncs_string("CNF|%1").arg(m_confNumber);
	}
	textPut("text", confName, PNL_MAIN, ALLOCATED_CONF);

	textPut("statesheet", (m_confNumber > 0) ? "enum_selected" : "enum_deselected", PNL_MAIN, BTN_CONF_ENABLE);
}