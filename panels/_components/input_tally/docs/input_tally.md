# input_tally Component

This simple component displays the source that is feeding an adjustable* device.
2 parameters need to be setup for the component to function, instance and input. The other 2 parameters, single_line, skin, control the look of the component.
This control relies on the instanceToPort dll to function, this needs to be setup correctly. The dest adjust database (db7) of the router needs to be setup, with the instances to adjust.
* "adjustable" means the name exists in "instances.xml" and there is a type handler ("typehandlers.xml") for that type of device.
 
## Commands
| Name | Use |
|------|------|
| instance | Instance of the device to take router names from
| input |  Which input to use if an instance has more than one, if not leave blank. Put in here whatever follows the # in dest adjust, example below. 
| single_line | Whether to display on a single line or multiple (false)(true)
| skin | Which skin to display (default)
| no_instance_in_dest_adjust_error_message | The error message to show if in instance is found in the dest adjust, E.g if the input is fixed this message could be set to "FIXED"
| router_instance | Optional parameter, will only return the instance if found on the router specifed. Useful if the same instance can be on more than one router
| check_instances_within_composite | True/False, If this param is set to True, a soucre will be displayed if an group instances within a composite is on a dest adjust. Real instances will be check first though, so if an instance is on a Dest adjust and also within a composite on a Dest ajust, the instance in dest adjust will always be returned before the instance in the composite.

## Example dest adjusts config

**2 Inputs 1 Output**

For instances that have 2 inputs and one output

0157=32/SHUF001#1

10158=32/SHUF001#2

The source adjust looks like this:

043=32/SHUF001

**2 Inputs 2 Outputs**

To show the first source put 1 in input, to show the second input put 2.

For instances which have 2 inputs and 2 outputs

120=32/UPCONV001_A

121=32/UPCONV002_B

Note a device type and instance is needed for both _A and _B

**1 Input 1 Output (Normal Instances)**

For normal instances 

123=32/ACR003

## Notifications

None

## Stylesheets

None used