#ifndef input_tally_INCLUDED
	#define input_tally_INCLUDED

#include <bncs_script_helper.h>
#include "instanceToPort.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class input_tally : public bncs_script_helper
{
public:
	input_tally( bncs_client_callback * parent, const char* path );
	virtual ~input_tally();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );

	void getIndex();
	void getSource();
	void changeLabel(bncs_string labelText);
	void changeSkin();
	bncs_string m_sInstance;
	bncs_string m_sInput;
	bncs_string m_sSkin;
	
	//Open instanceToPort Dll
	instanceToPort *itp;
	portEntry port;
	
	int m_iDevice;
	int m_iIndex;
	bool m_bSkinChange;
	bool m_bSingleLine;
	bncs_string m_sSingleLine;
private:
	bool m_bInitialPoll;
	bncs_string m_routerInstance;
	bool m_instanceWithinComposite;
	bncs_string m_sErrorMsg;
};


#endif // input_tally_INCLUDED