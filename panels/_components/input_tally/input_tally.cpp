#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "input_tally.h"

#define PANEL_MAIN 1
#define POPUP_SELECT 2


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( input_tally )

// constructor - equivalent to ApplCore STARTUP
input_tally::input_tally( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{

	
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN ,"default.bncs_ui" );
//	setSize( 1 );

	
	//Set Defaults
	m_sSkin = "default";
	itp = instanceToPort::getinstanceToPortObject();
	m_sInstance = "";
	m_sInput = "";
	m_iDevice = 0;
	m_iIndex = 0;
	m_bSkinChange = false;
	m_bSingleLine = false;
	m_sSingleLine = "false";
	m_sErrorMsg = "No instance|in dest adjust";
	m_routerInstance = "";
	m_instanceWithinComposite = false;
	m_bInitialPoll = true;
	getIndex();

}

// destructor - equivalent to ApplCore CLOSEDOWN
input_tally::~input_tally()
{
}

// all button pushes and notifications come here
void input_tally::buttonCallback( buttonNotify *b )
{

	if(b->panel() == PANEL_MAIN )
	{
		if (b->id() == "popup")
		{
			panelPopup(POPUP_SELECT,"..\\..\\components\\popup_router\\popup_layouts\\40x10_tall.bncs_ui" );	
			textPut("router_instance", "rtr_sdi_core", POPUP_SELECT, "source_grid");
		}
	}

}

// all revertives come here
int input_tally::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_iDevice && r->index() == m_iIndex)
	{
		changeLabel(r->sInfo());
	}

	return 0;
}

// all database name changes come back here
void input_tally::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string input_tally::parentCallback( parentNotify *p )
{

	if (p->command() == "instance")
	{
		if(p->value() != m_sInstance)
		{
			m_sInstance = p->value();
//			textPut("instance", m_sInstance, POPUP_SELECT, "popup_router");
			getIndex();
		}
	}
	else if(p->command() == "input")
	{
		if(p->value() != m_sInput)
		{
			m_sInput = p->value();
			getIndex();
		}
	}
	else if (p->command() == "single_line")
	{
		if(p->value() != m_sSingleLine)
		{
			m_sSingleLine = p->value();
			m_bSingleLine = p->value().upper() == "TRUE";
		}
	}
	else if(p->command() == "skin")
	{
		if(p->value() != m_sSkin)
		{
			m_sSkin = p->value();
			m_bSkinChange = true;
			getIndex();
		}
	}
	else if(p->command() == "no_instance_in_dest_adjust_error_message")
	{
		if(p->value() != "")
			m_sErrorMsg = p->value();
		else
			m_sErrorMsg = "No instance|in dest adjust";
	}
	else if(p->command() == "router_instance")
	{
		if(p->value() != m_routerInstance)
		{
			m_routerInstance = p->value();
			getIndex();
		}

	}
	else if(p->command() == "check_instances_within_composite")
	{
		bool tempBoolState = (p->value().lower() == "true" || p->value() == "1");
		if(tempBoolState != m_instanceWithinComposite)
		{
			m_instanceWithinComposite = tempBoolState;
			getIndex();
		}
	}
	else
	{
		bncs_stringlist sl_pramlist;
		sl_pramlist.append("input=" + m_sInput);
		sl_pramlist.append("single_line=" + m_sSingleLine);
		sl_pramlist.append("skin=" + m_sSkin);
		sl_pramlist.append("no_instance_in_dest_adjust_error_message="+m_sErrorMsg);
		sl_pramlist.append("router_instance="+m_routerInstance);
		sl_pramlist.append("check_instances_within_composite="+bncs_string((m_instanceWithinComposite == true?"true":"false")));
		return sl_pramlist.toString('\n');
	}
	return "";
}

// timer events come here
void input_tally::timerCallback( int id )
{

}


void input_tally::getIndex()
{
	//debug("input_tally::getIndex() Entering Function");
	changeSkin();
	if(m_sInstance.length() != 0)
	{
		//debug("input_tally::getIndex() m_sInstance='%1'",m_sInstance);
		bncs_string sErrorMsg;
		if(itp->getStatus(sErrorMsg) != true)
		{
			if(m_routerInstance.length() == 0)
			{
				//debug("itp->getValue(m_sInstance:%1,m_sInput:%2, m_instanceWithinComposite:%3)",m_sInstance,m_sInput,m_instanceWithinComposite);
				if(itp->getValue(m_sInstance,m_sInput, port, m_instanceWithinComposite) != true)
				{
					m_iDevice = port.m_device;
					m_iIndex = port.m_index;
					textPut(bncs_string("dest_index=%1").arg(port.m_index), PANEL_MAIN, "popup_router");
					getSource();
				}
				else
				{
					changeLabel(m_sErrorMsg);
				}
			}
			else
			{
				//debug("itp->getValue(m_sInstance:%1,m_sInput:%2, m_routerInstance:%3, m_instanceWithinComposite:%4)",m_sInstance,m_sInput,m_routerInstance,m_instanceWithinComposite);
				if(itp->getValue(m_sInstance,m_sInput, port,m_routerInstance, m_instanceWithinComposite) != true)
				{
					m_iDevice = port.m_device;
					m_iIndex = port.m_index;
					textPut(bncs_string("dest_index=%1").arg(port.m_index), PANEL_MAIN, "popup_router");
					getSource();
				}
				else
				{
					changeLabel(m_sErrorMsg);
					textPut(bncs_string("dest_index=%1").arg(port.m_index), PANEL_MAIN, "popup_router");
				}
			}
		}
		else
		{
			changeLabel(sErrorMsg);
			textPut(bncs_string("dest_index=%1").arg(port.m_index), PANEL_MAIN, "popup_router");
		}
	}
	else
	{
		changeLabel("Instance not set");
		textPut(bncs_string("dest_index=%1").arg(port.m_index), PANEL_MAIN, "popup_router");
	}
	debug("input_tally::getIndex() Instance:%1 PORT port.m_device:%2 port.m_index:%3",m_sInstance, port.m_device,port.m_index);
}

void input_tally::getSource()
{
	if(m_iDevice != 0 && m_iIndex != 0)
	{
		routerRegister(m_iDevice,m_iIndex,m_iIndex);
		if(m_bInitialPoll == true)
		{
			m_bInitialPoll = false;
			changeLabel("No Reply from Router");
		}
		routerPoll(m_iDevice,m_iIndex, m_iIndex);
	}
	else
	{
		changeLabel(m_sErrorMsg);
	}
}

void input_tally::changeLabel(bncs_string labelText)
{
	//debug("input_tally::changeLabel() Function Called labelText:'%1'", labelText);
	if(m_bSingleLine)
	{
		labelText.replace('|',' ');
	}
	textPut("text",labelText,PANEL_MAIN ,"tally");
}

void input_tally::changeSkin()
{
	if(m_bSkinChange)
	{
		panelDestroy(PANEL_MAIN );
		panelShow(PANEL_MAIN ,m_sSkin + ".bncs_ui");
		//setSize( 1 );
	}
}