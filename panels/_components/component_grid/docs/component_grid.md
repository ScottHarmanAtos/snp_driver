# Component Grid

Create a grid of one component, this can be used for source and dest grids where special components are needed.

Like a regular name grid this component is targeted with an instance, offset and a page. This information is passed to one of the components in the grid by the command getIndex(instance,offset, page). It is expected the component well return a string list of indexes, which will then be used to target all the loaded components in the grid.

## Components using the grid need to implment
* The component itself needs to know how its mapping is handled. The Component grid has no knowledge of this.
* The component needs to handle the take
* Component should highlight on selection, if that is required

### Host notifies
* button=released
* index=[value]

### Parent Callbacks

 command | mandatory | description 
---------|-----------|-------------
deselect | no | Should unhighlight on receiving the command
take | no | take=source, the subs are also passed though so extra data can be passed in these
undo | no |  undo the last take
index | yes | The index this button is targeted with, in mapping mode the string **"map_mode"** will be passed if the index is 0, the control should be enabled but clear.
map_mode | no | Only needed when the panel should be used for mapping, boolean true or false, true if the panel is being used for mapping, this allows for things like a different layout in mapping mode.
save.instance.offset.page | no | To save the mapping, subs(instance,offset,page) 

All are mandatory if mapping is enabled, except take.

#### save

This is an example of the save code for DB mapping

```cpp
if (p->command() == "save")
{
    if (p->subs() == 3)
    {
        bncs_string instance = p->sub(0);
        int offset = p->sub(1).toInt();
        int page = p->sub(2).toInt();

        int device = 0;
        if (instance.length() != 0 && getDev(instance, &device))
        {
            routerModify(device, DATABASE_MAPPING, offset + page, p->value(), true);
        }
    }
}
```

### TextGet
A special textGet, to get the mapping. The component grid will send the command to a single component.
**getIndex.instance.offset.page** returns comma delimited list of indexes.

The code to handle this should look something like this. This example does a normal Router Name of DB8
```cpp
if (p->command() == "return" && p->value().startsWith("getIndex"))
{
    bncs_stringlist sl(p->value(),'.');

    if (sl.count() == 4)
    {
        bncs_string instance = sl[1];
        bncs_string offset = sl[2];
        bncs_string page = sl[3];

        int device = 0;
        if (instance.length() != 0 && getDev(instance, &device))
        {
            bncs_string value;
            routerName(device, DATABASE_MAPPING, offset.toInt() + page.toInt(), value);

            return bncs_string("%1=%2").arg(p->value()).arg(value);
        }
        else
        {
            return bncs_string("%1=").arg(p->value());
        }
    }
}
```

Offset and Page are stored as strings so any information that is useful could be passed using them.

For instance if your group of components load from an xml file rather then a database.
