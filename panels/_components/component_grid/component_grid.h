#ifndef component_grid_INCLUDED
#define component_grid_INCLUDED

#include <bncs_script_helper.h>
#include <vector>
#include <set>
#include <map>

#ifdef WIN32
#ifdef DOEXPORT_SCRIPT
#define EXPORT_SCRIPT __declspec(dllexport) 
#else
#define EXPORT_SCRIPT 
#endif
#else 
#define EXPORT_SCRIPT
#endif


class component_grid : public bncs_script_helper
{
public:
	component_grid(bncs_client_callback * parent, const char* path);
	virtual ~component_grid();

	void buttonCallback(buttonNotify *b);
	int revertiveCallback(revertiveNotify * r);
	void databaseCallback(revertiveNotify * r);
	bncs_string parentCallback(parentNotify *p);
	void timerCallback(int);

private:
	bncs_string m_instance;
	int m_device;

	int m_width;
	int m_height;
	int m_rows;
	int m_cols;
	int m_horizontal_spacing;
	int m_vertical_spacing;

	bncs_string m_offset;
	bncs_string m_page;

	int m_buttonCount;

	enum Setting
	{
		NotFound,
		Found,
		FoundAndChanged
	};

	bool setValue(const bncs_string& name, int &value, int newValue, bool loadNow = true);
	bool setValue(const bncs_string& name, bncs_string &value, const bncs_string& newValue, bool loadNow = true);
	bool setValue(const bncs_string& name, bool &value, const bncs_string& newValue, bool loadNow = true);

	bool m_mapping;

	bool m_isDirty;
	int m_selectedButton;
	bncs_stringlist m_new_mapping;


	void load();
	void draw();

	void target(const bncs_string& offset, const bncs_string& page);

	void save(const bncs_string& offset, const bncs_string& page);
	void cancel();

	void setMapping(const bncs_string& index);
	void mapping(const bncs_string& index);

	bool m_firstDraw;

	bool m_mapping_clicked_waiting_for_index;
	bncs_string m_last_index_received;

	bncs_string m_component;

	map<bncs_string,bncs_string> m_extraParameters;

	//This is a list of all the parameter are expect to recieve before doing anything useful.
	//It includes those supplied by the component.
	set<bncs_string> m_startup;

	void removeStartup(const bncs_string& name);
	
	void setMultipleValues(const bncs_string& commands);

	//Bool Item Changed, Bool Item Found
	Setting processCommand(const bncs_string &command, const bncs_string &value, bool loadNow);

	Setting processComponentCommands(const bncs_string &command, const bncs_string &value);

	bool m_componentChanged;

};


#endif // component_grid_INCLUDED