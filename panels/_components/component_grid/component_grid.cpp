#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "component_grid.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

const int DATABASE_MAPPING(8);


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(component_grid)

// constructor - equivalent to ApplCore STARTUP
component_grid::component_grid(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path)
{
	bncs_string startupValues[] = { "instance","width","height","rows","cols","horizontal_spacing","vertical_spacing","offset","page","map_mode","component" };
	m_startup = set<bncs_string>(startupValues, startupValues + 10);

	// show a panel from file component_grid.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PNL_MAIN, "component_grid.bncs_ui");

	//These are the defaults
	m_width = 490;
	m_height = 550;
	m_rows = 5;
	m_cols = 4;
	m_horizontal_spacing = 10;
	m_vertical_spacing = 10;

	m_offset = 0;
	m_page = 0;

	m_buttonCount = 0;
	//debug("component_grid::component_grid m_buttonCount:%1", m_buttonCount);

	m_device = 0;
	m_mapping = false;

	m_isDirty = false;
	m_selectedButton = 0;

	m_mapping_clicked_waiting_for_index = false;

	m_last_index_received = "0";

	m_firstDraw = true;
	m_componentChanged = false;

	//timerStart(TIMER_SETUP, 2);
}

// destructor - equivalent to ApplCore CLOSEDOWN
component_grid::~component_grid()
{
}

// all button pushes and notifications come here
void component_grid::buttonCallback(buttonNotify *b)
{
	b->dump("component_grid::buttonCallback");
	//We need to store which the last pressed button was.
	//We also need to tell all the other buttons to unselect.
	if (b->command() == "button" && b->value() == "released")
	{
		if (m_mapping == true)
		{
			m_mapping_clicked_waiting_for_index = true;
		}

		if (b->id() != m_selectedButton)
		{
			if (m_selectedButton != 0)
				textPut("deselect", PNL_MAIN, m_selectedButton);
			m_selectedButton = b->id();
		}
		hostNotify("button=released");

		//Special thing so mapping works ok with select none
		if (m_mapping == true && m_last_index_received == 0)
		{
			setMapping(0);
		}
	}
	else if (b->command() == "index")
	{
		if (m_mapping == false)
		{
			bncs_string subs;
			for (int i = 0; i < b->subs(); i++)
			{
				subs.append(".");
				subs.append(b->sub(i));
			}
			hostNotify(bncs_string("index%1=%2").arg(subs).arg(b->value()));
		}
	}
	else
	{
		//Anything thing else we recieve lets print it out
		if (m_mapping == false)
		{
			hostNotify(bncs_string(b->command().append("=").append(b->value())));
		}
	}
}

// all revertives come here
int component_grid::revertiveCallback(revertiveNotify * r)
{
	return 0;
}

// all database name changes come back here
void component_grid::databaseCallback(revertiveNotify * r)
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string component_grid::parentCallback(parentNotify *p)
{
	p->dump("component_grid::parentCallback");
	if (p->command() == "return")
	{
		if (p->value() == "all")
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;

			sl << bncs_string("width=%1").arg(m_width);
			sl << bncs_string("height=%1").arg(m_height);
			sl << bncs_string("rows=%1").arg(m_rows);
			sl << bncs_string("cols=%1").arg(m_cols);
			sl << bncs_string("horizontal_spacing=%1").arg(m_horizontal_spacing);
			sl << bncs_string("vertical_spacing=%1").arg(m_vertical_spacing);
			sl << bncs_string("offset=%1").arg(m_offset);
			sl << bncs_string("page=%1").arg(m_page);
			sl << bncs_string("map_mode=%1").arg(m_mapping ? "true" : "false");
			sl << bncs_string("component=%1").arg(m_component);

			for (map<bncs_string, bncs_string>::iterator it = m_extraParameters.begin(); it != m_extraParameters.end(); ++it)
				sl << bncs_string("_").append(it->first).append("=").append(it->second);

			return sl.toString('\n');
		}

	}
	else if (processCommand(p->command(), p->value(), true) != Setting::NotFound)//Set stuff in this bit
	{
	}
	else if (p->command() == "multiSetupCommand")
	{
		//Deal with multiple commands at once
		setMultipleValues(p->value());
	}
	//Commands
	else if (p->command() == "_commands")
	{
		bncs_stringlist sl;

		sl << bncs_string("save");
		sl << bncs_string("cancel");
		sl << bncs_string("page=<value>");
		sl << bncs_string("index=<index>");
		sl << bncs_string("multiSetupCommand=<send#multiple,commands#likethis>");

		return sl.toString('\n');
	}

	else if (p->command() == "_events")
	{
		bncs_stringlist sl;

		sl << bncs_string("button=released");
		sl << bncs_string("index=<value>");
		sl << bncs_string("dirty");

		return sl.toString('\n');
	}

	//Mapping
	else if (p->command() == "save")
	{
		save(m_offset,m_page);
	}
	else if (p->command() == "cancel")
	{
		cancel();
	}
	else if (p->command() == "deselect")
	{
		if (m_selectedButton != 0)
		{
			textPut("deselect", PNL_MAIN, m_selectedButton);
			m_selectedButton = 0;
		}
	}
	else if (p->command() == "index")
	{
		setMapping(p->value()); 
	}
	else if (p->command() == "take")
	{
		debug("component_grid::parentCallback take:'%1' to button %2", p->value(), m_selectedButton);
		bncs_string s = "take";
		for (int i = 0; i < p->subs(); i++)
		{
			s += "." + p->sub(i);
		}
		textPut(s, p->value(), PNL_MAIN, m_selectedButton);
	}
	else if (p->command() == "undo")
	{
		bncs_string s = "undo";
		for (int i = 0; i < p->subs(); i++)
		{
			s += "." + p->sub(i);
		}
		textPut(s, p->value(), PNL_MAIN, m_selectedButton);
	}
	else if (processComponentCommands(p->command(), p->value()) != Setting::NotFound)
	{
		target(m_offset, m_page);
	}

	return "";
}

// timer events come here
void component_grid::timerCallback(int id)
{
	switch (id)
	{
	case TIMER_SETUP:
		timerStop(id);
		load();
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void component_grid::removeStartup(const bncs_string& name)
{
	//debug("component_grid::removeStartup() Name:%1", name);
	set<bncs_string>::iterator it = m_startup.find(name);
	if (it != m_startup.end())
	{
		//debug("component_grid::removeStartup() Name Remove:%1", name);
		m_startup.erase(it);
	}
}

bool component_grid::setValue(const bncs_string& name, int &value, int newValue, bool loadNow)
{
	removeStartup(name);
	if (newValue != value)
	{
		value = newValue;
		if (loadNow) load();
		return true;
	}
	return false;
}

bool component_grid::setValue(const bncs_string& name, bncs_string &value, const bncs_string& newValue, bool loadNow)
{
	removeStartup(name);
	if (newValue != value)
	{
		value = newValue;
		if (loadNow) load();
		return true;
	}
	return false;
}

bool component_grid::setValue(const bncs_string& name, bool &value, const bncs_string& newValue, bool loadNow)
{
	bool v = newValue.lower() == "true" ? true : false;

	removeStartup(name);
	if (v != value)
	{
		value = v;
		if (loadNow) load();
		return true;
	}
	return false;
}


void component_grid::load()
{
	debug("component_grid::load() m_startup.size() :%1", (int)m_startup.size());
	if (m_startup.size() != 0)
	{
		debug("component_grid::load() Size:%1", (int)m_startup.size());
		return;
	}

	//debug("component_grid::load() checks width:%1 height:%2 row:%3 cols:%4", m_width, m_height, m_rows, m_cols);
	//Check that we have all the values we need
	if (!(m_width > 0 && m_height > 0 && m_rows > 0 && m_cols > 0))
		return;

	//debug("component_grid::load() all checks good");
	if (m_instance.length() > 0)
		getDev(m_instance, &m_device);

	//debug("component_grid::load() draw some stuff");
	draw();

	//Only target on 0, as we don't have any more commands to send
	target(m_offset, m_page);
}

void component_grid::draw()
{
	debug("component_grid::draw() FirstDraw:%1 ComponentChanged:%2", m_firstDraw?"true":"false",m_componentChanged?"true":"false");

	if (!m_firstDraw)
	{
		//debug("component_grid::draw() panelDestroy(PNL_MAIN)");

		panelDestroy(PNL_MAIN);
		panelShow(PNL_MAIN, "component_grid.bncs_ui");
	}

	setSize(m_width, m_height);

	m_buttonCount = m_rows * m_cols;
	//debug("component_grid::draw() m_buttonCount:%1", m_buttonCount);

	//Get button widths
	double buttonWidth = (m_width - ((m_cols - 1) * m_horizontal_spacing)) / m_cols;
	double buttonHeight = (m_height - ((m_rows - 1)  * m_vertical_spacing)) / m_rows;

	int index = 1;
	double y = 0;

	bool f = true;;

	if (m_firstDraw || m_componentChanged) //Only do this the first time or if the component type has changed
	{
		//debug("component_grid::draw() Clear Extra Parameters");
		m_extraParameters.clear();
	}

	for (int r = 0; r < m_rows; r++)
	{
		double x = 0;
		for (int c = 0; c < m_cols; c++)
		{
			controlCreate(PNL_MAIN, index, "bncs_script_loader", (int)x, (int)y, (int)buttonWidth, (int)buttonHeight, bncs_string("scaleWithCanvas=true\nscriptname=%1\ninstance=%2\nmap_mode=%3").arg(m_component).arg(m_instance).arg(m_mapping?"true":"false"));
			
			if (m_firstDraw || m_componentChanged) //Only do this the first time, or if the component type has changed
			{
				if (f == true) // only do this on the first iteration
				{
					f = false;

					bncs_string values;
					textGet("all", PNL_MAIN, index, values);
					bncs_stringlist sl(values, '\n');

					for (bncs_stringlist::iterator it = sl.begin(); it != sl.end(); ++it)
					{
						if (!(*it).startsWith("scriptname") && !(*it).startsWith("index") && !(*it).startsWith("scaleWithCanvas") && !(*it).startsWith("instance"))
						{
							bncs_string k, v;
							(*it).split('=', k, v);
							m_extraParameters[k] = v;
							m_startup.insert(bncs_string("_").append(k));
						}
					}
				}
			}
			else
			{
				//If this isn't the first time we have been drawn, lets reapply previous settings.
				for (map<bncs_string, bncs_string>::const_iterator it = m_extraParameters.begin(); m_extraParameters.end() != it; ++it)
				{
					processComponentCommands(bncs_string("_").append(it->first), it->second);
				}
			}

			index++;
			x = x + m_horizontal_spacing + buttonWidth;
		}
		y = y + m_vertical_spacing + buttonHeight;
	}


	debug("component_grid::draw() Don't retarget components with the old data");
	m_firstDraw = false;

	m_componentChanged = false;

}


void component_grid::target(const bncs_string& offset, const bncs_string& page)
{

	//Only target on 0, as we don't have any more commands to send
	if (m_startup.size() != 0)
		return;

	debug("component_grid::target Start");

	bncs_string _indexes;
	//debug("component_grid::target %1", bncs_string("getIndex.%1.%2.%3").arg(m_instance).arg(offset).arg(page));
	textGet(bncs_string("getIndex.%1.%2.%3").arg(m_instance).arg(offset).arg(page), PNL_MAIN, 1, _indexes);
	//debug("component_grid::target get:%1 response:%2", bncs_string("getIndex.%1.%2.%3").arg(m_instance).arg(offset).arg(page),_indexes);
	
	bncs_stringlist indexes;
	if (_indexes != "!!!") //Check for bad router data.
		indexes = bncs_stringlist(_indexes);

	bncs_stringlist last_mapping = m_new_mapping;

	//New page, set to none selected
	hostNotify(bncs_string("index=-1"));

	//Inserts extra items in the list to ensure its the same size at the button count, so all buttons have something set.
	if (indexes.count() < m_buttonCount)
	{
		bncs_stringlist sl;
		sl.setCount(m_buttonCount - indexes.count()); 
		indexes + sl;
	}


	for (int index = 1, i = 0; index <= m_buttonCount; index++, i++)
	{
		bool isMapping = false;
		if (indexes[i].length() == 0 || indexes[i].toInt() <= 0)
		{
			if (m_mapping == false)
			{
				//debug("component_grid::target() but:%1 Disable", index);
				controlEnable(PNL_MAIN, index); //Hack to fix disable display error
				controlDisable(PNL_MAIN, index);
				textPut("index", "", PNL_MAIN, index);
			}
			else if (m_mapping == true)
			{
				isMapping = true;
				textPut("index", "map_mode", PNL_MAIN, index);
			}
		}
		else
		{
			//debug("component_grid::target() but:%1 Enable", index);
			controlEnable(PNL_MAIN, index);
		}
		//Don't retarget if its the same
		if (last_mapping[i] != indexes[i] && isMapping == false)
		{
			//debug("component_grid::target() but:%1 index:%2", index, indexes[i]);
			textPut("index", indexes[i], PNL_MAIN, index);
		}
	}
	
	m_new_mapping = indexes;

	debug("component_grid::target End");
}

void component_grid::save(const bncs_string& offset, const bncs_string& page)
{
	if (m_mapping == false)
		return;

	textPut(bncs_string("save.%1.%2.%3").arg(m_instance).arg(offset).arg(page), m_new_mapping.toString(), PNL_MAIN, 1);
}


void component_grid::cancel()
{
	if (m_mapping == false)
		return;

	target(m_offset, m_page);
	m_isDirty = false;
}

void component_grid::setMapping(const bncs_string& newIndex)
{
	//Get last index sent to us.
	bncs_string index = m_last_index_received;
	m_last_index_received = newIndex;
	mapping(index);
	m_mapping_clicked_waiting_for_index = false;
}

void component_grid::mapping(const bncs_string& index)
{
	//debug("component_grid::mapping index:%1", index);
	if (m_mapping == false || m_selectedButton == 0 || m_page == 0 || m_mapping_clicked_waiting_for_index == false)
		return;

	if (m_isDirty == false)
	{
		m_isDirty = true;
		hostNotify("dirty");
	}

	//debug("component_grid::mapping index:%1 go",index);
	if (m_mapping == true && index <= 0)
		textPut("index", "map_mode", PNL_MAIN, m_selectedButton);
	else
		textPut("index", index, PNL_MAIN, m_selectedButton);
	m_new_mapping[m_selectedButton - 1] = index;
}

void component_grid::setMultipleValues(const bncs_string& commands)
{
	bool parentChanged = false;

	bncs_stringlist sl(commands, ',');
	for (int i = 0; i < sl.count(); i++)
	{
		//Load each item passed
		bncs_string k, v;

		sl[i].split('#', k, v);

		if (processCommand(k, v, false) == Setting::FoundAndChanged)
		{
			parentChanged = true;
		}
	}

	//Store this, before load
	bool componentTypeChanged = m_componentChanged;

	if (parentChanged)
	{
		//debug("component_grid::setMultipleValue somethings changed, load");
		draw();
	}

	//bool componentsChanged = false;

	//Store a local list
	bncs_stringlist extraParams;

	//Check if there are any component values to set	
	for (int i = 0; i < sl.count(); i++)
	{
		bncs_string k, v;

		sl[i].split('#', k, v);

		Setting found = processComponentCommands(k, v);
		if (found == Setting::FoundAndChanged)
		{
			//componentsChanged = true;
		}
		if (found != Setting::NotFound)
		{
			//Store if it was found at all
			extraParams.append(k.right(k.length() - 1));
		}
	}

	//If the component hasn't changed, set all the other parameters we already know, only if the parent has changed though
	if (!componentTypeChanged && parentChanged == true)
	{
		for (map<bncs_string, bncs_string>::const_iterator it = m_extraParameters.begin(); m_extraParameters.end() != it; ++it)
		{
			if (extraParams.find(it->first) == -1)
			{
				//debug("component_grid::setMultipleValue Set Old Values K:%1 V:%2", it->first, it->second);
				processComponentCommands(bncs_string("_").append(it->first), it->second);
			}
		}
	}


	//If the type hasn't changed, ensure the mapping is cleared
	//if (!componentTypeChanged)
	m_new_mapping.clear(); //Always clear the mapping?

	target(m_offset, m_page);
}

component_grid::Setting component_grid::processComponentCommands(const bncs_string &command, const bncs_string &value)
{
	Setting ret = Setting::NotFound;
	if (command.startsWith("_"))
	{
		bncs_string _command = command.right(command.length() - 1);
		//Check we care about it
		if (m_extraParameters.find(_command) != m_extraParameters.end())
		{
			//debug("component_grid::processComponentCommands changed command:%1 value:%2",command,value);
			if (m_extraParameters[_command] != value)
			{
				ret = Setting::FoundAndChanged;
				m_extraParameters[_command] = value; //Store the new value
			}
			else
			{
				ret = Setting::Found;
			}
			
			//debug("component_grid::parentCallback set:%1 count:%2", p->value(), m_buttonCount);
			//Always retarget? Just incase
			for (int i = 1; i <= m_buttonCount; i++)
			{
				textPut(_command, value, PNL_MAIN, i);
			}
		}
		removeStartup(command);
	}
	return ret;

}

component_grid::Setting component_grid::processCommand(const bncs_string &command, const bncs_string &value, bool loadNow)
{
	bool changed = false;
	//debug("component_grid::processCommand command:%1 value:%2", command, value);
	if (command == "instance" && value != m_instance)
		changed = setValue(command, m_instance, value, true);
	else if (command == "width")
		changed = setValue(command, m_width, value, loadNow);
	else if (command == "height")
		changed = setValue(command, m_height, value, loadNow);
	else if (command == "rows")
		changed = setValue(command, m_rows, value, loadNow);
	else if (command == "cols")
		changed = setValue(command, m_cols, value, loadNow);
	else if (command == "horizontal_spacing")
		changed =  setValue(command, m_horizontal_spacing, value, loadNow);
	else if (command == "vertical_spacing")
		changed = setValue(command, m_vertical_spacing, value, loadNow);
	else if (command == "offset")
	{
		m_offset = value;
		removeStartup(command);
		//The mapping index to look at
		if(loadNow) target(m_offset, m_page);
		changed = true;
	}
	else if (command == "component")
	{
		//This needs to be done before the setValue as set value will call the load
		if (m_component != value)
			m_componentChanged = true;

		changed = setValue(command, m_component, value, loadNow);
	}
	else if (command == "page")
	{
		m_page = value;
		removeStartup(command);
		//The mapping index to look at
		if (loadNow) target(m_offset, m_page);
		changed = true;
	}
	else if (command == "map_mode")
		changed = setValue(command, m_mapping, value, loadNow); //Panel reload needed to set map mode on buttons
	else
	{
		return Setting::NotFound;
	}
	if (changed)
	{
		//debug("component_grid::processCommand command:%1 value:%2 CHANGED", command, value);
		return Setting::FoundAndChanged;
	}
	
	//debug("component_grid::processCommand command:%1 value:%2 Not changed", command, value);
	return Setting::Found;
}