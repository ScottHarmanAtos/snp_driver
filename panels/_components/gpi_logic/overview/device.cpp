#include "device.h"


device::device()
{
	inputPageCount = 0;
	outputPageCount = 0;
	inputsExist = false;
	outputsExist = false;
	inputPageSize = 0;
	outputPageSize = 0;

	deviceID = 0;

	inputCount = 0;
	outputCount = 0;
	comms = false;
}

device::device(instance inputs, instance pti_inputs) : device()
{
	inputsExist = true;
	outputsExist = false;
	outputPageCount = 0;
	this->outputCount = 0;
	this->inputs = inputs;
	this->pti_inputs = pti_inputs;
}
device::device(instance inputs, instance pti_inputs, instance outputs, instance pti_outputs, instance routes) : device()
{
	inputsExist = true;
	outputsExist = true;
	this->inputs = inputs;
	this->pti_inputs = pti_inputs;
	this->outputs = outputs;
	this->pti_outputs = pti_outputs;
	this->routes = routes;
}
device::device(instance outputs, instance pti_outputs, instance routes) : device()
{
	inputsExist = false;
	outputsExist = true;
	inputPageCount = 0;
	this->inputCount = 0;
	this->outputs = outputs;
	this->pti_outputs = pti_outputs;
	this->routes = routes;
}

void device::setInput(int inputCount, int pageSize)
{
	inputPageSize = pageSize;

	OutputDebugString(bncs_string("device::setInput inputCount:%1").arg(inputCount));
	this->inputCount = inputCount;
	
	int pageCount = 1;
	list<gpi*> pagelist;
	bool pageFull = false;
	int pageSlot = 1;

	for (int i = 1; i <= this->inputCount; ++i)
	{
		pageFull = false;
		OutputDebugString(bncs_string("device::setInput Creating Input:%1").arg(bncs_string(i,'0',2U)));
		gpi* g = new gpi();

		g->input = true;
		g->deviceID = deviceID;
		g->gpiID = i;
		g->device = this->inputs.device;
		g->slot = this->inputs.offset + i;


		g->ptiDevice = this->pti_inputs.device;
		g->ptiSlot = this->pti_inputs.offset + i;
		g->page = pageCount;
		g->pageSlot = pageSlot;

		inputGpis.push_back(g);
		pagelist.push_back(g);

		pageSlot++;
		if ((i) % inputPageSize == 0)
		{
			pageFull = true;
			inputPages[pageCount] = pagelist;
			inputPageCount = pageCount;

			pagelist = list<gpi*>();
			pageCount++;
			pageSlot = 1;
		}
	}
	if (pageFull != true)
	{
		inputPages[pageCount] = pagelist;
		inputPageCount = pageCount;
	}
}

void device::setOutput(int outputCount, int pageSize)
{
	outputPageSize = pageSize;

	OutputDebugString(bncs_string("device::setOutput outputCount:%1").arg(outputCount));
	this->outputCount = outputCount;

	int pageCount = 1;
	list<gpi*> pagelist;
	bool pageFull = false;
	int pageSlot = 1;

	if (outputCount == 0)
	{
		outputPageCount = 0;
		return;
	}

	for (int i = 1; i <= this->outputCount; ++i)
	{
		pageFull = false;
		OutputDebugString(bncs_string("device::setOutput Creating Output:%1").arg(bncs_string(i, '0', 2U)));
		gpi* g = new gpi();

		g->input = false;
		g->deviceID = deviceID;
		g->gpiID = i;
		g->device = this->outputs.device;
		g->slot = this->outputs.offset + i;

		g->ptiDevice = this->pti_outputs.device;
		g->ptiSlot = this->pti_outputs.offset + i;

		g->routerDevice = this->routes.device;
		g->routerSlot = this->routes.offset + i;

		g->page = pageCount;
		g->pageSlot = pageSlot;

		outputGpis.push_back(g);
		pagelist.push_back(g);

		pageSlot++;
		if ((i) % outputPageSize == 0)
		{
			pageFull = true;
			outputPages[pageCount] = pagelist;
			outputPageCount = pageCount;

			pagelist = list<gpi*>();
			pageCount++;
			pageSlot = 1;
		}
	}
	if (pageFull != true)
	{
		outputPages[pageCount] = pagelist;
		outputPageCount = pageCount;
	}



}


device::~device()
{
	inputGpis.clear();
	outputGpis.clear();
	inputPages.clear();
	outputPages.clear();

}
