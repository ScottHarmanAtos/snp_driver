#include "instance.h"


instance::instance()
{
	valid = false;
	device = 0;
	offset = 0;
}

instance::instance(bncs_string name, int device, int offset)
{
	valid = true;
	this->device = device;
	this->offset = offset;
	this->name = name;
}

instance::~instance()
{
}
