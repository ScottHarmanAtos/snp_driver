#pragma once
#include "bncs_script_helper.h"
#include <vector>
#include "states.h"
class gpi
{
public:
	gpi();
	~gpi();

	bncs_string name;

	int device;
	int slot;
	bncs_string value;

	int ptiDevice;
	int ptiSlot;
	//bncs_string ptiValue;
	vector<gpi*> ptiValue;

	int routerDevice;
	int routerSlot;
	vector<gpi*> routerValue;
	bool routeInput;

	int page;		//Page of the device we are part of
	int pageSlot;	//The number on the page we are

	int deviceID; //Id of the device we are part of
	int gpiID;

	bool input; //If its an input or an output
	bool noRouting;

	bncs_string getIdentifier();

	bool routeContainsID(gpi* g);
	bncs_string createNewRoute(gpi* g);

	States state;
};

