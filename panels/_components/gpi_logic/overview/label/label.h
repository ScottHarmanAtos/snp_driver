#ifndef label_INCLUDED
	#define label_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class item
{
public:
	bncs_string type;
	bncs_string id;
	bncs_string value;

	item(bncs_string type, bncs_string id)
	{
		this->type = type;
		this->id = id;
	}
	item()
	{

	}
	/*
	item( item& i)
	{
		this->id = i.id;
		this->type = i.type;
		this->value = i.value;
	}*/
};

class label : public bncs_script_helper
{
public:
	label( bncs_client_callback * parent, const char* path );
	virtual ~label();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	item id = item("text", "id");
	item text = item("text", "text");
	item state_text = item("text", "state");
	item state_state = item("statesheet", "state");
	item left_pix = item("pixmap", "left_pix");
	item right_pix = item("pixmap", "right_pix");

	void ifDifferent(item &item, const bncs_string ne);
	
};


#endif // label_INCLUDED