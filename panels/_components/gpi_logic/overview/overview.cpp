#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "instanceLookup.h"
#include "overview.h"
#include <set>

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( overview )

#define PANEL_MAIN		1
#define POPUP_INPUT		2
#define POPUP_OUTPUT	3

#define GPI_NORMAL		0
#define GPI_OFF			0
#define GPI_ON			1
#define FORCE_OFF		2
#define FORCE_ON		3

#define ADD_INPUT		"add_input"
#define COPY_OUTPUT		"copy_output"
#define CLEAR_INPUTS	"clear_inputs"
#define CLEAR_SELECTED_INPUT	"clear_selected_input"
#define CLEAR_OUTPUT	"clear_output"

#define LIST_INPUT_ROUTES "lvw_input_routes"

#define DATABASE_INPUT_NAME 0
#define DATABASE_OUTPUT_NAME 1
#define DATABASE_SOFT_IN_AND_OUTS 6
#define PAGE_SIZE 32

// constructor - equivalent to ApplCore STARTUP
overview::overview( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	//m_intCurrentInput = 0;
	//m_intCurrentOutput = 0;
	//m_intSelectedInput = 0;
	//m_intSelectedOutput = 0;
	//m_intOutputCopiedToCurrentOutput = 0;
	//m_blnCurrentOutputNonRoutable = false;
	//m_intUnit = 0;
	//m_intDevice = 291;
	//m_intPortToRemove = -1;
	
	inputsSelected = true;

	selectedInput = NULL;
	selectedInputForPopup = NULL;
	selectedOutputForPopup = NULL;
	selectedRoutedInput = NULL;
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel

	//init();

	unitInputCountOnPanel = getIdList(PANEL_MAIN,"inputUnitName_").count();
	unitOutputCountOnPanel = getIdList(PANEL_MAIN, "outputUnitName_").count();

}

// destructor - equivalent to ApplCore CLOSEDOWN
overview::~overview()
{

	debug("overview() Destructor");
	//Get rid of all the gpis that are floating in memory
	inputList.clear();
	outputList.clear();
	selectedInputList.clear();
	selectedOutputList.clear();
	selectedInput = NULL;
	selectedInputForPopup = NULL;
	selectedOutputForPopup = NULL;
	selectedRoutedInput = NULL;

	map<int, gpi*>::iterator it = allGpis.begin();
	set<gpi*>uniqueValues;
	while (it != allGpis.end())
	{
		uniqueValues.insert(it->second);
		++it;
	}
	allGpis.clear();

	//This step needs to be done as we have multiple pointers to the same gpi, if we try to delete in the map we will break the iterators
	set<gpi*>::iterator itSet = uniqueValues.begin();
	while (itSet != uniqueValues.end())
	{
		delete *itSet;
		++itSet;
	}

	debug("Finished");

}

// all button pushes and notifications come here
void overview::buttonCallback( buttonNotify *b )
{
	//Exmaples:
	//unit_1_page_1
	//inputusage_01
	bncs_stringlist sltControlName = bncs_stringlist(b->id(), '_');
	bncs_string strControlPrefix = sltControlName[0];
	int intControlIndex = sltControlName[1].toInt();

	bncs_string strSecondaryPrefix = "";
	int intSecondaryControlIndex = 0;
	if (sltControlName.count() == 4)
	{
		strSecondaryPrefix = sltControlName[2];
		intSecondaryControlIndex = sltControlName[3].toInt();
	}

	if (b->panel() == PANEL_MAIN)
	{
		if (strControlPrefix == "inputUnit" && strSecondaryPrefix == "page")
		{
			selectUnitPage(true, inputsSelected, intControlIndex, intSecondaryControlIndex);
		}
		else if (strControlPrefix == "outputUnit" && strSecondaryPrefix == "page")
		{
			selectUnitPage(false, false, intControlIndex, intSecondaryControlIndex);
		}
		//selectInputs_input OR selectInputs_output
		else if (strControlPrefix == "selectInputs")
		{
			if (sltControlName[1] == "input")
			{
				inputsSelected = true;
				setupSelections(true, inputsSelected);
				textPut("text", "Input", PANEL_MAIN, "grpInputs");
			}
			else if (sltControlName[1] == "output")
			{
				inputsSelected = false;
				setupSelections(true, inputsSelected);
				textPut("text", "Output", PANEL_MAIN, "grpInputs");
			}
			selectUnitPage(true, inputsSelected, selectedInputDevice, selectedInputPage);
		}
		else if (strControlPrefix == "input")
		{
			map<int, gpi*>::iterator it = selectedInputList.find(intControlIndex);
			if (it != selectedInputList.end())
				showInputPopup((*it).second);
		}
		else if (strControlPrefix == "output")
		{
			map<int, gpi*>::iterator it = selectedOutputList.find(intControlIndex);
			if (it != selectedOutputList.end())
				showOutputPopup((*it).second);
		}
		else if (b->id() == "clear_input")
			setSelectedInput(NULL);
	}
	else if (b->panel() == POPUP_INPUT)
	{
		if (b->id() == "close")
			panelDestroy(POPUP_INPUT);
		else if (b->id() == "select")
		{
			if (selectedInputForPopup != NULL)
			{
				setSelectedInput(selectedInputForPopup);
			}
			panelDestroy(POPUP_INPUT);
		}
		else if (b->id() == "normal")
		{
			if (selectedInputForPopup != NULL)
				infoWrite(selectedInputForPopup->device, GPI_OFF, selectedInputForPopup->slot);

			panelDestroy(POPUP_INPUT);
		}
		else if (b->id() == "force_off")
		{
			if (selectedInputForPopup != NULL)
				infoWrite(selectedInputForPopup->device, FORCE_OFF, selectedInputForPopup->slot);

			panelDestroy(POPUP_INPUT);
		}
		else if (b->id() == "force_on")
		{
			if (selectedInputForPopup != NULL)
				infoWrite(selectedInputForPopup->device, FORCE_ON, selectedInputForPopup->slot);

			panelDestroy(POPUP_INPUT);
		}
	}
	else if (b->panel() == POPUP_OUTPUT)
	{
		if (b->id() == "close")
			panelDestroy(POPUP_OUTPUT);
		else if (b->id() == ADD_INPUT)
		{
			if (selectedInput != NULL && selectedOutputForPopup != NULL)
			{
				bncs_string newRoute = selectedOutputForPopup->createNewRoute(selectedInput);
				if (newRoute.length() > 0)
					infoWrite(selectedOutputForPopup->routerDevice, selectedOutputForPopup->createNewRoute(selectedInput), selectedOutputForPopup->routerSlot);
			}
		}
		else if (b->id() == CLEAR_INPUTS)
		{
			//Clears routing
			if (selectedOutputForPopup != NULL)
				infoWrite(selectedOutputForPopup->routerDevice, "&CLEAR", selectedOutputForPopup->routerSlot);
		}
		else if (b->id() == "off")
		{
			if (selectedOutputForPopup != NULL)
				infoWrite(selectedOutputForPopup->device, GPI_OFF, selectedOutputForPopup->slot);
			panelDestroy(POPUP_OUTPUT);
		}

		else if (b->id() == "on")
		{
			if (selectedOutputForPopup != NULL)
				infoWrite(selectedOutputForPopup->device, GPI_ON, selectedOutputForPopup->slot);
			panelDestroy(POPUP_OUTPUT);
		}
		else if (b->id() == "force_off")
		{
			if (selectedOutputForPopup != NULL)
				infoWrite(selectedOutputForPopup->device, FORCE_OFF, selectedOutputForPopup->slot);
			panelDestroy(POPUP_OUTPUT);
		}
		else if (b->id() == "force_on")
		{
			if (selectedOutputForPopup != NULL)
				infoWrite(selectedOutputForPopup->device, FORCE_ON, selectedOutputForPopup->slot);
			panelDestroy(POPUP_OUTPUT);
		}
		else if (b->id() == CLEAR_SELECTED_INPUT)
		{
			if (selectedRoutedInput != NULL && selectedOutputForPopup != NULL)
			{
				infoWrite(selectedOutputForPopup->routerDevice, "&REMOVE=" + selectedRoutedInput->getIdentifier(), selectedOutputForPopup->routerSlot);
			}
		}
		else if (b->id() == LIST_INPUT_ROUTES)
		{
			b->dump(LIST_INPUT_ROUTES);

			if (b->command() == "selection")
			{
				if (selectedOutputForPopup != NULL)
				{
					unsigned int listItem = b->sub(0).toUInt();
					if (selectedOutputForPopup->routerValue.size() >= listItem)
					{
						debug("LIST ITEM :%1", (int)listItem);
						selectedRoutedInput = selectedOutputForPopup->routerValue[listItem];
						controlEnable(POPUP_OUTPUT, CLEAR_SELECTED_INPUT);
					}
				}
			}
		}

	}

}

// all revertives come here
int overview::revertiveCallback( revertiveNotify * r )
{
	r->dump("overview::revertiveCallback");

	int devslot = combine(r->device(), r->index());

	map<int,gpi*>::iterator itGPI = allGpis.find(devslot);

	//We have found the gpi we are after
	if (itGPI != allGpis.end())
	{
		debug("overview::revertiveCallback We are interested in this revertive");

		//Its a gpi
		if (itGPI->second->device == r->device() && itGPI->second->slot == r->index())
		{
			itGPI->second->value = r->sInfo();

			int i = r->sInfo().toInt();
			if (i >= -1 && i <= 3)
			{
				itGPI->second->state = static_cast<States>(i);
			}
		}
		//Its a Pti
		else if (itGPI->second->ptiDevice == r->device() && itGPI->second->ptiSlot == r->index())
		{
			//inputs|1.1,1.4,1.8
			debug("overview::revertiveCallback PTIs %1", r->sInfo());
			vector < gpi* > PTIList;

			bncs_stringlist sl(r->sInfo(), '|');

			if (sl.count() == 2)
			{
				bncs_stringlist slPTIs (sl[1]);

				for (int i = 0; i < slPTIs.count(); ++i)
				{
					map<bncs_string, gpi*>::iterator it = outputList.find(slPTIs[i]);

					if (it != outputList.end())
					{
						PTIList.push_back(it->second);
					}
				}
			}

			itGPI->second->ptiValue = PTIList;
		}
		else if (itGPI->second->routerDevice == r->device() && itGPI->second->routerSlot == r->index())
		{
			bool locked = false; //<----- THIS IS NEVER USEDs

			//$inputs|1.1,1.2,1.3
			bncs_string routes = r->sInfo();

			if (routes == "No Routing")
			{
				itGPI->second->noRouting = true;
				return 0; //Were done
			}

			if (routes.left(1) == "$")
			{
				//This means its locked
				locked = true;
				routes = routes.right(routes.length() - 1);
			}

			//inputs|1.1,1.2,1.3
			//output|1.1
			bncs_stringlist sl(routes, '|');
			
			vector < gpi* > RoutesList;
			if (sl.count() == 2)
			{

				if (sl[0] == "inputs")
				{
					itGPI->second->routeInput = true;
					bncs_stringlist slRoutes (sl[1], ',');

					for (int i = 0; i < slRoutes.count(); ++i)
					{
						map<bncs_string, gpi*>::iterator it = inputList.find(slRoutes[i]);

						if (it != inputList.end())
						{
							RoutesList.push_back(it->second);
						}
					}

				}
				else if (sl[0] == "output")
				{
					itGPI->second->routeInput = false;
					map<bncs_string, gpi*>::iterator it = outputList.find(sl[1]);

					if (it != outputList.end())
					{
						RoutesList.push_back(it->second);
					}
				}
			}

			itGPI->second->routerValue = RoutesList;
		}

		Tuple state;
		Tuple pix;
		//Check if its being shown
		if (itGPI->second->input == true)
		{
			if (itGPI->second->page == selectedInputPage && itGPI->second->deviceID == selectedInputDevice)
			{
				//Update Inputs
				pix = setPixmap(true, itGPI->second);
				state = displayState(true, itGPI->second);

				bncs_string sl = bncs_string("%1,%2,%3,%4").arg(state.left).arg(state.right).arg(pix.left).arg(pix.right);
				textPut("set_pix_state", sl, PANEL_MAIN, bncs_string("input_%1").arg(bncs_string(itGPI->second->pageSlot, '0', 2U)));
			}
		}
		else
		{
			if (inputsSelected == false && itGPI->second->page == selectedInputPage && itGPI->second->deviceID == selectedInputDevice)
			{
				//Update Outputs
				pix = setPixmap(true, itGPI->second);
				state = displayState(true, itGPI->second);

				bncs_string sl = bncs_string("%1,%2,%3,%4").arg(state.left).arg(state.right).arg(pix.left).arg(pix.right);
				textPut("set_pix_state", sl, PANEL_MAIN, bncs_string("input_%1").arg(bncs_string(itGPI->second->pageSlot, '0', 2U)));
			}

			if (itGPI->second->page == selectedOutputPage && itGPI->second->deviceID == selectedOutputDevice)
			{
				//Update Outputs
				pix = setPixmap(false, itGPI->second);
				state = displayState(false, itGPI->second);

				bncs_string sl = bncs_string("%1,%2,%3,%4").arg(state.left).arg(state.right).arg(pix.left).arg(pix.right);
				textPut("set_pix_state", sl, PANEL_MAIN, bncs_string("output_%1").arg(bncs_string(itGPI->second->pageSlot, '0', 2U)));
			}
		}



		//Check if there is any popup data that needs to be updated
		if (selectedInputForPopup != NULL)
		{
			if (selectedInputForPopup == itGPI->second)
			{
				inputPopupUpdateDisplay();
			}
		}
		if (selectedOutputForPopup != NULL)
		{
			if (selectedOutputForPopup == itGPI->second)
			{
				outputPopupUpdateDisplay();
			}
		}

	}


	return 0;
}

// all database name changes come back here
void overview::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string overview::parentCallback( parentNotify *p )
{
	if (p->command() == "instance")
	{
		loadInstances(p->value());
	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;
		
		return sl.toString( '\n' );
	}
	return "";
}

void overview::loadInstances(bncs_string instance)
{

	//Check that we have been supplied a good  instance
	if (instance.length() == 0)
		return;

	/*
	<instance composite="yes" id="gpi_logic_router">
		<group id="device_1" instance="gpi_frame_1" />
		<group id="device_2" instance="gpi_frame_2" />
		<group id="device_3" instance="gpi_soft_mixer" />
		<group id="device_4" instance="gpi_soft_misc" />
	</instance>
	*/

	bool last = false;
	int iDeviceCount = 1;
	while (last == false)
	{
		bncs_string dev;
		
		if (instanceLookupComposite(instance, bncs_string("device_%1").arg(iDeviceCount), dev))
		{
			debug("DeviceCount=%1", iDeviceCount);
			deviceList[iDeviceCount] = loadDevice(dev, iDeviceCount);
			debug("Device Loaded Name:%1 Instance:%2", deviceList[iDeviceCount].Name, deviceList[iDeviceCount].Instance);
			iDeviceCount++;
		}
		else
			last = true;
	}
	showOnPanel();
}

device overview::loadDevice(bncs_string dev, int deviceId)
{
	if (dev.length() > 0)
	{
		debug("overview::loadDevice DeviceName:%1", dev);
		/*
		<instance composite = "yes" id = "gpi_frame_1" type = "CGP4848" alt_id = "Axon GPI 1" address = "10.108.208.201" protocol = "GPI_Simulation" inputs = "48" outputs = "48">
		<group id = "inputs" instance = "gpi_frame_1/inputs" / >
		<group id = "outputs" instance = "gpi_frame_1/outputs" / >
		<group id = "input_pti" instance = "gpi_frame_1/input_pti" / >
		<group id = "output_pti" instance = "gpi_frame_1/output_pti" / >
		<group id = "routes" instance = "gpi_frame_1/routes" / >
		< / instance>
		*/
		int inputCount = 0;
		int outputCount = 0;
		bncs_string altID;
		bncs_config c(bncs_string("instances.%1").arg(dev));
		if (c.isValid())
		{
			inputCount = c.attr("inputs").toInt();
			outputCount = c.attr("outputs").toInt();
			altID = c.attr("alt_id");
		}


		instance input;
		bncs_string inputs;
		instance inputPti;
		bncs_string input_pti;
		if (instanceLookupComposite(dev, bncs_string("inputs"), inputs))
		{
			debug("overview::loadDevice LoadInputs:%1", inputs);
			input = loadInstance(inputs);

			if (instanceLookupComposite(dev, bncs_string("input_pti"), input_pti))
			{
				inputPti = loadInstance(input_pti);
			}
		}

		instance output;
		bncs_string outputs;
		instance outputPti;
		bncs_string output_pti;
		instance route;
		bncs_string routes;
		if (instanceLookupComposite(dev, bncs_string("outputs"), outputs))
		{
			debug("overview::loadDevice LoadOutputs:%1", outputs);
			output = loadInstance(outputs);

			if (instanceLookupComposite(dev, bncs_string("output_pti"), output_pti))
			{
				outputPti = loadInstance(output_pti);
			}

			if (instanceLookupComposite(dev, bncs_string("routes"), routes))
			{
				route = loadInstance(routes);
			}
		}

		device d;

		//Create the device
		if (input.valid == true && output.valid == true)
		{
			debug("overview::loadDevice Add Inputs and Outputs to device");

			d = device(input, inputPti, output, outputPti, route);
		}
		else if (input.valid)
		{
			debug("overview::loadDevice Add Only Inputs to device");

			d = device(input, inputPti);
		}
		else if (output.valid)
		{
			debug("overview::loadDevice Add Only Outputs to device");

			d = device(output, outputPti, route);
		}

		d.deviceID = deviceId;
		d.Name = altID;
		d.Instance = dev;
		debug("overview::loadDevice Set InputCount:%1 Set OutputCount:%2",inputCount,outputCount);

		debug("overview::loadDevice AltID:%1 Instance:%2", d.Name, d.Instance);

		if (d.inputsExist == true)
		{
			debug("overview::loadDevice Inputs Exist Load and Register For them");

			//Set the inputs on the device
			d.setInput(inputCount, PAGE_SIZE);

			//register for the inputs
			list<gpi*>::iterator it = d.inputGpis.begin();
			for (it; it != d.inputGpis.end(); ++it)
			{
				//Get Name
				bncs_string name;
				debug("overview::loadDevice Register for Input Dev:%1 Slot:%2", (*it)->device, (*it)->slot);
				routerName((*it)->device, DATABASE_INPUT_NAME, (*it)->slot, name);
				(*it)->name = name;
				

				addToMap((*it)->device, (*it)->slot, (*it));
				inputList[(*it)->getIdentifier()] = (*it);
				infoRegister((*it)->device, (*it)->slot, (*it)->slot, true);
				infoPoll((*it)->device, (*it)->slot, (*it)->slot);

				//Register for PTI
				if ((*it)->ptiDevice > 0)
				{
					addToMap((*it)->ptiDevice, (*it)->ptiSlot, (*it));
					infoRegister((*it)->ptiDevice, (*it)->ptiSlot, (*it)->ptiSlot, true);
					infoPoll((*it)->ptiDevice, (*it)->ptiSlot, (*it)->ptiSlot);
				}
			}
		}

		if (d.outputsExist == true)
		{
			debug("overview::loadDevice Outputs Exist Load and Register For them");

			//Set the inputs on the device
			d.setOutput(outputCount, PAGE_SIZE);

			//register for the inputs
			list<gpi*>::iterator it = d.outputGpis.begin();
			for (it; it != d.outputGpis.end(); ++it)
			{
				//Get Name
				bncs_string name;
				debug("overview::loadDevice Register for Output Dev:%1 Slot:%2", (*it)->device, (*it)->slot);
				routerName((*it)->device, DATABASE_OUTPUT_NAME, (*it)->slot, name);
				(*it)->name = name;
				//Register
				addToMap((*it)->device, (*it)->slot, (*it));
				outputList[(*it)->getIdentifier()] = (*it);
				infoRegister((*it)->device, (*it)->slot, (*it)->slot, true);
				infoPoll((*it)->device, (*it)->slot, (*it)->slot);

				//Register for the PTI
				if ((*it)->ptiDevice > 0)
				{
					addToMap((*it)->ptiDevice, (*it)->ptiSlot, (*it));
					infoRegister((*it)->ptiDevice, (*it)->ptiSlot, (*it)->ptiSlot, true);
					infoPoll((*it)->ptiDevice, (*it)->ptiSlot, (*it)->ptiSlot);
				}
				//Register for the Router
				if ((*it)->routerDevice > 0)
				{
					addToMap((*it)->routerDevice, (*it)->routerSlot, (*it));
					infoRegister((*it)->routerDevice, (*it)->routerSlot, (*it)->routerSlot, true);
					infoPoll((*it)->routerDevice, (*it)->routerSlot, (*it)->routerSlot);
				}
			}
		}

		debug("overview::loadDevice RETURN AltID:%1 Instance:%2", d.Name, d.Instance);
		return d;
	}
	return device();
}

instance overview::loadInstance(bncs_string inst)
{
	int dev;
	int offset;
	if (getDev(inst, &dev, &offset))
	{
		return instance(inst,dev,offset);
	}
	return instance();
}

void overview::addToMap(int dev, int slot, gpi* g)
{
	if (g != NULL)
	{
		int val = combine(dev, slot);
		allGpis[val] = g;
	}
}

int overview::combine(int dev, int slot)
{
	return (dev * 10000) + slot;
}

void overview::showOnPanel()
{
	setupSelections(true, true);
	setupSelections(false, false);

	selectUnitPage(true,true, 1, 1);
	selectUnitPage(false,false, 1, 1);
}

void overview::setupSelections(bool inputButtons, bool deviceInputs)
{
	map<int, device>::iterator it = deviceList.begin();

	bncs_string debugText;
	bncs_string buttonsPrefix;
	int buttonCount = 0;
	if (inputButtons)
	{
		debug("overview::setupSelections setupSelections Inputs");

		buttonsPrefix = "inputUnit";
		buttonCount = unitInputCountOnPanel;
		debugText = "input";
	}
	else
	{
		debug("overview::setupSelections setupSelections Outputs");

		buttonsPrefix = "outputUnit";
		buttonCount = unitOutputCountOnPanel;
		debugText = "output";
	}

	int count = 0;
	int PageCount = 0;
	bool gpisExist = false;
	if (deviceInputs)
	{
		if (inputButtons == true)
		{
			textPut("statesheet", "source_selected", PANEL_MAIN,"selectInputs_input");
			textPut("statesheet", "enum_notimportant", PANEL_MAIN, "selectInputs_output");
		}

		count = it->second.inputCount;
		PageCount = it->second.inputPageCount;
		gpisExist = it->second.inputsExist;
	}
	else
	{
		if (inputButtons == true)
		{
			textPut("statesheet", "dest_selected", PANEL_MAIN, "selectInputs_output");
			textPut("statesheet", "enum_notimportant", PANEL_MAIN, "selectInputs_input");
		}

		count = it->second.outputCount;
		PageCount = it->second.outputPageCount;
		gpisExist = it->second.outputsExist;
	}


	for (it; it != deviceList.end(); ++it)
	{

		if (deviceInputs)
		{
			count = it->second.inputCount;
			PageCount = it->second.inputPageCount;
			gpisExist = it->second.inputsExist;
		}
		else
		{
			count = it->second.outputCount;
			PageCount = it->second.outputPageCount;
			gpisExist = it->second.outputsExist;
		}

		textPut("text", it->second.Name, PANEL_MAIN, bncs_string("%1Name_%2").arg(buttonsPrefix).arg(it->first));

		//Figure out how many tabs we need to show, if its none let disable the main button
		
		int tabCount = getIdList(PANEL_MAIN, bncs_string("%1_%2_page_").arg(buttonsPrefix).arg(it->first)).count();

		//Gpis exist for this unit
		if (gpisExist)
		{
			debug("overview::setupSelections %1 Output Count = %2", debugText, count);

			debug("overview::setupSelections %1 PAGECOUNT = %2", debugText, PageCount);

			int iFirstValue = 1;
			for (int i = 1; i <= tabCount; i++)
			{
				
				if (PageCount >= i)
				{
					Show(bncs_string("%1_%2_page_%3").arg(buttonsPrefix).arg(it->first).arg(i));

					int nextValue = PAGE_SIZE * i;
					if (nextValue >= count)
						nextValue = count;
					textPut("text", bncs_string("%1-%2").arg(iFirstValue).arg(nextValue), PANEL_MAIN, bncs_string("%1_%2_page_%3").arg(buttonsPrefix).arg(it->first).arg(i));
					iFirstValue = nextValue + 1;
				}
				else
				{
					//controlHide(PANEL_MAIN, bncs_string("%1_%2_page_%3").arg(buttonsPrefix).arg(it->first).arg(i));
					Hide( bncs_string("%1_%2_page_%3").arg(buttonsPrefix).arg(it->first).arg(i));
				}
			}
		}
		else
		{
			for (int i = 1; i <= tabCount; i++)
			{
				//controlHide(PANEL_MAIN, bncs_string("%1_%2_page_%3").arg(buttonsPrefix).arg(it->first).arg(i));
				Hide(bncs_string("%1_%2_page_%3").arg(buttonsPrefix).arg(it->first).arg(i));
			}
		}

	}

	//debug("overview::setupSelections HIDE Section unitCountOnPanel:%1 deviceListSize:%2", (int)buttonCount, (int)deviceList.size());
	//This is to tidy up any extra buttons if there are more than the number of devices we have
	for (int i = deviceList.size() + 1; i <= buttonCount; ++i)
	{
		debug("overview::setupSelections %1 HIDE:%2", debugText, bncs_string("%1Name_%2").arg(buttonsPrefix).arg(i));
		Hide( bncs_string("%1Name_%2").arg(buttonsPrefix).arg(i));
		Hide(bncs_string("%1Comms_%2").arg(buttonsPrefix).arg(i));

		int tabCount = getIdList(PANEL_MAIN, bncs_string("%1_%2_page_").arg(buttonsPrefix).arg(i)).count();
		for (int ii = 1; ii <= tabCount; ii++)
		{
			Hide( bncs_string("%1_%2_page_%3").arg(buttonsPrefix).arg(i).arg(ii));
		}
	}
}

void overview::selectUnitPage(bool InputsSelection, bool DeviceSelection, int dev, int tab)
{
	bncs_string uiStart;
	bncs_string gpis;
	bncs_string indexLabel;
	if (InputsSelection)
	{
		uiStart = "inputusage_";
		gpis = "input_";
		indexLabel = "input_index_";
		//HideAllInputs();
	}
	else
	{
		uiStart = "outputusage_";
		gpis = "output_";
		indexLabel = "output_index_";
		//HideAllOutputs();
	}

	map<int, device>::iterator it = deviceList.find(dev);

	if (it != deviceList.end())
	{
		debug("overview::setupGPIs Found device");
		map<int, list<gpi*>>::iterator itt;
		if (DeviceSelection)
			itt= it->second.inputPages.find(tab);
		else
			itt = it->second.outputPages.find(tab);

		bool notEndofIt = false;
		if (DeviceSelection)
			notEndofIt = itt != it->second.inputPages.end();
		else
			notEndofIt = itt != it->second.outputPages.end();

		if (notEndofIt)
		{
			if (InputsSelection)
			{
				//Unselect last
				textPut("stylesheet", "enum_deselected", PANEL_MAIN, bncs_string("inputUnit_%1_page_%2").arg(selectedInputDevice).arg(selectedInputPage));

				selectedInputDevice = dev;
				selectedInputPage = tab;

				textPut("stylesheet", "enum_selected", PANEL_MAIN, bncs_string("inputUnit_%1_page_%2").arg(selectedInputDevice).arg(selectedInputPage));
			}
			else
			{
				textPut("stylesheet", "enum_deselected", PANEL_MAIN, bncs_string("outputUnit_%1_page_%2").arg(selectedOutputDevice).arg(selectedOutputPage));

				selectedOutputDevice = dev;
				selectedOutputPage = tab;

				textPut("stylesheet", "enum_selected", PANEL_MAIN, bncs_string("outputUnit_%1_page_%2").arg(selectedOutputDevice).arg(selectedOutputPage));
			}

			debug("overview::setupGPIs Found page");
			list<gpi*>::iterator ittt = itt->second.begin();
			int i = 0;

			map<int, gpi*>selection;
			for (ittt; ittt != itt->second.end(); ++ittt)
			{
				i++;
				selection[i] = (*ittt);
				bncs_string buttonID = bncs_string(i, '0', 2U);
				debug("overview::setupGPIs Loop de Loop :%1", buttonID);
				Show( bncs_string("%1%2").arg(gpis).arg(buttonID));
				
				/*
				Show( bncs_string("%1%2").arg(indexLabel).arg(buttonID));
				Show( bncs_string("%1%2").arg(gpis).arg(buttonID));
				Show( bncs_string("%1left_pixmap_%2").arg(uiStart).arg(buttonID));
				Show( bncs_string("%1right_pixmap_%2").arg(uiStart).arg(buttonID));
				*/


				//textPut("text", (*ittt)->name, PANEL_MAIN, bncs_string("%1%2").arg(uiStart).arg(buttonID));
				//textPut("text", i + ( tab - 1 )* PAGE_SIZE  , PANEL_MAIN, bncs_string("%1%2").arg(indexLabel).arg(buttonID));

				Tuple state = displayState(InputsSelection, (*ittt));
				Tuple pix = setPixmap(InputsSelection, (*ittt));

				bncs_string sl = bncs_string("%1,%2,%3,%4,%5,%6").arg((*ittt)->name).arg(i + (tab - 1)* PAGE_SIZE).arg(state.left).arg(state.right).arg(pix.left).arg(pix.right);

				textPut("set_all", sl, PANEL_MAIN, bncs_string("%1%2").arg(gpis).arg(buttonID));
			}

			i++;
			//Hide those not used
			for (;i <= PAGE_SIZE; i++)
			{
				bncs_string buttonID = bncs_string(i, '0', 2U);
				Hide(bncs_string("%1%2").arg(gpis).arg(buttonID));
				/*
				Hide( bncs_string("%1%2").arg(indexLabel).arg(buttonID));
				Hide( bncs_string("%1%2").arg(gpis).arg(buttonID));
				Hide( bncs_string("%1left_pixmap_%2").arg(uiStart).arg(buttonID));
				Hide( bncs_string("%1right_pixmap_%2").arg(uiStart).arg(buttonID));
				*/
			}

			if (InputsSelection)
			{
				selectedInputList = selection;
			}
			else
			{
				selectedOutputList = selection;
			}
		}
	}

}

Tuple overview::displayState(bool InputsSelection, gpi* g)
{
	Tuple st;
	if (g != NULL)
	{
		States state = g->state;
		
		bncs_string panel = bncs_string("%1_%2").arg(InputsSelection ? "input" : "output").arg(bncs_string(g->pageSlot, '0', 2U));

		bncs_string text = "UNKNOWN";
		bncs_string stateSheet = "enum_unknown";

		switch (state)
		{
		case States::OFF:
			text = "";
			stateSheet = "gpi_off";
			break;
		case States::ON:
			text = "";
			stateSheet = "gpi_on";
			break;
		case States::FORCED_OFF:
			text = " FORCED|OFF";
			stateSheet = "gpi_forced_off";
			break;
		case States::FORCED_ON:
			text = " FORCED|ON";
			stateSheet = "gpi_forced_on";
			break;
		}

		debug("displayState: Panel:%1 text:%2 state:%3",panel, text, stateSheet);
		st.left = stateSheet;
		st.right = text;
	}
	return st;
}

Tuple overview::setPixmap(bool input, gpi* g)
{
	Tuple state;
	if (g != NULL)
	{
		//bncs_string buttonID = bncs_string("%1_left_pixmap_%2").arg(input ? "inputusage" : "outputusage").arg(bncs_string(g->pageSlot, '0', 2U));
		if (g->noRouting == true)
		{
			state.left = "gpi_no_routing.png";
		}
		else if (g->routerValue.size() > 0)
		{
			if (g->routeInput == true)
				state.left = "gpi_no_routing.png";
			else
				state.left = "gpi_output_routing.png";
		}
		else
		{
			state.left = "";
		}
		
		//buttonID = bncs_string("%1_right_pixmap_%2").arg(input ? "inputusage" : "outputusage").arg(bncs_string(g->pageSlot, '0', 2U));
		if (g->ptiValue.size() > 0)
		{
			state.right = "gpi_input_pti.png";
		}
		else
		{
			state.right = "";
		}
	}
	return state;
}

// timer events come here
void overview::timerCallback( int id )
{
}

void overview::showInputPopup(gpi* inputGpi)
{

	selectedInputForPopup = inputGpi;
	if (selectedInputForPopup != NULL)
	{
		if (inputGpi->input == true)
		{
			panelPopup(POPUP_INPUT, "popup_input.bncs_ui");

			inputPopupUpdateDisplay();
		}
		//This is actually an output GPI acting an input GPI
		else
		{
			panelPopup(POPUP_INPUT, "popup_output_as_input.bncs_ui");

			inputPopupUpdateDisplay();
		}
	}
}

void overview::inputPopupUpdateDisplay()
{
	if (selectedInputForPopup != NULL)
	{
		textPut("clear", POPUP_INPUT, "lvw_input_pti");
		for (vector<gpi*>::iterator it = selectedInputForPopup->ptiValue.begin(); it != selectedInputForPopup->ptiValue.end(); ++it)
		{
			textPut("add", bncs_string("%1;%2").arg((*it)->getIdentifier()).arg((*it)->name), POPUP_INPUT, "lvw_input_pti");
		}
	}
}


void overview::showOutputPopup(gpi* outputGpi)
{
	selectedOutputForPopup = outputGpi;
	if (outputGpi != NULL)
	{
		panelPopup(POPUP_OUTPUT, "popup_output.bncs_ui");

		outputPopupUpdateDisplay();

		//Title
		textPut("text", bncs_string("Output %1 - Device:%2 Slot:%3").arg(outputGpi->getIdentifier()).arg(outputGpi->device).arg(outputGpi->slot), POPUP_OUTPUT, "lblOutput");

	}
}

void overview::outputPopupUpdateDisplay()
{
	if (selectedOutputForPopup != NULL)
	{
		controlHide(POPUP_OUTPUT, "lblNoRouting");
		controlDisable(POPUP_OUTPUT, CLEAR_INPUTS);
		controlDisable(POPUP_OUTPUT, CLEAR_SELECTED_INPUT);
		controlDisable(POPUP_OUTPUT, CLEAR_OUTPUT);

		textPut("clear", POPUP_OUTPUT, "lvw_input_pti");
		//Display current pti
		for (vector<gpi*>::iterator it = selectedOutputForPopup->ptiValue.begin(); it != selectedOutputForPopup->ptiValue.end(); ++it)
		{
			textPut("add", bncs_string("%1;%2").arg((*it)->getIdentifier()).arg((*it)->name), POPUP_OUTPUT, "lvw_input_pti");
		}

		textPut("clear", POPUP_OUTPUT, POPUP_OUTPUT, LIST_INPUT_ROUTES);
		//Display current routing
		for (vector<gpi*>::iterator it = selectedOutputForPopup->routerValue.begin(); it != selectedOutputForPopup->routerValue.end(); ++it)
		{
			textPut("add", bncs_string("%1;%2;%3").arg((*it)->input ? "Input" : "Output").arg((*it)->getIdentifier()).arg((*it)->name), POPUP_OUTPUT, LIST_INPUT_ROUTES);
		}
		if (selectedOutputForPopup->routerValue.size() > 0)
		{
			controlEnable(POPUP_OUTPUT, "clear_inputs");
		}


		if (selectedInput != NULL)
		{
			textPut("text", selectedInput->name, POPUP_OUTPUT, "selected_input");

			//Check if the selected input is actually an input. It could be an output it is also possible to route these.
			if (selectedInput->input == true)
			{

			}
			//Otherwise we must be an output
			else
			{
				//Make changes if its an Output being cloned
				textPut("text", "Selected|Output", POPUP_OUTPUT, "lblSelectedInput");
				textPut("text", "Clone", POPUP_OUTPUT, ADD_INPUT);
				controlHide(POPUP_OUTPUT, CLEAR_SELECTED_INPUT);

			}
		}
		else
		{
			controlDisable(POPUP_OUTPUT, "add_input");
		}
	}
}

void overview::setSelectedInput(gpi* selectedGpi)
{
	selectedInput = selectedGpi;

	if (selectedInput  != NULL)
	{
		bncs_string s(selectedInput->name);
		s.replace('|', ' ');
		textPut("text", bncs_string("Selected %1: %2").arg(selectedInput->input?"Input":"Output").arg(s), PANEL_MAIN, "selected_input");
		controlEnable(PANEL_MAIN, "clear_input");
	}
	else
	{
		textPut("text", bncs_string("Selected:"), PANEL_MAIN, "selected_input");
		controlDisable(PANEL_MAIN, "clear_input");
	}
}

void overview::Show(bncs_string name)
{
	if (Controls[name] == false)
	{
		controlShow(PANEL_MAIN, name);
		Controls[name] = true;
	}
}

void overview::Hide(bncs_string name)
{
	if (Controls[name] == true)
	{
		controlHide(PANEL_MAIN, name);
		Controls[name] = false;
	}
}

