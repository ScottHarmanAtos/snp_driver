#include "gpi.h"


gpi::gpi()
{
	noRouting = false;
	state = States::UNKNOWN;

	device = 0;
	slot = 0;

	ptiDevice = 0;
	ptiSlot = 0;

	routerDevice = 0;
	routerSlot = 0;
	routeInput = false;

	page = 0;
	pageSlot = 0;

	deviceID = 0;
	gpiID = 0;

	input = false;

}


gpi::~gpi()
{
	ptiValue.clear();
	routerValue.clear();
}

bncs_string gpi::getIdentifier()
{
	return bncs_string("%1.%2").arg(deviceID).arg(gpiID);
}

bool gpi::routeContainsID(gpi* g)
{
	if (g != NULL)
	{
		//We are the same Output
		if (this->getIdentifier() == g->getIdentifier() && g->input == false)
			return true;
		for (vector<gpi*>::iterator it = routerValue.begin(); it != routerValue.end(); ++it)
		{
			if ((*it)->getIdentifier() == g->getIdentifier() && (*it)->input == g->input)
			{
				return true;
			}
		}
	}
	return false;
}

bncs_string gpi::createNewRoute(gpi* g)
{
	bncs_string route;

	//Check it is not already routed
	if (!this->routeContainsID(g))
	{
		//Its an input so lets try to add it
		if (g->input == true)
		{
			route = "inputs|";

			//If we already have routes lets add those
			if (this->routeInput == true)
			{
				for (vector<gpi*>::iterator it = routerValue.begin(); it != routerValue.end(); ++it)
				{
					route = route + (*it)->getIdentifier() + ",";;
				}
			}
			route = route + g->getIdentifier();
			
			//Short hand for achiving whats going on above, above is useful whilst testing though
			//route = "&ADD=" + g->getIdentifier();
		}
		else
		{
			route = "output|" + g->getIdentifier();
		}
	}
	return route;
}