#ifndef overview_INCLUDED
	#define overview_INCLUDED

#pragma warning (disable:4786)
#include <map>
#include <bncs_script_helper.h>
#include "instance.h"
#include "device.h"
#include "states.h"
#include <vector>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 

class Tuple
{
public:
	bncs_string left;
	bncs_string right;
	Tuple(bncs_string left, bncs_string right)
	{
		this->left = left;
		this->right = right;
	}
	Tuple()
	{
		this->left = "";
		this->right = "";
	}
};

class overview : public bncs_script_helper
{
public:
	overview( bncs_client_callback * parent, const char* path );
	virtual ~overview();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:


//--------------------------------------------------------------------
	//Things I have added
	void loadInstances(bncs_string instance);
	device loadDevice(bncs_string dev, int deviceId);
	instance loadInstance(bncs_string);

	map<int, gpi*> allGpis;
	map<int,device> deviceList;

	//This stores the GPIs in there real format 1.1 <-- or 4.34
	map<bncs_string, gpi*> inputList;
	map<bncs_string, gpi*> outputList;

	map<int, gpi*> selectedInputList;
	map<int, gpi*> selectedOutputList;

	gpi* selectedInput;
	gpi* selectedInputForPopup;
	gpi* selectedOutputForPopup;

	gpi* selectedRoutedInput;

	bool inputsSelected;
	int selectedInputDevice = 0;
	int selectedInputPage = 0;
	int selectedOutputDevice = 0;
	int selectedOutputPage = 0;

	///Function joins Device and Slot together into an integer, for using as a map key
	int combine(int dev, int slot);

	void addToMap(int dev, int slot, gpi*);

	void showOnPanel();

	void setupSelections(bool InputPage, bool deviceInputGPIs);

	void selectUnitPage(bool InputsSelection, bool DeviceSelection, int dev, int tab);


	//How many units can be shown on the current UI panel
	int unitInputCountOnPanel;
	int unitOutputCountOnPanel;

	void showInputPopup(gpi*);
	void showOutputPopup(gpi*);

	void setSelectedInput(gpi* selectedGpi);

	Tuple setPixmap(bool inputPage, gpi* g);

	void outputPopupUpdateDisplay();
	void inputPopupUpdateDisplay();

	Tuple displayState(bool InputsSelection, gpi* g);

	map<bncs_string, bool> Controls;

	void Hide(bncs_string name);
	void Show(bncs_string name);

};


#endif // overview_INCLUDED