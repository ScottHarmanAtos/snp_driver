#pragma once
#include "bncs_script_helper.h"
#include "instance.h"
#include "gpi.h"
#include <list>
#include <map>
class device
{
public:
	device();
	device(instance inputs, instance pti_inputs);
	device(instance inputs, instance pti_inputs, instance outputs, instance pti_outputs, instance routes);
	device(instance outputs, instance pti_outputs, instance routes);
	~device();

	instance inputs;
	instance outputs;
	instance pti_inputs;
	instance pti_outputs;
	instance routes;

	bool inputsExist;
	bool outputsExist;

	int inputCount;
	int outputCount;

	void setInput(int inputCount, int pageSize);
	void setOutput(int outputCount, int pageSize);

	bncs_string Name;
	bncs_string Instance;

	list<gpi*> inputGpis;
	list<gpi*> outputGpis;

	map<int, list<gpi*>> inputPages;
	map<int, list<gpi*>> outputPages;

	int inputPageSize;
	int outputPageSize;

	int inputPageCount;
	int outputPageCount;

	int deviceID;
	bool comms;

};

