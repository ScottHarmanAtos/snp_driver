# gpi_logic overview Component

Deployed on the panels\gpi\gpi_manager\gpi_manager.bncs_ui panel

![Example](example.png)

Example of what it should look like


and includes these sub panels

popup input

![popup_input](popup_input.png)

popup output

![popup_output](popup_output.png)

popup output as input

![popup_output_as_input](popup_output_as_input.png)

## Config


### instance
gpi_logic_router is not in instances

### index


## Notifications
None

## Stylesheets
None