#pragma once
#include "bncs_script_helper.h"
class instance
{
public:
	instance();
	instance(bncs_string name, int device, int offset);
	~instance();

	bncs_string name;
	int device;
	int offset;
	bool valid;

};

