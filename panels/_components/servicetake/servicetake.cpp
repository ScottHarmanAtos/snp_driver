#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "servicetake.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( servicetake )

// constructor - equivalent to ApplCore STARTUP
servicetake::servicetake( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( 1, "servicetake.bncs_ui" );

	setSize( 1 );

	dev = 0;
	slotSelectedService = 0;
	slotServiceBegin = 0;
	slotServiceEnd = 0;
}

// destructor - equivalent to ApplCore CLOSEDOWN
servicetake::~servicetake()
{
}

// all button pushes and notifications come here
void servicetake::buttonCallback( buttonNotify *b )
{
//	debug(bncs_string("servicetake::buttonCallback id=%1 command=%2.%3 value=%2").arg(b->id()).arg(b->command()).arg(b->sub(0)).arg(b->value()));

	if( b->panel() == 1 )
	{
		if (b->id() == "s_take")
		{
			bncs_string selectedText;
			textGet("selected", 1, "lb_service", selectedText);
			infoWrite(dev, selectedText, slotSelectedService); 
		}
		else if (b->id() == "btn_up")
		{
			textPut(bncs_string("selected=previous") , 1, "lb_service" );
		}
		else if (b->id() == "btn_down")
		{
			textPut(bncs_string("selected=next") , 1, "lb_service" );
		}
		else if ( b->id() == "s_refresh")
		{
			infoWrite(dev, "refresh", slotNumberOfServices); 
		}
	}
}

// updates the content of the listbox based on the stored values
void servicetake::updateListbox()
{
	bncs_string selected;
	textGet("selected", 1, "lb_service", selected);
	textPut("clear", 1, "lb_service" );
	for(map<int, bncs_string>::iterator it = serviceNames.begin(); it != serviceNames.end(); it++)
	{
		bncs_string strService = it->second;
		strService.replace(0x09, '-');
		textPut("add", strService, 1, "lb_service" );
	}
	textPut("selected.text.exact", selected, 1, "lb_service");
}


// all revertives come here
int servicetake::revertiveCallback( revertiveNotify * r )
{
//	debug(bncs_string("servicetake::revertiveCallback dev=%1 slot=%2 value=%3").arg(r->device()).arg(r->index()).arg( r->sInfo()));

	if ( r->device() == dev)
	{
		int slot = r->index();
		if (slot >= slotServiceBegin && slot <= slotServiceEnd)
		{
			if (r->sInfo().length() != 0)
			{
				serviceNames[slot] = r->sInfo();
			}
			else
			{
				serviceNames.erase(slot);
			}
			updateListbox();
		}
	}
	
	return 0;
}

// all database name changes come back here
void servicetake::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string servicetake::parentCallback( parentNotify *p )
{
//	debug(bncs_string("servicetake::parentCallback dev=%1 command=%2 value=%3").arg(dev).arg(p->command()).arg(p->value()));

	if(p->command() == "instance")
	{
		dev = 0;
		slotSelectedService = 1;
		slotServiceBegin = 1;
		slotServiceEnd = 1;

		char devType[256];

		m_strInstance = p->value();

		getDevSlot(m_strInstance, "SelectedService", &dev, &slotSelectedService, devType);
		getDevSlot(m_strInstance, "ServiceNameBegin", &dev, &slotServiceBegin, devType);		
		getDevSlot(m_strInstance, "ServiceNameEnd", &dev, &slotServiceEnd, devType);
		getDevSlot(m_strInstance, "NumberOfServices", &dev, &slotNumberOfServices, devType); // sending anything to this slot will refresh the service list
		
		debug(bncs_string("servicetake::parentCallback dev=%1 SelectedService=%2").arg(dev).arg(slotSelectedService));
		debug(bncs_string("servicetake::parentCallback dev=%1 ServiceNameBegin=%2").arg(dev).arg(slotServiceBegin));
		debug(bncs_string("servicetake::parentCallback dev=%1 ServiceNameEnd=%2").arg(dev).arg(slotServiceEnd));
		debug(bncs_string("servicetake::parentCallback dev=%1 NumberOfServices=%2").arg(dev).arg(slotNumberOfServices));
		
		infoRegister(dev,slotNumberOfServices, slotNumberOfServices, true);
		
		textPut("clear", 1, "lb_service" );
		serviceNames.clear();
		if (slotServiceBegin <= slotServiceEnd)
		{
			infoRegister(dev,slotServiceBegin, slotServiceEnd, true);
			infoPoll(dev, slotServiceBegin, slotServiceEnd);
		}
	}
	else if(p->command() == "onair")
	{
		if (p->value() == "on")
		{
			controlDisable( "1", "s_take" );
		}
		else if (p->value() == "off")
		{
			controlEnable( "1", "s_take" );
		}

	}
	return "";
}

// timer events come here
void servicetake::timerCallback( int id )
{
}
