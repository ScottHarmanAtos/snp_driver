# servicetake Component

The servicetake component is for the purpose of displaying the selected service and a list box with the possible services, which the user can select and apply one from.

![as_used_on_panel](as_used_on_panel.png)

## Functionality</h2>
The servicetake displays the selected service, ID and network name.
The listbox is filled with the (nonempty) values of the range "ServiceNameBegin" and "ServiceNameEnd" in the infodriver of the device. 
By pressing the take button, the parameter "SelectedService" of the device is updated with the name of the selected service.
By pressing the refresh button, the driver refreshes the list of available services.

## Configuration</h2>
The component needs to be configured with the instance to display some of the parameters and register and poll to the appropriate range of slots.
The device description file has to have the following parameters:
| Name | Use |
|------|------|
| NetworkName | For display |
| ServiceName | For display |
| ServiceID | For display |
| ServiceNameBegin | Its slot number is the minimum of the range to be displayed in the listbox |
| ServiceNameEnd | Its slot number is the maximum of the range to be displayed in the listbox |
| SelectedService | The value of this parameter is updated with the selected service in the listbox	when the take button is pressed |

## Commands|
| Name | Use |
|------|------|
| instance | Instance of the device to take parameter values from |
| onair | The take button gets enabled or disabled based on the value of this command: on - disable, off - enable |

## Notifications
None

## Stylesheets
None