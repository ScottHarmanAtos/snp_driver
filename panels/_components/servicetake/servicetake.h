#ifndef servicetake_INCLUDED
	#define servicetake_INCLUDED

#pragma warning(disable: 4786) 

#include <bncs_script_helper.h>
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class servicetake : public bncs_script_helper
{
private:
	bncs_string m_strInstance;
	int dev;
	int slotSelectedService;
	int slotServiceBegin;
	int slotServiceEnd;
	map<int, bncs_string> serviceNames;
	int slotNumberOfServices;
	void updateListbox();

public:
	servicetake( bncs_client_callback * parent, const char* path );
	virtual ~servicetake();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
};


#endif // servicetake_INCLUDED