#ifndef adjust_to_level_INCLUDED
	#define adjust_to_level_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class adjust_to_level : public bncs_script_helper
{
public:
	adjust_to_level( bncs_client_callback * parent, const char* path );
	virtual ~adjust_to_level();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void init();
	void timerCallback( int );
private:

	bncs_string m_strLevelId;

	bncs_string m_strCompositeInstance;
	int m_intPackagerDevice;
	int m_intPackagerDatabase;

	bncs_string m_strRouterInstance;
	int m_intRouterDevice;
	int m_intRouterDatabase;

	bncs_string m_strButtonText;
	bncs_string m_strAdjustInstance;

	bool m_blnValid;

	bncs_string m_strPackagerGroupId;
	bncs_string m_strSdiRouterGroupId;
};


#endif // adjust_to_level_INCLUDED