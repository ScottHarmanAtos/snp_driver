#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "adjust_to_level.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( adjust_to_level )

#define PANEL_MAIN   1
#define TIMER_INIT   1
#define TIMEOUT_INIT 10


// constructor - equivalent to ApplCore STARTUP
adjust_to_level::adjust_to_level( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_intPackagerDatabase = -1;
	m_intRouterDatabase   = -1;
	m_intPackagerDevice   = 0;
	m_intRouterDevice     = 0;

	m_blnValid = false;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );
	controlDisable( PANEL_MAIN, "button" );
}

// destructor - equivalent to ApplCore CLOSEDOWN
adjust_to_level::~adjust_to_level()
{
}

// all button pushes and notifications come here
void adjust_to_level::buttonCallback( buttonNotify *b )
{
	if( navigateCanAdjust(m_strAdjustInstance) )
	{
		debug("trying to adjust to %1", m_strAdjustInstance);
		navigateAdjust(m_strAdjustInstance);
	}
}

// all revertives come here
int adjust_to_level::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void adjust_to_level::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string adjust_to_level::parentCallback( parentNotify *p )
{
	if( p->command() == "instance" )
	{
		m_strCompositeInstance = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "packager_groupid")
	{
		m_strPackagerGroupId = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "router_groupid")
	{
		m_strSdiRouterGroupId = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "level_id")
	{
		m_strLevelId = p->value();
	}
	else if( p->command() == "index" )
	{
		int packageIndex = p->value().toInt();
		debug("adjust_to_level::index=%1", packageIndex);
		if (m_blnValid)
		{
			debug("adjust_to_level::m_blnValid=%1", m_blnValid);
			bncs_string strLevels;
			routerName(m_intPackagerDevice, m_intPackagerDatabase, packageIndex, strLevels);
			int levelIndex = bncs_stringlist(strLevels).getNamedParam(m_strLevelId).toInt();
			debug("adjust_to_level::level=%1, %2=%3", strLevels, m_strLevelId, levelIndex);
			bncs_string strAdjustInstance, strConnector;
			bncs_string rawInstance;
			routerName(m_intRouterDevice, m_intRouterDatabase, levelIndex, rawInstance);
			debug("adjust_to_level::AdjustAndConnector=%1", rawInstance);
			if (navigateCanAdjust(rawInstance))
			{
				// these two calls to bncs_config are not necessaru for functionality, merely here to drive the denug
				bncs_config instances = bncs_config(bncs_string("instances.%1").arg(rawInstance));
				if (instances.isValid())
				{
					bncs_string type = instances.attr("type");
					bncs_config typeHandlers = bncs_config(bncs_string("typehandlers.%1").arg(type));
					if (typeHandlers.isValid())
					{
						debug("adjust_to_level::we can adjust from raw instance");
						bncs_string handler = typeHandlers.attr("default");
						debug("adjust_to_level::type=%1, handler=%2", type, handler);
						controlEnable(PANEL_MAIN, "button");
						m_strAdjustInstance = rawInstance;
					}
				}
			}
			else
			{
				debug("adjust_to_level::we cannot adjust from raw instance, try splitting on #");
				m_strAdjustInstance = "";
				if (rawInstance.contains('#'))
				{
					rawInstance.split('#', strAdjustInstance, strConnector);
				}
				else
				{
					m_strAdjustInstance = rawInstance;
				}
				controlDisable(PANEL_MAIN, "button");
			}
		}
		else
		{
			debug("adjust_to_level::we cannot adjust - duff config");

		}
	}
	else if( p->command() == "name" )
	{
		if( navigateCanAdjust( p->value() ))
		{
			controlEnable(PANEL_MAIN, "button" );
			m_strAdjustInstance = p->value();
		}
		else
		{
			m_strAdjustInstance = "";
			controlDisable(PANEL_MAIN, "button" );
		}
	}
	else if( p->command() == "packager_database" )
	{
		m_intPackagerDatabase = p->value();
	}
	else if (p->command() == "router_database")
	{
		m_intRouterDatabase = p->value();
	}
	else if( p->command() == "button_text" )
	{
		m_strButtonText = p->value();

		textPut( "text", m_strButtonText, PANEL_MAIN, "button" );
	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << bncs_string( "packager_database=%1" ).arg( m_intPackagerDatabase );
		sl << bncs_string( "level_id=%1").arg(m_strLevelId);
		sl << bncs_string( "packager_groupid=%1").arg(m_strPackagerGroupId);
		sl << bncs_string( "router_groupid=%1").arg(m_strSdiRouterGroupId);
		sl << bncs_string( "router_database=%1").arg(m_intRouterDatabase);
		sl << bncs_string( "button_text=%1").arg(m_strButtonText);

		return sl.toString( '\n' );
	}
	else if( p->command() == "_events" )
	{
//		bncs_stringlist sl;

//		sl << "dest=<dest index>";

//		return sl.toString( '\n' );
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;

		sl << "name=<name>";
		sl << "instance=<instance name>";
		sl << "database=<router database no>";
		sl << "index=<router index>";

		return sl.toString( '\n' );
	}
	return "";
}

void adjust_to_level::init()
{
	bncs_string strPackagerInstance, strSdiRouterInstance;
	instanceLookupComposite(m_strCompositeInstance, m_strPackagerGroupId, strPackagerInstance);
	instanceLookupComposite(m_strCompositeInstance, m_strSdiRouterGroupId, strSdiRouterInstance);
	getDev(strPackagerInstance, &m_intPackagerDevice);
	getDev(strSdiRouterInstance, &m_intRouterDevice);
	
	if (m_intPackagerDevice>0 && m_intRouterDevice>0 && m_intPackagerDatabase>-1 && m_intRouterDatabase>-1)
	{
		m_blnValid  = m_strLevelId.length();
	}

	debug("adjust_to_level::packager=%1, dev=%2, router=%3, dev=%4, level=%5, valid=%6", strPackagerInstance, m_intPackagerDevice, strSdiRouterInstance, m_intRouterDevice, m_strLevelId, m_blnValid);

}

// timer events come here
void adjust_to_level::timerCallback( int id )
{
	if( id==TIMER_INIT)
	{
		timerStop(id);
		init();

	}
}
