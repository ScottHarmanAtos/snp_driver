#ifndef monitor_stack_scroll_INCLUDED
	#define monitor_stack_scroll_INCLUDED

#include <bncs_script_helper.h>
#include <map>
#include <vector>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif

struct monitor
{
public:
	bncs_string Instance;
	int Index;
	bool vaild;

	monitor(const bncs_string& Instance, const int Index)
	{
		this->Instance = Instance;
		this->Index = Index;
		this->vaild = true;
	}

	monitor()
	{
		this->vaild = false;
	}
};

class grid_data
{
public:
	int rows;
	int columns;
	bncs_string name;
	map<DWORD, monitor> instanceAndIndex;
	int location_row;
	int location_colum;
	bool vaild;

	grid_data()
	{
		this->vaild = false;
	}

	grid_data(const grid_data &obj)
	{
		this->rows = obj.rows;
		this->columns = obj.columns;
		this->name = obj.name;
		this->instanceAndIndex = obj.instanceAndIndex;
		this->location_row = obj.location_row;
		this->location_colum = obj.location_colum;
		this->vaild = obj.vaild;
	}

	grid_data(const bncs_string& placeToLook)
	{
		//Load the GridData from somewhere
		/*
		<monitor_stack_scroll>
			<stack id="MCR" title="MCR">
				<item id="size">
					<size id="rows" value="2"/>
					<size id="columns" value="6"/>
				</item>
				<item id="monitors">
					<mv instance="package_router" index="1987" row="1" column="1"/>
					<mv instance="package_router" index="1988" row="1" column="2"/>
					<mv instance="package_router" index="1989" row="1" column="3"/>
					<mv instance="package_router" index="1990" row="1" column="4"/>
					<mv instance="package_router" index="1991" row="1" column="5"/>
					<mv instance="package_router" index="1992" row="1" column="6"/>
					<mv instance="package_router" index="1981" row="2" column="1"/>
					<mv instance="package_router" index="1982" row="2" column="2"/>
					<mv instance="package_router" index="1983" row="2" column="3"/>
					<mv instance="package_router" index="1984" row="2" column="4"/>
					<mv instance="package_router" index="1985" row="2" column="5"/>
					<mv instance="package_router" index="1986" row="2" column="6"/>
				</item>
			</stack>
		</monitor_stack_scroll>
		*/

		this->name = placeToLook;

		bncs_config cSize(bncs_string("monitor_stacks.%1.size").arg(placeToLook));
		this->rows = 0;
		this->columns = 0;
		while (cSize.isChildValid())
		{
			bncs_string id = cSize.childAttr("id");
			int value = cSize.childAttr("value");
			if (id == "rows")
			{
				this->rows = value;
			}
			else if (id == "columns")
			{
				this->columns = value;
			}
			cSize.nextChild();
		}

		bool MonitorFound = false;
		bncs_config cMVs(bncs_string("monitor_stacks.%1.monitors").arg(placeToLook));
		while (cMVs.isChildValid())
		{
			MonitorFound = true;
			bncs_string instance = cMVs.childAttr("instance");
			int index = cMVs.childAttr("index");
			int row = cMVs.childAttr("row");
			int column = cMVs.childAttr("column");
			instanceAndIndex[MAKELONG(row, column)] = monitor(instance, index);
			cMVs.nextChild();
		}

		location_row = 1;
		location_colum = 1;

		if (this->rows == 0 || this->columns == 0 || !MonitorFound)
			this->vaild = false;
		else
			this->vaild = true;
	}

	monitor getMonitor(int row, int column) const
	{
		map<DWORD, monitor>::const_iterator it = instanceAndIndex.find(MAKELONG(row, column));
		if (it != instanceAndIndex.end())
		{
			return it->second;
		}
		return monitor();
	}
};


class monitor_stack_scroll : public bncs_script_helper
{
public:
	monitor_stack_scroll( bncs_client_callback * parent, const char* path );
	virtual ~monitor_stack_scroll();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	grid_data DisplayMimicPanel(const grid_data& data);
	grid_data DisplayWallMimic(const grid_data&);
	grid_data m_monitor_stack_scroll;

	void EnableButton(const bncs_string& button,bool enable);

	enum direction
	{
		Up,
		Down,
		Left,
		Right,
		Initial
	};

	grid_data MoveStackMimic(const grid_data&, direction);
	void ShowHideMoveButtons(const grid_data&);
	void DisplayMimic(const grid_data& old, const grid_data& _new);
	void TargetMimicInstance(const grid_data& data);

	bncs_string m_id; //The stack to load
	int m_displayed_monitor_count;
};


#endif // monitor_stack_scroll_INCLUDED