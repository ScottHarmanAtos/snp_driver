#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "monitor_stack_scroll.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define FIRE_FIRST_CONNECTION 1

//Magic Numbers
const double WIDTH = 245;
const double HEIGHT = 75;
const double BORDER = 5;

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( monitor_stack_scroll )

// constructor - equivalent to ApplCore STARTUP
monitor_stack_scroll::monitor_stack_scroll( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_displayed_monitor_count = 2;//Default
}

// destructor - equivalent to ApplCore CLOSEDOWN
monitor_stack_scroll::~monitor_stack_scroll()
{
}

// all button pushes and notifications come here
void monitor_stack_scroll::buttonCallback( buttonNotify *b )
{
	b->dump("monitor_stack_scroll::buttonCallback");
	bncs_stringlist sl(b->id(), '_');
	if (b->panel() == PNL_MAIN)
	{
		if (sl[0] == "scroll")
		{
			direction d = direction::Initial;
			if (sl[1] == "up")
			{
				d = direction::Up;
			}
			else if (sl[1] == "down")
			{
				d = direction::Down;
			}
			else if (sl[1] == "left")
			{
				d = direction::Left;
			}
			else if (sl[1] == "right")
			{
				d = direction::Right;
			}
			m_monitor_stack_scroll = MoveStackMimic(m_monitor_stack_scroll, d);
		}
	}
}

// all revertives come here
int monitor_stack_scroll::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void monitor_stack_scroll::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string monitor_stack_scroll::parentCallback( parentNotify *p )
{
	if (p->command() == "id")
	{		
		m_id = p->value();
		m_monitor_stack_scroll = grid_data(m_id);
		m_monitor_stack_scroll = DisplayMimicPanel(m_monitor_stack_scroll);
	}
	else if (p->command() == "displayed_monitor_count")
	{
		//Has to be a minimum of 1, 0 makes no sense
		int i = m_displayed_monitor_count;
		if (p->value() <= 0)
			m_displayed_monitor_count = 1;
		else
			m_displayed_monitor_count = p->value();

		if (m_displayed_monitor_count != i)
			m_monitor_stack_scroll = DisplayMimicPanel(m_monitor_stack_scroll);
	}
	else if (p->command() == "return")
	{
		bncs_stringlist sl;
		sl << bncs_string("id=").append(m_id);
		sl << bncs_string("displayed_monitor_count=").append(m_displayed_monitor_count);
		return sl.toString('\n');
	}
	else if (p->command() == "_events")
	{
		bncs_stringlist sl;

		sl << "monitor.<id>.<instance>=<index>";

		return sl.toString('\n');
	}
	return "";
}

// timer events come here
void monitor_stack_scroll::timerCallback( int id )
{
	switch( id )
	{
	case FIRE_FIRST_CONNECTION:
		timerStop(id);
		TargetMimicInstance(m_monitor_stack_scroll);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
grid_data monitor_stack_scroll::DisplayMimicPanel(const grid_data& data)
{
	//Display the initial wall
	//Need to get information as to which wall this should be

	if (data.vaild == false)
	{
		//Hmm no walls to display, shouldn't show this panel then
		panelDestroy(PNL_MAIN);
		panelShow(PNL_MAIN, "monitor_stack_scroll_error.bncs_ui");
		bncs_string error;
		textGet("text", PNL_MAIN, "error", error);		
		textPut("text", error.arg(data.name), PNL_MAIN, "error");
		return grid_data();
	}

	panelDestroy(PNL_MAIN);
	panelShow(PNL_MAIN, "monitor_stack_scroll.bncs_ui");

	timerStart(FIRE_FIRST_CONNECTION, FIRE_FIRST_CONNECTION);//Fire Connection
	return DisplayWallMimic(data);;
}


grid_data monitor_stack_scroll::DisplayWallMimic(const grid_data& data)
{
	bncs_string x;
	bncs_string y;
	controlPosition(PNL_MAIN, "mv_wall_grid", x, y);

	textPut("text", data.name, PNL_MAIN, "mv_wall_name");
	//Width and Height WE JUST KNOW
	//Need to update if its changes.

	int bWidth = (int)(WIDTH - ((data.columns * BORDER) + BORDER)) / data.columns;
	int bHeight = (int)(HEIGHT - ((data.rows * BORDER) + BORDER)) / data.rows;

	//debug("monitor_stack_scroll::DisplayWallMimic rows:%1 Columns:%2", data.rows, data.columns);
	//Loop though and display
	int currentY = y;
	for (int r = 1; r <= data.rows; r++)
	{
		currentY += (int)BORDER;
		int currentX = x;
		for (int c = 1; c <= data.columns; c++)
		{
			currentX += (int)BORDER;
			controlCreate(PNL_MAIN, bncs_string("mimc_%1").arg(MAKELONG(r, c)), "bncs_control", currentX, currentY, bWidth, bHeight,bncs_string("style=label"));
			currentX += bWidth;
		}
		currentY += bHeight;
	}

	return MoveStackMimic(data, direction::Initial);
}

void monitor_stack_scroll::EnableButton(const bncs_string& button, bool enable)
{
	if (enable)
	{
		controlDisable(PNL_MAIN, button);
	}
	else
	{
		controlEnable(PNL_MAIN, button);
	}
}

void monitor_stack_scroll::ShowHideMoveButtons(const grid_data& data)
{
	EnableButton("scroll_up",data.location_row == 1 || data.rows == 1);
	EnableButton("scroll_down",data.location_row >= data.rows || data.rows == 1);
	EnableButton("scroll_left", data.location_colum == 1 || data.columns <= m_displayed_monitor_count);
	EnableButton("scroll_right", data.location_colum + (m_displayed_monitor_count-1) >= data.columns || data.columns <= m_displayed_monitor_count);
}

grid_data monitor_stack_scroll::MoveStackMimic(const grid_data& data, direction dir)
{
	grid_data d(data);

	switch (dir)
	{
		case direction::Initial:
			d.location_colum = 1;
			d.location_row = 1;
			break;
		case direction::Up:			
			d.location_row -= 1;
			break;
		case direction::Down:
			d.location_row += 1;
			break;
		case direction::Left:
			d.location_colum -= 1;
			break;
		case direction::Right:
			d.location_colum += 1;
			break;
	}

	ShowHideMoveButtons(d);
	DisplayMimic(data, d);
	TargetMimicInstance(d);
	return d;
}

void monitor_stack_scroll::DisplayMimic(const grid_data& old, const grid_data& _new)
{

	//Check old and new and display
	if (old.location_row != _new.location_row)
	{
		//Change everything
		for (int i = 0; i < m_displayed_monitor_count; i++)
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, bncs_string("mimc_%1").arg(MAKELONG(old.location_row, old.location_colum + i)));
			textPut("statesheet", "enum_selected", PNL_MAIN, bncs_string("mimc_%1").arg(MAKELONG(_new.location_row, _new.location_colum + i)));
		}
	}
	else
	{
		//Only partial change
		if (old.location_colum < _new.location_colum)
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, bncs_string("mimc_%1").arg(MAKELONG(old.location_row, old.location_colum)));
			textPut("statesheet", "enum_selected", PNL_MAIN, bncs_string("mimc_%1").arg(MAKELONG(_new.location_row, _new.location_colum + (m_displayed_monitor_count - 1))));
		}
		else if (old.location_colum > _new.location_colum )
		{
			textPut("statesheet", "enum_deselected", PNL_MAIN, bncs_string("mimc_%1").arg(MAKELONG(old.location_row, old.location_colum + (m_displayed_monitor_count -1))));
			textPut("statesheet", "enum_selected", PNL_MAIN, bncs_string("mimc_%1").arg(MAKELONG(_new.location_row, _new.location_colum)));
		}
		else if (old.location_colum == _new.location_colum)
		{
			//Probably the initial print all
			for (int i = 0; i < m_displayed_monitor_count; i++)
			{
				textPut("statesheet", "enum_selected", PNL_MAIN, bncs_string("mimc_%1").arg(MAKELONG(_new.location_row, _new.location_colum + i)));
			}
		}
	}
}

void monitor_stack_scroll::TargetMimicInstance(const grid_data& data)
{
	for (int i = 0; i < m_displayed_monitor_count; i++)
	{
		monitor mon = data.getMonitor(data.location_row, data.location_colum + i);
		if (mon.vaild == true)
		{
			hostNotify(bncs_string("monitor.%1.%2=%3").arg(i + 1).arg(mon.Instance).arg(mon.Index));
		}
		else
		{
			hostNotify(bncs_string("monitor.%1=").arg(i + 1));
		}
	}
}
