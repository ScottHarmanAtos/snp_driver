# Monitor Stack Scroll

This component can be used to select monitors from a multiple monitor stack.

For example on a monitor stack that is 2x6, you may only have enough room on the
panel to display 2 Monitors in full.

With this component you can select navigate around the full stack to select different
monitors.

Whenever the selection of which monitors on the stack are under control changes the 
component fires a host notify for each monitor displayed with the new instance and index
that should be shown.

## Config

The component looks at the monitor_stacks.xml

This tell the component the size of the monitor stack and which instance and index each monitor has.

``` xml

<monitor_stacks>
	<stack id="MCR" title="MCR">
		<item id="size">
			<size id="rows" value="2"/>
			<size id="columns" value="6"/>
		</item>
		<item id="monitors">
			<mv instance="package_router" index="1987" row="1" column="1"/>
			<mv instance="package_router" index="1988" row="1" column="2"/>
			<mv instance="package_router" index="1989" row="1" column="3"/>
			<mv instance="package_router" index="1990" row="1" column="4"/>
			<mv instance="package_router" index="1991" row="1" column="5"/>
			<mv instance="package_router" index="1992" row="1" column="6"/>
			<mv instance="package_router" index="1981" row="2" column="1"/>
			<mv instance="package_router" index="1982" row="2" column="2"/>
			<mv instance="package_router" index="1983" row="2" column="3"/>
			<mv instance="package_router" index="1984" row="2" column="4"/>
			<mv instance="package_router" index="1985" row="2" column="5"/>
			<mv instance="package_router" index="1986" row="2" column="6"/>
		</item>
	</stack>
</monitor_stacks>

```

## Parameters

### id
This is the id of the section within monitor_stacks.xml

### displayed_monitor_count
This is the number of monitors that are displayed on screen at any one time.

## Events

### monitor
An event is fired for each displayed monitor. So if 2 monitors are displayed 2 events will be fired

The events look like this. monitor.[id].[instance]=[index]

**Example**
```
monitor.1.package_router=1
monitor.2.package_router=2

```