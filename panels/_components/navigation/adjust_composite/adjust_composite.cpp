#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "adjust_composite.h"
#include <math.h> 
// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(adjust_composite)

#define PANEL_MAIN   1
#define TIMER_INIT   1
#define TIMEOUT_INIT 10

#define PANEL_POPUP  2

#define BUTTON_HEIGHT 60
#define BUTTON_WIDTH 170
#define POPUP_WIDTH 150
#define POPUP_MARGIN 0


#define BUTTON_PADDING 5

#define DATABASE_SOURCE_ADAPTATION 16
#define DATABASE_DEST_ADAPTATION 17

#define PANEL_HEIGHT 924

#define ICON_COMPOSITE  "/images/icon_composite.png"

// constructor - equivalent to ApplCore STARTUP
adjust_composite::adjust_composite( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_strPackagerCompositeInstance = "";
	m_intPackagerDatabase          = -1;
	m_intRouterDatabase            = -1;
	m_intPackagerDevice            = 0;
	m_intRouterDevice              = 0;

	m_blnValid                     = false;
	m_intPackageIndex              = 0;
	m_intRouterIndex               = 0;
	m_blnBaseInstanceSet = false;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );
	controlDisable( PANEL_MAIN, "button" );

}

// destructor - equivalent to ApplCore CLOSEDOWN
adjust_composite::~adjust_composite()
{
}

// all button pushes and notifications come here
void adjust_composite::buttonCallback( buttonNotify *b )
{
	b->dump("adjust_composite");
	if (b->panel() == PANEL_MAIN)
	{
		if (b->id() == "button")
		{
			m_mapButtonToInstance.clear();
			int adjustcount = m_mapGroupToInstance.size();
			debug("adjust_composite::buttonCallback adjustcount=%1", adjustcount);
			
			//If the only adjustable instance is the real instance than we should navigate to it, rahter than using a popup
			if (m_blnBaseInstanceSet && adjustcount == 1)
			{
				if (m_mapGroupToInstance.size() == 1)
					navigateAdjust(m_mapGroupToInstance.begin()->second);
			}
			else if (adjustcount > 0)
			{	
				//This is the number of columns needed, in most cases this will be one
				int NumberOfColumnsNeeded = (int)ceil((float)(BUTTON_HEIGHT *  (adjustcount + 1))/PANEL_HEIGHT); //The plus one is for the height of the label at the top

				int height = 0;
				if (NumberOfColumnsNeeded > 1)
				{
					int MaxButtonsPerColumn = PANEL_HEIGHT / BUTTON_HEIGHT;
					height = (MaxButtonsPerColumn * BUTTON_HEIGHT) + (BUTTON_PADDING * (m_mapGroupToInstance.size() + 2));
				}
				else
				{
					height = (m_mapGroupToInstance.size()*BUTTON_HEIGHT) + BUTTON_HEIGHT + (BUTTON_PADDING * (m_mapGroupToInstance.size() + 2));
				}

				panelPopup(PANEL_POPUP, "", 2 * POPUP_MARGIN + (BUTTON_WIDTH * NumberOfColumnsNeeded) + (BUTTON_PADDING * NumberOfColumnsNeeded -1), height);

				int y = BUTTON_PADDING;
				int x = BUTTON_PADDING;

				controlCreate(PANEL_POPUP, "info", "bncs_control", x, y, (BUTTON_WIDTH * NumberOfColumnsNeeded) + (BUTTON_PADDING * NumberOfColumnsNeeded - 1) + (2 * POPUP_MARGIN) - (2 * BUTTON_PADDING), BUTTON_HEIGHT, "style=label\nnotify.released=false\nstylesheet=adjust_details\n");

				y += BUTTON_HEIGHT + BUTTON_PADDING;
				x = POPUP_MARGIN;

				//Add elipses - this isn't great as it doesn't know the size of the char that are in the string
				bncs_string strAdjustName = addElipse(m_strAdjustName, 18 * NumberOfColumnsNeeded);
				debug("adjust_composite::buttonCallback m_strAdjustName:%1 strAdjustName:%2", m_strAdjustName, strAdjustName);
				// add some useful debugging text			
				textPut("text", bncs_string("Adjust| |%1").arg(strAdjustName), PANEL_POPUP, "info");
				//first button goes at position y
				
				debug("adjust_composite::buttonCallback y=%1", y);

				int adjustCount = m_mapGroupToInstance.size();
				debug("adjust_composite::buttonCallback adjustCount=%1", adjustCount);

				LIST_STRING_STRING_ITERATOR it;
				for (it = m_mapGroupToInstance.begin(); it != m_mapGroupToInstance.end(); ++it)
				{
					bncs_string controlID = bncs_string("adjust_%1_%2").arg(it->first).arg(it->second);
					bncs_string name = addElipse(it->second, 18);
					bncs_string controlText = bncs_string("%1| |%2").arg(it->first).replace('_', ' ').arg(name);

					bncs_string controlName = controlCreate(PANEL_POPUP, "adjust", "bncs_control", x, y, BUTTON_WIDTH, BUTTON_HEIGHT, bncs_string("style=button\nnotify.released=true\ntext=%1").arg(controlText));
					m_mapButtonToInstance.push_back(std::pair<bncs_string, bncs_string>(controlName, it->second));

					textPut("text", controlText, PANEL_POPUP, controlName);
					if (navigateCanAdjust(it->second))
					{
						controlEnable(PANEL_POPUP, controlName);
					}
					else
					{
						controlDisable(PANEL_POPUP, controlName);
					}
					controlShow(PANEL_POPUP, controlName);
					//						int y = POPUP_MARGIN + ((counter - 1) * BUTTON_HEIGHT);
					y += BUTTON_HEIGHT + BUTTON_PADDING;
					if (y > PANEL_HEIGHT)
					{
						y = BUTTON_HEIGHT + (2 * BUTTON_PADDING);
						x += POPUP_MARGIN + BUTTON_WIDTH + BUTTON_PADDING;
					}

				}
			}

		}
	}
	else if (b->panel() == PANEL_POPUP)
	{
		for (LIST_STRING_STRING_ITERATOR it = m_mapButtonToInstance.begin(); it != m_mapButtonToInstance.end(); ++it)
		{
			if (it->first == b->id())
			{
				debug("adjust_composite::buttonCallback instance is %1", it->second);
				if (it->second.length())
					navigateAdjust(it->second);
				break;
			}
		}
	}
}

bncs_string adjust_composite::addElipse(const bncs_string &s, int NumberOfCharsToElipseOn)
{
	if (s.length() > (unsigned int)NumberOfCharsToElipseOn)
	{
		bncs_string newS(s);
		newS.truncate(NumberOfCharsToElipseOn - 2);
		return newS + "...";
	}
	return bncs_string(s);
}

// all revertives come here
int adjust_composite::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void adjust_composite::databaseCallback( revertiveNotify * r )
{
	if (r->device()== m_intRouterDevice)
	{
		if ( r->index() == m_intRouterIndex)
		{
			if (r->database() == m_intRouterDatabase)
			{
				m_intPackageIndex = -1;
				m_intRouterIndex  = -1;
				controlDisable(PANEL_MAIN, "button");
			}

		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string adjust_composite::parentCallback( parentNotify *p )
{
	if( p->command() == "instance" )
	{
		m_strPackagerCompositeInstance = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "packager_groupid")
	{
		m_strPackagerGroupId = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "router_groupid")
	{
		m_strSdiRouterGroupId = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "level_id")
	{
		m_strLevelId = p->value();
	}
	else if( p->command() == "index" )
	{
		
		// set some assumed states
		textPut("pixmap", "", PANEL_MAIN, "button");
		controlDisable(PANEL_MAIN, "button");

		ProcessIndexChange(p->value().toInt());
	}
	else if( p->command() == "name" )
	{
		if( navigateCanAdjust( p->value() ))
		{
			controlEnable(PANEL_MAIN, "button" );
			m_strAdjustInstance = p->value();
		}
		else
		{
			m_strAdjustInstance = "";
			controlDisable(PANEL_MAIN, "button" );
		}
	}
	else if( p->command() == "packager_database" )
	{
		m_intPackagerDatabase = p->value();
	}
	else if (p->command() == "router_database")
	{
		m_intRouterDatabase = p->value();
	}
	else if( p->command() == "button_text" )
	{
		m_strButtonText = p->value();

		textPut( "text", m_strButtonText, PANEL_MAIN, "button" );
	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << bncs_string( "packager_database=%1" ).arg( m_intPackagerDatabase );
		sl << bncs_string( "level_id=%1").arg(m_strLevelId);
		sl << bncs_string( "packager_groupid=%1").arg(m_strPackagerGroupId);
		sl << bncs_string( "router_groupid=%1").arg(m_strSdiRouterGroupId);
		sl << bncs_string( "router_database=%1").arg(m_intRouterDatabase);
		sl << bncs_string( "button_text=%1").arg(m_strButtonText);

		return sl.toString( '\n' );
	}
	else if( p->command() == "_events" )
	{
//		bncs_stringlist sl;

//		sl << "dest=<dest index>";

//		return sl.toString( '\n' );
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;

		sl << "name=<name>";
		sl << "instance=<instance name>";
		sl << "database=<router database no>";
		sl << "index=<router index>";

		return sl.toString( '\n' );
	}
	return "";
}

// timer events come here
void adjust_composite::timerCallback( int id )
{
	if( id==TIMER_INIT)
	{
		timerStop(id);
		Init();

	}
}

void adjust_composite::Init()
{
	bncs_string strPackagerInstance, strSdiRouterInstance;
	instanceLookupComposite(m_strPackagerCompositeInstance, m_strPackagerGroupId, strPackagerInstance);
	instanceLookupComposite(m_strPackagerCompositeInstance, m_strSdiRouterGroupId, strSdiRouterInstance);
	getDev(strPackagerInstance, &m_intPackagerDevice);
	getDev(strSdiRouterInstance, &m_intRouterDevice);

	if (m_intPackagerDevice>0 && m_intRouterDevice>0 && m_intPackagerDatabase>-1 && m_intRouterDatabase>-1)
	{
		m_blnValid = m_strLevelId.length() > 0;
	}

	debug("adjust_composite::Init() packager=%1, dev=%2, router=%3, dev=%4, level=%5, valid=%6", strPackagerInstance, m_intPackagerDevice, strSdiRouterInstance, m_intRouterDevice, m_strLevelId, m_blnValid);

}

void adjust_composite::ProcessIndexChange(int Index)
{
	m_strAdjustName = "";
	m_blnBaseInstanceSet = false;

	m_intPackageIndex = Index;
	if (m_blnValid)
	{
		bncs_string strLevels;
		routerName(m_intPackagerDevice, m_intPackagerDatabase, m_intPackageIndex, strLevels);
		m_intRouterIndex = bncs_stringlist(strLevels).getNamedParam(m_strLevelId).toInt();
		debug("adjust_composite::ProcessIndexChange level=%1, levelIndex=%2", strLevels, m_intRouterIndex);
		bncs_string strAdjustInstance, strConnector, strAdjustInstanceAndConnector;
		debug("adjust_composite::ProcessIndexChange  dev:%1 db:%2 index:%3", m_intRouterDevice, m_intRouterDatabase, m_intRouterIndex);
		routerName(m_intRouterDevice, m_intRouterDatabase, m_intRouterIndex, strAdjustInstanceAndConnector);
		debug("adjust_composite::ProcessIndexChange AdjustAndConnector=%1", strAdjustInstanceAndConnector);

		bncs_string alt_id;
		int dev, slot, offset;
		char c;
		bool instanceFound = instanceLookup(strAdjustInstanceAndConnector, "", alt_id, &dev, &offset, &slot, &c);
		if (!instanceFound)
		{
			if (strAdjustInstanceAndConnector.contains('#'))
			{
				strAdjustInstanceAndConnector.split('#', strAdjustInstance, strConnector);
				instanceFound = instanceLookup(strAdjustInstance, "", alt_id, &dev, &offset, &slot, &c);
			}
		}
		else
		{
			strAdjustInstance = strAdjustInstanceAndConnector;
		}

		// we now have several scenarios:
		// 1. This is a composite instance with a typehandler and at least one chld has a typehandler
		// in which case expand popup panel with requisite number of adjust buttons
		// 2. This is a composite instance with a typehandler and no children have typehandlers
		// 3. This is a non-composite instance with typehandler
		// 4. This is a non-composite instance without a typehandler

		// in all cases


		m_mapButtonToInstance.clear();
		m_mapGroupToInstance.clear();

		/*
		if (!instanceFound)
		{
			debug("adjust_composite::ProcessIndexChange here..... {%1}", strAdjustInstanceAndConnector);
			textPut("pixmap", "", PANEL_MAIN, "button");
			controlDisable(PANEL_MAIN, "button");
			return;
		}
		*/

		if (navigateCanAdjust(strAdjustInstanceAndConnector))
		{
			m_blnBaseInstanceSet = true;
			m_mapGroupToInstance.push_back(std::pair<bncs_string, bncs_string>("Base Instance", strAdjustInstanceAndConnector));
		}

		bncs_config instanceLookup = bncs_config(bncs_string("instances.%1").arg(strAdjustInstance));
		
		m_strAdjustName = alt_id;

		if (instanceLookup.attr("composite") == "yes")
		{
			
			// we need to calculate the height of the popup before we instantiate it, so we work out how many buttons we require first
			debug("adjust_composite::ProcessIndexChange instance is composite:%1 ", alt_id);
			//Find child instances

			bool firstComposite = true;
			while (instanceLookup.isChildValid())
			{
				bncs_string id = instanceLookup.childAttr("id");
				bncs_string value = instanceLookup.childAttr("instance");			
				bncs_string type;
				bool instanceDoesNotExists = instanceLookupType(value, type);
				
				//Ignore those that can't be adjusted too, unless they have no instance.
				if (instanceDoesNotExists || navigateCanAdjust(value))
				{
					if (firstComposite)
					{
						textPut("pixmap", ICON_COMPOSITE, PANEL_MAIN, "button");
						firstComposite = false;
					}
					m_mapGroupToInstance.push_back(std::pair<bncs_string, bncs_string>(id, value));
				}
				instanceLookup.nextChild();
			}
		}

		// add the adaptation
		bncs_string streamRef, ipGateway;
		routerName(m_intRouterDevice, DATABASE_SOURCE_ADAPTATION, m_intRouterIndex, streamRef);

		bncs_stringlist sltAdaptation = bncs_stringlist(streamRef, '#');
		ipGateway = sltAdaptation[0];
		debug("adjust_composite::ProcessIndexChange streamref=%1 gateway=%2", streamRef, ipGateway);
		if (navigateCanAdjust(ipGateway))
		{
			debug("adjust_composite::ProcessIndexChange canadjust=%1", "true");

			m_mapGroupToInstance.push_back(std::pair<bncs_string, bncs_string>("adaptation", ipGateway));
		}

		
		if (m_mapGroupToInstance.size() > 0)
		{
			if (m_blnBaseInstanceSet && m_mapGroupToInstance.size() == 1)
			{
				textPut("pixmap", "", PANEL_MAIN, "button");
			}
			controlEnable(PANEL_MAIN, "button");
		}
		else
		{
			textPut("pixmap", "", PANEL_MAIN, "button");
			controlDisable(PANEL_MAIN, "button");
		}

	}
}
