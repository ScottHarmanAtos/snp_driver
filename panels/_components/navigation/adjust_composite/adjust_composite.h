#ifndef adjust_composite_INCLUDED
	#define adjust_composite_INCLUDED

#include <bncs_script_helper.h>
#include <list>
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
typedef list<pair<bncs_string,bncs_string>> LIST_STRING_STRING;
typedef list<pair<bncs_string, bncs_string>>::iterator LIST_STRING_STRING_ITERATOR;

class adjust_composite : public bncs_script_helper
{
public:
	adjust_composite( bncs_client_callback * parent, const char* path );
	virtual ~adjust_composite();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );	
	bncs_string parentCallback( parentNotify *p );	
	void timerCallback( int );
private:

	bncs_string m_strLevelId;

	bncs_string m_strPackagerCompositeInstance;
	int m_intPackagerDevice;
	int m_intPackagerDatabase;

	bncs_string m_strRouterInstance;
	int m_intRouterDevice;
	int m_intRouterDatabase;

	bncs_string m_strButtonText;
	bncs_string m_strAdjustInstance;

	bool m_blnValid;

	bncs_string m_strPackagerGroupId;
	bncs_string m_strSdiRouterGroupId;

	LIST_STRING_STRING m_mapButtonToInstance;
	LIST_STRING_STRING m_mapGroupToInstance;


	bncs_string m_strAdjustName;

	int m_intPackageIndex;
	int m_intRouterIndex;

	void ProcessIndexChange(int Index);
	void Init();
	bool m_blnBaseInstanceSet;
	bncs_string addElipse(const bncs_string &s, int NumberOfCharsToElipseOn);

};


#endif // adjust_composite_INCLUDED