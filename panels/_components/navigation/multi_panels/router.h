#pragma once
#include <bncs_script_helper.h>


class router
{
public:
	router()
	{
		this->Id = 0;
		this->Name = "";
		this->UiHost = "";
	}
	router(int Id, bncs_string Name = "", bncs_string UiHost = "")
	{
		this->Id = Id;
		this->Name = Name;
		this->UiHost = UiHost;
	}
	~router()
	{

	}

	int Id;
	bncs_string Name;
	bncs_string UiHost;
};


struct routerIdCompare
{
	bool operator() (const router& lhs, const router& rhs) const
	{
		return lhs.Id < rhs.Id;
	}
};