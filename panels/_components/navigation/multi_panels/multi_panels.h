#ifndef multi_panels_INCLUDED 
	#define multi_panels_INCLUDED

#include <bncs_script_helper.h>
#include "suite.h"
#include "router.h"
#include <map>
#include <vector>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
typedef vector<router> routers;
typedef map<suite, routers, suiteIdCompare> suites;

struct coords
{
	int x;
	int y;
	int h;
	int w;
};

class multi_panels : public bncs_script_helper
{
public:
	multi_panels( bncs_client_callback * parent, const char* path );
	virtual ~multi_panels();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int config_device;
	int config_db;
	suites Suites;

	bncs_string Area;

	void DisplaySuites(const suites& s);
	void DisplayRouters(int suite, const suites& s);

	void ShowError(bncs_string Error);

	int LastSelectedSuite;
	int LastSelectedRouter;

	bool CompareSuites(const suites &oldSuites, const suites &newSuites);
	bool CompareRoutes(int Suite, const suites &oldSuites, const suites &newSuites);

	void HidePanel();
	void HighlightSuite(int suiteId);
	void HighlightRouter(int routerId);

	void SelectSuite(int suiteId, const suites& s);
	void SelectRouter(int routerId, int suiteId, const suites& s);

	suites(multi_panels::*GetSuites)(void);

	void LoadFromDatabases();
	suites GetSuites_Databases(void);
	routers GetRouters_Databases(int Device, int DB, int RouteCount, int Index);

	void LoadFromXml();
	suites GetSuites_Xml(void);
	routers GetRouters_Xml(bncs_config routers);

	bncs_string CurrentLoadedRouter;
	map<bncs_string,bncs_string> LoadedRouters;

	coords uiHostLocation;
};


#endif // multi_panels_INCLUDED