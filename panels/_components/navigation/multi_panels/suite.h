#pragma once
#include <bncs_script_helper.h>

class suite
{
public:
	suite()
	{
		this->Id = 0;
		this->Name = "";
	}

	suite(int Id, bncs_string Name = "")
	{
		this->Id = Id;
		this->Name = Name;
	}
	~suite()
	{

	}

	int Id;
	bncs_string Name;

};

struct suiteIdCompare
{
	bool operator() (const suite& lhs, const suite& rhs) const
	{
		return lhs.Id < rhs.Id;
	}
};
