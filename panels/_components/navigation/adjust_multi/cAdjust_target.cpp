#include "cAdjust_target.h"
#include <bncs_config.h>


cAdjust_target::cAdjust_target()
{
	_ChildGroup = "";
	_ChildInstance = "";
}


cAdjust_target::~cAdjust_target()
{
}

bncs_string cAdjust_target::initTargetParameters(const bncs_string sGroup, const bncs_string sInstance, const bncs_string sConnector)
{
		_ChildGroup = sGroup;
		_ChildInstance = sInstance;
		return initChildren(sInstance, sConnector);
	
}

bncs_string  cAdjust_target::Group()
{
	return _ChildGroup;
}

bncs_string  cAdjust_target::initChildren(bncs_string sInstance, const bncs_string sConnector)
{
	bncs_config instanceLookup = bncs_config(bncs_string("instances.%1").arg(sInstance));

	if (instanceLookup.isValid())
	{
		if (instanceLookup.isChildValid())
		{

			while (instanceLookup.isChildValid())
			{
				bncs_string sId = "";
				bncs_string sValue;

				sId = instanceLookup.childAttr("id");

				if (instanceLookup.attr("composite") == "yes")
				{
					sValue = instanceLookup.childAttr("instance");
				}
				else
				{
					sValue = instanceLookup.childAttr("value");
				}

				if (sConnector == sId)
				{
					return sValue;
				}


				instanceLookup.nextChild();
			}
		}
		else
		{
			// No children so a straight instance
			return sInstance;
		}
	}
	return "";
}
