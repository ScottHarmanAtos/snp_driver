#pragma once
#include <bncs_stringlist.h>


class cVideo_Level
{
public:
	cVideo_Level(bool bSourceAdjust);
	virtual ~cVideo_Level();

	void reset();
	bool addVideo(bncs_string sParams);

	
	bncs_string AdjustTargetedInstance;
	bool AdjustTargetInstanceIsValid;

	bncs_string AdjustInstanceAndConnector;
	bncs_string AdjustInstance;
	bncs_string AdjustConnector;

	
	bncs_string Idx_Video_AndHubTag() const;
	bncs_string Idx_Video() const;
	bncs_string Idx_VidHubTag() const;

	bncs_string Video_ANC_1_AndHubTag() const;
	bncs_string Video_ANC_1() const;
	bncs_string Video_ANC_1_Idx_VidHubTag() const;

	bncs_string Video_ANC_2_AndHubTag() const;
	bncs_string Video_ANC_2() const;
	bncs_string Video_ANC_2_Idx_VidHubTag() const;

	bncs_string Video_ANC_3_AndHubTag() const;
	bncs_string Video_ANC_3() const;
	bncs_string Video_ANC_3_Idx_VidHubTag() const;

	bncs_string Video_ANC_4_AndHubTag() const;
	bncs_string Video_ANC_4() const;
	bncs_string Video_ANC_4_Idx_VidHubTag() const;

	bncs_string DestRev_Vision_AndHubTag() const;
	bncs_string DestRev_Vision() const;
	bncs_string DestRev_VisionTag() const;
	bncs_string DestIfb1() const;
	bncs_string DestIfb2() const;

	
	bncs_string Idx_ApAndHubTag() const;
	bncs_string Idx_Ap() const;
	bncs_string Idx_Ap_VidHubTag() const;

	private:

	bool _IsSourceAdjust;

	bncs_string _Idx_VideoAndHubTag;
	bncs_string _Idx_Video;
	bncs_string _Idx_Video_Idx_VidHubTag;

	bncs_string _Video_ANC_1_AndHubTag;
	bncs_string _Video_ANC_1;
	bncs_string _Video_ANC_1_Idx_VidHubTag;

	bncs_string _Video_ANC_2_AndHubTag;
	bncs_string _Video_ANC_2;
	bncs_string _Video_ANC_2_Idx_VidHubTag;

	bncs_string _Video_ANC_3_AndHubTag;
	bncs_string _Video_ANC_3;
	bncs_string _Video_ANC_3_Idx_VidHubTag;

	bncs_string _Video_ANC_4_AndHubTag;
	bncs_string _Video_ANC_4;
	bncs_string _Video_ANC_4_Idx_VidHubTag;

	bncs_string _Idx_AP_AndHubTag;
	bncs_string _Idx_AP;
	bncs_string _Idx_AP_Idx_VidHubTag;

	bncs_string _Rev_Vision_AndHubTag;
	bncs_string _Rev_Vision;
	bncs_string _Rev_Vision_HubTag;
	bncs_string _Ifb1;
	bncs_string _Ifb2;





	bncs_string extract_Index(bncs_string sParam);
	bncs_string extract_Tag(bncs_string sParam);

	void _reset();

};



