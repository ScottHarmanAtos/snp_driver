#ifndef adjust_multi_INCLUDED
	#define adjust_multi_INCLUDED

#include <bncs_script_helper.h>
#include <map>
#include <bncs_packager.h>
#include "cVideo_Level.h"
#include "cAdjust_Target.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif


#define SOURCE_MAX_BUTTON_VIDEO_LEVELS		4
#define DEST_MAX_BUTTON_VIDEO_LEVELS		1
class adjust_multi : public bncs_script_helper
{
public:
	adjust_multi( bncs_client_callback * parent, const char* path );
	virtual ~adjust_multi();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );	
	bncs_string parentCallback( parentNotify *p );	
	void timerCallback( int );
private:


	bncs_stringlist m_sLstLevelId;
	bncs_packager m_packagerRouter;
	
	bncs_string m_strPackagerCompositeInstance;
	int m_intPackagerMainBaseDevice;
	int m_intPackagerDatabase;

	bncs_string m_strVideoRouterCompositeInstance;
	int m_intVideoRouterMainBaseDevice;
	int m_intVideoRouterAdjustDatabase;
	bncs_string m_sVideoRouterGroup;
	map <bncs_string, int> mapVideoRouterSectionInstances;

	bncs_string m_strButtonText;
	bncs_string m_strAdjustInstance;

	// Can we put this in clsVideo levels
	//map<bncs_string, cAdjust_target *> mapAdjusts;


	bool m_blnValid;

	bncs_string m_strPackagerGroupId;
	bncs_string m_strPackagerGroupBaseInfoDriver;


	bncs_string m_strAdjustName;

	int m_intPackageIndex;			
	map<int, cVideo_Level*> m_mapRouterVideoLevel;			//  Level, class level data , 4 initially in Discovery

	void ProcessIndexChange(int Index);
	void Init();
	bool m_blnBaseInstanceSet;

	bool isComposite(bncs_string sEntry);
};


#endif // adjust_multi_INCLUDED
