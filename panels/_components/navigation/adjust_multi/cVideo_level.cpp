#include "cVideo_Level.h"


cVideo_Level::cVideo_Level(bool bSourceAdjust)
{
	_IsSourceAdjust = bSourceAdjust;
	
	_reset();
}


cVideo_Level::~cVideo_Level()
{
	_reset();
}

void cVideo_Level::_reset()
{

	AdjustInstanceAndConnector = "";
	AdjustInstance = "";
	AdjustConnector = "";
	AdjustTargetedInstance = "";

	AdjustTargetInstanceIsValid = false;

	_Idx_VideoAndHubTag= "";
	_Idx_Video = "";
	_Idx_Video_Idx_VidHubTag = "";

	_Video_ANC_1_AndHubTag = "";
	_Video_ANC_1 = "";
	_Video_ANC_1_Idx_VidHubTag = "";

	_Video_ANC_2_AndHubTag = "";
	_Video_ANC_2 = "";
	_Video_ANC_2_Idx_VidHubTag = "";

	_Video_ANC_3_AndHubTag = "";
	_Video_ANC_3 = "";
	_Video_ANC_3_Idx_VidHubTag = "";

	_Video_ANC_4_AndHubTag = "";
	_Video_ANC_4 = "";
	_Video_ANC_4_Idx_VidHubTag = "";

	_Rev_Vision_AndHubTag = "";
	_Rev_Vision = "";
	_Rev_Vision_HubTag = "";

	_Ifb1 = "";
	_Ifb2 = "";
		
	_Idx_AP_AndHubTag = "";
	_Idx_AP = "";
	_Idx_AP_Idx_VidHubTag = "";
}

bool cVideo_Level::addVideo(const bncs_string sParams)
{

	if (_IsSourceAdjust)
	{
		// check number of parameters passed
		// MUST be 6, '|' delimited

		// Split into the 6 pairs
		bncs_stringlist slstPairs(sParams, '|');

		if (slstPairs.count() == 6)
		{

			// Store these pairs as value # tag

			_Idx_VideoAndHubTag = slstPairs[0];
			_Video_ANC_1_AndHubTag = slstPairs[1];
			_Video_ANC_2_AndHubTag = slstPairs[2];
			_Video_ANC_3_AndHubTag = slstPairs[3];
			_Video_ANC_4_AndHubTag = slstPairs[4];
			_Idx_AP_AndHubTag = slstPairs[5];

			// Store as the actual index/Tag values
			_Idx_Video = extract_Index(slstPairs[0]);
			_Video_ANC_1 = extract_Index(slstPairs[1]);
			_Video_ANC_2 = extract_Index(slstPairs[2]);
			_Video_ANC_3 = extract_Index(slstPairs[3]);
			_Video_ANC_4 = extract_Index(slstPairs[4]);
			_Idx_AP = extract_Index(slstPairs[5]);

			_Idx_Video_Idx_VidHubTag = extract_Tag(slstPairs[0]);
			_Video_ANC_1_Idx_VidHubTag = extract_Tag(slstPairs[1]);
			_Video_ANC_2_Idx_VidHubTag = extract_Tag(slstPairs[2]);
			_Video_ANC_3_Idx_VidHubTag = extract_Tag(slstPairs[3]);
			_Video_ANC_4_Idx_VidHubTag = extract_Tag(slstPairs[4]);
			_Idx_AP_Idx_VidHubTag = extract_Tag(slstPairs[5]);

			return true;
		}
		else
		{
			// ERROR report here !
			return false;
		}
	}
	else
	{
		// check number of parameters passed
		// MUST be 8, ',' delimited

		// Split into the 6 pairs
		const bncs_stringlist slstParams(sParams, ',');

		if (slstParams.count() == 8)
		{
			// Store as the actual index/Tag values

			_Idx_VideoAndHubTag = bncs_stringlist(sParams).getNamedParam("video");
			_Idx_Video = extract_Index(_Idx_VideoAndHubTag);
			_Idx_Video_Idx_VidHubTag = extract_Tag(_Idx_VideoAndHubTag);

			_Video_ANC_1_AndHubTag = bncs_stringlist(sParams).getNamedParam("anc1");
			_Video_ANC_1 = extract_Index(_Video_ANC_1_AndHubTag);
			_Video_ANC_1_Idx_VidHubTag = extract_Tag(_Video_ANC_1_AndHubTag);

			_Video_ANC_2_AndHubTag = bncs_stringlist(sParams).getNamedParam("anc2");
			_Video_ANC_2 = extract_Index(_Video_ANC_2_AndHubTag);
			_Video_ANC_2_Idx_VidHubTag = extract_Tag(_Video_ANC_2_AndHubTag);

			_Video_ANC_3_AndHubTag = bncs_stringlist(sParams).getNamedParam("anc3");
			_Video_ANC_3 = extract_Index(_Video_ANC_3_AndHubTag);
			_Video_ANC_3_Idx_VidHubTag = extract_Tag(_Video_ANC_3_AndHubTag);

			_Video_ANC_4_AndHubTag = bncs_stringlist(sParams).getNamedParam("anc4");
			_Video_ANC_4 = extract_Index(_Video_ANC_4_AndHubTag);
			_Video_ANC_4_Idx_VidHubTag = extract_Tag(_Video_ANC_4_AndHubTag);

			_Rev_Vision_AndHubTag = bncs_stringlist(sParams).getNamedParam("rev_vision");
			_Rev_Vision = extract_Index(_Rev_Vision_AndHubTag);
			_Rev_Vision_HubTag = extract_Tag(_Rev_Vision_AndHubTag);
			
			_Ifb1 = bncs_stringlist(sParams).getNamedParam("ifb1");
			_Ifb2 = bncs_stringlist(sParams).getNamedParam("ifb2");

			return true;
		}
		else
		{
			// ERROR report here !
			return false;

		}

	}
}

bncs_string cVideo_Level::Idx_Video_AndHubTag() const
{
	return _Idx_VideoAndHubTag;
}

bncs_string cVideo_Level::Idx_Video() const
{
	return _Idx_Video;
}

bncs_string cVideo_Level::Idx_VidHubTag() const
{
	return _Idx_Video_Idx_VidHubTag;
}

bncs_string cVideo_Level::Video_ANC_1_AndHubTag() const
{
	return _Video_ANC_1_AndHubTag;
}

bncs_string cVideo_Level::Video_ANC_1() const
{
	return _Video_ANC_1;
}

bncs_string cVideo_Level::Video_ANC_1_Idx_VidHubTag() const
{
	return _Video_ANC_1_Idx_VidHubTag;
}

bncs_string cVideo_Level::Video_ANC_2() const
{
	return _Video_ANC_2;
}

bncs_string cVideo_Level::Video_ANC_2_AndHubTag() const
{
	return _Video_ANC_2_AndHubTag;
}

bncs_string cVideo_Level::Video_ANC_2_Idx_VidHubTag() const
{
	return _Video_ANC_2_Idx_VidHubTag;
}

bncs_string cVideo_Level::Video_ANC_3_AndHubTag() const
{
	return _Video_ANC_3_AndHubTag;
}

bncs_string cVideo_Level::Video_ANC_3() const
{
	return _Video_ANC_3;
}

bncs_string cVideo_Level::Video_ANC_3_Idx_VidHubTag() const
{
	return _Video_ANC_3_Idx_VidHubTag;
}

bncs_string cVideo_Level::Video_ANC_4_AndHubTag() const
{
	return _Video_ANC_4_AndHubTag;
}

bncs_string cVideo_Level::Video_ANC_4() const
{
	return _Video_ANC_4;
}

bncs_string cVideo_Level::Video_ANC_4_Idx_VidHubTag() const
{
	return _Video_ANC_4_Idx_VidHubTag;
}

bncs_string cVideo_Level::Idx_ApAndHubTag() const
{
	return _Idx_AP_AndHubTag;
}

bncs_string cVideo_Level::Idx_Ap() const
{
	return _Idx_AP;
}

bncs_string cVideo_Level::Idx_Ap_VidHubTag() const
{
	return _Idx_AP_Idx_VidHubTag;
}


bncs_string cVideo_Level::DestRev_Vision_AndHubTag() const
{
	return _Rev_Vision_AndHubTag;
}

bncs_string cVideo_Level::DestRev_Vision() const
{
	return _Rev_Vision;
}

bncs_string cVideo_Level::DestRev_VisionTag() const
{
	return _Rev_Vision_HubTag;
}

bncs_string cVideo_Level::DestIfb1() const
{
	return _Ifb1;
}

bncs_string cVideo_Level::DestIfb2() const
{
	return _Ifb2;
}


bncs_string cVideo_Level::extract_Index(bncs_string const sParam)
{
	bncs_string s0 = "";
	bncs_string s1 = "";

	bncs_string sTmp = sParam.split('#', s0, s1);

	return s0;

}

bncs_string cVideo_Level::extract_Tag(bncs_string const sParam)
{
	bncs_string s0 = "";
	bncs_string s1 = "";

	bncs_string sTmp = sParam.split('#', s0, s1);

	return s1;

}

void cVideo_Level::reset()
{
	_reset();
}


