#include <windows.h>
#include <cstdio>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "adjust_multi.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(adjust_multi)

#define PANEL_MAIN   1
#define TIMER_INIT   1
#define TIMEOUT_INIT 10

#define PANEL_LEVELS_POPUP  2

#define ICON_COMPOSITE  "/images/icon_composite.png"

#define BTN_BUTTON	"button"

// constructor - equivalent to ApplCore STARTUP
adjust_multi::adjust_multi( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_strPackagerCompositeInstance	= "";
	m_intPackagerDatabase			= -1;
	m_intVideoRouterAdjustDatabase	= -1;
	m_intPackagerMainBaseDevice		= 0;
	m_intVideoRouterMainBaseDevice	= 0;
	m_sVideoRouterGroup				= "";
	m_blnValid						= false;
	m_intPackageIndex				= 0;
	m_blnBaseInstanceSet			= false;

	m_strPackagerGroupBaseInfoDriver = "";
	
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );
	controlDisable(PANEL_MAIN, BTN_BUTTON);

}

// destructor - equivalent to ApplCore CLOSEDOWN
adjust_multi::~adjust_multi()
{
}

// all button pushes and notifications come here
void adjust_multi::buttonCallback( buttonNotify *b)
{
	b->dump("adjust_multi");
	if (b->panel() == PANEL_MAIN)
	{
		if (b->id() == BTN_BUTTON)
		{
			const int adjustcount = m_mapRouterVideoLevel.size();

			debug("adjust_multi::buttonCallback adjustcount=%1", adjustcount);
			
			//If there is only one adjustable instance is the real instance than we should navigate to it, rather than using a popup
			if (adjustcount == 1)
			{
				// First and only entry !
				if (m_mapRouterVideoLevel.begin()->second->AdjustTargetInstanceIsValid)
				{
					if (navigateCanAdjust(m_mapRouterVideoLevel.begin()->second->AdjustTargetedInstance))
					{
						navigateAdjust(m_mapRouterVideoLevel.begin()->second->AdjustTargetedInstance);
					}
					else
					{
							
					}
				}
			}
			else if (adjustcount > 0)
			{

				panelPopup(PANEL_LEVELS_POPUP, "popup_levels.bncs_ui");

				for (int i = 1; i <= SOURCE_MAX_BUTTON_VIDEO_LEVELS; i++)
				{
					controlDisable(PANEL_LEVELS_POPUP, bncs_string("btn_level_%1").arg(i));
				}

				for (map<int, cVideo_Level*>::iterator m_mapITRouterVideoLevel = m_mapRouterVideoLevel.begin(); m_mapITRouterVideoLevel != m_mapRouterVideoLevel.end(); m_mapITRouterVideoLevel++)
				{
					controlEnable(PANEL_LEVELS_POPUP, bncs_string("btn_level_%1").arg(m_mapITRouterVideoLevel->first));
				}
				
				return;
				
			}
		}
	}
	else if (b->panel() == PANEL_LEVELS_POPUP)
	{
		if (b->id().startsWith(("btn_")))
		{
			// Which button ?
			bncs_stringlist slstButtons(b->id(), '_');
			const int iID = slstButtons[slstButtons.count() - 1].toInt();

			if (m_mapRouterVideoLevel.find(iID) != m_mapRouterVideoLevel.end())
			{
				debug("adjust_multi::buttonCallback instance is %1", m_mapRouterVideoLevel[iID]->AdjustTargetedInstance);
				navigateAdjust(m_mapRouterVideoLevel[iID]->AdjustTargetedInstance);
			}
		}
	}
}

// all revertives come here
int adjust_multi::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void adjust_multi::databaseCallback( revertiveNotify * r )
{
	if (r->device() == m_intVideoRouterMainBaseDevice)
	{
		// Have po process for each level !
	}
	else if (r->device() == m_intPackagerMainBaseDevice)
	{
		if (r->index() == m_intPackageIndex)
		{
			if (r->database() == m_intPackagerDatabase)
			{
				ProcessIndexChange(m_intPackageIndex);
			}
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string adjust_multi::parentCallback( parentNotify *p )
{
	if( p->command() == "instance" )
	{
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if( p->command() == "index" )
	{
		
		// set some assumed states
		textPut("pixmap", "", PANEL_MAIN, BTN_BUTTON);
		controlDisable(PANEL_MAIN, BTN_BUTTON);

		ProcessIndexChange(p->value().toInt());
	}
	else if( p->command() == "name" )
	{
		if( navigateCanAdjust( p->value() ))
		{
			controlEnable(PANEL_MAIN, BTN_BUTTON);
			m_strAdjustInstance = p->value();
		}
		else
		{
			m_strAdjustInstance = "";
			controlDisable(PANEL_MAIN, BTN_BUTTON);
		}
	}
	else if( p->command() == "packager_database" )
	{
		m_intPackagerDatabase = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "router_adjust_database")
	{
		m_intVideoRouterAdjustDatabase = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "router_video_instance")
	{
		m_strVideoRouterCompositeInstance = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if (p->command() == "router_video_group")
	{
		m_sVideoRouterGroup = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	
	else if (p->command() == "button_text")
	{
		m_strButtonText = p->value();

		textPut("text", m_strButtonText, PANEL_MAIN, BTN_BUTTON);
	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << bncs_string( "packager_database=%1" ).arg( m_intPackagerDatabase );
		sl << bncs_string("router_video_instance=%1").arg(m_strVideoRouterCompositeInstance);
		sl << bncs_string("router_video_group=%1").arg(m_sVideoRouterGroup);
		sl << bncs_string("router_adjust_database=%1").arg(m_intVideoRouterAdjustDatabase);
		sl << bncs_string("button_text=%1").arg(m_strButtonText);

		return sl.toString( '\n' );
	}
	else if( p->command() == "_events" )
	{
//		bncs_stringlist sl;

//		sl << "dest=<dest index>";

//		return sl.toString( '\n' );
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;

		sl << "name=<name>";
		sl << "instance=<instance name>";
		sl << "database=<router database no>";
		sl << "index=<router index>";

		return sl.toString( '\n' );
	}
	return "";
}

// timer events come here
void adjust_multi::timerCallback( int id )
{
	if( id==TIMER_INIT)
	{
		timerStop(id);
		Init();

	}
}

void adjust_multi::Init()
{
	const bncs_string strPackagerInstance = "";
	bncs_string strPackagerRouterInstance = "";

	const bncs_string strVideoRouterInstance = "";

	//prepare for selected dest tally handling
	m_strPackagerCompositeInstance = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_strPackagerCompositeInstance, "router");

	debug("dest_package::initDestPackage() dest_status is %1", m_packagerRouter.isValid() ? "valid" : "NOT valid");

	m_intPackagerMainBaseDevice = m_packagerRouter.destDev(1);

	//Register for database changes
	infoRegister(m_intPackagerMainBaseDevice, 1, 1);
	
	////////////////////////////////////
	// Find the video router info
	// As of yet we do not have a helper for this so........

	bncs_string sVideoRouterBaseInstance = "";
	bncs_string sVideoRouterSectionInstance = "";

	instanceLookupComposite(m_strVideoRouterCompositeInstance, m_sVideoRouterGroup, sVideoRouterBaseInstance);

	mapVideoRouterSectionInstances.clear();

	int iVideoRouterSectionDevice = -1;

	if (isComposite(sVideoRouterBaseInstance))
	{
		int i = 1;
		do
		{
			sVideoRouterSectionInstance = "";
			
			// find as many sections ( _01, _02 etc) as we can - future expansion
			instanceLookupComposite(sVideoRouterBaseInstance, bncs_string("section_%1").arg(i,'0', 2), sVideoRouterSectionInstance);

			if (sVideoRouterSectionInstance.length() == 0)
			{
				break;
			}

			iVideoRouterSectionDevice = -1;
			getDev(sVideoRouterSectionInstance, &iVideoRouterSectionDevice);
			mapVideoRouterSectionInstances.insert(std::pair<bncs_string, int>(sVideoRouterSectionInstance, iVideoRouterSectionDevice));

			i++;
			
		} while (sVideoRouterSectionInstance.length() > 0);
	}
	else
	{
		getDev(strVideoRouterInstance, &iVideoRouterSectionDevice);
		mapVideoRouterSectionInstances.insert(std::pair <bncs_string, int>(sVideoRouterBaseInstance, iVideoRouterSectionDevice));
	}


	m_intVideoRouterMainBaseDevice = mapVideoRouterSectionInstances.begin()->second;

	if (m_intPackagerMainBaseDevice > 0 && (mapVideoRouterSectionInstances.begin()->second > 0) && m_intPackagerDatabase > -1 && m_intVideoRouterAdjustDatabase > -1)
	{
//		m_blnValid = m_sLstLevelId.count() > 0;
		m_blnValid = true;
	}

	debug("adjust_multi::Init() packager=%1, dev=%2, router=%3, dev=%4, levels=%5, valid=%6", strPackagerInstance, m_intPackagerMainBaseDevice, strVideoRouterInstance, m_intVideoRouterMainBaseDevice, m_sLstLevelId.toString(), m_blnValid);
}

void adjust_multi::ProcessIndexChange(int Index)
{
	m_strAdjustName = "";
	m_blnBaseInstanceSet = false;
	m_mapRouterVideoLevel.clear();
	
	m_intPackageIndex = Index;
	if ((m_blnValid) && (Index > -1))
	{

		bncs_string strLevels;
		routerName(m_intPackagerMainBaseDevice, m_intPackagerDatabase, m_intPackageIndex, strLevels);

		int iLevelCount;
		bool bALevelIsValid = false;
		bool m_bIsSourceAdjust = true;

		bncs_stringlist slstNamedParams = bncs_stringlist(strLevels).getNamedParam("L1");

		if (slstNamedParams.count() > 0)
		{
			iLevelCount = SOURCE_MAX_BUTTON_VIDEO_LEVELS;
		}
		else
		{
			// No source levels found
			// assume dest level adjust - but pass all poaramas as they are to do with this one level
//			slstNamedParams = bncs_stringlist(strLevels).getNamedParam("video");
			slstNamedParams = bncs_stringlist(strLevels);
			iLevelCount = DEST_MAX_BUTTON_VIDEO_LEVELS;
			m_bIsSourceAdjust = false;
		}

		if (slstNamedParams.count() == 0)
		{
			// No params - error - so do not add
		}
		else
		{

			for (auto i = 1; i <= iLevelCount; i++)
			{

				auto *clsVideo_level = new cVideo_Level(m_bIsSourceAdjust);

				clsVideo_level->addVideo(slstNamedParams.toString());
				debug("adjust_multi::ProcessIndexChange PackageIndex=%1 Videolevel=%1, levelIndex=%2", m_intPackageIndex, i, slstNamedParams.toString());

				// Find the adjust connector for this video level
				routerName(m_intVideoRouterMainBaseDevice, m_intVideoRouterAdjustDatabase, clsVideo_level->Idx_Video(), clsVideo_level->AdjustInstanceAndConnector);
				debug("adjust_multi::ProcessIndexChange AdjustAndConnector=%1", clsVideo_level->AdjustInstanceAndConnector);

				bncs_string alt_id;
				int dev = -1;
				int slot = -1;
				int offset = -1;
				char c;
				bool bInstanceFound = instanceLookup(clsVideo_level->AdjustInstanceAndConnector, "", alt_id, &dev, &offset, &slot, &c);
				if (!bInstanceFound)
				{
					if (clsVideo_level->AdjustInstanceAndConnector.contains('#'))
					{
						clsVideo_level->AdjustInstanceAndConnector.split('#', clsVideo_level->AdjustInstance, clsVideo_level->AdjustConnector);
						bInstanceFound = instanceLookup(clsVideo_level->AdjustInstance, "", alt_id, &dev, &offset, &slot, &c);
					}
				}
				else
				{
					clsVideo_level->AdjustInstance = clsVideo_level->AdjustInstanceAndConnector;
				}

				// we now have several scenarios:
				//// Composite, no type handler, with children
				// 
				// 1. This is a composite instance with a typehandler and at least one child has a typehandler
				// in which case expand popup panel with requisite number of adjust buttons
				// 2. This is a composite instance with a typehandler and no children have typehandlers
				// 3. This is a non-composite instance with typehandler
				// 4. This is a non-composite instance without a typehandler

				// in all cases

				bncs_config instanceLookup = bncs_config(bncs_string("instances.%1").arg(clsVideo_level->AdjustInstance));

				m_strAdjustName = alt_id;

				if (instanceLookup.isValid())
				{


					if (instanceLookup.attr("composite") == "yes")
					{
						debug("adjust_multi::ProcessIndexChange instance is composite - Multi levels present:%1 ", alt_id);
						//Find child instances

						
						while (instanceLookup.isChildValid())
						{
							const bncs_string sGroup = instanceLookup.childAttr("id");
							const bncs_string sValue = instanceLookup.childAttr("instance");


							// Check the class to see if we have the targetAdjustInstance yet !
							// create a new entry holding the adjust target Child data
							auto * clsAdjust = new cAdjust_target;
							bncs_string sTargetedInstance = clsAdjust->initTargetParameters(sGroup, sValue, clsVideo_level->AdjustConnector);


							if (sTargetedInstance.length() > 0)
							{
								clsVideo_level->AdjustTargetedInstance = sTargetedInstance;
								clsVideo_level->AdjustTargetInstanceIsValid = true;
								bALevelIsValid = true;
								m_mapRouterVideoLevel.insert(pair< int, cVideo_Level *>(i, clsVideo_level));
								break;
							}
							else
							{
								delete clsAdjust;
							}


							instanceLookup.nextChild();
						}
					}
					else
					{
						debug("adjust_multi::ProcessIndexChange instance is NOT composite - Single level present:%1 ", alt_id);

						const bncs_string sGroup = "";
						const bncs_string sInstance = instanceLookup.attr("id");

						// Check the class to see if we have the targetAdjustInstance yet !
						// create a new entry holding the adjust target Child data
						auto * clsAdjust = new cAdjust_target;
						bncs_string sTargetedInstance = clsAdjust->initTargetParameters(sGroup, sInstance, clsVideo_level->AdjustConnector);

						if (sTargetedInstance.length() > 0)
						{
							clsVideo_level->AdjustTargetedInstance = sTargetedInstance;
							clsVideo_level->AdjustTargetInstanceIsValid = true;
							bALevelIsValid = true;
							m_mapRouterVideoLevel.insert(pair< int, cVideo_Level *>(i, clsVideo_level));
							break;
						}
						else
						{
							delete clsAdjust;
						}
					}
				}
			}
		}
		
		if (bALevelIsValid)
		{
			textPut("pixmap", "", PANEL_MAIN, BTN_BUTTON);
			controlEnable(PANEL_MAIN, BTN_BUTTON);
		}
		else
		{
			textPut("pixmap", "", PANEL_MAIN, BTN_BUTTON);
			controlDisable(PANEL_MAIN, BTN_BUTTON);
		}

	}
	else
	{
		textPut("pixmap", "", PANEL_MAIN, BTN_BUTTON);
		controlDisable(PANEL_MAIN, BTN_BUTTON);
	}
}

bool adjust_multi::isComposite(bncs_string sEntry)
{
	bncs_config c(bncs_string("%1.%2").arg("instances").arg(sEntry));       // 

	if (c.isValid())
	{
		const bncs_string tag = c.attr("composite");

		if (tag.lower() == "yes")
		{
			return true;
		}
	}

	return false;
}

