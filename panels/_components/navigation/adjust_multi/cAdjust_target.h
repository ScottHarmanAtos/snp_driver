#pragma once
#include <bncs_stringlist.h>

class cAdjust_target
{
public:
	
	cAdjust_target();
	virtual ~cAdjust_target();

	bncs_string initTargetParameters(bncs_string sId, bncs_string sInstance, bncs_string sConnector);
	bncs_string Group();
	
private:

	bncs_string _ChildGroup;
	bncs_string _ChildInstance;

	bncs_string  initChildren(bncs_string sInstance, bncs_string sConnector);

};

