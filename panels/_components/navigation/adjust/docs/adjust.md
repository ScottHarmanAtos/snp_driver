# adjust Component

This is a simple control to take router name or index, determine if it's an "adjustable" thing**.

You can supply either a name directly or three parameters index, instance and database to look up a router name.
UI is just a single button that is enabled/disabled appropriately depending upon whether the name used is adjustable or not.
An adjustment name derived from BNCS databases when "index" parameter is supplied is split on # character so that the same instance can appear multiple times with context information appended.

![as_used_on_panel](as_used_on_panel.png)

![config](config.png)

![config2](config2.png)

## Notifications
None

## Stylesheets
None


e.g.
dev_xxx.db7
0383=26/PROC001#1
0383=26/PROC001#2

both are treated as instance 26/PROC001
This control is optimised for use with connections.
"adjustable thing" means the name exists in "instances.xml" and there is a type handler ("typehandlers.xml") for that type of device.

## Commands
| Name | Use |
|------|------|
| instance | Instance of the device to take router names from |
| index | The destination index - this is used to put the router dest name in the pre-select label |
| database | The source index - this is used to put the router source name in the pre-select label |
| name | The instance of the device to get lock information from |

## Notifications</h2>
None

## Stylesheets</h2>
None used
