#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_port.h"

#define PNL_MAIN	"comms_port"

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(comms_port)

// constructor - equivalent to ApplCore STARTUP
comms_port::comms_port( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	// show a panel from file source_port.bncs_ui and we'll know it as our panel PNL_MAIN
	//panelShow( PNL_MAIN, "source_port.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
	timerStart(TIMER_SETUP, 10);
	m_initalAlignment = "centre";
	m_only_take_if_selected = false;
	m_showTally = false;
	m_selected = false;
	m_4wire_reverse_set_recieved = false;
	m_4wire_setting = false;
	m_4wire_source_set_recieved = false;

	m_blnConf = false;
	m_blnIFB = false;
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_port::~comms_port()
{
}

// all button pushes and notifications come here
void comms_port::buttonCallback( buttonNotify *b )
{
	b->dump("comms_port::buttonCallback");
	if (b->value() == "released")
	{
		selected("");
		hostNotify("button=released");
	}

}

// all revertives come here
int comms_port::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_dest_port.device && r->index() == m_dest_port.slot_index)
	{
		bncs_string ringport = bncs_stringlist(r->sInfo(), ',')[0];
		if (m_4wire_setting)
		{
			if (ringport == m_sourceport_output.port.ring_port)
			{
				m_4wire_source_set_recieved = true;
				fourWire_rms();
			}
		}
		if (ringport.length() > 0)
		{
			m_sourceport = rh->GetPort(r->sInfo());
			if (m_sourceport.valid == true)
			{
				textPut("text", m_sourceport.sourceName_short.replace('|', ' '), PNL_MAIN, "tally");
			}
			else
			{
				textPut("text", "Error", PNL_MAIN, "tally");
			}
		}
		else
		{
			textPut("text", "", PNL_MAIN, "tally");
		}
	}
	else if (r->device() == m_sourceport_output.device && r->index() == m_sourceport_output.slot_index)
	{
		bncs_string ringport = bncs_stringlist(r->sInfo(), ',')[0];
		if (m_4wire_setting)
		{
			if (ringport == m_dest_port.port.ring_port)
			{
				m_4wire_reverse_set_recieved = true;
				fourWire_rms();
			}
		}
	}
	else if (r->device() == m_port.devices.Pti && r->index() == riedelHelpers::offset::LISTEN_PTI + m_port.slot_index)
	{
		bncs_stringlist sltOutputPTI(r->sInfo(), '|');
		if (sltOutputPTI.count() > 2)
		{
			bool blnConf = false;
			bool blnIfb = false;

			//Tally only contains conf listen member
			if (sltOutputPTI[0] != "0")
			{
				blnConf = true;
			}

			if (sltOutputPTI[1] != "0")
			{
				blnIfb = true;
			}

			DisplayConfIfb(blnConf, blnIfb);

		}
	}
	else if (r->device() == m_port.devices.Lock && r->index() == m_port.slot_index)
	{
		bool portIsLocked = r->sInfo() == "1" || (r->sInfo() != "0" && r->sInfo().length() > 0);
		if (portIsLocked)
		{
			textPut("pixmap.topRight", "\\images\\icon_lock_small_vertical.png", PNL_MAIN, "button");
		}
		else
		{
			textPut("pixmap.topRight", "", PNL_MAIN, "button");
		}
	}
	return 0;
}

// all database name changes come back here
void comms_port::databaseCallback( revertiveNotify * r )
{
	//Get information about the port take Type, if its 4wire stick an ugly picture on it
	if (r->device() == m_port.devices.Pti && r->database() == riedelHelpers::databases::Take_Type && r->index() == m_port.slot_index)
	{
		takeTypeChange(r->sInfo());
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_port::parentCallback( parentNotify *p )
{

	if (p->command() == "return")
	{
		bncs_stringlist sl;

		sl << bncs_string("ring_port=%1").arg(m_ring_port);
		sl << bncs_string("view_port_type=%1").arg(getPortType::view_port_type(m_view_port_type));
		sl << bncs_string("show_tally=%1").arg(m_showTally?"true":"false");
		sl << bncs_string("take_only_if_selected=%1").arg(m_only_take_if_selected?"true":"false");

		return sl.toString('\n');
	}
	else if (p->command() == "ring_port")
	{
		m_selected = false;
		m_ring_port = p->value();
		load();
	}
	else if (p->command() == "view_port_type")
	{
		m_view_port_type = getPortType::view_port_type(p->value());
		debug("comms_port::parentCallback m_view_port_type:%1", m_view_port_type);
		load();
	}
	else if (p->command() == "show_tally")
	{
		m_showTally = p->value().lower() == "true";
		load();
	}

	else if (p->command() == "selected")
	{
		selected(p->value());
	}

	else if (p->command() == "deselected")
	{
		deselected();
	}

	else if (p->command() == "take")
	{
		debug("comms_port::parentCallback take:%1", p->value());
		if (m_only_take_if_selected == false || (m_only_take_if_selected == true && m_selected))
		{
			//Can only take a dest
			if (m_dest_port.valid)
			{
				//Check we have been sent something sensible
				if (p->value().lower() == "park")
				{
					m_4wire_setting = false;
					comms_ringmaster_port port = comms_ringmaster_port();
					m_undo = m_sourceport;
					if (m_dest_port.port.portType == ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_4WIRE)
					{
						if (m_take_port_numb == "4")
						{
							m_undo_source = m_source_tally_reverse;
							//On 4 Wire Park both sides
							comms_ringmaster_output port_park = rh->GetOutputPort(m_take_port_id);
							if (port_park.valid)
							{
								infoWrite(port_park.device, "0", port_park.slot_index);
								routerModify(port_park.port.devices.Pti, riedelHelpers::databases::Take_Type, port_park.port.slot_index, "0.0", false);
							}
						}
						routerModify(m_dest_port.port.devices.Pti, riedelHelpers::databases::Take_Type, m_dest_port.port.slot_index, "0.0", false);
					}
					infoWrite(m_dest_port.device, "0", m_dest_port.slot_index);
				}
				else
				{
					if (p->subs() == 0)
					{
						m_4wire_setting = false;
						debug("comms_port::parentCallback Normal Take");
						comms_ringmaster_port port = rh->GetPort(p->value());

						if (port.valid)
						{
							m_undo = m_sourceport;
							if (m_dest_port.port.portType == ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_4WIRE)
							{
								routerModify(m_dest_port.port.devices.Pti, riedelHelpers::databases::Take_Type, m_dest_port.port.slot_index, bncs_string("%1|2").arg(port.ring_port_leading_zero), false);
							}
							infoWrite(m_dest_port.device, port.ring_port_leading_zero, m_dest_port.slot_index);
						}
						else
						{
							debug("comms_port::parentCallback Not taking as '%1' is not a vaild port", p->value());
						}
					}
					else
					{
						debug("comms_port::parentCallback Special Take");
						//Special Take!
						if (p->sub(0) == "4Wire")
						{
							//Get the Source tally
							bncs_string source_ring = p->sub(1);
							bncs_string source_port = p->sub(2);

							m_undo = m_sourceport;
							m_source_tally_reverse = rh->GetPort(bncs_string("%1.%2").arg(source_ring).arg(source_port));
							m_undo_source = m_source_tally_reverse;
							//Do a 4Wire Take
							//First make sure it is allowed

							//Is the source port valid
							m_sourceport_output = rh->GetOutputPort(p->value());
							if (m_sourceport_output.valid == true)
							{
								m_4wire_source_set_recieved = false;
								m_4wire_reverse_set_recieved = false;
								m_4wire_setting = true;

								//2 Wire Route
								infoWrite(m_dest_port.device, m_sourceport_output.port.ring_port_leading_zero, m_dest_port.slot_index);
								//2 Wire Route
								infoRegister(m_sourceport_output.device, m_dest_port.port.ring_port_leading_zero, m_sourceport_output.slot_index, true);
								infoWrite(m_sourceport_output.device, m_dest_port.port.ring_port_leading_zero, m_sourceport_output.slot_index);

								//Check if some if it is already routed
								if (m_sourceport == m_sourceport_output.port)
								{
									m_4wire_source_set_recieved = true;
								}
								if (m_source_tally_reverse == m_dest_port.port)
								{
									m_4wire_reverse_set_recieved = true;
								}
								fourWire_rms();
							}
							else
							{
								debug("comms_port::parentCallback Not taking as '%1' is not a vaild port", p->value());
							}
						}
					}
				}
			}
		}
	}
	else if (p->command() == "undo")
	{
		if (m_only_take_if_selected == false || (m_only_take_if_selected == true && m_selected))
		{
			if (m_take_port_numb == "4")
			{
				if (m_sourceport_output.valid)
				{
					comms_ringmaster_port nextUndo = m_source_tally_reverse;
					infoWrite(m_sourceport_output.device, m_undo_source.ring_port_leading_zero, m_sourceport_output.slot_index);
					m_undo_source = nextUndo; //Undo is now the thing before the undo
					routerModify(m_dest_port.port.devices.Pti, riedelHelpers::databases::Take_Type, m_dest_port.port.slot_index, "0.0", false);
					routerModify(m_sourceport_output.port.devices.Pti, riedelHelpers::databases::Take_Type, m_sourceport_output.port.slot_index, "0.0", false);
				}
			}
			if (m_dest_port.valid && m_undo.valid)
			{
				comms_ringmaster_port nextUndo = m_sourceport;
				infoWrite(m_dest_port.device, m_undo.ring_port_leading_zero, m_dest_port.slot_index);
				m_undo = nextUndo; //Undo is now the thing before the undo
			}
		}
	}
	else if (p->command() == "take_only_if_selected")
	{
		m_only_take_if_selected = p->value() == "true" ? true : false;
	}
	else if (p->command() == "_commands")
	{
		bncs_stringlist sl;

		sl << bncs_string("selected");
		sl << bncs_string("deselected");

		return sl.toString('\n');
	}

	else if (p->command() == "_events")
	{
		bncs_stringlist sl;

		sl << bncs_string("selected");
		sl << bncs_string("index=<index>");
		sl << bncs_string("button=released");

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void comms_port::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		load();
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void comms_port::load()
{
	m_blnConf = false;
	m_blnIFB = false;
	m_text = "";
	m_take_port_type = "";
	m_4wire_source_set_recieved = false;
	m_4wire_reverse_set_recieved = false;
	m_4wire_setting = false;

	timerStop(TIMER_SETUP);
	//Show panel
	panelDestroy(PNL_MAIN);
	if (m_view_port_type != VIEW_PORT_TYPE_OUTPUT)
	{
		m_showTally = false;
		panelShow(PNL_MAIN, "source_port.bncs_ui");
	}
	else
	{
		if (m_showTally)
			panelShow(PNL_MAIN, "dest_port.bncs_ui");
		else
			panelShow(PNL_MAIN, "source_port.bncs_ui");
	}

	if (m_ring_port.length() == 0)
	{
		//This also stops the controlDisable below, useful for when its used for mapping
		infoUnregister(m_dest_port.device);
		return;
	}

	bncs_string ring, port;
	m_ring_port.split('.', ring, port);

	if (m_view_port_type != ENUM_PORT_VIEWS::VIEW_PORT_TYPE_OUTPUT)
	{
		m_port = rh->GetPort(ring, port);
		infoRegister(m_port.devices.Pti, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, false);
		infoPoll(m_port.devices.Pti, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index);
	}
	else
	{
		m_dest_port = rh->GetOutputPort(ring, port);
		m_port = m_dest_port.port;
		if (m_showTally == true)
		{
			if (m_dest_port.valid == true)
			{
				//Register for revertives
				infoRegister(m_dest_port.device, m_dest_port.slot_index, m_dest_port.slot_index, false);
				infoPoll(m_dest_port.device, m_dest_port.slot_index, m_dest_port.slot_index);
				// xxx register with ring master 947 for rev for port pti use in ifbs
				infoRegister(m_port.devices.Pti, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, false);
				infoPoll(m_port.devices.Pti, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index);
				infoRegister(rh->GetRingmasterDevices().Ifb_pti, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, false);
				infoPoll(rh->GetRingmasterDevices().Ifb_pti, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index);
				infoRegister(m_port.devices.Lock, m_port.slot_index, m_port.slot_index, false);
				infoPoll(m_port.devices.Lock, m_port.slot_index, m_port.slot_index);
				debug("comms_port::load() m_dest_port.device:%1 m_port.devices.Pti:%2 m_port.devices.Lock:%3", m_dest_port.device, m_port.devices.Pti, m_port.devices.Lock);
				debug("comms_port::load() m_dest_port.slot_index.:%1 riedelHelpers::offset::LISTEN_PTI + m_dest_port.slot_index:%2 m_dest_port.slot_index:%3", m_dest_port.slot_index, riedelHelpers::offset::LISTEN_PTI + m_port.slot_index, m_port.slot_index);
			}
		}
	}

	if (m_view_port_type != ENUM_PORT_VIEWS::VIEW_PORT_MAPPING_INPUT && m_view_port_type != ENUM_PORT_VIEWS::VIEW_PORT_MAPPING_OUTPUT)
	{
		if (m_port.valid == false)
		{
			textPut("text", "Missing|", PNL_MAIN, "button");

			debug("comms_port::load() RingPort:%1 Invalid", m_port.ring_port);
			controlDisable(PNL_MAIN, "button");
			if (m_showTally)
			{
				textPut("text", "", PNL_MAIN, "tally");
				controlDisable(PNL_MAIN, "tally");
			}
			return;
		}
		else //For things that arent mapping and have valid ports
		{
			//For non output types register for the PTI
			//if (m_view_port_type != ENUM_PORT_VIEWS::VIEW_PORT_TYPE_OUTPUT)
			//	infoRegister(m_port.devices.Pti, 4096, 4096, false); //We only want the database callbacks

			//For all non mapping views get the current data from the take type db
			bncs_string takeType;
			routerName(m_port.devices.Pti, riedelHelpers::databases::Take_Type, m_port.slot_index, takeType);
			debug("comms_port::load() TakeType:Dev:%1 TakeType:%2", m_port.slot_index, takeType);
			takeTypeChange(takeType);
		}
	}

	bncs_string text = "";
	bncs_string icon = riedelHelpers::icons::NONE;
	bncs_string icon_tally = riedelHelpers::icons::NONE;
	bncs_string icon_centre = riedelHelpers::icons::NONE;
	bool shirkToFit = false;

	switch (m_view_port_type)
	{
	case ENUM_PORT_VIEWS::VIEW_PORT_TYPE_NONE:
		debug("comms_port::load() VIEW_PORT_TYPE_NONE");
		switch (m_port.portType)
		{
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_SPLIT:
			//if (blnMonitor)
			{
				//text = m_port.sourceName_short;
			}
			//else
			{
				text = bncs_string("%1| |%2").arg(m_port.sourceName_short).arg(m_port.destName_short);
				shirkToFit = true;
				icon_centre = riedelHelpers::icons::_4WIRE_SPLIT;
			}
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_4WIRE:
			if (m_port.sourceName_short == m_port.destName_short)
			{
				text = m_port.sourceName_short;
			}
			else
			{
				text = bncs_string("%1|%2").arg(m_port.sourceName_short).arg(m_port.destName_short);
			}
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL:
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL_DUAL_CHANNEL:
			text = m_port.sourceName_short;
			icon_tally = riedelHelpers::icons::PANEL;
			break;
		}
		break;
	case ENUM_PORT_VIEWS::VIEW_PORT_TYPE_INPUT:
		debug("comms_port::load() VIEW_PORT_TYPE_INPUT");
		switch (m_port.portType)
		{
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_SPLIT:
			text = m_port.sourceName_short;
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_4WIRE:
			text = m_port.sourceName_short;
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL:
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL_DUAL_CHANNEL:
			text = m_port.sourceName_short;
			icon_tally = riedelHelpers::icons::PANEL;
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_CODEC:
			text = m_port.sourceName_short;
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_POOL_PORT:
			text = m_port.sourceName_short;
			break;
		}
		break;
	case ENUM_PORT_VIEWS::VIEW_PORT_TYPE_OUTPUT:
		debug("comms_port::load() VIEW_PORT_TYPE_OUTPUT");
		switch (m_port.portType)
		{
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_SPLIT:
			text = m_port.destName_short;
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_4WIRE:
			text = m_port.destName_short;
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL:
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL_DUAL_CHANNEL:
			text = m_port.sourceName_short;
			icon_tally = riedelHelpers::icons::PANEL;
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_CODEC:
			text = m_port.sourceName_short;
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_POOL_PORT:
			text = m_port.sourceName_short;
			break;
		}
		break;
	case ENUM_PORT_VIEWS::VIEW_PORT_TYPE_SPLIT:
		debug("comms_port::load() VIEW_PORT_TYPE_SPLIT");
		switch (m_port.portType)
		{
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_SPLIT:
			//if (blnMonitor)
			{
			//	text = m_port.sourceName_short;
			}
			//else
			{
				text = bncs_string("%1| |%2").arg(m_port.sourceName_short.replace('|', ' ')).arg(m_port.destName_short.replace('|', ' '));
				shirkToFit = true;
				icon_centre = riedelHelpers::icons::_4WIRE_SPLIT;
			}
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_4WIRE:
			if (m_port.sourceName_short == m_port.destName_short)
			{
				text = m_port.sourceName_short;
			}
			else
			{
				text = bncs_string("%1|%2").arg(m_port.sourceName_short).arg(m_port.destName_short);
			}
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL:
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL_DUAL_CHANNEL:
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_INPUT:
			text = m_port.sourceName_short;
			controlDisable(PNL_MAIN, "button");
			break;
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_OUTPUT:
			text = m_port.destName_short;
			controlDisable(PNL_MAIN, "button");
			break;

		}
		break;
	case ENUM_PORT_VIEWS::VIEW_PORT_TYPE_PANEL:
		debug("comms_port::load() VIEW_PORT_TYPE_PANEL");
		switch (m_port.portType)
		{
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL:
		case ENUM_RIEDEL_PORT_TYPES::PORT_TYPE_PANEL_DUAL_CHANNEL:
			text = m_port.sourceName_short;
			icon_tally = riedelHelpers::icons::PANEL;
			break;
		}
		break;
	case ENUM_PORT_VIEWS::VIEW_PORT_MAPPING_INPUT:
		debug("comms_port::load() VIEW_PORT_MAPPING_INPUT");
		text = m_port.sourceName_short;
		break;
	case ENUM_PORT_VIEWS::VIEW_PORT_MAPPING_OUTPUT:
		debug("comms_port::load() VIEW_PORT_MAPPING_OUTPUT");
		text = m_port.destName_short;
		break;
	}

	m_text = text;

	if(shirkToFit)
		textPut("sizepolicy","shrinktofit", PNL_MAIN, "button");

	textPut("text", m_text, PNL_MAIN, "button");
	if (icon.length() > 0)
		textPut("pixmap", icon, PNL_MAIN, "button");

	if (icon_centre.length() > 0)
		textPut("pixmap.centre", icon_centre, PNL_MAIN, "button");

	if (icon_tally.length() > 0)
	{
		if (m_initalAlignment != "top")
		{
			m_initalAlignment = "top";
			textPut("textalign", m_initalAlignment, PNL_MAIN, "button");
		}
		textPut("pixmap.bottomRight", icon_tally, PNL_MAIN, "button"); //Used to be shown on tallies but looks silly when there are revertives.
	}
	else
	{
		if (m_initalAlignment != "centre")
		{
			m_initalAlignment = "centre";
			textPut("textalign", m_initalAlignment, PNL_MAIN, "button");
		}
	}
}

void comms_port::selected(bncs_string selected)
{
	if (selected.length() == 0 || selected == m_ring_port)
	{
		if (m_selected == false)
		{
			m_selected = true;
			if (m_view_port_type != ENUM_PORT_VIEWS::VIEW_PORT_TYPE_OUTPUT)
			{
				textPut("statesheet", "source_selected", PNL_MAIN, "button");
			}
			else
			{
				textPut("statesheet", "dest_selected", PNL_MAIN, "button");
			}
			hostNotify("selected");
			hostNotify(bncs_string("index=%1").arg(m_ring_port));
		}
	}
	else
	{
		deselected();
	}
}

void comms_port::deselected()
{
	if (m_selected == true)
	{
		m_selected = false;
		if (m_view_port_type != ENUM_PORT_VIEWS::VIEW_PORT_TYPE_OUTPUT)
		{
			textPut("statesheet", "source_deselected", PNL_MAIN, "button");
		}
		else
		{
			textPut("statesheet", "dest_deselected", PNL_MAIN, "button");
		}
	}
}

void comms_port::takeTypeChange(bncs_string type)
{
	if (type != m_take_port_type)
	{
		m_take_port_type = type;

		bncs_string strSource;
		bncs_string strType;
		if (type.length() > 0)
		{
			type.split('|', strSource, strType);
			m_take_port_id = strSource;
			m_take_port_numb = strType;
		}
		else
		{
			m_take_port_id = "";
		}

		if (strType == "4")
		{
			textPut("pixmap.bottom", riedelHelpers::icons::_4WIRE, PNL_MAIN, "button");
		}
		else
		{
			textPut("pixmap.bottom", "", PNL_MAIN, "button");
		}
	}
}

void comms_port::fourWire_rms()
{
	if (m_4wire_source_set_recieved && m_4wire_reverse_set_recieved)
	{
		routerModify(m_dest_port.port.devices.Pti, riedelHelpers::databases::Take_Type, m_dest_port.port.slot_index, bncs_string("%1|4").arg(m_sourceport_output.port.ring_port_leading_zero), false);
		routerModify(m_sourceport_output.port.devices.Pti, riedelHelpers::databases::Take_Type, m_sourceport_output.port.slot_index, bncs_string("%1|4").arg(m_dest_port.port.ring_port_leading_zero), false);
		m_4wire_source_set_recieved = false;
		m_4wire_reverse_set_recieved = false;
		m_4wire_setting = false;
	}

}

void comms_port::DisplayConfIfb(bool blnConf, bool blnIFB)
{
	bncs_string strTallyText = m_text;
	m_blnConf = blnConf;
	m_blnIFB = blnIFB;

	if (blnConf && blnIFB)
	{
		textPut(riedelHelpers::icons::PIXMAP_CONF, PNL_MAIN, "button");
		textPut(riedelHelpers::icons::PIXMAP_IFB, PNL_MAIN, "button");
	}
	else if (blnConf && !blnIFB)
	{
		textPut(riedelHelpers::icons::PIXMAP_CONF, PNL_MAIN, "button");
		textPut(riedelHelpers::icons::PIXMAP_LOCATION_IFB, "", PNL_MAIN, "button");
	}
	else if (!blnConf && blnIFB)
	{
		textPut(riedelHelpers::icons::PIXMAP_IFB, PNL_MAIN, "button");
		textPut(riedelHelpers::icons::PIXMAP_LOCATION_CONF, "", PNL_MAIN, "button");
	}
	else
	{
		textPut(riedelHelpers::icons::PIXMAP_LOCATION_CONF, "", PNL_MAIN, "button");
		textPut(riedelHelpers::icons::PIXMAP_LOCATION_IFB, "", PNL_MAIN, "button");
	}

	if ((blnConf || blnIFB) && !strTallyText.contains('|'))
	{
		textPut("textalign", "top", PNL_MAIN, "button");
	}
	else
	{
		textPut("textalign", m_initalAlignment, PNL_MAIN, "button");
	}
}