# Comms Port Component

The comms port has been created to work within a ringmaster system.

A comms port represents a single INPUT or OUTPUT.

It has been built to work with connections.

It has been built to work with mapping grids as well.


### Note

An instance does not have to be supplied as the component gets this information from the **riedelHelper.dll**

## Commands

| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| ring_port | The RING.PORT for this button. |
| view_port_type | The View the comm port should used |
| show_tally | Bool, only for OUTPUTs will display a tally below |
| selected | Selects this button |
| deselected | Deselects this button |
| take=park | Parks the port, if it is a 4Wire it will park both |
| take.4Wire.RING.PORT=RING.PORT | The first RING.PORT in the sub is the Reverse Port, second is the forward port  |
| take=RING.PORT | Route RING.PORT to the Port  |
| undo | Undo the last take |
| take_only_if_selected | Bool, if true will only take if the button is selected. |

## View Port Type

If the view is set to VIEW_PORT_TYPE_OUTPUT ( 1 ) it will display the dest_port.bncs_ui unless show_tally is set to false. Then it will show source_port.bncs_ui, which it will show if any other View type is chosen.

If not a mapping view VIEW_PORT_MAPPING_INPUT or VIEW_PORT_MAPPING_OUTPUT the button will be disabled if the RING.PORT is invalid.

What the button will display:

* VIEW_PORT_TYPE_NONE
  - PORT_TYPE_SPLIT - source and dest name, icon _4WIRE_SPLIT
  - PORT_TYPE_4WIRE - source and dest name, if they are the same shows only once
  - PORT_TYPE_PANEL/PORT_TYPE_PANEL_DUAL_CHANNEL - source name, icon PANEL
* VIEW_PORT_TYPE_INPUT
  - PORT_TYPE_SPLIT - source name
  - PORT_TYPE_4WIRE - source name
  - PORT_TYPE_PANEL/PORT_TYPE_PANEL_DUAL_CHANNEL - source name, icon PANEL
  - PORT_TYPE_CODEC - source name
  - PORT_TYPE_POOL_PORT - source name
* VIEW_PORT_TYPE_OUTPUT
  - PORT_TYPE_SPLIT - dest name
  - PORT_TYPE_4WIRE - dest name
  - PORT_TYPE_PANEL/PORT_TYPE_PANEL_DUAL_CHANNEL - source name, icon PANEL
  - PORT_TYPE_CODEC - source name
  - PORT_TYPE_POOL_PORT - source name
* VIEW_PORT_TYPE_SPLIT
  - PORT_TYPE_SPLIT - source nad dest name, icon _4WIRE_SPLIT
  - PORT_TYPE_4WIRE - source and dest name, if they are the same shows only once
  - PORT_TYPE_PANEL/PORT_TYPE_PANEL_DUAL_CHANNEL/PORT_TYPE_INPUT - source name, DISABLED
  - PORT_TYPE_OUTPUT - dest name, DISABLED
* VIEW_PORT_TYPE_PANEL
  - PORT_TYPE_PANEL/PORT_TYPE_PANEL_DUAL_CHANNEL - source name, icons PANEL
* VIEW_PORT_MAPPING_INPUT
  - source name
* VIEW_PORT_MAPPING_OUTPUT
  - dest name

The text will be blank if it does not match one of these states.


## Notifications

| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| button=released | button has been pressed |
| index=RING.PORT | button has been selected RING.PORT info |
| selected | button has been selected |



## Stylesheets
None