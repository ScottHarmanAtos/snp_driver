#ifndef comms_port_INCLUDED
	#define comms_port_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif

class comms_port : public bncs_script_helper
{
public:
	comms_port( bncs_client_callback * parent, const char* path );
	virtual ~comms_port();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper *rh;

	ENUM_PORT_VIEWS m_view_port_type;

	bool m_showTally;

	bncs_string m_ring_port;
	bncs_string m_take_port_type;//Used to see if its a 2 or 4 wire take
	bncs_string m_take_port_id;
	bncs_string m_take_port_numb;
	
	void load();

	comms_ringmaster_port m_port;
	comms_ringmaster_output m_dest_port;
	comms_ringmaster_port m_sourceport;

	comms_ringmaster_output m_sourceport_output;
	comms_ringmaster_port m_source_tally_reverse;
	comms_ringmaster_port m_undo;
	comms_ringmaster_port m_undo_source;

	bncs_string m_text;

	bool m_selected;

	void selected(bncs_string);
	void deselected();
	void takeTypeChange(bncs_string type);

	bool m_4wire_source_set_recieved;
	bool m_4wire_reverse_set_recieved;
	bool m_4wire_setting;

	void fourWire_rms();

	bncs_string m_initalAlignment;

	bool m_only_take_if_selected;

	void DisplayConfIfb(bool conf, bool ifb);

	bool m_blnConf;
	bool m_blnIFB;
};


#endif // comms_port_INCLUDED