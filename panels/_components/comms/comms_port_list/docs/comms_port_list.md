# Comms Port List Component

The comms port list has been created to work within a ringmaster system.

It can be used to display Inputs or Outputs on the ringmaster system and allows 
filtering of items by Ring, Port Type or Port name.

It has been built to work with connections.

It has been built to work with mapping grids as well.


### Note

An instance does not have to be supplied as the component gets this information from the **riedelHelper.dll**

## Commands
| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| SourceOrDest | source or dest, select to display the source(inputs) or dest(outputs) |
| FilterType | Set the filtering by type.<br> Options: all or PortType number <br> It is possible to select multiple in a comma delimited list. |
| FilterRing | Set the filtering by ring.<br> Options: all or ring number <br> It is possible to select multiple in a comma delimited list. |
| selected=next | Select the next item in the list |
| selected=previous |Select the previous item in the list |
| selected=none | Deselect the list item |



## Notifications
| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| index=RING.PORT | The RING.PORT that has been selected |
| index=0 | When nothing has been selected |


## Stylesheets
None