#ifndef comms_port_list_INCLUDED
	#define comms_port_list_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"
#include <functional>
#include <initializer_list>
#include <list>
#include <set>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class Button
{
public:
	int Id;
	bncs_string Name;
	bool Selected;
};

class comms_port_list : public bncs_script_helper
{
public:
	comms_port_list( bncs_client_callback * parent, const char* path );
	virtual ~comms_port_list();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper *rh;

	void load();

	list<Button> m_RiedelRings;
	list<Button> m_RiedelTypes;
	
	set<int> popupButtonSelect(bncs_string id, bncs_string panel, bncs_string popupButtonId, list<Button>& buttons);
	set<int> buttonSelectSetup(bncs_string value, bncs_string popupButtonId, list<Button>&buttons);


	void createPopup(bncs_string Panel, const list<Button>& Buttons);

	typedef std::function<bool(const comms_ringmaster_port&)> FILTER;
	typedef std::function<bool(const comms_ringmaster_port&, const bncs_string&)> FILTER_PARAM;

	void display();

	bool filter_compose(const comms_ringmaster_port& port, std::list<FILTER> list)
	{
		for (auto elem : list)
		{
			if (!elem(port))
				return false;
		}
		return true;
	}

	FILTER filter(std::initializer_list<FILTER> li)
	{
		list<FILTER> fil = li;
		return std::bind(&comms_port_list::filter_compose, this, std::placeholders::_1, fil);
	};

	bool filter_source_names(const comms_ringmaster_port& p, const bncs_string& search);
	bool filter_dest_names(const comms_ringmaster_port& p, const bncs_string& search);

	bool filter_remove_unknown_types(const comms_ringmaster_port &p);
	bool filter_types(const comms_ringmaster_port& p, const set<int>& types);

	bool filter_rings(const comms_ringmaster_port& p, const set<int>& types);

	FILTER_PARAM filter_search;
	//FILTER f_type;
	//FILTER f_remove_unknown_types;
	//FILTER f_standard_filters;
	//FILTER f_search;
	//FILTER f_rings;

	set<int> m_types;
	set<int> m_rings;

	bool standard_filters(const comms_ringmaster_port& p);

	bncs_string m_lastSearch;

	bool m_isSource;
	bncs_string m_filterTypes;
	bncs_string m_filterRings;

	bool m_nonSelected;
	int m_currentRow;
	int m_size;
};


#endif // comms_port_list_INCLUDED