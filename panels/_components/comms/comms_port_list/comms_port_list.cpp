#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_port_list.h"

#define PNL_MAIN		"comms_port_list"
#define PNL_KEYBOARD	"comms_keyboard"
#define PNL_RINGS	    "comms_rings"
#define PNL_TYPES	    "comms_type"


#define TIMER_SETUP	1
#define TIMER_SETUP_DURATION 1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_port_list )

// constructor - equivalent to ApplCore STARTUP
comms_port_list::comms_port_list( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	// show a panel from file comms_port_list.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "comms_port_list.bncs_ui" );

	m_isSource = true;

	//f_standard_filters = std::bind(&comms_port_list::standard_filters, this, std::placeholders::_1);
	//f_remove_unknown_types = std::bind(&comms_port_list::filter_remove_unknown_types, this, std::placeholders::_1);
	//f_type = std::bind(&comms_port_list::filter_types, this, std::placeholders::_1, set<int>()); //Pass empty stringlist
	//f_rings = std::bind(&comms_port_list::filter_rings, this, std::placeholders::_1, set<int>());

	m_rings = set<int>();
	m_types = set<int>();

	filter_search = std::bind(&comms_port_list::filter_source_names, this, std::placeholders::_1, std::placeholders::_2);

	textPut("text", "", PNL_MAIN, "lblSearch");

	m_nonSelected = true;
	m_currentRow = 0;

	//Load the riedel rings
	COMMS_RINGS_LOOPKUP rings = rh->GetRings();

	for (COMMS_RINGS_LOOPKUP::const_iterator it = rings.begin(); it != rings.end(); it++)
	{
		Button b;
		b.Id = it->second.ring_id;
		b.Name = bncs_string("%1 - %2").arg(it->second.ring_id).arg(it->second.label);
		b.Selected = true;
		m_RiedelRings.push_back(b);
	}

	list<int> validPort = getPortType::ValidPorts();
	for (list<int>::const_iterator it = validPort.begin(); it != validPort.end(); it++)
	{
		debug("comms_port_list::comms_port_list ID:%1", (*it));
		Button b;
		b.Id = (*it);
		b.Name = bncs_string("%1 - %2").arg(b.Id).arg(getPortType::Name_Type((ENUM_RIEDEL_PORT_TYPES)b.Id));
		b.Selected = true;
		m_RiedelTypes.push_back(b);
	}


}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_port_list::~comms_port_list()
{
}

// all button pushes and notifications come here
void comms_port_list::buttonCallback( buttonNotify *b )
{
	if (PNL_MAIN == b->panel())
	{
		if (b->id() == "btnSearch")
		{
			panelPopup(PNL_KEYBOARD, "keyboard.bncs_ui");
			textPut("setFocus", PNL_KEYBOARD, "keyboard");
			textPut("text", m_lastSearch, PNL_KEYBOARD, "keyboard");
		}
		else if (b->id() == "btnRings")
		{
			//Need to dynamically create the button and panel
			createPopup(PNL_RINGS, m_RiedelRings);
		}
		else if (b->id() == "btnType")
		{
			//Need to dynamically create the button and panel
			createPopup(PNL_TYPES, m_RiedelTypes);
		}
		if (b->id() == "list")
		{
			b->dump("comms_port_list::buttonCallback()");
			if (m_nonSelected == true)
			{
				m_nonSelected = false;
				textPut("colour.background=#bfbfbf", PNL_MAIN, "selectNone"); //decolour Select None button
			}
			m_currentRow = b->sub(0);
			hostNotify(bncs_string("index=%1").arg(b->value()));
		}
		else if (b->id() == "selectNone")
		{
			if (m_nonSelected == false)
			{
				m_nonSelected = true;
				textPut("colour.background=#1bff13", PNL_MAIN, "selectNone"); //colour Select None button
				textPut("clearSelection", PNL_MAIN, "list");
			}
			m_currentRow = 0;
			hostNotify("index=0");
		}
	}
	else if (PNL_KEYBOARD == b->panel())
	{
		bncs_string search = m_lastSearch;
		if (b->id() == "keyboard" )//&& b->sub(0) == "return")
		{
			textGet("text", PNL_KEYBOARD, "keyboard", search);

			m_lastSearch = search;

			if (b->sub(0) == "return")
			{
				textPut("text", m_lastSearch, PNL_MAIN, "search_criteria");
				panelDestroy(PNL_KEYBOARD);
			}
		}
		else if (b->id() == "Close")
		{
			panelDestroy(PNL_KEYBOARD);
		}
		else if (b->id() == "Cancel")
		{
			m_lastSearch = "";
			textPut("text", "", PNL_MAIN, "search_criteria");
			panelDestroy(PNL_KEYBOARD);
		}

		//f_search = std::bind(filter_search, std::placeholders::_1, m_lastSearch);

		if (m_lastSearch.length() > 0)
		{
			textPut("colour.background=#ffaa00", PNL_MAIN, "btnSearch");
			textPut("text", m_lastSearch, PNL_MAIN, "lblSearch");
		}
		else
		{
			textPut("colour.background=#bfbfbf", PNL_MAIN, "btnSearch");
			textPut("text", "", PNL_MAIN, "lblSearch");
		}

		load();
	}
	else if (PNL_TYPES == b->panel())
	{
		m_types = popupButtonSelect(b->id(), b->panel(), "btnType", m_RiedelTypes);
		//f_type = std::bind(&comms_port_list::filter_types, this, std::placeholders::_1, sl);

		load();
	}

	else if (PNL_RINGS == b->panel())
	{
		m_rings = popupButtonSelect(b->id(), b->panel(), "btnRings", m_RiedelRings);
		//f_rings = std::bind(&comms_port_list::filter_rings, this, std::placeholders::_1, sl);

		load();
	}

}

// all revertives come here
int comms_port_list::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void comms_port_list::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_port_list::parentCallback( parentNotify *p )
{
	if (p->command() == "SourceOrDest")
	{
		m_isSource = p->value() == "dest" ? false : true;

		if (m_isSource)
			filter_search = std::bind(&comms_port_list::filter_source_names, this, std::placeholders::_1, std::placeholders::_2);
		else
			filter_search = std::bind(&comms_port_list::filter_dest_names, this, std::placeholders::_1, std::placeholders::_2);

		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}

	else if (p->command() == "FilterType")
	{
		set<int> sl = buttonSelectSetup(p->value(), "btnType", m_RiedelTypes);

		bncs_string s;
		for (set<int>::iterator it = sl.begin(); it != sl.end();)
		{
			s += (*it);
			it++;
			if (it != sl.end())
			{
				s += ",";
			}
		}
		m_filterTypes = s;
		m_types = sl;
		//f_type = std::bind(&comms_port_list::filter_types, this, std::placeholders::_1, sl);

		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}

	else if (p->command() == "FilterRing")
	{
		set<int> sl = buttonSelectSetup(p->value(), "btnRings", m_RiedelRings);

		bncs_string s;
		for (set<int>::iterator it = sl.begin(); it != sl.end();)
		{
			s += (*it);
			it++;
			if (it != sl.end())
			{
				s += ",";
			}
		}
		m_filterRings = s;
		m_rings = sl;
		//f_rings = std::bind(&comms_port_list::filter_rings, this, std::placeholders::_1, sl);

		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}

	else if (p->command() == "selected")
	{
		debug("comms_port_list Select Next: command:%1 value:%2", p->command(), p->value());
		// this is a small workaround because listbox treats "next" as first in the list if nothing is selected
		bncs_string ret;
		textGet("selectedindex", PNL_MAIN, "list", ret);

		debug("comms_port_list Select Next : ret:%1", ret);

		if ((int)ret > -1)
		{
			if (p->value() == "next")
			{
				//If we go over the size caryy on selecting the last item in the list
				if (m_currentRow < (m_size - 1))
				{
					m_currentRow++;
				}
				else
				{
					textPut("clearSelection", PNL_MAIN, "list");
				}
				textPut(bncs_string("selectRow=%1").arg(m_currentRow), PNL_MAIN, "list");
			}
			else if (p->value() == "previous")
			{
				m_currentRow--;
				textPut(bncs_string("selectRow=%1").arg(m_currentRow), PNL_MAIN, "list");
			}
			else if (p->value() == "none")
			{
				m_nonSelected = true;
				textPut("colour.background=#1bff13", PNL_MAIN, "selectNone"); //colour Select None button
				textPut("clearSelection", PNL_MAIN, "list");
				hostNotify("index=0");
			}
		}
		else
		{
			hostNotify("index=0");
		}
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;

		sl << "selected=next";
		sl << "selected=previous";
		sl << "selected=none";
		sl << "index=[value]";

		return sl.toString('\n');
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if (p->command() == "_events")
	{
		bncs_stringlist sl;

		sl << "index=<router index>";

		return sl.toString('\n');
	}
	else if (p->command() == "return")
	{
		bncs_stringlist sl;

		sl << bncs_string("SourceOrDest=%1").arg(m_isSource==true?"source":"dest");

		sl << bncs_string("FilterType=%1").arg((m_filterTypes.contains(',', false) + 1) == (int)m_RiedelTypes.size() ? "all" : m_filterTypes);

		sl << bncs_string("FilterRing=%1").arg((m_filterRings.contains(',',false) + 1) == (int)m_RiedelRings.size() ? "all": m_filterRings);

		return sl.toString('\n');
	}
	return "";
}

// timer events come here
void comms_port_list::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		load();
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

bool comms_port_list::standard_filters(const comms_ringmaster_port& p)
{
	if (!filter_rings(p, m_rings))
		return false;
	if (!filter_remove_unknown_types(p))
		return false;
	if (!filter_types(p, m_types))
		return false;
	if (m_lastSearch.length() > 0)
	{
		if (!filter_search(p, m_lastSearch))
			return false;
	}

	return true;
}

void comms_port_list::load()
{
	display();
}

bool comms_port_list::filter_remove_unknown_types(const comms_ringmaster_port& p)
{
	if (getPortType::IsValid(p.portType))
		return true;
	return false;
}

bool comms_port_list::filter_source_names(const comms_ringmaster_port& p, const bncs_string& search)
{
	//debug("comms_port_list::filter_source_names search:%1 name:%2",search, p.sourceName_long);
	if (p.sourceName_long.find(search, 0, false) > -1)
		return true;
	return false;
}

bool comms_port_list::filter_dest_names(const comms_ringmaster_port& p, const bncs_string& search)
{
	if (p.destName_long.find(search, 0, false) > -1)
		return true;
	return false;
}

bool comms_port_list::filter_types(const comms_ringmaster_port& p, const set<int>& types)
{
	if (types.size() == 0)
		return false;
	if (types.find(p.portType) != types.end())
	{
		return true;
	}
	return false;
}

bool comms_port_list::filter_rings(const comms_ringmaster_port& p, const set<int>& types)
{
	if (types.size() == 0)
		return false;
	if (types.find(p.ring_id) != types.end())
	{
		return true;
	}
	return false;
}

void comms_port_list::display()
{
	m_size = 0;
	textPut("enableSorting", "false", PNL_MAIN, "list");
	if (m_isSource)
	{
		COMMS_RINGMASTER_PORT_LOOKUP p = rh->SourcePorts();

		debug("comms_port_list:: Show Loaded Sources");
		for (COMMS_RINGMASTER_PORT_LOOKUP::const_iterator it = p.begin(); p.end() != it; it++)
		{
			debug("comms_port_list:: Ring:%1 Name:%2", it->second.ring_port, it->second.sourceName_short);
		}

		textPut("clear", PNL_MAIN, "list");

		for (COMMS_RINGMASTER_PORT_LOOKUP::const_iterator it = p.begin(); p.end() != it; it++)
		{
			if (standard_filters(it->second))
			{
				bncs_string s = "%1;%2;%3-%4;#TAG=%5";
				s.replace("%1", it->second.ring_port_leading_zero);
				s.replace("%2", it->second.sourceName_long);
				s.replace("%3", it->second.portType);
				s.replace("%4", getPortType::Name_Type(it->second.portType));
				s.replace("%5", it->second.ring_port);
				
				textPut("add", s, PNL_MAIN, "list");
				//textPut("add", s, PNL_MAIN, "list");
				m_size++;
			}
			
		}
	}
	else
	{
		COMMS_RINGMASTER_PORT_LOOKUP p = rh->OutputPorts();

		textPut("clear", PNL_MAIN, "list");

		for (COMMS_RINGMASTER_PORT_LOOKUP::const_iterator it = p.begin(); p.end() != it; it++)
		{
			if (standard_filters(it->second))
			{
				bncs_string s = "%1;%2;%3-%4;#TAG=%5";
				s.replace("%1", it->second.ring_port_leading_zero);
				s.replace("%2", it->second.destName_long);
				s.replace("%3", it->second.portType);
				s.replace("%4", getPortType::Name_Type(it->second.portType));
				s.replace("%5", it->second.ring_port);

				textPut("add", s, PNL_MAIN, "list");
				//textPut("add", s, PNL_MAIN, "list");
				m_size++;
			}
				
		}
	}
	textPut("enableSorting", "true", PNL_MAIN, "list");
}

void comms_port_list::createPopup(bncs_string Panel, const list<Button>& Buttons)
{
	//Popup Button Size
	int width = 200;
	int height = 40;

	int NumOfButtons = (int)Buttons.size();
	int buttonCount = 0;
	if (NumOfButtons != 0)
	{
		panelPopup(Panel, "", width, height*NumOfButtons);

		for (list<Button>::const_iterator it = Buttons.begin(); it != Buttons.end(); it++)
		{
			controlCreate(Panel, bncs_string("%1").arg(it->Id), "bncs_control", 0, buttonCount * height, width, height, bncs_string("text=%1\nstatesheet=%2\nnotify.released=true").arg(it->Name).arg(it->Selected == true ? "enum_selected" : "enum_deselected"));
			buttonCount++;
		}
	}
}


set<int> comms_port_list::popupButtonSelect(bncs_string id, bncs_string panel, bncs_string popupButtonId, list<Button>& buttons)
{
	set<int> sl;
	for (list<Button>::iterator it = buttons.begin(); it != buttons.end(); it++)
	{
		if (it->Id) {
			if (it->Id == id)
			{
				if (it->Selected)
				{
					it->Selected = false;
					//Deselect
					textPut("statesheet", "enum_deselected", panel, id);

				}
				else
				{
					it->Selected = true;
					//Select
					textPut("statesheet", "enum_selected", panel, id);
				}

			}
			if (it->Selected)
				sl.insert(it->Id);
		}

	} // for
	if (sl.size() != buttons.size())
		textPut("statesheet", "enum_selected", PNL_MAIN, popupButtonId);
	else
		textPut("statesheet", "enum_deselected", PNL_MAIN, popupButtonId);

	return sl;
}

set<int> comms_port_list::buttonSelectSetup(bncs_string value, bncs_string popupButtonId, list<Button>&buttons)
{
	if (value == "all" || value.length() == 0)
	{
		//Set all on if all or nothing
		for (list<Button>::iterator it = buttons.begin(); it != buttons.end(); it++)
		{
			it->Selected = true;
		}
	}
	else
	{
		bncs_stringlist temp = bncs_stringlist(value);

		//Select and unselect depending on values
		for (list<Button>::iterator it = buttons.begin(); it != buttons.end(); it++)
		{
			if (it->Id) {
				if (temp.find(it->Id) == -1)
				{
					it->Selected = false;
				}
				else
				{
					it->Selected = true;
				}
			}
		}
	}

	//Set all the values
	set<int> sl;
	for (list<Button>::iterator it = buttons.begin(); it != buttons.end(); it++)
	{
		if ((it->Id)&&(it->Selected))
			sl.insert( it->Id);
	}

	if (sl.size() != buttons.size())
		textPut("statesheet", "enum_selected", PNL_MAIN, popupButtonId);
	else
		textPut("statesheet", "enum_deselected", PNL_MAIN, popupButtonId);

	return sl;
}