# comms_edit_ifb Component

Comms Edit IFB

This components allows the editing of IFBs, it works with the ringmaster IFB infodrivers.

The component loads comms_ifbs.xml, which ifb has edit permissions, if the ops_area of the workstation is the in edit_permissions
the button will be enabled, if not all buttons will be disabled.

The panel can be used from panelsets, using the **param** attribute to specifiy ring.ifb or it can be navigated to supplying ring.ifb as the argument. 
