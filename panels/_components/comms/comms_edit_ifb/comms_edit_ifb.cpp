#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_edit_ifb.h"
#include "instanceLookup.h"

#define PANEL_MAIN	   1
#define PANEL_KEYBOARD 2
#define PANEL_PARK     3

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_edit_ifb )

/*
NOTE: Because of ringmaster behaviour is it not possible to clear the label. 
Ringmaster remembers it even once the label has been removed from riedel.
There is not good way around this.

*/

// constructor - equivalent to ApplCore STARTUP
comms_edit_ifb::comms_edit_ifb( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_summary = false;
	m_ring = 0;
	m_ifb = 0;
	rh = riedelHelper::getriedelHelperObject();
	m_readonly = true;
	m_selected = selected::none;
	m_keyboardInput = k_none;

	panelDestroy(PANEL_MAIN);
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_edit_ifb::~comms_edit_ifb()
{
}

// all button pushes and notifications come here
void comms_edit_ifb::buttonCallback( buttonNotify *b )
{
	if (b->panel() == PANEL_MAIN)
	{
		if (b->id() == "park_ifb")
		{
			panelPopup(PANEL_PARK, "popup_confirm_park.bncs_ui");
		}
		else if (b->id() == "park_xy")
		{
			routerCrosspoint(m_port_device, 0, m_port_index);
		}
		else if (b->id() == "adjust")
		{
			navigateExecute(bncs_string("_components/comms/comms_edit_ifb,%1").arg(m_ring_port));
		}
		else if (b->id() == "set_defaults")
		{
			bncs_config c(bncs_string("comms_ifbs.%1.%2").arg(m_ring).arg(m_ifb));
			auto inputs = c.attr("input_port");
			auto mixminus = c.attr("mixminus_port");
			auto outputs = c.attr("output_port");
			auto label = c.attr("label");
			auto subtitle = c.attr("subtitle");
			infoWrite(m_ifb_device, bncs_string("%1|%2|%3|%4").arg(inputs).arg(mixminus).arg(outputs).arg(label), m_ifb);

			routerModify(m_ifb_device_database, riedelHelpers::databases::Subtitle_Input, m_ifb, subtitle, false);
		}
		else if (b->id() == "input_port")
		{
			select(input);
		}
		else if (b->id() == "mixminus_port")
		{
			select(mixminus);
		}
		else if (b->id() == "output_port")
		{
			select(output);
		}
		else if (b->id() == "comms_port_grid_in" || b->id() == "comms_port_grid_out")
		{

			if (b->command() == "index")
			{
				//Ignore duplicates indexes being sent
				if (m_lastIndexSelected == b->value()) return;
				m_lastIndexSelected = b->value();

				//Don't call if a non valid value is passed
				bncs_stringlist sl(b->value(), '.');
				if (sl.count() == 2 && sl[1] > 0)
				{
					update(b->value());
				}
			}
		}
		else if (b->id() == "select_none")
		{
			update("");
		}
		else if (b->id() == "label")
		{
			m_keyboardInput = k_label;
			panelPopup(PANEL_KEYBOARD, "popup_keyboard.bncs_ui");
			bncs_string text;
			textGet("text", PANEL_MAIN, "label", text);
			textPut("text",text, PANEL_KEYBOARD, "keyboard");
		}
		else if (b->id() == "subtitle")
		{
			m_keyboardInput = k_subtitle;
			panelPopup(PANEL_KEYBOARD, "popup_keyboard.bncs_ui");
			bncs_string text;
			textGet("text", PANEL_MAIN, "subtitle", text);
			textPut("text", text, PANEL_KEYBOARD, "keyboard");
		}
	}
	else if (b->panel() == PANEL_PARK)
	{
		if (b->id() == "confirm")
		{
			infoWrite(m_ifb_device, "&REMOVE=EVERYTHING", m_ifb);
		}
		routerModify(m_ifb_device_database, riedelHelpers::databases::Subtitle_Input, m_ifb, "", false);
		panelDestroy(PANEL_PARK);
	}
	else if (b->panel() == PANEL_KEYBOARD)
	{
		if (b->id() == "keyboard")
		{
			bncs_string input;
			textGet("text", PANEL_KEYBOARD, "keyboard", input);

			switch (m_keyboardInput)
			{
			case k_label:
				infoWrite(m_ifb_device, bncs_string("&LABEL=%1").arg(input), m_ifb);
				break;
			case k_subtitle:
				routerModify(m_ifb_device_database, riedelHelpers::databases::Subtitle_Input, m_ifb, input, false);
				break;
			}
		}
		panelDestroy(PANEL_KEYBOARD);
	}
}

// all revertives come here
int comms_edit_ifb::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_ifb_device && r->index() == m_ifb)
	{
		m_lastRevertive = r->sInfo();
		bncs_stringlist sl(r->sInfo(), '|');
		bncs_stringlist inputs  (sl[0],',');
		bncs_stringlist mixminus(sl[1],',');
		bncs_stringlist outputs (sl[2],',');
		bncs_string label = sl[3];

		textPut("text", label, PANEL_MAIN, "label");
		textPut("clear", PANEL_MAIN, "input_ports");
		textPut("clear", PANEL_MAIN, "mixminus_ports");
		textPut("clear", PANEL_MAIN, "output_ports");
		for (int i = 0; i < inputs.count(); ++i)
		{
			auto port = rh->GetPort(inputs[i]);
			if(m_summary) textPut("add", port.sourceName_short, PANEL_MAIN, "input_ports");
			else          textPut("add", bncs_string("%1;%2").arg(port.ring_port).arg(port.sourceName_short.replace('|', ' ')), PANEL_MAIN, "input_ports");
		}
		for (int i = 0; i < mixminus.count(); ++i)
		{
			auto port = rh->GetPort(mixminus[i]);
			if (m_summary) textPut("add", port.sourceName_short, PANEL_MAIN, "mixminus_ports");
			else           textPut("add", bncs_string("%1;%2").arg(port.ring_port).arg(port.sourceName_short.replace('|', ' ')), PANEL_MAIN, "mixminus_ports");
		}
		for (int i = 0; i < outputs.count(); ++i)
		{
			auto port = rh->GetOutputPort(outputs[i]);
			if (m_summary) textPut("add", port.port.destName_short, PANEL_MAIN, "output_ports");
			else           textPut("add", bncs_string("%1;%2").arg(port.port.ring_port).arg(port.port.destName_short.replace('|', ' ')), PANEL_MAIN, "output_ports");

			if (i == 0)
			{
				//Get the ringmaster device for the port
				m_port_device = m_portMapping[port.port.ring_id];
				m_port_index  = port.slot_index;
				infoRegister(port.device, port.slot_index, port.slot_index, false);
				infoPoll(port.device, port.slot_index, port.slot_index);
			}
		}
	}
	else if (r->device() == m_port_device && r->index() == m_port_index)
	{	
		r->dump("selected rev");
		bncs_stringlist sl(r->sInfo(), '.');
		if (sl.count() == 2 && sl[1] > 0)
		{
			bncs_string port;
			routerName(m_port_device, riedelHelpers::databases::Input_Port_Button_Name, r->index(), port);
			textPut("text", port, PANEL_MAIN, "tally");
			controlShow(PANEL_MAIN, "park_xy");
		}
		else
		{
			textPut("text", "", PANEL_MAIN, "tally");
			controlHide(PANEL_MAIN, "park_xy");
		}
	}

return 0;
}

// all database name changes come back here
void comms_edit_ifb::databaseCallback(revertiveNotify* r)
{
	if (r->device() == m_ifb_device_database && r->index() == m_ifb)
	{
		if (r->database() == riedelHelpers::databases::Subtitle_Input) textPut("text", r->sInfo(), PANEL_MAIN, "subtitle");
		if (r->database() == riedelHelpers::databases::Input_Long_Name) textPut("text", r->sInfo(), PANEL_MAIN, "defualt_name");
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_edit_ifb::parentCallback( parentNotify *p )
{
	if (p->command() == "summary")
	{
		if (p->value() == "true")
		{
			m_summary = true;
			panelShow(PANEL_MAIN, "summary.bncs_ui");
		}
		else
		{
			m_summary = false;
		}
	}
	else if (p->command() == "ring_ifb")
	{
		m_ring_port = p->value();
		bncs_stringlist sl(p->value(),'.');
		m_ring = sl[0];
		m_ifb = sl[1];
		init();
	}
	else if (p->command() == "return")
	{
		bncs_stringlist sl;
		sl << bncs_string("summary=%1").arg(m_summary ? "true" : "false");
		sl << bncs_string("ring_ifb=%1").arg(m_ring_port);
		return sl.toString('\n');
	}
	else if( p->command() == "raw_parameters" )
	{
		panelShow(PANEL_MAIN, "main.bncs_ui");
		bncs_stringlist sl(p->value(), '.');
		m_ring_port = p->value();
		m_ring = sl[0];
		m_ifb = sl[1];
		init();
	}
	return "";
}

// timer events come here
void comms_edit_ifb::timerCallback( int id )
{
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void comms_edit_ifb::init()
{
	bncs_config c(bncs_string("comms_ifbs.%1.%2").arg(m_ring).arg(m_ifb));

	controlHide(PANEL_MAIN, "park_xy");
	controlHide(PANEL_MAIN, "comms_port_grid_in");
	controlHide(PANEL_MAIN, "comms_port_grid_out");
	controlHide(PANEL_MAIN, "select_none");

	if (c.isValid() == false)
	{
		textPut("text", bncs_string("%1.%2 Not found").arg(m_ring).arg(m_ifb), PANEL_MAIN, "label");
		disable(true);
		return;
	}

	auto ring = rh->GetRing(m_ring);

	ringmaster_devices r = rh->GetRingmasterDevices();

	//Riedel Helper currently does not have info the IFBs on the ringmaster
	bncs_config rs("ring_master_config.RIEDEL_SYSTEMS");

	while (rs.isChildValid())
	{
		//Riedel_1
		bncs_string _, num;
		rs.childAttr("id").split('_', _, num);

		auto id = rs.childAttr("id").toInt();
		//"trunk_address=11,base_instance=c_riedel_london_01,name=LDC_01" 
		bncs_stringlist sl(rs.childAttr("value"));
		auto trunk = sl.getNamedParam("trunk_address");
		if (trunk == ring.ring_id)
		{
			//Ring found
			bncs_string inst;
			instanceLookupComposite((const char*)r.Composite, (const char*)bncs_string("ifbs_%1").arg(num), inst);
			getDev(inst, &m_ifb_device);
		}

		bncs_string portInst;
		int portDev = 0;
		instanceLookupComposite((const char*)r.Composite, (const char*)bncs_string("ports_%1").arg(num), portInst);
		getDev(portInst, &portDev);

		m_portMapping[trunk] = portDev;

		rs.nextChild();
	}

	m_ifb_device_database = ring.devices.Ifb;
	infoRegister(m_ifb_device_database, 4096, 4096); //For database callback

	//Ignore *
	bncs_stringlist sl(c.attr("edit_permissions"));

	auto area = bncs_config(bncs_string("workstation_settings.%1.ops_area").arg(workstation())).attr("value");

	m_readonly = sl.find(area) < 0;

	disable(m_readonly);

	infoRegister(m_ifb_device, m_ifb, m_ifb, false);
	infoPoll(m_ifb_device, m_ifb, m_ifb);

	//Get Subtitle
	bncs_string subtitle;
	routerName(m_ifb_device_database, riedelHelpers::databases::Subtitle_Input, m_ifb, subtitle);
	textPut("text", subtitle, PANEL_MAIN, "subtitle");
	textPut("text", m_ring_port, PANEL_MAIN, "ring_ifb");

	bncs_string defaultName;
	routerName(m_ifb_device_database, riedelHelpers::databases::Input_Long_Name, m_ifb, defaultName);
	textPut("text", defaultName, PANEL_MAIN, "default_name");
	
}

void comms_edit_ifb::disable(bool disable)
{
	if (disable)
	{
		controlDisable(PANEL_MAIN, "adjust");
		controlDisable(PANEL_MAIN, "label");
		controlDisable(PANEL_MAIN, "subtitle");
		controlDisable(PANEL_MAIN, "input_port");
		controlDisable(PANEL_MAIN, "mixminus_port");
		controlDisable(PANEL_MAIN, "output_port");
		controlDisable(PANEL_MAIN, "set_defaults");
		controlDisable(PANEL_MAIN, "park_xy");
		controlDisable(PANEL_MAIN, "park_ifb");
	}
	else
	{
		controlEnable(PANEL_MAIN, "adjust");
		controlEnable(PANEL_MAIN, "label");
		controlEnable(PANEL_MAIN, "subtitle");
		controlEnable(PANEL_MAIN, "input_port");
		controlEnable(PANEL_MAIN, "mixminus_port");
		controlEnable(PANEL_MAIN, "output_port");
		controlEnable(PANEL_MAIN, "set_defaults");
		controlEnable(PANEL_MAIN, "park_xy");
		controlEnable(PANEL_MAIN, "park_ifb");
	}
}

void comms_edit_ifb::select(selected _new)
{
	//Clear this whenever a new item is selected and even if the same one is selected.
	m_lastIndexSelected = "";
	if (m_selected == _new) return;

	bncs_string oldGrid;
	bncs_string newGrid;
	switch (m_selected)
	{
	case none:     controlShow(PANEL_MAIN, "select_none");	 break;
	case input:    oldGrid = "comms_port_grid_in"; textPut("statesheet", "enum_deselected", PANEL_MAIN, "input_port");  break;
	case mixminus: oldGrid = "comms_port_grid_in"; textPut("statesheet", "enum_deselected", PANEL_MAIN, "mixminus_port"); break;
	case output:   oldGrid = "comms_port_grid_out";  textPut("statesheet", "enum_deselected", PANEL_MAIN, "output_port"); break;
	}

	m_selected = _new;
	switch (m_selected)
	{
	case input:    newGrid = "comms_port_grid_in";  textPut("statesheet", "enum_selected", PANEL_MAIN, "input_port");  break;
	case mixminus: newGrid = "comms_port_grid_in";  textPut("statesheet", "enum_selected", PANEL_MAIN, "mixminus_port"); break;
	case output:   newGrid = "comms_port_grid_out"; textPut("statesheet", "enum_selected", PANEL_MAIN, "output_port"); break;
	}

	if (oldGrid == newGrid) return;
	if (oldGrid.length() > 0) controlHide(PANEL_MAIN, oldGrid);
	controlShow(PANEL_MAIN, newGrid);
}

void comms_edit_ifb::update(bncs_string value)
{
	//Assumption, we are always going to replace, this may change in future
	bncs_string command;
	//Special behaviour as ringmaster does not pass empty & commands to the riedel driver.
	if (value.length() == 0)
	{
		value = "0";
	}

	switch (m_selected)
	{
	case input:    command = "&INPUT=%1"; break;
	case mixminus: command = "&MIXMINUS=%1"; break;
	case output:   command = "&OUTPUT=%1"; break;
	}


	infoWrite(m_ifb_device, command.arg(value), m_ifb, true);
}