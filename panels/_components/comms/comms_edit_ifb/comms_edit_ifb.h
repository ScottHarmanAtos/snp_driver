#ifndef comms_edit_ifb_INCLUDED
	#define comms_edit_ifb_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"
#include <vector>
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_edit_ifb : public bncs_script_helper
{
public:
	comms_edit_ifb( bncs_client_callback * parent, const char* path );
	virtual ~comms_edit_ifb();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper *rh;

	bncs_string m_instance;

	enum selected
	{
		input,
		mixminus,
		output,
		label,
		none
	} m_selected;

	enum keyboard
	{
		k_none,
		k_label,
		k_subtitle
	} m_keyboardInput;

	void disable(bool disable);
	void select(selected _new);
	void init();
	void update(bncs_string value);

	bncs_string m_lastRevertive;
	bncs_string m_lastIndexSelected;
	bncs_string m_ring_port;
	bool m_summary = false;
	int m_ring = 0;
	int m_ifb = 0;
	bool m_readonly;
	int m_ifb_device;
	int m_ifb_device_database; //different so it can work with ringmaster and non ringmaster version

	int m_port_device;
	int m_port_index;

	vector<int> m_inputs;
	vector<int> m_mixminus;
	vector<int> m_outputs;

	map<int, int> m_portMapping;
	
};


#endif // comms_edit_ifb_INCLUDED