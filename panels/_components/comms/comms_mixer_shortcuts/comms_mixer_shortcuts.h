#ifndef comms_mixer_shortcuts_INCLUDED
	#define comms_mixer_shortcuts_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif

#include <map>

typedef map<int, int> MAP_INT_INT;
typedef map<int, int>::iterator MAP_INT_INT_ITERATOR;

typedef map<int, map<int, int>> MAP_INT_INT_INT;
typedef map<int, map<int, int>>::iterator MAP_INT_INT_INT_ITERATOR;

typedef map<int, comms_ringmaster_port> MAP_INT_PORT;
typedef map<int, MAP_INT_PORT> MAP_INT_INT_PORT;

class comms_mixer_shortcuts : public bncs_script_helper
{
public:
	comms_mixer_shortcuts( bncs_client_callback * parent, const char* path );
	virtual ~comms_mixer_shortcuts();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper * rh;
	bncs_string m_myParam;
	bncs_string m_instance;
	void init();
	int parseRevertive(bncs_string mixerstring);
	void highlightActivePreset(bncs_string revertive);
	MAP_INT_INT_PORT m_mapDefaultPortList;

};


#endif // comms_mixer_shortcuts_INCLUDED