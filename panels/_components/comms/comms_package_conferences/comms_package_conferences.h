#ifndef comms_package_conferences_INCLUDED
	#define comms_package_conferences_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_package_conferences : public bncs_script_helper
{
public:
	comms_package_conferences( bncs_client_callback * parent, const char* path );
	virtual ~comms_package_conferences();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_page;
	void setupPackages();
};


#endif // comms_package_conferences_INCLUDED