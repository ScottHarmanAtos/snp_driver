#ifndef comms_port_monitor_INCLUDED
	#define comms_port_monitor_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_port_monitor : public bncs_script_helper
{
public:
	comms_port_monitor( bncs_client_callback * parent, const char* path );
	virtual ~comms_port_monitor();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:
	void init();
	void setAMUStyle();
	void setupAMU(bncs_string);
	void updateMonitorOutput(bncs_string strPortAddress);
	void updateMonitorInput(bncs_string strPortAddress);
	void updateMonitorState();

	bool m_bRightAMU;
	bool m_bLeftAMU;
	bool m_blnReceivedInitialMonitorState;
	bool m_bReduced_mode;

	int m_iHydraDevice;
	int m_iAmuDest;
	int m_iAmuSource;

	int m_intMonitorSlot;
	int m_intMonitorPort;
	int m_intDevice;
	int m_intDeviceMonitor;

	bool m_waitForInitialRevertive;

	comms_ringmaster_port m_crapOutput;
	comms_ringmaster_port m_crapInput;
	comms_ringmaster_port  m_crapMonitorPort;  // current port being monitored

	bncs_string m_strInstance;
	bncs_string m_setting;

	riedelHelper * rh;

	bool m_selected;

	enum monitorState
	{
		Off = 0,
		Input,
		Output
	};

	monitorState m_monitorState;
};


#endif // comms_port_monitor_INCLUDED