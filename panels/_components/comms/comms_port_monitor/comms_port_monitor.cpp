#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <bncs_config.h>
#include "comms_port_monitor.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_port_monitor )

#define PANEL_MAIN				"comms_port_monitor"

#define TIMER_INIT      1
#define TIMEOUT_INIT	10

//This should be gotten from somewhere better.
#define RTR_HYDRA "rtr_hydra" 

// constructor - equivalent to ApplCore STARTUP
comms_port_monitor::comms_port_monitor( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();
	m_strInstance = rh->GetRingmasterDevices().Composite;

	m_intDevice = 0;
	m_intDeviceMonitor = 0;
	m_intMonitorPort = 0;
	m_intMonitorSlot = 0;
	m_monitorState = monitorState::Off;
	m_blnReceivedInitialMonitorState = false;

	m_iAmuSource = 0;
	m_iAmuDest = 0;
	m_iHydraDevice = 0;
	m_bReduced_mode = false;
	

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly


	m_waitForInitialRevertive = true;
	m_selected = false;
	panelShow(PANEL_MAIN, "main.bncs_ui");
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_port_monitor::~comms_port_monitor()
{
}

// all button pushes and notifications come here
void comms_port_monitor::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		m_selected = true;
		textPut("statesheet", "enum_selected", PANEL_MAIN, "grpMonitor");

		hostNotify(bncs_string("selected=%1").arg(m_setting));
		if( b->id() == "monitor_input")
		{
			switch(m_monitorState)
			{
			case monitorState::Off:
			case monitorState::Output:
					if(m_crapInput.valid)
					{
						infoWrite(m_intDeviceMonitor, bncs_string("%1,S").arg(m_crapInput.ring_port), m_intMonitorSlot);
					}
					else
					{
						infoWrite(m_intDeviceMonitor, "0", m_intMonitorSlot);
					}
					m_monitorState = monitorState::Input;
					break;
			case monitorState::Input:
					infoWrite(m_intDeviceMonitor, "0", m_intMonitorSlot);
					m_monitorState = monitorState::Off;
					break;
			}
			updateMonitorState();
		}
		else if( b->id() == "output_clone")
		{
			switch(m_monitorState)
			{
			case monitorState::Off:
			case monitorState::Input:
					if(m_crapOutput.valid)
					{
						infoWrite(m_intDeviceMonitor, bncs_string("%1,D").arg(m_crapOutput.ring_port), m_intMonitorSlot);
					}
					else
					{
						infoWrite(m_intDeviceMonitor, "0", m_intMonitorSlot);
					}
					m_monitorState = monitorState::Output;
					break;
			case monitorState::Output:
					infoWrite(m_intDeviceMonitor, "0", m_intMonitorSlot);
					m_monitorState = monitorState::Off;
			}
			updateMonitorState();
		}
		else if(b->id() == "force_amu" && b->value() == "released")
		{
			// reduced mode does not show/use AMU
			if(!m_bReduced_mode)
			{

				infoWrite(m_iHydraDevice, m_iAmuSource, m_iAmuDest);
				infoWrite(m_iHydraDevice, m_iAmuSource, m_iAmuDest+1);
			}
		}
	}
}

// all revertives come here
int comms_port_monitor::revertiveCallback( revertiveNotify * r )
{
	r->dump("comms_port_monitor::RCB:");
	if(r->device() == m_intDeviceMonitor && r->index() == m_intMonitorSlot)
	{
		r->dump("comms_port_monitor::RCB::monitor:");
		monitorState intCurrentMonitorState = monitorState::Off;
		bncs_string strMonitorPortName = "-";
		bncs_stringlist sltMonitorRevertive = bncs_stringlist().fromString(r->sInfo());
		if(sltMonitorRevertive[0] == "0")
		{
			m_intMonitorPort = 0;
			intCurrentMonitorState = monitorState::Off;
		}
		else if(sltMonitorRevertive[1].upper() == "S")
		{
			m_crapMonitorPort = rh->GetPort(sltMonitorRevertive[0]);
			if (m_crapMonitorPort.valid)
			{
				strMonitorPortName = m_crapMonitorPort.sourceName_short;
				intCurrentMonitorState = monitorState::Input;
			}

		}
		else if(sltMonitorRevertive[1].upper() == "D")
		{
			m_crapMonitorPort = rh->GetPort(sltMonitorRevertive[0]);
			if (m_crapMonitorPort.valid)
			{
				strMonitorPortName = m_crapMonitorPort.destName_short;
				intCurrentMonitorState = monitorState::Output;
			}
		}
		textPut("text", strMonitorPortName.replace("|", " "), PANEL_MAIN, "tally");

		if(m_blnReceivedInitialMonitorState == false)
		{
			m_blnReceivedInitialMonitorState = true;

			//Set the component to match the detected state of the monitor
			m_monitorState = intCurrentMonitorState;

		}
		if (m_waitForInitialRevertive == true)
		{
			m_waitForInitialRevertive = false;
			controlEnable(PANEL_MAIN, "monitor_input");
			controlEnable(PANEL_MAIN, "output_clone");
		}
		updateMonitorState();
	}
	else if(r->device() == m_iHydraDevice)
	{
		if(r->index() == m_iAmuDest)
		{
			m_bLeftAMU = r->sInfo().toInt() == m_iAmuSource?true:false;
			setAMUStyle();
		}
		else if(r->index() == m_iAmuDest + 1)
		{
			m_bRightAMU = r->sInfo().toInt() == m_iAmuSource?true:false;
			setAMUStyle();
		}
	}
	debug("comms_port_monitor::revertiveCallback r->device(%1) r->index(%2) r->sInfo(%3)",r->device(), r->index(), r->sInfo());
	return 0;
}

// all database name changes come back here
void comms_port_monitor::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_port_monitor::parentCallback( parentNotify *p )
{
	p->dump("comms_port_monitor::PCB::Dump():");
	if ( p->command() == "instance" && !m_strInstance.length())
	{
		m_strInstance = p->value();
		init();
	}
	else if( p->command() == "input" )
	{
		if (m_selected)
		{
			bncs_string strIndex = p->value();
			updateMonitorInput(strIndex);
		}
	}
	else if( p->command() == "output" )
	{
		if (m_selected)
		{
			bncs_string strIndex = p->value();
			updateMonitorOutput(strIndex);
		}
	}
	else if (p->command() == "setting")
	{
		m_setting = p->value();
		init();
	}
	else if (p->command() == "selected")
	{
		if (p->value() != m_setting)
		{
			infoWrite(m_intDeviceMonitor, "0", m_intMonitorSlot);
			m_monitorState = monitorState::Off;
			debug("Unselect:%1", m_setting);
			m_selected = false;
			updateMonitorState();
		}
	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << bncs_string("setting=%1").arg(m_setting);

		return sl.toString('\n');			
	}
	else if (p->command() == "_commands")
	{
		bncs_stringlist sl;

		sl << bncs_string("input=<value>");
		sl << bncs_string("output=<value>");
		sl << bncs_string("selected=<setting>");

		return sl.toString('\n');
	}
	else if (p->command() == "_events")
	{
		bncs_stringlist sl;

		sl << bncs_string("selected=<setting>");

		return sl.toString('\n');
	}
	return "";
}

// timer events come here
void comms_port_monitor::timerCallback( int id )
{
}

void comms_port_monitor::updateMonitorInput(bncs_string strPortAddress)
{
	m_crapInput = rh->GetPort(strPortAddress);
	if(m_monitorState == monitorState::Input)
	{
		if(m_crapInput.valid)
		{
			infoWrite(m_intDeviceMonitor, bncs_string("%1,S").arg(m_crapInput.ring_port), m_intMonitorSlot);
		}
	}
}

void comms_port_monitor::updateMonitorOutput(bncs_string strPortAddress)
{
	m_crapOutput = rh->GetPort(strPortAddress);
	if(m_monitorState == monitorState::Output)
	{
		if (m_crapOutput.valid)
		{
			infoWrite(m_intDeviceMonitor, bncs_string("%1,D").arg(m_crapOutput.ring_port), m_intMonitorSlot);
		}
	}
}

void comms_port_monitor::init()
{
	if (m_strInstance.length() == 0 || m_setting.length() == 0)
	{
		debug("comms_port_monitor::init()::no Init, instance <%1>, setting <%2>", m_strInstance, m_setting);
		return; 
	}

	bncs_string strRingMasterInstance;
	instanceLookupComposite(m_strInstance, "main", strRingMasterInstance);
	getDev(strRingMasterInstance, &m_intDeviceMonitor);
	debug("comms_port_monitor::init()::Instance %1, Ringmaster Instance %2, Monitor Device %3",m_strInstance, strRingMasterInstance,m_intDeviceMonitor);

	if (m_intDeviceMonitor > 0 && m_setting.length() > 0)
	{
		controlDisable(PANEL_MAIN, "monitor_input");
		controlDisable(PANEL_MAIN, "output_clone");

		bncs_string strOpsPosition = getWorkstationSetting("ops_position");
		//bncs_string monSetting = getObjectSetting(strOpsPosition, m_setting);
		bncs_string monSetting = bncs_config(bncs_string("ops_positions.%1.%2").arg(strOpsPosition).arg(m_setting)).attr("value");
		bncs_string monitor_ports_tag = bncs_config(bncs_string("ring_master_config.%1.monitor_ports").arg(m_strInstance)).attr("value");

		debug("comms_port_monitor::init()::ops_position %1, setting %2=%3, monitor_ports=%4", strOpsPosition, m_setting, monSetting,monitor_ports_tag);

		if (monSetting.length() == 0  || monitor_ports_tag.length() == 0)
		{
			//No setting lets not bother doing anything else
			return;
		}

		//Look for a related amu setting
		//bncs_string amu = getObjectSetting(strOpsPosition, bncs_string("%1_amu").arg(m_setting));
		//setupAMU(amu);

		//<item id="mcr_ifb_1" ring="12" port="589" instance="ring_master_main" slot_index="1021" />
		//bncs_config c = bncs_config(bncs_string("ring_master_config.mcr_monitor_ports.%1").arg(monSetting));
		bncs_config c = bncs_config(bncs_string("ring_master_config.%1.%2").arg(monitor_ports_tag).arg(monSetting));
		if (c.isValid())
		{
			m_intMonitorSlot = c.attr("slot_index");
			int ring = c.attr("ring").toInt();
			int port = c.attr("port").toInt();
			comms_ringmaster_port crap = rh->GetPort(ring, port);
			debug("comms_port_monitor::ring=%1, port=%2, destname=%3, monitor slot_index=%4", ring, port, crap.destName_short, m_intMonitorSlot);
			bncs_string sName = bncs_string("Monitor - %1").arg(crap.destName_short);
			textPut("text", sName, PANEL_MAIN, "grpMonitor");

			m_waitForInitialRevertive = true;

			//controlEnable(PANEL_MAIN, "monitor_input");
			//controlEnable(PANEL_MAIN, "output_clone");
			infoRegister(m_intDeviceMonitor, m_intMonitorSlot, m_intMonitorSlot, true);
			infoPoll(m_intDeviceMonitor, m_intMonitorSlot, m_intMonitorSlot);
		}

	}
	else
	{
		panelDestroy(PANEL_MAIN);
		panelShow(PANEL_MAIN, "error.bncs_ui");
	}
}

void comms_port_monitor::updateMonitorState()
{
	switch(m_monitorState)
	{
	case monitorState::Off:
			textPut("statesheet", "enum_deselected", PANEL_MAIN, "monitor_input");
			textPut("statesheet", "enum_deselected", PANEL_MAIN, "output_clone");
			textPut("statesheet", "enum_deselected", PANEL_MAIN, "grpMonitor");
			hostNotify("output_clone=0");
			break;
	case monitorState::Input:
			textPut("statesheet", "enum_ok", PANEL_MAIN, "monitor_input");
			textPut("statesheet", "enum_deselected", PANEL_MAIN, "output_clone");
			hostNotify("output_clone=0");
			break;
	case monitorState::Output:
			textPut("statesheet", "enum_deselected", PANEL_MAIN, "monitor_input");
			textPut("statesheet", "enum_warning", PANEL_MAIN, "output_clone");
			hostNotify("output_clone=1");
			break;
	}
}

void comms_port_monitor::setupAMU(bncs_string sourceDestAMU)
{

	// reduced mode does not show/use AMU
	controlHide(PANEL_MAIN, "force_amu");
	//controlDisable(PANEL_MAIN, "amu_tally");

	//If the string is found in object settings than un hide the button
	if(sourceDestAMU.length() > 0)
	{
		bncs_stringlist sl;
		sl.fromString(sourceDestAMU,',');

		m_iAmuSource = sl.getNamedParam("source").toInt();
		m_iAmuDest = sl.getNamedParam("dest").toInt();

		if(m_iAmuDest > 0)
		{
			controlShow(PANEL_MAIN, "force_amu");
			//controlEnable(PANEL_MAIN, "amu_tally");
				
			textPut("instance", RTR_HYDRA,PANEL_MAIN,"amu_tally");
			//textPut("global_dest",m_iAmuDest,PANEL_MAIN,"amu_tally");

			int intDeviceOffset = (m_iAmuDest / 4096);
			m_iAmuDest = m_iAmuDest % 4096;

			getDev(RTR_HYDRA,&m_iHydraDevice);
			m_iHydraDevice = m_iHydraDevice + intDeviceOffset;
				

			infoRegister(m_iHydraDevice, m_iAmuDest, m_iAmuDest+1);
			infoPoll(m_iHydraDevice, m_iAmuDest, m_iAmuDest+1);	
		}
	}
}

void comms_port_monitor::setAMUStyle()
{
	// reduced mode does not show/use AMU
	if(!m_bReduced_mode)
	{

		if(m_bLeftAMU && m_bRightAMU)
		{
			textPut("stylesheet", "enum_ok", PANEL_MAIN, "force_amu");
		}
		else
		{
			textPut("stylesheet", "enum_warning", PANEL_MAIN, "force_amu");
		}

	}
}