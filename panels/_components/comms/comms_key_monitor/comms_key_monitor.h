#ifndef comms_key_monitor_INCLUDED
	#define comms_key_monitor_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_key_monitor : public bncs_script_helper
{
public:
	comms_key_monitor( bncs_client_callback * parent, const char* path );
	virtual ~comms_key_monitor();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	void init();
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	bncs_string getOpsSetting(bncs_string position, bncs_string parameter);
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	bool m_blnEnabled;
	bool m_blnUseVox;
};


#endif // comms_key_monitor_INCLUDED