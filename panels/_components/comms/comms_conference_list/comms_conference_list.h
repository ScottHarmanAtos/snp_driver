#ifndef comms_conference_list_INCLUDED
	#define comms_conference_list_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_conference_list : public bncs_script_helper
{
public:
	comms_conference_list( bncs_client_callback * parent, const char* path );
	virtual ~comms_conference_list();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	void init(); 
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper * rh;
	bncs_string m_strInstanceComposite;
	bncs_string m_strInstanceConference;
	int m_intDeviceConference;

};


#endif // comms_conference_list_INCLUDED