#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_package_conference.h"

#define PNL_MAIN	"comms_package_conference"

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(comms_package_conference)

const int comms_package_conference::offset_conf_labels = 1000;

const bncs_string comms_package_conference::style_selected = "enum_selected";
const bncs_string comms_package_conference::style_deselected = "enum_deselected";

const bncs_string comms_package_conference::attr_text = "text";
const bncs_string comms_package_conference::attr_statesheet = "statesheet";

// constructor - equivalent to ApplCore STARTUP
comms_package_conference::comms_package_conference(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path),
	m_dev_conf_comms(0),
	m_source_package(0),
	m_active_conferences(0)
{
	panelShow(PNL_MAIN, "main.bncs_ui");

	rh = rh->getriedelHelperObject();

	m_dev_ringmaster_confs = rh->GetRingmasterDevices().Conferences;
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_package_conference::~comms_package_conference()
{
}

// all button pushes and notifications come here
void comms_package_conference::buttonCallback(buttonNotify *b)
{
	if (b->id().startsWith("btn_conf_"))
	{
		if (b->id() != "")
		{
			textPut(attr_statesheet, style_deselected, PNL_MAIN, m_selected_conference_btn);
		}

		m_selected_conference_btn = b->id();
		textPut(attr_statesheet, style_selected, PNL_MAIN, m_selected_conference_btn);

		for (pair<int, bncs_string> slot_label : m_slot_labels)
		{
			if (slot_label.second.firstInt() == b->id().firstInt())
			{
				hostNotify(bncs_string("selected=%1").arg(slot_label.first - offset_conf_labels));
				break;
			}
		}
	}
}

// all revertives come here
int comms_package_conference::revertiveCallback(revertiveNotify * r)
{
	if (r->device() == m_dev_ringmaster_confs && m_slot_labels.count(r->index()) > 0 && m_slot_labels[r->index()] != "")
	{
		textPut(attr_text, r->sInfo(), PNL_MAIN, m_slot_labels[r->index()]);
	}
	else if (r->device() == m_dev_conf_comms && r->index() == m_source_package)
	{
		const int min_conferences = 0,
			max_conferences = 3;

		bncs_stringlist conferences = bncs_stringlist(r->sInfo()),
			values = "";

		// Reset the count
		m_active_conferences = min_conferences;

		for (bncs_string response : conferences)
		{
			bncs_string key = "",
				value = "";

			response.split('=', key, value);

			// Not 0 so increase the conference count
			if (value.toInt() != 0)
			{
				values.append(value.toInt() + offset_conf_labels);
				m_active_conferences++;
			}
		}

		infoUnregister(m_dev_ringmaster_confs);

		bncs_stringlist elements = getIdList(PNL_MAIN, "btn_conf_");
		for (bncs_string element : elements)
		{
			int conference = element.firstInt();
			const bncs_string conf_lbl = bncs_string("lbl_conf_%1").arg(conference);

			if (conference > m_active_conferences)
			{
				controlDisable(PNL_MAIN, element);
				controlDisable(PNL_MAIN, conf_lbl);
			}
			else
			{
				controlEnable(PNL_MAIN, element);
				controlEnable(PNL_MAIN, conf_lbl);
			}

			int conference_slot = values[conference - 1];

			m_slot_labels[conference_slot] = conf_lbl;

			if (conference_slot > 0)
			{
				infoRegister(m_dev_ringmaster_confs, conference_slot, conference_slot, true);
				infoPoll(m_dev_ringmaster_confs, conference_slot, conference_slot);
			}
		}


	}

	return 0;
}

// all database name changes come back here
void comms_package_conference::databaseCallback(revertiveNotify * r)
{
	if (r->device() == m_dev_package_router && r->index() == m_source_package)
	{
		setSourceName();
	}
}

// all parent notifications come here i.e. when this script is just one
//  component of another dialog then our host might want to tell us things
bncs_string comms_package_conference::parentCallback(parentNotify *p)
{
	if (p->command() == "return")
	{
		if (p->value() == "all")
		{
			bncs_stringlist sl;

			sl << bncs_string("source_package=%1").arg(m_source_package);

			return sl.toString('\n');
		}
	}

	else if (p->command() == "instance" && p->value() != m_instance)
	{
		m_instance = p->value();

		bncs_string router_instance = getPackageInstance("router"),
			conf_instance = getPackageInstance("conf_comms");

		getDev(conf_instance, &m_dev_conf_comms);
		getDev(router_instance, &m_dev_package_router);
	}

	else if (p->command() == "source_package")
	{
		m_source_package = p->value().toInt();

		// Clear the tallies so that nothing is left over from last package
		m_slot_labels.clear();
		for (bncs_string tally : getIdList(PNL_MAIN, "lbl_"))
		{
			textPut(attr_text, PNL_MAIN, tally);
		}

		// Setup the new source name
		setSourceName();

		infoUnregister(m_dev_conf_comms);
		infoRegister(m_dev_conf_comms, m_source_package, m_source_package, true);
		infoPoll(m_dev_conf_comms, m_source_package, m_source_package);
	}

	else if (p->command() == "deselect")
	{
		if (m_selected_conference_btn != "")
		{
			textPut(attr_statesheet, style_deselected, PNL_MAIN, m_selected_conference_btn);
		}

		m_selected_conference = 0;
		m_selected_conference_btn = "";
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if (p->command() == "_events")
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "selected=*";

		return sl.toString('\n');
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if (p->command() == "_commands")
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;

		sl << "deselect=[value]";
		sl << "source_package=[value]";

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void comms_package_conference::timerCallback(int id)
{
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


bncs_string comms_package_conference::getPackageInstance(bncs_string id)
{
	bncs_config config(bncs_string("instances.%1.%2").arg(m_instance).arg(id));
	return config.attr("instance");
}

void comms_package_conference::setSourceName()
{
	bncs_string name = "";
	routerName(m_dev_package_router, 0, m_source_package, name);

	name.replace('|', " ");

	textPut(attr_text, name, PNL_MAIN, "lbl_package_name");
}