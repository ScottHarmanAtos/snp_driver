#ifndef comms_package_conference_INCLUDED
	#define comms_package_conference_INCLUDED

#include <bncs_script_helper.h>
#include <map>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_package_conference : public bncs_script_helper
{
public:
	comms_package_conference( bncs_client_callback * parent, const char* path );
	virtual ~comms_package_conference();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	// Commands
	int m_source_package;
	bncs_string m_instance;

	// General Members
	int m_dev_conf_comms;
	int m_dev_package_router;
	int m_active_conferences;
	int m_dev_ringmaster_confs;
	map<int, bncs_string> m_slot_labels;

	int m_selected_conference;
	bncs_string m_selected_conference_btn;

	riedelHelper *rh;

	// Methods
	bncs_string getPackageInstance(bncs_string id);

	void setSourceName();

	// Static variables
	static const int offset_conf_labels;

	static const bncs_string style_selected;
	static const bncs_string style_deselected;

	static const bncs_string attr_text;
	static const bncs_string attr_statesheet;
};


#endif // comms_package_conference_INCLUDED