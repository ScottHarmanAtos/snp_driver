# comms_package_conference

Given a source package, this component will:
- Get the DB0 name from the packager
- Find how many conferences are active and their labels
- On button press, alert the conference ID of the selected conference.

## Commands

### source_package
The source package index that should be used for this component. The expected range is 1-200.

### deselect
Will remove the selected state from this package's conferences.

## Notifications

### selected
The selected conference number.