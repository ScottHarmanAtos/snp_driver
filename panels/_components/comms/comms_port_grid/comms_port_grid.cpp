#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <bncs_config.h>
#include "comms_port_grid.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(comms_port_grid)

#define PANEL_MAIN			"main"

#define STORE bncs_string("comms_port_grid.%1.")

#define TIMER_SETPAGE 1
#define TIMER_SETPAGE_LENGTH 20

// constructor - equivalent to ApplCore STARTUP
comms_port_grid::comms_port_grid(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path)
{
	lc = local_cache::getLocalCache();
	rh = riedelHelper::getriedelHelperObject();
	m_PortView = VIEW_PORT_TYPE_INPUT;
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1

	m_panel = "main";
	panelShow(PANEL_MAIN, bncs_string("%1.bncs_ui").arg(m_panel));

	auto rmd = rh->GetRingmasterDevices();
	textPut("device", rmd.Main, PANEL_MAIN, "port_groups");
	textPut("device", rmd.Main, PANEL_MAIN, "port_pages");
	textPut("device.map", rmd.Main, PANEL_MAIN, "port_groups");
	textPut("device.map", rmd.Main, PANEL_MAIN, "port_pages");

	debug("comms_port_grid::  rmd.Main=%1 Enum:%2", rmd.Main);


	bncs_string r, c, p;
	textGet("rows", PANEL_MAIN, "port_pages", r);
	textGet("columns", PANEL_MAIN, "port_pages", c);

	m_pageOffset = (r.toInt() * c.toInt());
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_port_grid::~comms_port_grid()
{
}

// all button pushes and notifications come here
void comms_port_grid::buttonCallback( buttonNotify *b )
{
	debug("comms_port_grid::buttonCallback id=%1 command=%2 value=%3", b->id(), b->command(), b->value());

	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == "port_groups" && b->command() == "index")
		{
			int intGroup = b->value().toInt();
			textPut("page", intGroup, PANEL_MAIN, "port_pages");
			if (b->value() > -1)
				lc->setValue(STORE.arg(m_PortView) + "group", b->value());
			hostNotify("group=" + intGroup);
		}
		else if( b->id() == "port_pages" && b->command() == "index")
		{
			textPut("page", b->value(), PANEL_MAIN, "ports");
			if (b->value() > -1)
			{
				bncs_string p;
				textGet("page", PANEL_MAIN, "port_pages", p);
				if (p == 0)
					p = 1;
				int i = b->value().toInt() - (m_pageOffset * (p.toInt() - 1));

				lc->setValue(STORE.arg(m_PortView) + "page", i);
			}
			hostNotify( "index=-1");	//send no selection
			hostNotify("page=" + b->value());
		}
		else if(b->id() == "ports")
		{
			if (b->command() == "index")
			{
				hostNotify(bncs_string("index=%1").arg(b->value()));
			}
			else if (b->command() == "button" && b->value() == "released")
			{
				hostNotify("button=released");
			}
		}
	}
}

// all revertives come here
int comms_port_grid::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}
// all database name changes come back here
void comms_port_grid::databaseCallback( revertiveNotify * r )
{

}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_port_grid::parentCallback( parentNotify *p )
{
	if(p->command() == "port_view")
	{
		m_PortView = getPortType::view_port_type(p->value());
		debug("comms_port_grid:: port_view: String:%1 Enum:%2", p->value(), m_PortView);
		textPut("view_port_type", getPortType::view_port_type(m_PortView), PANEL_MAIN, "ports");
		if (m_PortView == ENUM_PORT_VIEWS::VIEW_PORT_MAPPING_INPUT || m_PortView == ENUM_PORT_VIEWS::VIEW_PORT_MAPPING_OUTPUT)
		{
			textPut("mapping", "true", PANEL_MAIN, "ports");
		}
		else
			textPut("mapping", "false", PANEL_MAIN, "ports");
	}
	else if (p->command() == "panel")
	{
		if (m_panel != p->value())
		{
			m_panel = p->value();
			panelDestroy(PANEL_MAIN);
			panelShow(PANEL_MAIN, bncs_string("%1.bncs_ui").arg(m_panel));

			auto rmd = rh->GetRingmasterDevices();
			textPut("device", rmd.Main, PANEL_MAIN, "port_groups");
			textPut("device", rmd.Main, PANEL_MAIN, "port_pages");
			textPut("device.map", rmd.Main, PANEL_MAIN, "port_groups");
			textPut("device.map", rmd.Main, PANEL_MAIN, "port_pages");

			debug("comms_port_grid:: port_view: rmd.Main=%1 Enum:%2", rmd.Main);


			bncs_string r, c, p;
			textGet("rows", PANEL_MAIN, "port_pages", r);
			textGet("columns", PANEL_MAIN, "port_pages", c);
			textPut("view_port_type", getPortType::view_port_type(m_PortView), PANEL_MAIN, "ports");
			if (m_PortView == ENUM_PORT_VIEWS::VIEW_PORT_MAPPING_INPUT || m_PortView == ENUM_PORT_VIEWS::VIEW_PORT_MAPPING_OUTPUT)
			{
				textPut("mapping", "true", PANEL_MAIN, "ports");
			}
			else
				textPut("mapping", "false", PANEL_MAIN, "ports");

			m_pageOffset = r.toInt() * c.toInt();
		}
		bncs_string group;
		lc->getValue(STORE.arg(m_PortView) + "group", group);
		if (group.toInt() > 0)
		{
			textPut("button." + group, "released", PANEL_MAIN, "port_groups");
			timerStart(TIMER_SETPAGE, TIMER_SETPAGE_LENGTH);
		}
	}
	else if (p->command() == "group")
	{
		textPut("button." + p->value(), "released", PANEL_MAIN, "port_groups");
	}
	else if (p->command() == "page")
	{
		textPut("button." + p->value(), "released", PANEL_MAIN, "port_pages");
	}
	else if (p->command() == "save")
	{
		textPut("save", PANEL_MAIN, "ports");
		textPut("save", PANEL_MAIN, "port_pages");
		textPut("save", PANEL_MAIN, "port_groups");
	}
	else if (p->command() == "cancel")
	{
		textPut("cancel", PANEL_MAIN, "ports");
		textPut("cancel", PANEL_MAIN, "port_pages");
		textPut("cancel", PANEL_MAIN, "port_groups");
	}
	else if (p->command() == "pages_rename")
	{
		p->dump("comms_port_grid:: pages_rename");
		textPut("rename", p->value(), PANEL_MAIN, "port_pages");
	}
	else if (p->command() == "groups_rename")
	{
		p->dump("comms_port_grid:: groups_rename:");
		textPut("rename", p->value(), PANEL_MAIN, "port_groups");
	}
	else if(p->command() == "deselect")
	{
		textPut("deselect", PANEL_MAIN, "ports");
	}
	else if (p->command() == "index")
	{
		//Used for mapping
		textPut("index", p->value(), PANEL_MAIN, "ports");
	}
	//Return Commands
	else if( p->command() == "return" )
	{
		if(p->value() == "all")
		{
			bncs_stringlist sl;

			sl << bncs_string("port_view=%1").arg(getPortType::view_port_type(m_PortView));
			sl << bncs_string("panel=%1").arg(m_panel);

			return sl.toString('\n');
		}
	}
	else if( p->command() == "_events" )
	{
		bncs_stringlist sl;

		sl << "index=<port index>";
		sl << "group=<value>";
		sl << "page=<value>";

		return sl.toString( '\n' );
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;

		sl << bncs_string("port_view=%1,%2,%3,%4,%5,%6,%7,%8,%9")
			.arg(VIEW_PORT_TYPE_NONE)
			.arg(VIEW_PORT_TYPE_INPUT)
			.arg(VIEW_PORT_TYPE_OUTPUT)
			.arg(VIEW_PORT_TYPE_SPLIT)
			.arg(VIEW_PORT_TYPE_4WIRE)
			.arg(VIEW_PORT_TYPE_PANEL)
			.arg(VIEW_PORT_TYPE_PANEL_DUAL_CHANNEL)
			.arg(VIEW_PORT_MAPPING_INPUT)
			.arg(VIEW_PORT_MAPPING_OUTPUT);
		sl << "group=<value>";
		sl << "page=<value>";
		sl << "index=<valueForMapping Ring.Port>";

		return sl.toString( '\n' );
	}
	else if (p->command() == "take")
	{
		debug("comms_grid::parentCallback take:'%1' to button %2", p->value(), "ports");
		bncs_string s = "take";
		bncs_string subs = "";
		for (int i = 0; i < p->subs(); ++i)
		{
			subs += "." + p->sub(i);
		}
		if (subs != ".")
			s += subs;
		textPut(s, p->value(), PANEL_MAIN, "ports");
	}
	else if (p->command() == "undo")
	{
		textPut("undo", PANEL_MAIN, "ports");
	}
	return "";
}

// timer events come here
void comms_port_grid::timerCallback( int id )
{
	timerStop(id);
	bncs_string page;
	lc->getValue(STORE.arg(m_PortView) + "page", page);
	if(page.toInt() > 0)
		textPut("button." + page, "released", PANEL_MAIN, "port_pages");
}


