# Comms Port Grid Component

The comms port gird has been created to work within a ringmaster system.

It displays the comms_grid with pages and groups, it works for both INPUTs and OUTPUTs.

It has been built to work with connections.

It has been built to work with mapping grids as well.

### Note

An instance does not have to be supplied as the component gets this information from the **riedelHelper.dll**

## Commands

| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| port_view | Port view to use passed to comms grid |
| panel | Panel select, there are 2 panels currently main.bncs_ui and assign_key.bncs_ui |
| group | Select group |
| page | Select Page |
| save | Save - Mapping Only |
| cancel | Cancel - Mapping Only |
| pages_rename | Passes page rename to page |
| groups_rename | Passes group rename to group |
| deselect | deselect the comms_grid |
| index | Pass an index to the selected button - Mapping Only |


## Notifications

| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| index=-1 | No buttons selected |
| index=RING.PORT | RING.PORT from selected comms_port |
| page=Index | Page selected |
| group=Index | Group selected |
| button=released | A comms_port has been pressed |


## Stylesheets
None