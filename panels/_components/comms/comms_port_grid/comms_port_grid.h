#ifndef comms_port_grid_INCLUDED
	#define comms_port_grid_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"
#include "local_cache.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_port_grid : public bncs_script_helper
{
public:
	comms_port_grid( bncs_client_callback * parent, const char* path );
	virtual ~comms_port_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:
	local_cache * lc;
	riedelHelper * rh;

	ENUM_PORT_VIEWS m_PortView;

	bncs_string m_panel;


	int m_pageOffset;

};


#endif // comms_port_grid_INCLUDED