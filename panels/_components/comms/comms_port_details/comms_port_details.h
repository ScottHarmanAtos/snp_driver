#ifndef comms_port_details_INCLUDED
	#define comms_port_details_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_port_details : public bncs_script_helper
{
public:
	comms_port_details( bncs_client_callback * parent, const char* path );
	virtual ~comms_port_details();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:
	riedelHelper *rh;
	void updateDetails(bool source, comms_ringmaster_port port);
	bncs_string m_strInstance;
	bncs_stringlist m_sltPortTypes;

	ENUM_RIEDEL_PORT_TYPES m_intPortDirection;
	int m_intAliasSlotOffset;
	comms_ringmaster_port m_intPortIndex;
	int m_intPortType;

	bncs_string m_strLayout;
};


#endif // comms_port_details_INCLUDED