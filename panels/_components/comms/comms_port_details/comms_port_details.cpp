#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_port_details.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_port_details )

#define PANEL_MAIN	"comms_port_details"
#define POPUP_KEYBOARD "comms_port_details_keyboard_popup"

// constructor - equivalent to ApplCore STARTUP
comms_port_details::comms_port_details( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	m_strLayout = "main";
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, bncs_string("%1.bncs_ui").arg(m_strLayout) );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
	setSize( 1 );				// set the size to the same as the specified panel

	//added 23.01.12 Rax & Tim  - decodes the enum for user labels...
	m_sltPortTypes = bncs_stringlist("None,Input,Output,Split,4-wire,Panel (1 ch),Panel (2 ch),Codec,Pool port");

	m_intPortDirection = PORT_TYPE_NONE;

	m_intAliasSlotOffset = 0;
	m_intPortIndex = comms_ringmaster_port();
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_port_details::~comms_port_details()
{
}

// all button pushes and notifications come here
void comms_port_details::buttonCallback( buttonNotify *b )
{
	if (b->panel() == PANEL_MAIN)
	{
		if (b->id() == "btnAlias" && b->value() == "released")
		{
			panelPopup(POPUP_KEYBOARD, "popup_keyboard.bncs_ui");
			bncs_string strBtnText;
			textGet("text", PANEL_MAIN, "lblAlias", strBtnText);
			textPut("default", strBtnText, POPUP_KEYBOARD, "keyboard");
		}
	}
	else if (b->panel() == POPUP_KEYBOARD)
	{
		if (b->id() == "close")
		{
			panelDestroy(POPUP_KEYBOARD);
		}
		else if (b->id() == "reset")
		{
			//NOT IMPLEMENTED, there is a button on the panel, but isn't any code behind.
		}
		else if (b->id() == "keyboard")
		{
			bncs_string strKeyboardRet;
			textGet("text", POPUP_KEYBOARD, "keyboard", strKeyboardRet);
			infoWrite(m_intPortIndex.devices.Alias , strKeyboardRet, m_intAliasSlotOffset + m_intPortIndex.port);
			panelDestroy(POPUP_KEYBOARD);
		}
	}
}

// all revertives come here
int comms_port_details::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_intPortIndex.devices.Alias && r->index() == m_intPortIndex.port + m_intAliasSlotOffset)
	{
		bncs_string alias = r->sInfo().left(8);

		textPut("text", alias, PANEL_MAIN, "lblAlias");
		//textPut("text", alias, PANEL_MAIN, "btnAlias");

	}
	
	return 0;
}

// all database name changes come back here
void comms_port_details::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_port_details::parentCallback( parentNotify *p )
{
	debug("pCommand = %1", p->command());

	if( p->command() == "input" )
	{
		m_intPortIndex = rh->GetPort(p->value());
		m_intAliasSlotOffset = 0;
		infoRegister(m_intPortIndex.devices.Alias, m_intPortIndex.port, m_intPortIndex.port);
		infoPoll(m_intPortIndex.devices.Alias, m_intPortIndex.port, m_intPortIndex.port);
		bncs_string sub;
		routerName(m_intPortIndex.devices.Grd, riedelHelpers::databases::Subtitle_Input, m_intPortIndex.slot_index, sub);
		textPut("text", sub, PANEL_MAIN, "subtitle");

		m_intPortDirection = PORT_TYPE_INPUT;

		//DATABASE_PORT_TYPE
		updateDetails(true, m_intPortIndex);

	}
	else if( p->command() == "output" )
	{
		m_intPortIndex = rh->GetPort(p->value());
		m_intAliasSlotOffset = riedelHelpers::offset::ALIAS_DEST;
		infoRegister(m_intPortIndex.devices.Alias, m_intAliasSlotOffset + m_intPortIndex.port, m_intAliasSlotOffset + m_intPortIndex.port);
		infoPoll(m_intPortIndex.devices.Alias, m_intAliasSlotOffset + m_intPortIndex.port, m_intAliasSlotOffset + m_intPortIndex.port);
		bncs_string sub;
		routerName(m_intPortIndex.devices.Grd, riedelHelpers::databases::Subtitle_Output, m_intPortIndex.slot_index, sub);
		textPut("text", sub, PANEL_MAIN, "subtitle");

		m_intPortDirection = PORT_TYPE_OUTPUT;
		updateDetails(false, m_intPortIndex);
	}
	else if (p->command() == "return")
	{
		return "layout=" + m_strLayout;
	}
	else if (p->command() == "layout")
	{
		bncs_string s = p->value();
		if (p->value().length() == 0)
		{
			s = "main";
		}
		if (m_strLayout != s)
		{
			m_strLayout = s;
			panelDestroy(PANEL_MAIN);
			panelShow(PANEL_MAIN, bncs_string("%1.bncs_ui").arg(m_strLayout));
		}
	}


	return "";
}

// timer events come here
void comms_port_details::timerCallback( int id )
{
}

void comms_port_details::updateDetails(bool source, comms_ringmaster_port port)
{
	debug("port = %1", port.ring_port);
	if (!port.valid)
	{
		textPut("text", "", PANEL_MAIN, "longname");
		textPut("text", "", PANEL_MAIN, "address");
		textPut("text", "", PANEL_MAIN, "type");

		textPut("text", "", PANEL_MAIN, "lblAlias");
		//textPut("text", "", PANEL_MAIN, "btnAlias");

		controlHide(PANEL_MAIN,"btnAlias");
		controlShow(PANEL_MAIN,"lblAlias");

	}
	else
	{
		bncs_string strName;
		if (source)
			strName = port.sourceName_long;
		else
			strName = port.destName_long;
		textPut("text", bncs_string(" %1").arg(strName.replace('|', ' ')), PANEL_MAIN, "longname");
		routerName(port.devices.Grd, riedelHelpers::databases::Port_Address, port.port, strName);
		textPut("text", strName, PANEL_MAIN, "address");

		//port type update on UI
		
		m_intPortType = port.portType;
		bncs_string strAlias;
		bool checkPermissions = true;
		// Check if passed input
		if (source)
		{
			if (m_intPortType == PORT_TYPE_OUTPUT)
			{
				strAlias = "*SPLIT*";
				checkPermissions = false;
			}
			else
			{
				strAlias = "";
			}
		}
		else
		{
			debug("intPortType = %1", m_intPortType);
			if (m_intPortType == PORT_TYPE_OUTPUT || m_intPortType == PORT_TYPE_SPLIT || m_intPortType == PORT_TYPE_PANEL)
			{
				// If output was passed then offset as first half of indexs are input, second are outputs
				strAlias = "";
				m_intAliasSlotOffset = riedelHelpers::offset::ALIAS_DEST;
			}
			else
			{
				m_intAliasSlotOffset = 0;
				strAlias = "";
				checkPermissions = false;
			}
		}
		//textPut("text", strAlias, PANEL_MAIN, "btnAlias");		
		textPut("text", strAlias, PANEL_MAIN, "lblAlias");

		if (checkPermissions)
		{
			bncs_string strPermissions;
			routerName(m_intPortIndex.devices.Alias, riedelHelpers::databases::Port_Address, port.port, strPermissions);
			bncs_stringlist listWorkstations(strPermissions, ',');
			if (strPermissions == "*" || listWorkstations.find(bncs_string(workstation())) >= 0)
			{
				controlShow(PANEL_MAIN, "btnAlias");
				//controlHide(PANEL_MAIN, "lblAlias");
			}
			else
			{
				controlHide(PANEL_MAIN, "btnAlias");
				//controlShow(PANEL_MAIN, "lblAlias");
			}
		}
		else
		{
			controlHide(PANEL_MAIN, "btnAlias");
			//controlShow(PANEL_MAIN, "lblAlias");
		}

		if (m_intPortType >= 0 && m_intPortType <= m_sltPortTypes.count())
		{
			textPut("text", m_sltPortTypes[m_intPortType] , PANEL_MAIN, "type");
		}			
		else
		{
			textPut("text", "Unknown!" , PANEL_MAIN, "type");
		}
	}
}
