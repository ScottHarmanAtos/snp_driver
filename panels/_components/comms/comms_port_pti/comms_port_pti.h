#ifndef comms_port_pti_INCLUDED
	#define comms_port_pti_INCLUDED

#include <bncs_script_helper.h>
#include <vector>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_port_pti : public bncs_script_helper
{
public:
	comms_port_pti( bncs_client_callback * parent, const char* path );
	virtual ~comms_port_pti();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );	
	void renderPTIIndication( bool blncontrolEnable, bool blnPortsFree);  // enable button are ports in use?
	void changePanel();
	void showFaultCondition();		//used to signify that the PTI revertive is not present or is badly formed

private:
	riedelHelper* rh;

	void initProbeListview();
	void init();
	bncs_string getConferenceDetails( int intMyConference );
	int m_intSelectedProbeCommand;
	bool m_blnProbeActive;
	int m_intTally;
	ENUM_RIEDEL_PORT_TYPES m_PortInputOutput;
	//bncs_string m_strInstance;
	comms_ringmaster_port m_Port;
	int m_intActiveProbePort;
	int m_intLargestConferenceVal;
	bncs_string m_strSpacer ;
	std::vector<bncs_string> m_strConferenceToSourcePackageLevel;
	bncs_string m_strAlias;                     //placeholder for replacement label
	bncs_string m_skin;

	void Pti(bncs_stringlist sltConfListen, bncs_stringlist sltIFBOutput, bncs_stringlist sltMixerOutput);

	bncs_stringlist m_sltConfListen;
	bncs_stringlist m_sltIFBOutput;
	bncs_stringlist m_sltMixerOutput;

	//int m_packagerIfbStatus;
	//int m_packagerRouter;
	bncs_packager m_packagerIfbStatus;
	bncs_packager m_packagerRouter;
};


#endif // comms_port_pti_INCLUDED