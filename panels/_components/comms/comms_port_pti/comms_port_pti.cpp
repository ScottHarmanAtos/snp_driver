#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <bncs_packager.h>
#include "comms_port_pti.h"
#include "packager_common.h"

#define PANEL_MAIN	"comms_port_pti"
#define POPUP_PTI	"comms_port_pti_popup"
#define POPUP_PROBE	"comms_port_pti_probe_popup"

#define SKIN_MAIN "withoutlabel"
#define SKIN_WITHLABEL "withlabel"

//Forward Route Indication Fields: 
//XY GRD|Monitoring|Conference(as talk)|IFB(mix minus)|Mixer(input)
#define FIELD_GRD			0
#define FIELD_MON			1
#define FIELD_CONF_TALK		2
#define FIELD_IFB_MM		3
#define FIELD_MIXER_INPUT	4

//Contributing Source Indication Fields: 
//Conference(as listen)|IFB(as output)|Mixer(output)
#define FIELD_CONF_LISTEN	0
#define FIELD_IFB_OUTPUT	1
#define FIELD_MIXER_OUTPUT	2

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_port_pti )

// constructor - equivalent to ApplCore STARTUP
comms_port_pti::comms_port_pti( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();
	//init vars
	m_skin = SKIN_MAIN;
	m_Port = comms_ringmaster_port();
	m_blnProbeActive = false;
	//m_strInstance = "";
	m_PortInputOutput = PORT_TYPE_NONE;
	m_intSelectedProbeCommand = 0;
	m_intActiveProbePort = 0;
	m_intLargestConferenceVal = 0;	
	m_strSpacer = "   ";	
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	changePanel();
	renderPTIIndication(false, false);

	//getDev(INSTANCE_PACKAGER_ROUTER, &m_packagerRouter);
	//getDev(INSTANCE_PACKAGER_STATUS, &m_packagerIfbStatus);
	m_packagerRouter = bncs_packager(getWorkstationSetting("packager_instance"));
	m_packagerIfbStatus = bncs_packager(getWorkstationSetting("packager_instance"),"dest_status");
	//getDev(INSTANCE_PACKAGER_ROUTER, &m_packagerRouter);
	//getDev(INSTANCE_PACKAGER_STATUS, &m_packagerIfbStatus);


	init();

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1);				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_port_pti::~comms_port_pti()
{
}

// all button pushes and notifications come here
void comms_port_pti::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == "pti")
		{
			if(m_PortInputOutput == PORT_TYPE_INPUT)
			{
				panelPopup(POPUP_PTI, "popup_input.bncs_ui");

				textPut("text", bncs_string(" PTI for Input Port %1 - %2").arg(m_Port.ring_port).arg(m_Port.sourceName_short.replace('|', ' ')), POPUP_PTI, "title");
				infoPoll(m_Port.devices.Pti, m_Port.port, m_Port.port);
			}
			else if(m_PortInputOutput == PORT_TYPE_OUTPUT)
			{
				panelPopup(POPUP_PTI, "popup_output.bncs_ui");
				controlHide(POPUP_PTI, "warning");
				textPut("text", bncs_string(" PTI for Output Port %1 - %2").arg(m_Port.ring_port).arg(m_Port.destName_short.replace('|', ' ')), POPUP_PTI, "title");
				infoPoll(m_Port.devices.Pti, m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO, m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO);
				
				routerPoll(m_Port.devices.Grd, m_Port.port, m_Port.port);
				debug("comms_port_pti::buttonCallback() RouterPoll for device %1 slot %2", m_Port.devices.Grd, m_Port.ring_port);


				//TH show Source Package IFB level - but only if the instance is the rtr_riedel_ifb

				infoRegister(m_packagerIfbStatus.destDev(m_Port.port), m_packagerIfbStatus.destSlot(m_Port.port), m_packagerIfbStatus.destSlot(m_Port.port), false);
				infoPoll(m_packagerIfbStatus.destDev(m_Port.port), m_packagerIfbStatus.destSlot(m_Port.port), m_packagerIfbStatus.destSlot(m_Port.port));

			}

			bncs_string strOpsPosition = getWorkstationSetting("ops_position");
			if ( getObjectSetting(strOpsPosition, "enable_port_probe") == "true")
			{
				controlShow(POPUP_PTI, "probe");
			}
			else
			{
				controlHide(POPUP_PTI, "probe");
			}
		}
	}
	else if( b->panel() == POPUP_PTI )
	{
		if( b->id() == "close")
		{
			panelRemove(POPUP_PTI);
		}
		else if( b->id() == "probe")
		{
			panelRemove(POPUP_PTI);
			panelPopup(POPUP_PROBE, "popup_probe.bncs_ui");

			textPut("text", bncs_string(" Probe Commands for Port %1 - %2").arg(m_Port.ring_port).arg(m_Port.destName_short.replace('|', ' ')), POPUP_PROBE, "title");

			textPut("statesheet", "enum_deselected", POPUP_PROBE, "probe");
			textPut("text", "-", POPUP_PROBE, "probe_count");
			controlDisable(POPUP_PROBE, "delete_command");
			controlHide(POPUP_PROBE, "warning");
			
			initProbeListview();

			//Immediately start probe
			infoWrite(m_Port.devices.Extras, bncs_string("%1").arg(m_Port.port), riedelHelpers::offset::EXTRAS_PROBE_PORT);
		}
	}
	else if( b->panel() == POPUP_PROBE )
	{
		if( b->id() == "close")
		{
			//TH 3/12/2011 - don't carry on processing probe revertives
			m_blnProbeActive = false;
			//TH 3/12/2011 - end 

			panelRemove(POPUP_PROBE);
		}
		else if( b->id() == "probe")
		{
			infoWrite(m_Port.devices.Extras, bncs_string("%1").arg(m_Port.port), riedelHelpers::offset::EXTRAS_PROBE_PORT);
		}
		else if( b->id() == "lvw_probe_items")
		{
			debug("comms_port_pti::buttonCallback() id=%1 command=%2 value=%3", b->id(), b->command(), b->value());
			controlEnable(POPUP_PROBE, "delete_command");

			bncs_stringlist sltSelectedCommand(b->value(),';');

			int intCommand = sltSelectedCommand[0].toInt();
			bncs_string strCommand = sltSelectedCommand[1];
			if(intCommand > 0 && strCommand.length() > 1)
			{
				m_intSelectedProbeCommand = intCommand;
				controlEnable(POPUP_PROBE, "delete_command");
			}
			else
			{
				m_intSelectedProbeCommand = 0;
				controlDisable(POPUP_PROBE, "delete_command");
			}
		}
		else if( b->id() == "delete_command")
		{
			if(m_blnProbeActive && m_intSelectedProbeCommand > 0)
			{
				int intDeleteCommandSlot = riedelHelpers::offset::EXTRAS_PROBE_COMMAND_FIRST + m_intSelectedProbeCommand - 1;
				infoWrite(m_Port.devices.Extras, bncs_string("&DELETE=%1").arg(m_Port.port), intDeleteCommandSlot);
			}
		}
	}
}

// all revertives come here
int comms_port_pti::revertiveCallback( revertiveNotify * r )
{
	debug("comms_port_pti::revertiveCallback() device=%1 slot=%2 content='%3' index=%4", 
		r->device(), r->index(), r->sInfo(), r->info());

	if( r->device() == m_Port.devices.Pti && r->index() == m_Port.port && m_PortInputOutput == PORT_TYPE_INPUT)
	{
		//Fields: XY GRD|Monitoring|Conference(as talk)|IFB(mix minus)|Mixer(input)
		bncs_stringlist sltFields(r->sInfo(), '|');
		bncs_stringlist sltGRD(sltFields[FIELD_GRD]);
		bncs_stringlist sltMon(sltFields[FIELD_MON]);
		bncs_stringlist sltConf(sltFields[FIELD_CONF_TALK]);
		bncs_stringlist sltIFBMixMinus(sltFields[FIELD_IFB_MM]);
		bncs_stringlist sltMixerInput(sltFields[FIELD_MIXER_INPUT]);
		
		
		if ( sltFields.count() == FIELD_MIXER_INPUT + 1 
			&& sltGRD.count() > 0  
			&& sltMon.count() > 0 
			&& sltConf.count() > 0 
			&& sltIFBMixMinus.count() > 0 
			&& sltMixerInput.count() > 0  )  //i.e. check for coreectly formed data
		{
			//GRD
			textPut("clear", "clear", POPUP_PTI, "lvw_grd");
			if(sltGRD.count() == 1 && sltGRD[0] == "0")
			{
				textPut("add", "<none>", POPUP_PTI, "lvw_grd");
			}
			else
			{
				for(int intItem = 0; intItem < sltGRD.count(); intItem++)
				{
					int intUsage = sltGRD[intItem];
					bncs_string strName;
					routerName(m_Port.devices.Grd, riedelHelpers::databases::Output_Port_Button_Name, intUsage, strName);
					textPut("add", bncs_string("%1;%2").arg(intUsage, '0', 4).arg(strName), POPUP_PTI, "lvw_grd");
				}
			}
			
			
			//Conference Talk
			textPut("clear", "clear", POPUP_PTI, "lvw_conf");
			
			if (sltConf.count() == 1 && sltConf[0] == "0")
			{
				// not speaking in any conferences
				textPut("add", "<none>", POPUP_PTI, "lvw_conf");
				renderPTIIndication(true , true);
			}
			else
			{
				renderPTIIndication(true, false);

				// speaking in one or more conferences
				for(int intItem = 0; intItem < sltConf.count(); intItem++)
				{
					debug("comms_port_pti::revertiveCallback() dev:%1 sltConf[intItem]:%2", m_Port.devices.Conference, sltConf[intItem]);
					int intUsage = sltConf[intItem].toInt();
					bncs_string strName;
					routerName(m_Port.devices.Conference, riedelHelpers::databases::Input_Port_Button_Name, intUsage, strName);
					debug("comms_port_pti::revertiveCallback() dev:%1 sltConf[intItem]:%2 str:%3", m_Port.devices.Conference, sltConf[intItem], strName);
					textPut("add", bncs_string("%1;%2").arg(intUsage, '0' ,4).arg(strName), POPUP_PTI, "lvw_conf");
					infoPoll(m_Port.devices.Conference, riedelHelpers::offset::CONF_LABEL + intUsage, riedelHelpers::offset::CONF_LABEL + intUsage);
				}
			}
			//IFB MixMinus
			textPut("clear", "clear", POPUP_PTI, "lvw_ifb_mixminus");
			if(sltIFBMixMinus.count() == 1 && sltIFBMixMinus[0] == "0")
			{
				textPut("add", "<none>", POPUP_PTI, "lvw_ifb_mixminus");
			}
			else
			{
				for(int intItem = 0; intItem < sltIFBMixMinus.count(); intItem++)
				{
					int intUsage = sltIFBMixMinus[intItem].toInt();
					bncs_string strName;
					routerName(m_Port.devices.Ifb, riedelHelpers::databases::Input_Port_Button_Name, intUsage, strName);
					if(strName == "---")
					{
						strName = bncs_string("IFB %1").arg(intUsage);
					}
					textPut("add", bncs_string("%1;%2").arg(intUsage, '0' ,4).arg(strName), POPUP_PTI, "lvw_ifb_mixminus");
					infoPoll(m_Port.devices.Ifb, intUsage, intUsage);
				}
			}
			
			//Mixer Input
			textPut("clear", "clear", POPUP_PTI, "lvw_mixer_input");
			if(sltMixerInput.count() == 1 && sltMixerInput[0] == "0")
			{
				textPut("add", "<none>", POPUP_PTI, "lvw_mixer_input");
			}
			else
			{
				for(int intItem = 0; intItem < sltMixerInput.count(); intItem++)
				{
					int intUsage = sltMixerInput[intItem].toInt();
					bncs_string strName;
					strName = bncs_string("Mixer %1").arg(intUsage);	//TODO get mixer name
					textPut("add", bncs_string("%1;%2").arg(intUsage, '0' ,2).arg(strName), POPUP_PTI, "lvw_mixer_input");
				}
			}
		}
		else
		{
			showFaultCondition();
		}
		
	}
	else if (r->device() == m_Port.devices.Pti && r->index() == m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO && m_PortInputOutput == PORT_TYPE_OUTPUT)
	{
		//Contributing Source Indication Fields: 
		//Conference(as listen)|IFB(as output)|Mixer(output)
		bncs_stringlist sltFields(r->sInfo(), '|');
		m_sltConfListen = bncs_stringlist(sltFields[FIELD_CONF_LISTEN]);
		m_sltMixerOutput = bncs_stringlist(sltFields[FIELD_MIXER_OUTPUT]);

		Pti(m_sltConfListen, m_sltIFBOutput, m_sltMixerOutput);
	}
	else if (r->device() == rh->GetRingmasterDevices().Ifb_pti && r->index() == m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO && m_PortInputOutput == PORT_TYPE_OUTPUT)
	{
		//Contributing Source Indication Fields: 
		//Conference(as listen)|IFB(as output)|Mixer(output)
		
		if (r->sInfo().length() == 0)
		{
			m_sltIFBOutput = bncs_stringlist("0");
		}
		else
		{
			bncs_stringlist sltFields(r->sInfo(), '|');
			m_sltIFBOutput = bncs_stringlist(sltFields[FIELD_IFB_OUTPUT]);
		}
		Pti(m_sltConfListen, m_sltIFBOutput, m_sltMixerOutput);
	}
	else if (r->device() == m_Port.devices.Conference)
	{

		debug("comms_port_pti::revertiveCallback() CONFERENCE device=%1 slot=%2 content='%3' index=%4",
			r->device(), r->index(), r->sInfo(), r->info());
		int intConference = r->index() - riedelHelpers::offset::CONF_LABEL;

		//Update conf label column
		bncs_string strReturn;
		textGet("count", POPUP_PTI, "lvw_conf", strReturn);
		int intItemCount = strReturn.toInt();
		//Check it the listview contains this conference
		if(intItemCount > 0)
		{
			debug("comms_port_pti::revertiveCallback() CONFERENCE WRITE 1");

			if(m_intTally > 0)
			{
				debug("comms_port_pti::revertiveCallback() CONFERENCE WRITE 2");

				controlShow(POPUP_PTI, "warning");
				bncs_string strTallyName;
				routerName(m_Port.devices.Grd, riedelHelpers::databases::Input_Port_Button_Name, m_intTally, strTallyName);
				bncs_string strWarning = bncs_string("Output port has %1 routed|and is also a conference member").arg(strTallyName);
				textPut("text", strWarning, POPUP_PTI, "warning");
			}
			else
			{
				debug("comms_port_pti::revertiveCallback() CONFERENCE WRITE 3");

				controlHide(POPUP_PTI, "warning");
			}
			for (int intRow = 0; intRow < intItemCount; intRow++)
			{
				debug("comms_port_pti::revertiveCallback() CONFERENCE WRITE");

				textGet(bncs_string("row.%1").arg(intRow), POPUP_PTI, "lvw_conf", strReturn);
				bncs_string strIndex, strNames;
				strReturn.split(';', strIndex, strNames);
				//check if the port index in this row matches the revertive index
				if(strIndex.toInt() == intConference)
				{
					textPut(bncs_string("cell.%1.%2").arg(intRow).arg(2), r->sInfo(), POPUP_PTI, "lvw_conf");
				}
			}
		}
	}
	else if (r->device() == rh->GetRingmasterDevices().Ifbs)
	{
		debug("comms_port_pti::revertiveCallback() m_intRingmasterIFB index:%1 ", r->index());
		int intIFB = r->index();

		bncs_stringlist sltIFBFields(r->sInfo(), '|');
		bncs_string strIFBLabel = sltIFBFields[3];

		//Update conf label column
		bncs_string strReturn;
		
		textGet("count", POPUP_PTI, "lvw_ifb_output", strReturn);
		int intItemCount = strReturn.toInt();
		debug("comms_port_pti::revertiveCallback() m_intRingmasterIFB intItemCount:%1 ", intItemCount);
		//Check it the listview contains this conference
		if(intItemCount > 0)
		{
			for (int intRow = 0; intRow < intItemCount; intRow++)
			{
				textGet(bncs_string("row.%1").arg(intRow), POPUP_PTI, "lvw_ifb_output", strReturn);
				bncs_string strIndex, strNames;
				strReturn.split(';', strIndex, strNames);
				//check if the port index in this row matches the revertive index
				debug("comms_port_pti::revertiveCallback() m_intRingmasterIFB loop index:%1 ", strIndex.toInt());
				if(strIndex.toInt() == intIFB)
				{
					textPut(bncs_string("cell.%1.%2").arg(intRow).arg(2), strIFBLabel, POPUP_PTI, "lvw_ifb_output");
				}
			}
		}
	}
	else if (r->device() == m_Port.devices.Grd && r->index() == m_Port.port)
	{
		m_intTally = r->info();
		debug("comms_port_pti::revertiveCallback() port=%1 tally=%2", m_Port.ring_port, m_intTally);
	}
	else if (r->device() == m_Port.devices.Extras)
	{
		m_intSelectedProbeCommand = 0;
		controlDisable(POPUP_PTI, "delete_command");
		if( r->index() == riedelHelpers::offset::EXTRAS_PROBE_PORT)
		{
			m_intActiveProbePort = r->sInfo().toInt();

			if( m_intActiveProbePort == m_Port.port && m_Port.valid)
			{
				m_blnProbeActive = true;
				textPut("statesheet", "enum_ok", POPUP_PROBE, "probe");

				controlHide(POPUP_PROBE, "warning");
				textPut("text", "", POPUP_PROBE, "warning");
			}
			else
			{
				m_blnProbeActive = false;
				textPut("statesheet", "enum_deselected", POPUP_PROBE, "probe");
				controlDisable(POPUP_PROBE, "delete_command");
				
				controlShow(POPUP_PROBE, "warning");
				textPut("text", bncs_string("Probe has been taken for use on port %1 - %2")
					.arg(m_intActiveProbePort).arg(m_Port.destName_short.replace('|', ' ')), POPUP_PROBE, "warning");

				textPut("text", "-", POPUP_PROBE, "probe_count");
				initProbeListview();

			}
		}
		else if( r->index() == riedelHelpers::offset::EXTRAS_PROBE_COMMAND_COUNT)
		{
			if(m_blnProbeActive)
			{
				textPut("text", r->sInfo(), POPUP_PROBE, "probe_count");
			}
		}
		else if(r->index() >= riedelHelpers::offset::EXTRAS_PROBE_COMMAND_FIRST && r->index() <= riedelHelpers::offset::EXTRAS_PROBE_COMMAND_LAST)
		{
			if(m_blnProbeActive)
			{
				int intCommand = r->index() - riedelHelpers::offset::EXTRAS_PROBE_COMMAND_FIRST;
				textPut(bncs_string("cell.%1.0").arg(intCommand), bncs_string("%1").arg(intCommand + 1), POPUP_PROBE, "lvw_probe_items");
				bncs_string strProbe = r->sInfo();
				
				// TH 03/12/2011 
				// replace the conference ID with H1 specific diagnostic information
				// Conference revertive looks like this: 
				// 'Function (Call to BNCS Conference 134) on Virtual Function Always, panel Trilogy Highway 06, Flags: TL
				// 'Function (Call to BNCS Conference 134) on Virtual Function Always, panel Trilogy Highway 06, Flags TL
				//  Function (Call to BNCS Conference 9) on Virtual Function Always, panel Trilogy Highway 01, Flags: TL
		     //slt  0         1    2  3    4          5  6  7       8        9       10    11      12          13
				// but we want to show
				// 'Call to Riedel Conference 101 [SPXX 4wn] from Trilogy Highway 06 on Virtual Function Always - Flags TL'

				int intMatchPos = strProbe.find("Function (Call to BNCS Conference",0,false);
				
				if ( intMatchPos > -1 )  // match
				{
					bncs_stringlist sltTemp = bncs_stringlist(strProbe.replace("(","").replace(")",""),' ');
					bncs_stringlist sltSections = bncs_stringlist(strProbe);
					bncs_string strPortName  = sltSections[1].mid(7,sltSections[1].length());
					bncs_string strFunctionTrigger = sltTemp[9].replace(",","").simplifyWhiteSpace();
					
					int intConfNumber = sltTemp[5].toInt();
					strProbe = bncs_string("%1 %2 [%3] %4 (%5) %6")
						.arg("Call to Riedel Conference")			//as is
						.arg(bncs_string(intConfNumber,'0',3))			//zero padded version of Riedel conf number
						.arg(getConferenceDetails(intConfNumber)) //lookup against tb_comms_mapping.xml
						.arg(strPortName)								// Trilogy Highway 06
						.arg(strFunctionTrigger)					//always or vox	
						.arg(sltSections[2].simplifyWhiteSpace());                       //flags
					
					
				}
				textPut(bncs_string("cell.%1.1").arg(intCommand), strProbe , POPUP_PROBE, "lvw_probe_items");
			}
		}
		// 		3.13.1	Slot layout
		// 		Slot    | Function
		// 		--------|-------------------------------------------------------------------------------------------------------------------------
		// 		301	    | The port to target by the Port Probe. This is a port index from the XY GRD and must correspond to an audio or pool port.
		// 		302	    | The number of commands found on the port's virtual functions.
		// 		306-325	| 20 slots, containing the detail of the command(s) found. One command per slot. 
		// 				| If more than 20 commands are on a port's virtual functions, only the first 20 will be shown.
		// 		        | Write "&DELETE" to remove manually the command from the port.
	}
	
	// TH add for source package membership
	else if (m_packagerIfbStatus.isDevice(r->device()) && m_packagerIfbStatus.destSlot(m_Port.port) == r->index())
	{
		int intPackageIndex = 0;
		bool usedInSoucePackage = false;
		//All this just to pick out if this exists in a source package as an IFB
		//'10:pkg=1;lvl=4w2|pkg=2;lvl=4w2*11:*12:*13:*21:*31:'
		bncs_stringlist rings(r->sInfo(),'*');
		for (int i = 0; i < rings.count(); i++)
		{
			//10:pkg=1;lvl=4w2|pkg=2;lvl=4w2
			//11:
			//12:
			//13:
			//21:
			//31:
			bncs_string ring, other;			
			rings[i].split(':', ring, other);
			//ring  = 10
			//other = pkg=1;lvl=4w2|pkg=2;lvl=4w2
			if (other.length() != 0)
			{
				//If we are here is must be in a source package for some reason
				usedInSoucePackage = true;

				bncs_stringlist pkgslvls(other, '|');
				for (int ii = 0; ii < pkgslvls.count(); ii++)
				{					
					//pkg=1;lvl=4w2
					//pkg=2;lvl=4w2
					bncs_stringlist pkglvl(pkgslvls[ii], ';');
					bncs_stringlist lvls(pkglvl.getNamedParam("lvl"));

					for (int iii = 0; iii < lvls.count(); iii++)
					{
						if (lvls[iii].startsWith("ifb"))
						{
							intPackageIndex = pkglvl.getNamedParam("pkg").toInt();
							break;
						}
					}
				}
			}
		}

		if (usedInSoucePackage == false)
		{
			controlShow(POPUP_PTI, "source_package_membership");
			// empty slot - not in a source package
			textPut("text", "Port is NOT used in a Source Package", POPUP_PTI, "source_package_membership");
		}
		else if (intPackageIndex == 0)
		{
			controlHide(POPUP_PTI, "source_package_membership");
		}
		else
		{
			controlShow(POPUP_PTI, "source_package_membership");
			//returns one source package indexfor each port
			bncs_string strSourcePackageName, strSourcePackageTitle;
			routerName(m_packagerRouter.deviceNames(DATABASE_SOURCE_NAME), DATABASE_SOURCE_NAME, intPackageIndex, strSourcePackageName);
			routerName(m_packagerRouter.deviceNames(DATABASE_SOURCE_TITLE), DATABASE_SOURCE_TITLE, intPackageIndex, strSourcePackageTitle);


			if ( strSourcePackageTitle.length() > 0)
			{
				strSourcePackageTitle = bncs_string("(%1)").arg(strSourcePackageTitle) ;
			}
			else
			{
				strSourcePackageTitle= ""; 
			}


			bncs_string strSourcePackageDescription = bncs_string("Port is used in %1 %2")
				.arg(strSourcePackageName).arg(strSourcePackageTitle);
			textPut("text", strSourcePackageDescription, POPUP_PTI, "source_package_membership");
		}
	}
	
	
	
	return 0;
}

// all database name changes come back here
void comms_port_pti::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_port_pti::parentCallback( parentNotify *p )
{
	//if ( p->command() == "instance" )
	//{
		//m_strInstance = p->value();
	//}	
	//else 
	if( p->command() == "input" )
	{
		m_blnProbeActive = false;
		m_Port = rh->GetPort(p->value());
		debug("comms_port_pti::parentCallback m_Port = %1", m_Port.toString());		

		if(m_Port.valid)
		{
			infoRegister(m_Port.devices.Conference, riedelHelpers::offset::CONF_LABEL + 1, riedelHelpers::offset::CONF_LABEL + riedelHelpers::max::Conferences);
			infoRegister(rh->GetRingmasterDevices().Ifbs, 1, riedelHelpers::max::IFBs);
			infoRegister(m_Port.devices.Extras, riedelHelpers::offset::EXTRAS_PROBE_START, riedelHelpers::offset::EXTRAS_PROBE_END);

			m_PortInputOutput = PORT_TYPE_INPUT;
			renderPTIIndication(true, true);
			infoRegister(m_Port.devices.Pti, m_Port.port, m_Port.port, true);
			infoPoll(m_Port.devices.Pti, m_Port.port, m_Port.port);
			debug("comms_port_pti::parentCallback() registering for device %1 slot %2", m_Port.devices.Pti, m_Port.port);
		}
		else
		{
			m_PortInputOutput = PORT_TYPE_NONE;
			renderPTIIndication(false, true);
			infoUnregister(m_Port.devices.Pti);
		}
	}
	else if( p->command() == "output" )
	{
		m_blnProbeActive = false;
		m_Port = rh->GetPort(p->value());
		m_intTally = 0;
		if(m_Port.valid)
		{
			infoRegister(m_Port.devices.Conference, riedelHelpers::offset::CONF_LABEL + 1, riedelHelpers::offset::CONF_LABEL + riedelHelpers::max::Conferences);
			infoRegister(rh->GetRingmasterDevices().Ifbs, 1, riedelHelpers::max::IFBs);
			infoRegister(m_Port.devices.Extras, riedelHelpers::offset::EXTRAS_PROBE_START, riedelHelpers::offset::EXTRAS_PROBE_END);

			renderPTIIndication(true, true);
			m_PortInputOutput = PORT_TYPE_OUTPUT;
			infoRegister(m_Port.devices.Pti, m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO,
				m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO, true);
			infoPoll(m_Port.devices.Pti, m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO,
				m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO);

			infoRegister(rh->GetRingmasterDevices().Ifb_pti, m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO,
				m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO, true);
			infoPoll(rh->GetRingmasterDevices().Ifb_pti, m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO,
				m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO);

			debug("comms_port_pti::parentCallback() InfoRegistering for device %1 slot %2", 
				m_Port.devices.Pti, m_Port.port + riedelHelpers::offset::CONTRIBUTING_SOURCE_INFO);
			routerRegister(m_Port.devices.Grd, m_Port.port, m_Port.port, true);
			debug("comms_port_pti::parentCallback() RouterRegistering for device %1 slot %2", 
				m_Port.devices.Grd, m_Port.port);
		}
		else
		{
			m_PortInputOutput = PORT_TYPE_NONE;
			renderPTIIndication(false, true);
			infoUnregister(m_Port.devices.Pti);
			//routerUnregister(m_intDevice);
		}
	}
	else if( p->command() == "alias" )
	{
		if (p->value().length() > 0)
		{
			m_strAlias = p->value();
		}
	
	}
	else if(p->command() == "skin")
	{
		if(m_skin != p->value())
		{
			if(p->value().right(8) == ".bncs_ui")
			{
				m_skin = p->value().left(p->value().length() - 8);
			}
			else
			{
				m_skin = p->value();
			}
			changePanel();
			renderPTIIndication(false, true);
		}		
	}
	else if (p->command() == "_events")
	{
		bncs_stringlist sl;
		sl << "input=[value]";
		sl << "output=[value]";
		return sl.toString('\n');
	}
	else if ( p->command() == "return")
	{
		if ( p->value() == "all")
		{
			bncs_stringlist sltReturn ;
			sltReturn <<	bncs_string( "alias=" ) + m_strAlias;			
			sltReturn <<	bncs_string( "\nskin=" ) + m_skin ;

			return sltReturn.toString('\n');
		}


	}

	return "";
}

// timer events come here
void comms_port_pti::timerCallback( int id )
{
}

void comms_port_pti::initProbeListview()
{
	//Init command listview
	textPut("clear", "clear", POPUP_PROBE, "lvw_probe_items");
	for(int intCommand = 0; intCommand <= riedelHelpers::offset::EXTRAS_PROBE_COMMAND_LAST - riedelHelpers::offset::EXTRAS_PROBE_COMMAND_FIRST; intCommand++)
	{
		textPut("add", bncs_string("%1; ").arg(intCommand + 1), POPUP_PROBE, "lvw_probe_items");
	}
}

void comms_port_pti::init()
{
	bncs_config cfgCommsMappingTables = bncs_config("comms_mapping_tb");  //get the list of tables
	bncs_stringlist sltTables;

	m_sltConfListen.clear();
	m_sltIFBOutput.clear();
	m_sltMixerOutput.clear();

//	<comms_mapping_tb>
//		<table id="source_package_4w1_conference">
//		<item id="source_1"  value="conference_1" />
//		etc...	
//		<item id="source_120" value="conference_120" />
//	</table>
//		<table id="source_package_4w2_conference">
//		<item id="source_1" value="conference_121" />
//		etc...
//		

	while (cfgCommsMappingTables.isChildValid() )
	{
		sltTables << cfgCommsMappingTables.childAttr("id");  // add each table to a stringlist
		cfgCommsMappingTables.nextChild();                   // iterate      
	}
	
	// get the required size of the array
					
	for ( int i=0; i< sltTables.count(); i++)				//iterate our list of tables	
	{
		bncs_config cfgCommsMapping = bncs_config(bncs_string("comms_mapping_tb.%1").arg(sltTables[i]));
		while ( cfgCommsMapping.isChildValid())
		{
			bncs_stringlist sltValue = bncs_stringlist(cfgCommsMapping.childAttr("value"),'_');
			int intValue =  sltValue[1].toInt() +1;
			// get the largest conference number 
			m_intLargestConferenceVal = intValue > m_intLargestConferenceVal ? intValue : m_intLargestConferenceVal;
			cfgCommsMapping.nextChild();
		}
	}

//m_intLargestConferenceVal =350;
	// create new array
	bncs_string* newArr = new bncs_string[m_intLargestConferenceVal];   
	for ( int j=0; j< sltTables.count(); j++)
	{
		
		bncs_stringlist sltConferenceLevelList = bncs_stringlist(sltTables[j],'_');
		bncs_string strConferenceType = bncs_string("%1 %2").arg(sltConferenceLevelList[0]).arg(sltConferenceLevelList[1]);
		bncs_string strConferenceLevel = sltConferenceLevelList[2];
		bncs_string strTemp = strConferenceLevel.length() > 0 ? strConferenceLevel : m_strSpacer; 

		bncs_config cfgCommsMapping = bncs_config(bncs_string("comms_mapping_tb.%1").arg(sltTables[j]));
		while ( cfgCommsMapping.isChildValid())
		{
			bncs_stringlist sltValue = bncs_stringlist(cfgCommsMapping.childAttr("value"),'_');
			bncs_stringlist sltID = bncs_stringlist(cfgCommsMapping.childAttr("id"),'_');
			int intValue =  sltValue[1].toInt();
			bncs_string strPackageNumber = sltID[1];
			newArr[intValue] = bncs_string("%1 %2 %3")
				.arg(strConferenceType.upper())
				.arg(bncs_string(strPackageNumber),'0',3)
				.arg(strTemp);
			cfgCommsMapping.nextChild();
		} 
	}

	for (int l = 0; l < m_intLargestConferenceVal; l++)
	{
		m_strConferenceToSourcePackageLevel.push_back(newArr[l]);
	}
	delete[] newArr;
}

bncs_string comms_port_pti::getConferenceDetails( int intMyConference )
{
	
	if ( intMyConference <= m_intLargestConferenceVal)
	{
		if (m_strConferenceToSourcePackageLevel[intMyConference].length() > 0)
		{
			return m_strConferenceToSourcePackageLevel[intMyConference];
		}
		else
		{
			return "* UNKNOWN CONFERENCE *";
		}
	}		
	else
	{
		return "* UNKNOWN CONFERENCE *";
		
	}
	
}

void comms_port_pti::renderPTIIndication(bool blnControlEnable, bool blnPortsFree)
{

	
	if (blnControlEnable )
	{
		controlEnable(PANEL_MAIN, "pti");
		if (m_skin == SKIN_WITHLABEL)
		{
			controlEnable(PANEL_MAIN, "lblPTIMsg");
		}
	}
	else
	{
		controlDisable(PANEL_MAIN, "pti");
		if (m_skin == SKIN_WITHLABEL)
		{
			controlDisable(PANEL_MAIN, "lblPTIMsg");
		}
	}
	if (m_skin == SKIN_WITHLABEL)
	{
		if (blnPortsFree)
		{
			textPut("text", "", PANEL_MAIN, "lblPTIMsg");
			textPut("statesheet", "comms_pti_normal", PANEL_MAIN, "lblPTIMsg");
		}
		else
		{
			debug("HERE TOO");
			bncs_string strInputOutput;

			if (m_PortInputOutput == PORT_TYPE_INPUT)
			{
				strInputOutput = "Input";
			}
			else if (m_PortInputOutput == PORT_TYPE_OUTPUT)
			{
				strInputOutput = "Output";
			}
			textPut("text", bncs_string("%1 is already|in a conference").arg(strInputOutput), PANEL_MAIN, "lblPTIMsg");			
			textPut("statesheet", "comms_pti_confmember", PANEL_MAIN, "lblPTIMsg");
		}
	}
}

void comms_port_pti::changePanel()
{
	panelDestroy(PANEL_MAIN);
	panelShow(PANEL_MAIN,"skins/" + m_skin + ".bncs_ui");
	bncs_string strLabel = (m_strAlias.length() == 0) ? "PTI" : m_strAlias ;
	textPut("text",strLabel, PANEL_MAIN, "pti");
}

void comms_port_pti::showFaultCondition()
{
	if ( m_skin == SKIN_WITHLABEL)
	{
		debug("BAD DATA - wrong number of fields in revertive");
		textPut("text", "Driver Error|Bad Data", PANEL_MAIN, "lblPTIMsg");	
		textPut("statesheet", "comms_pti_alarm", PANEL_MAIN, "lblPTIMsg");	
		controlDisable(PANEL_MAIN, "pti");	
	}
}

void comms_port_pti::Pti(bncs_stringlist sltConfListen, bncs_stringlist sltIFBOutput, bncs_stringlist sltMixerOutput)
{
	if (sltConfListen.count() > 0
		&& sltIFBOutput.count() > 0
		&& sltMixerOutput.count() > 0)
	{


		//Conference Listen
		textPut("clear", "clear", POPUP_PTI, "lvw_conf");
		//if (r->sInfo().length() == 0)
		//{
		//	renderPTIIndication(m_Port.valid, true);
		//}
		//else
		if (sltConfListen.count() == 1 && sltConfListen[0] == "0")
		{
			textPut("add", "<none>", POPUP_PTI, "lvw_conf");
			renderPTIIndication(m_Port.valid, true);
		}
		else
		{
			for (int intItem = 0; intItem < sltConfListen.count(); intItem++)
			{
				int intUsage = sltConfListen[intItem].toInt();
				bncs_string strName;
				routerName(m_Port.devices.Conference, riedelHelpers::databases::Input_Port_Button_Name, intUsage, strName);
				textPut("add", bncs_string("%1;%2").arg(intUsage, '0', 4).arg(strName), POPUP_PTI, "lvw_conf");
				infoPoll(m_Port.devices.Conference, riedelHelpers::offset::CONF_LABEL + intUsage, riedelHelpers::offset::CONF_LABEL + intUsage);
			}
			renderPTIIndication(m_Port.valid, false);
		}

		//IFB Output
		textPut("clear", "clear", POPUP_PTI, "lvw_ifb_output");
		if (sltIFBOutput.count() == 1 && sltIFBOutput[0] == "0")
		{
			textPut("add", "<none>", POPUP_PTI, "lvw_ifb_output");
		}
		else
		{
			//Something like
			//11:34,10:42
			for (int intItem = 0; intItem < sltIFBOutput.count(); intItem++)
			{
				bncs_string ring, ifb;
				sltIFBOutput[intItem].split(':', ring, ifb);

				if (m_Port.ring_id == ring)
				{
					int intUsage = ifb.toInt();
					bncs_string strName;
					routerName(m_Port.devices.Ifb, riedelHelpers::databases::Input_Port_Button_Name, intUsage, strName);
					if (strName == "---")
					{
						strName = bncs_string("IFB %1").arg(intUsage);
					}
					textPut("add", bncs_string("%1;%2").arg(intUsage, '0', 4).arg(strName), POPUP_PTI, "lvw_ifb_output");
					infoPoll(rh->GetRingmasterDevices().Ifbs, intUsage, intUsage);
				}
			}
		}

		//Mixer Output
		textPut("clear", "clear", POPUP_PTI, "lvw_mixer_output");
		if (sltMixerOutput.count() == 1 && sltMixerOutput[0] == "0")
		{
			textPut("add", "<none>", POPUP_PTI, "lvw_mixer_output");
		}
		else
		{
			for (int intItem = 0; intItem < sltMixerOutput.count(); intItem++)
			{
				int intUsage = sltMixerOutput[intItem].toInt();
				bncs_string strName;
				strName = bncs_string("Mixer %1").arg(intUsage);	//TODO get mixer name
				textPut("add", bncs_string("%1;%2").arg(intUsage, '0', 2).arg(strName), POPUP_PTI, "lvw_mixer_output");
			}
		}
	}
	else
	{
		showFaultCondition();
	}
}