#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "comms_ident_relay.h"

#define PNL_MAIN	"comms_ident_relay"

#define DESTINATION_DATABASE	7

#define TIMER_SETUP	1

#define RELAY_BUTTON	"btn_relay"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_ident_relay )

// constructor - equivalent to ApplCore STARTUP
comms_ident_relay::comms_ident_relay( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "main.bncs_ui" );

	m_instanceGPI = "";
	m_instance = "";
	m_btnText = "Line|Up";

	m_intCurrentOutput = 0;
	m_intCurrentOutput = 0;
	m_intCurrentDest = 0;
	m_intPreDefinedDest = 0;
	m_intCurrentDeviceGPI = 0;
	m_intPreDefinedDeviceGPI = 0;
	m_intDeviceGrd = 0;
	m_intPolledDevice = 0;
	m_intPolledSlot = 0;
	m_intPolledState = -1;

	m_blnUseStaticValues = false;

	textPut("text", m_btnText, PNL_MAIN, RELAY_BUTTON);

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_ident_relay::~comms_ident_relay()
{
}

// all button pushes and notifications come here
void comms_ident_relay::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if (b->id() == RELAY_BUTTON)
		{
			if ( (m_intPolledDevice > 0) && (m_intPolledSlot > 0) )
			{
				if (m_intPolledState == 0)
				{
					infoWrite(m_intPolledDevice, 1, m_intPolledSlot);
				} 
				else
				{
					infoWrite(m_intPolledDevice, 0, m_intPolledSlot);
				}
			}
		}
	}
}

// all revertives come here
int comms_ident_relay::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_intPolledDevice)
	{
		if (r->index() == m_intPolledSlot)
		{
			m_intPolledState = r->sInfo().toInt();

			if (m_intPolledState == 0)
			{
				textPut( "stylesheet=enum_normal", PNL_MAIN, RELAY_BUTTON );
			} 
			else
			{
				textPut( "stylesheet=enum_notnormal", PNL_MAIN, RELAY_BUTTON );
			}
		}
	}
	return 0;
}

// all database name changes come back here
void comms_ident_relay::databaseCallback( revertiveNotify * r )
{
}



// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_ident_relay::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "Use_Static_Values=%1" ).arg( ((m_blnUseStaticValues==true)?"true":"false") );
			sl << bncs_string( "GPI_Instance=%1" ).arg( m_instanceGPI );
			sl << bncs_string( "GPI_Output=%1" ).arg( m_intPreDefinedDest );
			sl << bncs_string( "button_text=%1" ).arg( m_btnText );
			
			return sl.toString( '\n' );
		}

		else if( p->value() == "m_instanceGPI" )
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_instanceGPI ) );
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{	// Our instance is being set/changed
		m_instance = p->value();
		bncs_string instanceGrd;
		instanceLookupComposite(m_instance, "lock", instanceGrd);
		getDev( instanceGrd, &m_intDeviceGrd );
	}

	else if (p->command() == "Use_Static_Values")
	{
		m_blnUseStaticValues = ((p->value()=="true")?true:false);
	}

	else if( p->command() == "output" )
	{
		textPut( "stylesheet=enum_deselected", PNL_MAIN, RELAY_BUTTON );

		m_intCurrentOutput = p->value().toInt();

		if (m_intCurrentOutput > -1)
		{
			controlEnable(PNL_MAIN, RELAY_BUTTON);
			bncs_string strName = "", strCurrentInstanceGPI = "", strCurrentIndex = "";
			routerName(m_intDeviceGrd, DESTINATION_DATABASE, m_intCurrentOutput, strName);

			strName.split(',', strCurrentInstanceGPI, strCurrentIndex);
			m_intCurrentDest = strCurrentIndex.toInt();
			getDev(strCurrentInstanceGPI, &m_intCurrentDeviceGPI);
			CheckSettings();
			debug("comms_ident_relay::parentCallback GPI Instance: %1 (dev: %2), GPI Dest: %3", strCurrentInstanceGPI, m_intCurrentDeviceGPI, m_intCurrentDest);
			pollGPI();
		}
		else
		{
			controlDisable(PNL_MAIN, RELAY_BUTTON);
		}
	}

	else if( p->command() == "GPI_Instance" )
	{	// Persisted value or 'Command' being set here
		m_instanceGPI = p->value();
		getDev( m_instanceGPI, &m_intPreDefinedDeviceGPI );
		pollGPI();
	}

	else if( p->command() == "GPI_Output" )
	{	// Persisted value or 'Command' being set here
		m_intPreDefinedDest = p->value().toInt();
		pollGPI();
	}

	else if (p->command() == "button_text")
	{
		m_btnText = p->value();
		textPut("text", m_btnText, PNL_MAIN, RELAY_BUTTON);
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "output=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void comms_ident_relay::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void comms_ident_relay::pollGPI()
{
	CheckSettings();
	infoUnregister(m_intPolledDevice);
	m_intPolledDevice = 0;
	m_intPolledSlot = 0;

	if (m_blnUseStaticValues == true)
	{
		if ( (m_intPreDefinedDeviceGPI > 0) && (m_intPreDefinedDest > 0)  )
		{
			m_intPolledDevice = m_intPreDefinedDeviceGPI;
			m_intPolledSlot = m_intPreDefinedDest;
		}
	} 
	else
	{
		if ( (m_intCurrentDeviceGPI > 0) && (m_intCurrentDest > 0)  )
		{
			m_intPolledDevice = m_intCurrentDeviceGPI;
			m_intPolledSlot = m_intCurrentDest;
		}
	}

	if ( (m_intPolledDevice > 0) && (m_intPolledSlot > 0) )
	{
		infoRegister(m_intPolledDevice, m_intPolledSlot, m_intPolledSlot);
		infoPoll(m_intPolledDevice, m_intPolledSlot, m_intPolledSlot);
	}
}
void comms_ident_relay::CheckSettings()
{
	if (m_blnUseStaticValues)
	{
		controlEnable(PNL_MAIN, RELAY_BUTTON);
	}
	else
	{
		if (m_intCurrentDeviceGPI && m_intCurrentDest)
		{
			controlEnable(PNL_MAIN, RELAY_BUTTON);
		}
		else
		{
			controlDisable(PNL_MAIN, RELAY_BUTTON);
		}
	}
}