#ifndef comms_ident_relay_INCLUDED
	#define comms_ident_relay_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_ident_relay : public bncs_script_helper
{
public:
	comms_ident_relay( bncs_client_callback * parent, const char* path );
	virtual ~comms_ident_relay();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	void CheckSettings();
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void pollGPI();
	
private:
	bncs_string m_instanceGPI;
	bncs_string m_instance;
	bncs_string m_btnText;

	int m_intCurrentOutput;
	int m_intCurrentDest;
	int m_intPreDefinedDest;
	int m_intCurrentDeviceGPI;
	int m_intPreDefinedDeviceGPI;
	int m_intDeviceGrd;
	
	int m_intPolledDevice;
	int m_intPolledSlot;
	int m_intPolledState;

	bool m_blnUseStaticValues;
};


#endif // comms_ident_relay_INCLUDED