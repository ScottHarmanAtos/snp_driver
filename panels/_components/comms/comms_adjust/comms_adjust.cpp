#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_adjust.h"
#include "packager_common.h"

#define PANEL_MAIN   "comms_adjust"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_adjust )

// constructor - equivalent to ApplCore STARTUP
comms_adjust::comms_adjust( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	m_blnAllowedToExecute=true;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow(PANEL_MAIN, "main.bncs_ui" );
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_adjust::~comms_adjust()
{
}

// all button pushes and notifications come here
void comms_adjust::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if (b->id() == "button")
		{
			bncs_string strExecute = bncs_string("comms/assign_keys,%1:%2").arg(m_panelPort.sourceName_short.replace('|', ' ')).arg(m_panelPort.ring_port);
			if ( m_blnAllowedToExecute)
			{
				navigateExecute(strExecute);
			}
		}
	}
}

// all revertives come here
int comms_adjust::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void comms_adjust::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_adjust::parentCallback( parentNotify *p )
{
	if ( p->command() == "port")
	{
		controlDisable(PANEL_MAIN, "button");

		m_panelPort = rh->GetPort(p->value());

		if (m_panelPort.valid)
		{
			m_blnAllowedToExecute = rh->CanAccessPanel(m_panelPort.ring_id, m_panelPort.port);

			textPut("text", "", PANEL_MAIN, "msgbox");
			bncs_string strPortType, strAssignableKeys, strPortDetails;
			routerName(m_panelPort.devices.Grd, riedelHelpers::databases::Port_Type, m_panelPort.slot_index, strPortType);
			routerName(m_panelPort.devices.Grd, riedelHelpers::databases::Assignable_Keys, m_panelPort.slot_index, strAssignableKeys);
			
			//0001=sysid=,type=DCP2116,select_default=0
			routerName (m_panelPort.devices.Grd, riedelHelpers::databases::Port_Details, m_panelPort.slot_index, strPortDetails);
			bncs_stringlist sltPortDetails = bncs_stringlist(strPortDetails);
			bncs_stringlist sltPanelType = bncs_stringlist(sltPortDetails[1], '=');
			debug("adjust_assignable_key::parentcallback port=%1 allowed to Execute = %2 type=%3 assignablekeyspresent=%4", m_panelPort.ring_port, m_blnAllowedToExecute, sltPanelType[1], isAssignableKeyPresent(strAssignableKeys));
			
			if ( m_blnAllowedToExecute )
			{
				if ( strPortType.toInt() == PORT_TYPE_PANEL || strPortType.toInt() == PORT_TYPE_PANEL_DUAL_CHANNEL)
				{
					if ( isAssignableKeyPresent(strAssignableKeys) )
					{
						controlEnable(PANEL_MAIN, "button");
					}
					else
					{
						textPut("text", "No assignable|keys on panel", PANEL_MAIN, "msgbox");
					}
				}
				else
				{
					textPut("text", "Port is|not a panel", PANEL_MAIN, "msgbox");
				}
			}
			else
			{
				textPut("text", "No instance defined", PANEL_MAIN, "msgbox");
				
			}
			
		}
		else
		{
			textPut("text", "", PANEL_MAIN, "msgbox");
		}
	
	}
	return "";
}

// timer events come here
void comms_adjust::timerCallback( int id )
{
}

bool comms_adjust::isAssignableKeyPresent( bncs_string keylist )
{
	bool flag= false;
	bncs_stringlist sltKeyList(keylist);
	for (int i=0; i<sltKeyList.count();i++)
	{
		if (sltKeyList[i].length()>0)
		{
			flag = true;
			break;
		}
	}
	return  flag;
}
