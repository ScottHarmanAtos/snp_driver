#ifndef comms_adjust_INCLUDED
	#define comms_adjust_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_adjust : public bncs_script_helper
{
public:

	riedelHelper * rh;

	comms_adjust( bncs_client_callback * parent, const char* path );
	virtual ~comms_adjust();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	bool isAssignableKeyPresent( bncs_string keylist );
	
private:
	bool m_blnAllowedToExecute;
	int m_intIndex;
	comms_ringmaster_port m_panelPort;
	
	int m_riedelRouter;
};


#endif // comms_adjust_INCLUDED