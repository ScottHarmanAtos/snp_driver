#ifndef comms_function_slot_grid_INCLUDED
	#define comms_function_slot_grid_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_function_slot_grid : public bncs_script_helper
{
public:
	comms_function_slot_grid( bncs_client_callback * parent, const char* path );
	virtual ~comms_function_slot_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	riedelHelper * rh;
	bncs_string m_function;

	int m_page;
	int m_offset;

	int m_names_db;
	int m_names_device;
	int m_alias_device;

	bncs_string m_selected_btn;

	bncs_stringlist m_mapping;

	int m_mapping_device;

	void buildGrid();
	void resetAll();

	bool isPort(bncs_string value);
	void toggleBtn(bncs_string element, bool select = true);

	static const bncs_string layout_main;
	static const bncs_string attr_text;


};


#endif // comms_function_slot_grid_INCLUDED