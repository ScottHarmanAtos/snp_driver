# comms_function_slot_grid

## Commands

### ringport
When passed the `ring.port`, the ring will be used to decide which ring's DB8 to look for.

### device
The device which should be used to look for the names on the grid buttons

### namesdb
The db to use for the names on the button.

### page
The page of mapping to use starting at `1`.

### offset
The offset for the mapping file in DB8 of the GRD in the active ring.

### function
The function to use e.g. `CALLTOPORT` or `IFB`.

## Notifications

### command
The command with the function and index that have been selected e.g. `CALLTOPORT|1`.
