#include <VECTOR>
#include <map>

#ifndef comms_edit_conference_INCLUDED
	#define comms_edit_conference_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_edit_conference : public bncs_script_helper
{
public:
	comms_edit_conference( bncs_client_callback * parent, const char* path );
	virtual ~comms_edit_conference();
	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper *rh;
	int m_intDeviceConf;
	bncs_string m_strInstanceGrd;
	int m_intConference;
	bool m_blnReadOnly;
	bncs_string m_strInstanceConference;
	comms_ringmaster_port m_port;
	bncs_string m_strConfPortMembers;
	int m_intDevicePTI;
	int m_intDeviceLock;
	bncs_string m_strInstanceComposite;
	void clearPortMemberList();
	void clearPanelMemberList();
	void clearMember(int portMember);

	void dropConferencePort();
	void manageConference(int intIndex);
	void modifyMembers(bncs_string strPermission);
	void updateConfPanelMembers();
	void updateConfPortMembers();
	void updatePort(comms_ringmaster_port port);
	void updateConfLabel(bncs_string strLabel);
	bncs_string m_strConfPanelMembers;

	void dropConferencePanelMember(int portMember);
	void disableEditControls();
	void editLabel();
	void editSubtitle();
	void updateConfSubtitle(bncs_string strSubtitle);

	void pollLock(comms_ringmaster_port portNumber);
	std::vector < map<int, int> > lockPorts;
	std::vector <int> m_vecRegisteredGPILocks;
	int m_intDeviceGpiLock;

};


#endif // comms_edit_conference_INCLUDED