#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "comms_edit_conference.h"

#define PANEL_MAIN					1
#define POPUP_EDIT_LABEL			2
#define POPUP_CONFIRM_PARK			3
#define POPUP_EDIT_SUBTILE			4

#define TIMER_INIT   1
#define TIMEOUT_INIT   10

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_edit_conference )

// constructor - equivalent to ApplCore STARTUP
comms_edit_conference::comms_edit_conference( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	auto master = rh->GetRingmasterDevices();
	m_intDeviceConf = master.Conferences;

	m_blnReadOnly = true;
	
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );

	disableEditControls();

}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_edit_conference::~comms_edit_conference()
{
}



// all button pushes and notifications come here
void comms_edit_conference::buttonCallback( buttonNotify *b )
{
//	b->dump("dump");
	if( b->panel() == PANEL_MAIN )
	{
		if(b->id() == "comms_port_grid" && b->command() == "index")
		{
			updatePort(rh->GetPort(b->value()));
		}
		else if(b->id() == "talklisten")
		{
			modifyMembers("TLV");
		}
		else if(b->id() == "talk")
		{
			modifyMembers("TV");
		}
		else if(b->id() == "listen")
		{
			modifyMembers("L");
		}
		else if(b->id() == "portmembers" && b->command() == "selection")
		{
			controlEnable(PANEL_MAIN, "drop_port");
		}
		else if( b->id() == "drop_port")
		{
			controlDisable(PANEL_MAIN, "drop_port");
			dropConferencePort();
		}
		else if(b->id() == "clear_ports")
		{
			controlDisable(PANEL_MAIN, "clear_ports");
			controlDisable(PANEL_MAIN, "drop_port");
			clearPortMemberList();
		}
		else if (b->id() == "panelmembers" && b->command() == "selection")
		{
			controlEnable(PANEL_MAIN, "drop_panel");
	//		debug("b->value()=%1", b->value());
		}
		else if (b->id() == "drop_panel")
		{
			controlDisable(PANEL_MAIN, "drop_panel");
			bncs_string ret;
			textGet("selected", PANEL_MAIN, "panelmembers", ret);
			debug("ret=%1", ret);
			if ( ret.length() > 0)
			{
				bncs_stringlist slt = bncs_stringlist(ret, ';');
				dropConferencePanelMember(slt[0].toInt());
			}
		}
		else if (b->id() == "clear_panels")
		{
			controlDisable(PANEL_MAIN, "clear_panels");
			controlDisable(PANEL_MAIN, "drop_panel");
			clearPanelMemberList();
		}
		else if (b->id() == "edit_label")
		{
			editLabel();
		}
		else if (b->id() == "conference_label")
		{
			editLabel();
		}
		else if (b->id() == "reset_conference")
		{
			panelPopup(POPUP_CONFIRM_PARK, "popup_confirm_park.bncs_ui");
		}
		else if (b->id() == "edit_subtitle")
		{
			editSubtitle();
		}
	}
	else if(b->panel() == POPUP_EDIT_LABEL)
	{
		if(b->id() == "close")
		{
			panelRemove(POPUP_EDIT_LABEL);
		}
		else if(b->id() == "keyboard" && b->command() == "button" && b->value() == "released")
		{
			bncs_string strKeyboard;
			textGet("text", POPUP_EDIT_LABEL, "keyboard", strKeyboard);
			panelRemove(POPUP_EDIT_LABEL);
			updateConfLabel(strKeyboard.left(8));//Trim conference label to 8 characters
		}
	}
	else if (b->panel() == POPUP_EDIT_SUBTILE)
	{
		if (b->id() == "close")
		{
			panelRemove(POPUP_EDIT_SUBTILE);
		}
		else if (b->id() == "keyboard" && b->command() == "button" && b->value() == "released")
		{
			bncs_string strKeyboard;
			textGet("text", POPUP_EDIT_SUBTILE, "keyboard", strKeyboard);
			panelRemove(POPUP_EDIT_SUBTILE);
			updateConfSubtitle(strKeyboard);
		}
	}
	else if (b->panel() == POPUP_CONFIRM_PARK)
	{
		if (b->id() == "confirm")
		{
			//send label first as it will not be refreshed until conf members change

			bncs_string  strName;
			routerName(m_intDeviceConf, riedelHelpers::databases::Conference_Name, m_intConference, strName);
			debug("def conf name = %1", strName);
			if (strName.length() == 0 || strName == "!!!")
				strName = bncs_string("Conf %1").arg(m_intConference);
			infoWrite(m_intDeviceConf, strName, m_intConference + riedelHelpers::offset::CONF_LABEL);
			infoWrite(m_intDeviceConf, "&REMOVE=EVERYTHING", m_intConference);
			panelDestroy(b->panel());

		}
		else
		{
			panelDestroy(b->panel());
		}
	}
}

// all revertives come here
int comms_edit_conference::revertiveCallback( revertiveNotify * r )
{
	r->dump("aux_conferences");
	if( r->device() == m_intDeviceConf )
	{
		if( r->index() == m_intConference)
		{
			m_strConfPortMembers = r->sInfo();
			updateConfPortMembers();
		}
		else if( r->index() == m_intConference + riedelHelpers::offset::CONF_LABEL)
		{
			textPut( "text", r->sInfo(), PANEL_MAIN, "conference_label" );
		}
		else if( r->index() == m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS)
		{
			m_strConfPanelMembers = r->sInfo();
			updateConfPanelMembers();
		}
	}
	for (int i = 0; i < (int)lockPorts.size(); i++)
	{
		map<int, int> tempMap;
		tempMap[r->device()] = r->index();
		if (tempMap == lockPorts[i])
		{
			bncs_string strSlot = r->sInfo();
			bncs_string lockStr = "";
			if (strSlot.stripWhiteSpace().length() > 0 && strSlot != "0")
			{
				lockStr = "red_dot_25";
			}
			bncs_string lockCmd = bncs_string("cellPixmap.%1.3").arg(i);
			textPut(lockCmd, lockStr, PANEL_MAIN, "portmembers");
		}
	}
	return 0;
}

// all database name changes come back here
void comms_edit_conference::databaseCallback( revertiveNotify * r )
{
	if (riedelHelpers::databases::Subtitle_Input == r->database() && r->device() == m_intDeviceConf && r->index() == m_intConference)
	{
		textPut("text", r->sInfo(), PANEL_MAIN, "subtitle_label");
	}
	else if (riedelHelpers::databases::Conference_Name == r->database() && r->device() == m_intDeviceConf && r->index() == m_intConference)
	{
		textPut("text", r->sInfo(), PANEL_MAIN, "conference");
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_edit_conference::parentCallback( parentNotify *p )
{
	debug("conference_summary::parentCallback() cmd=%1 value=%2", p->command(), p->value());
	if(p->command() == "instance")
	{
//		m_strInstanceGrd = p->value();
//		getDev(m_strInstanceGrd, &m_intDeviceConf);
	}
	else if(p->command() == "conference")
	{
//		m_intConference = p->value().toInt();
//		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if(p->command() == "read_only")
	{
//		m_blnReadOnly = p->value().lower() == "true" || p->value() == "1";
//		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if ( p->command() == "raw_parameters")
	{
		m_intConference = p->value().toInt();
		manageConference(m_intConference);
//		init();
	}
	else if (p->command() == "embedded")
	{
		panelDestroy(PANEL_MAIN);
		panelShow(PANEL_MAIN, "embedded.bncs_ui");
		disableEditControls();
		m_intConference = p->value().toInt();
		manageConference(m_intConference);
	}
	else if(p->command() == "return")
	{
//		bncs_stringlist slt;
//		slt << bncs_string("conference=%1").arg(m_intConference);
//		slt << bncs_string("read_only=%1").arg(m_blnReadOnly?"true":"false");
		return "";//slt.toString('\n');
		
	}
	return "";
}

// timer events come here
void comms_edit_conference::timerCallback( int id )
{
	if (id == TIMER_INIT)
	{
//		init();
		timerStop(TIMER_INIT);
	}
}



void comms_edit_conference::manageConference( int intIndex )
{
	controlDisable(PANEL_MAIN, "talklisten");
	controlDisable(PANEL_MAIN, "talk");
	controlDisable(PANEL_MAIN, "listen");
	
	if (m_intConference >0)
	{
		bncs_stringlist sltControlList= getIdList(PANEL_MAIN);
		for (int i=0; i<sltControlList.count();i++)
		{
			controlEnable(PANEL_MAIN, sltControlList[i]);
		}
		bncs_string strName;
		routerName(m_intDeviceConf, riedelHelpers::databases::Conference_Name, m_intConference, strName);
		textPut("text", strName, PANEL_MAIN, "conference");
		textPut("text", "", PANEL_MAIN, "name");
		textPut("text", "0", PANEL_MAIN, "port_count");
		textPut("text", "0", PANEL_MAIN, "panel_count");
		
		infoRegister(m_intDeviceConf, m_intConference , m_intConference );
		infoRegister(m_intDeviceConf, m_intConference + riedelHelpers::offset::CONF_LABEL, m_intConference + riedelHelpers::offset::CONF_LABEL, true);
		infoRegister(m_intDeviceConf, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS, true);
		infoPoll(m_intDeviceConf, m_intConference , m_intConference );
		infoPoll(m_intDeviceConf, m_intConference + riedelHelpers::offset::CONF_LABEL, m_intConference + riedelHelpers::offset::CONF_LABEL);
		infoPoll(m_intDeviceConf, m_intConference + riedelHelpers::offset::CONF_LABEL, m_intConference + riedelHelpers::offset::CONF_LABEL);
		bncs_string subtitle;
		routerName(m_intDeviceConf, riedelHelpers::databases::Subtitle_Input, m_intConference, subtitle);
		textPut("text", subtitle, PANEL_MAIN, "subtitle_label");


		bncs_string talkListenMode = getObjectSetting("comms_edit_conferences", "auto_conference_assign_mode");
		if ((talkListenMode.find("TV") == -1) && (talkListenMode.find("TL") == -1))
		{
			talkListenMode = "TL";
		}


		textPut("function", bncs_string("CONFERENCE|%1|%2").arg(m_intConference).arg(talkListenMode), PANEL_MAIN, "monitor");
	}
	else
	{
		textPut("text", "", PANEL_MAIN, "conference");
		textPut("text", "", PANEL_MAIN, "name");
		textPut("text", "0", PANEL_MAIN, "port_count");
		textPut("text", "0", PANEL_MAIN, "panel_count");
		bncs_stringlist sltControlList= getIdList(PANEL_MAIN);
		for (int i=0; i<sltControlList.count();i++)
		{
			controlDisable(PANEL_MAIN, sltControlList[i]);
		}
	}
	if ( m_blnReadOnly)
	{
		controlDisable(PANEL_MAIN, "conference");
	}
	else
	{
		controlEnable(PANEL_MAIN, "conference");
	}
	debug("aux_conferences::manageConference() index=%1", intIndex);
	m_intConference = intIndex;
	textPut("text", m_intConference, PANEL_MAIN, "conference_index");
	
	bncs_string strName;
	routerName(m_intDeviceConf, riedelHelpers::databases::Conference_Name, m_intConference, strName);
	textPut("text", strName, PANEL_MAIN, "default_name");
	textPut("text", "", PANEL_MAIN, "name");
	textPut("text", "0", PANEL_MAIN, "port_count");
	textPut("text", "0", PANEL_MAIN, "panel_count");
	
	controlDisable(PANEL_MAIN, "listen");
	controlDisable(PANEL_MAIN, "talk");
	controlDisable(PANEL_MAIN, "talklisten");
	
	infoRegister(m_intDeviceConf, m_intConference , m_intConference);
	infoRegister(m_intDeviceConf, m_intConference + riedelHelpers::offset::CONF_LABEL, m_intConference + riedelHelpers::offset::CONF_LABEL, true);
	infoRegister(m_intDeviceConf, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS, true);
	infoPoll(m_intDeviceConf, m_intConference , m_intConference);
	infoPoll(m_intDeviceConf, m_intConference + riedelHelpers::offset::CONF_LABEL, m_intConference + riedelHelpers::offset::CONF_LABEL);
	infoPoll(m_intDeviceConf, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS);
	
}

void comms_edit_conference::updatePort(comms_ringmaster_port port)
{
	m_port = port;
	//textPut("text", bncs_string("port=%1").arg(intPort), PNL_MAIN, "grpConference");
	
	if(m_port.valid)
	{
		//Get port type

		debug("updatePort.  port =%1 type =%2", m_port.ring_port, m_port.portType);
	
		if(m_port.portType == PORT_TYPE_SPLIT)
		{
			//Check if port is a monitor
			int intMonitorPort = routerIndex(m_port.devices.Monitor, riedelHelpers::databases::Port_Type, m_port.port);
			if(intMonitorPort > 0)
			{
				controlDisable(PANEL_MAIN, "listen");
			}
			else
			{
				controlEnable(PANEL_MAIN, "listen");
			}
			controlEnable(PANEL_MAIN, "talk");
			controlDisable(PANEL_MAIN, "talklisten");
			
		}
		else if(m_port.portType == PORT_TYPE_4WIRE)
		{
			controlEnable(PANEL_MAIN, "talk");
			controlEnable(PANEL_MAIN, "listen");
			controlEnable(PANEL_MAIN, "talklisten");
		}
	}
}

void comms_edit_conference::modifyMembers(bncs_string strPermission)
{
	bncs_stringlist sltMembers(m_strConfPortMembers);
	bncs_stringlist sltUpdatedMembers = sltMembers;
	
	debug("aux_conferences::modifyMembers members=%1", sltMembers.toString());
	
	bool blnExistingMember = false;
	bool blnSplitAdditionalPermission = false;
	for(int intMember = 0; intMember < sltMembers.count(); intMember++)
	{
		bncs_stringlist sltMemberDetails(sltMembers[intMember], '|');
		auto memberPort = rh->GetPort(sltMemberDetails[0]);
		
		if(m_port.ring_port == memberPort.ring_port)
		{
			blnExistingMember = true;
			//This member is being modified
			
			bncs_string strCurrentPermission = sltMemberDetails[1];
			
			if(strPermission == strCurrentPermission)
			{
				//No Action required
			}
			else
			{
				//Check if the port is a split port and the other permission is being used
;
				debug("manage_conference::intPortType =%1", m_port.portType);
				if (m_port.portType == PORT_TYPE_SPLIT)
				{
					//A split port that is a member is being added with a different permission
					blnSplitAdditionalPermission = true;
					debug("manage_conference::setting blnSplitAdditionalPermission =%1", blnSplitAdditionalPermission);
				}
				else
				{
					//Replace current permission
					sltUpdatedMembers[intMember] = bncs_string("%1|%2|0").arg(m_port.ring_port).arg(strPermission);
					debug("manage_conference::replacing existing permission=%1", blnSplitAdditionalPermission);
				}
			}
		}
	}
	
	if(!blnExistingMember)
	{
		//append this port
		sltUpdatedMembers.append(bncs_string("%1|%2|0").arg(m_port.ring_port).arg(strPermission));
	}
	
	if(blnSplitAdditionalPermission)
	{
		//append this port
		sltUpdatedMembers.append(bncs_string("%1|%2|0").arg(m_port.ring_port).arg(strPermission));
	}
	debug("manage_conference::modifyMembers updatedmembers=%1", sltUpdatedMembers.toString());
	
	//textPut("text", sltUpdatedMembers.toString(' '), PNL_MAIN, "grpConference");	
	infoWrite(m_intDeviceConf, sltUpdatedMembers.toString(), m_intConference);
}

void comms_edit_conference::dropConferencePort()
{

	bncs_stringlist sltUpdatedMembers;
	
	bncs_stringlist sltMembers(m_strConfPortMembers);
	
	bncs_string selected;
	textGet("selectedindex", PANEL_MAIN, "portmembers", selected);
	int intSelectedRow = selected.toInt();
	
	if(intSelectedRow > -1)
	{
		bncs_string strReturn;
		textGet(bncs_string("row.%1").arg(intSelectedRow), PANEL_MAIN, "portmembers", strReturn);
		bncs_stringlist sltColumns(strReturn, ';');
		bncs_string ringPortSelectedPort = sltColumns[0];
		bncs_string strSelectedPortTalkListen = sltColumns[2];
		
		if(ringPortSelectedPort.length() > 0)
		{
			for(int intMember = 0; intMember < sltMembers.count(); intMember++)
			{
				bncs_stringlist sltMemberDetails(sltMembers[intMember], '|');
				bncs_string ringPortMemberPort = sltMemberDetails[0];
				bncs_string strMemberTalkListen = sltMemberDetails[1];
				strMemberTalkListen.replace("TLV", "T+L V");
				strMemberTalkListen.replace("TL", "T+L");
				
				if(ringPortSelectedPort == ringPortMemberPort && strSelectedPortTalkListen == strMemberTalkListen)
				{
					//This member is being dropped
				}
				else
				{
					sltUpdatedMembers.append(sltMembers[intMember]);
				}
			}
			infoWrite(m_intDeviceConf, sltUpdatedMembers.toString(), m_intConference);
		}
	}
}

void comms_edit_conference::clearPortMemberList()
{

	debug("aux_conferences::clearPortMemberList");
	infoWrite(m_intDeviceConf, "", m_intConference);
}
void comms_edit_conference::clearPanelMemberList()
{
	debug("aux_conferences::clearPanelMemberList");
	infoWrite(m_intDevicePTI, "", m_intConference);
}

void comms_edit_conference::clearMember(int portMember)
{
	debug("aux_conferences::clearMember %1", portMember);
	infoWrite(m_intDevicePTI, bncs_string("&REMOVE=%1").arg(portMember), m_intConference);
}

void comms_edit_conference::updateConfLabel(bncs_string strLabel)
{
	debug("aux_conferences::updateConfLabel() new label=%1", strLabel);
	infoWrite(m_intDeviceConf, strLabel, m_intConference + riedelHelpers::offset::CONF_LABEL);
	infoWrite(m_intDeviceConf, "&RESEND", m_intConference);
}

void comms_edit_conference::updateConfPanelMembers()
{
	textPut("clear", "clear", PANEL_MAIN, "panelmembers");
	
	bncs_stringlist sltPanelMembers(m_strConfPanelMembers);
	for(int intMember = 0; intMember < sltPanelMembers.count(); intMember++)
	{
		auto port = rh->GetPort(sltPanelMembers[intMember]);
		
		textPut("add", bncs_string("%1;%2").arg(port.ring_port, '0', 3).arg(port.sourceName_short.replace('|',' ')), PANEL_MAIN, "panelmembers");
	}
}

void comms_edit_conference::updateConfPortMembers()
{
	debug("aux_conferences::updateConfPortMembers() members=%1", m_strConfPortMembers);
	int intSourcePackage = m_intConference % 100;
	
	textPut("clear", "clear", PANEL_MAIN, "portmembers");
	bncs_string strPortName, strPortType, strRow;
	
	bncs_stringlist sltPortMembers(m_strConfPortMembers);
	int intMemberCount = sltPortMembers.count();
	
	if(intMemberCount > 0)
	{
		controlEnable(PANEL_MAIN, "clear_ports");
	}
	else
	{
		controlDisable(PANEL_MAIN, "clear_ports");
	}
	controlDisable(PANEL_MAIN, "drop_port");
	
	//	debug("package_editor::updateSourceConfPortMembers() conf=%1 sp=%2 level=%3 members[%4]='%5'", 
	//		intConference, intSourcePackage, intLevel, intMemberCount, m_sltTBConfPortMembers[intConference] );
	
	//Unregister and clear the old list of lock ports
	for (int i = 0; i <  (int)m_vecRegisteredGPILocks.size(); i++)
	{
		infoUnregister(m_vecRegisteredGPILocks[i]);
	}
	m_vecRegisteredGPILocks.clear();
	lockPorts.clear();

	for(int intMember = 0; intMember < intMemberCount; intMember++)
	{
		bncs_stringlist sltMemberFields = bncs_stringlist().fromString(sltPortMembers[intMember], '|');
		//53|TL|0,54|TL|0,55|TL|0,56|TL|0
		auto port = rh->GetPort(sltMemberFields[0]);
		
		bncs_string strTalkListen = sltMemberFields[1];
		
		strTalkListen.replace("TLV", "T+L V");
		strTalkListen.replace("TL", "T+L");
		
		switch(port.portType)
		{
		case PORT_TYPE_NONE:
		case PORT_TYPE_PANEL:
		case PORT_TYPE_PANEL_DUAL_CHANNEL:
			strPortName = "";
			break;
		case PORT_TYPE_SPLIT:
			if(strTalkListen == "L")
			{
				strPortName = port.destName_short;
			}
			else
			{
				strPortName = port.sourceName_short;
			}
			break;
		case PORT_TYPE_4WIRE:
			strPortName = port.sourceName_short;
			break;
		}
		if(strPortName.length() != 0)
		{
			strRow = bncs_string("%1;%2;%3").arg(port.ring_port).arg(strPortName).arg(strTalkListen);
			textPut("add", strRow.replace('|', ' '), PANEL_MAIN, "portmembers");

			//This is for the lock icon
			pollLock(port);
		}
	}
}



void comms_edit_conference::dropConferencePanelMember(int portMember)
{
	// not implemented yet
}

void comms_edit_conference::disableEditControls()
{
	controlDisable(PANEL_MAIN, "drop_port");
	controlDisable(PANEL_MAIN, "clear_ports");

	// not sure why this does not work at present
	controlDisable(PANEL_MAIN, "drop_panel");
	controlDisable(PANEL_MAIN, "clear_panels");

}

void comms_edit_conference::pollLock(comms_ringmaster_port port)
{
	map <int, int> tempMap;
	int iGpiIndex = 0, m_intDeviceGpiLock = 0;
	bncs_string strInstanceGPI = "", strIndex = "";

	bncs_string strName;
	routerName(port.devices.Lock, riedelHelpers::databases::Upstream_Details, port.port, strName);
	strName.split(',', strInstanceGPI, strIndex);
	iGpiIndex = strIndex.toInt();
	getDev(strInstanceGPI, &m_intDeviceGpiLock);

	if ((m_intDeviceGpiLock > 0) && (iGpiIndex > 0))
	{
		infoRegister(m_intDeviceGpiLock, iGpiIndex, iGpiIndex, true);
		infoPoll(m_intDeviceGpiLock, iGpiIndex, iGpiIndex);
		m_vecRegisteredGPILocks.push_back(m_intDeviceGpiLock);
	}
	tempMap[m_intDeviceGpiLock] = iGpiIndex;
	lockPorts.push_back(tempMap);
}

void comms_edit_conference::editLabel()
{
	bncs_string strLabel;
	textGet("text", PANEL_MAIN, "conference_label", strLabel);
	panelPopup(POPUP_EDIT_LABEL, "popup_keyboard.bncs_ui");
	textPut("text", strLabel, POPUP_EDIT_LABEL, "keyboard");
	textPut("setFocus", "setFocus", POPUP_EDIT_LABEL, "keyboard");
}

void comms_edit_conference::editSubtitle()
{
	bncs_string strSubtitle;
	textGet("text", PANEL_MAIN, "subtitle_label", strSubtitle);
	panelPopup(POPUP_EDIT_SUBTILE, "popup_keyboard.bncs_ui");
	textPut("text", strSubtitle, POPUP_EDIT_SUBTILE, "keyboard");
	textPut("setFocus", "setFocus", POPUP_EDIT_SUBTILE, "keyboard");
}

void comms_edit_conference::updateConfSubtitle(bncs_string strSubtitle)
{
	debug("aux_conferences::updateConfSubtitle() new label=%1", strSubtitle);
	routerModify(m_intDeviceConf, riedelHelpers::databases::Subtitle_Input, m_intConference, strSubtitle, false);
}