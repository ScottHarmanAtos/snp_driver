# comms_conference_grid

This component is a simply grid of the `comms_package_conference` component. When passed a page, it will populate the next 8 source packages.

## Commands

### page
The page the component should be on, starting with index `1`.

## Notifications

### command
The command with the selected conference number e.g. `CONFERENCE|1|TL`
