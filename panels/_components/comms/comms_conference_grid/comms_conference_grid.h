#ifndef comms_conference_grid_INCLUDED
	#define comms_conference_grid_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_conference_grid : public bncs_script_helper
{
public:
	comms_conference_grid( bncs_client_callback * parent, const char* path );
	virtual ~comms_conference_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_page;

	bncs_string m_layout;

	void setupGrid();
	void setupLayout();

	void setupAux();
	void setupPackages();

	static const bncs_string layout_packages;
};


#endif // comms_conference_grid_INCLUDED