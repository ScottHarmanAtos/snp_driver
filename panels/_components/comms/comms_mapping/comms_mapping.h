#ifndef comms_mapping_INCLUDED
	#define comms_mapping_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_mapping : public bncs_script_helper
{
public:
	comms_mapping( bncs_client_callback * parent, const char* path );
	virtual ~comms_mapping();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );

private:
	bncs_string m_panel;
	bncs_string m_pageName;
	bncs_string m_groupName;
	bncs_string list_name;

	riedelHelper *rh;
	bool m_renamePage;
	bool enable_list_auto_next;

	int last_page_val;
	int m_buttons;
	bncs_string m_instance;
	
	void init( void );
};


#endif // comms_mapping_INCLUDED 