#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_mapping.h"

#define PNL_MAIN	"comms_mapping"
#define PNL_MAIN_POPUP	"comms_mapping_popup"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_mapping )

// constructor - equivalent to ApplCore STARTUP
comms_mapping::comms_mapping( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_panel = "main";
	//m_pasteMode = false;
	list_name = "list";
	enable_list_auto_next = true;
	rh = riedelHelper::getriedelHelperObject();
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_mapping::~comms_mapping()
{
}

// all button pushes and notifications come here
void comms_mapping::buttonCallback( buttonNotify *b )
{
	b->dump("comms_mapping::buttonCallback");
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == list_name )
		{
			textPut("index", b->value(), PNL_MAIN, "grid");
		}
		
		else if( b->id() == "grid" )
		{
			
			if( b->value() == "released")
			{
				if(enable_list_auto_next){
					debug("comms_mapping::buttonCallback Select Next");
					textPut( "selected=next", PNL_MAIN, list_name);
				}				

				textPut( "dirty", PNL_MAIN, "pages" );
				textPut( "dirty", PNL_MAIN, "groups" );
				
				controlEnable( PNL_MAIN, "save" );
				controlEnable( PNL_MAIN, "cancel" );
			}
				
			
		}
		else if( b->id() == "save" )
		{
			controlDisable( PNL_MAIN, "save" );
			controlDisable( PNL_MAIN, "cancel" );
			controlEnable( PNL_MAIN, "renamegroups" );
			controlEnable( PNL_MAIN, "rename" );

			textPut( "save", PNL_MAIN, "grid" );
			textPut( "save", PNL_MAIN, "pages" );
			textPut( "save", PNL_MAIN, "groups" );

			textPut( "selected=none", PNL_MAIN, list_name );
		}
		else if( b->id() == "cancel" )
		{
			controlDisable( PNL_MAIN, "save" );
			controlDisable( PNL_MAIN, "cancel" );
			controlEnable( PNL_MAIN, "renamegroups" );
			controlEnable( PNL_MAIN, "rename" );

			textPut( "cancel", PNL_MAIN, "grid" );
			textPut( "cancel", PNL_MAIN, "pages" );
			textPut( "cancel", PNL_MAIN, "groups" );

			textPut( "selected=none", PNL_MAIN, list_name );
			
		}
		else if( b->id() == "pages" )
		{
			if( b->command() == "index" )
			{
				textPut( "page", b->value(), PNL_MAIN, "grid" );
				last_page_val = b->value().toInt();
				hostNotify(bncs_string("page_index=%1").arg(last_page_val));
				
			}
			else if( b->command() == "dirty" )
			{
				controlEnable( PNL_MAIN, "save" );
				controlEnable( PNL_MAIN, "cancel" );
			}
			else if( b->command() == "name" )
			{
				m_pageName = b->value();
			}
		}		
		else if( b->id() == "groups" )
		{
			if( b->command() == "index" )
			{
				textPut( "page", b->value(), PNL_MAIN, "pages" );				
				textPut( "button.1=released", PNL_MAIN, "pages" );
			}
			else if( b->command() == "dirty" )
			{
				controlEnable( PNL_MAIN, "save" );
				controlEnable( PNL_MAIN, "cancel" );
			}
			else if( b->command() == "name" )
			{
				m_groupName = b->value();
			}
		}	
		else if( b->id() == "rename" )
		{
			panelPopup( PNL_MAIN_POPUP, "keyboard.bncs_ui" );
			textPut( "text", m_pageName, PNL_MAIN_POPUP, "keyboard" );
			m_renamePage = true;
			textPut( "dirty", PNL_MAIN, "groups" );
			textPut("text","Rename Page",PNL_MAIN_POPUP,"rename");
		}
		else if( b->id() == "renamegroups" )
		{
			panelPopup( PNL_MAIN_POPUP, "keyboard.bncs_ui" );
			textPut( "text", m_groupName, PNL_MAIN_POPUP, "keyboard" );
			m_renamePage = false;
			textPut( "dirty", PNL_MAIN, "pages" );
			textPut("text","Rename Group",PNL_MAIN_POPUP,"rename");
		}
		else if (b->command() == "select_next"){
			textPut( "selected=next", PNL_MAIN, list_name);
		}
	}
	else if( b->panel() == PNL_MAIN_POPUP )
	{
		if( b->id() == "keyboard" )
		{
			bncs_string ret;

			textGet( "text", PNL_MAIN_POPUP, "keyboard", ret );
			if(m_renamePage)
			{
				if(m_panel == "functions")
					textPut( "rename", ret, PNL_MAIN, "pages" );
				else
				{
					textPut("pages_rename", ret, PNL_MAIN, "grid");
					debug("comms_mapping::set pages Name:%1", ret);
				}
				controlDisable( PNL_MAIN, "renamegroups" );
				controlEnable(PNL_MAIN, "save");
			}
			else
			{
				if(m_panel == "functions")
					textPut( "rename", ret, PNL_MAIN, "groups" );
				else
				{
					textPut("groups_rename", ret, PNL_MAIN, "grid");
					debug("comms_mapping::set groups Name:%1", ret);
				}
				controlDisable( PNL_MAIN, "rename" );
				controlEnable(PNL_MAIN, "save");
			}
		}
		else
		{
			textPut( "cancel", PNL_MAIN, "groups" );
			textPut( "cancel", PNL_MAIN, "pages" );
			controlEnable( PNL_MAIN, "renamegroups" );
			controlEnable( PNL_MAIN, "rename" );
		}
		panelDestroy( PNL_MAIN_POPUP );
	}
}

// all revertives come here
int comms_mapping::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void comms_mapping::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_mapping::parentCallback( parentNotify *p )
{	
	if( p->command() == "panel" )
	{
		m_panel = p->value().lower().replace( ".bncs_ui", "" );

		timerStart( 1, 1 );
	}
	else if(p->command() == "instance")
	{
		m_instance = p->value();
		
		timerStart(1, 1);
	}
	else if( p->command() == "no_buttons"){
		m_buttons = p->value().toInt();
	}
	else if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << bncs_string( "panel=%1" ).arg( m_panel );
		sl << bncs_string( "no_buttons=%1" ).arg( m_buttons );

		return sl.toString( '\n' );
	}
	else if( p->command() == "paste"){
		bncs_stringlist received(p->value(),',');
		debug("CRIS paste to page[%3] count=%1 received=%2",received.count(),received.toString(),last_page_val);
		int i = 0;
		int max = (received.count()<m_buttons?m_buttons:received.count());
		enable_list_auto_next = false;
		while(i < max){
			//if(received[i].toInt()!=0){
				debug("CRIS paste received[%1]=%2",i,received[i]);
				textPut( "index", received[i], PNL_MAIN, "grid" );
				textPut( bncs_string("button.%1=released").arg(i+1), PNL_MAIN, "grid" );			
			//}
			i++;
		}
		enable_list_auto_next = true;
	}
	return "";
}

// timer events come here
void comms_mapping::timerCallback( int id )
{
	timerStop( id );
	if(id==1){
		init();
	}
	else if(id==2){
		if (m_panel == "functions")
		{
			textPut("button.1=released", PNL_MAIN, "groups");
			textPut("button.1=released", PNL_MAIN, "pages");

			hostNotify(bncs_string("page_index=%1").arg(last_page_val));
		}
	}
}

void comms_mapping::init( void )
{
	panelDestroy( PNL_MAIN );

	panelShow( PNL_MAIN, m_panel + ".bncs_ui" );	

	controlDisable( PNL_MAIN, "save" );
	controlDisable( PNL_MAIN, "cancel" );

	timerStart(2,250);
	
	if (m_panel == "functions")
	{
		textPut("map_mode=true", PNL_MAIN, "grid");
	}

	textPut( "statesheet=enum_selected", PNL_MAIN, "sources" );
	textPut( "statesheet=enum_deselected", PNL_MAIN, "destinations" );
}


