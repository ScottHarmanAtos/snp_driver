#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_panel_mimic.h"

#define  PANEL_MIMIC		"panel_mimic"

#define SHIFT_OFF	1
#define SHIFT_ON	2

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_panel_mimic )

// constructor - equivalent to ApplCore STARTUP
comms_panel_mimic::comms_panel_mimic( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	//init vars
	m_shiftPage = SHIFT_OFF;
	m_intCurrentKey = -1;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MIMIC, "panel_mimics\\default.bncs_ui" );

}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_panel_mimic::~comms_panel_mimic()
{
}

// all button pushes and notifications come here
void comms_panel_mimic::buttonCallback( buttonNotify *b )
{
	b->dump("assign_keys::buttonCallback()");
	
	bncs_stringlist sltControlName = bncs_stringlist(b->id(), '_');
	bncs_string strControlPrefix = sltControlName[0];
	bncs_string strControlSuffix = sltControlName[1];
	int intControlIndex = sltControlName[1].toInt();
	
	if( b->panel() == PANEL_MIMIC )
	{
		if(b->id() == "shift")
		{
			if(m_shiftPage == SHIFT_ON)
			{
				m_shiftPage = SHIFT_OFF;
			}
			else
			{
				m_shiftPage = SHIFT_ON;
			}
			
			loadKeys();
			textPut("statesheet", (m_shiftPage == SHIFT_ON)?"enum_ok":"enum_deselected", PANEL_MIMIC, "shift");
		}
		else if(strControlPrefix == "key" && b->command() == "key")
		{
			if(m_intCurrentKey > 0)
			{
				textPut("deselect", "deselect", PANEL_MIMIC, bncs_string("key_%1").arg(m_intCurrentKey));
			}
			m_intCurrentKey = intControlIndex;
	
			hostNotify(bncs_string("key=%1").arg(b->value()));

			textPut("select", "select", PANEL_MIMIC, bncs_string("key_%1").arg(m_intCurrentKey));
			if (b->command() == "keymode" )
			{
				bncs_stringlist sltKeyProperties = bncs_stringlist(b->value(), '*');		
				bncs_string strTalkListenMode = sltKeyProperties[0];
				bncs_string strKeyMode = sltKeyProperties[3];
				debug("strKeyMode=%1", strKeyMode);
			}
		}

	}
}

// all revertives come here
int comms_panel_mimic::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void comms_panel_mimic::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_panel_mimic::parentCallback( parentNotify *p )
{
	p->dump("comms_panel_mimic::parentCallback");

	if ( p->command() == "ringport")
	{
		//Posibilities Name:ring.port.expansion or Name:ring.port or ring.port.expansion or ring.port
		//PCR 01 GFX:11.1
		bncs_string ringport;
		if(p->value().contains(':'))
		{
			bncs_string name;
			p->value().split(':', name, ringport);
		}
		else
		{
			ringport = p->value();
		}
		bncs_stringlist rpe(ringport, '.');
		m_panelPort = rh->GetPort(ringport);
		m_shiftPage = SHIFT_OFF;
		m_expansion = rpe[2]; //Will be 0 if empty
		if (m_panelPort.valid)
		{
			loadPanel();
		}
	}
	else if (p->command() == "_events")
	{
		return "key=<ring.port.expansion.shift.keynum>";
	}
	return "";
}

// timer events come here
void comms_panel_mimic::timerCallback( int id )
{
}

void comms_panel_mimic::loadPanel()
{
	bncs_string strPanelPortName;
	debug("comms_panel_mimic::loadPanel() m_panelPort=%1 expansion=%2", m_panelPort.toString(), m_expansion);

	textPut("text", m_panelPort.sourceName_short , PANEL_MIMIC, "grpPanel");

	bncs_string strAssignableKeys;
	routerName(m_panelPort.devices.Grd, riedelHelpers::databases::Assignable_Keys, m_panelPort.port, strAssignableKeys);
	//textPut("text", strPanelPortName, PANEL_MAIN, "grpPanel");
	
	//0001=sysid=,type=DCP2116,select_default=0
	bncs_string strPanelPortDetails;
	routerName(m_panelPort.devices.Grd, riedelHelpers::databases::Port_Details , m_panelPort.port, strPanelPortDetails);
	bncs_stringlist sltPortDetails(strPanelPortDetails);

	bncs_string paneltype;
	if (m_expansion == 0)
	{
		paneltype = sltPortDetails.getNamedParam("type");
	}
	else
	{
		paneltype = sltPortDetails.getNamedParam(bncs_string("exp_%1").arg(m_expansion));
	}
	
	if (m_panelType != paneltype)
	{
		m_panelType = paneltype;
		bncs_string strPanelMimic = bncs_string("panel_mimics\\%1.bncs_ui").arg(paneltype);
		panelDestroy(PANEL_MIMIC);
		panelShow(PANEL_MIMIC, strPanelMimic);
	}

	bncs_stringlist allKeys(strAssignableKeys);
	m_keys.clear();
	for (auto i = allKeys.begin(); i != allKeys.end(); ++i)
	{
		if ((*i)[0] == (m_expansion + 48))
		{
			m_keys << *i;
		}
	}
	
	loadKeys();
	
}

void comms_panel_mimic::loadKeys()
{
	bool blnFoundShiftedKeys = false;

	set<int> enabledKeys;
	for (int intPanelKey = 0; intPanelKey < m_keys.count(); intPanelKey++)
	{
		bncs_string strKeyAddress = bncs_string("%1.%2.%3").arg(m_panelPort.ring_id).arg(m_panelPort.port).arg(m_keys[intPanelKey]);
		bncs_stringlist sltKeyAddress(strKeyAddress, '.');
		int intExpansion = sltKeyAddress[2].toInt();
		int intShiftPage = sltKeyAddress[3].toInt();
		int intKeyNumber = sltKeyAddress[4].toInt();
		enabledKeys.insert(intKeyNumber);

		//used to disable the shift button, only really needs to be done the first time the panel is loaded, but easier to do here.
		if (intShiftPage == SHIFT_ON)
		{
			blnFoundShiftedKeys = true;
		}

		debug("assign_keys::loadPanel() address=%1 keynumber=%2", strKeyAddress, intKeyNumber);

		auto key = bncs_string("key_%1").arg(intKeyNumber);
		if (intKeyNumber > 0 && intShiftPage == m_shiftPage)
		{		
			textPut("key", strKeyAddress, PANEL_MIMIC,key);
			controlEnable(PANEL_MIMIC, key);
		}
	}

	for (int i = 1; i <= riedelHelpers::max::Keys; ++i)
	{
		//if the key is not in the list, disable it
		if (enabledKeys.find(i) == enabledKeys.end())
		{
			auto key = bncs_string("key_%1").arg(i);
			textPut("key", "", PANEL_MIMIC, key);
			controlDisable(PANEL_MIMIC, key);
		}
	}


	if (blnFoundShiftedKeys)
	{
		controlEnable(PANEL_MIMIC, "shift");
	}
	else
	{
		controlDisable(PANEL_MIMIC, "shift");
	}
}