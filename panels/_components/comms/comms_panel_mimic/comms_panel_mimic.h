#ifndef comms_panel_mimic_INCLUDED
	#define comms_panel_mimic_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"
#include "set"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_panel_mimic : public bncs_script_helper
{
public:
	comms_panel_mimic( bncs_client_callback * parent, const char* path );
	virtual ~comms_panel_mimic();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void loadPanel();
	void loadKeys();
	
private:
	riedelHelper * rh;
	int m_shiftPage;
	int m_expansion;
	int m_intCurrentKey;
	bncs_stringlist m_keys;
	comms_ringmaster_port m_panelPort;
	bncs_string m_panelType;
	
};


#endif // comms_panel_mimic_INCLUDED