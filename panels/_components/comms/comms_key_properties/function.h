#pragma once
#include "bncs_string.h"
#include "bncs_stringlist.h"
#include <vector>

enum CommandName
{
	NA,
	CALLTOPORT,
	TRUNKCALL,
	TRUNKIFB,
	CALLTOGROUP,
	LISTENPORT,
	CONFERENCE,
	IFB,
	LOGIC,
	SENDSTRING
};

enum Flag
{
	U_CTP_AutoListenFromDest,
	L_CTP_ListenAlways_OR_CONF_Listen,
	V_CTP_ListenVOXTriggered,
	I_CTG_IncommingCallAlert,
	T_CONF_Talk,
	S_SecondChannel_Source,
	D_SecondChannel_Dest
};


struct function
{
public:

	CommandName Command = CommandName::NA;
	bncs_string Value;
	vector<Flag> Flags;

	static function Empty;

	bool isValid() const;

	bool operator==(const function& a) const;

	static bool FlagsContains(const function& func, Flag flag);

	static void FlagAdd(function& func, Flag flag);

	static void FlagRemove(function& func, Flag flag);

	static void FlagToggle(function& func, Flag flag);

	static bncs_string GetCommandName(CommandName commandName);

	static bncs_string GetCommand(CommandName commandName);

	static bncs_string GetFlagsName(const function& func);

	static bncs_string GetFlags(const function& func);

};

