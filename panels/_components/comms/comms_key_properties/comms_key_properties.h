#ifndef comms_key_properties_INCLUDED
	#define comms_key_properties_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#include "Command.h"
#include <vector>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif




class comms_key_properties : public bncs_script_helper
{
public:
	comms_key_properties( bncs_client_callback * parent, const char* path );
	virtual ~comms_key_properties();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void processFunctionCall( bncs_string functionstring );



	int m_intSourceChanneMode;
private:
//	bncs_string m_myParam;
	riedelHelper * rh;
	
	CCommand m_current;
	CCommand m_new;
	vector<function> m_tempFunctions;

	
	bool m_dualPort;

	void init(bncs_stringlist key);
	
	void updateView(const CCommand& oldCommand, const CCommand& newCommand);
	void functionView();
	void checkChange();
	void take();
	vector<function> trunkFunctions(const vector<function>& funcs);

	bncs_stringlist m_keyId;

	int m_ring;
	int m_intDeviceRiedelMonitor;
	int m_intMonitorSlot;
	int m_intDeviceRiedelGrd;
	int m_intRingmasterConference;
	int m_intDeviceIFB;
	int m_intDeviceConference;

	map<int, bncs_string> m_conferenceNames;
	map<pair<int,int>, bncs_string> m_ifbNames;
	map<int, int> m_ifbDeviceRing;

	enum selectionType
	{
		none,
		newFunction,
		existingFunction
	};

	selectionType m_selectionType;
	int m_selectionIndex;

	function& GetFunction();
	
	bncs_string GetSubtitle(const vector<function>& functions);
	bncs_string GetSubtitle(const function& function);

	enum PopupType
	{
		Label,
		Subtitle
	}  m_popupType;
};


#endif // comms_key_properties_INCLUDED