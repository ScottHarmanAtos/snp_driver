#include "function.h"

function function::Empty = function();

bool function::isValid() const
{
	if (Command == NA) return false;
	return true;
}

bool function::operator==(const function& a) const
{
	if (Command == a.Command &&
		Value == a.Value &&
		Flags == a.Flags)
		return true;
	return false;
}

bool function::FlagsContains(const function& func, Flag flag)
{
	for (int i = 0; i < (int)func.Flags.size(); ++i)
	{
		if (func.Flags[i] == flag) return true;
	}
	return false;
}

void function::FlagAdd(function& func, Flag flag)
{
	bool b = FlagsContains(func, flag);
	if (b == false)
	{
		func.Flags.push_back(flag);
	}
}

void function::FlagRemove(function& func, Flag flag)
{
	for (auto f = func.Flags.begin(); f != func.Flags.end(); ++f)
	{
		if (*f == flag)
		{
			func.Flags.erase(f);
			return;
		}
	}
}

void function::FlagToggle(function& func, Flag flag)
{
	bool contains = FlagsContains(func, flag);
	if (contains)
	{
		FlagRemove(func, flag);
	}
	else
	{
		FlagAdd(func, flag);
	}
}

bncs_string function::GetCommandName(CommandName commandName)
{
	switch (commandName)
	{
	case CommandName::CALLTOPORT: return "C2PORT";
	case CommandName::TRUNKCALL: return "TCALL";
	case CommandName::TRUNKIFB: return "TIFB";
	case CommandName::CALLTOGROUP: return "C2GROUP";
	case CommandName::LISTENPORT: return "LPORT";
	case CommandName::CONFERENCE: return "CONF";
	case CommandName::IFB: return "IFB";
	case CommandName::LOGIC: return "LOGIC";
	case CommandName::SENDSTRING: return "SENDSTR";
	default: return "";
	}
}

bncs_string function::GetCommand(CommandName commandName)
{
	switch (commandName)
	{
	case CommandName::CALLTOPORT: return "CALLTOPORT";
	case CommandName::TRUNKCALL: return "TRUNKCALL";
	case CommandName::TRUNKIFB: return "TRUNKIFB";
	case CommandName::CALLTOGROUP: return "CALLTOGROUP";
	case CommandName::LISTENPORT: return "LISTENTOPORT";
	case CommandName::CONFERENCE: return "CONFERENCE";
	case CommandName::IFB: return "IFB";
	case CommandName::LOGIC: return "LOGIC";
	case CommandName::SENDSTRING: return "SENDSTRING";
	default: return "";
	}
}

bncs_string function::GetFlagsName(const function& func)
{
	bncs_stringlist s;
	for (int i = 0; i < (int)func.Flags.size(); ++i)
	{
		switch (func.Flags[i])
		{
		case Flag::U_CTP_AutoListenFromDest: s << "Auto Listen From Dest"; break;
		case Flag::T_CONF_Talk: s << "Talk"; break;
		case Flag::L_CTP_ListenAlways_OR_CONF_Listen: s << "Listen"; break;
		case Flag::V_CTP_ListenVOXTriggered: s << "Listen Vox Trigger"; break;
		case Flag::I_CTG_IncommingCallAlert: s << "Incomming Call Alert"; break;
		case Flag::S_SecondChannel_Source: s << "2nd Source"; break;
		case Flag::D_SecondChannel_Dest: s << "2nd Dest"; break;
		}
	}
	return s.toString();
}

bncs_string function::GetFlags(const function& func)
{
	bncs_string s;
	for (int i = 0; i < (int)func.Flags.size(); ++i)
	{
		switch (func.Flags[i])
		{
		case Flag::U_CTP_AutoListenFromDest: s.append("U"); break;
		case Flag::T_CONF_Talk: s.append("T"); break;
		case Flag::L_CTP_ListenAlways_OR_CONF_Listen: s.append("L"); break;
		case Flag::V_CTP_ListenVOXTriggered: s.append("V"); break;
		case Flag::I_CTG_IncommingCallAlert: s.append("I"); break;
		case Flag::S_SecondChannel_Source: s.append("S"); break;
		case Flag::D_SecondChannel_Dest: s.append("D"); break;
		}
	}
	return s;
}