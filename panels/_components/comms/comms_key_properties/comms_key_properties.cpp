#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "comms_key_properties.h"

#define PANEL_MAIN  "comms_key_properties"
#define PANEL_POPUP "popup_keyboard"

#define ICON_CHANNEL_1 "icon_channel_1.png" 
#define ICON_CHANNEL_2 "icon_channel_2.png" 



// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(comms_key_properties)

// constructor - equivalent to ApplCore STARTUP
comms_key_properties::comms_key_properties(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path)
{
	rh = riedelHelper::getriedelHelperObject();

	m_current = CCommand();
	m_new = CCommand();
	m_tempFunctions = vector<function>();

	m_dualPort = false;
	m_ring = 0;
	m_intDeviceRiedelMonitor = 0;
	m_intDeviceRiedelGrd = 0;
	m_selectionIndex = -1;
	m_selectionType = comms_key_properties::selectionType::none;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow(PANEL_MAIN, "main.bncs_ui");

	controlDisable(PANEL_MAIN, "take");
	controlDisable(PANEL_MAIN, "clear");
	controlDisable(PANEL_MAIN, "clear_all");
	controlDisable(PANEL_MAIN, "add_function");
	controlDisable(PANEL_MAIN, "conf_tl");
	controlDisable(PANEL_MAIN, "conf_t");
	controlDisable(PANEL_MAIN, "conf_l");
	controlDisable(PANEL_MAIN, "autolisten");
	controlDisable(PANEL_MAIN, "keymode_MonDim");
	controlDisable(PANEL_MAIN, "keymode_AutoDim");
	controlDisable(PANEL_MAIN, "keymode_LatchNoDim");
	controlDisable(PANEL_MAIN, "autolisten");
	controlDisable(PANEL_MAIN, "targetchannel_1");
	controlDisable(PANEL_MAIN, "targetchannel_2");
	controlDisable(PANEL_MAIN, "functionchannel_1");
	controlDisable(PANEL_MAIN, "functionchannel_2");
	controlDisable(PANEL_MAIN, "stack");

	m_intRingmasterConference = rh->GetRingmasterDevices().Conferences;

	auto rings = rh->GetRings();
	for (auto it = rings.cbegin();  it != rings.cend(); ++it)
	{
		infoRegister(it->second.devices.Ifb, 1, riedelHelpers::max::IFBs);
		infoPoll(it->second.devices.Ifb, 1, riedelHelpers::max::IFBs);

		for (int i = 1; i < riedelHelpers::max::IFBs; ++i)
		{
			m_ifbNames[make_pair(it->second.devices.Ifb, i)] = "";
		}
		m_ifbDeviceRing[it->second.devices.Ifb] = it->first;
	}
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_key_properties::~comms_key_properties()
{
}

// all button pushes and notifications come here
void comms_key_properties::buttonCallback(buttonNotify* b)
{
	b->dump("assign_keys::buttonCallback()");

	function& func = GetFunction();

	if (b->panel() == PANEL_MAIN)
	{
		if (b->id() == "autolisten")
		{
			if (!func.isValid()) return;
			function::FlagToggle(func, Flag::L_CTP_ListenAlways_OR_CONF_Listen);
			functionView();
		}
		else if (b->id().startsWith("conf_"))
		{
			if (!func.isValid()) return;
			if (b->id() == "conf_tl")
			{
				function::FlagAdd(func, Flag::L_CTP_ListenAlways_OR_CONF_Listen);
				function::FlagAdd(func, Flag::T_CONF_Talk);
			}
			else if (b->id() == "conf_t")
			{
				function::FlagAdd(func, Flag::T_CONF_Talk);
				function::FlagRemove(func, Flag::L_CTP_ListenAlways_OR_CONF_Listen);
			}
			else if (b->id() == "conf_l")
			{
				function::FlagAdd(func, Flag::L_CTP_ListenAlways_OR_CONF_Listen);
				function::FlagRemove(func, Flag::T_CONF_Talk);
			}
			functionView();
		}
		else if (b->id().startsWith("targetchannel_"))
		{
			if (!func.isValid()) return;
			if (b->id() == "targetchannel_1")
			{
				function::FlagRemove(func, Flag::S_SecondChannel_Source);
			}
			else
			{
				function::FlagAdd(func, Flag::S_SecondChannel_Source);
			}
			functionView();
		}
		else if (b->id().startsWith("functionchannel_"))
		{
			if (!func.isValid()) return;
			if (b->id() == "functionchannel_1")
			{
				function::FlagRemove(func, Flag::D_SecondChannel_Dest);
			}
			else
			{
				function::FlagAdd(func, Flag::D_SecondChannel_Dest);
			}
			functionView();
		}
		else if (b->id().startsWith("keymode_"))
		{
			if (b->id() == "keymode_MonDim")
			{
				m_new.keymode = KeyMode::Momentary;
				m_new.dim = Dim::Yes;
			}
			else if (b->id() == "keymode_AutoDim")
			{
				m_new.keymode = KeyMode::Auto;
				m_new.dim = Dim::Yes;
			}
			else if (b->id() == "keymode_LatchNoDim")
			{
				m_new.keymode = KeyMode::Latching;
				m_new.dim = Dim::No;
			}
			updateView(m_current, m_new);

		}
		else if (b->id() == "functions")
		{
			if (b->command() == "selection")
			{
				auto i = b->sub(0);
				if (m_selectionType != comms_key_properties::selectionType::existingFunction || m_selectionIndex != i)
				{
					m_selectionType = comms_key_properties::selectionType::existingFunction;
					m_selectionIndex = i;

					functionView();
				}
			}
		}
		else if (b->id() == "clear_all")
		{
			m_new.functions.clear();
			m_new.label = "";
			m_new.subtitle = "";
			auto command = CCommand::GetFullCommand(m_new);
			infoWrite(m_intDeviceRiedelMonitor, command, m_intMonitorSlot, false);
		}
		else if (b->id() == "clear_label")
		{
			m_new.label = "";
			updateView(m_current, m_new);
		}
		else if (b->id() == "clear_function")
		{
			if (m_selectionType == comms_key_properties::selectionType::existingFunction)
			{
				if (m_selectionIndex < (int)m_new.functions.size() && m_selectionIndex > -1)
				{
					m_new.functions.erase(m_new.functions.begin() + m_selectionIndex);
				}
				//functionView(m_current, m_selectedFunction);
			}
			m_new.subtitle = GetSubtitle(m_new.functions);
			take();
		}
		else if (b->id() == "add_function")
		{
			auto f = m_current.functions;
			auto tf = trunkFunctions(m_tempFunctions);
			for (auto nf = tf.begin(); nf != tf.end(); nf++)
			{
				f.push_back(*nf);
			}
			m_new.functions = f;
			m_new.subtitle = GetSubtitle(m_new.functions);
			take();
		}
		else if (b->id() == "replace_function")
		{
			m_new.functions = trunkFunctions(m_tempFunctions);;
			m_new.subtitle = GetSubtitle(m_new.functions);
			take();
		}
		else if (b->id() == "take_function")
		{
			m_new.functions = trunkFunctions(m_tempFunctions);;
			m_new.subtitle = GetSubtitle(m_new.functions);
			take();
		}
		else if (b->id() == "take")
		{
			take();
		}
		else if (b->id() == "label")
		{
			panelPopup(PANEL_POPUP, "popup_keyboard.bncs_ui");
			textPut("text", m_new.label, PANEL_POPUP, "keyboard");
			m_popupType = Label;
		}
		else if (b->id() == "subtitle")
		{
			panelPopup(PANEL_POPUP, "popup_keyboard.bncs_ui");
			textPut("text", m_new.subtitle, PANEL_POPUP, "keyboard");
			m_popupType = Subtitle;
		}
	}
	else if (b->panel() == PANEL_POPUP)
	{
		bool save = false;
		if (b->id() == "keyboard")
		{
			if (b->sub(0) == "return")
			{
				save = true;
			}
			else
			{
				return;
			}
		}
		if (b->id() == "default")
		{
			bncs_string default;
			if (m_popupType == Label)
			{
				routerName(m_intDeviceRiedelMonitor, 2, m_intMonitorSlot, default);
			}
			else if (m_popupType == Subtitle)
			{
				default = GetSubtitle(m_new.functions);
			}
			textPut("text", default, PANEL_POPUP, "keyboard");
		}
		else if (b->id() == "cancel")
		{
			panelDestroy(PANEL_POPUP);
		}
		else if (save || b->id() == "save")
		{
			bncs_string text;
			textGet("text", PANEL_POPUP, "keyboard", text);
			if (m_popupType == Label)
			{
				infoWrite(m_intDeviceRiedelMonitor, "&LABEL=" + text, m_intMonitorSlot);
			}
			else if (m_popupType == Subtitle)
			{
				infoWrite(m_intDeviceRiedelMonitor, "&SUBTITLE=" + text, m_intMonitorSlot);
			}
			panelDestroy(PANEL_POPUP);
		}
	}
}

// all revertives come here
int comms_key_properties::revertiveCallback(revertiveNotify * r)
{
	if (r->device() == m_intDeviceRiedelMonitor)
	{
		bncs_string labelDefault;
		routerName(m_intDeviceRiedelMonitor, 2, m_intMonitorSlot, labelDefault);
		m_current = CCommand(r->sInfo(), labelDefault);
		m_new = CCommand(m_current);
		updateView(m_new, m_current);
		functionView();
	}
	else if (r->device() == m_intRingmasterConference)
	{
		m_conferenceNames[r->index() - riedelHelpers::offset::CONF_LABEL] = r->sInfo();
	}
	else
	{
		auto pair = make_pair(r->device(), r->index());
		auto it = m_ifbNames.find(pair);
		
		if (it != m_ifbNames.end())
		{
			bncs_stringlist sl(r->sInfo(),'|');
			if (sl.count() == 4)
			{
				m_ifbNames[pair] = sl[3];
			}
		}
	}

	return 0;
}

// all database name changes come back here
void comms_key_properties::databaseCallback(revertiveNotify * r)
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_key_properties::parentCallback(parentNotify *p)
{
	if (p->command() == "return")
	{
		bncs_stringlist sl;
		sl << bncs_string("key=%1").arg(m_keyId.toString('.'));
		return sl.toString('\n');
	}
	else if (p->command() == "function")
	{
		auto functions = CCommand::GetFunctions(p->value());

		if (functions.size() > 0)
		{
			m_tempFunctions = functions;
			m_selectionType = comms_key_properties::selectionType::newFunction;

			if(m_ring != 0) functionView();
		}
		else
		{
			if (m_tempFunctions.size() > 0)
			{
				m_tempFunctions.clear();
				m_selectionType = comms_key_properties::selectionType::none;
				functionView();
			}
		}
	}
	else if (p->command() == "index")
	{
		//Ignore if no button seleced
		auto port = rh->GetPort(p->value());
		debug("comms_key_properties::parentCallback port:%1", port.ring_port);
		int index = port.port;
		if (port.valid)
		{
			m_tempFunctions = vector<function>();
			function f;
			f.Command = CommandName::CALLTOPORT;
			f.Value = p->value();

			m_tempFunctions.push_back(f);
			m_selectionType = comms_key_properties::selectionType::newFunction;
			if(m_ring != 0) functionView();
		}
		else
		{
			if (m_tempFunctions.size() > 0)
			{
				m_tempFunctions.clear();
				m_selectionType = comms_key_properties::selectionType::none;
				functionView();
			}
		}
	}
	else if (p->command() == "key")
	{
		//Expecting ring.port.expansion.shift.key
		if (p->value() != m_keyId.toString('.'))
		{
			infoUnregister(m_intDeviceRiedelMonitor);
			m_keyId = bncs_stringlist(p->value(), '.');
			init(m_keyId);
		}
		else
		{

		}
	}

	return "";
}

// timer events come here
void comms_key_properties::timerCallback(int id)
{
}

void comms_key_properties::init(bncs_stringlist key)
{
	//Disable everything
	controlDisable(PANEL_MAIN, "take");
	controlDisable(PANEL_MAIN, "clear_function");
	controlDisable(PANEL_MAIN, "clear_all");
	controlDisable(PANEL_MAIN, "add_function");
	controlDisable(PANEL_MAIN, "conf_tl");
	controlDisable(PANEL_MAIN, "conf_t");
	controlDisable(PANEL_MAIN, "conf_l");
	controlDisable(PANEL_MAIN, "autolisten");
	controlDisable(PANEL_MAIN, "keymode_MonDim");
	controlDisable(PANEL_MAIN, "keymode_AutoDim");
	controlDisable(PANEL_MAIN, "keymode_LatchNoDim");
	controlDisable(PANEL_MAIN, "targetchannel_1");
	controlDisable(PANEL_MAIN, "targetchannel_2");
	controlDisable(PANEL_MAIN, "functionchannel_1");
	controlDisable(PANEL_MAIN, "functionchannel_2");
	textPut("clear", "", PANEL_MAIN, "functions");
	textPut("key", "", PANEL_MAIN, "preselected_key");

	if (key.count() < 5)
	{
		//error
		m_ring = 0;
		return;
	}

	//Check if the ring is the same, as less needs to be registed
	auto ring = key[0].toInt();
	if (ring != m_ring)
	{
		m_ring = ring;
		auto r = rh->GetRing(m_ring);
		if (r.valid)
		{
			m_intDeviceRiedelMonitor = r.devices.Monitor;
			m_intDeviceRiedelGrd = r.devices.Grd;
			m_intDeviceIFB = r.devices.Ifb;
			m_intDeviceConference = r.devices.Conference;
		}
	}

	auto port = rh->GetPort(key[0], key[1]);
	if (port.valid)
	{
		textPut("text", port.ring_port, PANEL_MAIN, "port");
		textPut("text", bncs_string("%1.%2.%3").arg(key[2]).arg(key[3]).arg(key[4]), PANEL_MAIN, "key");

		textPut("text", port.sourceName_long, PANEL_MAIN, "longName");
		textPut("text", port.source_8_char, PANEL_MAIN, "8Char");	
	}

	m_intMonitorSlot = routerIndex(m_intDeviceRiedelMonitor, riedelHelpers::databases::Monitor_Lookup, bncs_string("%1.%2.%3.%4").arg(m_keyId[1]).arg(m_keyId[2]).arg(m_keyId[3]).arg(m_keyId[4]));

	infoRegister(m_intDeviceRiedelMonitor, m_intMonitorSlot, m_intMonitorSlot, false);
	infoPoll(m_intDeviceRiedelMonitor, m_intMonitorSlot, m_intMonitorSlot);

	textPut("key",key.toString('.'), PANEL_MAIN, "preselected_key");

	bncs_string portType;
	routerName(m_intDeviceRiedelGrd, riedelHelpers::databases::Port_Type, m_keyId[1], portType);
	
	m_dualPort = false;
	if (portType == 6)
	{
		m_dualPort = true;
	}

	controlEnable(PANEL_MAIN, "keymode_MonDim");
	controlEnable(PANEL_MAIN, "keymode_AutoDim");
	controlEnable(PANEL_MAIN, "keymode_LatchNoDim");
	
	if (m_selectionType == comms_key_properties::selectionType::existingFunction)
	{
		if ((int)m_tempFunctions.size() > 0)
		{
			comms_key_properties::selectionType::newFunction;
		}
		else
		{
			comms_key_properties::selectionType::none;
		}
	}

}


void comms_key_properties::updateView(const CCommand& oldCommand, const CCommand& newCommand)
{
	textPut("text", newCommand.subtitle, PANEL_MAIN, "subtitle");
	textPut("text", newCommand.label, PANEL_MAIN, "label");

	textPut("statesheet", "enum_deselected", PANEL_MAIN, "keymode_MonDim");
	textPut("statesheet", "enum_deselected", PANEL_MAIN, "keymode_AutoDim");
	textPut("statesheet", "enum_deselected", PANEL_MAIN, "keymode_LatchNoDim");

	//textPut("statesheet", oldCommand.label != newCommand.label ? "enum_warning" : "enum_deselected", PANEL_MAIN, "label");
	//textPut("statesheet", oldCommand.subtitle != newCommand.subtitle ? "enum_warning" : "enum_deselected", PANEL_MAIN, "subtitle");

	bncs_string state = "enum_selected";
	if (oldCommand.keymode != newCommand.keymode || oldCommand.dim != newCommand.dim)
	{
		state = "enum_warning";
	}

	if (newCommand.keymode == KeyMode::Latching && newCommand.dim == Dim::No)
	{
		textPut("statesheet", state, PANEL_MAIN, "keymode_LatchNoDim");
	}
	else if (newCommand.keymode == KeyMode::Auto && newCommand.dim == Dim::Yes)
	{
		textPut("statesheet", state, PANEL_MAIN, "keymode_AutoDim");
	}
	else if (newCommand.keymode == KeyMode::Momentary && newCommand.dim == Dim::Yes)
	{
		textPut("statesheet", state, PANEL_MAIN, "keymode_MonDim");
	}
	else if (newCommand.keymode == KeyMode::KeyMode_NotSet && newCommand.dim == Dim::Dim_NotSet)
	{
		//This is a lie, the real value is of not specified is KA|D1|AU
		//But the desired value is KM|D1 so on null we will pretend the value is KM|D1 and then tell the riedel driver to set this value
		textPut("statesheet", state, PANEL_MAIN, "keymode_MonDim");
	}

	//Dont select any if it does not match
	checkChange();
}

void comms_key_properties::functionView()
{
	//Selection
	int selectedIndex = -1;
	if (m_selectionType == comms_key_properties::selectionType::existingFunction)
	{
		if (m_selectionIndex >= 0)
		{
			if ((int)m_new.functions.size() > m_selectionIndex) selectedIndex = m_selectionIndex;
			else if ((int)m_new.functions.size() == 0) selectedIndex = -1;
			else selectedIndex = (int)m_new.functions.size() - 1; //select the last item in the list
		}
	}

	if (m_new.functions.size() > 0)
	{
		controlHide(PANEL_MAIN, "take_function");
		
		if (m_tempFunctions.size() > 0)
		{
			controlEnable(PANEL_MAIN, "add_function");	
			controlEnable(PANEL_MAIN, "replace_function");
		}
		else
		{
			controlDisable(PANEL_MAIN, "add_function");
			controlDisable(PANEL_MAIN, "replace_function");
		}

		if (selectedIndex >= 0)
		{
			controlEnable(PANEL_MAIN, "clear_function");
		}
		else
		{
			controlDisable(PANEL_MAIN, "clear_function");
		}
		controlEnable(PANEL_MAIN, "clear_all");
	}
	else
	{
		controlShow(PANEL_MAIN, "take_function");
		if (m_tempFunctions.size() > 0)
		{
			controlEnable(PANEL_MAIN, "take_function");
		}
		else
		{
			controlDisable	(PANEL_MAIN, "take_function");
		}
		controlDisable(PANEL_MAIN, "clear_function");
		controlDisable(PANEL_MAIN, "clear_all");
	}

	textPut("clear", "", PANEL_MAIN, "functions");
	for (int i = 0; i < (int)m_new.functions.size(); i++)
	{
		const function& f = m_new.functions[i];

		bncs_string label = f.Value;
		switch (f.Command)
		{
		case CommandName::CALLTOPORT:
			routerName(m_intDeviceRiedelGrd, riedelHelpers::databases::Input_Long_Name, f.Value, label);
			break;
		case CommandName::TRUNKCALL:
		{
			auto p = rh->GetPort(f.Value);
			label = p.sourceName_short;
		}
			break;
		case CommandName::TRUNKIFB:
		{
			bncs_string ring, ifb;
			f.Value.split('.', ring, ifb);
			auto r = rh->GetRing(ring);
			label = m_ifbNames[make_pair(r.devices.Ifb,ifb)];
			if (label.length() == 0)
			{
				label = f.Value;
			}
		}
			break;
		case CommandName::CALLTOGROUP: break;
		case CommandName::LISTENPORT: break;
		case CommandName::CONFERENCE:
			label = m_conferenceNames[f.Value];
			if(label.length() == 0)
				routerName(m_intRingmasterConference, riedelHelpers::databases::Conference_Name, f.Value, label);
			break;
		case CommandName::IFB:
		{
			auto r = rh->GetRing(m_ring);
			label = m_ifbNames[make_pair(r.devices.Ifb, f.Value)];
			if (label.length() == 0)
			{
				label = f.Value;
			}
		}
			break;
		case CommandName::LOGIC: break;
		case CommandName::SENDSTRING: break;
		}
		auto labelReplace = label.replace('|', ' ');
		bncs_string s = bncs_string("%1-%2-%3").arg(function::GetCommandName(f.Command)).arg(labelReplace).arg(function::GetFlagsName(f));

		textPut("add", s, PANEL_MAIN, "functions");
	}

	textPut("selected.id", selectedIndex, PANEL_MAIN, "functions");

	controlDisable(PANEL_MAIN, "conf_tl");
	controlDisable(PANEL_MAIN, "conf_t");
	controlDisable(PANEL_MAIN, "conf_l");
	controlDisable(PANEL_MAIN, "autolisten");

	const function& f = GetFunction();
	if (f.isValid())
	{
		bncs_string state = "enum_warning";
		for (auto it = m_new.functions.begin(); it != m_new.functions.end(); ++it)
		{
			if (f.Command == it->Command && f.Value == it->Value)
			{
				if (it->Flags == f.Flags)
				{
					state = "enum_selected";
				}
				break;
			}
		}		

		if (m_dualPort)
		{
			controlEnable(PANEL_MAIN, "targetchannel_1");
			controlEnable(PANEL_MAIN, "targetchannel_2");
			controlEnable(PANEL_MAIN, "functionchannel_1");
			controlEnable(PANEL_MAIN, "functionchannel_2");

			bool source = function::FlagsContains(f, Flag::S_SecondChannel_Source);
			bool dest = function::FlagsContains(f, Flag::D_SecondChannel_Dest);
			
			if (!(f.Command == CommandName::CALLTOPORT || f.Command == CommandName::TRUNKCALL || f.Command == CommandName::LISTENPORT))
			{
				//If not one of the above dest cannot be set
				controlDisable(PANEL_MAIN, "functionchannel_1");
				controlDisable(PANEL_MAIN, "functionchannel_2");
				dest = false;
			}

			textPut("statesheet", "enum_deselected" PANEL_MAIN, "targetchannel_1");
			textPut("statesheet", "enum_deselected" PANEL_MAIN, "targetchannel_2");
			textPut("statesheet", "enum_deselected" PANEL_MAIN, "functionchannel_1");
			textPut("statesheet", "enum_deselected" PANEL_MAIN, "functionchannel_2");

			if (source && dest)
			{
				textPut("statesheet", "enum_selected" PANEL_MAIN, "targetchannel_2");
				textPut("statesheet", "enum_selected" PANEL_MAIN, "functionchannel_2");
			}
			else if (source)
			{
				textPut("statesheet", "enum_selected" PANEL_MAIN, "targetchannel_2");
				textPut("statesheet", "enum_selected" PANEL_MAIN, "functionchannel_1");
			}
			else if (dest)
			{
				textPut("statesheet", "enum_selected" PANEL_MAIN, "functionchannel_2");
				textPut("statesheet", "enum_selected" PANEL_MAIN, "targetchannel_1");
			}
			else
			{
				textPut("statesheet", "enum_selected" PANEL_MAIN, "targetchannel_1");
				textPut("statesheet", "enum_selected" PANEL_MAIN, "functionchannel_1");
			}
		}

		switch (f.Command)
		{
		case CommandName::CALLTOPORT:
		{
			controlEnable(PANEL_MAIN, "autolisten");

			if (function::FlagsContains(f, Flag::L_CTP_ListenAlways_OR_CONF_Listen))
			{
				textPut("statesheet", state, PANEL_MAIN, "autolisten");
			}
			else
			{
				textPut("statesheet", "enum_deselected", PANEL_MAIN, "autolisten");
			}
		}
			break;
		case CommandName::TRUNKCALL: break;
		case CommandName::CALLTOGROUP: break;
		case CommandName::LISTENPORT: break;
		case CommandName::CONFERENCE:
		{
			controlEnable(PANEL_MAIN, "conf_tl");
			controlEnable(PANEL_MAIN, "conf_t");
			controlEnable(PANEL_MAIN, "conf_l");

			bool talk = function::FlagsContains(f, Flag::T_CONF_Talk);
			bool listen = function::FlagsContains(f, Flag::L_CTP_ListenAlways_OR_CONF_Listen);

			textPut("statesheet", "enum_deselected", PANEL_MAIN, "conf_tl");
			textPut("statesheet", "enum_deselected", PANEL_MAIN, "conf_t");
			textPut("statesheet", "enum_deselected", PANEL_MAIN, "conf_l");
			if (talk && listen)
			{
				textPut("statesheet", state, PANEL_MAIN, "conf_tl");
			}
			else if (talk)
			{
				textPut("statesheet", state, PANEL_MAIN, "conf_t");
			}
			else if (listen)
			{
				textPut("statesheet", state, PANEL_MAIN, "conf_l");
			}
		}
		break;
		case CommandName::IFB: break;
		case CommandName::LOGIC: break;
		case CommandName::SENDSTRING: break;
		}
	}
	checkChange();
}

void comms_key_properties::checkChange()
{
	if (m_new.functions == m_current.functions &&
		m_new.dim == m_current.dim &&
		m_new.keymode == m_current.keymode &&
		m_new.marker == m_current.marker)
	{

		controlDisable(PANEL_MAIN, "take");
		textPut("statesheet", "enum_deselected", PANEL_MAIN, "take_border");
	}
	else
	{
		controlEnable(PANEL_MAIN, "take");
		textPut("statesheet", "enum_warning", PANEL_MAIN, "take_border");
	}

	if (m_new.functions != m_current.functions)
	{
		textPut("stylesheet", "enum_warning", PANEL_MAIN, "functions");
	}
	else
	{
		textPut("stylesheet", "enum_deselected", PANEL_MAIN, "functions");
	}

}

void comms_key_properties::take()
{
	if (m_new.keymode == KeyMode::KeyMode_NotSet)
		m_new.keymode = KeyMode::Momentary;
	if (m_new.dim == Dim::Dim_NotSet)
		m_new.dim = Dim::Yes;

	auto command = CCommand::GetFullCommand(m_new);

	infoWrite(m_intDeviceRiedelMonitor, command, m_intMonitorSlot, false);
}

function& comms_key_properties::GetFunction()
{
	if (m_selectionType == comms_key_properties::selectionType::existingFunction)
	{
		if (m_selectionIndex > -1 && m_selectionIndex < (int)m_new.functions.size() && (int)m_new.functions.size() > 0)
		{
			return m_new.functions[m_selectionIndex];
		}
		else
		{
			return function::Empty;
		}
	}
	else if(m_selectionType == comms_key_properties::selectionType::newFunction)
	{
		if ((int)m_tempFunctions.size() > 0)
		{
			return m_tempFunctions[0];
		}
		else
		{
			return function::Empty;
		}
	}

	return function::Empty;
}

bncs_string comms_key_properties::GetSubtitle(const vector<function>& functions)
{
	bncs_string sub;
	for (int i = 0; i < (int)functions.size(); ++i)
	{
		bncs_string s = GetSubtitle(functions[0]);

		if (sub.length() == 0)
			sub = s;
		else
		{
			sub = sub + " +";
			break;
		}
	}
	return sub;
}

bncs_string comms_key_properties::GetSubtitle(const function& function)
{
	/*
	NA,
	CALLTOPORT,
	TRUNKCALL,
	CALLTOGROUP,
	LISTENPORT,
	CONFERENCE,
	IFB,
	LOGIC,
	SENDSTRING
	*/

	bncs_string sub;
	if (function.Command == CALLTOPORT) routerName(m_intDeviceRiedelGrd, riedelHelpers::databases::Subtitle_Input, function.Value, sub);
	else if (function.Command == CONFERENCE) routerName(rh->GetRingmasterDevices().Conferences, riedelHelpers::databases::Subtitle_Input, function.Value, sub);
	else if (function.Command == TRUNKCALL)
	{
		auto p = rh->GetPort(function.Value);
		auto ring = rh->GetRing(p.ring_id);

		routerName(ring.devices.Grd, riedelHelpers::databases::Subtitle_Input, p.slot_index, sub);
	}
	else if(function.Command == IFB) routerName(m_intDeviceIFB, riedelHelpers::databases::Subtitle_Input, function.Value, sub);
	else if (function.Command == TRUNKIFB)
	{
		auto p = rh->GetPort(function.Value);
		auto ring = rh->GetRing(p.ring_id);

		routerName(ring.devices.Ifb, riedelHelpers::databases::Subtitle_Input, p.slot_index, sub);
	}
	return sub;
}

vector<function> comms_key_properties::trunkFunctions(const vector<function>& functions)
{
	vector<function> funcs(functions);
	for (auto i = funcs.begin(); i != funcs.end(); ++i)
	{
		//Check if a CALLTOPORT should be a TRUNK CALL
		if (i->Command == CALLTOPORT)
		{
			auto v = i->Value;
			if (!v.contains('.'))
			{
				continue;
			}
			auto port = rh->GetPort(i->Value);
			if (port.valid)
			{
				if (m_ring == port.ring_id)
				{
					i->Value = port.port;
				}
				else
				{
					i->Command = TRUNKCALL;
				}
			}
		}
		//Check if a IFB should be a TRUNK IFB
		if (i->Command == IFB)
		{
			auto v = i->Value;
			if (!v.contains('.'))
			{
				continue;
			}
			bncs_string ring, ifb;
			v.split('.', ring, ifb);

			if (m_ring == ring)
			{
				i->Value = ifb;
			}
			else
			{
				i->Command = TRUNKIFB;
			}
		}
	}
	return funcs;
}