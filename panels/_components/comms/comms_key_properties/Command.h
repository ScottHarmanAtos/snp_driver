// Command.h: interface for the CCommand class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMMAND_H__47F63BB0_0A7C_46A0_864A_22AB55F02F76__INCLUDED_)
#define AFX_COMMAND_H__47F63BB0_0A7C_46A0_864A_22AB55F02F76__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "bncs_string.h"
#include "bncs_stringlist.h"
#include <bncs_script_helper.h>
#include "riedelHelper.h"
#include "function.h"

enum KeyMode
{
	KeyMode_NotSet,
	Momentary,
	Latching,
	Auto
};
enum Dim
{
	Dim_NotSet,
	No,
	Yes
};


class CCommand  
{
public:


	CCommand();
	CCommand(const bncs_string& message, const bncs_string& labelDefault);
	CCommand(const CCommand& command);
	virtual ~CCommand();
	static vector<function> GetFunctions(const bncs_string& string);
	static bncs_string GetFullCommand(const CCommand& command);
	
	static bncs_string GetKeymode(const CCommand& command);
	static bncs_string GetDim(const CCommand& command);

	vector<function> functions;
	bncs_string marker;
	bncs_string label;
	KeyMode keymode;
	Dim dim;
	bncs_string subtitle;
	bncs_string colour;

};

#endif // !defined(AFX_COMMAND_H__47F63BB0_0A7C_46A0_864A_22AB55F02F76__INCLUDED_)
