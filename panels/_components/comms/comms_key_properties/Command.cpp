
// Command.cpp: implementation of the CCommand class.
//
//////////////////////////////////////////////////////////////////////

#include "Command.h"
#include "bncs_stringlist.h"
#include <bncs_script_helper.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCommand::CCommand()
{
	keymode = KeyMode::KeyMode_NotSet;
	dim = Dim::Dim_NotSet;
	functions = vector<function>();
	marker = "0";
	label = "";
	subtitle = "";
	colour = "";
}

CCommand::CCommand(const CCommand& command)
{
	functions = command.functions;
	marker = command.marker;
	label = command.label;
	keymode = command.keymode;
	dim = command.dim;
	subtitle = command.subtitle;
	colour = command.colour;
}

CCommand::CCommand(const bncs_string& msg, const bncs_string& labelDefault)
{
	// CALLTOPORT|12|*0**KM|D1
	         
	keymode = KeyMode::KeyMode_NotSet;
	dim = Dim::Dim_NotSet;

	//{FUNCTION}[,{FUNCTION}]*{MARKER}*{LABEL}*{KEYPROPERTIES}*{SUBTITLE}*{TEXTCOLOUR}
	//  separate all elements of the message 
	bncs_stringlist sltWholeMessageList(msg, '*');
	
	marker = sltWholeMessageList[1].length() > 0 ? sltWholeMessageList[1] : "0" ;
	label = sltWholeMessageList[2];
	subtitle = sltWholeMessageList[4];

	//Don't do anything with the colour yet, just pass it though
	colour = sltWholeMessageList[5];

	//If the string passed is the default value, clear the label text
	if (label == labelDefault)
		label = "";

	//Set Properties
	auto props = bncs_stringlist(sltWholeMessageList[3], '|');
	for (int i = 0; i < props.count(); ++i)
	{
		switch (props[i][0])
		{
		case 'K':
			switch (props[i][1])
			{
			case 'M': keymode = KeyMode::Momentary; break;
			case 'A': keymode = KeyMode::Auto; break;
			case 'L': keymode = KeyMode::Latching; break;

			}break;
		case 'D':
			switch (props[i][1])
			{
			case '1': dim = Dim::Yes; break;
			case '0': dim = Dim::No; break;
			}
			break;
		}
	}

	functions = GetFunctions(sltWholeMessageList[0]);
}

CCommand::~CCommand()
{

}

vector<function> CCommand::GetFunctions(const bncs_string& string)
{
	vector<function> fun;
	bncs_stringlist funcs = bncs_stringlist(string, ',');
	//Set Functions
	//{FUNCTION}[,{FUNCTION}
	for (int i = 0; i < funcs.count(); i++)
	{
		//FunctionName|Value|Flag
		bncs_stringlist fvf = bncs_stringlist(funcs[i], '|');
		if (fvf.count() < 2) continue;

		function f;
		auto commandName = fvf[0].upper();
		if (commandName == "CALLTOPORT")f.Command = CommandName::CALLTOPORT;
		else if (commandName == "TRUNKCALL")f.Command = CommandName::TRUNKCALL;
		else if (commandName == "TRUNKIFB")f.Command = CommandName::TRUNKIFB;
		else if (commandName == "CALLTOGROUP")f.Command = CommandName::CALLTOGROUP;
		else if (commandName == "LISTENTOPORT")f.Command = CommandName::LISTENPORT;
		else if (commandName == "CONFERENCE")f.Command = CommandName::CONFERENCE;
		else if (commandName == "IFB")f.Command = CommandName::IFB;
		else if (commandName == "LOGIC")f.Command = CommandName::LOGIC;
		else if (commandName == "SENDSTRING")f.Command = CommandName::SENDSTRING;

		f.Value = fvf[1];

		//Only do if we have a 3rd item
		if (fvf.count() == 3)
		{
			for (int flag = 0; flag < (int)fvf[2].length(); flag++)
			{
				switch (fvf[2][flag])
				{
				case 'U': f.Flags.push_back(Flag::U_CTP_AutoListenFromDest); break;
				case 'L': f.Flags.push_back(Flag::L_CTP_ListenAlways_OR_CONF_Listen); break;
				case 'V': f.Flags.push_back(Flag::V_CTP_ListenVOXTriggered); break;
				case 'I': f.Flags.push_back(Flag::I_CTG_IncommingCallAlert); break;
				case 'T': f.Flags.push_back(Flag::T_CONF_Talk); break;
				case 'S': f.Flags.push_back(Flag::S_SecondChannel_Source); break;
				case 'D': f.Flags.push_back(Flag::D_SecondChannel_Dest); break;
				}
			}
		}
		fun.push_back(f);
	}
	return fun;
}

bncs_string CCommand::GetKeymode(const CCommand& command)
{
	switch (command.keymode)
	{
		case Momentary: return "KM";
		case Latching: return "KL";
		case Auto: return "KA";
		default: return "";
	}
}

bncs_string CCommand::GetDim(const CCommand& command)
{
	switch (command.dim)
	{
	case No: return "D0";
	case Yes: return "D1";
	default: return "";
	}
}

bncs_string CCommand::GetFullCommand(const CCommand& command)
{
	bncs_stringlist funcs;
	bncs_stringlist full;
	for (auto f = command.functions.begin(); f != command.functions.end(); ++f)
	{
		funcs << bncs_string("%1|%2|%3").arg(function::GetCommand(f->Command)).arg(f->Value).arg(function::GetFlags(*f));
	}
	full << funcs.toString();
	full << command.marker;
	full << command.label;

	bncs_stringlist props;
	if(command.keymode != KeyMode::KeyMode_NotSet)
		props << CCommand::GetKeymode(command);
	if(command.dim != Dim_NotSet)
		props << CCommand::GetDim(command);

	full << props.toString('|');
	full << command.subtitle;
	full << command.colour;

	return full.toString('*');
}