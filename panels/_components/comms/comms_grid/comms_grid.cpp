#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_grid.h"

#define PNL_MAIN	"comms_grid"

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_grid )

// constructor - equivalent to ApplCore STARTUP
comms_grid::comms_grid( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();
	// show a panel from file comms_grid.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "comms_grid.bncs_ui" );

	//These are the defaults
	m_width = 490;
	m_height = 550;
	m_rows = 5;
	m_cols = 4;
	m_horizontal_spacing = 10;
	m_vertical_spacing = 10;
	m_view_port_type = ENUM_PORT_VIEWS::VIEW_PORT_TYPE_INPUT;

	m_offset = 0;
	m_page = 0;

	m_buttonCount = 0;
	m_mapping = false;

	m_isDirty = false;
	m_selectedButton = 0;

	m_show_tally = true;

	m_mapping_clicked_waiting_for_index = false;

	m_last_index_received = "";

	timerStart(TIMER_SETUP, 2);
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_grid::~comms_grid()
{
}

// all button pushes and notifications come here
void comms_grid::buttonCallback(buttonNotify *b)
{
	b->dump("comms_grid::buttonCallback");
	//We need to store which the last pressed button was.
	//We also need to tell all the other buttons to unselect.
	if (b->command() == "button" && b->value() == "released")
	{
		if (m_mapping == true)
			m_mapping_clicked_waiting_for_index = true;

		if (b->id() != m_selectedButton)
		{
			if (m_selectedButton != 0)
				textPut("deselected", PNL_MAIN, m_selectedButton);
			m_selectedButton = b->id();
		}
		/*
		for (int i = 1; i <= m_buttonCount; i++)
		{
			if (i != b->id())
				textPut("deselected", PNL_MAIN, i);
		}
		*/
		hostNotify("button=released");
	}
	else if (b->command() == "index")
	{
		if (m_mapping == false)
			hostNotify(bncs_string("index=%1").arg(b->value()));
	}
}

// all revertives come here
int comms_grid::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void comms_grid::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_grid::parentCallback(parentNotify *p)
{
	p->dump("comms_grid::parentCallback");
	if (p->command() == "return")
	{
		if (p->value() == "all")
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;

			sl << bncs_string("width=%1").arg(m_width);
			sl << bncs_string("height=%1").arg(m_height);
			sl << bncs_string("rows=%1").arg(m_rows);
			sl << bncs_string("cols=%1").arg(m_cols);
			sl << bncs_string("horizontal_spacing=%1").arg(m_horizontal_spacing);
			sl << bncs_string("vertical_spacing=%1").arg(m_vertical_spacing);
			sl << bncs_string("view_port_type=%1").arg(getPortType::view_port_type(m_view_port_type));
			sl << bncs_string("offset=%1").arg(m_offset);
			sl << bncs_string("page=%1").arg(m_page);
			sl << bncs_string("mapping=%1").arg(m_mapping ? "true" : "false");
			sl << bncs_string("show_tally=%1").arg(m_show_tally? "true" : "false");
			return sl.toString('\n');
		}

	}

	else if (p->command() == "width")
		setValue(m_width, p->value());
	else if (p->command() == "height")
		setValue(m_height, p->value());
	else if (p->command() == "rows")
		setValue(m_rows, p->value());
	else if (p->command() == "cols")
		setValue(m_cols, p->value());
	else if (p->command() == "horizontal_spacing")
		setValue(m_horizontal_spacing, p->value());
	else if (p->command() == "vertical_spacing")
		setValue(m_vertical_spacing, p->value());
	else if (p->command() == "view_port_type")
	{
		m_view_port_type = getPortType::view_port_type(p->value().lower());
		debug("comms_grid::parentCallback view_port_type:%1-%2", p->value(), m_view_port_type);
		timerStart(TIMER_SETUP, TIMER_SETUP);
	}
	else if (p->command() == "offset")
		setValue(m_offset, p->value());
	else if (p->command() == "page")
	{
		m_page = p->value();		
		//The mapping index to look at
		target(m_page + m_offset);
	}
	else if (p->command() == "mapping")
	{
		bool mapping = p->value().lower() == "true" ? true : false;

		if (m_mapping != mapping)
		{
			m_mapping = mapping;
			timerStart(TIMER_SETUP, TIMER_SETUP);
		}
	}
	else if (p->command() == "show_tally")
	{
		m_show_tally = p->value().lower() == "true";
		timerStart(TIMER_SETUP, TIMER_SETUP);
	}
	//Commands
	else if (p->command() == "_commands")
	{
		bncs_stringlist sl;

		sl << bncs_string("save");
		sl << bncs_string("cancel");
		sl << bncs_string("index=<ring.port>");
		sl << bncs_string("deselect");

		return sl.toString('\n');
	}

	//Mapping
	else if (p->command() == "save")
	{
		save(m_page + m_offset);
	}
	else if (p->command() == "cancel")
	{
		cancel();
	}
	else if (p->command() == "deselect")
	{
		if (m_selectedButton != 0)
		{
			textPut("deselected", PNL_MAIN, m_selectedButton);
			m_selectedButton = 0;
		}
	}
	else if (p->command() == "index")
	{
		//Index will be ring.port
		bncs_string index = m_last_index_received;
		m_last_index_received = p->value();
		map(index);
	
		m_mapping_clicked_waiting_for_index = false;
	}
	else if (p->command() == "take")
	{
		debug("comms_grid::parentCallback take:'%1' to button %2", p->value(), m_selectedButton);
		bncs_string s = "take";
		for (int i = 0; i < p->subs(); ++i)
		{
			s += "." + p->sub(i);
		}
		textPut(s, p->value(), PNL_MAIN, m_selectedButton);
	}
	else if (p->command() == "undo")
	{
		textPut("undo", PNL_MAIN, m_selectedButton);
	}

	return "";
}

// timer events come here
void comms_grid::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
	
		timerStop(id);
		load();
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void comms_grid::setValue(int &value, int newValue)
{
	if (newValue != value)
	{
		value = newValue;

		timerStart(TIMER_SETUP, TIMER_SETUP);
	}
}

void comms_grid::load()
{
	//Check that we have all the values we need
	if (!(m_width > 0 && m_height > 0 && m_rows > 0 && m_cols > 0))
		return;

	draw();


}

void comms_grid::draw()
{
	setSize(m_width, m_height);

	m_buttonCount = m_rows * m_cols;

	//Get button widths
	double buttonWidth  = (m_width - ((m_cols - 1) * m_horizontal_spacing)) / m_cols;
	double buttonHeight = (m_height - ((m_rows - 1)  * m_vertical_spacing)) / m_rows;

	int index = 1;
	double x = 0;
	double y = 0;

	for (int r = 0; r < m_rows; ++r)
	{
		double x = 0;
		for (int c = 0; c < m_cols; ++c)
		{
			controlCreate(PNL_MAIN, index, "bncs_script_loader", (int)x, (int)y, (int)buttonWidth, (int)buttonHeight, bncs_string("scaleWithCanvas=true\nscriptname=/_components/comms/comms_port\nring_port=\nview_port_type=%1\nshow_tally=%2").arg(getPortType::view_port_type(m_view_port_type)).arg(m_show_tally?"true":"false"));

			index++;
			x = x + m_horizontal_spacing + buttonWidth;
		}
		y = y + m_vertical_spacing + buttonHeight;
	}

	target(m_page + m_offset);
}


void comms_grid::target(int index)
{
	hostNotify(bncs_string("index=-1"));
	if (index == 0 || rh->GetRingmasterDevices().Main == 0)
	{
		if (m_mapping == false)
		{
			for (int i = 1; i <= m_buttonCount; ++i)
			{
				//Disable all if we have no index
				controlDisable(PNL_MAIN, i);
			}
		}
		return;
	}

	bncs_string mapping;
	routerName(rh->GetRingmasterDevices().Main, riedelHelpers::databases::Mapping, index, mapping);
	debug("comms_grid::target index:%1 mapping:%2", index, mapping);

	vector<ringport>old_mapping;
	for (vector<ringport>::iterator it = m_mapping_ports.begin(); it != m_mapping_ports.end(); ++it)
	{
		debug(bncs_string("comms_grid::target Copy old:%1").arg(it->getRingPort()));
		old_mapping.push_back(*it);
	}

	m_mapping_ports.clear();

	//Parsing the router mapping as there are no commas.
	bncs_string s;
	for (unsigned int i = 0, j = 1; i < mapping.length(); ++i, ++j)
	{
		s.append(mapping[(int)i]);
		if (j % 6 == 0)
		{
			m_mapping_ports.push_back(ringport(s));
			s = bncs_string();
		}
	}

	//Add others that may not be mapped as blank
	m_mapping_ports.resize(m_buttonCount);
//	for (int i = mapping.length(); i < m_buttonCount; i++)
//	{
//		m_mapping_ports.push_back(ringport());
//	}

	vector<ringport>::iterator it = m_mapping_ports.begin();
	for (int but = 1; but <= m_buttonCount; ++but)
	{
		//This just ensure that we only update the bits that havn't changed
		bncs_string old = "";
		if ((int)old_mapping.size() < but && m_mapping == false)
			controlDisable(PNL_MAIN, but); //Disable those the first time round when mapping is false
		else if ((int)old_mapping.size() >= but)
			old = old_mapping[but - 1].getRingPort();

		if (it != m_mapping_ports.end())
		{
			bncs_string rp = it->getRingPort();
			debug("comms_grid::target but:%1 RingPort:%2", but, rp);

			//Only write to screen if its different than what was last there
			if (old != rp )
			{

				if (rp.length() > 0)
				{
					textPut("ring_port", rp, PNL_MAIN, but);
					if (old.length() == 0 && m_mapping == false)
					{
						debug("comms_grid:: HEREH");
						//Enable if previously 0
						controlEnable(PNL_MAIN, but);
					}
				}
				else
				{
					textPut("ring_port", "", PNL_MAIN, but);
					if (m_mapping == false)
						controlDisable(PNL_MAIN, but);
				}
			}

			it++;
		}
		else
		{
			if (old.length() > 0)
			{
				textPut("ring_port", "", PNL_MAIN, but);
				if (m_mapping == false)
				{
					controlDisable(PNL_MAIN, but);
				}
			}
		}
	}
	m_mapping_ports.resize(m_buttonCount);
}

void comms_grid::save(int index)
{
	//debug("comms_grid::save");
	if (m_mapping == false)
		return;

	bncs_string rm = "";

	int i = 1;
	for (vector<ringport>::iterator it = m_mapping_ports.begin(); it != m_mapping_ports.end(); ++it)
	{
		rm.append(it->getRingPortCompressed());
		i++;
		//debug("comms_grid::save rm:%1-%2.%3", i, it->ring, it->port);
	}
	debug("comms_grid::save rm:%1",rm);
	routerModify(rh->GetRingmasterDevices().Main, riedelHelpers::databases::Mapping, index, rm, true);
}


void comms_grid::cancel()
{
	if (m_mapping == false)
		return;

	target(m_page + m_offset);
	m_isDirty = false;
}

void comms_grid::map(bncs_string index)
{
	if (m_mapping == false || m_selectedButton == 0 || m_page == 0 || m_mapping_clicked_waiting_for_index == false)
		return;

	if (m_isDirty == false)
	{
		m_isDirty = true;
		hostNotify("dirty");
	}

	ringport r = ringport(index, false);
	textPut("ring_port",r.getRingPort(), PNL_MAIN, m_selectedButton);
	m_mapping_ports[m_selectedButton - 1] = r;
}