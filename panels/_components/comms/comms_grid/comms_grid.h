#ifndef comms_grid_INCLUDED
	#define comms_grid_INCLUDED

#include <bncs_script_helper.h>
#include <vector>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif

class ringport
{
public:
	int ring;
	int port;

	ringport()
	{
		ring = 0;
		port = 0;
	}

	ringport(bncs_string string, bool fixedWidth = true)
	{
		if (fixedWidth)
		{
			ring = string.left(2);
			port = string.right(4);
		}
		else
		{
			bncs_string sring, sport;
			string.split('.', sring, sport);
			ring = sring;
			port = sport;
		}
	}

	bncs_string getRingPort()
	{
		if (ring + port == 0)
			return bncs_string();
		return bncs_string("%1.%2").arg(ring).arg(port);
	}

	bncs_string getRingPortCompressed()
	{
		if (ring + port == 0)
			return bncs_string("000000");
		return bncs_string("%1%2").arg(ring, '\00', 2U).arg(port, '\0000', 4U);
	}
};

class comms_grid : public bncs_script_helper
{
public:
	comms_grid( bncs_client_callback * parent, const char* path );
	virtual ~comms_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper *rh;
	int m_width;
	int m_height;
	int m_rows;
	int m_cols;
	int m_horizontal_spacing;
	int m_vertical_spacing;
	
	int m_offset;
	int m_page;

	int m_buttonCount;

	void setValue(int &value, int newValue);

	ENUM_PORT_VIEWS m_view_port_type;
	bool m_mapping;

	bool m_isDirty;
	int m_selectedButton;
	vector<ringport> m_mapping_ports;

	void load();
	void draw();

	void target(int index);

	void save(int index);
	void cancel();
	void map(bncs_string index);

	bool m_mapping_clicked_waiting_for_index;
	bncs_string m_last_index_received;

	bool m_show_tally;
};


#endif // comms_grid_INCLUDED