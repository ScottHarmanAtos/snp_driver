# Comms Grid Component

The comms gird has been created to work within a ringmaster system.

It displays a gird of comm_port components.

It has been built to work with connections.

It has been built to work with mapping grids as well.

### Note

An instance does not have to be supplied as the component gets this information from the **riedelHelper.dll**

## Commands

| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| width | Width in Pixels of the Grid |
| height | Height in Pixels of the Grid |
| rows | Number of rows |
| cols| Number of columns |
| horizontal_spacing | Horizontal spacing between the buttons |
| vertical_spacing | vertical spacing between the buttons |
| view_port_type | view_port_type passed to comm_port buttons |
| offset | Offset to use for the mapping |
| page | Page to use for the mapping |
| mapping | Bool, if true will work for button mapping |
| show_tally | Passed to comms_port buttons |
| save | Save button mapping - Only mapping mode |
| cancel | Cancel changes - Only mapping mode |
| deselect | deselect the button |
| index | Pass index to selected button - Only Mapping mode |
| take | Passes take to selected button |
| undo | Passes undo to selected button |

## Notifications

| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| index=RING.PORT | The RING.PORT of the selected comm_port |#
| index=-1 | Nothing selected |
| dirty | Mapping is dirty |
| button=released | A button has been pressed |

## Stylesheets
None