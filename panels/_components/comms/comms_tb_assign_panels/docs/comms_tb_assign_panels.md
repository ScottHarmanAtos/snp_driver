# comms_tb_assign_panels

Based on the the config in `ops_positions.xml` this panel requires the position to have:
- `tb_assign_groups` which will contain a `stringlist` of indices
- `local_ring` will determine which ring the position will use for this component

`tb_assign_groups` will contain:

- First item is the default local panel
- The other possible panels that can be used
  - It will use DB11 which has the panel name
  - DB0 for the labels to put on the button

## Commands

**N/A**

## Notifications

### local_panel
The DB4 config of the local panel that has been selected following the format `sysid=x,type=x`, where type is the panel name.

### local_ringport
The ring port that has been found from the configuration following the format of `ring.port`

### selected_panels
A list of the panel names that the user has selected using the buttons. Panel names found from DB4 type property.

### selected_sysids
A list of the sysids that the user has selected using the buttons. Sysids found from DB4 sysid property.