#ifndef comms_tb_assign_panels_INCLUDED
	#define comms_tb_assign_panels_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_tb_assign_panels : public bncs_script_helper
{
public:
	comms_tb_assign_panels( bncs_client_callback * parent, const char* path );
	virtual ~comms_tb_assign_panels();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper * rh;

	int m_limit;
	int m_offset;

	int m_dev;

	bncs_string m_selected_page;

	int m_conf_local_ring;
	bncs_string m_conf_local_panel;
	bncs_stringlist m_conf_tb_groups;

	bncs_stringlist m_local_panel;
	bncs_string m_local_panel_name;

	bncs_stringlist m_tb_groups;

	bncs_stringlist m_selected_indices;
	bncs_stringlist m_selected_panels;
	bncs_stringlist m_selected_sysids;

	void setupGroups();
	
	void resetButtons();
	void populateButtons();
	void setupPageButtons(int max);

	void toggleActivePage(bncs_string element);

	bncs_string getConfig(bncs_string id, bncs_string attr = "value");

	static const bncs_string attr_text;
	static const bncs_string attr_statesheet;

	static const bncs_string style_selected;
	static const bncs_string style_deselected;

	static const bncs_string btn_page_base;
	static const bncs_string btn_select_base;
};


#endif // comms_tb_assign_panels_INCLUDED