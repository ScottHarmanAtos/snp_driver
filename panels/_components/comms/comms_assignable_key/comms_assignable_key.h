#ifndef comms_assignable_key_INCLUDED
	#define comms_assignable_key_INCLUDED

#pragma warning (disable:4786)
#include <map>

#include <bncs_script_helper.h>
#include <packager_common.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
typedef map<int,  int> MAP_INT_INT;
typedef map<int,  int>::iterator MAP_INT_INT_ITERATOR;

typedef map<int,  bncs_string> MAP_INT_STRING;
typedef map<int,  bncs_string>::iterator MAP_INT_STRING_ITERATOR;

class comms_assignable_key : public bncs_script_helper
{
public:
	comms_assignable_key( bncs_client_callback * parent, const char* path );
	virtual ~comms_assignable_key();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:

	riedelHelper * rh;

	void setIFBAssignment(int intIFB);
	void getMonKey(bncs_string strPanelKeys);
	void setConferenceAssignment(int intConference);
	void showKeyFunction();
	void updateSelectionAppearance();
	void loadLayout(bncs_string layout);
	void setPortAssignment(int port);
	void setFunction(bncs_string command);
	void showMonKeyChannel();

	int getMasterChannelMode(bncs_stringlist list);

	int GetPersistedChannel(int monitor);

	int m_intDeviceRiedelMonitor;
	int m_intDeviceRiedelConference;

	int m_intMonitorSlot;

	bool m_blnSelected;
	bool m_blnWideButton;
	bool m_blnTopFunctionSecondLocalChannel;

	bncs_string m_strPanelKeys;
	bncs_string m_strKeyMode;
	bncs_string m_strKeyFlags;
	bncs_string m_strButtonLabel;
	bncs_string  getFormattedText(bncs_string label);
	void notifyActiveFunctions();

	bncs_stringlist m_sltMonitorSlots;

	bncs_string m_monitorValue;

	comms_rings m_ring;
	enum CommandName
	{
		NA,
		CALLTOPORT,
		TRUNKCALL,
		TRUNKIFB,
		CALLTOGROUP,
		LISTENPORT,
		CONFERENCE,
		IFB,
		LOGIC,
		SENDSTRING
	};

	int m_index;
	int m_device;
	int m_deviceDatabase;

	CommandName m_function;
};


#endif // comms_assignable_key_INCLUDED