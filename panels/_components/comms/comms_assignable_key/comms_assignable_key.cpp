#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "comms_assignable_key.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(comms_assignable_key)

#define PANEL_MAIN	"comms_assignable_key"

// constructor - equivalent to ApplCore STARTUP
comms_assignable_key::comms_assignable_key( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	m_blnSelected = false;
	m_strKeyFlags = DEFAULT_TALK_LISTEN_MODE;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "default.bncs_ui");
	
	m_strKeyMode= "KA|D1";
	m_intDeviceRiedelMonitor = 0;		
	m_intDeviceRiedelConference = 0;			
	m_intMonitorSlot = 0;
	m_ring = 0;


}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_assignable_key::~comms_assignable_key()
{
}

// all button pushes and notifications come here
void comms_assignable_key::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == "key" )
		{
			m_blnSelected = true;
			updateSelectionAppearance();
			hostNotify(bncs_string("selected=%1").arg("true"));
			hostNotify(bncs_string("keymode=%1").arg(m_strKeyMode));
			hostNotify(bncs_string("slots=%1").arg(m_sltMonitorSlots.toString()));
			hostNotify(bncs_string("topchannel=%1").arg(m_blnTopFunctionSecondLocalChannel == true ? "2" : "1"));
			hostNotify(bncs_string("key=%1").arg(m_strPanelKeys));
			notifyActiveFunctions();
			//Unneeded?
			//bncs_stringlist sltPanelKeys(m_strPanelKeys);
			//int intMonitorSlot = routerIndex(m_intDeviceRiedelMonitor, riedelHelpers::databases::Monitor_Lookup, sltPanelKeys[0]);
			//hostNotify(bncs_string("slot=%1").arg(intMonitorSlot));
		}
	}
}

// all revertives come here
int comms_assignable_key::revertiveCallback( revertiveNotify * r )
{
//	debug("comms_assignable_key::revertiveCallback() dev=%1 index=%2 slot=%3", r->device(), r->index(), r->sInfo());
	if( r->device() == m_intDeviceRiedelMonitor && r->index() == m_intMonitorSlot)
	{
		// IFB|301|*0**KM*subtitle*color
		// IFB|301|,Conference|1|TL*0**KM
		// CallToPort|1|S*0**KM
		// {FUNCTION}[,{FUNCTION}]*{MARKER}*{LABEL}*{KEYPROPERTIES}
		// we have a list of functions. Button will display these functions in order of preference.....
		// if the function list has an over-ride label, the label will take precedence
		// if the function to be displayed is call to port  - port alias will take precedence
		// if the function to be displayed is a CONFERENCE, conference label will take precedence
		// if the function to be displayed is an IFB, conference label will take precedence
		
		//Do nothing if it is the same
		if (m_monitorValue == r->sInfo())
			return 0;

		m_monitorValue = r->sInfo();
		notifyActiveFunctions();

		bncs_stringlist sltFullRevertive = bncs_stringlist(r->sInfo(), '*');
		m_strButtonLabel = sltFullRevertive[2];
		bncs_string strFunctionList = sltFullRevertive[0];
		bncs_stringlist sltFunctionList = bncs_stringlist(strFunctionList,',');

		bncs_stringlist sltTopFunctionFields = bncs_stringlist(sltFunctionList[0],'|');
		bncs_string strTopFunction = sltTopFunctionFields[0].upper();
		int index = sltTopFunctionFields[1].toInt(); 
		
		// Driver will assume D1 is default and not send it back.....
		bncs_string strReturnedKeyMode= sltFullRevertive[3];
		if ( strReturnedKeyMode.length() >0 )
		{
			if ( strReturnedKeyMode.length() == 2)
			{
				m_strKeyMode = bncs_string("%1|%2").arg(strReturnedKeyMode.upper()).arg(DEFAULT_DIM_MODE); 
			}
			m_strKeyMode = strReturnedKeyMode;
		}
		else
		{
			m_strKeyMode = "KA|D1";
		}

		// we need to work out if the second channel is live
		bncs_string strTopFunctionFlags = sltTopFunctionFields[2];
		m_blnTopFunctionSecondLocalChannel = (strTopFunctionFlags.find("S") > -1 ) ? true : false;
		debug("comms_assignable_key::checking channel flags=%1", m_blnTopFunctionSecondLocalChannel);

		m_function = NA;
		m_index = 0;
		m_device = 0;
		int database = -1;
		bncs_string functionLabel = "";

		if(strTopFunction == "CONFERENCE" && index > 0)
		{
			m_function = CONFERENCE;
			m_index = index + riedelHelpers::offset::CONF_LABEL;
			m_device = m_intDeviceRiedelConference;
			m_deviceDatabase = m_intDeviceRiedelConference;
			database = riedelHelpers::databases::Conference_Name;
		}
		else if(strTopFunction == "CALLTOPORT")
		{
			m_function = CALLTOPORT;
			m_index = index;
			m_device = m_ring.devices.Alias;
			m_deviceDatabase = m_ring.devices.Grd;
			database = riedelHelpers::databases::Input_Port_Button_Name;
		}
		else if(strTopFunction == "LISTENTOPORT")
		{
			m_function = LISTENPORT;
			m_index = index;
			m_device = m_ring.devices.Alias;
			m_deviceDatabase = m_ring.devices.Grd;
			database = riedelHelpers::databases::Input_Port_Button_Name;
		}
		else if(strTopFunction == "IFB")
		{
			m_function = IFB;
			m_index = index;
			m_device = m_ring.devices.Ifb;
			m_deviceDatabase = -1;
			database = riedelHelpers::databases::IFB_Name;
		}
		else if (strTopFunction == "LOGIC")
		{
			m_function = LOGIC;
			m_index = index;
			m_device = m_ring.devices.Alias;
			m_deviceDatabase = m_ring.devices.Grd;
			database = riedelHelpers::databases::Logic_Name;
		}
		else if (strTopFunction == "TRUNKCALL")
		{
			auto p = rh->GetPort(sltTopFunctionFields[1]);

			m_function = TRUNKCALL;
			m_index = p.slot_index;
			m_device = p.devices.Alias;
			m_deviceDatabase = p.devices.Grd;
			database = riedelHelpers::databases::Input_Port_Button_Name;
		}
		else if (strTopFunction == "TRUNKIFB")
		{
			m_function = TRUNKIFB;
			bncs_string sRing, port;
			sltTopFunctionFields[1].split('.', sRing, port);
			auto ring = rh->GetRing(sRing.toInt());

			m_device = ring.devices.Ifb;
			m_deviceDatabase = -1;
			m_index = port;
			database = riedelHelpers::databases::IFB_Name;
		}

		if (m_function != NA)
		{
			infoRegister(m_device, m_index, m_index, false);
			infoPoll(m_device, m_index, m_index);
			if (m_deviceDatabase != -1)
			{
				routerName(m_deviceDatabase, database, m_index, functionLabel);
			}
			else
			{
				functionLabel = m_index;
			}
			functionLabel = getFormattedText(functionLabel);
		}

		bncs_string strIconTopLeft = bncs_string("%1.png").arg(m_strKeyMode.remove(m_strKeyMode.find('|'),1));
		textPut("pixmap.topLeft", strIconTopLeft, PANEL_MAIN, "key");

		textPut("text", functionLabel, PANEL_MAIN, "key");
		showKeyFunction();
	}
	else if (r->device() == m_device && r->index() == m_index)
	{
		bncs_string functionLabel;
		switch (m_function)
		{
		case CONFERENCE:
		{
			auto str = r->sInfo();
			if (str.stripWhiteSpace().length() > 0)
			{
				functionLabel = str.left(8);
			}
			else
			{
				int currentConerence = m_index - riedelHelpers::offset::CONF_LABEL;
				bncs_string strConferenceLabel;
				routerName(m_deviceDatabase, riedelHelpers::databases::Conference_Name, currentConerence, functionLabel);

				if (functionLabel == "---")
				{
					functionLabel = bncs_string("CONF %1").arg(currentConerence);
				}
			}
		}
		break;
		case IFB:
		case TRUNKIFB:
		{
			r->dump("sdfsd");
			bncs_stringlist slt = bncs_stringlist(r->sInfo(), '|');
			if (slt[3].simplifyWhiteSpace().length())
			{
				functionLabel = slt[3];
			}
			else
			{
				functionLabel = bncs_string("ifb %1").arg(m_index);
			}
		}
		break;
		case CALLTOPORT:
		case LISTENPORT:
		case TRUNKCALL:
		{
			bncs_string strName = r->sInfo();
			if (strName.length() == 0)
			{
				routerName(m_deviceDatabase, riedelHelpers::databases::Input_Port_Button_Name, m_index, strName);
			}
			functionLabel = strName;
		}
		break;
		}

		functionLabel = getFormattedText(functionLabel);
		textPut("text", functionLabel, PANEL_MAIN, "key");
	}
	return 0;
}

// all database name changes come back here
void comms_assignable_key::databaseCallback( revertiveNotify * r )
{

}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_assignable_key::parentCallback( parentNotify *p )
{
	debug("comms_assignable_key::parentCallback() %1='%2'", p->command(), p->value());
	
	if(p->command() == "key")
	{
		//textPut("text", p->value(), PANEL_MAIN, "key");
		if (p->value() != m_strPanelKeys)
		{
			m_strPanelKeys = p->value();

			m_blnSelected = false;
			updateSelectionAppearance();

			getMonKey(m_strPanelKeys);
			showMonKeyChannel();
		}
	}
	else if(p->command() == "select")
	{
		m_blnSelected = true;
		debug("key deslected");
		updateSelectionAppearance();
	}
	else if(p->command() == "deselect")
	{
		m_blnSelected = false;
		debug("key deslected %1");
		updateSelectionAppearance();
	}
	else if(p->command() == "clear")
	{
		setConferenceAssignment(0);
	}
	else if(p->command() == "assign")
	{
		setConferenceAssignment(p->value().toInt());
	}
	else if(p->command() == "function")
	{
		if (m_blnSelected)
		{
			setFunction(p->value());
		}
	}
	else if(p->command() == "clear")
	{
		p->dump("clear received::");
		if (p->value() == "key")
		{
			if (m_blnSelected)
			{
				setFunction("&CLEAR");
			}
		}
		else if ( p->value() == "all")  // we don't care if we are selected
		{
			setFunction("&CLEAR");
		}
	}
	
	else if(p->command() == "conference")
	{
		setConferenceAssignment(p->value().toInt());
	}
	else if(p->command() == "clear_ifb")
	{
		setIFBAssignment(0);
	}
	else if(p->command() == "ifb")
	{
		setIFBAssignment(p->value());
	}
	else if(p->command() == "calltoport")
	{
		
		//  CALLTOPORT
		//	Creates a �Call to Port� function on the key.
		//  Value: The BNCS port number to target.
		//  Flags: �U� enables the Riedel's �AutolistenFromDest� option which automatically creates a listen-to-port function on the key.
		
		// this bit is just here to clean up the function call if it contains any flags
		bncs_stringlist sltCallToPort = bncs_stringlist(p->value(),'|');
		int intPort = sltCallToPort[0].toInt();
		bncs_string strPortType;
		routerName(m_ring.devices.Grd, riedelHelpers::databases::Port_Type, intPort, strPortType);

		
		if ( m_strKeyFlags.find("TL") > -1 || m_strKeyFlags.find("L") > -1 )
		{
			if ( strPortType == PORT_TYPE_4WIRE ) 
			{
				m_strKeyFlags ="U";
			}
		}

		setPortAssignment(intPort);
	}
	else if (p->command() == "keymode")
	{
		m_strKeyMode= p->value();
		debug("new keymode %1", m_strKeyMode);

		infoWrite(m_intDeviceRiedelMonitor, bncs_string("&KEYPROPERTIES=%1").arg(m_strKeyMode), m_intMonitorSlot);

		
//		bncs_string strSVGFile = bncs_string("%1.svg").arg(m_strKeyMode);
//		textPut("svg.file", strSVGFile, PANEL_MAIN, "key");

	}
	else if (p->command() == "talk_mode")
	{
		m_strKeyFlags = p->value();
	}
	else if ( p->command() == "return")
	{
		bncs_stringlist slt;
		return slt.toString('\n');
	}
	return "";
}

// timer events come here
void comms_assignable_key::timerCallback( int id )
{
}

void comms_assignable_key::getMonKey(bncs_string strPanelKeys)
{
	//Clear everything
	textPut("pixmap.topLeft", "", PANEL_MAIN, "key");
	textPut("pixmap.bottomRight", "", PANEL_MAIN, "key");
	textPut("pixmap.bottomLeft", "", PANEL_MAIN, "key");
	textPut("pixmap.topRight", "", PANEL_MAIN, "key");
	textPut("pixmap.centre", "", PANEL_MAIN, "key");
	textPut("text", "", PANEL_MAIN, "key");

	m_function = NA;
	m_index = 0;
	m_device = 0;
	m_intMonitorSlot = 0;
	m_monitorValue = "EMPTY";

	//Expecting ring.port.expansion.shift.key
	bncs_stringlist key(strPanelKeys, '.');

	if (key.count() < 5)
	{
		return;
	}

	int ring = key[0].toInt();
	if (m_ring.ring_id != ring)
	{
		m_ring = rh->GetRing(ring);

		m_intDeviceRiedelMonitor = m_ring.devices.Monitor;
		m_intDeviceRiedelConference = m_ring.devices.Conference;
	}

	m_intMonitorSlot = routerIndex(m_intDeviceRiedelMonitor, riedelHelpers::databases::Monitor_Lookup, bncs_string("%1.%2.%3.%4").arg(key[1]).arg(key[2]).arg(key[3]).arg(key[4]));

	infoRegister(m_intDeviceRiedelMonitor, m_intMonitorSlot, m_intMonitorSlot, false);
	infoPoll(m_intDeviceRiedelMonitor, m_intMonitorSlot, m_intMonitorSlot);
	debug("comms_assignable_key::getMonKeys() Reg %1 %2", m_intDeviceRiedelMonitor, m_intMonitorSlot);
} 

void comms_assignable_key::showKeyFunction()
{
	bncs_string iconBR = "";
	bncs_string iconBL = "";
	bncs_string iconTR = "";
	bncs_string iconC = "";
	bncs_string strPortType;
	switch (m_function)
	{
		case TRUNKCALL:
			iconBR = "/images/comms/icon_trunk.png";
		case CALLTOPORT:
			routerName(m_deviceDatabase, riedelHelpers::databases::Port_Type, m_index, strPortType);
			if (strPortType == "5" || strPortType == "6")
			{
				iconBL = riedelHelpers::icons::PANEL;
			}
			else if (strPortType == "4")
			{
				iconBL = riedelHelpers::icons::_4WIRE;
			}
		break;
		case LISTENPORT:
			iconBL = riedelHelpers::icons::PANEL;
			break;
		case TRUNKIFB:
			iconBR = "/images/comms/icon_trunk.png";
		case IFB:
			iconBL = riedelHelpers::icons::IFB;
			break;
		case LOGIC:
			iconBL = riedelHelpers::icons::LOGIC;
			break;
		case CONFERENCE:
			iconBL = riedelHelpers::icons::CONF;
	}

	if (m_blnTopFunctionSecondLocalChannel)
	{
		iconTR = riedelHelpers::icons::LOCAL_CH2;
	}


	textPut("pixmap.bottomRight", iconBR, PANEL_MAIN, "key");
	textPut("pixmap.bottomLeft", iconBL, PANEL_MAIN, "key");
	textPut("pixmap.topRight", iconTR, PANEL_MAIN, "key");
	textPut("pixmap.centre", iconC, PANEL_MAIN, "key");

}

void comms_assignable_key::updateSelectionAppearance()
{
	textPut("statesheet", m_blnSelected?"source_selected":"source_deselected", PANEL_MAIN, "key");
}

void comms_assignable_key::setConferenceAssignment(int intConference)
{	

	//bncs_string strAssignment = "*0*  ....  *";
	bncs_string strAssignment = "&CLEAR";

	if(intConference > 0)
	{
		strAssignment = bncs_string("CONFERENCE|%1|%2*0**%3").arg(intConference).arg(m_strKeyFlags).arg(m_strKeyMode);
	}
	debug("comms_assignable_key::setConferenceAssignment() strAssigment=%1", strAssignment);

	infoWrite(m_intDeviceRiedelMonitor, strAssignment, m_intMonitorSlot);

}

void comms_assignable_key::setIFBAssignment(int intIFB)
{
	//Send to Packager IFB Key assignment

	bncs_string strAssignment = "&CLEAR";
		
	if(intIFB > 0)
	{
		strAssignment = bncs_string("IFB|%1|%2*0**%3").arg(intIFB).arg(m_strKeyFlags).arg(m_strKeyMode);

	}

	infoWrite(m_intDeviceRiedelMonitor, strAssignment, m_intMonitorSlot);

}

void comms_assignable_key::loadLayout( bncs_string layout )
{
	panelDestroy(PANEL_MAIN);
	panelShow(PANEL_MAIN, bncs_string("%1.bncs_ui").arg(layout));
//	setSize(PANEL_MAIN);
}

bncs_string comms_assignable_key::getFormattedText( bncs_string label )
{
	return label;
}

void comms_assignable_key::setPortAssignment( int port)
{
	bncs_string strAssignment ="";
	if(m_ring.devices.Grd > 0)
	{
		if(port > 0)
		{
			strAssignment = bncs_string("CALLTOPORT|%1|%2*0**%3").arg(port).arg(m_strKeyFlags).arg(m_strKeyMode);
		}
		debug("comms_assignable_key::setPOrtAssignment() strAssigment=%1", strAssignment);
		infoWrite(m_intDeviceRiedelMonitor, strAssignment, m_intMonitorSlot);

	}
}

void comms_assignable_key::setFunction( bncs_string command )
{

	if(command.length()> 0)
	{
		infoWrite(m_intDeviceRiedelMonitor, command, m_intMonitorSlot);
	}
	
}

void comms_assignable_key::showMonKeyChannel()
{
	int c = getMasterChannelMode(m_sltMonitorSlots);
	if (c == 1 || c== 2)
	{
		bncs_string strChannelIcon = bncs_string("icon_channel_%1.png").arg(c);
		textPut("pixmap.topRight", strChannelIcon, PANEL_MAIN, "key");
	}
	else 
	{
		// this is an error!!!
		bncs_string strChannelIcon = ""; //  "icon_error.png";
		textPut("pixmap.topRight", strChannelIcon, PANEL_MAIN, "key");
	}
	//	debug("comms_assignable_key::CheckSecondChannel(%1) - %2", monitor, strChannel);
	//	if (strChannel == "1")
	//	{
	//		bncs_string strChannelIcon = "icon_channel_1.png";
	//		textPut("pixmap.bottomRight", strChannelIcon, PANEL_MAIN, "key");
	//	}
	//	else if (strChannel == "2")
	//	{
	//		bncs_string strChannelIcon = "icon_channel_2.png";
	//		textPut("pixmap.bottomRight", strChannelIcon, PANEL_MAIN, "key");
	////		bncs_string strSVGFile = "second_channel.svg";
	////		textPut("svg.file", strSVGFile, PANEL_MAIN, "key");
	//	}
	//	else // i.e. doesn't exist
	//	{
	//		routerModify(m_intDeviceRiedelMonitor, "1", monitor, strChannel, true);
	//		strChannel = "1";
	//		textPut("pixmap.bottomRight", "", PANEL_MAIN, "key");
	//	}

}
int comms_assignable_key::GetPersistedChannel(int monitor)
{
	bncs_string strChannel = "";
	routerName(m_intDeviceRiedelMonitor, riedelHelpers::databases::Monitor_Channel, monitor, strChannel);
	if (strChannel == "1")
	{
		return 1;
	}
	else if (strChannel == "2")
	{
		return 2;
	}
	else // i.e. doesn't exist
	{
		routerModify(m_intDeviceRiedelMonitor, riedelHelpers::databases::Monitor_Channel, monitor, "1",  true);
		return 1;
	}


}

/// <summary>
/// Determines whether channels of all keys are the same.
/// </summary>
/// <param name="list">list of channel modes</param>
/// <returns></returns>
int comms_assignable_key::getMasterChannelMode(bncs_stringlist list)
{
	debug("comms_assignable_key::getMasterChannelMode(%1)", list.toString());
	bncs_string test = list[0];
	if (list.count() > 1)
	{
		for (int i = 1; i < list.count(); i++)
		{
			if (list[i] == test)
			{
				test = list[i];
			}
			else
			{
				debug("comms_assignable_key::getMasterChannelMode(%1) returns %2", list.toString(), MODE_MIXED);
				return MODE_MIXED;
			}
		}
		// last value is the same as all other values
		return test.toInt();
	}
	else if (list.count() == 1)
	{
		// return our only value
		debug("comms_assignable_key::getMasterChannelMode(%1) returns %2", list.toString(), test.toInt());
		return test.toInt();
	}
	else
	{
		// return unknown value
		debug("comms_assignable_key::getMasterChannelMode(%1) returns %2", list.toString(), MODE_UNKNOWN);

		return MODE_UNKNOWN;
	}

}

void comms_assignable_key::notifyActiveFunctions()
{
	if (m_blnSelected)
	{
		hostNotify(bncs_string("active_functions=%1").arg(m_monitorValue));
	}
}
