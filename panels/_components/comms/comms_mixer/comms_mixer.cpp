#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "comms_mixer.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_mixer )

#define PANEL_MAIN				"comms_mixer"
#define POPUP_SELECT_INPUT		"comms_mixer_select_input_popup"
#define POPUP_KEYBOARD			"comms_mixer_keyboard_popup"
#define POPUP_PRESET_GAIN		"comms_mixer_preset_gain_popup"
#define POPUP_SELECT_PREHEAR    "comms_mixer_select_prehear_popup"
#define POPUP_SELECT_OUTPUT     "comms_mixer_select_output_popup"

#define	TIMER_INIT				1
#define	TIMEOUT_INIT			10

#define	TIMER_SLUG				2
#define	TIMEOUT_SLUG			10000


#define GAIN_MAX				12.5f
#define GAIN_MIN				-12.5f
#define GAIN_STEP				2.0f  // NB this is tiny nudge
#define GAIN_DEFAULT_PRESETS    "12,6,3,0,-3,-6,-10,-20"

#define IFB_REVERTIVE_ARG_COUNT 4
#define IFB_REVERTIVE_LABEL_ARG 3

#define DEFAULT_LAYOUT    "unknown"
#define MAX_INPUTS              10
#define MAX_OUTPUTS              10

// constructor - equivalent to ApplCore STARTUP
comms_mixer::comms_mixer( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();
	
	m_intDeviceRouter = rh->GetRingmasterDevices().Main;
	m_intDeviceIfb = rh->GetRingmasterDevices().Ifbs;
	m_intDeviceExtras = rh->GetRingmasterDevices().Main;

	//init vars
	m_intIfb = 0;
	m_intMixer = 0;
	m_intDeviceMonitor = 0;
	m_intMixerSlot = 0;
	m_intMonitorSlot = 0;
	m_strInstance = "";
	m_blnShowNickname = false;
	m_strLayout = DEFAULT_LAYOUT;
	m_intPresetGain  = 0; 
	m_intPreselectedMixerInput = 0;
	m_intSelectedDest =0;
	m_intOutputCount = MAX_OUTPUTS;
	m_intDefaultPrehearPort = 0 ;
	m_blnMonitorAvailable = false;
	m_blnMonitorActive = false;
	m_intFirstUnusedOutput = -1;
	m_strMixerSetup = "";
	m_strNewMixerSetup = "";

	for(int intInput = 0; intInput <=4; intInput++)
	{
		m_intInputPort[intInput] = comms_ringmaster_port();
		m_intDefaultInputPort[intInput] = comms_ringmaster_port();
		m_intOutputPort[intInput] = comms_ringmaster_port();
		m_intDefaultOutputPort[intInput] = comms_ringmaster_port();
	}

	m_sltDefaultInputGains = bncs_stringlist("NULL,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0"); // first element is ignored, so we work 1-based
	m_strMixerName = "Mixer Name";
//	m_sltInputLabels = bncs_stringlist("PTB,PROG,TALK-|THROUGH,ASSIGN");

	
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, bncs_string("layouts\\%1.bncs_ui" ).arg(m_strLayout));
//	setSize( PANEL_MAIN );				// set the size to the same as the specified panel
	m_intInputCount = getIdList(PANEL_MAIN, "input_").count();

//	debug("comms_mixer::comms_mixer() m_sltInputPorts=%1", m_sltInputPorts.toString());
//	debug("comms_mixer::comms_mixer() m_sltInputLabels=%1", m_sltInputLabels.toString());

	//hide debug label
	controlHide(PANEL_MAIN, "debug");
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_mixer::~comms_mixer()
{
}

// all button pushes and notifications come here
void comms_mixer::buttonCallback( buttonNotify *b )
{
//	debug("comms_mixer::buttonCallback() panel=%1 id=%2 command=%3 value=%4", b->panel(), b->id(), b->command(), b->value());
	bncs_stringlist sltControlName = bncs_stringlist(b->id(), '_');
	bncs_string strControlPrefix = sltControlName[0];
	int intNewControlIndex = sltControlName[1].toInt();

	if( b->panel() == PANEL_MAIN )
	{
		textPut("index", -1, PANEL_MAIN, "output_gain");
		if(b->id() == "restore_default_outputs")
		{
			setDefaultOutputPorts();
			buttonEnable(false);		
		}
		else if(b->id() == "monitor")
		{
			toggleMonitor();
		}
		else if( strControlPrefix == "input" )
		{
			//Get button label
			bncs_string strLabel;
			textGet("text", PANEL_MAIN, b->id(), strLabel);
			if(strLabel.lower() == "select")
			{
				panelPopup(POPUP_SELECT_INPUT, "popup_select_input.bncs_ui");

				textPut("text", intNewControlIndex, POPUP_SELECT_INPUT, "mixer_input");
			}
			else if(strLabel == "Restore|Default")
			{
				//Get the default input
				auto port = m_intDefaultInputPort[intNewControlIndex];
				
//				debug("input %1 default port =%2", intNewControlIndex, intDefaultPort);
				if(port.valid)
				{
					setInputPort(port, intNewControlIndex);
				}
			}
		}
		else if( strControlPrefix == "up" )
		{
			setInputGain(intNewControlIndex, "UP");
		}
		else if( strControlPrefix == "down" )
		{
			setInputGain(intNewControlIndex, "DOWN");
		}
		else if( strControlPrefix == "unity" )
		{
			setInputGain(intNewControlIndex, "UNITY");
//			setInputGain(intNewControlIndex, bncs_string("SET=%1").arg(m_sltDefaultInputGains[intNewControlIndex]));

		}
		else if( strControlPrefix == "mute" )
		{
			setInputGain(intNewControlIndex, "MUTE");
		}
		else if( b->id() == "edit_nickname" )
		{
			if ( m_intMixer > 0)
			{
				bncs_string strReturn;
				textGet("text", PANEL_MAIN, "mixer_nickname", strReturn);
				panelPopup(POPUP_KEYBOARD, "popup_keyboard.bncs_ui");
				if (strReturn.length() > 0 )
				textPut("text", strReturn, POPUP_KEYBOARD,  "keyboard") ;
				bncs_config cfgIfbLabel = bncs_config(bncs_string("comms_tb_mixers.%1.label").arg(m_intMixer));
				bncs_string strLabel  = cfgIfbLabel.attr("value");
				if (strLabel.length())
				{
					controlEnable(POPUP_KEYBOARD, "reset_label");
					textPut("text", strLabel, POPUP_KEYBOARD,  "default_label") ;
				}
				else
				{
					controlDisable(POPUP_KEYBOARD, "reset_label");
					textPut("text", "No Default Label", POPUP_KEYBOARD,  "default_label") ;

				}
			}
		}
		else if (strControlPrefix == "gain")
		{
			m_intPreselectedMixerInput = intNewControlIndex;
			if ( getCurrentChannelGain(m_intPreselectedMixerInput) == "MUTE")
			{
				setInputGain(m_intPreselectedMixerInput, "0.0" );
			}
			else
			{
				bncs_string strGain;
				textGet("text", PANEL_MAIN, b->id(), strGain);
				showPresetGainSelector(intNewControlIndex, strGain.toDouble());
			}
			
		}
		else if ( b->id() == "restore_default_inputs")
		{
			setAllInputPortsToDefault(false);
		}
		else if ( b->id() == "restore_default_gains")
		{
			setAllInputPortsToDefault(true);
		}
		else if ( b->id() == "output_list")
		{
			b->dump("tally list");
		//	m_intOutputChannel = b->sub(0).toInt() + 1;
			m_intSelectedDest = -1;
			bncs_stringlist slt(b->value(), ';');
			m_intSelectedDest = slt[0].toInt();
			
//			debug("pos=%1 m_intselectedDest=%2", m_intOutputChannel, m_intSelectedDest);
			if ( m_intSelectedDest > 0 )
			{
				controlEnable(PANEL_MAIN, "output_drop");
				if (m_blnMonitorAvailable)
				{
					controlEnable(PANEL_MAIN, "monitor");
					controlEnable(PANEL_MAIN, "tally_mon");
					if (m_blnMonitorActive)
					{
						infoWrite(m_intDeviceMonitor, bncs_string("%1,D").arg(m_intSelectedDest), m_intMonitorSlot);
					}
				}
				textPut("index", m_intSelectedDest, PANEL_MAIN, "output_gain");
			}
			else
			{
				controlDisable(PANEL_MAIN, "output_drop");
				controlDisable(PANEL_MAIN, "monitor");
				controlDisable(PANEL_MAIN, "tally_mon");
				textPut("index", 0, PANEL_MAIN, "output_gain");

			}
			hostNotify(bncs_string("selected_dest=%1").arg(m_intSelectedDest));
		}
		else if ( b->id() == "output_drop")
		{
			dropOutputPutPort(m_intSelectedDest);
		}
		else if ( b->id() == "output_add")
		{
			panelPopup(POPUP_SELECT_OUTPUT, "popup_select_output.bncs_ui");

			textPut("text", intNewControlIndex, POPUP_SELECT_OUTPUT, "mixer_input");
		}
		else if ( b->id() == "copy")
		{
			hostNotify(bncs_string("setup=%1").arg(m_strMixerSetup));
		}
		else if ( b->id() == "paste")
		{
			infoWrite(m_intDeviceExtras, m_strNewMixerSetup, m_intMixer + riedelHelpers::offset::MIXERS);
			controlDisable(PANEL_MAIN, "paste");
		}
		else if ( b->id() == "prehear")
		{
			panelPopup(POPUP_SELECT_PREHEAR, "popup_select_input.bncs_ui");
			
			textPut("text", intNewControlIndex, POPUP_SELECT_INPUT, "mixer_input");
		}
		else if ( b->id() == "restore_default_prehear")
		{
			bncs_config cfgIfbPrehear = bncs_config(bncs_string("comms_tb_mixers.%1.prehear").arg(m_intMixer));
			infoWrite(m_intDeviceIfb, bncs_string("&INPUT=%1").arg(cfgIfbPrehear.attr("value").toInt()), m_intIfb);
		}
		else if (b->id() == "shortcuts")
		{
			b->dump("shortcuts");
			// &INPUT=1,2
			
			if (b->command().startsWith("&INPUT"))
			{
				//bncs_stringlist sltCommand = bncs_stringlist(b->value(), ',');
				//if (sltCommand.count() ==2 )
			//	{
					
					infoWrite(m_intDeviceExtras, bncs_string("%1=%2").arg(b->command()).arg(b->value()), m_intMixer + riedelHelpers::offset::MIXERS);
				//}
			}
		}
	}
	else if( b->panel() == POPUP_KEYBOARD )
	{
		if (b->id() == "keyboard")
		{
			bncs_string strReturn;
			textGet("text", POPUP_KEYBOARD, "keyboard", strReturn);
			infoWrite(m_intDeviceIfb, bncs_string("&LABEL=%1").arg(strReturn), m_intIfb, true);
			panelDestroy(POPUP_KEYBOARD);
		}
		else if (b->id() == "close")
		{
			panelDestroy(POPUP_KEYBOARD);
		}
		else if (b->id() == "reset_label")
		{
//			bncs_string strNickname= "";
//			routerName(m_intDeviceIfb, DATABASE_MIXER_NICKNAME, m_intIfb, strNickname);
			bncs_config cfgIfbLabel = bncs_config(bncs_string("comms_tb_mixers.%1.label"));

			bncs_string strLabel  = cfgIfbLabel.attr("value");
			if (strLabel.length())
			{
				infoWrite(m_intDeviceIfb, bncs_string("&LABEL=%1").arg(strLabel), m_intIfb, true);
			}
			panelDestroy(POPUP_KEYBOARD);
		}
	}
	else if( b->panel() == POPUP_SELECT_INPUT )
	{
		bncs_string strMixerInput;
		textGet("text", POPUP_SELECT_INPUT, "mixer_input", strMixerInput);
		int intMixerInput = strMixerInput.toInt();

		if (b->id() == "close")
		{
			panelRemove(POPUP_SELECT_INPUT);
		}
		else if (b->id() == "none")
		{
			setInputPort(comms_ringmaster_port(), intMixerInput);
			panelRemove(POPUP_SELECT_INPUT);
		}
		else if (b->id() == "input_port_grid" && b->command() == "index")
		{
			auto port = rh->GetPort(m_ring, b->value().toInt());
			if(port.valid)
			{
				setInputPort(port, intMixerInput);
				panelRemove(POPUP_SELECT_INPUT);
			}
		}
	}
	else if ( b->panel() == POPUP_PRESET_GAIN )
	{
		bncs_string presetGain = m_sltGainPresets[intNewControlIndex-1];
		modifyChannelGain(m_intPreselectedMixerInput, presetGain);
		panelDestroy(POPUP_PRESET_GAIN);
	}
	else if ( b->panel() == POPUP_SELECT_PREHEAR )
	{
		if ( b->id() == "input_port_grid")
		{
			debug("new output port '%1'", b->value());
			if ( b->value().toInt() > 0 )
			{
//				debug("new output port %1", b->value().toInt());
				infoWrite(m_intDeviceIfb, bncs_string ("&INPUT=%1").arg(b->value().toInt()), m_intIfb);
				panelRemove(b->panel());
			}
		}
		else if ( b->id() == "none")
		{
//			debug("new output port %1", 0);
			infoWrite(m_intDeviceIfb, bncs_string ("&INPUT=%1").arg(0), m_intIfb);
			panelRemove(b->panel());
		}
		else if ( b->id() == "close")
		{
			panelRemove(b->panel());
		}
	}
	else if( b->panel() == POPUP_SELECT_OUTPUT )
	{
		bncs_string strMixerOutput;
		textGet("text", POPUP_SELECT_INPUT, "mixer_output", strMixerOutput);
		int intMixerOutput = strMixerOutput.toInt();
		
		if (b->id() == "close")
		{
			panelRemove(POPUP_SELECT_OUTPUT);
		}
		else if (b->id() == "none")
		{
			setOutputPort(0, m_intFirstUnusedOutput);
			panelRemove(POPUP_SELECT_OUTPUT);
		}
		else if (b->id() == "output_port_grid" && b->command() == "index")
		{
//			debug("HERE");
			int intPort = b->value().toInt();
			if(intPort > -1)
			{
				setOutputPort(intPort, m_intFirstUnusedOutput);
				panelRemove(POPUP_SELECT_OUTPUT);
			}
		}
	}

}

// all revertives come here
int comms_mixer::revertiveCallback( revertiveNotify * r )
{
//	debug("comms_mixer::revertiveCallback() device=%1 index=%2 slot=%3", r->device(), r->index(), r->sInfo());
	if(r->device() == m_intDeviceExtras && r->index() == m_intMixerSlot)
	{
		buttonEnable(true);
		bncs_string strDebug = bncs_string("slot %1 = %2").arg(m_intMixerSlot).arg(r->sInfo());

		textPut("text", strDebug.replace('|', '�'), PANEL_MAIN, "debug");
		m_strMixerSetup = r->sInfo();
		
		updateMixer();
	}
	else if(r->device() == m_intDeviceMonitor && r->index() == m_intMonitorSlot)
	{
		m_strCurrentMonitorTarget = r->sInfo();
		updateMonitor();
	}
	else if ( r->device() == m_intDeviceIfb )
	{
		if (r->index() == m_intIfb)
		{
			bncs_stringlist sltIfb(r->sInfo(), '|');
			if (sltIfb.count() == IFB_REVERTIVE_ARG_COUNT )
			{
				bncs_string strNickName = sltIfb[IFB_REVERTIVE_LABEL_ARG];
				textPut("text", strNickName , PANEL_MAIN, "mixer_nickname");
				
				bncs_config cfgIfbLabel = bncs_config(bncs_string("comms_tb_mixers.%1.label").arg(m_intMixer));
				bncs_string strLabel  = cfgIfbLabel.attr("value");				
				if (strNickName==strLabel)
				{
					textPut("stylesheet", "enum_normal", PANEL_MAIN, "mixer_nickname");
				}
				else
				{
					textPut("stylesheet", "enum_warning", PANEL_MAIN, "mixer_nickname");
				}
				int inputPort = sltIfb[0].toInt();
				bncs_string strName;
				routerName(m_intDeviceRouter, riedelHelpers::databases::Input_Port_Button_Name, inputPort, strName);
				textPut("text", strName.replace('|', ' '), PANEL_MAIN, "tally_ph");
				
				
				if ( inputPort == m_intDefaultPrehearPort )
				{
					textPut("stylesheet", "enum_normal", PANEL_MAIN, "tally_ph");
					controlDisable(PANEL_MAIN, "restore_default_prehear");
				}
				else
				{
					textPut("stylesheet", "enum_warning", PANEL_MAIN, "tally_ph");
					controlEnable(PANEL_MAIN, "restore_default_prehear");
				}
			}
			else
			{
				textPut("text", "", PANEL_MAIN, "mixer_nickname");
			}
		}
	}
	return 0;
}

// all database name changes come back here
void comms_mixer::databaseCallback( revertiveNotify * r )
{
	if (r->device() == m_intDeviceExtras && r->database() == riedelHelpers::databases::Mixer_Nickname )
	{
		if  (r->index() == m_intMixer && m_blnShowNickname)
		{
			textPut("text", r->sInfo(), PANEL_MAIN, "mixer_nickname");
			
		}
	}

}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_mixer::parentCallback( parentNotify *p )
{
//	debug("comms_mixer::parentCallback() command=%1 value=%2", p->command(), p->value());
	if (p->command() == "instance")
	{
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if(p->command() == "mixer")
	{
		int intMixer = p->value().toInt();
		if(intMixer > 0 && intMixer <= 100)  // would need to be looked at it the mixer number was changed to be the same as the IFB number
		{
			m_intMixer = intMixer;
			timerStart(TIMER_INIT, TIMEOUT_INIT);
		}
		else
		{
			m_strMixerName = "";
			if(intMixer == 0)
			{
				textPut("text", "Select Mixer", PANEL_MAIN, "mixer_name");
			}
			else if ( intMixer = -1)
			{
				panelShow( PANEL_MAIN, bncs_string("layouts\\%1.bncs_ui" ).arg(m_strLayout));
			//	controlMove(PANEL_MAIN, "message",1280,0);
			}
			else
			{
				textPut("text", bncs_string("mixer=%1").arg(m_intMixer), PANEL_MAIN, "mixer_name");
			}
		}
	}
	else if ( p->command() == "setup" )
	{
		//get existing mixer setup
		bncs_stringlist sltThisMixerInputsOutputs(m_strMixerSetup, '|');
		bncs_stringlist sltThisMixerInputs(sltThisMixerInputsOutputs[0]);
		bncs_stringlist sltThisMixerOutputs(sltThisMixerInputsOutputs[1]);
		int intThisMixerInputCount = sltThisMixerInputs.valueCount(":");
		int intThisMixerOutputCount = sltThisMixerInputs.valueCount(":");
		
		//get copied mixer setup
		bncs_stringlist sltCopyMixerInputsOutputs(p->value(), '|');
		bncs_stringlist sltCopyMixerInputs(sltCopyMixerInputsOutputs[0]);
		bncs_stringlist sltCopyMixerOutputs(sltCopyMixerInputsOutputs[1]);
		int intCopyMixerInputCount = sltCopyMixerInputs.valueCount(":");
		int intCopyMixerOutputCount = sltCopyMixerInputs.valueCount(":");
		
		//check if both mixers have the same input count  -i.e. are they compatible?
		if ( intThisMixerInputCount == intCopyMixerInputCount && intThisMixerOutputCount == intCopyMixerOutputCount)
		{
//			debug("mixer configurations match");
			//replace input params
			bncs_string strNewMixerSetup = bncs_string("%1|%2").arg(sltCopyMixerInputs.toString()).arg(sltThisMixerOutputs.toString());
//			debug("newsetup=%1", strNewMixerSetup);
			m_strNewMixerSetup = strNewMixerSetup ;
			controlEnable(PANEL_MAIN, "paste");
		}
		else
		{
//			debug("mixer configurations DO NOT match");
		}
	}
	else if (p->command() == "show_nickname")
	{
		m_blnShowNickname = ((p->value().lower() == "true") || (p->value().lower() == "1")) ? true : false;
	}
	else if (p->command() == "layout")
	{
		if (p->value() != m_strLayout)
		{
			m_strLayout = p->value();
			panelDestroy(PANEL_MAIN);
			panelShow( PANEL_MAIN, bncs_string("layouts\\%1.bncs_ui" ).arg(m_strLayout));
			setSize(PANEL_MAIN);
			m_intInputCount = getIdList(PANEL_MAIN, "input_").count();
		}

	}

	//Return Commands
	else if( p->command() == "return" )
	{
		if(p->value() == "all")
		{
			bncs_stringlist sltReturn;
			sltReturn << bncs_string( "mixer=" ) + bncs_string(m_intMixer);
			sltReturn << bncs_string( "show_nickname=" ) + bncs_string(m_blnShowNickname ?"true":"false");
			sltReturn << bncs_string("layout=") + bncs_string(m_strLayout);
			return sltReturn.toString('\n');
		}
	}
	return "";
}

// timer events come here
void comms_mixer::timerCallback( int id )
{
	if(id == TIMER_INIT)
	{
		timerStop(TIMER_INIT);
		initMixer();
		initMonitor();
	}
	else if ( id == TIMER_SLUG)
	{
		buttonEnable(true);
	}
}

void comms_mixer::initMixer()
{
//	debug("comms_mixer::initMixer(mixer id=%1)", m_intMixer);
	if(m_intMixer > 0 && m_intDeviceExtras > 0)
	{
		
		//Get Mixer Settings
// 	<mixer id="1">
// 		<setting id="name" value="PCR A Presenter TB 1" />
// 		<setting id="input_1" label="A DIR" port="5" defaultgain="mute" />
// 		<setting id="input_2" label="A PA" port="4" defaultgain="mute" />
// 		<setting id="input_3" label="A VMIX" port="12" defaultgain="mute" />
// 		<setting id="input_4" label="A PROD" port="13" defaultgain="mute" />
// 		<setting id="input_5" label="A PTB" port="397" defaultgain="0.0" />
// 		<setting id="input_6" label="AIEMACF" port="465" defaultgain="0.0" />
// 		<setting id="input_7" label="" port="manual" defaultgain="0.0" />
// 		<setting id="output_1" port="357" />
// 		<setting id="output_2" port="0" />
// 		<setting id="output_3" port="0" />
// 		<setting id="output_4" port="0" />
// 		<setting id="output_5" port="0" />
// 		<setting id="output_6" port="0" />
// 		<setting id="output_7" port="0" />
// 		<setting id="output_8" port="0" />
// 		<setting id="output_9" port="0" />
// 		<setting id="output_10" port="0" />
// 		<setting id="ifb" value="501" />
// 		<setting id="layout" value="7_inputs.bncs_ui" />
// 	</mixer>
		m_ring = 0;
		bncs_config cfgMixer = bncs_config(bncs_string("comms_tb_mixers.%1").arg(m_intMixer));
		if(cfgMixer.isValid())
		{
			bncs_config cfgMixerLayout = bncs_config(bncs_string("comms_tb_mixers.%1.layout").arg(m_intMixer));
			bncs_string strLayout = cfgMixerLayout.attr("value");
			panelDestroy(PANEL_MAIN);
			if ( strLayout.length() >0 )
			{
				panelShow(PANEL_MAIN, bncs_string("layouts\\%1.bncs_ui").arg(strLayout));
				setSize(PANEL_MAIN);
			}
			else
			{
				panelShow(PANEL_MAIN, "layouts\\default.bncs_ui");
				setSize(PANEL_MAIN);
			}
			textPut("index", -1, PANEL_MAIN, "output_gain");
			m_intInputCount = getIdList(PANEL_MAIN, "input_").count();

			bncs_config cfgMRing = bncs_config(bncs_string("comms_tb_mixers.%1.ring").arg(m_intMixer));
			m_ring = cfgMRing.attr("value").toInt();

			COMMS_RINGS_LOOPKUP rings = rh->GetRings();
			COMMS_RINGS_LOOPKUP::const_iterator itRing = rings.find(m_ring);

			if (itRing != rings.end())
			{
				m_intDeviceMonitor = itRing->second.devices.Monitor;
				m_intDeviceMonitorRouter = itRing->second.devices.Grd;
			}

			//register for mixer slot
			m_intMixerSlot = riedelHelpers::offset::MIXERS + m_intMixer;
			infoRegister(m_intDeviceExtras, m_intMixerSlot, m_intMixerSlot);
			infoPoll(m_intDeviceExtras, m_intMixerSlot, m_intMixerSlot);

			bncs_config cfgMixerName = bncs_config(bncs_string("comms_tb_mixers.%1.name").arg(m_intMixer));
			m_strMixerName = cfgMixerName.attr("value");
			textPut("text", bncs_string("  %1").arg(m_strMixerName), PANEL_MAIN, "mixer_name");

			for(int intInput = 1; intInput <= m_intInputCount; intInput++)
			{
				bncs_config cfgMixerInput = bncs_config(bncs_string	("comms_tb_mixers.%1.input_%2").arg(m_intMixer).arg(intInput));
				m_sltInputLabels[intInput - 1] = cfgMixerInput.attr("label");
				textPut("text", m_sltInputLabels[intInput - 1], PANEL_MAIN, bncs_string("grpInput%1").arg(intInput));

				bncs_string strInputPort = cfgMixerInput.attr("port");
				if(strInputPort == "manual")
				{
					textPut("text", "Select", PANEL_MAIN, bncs_string("input_%1").arg(intInput));
					controlShow(PANEL_MAIN, bncs_string("input_%1").arg(intInput));
					m_intDefaultInputPort[intInput] = comms_ringmaster_port();
					m_sltDefaultInputGains[intInput] = "MUTE";
				}
				else
				{

					if(strInputPort.toInt() > 0)
					{
						textPut("text", "Restore|Default",PANEL_MAIN, bncs_string("input_%1").arg(intInput));
						m_intDefaultInputPort[intInput] = rh->GetPort(m_ring, strInputPort.toInt());
						bncs_string strDefaultGainSetting = cfgMixerInput.attr("defaultgain").upper();
						bncs_string strUnityButton = bncs_string("unity_%1").arg(intInput);
						bncs_string strMuteButton = bncs_string("mute_%1").arg(intInput);
					//	controlShow(PANEL_MAIN, strMuteButton);
						if ( (strDefaultGainSetting  == "0.0")  || (strDefaultGainSetting  == "0")  || (strDefaultGainSetting.upper()  == "UNITY") )
						{
							m_sltDefaultInputGains[intInput] = "0";
						}
						else if ( strDefaultGainSetting  == "MUTE"  )
						{
							m_sltDefaultInputGains[intInput] = "MUTE";
//							controlHide(PANEL_MAIN, strMuteButton);
						}
						else 
						{
							m_sltDefaultInputGains[intInput] = strDefaultGainSetting;
						}
//						debug("m_sltDefaultInputGains=%1", m_sltDefaultInputGains.toString());
					//	textPut("text", m_sltDefaultInputGains[intInput], PANEL_MAIN, strUnityButton );

					}
					//TEMP - the restore default button should always be available
				//	controlHide(PANEL_MAIN, bncs_string("input_%1").arg(intInput));

				}

				bncs_string strMixerPort = cfgMixerInput.attr("port");
				//TODO store input ports for validation
			}

			bncs_config cfgMixerIFB = bncs_config(bncs_string("comms_tb_mixers.%1.ifb").arg(m_intMixer));
			m_intIfb = cfgMixerIFB.attr("value").toInt();

			textPut("text", bncs_string("IFB %1").arg( bncs_string(m_intIfb,'0',4)), PANEL_MAIN, "ifb_number");
			debug("mixer %1 ifb=%2", m_intMixer, m_intIfb);
			
			if ( m_blnShowNickname )
			{
				infoRegister(m_intDeviceIfb, m_intIfb, m_intIfb,false);
				infoPoll(m_intDeviceIfb, m_intIfb, m_intIfb);
			}
			else
			{
				controlHide(PANEL_MAIN, "mixer_nickname");
				controlHide(PANEL_MAIN, "edit_nickname");
			}

			bncs_config cfgIfbPrehear = bncs_config(bncs_string("comms_tb_mixers.%1.prehear").arg(m_intMixer));
			if ( cfgIfbPrehear.isValid())
			{
				m_intDefaultPrehearPort = cfgIfbPrehear.attr("value").toInt();
			}
			else
			{
//				controlDisable(PANEL_MAIN, "prehear");
//				controlDisable(PANEL_MAIN, "tally_ph");
			}

		}
		else
		{
			m_strMixerName = "";
			textPut("text", bncs_string("mixer=%1").arg(m_intMixer), PANEL_MAIN, "mixer_name");
		}
		
		bncs_config cfgMixerOutput = bncs_config(bncs_string("comms_tb_mixers.%1").arg(m_intMixer));
		
	
// 		<mixer id="31">
// 			<setting id="name" value="PCR D Presenter TB 1" />
// 			<setting id="input_1" label="D DIR" port="71" defaultgain="mute" />
// 			<setting id="input_2" label="D PA" port="73" defaultgain="mute" />
// 			<setting id="input_3" label="D VMIX" port="74" defaultgain="mute" />
// 			<setting id="input_4" label="D PROD" port="75" defaultgain="mute" />
// 			<setting id="input_5" label="D PTB" port="400" defaultgain="0.0" />
// 			<setting id="input_6" label="DIEMACF" port="537" defaultgain="0.0" />
// 			<setting id="input_7" label="ASSIGN" port="manual" defaultgain="0.0" />
// 		<setting id="output_1" port="369" />
		
		int intValidOutputCount=0;
		for (int intOutput=1 ; intOutput<= m_intOutputCount ; intOutput++) 
		{
			bncs_config cfgMixerOutput = bncs_config(bncs_string("comms_tb_mixers.%1.output_%2").arg(m_intMixer).arg(intOutput));
			
			int output = cfgMixerOutput.attr("port").toInt();
			m_intDefaultOutputPort[intOutput] = rh->GetPort(m_ring, output);
			if (output >-1 ) 
			{
				intValidOutputCount++;
			}

		}
		if (intValidOutputCount>0) 
		{
			controlEnable(PANEL_MAIN,"restore_default_outputs" );
		}
		else
		{
			controlDisable(PANEL_MAIN,"restore_default_outputs" );
		}
		textPut("text", bncs_string("Outputs (%1 Max)").arg(m_intOutputCount), PANEL_MAIN, "grpOutput");
		//debug("got default output ports %1", m_sltDefaultOutputPorts.)
		
		
		//OLD
		//m_intDefaultOutputPort = cfgMixerOutput.attr("port").toInt();

		//textPut("text", bncs_string("out=%1").arg(m_intDefaultOutputPort), PANEL_MAIN, "grpOutput");
		if (m_intMixer == 0 && m_blnShowNickname)
		{
			controlDisable(PANEL_MAIN, "edit_nickname");
		}
		else
		{
			controlEnable(PANEL_MAIN, "edit_nickname");
		}
		//register for mixer slot
		m_intMixerSlot = riedelHelpers::offset::MIXERS + m_intMixer;
		infoRegister(m_intDeviceExtras, m_intMixerSlot, m_intMixerSlot);
		infoPoll(m_intDeviceExtras, m_intMixerSlot, m_intMixerSlot);
//		debug("registering device=%1 slot=%2", m_intDeviceExtras, m_intMixerSlot);

		bncs_string strGainPresets = getObjectSetting("comms_mixers", "preset_gains");
		if ( strGainPresets.length() > 0 )
		{
			m_sltGainPresets = bncs_stringlist (strGainPresets);
		}
		else
		{
			m_sltGainPresets = bncs_stringlist ("");
		}
		bncs_string strGainStep = getObjectSetting("comms_mixers", "gain_steps");
		if (strGainStep.length() > 0)
		{
			m_dblGainStep = strGainStep.toDouble();
		}
		else
		{
			m_dblGainStep = GAIN_STEP;
		}




	}
	else
	{
		m_strMixerName = "";
		if(m_intMixer == 0)
		{
			textPut("text", "Select Mixer", PANEL_MAIN, "mixer_name");
			updateMixer();
		}
		else
		{
			textPut("text", bncs_string("mixer=%1").arg(m_intMixer), PANEL_MAIN, "mixer_name");
		}
	}
	controlDisable(PANEL_MAIN, "paste");
}

void comms_mixer::updateTally(int intInput)
{
	textPut("text", m_intInputPort[intInput].sourceName_short, PANEL_MAIN, bncs_string("tally_%1").arg(intInput));

//	debug("comms_mixer::updateTally() input=%1 port=%2 name=%3", 
//		intInput, m_sltInputPorts[intInput - 1], strInputName);
}

void comms_mixer::initMonitor()
{
	//Get details of local monitor port
	bncs_string strOpsPosition = getWorkstationSetting("ops_position");
	bncs_string strOperatorMonitorPort = "";

	if(m_strInstance == "riedel_grd")
	{
		strOperatorMonitorPort = getOpsSetting(strOpsPosition, "mon_dest_tb");
	}
	int intOperatorMonitorPort = strOperatorMonitorPort.toInt();
//	debug("monitor port=%1", m_intMonitorSlot);

	if(intOperatorMonitorPort > 0)
	{
		//Get the monitor slot for this port
		m_intMonitorSlot = routerIndex(m_intDeviceMonitor, riedelHelpers::databases::Monitor_Lookup, strOperatorMonitorPort);
		if(m_intMonitorSlot > 0)
		{
//			debug("comms_mixer::initMonitor() Register monitorslot=%1", m_intMonitorSlot);
			m_blnMonitorAvailable = true;
			infoRegister(m_intDeviceMonitor, m_intMonitorSlot, m_intMonitorSlot);
			infoPoll(m_intDeviceMonitor, m_intMonitorSlot, m_intMonitorSlot);
		}
		else
		{
			controlDisable(PANEL_MAIN, "monitor");
		}
	}
	else
	{
		controlDisable(PANEL_MAIN, "monitor");
	}
	
	
//	debug("comms_mixer::initMonitor() ops_position=%1 mon_dest=%2 monitorslot=%3", 
//		strOpsPosition, intOperatorMonitorPort, m_intMonitorSlot);
}


void comms_mixer::updateMixer()
{
	//49:0.0,0:MUTE,0:MUTE,0:MUTE|56,etc

	bncs_stringlist sltMixerInputsOutputs = bncs_stringlist(m_strMixerSetup, '|');
	bncs_stringlist sltMixerInputs = bncs_stringlist(sltMixerInputsOutputs[0]);
	bncs_stringlist sltMixerOutputs = bncs_stringlist(sltMixerInputsOutputs[1]);

	m_intFirstUnusedOutput = sltMixerOutputs.find("0") + 1;
//	debug("sltMixerOutputs=%1 first=%2", sltMixerOutputs.toString(), m_intFirstUnusedOutput);
	
//	controlDisable(PANEL_MAIN, "output_add");
	controlDisable(PANEL_MAIN, "output_drop");
//	debug("comms_mixer:: sltMixerOutputs =%1", sltMixerOutputs.toString());
	//Update Inputs
	for(int intInput=1; intInput <= m_intInputCount; intInput++)
	{
		
		bncs_stringlist sltPortGain = bncs_stringlist(sltMixerInputs[intInput - 1], ':');
		comms_ringmaster_port intInputPort = rh->GetPort(sltPortGain[0]);;

		//Update tally
		m_intInputPort[intInput] = intInputPort;
		updateTally(intInput);
		
//		debug("updateMixer - m_intDefaultInputPort[%1] = %2", intInput, m_intDefaultInputPort[intInput] );
		if(m_intDefaultInputPort[intInput].valid)
		{
			if(m_intDefaultInputPort[intInput] != m_intInputPort[intInput])
			{
				controlShow(PANEL_MAIN, bncs_string("input_%1").arg(intInput));
			}
			else
			{
				//TEMP - the restore default button should always be available
			//	controlHide(PANEL_MAIN, bncs_string("input_%1").arg(intInput));
			}
		}

		if(intInputPort.valid)
		{
			//Disable gain controls
			controlDisable(PANEL_MAIN, bncs_string("up_%1").arg(intInput));
			controlDisable(PANEL_MAIN, bncs_string("down_%1").arg(intInput));
			controlDisable(PANEL_MAIN, bncs_string("unity_%1").arg(intInput));
			controlDisable(PANEL_MAIN, bncs_string("mute_%1").arg(intInput));
			controlDisable(PANEL_MAIN, bncs_string("gain_%1").arg(intInput));
		}
		else
		{
			//Enable gain controls
			controlEnable(PANEL_MAIN, bncs_string("up_%1").arg(intInput));
			controlEnable(PANEL_MAIN, bncs_string("down_%1").arg(intInput));
			controlEnable(PANEL_MAIN, bncs_string("unity_%1").arg(intInput));
			controlEnable(PANEL_MAIN, bncs_string("mute_%1").arg(intInput));
			controlEnable(PANEL_MAIN, bncs_string("gain_%1").arg(intInput));
		}

		//Show port gain
		bool blnMute = (sltPortGain[1] == "MUTE")?true:false;
	
		bncs_string strGain;
		bncs_string strStatesheet, strMuteButtonStateSheet;
		if(blnMute)
		{
			strGain = "MUTE";
			strStatesheet = "enum_notimportant";
			strMuteButtonStateSheet = "enum_alarm";
		}
		else
		{
			double dblInputGain = sltPortGain[1].toDouble();
			char szGain[16];
			sprintf(szGain, "%1.1f dB", dblInputGain);
			strGain = szGain;

			if (dblInputGain == 0.0)
			{
				strStatesheet = "enum_normal";
			}
			else if (dblInputGain > 0.0)
			{
				strStatesheet = "enum_warning";
				if ( dblInputGain >= GAIN_MAX )
				{
					controlDisable(PANEL_MAIN, bncs_string("up_%1").arg(intInput));
				}
				else
				{
					controlEnable(PANEL_MAIN, bncs_string("up_%1").arg(intInput));
				}
			}			
			else if (dblInputGain < 0.0)
			{
				strStatesheet = "enum_warning";
				if ( dblInputGain <= GAIN_MIN )
				{
					controlDisable(PANEL_MAIN, bncs_string("down_%1").arg(intInput));
				}
				else
				{
					controlEnable(PANEL_MAIN, bncs_string("down_%1").arg(intInput));
				}
			}
/*  TODO sort out statesheet based on default gain setting
			debug("m_sltDefaultInputGains[1]=%1", m_sltDefaultInputGains[intInput]);
			if ( dblInputGain == m_sltDefaultInputGains[intInput].toDouble())
			{
				strStatesheet = "enum_normal";
			}
			else
			{
				strStatesheet = "enum_warning";
			}			
			*/
/*			if(strGain == "0.0 dB")
			{
			}
			else
			{
			}
*/
			strMuteButtonStateSheet = "enum_deselected";
		}

//		debug("comms_mixer::updateMixer() input_%1 input=%2 gain=%3", 
//			intInput, intInputPort, strGain);

		textPut("text", strGain, PANEL_MAIN, bncs_string("gain_%1").arg(intInput));
		textPut("statesheet", strStatesheet, PANEL_MAIN, bncs_string("gain_%1").arg(intInput));
		textPut("statesheet", strMuteButtonStateSheet, PANEL_MAIN, bncs_string("mute_%1").arg(intInput));

		textPut("status", bncs_string("%1|%2").arg(m_intMixer).arg(m_strMixerSetup), PANEL_MAIN, "shortcuts");
	}
	
	//Update outputs
	textPut("clear", PANEL_MAIN, "output_list");
	int defaultDifferences = 0;
	int outputCount = 0;
	for ( int intOutput=1; intOutput <= m_intOutputCount ; intOutput++)  
	{
		sltMixerOutputs = sltMixerOutputs.sort();
		auto port = rh->GetPort(m_ring, sltMixerOutputs[intOutput - 1].toInt());
		m_intOutputPort[intOutput] = port;

		if ( m_intOutputPort[intOutput].valid)
		{
			textPut("add", bncs_string("%1;%2").arg( m_intOutputPort[intOutput].ring_port_leading_zero ).arg(m_intOutputPort[intOutput].destName_short), PANEL_MAIN, "output_list");
			outputCount++;
		}
		if ( m_intOutputPort[intOutput].ring_port != m_intDefaultOutputPort[intOutput].ring_port )
		{
			defaultDifferences++;
		}
	}

	if ( outputCount <= MAX_OUTPUTS )
	{
		controlEnable(PANEL_MAIN, "output_add");
	}
	if(defaultDifferences == 0)
	{
		//TEMP - the restore default output button should always be available
		//controlHide(PANEL_MAIN, "restore_default_output");
	}
	else
	{
		controlShow(PANEL_MAIN, "restore_default_output");
	}


	updateMonitor();
}


void comms_mixer::setInputPort(comms_ringmaster_port port, int intMixerInput)
{
	if(m_intDeviceExtras > 0 && m_intMixerSlot > 0)
	{
		buttonEnable(false);
		infoWrite(m_intDeviceExtras, bncs_string("&INPUT%1=%2").arg(intMixerInput).arg(port.ring_port), m_intMixerSlot);
	}
}

void comms_mixer::setAllInputPortsToDefault(bool setGains)
{
	// NB uses Mixer Shortcut Processing feature in driver 1.8.4 or later 
	buttonEnable(false);
	for (int intInput=1;intInput <= m_intInputCount; intInput++ )

	{
		bncs_string strCommand;
		if (setGains)
		{
			strCommand = bncs_string("&INPUT%1=%2:%3").arg(intInput).arg(m_intDefaultInputPort[intInput].ring_port).arg(m_sltDefaultInputGains[intInput]);
		}
		else
		{
			strCommand = bncs_string("&INPUT%1=%2").arg(intInput).arg(m_intDefaultInputPort[intInput].ring_port);
		}
//		debug("comms_mixer::setAllInputPortsToDefault (setGains %1 ) command=%2", setGains, strCommand);
		if ( strCommand.length())
		{
			infoWrite(m_intDeviceExtras, strCommand, m_intMixerSlot);
		}
	}
}

void comms_mixer::setInputGain(int intMixerInput, bncs_string strGain)
{
	bncs_stringlist sltMixerInputsOutputs = bncs_stringlist(m_strMixerSetup, '|');
	bncs_stringlist sltMixerInputs = bncs_stringlist(sltMixerInputsOutputs[0]);
	bncs_stringlist sltPortGain = bncs_stringlist(sltMixerInputs[intMixerInput - 1], ':');

	bool blnMute = (sltPortGain[1] == "MUTE")?true:false;

	bncs_string strNewValue = "NOT_VALID";
	if(strGain == "UP")
	{
		if(blnMute)
		{
			strNewValue = "0.0";
		}
		else
		{
			double dblInputGain = sltPortGain[1].toDouble();
			if(dblInputGain + m_dblGainStep <= GAIN_MAX)
			{
				char szGain[16];
				sprintf(szGain, "%1.1f", dblInputGain + m_dblGainStep);
				strNewValue = szGain;
			}
		}
	}
	else if(strGain == "DOWN")
	{
		if(blnMute)
		{
			strNewValue = "0.0";
		}
		else
		{
			double dblInputGain = sltPortGain[1].toDouble();
			if (dblInputGain - m_dblGainStep >= GAIN_MIN)
			{
				char szGain[16];
				sprintf(szGain, "%1.1f", dblInputGain - m_dblGainStep);
				strNewValue = szGain;
			}
		}
	}
	else if(strGain == "UNITY")
	{
		strNewValue = "0.0";
	}
	else if(strGain == "MUTE")
	{
		strNewValue = "MUTE";
	}
/*	else if(strGain.startsWith("SET") )
	{
		bncs_stringlist slt = bncs_stringlist(strGain);
		strNewValue = slt.getNamedParam("SET");
	}
*/
	if(strNewValue == "NOT_VALID")
	{
		//invalid value requested - do not send
//		debug("comms_mixer::setInputGain() NOT_VALID");
	}
	else
	{
		modifyChannelGain(intMixerInput, strNewValue);
	}
}

// void comms_mixer::modifyChannelGain(int mixerInput, bncs_string newGain)
// {
// 	//Split setup
// 	bncs_stringlist sltMixerInputsOutputs = bncs_stringlist(m_strMixerSetup, '|');
// 	bncs_stringlist sltMixerInputs = bncs_stringlist(sltMixerInputsOutputs[0]);
// 	bncs_stringlist sltMixerOutputs = bncs_stringlist(sltMixerInputsOutputs[1]);
// 	bncs_stringlist sltPortGain = bncs_stringlist(sltMixerInputs[mixerInput - 1], ':');
// 	
// 	//Rewrite affected input setup
// 	sltPortGain[1] = bncs_string(newGain);
// 	sltMixerInputs[mixerInput - 1] = sltPortGain.toString(':');
// 	
// 	//Rewrite entire mixer setup
// 	bncs_string strModfiedSetup = bncs_string("%1|%2").arg(sltMixerInputs.toString(',')).arg(sltMixerOutputs[0]);
// 	debug("comms_mixer::setInputGain() send %1", strModfiedSetup);
// 	
// 	if(m_intDeviceExtras > 0 && m_intMixerSlot > 0)
// 	{
// 		buttonEnable(false);
// 		infoWrite(m_intDeviceExtras, strModfiedSetup, m_intMixerSlot);
// 	}
// 
// }
void comms_mixer::modifyChannelGain(int mixerInput, bncs_string newGain)
{
	//Split setup
	bncs_stringlist sltMixerInputsOutputs = bncs_stringlist(m_strMixerSetup, '|');
	bncs_stringlist sltMixerInputs = bncs_stringlist(sltMixerInputsOutputs[0]);
//	debug("pos=%1 input=%2", mixerInput, sltMixerInputs[mixerInput]) ;
	if(m_intDeviceExtras > 0 && m_intMixerSlot > 0 && mixerInput > 0 && mixerInput <= MAX_INPUTS )
	{
		buttonEnable(false);
		infoWrite(m_intDeviceExtras, bncs_string("&INPUT%1=%2:%3").arg(mixerInput).arg(sltMixerInputs[mixerInput-1].toInt()).arg(newGain), m_intMixerSlot);
	}
	
}

void comms_mixer::setOutputPort(int port)
{
	//Split setup
	bncs_stringlist sltMixerInputsOutputs = bncs_stringlist(m_strMixerSetup, '|');
	
	//Rewrite entire mixer setup
	bncs_string strModfiedSetup = bncs_string("%1|%2").arg(sltMixerInputsOutputs[0]).arg(port);
//	debug("comms_mixer::setOutputPort() send %1", strModfiedSetup);

	if(m_intDeviceExtras > 0 && m_intMixerSlot > 0 )
	{
		buttonEnable(false);
		infoWrite(m_intDeviceExtras, strModfiedSetup, m_intMixerSlot);
	}
}

void comms_mixer::setOutputPort(int port, int channel)
{
	if(m_intDeviceExtras > 0 && m_intMixerSlot > 0 && port >1 && port < 0x1000 && channel >0  && channel <= MAX_OUTPUTS)
	{
		buttonEnable(false);
		infoWrite(m_intDeviceExtras, bncs_string("&OUTPUT%1=%2").arg(channel).arg(port), m_intMixerSlot);
	}
}

void comms_mixer::updateMonitor()
{	

	bncs_string strMonitorOutput = bncs_string("%1,D").arg(m_intSelectedDest);
	//textPut("text", bncs_string("%1|%2").arg(m_strCurrentMonitorTarget).arg(strMonitorOutput), PANEL_MAIN, "monitor");
	bncs_string strName;
	routerName(m_intDeviceRouter, riedelHelpers::databases::Output_Port_Button_Name, m_strCurrentMonitorTarget.toInt(), strName);

	if(m_strCurrentMonitorTarget == strMonitorOutput)
	{
		m_blnMonitorActive = true;
		textPut("statesheet", "enum_ok", PANEL_MAIN, "monitor");
		textPut("text", strName.replace('|',' '), PANEL_MAIN, "tally_mon");
	}
	else
	{
		m_blnMonitorActive = false;
		textPut("statesheet", "default", PANEL_MAIN, "monitor");
		textPut("text", strName.replace('|',' '), PANEL_MAIN, "tally_mon");
	}
}

void comms_mixer::toggleMonitor()
{
	bncs_string strMonitorOutput = bncs_string("%1,D").arg(m_intSelectedDest);
	if(m_strCurrentMonitorTarget == strMonitorOutput)
	{
		infoWrite(m_intDeviceMonitor, "0", m_intMonitorSlot);
	}
	else
	{
		infoWrite(m_intDeviceMonitor, strMonitorOutput, m_intMonitorSlot);
	}
}

void comms_mixer::showPresetGainSelector( int channel, double gain )
{

//	<object id="comms_mixers">
//		<setting id="preset_gains" value="12.5,6.0,3.0,0.0,-3.0,-6.0,-10.0,-12.5" />
//	</object>

	
	char szGain[16];
	sprintf(szGain, "%1.1f", gain);
	bncs_string strGain = szGain;
//	debug("gain=%1", strGain);
	panelPopup(POPUP_PRESET_GAIN, "popup_preset_gain.bncs_ui" );

	bncs_stringlist sltControlList = getIdList(POPUP_PRESET_GAIN, "preset");
	for (int i=0; i<sltControlList.count(); i++)
	{
		bncs_string strControl = bncs_string ("preset_%1").arg(i+1);
		bncs_string gain = m_sltGainPresets[i];
		textPut("text", gain, POPUP_PRESET_GAIN, strControl);
		textPut("stylesheet", "enum_deselected", POPUP_PRESET_GAIN, strControl);
//		debug("channel=[%1] , strGain=[%2], getCurrentChannelGain=[%3] ", channel, strGain, getCurrentChannelGain(channel));
		double dGain = gain.toDouble();
		double dCurrentGain = getCurrentChannelGain(channel).toDouble();
		if ( dGain == dCurrentGain )
		{		
			textPut("stylesheet", "enum_ok", POPUP_PRESET_GAIN, strControl);
		}
	}

}

bncs_string comms_mixer::getSignedGainValue(double gain, bool units)
{

	/*bncs_string s(gain,4,'0');
	bncs_string ret;
	if ( gain == 0.0f)
	{
		ret = s;
	}
	else if ( gain < 0.0)
	{
		ret =  "-" + s ;
	}
	else if ( gain > 0.0)
	{
		ret = "+" + s;
	}
	else
	{
		ret = "";
	}
	return units?ret+ " dB":ret;  */
	return "";
}

bncs_string comms_mixer::getCurrentChannelGain(int mixerInput)
{
	//Split setup
	bncs_stringlist sltMixerInputsOutputs = bncs_stringlist(m_strMixerSetup, '|');
	bncs_stringlist sltMixerInputs = bncs_stringlist(sltMixerInputsOutputs[0]);
	bncs_stringlist sltPortGain = bncs_stringlist(sltMixerInputs[mixerInput - 1], ':');
//	debug("channel=%1 gain=%2", mixerInput, sltPortGain[1]);
	return sltPortGain[1];
}

#include <sstream>
double comms_mixer::stringToDouble( const std::string& s )
{
	std::istringstream i(s);
	double x;
	if (!(i >> x))
		return 0;
	return x;
 } 

void comms_mixer::buttonEnable( bool enabled )
{
	if (!enabled)
	{
		for (int i=1; i <= 7; i++)
		{
			controlDisable(PANEL_MAIN, bncs_string("input_%1").arg(i));
			controlDisable(PANEL_MAIN, bncs_string("unity_%1").arg(i));
			controlDisable(PANEL_MAIN, bncs_string("up_%1").arg(i));
			controlDisable(PANEL_MAIN, bncs_string("gain_%1").arg(i));
			controlDisable(PANEL_MAIN, bncs_string("down_%1").arg(i));
			controlDisable(PANEL_MAIN, bncs_string("mute_%1").arg(i));
		//	controlDisable(PANEL_MAIN, "output_list");
			controlMove(PANEL_MAIN, "message",0,0);
		}
		timerStart(TIMER_SLUG, TIMEOUT_SLUG);
//		debug("SLUG TIMER START");
	}
	else
	{
		for (int i=1; i <= 7; i++)
		{
			controlEnable(PANEL_MAIN, bncs_string("input_%1").arg(i));
			controlEnable(PANEL_MAIN, bncs_string("unity_%1").arg(i));
			controlEnable(PANEL_MAIN, bncs_string("up_%1").arg(i));
			controlEnable(PANEL_MAIN, bncs_string("gain_%1").arg(i));
			controlEnable(PANEL_MAIN, bncs_string("down_%1").arg(i));
			controlEnable(PANEL_MAIN, bncs_string("mute_%1").arg(i));
		//	controlEnable(PANEL_MAIN, "output_list");
			controlMove(PANEL_MAIN, "message",1280,0);
		}
		timerStop(TIMER_SLUG);
	}

}

void comms_mixer::setDefaultOutputPorts()
{

	for ( int i=0; i < m_intOutputCount; i++)
	{
		infoWrite(m_intDeviceExtras, bncs_string("&OUTPUT%1=%2").arg(i+1).arg(m_intDefaultOutputPort[i+1].ring_port), m_intMixerSlot);	
	}
	
}

void comms_mixer::dropOutputPutPort( int port )
{
	//Split setup
	bncs_stringlist sltMixerInputsOutputs = bncs_stringlist(m_strMixerSetup, '|');
	bncs_stringlist sltMixerInputs = bncs_stringlist(sltMixerInputsOutputs[0]);
	bncs_stringlist sltMixerOutputs = bncs_stringlist(sltMixerInputsOutputs[1]);

	
	int pos = sltMixerOutputs.find(port);
//	debug ("pos=%1", pos);
	sltMixerOutputs[pos] = "0";
	bncs_string strMixerSetup = bncs_string("%1|%2").arg(sltMixerInputs.toString()).arg(sltMixerOutputs.toString());
	buttonEnable(false);
	infoWrite(m_intDeviceExtras, strMixerSetup , m_intMixerSlot);
}

bncs_string comms_mixer::getOpsSetting(bncs_string position, bncs_string parameter)
{
	bncs_config cfg(bncs_string("ops_positions.%1").arg(position));
	
	bncs_string setting = "";
	while (cfg.isChildValid())
	{
		if (cfg.childAttr("id") == parameter)
		{
			setting = cfg.childAttr("value");
			break;
		}
		cfg.nextChild();
	}
	return setting;
	
}

int comms_mixer::getRiedelDevice(bncs_string composite, bncs_string device)
{
	int ret = -1;
	bncs_config cfgInstance = bncs_config(bncs_string("instances.%1").arg(composite));
	if (cfgInstance.isValid())
	{
		if (cfgInstance.attr("composite").lower() == "yes")
		{
			bncs_string groupItem = cfgInstance.attr("instance");
			bncs_config cfgGroupItemInstance = bncs_config(bncs_string("instances.%1.%2").arg(composite).arg(device));
			if (cfgGroupItemInstance.isValid())
			{
				bncs_string strGroupItemInstance = cfgGroupItemInstance.attr("instance");
				getDev(strGroupItemInstance, &ret);
			}
		}
	}
	return ret;
}

bncs_string comms_mixer::getRiedelInstance(bncs_string composite, bncs_string device)
{
	bncs_string ret;
	instanceLookupComposite(composite, device, ret);
	return ret;
}
