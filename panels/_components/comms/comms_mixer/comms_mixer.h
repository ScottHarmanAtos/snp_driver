#ifndef comms_mixer_INCLUDED
	#define comms_mixer_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_mixer : public bncs_script_helper
{
public:
	comms_mixer( bncs_client_callback * parent, const char* path );
	virtual ~comms_mixer();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:
	riedelHelper * rh;
	int m_ring;

	void toggleMonitor();
	bncs_string m_strMixerSetup;

	int m_intMixer;
	int m_intMixerSlot;
	int m_intMonitorSlot;
	int m_intDeviceRouter;	
	int m_intDeviceMonitor;	
	int m_intDeviceExtras;	
	int m_intDeviceIfb;     
	int m_intDeviceMonitorRouter;


	comms_ringmaster_port m_intInputPort[11];					// Current Input ports
	comms_ringmaster_port m_intDefaultInputPort[11];			// Default Input ports	
	comms_ringmaster_port m_intOutputPort[11];				// Current Output ports	
	comms_ringmaster_port m_intDefaultOutputPort[11];			// Default Output ports	
	bncs_stringlist m_sltDefaultInputGains; // placeholder for default gains to be loaded


	bncs_string m_strCurrentMonitorTarget;
	bncs_string m_strInstance;
	bncs_string m_strMixerName;
	bncs_stringlist m_sltInputLabels;
	bncs_stringlist m_sltInputPorts;
//	bncs_stringlist m_sltDefaultOutputPorts;
	bool m_blnShowNickname;
	int m_intIfb;				
	bncs_string m_strLayout;
	int m_intInputCount;
	int m_intPresetGain;
	int m_intPreselectedMixerInput;
	double m_dblGainStep;

	void initMixer();
	void initMonitor();
	void setInputGain(int intMixerInput, bncs_string strGain);
	void setInputPort(comms_ringmaster_port intPort, int channel);
	void setOutputPort(int intPort);
	void setOutputPort(int intPort, int channel);
	void updateMixer();
	void updateMonitor();
	void updateTally(int intInput);
	void showPresetGainSelector( int channel, double gain );
	bncs_string getSignedGainValue(double gain, bool units=false);
	bncs_stringlist m_sltGainPresets;
	int m_intOutputCount;
	int m_intSelectedDest;
	int m_intDefaultPrehearPort;
	bool m_blnMonitorAvailable;
	bool m_blnMonitorActive;
//	int m_intOutputChannel;
	int m_intFirstUnusedOutput;
	bncs_string m_strNewMixerSetup;
	void modifyChannelGain(int mixerInput, bncs_string newGain);
	bncs_string getCurrentChannelGain(int mixerInput);
	
	double stringToDouble( const std::string& s );
	void buttonEnable( bool enabled );
	void setDefaultOutputPorts();
	void setAllInputPortsToDefault(bool setGains);
	void dropOutputPutPort( int port ) ;  // port to be removed
	bncs_string getOpsSetting(bncs_string position, bncs_string parameter);
	int getRiedelDevice(bncs_string composite, bncs_string device);
	bncs_string getRiedelInstance(bncs_string composite, bncs_string device);

	bncs_string m_strInstanceGRD;

};


#endif // comms_mixer_INCLUDED