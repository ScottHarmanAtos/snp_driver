#ifndef comms_function_list_INCLUDED
	#define comms_function_list_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_function_list : public bncs_script_helper
{
public:
	comms_function_list( bncs_client_callback * parent, const char* path );
	virtual ~comms_function_list();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int m_selected;

	bncs_stringlist m_functions;
	bncs_stringlist m_sanitised_functions;

	void setupList();
	void resetSelected();
};


#endif // comms_function_list_INCLUDED