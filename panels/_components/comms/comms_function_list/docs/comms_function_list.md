# comms_function_list

A simple component that lists out the functions that are passed to it and on pressing the button, emits the index of the pressed command.

## Commands

### functions
The active functions which have been set on a key e.g. `CALLTOPORT|1|*0**KMD1,IFB|1|*0**KMD1`

## Notifications

### remove_function=\<index>
The index of the pressed element in the set `functions` list.