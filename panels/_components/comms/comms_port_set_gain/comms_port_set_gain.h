#ifndef comms_port_set_gain_INCLUDED
	#define comms_port_set_gain_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_port_set_gain : public bncs_script_helper
{
public:
	comms_port_set_gain( bncs_client_callback * parent, const char* path );
	virtual ~comms_port_set_gain();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:
	riedelHelper * rh;
	void setValue(bncs_string sValue);
	void adjustValue(double dblAdjust);
	void showPresetGainSelector();
	void updateValue();
	double m_dblGain;
	int m_intGainSlot;
	comms_ringmaster_port m_port;
	int m_intDeviceGRD;
	int m_intDeviceGain;


	enum Gain
	{
		Input = 1,
		Output
	};

	Gain m_gainType;
	bncs_string m_strLayout;
	bncs_stringlist m_sltGainPresets;
};


#endif // comms_port_set_gain_INCLUDED