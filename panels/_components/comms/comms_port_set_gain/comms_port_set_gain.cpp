#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "comms_port_set_gain.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_port_set_gain )

#define PANEL_MAIN		"comms_port_set_gain"
#define POPUP_PRESET_GAIN	"comms_port_set_gain_popup"


#define DEVICE_OFFSET_GAIN 5


//Riedel port types

// constructor - equivalent to ApplCore STARTUP
comms_port_set_gain::comms_port_set_gain( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{

	rh = riedelHelper::getriedelHelperObject();
	m_intDeviceGRD = 0;
	m_intDeviceGain = 0;
	m_gainType = Gain::Input;
	m_port = comms_ringmaster_port();
	m_dblGain = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1

	m_strLayout = "main";
	panelShow( PANEL_MAIN, bncs_string("%1.bncs_ui" ).arg(m_strLayout));
	textPut("text", "Input", PANEL_MAIN, "label");
	textPut("text", "", PANEL_MAIN, "adjust");
	controlDisable(PANEL_MAIN, "adjust");


	bncs_string strGainPresets = getObjectSetting("comms_mixers", "preset_gains");
	if ( strGainPresets.length() > 0 )
	{
		m_sltGainPresets = bncs_stringlist (strGainPresets);
	}
	else
	{
		m_sltGainPresets = bncs_stringlist ("");
	}

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_port_set_gain::~comms_port_set_gain()
{
}

// all button pushes and notifications come here
void comms_port_set_gain::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == "adjust")
		{
			panelPopup(POPUP_PRESET_GAIN, "popup_preset_gain.bncs_ui");
			showPresetGainSelector();
		}
	}
	else if(b->panel() == POPUP_PRESET_GAIN)
	{
		bncs_stringlist sltControlName = bncs_stringlist(b->id(), '_');
		int intNewControlIndex = sltControlName[1].toInt();
		bncs_string presetGain = m_sltGainPresets[intNewControlIndex-1];
		setValue(presetGain);
		panelDestroy(POPUP_PRESET_GAIN);
	}
}

// all revertives come here
int comms_port_set_gain::revertiveCallback( revertiveNotify * r )
{
	debug("comms_port_set_gain::revertiveCallback index=%1 value=%2", r->index(), r->sInfo());
	if( r->device() == m_intDeviceGain)
	{
		if(r->index() == m_intGainSlot)
		{
			m_dblGain = r->sInfo().toDouble();
			updateValue();
		}
	}
	return 0;
}

// all database name changes come back here
void comms_port_set_gain::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_port_set_gain::parentCallback( parentNotify *p )
{
	if(p->command() == "mode")
	{
		if(p->value().upper() == "OUTPUT")
		{
			m_gainType = Gain::Output;
			textPut("text", "Output", PANEL_MAIN, "label");
		}
		else
		{
			m_gainType = Gain::Input;
			textPut("text", "Input", PANEL_MAIN, "label");
		}
	}
	else if(p->command() == "index")
	{
		p->dump("$$$ index recieved");
		bool blnGainAvailable = false;
		m_port = rh->GetPort(p->value());

;
		if(m_port.portType == PORT_TYPE_SPLIT || m_port.portType == PORT_TYPE_4WIRE)
		{
			blnGainAvailable = true;
		}

		if ( blnGainAvailable )
		{
			if(m_port.valid )
			{
				if(m_gainType == Gain::Output)
				{
					m_intGainSlot = m_port.slot_index + riedelHelpers::offset::OUTPUT_GAIN;
				}
				else
				{
					m_intGainSlot = m_port.slot_index;
				}
				if(m_intDeviceGain > 0)
				{
					infoRegister(m_intDeviceGain, m_intGainSlot, m_intGainSlot);
					infoPoll(m_intDeviceGain, m_intGainSlot, m_intGainSlot);
				}
				controlEnable(PANEL_MAIN, "adjust");
			}
			else
			{
				m_intGainSlot = 0;
				if(m_intDeviceGain > 0)
				{
					infoUnregister(m_intDeviceGain);
				}
				textPut("text", "", PANEL_MAIN, "adjust");
				controlDisable(PANEL_MAIN, "adjust");
			}
		}
		else
		{
			controlDisable(PANEL_MAIN, "adjust");
		}
	}
	else if(p->command() == "instance")
	{
		//riedel_gain
		auto ring = rh->GetRing(p->value());

		m_intDeviceGRD = ring.devices.Grd;
		m_intDeviceGain = ring.devices.Gain;

		debug("grd device is %1, gain device is %2", m_intDeviceGRD, m_intDeviceGain);
	}
	else if(p->command() == "layout")
	{
		if ( p->value().length() >0 && p->value() != m_strLayout)
		{
			m_strLayout = p->value();
			panelDestroy(PANEL_MAIN);
			panelShow(PANEL_MAIN, bncs_string("%1.bncs_ui").arg(m_strLayout));
		}
	}
	//Return Commands
	else if( p->command() == "return" )
	{
		//Called by visual editor to persist settings in bncs_ui
		if(p->value() == "all")
		{
			bncs_stringlist slt;
			slt << bncs_string( "mode=" ) + bncs_string((m_gainType == Gain::Output)?"OUTPUT":"INPUT");
			slt << bncs_string( "layout=" ) + bncs_string(m_strLayout);
			return slt.toString('\n');
		}
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;
		
		sl << "index=<port index>";
		
		return sl.toString( '\n' );
	}
	return "";
}

// timer events come here
void comms_port_set_gain::timerCallback( int id )
{
}

void comms_port_set_gain::updateValue()
{
	char szGain[16];
	sprintf(szGain, "%1.1f", m_dblGain);
	bncs_string strDisplay = bncs_string("%1 dB").arg(szGain);
	textPut("text", strDisplay, PANEL_MAIN, "adjust");
/*	textPut("text", strDisplay, POPUP_PRESET_GAIN, "value");*/

	if ( m_dblGain == 0.0 )
	{
		textPut("stylesheet", "enum_normal", PANEL_MAIN, "adjust");
	}
	else
	{
		textPut("stylesheet", "enum_warning", PANEL_MAIN, "adjust");
	}
}

void comms_port_set_gain::adjustValue(double dblAdjust)
{
	double dblNewValue = m_dblGain + dblAdjust;
	char szGain[16];
	sprintf(szGain, "%1.1f", dblAdjust);
	if(m_intDeviceGain > 0 && m_intGainSlot > 0)
	{
		infoWrite(m_intDeviceGain, bncs_string("%1").arg(szGain), m_intGainSlot);
	}
}

void comms_port_set_gain::setValue(bncs_string sValue)
{
	if(m_intDeviceGain > 0 && m_intGainSlot > 0)
	{
		infoWrite(m_intDeviceGain, bncs_string("%1").arg(sValue), m_intGainSlot);
	}
}

void comms_port_set_gain::showPresetGainSelector()
{
	
	//	<object id="comms_mixers">
	//		<setting id="preset_gains" value="12.5,6.0,3.0,0.0,-3.0,-6.0,-10.0,-12.5" />
	//	</object>

	bncs_stringlist sltControlList = getIdList(POPUP_PRESET_GAIN, "preset");
	for (int i=0; i<sltControlList.count(); i++)
	{
		bncs_string strControl = bncs_string ("preset_%1").arg(i+1);
		bncs_string gain = m_sltGainPresets[i];
		textPut("text", gain, POPUP_PRESET_GAIN, strControl);
		textPut("stylesheet", "enum_deselected", POPUP_PRESET_GAIN, strControl);
		double selectTest = gain.toDouble();
		if (selectTest == m_dblGain)
		{		
			textPut("stylesheet", "enum_ok", POPUP_PRESET_GAIN, strControl);
		}
	}
}