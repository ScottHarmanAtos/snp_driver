# Comms Take Component

The comms take component has been created to work within a ringmaster system.

It is a simple take component created to work with connections, it does not understand how to carry out a take but instead relies on the comms_port component to do it. As the comms_port component already needs to understand this.

Checks are made to ensure the only valid routes can be made
* A route cannot be made to a panel port.
* A route cannot be made if either the input or output already has a 4 Wire route.
* If the IN and OUTPUT are the same port only a 4 Wire route can be made
* A route cannot be made if OUTPUT is lock.
* Only a 2 wire route can be made if the INPUT is locked.
* Only allow 4 wire if the INPUT is also a valid OUTPUT

### Possible actions:
* Take
* Undo
* Take Special - 2 Wire and 4 Wire

### Note

An instance does not have to be supplied as the component gets this information from the **riedelHelper.dll**

## Commands

| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| input | Value in Ring.Port format,  the input to route |
| output | Value in Ring.Port format, the output to route |

## Notifications

| Name | Use                                                                  |
|------|--------------------------------------------------------------------- |
| take=park | Park the dest |
| take=RING.PORT | Take the input RING.PORT - 2 Wire take | 
| take.4Wire.RING.PORT=RING.PORT | Do a 4 Wire take first RING.PORT in the sub is the reverse lookup second is the normal source RING.PORT |
| undo | Undo the last take |
| park=RING.PORT | Park has been pressed |


## Stylesheets
None