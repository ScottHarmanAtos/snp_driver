#ifndef comms_take_INCLUDED
	#define comms_take_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_take : public bncs_script_helper
{
public:
	comms_take( bncs_client_callback * parent, const char* path );
	virtual ~comms_take();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper *rh;
	comms_ringmaster_port input;
	comms_ringmaster_output output;
	comms_ringmaster_output inputOutput;

	void check();

	bool park;

	bncs_string currentLabelText;

	void SetLabelText(bncs_string s = "");

	bool Is4WireRouted(bncs_string type);

	void SetTake(bool IsSpecial, bncs_string only2Wire = "");
	bool specialTake;

	bool outputLock;
	bool inputLock;

	bncs_string fourWireNotPossible;
	bncs_string sourceTally;
};


#endif // comms_take_INCLUDED