#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_take.h"

#define PNL_MAIN	"comms_take"
#define PNL_POP		"comms_popup"

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_take )

// constructor - equivalent to ApplCore STARTUP
comms_take::comms_take( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();
	// show a panel from file comms_take.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "comms_take.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PNL_MAIN );		// set the size to the same as the specified panel

	specialTake = false;
	park = false;

	controlDisable(PNL_MAIN, "btn_take");
	controlDisable(PNL_MAIN, "btn_undo");
	controlHide(PNL_MAIN, "lbl_message");

	inputLock = false;
	outputLock = false;
	fourWireNotPossible = "";
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_take::~comms_take()
{
}

// all button pushes and notifications come here
void comms_take::buttonCallback( buttonNotify *b )
{
	if (b->panel() == PNL_MAIN)
	{
		if (b->id() == "btn_take")
		{
			if (!specialTake)
			{
				if (park)
					hostNotify(bncs_string("take=park"));
				else
					hostNotify(bncs_string("take=%1").arg(input.ring_port));
				controlEnable(PNL_MAIN, "btn_undo");
			}
			else //Special Take
			{
				panelPopup(PNL_POP, "popup_take_special.bncs_ui");

				if (fourWireNotPossible.length() > 0)
				{
					textPut("text", fourWireNotPossible, PNL_POP, "btn_take_4_wire");
					controlDisable(PNL_POP, "btn_take_4_wire");
				}
			}
		}
		else if (b->id() == "btn_undo")
		{
			hostNotify(bncs_string("undo"));
			controlDisable(PNL_MAIN, "btn_undo"); //Only allow 1 undo
		}
		else if (b->id() == "btn_park")
		{
			textPut("text", "park", PNL_MAIN, "lbl_input");
			textPut("text", bncs_string("Input|%1").arg("park"), PNL_MAIN, "lbl_preset_input");
			park = true;
			input = comms_ringmaster_port();
			hostNotify(bncs_string("park=%1").arg(input.ring_port));
			check();
		}
	}
	else if (b->panel() == PNL_POP)
	{
		if (b->id() == "btn_take_2_wire")
		{
			debug(bncs_string("btn_take_2_wire take=%1").arg(input.ring_port));
			hostNotify(bncs_string("take=%1").arg(input.ring_port));
			controlEnable(PNL_MAIN, "btn_undo");
		}
		else if (b->id() == "btn_take_4_wire")
		{
			debug(bncs_string("comms_take::buttonCallback take.4Wire.%1=%2").arg(sourceTally).arg(input.ring_port));
			hostNotify(bncs_string("take.4Wire.%1=%2").arg(sourceTally).arg(input.ring_port));
			controlEnable(PNL_MAIN, "btn_undo");
		}
		panelDestroy(PNL_POP);
	}	
}

// all revertives come here
int comms_take::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == input.devices.Lock && r->index() == input.slot_index)
	{
		inputLock = r->sInfo() == "1" ? true : false;
		check();
	}
	if (r->device() == output.port.devices.Lock && r->index() == output.slot_index)
	{
		outputLock = r->sInfo() == "1" ? true : false;
		check();
	}
	else if (r->device() == inputOutput.device && r->index() == inputOutput.slot_index)
	{
		sourceTally = r->sInfo();
		debug("comms_take::revertiveCallback sourceTally:%1", sourceTally);
		check();
	}
	
	return 0;
}

// all database name changes come back here
void comms_take::databaseCallback( revertiveNotify * r )
{
	if (r->device() == output.port.devices.Pti && r->database() == riedelHelpers::databases::Take_Type && r->index() == output.slot_index)
	{
		check();
	}
	else if (r->device() == input.devices.Pti && r->database() == riedelHelpers::databases::Take_Type && r->index() == input.slot_index)
	{
		check();
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_take::parentCallback( parentNotify *p )
{
	
	if (p->command() == "input")
	{
		park = false;
		input = rh->GetPort(p->value());

		textPut("text", input.sourceName_short, PNL_MAIN, "lbl_input");
		textPut("text", bncs_string("Input|%1").arg(input.ring_port), PNL_MAIN, "lbl_preset_input");
		infoRegister(input.devices.Lock, input.slot_index, input.slot_index, true);
		infoPoll(input.devices.Lock, input.slot_index, input.slot_index);
		
		infoRegister(input.devices.Pti, input.slot_index, input.slot_index, true); //To see route modifies

		inputOutput = rh->GetOutputPort(input.ring_port);
		if (inputOutput.valid)
		{
			sourceTally = "0.0";
			debug("comms_take::parentCallback dev:%1 slot:%2", inputOutput.device, inputOutput.slot_index);
			infoRegister(inputOutput.device, inputOutput.slot_index, inputOutput.slot_index, true);
			infoPoll(inputOutput.device, inputOutput.slot_index, inputOutput.slot_index);
		}
		
		check();
	}
	else if (p->command() == "output")
	{
		output = rh->GetOutputPort(p->value());

		textPut("text", output.port.destName_short, PNL_MAIN, "lbl_output");
		textPut("text", bncs_string("Output|%1").arg(output.port.ring_port), PNL_MAIN, "lbl_preset_output");
		infoRegister(output.port.devices.Lock, output.slot_index, output.slot_index, true);
		infoPoll(output.port.devices.Lock, output.slot_index, output.slot_index);

		infoRegister(output.port.devices.Pti, output.port.slot_index, output.port.slot_index, true); //To see route modifies
		controlDisable(PNL_MAIN, "btn_undo");
		check();
	}

	return "";
}

// timer events come here
void comms_take::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void comms_take::check()
{
	if (output.valid && (input.valid || park))
	{
		if (outputLock)
		{
			//If the output is locked you can't route it.
			SetLabelText("OUTPUT|LOCKED");
			controlDisable(PNL_MAIN, "btn_take");
		}
		else if (output.port.portType == PORT_TYPE_PANEL || output.port.portType == PORT_TYPE_PANEL_DUAL_CHANNEL)
		{
			SetLabelText("Cannot route to|panel port");
			controlDisable(PNL_MAIN, "btn_take");
		}
		else if (park) //We can park anything except panels
		{
			SetLabelText();
			SetTake(false);
		}
		else if (output.port.portType == PORT_TYPE_4WIRE || input.portType == PORT_TYPE_4WIRE)
		{
			//Check if either has a 4 wire connection
			bncs_string takeType;

			bool output4 = false;
			if (output.valid && output.port.portType == PORT_TYPE_4WIRE)
			{
				//infoRegister(output.port.devices.Pti, 4096, 4069, false); //Get database callbacks
				routerName(output.port.devices.Pti, riedelHelpers::databases::Take_Type, output.port.slot_index, takeType);
				output4 = Is4WireRouted(takeType);
			}

			bool input4 = false;
			if (input.valid && input.portType == PORT_TYPE_4WIRE)
			{
				//infoRegister(input.devices.Pti, 4096, 4069, false); //Get database callbacks
				routerName(input.devices.Pti, riedelHelpers::databases::Take_Type, input.slot_index, takeType);
				input4 = Is4WireRouted(takeType);
			}

			if (output4 && input4)
			{
				SetLabelText("PORTS|HAVE|4-WIRE|CONNECTIONS");
				controlDisable(PNL_MAIN, "btn_take");
			}
			else if (output4)
			{
				SetLabelText("OUTPUT|HAS A|4-WIRE|CONNECTIONS");
				controlDisable(PNL_MAIN, "btn_take");
			}
			else if (input4)
			{
				SetLabelText("INPUT|HAS A|4-WIRE|CONNECTIONS");
				controlDisable(PNL_MAIN, "btn_take");
			}
			else
			{
				SetLabelText();
				if (output.port.portType == PORT_TYPE_4WIRE && input.portType == PORT_TYPE_4WIRE)
				{
					//If there are the same port we can only make a 2 wire route
					if (output.port == input)
					{
						SetTake(false);
					}
					else
					{
						//If the input is locked, we can't do a 4 wire route, but we can do a 2 wire route.
						if (inputLock)
						{
							SetTake(true, "INPUT|4 WIRE|LOCKED");
						}
						else
						{
							//Check the input is a valid output.
							comms_ringmaster_output p = rh->GetOutputPort(input.ring_port);
							if (p.valid)
							{
								SetTake(true);
							}
							else
							{
								SetTake(true, "INPUT|4 WIRE NO|RINGMASTER|OUTPUT");
							}
						}
					}
				}
				else
				{
					SetTake(false);
				}
			}
		}
		else
		{
			if (input.valid || park)
			{
				SetLabelText();
				SetTake(false);
			}
		}
	}
	else
	{
		controlDisable(PNL_MAIN, "btn_take");
	}
}

void comms_take::SetLabelText(bncs_string s)
{
	if (currentLabelText != s)
	{
		currentLabelText = s;
		if (currentLabelText.length() > 0)
		{
			controlShow(PNL_MAIN, "lbl_message");
			textPut("text", s, PNL_MAIN, "lbl_message");
		}
		else
		{
			controlHide(PNL_MAIN, "lbl_message");
		}
	}
}

bool comms_take::Is4WireRouted(bncs_string type)
{
	bncs_stringlist s(type,'|');
	if (s.count() == 2)
	{
		if (s[1] == "4")
			return true;
	}
	return false;
}

void comms_take::SetTake(bool IsSpecial, bncs_string only2Wire)
{
	fourWireNotPossible = only2Wire;
	if (specialTake != IsSpecial)
	{
		specialTake = IsSpecial;

		if (specialTake)
		{
			textPut("text", "TAKE|SPECIAL", PNL_MAIN, "btn_take");
			textPut("doublepush", "false", PNL_MAIN, "btn_take");
		}
		else
		{
			textPut("text", "TAKE", PNL_MAIN, "btn_take");
			textPut("doublepush", "true", PNL_MAIN, "btn_take");
		}
	}

	controlEnable(PNL_MAIN, "btn_take");
}