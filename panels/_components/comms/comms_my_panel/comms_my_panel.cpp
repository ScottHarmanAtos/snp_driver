#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_my_panel.h"


#define PANEL_MAIN  "comms_my_panel"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_my_panel )

// constructor - equivalent to ApplCore STARTUP
comms_my_panel::comms_my_panel( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();
	m_panelPort=comms_ringmaster_port();

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );
	controlDisable(PANEL_MAIN, "button");

	bncs_string strOpsPosition = getWorkstationSetting("ops_position");

	bncs_config cfgOps(bncs_string("ops_positions.%1").arg(strOpsPosition));
	
	cfgOps.childAttr("name");
	
	while (cfgOps.isChildValid())
	{
		bncs_string id = cfgOps.childAttr("id");
		if ( id == "comms_panel_port" )
		{
			 m_panelPort = rh->GetPort(cfgOps.childAttr("value"));
			 if ( m_panelPort.valid)
			 {
				 

				 bncs_string  strAssignableKeys, strPortDetails;
				 routerName(m_panelPort.devices.Grd, riedelHelpers::databases::Assignable_Keys, m_panelPort.port, strAssignableKeys);

				 if (m_panelPort.portType == PORT_TYPE_PANEL || m_panelPort.portType == PORT_TYPE_PANEL_DUAL_CHANNEL )
				 {
					 if ( isAssignableKeyPresent(strAssignableKeys) )
					 {
						 controlEnable(PANEL_MAIN, "button");
					 }
					 else
					 {
						 debug("comms_my_panel:: No assignable|keys on panel %1", m_panelPort.ring_port);
					 }
				 }
				 else
				 {
					 debug("comms_my_panel:: Port is %1 not a panel", m_panelPort.ring_port);
				 }
			 }
			 else
			 {
				 debug("comms_my_panel:: No instance defined");
				 
			 }

		}
		cfgOps.nextChild();
	}

	
	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_my_panel::~comms_my_panel()
{
}

// all button pushes and notifications come here
void comms_my_panel::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		bncs_string strExecute = bncs_string("adjust/adjust_intercom/assign_keys,%1:%2").arg(m_panelPort.sourceName_short.replace('|', ' ')).arg(m_panelPort.ring_port);
		navigateExecute(strExecute);	
	}
}

// all revertives come here
int comms_my_panel::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void comms_my_panel::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_my_panel::parentCallback( parentNotify *p )
{
	return "";
}

// timer events come here
void comms_my_panel::timerCallback( int id )
{
}

bool comms_my_panel::isAssignableKeyPresent( bncs_string keylist )
{
	bool flag= false;
	bncs_stringlist sltKeyList(keylist);
	for (int i=0; i<sltKeyList.count();i++)
	{
		if (sltKeyList[i].length()>0)
		{
			flag = true;
			break;
		}
	}
	return  flag;
}