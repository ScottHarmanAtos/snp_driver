#ifndef comms_my_panel_INCLUDED
	#define comms_my_panel_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_my_panel : public bncs_script_helper
{
public:
	comms_my_panel( bncs_client_callback * parent, const char* path );
	virtual ~comms_my_panel();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	bool isAssignableKeyPresent( bncs_string keylist );	
private:
	riedelHelper * rh;
	comms_ringmaster_port m_panelPort;
	
};


#endif // comms_my_panel_INCLUDED