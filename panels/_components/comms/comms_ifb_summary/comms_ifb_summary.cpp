#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "comms_ifb_summary.h"

#define PANEL_MAIN   "comms_ifb_summary"
#define TIMER_INIT   1
#define TIMEOUT_INIT   10

#define DATABASE_SOURCE_NAME  0
#define DATABASE_DEST_NAME    1

#define DATRABASE_DEFAULT_IFB_NAME   0

#define MAX_LABEL_CHAR  8
// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_ifb_summary )

// constructor - equivalent to ApplCore STARTUP
comms_ifb_summary::comms_ifb_summary( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();
	
	m_intRiedelGrdDevice = 0;
	m_intIfbNumber = 0;
	m_intRiedelGrdDevice =0 ;
	m_blnReadOnly = false;
	
	
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_ifb_summary::~comms_ifb_summary()
{
}

// all button pushes and notifications come here
void comms_ifb_summary::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN)
	{
		if ( b->id() == "adjust")
		{
			navigateExecute(bncs_string("adjust_ifb,%1").arg(m_intIfbNumber));
		}
		else
		{
			textPut("selected=none", PANEL_MAIN, b->id());
		}
	}
}

// all revertives come here
int comms_ifb_summary::revertiveCallback( revertiveNotify * r )
{
	r->dump("comms_ifb_summary");
	if ( r->device() == m_intRiedelIfbDevice && r->index() == m_intIfbNumber)
	{
		//Input port(s)|Mix minus port(s)|Output port(s)|Label			
		bncs_stringlist sltRevertive  = bncs_stringlist(r->sInfo(), '|');
		bncs_stringlist sltInputPorts = bncs_stringlist(sltRevertive[0]) ;
		bncs_stringlist sltMixMinusPorts = bncs_stringlist(sltRevertive[1]) ;
		m_sltOutputPorts = bncs_stringlist(sltRevertive[2]);
		bncs_string strLabel = sltRevertive[3];
		

		textPut("clear", PANEL_MAIN, "input");
		textPut("clear", PANEL_MAIN, "mix_minus");
		textPut("clear", PANEL_MAIN, "output");

		int i;
		for (i=0;i<sltInputPorts.count();i++)
		{
			bncs_string strInput;
			routerName(m_intRiedelGrdDevice, DATABASE_SOURCE_NAME, sltInputPorts[i], strInput );
			textPut("add", strInput.replace('|', ' '), PANEL_MAIN, "input");
		}

		for (i=0;i<sltMixMinusPorts.count();i++)
		{
			bncs_string strMixMinus;
			routerName(m_intRiedelGrdDevice, DATABASE_SOURCE_NAME, sltMixMinusPorts[i], strMixMinus );
			textPut("add", strMixMinus.replace('|', ' '), PANEL_MAIN, "mix_minus");
		}

		for (i=0;i<m_sltOutputPorts.count();i++)
		{
			bncs_string strOutput;
			routerName(m_intRiedelGrdDevice, DATABASE_DEST_NAME, m_sltOutputPorts[i], strOutput );
			routerRegister(m_intRiedelGrdDevice, m_sltOutputPorts[i].toInt(), m_sltOutputPorts[i].toInt(), true);
			routerPoll(m_intRiedelGrdDevice, m_sltOutputPorts[i].toInt(), m_sltOutputPorts[i].toInt());
			textPut("add", strOutput.replace('|', ' '), PANEL_MAIN, "output");
		}
		textPut("text", strLabel, PANEL_MAIN, "ifb_label");
	}
	return 0;
}

// all database name changes come back here
void comms_ifb_summary::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_ifb_summary::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		bncs_stringlist sl;
		
		sl << bncs_string( "ifb_number=%1" ).arg( m_intIfbNumber);
		sl << bncs_string( "read_only=%1" ).arg( m_blnReadOnly==true?"true":"false");
		
		return sl.toString( '\n' );
	}
	else if( p->command() == "ifb_number" )
	{
		m_intIfbNumber = p->value().toInt();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if( p->command() == "instance" )
	{
		auto ring = rh->GetRing(p->value());
		m_intRiedelIfbDevice = ring.devices.Ifb;
		m_intRiedelGrdDevice = ring.devices.Grd;

		timerStart(TIMER_INIT, TIMEOUT_INIT);
		textPut("instance", ring.devices.Grd_Instance, PANEL_MAIN, "pti");
	}
	else if(p->command() == "read_only")
	{
		m_blnReadOnly = p->value().lower() == "true" || p->value() == "1";

	}

	return "";
}

// timer events come here
void comms_ifb_summary::timerCallback( int id )
{
	if ( id == TIMER_INIT )
	{
		timerStop(id);

		init();
	}
}


void comms_ifb_summary::init()
{
	OutputDebugString(bncs_string("XXX IFBSUMMARY - INIT  ifb %1 grd %2 \n").arg(m_intRiedelIfbDevice).arg(m_intRiedelGrdDevice));
	bncs_string strIfbName;
	routerName(m_intRiedelIfbDevice, DATRABASE_DEFAULT_IFB_NAME, m_intIfbNumber, strIfbName);
	bncs_string strId(m_intIfbNumber, '0', 3, 10 ); 
		
	textPut("text", bncs_string("IFB %1").arg(strId) , PANEL_MAIN, "ifb_number");

	if (m_intIfbNumber && m_intRiedelIfbDevice)
	{
		infoRegister(m_intRiedelIfbDevice, m_intIfbNumber, m_intIfbNumber);
		infoPoll(m_intRiedelIfbDevice, m_intIfbNumber, m_intIfbNumber);
	}
	if (m_blnReadOnly)
	{
		controlDisable(PANEL_MAIN, "adjust");
	}
	else
	{
		controlEnable(PANEL_MAIN, "adjust");
	}

}