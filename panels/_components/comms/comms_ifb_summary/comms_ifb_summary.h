#ifndef comms_ifb_summary_INCLUDED
	#define comms_ifb_summary_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_ifb_summary : public bncs_script_helper
{
public:
	comms_ifb_summary( bncs_client_callback * parent, const char* path );
	virtual ~comms_ifb_summary();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper * rh;

	bncs_string m_myParam;
	int m_intIfbNumber;
	int m_intRiedelIfbDevice;
	int m_intRiedelGrdDevice;
	bool m_blnReadOnly;
	void init();
	bncs_stringlist m_sltOutputPorts ;

};


#endif // comms_ifb_summary_INCLUDED