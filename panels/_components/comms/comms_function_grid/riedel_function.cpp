#include "riedel_function.h"

riedel_function::riedel_function():
	device(0),
	database(0),
	offset(0)
{
}

riedel_function::riedel_function(int device, int database, int offset):
	device(0),
	database(0),
	offset(0)
{
	this->device = device;
	this->database = database;
	this->offset = offset;
}


riedel_function::~riedel_function()
{
}
