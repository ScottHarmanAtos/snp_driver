# comms_function_grid

## Commands

### local_ringport
This is used to decide which ring the component should use to find device numbers. This uses the `ring.port` standard as this is the most common notification emitted across comms components.

## Notifications

### page
The index of the page that has been selected on the grid.

### local_ring
The ring split out from the `ring.port` that the has been set to this component.

### function_device
The device number found from the `local_ring` for the specified function.

### function_db
The db number found from the `local_ring` for the specified function.

### function
The pressed function as the RiedelArtistDrv would expect it e.g. `CALLTOPORT` rather than `Call To|Port`.

### offset
The calculated mapping offset based on the pressed function.

### use_port_grid
Whether or not the selected function will work with a grid. E.g. conferences needs to use the custom conference grid.