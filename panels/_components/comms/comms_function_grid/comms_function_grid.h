#ifndef comms_function_grid_INCLUDED
	#define comms_function_grid_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>
#include <map>
#include "riedel_function.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_function_grid : public bncs_script_helper
{
public:
	comms_function_grid( bncs_client_callback * parent, const char* path );
	virtual ~comms_function_grid();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	// Parameters
	int m_local_ring;
	bncs_string m_instance;

	// General members
	bool m_port_mode;
	bncs_string m_active_function;
	bncs_string m_active_function_btn;
	map<bncs_string, riedel_function> function_devices;

	bncs_string m_gpi_type;

	riedelHelper* rh;

	// Methods
	void setupFunction(bncs_string id);
	bncs_string getFunction(bncs_string id);
	bncs_string strBool(bool value);
	void activeFunction(bncs_string id);
	void handleOffset(bncs_string id);
	void togglePortGrid();

	// Elements
	static const bncs_string grid_pages;
	static const bncs_string grid_functions;

	static const bncs_string btn_ring_base;
	static const bncs_string btn_function_base;

	// General
	static const int function_offset;
	static const int function_port_grid;

};


#endif // comms_function_grid_INCLUDED