#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_mixer_select.h"

#define PANEL_MAIN  "comms_mixer_select"

#define DEFAULT_STYLE_SELECTED    "enum_ok"
#define DEFAULT_STYLE_DESELECTED  "enum_notimportant"

#define TIMER_INIT   1
#define TIMEOUT_INIT   10


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_mixer_select )

// constructor - equivalent to ApplCore STARTUP
comms_mixer_select::comms_mixer_select( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
//	setSize( PANEL_MAIN );				// set the size to the same as the specified panel

	m_strLayout = "main";
	m_strStyleDeselected = DEFAULT_STYLE_DESELECTED;
	m_strStyleSelected = DEFAULT_STYLE_SELECTED;
	m_intInitialMixerSelection  = 0;
	m_strMixerGroup="";
//	timerStart(TIMER_INIT, TIMEOUT_INIT);

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_mixer_select::~comms_mixer_select()
{
}

// all button pushes and notifications come here
void comms_mixer_select::buttonCallback( buttonNotify *b )
{
	bncs_stringlist sltControlName = bncs_stringlist(b->id(), '_');
	bncs_string strControlPrefix = sltControlName[0];
	int intNewControlIndex = sltControlName[1].toInt();

	if( b->panel() == PANEL_MAIN && strControlPrefix == "mixer")
	{
		selectMixer(intNewControlIndex);
		

	}
}

// all revertives come here
int comms_mixer_select::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void comms_mixer_select::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_mixer_select::parentCallback( parentNotify *p )
{
	if( p->command() == "mixer" )
	{

	}
	else if ( p->command()  == "style_selected")
	{
		m_strStyleSelected = p->value();
	}
	else if ( p->command()  == "style_deselected")
	{
		m_strStyleDeselected = p->value();
	}
	else if ( p->command()  == "mixer_group" )
	{
		m_strMixerGroup = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if ( p->command()  == "initial_mixer_selection" )
	{
		m_intInitialMixerSelection = p->value().toInt();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if ( p->command()  == "return")
	{
		if (p->value() ==	"all" )
		{
			debug("HERE");
			bncs_stringlist slt;
			slt << bncs_string("style_selected=%1").arg(m_strStyleSelected);
			slt << bncs_string("style_deselected=%1").arg(m_strStyleDeselected);
			slt << bncs_string("mixer_group=%1").arg(m_strMixerGroup);
			slt << bncs_string("initial_mixer_selection=%1").arg(m_intInitialMixerSelection);
			slt << bncs_string("layout=%1").arg(m_strLayout);
			return slt.toString('\n');
		}
	}
	else if (p->command() == "layout")
	{
		if (p->value().length() > 0)
		{
			m_strLayout = p->value();
			panelDestroy(PANEL_MAIN);
			panelShow(PANEL_MAIN, bncs_string("\\layouts\\%1.bncs_ui").arg(m_strLayout));
		}
	}
	return "";
}

// timer events come here
void comms_mixer_select::timerCallback( int id )
{
	if ( id == TIMER_INIT)
	{
		timerStop(TIMER_INIT);
		init();

	}
}

void comms_mixer_select::init()
{
	bncs_string strButtonId;
	for (int i=1;i<=15;i++)
	{
		strButtonId = bncs_string("mixer_%1").arg(i);
		bncs_config cfgMixerListItem = bncs_config(bncs_string("comms_gallery_mixers.%1.%2").arg(m_strMixerGroup).arg(strButtonId)); 

		if ( cfgMixerListItem.isValid() )
		{
			controlEnable(PANEL_MAIN, strButtonId);
			textPut("text", cfgMixerListItem.attr("label"), PANEL_MAIN, strButtonId );
		}
		else
		{
			controlDisable(PANEL_MAIN, strButtonId);
		}
	}
	if ( m_intInitialMixerSelection>0)
	{
		selectMixer(m_intInitialMixerSelection);
	}
	
}

void comms_mixer_select::selectMixer( int mixerIndex )
{
	bncs_config cfgMixerIdx = bncs_config(bncs_string("comms_gallery_mixers.%1.mixer_%2").arg(m_strMixerGroup).arg(mixerIndex)); 
	if ( cfgMixerIdx.isValid())
	{
		int intMixer = cfgMixerIdx.attr("value").toInt();
		hostNotify(bncs_string("mixer=%1").arg(intMixer));
		bncs_stringlist sltButtons = getIdList(PANEL_MAIN, "mixer");
		for (bncs_stringlist::iterator it = sltButtons.begin() ; it != sltButtons.end(); ++it )
		{
			textPut("stylesheet", m_strStyleDeselected, PANEL_MAIN, *it);
		}
		textPut("stylesheet", m_strStyleSelected, PANEL_MAIN, bncs_string("mixer_%1").arg(mixerIndex));
	}
}


