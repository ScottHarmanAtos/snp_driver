#ifndef comms_mixer_select_INCLUDED
	#define comms_mixer_select_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_mixer_select : public bncs_script_helper
{
public:
	comms_mixer_select( bncs_client_callback * parent, const char* path );
	virtual ~comms_mixer_select();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void init();
	void selectMixer( int mixerIndex );
	
private:
	bncs_string m_myParam;
	bncs_string m_strStyleDeselected;
	bncs_string m_strStyleSelected;
	bncs_stringlist m_sltButtonList ;
	int m_intInitialMixerSelection;
	bncs_string m_strMixerGroup;
	bncs_string m_strLayout;

	
};


#endif // comms_mixer_select_INCLUDED