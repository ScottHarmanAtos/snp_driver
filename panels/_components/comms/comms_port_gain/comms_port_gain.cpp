#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_port_gain.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_port_gain )

#define PANEL_MAIN		"comms_port_gain"
#define POPUP_ADJUST	"comms_port_gain_popup"

// constructor - equivalent to ApplCore STARTUP
comms_port_gain::comms_port_gain( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	m_strInstance = "";
	m_type = modeType::INPUT;
	m_Port = comms_ringmaster_port();
	m_dblGain = 0;

	m_strLayout = "main";
	panelShow( PANEL_MAIN, bncs_string("%1.bncs_ui" ).arg(m_strLayout));

	textPut("text", "Input", PANEL_MAIN, "label");
	textPut("text", "", PANEL_MAIN, "adjust");
	controlDisable(PANEL_MAIN, "adjust");

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_port_gain::~comms_port_gain()
{
}

// all button pushes and notifications come here
void comms_port_gain::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == "adjust")
		{
			panelPopup(POPUP_ADJUST, "popup_adjust.bncs_ui");
			if(m_type == modeType::OUTPUT)
			{
				textPut("text", "Output Gain", POPUP_ADJUST, "title");
			}
			else
			{
				textPut("text", "Input Gain", POPUP_ADJUST, "title");
			}
			updateValue();
		}
	}
	else if(b->panel() == POPUP_ADJUST)
	{
		if(b->id() == "up_coarse")
		{
			adjustValue(3);
		}
		else if(b->id() == "up_fine")
		{
			adjustValue(0.5);
		}
		else if(b->id() == "down_fine")
		{
			adjustValue(-0.5);
		}
		else if(b->id() == "down_coarse")
		{
			adjustValue(-3);
		}
		else if(b->id() == "default")
		{
			setValue(0);
		}
		else if(b->id() == "close")
		{
			panelDestroy(POPUP_ADJUST);
		}
	}
}

// all revertives come here
int comms_port_gain::revertiveCallback( revertiveNotify * r )
{
	debug("comms_port_gain::revertiveCallback index=%1 value=%2", r->index(), r->sInfo());
	if (r->device() == m_Port.devices.Gain)
	{
		if(r->index() == m_intGainSlot)
		{
			m_dblGain = r->sInfo().toDouble();
			updateValue();
		}
	}
	return 0;
}

// all database name changes come back here
void comms_port_gain::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_port_gain::parentCallback( parentNotify *p )
{
	if(p->command() == "mode")
	{
		if(p->value().upper() == "OUTPUT")
		{
			m_type = modeType::OUTPUT;
			textPut("text", "Output", PANEL_MAIN, "label");
		}
		else
		{
			m_type = modeType::INPUT;
			textPut("text", "Input", PANEL_MAIN, "label");
		}
	}
	else if(p->command() == "index")
	{
		bool blnGainAvailable = false;
		m_Port = rh->GetPort(p->value());

		//get port type
		int intPortType = m_Port.portType;
		if (intPortType == PORT_TYPE_SPLIT || intPortType == PORT_TYPE_4WIRE)
		{
			blnGainAvailable = true;
		}
		
		if(m_Port.valid && blnGainAvailable)
		{
			if(m_type == modeType::OUTPUT)
			{
				m_intGainSlot = m_Port.port + riedelHelpers::offset::OUTPUT_GAIN;
			}
			else
			{
				m_intGainSlot = m_Port.port;
			}
			if (m_Port.devices.Gain > 0)
			{
				infoRegister(m_Port.devices.Gain, m_intGainSlot, m_intGainSlot);
				infoPoll(m_Port.devices.Gain, m_intGainSlot, m_intGainSlot);
			}
			controlEnable(PANEL_MAIN, "adjust");
		}
		else
		{
			m_intGainSlot = 0;
			if (m_Port.devices.Gain > 0)
			{
				infoUnregister(m_Port.devices.Gain);
			}
			textPut("text", "", PANEL_MAIN, "adjust");
			controlDisable(PANEL_MAIN, "adjust");
		}
	}
	//Return Commands
	else if( p->command() == "return" )
	{
		//Called by visual editor to persist settings in bncs_ui
		if(p->value() == "all")
		{
			bncs_stringlist sl;
			
			sl << bncs_string( "layout=" ) + bncs_string(m_strLayout);
			sl << bncs_string( "mode=" ) + bncs_string((m_type == modeType::OUTPUT)?"OUTPUT":"INPUT");
			
			return sl.toString('\n');
		}
	}
	else if( p->command() == "_commands" )
	{
		bncs_stringlist sl;
		
		sl << "index=<port index>";
		
		return sl.toString( '\n' );
	}
	else if(p->command() == "layout")
	{
		if ( p->value().length() >0 && p->value() != m_strLayout)
		{
			m_strLayout = p->value();
			panelDestroy(PANEL_MAIN);
			panelShow(PANEL_MAIN, bncs_string("%1.bncs_ui").arg(m_strLayout));
		}
	}
	return "";
}

// timer events come here
void comms_port_gain::timerCallback( int id )
{
}

void comms_port_gain::updateValue()
{
	char szGain[16];
	sprintf_s(szGain, "%1.1f", m_dblGain);
	bncs_string strDisplay = bncs_string("%1 dB").arg(szGain);
	textPut("text", strDisplay, PANEL_MAIN, "adjust");
	textPut("text", strDisplay, POPUP_ADJUST, "value");

	if ( m_dblGain == 0.0 )
	{
		textPut("stylesheet", "enum_deselected", PANEL_MAIN, "adjust");
	}
	else
	{
		textPut("stylesheet", "enum_warning", PANEL_MAIN, "adjust");
	}
}

void comms_port_gain::adjustValue(double dblAdjust)
{
	double dblNewValue = m_dblGain + dblAdjust;
	char szGain[16];
	sprintf_s(szGain, "%1.1f", dblNewValue);
	if (m_Port.devices.Gain > 0 && m_intGainSlot > 0)
	{
		infoWrite(m_Port.devices.Gain, bncs_string("%1").arg(szGain), m_intGainSlot);
	}
}

void comms_port_gain::setValue(double dblValue)
{
	char szGain[16];
	sprintf_s(szGain, "%1.1f", dblValue);
	if (m_Port.devices.Gain > 0 && m_intGainSlot > 0)
	{
		infoWrite(m_Port.devices.Gain, bncs_string("%1").arg(szGain), m_intGainSlot);
	}
}
