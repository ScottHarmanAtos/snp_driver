#ifndef comms_port_gain_INCLUDED
	#define comms_port_gain_INCLUDED

#include <bncs_script_helper.h>
#include <riedelHelper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_port_gain : public bncs_script_helper
{
public:
	comms_port_gain( bncs_client_callback * parent, const char* path );
	virtual ~comms_port_gain();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:
	riedelHelper *rh;

	void setValue(double dblValue);
	void adjustValue(double dblAdjust);
	void updateValue();
	double m_dblGain;
	int m_intGainSlot;
	comms_ringmaster_port m_Port;
	bncs_string m_strInstance;
	bncs_string m_strLayout;

	enum modeType
	{
		INPUT  =1,
		OUTPUT
	};

	modeType m_type;
};


#endif // comms_port_gain_INCLUDED