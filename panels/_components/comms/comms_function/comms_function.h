#ifndef comms_function_INCLUDED
	#define comms_function_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"
#include <vector>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif

enum CommandName
{
	NA,
	CALLTOPORT,
	TRUNKCALL,
	TRUNKIFB,
	CALLTOGROUP,
	LISTENPORT,
	CONFERENCE,
	IFB,
	LOGIC,
	SENDSTRING
};

struct function
{
public:
	CommandName Command;
	int Ring;
	int Value;
	bncs_string Fullvalue;

	//The label if there is one
	bncs_string Label;

	//Device and slot used to find label
	int Device;
	int Slot;
};

class comms_function : public bncs_script_helper
{
public:
	comms_function( bncs_client_callback * parent, const char* path );
	virtual ~comms_function();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	riedelHelper* rh;
	int m_index;
	bncs_string m_label;
	bncs_string m_alias;
	int m_device;
	bool m_map_mode;
	void init();
	void update();
	void select(bool select);
	vector<function> GetFunctions(const bncs_string& string);
	int m_receviedAll;

	bncs_string m_function;
	vector<function> m_functions;
};


#endif // comms_function_INCLUDED