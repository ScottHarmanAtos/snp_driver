#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_function.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

#define DATABASE_LABEL 10
#define DATABASE_FUNCTIONs 11
#define DATABASE_MAPPING 12

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_function )

// constructor - equivalent to ApplCore STARTUP
comms_function::comms_function( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_label = "";
	m_function = "";
	m_index = 0;
	m_map_mode = false;
	m_receviedAll = 0;
	rh = riedelHelper::getriedelHelperObject();

	m_device = rh->GetRingmasterDevices().Main;

	// show a panel from file comms_function.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PNL_MAIN, "comms_function.bncs_ui" );
	select(false);
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_function::~comms_function()
{
}

// all button pushes and notifications come here
void comms_function::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if(!m_map_mode) select(true);
		hostNotify("button=released");
		hostNotify("index." + bncs_string("%1").arg(m_index) + "=" + m_index);
		hostNotify(bncs_string("index_only=").append(m_index));
		hostNotify(bncs_string("function=%1").arg(m_function));
	}
}

// all revertives come here
int comms_function::revertiveCallback( revertiveNotify * r )
{
	for (int i = 0; i < (int)m_functions.size(); i++)
	{
		function& f = m_functions[i];
		if (f.Device == r->device() && f.Slot == r->index())
		{
			auto oldLabel = f.Label;
			if (f.Command == CommandName::IFB)
			{
				bncs_stringlist sl(r->sInfo(),'|');
					if (sl.count() >= 4 && sl[3].length() > 0)
					{
						f.Label = sl[3];
					}
					else
					{
						f.Label = "";
					}
			}
			else if (f.Command == CommandName::CONFERENCE)
			{
				if (r->sInfo().length() == 0)
				{
					bncs_string lbl;
					routerName(f.Device, riedelHelpers::databases::Conference_Name, f.Value, lbl);
					f.Label = lbl;
				}
				else
				{
					f.Label = r->sInfo();
				}
			}
			else
			{
				f.Label = r->sInfo();
				break;
			}
			if (oldLabel != f.Label)
			{
				update();
			}
			break;
		}
	}
	return 0;
}

// all database name changes come back here
void comms_function::databaseCallback(revertiveNotify* r)
{
	if (r->index() == m_index)
	{
		if (DATABASE_LABEL == r->database())
		{
			m_label = r->sInfo();
		}
		else if (DATABASE_FUNCTIONs == r->database())
		{
			m_function = r->sInfo();
		}
		update();
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_function::parentCallback( parentNotify *p )
{
	if (p->command() == "index")
	{
		m_receviedAll = m_receviedAll | 1;
		if (m_index != p->value())
		{
			m_index = p->value();
				init();
		}
	}
	else if (p->command() == "selected")
	{
		select(true);
	}
	else if (p->command() == "deselect")
	{
		select(false);
	}

	else if (p->command() == "map_mode")
	{
		m_receviedAll = m_receviedAll | 2;

		if (p->value() == "true")
		{
			if (m_map_mode == false)
			{
				m_map_mode = true;
				//This is runtime so s
				//panelDestroy(PNL_MAIN);
				//panelShow(PNL_MAIN, "mapping.bncs_ui");
			}
		}
		else
			m_map_mode = false;
		init();
	}
	else if (p->command() == "save")
	{
		if (p->subs() == 3)
		{
			bncs_string instance = p->sub(0);
			int offset = p->sub(1).toInt();
			int page = p->sub(2).toInt();

			routerModify(m_device, DATABASE_MAPPING, offset + page, p->value(), true);
		}
	}

	else if (p->command() == "return")
	{
		if (p->value().startsWith("getIndex"))
		{
			bncs_stringlist sl(p->value(), '.');

			if (sl.count() == 4)
			{
				bncs_string instance = sl[1];
				bncs_string offset = sl[2];
				bncs_string page = sl[3];

				bncs_string value;
				routerName(m_device, DATABASE_MAPPING, offset.toInt() + page.toInt(), value);

				return bncs_string("%1=%2").arg(p->value()).arg(value);
			}
		}
		else
		{
			bncs_stringlist sl;

			sl << bncs_string("index=").append(m_index);

			return sl.toString('\n');
		}
	}
	return "";
}

// timer events come here
void comms_function::timerCallback( int id )
{

}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void comms_function::init()
{
	//Doing bitwise counting, 3 means we have received both the values we are waiting for
	if (m_receviedAll == 3)
	{
		select(false);
		infoRegister(m_device, 4096, 4096, false);

		routerName(m_device, DATABASE_LABEL, m_index, m_label);
		routerName(m_device, DATABASE_FUNCTIONs, m_index, m_function);

		m_functions = GetFunctions(m_function);

		update();	
	}
}

void comms_function::update()
{
	if (m_function.length() == 0 || m_function == "!!!")
	{
		textPut("text", "", PNL_MAIN, "Button");
		textPut("text", "", PNL_MAIN, "Label");
		if (m_map_mode == false)
		{
			controlDisable(PNL_MAIN, "Button");
			controlDisable(PNL_MAIN, "Label");
		}
		return;
	}

	controlEnable(PNL_MAIN, "Button");
	controlEnable(PNL_MAIN, "Label");

	textPut("text", m_label, PNL_MAIN, "Button");

	bncs_stringlist sl;
	for (auto fun : m_functions)
	{
		if (fun.Label.length() > 0)
		{
			sl.append(fun.Label);
		}
	}
	textPut("text", sl.toString(','), PNL_MAIN, "Label");
}

void comms_function::select(bool select)
{
	if (select)
	{
		textPut("statesheet", "source_selected", PNL_MAIN, "Button");
	}
	else
	{
		textPut("statesheet", "source_deselected", PNL_MAIN, "Button");
	}
}


vector<function> comms_function::GetFunctions(const bncs_string& string)
{
	vector<function> fun;
	bncs_stringlist funcs = bncs_stringlist(string, ',');
	//Set Functions
	//{FUNCTION}[,{FUNCTION}
	for (int i = 0; i < funcs.count(); i++)
	{
		//FunctionName|Value|Flag
		bncs_stringlist fvf = bncs_stringlist(funcs[i], '|');
		if (fvf.count() < 2) continue;

		function f;
		auto commandName = fvf[0].upper();
		if (commandName == "CALLTOPORT")f.Command = CommandName::CALLTOPORT;
		else if (commandName == "TRUNKCALL")f.Command = CommandName::TRUNKCALL;
		else if (commandName == "TRUNKIFB")f.Command = CommandName::TRUNKIFB;
		else if (commandName == "CALLTOGROUP")f.Command = CommandName::CALLTOGROUP;
		else if (commandName == "LISTENTOPORT")f.Command = CommandName::LISTENPORT;
		else if (commandName == "CONFERENCE")f.Command = CommandName::CONFERENCE;
		else if (commandName == "IFB")f.Command = CommandName::IFB;
		else if (commandName == "LOGIC")f.Command = CommandName::LOGIC;
		else if (commandName == "SENDSTRING")f.Command = CommandName::SENDSTRING;

		f.Fullvalue = fvf[1];
		
		if (f.Fullvalue.contains('.') > 0)
		{
			bncs_string ring, value;
			f.Fullvalue.split('.', ring, value);
			f.Ring = ring.toInt();
			f.Value = value.toInt();
		}
		else
		{
			f.Ring = 0;
			f.Value = f.Fullvalue.toInt();
		}

		auto ring = rh->GetRing(f.Ring);

		if (f.Command == CommandName::CALLTOPORT)
		{
			f.Device = ring.devices.Alias;
			f.Slot = f.Value;

			infoRegister(f.Device, f.Slot, f.Slot, true);
			infoPoll(f.Device, f.Slot, f.Slot);
		}
		else if (f.Command == CommandName::CONFERENCE)
		{
			f.Device = rh->GetRings().begin()->second.devices.Conference;//Grab ring from first conference.
			f.Slot = f.Value + riedelHelpers::offset::CONF_LABEL;

			bncs_string lbl;
			routerName(f.Device, riedelHelpers::databases::Conference_Name, f.Value, lbl);
			f.Label = lbl;

			infoRegister(f.Device, f.Slot, f.Slot, true);
			infoPoll(f.Device, f.Slot, f.Slot);
		}
		else if (f.Command == CommandName::IFB)
		{
			if (f.Ring == 0)
			{
				f.Device = rh->GetRingmasterDevices().Ifbs;
				f.Slot = f.Value + riedelHelpers::offset::CONF_LABEL;
			}
			else
			{
				f.Device = ring.devices.Ifb;
				f.Slot = f.Value;
			}
			infoRegister(f.Device, f.Slot, f.Slot, true);
			infoPoll(f.Device, f.Slot, f.Slot);
		}

		fun.push_back(f);
	}

	return fun;
}