#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_conference_summary.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_conference_summary )

#define PNL_MAIN				"comms_conference_summary"

#define TIMER_INIT   1
#define TIMEOUT_INIT   10


// constructor - equivalent to ApplCore STARTUP
comms_conference_summary::comms_conference_summary( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	//init vars
	m_intConference = 0;
	m_intDeviceConference = 0;
	m_strInstance = "";
	m_blnReadOnly = true;
	
	m_intDeviceConference = rh->GetRingmasterDevices().Conferences;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PNL_MAIN, "main.bncs_ui" );
	m_slPanelIDs = getIdList(PNL_MAIN);
	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_conference_summary::~comms_conference_summary()
{
}

// all button pushes and notifications come here
void comms_conference_summary::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PNL_MAIN )
	{
		if( b->id() == "conference" )
		{
			//hostNotify(bncs_string("select=%1").arg(m_intConference));
			navigateExecute(bncs_string("_components/comms/comms_edit_conference,%1").arg(m_intConference));
		}
	}
}

// all revertives come here
int comms_conference_summary::revertiveCallback( revertiveNotify * r )
{
	if( r->device() == m_intDeviceConference )
	{
		if( r->index() == m_intConference)
		{
			bncs_stringlist sltPortMembers(r->sInfo());
			textPut( "text", sltPortMembers.count(), PNL_MAIN, "port_count" );
		}
		else if( r->index() == m_intConference + riedelHelpers::offset::CONF_LABEL)
		{
			textPut( "text", r->sInfo(), PNL_MAIN, "name" );
		}
		else if( r->index() == m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS)
		{
			bncs_stringlist sltPortMembers(r->sInfo());
			textPut( "text", sltPortMembers.count(), PNL_MAIN, "panel_count" );
		}
	}
	return 0;
}

// all database name changes come back here
void comms_conference_summary::databaseCallback( revertiveNotify * r )
{
	if ( r->device() == m_intDeviceConference && r->index() == m_intConference && r->database() == riedelHelpers::databases::Conference_Name)
	{
		textPut("text", r->sInfo(), PNL_MAIN, "conference");
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_conference_summary::parentCallback( parentNotify *p )
{
	debug("comms_conference_summary::parentCallback() cmd=%1 value=%2", p->command(), p->value());
	if(p->command() == "conference")
	{
		m_intConference = p->value().toInt();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if(p->command() == "read_only")
	{
		m_blnReadOnly = p->value().lower() == "true" || p->value() == "1";
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if(p->command() == "return")
	{
		bncs_stringlist slt;
		slt << bncs_string("conference=%1").arg(m_intConference);
		slt << bncs_string("read_only=%1").arg(m_blnReadOnly?"true":"false");
		return slt.toString('\n');
	}
	else if (p->command() == "raw_parameter")
	{

	}

	return "";
}

// timer events come here
void comms_conference_summary::timerCallback( int id )
{
	if (id == TIMER_INIT)
	{
		init();
		timerStop(TIMER_INIT);
	}
}

void comms_conference_summary::init()
{
	//TODO init
	if (m_intConference >0)
	{
		for (int i=0; i<m_slPanelIDs.count();i++)
		{
			controlEnable(PNL_MAIN, m_slPanelIDs[i]);
		}
		//TEMP
		bncs_string strName;
		routerName(m_intDeviceConference, riedelHelpers::databases::Conference_Name, m_intConference, strName);
		//		debug("****** m_intdeviceconference=%1 m_intConference=%2 strName=%3", m_intDeviceConference, m_intConference, strName);		
		textPut("text", strName, PNL_MAIN, "conference");
		textPut("text", "", PNL_MAIN, "name");
		textPut("text", "0", PNL_MAIN, "port_count");
		textPut("text", "0", PNL_MAIN, "panel_count");
		
		infoRegister(m_intDeviceConference, m_intConference, m_intConference);
		infoRegister(m_intDeviceConference, m_intConference + riedelHelpers::offset::CONF_LABEL, m_intConference + riedelHelpers::offset::CONF_LABEL, true);
		infoRegister(m_intDeviceConference, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS, true);
		infoPoll(m_intDeviceConference, m_intConference, m_intConference);
		infoPoll(m_intDeviceConference, m_intConference + riedelHelpers::offset::CONF_LABEL, m_intConference + riedelHelpers::offset::CONF_LABEL);
		infoPoll(m_intDeviceConference, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS, m_intConference + riedelHelpers::offset::CONF_PANEL_MEMBERS);
	}
	else
	{
		textPut("text", "", PNL_MAIN, "conference");
		textPut("text", "", PNL_MAIN, "name");
		textPut("text", "0", PNL_MAIN, "port_count");
		textPut("text", "0", PNL_MAIN, "panel_count");
		for (int i=0; i<m_slPanelIDs.count();i++)
		{
			controlDisable(PNL_MAIN, m_slPanelIDs[i]);
		}
	}
	if ( m_blnReadOnly)
	{
		controlDisable(PNL_MAIN, "conference");
	}
	else
	{
		controlEnable(PNL_MAIN, "conference");
	}

} 