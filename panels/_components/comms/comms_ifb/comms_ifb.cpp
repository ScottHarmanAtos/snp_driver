#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "comms_ifb.h"

#define PANEL_MAIN  "comms_ifb"
#define POPUP_KEYBOARD  "comms_ifb_keyboard_popup"

#define TIMER_INIT  1
#define TIMEOUT_INIT  10

#define PAD_STRING  "    "


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( comms_ifb )

// constructor - equivalent to ApplCore STARTUP
comms_ifb::comms_ifb( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	rh = riedelHelper::getriedelHelperObject();

	m_intDeviceRiedelIFB = rh->GetRingmasterDevices().Ifbs;
	m_intDeviceRiedelGRD = rh->GetRingmasterDevices().Main;

	m_strInstance = "";

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "main.bncs_ui" );

}

// destructor - equivalent to ApplCore CLOSEDOWN
comms_ifb::~comms_ifb()
{
}

// all button pushes and notifications come here
void comms_ifb::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if (b->id() == "ifb_label")
		{
			panelPopup(POPUP_KEYBOARD, "popup_edit_label.bncs_ui");
		}
	}
}

// all revertives come here
int comms_ifb::revertiveCallback( revertiveNotify * r )
{
	if ( r->device() == m_intDeviceRiedelIFB)
	{
		if ( r->index() == m_intIfbNumber)
		{
			bncs_stringlist sltRevertive(r->sInfo(), '|');
			auto inPort = rh->GetPort(sltRevertive[0]);
			auto mixPort = rh->GetPort(sltRevertive[1]);
			auto outPort = rh->GetPort(sltRevertive[2]);
			bncs_string strLabel = sltRevertive[3];

			textPut("text", inPort.ring_port, PANEL_MAIN, "input_index");
			textPut("text", bncs_string("%1%2").arg(PAD_STRING).arg(inPort.sourceName_short), PANEL_MAIN, "input_port");
			textPut("text", mixPort.ring_port, PANEL_MAIN, "mixminus_index");
			textPut("text", bncs_string("%1%2").arg(PAD_STRING).arg(mixPort.sourceName_short), PANEL_MAIN, "mixminus_port");
			textPut("text", outPort.ring_port, PANEL_MAIN, "output_index");
			textPut("text", bncs_string("%1%2").arg(PAD_STRING).arg(outPort.destName_short), PANEL_MAIN, "output_port");
			textPut("text", bncs_string("%1%2").arg(PAD_STRING).arg(strLabel), PANEL_MAIN, "ifb_label");
			textPut("text", inPort.rrcs_address, PANEL_MAIN, "input_rrcs");
			textPut("text", mixPort.rrcs_address, PANEL_MAIN, "mixminus_rrcs");
			textPut("text", outPort.rrcs_address, PANEL_MAIN, "output_rrcs");
			updateMixMinusTrace(mixPort);
		}
	}


	return 0;
}

// all database name changes come back here
void comms_ifb::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string comms_ifb::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		bncs_stringlist sl;
		
		sl << bncs_string( "ifb_number=%1" ).arg( m_intIfbNumber );
		
		return sl.toString( '\n' );
	}
	else if( p->command() == "ifb_number" )
	{
		m_intIfbNumber = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if( p->command() == "instance" )
	{
		m_strInstance = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}

	return "";
}

// timer events come here
void comms_ifb::timerCallback( int id )
{
	if ( id == TIMER_INIT)
	{
		timerStop(TIMER_INIT);
		init();
	}
}

void comms_ifb::init()
{
	if ( m_intIfbNumber )
	{
		infoRegister(m_intDeviceRiedelIFB, m_intIfbNumber, m_intIfbNumber)	;
		infoPoll(m_intDeviceRiedelIFB, m_intIfbNumber, m_intIfbNumber)	;
		bncs_string strDefaultLabel;
		routerName(m_intDeviceRiedelIFB, riedelHelpers::databases::IFB_Name, m_intIfbNumber, strDefaultLabel);
		textPut("text", bncs_string ("IFB %1: %2").arg(bncs_string(m_intIfbNumber,'0',3)).arg(strDefaultLabel), PANEL_MAIN, "grp_background");
	}
	else
	{
		//TODO NOT CONFIGURED

	}

}

void comms_ifb::updateMixMinusTrace( const comms_ringmaster_port& mixMinusPort )
{
	bncs_string strUpstreamRouterDetails;
	routerName(mixMinusPort.devices.Grd, riedelHelpers::databases::Upstream_Details, mixMinusPort.ring_port, strUpstreamRouterDetails);
	bncs_stringlist sltUpstreamRouterDetails(strUpstreamRouterDetails);
	bncs_string strRouterInstance = sltUpstreamRouterDetails[0];
	int intRouterDest = sltUpstreamRouterDetails[1].toInt();

	if ( strUpstreamRouterDetails.length() >0 )
	{
		textPut("global_dest", intRouterDest , PANEL_MAIN, "audio_tally");		
		controlShow(PANEL_MAIN, "audio_tally");
		controlShow(PANEL_MAIN, "audio_label");
	//	controlShow(PANEL_MAIN, "hydra_tally");


	}
	else
	{
		controlHide(PANEL_MAIN, "audio_tally");
		controlHide(PANEL_MAIN, "audio_label");
//		controlHide(PANEL_MAIN, "hydra_tally");


	}

}
