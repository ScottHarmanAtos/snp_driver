#ifndef comms_ifb_INCLUDED
	#define comms_ifb_INCLUDED

#include <bncs_script_helper.h>
#include "riedelHelper.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class comms_ifb : public bncs_script_helper
{
public:
	comms_ifb( bncs_client_callback * parent, const char* path );
	virtual ~comms_ifb();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void init();
	void updateMixMinusTrace(const comms_ringmaster_port& mixMinusPort);
	
private:
	riedelHelper * rh;
	int m_intIfbNumber;
	bncs_string m_strInstance;
	int m_intDeviceRiedelIFB;
	int m_intDeviceRiedelGRD;
	
};


#endif // comms_ifb_INCLUDED