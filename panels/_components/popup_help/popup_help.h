#ifndef popup_help_INCLUDED
	#define popup_help_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class popup_help : public bncs_script_helper
{
public:
	popup_help( bncs_client_callback * parent, const char* path );
	virtual ~popup_help();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void init();
	
private:
	//bncs_string m_myParam;
	bncs_string m_strHelpPage;
	bncs_string m_instance;
	bncs_string m_strPersistedHelpPage;
	
};


#endif // popup_help_INCLUDED