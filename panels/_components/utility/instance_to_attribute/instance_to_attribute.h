#ifndef instance_to_attribute_INCLUDED
	#define instance_to_attribute_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class instance_to_attribute : public bncs_script_helper
{
public:
	instance_to_attribute( bncs_client_callback * parent, const char* path );
	virtual ~instance_to_attribute();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void init();
	
private:
	bncs_string m_strAttribute;
	bncs_string m_strInstance;
	bncs_string m_strAlias;
	bncs_string m_strAttributeValue;
	bncs_string m_strSendAsAttribute;
	
};


#endif // instance_to_attribute_INCLUDED