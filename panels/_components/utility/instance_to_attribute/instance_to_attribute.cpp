#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "instance_to_attribute.h"

#define PANEL_MAIN		1

#define TIMER_INIT		1
#define TIMEOUT_INIT	10

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( instance_to_attribute )

// constructor - equivalent to ApplCore STARTUP
instance_to_attribute::instance_to_attribute( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file main.bncs_ui and we'll know it as our panel PANEL_MAIN
	panelShow( PANEL_MAIN, "main.bncs_ui" );
	m_strAttribute = "";
	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PANEL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
instance_to_attribute::~instance_to_attribute()
{
}

// all button pushes and notifications come here
void instance_to_attribute::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		
		hostNotify(bncs_string("%1=%2").arg(m_strSendAsAttribute.length() >0?m_strSendAsAttribute:m_strAttribute).arg(m_strAttributeValue));
	}
}

// all revertives come here
int instance_to_attribute::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void instance_to_attribute::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string instance_to_attribute::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;
			
			sl << bncs_string( "attribute=%1" ).arg( m_strAttribute );
			sl << bncs_string( "alias=%1" ).arg( m_strAlias );
			sl << bncs_string( "send_as=%1" ).arg( m_strSendAsAttribute );
			
			return sl.toString( '\n' );
		}

		else if( p->value() == "attribute" )
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_strAttribute ) );
		}
		else if( p->value() == "alias" )
		{	// Specific value being asked for by a textGet
			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_strAlias) );
		}
	}
	else if( p->command() == "instance" && p->value() != m_strInstance )
	{	// Our instance is being set/changed
		m_strInstance = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if( p->command() == "alias" && p->value() != m_strAlias )
	{	// Our instance is being set/changed
		m_strAlias = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if( p->command() == "attribute" && p->value() != m_strAttribute )
	{	// Persisted value or 'Command' being set here
		m_strAttribute = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}
	else if( p->command() == "send_as" && p->value() != m_strSendAsAttribute )
	{	// Persisted value or 'Command' being set here
		m_strSendAsAttribute = p->value();
		timerStart(TIMER_INIT, TIMEOUT_INIT);
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void instance_to_attribute::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_INIT:
		timerStop(id);
		init();
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////



void instance_to_attribute::init()
{
	controlDisable(PANEL_MAIN, "button");
	if ( m_strInstance.length() >0 )
	{
		if (m_strAttribute.length() >0)
		{
			textPut("text", m_strAlias.length()>0?m_strAlias:m_strAttribute, PANEL_MAIN, "button");
			bncs_config cfg = bncs_config(bncs_string("instances.%1").arg(m_strInstance));
			if (cfg.isValid())
			{
				controlEnable(PANEL_MAIN, "button");
				m_strAttributeValue = cfg.attr(m_strAttribute);
			}
			if (m_strAttributeValue.length() >0 )
			{
				debug("instance_to_attribute:: instance=%1 attribute=%2 value=%3", m_strInstance, m_strAttribute, m_strAttributeValue);
				hostNotify(bncs_string("%1=%2").arg(m_strSendAsAttribute.length() >0?m_strSendAsAttribute:m_strAttribute).arg(m_strAttributeValue));
			}
		}
		else
		{
			debug("instance_to_attribute:: attribute not set - nothing to look for");
		}
	}
	else
	{
		debug("instance_to_attribute:: instance not set - nothing to look for");
	}
}
