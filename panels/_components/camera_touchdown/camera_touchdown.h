#ifndef camera_touchdown_INCLUDED
	#define camera_touchdown_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class camera_touchdown : public bncs_script_helper
{
public:
	camera_touchdown( bncs_client_callback * parent, const char* path );
	virtual ~camera_touchdown();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_sSdiRouterInstance;
	bncs_string m_sTouchdownMarketBaseInstance;
	bncs_string m_sTouchDownControlId;

	bncs_string m_sTouchdownMarketConfigInstance;
	bncs_string m_iTouchdownMarketConfigDevice;

	bncs_string m_sTouchdownMarket_GpiInputInstance;
	bncs_string m_sTouchdownMarket_SdiRouterInstance;
		
	bool m_bTouchDownMarket_TestMode;

	void passToAllTouchDowns(bncs_string sParam, bncs_string sValue);
	void loadInstancesConfig();
	void showGpiControlsIfTestMode();


};


#endif // camera_touchdown_INCLUDED