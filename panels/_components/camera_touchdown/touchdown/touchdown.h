#ifndef touchdown_INCLUDED
	#define touchdown_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
#define PANEL_MAIN		1
	
 
class touchdown : public bncs_script_helper
{
public:
	touchdown( bncs_client_callback * parent, const char* path );
	virtual ~touchdown();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	// General useage
	bncs_string m_sControlType;
	bncs_string m_sControlId;



	bool m_bEnabled;
	bool m_bSelected;
		
	//Internal methods
	void init();								// called by the parent via a parantCallback "method"
	void loadAndRegisterTouchdownAutoConfig();
	void loadAndRegisterTouchdownSpecificConfig();
	void loadInstancesConfig();
	void updateButtonState();

	bncs_string m_sTouchdownMarket_GpiInputInstance;
	int m_iInstances_Config_Gpi_input_device;
	
	bncs_string m_sTouchdownMarket_SdiRouterInstance;
	int m_iInstances_Config_Sdi_router_device;

	bool m_bTouchDownMarket_TestMode;

	// gpi_touchdown_auto.xml
	bncs_string m_sTouchdownMarketConfigInstance;
	int m_iTouchdownMarketConfigDevice;

	bncs_string m_sTouchdownMarketBaseInstance;


	bncs_string m_sAuto_Status;
	bncs_string m_sAuto_Touchdown_Destination;
	bncs_string m_sAuto_Wfm_Destination;
	bncs_string m_sAuto_Robotic_Destination;
	bncs_string m_sAuto_Default_Rcp_Flyback;
	bncs_string m_sAuto_Rcp_Flyback;
	bncs_string m_sAuto_Wfm_Monitor;
	bncs_string m_sAuto_TouchDown_Monitor;

	int m_iAuto_Status_Slot;
	int m_iAuto_Touchdown_Destination_Slot;
	int m_iAuto_Wfm_Destination_Slot;
	int m_iAuto_Robotic_Destination_Slot;
	int m_iAuto_Default_Rcp_Flyback_Slot;
	int m_iAuto_Rcp_Flyback_Slot;
	int m_iAuto_Wfm_Monitor_Slot;
	int m_iAuto_TouchDown_Monitor_Slot;

	// gpi_touchdown.xml specific to each control
	bncs_string m_sTouchdownInstance;
	int m_iTouchdownDevice;			

	
	// Touchdown/Robotics use specific for each touchdown/robotic control
	bncs_string m_sTouchdownType;
	bncs_string m_sGpi_Status;
	int m_iGpi_input;
	int m_iGpi_input_With_Offset;
	bncs_string m_sDefault_sdi_source;
	bncs_string m_sSdi_source;
	bncs_string m_sLatching_input;

	int m_iTouchdownTypeSlot;
	int m_iGpi_inputSlot;
	int m_iDefault_sdi_sourceSlot;
	int m_iSdi_sourceSlot;
	int m_iLatching_inputSlot;


	
	
};


#endif // touchdown_INCLUDED