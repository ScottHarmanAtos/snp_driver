#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "touchdown.h"

#define PANEL_MAIN					1

#define TIMER_SETUP					1

#define CONTROL_TOUCHDOWN			"touchdown"
#define CONTROL_TOUCHDOWN_FLYBACK	"touchdownflyback"
#define CONTROL_ROBOTIC				"robotic"
#define CONTROL_GPI_TEST			"gpitest"
#define GPI_TOUCHDOWN_CONFIG_FILE	"gpi_touchdown_config.%1"

#define BTN_DEFAULT					"btn_Default"
#define BTN_DEST					"btn_Dest"
#define BTN_PULSE					"btn_pulse"
#define BTN_OFF						"btn_off"
#define BTN_ON						"btn_on"

#define LBL_TALLY					"lbl_Tally"

#define DB_SOURCE_NAMES				0

#define STATESHEET_DESTSELECTED		"dest_selected"
#define SSTATESHEET_DESTDESELECTED	"dest_deselected"


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( touchdown )

// constructor - equivalent to ApplCore STARTUP
touchdown::touchdown( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{

	//Initialise Member Vars
	m_bEnabled = true;
	m_bSelected = false;


	m_sTouchdownMarket_GpiInputInstance = "";
	m_sTouchdownMarket_SdiRouterInstance = "";
	m_iInstances_Config_Sdi_router_device = -1;

	// 
	// gpi_touchdown_auto.xml
	m_sTouchdownMarketConfigInstance = "";
	m_iTouchdownMarketConfigDevice = -1;

	m_sTouchdownMarketBaseInstance = "";

	m_sAuto_Status = "";
	m_sAuto_Touchdown_Destination = "";
	m_sAuto_Wfm_Destination = "";
	m_sAuto_Robotic_Destination = "";
	m_sAuto_Default_Rcp_Flyback = "";
	m_sAuto_Rcp_Flyback = "";
	m_sAuto_Wfm_Monitor = "";
	m_sAuto_TouchDown_Monitor = "";

	m_iAuto_Status_Slot = -1;
	m_iAuto_Touchdown_Destination_Slot = -1;
	m_iAuto_Wfm_Destination_Slot = -1;
	m_iAuto_Robotic_Destination_Slot = -1;
	m_iAuto_Default_Rcp_Flyback_Slot = -1;
	m_iAuto_Rcp_Flyback_Slot = -1;
	m_iAuto_Wfm_Monitor_Slot = -1;
	m_iAuto_TouchDown_Monitor_Slot = -1;

	
	// gpi_touchdown.xml specific to each control
	m_sTouchdownInstance = "";
	m_iTouchdownDevice = -1;


	// Touchdown use
	m_sTouchdownType = "1";
	m_iGpi_input = -1;
	m_sDefault_sdi_source = "";
	m_sSdi_source = "";
	m_sLatching_input = "";

	m_iTouchdownTypeSlot = 0;
	m_iGpi_inputSlot = 0;
	m_iDefault_sdi_sourceSlot = 0;
	m_iSdi_sourceSlot = 0;
	m_iLatching_inputSlot = 0;
	
	m_sControlType = CONTROL_TOUCHDOWN;

	m_sControlId = this->id();

	// show the default panel layout on start
	panelShow( PANEL_MAIN, bncs_string("touchdown.bncs_ui"));

}

// destructor - equivalent to ApplCore CLOSEDOWN
touchdown::~touchdown()
{
}

// all button pushes and notifications come here
void touchdown::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		if (b->id() == BTN_DEST)
		{
			debug("touchdown::buttonCallback btnDest Released");
			hostNotify(bncs_string("selected.%1").arg(m_sTouchdownMarket_SdiRouterInstance));
		}
		else if (b->id() == BTN_DEFAULT)
		{

			if ((m_sControlType == CONTROL_TOUCHDOWN) || (m_sControlType == CONTROL_ROBOTIC))
			{
				//			debug("touchdown::buttonCallback button route_default rcp sdi index='%1'", m_iDefault_rcp_sdi_source);
				infoWrite(m_iTouchdownDevice, m_sDefault_sdi_source, m_iSdi_sourceSlot, true);
			}
			else if (m_sControlType == CONTROL_TOUCHDOWN_FLYBACK)
			{
				//			debug("touchdown::buttonCallback button route_default rcp flyback index='%1'", m_sDefault_Rcp_flyback);
				infoWrite(m_iTouchdownMarketConfigDevice, m_sAuto_Default_Rcp_Flyback, m_iAuto_Rcp_Flyback_Slot, true);
			}
		}

		else if ((m_sControlType == CONTROL_GPI_TEST) && (m_bTouchDownMarket_TestMode))
		{
			if (b->id() == BTN_PULSE)
			{

				if (b->value() == "pressed")
				{
					if (m_sGpi_Status == "0")
					{
						infoWrite(m_iInstances_Config_Gpi_input_device, "1", m_iGpi_input_With_Offset);
					}
					else
					{
						infoWrite(m_iInstances_Config_Gpi_input_device, "0", m_iGpi_input_With_Offset);
					}
				}
				else if (b->value() == "released")
				{
					if (m_sGpi_Status == "0")
					{
						infoWrite(m_iInstances_Config_Gpi_input_device, "1", m_iGpi_input_With_Offset);
					}
					else
					{
						infoWrite(m_iInstances_Config_Gpi_input_device, "0", m_iGpi_input_With_Offset);
					}
				}
			}

			else if (b->id() == BTN_OFF)
			{
				infoWrite(m_iInstances_Config_Gpi_input_device, "0", m_iGpi_input_With_Offset);
			}
			else if (b->id() == BTN_ON)
			{
				infoWrite(m_iInstances_Config_Gpi_input_device, "1", m_iGpi_input_With_Offset);
			}

		}
	}
}

// all revertives come here
int touchdown::revertiveCallback(revertiveNotify * r)
{
	debug("touchdown::Begin revertiveCallback this=%1, dev='%2' index='%3' sInfo='%4' info=%5 type=%6",
		this->id(), r->device(), r->index(), r->sInfo(), r->info(), r->type());


	// Handler for the touchdown auto touchdowns
	if ((r->device() == m_iTouchdownDevice) || (r->device() == m_iTouchdownMarketConfigDevice))
	{
		
		if (r->index() == m_iAuto_Status_Slot) { m_sAuto_Status = r->sInfo(); }
		else if (r->index() == m_iAuto_Touchdown_Destination_Slot) { m_sAuto_Touchdown_Destination = r->sInfo(); }
		else if (r->index() == m_iAuto_Wfm_Destination_Slot) { m_sAuto_Wfm_Destination = r->sInfo(); }
		else if (r->index() == m_iAuto_Robotic_Destination_Slot) { m_sAuto_Robotic_Destination = r->sInfo(); }
		else if (r->index() == m_iAuto_Default_Rcp_Flyback_Slot)
		{
			m_sAuto_Default_Rcp_Flyback = r->sInfo();

			if (m_sControlType == CONTROL_TOUCHDOWN_FLYBACK)
			{
				bncs_string sSourceName = "";
				routerName(m_iInstances_Config_Sdi_router_device, DB_SOURCE_NAMES, m_sAuto_Default_Rcp_Flyback.toInt(), sSourceName);

				// Are in test mode ?
				if (m_bTouchDownMarket_TestMode)
				{
					textPut("text", bncs_string("(%1)|%2").arg(m_sAuto_Default_Rcp_Flyback).arg(sSourceName), PANEL_MAIN, BTN_DEFAULT);
				}
				else
				{
					textPut("text", bncs_string("%1").arg(sSourceName), PANEL_MAIN, BTN_DEFAULT);
				}
			}

		}
		else if (r->index() == m_iAuto_Rcp_Flyback_Slot)
		{
			if (m_sControlType == CONTROL_TOUCHDOWN_FLYBACK)
			{
				m_sAuto_Rcp_Flyback = r->sInfo();
				// At moment gets names from sdi router
				//
				bncs_string sSourceName = "";
				routerName(m_iInstances_Config_Sdi_router_device, DB_SOURCE_NAMES, m_sAuto_Rcp_Flyback.toInt(), sSourceName);

				// Are we in test mode ?
				if (m_bTouchDownMarket_TestMode)
				{
					textPut("text", bncs_string("(%1)|%2").arg(m_sAuto_Rcp_Flyback).arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
				else
				{
					textPut("text", bncs_string("%1").arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
			}
		}
		else if (r->index() == m_iAuto_Rcp_Flyback_Slot) { m_sAuto_Rcp_Flyback = r->sInfo(); }
		else if (r->index() == m_iAuto_Wfm_Monitor_Slot) { m_sAuto_Wfm_Monitor = r->sInfo(); }
		else if (r->index() == m_iAuto_TouchDown_Monitor_Slot) { m_sAuto_TouchDown_Monitor = r->sInfo(); }

		if ((m_sControlType == CONTROL_TOUCHDOWN) || (m_sControlType == CONTROL_ROBOTIC))
		{
			if (r->index() == m_iGpi_inputSlot)	{}
			else if (r->index() == m_iTouchdownTypeSlot) {	m_sTouchdownType = r->sInfo();}
			else if (r->index() == m_iDefault_sdi_sourceSlot)
			{
				m_sDefault_sdi_source = r->sInfo();

				// At moment gets names from sdi router
				//
				bncs_string sDefaultName = "";
				routerName(m_iInstances_Config_Sdi_router_device, DB_SOURCE_NAMES, m_sDefault_sdi_source.toInt(), sDefaultName);

				// Are we in test mode ??
				if (m_bTouchDownMarket_TestMode)
				{
					textPut("text", bncs_string("(%1)Default|%2").arg(m_sDefault_sdi_source).arg(sDefaultName), PANEL_MAIN, BTN_DEFAULT);

				}
				else
				{
					textPut("text", bncs_string("Default|%1").arg(sDefaultName), PANEL_MAIN, BTN_DEFAULT);
				}
			}
			else if (r->index() == m_iSdi_sourceSlot)
			{
				m_sSdi_source = r->sInfo();

				// At moment gets names from sdi router
				//
				bncs_string sSourceName = "";
				routerName(m_iInstances_Config_Sdi_router_device, DB_SOURCE_NAMES, m_sSdi_source.toInt(), sSourceName);

				// Are we in test mode ??
				if (m_bTouchDownMarket_TestMode)
				{
					textPut("text", bncs_string("(%1)|%2").arg(m_sSdi_source).arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
				else
				{
					textPut("text", bncs_string("%1").arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
			}
			else if (r->index() == m_iLatching_inputSlot) {	m_sLatching_input = r->sInfo();	}




			
		}
		else if ((m_sControlType == CONTROL_GPI_TEST) && (m_bTouchDownMarket_TestMode))
		{
			if (r->index() == m_iGpi_inputSlot)
			{
				// If this has changed re-poll it
				if (m_iGpi_input != r->sInfo().toInt())
				{
					m_iGpi_input = r->sInfo().toInt();

					int iOffset = 0;
					getDev(m_sTouchdownMarket_GpiInputInstance, &m_iInstances_Config_Gpi_input_device, &iOffset);

					m_iGpi_input_With_Offset = m_iGpi_input + iOffset;
					infoRegister(m_iInstances_Config_Gpi_input_device, m_iGpi_input_With_Offset, m_iGpi_input_With_Offset, true);
					infoPoll(m_iInstances_Config_Gpi_input_device, m_iGpi_input_With_Offset, m_iGpi_input_With_Offset);
				}
			}
		}

		updateButtonState();

	}
	else if (r->device() == m_iInstances_Config_Gpi_input_device)
	{
		if (m_bTouchDownMarket_TestMode)
		{
			if (r->index() == m_iGpi_input_With_Offset)
			{
				m_sGpi_Status = r->sInfo();

				if (m_sGpi_Status == "0")
				{
					textPut("statesheet", STATESHEET_DESTSELECTED, PANEL_MAIN, BTN_OFF);
					textPut("statesheet", "enum_notimportant", PANEL_MAIN, BTN_ON);
				}
				else if (m_sGpi_Status == "1")
				{
					textPut("statesheet", STATESHEET_DESTSELECTED, PANEL_MAIN, BTN_ON);
					textPut("statesheet", "enum_notimportant", PANEL_MAIN, BTN_OFF);
				}

			}
		}


		updateButtonState();
	}


	return 0;

}
void touchdown::updateButtonState()
{
	if (m_sControlType != CONTROL_GPI_TEST)
	{

		bncs_string sState = "enum_notnormal";
		if ((m_sControlType == CONTROL_TOUCHDOWN) && (m_sDefault_sdi_source == m_sSdi_source) ||
			((m_sControlType == CONTROL_TOUCHDOWN_FLYBACK) && (m_sAuto_Default_Rcp_Flyback == m_sAuto_Rcp_Flyback)) ||
			((m_sControlType == CONTROL_ROBOTIC) && (m_sDefault_sdi_source == m_sSdi_source)))
		{
			sState = "enum_normal";
		}

		textPut("statesheet", sState, PANEL_MAIN, BTN_DEFAULT);
	}
	
}

// all database name changes come back here
void touchdown::databaseCallback( revertiveNotify * r )
{
	//debug("touchdown::databaseCallback() %1 %2 %3", r->device(), r->database(), r->index());
	if( r->device() == m_sTouchdownInstance )
	{
		// source name 0 relevant
		if( r->database() == 0)
		{
			//debug("database router=%1 index=%2 info=%3 sInfo=%4", r->device(), r->index(), r->info(), r->sInfo());
			if (r->index() == m_iSdi_sourceSlot)
			{
				// At moment gets names from sdi router
				//
				bncs_string sSourceName = "";
				routerName(m_iInstances_Config_Sdi_router_device, DB_SOURCE_NAMES, m_iSdi_sourceSlot, sSourceName);

				// Are in test mode ?
				if (m_bTouchDownMarket_TestMode)
				{
					textPut("text", bncs_string("(%1)|%2").arg(m_iSdi_sourceSlot).arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
				else
				{
					textPut("text", bncs_string("%1").arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
			}
			else if (r->index() == m_iDefault_sdi_sourceSlot)
			{
				// At moment gets names from sdi router
				//
				bncs_string sSourceName = "";
				routerName(m_iInstances_Config_Sdi_router_device, DB_SOURCE_NAMES, m_iDefault_sdi_sourceSlot, sSourceName);

				// Are in test mode ?
				if (m_bTouchDownMarket_TestMode)
				{
					textPut("text", bncs_string("(%1)|%2").arg(m_iDefault_sdi_sourceSlot).arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
				else
				{
					textPut("text", bncs_string("%1").arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
			}
			//debug("database router=%1 index=%2 info=%3 sInfo=%4", r->device(), r->index(), r->info(), r->sInfo());
			else if (r->index() == m_iAuto_Rcp_Flyback_Slot)
			{
				// At moment gets names from sdi router
				//
				bncs_string sSourceName = "";
				routerName(m_iInstances_Config_Sdi_router_device, DB_SOURCE_NAMES, m_iAuto_Rcp_Flyback_Slot, sSourceName);

				// Are in test mode ?
				if (m_bTouchDownMarket_TestMode)
				{
					textPut("text", bncs_string("(%1)|%2").arg(m_iAuto_Rcp_Flyback_Slot).arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
				else
				{
					textPut("text", bncs_string("%1").arg(sSourceName), PANEL_MAIN, LBL_TALLY);
				}
			}
	}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string touchdown::parentCallback( parentNotify *p )
{
//	debug("\ntouchdown::parentCallback command='%1' value='%2'", p->command(), p->value());

	//Property set commands

	if ((p->command() == "instance") && (m_sTouchdownInstance != p->value()))
	{
		m_sTouchdownInstance = p->value();

//		debug("touchdown::parentCallback touchdown instance=%1", m_sTouchdownInstance);
		timerStart(TIMER_SETUP, 50);

	}
	else if (p->command() == "group")
	{
//		debug("touchdown::parentCallback group=%1", p->value());
	}
	else if ((p->command() == "touchdown_market_base_instance") && (m_sTouchdownMarketBaseInstance != p->value()))
	{
		m_sTouchdownMarketBaseInstance = p->value();

//		debug("touchdown::parentCallback touchdown_instance=%1", p->value());

		timerStart(TIMER_SETUP, 50);

	}
	else if (p->command() == "control_type")
	{
		if(p->value().length() > 0)
		{
			if ((p->value().lower() != CONTROL_TOUCHDOWN) &&
				(p->value().lower() != CONTROL_TOUCHDOWN_FLYBACK) &&
				(p->value().lower() != CONTROL_TOUCHDOWN_FLYBACK) && 
				(p->value().lower() != CONTROL_GPI_TEST))
			{
				m_sControlType = CONTROL_TOUCHDOWN;
			}
			else
			{
				m_sControlType = p->value().lower();
			}

			m_sControlType = p->value();
			
			// Special for testing
			if (m_sControlType == CONTROL_GPI_TEST)
			{
				panelDestroy(PANEL_MAIN);
				panelShow(PANEL_MAIN, bncs_string("gpi_test.bncs_ui"));
			}
			
		}
		else
		{
			m_sControlType = CONTROL_TOUCHDOWN;
		}
	}

	//Runtime "methods"
	else if( p->command() == "start" )
	{
		init();
	}
	else if( p->command() == "select" )
	{
		debug("touchdown::parentCallback select");

		// p->sub(0) contains the sdi router instance
		if (p->sub(0).length() && (p->sub(0) != m_sTouchdownMarket_SdiRouterInstance))
		{
			if(m_bSelected)
			{
				textPut("statesheet", SSTATESHEET_DESTDESELECTED, PANEL_MAIN, BTN_DEST);
				m_bSelected = false;
			}
			return 0;
		}

		// p->sub(1) contains the controlId that started this action
		if (p->sub(1) == bncs_string(m_sControlId))
		{

			if ((m_sControlType == CONTROL_TOUCHDOWN) || (m_sControlType == CONTROL_ROBOTIC))
			{
				infoWrite(m_iTouchdownDevice, p->value(), m_iSdi_sourceSlot, true);
			}
			else if (m_sControlType == CONTROL_TOUCHDOWN_FLYBACK)
			{
				infoWrite(m_iTouchdownMarketConfigDevice, p->value(), m_iAuto_Rcp_Flyback_Slot, true);
			}
			
			if (!m_bSelected)
			{
				textPut("statesheet", STATESHEET_DESTSELECTED, PANEL_MAIN, BTN_DEST);
				m_bSelected = true;
			}
		}
		else
		{
			if(m_bSelected)
			{
				textPut("statesheet", SSTATESHEET_DESTDESELECTED, PANEL_MAIN, BTN_DEST);
				m_bSelected = false;
			}
		}
	}
	else if( p->command() == "deselect" )
	{
		debug("touchdown::parentCallback deselect");
		textPut("statesheet", SSTATESHEET_DESTDESELECTED, PANEL_MAIN, BTN_DEST);
	}
	else if( p->command() == "disable" )
	{
		if(m_bEnabled)
		{
			m_bEnabled = false;
			controlDisable(PANEL_MAIN, BTN_DEST);
		}
	}
	else if( p->command() == "enable" )
	{
		if(!m_bEnabled)
		{
			m_bEnabled = true;
			controlEnable(PANEL_MAIN, BTN_DEST);
		}
	}

	//Return Commands
	else if( p->command() == "return" )
	{
		//Called by visual editor to persist settings in bncs_ui
		if(p->value() == "all")
		{
			return
				bncs_string("control_type=") + m_sControlType + "\n";
		
		}
		//Called by parent to learn the components settings
		else if (p->value() == "getSettings")
		{
			return bncs_string("getSettings=")
				+ bncs_string("instance=") + m_sTouchdownMarket_SdiRouterInstance + ","
				+ bncs_string("control_type=") + m_sControlType;
		}
		else if (p->value() == "control_type")
		{
			return  bncs_string("control_type=") + m_sControlType;
		}
	}

	//Connections information for BNCS Vis Ed
	else if(p->command() == "_events")
	{
		bncs_stringlist sltEvents;
		sltEvents << "selected.<instance>=<dest_index>";
		sltEvents << "instance=<instance>";
		return sltEvents.toString('\n');
	}
	else if(p->command() == "_commands")
	{
		bncs_stringlist sltCommands;
		sltCommands << "disable";
		sltCommands << "enable";
		sltCommands << "take=<source_index>";
		sltCommands << "select.<instance>=<dest_index>";
		sltCommands << "undo";
		return sltCommands.toString( '\n' );
	}

	return "";
}

// timer events come here
void touchdown::timerCallback( int id )
{
	if (id == TIMER_SETUP)
	{
		timerStop(TIMER_SETUP);
		init();
	}
}

void touchdown::init()
{
	debug("\ntouchdown::init Begin CONTROL %1 %2", m_sControlType, this->id());
	
	// Extract the Config instance
	m_sTouchdownMarketConfigInstance = m_sTouchdownMarketBaseInstance.append("/config");

	// Required for all controls
	loadInstancesConfig();

	// Required for all controls
	loadAndRegisterTouchdownAutoConfig();

	if (m_sControlType != CONTROL_GPI_TEST)
	{
		// display this here for when in vis dev development mode
		textPut("text", bncs_string("Default|---"), PANEL_MAIN, BTN_DEFAULT);
	}

	// Extract the input button caption index
	bncs_stringlist slstTemp = bncs_stringlist(m_sTouchdownInstance, '/');
	bncs_stringlist sButtonCaptionIndex = slstTemp[slstTemp.count() - 1];


	// What type of control are we running as ????

	if (m_sControlType == CONTROL_TOUCHDOWN_FLYBACK)
	{
		// Display the button name
		textPut("text", "Flyback|Source", PANEL_MAIN, BTN_DEST);

	}
	else if ((m_sControlType == CONTROL_TOUCHDOWN) | (m_sControlType == CONTROL_ROBOTIC))
	{
		loadAndRegisterTouchdownSpecificConfig();

		// Display the button name
		bncs_string strDestName = bncs_string("TouchDown| %1").arg(sButtonCaptionIndex);

		textPut("text", strDestName, PANEL_MAIN, BTN_DEST);

	}
	else if ((m_sControlType == CONTROL_GPI_TEST) && (m_bTouchDownMarket_TestMode))
	{

		loadAndRegisterTouchdownSpecificConfig();

		textPut("text", bncs_string("Gpi %1 Latching").arg(sButtonCaptionIndex), PANEL_MAIN, "lbl");

		
		// Pass the specific instance to the Latching button on the control
		textPut("instance", m_sTouchdownInstance, PANEL_MAIN, "btn_latching");
		
	}
	else if ((m_sControlType == CONTROL_GPI_TEST) && (! m_bTouchDownMarket_TestMode))
	{
	}
	else
	{
		debug("ERROR touchdown::init CONTROL type = %1 not found !- check design time params", m_sControlType);
	}


}




void touchdown::loadInstancesConfig()
{
	// Get required config from gpi_touchdown_config.xml (/touchdown/config)
	bncs_config c(bncs_string(GPI_TOUCHDOWN_CONFIG_FILE).arg(m_sTouchdownMarketConfigInstance));
	if (c.isValid())
	{
		while (c.isChildValid())
		{
			bncs_string id = c.childAttr("id");
			bncs_string value = c.childAttr("value");

			if (id == "gpi_input_instance")
			{
				m_sTouchdownMarket_GpiInputInstance = value;
			}
			else if (id == "sdi_router_instance")
			{
				m_sTouchdownMarket_SdiRouterInstance = value;
			}
			else if (id == "test_mode")
			{
				m_bTouchDownMarket_TestMode = value == "1" ? true : false;
			}

			c.nextChild();
		}
	}
}

void touchdown::loadAndRegisterTouchdownAutoConfig()
{
	debug("\ntouchdown::loadAndRegisterTouchdownAutoConfig::Instance=%1", m_sTouchdownMarketConfigInstance);

	// Register and poll the touchdown auto configuration slots in the touchdown auto
	int iOffset = 0;
	char cType;
	getDev(m_sTouchdownMarketConfigInstance, &m_iTouchdownMarketConfigDevice, &iOffset);

		
	getDevSlot(m_sTouchdownMarketConfigInstance, "status", &m_iTouchdownMarketConfigDevice, &m_iAuto_Status_Slot, &cType);
	getDevSlot(m_sTouchdownMarketConfigInstance, "touchdown_destination", &m_iTouchdownMarketConfigDevice, &m_iAuto_Touchdown_Destination_Slot, &cType);
	getDevSlot(m_sTouchdownMarketConfigInstance, "wfm_destination", &m_iTouchdownMarketConfigDevice, &m_iAuto_Wfm_Destination_Slot, &cType);
	getDevSlot(m_sTouchdownMarketConfigInstance, "robotic_destination", &m_iTouchdownMarketConfigDevice, &m_iAuto_Robotic_Destination_Slot, &cType);
	getDevSlot(m_sTouchdownMarketConfigInstance, "default_rcp_flyback", &m_iTouchdownMarketConfigDevice, &m_iAuto_Default_Rcp_Flyback_Slot, &cType);
	getDevSlot(m_sTouchdownMarketConfigInstance, "rcp_flyback", &m_iTouchdownMarketConfigDevice, &m_iAuto_Rcp_Flyback_Slot, &cType);
	getDevSlot(m_sTouchdownMarketConfigInstance, "wfm_monitor", &m_iTouchdownMarketConfigDevice, &m_iAuto_Wfm_Monitor_Slot, &cType);
	getDevSlot(m_sTouchdownMarketConfigInstance, "touchdown_monitor", &m_iTouchdownMarketConfigDevice, &m_iAuto_TouchDown_Monitor_Slot, &cType);

	// Just in case Scott has changed one of these, do this again
	// If BNCS cannot find the entry - it sets the device number to 0

	if (m_iTouchdownMarketConfigDevice == 0)
	{
		debug("ERROR touchdown::loadAndRegisterTouchdownAutoConfig One of the entries in '%1'was not found !", m_sTouchdownMarketConfigInstance);

		getDev(m_sTouchdownMarketConfigInstance, &m_iTouchdownMarketConfigDevice, &iOffset);
	}


	getDev(m_sTouchdownMarket_SdiRouterInstance, &m_iInstances_Config_Sdi_router_device);

	infoRegister(m_iTouchdownMarketConfigDevice, 1 + iOffset, 10 + iOffset, true);
	infoPoll(m_iTouchdownMarketConfigDevice, 1 + iOffset, 10 + iOffset);

}

void touchdown::loadAndRegisterTouchdownSpecificConfig()
{
	debug("\ntouchdown::loadAndRegisterTouchdownSpecificConfig::Instance=%1", m_sTouchdownInstance);

	// Register and poll the touchdown specific configuration slots in the touchdown auto
	int iOffset = 0;
	char cType;
	getDev(m_sTouchdownInstance, &m_iTouchdownDevice, &iOffset);


	// Read in values for slots
	getDevSlot(m_sTouchdownInstance, "touchdown_type", &m_iTouchdownDevice, &m_iTouchdownTypeSlot, &cType);
	getDevSlot(m_sTouchdownInstance, "gpi_input", &m_iTouchdownDevice, &m_iGpi_inputSlot, &cType);
	getDevSlot(m_sTouchdownInstance, "default_sdi_source", &m_iTouchdownDevice, &m_iDefault_sdi_sourceSlot, &cType);
	getDevSlot(m_sTouchdownInstance, "sdi_source", &m_iTouchdownDevice, &m_iSdi_sourceSlot, &cType);

	getDevSlot(m_sTouchdownInstance, "latching_input", &m_iTouchdownDevice, &m_iLatching_inputSlot, &cType);
	
	if (m_iTouchdownDevice == 0)
	{
		debug("ERROR touchdown::loadAndRegisterTouchdownSpecificConfig One of the entries in '%1'was not found !", m_sTouchdownInstance);
		getDev(m_sTouchdownInstance, &m_iTouchdownDevice, &iOffset);
	}

	
	infoRegister(m_iTouchdownDevice, 1 + iOffset, 10 + iOffset, true);
	infoPoll(m_iTouchdownDevice, 1 + iOffset, 10 + iOffset);


}