#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include "vob_assignment.h"
#include <packager_common.h>

#define PANEL_MAIN							1
#define POPUP_LANGUAGE_TAG					2

#define TIMER_SETUP							1
#define TIMER_SETUP_DURATION				100

#define POPUP_LANGUAGE						"popup_language_tag.bncs_ui"
#define POPUP_PRESELECT_SOURCE_GRID			"popup_preselect_source_grid"
#define POPUP_MON_2_SOURCE_GRID				"popup_mon_2_source_grid"
#define POPUP_MON_3_SOURCE_GRID				"popup_mon_3_source_grid"

#define BTN_PRESELECT_SOURCE				"btn_preselect_src"
#define BTN_TAKE							"btn_take"
#define BTN_MON_1							"btn_mon_1"
#define BTN_MON_2							"btn_mon_2"
#define BTN_MON_3							"btn_mon_3"
#define BTN_AP_EDIT_LANGUAGE				"btn_ap_edit_language"
#define BTN_AP_EDIT							"btn_ap_edit"
#define BTN_LIVE							"btn_live"
#define BTN_SCOOP_VOB_AUDIO_PACKAGE_NAME	"btn_scoop_vob_audio_package_name"
#define BTN_MIXER_ASSIGN					"btn_mixer_assign"
#define BTN_CONFIG_MIX						"btn_config_mix"


#define LBL_SCOOP_VOB_AUDIO_PACKAGE_TALLY	"lbl_scoop_vob_audio_package_tally"
#define LBL_TITLE							"title"
#define LBL_MON_2							"lbl_mon_2" 
#define LBL_MON_3							"lbl_mon_3" 
#define LBL_PRESET_WARN						"lbl_warn" 
#define LBL_LANG_WARN						"lbl_lang_warn"
#define LANGUAGE_GRID						"language_grid"

#define BTN_CANCEL							"cancel"
#define BTN_NONE							"none"
#define BTN_SET								"set"

#define VOB_CLEAR							false
#define VOB_SET								true

#define DEBUG_LISTBOX						"debug_Listbox"
/*
#define DATABASE_SOURCE_NAME	0
#define DATABASE_DEST_NAME		1
#define DATABASE_SOURCE_TITLE	2
#define DATABASE_SOURCE_AP		6
#define DATABASE_MCR_UMD		9
#define DATABASE_PROD_UMD		10
#define DATABASE_SOURCE_VIDEO	11
#define DATABASE_SPEV_ID		31
#define DATABASE_RECORD_ID		32
#define DATABASE_MCR_NOTES		37
#define DATABASE_VIDEO_HUB_TAG	41
#define DATABASE_LANG_TAG		43
#define DATABASE_AP_NAME		60
#define DATABASE_AP_TAGS		63
#define DATABASE_AP_VIDEO_LINK	66


#define DATABASE_SOURCE_AP		6
#define DATABASE_LANG_TAG		43
#define DATABASE_AP_NAME		60
#define DATABASE_AP_LONG_NAME	61
#define DATABASE_AP_ID_OWNER	62
#define DATABASE_AP_TAGS		63

These are now all defined in packager_common.h
*/

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( vob_assignment )

// constructor - equivalent to ApplCore STARTUP
vob_assignment::vob_assignment( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{

	m_sInstance = "";
	m_sWorkspace = "";

	m_bInitialising = true;
	m_bLiveSet = false;

	m_bScoopVOBOutputPackageEditingEnabled = false;

	m_sName = "";
	m_sDest_pages_template = "";
	m_sSource_pages_template = "";
	m_sGroup = "";

	m_iPreselectedSourcePackage = -1;
	m_bLiveToggleEnabled = false;

	// show a panel from file vob_assignment.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PANEL_MAIN, "vob_assignment.bncs_ui" );

	vobReset();

	m_sCompositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_sCompositePackager);

	//Get main router device
	m_iPackagerRouterMainDevice = m_packagerRouter.destDev(1);
	m_iFirstVirtualPackage = m_packagerRouter.firstVirtual();

	// Pass to associated popup_sourceGrid etc
	textPut("instance", m_packagerRouter.instance(1), PANEL_MAIN, POPUP_PRESELECT_SOURCE_GRID);

	debug("instance=%1", m_packagerRouter.instance(1));

	

}

vob_assignment::~vob_assignment()
{
}

// all button pushes and notifications come here
void vob_assignment::buttonCallback( buttonNotify *b )
{

	bncs_string sText = "";
	
	if( b->panel() == PANEL_MAIN )
	{

		if (b->id() == "grp_vob")
		{
			hostNotify("reset=preselect");
		}
		else if (b->id() == BTN_LIVE)
		{
			if (m_iScoopVobAPTagSelectedIndex > 0)
			{

				if (m_bLiveToggleEnabled)
				{
					m_bLiveSet = !m_bLiveSet;

					// This requires storing in the_base component, in case we do a
					// market tag/vob change

					setLiveButtonState(m_bLiveSet);

					// Add any live Scoop AP insertion routing if present
					// But only if 'Live set' is enabled
					if (m_bLiveSet)
					{

						// Send notification to refresh all the Vobs to pick up the "LIVE" change, turn off other Vobs that are routed to this Source
						hostNotify(bncs_string("setLive=%1.%2.%3.%4").arg(m_sGroupname).arg(m_sWorkspace).arg(m_iVob_in_01_routed_source).arg(m_iScoopVobAPOutputLanguageTag));

						// Add Scoop vob output into current routed source
						routeScoopVobOutputLive(VOB_CLEAR, m_iVob_in_01_routed_source);

					}
					else
					{

						// button set off so remove from current routed source
						routeScoopVobOutputLive(VOB_SET, m_iVob_in_01_routed_source);

					}

				}
			}
			else
			{
				// Handles error in 1001.db6 - no VOB out package set in slot [0]
				m_bLiveSet = false;

				// This requires storing in the_base component, in case we do a
				// market tag/vob change

				setLiveButtonState(m_bLiveSet);
			}
		}
		else if (b->id() == BTN_PRESELECT_SOURCE)
		{
			textPut("statesheet", "mon_notfollowing", PANEL_MAIN, BTN_TAKE);
			showVirtual(0, BTN_TAKE);
			controlDisable(PANEL_MAIN, BTN_TAKE);

			if (b->command() == "index")
			{
				textPut("show_popup", m_iVob_in_01_dest, PANEL_MAIN, POPUP_PRESELECT_SOURCE_GRID);
			}
		}
		else if (b->id() == BTN_MON_2)
		{
			if (b->command() == "dest")
			{
				textPut("show_popup", m_iVob_in_02_dest, PANEL_MAIN, POPUP_MON_2_SOURCE_GRID);
			}
		}
		else if (b->id() == BTN_MON_3)
		{
			if (b->command() == "dest")
			{
				textPut("show_popup", m_iVob_in_03_dest, PANEL_MAIN, POPUP_MON_3_SOURCE_GRID);
			}
		}
		else if (b->id() == POPUP_PRESELECT_SOURCE_GRID)
		{
			debug("Command=%1", b->command());

			if (b->command() == "source_index")
			{
				m_bInitialising = false;
				m_sVob_in_01_selected_source = b->value();
				m_iPreselectedSourcePackage = b->value().toInt();
				textPut("index", m_iPreselectedSourcePackage, PANEL_MAIN, BTN_PRESELECT_SOURCE);

				// A Preselect Next Source has been routed to the Take button - compare
				// With what is currently routed on the input side

				showPresetWarningLabel(validateSourcePackageScoopOutputPackageLanguageMatch(m_iPreselectedSourcePackage));
				updateTakeButton();
			}
		}
		else if (b->id() == POPUP_MON_2_SOURCE_GRID)
		{
			debug("Command=%1", b->command());

			if (b->command() == "source_index")
			{
				m_sVob_in_02_selected_source = b->value();
				textPut("take", m_sVob_in_02_selected_source, PANEL_MAIN, BTN_MON_2);
			}
		}
		else if (b->id() == POPUP_MON_3_SOURCE_GRID)
		{
			debug("Command=%1", b->command());

			if (b->command() == "source_index")
			{
				m_sVob_in_03_selected_source = b->value();
				textPut("take", m_sVob_in_03_selected_source, PANEL_MAIN, BTN_MON_3);
			}
		}
		else if (b->id() == BTN_TAKE)
		{
			if (m_iVob_in_01_routed_source != m_iPreselectedSourcePackage)
			{
				if (b->value() == "released")
				{
					// Remove any live Scoop AP insertion routing if present from currently routed source
					// doesnt matter if 'on live' set or 'no link' - should not be
					// the vob output package in the routes source package when we release it 
					routeScoopVobOutputLive(VOB_SET, m_iVob_in_01_routed_source);

					// Add any live Scoop AP insertion routing if present
					// But only if 'Live set' is enabled
					if (m_bLiveSet)
					{
						routeScoopVobOutputLive(VOB_CLEAR, m_iPreselectedSourcePackage);
					}
					
					textPut("take", m_iPreselectedSourcePackage, PANEL_MAIN, BTN_MON_1);
					showPresetWarningLabel(false);
				}

			}
		}
		else if (b->id() == BTN_AP_EDIT_LANGUAGE)
		{
			panelPopup(POPUP_LANGUAGE_TAG, POPUP_LANGUAGE);
			textPut("text", " Set Audio Package Language Tag", POPUP_LANGUAGE_TAG, LBL_TITLE);
			textPut("instance", m_packagerRouter.instance(1), POPUP_LANGUAGE_TAG, LANGUAGE_GRID);

			textPut("selected", m_iScoopVobAPOutputLanguageTag, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);

		}
		else if (b->id() == BTN_AP_EDIT)
		{
			hostNotify(bncs_string("editingAP=%1,%2").arg(m_iScoopVobAPTagSelectedIndex).arg(m_iVob_out_01_dest));
		}
	}
	else if (b->panel() == POPUP_LANGUAGE_TAG)
	{
		if (b->id() == BTN_SET)
		{
			showLanguageChangeWarningLabel(false);
			panelDestroy(POPUP_LANGUAGE_TAG);
			setEditAudioLanguageTag(m_iPopUpLanguageSelected);

		}
		else if (b->id() == LANGUAGE_GRID)
		{
			if (b->command() == "select")
			{
				m_iPopUpLanguageSelected = b->value().toInt();

				if (m_iPopUpLanguageSelected > 0)
				{
					// Check for duplication here !
					if (validateSourcePackageContainsSelectedLanguage(m_iVob_in_01_routed_source, m_iPopUpLanguageSelected))
					{
						showLanguageChangeWarningLabel(true);
						controlDisable(POPUP_LANGUAGE_TAG,BTN_SET);
					}
					else
					{
						showLanguageChangeWarningLabel(false);
						controlEnable(POPUP_LANGUAGE_TAG,BTN_SET);
					}
				}
				
				textPut("selected", m_iPopUpLanguageSelected, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
			}
		}
		else if (b->id() == BTN_NONE)
		{
			showLanguageChangeWarningLabel(false);
			controlEnable(POPUP_LANGUAGE_TAG, BTN_SET);

			m_iPopUpLanguageSelected = 0;
			textPut("selected", m_iPopUpLanguageSelected, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_LANGUAGE_TAG);
		}
	}
}

// all revertives come here
int vob_assignment::revertiveCallback( revertiveNotify * r )
{
	r->dump(bncs_string("\nvob_assignment::revertiveCallback == %1").arg(m_sName));

//	debug("\nvob_assignment::revertiveCallback:device=%1",r->device());
//	debug("\vob_assignment::revertiveCallback:slot   =%1", r->index());
//	debug("\vob_assignment::revertiveCallback:sInfo  =%1", r->sInfo());

	if ((r->device() == m_iVob_in_01_device) && (r->index() == m_iVob_in_01_slot))
	{
		bncs_stringlist sltTally(r->sInfo());
		m_iVob_in_01_routed_source = sltTally.getNamedParam("index").toInt();
		debug("\vob_assignment::revertiveCallback:sInfo::m_sVob_in_01_routed_source  =%1", m_iVob_in_01_routed_source);

		m_iPreselectedSourcePackage = m_iVob_in_01_routed_source;
			
		// First time displayed so show what source is already routed on the preset button
		textPut("index", m_iVob_in_01_routed_source, PANEL_MAIN, BTN_PRESELECT_SOURCE);

		updateTakeButton();

		// check for live !
		m_bLiveSet = validateCurrentlyLive();

	}
	else if ((r->device() == m_iVob_in_02_device) && (r->index() == m_iVob_in_02_slot))
	{
		bncs_stringlist sltTally(r->sInfo());
		m_iVob_in_02_routed_source = sltTally.getNamedParam("index").toInt();
		debug("\vob_assignment::revertiveCallback:sInfo::m_sVob_in_02_routed_source  =%1", m_iVob_in_02_routed_source);

	}
	else if ((r->device() == m_iVob_out_01_device) && (r->index() == m_iVob_out_01_slot))
	{
		bncs_stringlist sltTally(r->sInfo());
		m_iVob_out_01_routed_source = sltTally.getNamedParam("index").toInt();
		debug("\vob_assignment::revertiveCallback:sInfo::m_sVob_out_01_routed_source  =%1", m_iVob_out_01_routed_source);

		updateScoopVobAPAdjustment(m_iVob_out_01_dest);


		// Does the routed Vob 1 input match the Scoop vob output ?
		
		showPresetWarningLabel(validateSourcePackageScoopOutputPackageLanguageMatch(m_iVob_in_01_routed_source));

		// check for live !
		m_bLiveSet = validateCurrentlyLive();
	}

	updateDebugListBox();
	
	return 0;
}

// all database name changes come back here
void vob_assignment::databaseCallback( revertiveNotify * r )
{
	if (r->device() == m_iPackagerRouterMainDevice)
	{
		if (r->database() == DATABASE_AP_TAGS)
		{
			if (r->index() == m_iScoopVobAPTagSelectedIndex)
			{

				bncs_stringlist slstParms = r->sInfo();
				m_iScoopVobAPOutputLanguageTag = slstParms.getNamedParam("language_tag").toInt();

				// this should be by revertive a well !
				bncs_string sNewLanguageName;
				routerName(m_iPackagerRouterMainDevice, DATABASE_LANG_TAG, m_iScoopVobAPOutputLanguageTag, sNewLanguageName);


				bncs_stringlist sltNameStatus(sNewLanguageName, '|');
				m_sScoopVobAPlanguageName = sltNameStatus[0];
				textPut("text", m_sScoopVobAPlanguageName, PANEL_MAIN, BTN_AP_EDIT_LANGUAGE);

			}

			// Something has changed externally on the Scoop Vob Output side - compare
			// With what is currently routed to the input
			showPresetWarningLabel(validateSourcePackageScoopOutputPackageLanguageMatch(m_iVob_in_01_routed_source));

		}
		else if (r->database() == DATABASE_SOURCE_AP)
		{
			
			if (r->index() == m_iVob_in_01_routed_source)
			{
				updateScoopVobAPAdjustment(m_iVob_out_01_dest);

				// check for live ! - base panel may have changed it !
				m_bLiveSet = validateCurrentlyLive();
				
			}
			
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string vob_assignment::parentCallback( parentNotify *p )
{
	p->dump("\n vob_assignment())");
	
	if (p->command() == "instance" && p->value() != m_sInstance)
	{
		m_sInstance = p->value();
		timerStop(TIMER_SETUP);
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}
	else if (p->command() == "live_toggle_enabled")
	{
		m_bLiveToggleEnabled = p->value() == "true" ? true : false;
		timerStop(TIMER_SETUP);
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}
	else if (p->command() == "audio_output_edit_enabled")
	{
		m_bScoopVOBOutputPackageEditingEnabled = p->value() == "true" ? true : false;
		timerStop(TIMER_SETUP);
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}
	
	else if (p->command() == "groupname")
	{
		m_sGroupname = p->value();
		timerStop(TIMER_SETUP);
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}
	else if (p->command() == "workspace" && p->value() != m_sWorkspace)
	{
		m_sWorkspace = p->value();

		// Pass to associated popup_sourceGrid etc
		textPut("workspace", m_sWorkspace, PANEL_MAIN, POPUP_PRESELECT_SOURCE_GRID);

		timerStop(TIMER_SETUP);
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}
	else if (p->command() == "reset")
	{
		if (p->value() == "preselect")
		{
			// Reset colour of preselect button incase popup source select has not been used correctly
			textPut("index", m_iPreselectedSourcePackage, PANEL_MAIN, BTN_PRESELECT_SOURCE);
		}
	}
	
	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void vob_assignment::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		initProperties();
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void vob_assignment::initProperties()
{

	vobReset();

	// register for databases
	routerRegister(m_iPackagerRouterMainDevice, 1, 1, true);

	///////////////////////////////////////////////////////
	// Set the workspaceName to be the "--"

	if (m_sWorkspace.length() > 0)
	{

		//get name of workspace
		bncs_config cWorkspaceProperties(bncs_string("workspace_resources/%1.properties").arg(m_sWorkspace));
		if (cWorkspaceProperties.isValid())
		{
			while (cWorkspaceProperties.isChildValid())
			{
				bncs_string id = cWorkspaceProperties.childAttr("id");
				bncs_string sWorkSpaceValue = cWorkspaceProperties.childAttr("value");

				if (id == "name")
				{
					// Some of these may have a '|' in then . so......
					m_sName = sWorkSpaceValue.replace('|', ' ');
					textPut("text", m_sName + " VOB", PANEL_MAIN, "grp_vob");
				}
				else if (id == "dest_pages_template")
				{
					m_sDest_pages_template = sWorkSpaceValue;
				}
				else if (id == "source_pages_template")
				{
					m_sSource_pages_template = sWorkSpaceValue;
				}
				else if (id == "group")
				{
					m_sGroup = sWorkSpaceValue;
				}

				cWorkspaceProperties.nextChild();
			}


			/////////////////////////
			/// VOB INPUTS
			/////////////////////////
			bncs_config cCfgVob_in(bncs_string("workspace_resources/%1.vob_in").arg(m_sWorkspace));
			if (cCfgVob_in.isValid())
			{
				while (cCfgVob_in.isChildValid())
				{
					bncs_string sId = cCfgVob_in.childAttr("id");
					bncs_string sValue = cCfgVob_in.childAttr("value");

					if (sId == "01")
					{

						m_iVob_in_01_dest = sValue.toInt();

						// Update 'Vob In 1' source button index
						textPut("index", m_iVob_in_01_dest, PANEL_MAIN, BTN_PRESELECT_SOURCE);

						// update take + dest package buttons
						if (m_iVob_in_01_dest > 0)
						{
							controlEnable(PANEL_MAIN, BTN_MON_1);
							controlEnable(PANEL_MAIN, BTN_PRESELECT_SOURCE);
							controlEnable(PANEL_MAIN, BTN_LIVE);

							setLiveButtonType(m_bLiveToggleEnabled);

							textPut("index", m_iVob_in_01_dest, PANEL_MAIN, BTN_MON_1);
							textPut("dest", m_iVob_in_01_dest, PANEL_MAIN, BTN_MON_1);

							// Register for revertives
							// Get the actual info device for this slot
							m_iVob_in_01_device = m_packagerRouter.destDev(m_iVob_in_01_dest);
							m_iVob_in_01_slot = m_packagerRouter.destSlot(m_iVob_in_01_dest);

							infoRegister(m_iVob_in_01_device, m_iVob_in_01_slot, m_iVob_in_01_slot, true);
							infoPoll(m_iVob_in_01_device, m_iVob_in_01_slot, m_iVob_in_01_slot);
						}
						else
						{
							controlDisable(PANEL_MAIN, BTN_MON_1);
						}

					}
					else if (sId == "02")
					{
						m_iVob_in_02_dest = sValue.toInt();

						if (m_iVob_in_02_dest > 0)
						{
							controlShow(PANEL_MAIN, BTN_MON_2);
							controlShow(PANEL_MAIN, LBL_MON_2);

							// Register for revertive
							m_iVob_in_02_device = m_packagerRouter.destDev(m_iVob_in_02_dest);
							m_iVob_in_02_slot = m_packagerRouter.destSlot(m_iVob_in_02_dest);

							infoRegister(m_iVob_in_02_device, m_iVob_in_02_slot, m_iVob_in_02_slot, true);
							infoPoll(m_iVob_in_02_device, m_iVob_in_02_slot, m_iVob_in_02_slot);

							textPut("index", m_iVob_in_02_dest, PANEL_MAIN, BTN_MON_2);
							textPut("dest", m_iVob_in_02_dest, PANEL_MAIN, BTN_MON_2);
						}
						else
						{
							controlHide(PANEL_MAIN, BTN_MON_2);
							controlHide(PANEL_MAIN, LBL_MON_2);
						}
					}

					else if (sId == "03")
					{
						m_iVob_in_03_dest = sValue.toInt();
						if (m_iVob_in_03_dest > 0)
						{
							controlShow(PANEL_MAIN, BTN_MON_3);
							controlShow(PANEL_MAIN, LBL_MON_3);

							// Register for revertive
							m_iVob_in_03_device = m_packagerRouter.destDev(m_iVob_in_03_dest);
							m_iVob_in_03_slot = m_packagerRouter.destSlot(m_iVob_in_03_dest);

							infoRegister(m_iVob_in_03_device, m_iVob_in_03_slot, m_iVob_in_03_slot, true);
							infoPoll(m_iVob_in_03_device, m_iVob_in_03_slot, m_iVob_in_03_slot);

							textPut("index", m_iVob_in_03_dest, PANEL_MAIN, BTN_MON_3);
							textPut("dest", m_iVob_in_03_dest, PANEL_MAIN, BTN_MON_3);
						}
						else
						{
							controlHide(PANEL_MAIN, BTN_MON_3);
							controlHide(PANEL_MAIN, LBL_MON_3);
						}
					}

					cCfgVob_in.nextChild();

				}

			}
			else
			{
				debug("ERROR::Vob_assignment::initWorkspaceGroups()::Cannot find 'workspace_resources/%1.vob_in 01'", m_sWorkspace);
			}

			/////////////////////////
			/// VOB OUTPUTS
			/////////////////////////
			bncs_config cCfgVob_out(bncs_string("workspace_resources/%1.vob_out").arg(m_sWorkspace));
			if (cCfgVob_out.isValid())
			{
				while (cCfgVob_out.isChildValid())
				{
					bncs_string sId = cCfgVob_out.childAttr("id");
					bncs_string sValue = cCfgVob_out.childAttr("value");

					if (sId == "01")
					{
						m_iVob_out_01_dest = sValue.toInt();

						if (m_iVob_in_01_dest > 0)
						{
							// Register for revertives
							m_iVob_out_01_device = m_packagerRouter.destDev(m_iVob_out_01_dest);
							m_iVob_out_01_slot = m_packagerRouter.destSlot(m_iVob_out_01_dest);

							infoRegister(m_iVob_out_01_device, m_iVob_out_01_slot, m_iVob_out_01_slot, true);
							infoPoll(m_iVob_out_01_device, m_iVob_in_01_slot, m_iVob_out_01_slot);

							setLiveButtonType(m_bLiveToggleEnabled);

							controlEnable(PANEL_MAIN, BTN_MIXER_ASSIGN);
							controlEnable(PANEL_MAIN, BTN_CONFIG_MIX);
						}
						else
						{
							setLiveButtonType(false);
							controlDisable(PANEL_MAIN, BTN_MIXER_ASSIGN);
							controlDisable(PANEL_MAIN, BTN_CONFIG_MIX);

						}
					}

					cCfgVob_out.nextChild();
				}
			}
			else
			{
				debug("ERROR::Vob_assignment::initWorkspaceGroups()::Cannot find 'workspace_resources/%1.vob_out 01'", m_sWorkspace);
			}

//			Now updated in revertive call back
//			updateDebugListBox();
		}
		else
		{
			debug("ERROR::Vob_assignment::initWorkspaceGroups()::Cannot find 'workspace_resources/%1.properties'", m_sWorkspace);
		}

	}

}

void vob_assignment::updateDebugListBox()
{
	// display in debug listbox

	textPut("clear", "", PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("Config file"), PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("workspace_resources/%1.xml").arg(m_sWorkspace), PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", "", PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("Name = %1").arg(m_sName), PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("dest_pages_template = %1").arg(m_sDest_pages_template), PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("source_pages_template = %1").arg(m_sSource_pages_template), PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("group = %1").arg(m_sGroup), PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", "", PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("VOB IN 01  = %1 (Src=%2)").arg(m_iVob_in_01_dest).arg(m_iVob_in_01_routed_source), PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("VOB IN 02  = %1 (Src=%2)").arg(m_iVob_in_02_dest).arg(m_iVob_in_02_routed_source), PANEL_MAIN, DEBUG_LISTBOX);

	if (m_iVob_in_03_dest > 0)
	{
		textPut("add", bncs_string("VOB IN 03  = %1 (Src=%2)").arg(m_iVob_in_03_dest).arg(m_iVob_in_03_routed_source), PANEL_MAIN, DEBUG_LISTBOX);
	}
	
	textPut("add", "", PANEL_MAIN, DEBUG_LISTBOX);

	textPut("add", bncs_string("VOB OUT 01 = %1 (Src=%2)").arg(m_iVob_out_01_dest).arg(m_iVob_out_01_routed_source), PANEL_MAIN, DEBUG_LISTBOX);

}

void vob_assignment::updateTakeButton()
{
	debug("\nvob_assignment::updateTakeButton:routes_source==%1, preselected_source=%2", m_iVob_in_01_routed_source, m_iPreselectedSourcePackage);


	// Update the colour of the TAKE button
	
	if ((m_iPreselectedSourcePackage == -1) || (m_iVob_in_01_routed_source == m_iPreselectedSourcePackage))
	{
		textPut("statesheet", "mon_notfollowing", PANEL_MAIN, BTN_TAKE);

		// Clear
		showVirtual(0, BTN_TAKE);

		controlDisable(PANEL_MAIN, BTN_TAKE);
	}
	else
	{
		
		textPut("statesheet", "source_selected", PANEL_MAIN, BTN_TAKE);

		// Show/Hide 'V'
		showVirtual(m_iPreselectedSourcePackage, BTN_TAKE);

		controlEnable(PANEL_MAIN, BTN_TAKE);

	}


}

bncs_stringlist vob_assignment::loadAPLanguageTags(int iPackageIndex)
{
	// Get all the indexes for the AD Tags 
	bncs_string sTagIndexes = "";

	routerName(m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, iPackageIndex, sTagIndexes);

	bncs_stringlist slstTagIndexes(sTagIndexes, '|');
	bncs_stringlist slstRet;
	
	for (int i = 0; i < slstTagIndexes.count(); i++)
	{
		if (slstTagIndexes[i] == 0)
		{
			slstRet << "0";
		}
		else
		{
			bncs_string sAPTags = "";
			routerName(m_iPackagerRouterMainDevice, DATABASE_AP_TAGS, slstTagIndexes[i], sAPTags);

			bncs_stringlist sltAPTags(sAPTags);
			bncs_string sLanguage;

			sLanguage = sltAPTags.getNamedParam("language_tag");
			slstRet << sLanguage;


			// Only used here for debugging
			bncs_string sLanguageName;
			routerName(m_iPackagerRouterMainDevice, DATABASE_LANG_TAG, sLanguage, sLanguageName);

			bncs_stringlist sltNameStatus(sLanguageName, '|');
			bncs_string languageName = sltNameStatus[0];
		}
	}

	debug("\nvob_assignment::loadAPLanguageTags:: PackageIndex = %1 => %2", iPackageIndex, slstRet.toString());
	return slstRet;

}
bncs_stringlist vob_assignment::loadAPSourceTags(int iPackageIndex)
{
	// Get all the indexes for the AP Tags 
	bncs_string sTagIndexes = "";

	routerName(m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, iPackageIndex, sTagIndexes);

	bncs_stringlist slstTagIndexes(sTagIndexes, '|');

	debug("\nvob_assignment::loadAPSourceTags:: PackageIndex = %1 => %2", iPackageIndex, sTagIndexes);
	return slstTagIndexes;
}



void vob_assignment::updateScoopVobAPAdjustment(int iPackageIndex)
{

	// Is this virtual ?
	// Show/Hide 'V'
	showVirtual(iPackageIndex, BTN_SCOOP_VOB_AUDIO_PACKAGE_NAME);
	
	
	
	// Get all the indexes for the AP Tags 
	bncs_string sTagIndexes = "";
	
	routerName(m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, iPackageIndex, sTagIndexes);
	m_slstScoopVobAPTagIndexes = bncs_stringlist(sTagIndexes, '|');
	m_iScoopVobAPTagSelectedIndex = m_slstScoopVobAPTagIndexes[0].toInt();

	
	if (m_iScoopVobAPTagSelectedIndex > 0)
	{

		bncs_string sName = "";
		routerName(m_iPackagerRouterMainDevice, DATABASE_AP_NAME, m_iScoopVobAPTagSelectedIndex, sName);

		textPut("text", sName, PANEL_MAIN, BTN_SCOOP_VOB_AUDIO_PACKAGE_NAME);


		if (m_bScoopVOBOutputPackageEditingEnabled)
		{
			controlEnable(PANEL_MAIN, BTN_AP_EDIT_LANGUAGE);
			controlEnable(PANEL_MAIN, BTN_AP_EDIT);
		}

		controlEnable(PANEL_MAIN, BTN_SCOOP_VOB_AUDIO_PACKAGE_NAME);
		controlEnable(PANEL_MAIN, LBL_SCOOP_VOB_AUDIO_PACKAGE_TALLY);

		textPut("text", m_iScoopVobAPTagSelectedIndex, PANEL_MAIN, LBL_SCOOP_VOB_AUDIO_PACKAGE_TALLY);


		routerName(m_iPackagerRouterMainDevice, DATABASE_AP_TAGS, m_iScoopVobAPTagSelectedIndex, m_sScoopVobAPAPTags);

		bncs_stringlist sltAPTags(m_sScoopVobAPAPTags);

		m_iScoopVobAPOutputLanguageTag = sltAPTags.getNamedParam("language_tag").toInt();
		routerName(m_iPackagerRouterMainDevice, DATABASE_LANG_TAG, m_iScoopVobAPOutputLanguageTag, m_sScoopVobAPlanguageNameStatus);


		bncs_stringlist sltNameStatus(m_sScoopVobAPlanguageNameStatus, '|');
		m_sScoopVobAPlanguageName = sltNameStatus[0];
		textPut("text", m_sScoopVobAPlanguageName, PANEL_MAIN, BTN_AP_EDIT_LANGUAGE);
	}
	else
	{
		textPut("text", "???", PANEL_MAIN, BTN_SCOOP_VOB_AUDIO_PACKAGE_NAME);

		textPut("text", "Invalid SRC AP[0]", PANEL_MAIN, LBL_SCOOP_VOB_AUDIO_PACKAGE_TALLY);

		//		textPut("text", bncs_string("SRC Pkg (db6)|%1.%2|Invalid Data").arg(m_iPackagerRouterMainDevice).arg(iPackageIndex), PANEL_MAIN, BTN_AP_EDIT_LANGUAGE);
				textPut("text", bncs_string("VOB AP|Not Found In|SP %1").arg(iPackageIndex), PANEL_MAIN, BTN_AP_EDIT_LANGUAGE);

		setLiveButtonState(false);
		controlDisable(PANEL_MAIN, BTN_LIVE);

	}
}

void vob_assignment::updateScoopVobAPLive(int iPackageIndex)
{

	// Is live button set ?
	if (m_bLiveSet)
	{

		// If any vobs are using this source package AND they are set to the same language then
		// Remove their Scoop Vob Output package from the SP (if it exists in it, this shows they were live),
		// hence make them not live

		
		// Has the package index changed ?
		// If so remove the old AP and set the new one
		if ((iPackageIndex != m_iScoopVobPreviousPackageIndex) && (m_iScoopVobPreviousPackageIndex > 0) || (m_iScoopVobAPTagPreviousSelectedIndex != m_iScoopVobAPTagSelectedIndex))
		{
			// Get all the indexes for the AP Tags 
			bncs_string sTagIndexes = "";

			routerName(m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, iPackageIndex, sTagIndexes);
			m_slstScoopVobAPTagIndexes = bncs_stringlist(sTagIndexes, '|');

			m_iScoopVobAPTagSelectedIndex = m_slstScoopVobAPTagIndexes[0].toInt();

			if (m_iScoopVobAPTagSelectedIndex > 0)
			{

				bncs_string sName = "";
				routerName(m_iPackagerRouterMainDevice, DATABASE_AP_NAME, m_iScoopVobAPTagSelectedIndex, sName);

//				textPut("text", sName, PANEL_MAIN, BTN_SCOOP_VOB_AUDIO_PACKAGE_NAME);

//				textPut("text", m_sScoopVobAPTagSelectedIndex, PANEL_MAIN, LBL_SCOOP_VOB_AUDIO_PACKAGE_TALLY);


				routerName(m_iPackagerRouterMainDevice, DATABASE_AP_TAGS, m_iScoopVobAPTagSelectedIndex, m_sScoopVobAPAPTags);

				bncs_stringlist sltAPTags(m_sScoopVobAPAPTags);
				m_iScoopVobAPOutputLanguageTag = sltAPTags.getNamedParam("language_tag").toInt();
				routerName(m_iPackagerRouterMainDevice, DATABASE_LANG_TAG, m_iScoopVobAPOutputLanguageTag, m_sScoopVobAPlanguageNameStatus);


				bncs_stringlist sltNameStatus(m_sScoopVobAPlanguageNameStatus, '|');
				m_sScoopVobAPlanguageName = sltNameStatus[0];
			}
		}

		m_iScoopVobAPTagPreviousSelectedIndex = m_iScoopVobAPTagSelectedIndex;
		m_iScoopVobPreviousPackageIndex = iPackageIndex;
	}

}

void vob_assignment::setEditAudioLanguageTag(int languageTag)
{
	debug("audio_package_edit_view::setEditAudioLanguageTag languageTag=%1", languageTag);

	bncs_stringlist slstTags(m_sScoopVobAPAPTags);

	debug("\nslstTags.toString() = %1", slstTags.toString());
		// Set new language tag

	const bncs_string sNewLanguageTag = bncs_string("language_tag=%1").arg(languageTag);

		slstTags[1] = sNewLanguageTag;

	const bncs_string sIndex = m_iScoopVobAPTagSelectedIndex;

		debug("\nslstTags.toString() = %1", slstTags.toString());
	
		routerModify(m_iPackagerRouterMainDevice, DATABASE_AP_TAGS, sIndex, slstTags.toString(), false);
}

void vob_assignment::vobReset()
{
	
	m_iVob_in_01_dest = -1;
	m_iVob_in_01_routed_source = -1;
	m_sVob_in_02_selected_source = "-1";
	m_iVob_in_01_device = -1;
	m_iVob_in_01_slot = -1;

	m_iVob_in_02_dest = -1;
	m_iVob_in_02_routed_source = -1;
	m_sVob_in_02_selected_source = "-1";
	m_iVob_in_02_device = -1;
	m_iVob_in_02_slot = -1;

	m_iVob_in_03_dest = -1;
	m_iVob_in_03_routed_source = -1;
	m_sVob_in_03_selected_source = "-1";
	m_iVob_in_03_device = -1;
	m_iVob_in_03_slot = -1;

	m_iVob_out_01_dest = -1;
	m_iVob_out_01_routed_source = -1;
	m_iVob_out_01_device = -1;
	m_iVob_out_01_slot = -1;


	m_slstScoopVobAPTagIndexes.clear();
	m_sScoopVobAPAPTags = "";
	m_sScoopVobAPlanguageNameStatus = "";
	m_sScoopVobAPlanguageName = "";
	m_iScoopVobAPOutputLanguageTag = -1;


	m_iPopUpLanguageSelected = -1;
	m_iScoopVobAPOutputLanguageTag = -1;

	m_iScoopVobPreviousPackageIndex = -1;
	m_iScoopVobAPTagPreviousSelectedIndex = -1;

	m_slstPreselectedSourcePackageLanguageTags = "";
	m_iPreselectedSourcePackage = -1;
	
	controlDisable(PANEL_MAIN, BTN_PRESELECT_SOURCE);
	controlDisable(PANEL_MAIN, BTN_MON_1);
	controlHide(PANEL_MAIN, BTN_MON_2);
	controlHide(PANEL_MAIN, LBL_MON_2);
	controlHide(PANEL_MAIN, BTN_MON_3);
	controlHide(PANEL_MAIN, LBL_MON_3);

	controlDisable(PANEL_MAIN, BTN_SCOOP_VOB_AUDIO_PACKAGE_NAME);
	controlDisable(PANEL_MAIN, LBL_SCOOP_VOB_AUDIO_PACKAGE_TALLY);
	controlDisable(PANEL_MAIN, BTN_AP_EDIT_LANGUAGE);
	controlDisable(PANEL_MAIN, BTN_AP_EDIT);

	controlDisable(PANEL_MAIN, BTN_MIXER_ASSIGN);
	controlDisable(PANEL_MAIN, BTN_CONFIG_MIX);

	controlDisable(PANEL_MAIN, BTN_LIVE);

	controlHide(PANEL_MAIN, BTN_MON_3);
	controlHide(PANEL_MAIN, LBL_MON_3);

	showPresetWarningLabel(false);
	
	textPut("clear", "", PANEL_MAIN, DEBUG_LISTBOX);
	
	textPut("text", "", PANEL_MAIN, BTN_SCOOP_VOB_AUDIO_PACKAGE_NAME);

	textPut("text", "", PANEL_MAIN, LBL_SCOOP_VOB_AUDIO_PACKAGE_TALLY);

	textPut("index", "", PANEL_MAIN, BTN_PRESELECT_SOURCE);
	textPut("source", "", PANEL_MAIN, BTN_PRESELECT_SOURCE);
	textPut("dest", "", PANEL_MAIN, BTN_PRESELECT_SOURCE);


	textPut("index", "", PANEL_MAIN, BTN_MON_1);
	textPut("dest", "", PANEL_MAIN, BTN_MON_1);

	textPut("index", "", PANEL_MAIN, BTN_MON_2);
	textPut("dest", "", PANEL_MAIN, BTN_MON_2);

	textPut("index", "", PANEL_MAIN, BTN_MON_3);
	textPut("dest", "", PANEL_MAIN, BTN_MON_3);
	
	textPut("text", "", PANEL_MAIN, BTN_AP_EDIT_LANGUAGE);

	textPut("statesheet", "mon_notfollowing", PANEL_MAIN, BTN_TAKE);
	showVirtual(0, BTN_TAKE);
	controlDisable(PANEL_MAIN, BTN_TAKE);

	

	setLiveButtonType(m_bLiveToggleEnabled);

}

void vob_assignment::routeScoopVobOutputLive(bool bClear, const int iSource)
{

	bncs_stringlist slstSourceTagIndexes = loadAPSourceTags(iSource);
	const bncs_stringlist slstSourceTagIndexesOriginal = slstSourceTagIndexes;
	
	bncs_stringlist slstLanguageTags = loadAPLanguageTags(iSource);

	
	// If we are clearing then remove the currently inserted vob output source package language AP from the routed input source package language AP list

	if (bClear)
	{
		debug("setRouteScoopVobOutputLive::Clear Live::PreselectedSourcePackage %1", m_iPreselectedSourcePackage);

		const int iPos = slstSourceTagIndexes.find(bncs_string(m_iScoopVobAPTagSelectedIndex));
		if(iPos > -1)
		{
			// Exists so remove it - set to "";
			slstSourceTagIndexes[iPos] = "0";

			debug("%1::%2::%2", m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, slstSourceTagIndexes.toString('|'));
		}

	}
	else
	{

		debug("setRouteScoopVobOutputLive::Set Live::PreselectedSourcePackage %1", m_iPreselectedSourcePackage);
		// Check for any source languages that matched the rScoop language
		// remove these
		if (slstLanguageTags.find(m_iScoopVobAPOutputLanguageTag) > -1)
		{
			debug("%1::%2::%2", m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, slstSourceTagIndexes.toString('|'));

			for (auto& slstSourceTagIndexe : slstSourceTagIndexes)
			{
				bncs_string sAPTags = "";
				routerName(m_iPackagerRouterMainDevice, DATABASE_AP_TAGS, slstSourceTagIndexe, sAPTags);

				bncs_stringlist sltAPTags(sAPTags);
				bncs_string sLanguage;

				sLanguage = sltAPTags.getNamedParam("language_tag");

				//
				// Do not remove if this is the Scoop !
				// 
				if (slstSourceTagIndexe.toInt() != m_iScoopVobAPTagSelectedIndex)
				{
					if (sLanguage == m_iScoopVobAPOutputLanguageTag)
					{
						// Remove this source/language !
						slstSourceTagIndexe = "0";
					}
				}
			}

			// Found a duplicate language so overwrite it with the scoop output source package
			debug("%1::%2::%2", m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, slstSourceTagIndexes.toString('|'));
		}

		// Check for the existence of the routed scoop vob package.
		// This will exists if "Live" is set to true, and the panel is being run up initially
		const int iPos = slstSourceTagIndexes.find(bncs_string(m_iScoopVobAPTagSelectedIndex));
		if (iPos < 0)
		{
			// Does not exist
			// Find the first empty tag index in the routed source and insert the vob output package into it
			for (bncs_stringlist::iterator IT = slstSourceTagIndexes.begin(); IT != slstSourceTagIndexes.end(); IT++)
			{
				if ((*IT == "0") || (*IT == ""))
				{
					// 1st empty one found so set it
					*IT = bncs_string(m_iScoopVobAPTagSelectedIndex);
					debug("%1::%2::%2", m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, slstSourceTagIndexes.toString('|'));
					break;
				}
			}
		}
	}

	// Have we changed anything - if so update the packager source AP database
	if (slstSourceTagIndexes != slstSourceTagIndexesOriginal)
	{
		routerModify(m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, iSource, slstSourceTagIndexes.toString('|'), true);
	}
}

bool vob_assignment::validateSourcePackageScoopOutputPackageLanguageMatch(const int iSource)
{
	debug("validateSourcePackageScoopOutputPackageLanguageMatch::%1", iSource);

	if (iSource > 0)
	{
		bncs_stringlist m_slstSourcePackageLanguageTags = loadAPLanguageTags(iSource);

		debug("validateSourcePackageScoopOutputPackageLanguageMatch::%1::%2", iSource, m_slstSourcePackageLanguageTags.toString());

		if (m_slstSourcePackageLanguageTags.find(m_iScoopVobAPOutputLanguageTag) > -1)
		{
			return true;
		}
	}

	return false;
}

bool vob_assignment::validateSourcePackageContainsSelectedLanguage(int iSource, const bncs_string sSelectedLanguage)
{
	debug("validateSourcePackageContainsSelectedLanguage::%1", iSource);

	if (iSource > 0)
	{
		bncs_stringlist m_slstSourcePackageLanguageTags = loadAPLanguageTags(iSource);

		debug("validateSourcePackageContainsSelectedLanguage::%1::%2", iSource, m_slstSourcePackageLanguageTags.toString());

		if (m_slstSourcePackageLanguageTags.find(sSelectedLanguage) > -1)
		{
			return true;
		}
	}

	return false;

}
bool vob_assignment::validateSourcePackageContainsScoopPackageAPIndex(const bncs_string& s_source)
{

	debug("validateSourcePackageContainsScoopPackageAPIndex::%1", s_source);

	if (s_source.length() > 0)
	{

		bncs_stringlist m_slstSourcePackageTags = loadAPSourceTags(s_source);

		debug("validateSourcePackageContainsScoopPackageAPIndex::%1::%2", s_source, m_slstSourcePackageTags.toString());

		if (m_slstSourcePackageTags.find(bncs_string(m_iScoopVobAPTagSelectedIndex)) > -1)
		{
			return true;
		}
	}
	return false;
}


void vob_assignment::showVirtual(const int iPackageIndex, const bncs_string s_control_id)
{
	
	if (iPackageIndex >= m_iFirstVirtualPackage)
	{
		textPut("pixmap.topLeft", "/images/icon_virtual.png", PANEL_MAIN, s_control_id);
	}
	else
	{
		textPut("pixmap.topLeft", "", PANEL_MAIN, s_control_id);
	}
}

void vob_assignment::showPresetWarningLabel(bool bShow)
{
	// Only show if preselect source is not routed

	if (m_iPreselectedSourcePackage != m_iVob_in_01_routed_source)
	{

		if (bShow)
		{
			controlShow(PANEL_MAIN, LBL_PRESET_WARN);
			return;
		}
	}		controlHide(PANEL_MAIN, LBL_PRESET_WARN);
}

void vob_assignment::showLanguageChangeWarningLabel(bool bShow)
{
	if (bShow)
	{
		controlShow(POPUP_LANGUAGE_TAG, LBL_LANG_WARN);
		return;
	}

	controlHide(POPUP_LANGUAGE_TAG, LBL_LANG_WARN);
}

void vob_assignment::setLiveButtonType(bool bSetEnabled)
{
	if (bSetEnabled)
	{
		textPut("style", "button", PANEL_MAIN, BTN_LIVE);
		textPut("doublepush", "true", PANEL_MAIN, BTN_LIVE);
		textPut("notify.released", "true", PANEL_MAIN, BTN_LIVE);
	}
	else
	{
		textPut("style", "label", PANEL_MAIN, BTN_LIVE);
		textPut("doublepush", "false", PANEL_MAIN, BTN_LIVE);
		textPut("notify.released", "false", PANEL_MAIN, BTN_LIVE);
	}
}

void vob_assignment::setLiveButtonState(bool bIsLive)
{
	if (bIsLive)
	{
		textPut("statesheet", "mon_following", PANEL_MAIN, BTN_LIVE);
		textPut("text", "LIVE", PANEL_MAIN, BTN_LIVE);
	}
	else
	{
		textPut("statesheet", "mon_notfollowing", PANEL_MAIN, BTN_LIVE);
		textPut("text", "NO LINK", PANEL_MAIN, BTN_LIVE);
	}

}

bool vob_assignment::validateCurrentlyLive()
{
	// If we have a match in the source input AP tag with the Scoop Output Tag then assume Live button should be set

	bncs_stringlist slstSourceTagIndexes = loadAPSourceTags(m_iVob_in_01_routed_source);

	bool bFound = false;

	if (m_iScoopVobAPTagSelectedIndex > 0)
	{
		const int iPos = slstSourceTagIndexes.find(bncs_string(m_iScoopVobAPTagSelectedIndex));
		if (iPos > -1)
		{
			bFound = true;
		}
	}
	
	setLiveButtonState(bFound);
	return bFound;

}
