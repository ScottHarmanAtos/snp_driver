#ifndef vob_assignment_INCLUDED
	#define vob_assignment_INCLUDED

#include <bncs_script_helper.h>
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class vob_assignment : public bncs_script_helper
{
public:
	vob_assignment( bncs_client_callback * parent, const char* path );
	virtual ~vob_assignment();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_sInstance;
	bncs_string m_sWorkspace;
	bncs_string m_sGroupname;
	
	bncs_string m_sCompositePackager;
	bncs_packager m_packagerRouter;
	int m_iPackagerRouterMainDevice;
	int m_iFirstVirtualPackage;

	bool m_bInitialising;
	
	bncs_string m_sName;
	bncs_string m_sDest_pages_template;
	bncs_string m_sSource_pages_template;
	bncs_string m_sGroup;

	int m_iPreselectedSourcePackage;
	bncs_stringlist m_slstPreselectedSourcePackageLanguageTags;
	
	int m_iVob_in_01_dest;
	int m_iVob_in_01_routed_source;
	bncs_string m_sVob_in_01_selected_source;
	int m_iVob_in_01_device;
	int m_iVob_in_01_slot;

	int m_iVob_in_02_dest;
	int m_iVob_in_02_routed_source;
	bncs_string m_sVob_in_02_selected_source;
	int m_iVob_in_02_device;
	int m_iVob_in_02_slot;

	int m_iVob_in_03_dest;
	int m_iVob_in_03_routed_source;
	bncs_string m_sVob_in_03_selected_source;
	int m_iVob_in_03_device;
	int m_iVob_in_03_slot;

	int  m_iVob_out_01_dest;
	int m_iVob_out_01_routed_source;
	int m_iVob_out_01_device;
	int m_iVob_out_01_slot;

	int m_iPopUpLanguageSelected;
	int m_iScoopVobAPOutputLanguageTag;

	bncs_stringlist m_slstScoopVobAPTagIndexes;
	int m_iScoopVobAPTagSelectedIndex;
	int m_iScoopVobAPTagPreviousSelectedIndex;
	bncs_string m_sScoopVobAPAPTags;
	bncs_string m_sScoopVobAPlanguageNameStatus;
	bncs_string m_sScoopVobAPlanguageName;

	
	bool m_bScoopVOBOutputPackageEditingEnabled;
	bool m_bLiveSet;
	bool m_bLiveToggleEnabled;

	int m_iScoopVobPreviousPackageIndex;

	void initProperties();
	void vobReset();
	void showVirtual(int iPackageIndex, bncs_string s_control_id);


	void updateTakeButton();
	void updateScoopVobAPAdjustment(int iPackageIndex);
	void updateScoopVobAPLive(int iPackageIndex);
	void updateDebugListBox();


	void routeScoopVobOutputLive(bool bClear, int iSource);
	bncs_stringlist loadAPLanguageTags(int iPackageIndex);
	bncs_stringlist loadAPSourceTags(int iPackageIndex);

	bool validateSourcePackageScoopOutputPackageLanguageMatch(int iSource);
	bool validateSourcePackageContainsScoopPackageAPIndex(const bncs_string& s_source);
	bool validateSourcePackageContainsSelectedLanguage(int iSource, bncs_string sSelectedLanguage);

	bool validateCurrentlyLive();
	void showPresetWarningLabel(bool bShow);
	void showLanguageChangeWarningLabel(bool bShow);


	void setEditAudioLanguageTag(int languageTag);
	void setLiveButtonType(bool bSetEnabled);
	void setLiveButtonState(bool bIsLive);



};


#endif // vob_assignment_INCLUDED