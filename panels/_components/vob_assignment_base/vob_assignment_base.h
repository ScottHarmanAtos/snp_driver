#ifndef Vob_assignment_base_INCLUDED
	#define Vob_assignment_base_INCLUDED

#include <bncs_script_helper.h>
#include "workspace_group.h"
#include "bncs_packager.h"
#include <packager_common.h>

#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT _declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class Vob_assignment_base : public bncs_script_helper
{
public:
	Vob_assignment_base( bncs_client_callback * parent, const char* path );
	virtual ~Vob_assignment_base();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;

	bncs_string m_workspaceId;
	bncs_string m_title;
	bncs_string m_currentGroup;

	bncs_string m_sCompositePackager;
	bncs_packager m_packagerRouter;
	int m_iPackagerRouterMainDevice;
	int m_iFirstVirtualPackage;

	bncs_string m_workspace;
	bncs_string m_area;

	bncs_string m_sFile_Market_Config;
	
	int m_iSelectedTab;
	int m_iSelectedMarket;
	
	bool m_bInMcr;
	void initWorkspaceGroups();

	void showTabs(const bncs_string& sTab);

	void populatePopupMarketPanel();

	
	map<bncs_string, workspace_group *> m_groupWorkspaces; // ControlId, workspace data

	map<bncs_string, Vob *> m_Vob_Data; // workspace, vob dynamic data 

	void selectTab(int iSelectedTab);

	void passToAllVobs(const bncs_string& sCommand, const bncs_string& sValue);
	void passWorkSpaceToVob(int iSettingId, const bncs_string& sVobId);
	void setButtonCaption(const bncs_string& sCaption);

	workspace * getWorkspaceGroup(const bncs_string& s_workspace);
	static bool workspaceExists(const bncs_string& sWorkspace);

	void resetGroupWorkspaceLive(const bncs_string& sWorkSpace, const bncs_string& sRoutedSource, const bncs_string& sScoopVobAPOutputLanguageTag);

	bncs_string getAPLanguage(const int& iSourceAP);

};


#endif // Vob_assignment_base_INCLUDED