﻿#pragma once
#include <bncs_string.h>
#include <vector>

class Vob
{

public:
	Vob(const bncs_string& sWorkspace);
	~Vob();

	void Vob_in_01_dest(const bncs_string& sDest);
	int Vob_in_01_dest() const;

	void Vob_out_01_dest(const bncs_string& sDest);
	int Vob_out_01_dest() const;

	void Vob_in_01_routedSource(const bncs_string& sSource);
	int Vob_in_01_routedSource() const;

	void Vob_out_01_routedSource(const bncs_string& sSource);
	int Vob_out_01_routedSource() const;

	void Vob_in_01_infoDriverDevice(const bncs_string& sDevice);
	int Vob_in_01_infoDriverDevice() const;

	void Vob_out_01_infoDriverDevice(const bncs_string& sDevice);
	int Vob_out_01_infoDriverDevice() const;

	void Vob_in_01_infoDriverSlot(const bncs_string& sSlot);
	int Vob_in_01_infoDriverSlot() const;

	void Vob_out_01_infoDriverSlot(const bncs_string& sSlot);
	int Vob_out_01_infoDriverSlot() const;

	void Vob_in_01_APLanguage(const bncs_string& sLanguage);
	int Vob_in_01_APLanguage() const;

	void Vob_out_01_APLanguage(const bncs_string& sLanguage);
	int Vob_out_01_APLanguage() const;

	void Vob_out_01_ScoopVobAPSelectedIndex(const bncs_string& sIndex);
	int Vob_out_01_ScoopVobAPSelectedIndex() const;
	
	void WorkSpace(const bncs_string& sWorkspace);
	bncs_string WorkSpace() const;


private:
	int _Vob_in_01_dest;
	int _Vob_out_01_dest;
	
	int _Vob_in_01_routed_source;
	int _Vob_out_01_routed_source;

	int _Vob_in_01_infoDriverDevice;
	int _Vob_out_01_infoDriverDevice;

	int _Vob_in_01_infoDriverSlot;
	int _Vob_out_01_infoDriverSlot;

	int _Vob_in_01_APLanguage;
	int _Vob_out_01_APLanguage;

	int _Vob_out_01_ScoopVobAPSelectedIndex;
	
	bncs_string _WorkSpace;

};


class workspace
{

public:
	workspace(const bncs_string& sWorkspaceId, const bncs_string& sWorkSpaceInstance, const bncs_string& sWorkSpaceName,
	          const bncs_string& _WorkspaceGroup);
	~workspace();

	bncs_string _WorkspaceInstance;
	bncs_string _WorkspaceName;
	bncs_string _WorkspaceId;
	bncs_string _WorkspaceGroup;
		
};


class workspace_group
{

public:
	workspace_group();
	~workspace_group();

	bncs_string sGroupButtonName;
	bncs_string sGroupId;
	bncs_string sGroupLiveToggleEnabled;
	bncs_string sGroupAudioOutputEditEnabled;

	void addWorkSpace(const bncs_string &sworkspaceId, const bncs_string &sWorkSpaceInstance, const bncs_string &sWorkSpaceName, const bncs_string& sWorkspaceGroup);
	void removeUnusedWorkSpaceTrailingEntries();

	bncs_string getWorkspaceName( int iSettingID);
	bncs_string getWorkspaceInstance(int iSettingID);

	int getWorkSpaceCount() const;
	workspace * getWorkSpaceGroup(const bncs_string& _Workspace);
	
	std::vector <workspace *> vec_workspace;

};

