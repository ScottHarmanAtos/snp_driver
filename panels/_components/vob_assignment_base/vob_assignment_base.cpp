#include <windows.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "Vob_assignment_base.h"

#define PANEL_MAIN					1
#define PANEL_POPUP_MARKET			2
#define POPOVER_EDIT_AUDIO_PACKAGE	3

#define TIMER_SETUP					1

#define POPUP_MARKET_UI				"popup_market.bncs_ui"
#define POPOVER_EDIT_UI				"popover_edit_ap.bncs_ui"		
#define MAX_TABS					10
#define INIT						"-1"

#define BTN_MARKET					"btn_market"
#define GRP_TABS					"grp_tabs"
#define LBL_INSTRUCTION				"lbl_Instruction"

#define DEFAULT_FILE_MARKET_VOBS	"workspace_resources/_market_vobs"


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( Vob_assignment_base )

// constructor - equivalent to ApplCore STARTUP
Vob_assignment_base::Vob_assignment_base( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file Vob_assignment_base.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow( PANEL_MAIN, "vob_assignment_base.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
	m_workspaceId = "";
	m_iSelectedTab = -1; 
	m_iSelectedMarket = -1;
	m_title = "";
	m_currentGroup = INIT;
	m_bInMcr = false;


	m_workspace = getWorkstationSetting("workspace");
	m_area = getWorkstationSetting("ops_area");
	
	if (m_area.lower().find("mcr") > -1)
	{
		m_bInMcr = true;
	}

	m_sCompositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_sCompositePackager);

	//Get main router device
	m_iPackagerRouterMainDevice = m_packagerRouter.destDev(1);

	m_iFirstVirtualPackage = m_packagerRouter.firstVirtual();

	m_sFile_Market_Config = DEFAULT_FILE_MARKET_VOBS;


	textPut("statesheet", "dest_deselected", PANEL_MAIN, BTN_MARKET);

	m_currentGroup = INIT;
	m_iSelectedTab = -1;
	setButtonCaption("");

	textPut("text", "Market", PANEL_MAIN, GRP_TABS);

}

// destructor - equivalent to ApplCore CLOSEDOWN
Vob_assignment_base::~Vob_assignment_base()
{
}

// all button pushes and notifications come here
void Vob_assignment_base::buttonCallback( buttonNotify *b )
{
	//b->dump("\nbuttonCallback::");
	if (b->panel() == PANEL_MAIN)
	{
		if (b->id() == BTN_MARKET)
		{
			// Ignore button if not in MCR
			if (m_bInMcr)
			{
				controlHide(PANEL_MAIN, LBL_INSTRUCTION);

				textPut("statesheet", "dest_selected", PANEL_MAIN, BTN_MARKET);
				panelPopup(PANEL_POPUP_MARKET, POPUP_MARKET_UI);
				populatePopupMarketPanel();
			}
		}
		else if (b->id().startsWith("tab_") && b->id().endsWith("_button"))
		{
			m_iSelectedTab = bncs_stringlist(b->id(), '_')[1].toInt();

			// Check limit
			if (m_iSelectedTab > MAX_TABS)
			{
				m_iSelectedTab = MAX_TABS;
			}
			
			selectTab(m_iSelectedTab);

		}
		else if (b->id().startsWith("vob_"))
		{
			if (b->command() == "setLive")
			{
				// Data

				bncs_stringlist slstData(b->value(), '.');

				const bncs_string sGroup = slstData[0];
				const bncs_string sWorkSpace = slstData[1];
				const bncs_string sRoutedPackage = slstData[2];
				const bncs_string sScoopVobAPOutputLanguageTag = slstData[3];

				// This resets the live on all the workspaces in this group
				// and sets selected workspace to be live
				resetGroupWorkspaceLive(sWorkSpace, sRoutedPackage,  sScoopVobAPOutputLanguageTag);
			}
			else if (b->command() == "editingAP")
				{
					panelShow(POPOVER_EDIT_AUDIO_PACKAGE, POPOVER_EDIT_UI);

				bncs_stringlist slstValues(b->value());
				debug("Vob_assignment_base::buttonCallback::editingAP::ap=%1 sp=%2", slstValues[0], slstValues[1]);
				textPut("ap", slstValues[0], POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
				textPut("sp", slstValues[1], POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
			}
			else if (b->command() == "reset")
			{
				if (b->value() == "preselect")
				{
					passToAllVobs("reset", "preselect");
				}
			}
		}
		else if (b->id() == GRP_TABS)
		{
			passToAllVobs("reset", "preselect");
		}
	}
	else if (b->panel() == POPOVER_EDIT_AUDIO_PACKAGE)
	{
		if (b->id() == "audio_package_edit_view")
		{
			if (b->command() == "action" && b->value() == "close")
			{
				panelRemove(POPOVER_EDIT_AUDIO_PACKAGE);
			}
		}
	}

	else if (b->panel() == PANEL_POPUP_MARKET)
	{
		if (b->id().startsWith("group"))
		{
			m_iSelectedMarket = bncs_stringlist(b->id(), '_')[1].toInt();

			// Get button group caption
			
			textGet("text", PANEL_POPUP_MARKET, b->id(), m_currentGroup);

			if (m_currentGroup == NULL)
			{
				m_currentGroup = INIT;
				m_iSelectedTab = -1;
			}
			else
			{
				m_iSelectedTab = 1;
			}
			
			showTabs(m_currentGroup);
			selectTab(m_iSelectedTab);

			bncs_string sCaption = "";
			textGet("text", PANEL_POPUP_MARKET, bncs_string("group_%1").arg(m_iSelectedMarket, '0' , 2, 10), sCaption);

			setButtonCaption(sCaption);

			panelRemove(PANEL_POPUP_MARKET);

			textPut("statesheet", "dest_deselected", PANEL_MAIN, BTN_MARKET);
		}
	}
}

// all revertives come here
int Vob_assignment_base::revertiveCallback(revertiveNotify * r)
{

	// Example data coming in from packager_router_main is
	// index=46185,v_level=0,v_index=0

	bncs_stringlist sSourceData(r->sInfo());
	const bncs_string sSourceIndex = sSourceData.getNamedParam("index");


	for (auto& ITVobEntry : m_Vob_Data)
	{
		// Find matching vob entry if a VOB IN 01 revertive
		if ((ITVobEntry.second->Vob_in_01_infoDriverDevice() == r->device()) && (ITVobEntry.second->Vob_in_01_infoDriverSlot() == r->index()))
		{
			// Update/store the routed AP audio source for Vob 1 output
			ITVobEntry.second->Vob_in_01_routedSource(sSourceIndex.toInt());
			debug(bncs_string("\nVob_Data In= WS=%1, Dest=%2, Device=%3, Slot=%4,RoutedSource=%5, APLanguage=%6")
				.arg(ITVobEntry.second->WorkSpace())
				.arg(ITVobEntry.second->Vob_in_01_dest())
				.arg(ITVobEntry.second->Vob_in_01_infoDriverDevice())
				.arg(ITVobEntry.second->Vob_in_01_infoDriverSlot())
				.arg(ITVobEntry.second->Vob_in_01_routedSource())
				.arg(ITVobEntry.second->Vob_in_01_APLanguage()));

		}
		else if   ((ITVobEntry.second->Vob_out_01_infoDriverDevice() == r->device()) && (ITVobEntry.second->Vob_out_01_infoDriverSlot() == r->index()))
		{
			// Find matching vob entry if a VOB OUT 01 revertive
			
		   // Update/store the routed AP audio source for Vob 1 output
			ITVobEntry.second->Vob_out_01_routedSource(sSourceIndex.toInt());

			// Get the Scoop Vob Output AP list
			bncs_string sTagIndexes = "";

			routerName(m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, ITVobEntry.second->Vob_out_01_dest(), sTagIndexes);
			bncs_stringlist slstScoopVobAPTagIndexes = bncs_stringlist(sTagIndexes, '|');
			ITVobEntry.second->Vob_out_01_ScoopVobAPSelectedIndex(slstScoopVobAPTagIndexes[0]);
			
			// Get the language set for the Scoop Vob, always use the first entry in the AP list
			ITVobEntry.second->Vob_out_01_APLanguage(getAPLanguage(ITVobEntry.second->Vob_out_01_ScoopVobAPSelectedIndex()));

			debug(bncs_string("\nVob_Data Out=WS=%1, Dest=%2, Device=%3, Slot=%4,RoutedSource=%5, APLanguage=%6, ScoopPackageIndex=%7")
				.arg(ITVobEntry.second->WorkSpace())
				.arg(ITVobEntry.second->Vob_out_01_dest())
				.arg(ITVobEntry.second->Vob_out_01_infoDriverDevice())
				.arg(ITVobEntry.second->Vob_out_01_infoDriverSlot())
				.arg(ITVobEntry.second->Vob_out_01_routedSource())
				.arg(ITVobEntry.second->Vob_out_01_APLanguage())
				.arg(ITVobEntry.second->Vob_out_01_ScoopVobAPSelectedIndex()));

		}
		
	}
	
	return 0;
}

// all database name changes come back here
void Vob_assignment_base::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string Vob_assignment_base::parentCallback( parentNotify *p )
{

	if( p->command() == "return" )
	{
		bncs_stringlist sl;

		sl << bncs_string("market_config=%1").arg(m_sFile_Market_Config);

		return sl.toString('\n');
	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{
		// Unused just here because
		m_instance = p->value();
	}
	else if (p->command() == "market_config" && p->value() != m_instance)
	{
		m_sFile_Market_Config = p->value();
		initWorkspaceGroups();

	}


	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
//		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void Vob_assignment_base::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void Vob_assignment_base::selectTab(int iSelectedTab)
{

	// Hide all tab selections
	for (int i = 1; i <= MAX_TABS; i++)
	{
       	controlHide(PANEL_MAIN, bncs_string("tab_%1_selected").arg(i));
	}

	if (iSelectedTab == -1)
	{

		setButtonCaption("");

		// Hide all vob controls
		passWorkSpaceToVob(iSelectedTab, "vob_1");
		passWorkSpaceToVob(iSelectedTab, "vob_2");
		passWorkSpaceToVob(iSelectedTab, "vob_3");
	}
	else
	{
		// show selected tab
		controlShow(PANEL_MAIN, bncs_string("tab_%1_selected").arg(iSelectedTab));

		// Set to the group of 3 vob controls we need to activate
		const int iTabOffset = (iSelectedTab * 3) - 2;

		passWorkSpaceToVob(iTabOffset, "vob_1");
		passWorkSpaceToVob(iTabOffset + 1, "vob_2");
		passWorkSpaceToVob(iTabOffset + 2, "vob_3");
	}


}

void  Vob_assignment_base::passWorkSpaceToVob(const int iSettingId, const bncs_string& sVobId)
{
	if ((iSettingId > 0) && (iSettingId <= m_groupWorkspaces[m_currentGroup]->getWorkSpaceCount()))
	{
		const bncs_string sWorkspaceInstance = m_groupWorkspaces[m_currentGroup]->getWorkspaceInstance(iSettingId);
		const bncs_string sGroupLiveToggleEnabled = m_groupWorkspaces[m_currentGroup]->sGroupLiveToggleEnabled;
		const bncs_string sGroupLiveAudioOutputEditEnabled = m_groupWorkspaces[m_currentGroup]->sGroupAudioOutputEditEnabled;
		const bncs_string sGroupAudioOutputEditEnabled = m_groupWorkspaces[m_currentGroup]->sGroupAudioOutputEditEnabled;

		if ((sWorkspaceInstance != "") && (workspaceExists(sWorkspaceInstance)))
		{

			debug("\nVob_assignment::passWorkSpaceToVob() workspace=%1", sWorkspaceInstance);

			textPut("groupname", m_currentGroup, PANEL_MAIN, sVobId);
			textPut("workspace", sWorkspaceInstance, PANEL_MAIN, sVobId);
			textPut("live_toggle_enabled", sGroupLiveToggleEnabled, PANEL_MAIN, sVobId);
			textPut("audio_output_edit_enabled", sGroupAudioOutputEditEnabled, PANEL_MAIN, sVobId);

			controlShow(PANEL_MAIN, sVobId);

			return;
		}

		debug("\nERROR::Vob_assignment::passWorkSpaceToVob() workspace or instance does not exist = %1", sWorkspaceInstance);

	}


	controlHide(PANEL_MAIN, sVobId);

}

void Vob_assignment_base::passToAllVobs(const bncs_string& sCommand, const bncs_string& sValue)
{
	// Find all the controls starting with dest_

	bncs_stringlist slstList = getIdList(PANEL_MAIN, "vob_");

	for (auto& slstIT : slstList)
	{
		//debug(bncs_string("%1=%2 => dest = %3").arg(sCommand).arg(sValue).arg(*slstIT));

		textPut(sCommand, sValue, PANEL_MAIN, slstIT);
	}

}

void Vob_assignment_base::initWorkspaceGroups()
{
	
	bncs_config cGroups(m_sFile_Market_Config);

	m_groupWorkspaces.clear();
	
	//debug("Vob_assignment::initGroups() cGroups.isValid()=%1", cGroups.isValid()? "true" : "false");
	//debug("Vob_assignment::initGroups() cGroups.isChildValid()=%1", cGroups.isChildValid()? "true" : "false");
	//debug("Vob_assignment::initGroups() cGroups.Attr(\"id\")=%1", cGroups.childAttr("id"));
	//debug("Vob_assignment::initGroups() cGroups.Attr(\"name\")=%1", cGroups.childAttr("name"));


	if (cGroups.isValid())
	{
		while (cGroups.isChildValid())
		{
			const bncs_string groupId = cGroups.childAttr("id");
			const bncs_string groupName = cGroups.childAttr("name");
			const bncs_string groupLiveEnable = cGroups.childAttr("live_enable");
			const bncs_string groupAudioOutputEditEnabled = cGroups.childAttr("audio_output_edit_enabled");

			auto* clsWorkSpaceGroup = new workspace_group;
			clsWorkSpaceGroup->sGroupButtonName = groupName;
			clsWorkSpaceGroup->sGroupId = groupId;
			clsWorkSpaceGroup->sGroupLiveToggleEnabled = groupLiveEnable;
			clsWorkSpaceGroup->sGroupAudioOutputEditEnabled = groupAudioOutputEditEnabled;


			// These are always enabled if in an mcr - overwrite
			if (m_bInMcr)
			{
				clsWorkSpaceGroup->sGroupLiveToggleEnabled = "true";
				clsWorkSpaceGroup->sGroupAudioOutputEditEnabled = "true";
			}
			
			//////////////////////

			bncs_config cId(bncs_string("%1.%2").arg(m_sFile_Market_Config).arg(groupId));
			if (cId.isValid())
			{
				while (cId.isChildValid())
				{
					//debug("Vob_assignment::initGroups() cId.childAttr(\"id\")=%1", cId.childAttr("id"));
					//debug("Vob_assignment::initGroups() cId.childAttr(\"value\")=%1", cId.childAttr("value"));

					bncs_string sWorkspaceId = cId.childAttr("id");
					bncs_string sWorkspace = cId.childAttr("value");

					// If there is no value (workspace name), keep the position empty
					// There will be no XML file for it
					// Set the workspaceName to be the "--"

					if (sWorkspace.length() > 0)
					{

						//get name of workspace
						bncs_config cWorkspaceProperties(bncs_string("workspace_resources/%1.properties").arg(sWorkspace));
						if (cWorkspaceProperties.isValid())
						{
							while (cWorkspaceProperties.isChildValid())
							{
								bncs_string id = cWorkspaceProperties.childAttr("id");
								const bncs_string sWorkSpaceName = cWorkspaceProperties.childAttr("value");
								if (id == "name")
								{
									clsWorkSpaceGroup->addWorkSpace(sWorkspaceId, sWorkspace, sWorkSpaceName, groupName);
									break;
								}

								cWorkspaceProperties.nextChild();
							}

							//
							// Properties :: Name found so continue !
							// Build a map of all the source packages and how they are used
							// Requires vob 1 in dest - get SP from revertive - update on revertive
							//
							Vob *clsVob = new Vob(sWorkspace);

							/////////////////////////
							/// VOB INPUTS
							/////////////////////////
							bncs_config cCfgVob_in(bncs_string("workspace_resources/%1.vob_in").arg(sWorkspace));
							if (cCfgVob_in.isValid())
							{

								while (cCfgVob_in.isChildValid())
								{
									bncs_string sId = cCfgVob_in.childAttr("id");

									if (sId == "01")
									{
										bncs_string sValue = cCfgVob_in.childAttr("value");

										clsVob->Vob_in_01_dest(sValue);
										clsVob->Vob_in_01_infoDriverDevice(m_packagerRouter.destDev(sValue));
										clsVob->Vob_in_01_infoDriverSlot(m_packagerRouter.destSlot(sValue));

										infoRegister(clsVob->Vob_in_01_infoDriverDevice(), clsVob->Vob_in_01_infoDriverSlot(), clsVob->Vob_in_01_infoDriverSlot(), true);
										infoPoll(clsVob->Vob_in_01_infoDriverDevice(), clsVob->Vob_in_01_infoDriverSlot(), clsVob->Vob_in_01_infoDriverSlot());
									}
									else if (sId == "02")
									{
										// Unused at present 
									}
									else if (sId == "03")
									{
										// Unused at present 
									}
									cCfgVob_in.nextChild();
								}
							}
							else
							{
								debug("ERROR::Vob_assignment::initWorkspaceGroups()::Cannot find 'workspace_resources/%1.vob_in 01'", sWorkspace);
							}

							/////////////////////////
							/// VOB OUTPUTS
							/////////////////////////
							bncs_config cCfgVob_out(bncs_string("workspace_resources/%1.vob_out").arg(sWorkspace));
							if (cCfgVob_out.isValid())
							{
								while (cCfgVob_out.isChildValid())
								{
									bncs_string sId = cCfgVob_out.childAttr("id");

									if (sId == "01")
									{
										bncs_string sValue = cCfgVob_out.childAttr("value");

										clsVob->Vob_out_01_dest(sValue);
										clsVob->Vob_out_01_infoDriverDevice(m_packagerRouter.destDev(sValue));
										clsVob->Vob_out_01_infoDriverSlot(m_packagerRouter.destSlot(sValue));

										// Add into map using workspace as a key
										m_Vob_Data[clsVob->WorkSpace()] = clsVob;

										infoRegister(clsVob->Vob_out_01_infoDriverDevice(), clsVob->Vob_out_01_infoDriverSlot(), clsVob->Vob_out_01_infoDriverSlot(), true);
										infoPoll(clsVob->Vob_out_01_infoDriverDevice(), clsVob->Vob_out_01_infoDriverSlot(), clsVob->Vob_out_01_infoDriverSlot());
									}
									cCfgVob_out.nextChild();
								}
							}
							else
							{
								debug("ERROR::Vob_assignment::initWorkspaceGroups()::Cannot find 'workspace_resources/%1.vob_out 01'", sWorkspace);
							}
						}
						else
						{
							debug("ERROR::Vob_assignment::initWorkspaceGroups()::Cannot find 'workspace_resources/%1.properties'", sWorkspace);
						}

					}
					else
					{
//						debug("ERROR::Vob_assignment::Empty workspace entry=%1, cannot be found", groupName);
						clsWorkSpaceGroup->addWorkSpace(sWorkspaceId, sWorkspace, "--", groupName);
					}
						//debug("Vob_assignment::initWorkSpaces(%1) Group=%2 index=%3 workspace=%4", groupName, bncs_string("tab_%1_button").arg(groupId), workspaceId, workspaceValue);

				cId.nextChild();
				}

				// Now we have a full list of all indexes from market_vobs.xlm for a specific Market
				// Starting from the end remove the unwanted empty entries
				// Stop as soon as a valid entry is found.
				// This leaves the valid entries with blank entries in between them !!

				clsWorkSpaceGroup->removeUnusedWorkSpaceTrailingEntries();

				m_groupWorkspaces.insert(std::make_pair(groupName, clsWorkSpaceGroup));

			}
			else
			{
				debug(bncs_string("ERROR::Vob_assignment::initWorkspaceGroups()::Group::%1=%2 Child Id/name not found").arg(m_sFile_Market_Config).arg(groupId));

				delete clsWorkSpaceGroup;
			}

			cGroups.nextChild();
		}


		workspace * wsGroup = nullptr;

		if (m_workspace.length() > 0)
		{

			// Get to current workspace defined in workstation_settings.xml for this Mcr area
			wsGroup = getWorkspaceGroup(m_workspace);

			if (wsGroup != nullptr)
			{
				m_currentGroup = wsGroup->_WorkspaceGroup;
				m_iSelectedTab = 1;
			}
		}


		if (m_bInMcr)
		{
			// If in MCR we show the "Select Market" button
			// We also have have 2 options
			// [1]
			// If the workspace exists and has relevant Vobs - show these
			// do NOT show the "Select Market" instruction label
			// [2]
			// If the workspace does NOT exist, or is empty
			// show an empty screen 
			// show the "Select Market" instruction label

			textPut("style", "button", PANEL_MAIN, BTN_MARKET);

			if (wsGroup != nullptr)
			{
				controlHide(PANEL_MAIN, LBL_INSTRUCTION);
			}
		}
		else
		{
			// Do NOT show the "Select Market" instruction label

			// If NOT in MCR we show the "Market" label - not button
			// We also have have 2 options
			// [1]
			// If the workspace exists and has relevant Vobs - show these
			// [2]
			// If the workspace does NOT exist, or is empty
			// show an empty screen 
			// show an error on the "Market" label

			textPut("style", "label", PANEL_MAIN, BTN_MARKET);

			controlHide(PANEL_MAIN, LBL_INSTRUCTION);

			if (m_workspace.length() > 0)
			{

				if (wsGroup != nullptr)
				{
					setButtonCaption(m_groupWorkspaces[m_currentGroup]->sGroupButtonName);
				}
				else
				{
					textPut("text", "Incorrect|Workspace", PANEL_MAIN, BTN_MARKET);
				}
			}
			else
			{
				textPut("text", "No Work|space set", PANEL_MAIN, BTN_MARKET);
			}
		}

		showTabs(m_currentGroup);
		selectTab(m_iSelectedTab);
	}
	else
	{
		// Error - the config file cannot be found
		// Prob set up wrongly at design time on the main panel
		// Needs setting on component panel
		
		showTabs(INIT);
		controlHide(PANEL_MAIN, "vob_1");
		controlHide(PANEL_MAIN, "vob_2");
		controlHide(PANEL_MAIN, "vob_3");

		controlHide(PANEL_MAIN, LBL_INSTRUCTION);

		textPut("statesheet", "enum_alarm", PANEL_MAIN, BTN_MARKET);

		textPut("text", bncs_string("ERROR Main|Cfg File"), PANEL_MAIN, BTN_MARKET);

		debug("ERROR::Vob_assignment::initWorkspaceGroups()::ERROR Cannot find %1", m_sFile_Market_Config);
	}



}


void Vob_assignment_base::populatePopupMarketPanel()
{

	int iControlId = 1;

	bncs_string sText = "";

	// Get number of buttons on the popup panel

	const bncs_stringlist slstButtons = getIdList(PANEL_POPUP_MARKET,"group_");
	const int iButtonCount = slstButtons.count();

	for (auto& m_groupWorkspace : m_groupWorkspaces)
	{
		bncs_string control = bncs_string("group_%1").arg(iControlId, '0', 2);

		controlEnable(PANEL_POPUP_MARKET, control);

		sText = m_groupWorkspace.first;

		textPut("text", sText, PANEL_POPUP_MARKET, control);
		iControlId++;
	}

	for (int i = iControlId; i <= iButtonCount; i++)
	{
		bncs_string control = bncs_string("group_%1").arg(i, '0', 2);

		controlDisable(PANEL_POPUP_MARKET, control);
	}
}


void Vob_assignment_base::showTabs(const bncs_string& sTab)
{

	for (int controlItem = 1; controlItem <= MAX_TABS; controlItem++)
	{
		controlHide(PANEL_MAIN, bncs_string("tab_%1_button").arg(controlItem));
		controlHide(PANEL_MAIN, bncs_string("tab_%1_selected").arg(controlItem));
	}


// If group == -1, then first time run up, so disable all workspace buttons

	if ((sTab == INIT) || (sTab == ""))
	{

		controlShow(PANEL_MAIN, "Label");
	}
	else
	{

		// Show workspace names on the grid

		controlEnable(PANEL_MAIN, "vob_1");
		controlEnable(PANEL_MAIN, "vob_2");
		controlEnable(PANEL_MAIN, "vob_3");

		controlHide(PANEL_MAIN, "Label");

		int iControlId = 1;

		if (m_groupWorkspaces.find(sTab) != m_groupWorkspaces.end())
		{
			workspace_group * groupWorkSpace = m_groupWorkspaces[sTab];

			for (auto ITworkspace = groupWorkSpace->vec_workspace.begin(); ITworkspace != groupWorkSpace->vec_workspace.end(); ++ITworkspace)
			{

				bncs_string workspaceName = (*ITworkspace)->_WorkspaceName;

				controlShow(PANEL_MAIN, bncs_string("tab_%1_button").arg(iControlId));

				// Adust the workspace name to display on the button
				workspaceName.replace(" VOB", "|VOB");

				// Adust the workspace name ID count
				if (workspaceName == "")
				{
					textPut("text", "---", PANEL_MAIN, bncs_string("tab_%1_button").arg(iControlId));
				}
				else
				{
					bncs_stringlist workSpaceCaptionSplit(workspaceName, ' ');
					const int wsCaptionIndex = workSpaceCaptionSplit[workSpaceCaptionSplit.count() - 1].toInt();

					if (wsCaptionIndex > 0)
					{
						textPut("text", bncs_string("%1-%2").arg(workspaceName).arg(wsCaptionIndex + 2), PANEL_MAIN, bncs_string("tab_%1_button").arg(iControlId));
					}
					else
					{
						textPut("text", bncs_string("%1").arg(workspaceName), PANEL_MAIN, bncs_string("tab_%1_button").arg(iControlId));
					}
				}

				iControlId++;

				++ITworkspace;
				if (ITworkspace == m_groupWorkspaces[sTab]->vec_workspace.end()) { break; }

				++ITworkspace;
				if (ITworkspace == m_groupWorkspaces[sTab]->vec_workspace.end()) { break; }
			}
		}
		else
		{
			debug("\nERROR::Vob_assignment::showGroup()::Group not found=%1", sTab);
		}
	}
}


workspace* Vob_assignment_base::getWorkspaceGroup(const bncs_string& s_workspace)
{
	for (auto& m_groupWorkspace : m_groupWorkspaces)
	{
		workspace* sRet = m_groupWorkspace.second->getWorkSpaceGroup(s_workspace);
		
		if (sRet != nullptr)
		{
			return sRet;
		}
	}

	return nullptr;


}

void Vob_assignment_base::setButtonCaption(const bncs_string& sCaption)
{

	if (m_bInMcr)
	{
		textPut("style", "button", PANEL_MAIN, BTN_MARKET);

		if (sCaption.length() == 0)
		{
			textPut("text", "Select|Market", PANEL_MAIN, BTN_MARKET);
			textPut("statesheet", "dest_deselected", PANEL_MAIN, BTN_MARKET);
		}
	}
	else
	{
		textPut("style", "label", PANEL_MAIN, BTN_MARKET);

		if (sCaption.length() == 0)
		{
			textPut("statesheet", "enum_alarm", PANEL_MAIN, BTN_MARKET);
		}
		else
		{
			textPut("statesheet", "dest_deselected", PANEL_MAIN, BTN_MARKET);
		}
	}

	if (sCaption.length() != 0)
	{
		textPut("text", sCaption, PANEL_MAIN, BTN_MARKET);
	}

}

bool Vob_assignment_base::workspaceExists(const bncs_string& sWorkspace)
{
	bncs_config cId(bncs_string("workspace_resources/%1").arg(sWorkspace));
	if (cId.isValid())
	{
		return true;
	}
	return false;

}


void Vob_assignment_base::resetGroupWorkspaceLive(const bncs_string& sWorkSpace, const bncs_string& sRoutedSource,
                                                  const bncs_string&
                                                  sScoopVobAPOutputLanguageTag)
{
	// Clear all the lives that are set using this sRoutedPackage, except the workspace that has just been set to live

	for (auto& ITVob_Data : m_Vob_Data)
	{
		// We need to find any VObs that are set to the same routed SourcePackage as the passed one
		//
		if (ITVob_Data.second->Vob_in_01_routedSource() == sRoutedSource.toInt())
		{
				
			// Ignore the one that passed the LIVE set notify that triggered this call
			if (ITVob_Data.second->WorkSpace() != sWorkSpace)
			{
				if ((ITVob_Data.second->Vob_in_01_dest() > -1) && (ITVob_Data.second->Vob_out_01_dest() > -1))
				{
					// Get the AP's related to Vob 1 Input
					bncs_string sVob_01_In_APTagIndexes = "";

					routerName(m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, sRoutedSource.toInt(), sVob_01_In_APTagIndexes);

					bncs_stringlist slstSource_01_in_TagIndexes(sVob_01_In_APTagIndexes, '|');

					debug("\nVob_assignment_base::resetGroupWorkspaceLive:: PackageIndex = %1, sVob_01_In_APTagIndexes => %2", sRoutedSource, sVob_01_In_APTagIndexes);

					// Scan the available AudioPackage to find the one that matches the ScoopVobOutput
					// Then check to see if it has the same Language set
					// If so remove from the SourcePackage AP Entries
					for (auto ITentry = slstSource_01_in_TagIndexes.begin(); ITentry != slstSource_01_in_TagIndexes.end(); ITentry++)
					{
						if (getAPLanguage(*ITentry) == sScoopVobAPOutputLanguageTag)
						{
							// Match so remove the ScoopVobOutput AP from the source Package language AP list
							// The revertive generated will turn the LIVE button off if it is on !
							const int iPos = slstSource_01_in_TagIndexes.find(ITVob_Data.second->Vob_out_01_ScoopVobAPSelectedIndex());
							if (iPos > -1)
							{
								// Exists so remove it - set to "";
								slstSource_01_in_TagIndexes[iPos] = "0";

								debug("%1::%2::%2", m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, slstSource_01_in_TagIndexes.toString('|'));

								routerModify(m_iPackagerRouterMainDevice, DATABASE_SOURCE_AP, sRoutedSource, slstSource_01_in_TagIndexes.toString('|'), true);

							}
						}
					}
				}
				else
				{
					// Match - ignore this one
					// This is the that has just gone Live !
				}
			}
		}
	}
}

bncs_string Vob_assignment_base::getAPLanguage(const int& iSourceAP)
{
	bncs_string sLang = "0";
	
	if (iSourceAP != 0)
	{
		// Get the language for the APSource
		bncs_string sAudioPackageTags;
		routerName(m_iPackagerRouterMainDevice, DATABASE_AP_TAGS, iSourceAP, sAudioPackageTags);

		bncs_stringlist slstAudioPackageTags(sAudioPackageTags);
		sLang = slstAudioPackageTags.getNamedParam("language_tag").toInt();

		if (sLang == NULL)
		{
			debug("\nError::Vob_assignment_base::getAPLanguage SourceAP index=%1, No Language Set in %2.DB63", iSourceAP, m_iPackagerRouterMainDevice, DATABASE_AP_TAGS);
			return "0";
		}
		if (sLang == "")
		{
			debug("\nError::Vob_assignment_base::getAPLanguage SourceAP index=%1, No Language Set in %2.DB63", iSourceAP, m_iPackagerRouterMainDevice, DATABASE_AP_TAGS);
			return "0";
		}
	}
	
	return sLang;
}

