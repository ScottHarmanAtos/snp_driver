﻿#include "workspace_group.h"

#include <bncs_string.h>

workspace::workspace(const bncs_string& sWorkspaceId, const bncs_string& sWorkSpaceInstance, const bncs_string&
	sWorkSpaceName, const bncs_string& sWorkspaceGroup)
{
	_WorkspaceId = sWorkspaceId;
	_WorkspaceInstance = sWorkSpaceInstance;
	_WorkspaceName = sWorkSpaceName;
	_WorkspaceGroup = sWorkspaceGroup;
}

// destructor - equivalent to ApplCore CLOSEDOWN
workspace::~workspace()
{
}

Vob::Vob(const bncs_string& sWorkspace)
{

	_Vob_in_01_dest = -1;
	_Vob_in_01_routed_source = -1;
	_Vob_in_01_infoDriverDevice = -1;
	_Vob_in_01_infoDriverSlot = -1;
	_Vob_in_01_APLanguage = -1;

	_Vob_out_01_dest = -1;
	_Vob_out_01_routed_source = -1;
	_Vob_out_01_infoDriverDevice = -1;
	_Vob_out_01_infoDriverSlot = -1;
	_Vob_out_01_APLanguage = -1;

	_Vob_out_01_ScoopVobAPSelectedIndex = -1;

	_WorkSpace = sWorkspace;

}

// destructor - equivalent to ApplCore CLOSEDOWN
Vob::~Vob()
{
}

//
//---------------
//
void Vob::Vob_in_01_dest(const bncs_string& sDest)
{
	_Vob_in_01_dest = sDest.toInt();
}

int Vob::Vob_in_01_dest() const
{
	return _Vob_in_01_dest;
}

//
//---------------
//
void Vob::Vob_in_01_routedSource(const bncs_string& sSource)
{
	_Vob_in_01_routed_source = sSource.toInt();
}
int Vob::Vob_in_01_routedSource() const
{
	return _Vob_in_01_routed_source;
}
//
//---------------
//
void Vob::Vob_in_01_infoDriverDevice(const bncs_string& sDevice)
{
	_Vob_in_01_infoDriverDevice = sDevice.toInt();
}
int Vob::Vob_in_01_infoDriverDevice() const
{
	return _Vob_in_01_infoDriverDevice;
}
//
//---------------
//
void Vob::Vob_in_01_infoDriverSlot(const bncs_string& sSlot)
{
	_Vob_in_01_infoDriverSlot = sSlot.toInt();
}
int Vob::Vob_in_01_infoDriverSlot() const
{
	return _Vob_in_01_infoDriverSlot;
}
//
//---------------
//
void Vob::Vob_in_01_APLanguage(const bncs_string& sLanguage)
{
	_Vob_in_01_APLanguage = sLanguage.toInt();
}
int Vob::Vob_in_01_APLanguage() const
{
	return _Vob_in_01_APLanguage;
}
//
//---------------
//
//
//---------------
//
void Vob::Vob_out_01_dest(const bncs_string& sDest)
{
	_Vob_out_01_dest = sDest.toInt();
}

int Vob::Vob_out_01_dest() const
{
	return _Vob_out_01_dest;
}
//
//---------------
//
void Vob::Vob_out_01_routedSource(const bncs_string& sSource)
{
	_Vob_out_01_routed_source = sSource.toInt();
}
int Vob::Vob_out_01_routedSource() const
{
	return _Vob_out_01_routed_source;
}
//
//---------------
//
void Vob::Vob_out_01_infoDriverDevice(const bncs_string& sDevice)
{
	_Vob_out_01_infoDriverDevice = sDevice.toInt();
}
int Vob::Vob_out_01_infoDriverDevice() const
{
	return _Vob_out_01_infoDriverDevice;
}
//
//---------------
//
void Vob::Vob_out_01_infoDriverSlot(const bncs_string& sSlot)
{
	_Vob_out_01_infoDriverSlot = sSlot.toInt();
}
int Vob::Vob_out_01_infoDriverSlot() const
{
	return _Vob_out_01_infoDriverSlot;
}
//
//---------------
//
void Vob::Vob_out_01_APLanguage(const bncs_string& sLanguage)
{
	_Vob_out_01_APLanguage = sLanguage.toInt();
}
int Vob::Vob_out_01_APLanguage() const
{
	return _Vob_out_01_APLanguage;
}
//
//---------------
//
void Vob::Vob_out_01_ScoopVobAPSelectedIndex(const bncs_string& sIndex)
{
	_Vob_out_01_ScoopVobAPSelectedIndex = sIndex.toInt();
}

int Vob::Vob_out_01_ScoopVobAPSelectedIndex() const
{
	return _Vob_out_01_ScoopVobAPSelectedIndex;
}


void Vob::WorkSpace(const bncs_string& sWorkspace)
{
	_WorkSpace = sWorkspace;
}
bncs_string Vob::WorkSpace() const
{
	return _WorkSpace;
}



workspace_group::workspace_group()
{
	sGroupButtonName = "";
	sGroupId = "";
	sGroupLiveToggleEnabled = "false";
	sGroupAudioOutputEditEnabled = "false";
	vec_workspace.clear();

}

// destructor - equivalent to ApplCore CLOSEDOWN
workspace_group::~workspace_group()
{
}

void workspace_group::addWorkSpace(const bncs_string &sWorkspaceId, const bncs_string &sWorkSpaceInstance, const bncs_string &sWorkSpaceName, const bncs_string
                                   & sWorkspaceGroup)
{
	auto* clsWorkpace = new workspace(sWorkspaceId, sWorkSpaceInstance, sWorkSpaceName, sWorkspaceGroup);
		
	vec_workspace.push_back(clsWorkpace);

}

// Now we have a full list of all 30 indexes from market_Vob_Ins.xlm for a specific Market
// Starting from the end remove the unwanted empty entries
// Stop as soon as a valid entry is found.
// This leaves the valid entries with blank entries in between them !!
void workspace_group::removeUnusedWorkSpaceTrailingEntries()
{
	while (vec_workspace.back()->_WorkspaceName == "--")
	{
		vec_workspace.pop_back();
	}

}

bncs_string workspace_group::getWorkspaceName(const int iSettingID)
{
	//bncs_string sFormattedSettingID = bncs_string(iSettingID, '0', 2, 10);

	return vec_workspace[iSettingID-1]->_WorkspaceName;
}

bncs_string workspace_group::getWorkspaceInstance(const int iSettingID)
{
	//bncs_string sFormattedSettingID = bncs_string(iSettingID, '0', 2, 10);

	return vec_workspace[iSettingID - 1]->_WorkspaceInstance;
}


int workspace_group::getWorkSpaceCount() const
{
	return static_cast<int>(vec_workspace.size());
}

workspace * workspace_group::getWorkSpaceGroup(const bncs_string& sWorkspace)
{
	for (auto& vec_ITworkspace : vec_workspace)
	{
		if (vec_ITworkspace->_WorkspaceInstance == sWorkspace)
		{
			return vec_ITworkspace;
		}
	}
	return nullptr;
}



