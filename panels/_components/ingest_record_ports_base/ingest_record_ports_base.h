#ifndef ingest_record_ports_base_INCLUDED
	#define ingest_record_ports_base_INCLUDED

#include <bncs_script_helper.h>
#include "TabData.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
#define MAX_TABS	10
class ingest_record_ports_base : public bncs_script_helper
{
public:
	ingest_record_ports_base( bncs_client_callback * parent, const char* path );
	virtual ~ingest_record_ports_base();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;
	bncs_string m_workspaceId;
	bncs_string m_title;

	int m_iSelectedTab;
	TabData * m_pSelectedData;
	TabData m_sTab[MAX_TABS+1];	// Set to 11 but only 10- entries - entry [0] is unused

	void initWorkspaceFunction(void);
	void updateMonitors(void);
	void setMonitor(bncs_string sMonitorId);

	void selectTab(int iSelectedTab);
	void updateButtonCaptions(void);

	void passToAllDests(bncs_string sCommand, bncs_string sValue);
	void passToAllPreselects(bncs_string sCommand, bncs_string sValue);
	void passToAllPreselectMonitorSources(bncs_string sCommand, bncs_string sValue);


};


#endif // ingest_record_ports_base_INCLUDED
