#ifndef ingest_rp_INCLUDED
	#define ingest_rp_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 



class ingest_rp : public bncs_script_helper
{
public:
	ingest_rp( bncs_client_callback * parent, const char* path );
	virtual ~ingest_rp();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_sInstance;
	bncs_string m_sWorkspaceId;
	bncs_string m_sSection;
	bncs_stringlist slstFunctionIndexes;
	bncs_string m_sTitle;
	int iPage;

	// Each page has an offset of 16
	int iPageOffset;
	

	void init(void);

	void initWorkspaceFunction(void);

	void updateDestsFunction(void);

	void updatePageButtons(void);
	int pageCount(void) const;

	void passThroughAllDests(bncs_string sCommand, bncs_string sValue);
	void disableDestsAndPageButtons(void);
	
};






#endif // ingest_rp_INCLUDED