#pragma once
#include <bncs_string.h>
#include <bncs_stringlist.h>

#define MAX_DESTS			16
#define MAX_PAGE_BUTTONS	24

class TabData
{
public:
	TabData();
	virtual ~TabData();

	bncs_string getFormattedEntry(void);
	void setFormattedEntry(bncs_string);

public:
	bncs_stringlist slstFunctionIndexes;

	bncs_string tabName;
	bncs_string function;
	bool isValid(void);
	int getPagesCount();
		

private:


};