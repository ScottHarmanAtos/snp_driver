#include "TabData.h"


TabData::TabData()
{
	tabName = "--";
	function = "-1";
	slstFunctionIndexes = "";
}

TabData::~TabData()
{

}

bool TabData::isValid(void)
{

	if (tabName == "--" || function == "-1") return false;

	return true;
}

bncs_string TabData::getFormattedEntry(void)
{
	return bncs_string("%1.%2").arg(tabName).arg(function);
}

void TabData::setFormattedEntry(bncs_string sEntry)
{

	bncs_stringlist slstValue = bncs_stringlist(sEntry, '.');

	if (slstValue.count() > 0)
	{
		tabName = slstValue[0];
		if (tabName.length() == 0)
		{
			tabName = "--";
		}
	}
	
	if (slstValue.count() > 1)
	{
		function = slstValue[1];
		if (function.length() == 0)
		{
			function = "-1";
		}
	}

}

int TabData::getPagesCount()
{
	// Get the number of entries, and calculate the pages used

	int iPageCount = slstFunctionIndexes.count() / MAX_DESTS;
	int iPageCountRemainder = slstFunctionIndexes.count() % MAX_DESTS;

	if (iPageCountRemainder > 0)
	{
		return iPageCount + 1;
	}
	else
	{
		return iPageCount;
	}
}