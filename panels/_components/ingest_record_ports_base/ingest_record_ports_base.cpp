#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "ingest_record_ports_base.h"

#define PANEL_MAIN	1

#define TIMER_SETUP	1


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( ingest_record_ports_base )

// constructor - equivalent to ApplCore STARTUP
ingest_record_ports_base::ingest_record_ports_base( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file ingest_record_ports_base.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PANEL_MAIN, "ingest_record_ports_base.bncs_ui");

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
	m_workspaceId = "";
	m_iSelectedTab = 1; 
	m_title = "";

//	setSize( 1024,668 );		// set the size explicitly
//	setSize( PANEL_MAIN );		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
ingest_record_ports_base::~ingest_record_ports_base()
{
}

// all button pushes and notifications come here
void ingest_record_ports_base::buttonCallback(buttonNotify *b)
{
	if (b->panel() == PANEL_MAIN)
	{
		if (b->id().startsWith("tab_") && b->id().endsWith("_button"))
		{
			m_iSelectedTab = bncs_stringlist(b->id(), '_')[1].toInt();

			selectTab(m_iSelectedTab);

		}
		else if (b->id().startsWith("dest_"))
		{

			if (b->id() == "dest_rp" && b->command() == "page_change")
			{
				// Special individual handling

				// Deselect Main Grid and monitors
				passToAllDests("deselect", "deselect");
			}
			else if (b->id().startsWith("dest_mon_") && b->command() == "button" && b->value() == "released")
			{
				// Special individual handling

				// Deselect Main Grid
				textPut("deselect", "deselect", PANEL_MAIN, "dest_rp");
			}
			else if (b->command() == "dest")
			{

				// Show the source grid popup 
				textPut("show_popup", b->value(), PANEL_MAIN, "popup_source_grid");

				// Pass value to all dests - mutual selection
				passToAllDests("dest", b->value());
			}
			else if (b->command() == "completed")
			{
				passToAllDests("preselect", "none");
				passToAllPreselects("deselect", "deselect");

			}
			else if (b->command() == "monitor_source_dest")
			{
				passToAllPreselectMonitorSources(b->command(), b->value());
			}
		}
		else if (b->id() == "workspace_resources")
		{
			// Pass to all dests
			passToAllDests(b->command(), b->value());

			// Pass to popup control
			textPut(b->command(), b->value(), PANEL_MAIN, "popup_source_grid");

			initWorkspaceFunction();

			updateButtonCaptions();

			// set default
			m_iSelectedTab = 1;

			// Set for first tab that has valid data
			for (int i = 1; i <= MAX_TABS; i++)
			{
				if( m_sTab[i - 1].isValid())
				{
					m_iSelectedTab = i;
					break;
				}
			}

			// Deselect Main Grid and monitors as we are just starting
			passToAllDests("disable", "disable");


			selectTab(m_iSelectedTab);

			updateMonitors();


		}
		else if (b->id() == "popup_source_grid")
		{
			// Pass to all dests and popup source grid
			if (b->command() == "source_index")
			{
				passToAllDests("take", b->value());

			}
		}
		else if (b->id().startsWith("route_monitor_"))
		{
			// Pass to all dests and popup source grid
			passToAllDests("monitor", b->value());
		}
		else if (b->id().startsWith("preselect_"))
		{

			if (b->id().startsWith("preselect_mon_"))
			{
				bncs_stringlist slstSplit(b->id(), '_');

				if (b->command() == "source_index")
				{
					// Route source from Dest to selected monitor
					textPut("take", b->value(), PANEL_MAIN, bncs_string("dest_mon_%1").arg(slstSplit[2]));
				}
			}

			// Pass all to all dests 
			passToAllDests(b->command(), b->value());

			

			if (b->command() == "preselect")
			{
				// Pass all preselect modes to all preselect controls
				// clears all other ones
				passToAllPreselects(b->command(), b->value());
			
			}

			else if (b->command() == "deselect")
			{
				// Pass all preselect modes to all preselect controls
				// clears all other ones
				passToAllPreselects(b->command(), "deselect");

			}

		}
	}
}

// all revertives come here
int ingest_record_ports_base::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void ingest_record_ports_base::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string ingest_record_ports_base::parentCallback( parentNotify *p )
{

//	p->dump("\ningest_record_ports_base::parentCallback");

	if( p->command() == "return" )
	{
		if( p->value() == "all" )
		{
			bncs_stringlist sl;
			
			for (int i = 0; i < MAX_TABS; i++)
			{
				//debug(bncs_string("\n 1 command=%1, value =%2").arg(p->command()).arg(m_sTab[i].Name));
				//debug(bncs_string("\n 1 command=%1, value =%2").arg(p->command()).arg(m_sTab[i].Section));
				sl << bncs_string("tab_name_%1=%2").arg(i+1).arg(m_sTab[i].Name);
				sl << bncs_string("tab_section_%1=%2").arg(i+1).arg(m_sTab[i].Section);
			}

			sl << bncs_string("workspace=%1").arg(m_workspaceId);
			sl << bncs_string("title=%1").arg(m_title);

			return sl.toString( '\n' );
		}

		else if( p->value().startsWith("tab_name_"))
		{
			// Get the index
			bncs_stringlist slstValue = bncs_stringlist(p->value(), '_');
			const int iKey = slstValue[2].toInt();

			return(bncs_string("%1=%2").arg(p->value()).arg(m_sTab[iKey-1].Name));
		}
		else if (p->value().startsWith("tab_section_"))
		{
			// Get the index
			bncs_stringlist slstValue = bncs_stringlist(p->value(), '_');
			const int iKey = slstValue[2].toInt();

			return(bncs_string("%1=%2").arg(p->value()).arg(m_sTab[iKey - 1].Section));
		}
		else if (p->value().startsWith("workspace"))
		{
			return(bncs_string("%1=%2").arg(p->value()).arg(m_workspaceId));
		}
		else if (p->value().startsWith("title"))
		{
			return(bncs_string("%1=%2").arg(p->value()).arg(m_title));
		}

	}
	else if( p->command() == "instance" && p->value() != m_instance )
	{
		m_instance = p->value();
	}

	else if (p->command().startsWith("tab_name_"))
	{
		// Get the index
		bncs_stringlist slstValue = bncs_stringlist(p->command(), '_');
		const int iKey = slstValue[2].toInt();

		// Do not repeat action
		if (m_sTab[iKey - 1].Name != p->value())
		{
			m_sTab[iKey - 1].Name = p->value();
		}

		textPut("text", m_sTab[iKey - 1].Name, PANEL_MAIN, bncs_string("tab_%1_button").arg(iKey));

	}
	else if (p->command().startsWith("tab_section_"))
	{
		// Get the index
		bncs_stringlist slstValue = bncs_stringlist(p->command(), '_');
		const int iKey = slstValue[2].toInt();

		// Do not repeat action
		if (m_sTab[iKey - 1].Section != p->value())
		{
			m_sTab[iKey - 1].Section = p->value();
		}
	}

	else if (p->command() == "title")
	{
		// Do not repeat action
		if (m_title != p->value())
		{
			m_title = p->value();

			// Pass this straight through to all the ingest_rp_component
			textPut("title", m_title, PANEL_MAIN, "dest_rp");
		}
	}
	else if (p->command() == "workspace")
	{

		// Do not repeat action
		if (m_workspaceId != p->value())
		{
			m_workspaceId = p->value();

			// Pass it to the workspace_resources control
			textPut("workspace", m_workspaceId, PANEL_MAIN, "workspace_resources");
		}
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if( p->command() == "_events" )
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "notify=*";		
		
		return sl.toString( '\n' );
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if( p->command() == "_commands" )
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;
		
		sl << "myParam=[value]";
		
		return sl.toString( '\n' );
	}

	return "";
}

// timer events come here
void ingest_record_ports_base::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void ingest_record_ports_base::initWorkspaceFunction(void)
{
	
	// Load in the function indexes for each tab - as they will have diff function name entries


	for (int i = 0; i < MAX_TABS; i++)
	{

		if (m_sTab[i].isValid())
		{
			//debug(bncs_string("\nWorkspace_resources/%1.%2").arg(m_workspaceId).arg(m_sTab[i].Section));

			bncs_config c(bncs_string("workspace_resources/%1.%2").arg(m_workspaceId).arg(m_sTab[i].Section));
			bool init = false;

			m_sTab[i].slstFunctionIndexes = "";

			if (c.isValid())
			{
				while (c.isChildValid())
				{
					bncs_string id = c.childAttr("id");
					bncs_string value = c.childAttr("value");
					init = true;

					//debug("ingest_record_ports_base::initWorkspaceFunction()::Tab %1 %2=%3", i, id, value);

					// Pull out and store all the entries for this function
					m_sTab[i].slstFunctionIndexes << bncs_string("%1").arg(id);
					c.nextChild();
				}
			}

			//Workspace function not found
			if (init == false)
			{
				//		textPut("text", bncs_string("%1|Not Found").arg(m_function), PNL_MAIN, BTN_DEST);
			}
		}
	}
}

void ingest_record_ports_base::updateMonitors(void)
{

	// Hide the monitors that cannot be used depending on the workspace
	// Do this by checking to see if they have been allocated a function


	setMonitor("dest_25");
	setMonitor("dest_26");
	setMonitor("dest_27");
	setMonitor("dest_28");
}

void ingest_record_ports_base::setMonitor(const bncs_string sMonitorId)
{
	bncs_string sRet = "";
	textGet("function", PANEL_MAIN, sMonitorId, sRet);

	
	if (sRet == "")
	{
		controlHide(PANEL_MAIN, bncs_string(sMonitorId));
	}
	else
	{
		controlShow(PANEL_MAIN, bncs_string(sMonitorId));
	}
}

void ingest_record_ports_base::updateButtonCaptions(void)
{
	for (int i = 1; i <= MAX_TABS; i++)
	{
		textPut("text", m_sTab[i - 1].Name, PANEL_MAIN, bncs_string("tab_%1_button").arg(i));
	}
}

void ingest_record_ports_base::selectTab(int iSelectedTab)
{
	// Hide all tab selections
	for (int i = 1; i <= MAX_TABS; i++)
	{
		controlHide(PANEL_MAIN, bncs_string("tab_%1_selected").arg(i));
	}


	m_pSelectedData = &m_sTab[iSelectedTab-1]; // tabs are 1-> MAX, array is 0->MAX - 1

	// Pass the selected section to the rp destination component
	textPut("section", m_pSelectedData->Section, PANEL_MAIN, "dest_rp");

	// show selected tab
	controlShow(PANEL_MAIN, bncs_string("tab_%1_selected").arg(iSelectedTab));

	// Deselect Main Grid and monitors as we are on a new page
	passToAllDests("deselect", "deselect");


}

void ingest_record_ports_base::passToAllDests(const bncs_string sCommand, const bncs_string sValue)
{
	// Find all the controls starting with dest_

	bncs_stringlist slstList = getIdList(PANEL_MAIN, "dest_");

	for (bncs_stringlist::iterator slstIT = slstList.begin(); slstIT != slstList.end(); slstIT++)
	{
		//debug(bncs_string("%1=%2 => dest = %3").arg(sCommand).arg(sValue).arg(*slstIT));

		textPut(sCommand, sValue, PANEL_MAIN, *slstIT);
	}

}

void ingest_record_ports_base::passToAllPreselects(const bncs_string sCommand, const bncs_string sValue)
{
	// Find all the controls starting with preselect_

	bncs_stringlist slstList = getIdList(PANEL_MAIN, "preselect_");

	for (bncs_stringlist::iterator slstIT = slstList.begin(); slstIT != slstList.end(); slstIT++)
	{
		//debug(bncs_string("%1=%2 => preselect = %3").arg(sCommand).arg(sValue).arg(*slstIT));

		textPut(sCommand, sValue, PANEL_MAIN, *slstIT);
	}

}

void ingest_record_ports_base::passToAllPreselectMonitorSources(const bncs_string sCommand, const bncs_string sValue)
{
	// Find all the controls starting with preselect_mon_

	bncs_stringlist slstList = getIdList(PANEL_MAIN, "preselect_mon_");

	for (bncs_stringlist::iterator slstIT = slstList.begin(); slstIT != slstList.end(); slstIT++)
	{
		//debug(bncs_string("%1=%2 => preselect = %3").arg(sCommand).arg(sValue).arg(*slstIT));

		textPut(sCommand, sValue, PANEL_MAIN, *slstIT);
	}

}
