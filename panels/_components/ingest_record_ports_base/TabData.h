#pragma once
#include <bncs_string.h>
#include <bncs_stringlist.h>

#define MAX_DESTS			16
#define MAX_PAGE_BUTTONS	24

class TabData
{
public:
	TabData();
	virtual ~TabData();

	bncs_string Name;
	bncs_string Section;
	bncs_stringlist slstFunctionIndexes;

	bool isValid(void) const;
		

private:


};