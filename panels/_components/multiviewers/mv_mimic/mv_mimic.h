#ifndef mv_mimic_INCLUDED
	#define mv_mimic_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class mv_mimic : public bncs_script_helper
{
public:
	mv_mimic( bncs_client_callback * parent, const char* path );
	virtual ~mv_mimic();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void httpCallback(httpNotify *p);
	
private:
	void reloadPanel();
	void initialise();

	bncs_string panel;

	bncs_string driverUrl;
	bncs_string driverPort;

	bncs_string outputLayout;

	bncs_string outputID;

	bncs_string instance;

	bncs_string outputGroup;
};


#endif // mv_mimic_INCLUDED