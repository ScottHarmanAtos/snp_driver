#pragma once
#include "jsonParse.h"
#include "tile.h"
#include <list>

class mv_layout :
	public JsonParse
{
public:
	mv_layout();
	mv_layout(bncs_string& data);
	~mv_layout();

	bncs_string uuid;

	bncs_string label;

	bncs_string resolution;

	list<tile> tiles;

	void GetValues(Json::Value& v);
};



