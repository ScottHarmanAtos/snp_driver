#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "mv_mimic.h"
#include "mv_layout.h"

#define PNL_MAIN	1

#define TIMER_SETUP	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( mv_mimic )

// constructor - equivalent to ApplCore STARTUP
mv_mimic::mv_mimic( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	panel = "main.bncs_ui";

	instance = "";
	outputGroup = "";
	outputID = "";
	outputLayout = "";

	driverUrl = "";
	driverPort = "";

	panelShow(PNL_MAIN, "main.bncs_ui");
}

// destructor - equivalent to ApplCore CLOSEDOWN
mv_mimic::~mv_mimic()
{

}

// all button pushes and notifications come here
void mv_mimic::buttonCallback( buttonNotify *b )
{

}

// all revertives come here
int mv_mimic::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void mv_mimic::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string mv_mimic::parentCallback( parentNotify *p )
{
	debug("mv_mimic::parentCallback() command=%1 value=%2", p->command(), p->value());
	if (p->command() == "return")
	{
		bncs_stringlist sl;
		
		sl << bncs_string("groupParam=%1").arg(outputGroup);

		return sl.toString('\n');
	}
	else if (p->command() == "groupParam")
	{
		outputGroup = p->value();

		initialise();
	}
	else if (p->command() == "instance")
	{
		instance = p->value();

		initialise();
	}
	else if (p->command() == "_events")
	{
		bncs_stringlist sl;

		sl << "zoom=in";
		sl << "zoom=out";
		sl << "zoom=reset";
		sl << "scroll=left";
		sl << "scroll=right";
	}

	return 0;
}

// timer events come here
void mv_mimic::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_SETUP:
		timerStop(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void mv_mimic::reloadPanel()
{
	panelDestroy(PNL_MAIN);
	panelShow(PNL_MAIN, panel);
}

void mv_mimic::httpCallback(httpNotify *h)
{
	//Received ok
	if (!h->ok() || h->data() == "null")
	{
		debug("mv_mimic::httpCallback ERROR Receiving data: Error:%1", h->err());

		controlHide(PNL_MAIN, "dummy_background");

		return;
	}

	controlHide(PNL_MAIN, "dummy_background");
	controlHide(PNL_MAIN, "error_background");

	mv_layout mv_layout(h->data());	

	if (mv_layout.tiles.size() > 0)
	{
		controlHide(PNL_MAIN, "dummy_background");
		controlHide(PNL_MAIN, "error_background");

		for (list<tile>::iterator p = mv_layout.tiles.begin(); p != mv_layout.tiles.end(); p++)
		{
			if (p->content_type == "Clock.tile")
			{
				controlCreate(PNL_MAIN, bncs_string("%1").arg(p->destination), "bncs_control", p->x_axis / 4, p->y_axis / 4 + 35, p->width / 4, p->height / 4, bncs_string("scaleWithCanvas=true\nstyle=label\nstylesheet=mv_default\ntext=%1\ncolour.background=%2").arg("Clock").arg("purple"));
			}
			else
			{
				controlCreate(PNL_MAIN, bncs_string("%1").arg(p->destination), "bncs_control", p->x_axis / 4, p->y_axis / 4 + 35, p->width / 4, p->height / 4, bncs_string("scaleWithCanvas=true\nstyle=label\nstylesheet=mv_default"));
			}
		}

		textPut(bncs_string("text=%1").arg(mv_layout.label), PNL_MAIN, "layoutName");
	}
}

void mv_mimic::initialise()
{
	if (instance.length() > 0 && outputGroup.length() > 0)
	{
		bncs_config cCompInstanceDriver = bncs_config(bncs_string("instances.%1.%2").arg(instance).arg("driver"));
		if (cCompInstanceDriver.isValid())
		{
			bncs_string cDriverInstance = cCompInstanceDriver.attr("instance");

			bncs_config cInstance = bncs_config(bncs_string("instances.%1").arg(cDriverInstance));
			if (cInstance.isValid())
			{
				bncs_config cDriverUrlSetting = bncs_config(bncs_string("instances.%1.%2").arg(cDriverInstance).arg("driver_address"));
				if (cDriverUrlSetting.isValid())
				{
					driverUrl = cDriverUrlSetting.attr("value");
				}

				bncs_config cDriverPortSetting = bncs_config(bncs_string("instances.%1.%2").arg(cDriverInstance).arg("driver_port"));
				if (cDriverPortSetting.isValid())
				{
					driverPort = cDriverPortSetting.attr("value");
				}
			}
		}

		bncs_config cCompInstanceOutputs = bncs_config(bncs_string("instances.%1.%2").arg(instance).arg("outputs"));
		if (cCompInstanceOutputs.isValid())
		{
			bncs_string cOutputsInstance = cCompInstanceOutputs.attr("instance");

			bncs_config cOutputGroup = bncs_config(bncs_string("instances.%1.%2").arg(cOutputsInstance).arg(outputGroup));
			if (cOutputGroup.isValid())
			{
				bncs_string outputInstance = cOutputGroup.attr("instance");

				bncs_config cOutputInstance = bncs_config(bncs_string("instances.%1").arg(outputInstance));
				if (cOutputInstance.isValid())
				{
					bncs_config cOutputIdSetting = bncs_config(bncs_string("instances.%1.%2").arg(outputInstance).arg("id"));
					if (cOutputIdSetting.isValid())
					{
						outputID = cOutputIdSetting.attr("value");
					}
				}
			}
		}

		bncs_string requestUri = bncs_string("http://%1:%2/outputs/%3/layout").arg(driverUrl).arg(driverPort).arg(outputID);

		debug("mv_mimic::initialise() URI:%1", requestUri);

		httpGetString(requestUri);
	}
}