#ifndef router_mv_mon_INCLUDED
	#define router_mv_mon_INCLUDED

#include <bncs_script_helper.h>
#include <list>
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif

enum MonType
{
	TypeMonitor = 1,
	TypeMV = 2
};
 
enum Mode
{
	Monitor = 1,
	Tile = 2,
	Layout = 3,
	NotSet = 4
};


struct TypeInfo
{
	bncs_string CompositeInstance;
	bncs_string Group_1;
	bncs_string Group_2;
	bncs_string Instance_1;
	bncs_string Instance_2;
	MonType Type;
};

class router_mv_mon : public bncs_script_helper
{
public:
	router_mv_mon( bncs_client_callback * parent, const char* path );
	virtual ~router_mv_mon();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	Mode m_mode;	
	Mode m_currentPanel;
	Mode m_behaviour; //This allows use to treat mode and behaviour separatly. E.g In tile mode, without an MV routed, it should behave like a monitor

	bncs_string m_packagerInstance;
	int m_packagerDevice;
	int m_packagerIndex;
	int m_videoDevice;
	bool m_isSelected;
	bool m_ignoreDeselect; //Allows us to ignore a deselect when we sent it.

	int m_packagerSourceIndex;
	int m_videoSourceIndex;

	int m_width;
	int m_height;

	int m_recurse;
	int m_levelsRecursed;

	void init();

	void InitialDisplay();
	void UpdateDisplay(Mode mode);
	void PanelShow(Mode Panel);
	void Deselect();
	void Take(int Index);

	void DisplayTile();
	void DisplayTile(bncs_string Layout, bncs_string Panel, int StartingX, int StartingY, int panelWidth, int panelHeight);

	void Resize(int width, int height);

	bncs_stringlist m_tileList;

	bncs_string m_currentUI;

	bncs_string m_topLevelInstance;

	list<bncs_string> m_displayedTiles;

	TypeInfo GetType();

	map<bncs_string, pair<bncs_string, bncs_string>> m_layouts;
	list<bncs_string> m_layoutTiles;
	bncs_string m_selectedLayout;
};


#endif // router_mv_mon_INCLUDED