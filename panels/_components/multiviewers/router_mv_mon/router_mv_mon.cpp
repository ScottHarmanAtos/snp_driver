#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "router_mv_mon.h"
#include "instanceLookup.h"

#define TIMER_INFO	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(router_mv_mon)

const int DATABASE_SOURCE_NAME(0);
const int DATABASE_SOURCE_TITLE(2);
const int DATABASE_SOURCE_ADJUST(4);
const int DATABASE_DEST_ADJUST(5);
const int DATABASE_SOURCE_INFO(6);
const int DATABASE_DEST_INFO(7);

const int DATABASE_VIP_SOURCE_MV_META(26);

const int DEFAULT_WIDTH(480);
const int DEFAULT_HEIGHT(270);

// constructor - equivalent to ApplCore STARTUP
router_mv_mon::router_mv_mon( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file router_mv_mon.bncs_ui and we'll know it as our panel PNL_MAIN

	m_topLevelInstance = "";
	m_levelsRecursed = 0;
	m_packagerIndex = 0;
	m_mode = NotSet; //Initial Mode
	m_currentPanel = NotSet;
	m_behaviour = NotSet;
	m_isSelected = false;
	m_width = -1;
	m_height = -1;
	m_recurse = -1;
	m_ignoreDeselect = false;
	m_packagerSourceIndex = 0;

	bncs_string videoInstance = bncs_config("packager_config.PACKAGER_AUTO.video_router_hd").attr("value");

	bncs_stringlist split = bncs_stringlist(videoInstance, '=');
	if (split.count() == 2)
	{
		string actualVideoHdInstance = split[1];

		if (actualVideoHdInstance.length() == 0)
			debug("router_mv_mon::router_mv_mon ERROR: failed to get the video HD instance for the packager");
		if (!getDev(actualVideoHdInstance, &m_videoDevice))
		{
			debug("router_mv_mon::router_mv_mon ERROR: failed to get device number for the video instance '%1'", videoInstance);
		}
	}
	else
	{
		debug("router_mv_mon::router_mv_mon ERROR: failed to get the video HD instance for the packager (split failed on =)");
	}	
}

// destructor - equivalent to ApplCore CLOSEDOWN
router_mv_mon::~router_mv_mon()
{
}

// all button pushes and notifications come here
void router_mv_mon::buttonCallback( buttonNotify *b )
{
	b->dump(bncs_string("router_mv_mon::buttonCallback fff panel:%1").arg(b->panel()));
	if (b->panel() == Layout + 10)
	{
		if (b->id() == "list")
		{
			bncs_string x, y;
			controlPosition(Layout + 10, "mimic", x, y);
			int width = 240;
			int height = 150;

			map<bncs_string, pair<bncs_string, bncs_string>>::iterator it = m_layouts.begin();

			advance(it, b->sub(0).toInt());

			std::map<bncs_string, pair<bncs_string, bncs_string>>::iterator iterator;

			for (map<bncs_string, pair<bncs_string, bncs_string>>::iterator iterator = m_layouts.begin(); iterator != m_layouts.end(); iterator++)
			{
				if (it->second.first == b->value())
				{
					m_selectedLayout = it->first;

					break;
				}
			}

			DisplayTile(it->second.second, Layout + 10, x, y, width, height);
		}
		else if (b->id() == "select_cancel")
		{
			panelDestroy(Layout + 10);
		}
		else if (b->id() == "select_ok")
		{
			TypeInfo t = GetType();

			char type;
			int dev;
			int slot;

			getDevSlot(t.Instance_2, "layout_id", &dev, &slot, &type);

			infoWrite(dev, m_selectedLayout, slot);

			m_selectedLayout = "";

			panelDestroy(Layout + 10);

			/*bncs_string controlInstance;
			instanceLookupComposite(t.CompositeInstance, "control", controlInstance);

			bncs_string s1, number;
			t.Group_2.split('_', s1, number);

			int dev, slot;
			char c;
			getDevSlot(controlInstance, bncs_string("Layout Wall 0%1").arg(number.toInt()),&dev,&slot, &c);

			bncs_string address;
			bncs_config con = bncs_config(bncs_string("instances.%1").arg(controlInstance));
			if (con.isValid())
			{
				address = con.attr("rollcall_address");
			}

			//Output1 = 1000
			//Output2 = 1010
			//Output3 = 1020
			//Output4 = 1030

			//@TODO put this back in if we get the RollCall Driver to behave correctly.
			//infoWrite(dev, m_selectedLayout, slot);
			infoWrite(dev, bncs_string("SETPARAM|%1|%2|%3").arg(address).arg(bncs_string(1000 + ((number.toInt() - 1) * 10))).arg(m_selectedLayout), 4000);

			panelDestroy(Layout + 10);*/
		}
	}
	else
	{
		m_ignoreDeselect = true;
		if (m_mode == Layout && b->command() != "layout")
		{
			b->dump("router_mv_mon::buttonCallback");
			if (m_behaviour != Monitor && b->command() == "button" && b->value() == "released")
			{
				debug("router_mv_mon::buttonCallback Clickity Click Click");
				panelPopup(Layout + 10, "layout_popup.bncs_ui");

				for (map<bncs_string, pair<bncs_string, bncs_string>>::iterator it = m_layouts.begin(); it != m_layouts.end(); it++)
				{
					textPut("add", it->second.first, Layout + 10, "list");
				}

				bncs_string sourceName;
				routerName(m_packagerDevice, DATABASE_SOURCE_NAME, m_packagerSourceIndex, sourceName);
				bncs_string sourceNameNoPipe = sourceName.replace('|', ' ');
				textPut("text", sourceNameNoPipe, Layout + 10, "label");
				textPut("selected=next", Layout + 10, "list");

			}
			else if (b->command() == "layouts")
			{
				debug(bncs_string("router_mv_mon::buttonCallback Layouts:%1").arg(b->value()));

				m_layouts.clear();
				bncs_stringlist sl(b->value(), '@');
				for (int i = 0; i < sl.count(); i++)
				{
					bncs_string name, data;
					sl[i].split('#', name, data);

					bncs_string id, geometry;
					data.split('~', id, geometry);

					debug(bncs_string("router_mv_mon::buttonCallback Layouts populating -> name=%1, id=%2, geometry=%3").arg(name).arg(id).arg(geometry));

					m_layouts[id] = pair<bncs_string, bncs_string>(name, geometry);
				}
			}
		}
		else
		{
			switch (m_behaviour)
			{
			case Monitor:
				if (b->command() == "button" && b->value() == "released")
				{
					debug("router_mv_mon::buttonCallback Monitor Pressed");
					textPut("statesheet", "mv_selected", Monitor, "1");
					hostNotify(bncs_string("index.%1=%2").arg(m_packagerInstance).arg(m_packagerIndex));
					m_isSelected = true;
				}
				break;

			case Tile:
				if (b->command() == "layout")//We expect a layout command here
				{
					debug("router_mv_mon::buttonCallback layout:%1", b->value());

					if (b->value().length() > 0)
					{
						textPut("text", "", Tile, "button");
					}

					if (m_mode == Mode::Layout)
					{
						textPut("get_layouts", Tile, "mv_mimic");
					}
					DisplayTile(b->value(),Tile, 0, 0, m_width, m_height);
				}

				else
				{
					//Child Selected
					for (int i = 1; i <= m_tileList.count(); i++)
					{
						textPut("deselect", Tile, m_tileList[i - 1]);
					}

					hostNotify(bncs_string("index.%1=%2").arg(b->sub(0)).arg(b->value()));
				}
				break;

			case Layout:
				break;
			}
		}
	}
}

// all revertives come here
int router_mv_mon::revertiveCallback( revertiveNotify * r )
{
	debug("router_mv_mon::revertiveCallback dev=%1, index=%2, sInfo=%2", r->device(), r->index(), r->sInfo());

	if (r->device() == m_packagerDevice && r->index() == m_packagerIndex)
	{	
		debug("router_mv_mon::revertiveCallback :%1", r->sInfo());
		//We will get the source that is currently routed
		int index = 0;

		if (m_packagerInstance == "packager_router")
		{
			index = bncs_stringlist(r->sInfo()).getNamedParam("index");
		}
		else
		{
			index = r->info();
		}

		debug("router_mv_mon::revertiveCallback m_packagerSourceIndex=%1 index=%2", m_packagerSourceIndex, index);

		if (m_packagerSourceIndex != index) //Don't set if we are already this
		{
			m_packagerSourceIndex = index;
			debug("router_mv_mon::revertiveCallback Set m_packagerSourceIndex:%1", m_packagerSourceIndex);

			//Get the source video index
			bncs_string sourcePackageIndexData;
			routerName(m_packagerDevice, DATABASE_SOURCE_INFO, m_packagerSourceIndex, sourcePackageIndexData);
			//m_videoSourceIndex = bncs_stringlist(sourcePackageIndexData).getNamedParam("hd");

			debug("router_mv_mon::revertiveCallback Set m_packagerSourceIndex=%1 data=%2", m_packagerSourceIndex, sourcePackageIndexData);

			bncs_stringlist splitPipe = bncs_stringlist(sourcePackageIndexData, '|');

			m_videoSourceIndex = splitPipe[1].toInt();			

			UpdateDisplay(m_mode);
		}
	}
	return 0;
}

// all database name changes come back here
void router_mv_mon::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string router_mv_mon::parentCallback( parentNotify *p )
{
	if (p->command() == "instance")
	{
		if (p->value() != m_packagerInstance)
		{
			m_packagerInstance = p->value();
			init();
		}
	}
	else if (p->command() == "index")
	{
		if (p->value() != m_packagerIndex)
		{
			if (m_topLevelInstance.length() == 0 || m_packagerInstance == m_topLevelInstance)
			{
				m_packagerIndex = p->value();
				init();
			}
			else
			{
				//The instance we have is not the same as the routing instance. 
				debug("bncs_string router_mv_mon::parentCallback Cannot route index as the instance is not the same as the top level instance");
			}
		}
	}
	else if (p->command() == "width")
	{
		if (p->value() != m_width)
		{
			int width;
			int i = p->value();
			if (i <= 0)
				width = DEFAULT_WIDTH;
			else
				width = i;
			Resize(width, m_height);
		}
	}
	else if (p->command() == "height")
	{
		if (p->value() != m_height)
		{
			int height;
			int i = p->value();
			if (i <= 0)
				height = DEFAULT_HEIGHT;
			else
				height = i;
			Resize(m_width, height);
		}
	}
	else if (p->command() == "mode")
	{

		bncs_string s = p->value().lower();
		Mode m;
		if (s.startsWith("monitor"))
			m = Monitor;
		else if (s.startsWith("layout"))
			m = Layout;
		else
			m = Tile;

		if (m != m_mode)
		{
			Mode old = m_mode;
			m_mode = m;
			
			if (old == NotSet)
				init(); //Only call init the first time this changes
			else
				InitialDisplay();
		}
	}
	else if (p->command() == "deselect")
	{
		Deselect();
	}
	else if (p->command() == "take")
	{
		if (p->value() != -1)
			Take(p->value());
	}
	else if (p->command() == "recurse")
	{
		int i = p->value().toInt();
		
		if (i != m_recurse)
		{
			m_recurse = i;
			InitialDisplay();
		}
	}
	else if (p->command() == "all")
	{
		bncs_string s = p->value().replace(':', '=');

		bncs_stringlist sl(s, ',');
		m_packagerInstance = sl.getNamedParam("instance");
		m_packagerIndex = sl.getNamedParam("index");
		int width = sl.getNamedParam("width");
		int height = sl.getNamedParam("height");
		m_mode = sl.getNamedParam("mode") == "monitor" ? Monitor : sl.getNamedParam("mode") == "layout" ? Layout : Tile;
		m_recurse = sl.getNamedParam("recurse");
		m_levelsRecursed = sl.getNamedParam("levelsRecursed");
		m_topLevelInstance = sl.getNamedParam("topLevelInstance");
		m_ignoreDeselect = false;
		debug("router_mv_mon::parentCallback ALL m_packagerInstance=%1 m_packagerIndex=%2 width=%3 height=%4 m_mode=%5", m_packagerInstance, m_packagerIndex, width, height, m_mode);

		init();
		Resize(width, height);
		Deselect();
	}
	else if (p->command() == "_events")
	{
		bncs_stringlist sl; 

		sl << bncs_string("index.<instance>=<index>");

		return sl.toString('\n');
	}
	else if (p->command() == "_commands")
	{
		bncs_stringlist sl;

		sl << "mode=<modeName>";
		sl << "deselect=<index>";
		sl << "take=<index>";
		sl << "width=<width>";
		sl << "height=<height>";
		sl << "recurse=<recurse number>";

		return sl.toString('\n');
	}
	else if (p->command() == "return")
	{
		bncs_stringlist sl;

		sl << bncs_string("index=").append(m_packagerIndex);
		sl << bncs_string("width=").append(m_width);
		sl << bncs_string("height=").append(m_height);
		sl << bncs_string("recurse=").append(m_recurse);
		sl << bncs_string("mode=").append(m_mode == Monitor ? "monitor" : m_mode == Layout ? "layout" : "tile");

		return sl.toString('\n');
	}
	else if (p->command() == "info")
	{
		//Destroy first, just incase it is already being shown
		controlDestroy(m_behaviour, "info");
		controlCreate(m_behaviour, "info", "bncs_control", 0, 0, m_width, m_height, bncs_string("scaleWithCanvas=true\nstyle=label\nstylesheet=mv_default"));

		bncs_string packageName;
		routerName(m_packagerDevice, 1, m_packagerIndex, packageName);
		bncs_string packageNameNoPipes = packageName.replace('|', ' ');

		bncs_string packageSourceName;
		routerName(m_packagerDevice, 0, m_packagerSourceIndex, packageSourceName);
		bncs_string packageSourceNameNoPipes = packageSourceName.replace('|', ' ');


		bncs_string s = bncs_string("Package Index: %1|Package Name: %2| |Current Source Index: %3|Source Name: %4")
			.arg(m_packagerIndex).arg(packageNameNoPipes).arg(m_packagerSourceIndex).arg(packageSourceNameNoPipes);


		TypeInfo t = GetType();

		if (t.Type == TypeMV)
		{
			//MV
			bncs_config c(bncs_string("instances.").append(t.Instance_1));
			bncs_string sIP;
			if (c.isValid())
			{
				sIP = c.attr("device_ip");
			}

			c = bncs_config(bncs_string("instances.").append(t.Instance_2));
			bncs_string sId;
			if (c.isValid())
			{
				sId = c.attr("id");
			}

			c = bncs_config(bncs_string("instances.").append(t.Instance_2));
			bncs_string sAltId;
			if (c.isValid())
			{
				sAltId = c.attr("alt_id");
			}

			s = bncs_string("%1| | --MV-- | |Id: %2|Alt ID: %3|IP: %4").arg(s).arg(sId).arg(sAltId).arg(sIP);
		}
		else
		{

		}

		textPut("text",
			s,
			m_behaviour, "info");

		timerStart(TIMER_INFO, 6000);
	}


	return "";
}

// timer events come here
void router_mv_mon::timerCallback( int id )
{
	if (id == TIMER_INFO)
	{
		controlDestroy(m_behaviour, "info");
	}
	timerStop(id);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
void router_mv_mon::init()
{
	if (m_packagerInstance.length() == 0 || m_packagerIndex == 0 || m_mode == NotSet || m_recurse == -1)
	{
		//Not enough good data yet.
		//debug("router_mv_mon::init() not enough data");
		return;
	}
	//debug("router_mv_mon::init() enough data");


	if (!getDev(m_packagerInstance, &m_packagerDevice))
	{
		debug("router_mv_mon::init() Error: could not get a device for '%1'", m_packagerInstance);
		return;
	}
	

	if (m_packagerInstance == "packager_router")
	{
		debug("router_mv_mon::init() register and poll: m_packagerDevice=%1 m_packagerIndex=%2", m_packagerDevice, m_packagerIndex);

		infoRegister(m_packagerDevice, m_packagerIndex, m_packagerIndex, false);
		infoPoll(m_packagerDevice, m_packagerIndex, m_packagerIndex);
	}
	else
	{
		routerRegister(m_packagerDevice, m_packagerIndex, m_packagerIndex, false);
		routerPoll(m_packagerDevice, m_packagerIndex, m_packagerIndex);
	}

	InitialDisplay();
}

void router_mv_mon::InitialDisplay()
{
	Deselect();
	//debug("router_mv_mon::InitialDisplay m_mode:%1", m_mode);
	UpdateDisplay(m_mode);
}

void router_mv_mon::UpdateDisplay(Mode mode)
{
	bncs_string sourceName;
	TypeInfo t;

	switch (mode)
	{
	case Monitor:	
		debug("router_mv_mon::UpdateDisplay Monitor");
		m_behaviour = Monitor;
		PanelShow(m_behaviour);
		routerName(m_packagerDevice, DATABASE_SOURCE_NAME, m_packagerSourceIndex, sourceName);
		textPut("text", sourceName, Monitor, "1");

		if (m_mode == Monitor) //This is in mointor mode
		{
			//This only has an affect in Monitor Mode with CRT, as that has "mv" tiles
			t = GetType();
			if (t.Type == TypeMV)
				controlShow(Monitor, "mv");
			else
				controlHide(Monitor, "mv");
		}
		break;
	case Tile:
		DisplayTile();
		break;
	case Layout:
		m_behaviour = Layout;

		//If the type is an MV
		t = GetType();
		if (t.Type == TypeMV)
		{
			DisplayTile();
		}
		else
		{
			//If it isn't an MV, just show a monitor?
			PanelShow(Monitor);
			controlHide(Monitor, "mv");
			m_behaviour = Monitor;
		}

		break;
	case NotSet:
		break;
	}
}

TypeInfo router_mv_mon::GetType()
{
	TypeInfo t;
	//If a MV is routed to us, then show that. Otherwise work like a monitor
	bncs_string adjust;
	routerName(m_videoDevice, DATABASE_VIP_SOURCE_MV_META, m_videoSourceIndex, adjust);
	debug("router_mv_mon::GetType() m_videoDevice=%1 DATABASE_VIP_SOURCE_MV_META=%2 m_videoSourceIndex=%3 Adjust:%4", m_videoDevice, DATABASE_VIP_SOURCE_MV_META, m_videoSourceIndex, adjust);
	//If the instance is a MV type we understand, do something special, otherwise behave like a monitor
	//Adjust looks like compositeInstance#group_01#group_02
	//c_tag_driver#outputs#output_01
	bncs_string second;

	adjust.split('#', t.CompositeInstance, second);

	second.split('#', t.Group_1, t.Group_2);

	instanceLookupComposite(t.CompositeInstance, t.Group_1, t.Instance_1); //Get the composite from the Instance

	instanceLookupComposite(t.Instance_1, t.Group_2, t.Instance_2); //Get the composite from the Instance

	bncs_string type;
	instanceLookupType(t.Instance_2, type); //Get the Type from the instance

	debug("router_mv_mon::GetType() instance2:%1 type=%2", t.Instance_2, type);

	//debug("router_mv_mon::DisplayTile compInst:%1 group:%2 inst:%3, type:%4", compInstance, group, instance, type);

	if (type == "tag_output")
		t.Type = TypeMV;
	else
		t.Type = TypeMonitor;
	return t;
}

void router_mv_mon::DisplayTile()
{
	TypeInfo type = GetType();
	if (type.Type == TypeMV && m_recurse >= 1)
	{
		m_behaviour = Tile;
	}
	else
		m_behaviour = Monitor;
	PanelShow(m_behaviour);
	if (m_behaviour == Monitor)
	{
		UpdateDisplay(Monitor);
	}
	else if (m_behaviour == Tile)
	{
		//debug("router_mv_mon::DisplayTile MV-820_LAYOUTS inst:%1 group:%2", compInstance, group);
		bncs_stringlist sl;
		sl << bncs_string("main_composite:").append(type.CompositeInstance);
		sl << bncs_string("instance:").append(type.Instance_1);
		sl << bncs_string("groupParam:").append(type.Group_2);
		textPut("all", sl.toString(','), Tile, "mv_mimic");
	}
}

void router_mv_mon::PanelShow(Mode panel)
{
	switch (panel)
	{
	case Monitor:
		//Test code remove once we are done with it
		if (m_mode == Monitor || m_mode == Layout)
		{
			if (m_currentUI != "monitor_crt.bncs_ui")
			{
				panelDestroy(m_currentPanel);
				m_currentUI = "monitor_crt.bncs_ui";
				panelShow(Monitor, "monitor_crt.bncs_ui");
			}
		}
		else
		{
			if (m_currentUI != "monitor.bncs_ui")
			{
				panelDestroy(m_currentPanel);
				m_currentUI = "monitor.bncs_ui";
				panelShow(Monitor, "monitor.bncs_ui");
			}
		}
		break;
	case Tile:
		panelDestroy(m_currentPanel); //Always destroy, otherwise we get weird results	
		m_currentUI = "tile.bncs_ui";
		panelShow(Tile, "tile.bncs_ui");
		break;
	case Layout:
		panelDestroy(m_currentPanel); //Always destroy, otherwise we get weird results	
		m_currentUI = "layout.bncs_ui";
		panelShow(Layout, "layout.bncs_ui");
		break;
	case NotSet:
		break;
	}
	m_currentPanel = panel;

}

void router_mv_mon::Deselect()
{
	if (m_ignoreDeselect)
	{
		m_ignoreDeselect = false;//Remove the ignore
		return;
	}

	m_isSelected = false;
	switch (m_behaviour)
	{
	case Monitor:
		if (m_levelsRecursed <= 1)
			textPut("statesheet", "mv_deselected", Monitor, "1");
		else if(m_levelsRecursed % 2 == 1)
			textPut("statesheet", "mv_deselected", Monitor, "1");
		else
			textPut("statesheet", "mv_deselected_2", Monitor, "1");
		break;
	case Tile:
		for (int i = 1; i <= m_tileList.count(); i++)
		{
			textPut("deselect", Tile, m_tileList[i - 1]);
		}
		break;
	case Layout:
		
		break;
	case NotSet:
		break;
	}

}

void router_mv_mon::Take(int Index)
{
	//debug("router_mv_mon::Take(index %1)", Index);
	switch (m_behaviour)
	{
	case Monitor:
		if (m_isSelected)
		{
			infoWrite(m_packagerDevice, bncs_string("index=").append(Index), m_packagerIndex);
		}
		break;
	case Tile:
		for (int i = 1; i <= m_tileList.count(); i++)
		{
			textPut("take", Index, Tile, m_tileList[i - 1]);
		}
		break;
	case Layout:
		
		break;
	case NotSet:
		break;
	}
}

void router_mv_mon::Resize(int width, int height)
{
	if (width != m_width || height != m_height)
	{
		m_width = width;
		m_height = height;
		//debug("router_mv_mon::Resize() Width:%1 Height:%2", m_width, m_height);
		setSize(m_width, m_height);
	}
}


void router_mv_mon::DisplayTile(bncs_string Layout, bncs_string Panel, int StartingX, int StartingY, int panelWidth, int panelHeight)
{
	//debug("router_mv_mon::DisplayTile");

	bncs_stringlist l(Layout, ',');

	if (l.count() > 0)
	{

		//GET THE WALL GEOMERTY - Should be getting this from the json
		double wallWidth = 1920;
		double wallHeight = 1080;

		double scaleWidth = panelWidth / wallWidth;
		double scaleHeight = panelHeight / wallHeight;

		list<bncs_string> tiles;

		if (Tile == Panel)
			tiles = m_displayedTiles;
		else
			tiles = m_layoutTiles;

		if (tiles.size() > 0)
		{
			for (list<bncs_string>::iterator it = tiles.begin(); it != tiles.end(); it++)
			{
				debug("router_mv_mon::DisplayTile Delete IDs:%1", *it);
				controlDestroy(Panel, *it);
			}
			tiles.clear();
		}

		if (Tile == Panel)
		{
			m_tileList.clear();
		}

		//This is needed to ensure that all IDs are unique		
		for (int i = 0; i < l.count(); i++)
		{
			bncs_stringlist types(l[i], '=');
			bncs_string d = types[1].replace(':','=');
			bncs_stringlist data(d, '.');
			
			debug("router_mv_mon::DisplayTile types:%1  data:%2",types.toString('@'),data.toString('@'));

			bncs_string id = data.getNamedParam("id");
			int x = data.getNamedParam("x");
			int y = data.getNamedParam("y");
			int width = data.getNamedParam("width");
			int height = data.getNamedParam("height");

			bncs_string id_name;
			if (types[0] == "clock")
			{
				id_name = bncs_string("clock_").append(id);
				controlCreate(Panel, id_name, "bncs_control", (int)(x * scaleWidth) + StartingX, (int)(y * scaleHeight) + StartingY, (int)(width * scaleWidth), (int)(height * scaleHeight), bncs_string("scaleWithCanvas=true\nstyle=label\nstylesheet=mv_default\ntext=%1\ncolour.background=%2").arg("Clock").arg("purple"));
			}
			else if (types[0] == "decor")
			{
				int fgColour = data.getNamedParam("fgcolour");
				int bgColour = data.getNamedParam("bgcolour");
				int text = data.getNamedParam("text");
				id_name = bncs_string("decor_").append(id);
				controlCreate(Panel, id_name, "bncs_control", (int)(x * scaleWidth) + StartingX, (int)(y * scaleHeight) + StartingY, (int)(width * scaleWidth), (int)(height * scaleHeight), bncs_string("scaleWithCanvas=true\nstyle=label\nstylesheet=mv_default\ntext=%1\ncolour.background=%2\ncolour.text=%3").arg(text).arg(fgColour).arg(bgColour));
			}
			else
			{
				int index = data.getNamedParam("index");
				bncs_string instance = data.getNamedParam("instance");
				id_name = bncs_string("mimic_").append(id);
				if (Tile == Panel)
					m_tileList << id_name;
				debug("router_mv_mon::DisplayTile normal tile id:%1", id);
				debug("router_mv_mon::DisplayTile normal tile x:%1 y:%2 w:%3 h:%4", (int)(x * scaleWidth), (int)(y * scaleHeight),  (int)(width * scaleWidth), (int)(height * scaleHeight));
				if (m_mode == Mode::Layout)
					controlCreate(Panel, id_name, "bncs_control", (int)(x * scaleWidth) + StartingX, (int)(y * scaleHeight) + StartingY, (int)(width * scaleWidth), (int)(height * scaleHeight), bncs_string("scaleWithCanvas=true\nstyle=label\nstylesheet=mv_default"));
				else
				{
					if (index > 0)
					{
						controlCreate(Panel, id_name, "bncs_script_loader", (int)(x * scaleWidth) + StartingX, (int)(y * scaleHeight) + StartingY, (int)(width * scaleWidth), (int)(height * scaleHeight), bncs_string("scaleWithCanvas=true\nscriptname=/_components/multiviewers/router_mv_mon\nall=instance:%1,index:%2,mode:%3,width:%4,height:%5,levelsRecursed:%6,recurse:%7,topLevelInstance:%8").arg(instance).arg(index).arg("tile").arg((int)(width * scaleWidth)).arg((int)(height * scaleHeight)).arg(m_levelsRecursed + 1).arg(m_recurse - 1).arg(m_topLevelInstance.length() == 0 ? m_packagerInstance : m_topLevelInstance));
					}
					else
					{
						controlCreate(Panel, id_name, "bncs_control", (int)(x * scaleWidth) + StartingX, (int)(y * scaleHeight) + StartingY, (int)(width * scaleWidth), (int)(height * scaleHeight), bncs_string("scaleWithCanvas=true\nstyle=label\nstylesheet=mv_no_input\ntext=No input"));
					}
				}					
			}
			tiles.push_back(id_name);			
		}

		if (Tile == Panel)
			m_displayedTiles = tiles;
		else
			m_layoutTiles = tiles;
		
		//textPut(bncs_string("text=%1").arg(mv_layout.name), PNL_MAIN, "layoutName");
	}

}
