#pragma once
#include "jsonParse.h"

class tile :
	public JsonParse
{
public:
	tile();
	tile(bncs_string& data);
	tile(Json::Value& data);
	tile(int index, bncs_string type, int x_axis, int y_axis, int width, int height, int destination, bncs_string content_type);

	~tile();

	int index;

	bncs_string type;

	int x_axis;

	int y_axis;

	int width;

	int height;

	int destination;

	bncs_string content_type;

	void GetValues(Json::Value& v);
};