#include "tile.h"


tile::tile()
{
}

tile::tile(Json::Value& data)
{
	Parse(data);
}

tile::tile(bncs_string& data)
{
	Parse(data);
}

tile::tile(int index, bncs_string type, int x_axis, int y_axis, int width, int height, int destination, bncs_string content_type)
{
	this->index = index;
	this->type = type;
	this->x_axis = x_axis;
	this->y_axis = y_axis;
	this->width = width;
	this->height = height;
	this->destination = destination;
	this->content_type = content_type;
}

tile::~tile()
{
}

void tile::GetValues(Json::Value& v)
{
	//OutputDebugString(bncs_string("JsonParse::Parse Node :%1").arg(JsonToString(v)));
	type = v["type"].asString().c_str();

	index = v["index"].asInt();

	x_axis = v["x_axis"].asInt();

	y_axis = v["y_axis"].asInt();

	width = v["width"].asInt();

	height = v["height"].asInt();

	destination = v["destination"].asInt();

	content_type = v["content_type"].asString().c_str();
}
