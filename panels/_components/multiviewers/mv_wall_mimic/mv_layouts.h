#pragma once
#include "jsonParse.h"
#include "mv_layout.h"
#include "bncs_stringlist.h"
#include <list>

class mv_layouts :
	public JsonParse
{
public:
	mv_layouts();
	mv_layouts(bncs_string& data);
	~mv_layouts();

	list<mv_layout> layouts;
	void GetValues(Json::Value& v);
	bool isValid;
};
