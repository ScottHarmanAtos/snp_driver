#include "mv_layouts.h"


mv_layouts::mv_layouts()
{
	isValid = false;
}

mv_layouts::mv_layouts(bncs_string& data)
{
	Parse(data);
}

mv_layouts::~mv_layouts()
{
}


void mv_layouts::GetValues(Json::Value& v)
{
	layouts.clear();
	isValid = true;

	for (unsigned int i = 0; i < v.size(); i++)
	{
		layouts.push_back(mv_layout(v[i]));
	}
}

