#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "mv_wall_mimic.h"
#include "instanceLookup.h"

#define PNL_MAIN	1

#define TIMER_RETRY	1

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( mv_wall_mimic )

// constructor - equivalent to ApplCore STARTUP
mv_wall_mimic::mv_wall_mimic( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_test = false;
	m_received = "";
	bncs_string toSend = bncs_string("{\"route\":{\"router\":\"<%1 | %2>\",\"source\":\"<%3>\",\"dest\":\"<%4>\"}}").arg("instance_name").arg(100).arg(1).arg(1);
}

// destructor - equivalent to ApplCore CLOSEDOWN
mv_wall_mimic::~mv_wall_mimic()
{
}

// all button pushes and notifications come here
void mv_wall_mimic::buttonCallback( buttonNotify *b )
{
}

// all revertives come here
int mv_wall_mimic::revertiveCallback( revertiveNotify * r )
{
	if (r->device() == m_intInstance)
	{
		debug(bncs_string("mv_wall_mimic::revertiveCallback from dev=%1 index=%2 m_slotLayout=%3 contents=%4").arg(m_intInstance).arg(r->index()).arg(m_slotLayout).arg(r->sInfo()));

		if (r->index() == m_slotLayout)
		{
			if (m_version == r->sInfo())
			{
				return 0;
			}
			else
			{
				m_version = r->sInfo();

				debug(bncs_string("mv_wall_mimic::revertiveCallback new version=%1").arg(m_version));

				bncs_string requestUri = bncs_string("http://%1:%2/outputs/%3/layout").arg(m_url).arg(m_port).arg(m_outputId);

				GetHttp(requestUri);
			}
		}

		/*if (r->index() == m_slotLayout)
		{
			if (m_received == r->sInfo())
			{
				return 0;
			}
			m_received = r->sInfo();

			if (r->sInfo().lower().startsWith("test="))
			{
				Test(bncs_stringlist(r->sInfo(), '=')[1]);
				return 0;
			}
			else
			{
				m_test = false;
			}

			//layout

			if (r->sInfo().length() != 0)
			{
				bncs_stringlist sltItemsComma = bncs_stringlist(r->sInfo(), ',');

				bncs_stringlist sltLayout = bncs_stringlist(sltItemsComma[0], '=');

				bncs_stringlist sltVersion = bncs_stringlist(sltItemsComma[1], '=');

				if (m_layout != sltLayout[1])
				{
					//new layout
					m_layout = sltLayout[1];



					m_version = sltVersion[1];

					debug(bncs_string("mv_wall_mimic::revertiveCallback my layout is '%1' from slot %2").arg(m_layout).arg(r->index()));

					//textPut(bncs_string("text=%1").arg(m_layout), PNL_MAIN, "layoutName");

					if (m_layout.length() != 0)
					{
						hostNotify(bncs_string("layoutName=%1").arg(m_layout));
						GetHttp(bncs_string("http://%1:%2/output/%3/%4.json").arg(m_url).arg(m_port).arg(m_outputId).arg(m_layout));
					}
					else
					{
						//No Layout set, lets send an empty setting
						hostNotify("layout=");
					}
				}
				else
				{
					if (m_version != sltVersion[1])
					{
						// geometry changed
						m_version = sltVersion[1];

						debug(bncs_string("mv_wall_mimic::revertiveCallback my layout is '%1' from slot %2 and my geometry changed").arg(m_layout).arg(r->index()));

						if (m_layout.length() != 0)
						{
							GetHttp(bncs_string("http://%1:%2/output/%3/%4.json").arg(m_url).arg(m_port).arg(m_outputId).arg(m_layout));
						}
						else
						{
							//No Layout set, lets send an empty setting
							hostNotify("layout=");
						}
					}
				}
			}
			else
			{
			}
		}
		else if (r->index() == m_slotLayoutList)
		{

		}*/
	}

	return 0;
}

// all database name changes come back here
void mv_wall_mimic::databaseCallback( revertiveNotify * r )
{

}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string mv_wall_mimic::parentCallback( parentNotify *p )
{
	debug("mv_wall_mimic::parentCallback() command=%1 value=%2", p->command(), p->value());
	if (p->command() == "return")
	{
		bncs_stringlist sl;

		sl << bncs_string("groupParam=%1").arg(m_Group);

		return sl.toString('\n');
	}
	else if (p->command() == "groupParam")
	{
		m_Group = p->value();

		initialise();
	}
	else if (p->command() == "instance")
	{
		m_Instance = p->value();

		initialise();
	}
	else if (p->command() == "test")
	{
		//Allows thet setting of a test layout
		Test(p->value());
	}
	else if (p->command() == "get_layouts")
	{
		//GetHttp(bncs_string("http://%1:%2/output/%3").arg(m_url).arg(m_port).arg(m_outputId));

		listOfLayouts.clear();

		GetHttp(bncs_string("http://%1:%2/layouts").arg(m_url).arg(m_port));
	}
	else if (p->command() == "all")
	{
		bncs_string s = p->value().replace(':', '=');
		bncs_stringlist sl(s, ',');
		main_composite = sl.getNamedParam("main_composite");
		m_Instance = sl.getNamedParam("instance");
		m_Group = sl.getNamedParam("groupParam");

		initialise();
	}
	else if (p->command() == "_commands")
	{
		bncs_stringlist sl;

		sl << "instance=<CompositeInstance>";
		sl << "test=<num>";
		sl << "groupParam=<group>";
		sl << "width=<width>";
		sl << "height=<height>";

		return sl.toString('\n');

	}
	else if (p->command() == "_events")
	{
		bncs_stringlist sl;

		sl << "layoutName=<name>";

		return sl.toString('\n');
	}

	return 0;
}

// timer events come here
void mv_wall_mimic::timerCallback( int id )
{
	switch( id )
	{
		case TIMER_RETRY:
		{
			timerStop(id);
			
			GetHttp(m_httpLastRequest);			
		}
		break;

		default:	// Unhandled timer event
			timerStop(id);
			break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void mv_wall_mimic::httpCallback(httpNotify *h)
{	
	if (h->url().endsWith("/layout"))
	{
		//geometry for an output

		debug("mv_wall_mimic::httpCallback data:%1 err:%2 ok:%3", h->data(), h->err(), h->ok());
		if (m_test == true)
			return;
		//Received ok
		if (!h->ok() || h->data() == "null")
		{
			debug("mv_wall_mimic::httpCallback ERROR for getting layout of an output Error:%1", h->err());

			timerStart(TIMER_RETRY, 1000);
			return;
		}

		//controlHide(PNL_MAIN, "error_background");

		debug("mv_wall_mimic::httpCallback data is:%1", h->data());

		mv_layout mv_layout(h->data());

		hostNotify(bncs_string("layoutName=%1").arg(mv_layout.label));

		LoadLayout(mv_layout);
	}
	else if (h->url().endsWith("/layouts"))
	{
		//all layouts

		bncs_stringlist listLayouts = bncs_stringlist(h->data());

		if (listLayouts.count() > 0)
		{
			for (int i = 0; i < listLayouts.count(); i++)
			{
				GetHttp(bncs_string("http://%1:%2/layout/%3").arg(m_url).arg(m_port).arg(listLayouts[i]));
			}
		}
	}
	else
	{
		//geometry for a layout
		if (!h->ok() || h->data() == "null")
		{
			debug("mv_wall_mimic::httpCallback ERROR for getting a layout geometry, Error:%1", h->err());			
			return;
		}

		mv_layout new_layout(h->data());

		listOfLayouts.push_back(new_layout);

		bncs_stringlist sl;

		for (list<mv_layout>::const_iterator p = listOfLayouts.begin(); p != listOfLayouts.end(); p++)
		{
			bncs_string s = bncs_string(p->label).append('#').append(p->uuid).append('~').append(GenerateReturnString(*p).toString(','));
			sl.append(s);
		}

		hostNotify(bncs_string("layouts=").append(sl.toString('@')));
	}
}

void mv_wall_mimic::initialise()
{
	debug("mv_wall_mimic::initialise() lookup m_Instance=%1 m_Group=%2 main_composite=%3", m_Instance, m_Group, main_composite);

	if (m_Instance.length() > 0 && m_Group.length() > 0 && main_composite.length() > 0)
	{
		bncs_config cCompInstanceDriver = bncs_config(bncs_string("instances.%1.%2").arg(main_composite).arg("driver"));
		if (cCompInstanceDriver.isValid())
		{
			bncs_string cDriverInstance = cCompInstanceDriver.attr("instance");

			bncs_config cInstance = bncs_config(bncs_string("instances.%1").arg(cDriverInstance));
			if (cInstance.isValid())
			{
				bncs_config cDriverUrlSetting = bncs_config(bncs_string("instances.%1.%2").arg(cDriverInstance).arg("driver_address"));
				if (cDriverUrlSetting.isValid())
				{
					m_url = cDriverUrlSetting.attr("value");
				}

				bncs_config cDriverPortSetting = bncs_config(bncs_string("instances.%1.%2").arg(cDriverInstance).arg("driver_port"));
				if (cDriverPortSetting.isValid())
				{
					m_port = cDriverPortSetting.attr("value");
				}
			}
		}

		debug("mv_wall_mimic::initialise() m_url=%1 m_port=%2", m_url, m_port);

		bncs_config cCompInstanceOutputs = bncs_config(bncs_string("instances.%1.%2").arg(main_composite).arg("outputs"));
		if (cCompInstanceOutputs.isValid())
		{
			bncs_string cOutputsInstance = cCompInstanceOutputs.attr("instance");

			bncs_config cOutputGroup = bncs_config(bncs_string("instances.%1.%2").arg(cOutputsInstance).arg(m_Group));
			if (cOutputGroup.isValid())
			{
				bncs_string outputInstance = cOutputGroup.attr("instance");

				bncs_config cOutputInstance = bncs_config(bncs_string("instances.%1").arg(outputInstance));
				if (cOutputInstance.isValid())
				{
					bncs_config cOutputIdSetting = bncs_config(bncs_string("instances.%1.%2").arg(outputInstance).arg("id"));
					if (cOutputIdSetting.isValid())
					{
						m_outputId = cOutputIdSetting.attr("value");

						debug("mv_wall_mimic::initialise() m_outputId=%1", m_outputId);
					}

					char type;

					getDevSlot(outputInstance, "geometry_version", &m_intInstance, &m_slotLayout, &type);

					infoRegister(m_intInstance, m_slotLayout, m_slotLayout, true);
					infoPoll(m_intInstance, m_slotLayout, m_slotLayout);
				}
			}
		}




		/*bncs_config cComposite = bncs_config(bncs_string("instances.%1").arg(m_Instance));
		if (cComposite.isValid())
		{
			m_url = cComposite.attr("driver_ip");
			m_port = cComposite.attr("driver_port");
		}

		//debug("mv_wall_mimic::initialise() lookup m_Instance:%1 m_Group:%2", m_Instance, m_Group);
		if (instanceLookupComposite(m_Instance, m_Group, m_wallInstance))
		{
			//debug("mv_wall_mimic::initialise() wallInstance:%1", m_wallInstance);
			char type;

			getDevSlot(m_wallInstance, "wall_current_layout", &m_intInstance, &m_slotLayout, &type);
			getDevSlot(m_wallInstance, "wall_layouts_list", &m_intInstance, &m_slotLayoutList, &type);

			bncs_config cWallInstance = bncs_config(bncs_string("instances.%1").arg(m_wallInstance));
			if (cWallInstance.isValid())
			{
				m_outputId = cWallInstance.attr("output_id");

				//debug("mv_wall_mimic::initialise() Dev:%1 Slot:%2", m_intInstance, m_slotLayout);
				infoRegister(m_intInstance, m_slotLayout, m_slotLayoutList, true);
				infoPoll(m_intInstance, m_slotLayout, m_slotLayoutList);
			}
		}

		//Get the Tile mapping


		//@HACK whilst mv_inputs_outputs does not have the correct instances
		bncs_string first, second;
		main_composite.split('_', first, second);

		map<int, pair<bncs_string, int>> tile_index;
		bncs_config c(bncs_string("mv_inputs_outputs.").append(second));
		while (c.isChildValid())
		{
			//Only store the inputs
			if (c.childAttr("id").startsWith("input_"))
			{
				tile_index[c.childAttr("id").firstInt()] = pair<bncs_string,int>(c.childAttr("router_instance"),c.childAttr("router_index"));
			}
			//debug("mv_wall_mimic::initialise() id:%1 index;%2", c.childAttr("id"), c.childAttr("router_index"));
			c.nextChild();
		}
		m_tile_index = tile_index;*/
	}
}

list<mv_layout> mv_wall_mimic::GetAllTests()
{
	list<mv_layout> l;

	int i = 1;
	while (true)
	{
		mv_layout m = GetTest(i);

		if (m.isValid)
			l.push_back(m);
		else
			break;
		i++;
	}
	return l;
}

mv_layout mv_wall_mimic::GetTest(int i)
{
	mv_layout l;
	l.isValid = true;
	if (i == 1)
	{
		debug("mv_wall_mimic::Test i:%1", i);
		l.label = "Test 1";
		l.tiles.push_back(tile(1, "test", 0, 0, 960, 540, 1, "Channel"));
		l.tiles.push_back(tile(2, "test", 960, 0, 960, 540, 2, "Channel"));
		l.tiles.push_back(tile(3, "test", 0, 540, 960, 540, 3, "Channel"));
		l.tiles.push_back(tile(4, "test", 960, 540, 960, 540, 4, "Channel"));
	}
	else if (i == 2)
	{
		debug("mv_wall_mimic::Test i:%1", i);
		l.label = "Test 2";
		l.tiles.push_back(tile(1, "test", 0, 0, 960, 540, 1, "Channel"));
		l.tiles.push_back(tile(2, "test", 960, 0, 960, 540, 2, "Channel"));
		l.tiles.push_back(tile(3, "test", 0, 540, 960, 540, 3, "Channel"));
	}
	else
	{
		return mv_layout();
	}
	return l;
}

void mv_wall_mimic::Test(int i)
{
	if (i == 0)
	{
		m_test = false;
		return;
	}
	else
		m_test = true;


	mv_layout l = GetTest(i);

	if (!l.isValid)
		m_test = false;
	
	LoadLayout(l);
}

void mv_wall_mimic::LoadLayouts(const mv_layouts& l)
{
	bncs_stringlist sl;

	for (list<mv_layout>::const_iterator p = l.layouts.begin(); p != l.layouts.end(); p++)
	{
		bncs_string s = bncs_string(p->label).append('#').append(GenerateReturnString(*p).toString(','));		
		sl.append(s);
	}

	hostNotify(bncs_string("layouts=").append(sl.toString('@')));
}

void mv_wall_mimic::LoadLayout(const mv_layout& l)
{
	bncs_stringlist sl = GenerateReturnString(l);
	//debug("mv_wall_mimic::LoadLayout :%1", sl.toString());
	hostNotify(bncs_string("layout=").append(sl.toString()));
}

bncs_stringlist mv_wall_mimic::GenerateReturnString(const mv_layout& l)
{
	//Tiles are comma(,) separated
	//tile,tile,tiles
	//Each tile has values which are dot(.) separated
	//Each tile value are keyvalue pairs that are colon(:) separated
	bncs_stringlist sl;
	for (list<tile>::const_iterator p = l.tiles.begin(); p != l.tiles.end(); p++)
	{
		/*if (p->content_type == "Clock.tile" || p->content_type == "SELMA Timezone Clock.tile")
		{
			sl << bncs_string("clock=id:%1.x:%2.y:%3.width:%4.height:%5")
				.arg(p->destination)
				.arg(p->x_axis)
				.arg(p->y_axis)
				.arg(p->width)
				.arg(p->height);
		}
		else if (p->content_type == "SELMA Column Label.tile" && p->content_type == "SELMA Row Label.tile" || p->destination <= 0)
		{
			sl << bncs_string("decor=id:%1.fgcolour:%2.bgcolour:%3.x:%4.y:%5.width:%6.height:%7")
				.arg(p->destination)
				.arg("green")
				.arg("black")
				.arg(p->x_axis)
				.arg(p->y_axis)
				.arg(p->width)
				.arg(p->height);
		}
		else
		{
			sl << bncs_string("tile=id:%1.instance:%2.index:%3.x:%4.y:%5.width:%6.height:%7")
				.arg(p->destination)
				.arg(m_tile_index[p->destination].first)
				.arg(m_tile_index[p->destination].second)
				.arg(p->x_axis)
				.arg(p->y_axis)
				.arg(p->width)
				.arg(p->height);
		}*/

		sl << bncs_string("tile=id:%1.instance:%2.index:%3.x:%4.y:%5.width:%6.height:%7")
			.arg(p->index)
			.arg("packager_router")
			.arg(p->destination)
			.arg(p->x_axis)
			.arg(p->y_axis)
			.arg(p->width)
			.arg(p->height);
	}
	return sl;
}

void mv_wall_mimic::GetHttp(bncs_string s)
{
	m_httpLastRequest = s;
	httpGetString(s);
}