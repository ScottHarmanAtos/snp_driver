#include "mv_layout.h"

mv_layout::mv_layout()
{
	isValid = false;
}


mv_layout::mv_layout(bncs_string& data)
{
	Parse(data);
}

mv_layout::mv_layout(Json::Value& data)
{
	Parse(data);
}

mv_layout::~mv_layout()
{
}

void mv_layout::GetValues(Json::Value& v)
{
	isValid = true;

	uuid = v["uuid"].asString().c_str();

	label = v["label"].asString().c_str();

	resolution = v["resolution"].asString().c_str();
	
	Json::Value rawTiles = v["Tiles"];

	for (unsigned int i = 0; i < rawTiles.size(); i++)
	{
		tiles.push_back(tile(v["Tiles"][i]));
	}
}