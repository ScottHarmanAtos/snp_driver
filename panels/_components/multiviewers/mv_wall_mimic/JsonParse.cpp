#include "JsonParse.h"
//#include "Windows.h"

JsonParse::JsonParse()
{
}

JsonParse::~JsonParse()
{
}

bool JsonParse::isParseValid()
{
	return _isParseValid;
}

Json::Value JsonParse::Parse(bncs_string& value)
{
	Json::Value root;
	Json::Reader reader;

	//Parse the Json Object
	if (!reader.parse((const char*)value, Json))
	{
		_isParseValid = false;
	}
	else
	{
		_isParseValid = true;
		GetValues(Json);
		String = JsonToString(Json); //Doing this to collect any extra string that have been added
		//OutputDebugString(bncs_string("JsonParse::Parse String:%1").arg(String));
	}
	//OutputDebugString(bncs_string("Parse:%1").arg(_isParseValid));

	return Json;
}

bncs_string JsonParse::Parse(Json::Value& value)
{
	
	if (value != Json::Value::null)
	{
		_isParseValid = true;
		GetValues(value);
		String = JsonToString(value);
		//OutputDebugString(bncs_string("JsonParse::Parse Json:%1").arg(String));
	}
	else
	{
		_isParseValid = false;
		String = "";
	}

	
	Json = value;
	return String;
}

bncs_string JsonParse::JsonToString(Json::Value v)
{
	Json::FastWriter f;
	return f.write(v).c_str();
}