#ifndef mv_wall_mimic_INCLUDED
	#define mv_wall_mimic_INCLUDED

#include <bncs_script_helper.h>
#include <map>
#include "mv_layout.h"
#include "mv_layouts.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class mv_wall_mimic : public bncs_script_helper
{
public:
	mv_wall_mimic( bncs_client_callback * parent, const char* path );
	virtual ~mv_wall_mimic();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void httpCallback(httpNotify *p);
	
private:
	void initialise();

	bncs_string m_layout;

	bncs_string m_version;

	bncs_string m_url;
	bncs_string m_port;

	bncs_string m_outputId;

	bncs_string main_composite;

	bncs_string m_Instance;
	bncs_string m_wallInstance;

	bncs_string m_Group;

	int m_intInstance;

	int m_slotLayout;

	std::map<int, bncs_string> mapOfTiles;

	list<mv_layout> listOfLayouts;

	void Test(int);
	mv_layout GetTest(int);
	list<mv_layout> GetAllTests();
	void LoadLayout(const mv_layout&);
	void LoadLayouts(const mv_layouts& l);
	bncs_stringlist GenerateReturnString(const mv_layout&);

	bool m_test;
	bncs_string m_received;

	map<int, pair<bncs_string,int>> m_tile_index;

	bncs_string m_httpLastRequest;

	void GetHttp(bncs_string s);
};


#endif // mv_wall_mimic_INCLUDED
