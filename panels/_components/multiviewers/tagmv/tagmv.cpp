#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <map>
#include "bncs_packager.h"
#include "bncs_tagmv.h"
//#include <string.h>
#include "tagmv.h"

#define PNL_MAIN	1
#define PNL_POPUP	2
#define PNL_STATS	3

#define TIMER_SETUP				1
#define TIMER_SETUP_DURATION	100

#define TIMER_RETRY				2
#define TIMER_RETRY_DURATION	30000

#define SETTING_PACKAGER_INSTANCE	"packager_instance"
#define TYPE_TAGMV					"c_tag_mcm9000"

#define CONTENT_TYPE_TEXT		"text"
#define CONTENT_TYPE_CLOCK		"clock"
#define CONTENT_TYPE_CHANNEL	"channel"
#define CONTENT_TYPE_UNKNOWN	"unknown"

#define LAYOUT_NAME_HEIGHT		30
#define LAYOUT_BORDER_SIZE		10

#define LAYOUTS_PAGESIZE		12

#define BUTTON_LAYOUTS_LESS		"less"
#define BUTTON_LAYOUTS_MORE		"more"
#define BUTTON_LAYOUTS_CLOSE	"close"
#define BUTTON_LAYOUTS_INFO		"info"


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(tagmv)

// constructor - equivalent to ApplCore STARTUP
tagmv::tagmv(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path)
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PNL_MAIN, "main.bncs_ui");

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
	//	setSize( 1024,668 );		// set the size explicitly
	// setSize(PNL_MAIN);		// set the size to the same as the specified panel

	/*
		float f1 = 1.23456;
		int i1 = 10;
		int i2 = 3;
		float f2 = (float)i1 / i2;
		int i3 = (f1 * i2) + 0.5;

		debug("float::f1 %1, f2 %2, i1 %3, i3 %4, f1*i2 %5, i3 %6", to_string(f1), to_string(f2), i1, i2, to_string(f1 * i2), i3);

		*/
	/*
	bncs_packager test("c_packager_ldc");
	debug("tagmv::bncs_packager test::   1,dev %1, slot %2, toDest %3", test.destDev(1), test.destSlot(1), test.toDest(test.destDev(1), test.destSlot(1)));
	debug("tagmv::bncs_packager test::3999,dev %1, slot %2, toDest %3", test.destDev(3999), test.destSlot(3999), test.toDest(test.destDev(3999), test.destSlot(3999)));
	debug("tagmv::bncs_packager test::4000,dev %1, slot %2, toDest %3", test.destDev(4000), test.destSlot(4000), test.toDest(test.destDev(4000), test.destSlot(4000)));
	debug("tagmv::bncs_packager test::4001,dev %1, slot %2, toDest %3", test.destDev(4001), test.destSlot(4001), test.toDest(test.destDev(4001), test.destSlot(4001)));
	debug("tagmv::bncs_packager test::base device %1", test.device());
	debug("tagmv::bncs_packager test::names device %1", test.deviceNames());
	debug("tagmv::bncs_packager test::first virtual %1", test.firstVirtual());
	debug("tagmv::bncs_packager test::number of packages %1", test.numberOfPackages());
	debug("tagmv::bncs_packager test::is 1001 a device %1", test.isDevice(1001) ? "true" : "false");
	debug("tagmv::bncs_packager test::is 1016 a device %1", test.isDevice(1016) ? "true" : "false");
	debug("tagmv::bncs_packager test::is 101  a device %1", test.isDevice(101) ? "true" : "false");
	debug("tagmv::bncs_packager test::config in use %1", test.config());
	debug("tagmv::bncs_packager test::config item video_router_hd %1", test.getConfigItem("video_router_hd"));
	*/

	//m_mv_layout = mv_layout(bncs_string(""));

	init();	// register for the packager trace elements
}

// destructor - equivalent to ApplCore CLOSEDOWN
tagmv::~tagmv()
{
}

// all button pushes and notifications come here
void tagmv::buttonCallback(buttonNotify *b)
{
	b->dump("tagmv::BCB::dump:");

	if (b->panel() == PNL_MAIN)
	{
		if (b->id() == "taglayout")
		{	//
			if (b->command() == "dests")
			{	// this is the index of our destinations, build the maps and poll the dest_status
				m_tile2dest.clear();
				m_dest2tile.clear();
				m_dest4tile.clear();

				bncs_stringlist sld = bncs_stringlist(b->value());
				for (bncs_stringlistIterator slit = sld.begin(); slit != sld.end(); slit++)
				{
					bncs_string t, d;
					slit->split('=', t, d);
					debug("tagmv::BCB::tile %1 goes to dest %2", t.toInt(), d.toInt());
					m_tile2dest[t.toInt()] = d.toInt();

					m_dest2tile[d.toInt()] << t;

					debug("tagmv::BCB::m_dest2tile[%1] >> %2", d.toInt(), m_dest2tile[d.toInt()].toString());

					packagerPoll(m_packager_status, d, d);
				}
			}
			else if (b->command() == "dest" && b->subs() == 1)
			{
				// m_dest4tile[] contains our map of tile to the destination for(4) the route to that tile
				m_dest = m_dest4tile[b->sub(0)];
				debug("tagmv::BCB::Dest tile %1 selected, dest %2", b->sub(0), m_dest);
				hostNotify(bncs_string("dest=%1").arg(m_dest));
			}
			else if (b->command() == "name")
			{
				if (b->subs() && b->sub(0) == "button" && b->value() == "released")
				{	// layout name pressed
					for (map<int, bncs_stringlist>::iterator it = m_dest2tile.begin(); it != m_dest2tile.end(); it++)
					{
						debug("tagmv::BCB::m_dest2tile[%1] = %2", it->first, it->second.toString());
					}
					for (map<int, int>::iterator it = m_tile2dest.begin(); it != m_tile2dest.end(); it++)
					{
						debug("tagmv::BCB::m_tile2dest[%1] = %2", it->first, it->second);
					}
					for (map<int, int>::iterator it = m_dest4tile.begin(); it != m_dest4tile.end(); it++)
					{
						debug("tagmv::BCB::m_dest4tile[%1] = %2", it->first, it->second);
					}
				}
				else if (b->subs() && b->sub(0) == "button" && b->value() == "pushhold")
				{
					panelPopup(PNL_STATS, "stats.bncs_ui");
					stats();
				}
			}
			else if (b->command() == "layout")
			{
				if (b->subs() && b->sub(0) == "button" && b->value() == "released")
				{
					if (m_isLayoutsValid)
					{
						renderLayouts();
					}
				}
			}
			else if (b->command() == "monitor")
			{
				if (b->subs() && b->sub(0) == "button" && b->value() == "released")
				{
					if (m_tagmv.isValid())
					{
						// Create dest_monitor button
						controlCreate(PNL_MAIN,
							"dest_monitor",
							"bncs_script_loader",
							0,
							0,
							m_width,
							m_height,
							bncs_string("scriptname=/_components/routing/dest_package\nscaleWithCanvas=true\nlayout=dest_monitor\nfunction=%1\nshow_highlight=false\npriority=3").arg(m_function));

						textPut("workspace", m_workspace, PNL_MAIN, "dest_monitor");
						m_dest = tDest;

						// Create return button
						controlCreate(PNL_MAIN,
							"dest_return",
							"bncs_control",
							m_width - 70,
							25,
							40,
							38,
							"style=button\ntext=\npixmap=/images/icon_undo.png");

					}
				}
			}

		}
		/*if (b->id().startsWith("tag_"))
		{
		textPutAll("statesheet", "enum_deselected", PNL_MAIN, "tag_channel_");

		bncs_string t;
		bncs_stringlist sl = bncs_stringlist(b->id(), '_');

		// tag_{type}_{tile}_{destination}
		// 0    1      2      3

		if (sl.count() == 4 && sl[1] == CONTENT_TYPE_CHANNEL)
		{
		t = sl[2];

		// find the dest
		tSource = sl[3].toInt();
		textPutAll("statesheet", "enum_selected", PNL_MAIN, bncs_string("tag_channel_%1_").arg(t));

		debug(bncs_string("tagmv::BCB::MV tile %1 pressed which is dest_pkg %2").arg(t).arg(tSource));

		//packagerPoll(m_packager_status, tSource, tSource);
		// lets try extracting the info from the layout structure
		for (list<tile>::iterator p = m_mv_layout.tiles.begin(); p != m_mv_layout.tiles.end(); p++)
		{
		if (p->index == t && p->trace.count() > 1)
		{
		debug("tagmv::BCB::Tile Source trace list %1 >> %2", p->trace.toString(), tSource);
		// p->trace.count() is true count of the number of elements
		// we need the last entry in the trace
		// the slt array is zero based so subtract 1 to the count
		// check that end element is reasonable
		if (p->trace[p->trace.count() - 1].toInt() > 0)
		{
		m_dest = p->trace[p->trace.count() - 1].toInt();
		debug("tagmv::BCB::Tile adjust dest is d%1", m_dest);
		// tell the host our destination
		hostNotify(bncs_string("dest=%1").arg(m_dest));
		}
		}
		}

		}
		}*/
		else if (b->id() == "dest_xs" && b->command() == "dest")
		{
			m_dest = tDest;
			m_tagmv = bncs_tagmv();
			hostNotify(bncs_string("dest=%1").arg(m_dest));
		}
		else if (b->id() == "dest_monitor" && b->command() == "dest")
		{
			//m_dest = tDest;
			//m_tagmv = bncs_tagmv();
			hostNotify(bncs_string("dest=%1").arg(m_dest));
		}
		else if (b->id() == "dest_return")
		{
			controlDestroy(PNL_MAIN, "dest_monitor");
			controlDestroy(PNL_MAIN, "dest_return");
		}
	}
	else if (b->panel() == PNL_POPUP)
	{
		if (b->id().startsWith("layout_") && (b->command() == "name" || b->command() == "dest"))
		{	// popup layout name pressed, b->id() is in the form "layout_nn"
			bncs_string c, l;
			b->id().split('_', c, l);
			int layout = l.toInt() + (m_layoutsPage * LAYOUTS_PAGESIZE);
			debug("tagmv::BCB::layout button %1 selected for layout %2, we need to send ID %3 to dev %4 on index %5", l, layout, m_layouts[layout], m_tagmv.headDevice(), m_tagmv.layoutIdIndex());
			infoWrite(m_tagmv.headDevice(), m_layouts[layout], m_tagmv.layoutIdIndex());
			panelDestroy(PNL_POPUP);
		}
		else if (b->id() == BUTTON_LAYOUTS_LESS)
		{
			setLayoutsPage(m_layoutsPage - 1);
		}
		else if (b->id() == BUTTON_LAYOUTS_MORE)
		{
			setLayoutsPage(m_layoutsPage + 1);
		}
		else if (b->id() == BUTTON_LAYOUTS_CLOSE)
		{
			panelDestroy(PNL_POPUP);
		}
	}
	else if (b->panel() == PNL_STATS)
	{
		if (b->id() == "close")
		{
			panelDestroy(PNL_STATS);
		}
		else if (b->id() == "btn_request_url")
		{
			clearHttpData();
			httpGetString(m_tagmv.requestURI());
			stats();
		}
	}
}

// all revertives come here
int tagmv::revertiveCallback(revertiveNotify * r)
{
	r->dump("tagmv::RCB::dump:");

	if (m_packager.isDevice(r->device()))
	{	// this is a revertive from  packager_status

		// is the revertive for the destination trace (tDest) ?
		if (tDest == m_packager.toDest(r->device(), r->index()))
		{
			// Use packager data to find out what, if any, multiviewer we are using

			// cleardown old data first
			m_v_index = 0;
			m_hd_section_01_Instance = "";
			m_hd_device = 0;
			m_adjust = "";

			// The packager dest returns data in the form
			// r->sInfo() = "index=1,v_level=3,v_index=501"

			// create stringlist of sInfo(), getNamedParam v_index gets us the index of the video
			m_v_index = bncs_stringlist(r->sInfo()).getNamedParam("v_index");
			debug("tagmv::RCB::tDest %1, v_index %1", tDest, m_v_index);

			if (m_v_index.toInt())
			{	// we have a video index
				// get the real video hd router device number
				bncs_string hd_cInstance = m_packager.getConfigItem("video_router_hd");
				m_hd_section_01_Instance = bncs_config(bncs_string("instances.%1.section_01").arg(hd_cInstance)).attr("instance");
				getDev(m_hd_section_01_Instance, &m_hd_device);
				debug("tagmv::RCB::v_index resolves to <%1>/<%2>/%3", hd_cInstance, m_hd_section_01_Instance, m_hd_device);

				// get the adjust data from the index on real video hd router database 4
				routerName(m_hd_device, 4, m_v_index.toInt(), m_adjust);

				// a multiviewer adjust takes the form of <c_tag_instance>#<head_output_group_name>
				// example 'c_51000/MVWR/019#output_01'
				// split the adjust into instance and head
				bncs_string mvwr_instance = "", mvwr_head = "";
				m_adjust.split('#', mvwr_instance, mvwr_head);
				debug("tagmv::RCB::Adjust lookup resolves to <%1>#<%2>", mvwr_instance, mvwr_head);
				debug("tagmv::RCB::Current mv    resolves to <%1>#<%2>", m_tagmv.instance(), m_tagmv.head());

				status(bncs_string("PkgrRev %1|Video Srce %2|Adjust %3").arg(r->sInfo()).arg(m_v_index).arg(m_adjust), true);

				// check if we are the same multiviewer
				if (m_tagmv == bncs_tagmv(mvwr_instance, mvwr_head.firstInt()) && m_tagmv.isValid())
				{
					// we are the same, leave well alone 
					debug("tagmv::RCB::Existing MV");

					status(bncs_string(" |Existing MV"), true);
					textPut("layout", m_current_layout, PNL_MAIN, "taglayout");
				}
				// check to see if what we have found is a different multiviewer
				else if (m_tagmv != bncs_tagmv(mvwr_instance, mvwr_head.firstInt()) && bncs_tagmv(mvwr_instance, mvwr_head.firstInt()).isValid())
				{
					debug("tagmv::RCB::found a new MV");
					// This means we were a new multiviewer
					// Cleanup and remove the old display
					//packagerUnregister(m_packager);
					controlDestroyAll(PNL_MAIN, "tag_");
					controlDestroyAll(PNL_MAIN, "dest_");
					m_dest = -1;
					m_isMV = true;
					m_current_layout = "";

					// create a tagmv using the instance and head found
					m_tagmv = bncs_tagmv(mvwr_instance, mvwr_head.firstInt());

					status(bncs_string(" |New MV"), true);

					infoRegister(m_tagmv.headDevice(), m_tagmv.geometryIndex(), m_tagmv.geometryIndex());
					infoPoll(m_tagmv.headDevice(), m_tagmv.geometryIndex(), m_tagmv.geometryIndex());
					debug("tagmv::RCB::Tag geometry requested device %1 index %2", m_tagmv.headDevice(), m_tagmv.geometryIndex());

					status(bncs_string(" |Multiviewer %1 found|Requesting layout for head %2").arg(mvwr_instance).arg(mvwr_head.firstInt()));
					// reset the retry count
					m_retry = 0;
					// get the active layout on the output
					clearHttpData();
					httpGetString(m_tagmv.requestURI());
					// get the layout id list
					httpGetString(m_tagmv.layoutsURI());
				}
				else
				{
					// we are not a multiviewer
					status(bncs_string(" |Invalid trace|No multiviewer found| |s%1 adjust<%2>").arg(m_v_index.toInt()).arg(m_adjust));
					// ...but have we been here before?
					if (m_isMV)
					{	// no we have not
						m_isMV = false;
						m_tagmv = bncs_tagmv();
						m_geometry = "";

						debug("tagmv::RCB::Invalid trace, creating dest_package workspace %1, function %2", m_workspace, m_function);
						status(bncs_string(" |Creating Dest Ctrl"), true);

						controlCreate(PNL_MAIN,
							"dest_xs",
							"bncs_script_loader",
							0,
							0,
							m_width,
							m_height,
							bncs_string("scriptname=/_components/routing/dest_package\nscaleWithCanvas=true\nlayout=dest_monitor\nfunction=%1\nshow_highlight=false\npriority=3").arg(m_function));

						textPut("workspace", m_workspace, PNL_MAIN, "dest_xs");
					}
				}
			}
			else
			{
				status(bncs_string("PkgrRev %1|No Video Srce").arg(r->sInfo()), true);
			}
		}
	}

	else if (m_packager_status.isDevice(r->device()))
	{	// this is a revertive from  packager_status

		// check for tile destination and extract and name the tile
		int tileDest = m_packager_status.toDest(r->device(), r->index());
		if (m_dest2tile.count(tileDest) && m_dest2tile[tileDest].count())	// check existence THEN check if there is a count in the entry
		{
			debug("tagmv::RCB::dest2tile match, dest %1 is tile(s) %2", tileDest, m_dest2tile[tileDest].toString());

			bncs_stringlist trace = bncs_stringlist(bncs_stringlist(r->sInfo()).getNamedParam("trace"), '|');
			// this extracts the "trace = xxx | yyy | zzz" section of the dest status
			// and stores it in the tile structure
			debug("tagmv::RCB::Tile Source locate trace list %1 >> %2", trace.toString(), tileDest);

			// check for at least 2 entries in the trace
			if (trace.count() > 1)
			{
				// get the name of our source
				bncs_string name;
				routerName(m_packager.deviceNames(), 0, trace[trace.count() - 2].toInt(), name);
				debug("tagmv::RCB::Traced Tile Source %1 has name %2", trace[trace.count() - 2].toInt(), name);

				for (bncs_stringlistIterator slit = m_dest2tile[tileDest].begin(); slit != m_dest2tile[tileDest].end(); slit++)
				{	// iterate through our tile list
					// ...and extract our entry virtual DEST p->trace[p->trace.count() - 1].toInt()
					m_dest4tile[slit->toInt()] = trace[trace.count() - 1].toInt();
					debug("tagmv::RCB::m_dest4tile[%1] >> %2, notify taglayout <%3=%4>", slit->toInt(), m_dest4tile[slit->toInt()], bncs_string("dest.%1.text").arg(slit->toInt()), name);
					// name our tile with the source
					textPut(bncs_string("dest.%1.text").arg(slit->toInt()), name, PNL_MAIN, "taglayout");
				}
			}
		}
	}

	else if (m_tagmv.isValid() && r->device() == m_tagmv.headDevice() && r->index() == m_tagmv.geometryIndex())
	{
		if (!m_geometry.length())
		{ // we are first time geometry
			m_geometry = r->sInfo();
			debug("tagmv::RCB::geometry set for first time <%1>", m_geometry);
		}
		else if (m_geometry != r->sInfo())
		{ // we are changed geometry
			m_geometry = r->sInfo();
			debug("tagmv::RCB::geometry is NEW <%1>", m_geometry);
			// go get the new layout
			httpGetString(m_tagmv.requestURI());
		}
		else
		{ // we are existing geometry
			debug("tagmv::RCB::geometry is existing <%1>", m_geometry);
		}
	}

	return 0;
}

// all database name changes come back here
void tagmv::databaseCallback(revertiveNotify * r)
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string tagmv::parentCallback(parentNotify *p)
{
	p->dump("tagmv::PCB::dump():");

	if (p->command() == "return")
	{
		if (p->value() == "all")
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;

			sl << bncs_string("workspace=%1").arg(m_workspace);
			sl << bncs_string("function=%1").arg(m_function);
			sl << bncs_string("width=%1").arg(m_width);
			sl << bncs_string("height=%1").arg(m_height);

			return sl.toString('\n');
		}
	}
	else if (p->command() == "instance" && p->value() != m_instance)
	{	// Our instance is being set/changed
		// we get our instance elsewhere, this is not required
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "workspace")
	{	// Set the workspace
		// to ensure we either set manually or automatic
		// we need to check if the workspace is already set

		if (!m_workspace.length())
		{
			m_workspace = p->value();
			timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
		}
	}
	else if (p->command() == "width")
	{	// set the width
		m_width = p->value();
		setupPanel();
	}
	else if (p->command() == "height")
	{	// set the height
		m_height = p->value();
		setupPanel();
	}
	else if (p->command() == "function")
	{	// set the function
		// this is not utilised until a workspace is sent to us
		// and that will always be after a delay
		m_function = p->value();
	}
	else if (p->command() == "dest")
	{	// set the destination
		// we need to clear down if this is not our selected dest
		debug("tagmv::PCB::dest %1 supplied, our dest %2", p->value(), m_dest);
		if (m_dest != p->value())
		{
			textPutAll("statesheet", "enum_deselected", PNL_MAIN, "tag_channel_");
			m_dest = -1;
		}
		// check if we own a tile with that dest ?
	}
	else if (p->command() == "take")
	{	// If we are selected - do the crosspoint
		debug("tagmv::PCB::take requested, source %1, dest %2", p->value(), m_dest);
		if (m_dest > 0)
		{
			debug("tagmv::PCB::take actioned, source %1, dest %2", p->value(), m_dest);
			infoWrite(m_packager.destDev(m_dest), bncs_string("index=%1").arg(p->value()), m_packager.destSlot(m_dest), true);
			textPutAll("statesheet", "enum_deselected", PNL_MAIN, "tag_channel_");
			//m_dest = -1;
		}
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if (p->command() == "_events")
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "dest=*";

		return sl.toString('\n');
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if (p->command() == "_commands")
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;

		sl << "width=[width]";
		sl << "height=[height]";
		sl << "dest=[dest]";
		sl << "take=[source]";

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void tagmv::timerCallback(int id)
{
	switch (id)
	{
	case TIMER_SETUP:
		timerStop(id);

		setupPanel();
		if (m_packager.isValid())
		{
			debug("tagmv::TCB::TIMER_SETUP::trying workspace function %1.%2", m_workspace, m_function);
			tDest = bncs_config(bncs_string("workspace_resources/%1.%2").arg(m_workspace).arg(m_function)).attr("value").toInt();
			if (tDest)
			{
				debug("tagmv::TCB::TIMER_SETUP::workspace function %1.%2 sucess, found dest %3", m_workspace, m_function, tDest);
				// holding code for testing
				// if we have a valid tDest then we should try to get our AMU entry
				int decDest = bncs_config(bncs_string("workspace_resources/%1._decoders.%2").arg(m_workspace).arg(m_function)).attr("value").toInt();
				if (decDest)
				{
					debug("tagmv::TCB::TIMER_SETUP::workspace function %1._decoder.%2 sucess, found decoder dest %3", m_workspace, m_function, decDest);
				}
				else
				{
					debug("tagmv::TCB::TIMER_SETUP::workspace function %1._decoder.%2 - no decoder dest found", m_workspace, m_function);
				}

				debug("tagmv::TCB::TIMER_SETUP::workspace function %1.%2 invoked, polling dest %3 (%4 / %5)", m_workspace, m_function, tDest, m_packager.destDev(tDest),m_packager.destSlot(tDest));
				status(bncs_string("tagmv--TCB|Function %1|PollDest %2 (%3/%4)").arg(m_function).arg(tDest).arg(m_packager.destDev(tDest)).arg(m_packager.destSlot(tDest)), true);
				//packagerPoll(m_packager_status, tDest, tDest);
				packagerRegister(m_packager, tDest, tDest, false);
				packagerPoll(m_packager, tDest, tDest);
			}
			else
			{
				status(bncs_string("tagmv--TCB|Function %1|No Dest").arg(m_function), true);
			}
		}
		break;

	case TIMER_RETRY:
		timerStop(id);
		httpGetString(m_tagmv.requestURI());
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}

void tagmv::httpCallback(httpNotify *h)
{
	// It would be good for this to be handled by bncs_tagmv
	// To do that we would need to include the script helper
	// in order to pass an httpNotify
	// but we would also need to report back as to *what* was processed
	// in order to reset/trigger
	// let's leave it here for now

	// with http callbacks, the errors are reset so you cannot rely on multiple reads
	// you need to copy and store locally should you want to use the data for multiple purposes
	bncs_string h_err = h->err();
	/*
	bncs_string h_data = h->data();
	bncs_string h_data_display = h_data;
	h_data_display.replace(13, 'r');
	h_data_display.replace(10, 'n');
	bool h_ok = h->ok();
	bool h_string = h->string();
	bncs_string h_url = h->url();

	debug("tagmv::HCB::RX %1", h_ok ? "OK" : "FAIL");
	debug("tagmv::HCB::URL <%1>", h_url);
	debug("tagmv::HCB::Type %1", h_string ? "STRING" : "FILE");
	debug("tagmv::HCB::Data <%1>", h_data_display);
	debug("tagmv::HCB::Error <%1>", h_err);
	*/

	m_dest = -1;

	if (!h->ok() || h->data() == "null")
	{	// ERROR CONDITION
		debug("tagmv::httpCallback ERROR Receiving data: Error:<%1>, URL <%2>", h_err, h->url());

		if (h->url().find("outputs") > -1)
		{	// check for error on return call for our head output
			debug("tagmv::httpCallback outputs <%1>", h->url());
			m_h_data = h->data();		// store for later since it is an output http response
			m_h_err = h_err;		// store for later since it is an output http response
			m_h_ok = h->ok();			// store for later since it is an output http response
			m_h_string = h->string();	// store for later since it is an output http response
			m_h_url = h->url();		// store for later since it is an output http response
			stats();
		}

		if (!m_tagmv.isValid())
		{	// only report this if the head is not valid
			status(bncs_string("HTTP data error| |%1").arg(h_err));
			debug("tagmv::httpCallback sent error to status");
		}

		// lets check if it is a missing layout
		// we need to be canny on our search
		// we can look for "/layout/" in the URL and be sure it is a layout
		// an extra check for validity is if our map sizes look sensible
		if (h->url().find("/layout/") > -1)
		{	// an extra check for validity is if our map sizes look sensible, layout items should be less than layout list
			if (m_layout.size() < m_layouts.size())
			{	// so we are a layout, extract the id and save in our map
				bncs_stringlist id = bncs_stringlist(h->url(), '/');
				debug("tagmv::httpCallback found erroneous layout, URL split qty %1", id.count());
				// our layout id should be the last item in the list
				m_layout[id[id.count() - 1]] = bncs_string("ERROR:|%1").arg(h_err);
				debug("tagmv::httpCallback layout id %1 marked as %2", id[id.count() - 1], m_layout[id[id.count() - 1]]);

				// check for a full house
				if (m_layout.size() == m_layouts.size())
				{
					m_isLayoutsValid = true;
				}
				////debug("tagmv::httpCallback found %1 layouts of %2", (int)m_layout.size(), (int)m_layouts.size());
			}
		}
		else
		{
			// so if we are here then the main layout has not loaded
			m_retry++;
			status(bncs_string("Multiviewer %1 found| |Requesting layout for head %2| |Retry count %3").arg(m_tagmv.instance()).arg(m_tagmv.head()).arg(m_retry));
			timerStart(TIMER_RETRY, TIMER_RETRY_DURATION);
			textPut("monitor", tDest, PNL_MAIN, "taglayout");	// tell taglayout we have a mv dest but no responding mv
			stats();
		}
	}
	else
	{	// NON ERROR CONDITION
		// we have valid http data but we need to resolve what type it is
		// the 3 formats are outputs, layouts, and layout
		//
		// outputs
		// http://{driver_address}:{driver_port}/outputs/{id}/layout
		//
		// layouts
		// http://{driver_address}:{driver_port}/layouts
		//
		// layout
		// http://{driver_address}:{driver_port}/layout/{id}
		//
		// if we search for "outputs" that will only catch a head output, so that needs to be done first
		// if we then search for "layouts" that will only catch the layouts set, so do that next
		// a final check for "layout" would verify an individual layout

		// #### OUTPUTS ####
		if (h->url().find("outputs") > -1)
		{	// this should be the return call for our head output
			debug("tagmv::httpCallback outputs <%1>", h->url());
			m_h_data = h->data();		// store for later since it is an output http response
			m_h_err = h_err;		// store for later since it is an output http response
			m_h_ok = h->ok();			// store for later since it is an output http response
			m_h_string = h->string();	// store for later since it is an output http response
			m_h_url = h->url();		// store for later since it is an output http response
			m_retry = 0;
			if (m_tagmv.isValid() && h->url().find(m_tagmv.headId()) > -1)
			{	// so we are valid and we have the correct head ID
				m_current_layout = h->data();
				debug("tagmv::httpCallback output for our head id <%1>, current_layout set to %2", m_tagmv.headId(), m_current_layout);
				status(bncs_string("Multiviewer %1|Head %2|Head ID %3").arg(m_tagmv.instance()).arg(m_tagmv.head()).arg(m_tagmv.headId()));
				textPut("layout", m_current_layout, PNL_MAIN, "taglayout");
			}
			stats();
		}
		// #### LAYOUTS ####
		else if (h->url().find("layouts") > -1)
		{	// store the layouts available
			debug("tagmv::httpCallback layouts <%1>", h->data());
			m_layouts.clear();
			m_layout.clear();
			m_isLayoutsValid = false;

			bncs_stringlist layouts = bncs_stringlist(h->data());
			if (layouts.count())
			{
				int count = 0;
				for (bncs_stringlistIterator slit = layouts.begin(); slit != layouts.end(); slit++)
				{
					count++;
					m_layouts[count] = *slit;

					// now send for the layout
					////debug("tagmv::httpCallback requesting layout %1", m_layouts[count]);
					httpGetString(m_tagmv.layoutURI(m_layouts[count]));
				}
				////debug("tagmv::httpCallback requested %1 layouts", count);
			}
		}
		// #### LAYOUT ####
		else if (h->url().find("layout"))
		{	// an individual layout
			//debug("tagmv::httpCallback layout <%1>", h->url());
			bncs_stringlist id = bncs_stringlist(h->url(), '/');
			//debug("tagmv::httpCallback layout URL split qty %1", id.count());
			// our layout id should be the last item in the list
			m_layout[id[id.count() - 1]] = h->data();
			//debug("tagmv::httpCallback layout id %1 is %2", id[id.count() - 1], h->data().cleanup());

			// check for a full house
			if (m_layout.size() == m_layouts.size())
			{
				m_isLayoutsValid = true;
			}
			////debug("tagmv::httpCallback found %1 layouts of %2", (int)m_layout.size(), (int)m_layouts.size());
		}
		// #### UNRESOLVED ####
		else
		{	// unresolved http callback
			debug("tagmv::httpCallback UNRESOLVED <%1>", h->url());
		}

	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void tagmv::status(bncs_string text, bool add)
{
	text.replace(13, 'r');
	text.replace(10, 'n');

	if (add)
	{
		m_status = m_status + "|" + text;
	}
	else
	{
		m_status = text;
	}

	bncs_stringlist sl(m_status,'|');
	while (sl.count() > 12)
	{	// trim to 10 entries
		sl.deleteItem(0);
	}
	m_status = sl.toString('|');

	textPut("status", m_status.cleanup(), PNL_MAIN, "taglayout");
}

void tagmv::setupPanel(void)
{
	setSize(m_width, m_height);
	textPut("width", m_width, PNL_MAIN, "taglayout");
	textPut("height", m_height, PNL_MAIN, "taglayout");
	status("tagmv--setupPanel()");
	status(bncs_string("WS Pkgr %1|Cfg %2|RtrComp %3%4|StatComp %5%6|Workspace %7|Size %8x%9")
		.arg(m_workstation_packager)
		.arg(m_packager.config())
		.arg(m_packager.instance())
		.arg(m_packager.isValid() ? "(v)" : "(x)")
		.arg(m_packager_status.instance())
		.arg(m_packager_status.isValid() ? "(v)" : "(x)")
		.arg(m_workspace.length() ? m_workspace : "undefined")
		.arg(m_width)
		.arg(m_height),
		true);
}

void tagmv::init(void)
{
	status("tagmv--init()");
	// get our packager and make a bncs_packager
	// as it stands this is NOT the root composite (which is c_packager)
	// it is the router group (c_packager_router)
	m_workstation_packager = getWorkstationSetting(SETTING_PACKAGER_INSTANCE);
	debug("tagmv??init()::workstation packager is %1", m_workstation_packager);
	m_packager = bncs_packager(m_workstation_packager);
	debug("tagmv::init()::m_packager is %1", m_packager.isValid() ? "valid" : "NOT valid");
	if (m_packager.isValid())
	{
		// now get our packager status and make a bncs_packager status
		// we need to access this using the root composite
		// which we have no reference to within the settings
		// so currently the root composite is hard coded
		// the "dest_status" group instance is obtained from that root composite instance
		m_packager_status = bncs_packager(m_workstation_packager, "dest_status");
		debug("tagmv::init()::m_packager_status is %1", m_packager_status.isValid() ? "valid" : "NOT valid");
		if (m_packager_status.isValid())
		{
			packagerRegister(m_packager_status, 1, m_packager_status.numberOfPackages(), true);
		}
	}



	tDest = -1;
	tSource = -1;
	m_dest = -1;

	m_tagmv = bncs_tagmv();
	m_width = 480;
	m_height = 270;
	m_isMV = true;

	m_tile2dest.clear();
	m_dest2tile.clear();
	m_dest4tile.clear();
	m_isLayoutsValid = false;

	m_workspace = "";
	m_geometry = "";

	m_retry = 0;

	m_v_index = 0;
	m_hd_section_01_Instance = "";
	m_hd_device = 0;
	m_adjust = "";
}

void tagmv::renderLayouts(void)
{
	panelPopup(PNL_POPUP, "layouts.bncs_ui");
	setLayoutsPage(0);
}

void tagmv::setLayoutsPage(int page)
{
	int offset = page * LAYOUTS_PAGESIZE;

	if ((int)m_layouts.size() > offset && page >= 0)
	{
		if (page)
		{
			controlEnable(PNL_POPUP, BUTTON_LAYOUTS_LESS);
		}
		else
		{
			controlDisable(PNL_POPUP, BUTTON_LAYOUTS_LESS);
		}
		controlEnable(PNL_POPUP, BUTTON_LAYOUTS_MORE);

		debug("tagmv::setLayoutsPage::setting layouts %1 to %2", (offset + 1), (offset + 1 + LAYOUTS_PAGESIZE));
		for (int i = 0; i < LAYOUTS_PAGESIZE; i++)
		{
			int layout = i + 1 + offset; // layout number
			int button = i + 1;	// button number
			if (layout <= (int)m_layouts.size())
			{
				debug("tagmv::setLayoutsPage::layout %1 starts with %2", layout, m_layout[m_layouts[layout]].left(10));
				if (m_layout[m_layouts[layout]].startsWith("ERROR:"))
				{
					textPut("status", m_layout[m_layouts[layout]].cleanup(), PNL_POPUP, bncs_string("layout_%1").arg(bncs_string(button, '0', 2)));
				}
				else
				{
					textPut("layout", m_layout[m_layouts[layout]], PNL_POPUP, bncs_string("layout_%1").arg(bncs_string(button, '0', 2)));
				}
			}
			else
			{
				debug("tagmv::setLayoutsPage::layout %1 not used, max is %2", layout, (int)m_layouts.size());
				textPut("status", " ", PNL_POPUP, bncs_string("layout_%1").arg(bncs_string(button, '0', 2)));
				// this means we have no more
				controlDisable(PNL_POPUP, BUTTON_LAYOUTS_MORE);
			}
		}

		m_layoutsPage = page;

		textPut("text", bncs_string("%1   -   Page %2 of %3, Total layouts %4").arg(m_tagmv.altId()).arg(m_layoutsPage + 1).arg((int)(m_layouts.size() / LAYOUTS_PAGESIZE) + 1).arg((int)m_layouts.size()), PNL_POPUP, BUTTON_LAYOUTS_INFO);
	}
}


/*
bool tagmv::infoPackagerPoll(bncs_packager &packager, int index)
{
debug("tagmv::infoPackagerPoll::index %1, packager.device %2, packager.slot %3 ", index, packager.destDev(index), packager.destSlot(index));
if (packager.isValid() && index > 0 && index < 64001)
{
infoPoll(packager.destDev(index), packager.destSlot(index), packager.destSlot(index));
return true;
}
else
{
return false;
}
}
*/

bncs_string tagmv::getName(int index, int db)
{
	bncs_string ret = "---";
	if (m_packager.isValid())
	{
		routerName(m_packager.device(1), db, index, ret);
	}
	return ret;
}

int tagmv::textPutAll(const bncs_string & param, const bncs_string & value, const bncs_string & pnl, const bncs_string & filter)
{
	bncs_stringlist sl = getIdList(pnl, filter);
	int c = 0;

	if (sl.count())
	{
		for (bncs_stringlistIterator slit = sl.begin(); slit != sl.end(); slit++)
		{
			c++;
			textPut(param, value, pnl, *slit);
		}
	}
	return c;
}

int tagmv::controlDestroyAll(const bncs_string & pnl, const bncs_string & filter)
{
	bncs_stringlist sl = getIdList(pnl, filter);
	int c = 0;

	if (sl.count())
	{
		for (bncs_stringlistIterator slit = sl.begin(); slit != sl.end(); slit++)
		{
			if (!controlDestroy(pnl, *slit))
			{
				c++;
			}
		}
	}
	return c;
}

bool tagmv::packagerUnregister(bncs_packager & packager)
{
	if (packager.isValid())
	{
		for (int device = 1; device <= packager.deviceCount(); device++)
		{
			infoUnregister(packager.device(device));
		}

		return true;
	}
	else
	{
		return false;
	}
}

bool tagmv::packagerRegister(bncs_packager & packager, int start, int end, bool add)
{
	if (packager.isValid() && start > 0 && end >= start && end <= packager.numberOfPackages())
	{
		int groupDev = packager.destDev(start);
		int groupStart = start;

		for (int dest = start; dest <= end; dest++)
		{
			if (packager.destDev(dest) != groupDev)
			{	// traversed a boundary
				infoRegister(groupDev, packager.destSlot(groupStart), packager.destSlot(dest - 1), add);
				debug("tagmv::packagerRegister::Registered %1 from %2 to %3", groupDev, packager.destSlot(groupStart), packager.destSlot(dest - 1));
				groupDev = packager.destDev(dest);
				groupStart = dest;
			}

			if (dest == end)
			{	// register to the end
				infoRegister(groupDev, packager.destSlot(groupStart), packager.destSlot(dest), add);
				debug("tagmv::packagerRegister::Registered %1 from %2 to %3", groupDev, packager.destSlot(groupStart), packager.destSlot(dest));
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}

bool tagmv::packagerPoll(bncs_packager & packager, int start, int end)
{
	if (packager.isValid())
	{
		for (int dest = start; dest <= end; dest++)
		{
			infoPoll(packager.destDev(dest), packager.destSlot(dest), packager.destSlot(dest));
		}

		return true;
	}
	else
	{
		return false;
	}
}

void tagmv::clearHttpData(void)
{
	m_h_data = "";		
	m_h_err = "";		
	m_h_ok = false;		
	m_h_string = false;	
	m_h_url = "";		
}

void tagmv::stats(void)
{
	bncs_string name;

	textPutAll("text", "", PNL_STATS, "stat_");
	textPutAll("statesheet", "enum_notimportant", PNL_STATS, "stat_");
	textPut("clear", PNL_MAIN, "stat_layout");

	textPutAll("instance", m_tagmv.headInstance(), PNL_STATS, "mv_");
	debug("tagmv::stats::target instance %1", m_tagmv.headInstance());

	textPut("text", m_workstation_packager, PNL_STATS, "stat_workstation_packager_instance");
	textPut("text", m_packager.instance(), PNL_STATS, "stat_packager_instance");
	if (m_packager.isValid())
	{
		textPut("statesheet", "enum_ok", PNL_STATS, "stat_packager_instance");
	}
	else
	{
		textPut("statesheet", "enum_alarm", PNL_STATS, "stat_packager_instance");
	}
	textPut("text", m_packager_status.instance(), PNL_STATS, "stat_status_instance");
	if (m_packager_status.isValid())
	{
		textPut("statesheet", "enum_ok", PNL_STATS, "stat_status_instance");
	}
	else
	{
		textPut("statesheet", "enum_alarm", PNL_STATS, "stat_status_instance");
	}
	textPut("text", m_workspace, PNL_STATS, "stat_workspace");
	textPut("text", m_function, PNL_STATS, "stat_function");
	textPut("text", m_width, PNL_STATS, "stat_width");
	textPut("text", m_height, PNL_STATS, "stat_height");
	textPut("text", tDest, PNL_STATS, "stat_tDest");
	routerName(m_packager.deviceNames(), 0, tDest, name);
	textPut("text", name, PNL_STATS, "stat_tDest_name");
	textPut("text", bncs_string("%1/%2").arg(m_packager.destDev(tDest)).arg(m_packager.destSlot(tDest)), PNL_STATS, "stat_tDest_loc");
	textPut("text", m_v_index, PNL_STATS, "stat_v_index");
	routerName(m_hd_device, 0, m_v_index, name);
	textPut("text", name, PNL_STATS, "stat_v_index_name");
	textPut("text", m_hd_section_01_Instance, PNL_STATS, "stat_hd_section_01_Instance");
	textPut("text", m_hd_device, PNL_STATS, "stat_hd_device");
	textPut("text", m_adjust, PNL_STATS, "stat_adjust");
	if (m_tagmv.isValid())
	{
		textPut("statesheet", "enum_ok", PNL_STATS, "stat_adjust");
	}
	else
	{
		textPut("statesheet", "enum_alarm", PNL_STATS, "stat_adjust");
	}
	textPut("text", m_tagmv.requestURI(), PNL_STATS, "stat_requestURI");
	textPut("text", m_retry, PNL_STATS, "stat_retry");

	if (m_h_data.length())
	{
		textPut("text", m_h_data.cleanup(), PNL_STATS, "stat_h_data");
	}
	else
	{
		textPut("text", "<NULL>", PNL_STATS, "stat_h_data");
	}
	if (m_h_err.length())
	{
		textPut("text", m_h_err.cleanup(), PNL_STATS, "stat_h_err");
	}
	else
	{
		textPut("text", "<NULL>", PNL_STATS, "stat_h_err");
	}
	textPut("text", m_h_ok ? "OK" : "FAIL", PNL_STATS, "stat_h_ok");
	textPut("text", m_h_string ? "STRING" : "FILE", PNL_STATS, "stat_h_string");
	if (m_h_url.length())
	{
		textPut("text", m_h_url, PNL_STATS, "stat_h_url");
	}
	else
	{
		textPut("text", "<NULL>", PNL_STATS, "stat_h_url");
	}

	textPut("text", tSource, PNL_STATS, "stat_tSource");
	textPut("text", m_dest, PNL_STATS, "stat_m_dest");

	int r = 0;
	for (map<int, int>::iterator it = m_tile2dest.begin(); it != m_tile2dest.end(); it++)
	{
		//debug("tagmv::stats::adding %1>%2", it->first, it->second);
		textPut(bncs_string("cell.%1.%2").arg(r).arg(0), bncs_string("%1 > %2").arg(it->first).arg(it->second), PNL_STATS, "stat_layout");
		r++;
	}
	r = 0;
	for (map<int, bncs_stringlist>::iterator it = m_dest2tile.begin(); it != m_dest2tile.end(); it++)
	{
		//debug("tagmv::stats::adding %1>%2", it->first, it->second.toString());
		textPut(bncs_string("cell.%1.%2").arg(r).arg(1), bncs_string("%1 > %2").arg(it->first).arg(it->second), PNL_STATS, "stat_layout");
		r++;
	}
	r = 0;
	for (map<int, int>::iterator it = m_dest4tile.begin(); it != m_dest4tile.end(); it++)
	{
		//debug("tagmv::stats::adding %1>%2", it->first, it->second);
		textPut(bncs_string("cell.%1.%2").arg(r).arg(2), bncs_string("%1 > %2").arg(it->first).arg(it->second), PNL_STATS, "stat_layout");
		r++;
	}
}
