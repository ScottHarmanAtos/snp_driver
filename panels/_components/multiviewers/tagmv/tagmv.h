#ifndef tagmv_INCLUDED
#define tagmv_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
#ifdef DOEXPORT_SCRIPT
#define EXPORT_SCRIPT __declspec(dllexport) 
#else
#define EXPORT_SCRIPT 
#endif
#else 
#define EXPORT_SCRIPT
#endif

class tagmv : public bncs_script_helper
{
public:
	tagmv(bncs_client_callback * parent, const char* path);
	virtual ~tagmv();

	void buttonCallback(buttonNotify *b);
	int revertiveCallback(revertiveNotify * r);
	void databaseCallback(revertiveNotify * r);
	bncs_string parentCallback(parentNotify *p);
	void timerCallback(int);
	void httpCallback(httpNotify *p);

private:
	bncs_string m_instance;
	bncs_string m_workspace;
	bncs_string m_function;
	bncs_string m_workstation_packager;
	bncs_packager m_packager;
	bncs_packager m_packager_status;
	bncs_tagmv m_tagmv;
	bncs_string m_current_layout;
	
	map<int, int> m_tile2dest;	// tile number >> destination on router that feeds that tile
	map<int, bncs_stringlist> m_dest2tile;	// destination on router that feeds a tile >> tile number
	map<int, int> m_dest4tile;	// tile number >> destination on router that we use to switch the content that feeds that tile 
	map<int, bncs_string> m_layouts;	// layout number >> layout id
	map<bncs_string, bncs_string> m_layout;	// layout id >> layout data
	bool m_isLayoutsValid;	// true when all the layouts have been accounted for
	bncs_string m_geometry;	// geometry string for active layout
	int m_layoutsPage;	// page we are on in the layouts display

	int m_dest_pkg;
	bncs_stringlist m_sl;
	bool m_isMV;

	int tDest;								// output side tracing - virtual packager dest driving our xs decoder
	bncs_string m_v_index;					// output side tracing - traced real video index
	bncs_string m_hd_section_01_Instance;	//
	int m_hd_device;						// 
	bncs_string m_adjust;					//
	int tSource;							// input side tracing
	int m_dest;								// selected tile calculated virtual dest
	bncs_string m_h_data;
	bncs_string m_h_err;
	bool m_h_ok;
	bool m_h_string;
	bncs_string m_h_url;

	int m_width;	// panel width
	int m_height;	// panel height

	int m_retry;	// retry count;

	bncs_string m_status;	// status text

	void status(bncs_string text, bool add = false);
	void setupPanel(void);

	void init(void);
	//bool infoPackagerPoll(bncs_packager &packager, int index);
	bncs_string getName(int index, int db);

	int textPutAll(const bncs_string & param, const bncs_string & value, const bncs_string & pnl, const bncs_string & filter = "");
	int controlDestroyAll(const bncs_string & pnl, const bncs_string & filter);

	bool packagerUnregister(bncs_packager & packager);
	bool packagerRegister(bncs_packager & packager, int start, int end, bool add = false);
	bool packagerPoll(bncs_packager & packager, int start, int end);
	void clearHttpData(void);
	void stats(void);

	void renderLayouts(void);
	void setLayoutsPage(int page);
};


#endif // tagmv_INCLUDED