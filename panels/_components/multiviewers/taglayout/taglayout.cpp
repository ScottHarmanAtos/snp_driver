#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "mv_layout.h"
#include "taglayout.h"

#define PNL_MAIN	1

#define TIMER_SETUP				1
#define TIMER_SETUP_DURATION	250
#define TIMER_PUSHHOLD			2
#define TIMER_PUSHHOLD_DURATION	2000
#define TIMER_LAYOUT			3
#define TIMER_LAYOUT_DURATION	TIMER_SETUP_DURATION + 1

#define CONTENT_TYPE_TEXT		"text"
#define CONTENT_TYPE_CLOCK		"clock"
#define CONTENT_TYPE_CHANNEL	"channel"
#define CONTENT_TYPE_UNKNOWN	"unknown"

#define LAYOUT_BORDER_SIZE		5

#define TOPSTRAP_HEIGHT			40
#define BUTTON_LAYOUT_WIDTH		80
#define BUTTON_MONITOR_WIDTH	80

#define BTN_LAYOUT_NAME			"tag_topstrap_layoutName"
#define BTN_LAYOUT_SELECT		"tag_topstrap_layoutSelect"
#define BTN_MONITOR_SELECT		"tag_topstrap_monitorSelect"
#define BTN_STATUS				"tag_status"
#define BTN_BGND				"tag_mv_bgnd"
#define BTN_TOPSTRAP_PREFIX		"tag_topstrap_"
#define BTN_TAG_PREFIX			"tag_mv_"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(taglayout)

// constructor - equivalent to ApplCore STARTUP
taglayout::taglayout(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path)
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	panelShow(PNL_MAIN, "main.bncs_ui");

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
	//	setSize( 1024,668 );		// set the size explicitly
	//	setSize( PNL_MAIN );		// set the size to the same as the specified panel

	m_width = 480;
	m_height = 270;
	m_isText = true;
	m_isNamePushHold = false;
	m_buttons = "all";
}

// destructor - equivalent to ApplCore CLOSEDOWN
taglayout::~taglayout()
{
}

// all button pushes and notifications come here
void taglayout::buttonCallback(buttonNotify *b)
{
	b->dump("taglayout::BCB::Dump:");
	//debug("taglayout::BCB::pnl %1, id %2, cmd %3, val %4, subs %5, sub(0) %6, sub(1) %7", b->panel(), b->id(), b->command(), b->value(), b->subs(), b->sub(0), b->sub(1));
	if (b->panel() == PNL_MAIN)
	{
		if (b->id() == BTN_LAYOUT_NAME)
		{	// trap for layout buttons first
			if (b->command() == "button")
			{
				if (b->value() == "pressed")
				{
					timerStop(TIMER_PUSHHOLD); 
					m_isNamePushHold = false;
					timerStart(TIMER_PUSHHOLD, TIMER_PUSHHOLD_DURATION);
				}
				else
				{
					timerStop(TIMER_PUSHHOLD);
					textPut("statesheet", "enum_notimportant", PNL_MAIN, BTN_LAYOUT_NAME);
					if (m_isNamePushHold)
					{ // push_hold code
						m_isNamePushHold = false;
						hostNotify(bncs_string("name.button=%1").arg("pushhold"));
					}
					else
					{ // normal release code
						hostNotify(bncs_string("name.button=%1").arg(b->value()));
					}
				}
			}
		}
		else if (b->id() == BTN_LAYOUT_SELECT)
		{	// trap for layout buttons first
			hostNotify(bncs_string("layout.button=%1").arg(b->value()));
		}
		else if (b->id() == BTN_MONITOR_SELECT)
		{	// trap for layout buttons first
			hostNotify(bncs_string("monitor.button=%1").arg(b->value()));
		}
		else if (b->id().startsWith(bncs_string("%1%2").arg(BTN_TAG_PREFIX).arg(CONTENT_TYPE_CHANNEL)))
		{
			// destinations are called {BTN_TAG_PREFIX}{CONTENT_TYPE_CHANNEL}_{INDEX}
			hostNotify(bncs_string("dest.%1=%2").arg(b->id().firstInt()).arg(m_sldests.getNamedParam(b->id().firstInt())));
		}
		else if (b->id().startsWith(bncs_string("%1%2").arg(BTN_TAG_PREFIX).arg(CONTENT_TYPE_CLOCK)))
		{
			// clocks are called {BTN_TAG_PREFIX}{CONTENT_TYPE_CLOCK}_{INDEX}
			hostNotify(bncs_string("clock.%1=%2").arg(b->id().firstInt()).arg(m_sldests.getNamedParam(b->id().firstInt())));
		}
		else if (b->id().startsWith(bncs_string("%1%2").arg(BTN_TAG_PREFIX).arg(CONTENT_TYPE_TEXT)))
		{
			// text blocks are called {BTN_TAG_PREFIX}{CONTENT_TYPE_TEXT}_{INDEX}
			hostNotify(bncs_string("text.%1=%2").arg(b->id().firstInt()).arg(m_sldests.getNamedParam(b->id().firstInt())));
		}
	}
}

// all revertives come here
int taglayout::revertiveCallback(revertiveNotify * r)
{
	return 0;
}

// all database name changes come back here
void taglayout::databaseCallback(revertiveNotify * r)
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string taglayout::parentCallback(parentNotify *p)
{
	p->dump("taglayout::PCB::Dump:");
	//debug("taglayout::PCB::subs() <%1>, sub(0) <%2>, sub(1) <%3>, sub(2) <%4>", p->subs(), p->sub(0), p->sub(1), p->sub(2));
	if (p->command() == "return")
	{
		if (p->value() == "all")
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;

			sl << bncs_string("width=%1").arg(m_width);
			sl << bncs_string("height=%1").arg(m_height);
			sl << bncs_string("show_text=%1").arg(m_isText ? "true" : "false");
			sl << bncs_string("buttons=%1").arg(m_buttons);

			return sl.toString('\n');
		}
		else if (p->value() == "dests")
		{
			return(bncs_string("%1=%2").arg(p->value()).arg(m_sldests.toString()));;
		}
	}
	else if (p->command() == "instance" && p->value() != m_instance)
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
	}

	else if (p->command() == "layout")
	{	// Persisted value or 'Command' being set here
		m_layout = p->value();
		timerStart(TIMER_LAYOUT, TIMER_LAYOUT_DURATION);
	}
	else if (p->command() == "status")
	{	// Persisted value or 'Command' being set here
		render(p->value(), true);
	}
	else if (p->command() == "width")
	{	// set the width
		m_width = p->value();
		timerStart(TIMER_SETUP,TIMER_SETUP_DURATION);
	}
	else if (p->command() == "height")
	{	// set the height
		m_height = p->value();
		timerStart(TIMER_SETUP, TIMER_SETUP_DURATION);
	}
	else if (p->command() == "show_text")
	{
		bncs_string show_text = p->value().lower();
		if (show_text.startsWith("t") || show_text.startsWith("y"))
		{
			m_isText = true;
		}
		else
		{
			m_isText = false;
		}
	}
	else if (p->command() == "buttons")
	{
		m_buttons = p->value().lower();
	}
	else if (p->command() == "monitor")
	{
		renderTopstrap(true, true);
	}

	else if (p->command() == "dest" && p->subs() == 2)
	{	// dest command
		if (p->sub(1) == "text")
		{
			textPut("text", p->value(), PNL_MAIN, bncs_string("%1%2_%3").arg(BTN_TAG_PREFIX).arg(CONTENT_TYPE_CHANNEL).arg(p->sub(0)));
			debug("taglayout::PCB::dest %1 text set to %2", bncs_string("%1%2_%3").arg(BTN_TAG_PREFIX).arg(CONTENT_TYPE_CHANNEL).arg(p->sub(0)), p->value());
		}

	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if (p->command() == "_events")
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "dest=*";

		return sl.toString('\n');
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if (p->command() == "_commands")
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;

		sl << "layout=[value]";
		sl << "error=[value]";

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void taglayout::timerCallback(int id)
{
	switch (id)
	{
	case TIMER_SETUP:
		timerStop(id);
		controlDestroy(PNL_MAIN, BTN_STATUS);
		setSize(m_width, m_height);
		renderStatus();
		renderTopstrap(false);
		break;

	case TIMER_PUSHHOLD:
		timerStop(id);
		textPut("statesheet", "enum_selected", PNL_MAIN, BTN_LAYOUT_NAME);
		m_isNamePushHold = true;
		break;

	case TIMER_LAYOUT:
		timerStop(id);
		render(m_layout);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

void taglayout::render(bncs_string data, bool isStatus)
{
	controlDestroyAll(PNL_MAIN, BTN_TAG_PREFIX);
	bncs_string status_text = "";

	if (isStatus)
	{
		status_text = data;
	}
	else
	{
		m_mv_layout = mv_layout(data);

		if (m_mv_layout.isParseValid())
		{
			if (m_mv_layout.tiles.size() > 0)
			{
				bncs_stringlist resolution = bncs_stringlist(m_mv_layout.resolution, 'x');

				if (resolution.count() == 2)
				{
					m_sldests.clear();

					//float xRes = resolution[0].firstInt();
					float xScale = ((float)m_width - 2 - (2 * LAYOUT_BORDER_SIZE)) / resolution[0].firstInt();
					//float yRes = resolution[1].firstInt();
					float yScale = ((float)m_height - TOPSTRAP_HEIGHT - 2 - (3 * LAYOUT_BORDER_SIZE)) / resolution[1].firstInt();
					//debug("tagmv::render()::xRes %1, yRes %2, width %3, height %4, xScale %5, yScale %6", resolution[0], resolution[1], m_width, m_height, to_string(xScale), to_string(yScale));

					controlCreate(PNL_MAIN,
						BTN_BGND,
						"bncs_control",
						0,
						0,
						m_width,
						m_height,
						"style=label\nstylesheet=groupbox\nnotify.released=false");

					renderTopstrap();

					for (list<tile>::iterator p = m_mv_layout.tiles.begin(); p != m_mv_layout.tiles.end(); p++)
					{
						debug("tagmv::render()::tile dest %1, tile index %2", p->destination, p->index);
						bncs_string text;
						bncs_string type = p->content_type.lower();

						if (type == CONTENT_TYPE_CLOCK || p->content_type.lower() == "clock.tile")
						{
							text = "Clock";
						}
						else if (type == CONTENT_TYPE_TEXT)
						{
							text = "Text";
						}
						else if (type == CONTENT_TYPE_CHANNEL)
						{
							text = bncs_string("d%1").arg(p->destination);
							m_sldests << bncs_string("%1=%2").arg(p->index).arg(p->destination);
						}
						else
						{
							type = CONTENT_TYPE_UNKNOWN;
							text = "Unknown";
						}

						controlCreate(PNL_MAIN,
							bncs_string("%1%2_%3").arg(BTN_TAG_PREFIX).arg(type).arg(p->index),
							"uiHost",
							(p->x_axis * xScale + 0.5) + 1 + LAYOUT_BORDER_SIZE,
							(p->y_axis * yScale + 0.5) + TOPSTRAP_HEIGHT + (2 * LAYOUT_BORDER_SIZE),
							p->width * xScale + 0.5,
							p->height * yScale + 0.5,
							bncs_string("scaleWithCanvas=true\nbncs_ui=/_components/multiviewers/taglayout/tile.bncs_ui\ntext=%1").arg(m_isText ? text : ""));
					}

					// since we have a good set of tiles, clear the status
					status("", false);

					// label the layout
					textPut(bncs_string("text=%1").arg(m_mv_layout.label), PNL_MAIN, BTN_LAYOUT_NAME);

					// notify the host
					hostNotify(bncs_string("dests=%1").arg(m_sldests.toString()));
				}
				else
				{
					status_text = "HTTP data error| |No resolution specified";
				}
			}
			else
			{
				status_text = "HTTP data error| |No tiles specified";
			}
		}
		else
		{
			status_text = "HTTP data error| |Data parse failed";
		}
	}

	if (status_text.length())
	{
		status(status_text);
	}
}

void taglayout::status(bncs_string text, bool show)
{
	debug("taglayout::status::%1SHOW::<%2>", show ? "" : "DO NOT ", text);
	m_status = text;
	text = text + "| ";
	textPut("text", text.cleanup(), PNL_MAIN, BTN_STATUS);

	if (show)
	{
		controlShow(PNL_MAIN, BTN_STATUS);
	}
	else
	{
		controlHide(PNL_MAIN, BTN_STATUS);
	}
}

void taglayout::renderTopstrap(bool isEnabled, bool isMonitor)
{
	controlDestroyAll(PNL_MAIN, BTN_TOPSTRAP_PREFIX);

	int buttonLayoutWidth = (m_buttons == "all" || m_buttons.find("layout") >= 0) ? (BUTTON_LAYOUT_WIDTH + LAYOUT_BORDER_SIZE) : 0;
	int buttonMonitorWidth = (m_buttons == "all" || m_buttons.find("monitor") >= 0) ? (BUTTON_MONITOR_WIDTH + LAYOUT_BORDER_SIZE) : 0;
	// Layout name
	controlCreate(PNL_MAIN,
		BTN_LAYOUT_NAME,
		"bncs_control",
		LAYOUT_BORDER_SIZE + 1 + buttonLayoutWidth,
		LAYOUT_BORDER_SIZE,
		m_width - 2 - (2 * LAYOUT_BORDER_SIZE) - buttonLayoutWidth - buttonMonitorWidth,
		TOPSTRAP_HEIGHT,
		"style=label\nnotify.pressed=true\nnotify.lostfocus=true\ntext=- - - - -\nstatesheet=enum_notimportant");

	if (m_buttons == "all" || m_buttons.find("layout") >= 0)
	{	// Layout select button
		controlCreate(PNL_MAIN,
			BTN_LAYOUT_SELECT,
			"bncs_control",
			LAYOUT_BORDER_SIZE + 1,
			LAYOUT_BORDER_SIZE,
			BUTTON_LAYOUT_WIDTH,
			TOPSTRAP_HEIGHT,
			"style=button\ntext=Layout");
	}

	if (m_buttons == "all" || m_buttons.find("monitor") >= 0)
	{	// Monitor source select button
		controlCreate(PNL_MAIN,
			BTN_MONITOR_SELECT,
			"bncs_control",
			m_width - 2 - LAYOUT_BORDER_SIZE - BUTTON_MONITOR_WIDTH,
			LAYOUT_BORDER_SIZE,
			BUTTON_MONITOR_WIDTH,
			TOPSTRAP_HEIGHT,
			"style=button\ntext=Monitor");
	}

	if (!isEnabled)
	{
		controlDisable(PNL_MAIN, BTN_MONITOR_SELECT);
		controlDisable(PNL_MAIN, BTN_LAYOUT_SELECT);
	}
	else if (isMonitor)
	{
		controlDisable(PNL_MAIN, BTN_LAYOUT_SELECT);
	}
}

void taglayout::renderStatus(void)
{
	debug("taglayout::renderStatus");
	controlDestroy(PNL_MAIN, BTN_STATUS);

	controlCreate(PNL_MAIN,
		BTN_STATUS,
		"bncs_control",
		LAYOUT_BORDER_SIZE + 1,
		TOPSTRAP_HEIGHT + (2 * LAYOUT_BORDER_SIZE),
		m_width - 2 - (2 * LAYOUT_BORDER_SIZE),
		m_height - TOPSTRAP_HEIGHT - (3 * LAYOUT_BORDER_SIZE),
		"style=label\nlabelstyle=whitebox\ntextalign=bottom\ntext=TAG MV\nnotify.released=false");
	status(m_status);
}

int taglayout::textPutAll(const bncs_string & param, const bncs_string & value, const bncs_string & pnl, const bncs_string & filter)
{
	bncs_stringlist sl = getIdList(pnl, filter);
	int c = 0;

	if (sl.count())
	{
		for (bncs_stringlistIterator slit = sl.begin(); slit != sl.end(); slit++)
		{
			c++;
			textPut(param, value, pnl, *slit);
		}
	}
	return c;
}

int taglayout::controlDestroyAll(const bncs_string & pnl, const bncs_string & filter)
{
	bncs_stringlist sl = getIdList(pnl, filter);
	int c = 0;

	if (sl.count())
	{
		for (bncs_stringlistIterator slit = sl.begin(); slit != sl.end(); slit++)
		{
			if (!controlDestroy(pnl, *slit))
			{
				c++;
			}
		}
	}
	return c;
}
