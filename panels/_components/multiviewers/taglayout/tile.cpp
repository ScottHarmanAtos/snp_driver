#include "tile.h"


tile::tile()
{
}

tile::tile(Json::Value& data)
{
	Parse(data);
}

tile::tile(bncs_string& data)
{
	Parse(data);
}

tile::~tile()
{
}

void tile::GetValues(Json::Value& v)
{
	//OutputDebugString(bncs_string("JsonParse::Parse Node :%1").arg(JsonToString(v)));
	type = v["type"].asString().c_str();

	index = v["index"].asInt();

	x_axis = v["x_axis"].asInt();

	y_axis = v["y_axis"].asInt();

	width = v["width"].asInt();

	height = v["height"].asInt();

	destination = v["destination"].asInt();

	content_type = v["content_type"].asString().c_str();

	trace = "";
}
