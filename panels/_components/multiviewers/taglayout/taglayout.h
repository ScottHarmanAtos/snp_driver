#ifndef taglayout_INCLUDED
	#define taglayout_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class taglayout : public bncs_script_helper
{
public:
	taglayout( bncs_client_callback * parent, const char* path );
	virtual ~taglayout();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_layout;
	bncs_string m_instance;
	mv_layout m_mv_layout;
	bncs_stringlist m_sldests;
	bncs_string m_buttons;

	int m_width;
	int m_height;
	bool m_isText;
	bool m_isNamePushHold;
	bncs_string m_status;

	void render(bncs_string data, bool isStatus = false);
	void status(bncs_string text, bool show = true);
	void renderTopstrap(bool isEnabled = true, bool isMonitor = false);
	void renderStatus(void);

	int textPutAll(const bncs_string & param, const bncs_string & value, const bncs_string & pnl, const bncs_string & filter = "");
	int controlDestroyAll(const bncs_string & pnl, const bncs_string & filter);

};


#endif // taglayout_INCLUDED