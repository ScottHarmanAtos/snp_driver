#ifndef workspace_resources_INCLUDED
	#define workspace_resources_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class workspace_resources : public bncs_script_helper
{
public:
	workspace_resources( bncs_client_callback * parent, const char* path );
	virtual ~workspace_resources();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_workspaceId;
	bncs_string m_instance;

	void initWorkspace();
	void getSetting(bncs_string id, bncs_string section);
	
};


#endif // workspace_resources_INCLUDED