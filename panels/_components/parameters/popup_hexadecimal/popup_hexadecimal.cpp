#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "popup_hexadecimal.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( popup_hexadecimal )

// constructor - equivalent to ApplCore STARTUP
popup_hexadecimal::popup_hexadecimal( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
//	panelShow( 1, "popup_hexadecimal.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
}

// destructor - equivalent to ApplCore CLOSEDOWN
popup_hexadecimal::~popup_hexadecimal()
{
}

// all button pushes and notifications come here
void popup_hexadecimal::buttonCallback( buttonNotify *b )
{
	if( b->panel() == 1 )
	{
		bncs_string text;

		bncs_string newtext, valuetext;
		switch ( b->id() )
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
			case 16:
			case 17:
				textGet( "text", 1, "value", valuetext );
				textGet( "text", 1, b->id(), newtext );
				textPut( "text", valuetext + newtext, 1, "value" );

				newtext = valuetext + newtext;

				if( (newtext.length() >= m_min) && (newtext.length() <= m_max ))
					controlEnable( 1, "enter" );
				else
					controlDisable( 1, "enter" );
				break;
		}
		if (b->id() == "del")
		{
			textGet( "text", 1, "value", valuetext );
			if (valuetext.length())
			{
				valuetext.truncate(valuetext.length() - 1);
				textPut( "text", valuetext, 1, "value" );
			}
			if( (valuetext.length() >= m_min) && (valuetext.length() <= m_max ))
				controlEnable( 1, "enter" );
			else
				controlDisable( 1, "enter" );

		}
		if (b->id() == "clear")
		{
			controlEnable( 1, 10 );
			textPut( "text=", 1, "value" );
		}

		if (b->id() == "enter")
		{
			textGet("text", 1, "value", text);

			hostNotify("value=" + text );
			hostNotify("dismiss");
		}

		if (b->id() == "exit")
		{
			hostNotify("dismiss");
		}
	}
}

// all revertives come here
int popup_hexadecimal::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void popup_hexadecimal::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string popup_hexadecimal::parentCallback( parentNotify *p )
{
	debug( "%1 %2", p->command(), p->value ());

	if (p->command() == "name")
	{
		m_name = p->value();
	}
	else if (p->command() == "min" )
		m_min = p->value();
	else if (p->command() == "max" )
		m_max = p->value();
	else if( p->command() == "kb" )
	{
		if( p->value().lower() == "hex" )
		{
			panelShow( 1, "hex.bncs_ui" );
			setSize( 1 );
		}
		else
		{
			panelShow( 1, "dec.bncs_ui" );
			setSize( 1 );
		}

		if( m_min == m_max )
		{
			textPut( "text", bncs_string( "%1 characters only" ).arg( m_min ), 1, "hint" );
		}
		else if( m_max && !m_min )
			textPut( "text", bncs_string( "Up to %1 characters" ).arg( m_max ), 1, "hint" );
		else if( m_max )
			textPut( "text", bncs_string( "%1 to %2 characters" ).arg( m_min ).arg( m_max ), 1, "hint" );

		textPut( "text", m_name, 1, "name" );

		if( m_min )
			controlDisable( 1, "enter" );
	}
	return "";

}

// timer events come here
void popup_hexadecimal::timerCallback( int id )
{
}
