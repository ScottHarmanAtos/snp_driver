#ifndef popup_hexadecimal_INCLUDED
	#define popup_hexadecimal_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class popup_hexadecimal : public bncs_script_helper
{
public:
	popup_hexadecimal( bncs_client_callback * parent, const char* path );
	virtual ~popup_hexadecimal();
	bncs_string units;

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	int m_min;
	int m_max;
	bncs_string m_name;
};


#endif // popup_hexadecimal_INCLUDED