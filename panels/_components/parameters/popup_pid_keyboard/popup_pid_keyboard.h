#ifndef popup_pid_keyboard_INCLUDED
	#define popup_pid_keyboard_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class popup_pid_keyboard : public bncs_script_helper
{
private:
	bncs_string m_strInstance;
	int dev, slotServiceBegin, slotServiceEnd;
public:
	popup_pid_keyboard( bncs_client_callback * parent, const char* path );
	virtual ~popup_pid_keyboard();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
};


#endif // popup_pid_keyboard_INCLUDED