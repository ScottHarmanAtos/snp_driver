#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "popup_pid_keyboard.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( popup_pid_keyboard )

// constructor - equivalent to ApplCore STARTUP
popup_pid_keyboard::popup_pid_keyboard( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( 1, "popup_pid_keyboard.bncs_ui" );

	// you may need this call to set the size of this component 
	setSize( 1 );				// set the size to the same as the specified panel

}

// destructor - equivalent to ApplCore CLOSEDOWN
popup_pid_keyboard::~popup_pid_keyboard()
{
	infoUnregister(dev);
}

// all button pushes and notifications come here
void popup_pid_keyboard::buttonCallback( buttonNotify *b )
{
	debug(bncs_string("popup_pid_keyboard::buttonCallback id=%1 command=%2.%3 value=%2").arg(b->id()).arg(b->command()).arg(b->sub(0)).arg(b->value()));

	if( b->panel() == 1 )
	{
		bncs_string text;
		bncs_string newtext, valuetext;

		if ( b->id().toInt() > 0 )
		{
			textGet( "text", 1, "value", valuetext );
			textGet( "text", 1, b->id(), newtext );
			textPut( "text", valuetext + newtext, 1, "value" );
		}
		else if (b->id() == "clear")
		{
			textPut( "text=", 1, "value" );
		}
		else if (b->id() == "del")
		{
			textGet( "text", 1, "value", valuetext );
			if (valuetext.length())
			{
				valuetext.truncate(valuetext.length() - 1);
				textPut( "text", valuetext, 1, "value" );
			}
		}
		else if (b->id() == "enter")
		{
			textGet("text", 1, "value", text);
			hostNotify("value=" + text);
			hostNotify("dismiss");
		}
		else if (b->id() == "exit")
		{
			hostNotify("dismiss");
		}
		else if (b->id() == "btn_up")
		{
			textPut(bncs_string("selected=previous") , 1, "lvw_service" );
		}
		else if (b->id() == "btn_down")
		{
			textPut(bncs_string("selected=next") , 1, "lvw_service" );
		}
		else if (b->id() == "lvw_service" && b->command() =="selection")
		{
			bncs_string strPID, strName;
			b->value().split(';', strPID, strName);
			textPut("text", strPID, 1, "value");
		}
	}
}

// all revertives come here
int popup_pid_keyboard::revertiveCallback( revertiveNotify * r )
{
	debug(bncs_string("popup_pid_keyboard::revertiveCallback dev=%1 slot=%2 value=%3").arg(r->device()).arg(r->index()).arg( r->sInfo()));

	if ( r->device() == dev)
	{
		int slot = r->index();
		if (slot >= slotServiceBegin && slot <= slotServiceEnd)
		{
			if (r->sInfo().length() != 0)
			{
				bncs_string strPID = r->sInfo();
				strPID.replace(0x09, ';');
				textPut(bncs_string("add"), strPID, 1, "lvw_service" );
			}
		}
	}
	
	return 0;
}

// all database name changes come back here
void popup_pid_keyboard::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string popup_pid_keyboard::parentCallback( parentNotify *p )
{
	debug(bncs_string("popup_pid_keyboard::parentCallback dev=%1 command=%2 value=%3").arg(dev).arg(p->command()).arg(p->value()));

	if(p->command() == "instance")
	{
		dev = 0;
		slotServiceBegin = 1;
		slotServiceEnd = 1;

		char devType[256];

		m_strInstance = p->value();

//		getDevSlot(m_strInstance, "AudioServiceBegin", &dev, &slotServiceBegin, devType);		
//		getDevSlot(m_strInstance, "AudioServiceEnd", &dev, &slotServiceEnd, devType);		
		
		//TEMP Change from "AudioServiceBegin-AudioServiceEnd"
		// to "AudioLanguageName1-AudioLanguageName6"
		// note: this works for RX8200 devicetype only

		getDevSlot(m_strInstance, "AudioLanguageName1", &dev, &slotServiceBegin, devType);		
		getDevSlot(m_strInstance, "AudioLanguageName6", &dev, &slotServiceEnd, devType);		

		
		//TODO - Need to use a new devicetype entry:
		// slot 220 = AudioLanguageCount (typical value = 3)
		// this should be present in both RX1290 and RX8200 devicetypes
		//Poll this slot and then when the value is returned the 
		// number should be added to slotServiceBegin to obtain slotServiceEnd
		//  e.g slotServiceEnd = slotServiceBegin + intServiceCount - 1
		//   i.e. 223 + 3 - 1  = 225

		debug(bncs_string("popup_pid_keyboard::parentCallback dev=%1 AudioServiceBegin=%2").arg(dev).arg(slotServiceBegin));
		debug(bncs_string("popup_pid_keyboard::parentCallback dev=%1 AudioServiceEnd=%2").arg(dev).arg(slotServiceEnd));

		textPut(bncs_string("clear"), 1, "lb_service" );
		if (slotServiceBegin <= slotServiceEnd)
		{
			debug("popup_pid_keyboard::parentCallback before register");
			infoRegister(dev,slotServiceBegin, slotServiceEnd);
			debug("popup_pid_keyboard::parentCallback after register before poll");
			infoPoll(dev, slotServiceBegin, slotServiceEnd);
			debug("popup_pid_keyboard::parentCallback after");
		}
	}
	else if (p->command() == "title")
	{
		textPut("text", p->value() , 1, "lbl_title" );
	}
	else if (p->command() == "warning")
	{
		textPut("text", p->value() , 1, "lbl_warning" );
	}
	return "";
}

// timer events come here
void popup_pid_keyboard::timerCallback( int id )
{
}
