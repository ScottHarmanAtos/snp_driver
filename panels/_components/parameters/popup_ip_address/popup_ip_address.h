#ifndef popup_ip_address_INCLUDED
	#define popup_ip_address_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class popup_ip_address : public bncs_script_helper
{
private:
	void updateButtons();
public:
	popup_ip_address( bncs_client_callback * parent, const char* path );
	virtual ~popup_ip_address();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
};


#endif // popup_ip_address_INCLUDED