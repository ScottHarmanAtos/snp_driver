#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "popup_ip_address.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( popup_ip_address )

// constructor - equivalent to ApplCore STARTUP
popup_ip_address::popup_ip_address( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( 1, "popup_ip_address.bncs_ui" );

	// you may need this call to set the size of this component 
	setSize( 1 );				// set the size to the same as the specified panel
	updateButtons();
}

// destructor - equivalent to ApplCore CLOSEDOWN
popup_ip_address::~popup_ip_address()
{
}

void popup_ip_address::updateButtons()
{
	bncs_string text;
	bool bValid = true;
	textGet( "text", 1, "value", text );
	bncs_stringlist numbers(text, '.');
	if (numbers.count() == 4)
	{
		controlDisable(1, "dot");
		bValid = true;
		for (int i=0; i<4; i++)
		{
			if (numbers[i].toInt() >= 255)
				bValid = false;
		}
	}
	else if (!text.length() || text.endsWith("."))
	{
		controlDisable(1, "dot");
		bValid = false;
	}
	else
	{
		controlEnable(1, "dot");
		bValid = false;
	}

	if (bValid)
		controlEnable(1, "enter");
	else
		controlDisable(1, "enter");

}

// all button pushes and notifications come here
void popup_ip_address::buttonCallback( buttonNotify *b )
{
	if( b->panel() == 1 )
	{
		bncs_string text;
		bncs_string newtext, valuetext;

		if ( b->id().toInt() > 0 || b->id() == "dot")
		{
			textGet( "text", 1, "value", valuetext );
			textGet( "text", 1, b->id(), newtext );
			textPut( "text", valuetext + newtext, 1, "value" );
			updateButtons();
		}
		else if (b->id() == "clear")
		{
			textPut( "text=", 1, "value" );
			updateButtons();
		}
		else if (b->id() == "del")
		{
			textGet( "text", 1, "value", valuetext );
			if (valuetext.length())
			{
				valuetext.truncate(valuetext.length() - 1);
				textPut( "text", valuetext, 1, "value" );
				updateButtons();
			}
		}
		else if (b->id() == "enter")
		{
			textGet("text", 1, "value", text);
			hostNotify("value=" + text);
			hostNotify("dismiss");
		}
		else if (b->id() == "exit")
		{
			hostNotify("dismiss");
		}
	}
}

// all revertives come here
int popup_ip_address::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void popup_ip_address::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string popup_ip_address::parentCallback( parentNotify *p )
{
	if (p->command() == "name")
	{
		textPut( "text", p->value(), 1, "name" );
	}
	return "";
}

// timer events come here
void popup_ip_address::timerCallback( int id )
{
}
