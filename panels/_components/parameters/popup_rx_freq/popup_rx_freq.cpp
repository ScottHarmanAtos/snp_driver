#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "popup_rx_freq.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( popup_rx_freq )

// constructor - equivalent to ApplCore STARTUP
popup_rx_freq::popup_rx_freq( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_kText = "KHz";
	m_gText = "GHz";
	m_mText = "MHz";

	// set default values
	m_min = 0.0;
	m_max = 0.0;
	m_dp = 0;
	m_step = 1.0;
}

// destructor - equivalent to ApplCore CLOSEDOWN
popup_rx_freq::~popup_rx_freq()
{
}

// rounding
bncs_string popup_rx_freq::Round( double value )
{
	char formattedtext[100];
	char format[ 16 ];

	value += (m_step / 2.0 );

	value = floor( value / m_step ) * m_step;

	sprintf( format, "%%1.%df", m_dp );

	sprintf(formattedtext, format, value );
	debug( formattedtext );

	return formattedtext;
}

// all button pushes and notifications come here
void popup_rx_freq::buttonCallback( buttonNotify *b )
{
	bncs_string text;

	bncs_string newtext, valuetext;
	switch ( b->id() )
	{
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
			textGet( "text", 1, "value", valuetext );
			textGet( "text", 1, b->id(), newtext );
			textPut( "text", valuetext + newtext, 1, "value" );

			if( (valuetext + newtext).contains( '.' ))
				controlDisable( 1, 10 );
			break;
	}

	if (b->id() == "del")
	{
		textGet( "text", 1, "value", valuetext );
		if (valuetext.length())
		{
			valuetext.truncate(valuetext.length() - 1);
			textPut( "text", valuetext, 1, "value" );
		}

		if( !valuetext.contains( '.' ))
			controlEnable( 1, 10 );
	}

	if (b->id() == "negate")
	{
		textGet( "text", 1, "value", valuetext );
		if (valuetext.length())
		{
			if (valuetext.left(1) == "-")
				valuetext.remove(0,1);
			else
				valuetext = "-" + valuetext;
		}
		else
			valuetext = "-";

		textPut( "text", valuetext, 1, "value" );

	}
	if (b->id() == "clear")
	{
		controlEnable( 1, 10 );
		textPut( "text=", 1, "value" );
	}

	if (b->id() == "ghz")
	{
		textGet("text", 1, "value", text);

		double d = text.toDouble();

		d = d * 1000;

		hostNotify("value=" + Round(d) + m_units);
		hostNotify("dismiss");
	}
	else if (b->id() == "mhz")
	{
		textGet("text", 1, "value", text);

		double d = text.toDouble();

		hostNotify("value=" + Round( d ) + m_units);
		hostNotify("dismiss");
	}
	else if (b->id() == "khz")
	{
		textGet("text", 1, "value", text);
		double d = text.toDouble() / 1000.00;
//		text = bncs_string(temp,10);
		hostNotify("value=" + Round( d ) + m_units);
		hostNotify("dismiss");
	}
	else if (b->id() == "exit")
	{
		hostNotify("dismiss");
	}
	else if( b->id().left( 6 ) == "preset" )
	{
		bncs_string value;
		textGet( "text", 1, b->id(), value );

		parsePresetText( value );
	}
}

// all revertives come here
int popup_rx_freq::revertiveCallback( revertiveNotify * r )
{
	return 0;
}

// all database name changes come back here
void popup_rx_freq::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string popup_rx_freq::parentCallback( parentNotify *p )
{
	debug( "%1 %2", p->command(), p->value() );

	if (p->command() == "units")
	{
		m_units = p->value();
	}
	else if (p->command() == "min")
	{
		m_min = p->value().toDouble();
	}
	else if (p->command() == "max")
	{
		m_max = p->value().toDouble();
	}
	else if (p->command() == "step")
	{
		m_step = p->value().toDouble();
	}
	else if (p->command() == "dp")
	{
		m_dp = p->value();
	}
	else if (p->command() == "name")
	{
		m_name = p->value();
	}
	else if( p->command() == "presets" )
	{
		m_presets = p->value();

		debug( m_presets );
	}
	else if( p->command() == "G" )
	{
		debug( "got \"%1\" \"%2\"", p->command(), p->value() );
		m_gText = p->value();
	}
	else if( p->command() == "M" )
	{
		debug( "got \"%1\" \"%2\"", p->command(), p->value() );
		m_mText = p->value();
	}
	else if( p->command() == "K" )
	{
		debug( "got \"%1\" \"%2\"", p->command(), p->value() );
		m_kText = p->value();
	}
	else if( p->command() == "__GO" )
	{
		if( m_presets.length() )
		{
			panelShow( 1, "popup_rx_freq_presets.bncs_ui" );
		}
		else
		{
			panelShow( 1, "popup_rx_freq.bncs_ui" );
		}

		// you may need this call to set the size of this component 
		//  if it's used in a popup window 
		setSize( 1 );				// set the size to the same as the specified panel

		writeHint();

		bncs_stringlist sl( m_presets );

		int x;
		for( x = 1 ; x <= sl.count() ; x++ )
			textPut( "text", sl[ x - 1 ], 1, bncs_string( "preset%1" ).arg( x ));

		for(  ; x < 10 ; x++ )
			controlHide( 1, bncs_string( "preset%1" ).arg( x ) );

		if( m_gText.length() )
			textPut( "text", m_gText, 1, "ghz" );
		else
			controlHide( 1, "ghz" );

		if( m_mText.length() )
			textPut( "text", m_mText, 1, "mhz" );
		else
			controlHide( 1, "mhz" );

		if( m_kText.length() )
			textPut( "text", m_kText, 1, "khz" );
		else
			controlHide( 1, "khz" );

		if( m_min >= 0.0 )
			controlHide( 1, "negate");
	
	}
	return "";
}

// timer events come here
void popup_rx_freq::timerCallback( int id )
{
}


void popup_rx_freq::writeHint( void )
{
	char format[ 16 ];
	char smin[ 64 ];
	char smax[ 64 ];

	sprintf( format, "%%1.%df", m_dp );
	sprintf( smin, format, m_min );
	sprintf( smax, format, m_max );

	bncs_string s = bncs_string( "%1%2 - %3%4" ).arg( smin ).arg( m_units ).arg( smax ).arg( m_units );

	textPut( "text", s, 1, "hint" );

	textPut( "text", m_name, 1, "name" );
}

void popup_rx_freq::parsePresetText( const bncs_string & text )
{
	if( text.lower().contains( 'g' ))
	{
		double d = text.toDouble();

		d = d * 1000;

		hostNotify("value=" + Round(d) + m_units);
		hostNotify("dismiss");
	}
	else if( text.lower().contains( 'm' ))
	{
		double d = text.toDouble();

		hostNotify("value=" + Round(d) + m_units);
		hostNotify("dismiss");
	}
	else if( text.lower().contains( 'k' ))
	{
		double d = text.toDouble() / 1000.00;

		hostNotify("value=" + Round( d ) + m_units);
		hostNotify("dismiss");
	}
	else
	{
		debug( "popup_rx_freq: can't parse preset text value (doesn't end in GHz, MHz or KHz)" );
		hostNotify( "dismiss" );
	}
}

