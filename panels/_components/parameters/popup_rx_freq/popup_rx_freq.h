#ifndef popup_rx_freq_INCLUDED
	#define popup_rx_freq_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class popup_rx_freq : public bncs_script_helper
{
public:
	popup_rx_freq( bncs_client_callback * parent, const char* path );
	virtual ~popup_rx_freq();
	bncs_string m_units;

	double m_min;
	double m_max;
	int m_dp;
	double m_step;
	bncs_string m_name;
	bncs_string m_presets;

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	bncs_string Round( double value );
	void writeHint( void );
	void parsePresetText( const bncs_string & text );

private:
	bncs_string m_kText;
	bncs_string m_gText;
	bncs_string m_mText;

};


#endif // popup_rx_freq_INCLUDED