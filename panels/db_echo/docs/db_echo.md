# db_echo Component

Database Echo component that takes a routerModify callback and turns is around to another database
It is part of the testing system and is only functional within that system and should not be used outside of that

The component listens for database modifications for its targeted instance
If the RM change begins with a trigger key it sends out a copy of the modified database contents on another database with the same index
This is used to ensure a routerModify transitions across the network correctly to another CSI
The transition could involve a FABIAN or slink connection allowing for testing of remote site RM functionality

To avoid feedback loops the trigger key is changed for an echo response
These are set as two #defines as follows

> \#define TRIGGER_KEY			"RUN_DATABASE_WRITE::"
> \#define TRIGGER_RESPONSE		"RUN_DATABASE_ECHO::"

The offset added to the database number is derived from the first integer within the incoming message
This is performed via a bncs_string firstInt() method

The application doesn't care *what* the offset value is so long as when added to the original database number it falls within acceptable limits

The limits are defined as inclusive between 0 and MAXDATABASES_V45

MAXDATABASES_V45 is defined in bncs.h

## Example

Assume the instance targeted resolves to device number 101

A typical routerModify command would be 

> RM 101 3 25 'RUN_DATABASE_WRITE::1000::test_message'

Which resolves to device 101, database 3, index 25

The offset would resolve to 1000 as that is the first integer within the message

The subsequent routerModify response would be

>RM 101 1003 25 'RUN_DATABASE_ECHO::1000::test_message'

Note also that if the firstInt() was zero then it would "turn round" the original routerModify with a modified trigger key


## Commands

### instance
Instance of the device to respond to routerModify revertives from

