#ifndef testman_INCLUDED
	#define testman_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class testman : public bncs_script_helper
{
public:
	testman( bncs_client_callback * parent, const char* path );
	virtual ~testman();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	int test = 1;
	bncs_stringlist tests;
	bncs_stringlistIterator itTests;
	bncs_string m_instance;
	
	int textPutAll(bncs_string szText, int iPanel, bncs_string szFilter = "");
};


#endif // testman_INCLUDED