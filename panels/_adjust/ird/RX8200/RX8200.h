#ifndef RX8200_INCLUDED
	#define RX8200_INCLUDED

#pragma warning(disable: 4786)

#include <bncs_script_helper.h>
#include <map>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class RX8200 : public bncs_script_helper
{
private:
	bncs_stringlist m_slMemButtonAndValue;
	bncs_stringlist takeButtonList;
	bncs_string m_strInstance;
	int dev;
	bncs_string onair;//, rxfreq, localosc;

	int idestIndex;

	void setCurrentValues(bncs_string values);

	int m_intCurrentTab;
	bncs_stringlist m_sltTabs;

	void showFirstTab( void );
	void calcLO( void );

	int m_intIrdMode ;
	int m_intBissMode ;
	int m_blnOnAir ;

	map<bncs_string, bncs_string> m_controls;

public:
	RX8200( bncs_client_callback * parent, const char* path );
	virtual ~RX8200();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );

	enum ENUM_IRD_MODE
	{
		MODE_UNKNOWN,
		MODE_ASI,
		MODE_SAT
	};

	enum ENUM_BISS_MODE
	{
		BISSMODE_UNKNOWN,
		BISSMODE_1,
		BISSMODE_E
	};
};


#endif // RX8200_INCLUDED