#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <instanceLookup.h>
#include "RX8200.h"

#define SLUG_TIMER 1
#define SLUG_TIMER_LEN 400

//These need to be updated when the bncs_stringlist m_sltTabs is changed
#define PANEL_MAIN 1
#define PANEL_VIDEO_AUDIO 2
#define PANEL_ADV_STATUS_GENLOCK 3
#define PANEL_BROWSER 4


// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( RX8200 )

// constructor - equivalent to ApplCore STARTUP
RX8200::RX8200( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_intCurrentTab = 0;

	idestIndex = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( "tabs", "tabs.bncs_ui" );
	// setSize( 1610,900 );		// set the size explicitly
	// setSize( PANEL_MAIN );		// set the size to the same as the specified panel

	showFirstTab();

	m_sltTabs << "RX8200";
	m_sltTabs << "RX8200_Video_Audio";
	m_sltTabs << "RX8200_Adv_Status_Genlock";
	m_sltTabs << "RX8200_Browser";

	m_intIrdMode = MODE_UNKNOWN;
	m_intBissMode = BISSMODE_UNKNOWN;  
	m_blnOnAir = false;							 // assume off air
}

void RX8200::setCurrentValues(bncs_string currentValues)
{
	debug("RX8200::setCurrentValues() currentValues: " + currentValues);
	//debug("RX1290::setCurrentValues() currentValues: " + currentValues);

	textPut("takepreset", 1, "memory");
	
	bncs_stringlist lValues(currentValues, '|');
	
	m_slMemButtonAndValue.clear();
	
	//This compares what is currently there with what we are about to assign, do nothing if they are the same
	for( bncs_stringlist::iterator it = lValues.begin() ; it != lValues.end(); ++it )
	{	
		bncs_string parameter, value, retCurrentVal;
		it->split('*', parameter, value);
		textGet("value", PANEL_MAIN, m_controls[parameter], retCurrentVal);
		if(retCurrentVal != value)
		{
			//Store all values to change
			m_slMemButtonAndValue << bncs_string("%1;%2").arg(m_controls[parameter]).arg(value);
		}
	}
	debug("RX8200::setCurrentValues Mems needed to Recall %1",m_slMemButtonAndValue.toString(','));
	//Sort this is done so we recall the buttons in the same order the mems are numbered (breaks at mem_mod_mode (becasue mem_rxfreq,mem_mod_mode,mem_2) unless the other mems are mem_01)
	m_slMemButtonAndValue = m_slMemButtonAndValue.sort(true);
	
	//Take Values
	if(m_slMemButtonAndValue.count() != 0)
	{
		bncs_stringlist sl(m_slMemButtonAndValue[0],';');
		textPut("value",sl[1], 1,sl[0]);
		m_slMemButtonAndValue.deleteItem(0);
	}
	if(m_slMemButtonAndValue.count() != 0)
	{
		timerStart(SLUG_TIMER, SLUG_TIMER_LEN);
	}
}


// destructor - equivalent to ApplCore CLOSEDOWN
RX8200::~RX8200()
{
}

// timer events come here
void RX8200::timerCallback( int id )
{
	if(id == SLUG_TIMER)
	{
		if(m_slMemButtonAndValue.count() != 0)
		{
			bncs_stringlist sl(m_slMemButtonAndValue[0],';');
			textPut("value",sl[1], 1,sl[0]);
			m_slMemButtonAndValue.deleteItem(0);
		}
		else
		{
			timerStop(SLUG_TIMER);
		}
	}
}

//******** end of methods for memory handling ********//

// all button pushes and notifications come here
void RX8200::buttonCallback( buttonNotify *b )
{
	debug(bncs_string("RX8200::buttonCallback panel=%1 btn=%2").arg(b->panel()).arg(b->id()));
	if( b->panel() == "tabs" )
	{
		bncs_stringlist sl( b->id(), '_' );

		int intControlIndex = sl[ 1 ];

		if(intControlIndex  && ( intControlIndex != m_intCurrentTab))
		{
			controlHide("tabs", bncs_string("bg_tab_%1").arg(m_intCurrentTab) );
			panelRemove(m_intCurrentTab);

			m_intCurrentTab = intControlIndex;
			controlShow("tabs", bncs_string("bg_tab_%1").arg(m_intCurrentTab) );
			panelShow( m_intCurrentTab, bncs_string("%1.bncs_ui").arg(m_sltTabs[m_intCurrentTab - 1]), m_strInstance );

		}
	}
	else if (b->panel() == PANEL_MAIN)
	{
		debug( "%1 %2 %3 %4", b->id(), b->command(), b->sub( 0 ), b->value() );
		if (b->id() == "mem_rxfreq")
		{
			if(b->command() == "presetvaluechangerequested")
				calcLO();
			if(b->command() == "value")
				calcLO();
		}
		if (b->id() == "mem_localosc")
		{
			if(b->command() == "presetvaluechangerequested")
				calcLO();
			if(b->command() == "value")
				calcLO();
		}


		if ( b->id() == "btn_AsiSat")
		{
			if(b->command() == "notify")
			{
				bncs_string asiSat =  b->value();
				
				if (asiSat == "SAT")
				{
					controlShow(PANEL_MAIN, "4");
					controlShow(PANEL_MAIN, "5");
					controlShow(PANEL_MAIN, "6");
					controlShow(PANEL_MAIN, "7");
					controlShow(PANEL_MAIN, "8");
					controlShow(PANEL_MAIN, "9");
					controlShow(PANEL_MAIN, "10");
					controlShow(PANEL_MAIN, "12");
					controlShow(PANEL_MAIN, "inputTallyName");
					controlShow(PANEL_MAIN, "inputTally");
					controlEnable(PANEL_MAIN, "take");
					controlEnable( PANEL_MAIN, "mem_rxfreq" );
					controlEnable( PANEL_MAIN, "mem_2" );
					controlEnable( PANEL_MAIN, "mem_5" );
					controlEnable( PANEL_MAIN, "mem_6" );
					controlEnable( PANEL_MAIN, "mem_9" );
					controlEnable( PANEL_MAIN, "mem_localosc" );
					controlEnable( PANEL_MAIN, "mem_spec" );
					controlEnable( PANEL_MAIN, "mem_mod_mode" );
					controlEnable( PANEL_MAIN, "mem_4" );
					controlEnable( PANEL_MAIN, "mem_8" );
					controlEnable( PANEL_MAIN, "mem_fec");
					controlEnable( PANEL_MAIN, "mem_mod_type");
				}
				else if (asiSat == "ASI")
				{
					controlHide(PANEL_MAIN, "4");
					controlHide(PANEL_MAIN, "5");
					controlHide(PANEL_MAIN, "6");
					controlHide(PANEL_MAIN, "7");
					controlHide(PANEL_MAIN, "8");
					controlHide(PANEL_MAIN, "9");
					controlHide(PANEL_MAIN, "10");
					controlHide(PANEL_MAIN, "12");
					controlHide(PANEL_MAIN, "inputTallyName");
					controlHide(PANEL_MAIN, "inputTally");
					controlDisable(PANEL_MAIN, "take");
					controlDisable( PANEL_MAIN, "mem_rxfreq" );
					controlDisable( PANEL_MAIN, "mem_2" );
					controlDisable( PANEL_MAIN, "mem_5" );
					controlDisable( PANEL_MAIN, "mem_6" );
					controlDisable( PANEL_MAIN, "mem_9" );
					controlDisable( PANEL_MAIN, "mem_localosc" );
					controlDisable( PANEL_MAIN, "mem_spec" );
					controlDisable( PANEL_MAIN, "mem_mod_mode" );
					controlDisable( PANEL_MAIN, "mem_fec");
					controlDisable( PANEL_MAIN, "mem_mod_type");
				}
				else
				{
					controlDisable(PANEL_MAIN, "4");
					controlDisable(PANEL_MAIN, "6");
					controlDisable(PANEL_MAIN, "8");
					controlDisable( PANEL_MAIN, "10" );
					controlDisable( PANEL_MAIN, "12" );
					controlDisable( PANEL_MAIN, "mem_rxfreq" );
					controlDisable( PANEL_MAIN, "mem_2" );
					controlDisable( PANEL_MAIN, "mem_5" );
					controlDisable( PANEL_MAIN, "mem_6" );
					controlDisable( PANEL_MAIN, "mem_9" );
					controlDisable( PANEL_MAIN, "mem_localosc" );
					controlDisable( PANEL_MAIN, "mem_spec" );
					controlDisable( PANEL_MAIN, "mem_mod_mode" );
					controlDisable( PANEL_MAIN, "mem_4" );
					controlDisable( PANEL_MAIN, "mem_8" );
					controlDisable( PANEL_MAIN, "mem_fec");
					controlDisable( PANEL_MAIN, "mem_mod_type");
				}
			}
		}
		if ( b->id() == "btn_onair")
		{
			if(b->command() == "notify")
			{
				onair = b->value();
				
				if (onair == "1")
				{
					controlDisable( PANEL_MAIN, "take" );
					controlDisable( PANEL_MAIN, "audiotake" );
					controlDisable( "tabs", "btnTab_2" );
					controlDisable( "tabs", "btnTab_3" );
					controlDisable( "tabs", "btnTab_4");

					textPut("onair", "on", 1, "serviceComponent");

					showFirstTab();
				}
				if (onair == "0")
				{
					controlEnable( PANEL_MAIN, "take" );
					controlEnable( PANEL_MAIN, "audiotake" );
					controlEnable( "tabs", "btnTab_2" );
					controlEnable( "tabs", "btnTab_3" );
					controlEnable( "tabs", "btnTab_4");

					textPut("onair", "off", 1, "serviceComponent");
				}
			}

		}
		
		else if ( b->id() == "mem_8")
		{
			if(b->command() == "notify")
			{
				if (b->value() == "Fixed")
				{
					controlDisable( PANEL_MAIN, "mem_4" );
				}
				else
				{
					controlEnable(PANEL_MAIN, "mem_4");
				}
			}
		}

		else if( b->id() == "memory")
		{
			if( b->command() == "recall")
			{
				setCurrentValues(b->value());
			}
		}
		else if (b->id()=="audiotake")
		{
			textPut("takepreset", PANEL_MAIN, "memory");
			bncs_stringlist lControls = getIdList(PANEL_MAIN, "audiomem_");
			for( bncs_stringlist::iterator it = lControls.begin() ; it != lControls.end() ; ++it )
			{
				textPut("takepreset", PANEL_MAIN, *it);
			}
		}
		else if (b->id()=="revert")
		{
			textPut("revertpreset", PANEL_MAIN, "memory");
			bncs_stringlist lControls = getIdList(1,"mem_");
			for( bncs_stringlist::iterator it = lControls.begin() ; it != lControls.end() ; ++it )
			{
				textPut("revertpreset", PANEL_MAIN, *it);
			}
		}
		else if ( ( b->id().startsWith("mem_") || b->id().startsWith("audiomem_") ) && b->command() == "presetvaluechangerequested")
		{
			bncs_string name;
			textGet("parameter", PANEL_MAIN, b->id(), name);
			
			textPut("preset", bncs_string("%1|%2").arg(name).arg(b->value()), PANEL_MAIN, "memory");

		}
		else if (b->id() == "take")
		{
			bncs_stringlist memList = getIdList(PANEL_MAIN, "mem_");
			for (int i = 0; i < memList.count(); i++)
			{
				textPut("takepreset", PANEL_MAIN, memList[i]);
			}
		}

	}			
	else if (b->panel() == PANEL_BROWSER)
	{
		if (b->id() == "deviceip")
		{
			textPut("homepage", b->value(), PANEL_BROWSER, "browser");
			textPut("text", bncs_string("%1 - %2").arg(m_strInstance).arg(b->value()), PANEL_BROWSER, "banner");
		}
	}
	debug("RX8200::buttonCallback END");
}

// all revertives come here
int RX8200::revertiveCallback( revertiveNotify * r )
{
	if( r->device() == 204)											//added to APTN to extract default LO frequency from db4 for routed source
	{
		if(r->index() == idestIndex)
		{
			bncs_string sTemp;
			routerName( 204, 4, r->info(), sTemp);
			textPut("popupScriptParam=presets", sTemp, 1, "mem_localosc");
			textPut("value", sTemp, 1, "mem_localosc");	//automatically write lookup LO value to button
			routerName( 204, 0, r->info(), sTemp);
			textPut("text", sTemp, 1, 16);
		}
	}
	return 0;
}

// all database name changes come back here
void RX8200::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string RX8200::parentCallback( parentNotify *p )
{
	debug(bncs_string("RX8200::parentCallback command=%1 value=%2").arg(p->command()).arg(p->value()));
	if(p->command() == "instance")
	{
		// hide the advanced pages
		showFirstTab();
		dev = 0;

		m_strInstance = p->value();

		idestIndex = routerIndex( 204, 7, m_strInstance );	//added to APTN to extract default LO frequency from db4 for routed source
		
		if(idestIndex > 0)
		{
			routerRegister( 204, idestIndex, idestIndex, true );
			routerPoll( 204, idestIndex, idestIndex );
		}

		// fills the parameters field of memory component. Could have been done on the panel design time also.
		bncs_stringlist lControls = getIdList(1,"mem_") + getIdList(1,"audiomem_");

		bncs_stringlist lParameters;
		bncs_string name, units;
		for( bncs_stringlist::iterator it = lControls.begin(); it != lControls.end(); ++it)
		{
			textGet("parameter", 1, *it, name);
			m_controls[name] = *it;
			units = "";
			if (*it == "mem_rxfreq" || *it == "mem_2" || *it == "mem_localosc" || *it == "mem_5")
			{ // has to take care about not calling this on bncs_smartstring, because that crashes.
				textGet("units", 1, *it, units);
			}
			lParameters.append(bncs_string("%1;%2").arg(name).arg(units));
		}
		textPut("parameters", lParameters.toString(), 1, "memory");
	}
	return "";
}


void RX8200::showFirstTab( void )
{
	if( m_intCurrentTab != 1 )
	{
		controlHide("tabs", bncs_string("bg_tab_%1").arg(m_intCurrentTab) );
		panelRemove( m_intCurrentTab );

		m_intCurrentTab=1;
		panelShow( 1, "RX8200.bncs_ui" );
		controlShow("tabs", "bg_tab_1" );
		controlHide("tabs", "bg_tab_2");
		controlHide("tabs", "bg_tab_3");
		controlHide("tabs", "bg_tab_4");
	}
}



void RX8200::calcLO( void )
{
	bncs_string rxfreq, localosc;
	bool usesPresetValue = false;

	textGet("presetvalue",1,"mem_localosc", localosc);
	if (localosc.length() == 0)
		textGet("value",1,"mem_localosc", localosc);
	else 
		usesPresetValue = true;

	textGet("presetvalue",1,"mem_rxfreq", rxfreq);
	if (rxfreq.length() == 0)
		textGet("value",1,"mem_rxfreq", rxfreq);
	else 
		usesPresetValue = true;


	double value;
	value = rxfreq.toDouble() - localosc.toDouble();

	char format[ 16 ];
	char formattedtext[100];

	sprintf( format,"%%1.%dfMHz", 3 );	 // this should be device description value for dp and units.....but hasn't

	sprintf(formattedtext, format, value);
	textPut("text", formattedtext, 1, "mem_2");

	if( usesPresetValue )
		textPut("stylesheet", "presetstyle", 1, "mem_2");
	else
		textPut("stylesheet", "default", 1, "mem_2");
}
