#ifndef ns2000_INCLUDED
	#define ns2000_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class ns2000 : public bncs_script_helper
{
public:
	ns2000( bncs_client_callback * parent, const char* path );
	virtual ~ns2000();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	//bncs_string m_instance;
	bncs_string m_strInstance;
	int m_intDevice;

	bncs_stringlist m_sltTabs; 
	int m_intCurrentTab;

	int m_intCommsSlot;
	int m_intFreqSlot;
	int m_intLoSlot;
	int m_intSymSlot;
	
	bool isedited_freq;
	bool isedited_lo;
	bool isedited_sym;

	double m_dblNsFreqLive;
	double m_dblNsLoLive;
	double m_dblNsSymLive;

	double m_dblNsFreqEdit;
	double m_dblNsLoEdit;
	double m_dblNsSymEdit;

	bncs_string popupreason;

	bncs_string lband_rtr_instance;

	void showFirstTab(void);
	void delayOnApply(void);
	void enableAfterDelay(void);
	int LBandFreqCheck(double downlinkFrequency, double LocalOscillatorFrequency);
	bool validatetext(bncs_string userinput, bncs_string validationreason);
	bncs_string GetLBandRouterName(bncs_string irdinstance);
};


#endif // ns2000_INCLUDED