#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "ns2000.h"


/// NOTES: to future developers of the NS2000 panel
//Yes, Novelsat count in tens and hundreds and hundredths of the SI units. It's why we haven't used the stock RXfreq component.
//Novelsat demods "helpfully" change the RX frequency you entered after a take, IF the LO freq is also changed.
//if you change the LO on its own, it will also change the RX freq.
//so ALWAYS set the LO, THEN the RXfeq.

#pragma region usefulnumbers
//#define TIMER_SETUP	1
#define TIMER_WAITBAR 2 // discovery specification calls for a "make everything grey and wait" timer after applying settings. This is the timer
#define SUITABLE_DELAY 2000 //...ad this is how long (milliseconds) that is.


#define RXFREQ_MULT 100000 //novelsat units (10Hz) to MHz (1000000) multiplication factor (rx frequency)
#define LOFREQ_MULT 100000 //novelsat units (10Hz) to MHz (1000000) multiplication factor (local oscillator frequency)
#define SYMRATE_MULT 1000000 //novelsat units (1sampe/sec) to MS/s(1000000) multiplication factor (demodulator symbol rate)

#define LBAND_MIN 950 //novelsat Lband input minimum frequency in MHz
#define LBAND_MAX 2150 //novelsat Lband input maximum frequency in MHz
#define FREQ_LOW 1 //after calculation, for if Lband frequency is below Lband spectrum filters
#define FREQ_OK 2 // -//- for if Lband frequency is inside Lband
#define FREQ_HIGH 3 //if calculated frequency is above Lband spectrum

//These need to be updated when the bncs_stringlist m_sltTabs is changed
#define PANEL_DEMODULATOR 1
#define PANEL_ENGINEERING 2
#define PANEL_BROWSER 3


#pragma endregion usefulnumbers

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( ns2000 )

// constructor - equivalent to ApplCore STARTUP
ns2000::ns2000( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//initialise and populate tab data
	m_intCurrentTab = 0;
	m_sltTabs << "NS2000_demodulator";
	m_sltTabs << "NS2000_engineering";
	m_sltTabs << "NS2000_browser";
	
	//show the tabs!
	panelShow("tabs", "tabs.bncs_ui");
	showFirstTab();

	//set the edited flag for our homegrown novelsat scaled values:
	isedited_freq = false;
	isedited_lo = false;
	isedited_sym = false;
	popupreason = "none";
}

// destructor - equivalent to ApplCore CLOSEDOWN
ns2000::~ns2000()
{
}

// all button pushes and notifications come here
void ns2000::buttonCallback( buttonNotify *b ) 
{
	debug(bncs_string("NS2000::buttonCallback panel=%1 btn=%2 command=%3 value=%4").arg(b->panel()).arg(b->id()).arg(b->command()).arg(b->value()));
	b->dump();
	//b->dump("NS2000::buttoncallback   ");
	if (b->panel() == "tabs")
	{
		bncs_stringlist sl(b->id(), '_');

		int intControlIndex = sl[1];

		if (intControlIndex && (intControlIndex != m_intCurrentTab))
		{
			controlHide("tabs", bncs_string("bg_tab_%1").arg(m_intCurrentTab));
			panelRemove(m_intCurrentTab);

			m_intCurrentTab = intControlIndex;
			controlShow("tabs", bncs_string("bg_tab_%1").arg(m_intCurrentTab));
			panelShow(m_intCurrentTab, bncs_string("%1.bncs_ui").arg(m_sltTabs[m_intCurrentTab - 1]), m_strInstance);
			if (m_intCurrentTab == PANEL_DEMODULATOR) {
				debug(bncs_string(" ns2000::buttonCallback textput\"router_instance\" = %1").arg(GetLBandRouterName(m_strInstance)));
				textPut("router_instance", GetLBandRouterName(m_strInstance), PANEL_DEMODULATOR, "lband_tally");//!!!WARNING!!!  instancetoport (the driving engine behind the input tally component) doesn't care which router 
				textPut("instance", m_strInstance, PANEL_DEMODULATOR, "lband_tally");							// you are connected to, and will just cause the tally to take the first router/port combination that has your instance.
																												// ...in this case, the router instance is passed in, but is ignored. In any case, you should only have the instance in one adjust db anyway...
																												// so it won't be an issue unless you happen to have left it in multiple places...
			}

		}
	}
	else if (b->panel() == "popup_numpad")
	{
		bncs_string currenttext="";
		textGet("text", "popup_numpad", "Keyboard", currenttext);
		//do something with the text.

		debug(bncs_string("b->sub(0) = %1").arg(b->sub(0)));
		//user wants this value
		if (b->sub(0) == "return") {
			panelDestroy("popup_numpad");
			debug(bncs_string("NS2000::buttonCallback: RETURN(accpet value) called. Current text =\"%1\"   validation reason = \"%2\"").arg(currenttext).arg(popupreason));
			if (validatetext(currenttext, popupreason)) { //if the input is within acceptable limits
				if (popupreason == "freq") {
					m_dblNsFreqEdit = currenttext.toDouble();
					textPut("text",currenttext + " MHz", PANEL_DEMODULATOR,"lbl_ns_freq_edited");
					controlShow(PANEL_DEMODULATOR, "lbl_ns_freq_edited");
					controlShow(PANEL_DEMODULATOR, "btn_ns_freq_live_edited");
					controlHide(PANEL_DEMODULATOR, "btn_ns_freq_live_unedited");
					isedited_freq = true;
				}
				else if (popupreason == "lo") {
					m_dblNsLoEdit = currenttext.toDouble();
					textPut("text", currenttext + " MHz", PANEL_DEMODULATOR, "lbl_ns_lo_edited");
					controlShow(PANEL_DEMODULATOR, "lbl_ns_lo_edited");
					controlShow(PANEL_DEMODULATOR, "btn_ns_lo_live_edited");
					controlHide(PANEL_DEMODULATOR, "btn_ns_lo_live_unedited");
					isedited_lo = true;
				}
				else if (popupreason == "sym") {
					m_dblNsSymEdit = currenttext.toDouble();
					textPut("text", currenttext + " MS/s", PANEL_DEMODULATOR, "lbl_ns_sym_edited");
					controlShow(PANEL_DEMODULATOR, "lbl_ns_sym_edited");
					controlShow(PANEL_DEMODULATOR, "btn_ns_sym_live_edited");
					controlHide(PANEL_DEMODULATOR, "btn_ns_sym_live_unedited");
					isedited_sym = true;
				}
				else {
					//no idea why you had the popup keypad open, but you did. we'll just close it.
				}
			}
		}
		else if (b->id() == "pst_9750")
		{
			panelDestroy("popup_numpad");
			m_dblNsLoEdit = 9750;
			textPut("text", "9750MHz", PANEL_DEMODULATOR, "lbl_ns_lo_edited");
			controlShow(PANEL_DEMODULATOR, "lbl_ns_lo_edited");
			controlShow(PANEL_DEMODULATOR, "btn_ns_lo_live_edited");
			controlHide(PANEL_DEMODULATOR, "btn_ns_lo_live_unedited");
			isedited_lo = true;
		}
		else if (b->id() == "pst_10600")
		{
			panelDestroy("popup_numpad");
			m_dblNsLoEdit = 10600;
			textPut("text", "10600MHz", PANEL_DEMODULATOR, "lbl_ns_lo_edited");
			controlShow(PANEL_DEMODULATOR, "lbl_ns_lo_edited");
			controlShow(PANEL_DEMODULATOR, "btn_ns_lo_live_edited");
			controlHide(PANEL_DEMODULATOR, "btn_ns_lo_live_unedited");
			isedited_lo = true;
		}
		else {
			//new preset used perhaps?
		}
	}
	else if(b->panel() == PANEL_DEMODULATOR)
	{
		if (b->id() == "easter_egg") {
			//no easter eggs. ever.
		}

		else if (b->id() == "btn_ns_freq_live_unedited") {
			popupreason = "freq";
			//popup the editor
			panelPopup("popup_numpad", "numpad.bncs_ui");
			textPut("text", "Enter Downlink frequency (MHz)", "popup_numpad", "hint");
		}
		else if (b->id() == "btn_ns_freq_live_edited") {
			popupreason = "freq";
			panelPopup("popup_numpad", "numpad.bncs_ui");
			textPut("text", "Enter Downlink frequency (MHz)", "popup_numpad", "hint");
		}

		else if (b->id() == "btn_ns_lo_live_edited")
		{
			popupreason = "lo";
			panelPopup("popup_numpad", "numpad_psts.bncs_ui");
			textPut("text", "Enter Local Oscillator frequency (MHz)", "popup_numpad", "hint");
		}
		else if (b->id() == "btn_ns_lo_live_unedited")
		{
			popupreason = "lo";
			panelPopup("popup_numpad", "numpad_psts.bncs_ui");
			textPut("text", "Enter Local Oscillator frequency (MHz)", "popup_numpad", "hint");
		}

		else if (b->id() == "btn_ns_sym_live_edited")
		{
			popupreason = "sym";
			panelPopup("popup_numpad", "numpad.bncs_ui");
			textPut("text", "Enter Symbol Rate (MS/s)", "popup_numpad", "hint");
		}
		else if (b->id() == "btn_ns_sym_live_unedited")
		{
			popupreason = "sym";
			panelPopup("popup_numpad", "numpad.bncs_ui");
			textPut("text", "Enter Symbol Rate (MS/s)", "popup_numpad", "hint");
		}


		else if (b->id() == "mem_RO") {
			//[13588] NS2000::buttonCallback panel = 1 btn = mem_RO command = presetvaluechangerequested value = 6 --> 6 being the enum value, internally this doesn't atter, but presentation side it will need noting
		}
		else if (b->id() == "mem_demodmode") {

			//if we turn it NS3 (3): hide,dis mem_label_ns4nlc
						          //* hide  mem_ns4nlc
								//show mem_label_ns3nl
								//show mem_ns3nl
								//set mem copy of ns3nl to OFF
			//if we turn it NS4 (4) : show mem_label_ns4nlc
						          //* show  mem_ns4nlc
								//hide mem_label_ns3nl
								//hide mem_ns3nl
			//if other (0,1,2,5,255 etc)
								//hide all four.
			
			//## If the demodulator state list changes in future (not expected, new modes will just get added to the enum) then you will need to change the numbers to match.

			if (b->value() == "3") {
				controlHide(PANEL_DEMODULATOR, "mem_ns4nlc");
				controlHide(PANEL_DEMODULATOR, "mem_label_ns4nlc");
				controlShow(PANEL_DEMODULATOR, "mem_ns3nl");
				controlShow(PANEL_DEMODULATOR, "mem_label_ns3nl");
				
				//if AND ONLY IF this is in response to a user request (not just a live update) set mem_ns3nl to off (default)
				if (b->command() == "presetvaluechangerequested"){
					textPut("value", "0", PANEL_DEMODULATOR, "mem_ns3nl");
				}
			}
			else if (b->value() == "4") {
				controlShow(PANEL_DEMODULATOR, "mem_ns4nlc");
				controlShow(PANEL_DEMODULATOR, "mem_label_ns4nlc");
				controlHide(PANEL_DEMODULATOR, "mem_ns3nl");
				controlHide(PANEL_DEMODULATOR, "mem_label_ns3nl");
			}
			else {
				controlHide(PANEL_DEMODULATOR, "mem_ns4nlc");
				controlHide(PANEL_DEMODULATOR, "mem_label_ns4nlc");
				controlHide(PANEL_DEMODULATOR, "mem_ns3nl");
				controlHide(PANEL_DEMODULATOR, "mem_label_ns3nl");
			}
		}
		else if (b->id() == "mem_ns3nl")
		{

		}
		else if (b->id() == "mem_ns4nlc") {

		}

		else if (b->id() == "take") {
			//take the enums (all prefixed with mem_)
			bncs_stringlist memList = getIdList(PANEL_DEMODULATOR, "mem_");
			for (int i = 0; i < memList.count(); i++)
			{
				textPut("takepreset", PANEL_DEMODULATOR, memList[i]);
			}

			bncs_string editedvalue = "";
			int newvalue = 0;
			//take the live and edited values. if the parameter has  been edited AND they are different, proceed.
				//convert value (from the "edited" label) to an integer in the novelsat unit
				//compare to current known live value
				//write to dev



				// LO / local oscillator
			textGet("text", PANEL_DEMODULATOR, "lbl_ns_lo_edited", editedvalue);
			if (isedited_lo && (bncs_string(m_dblNsLoLive != m_dblNsLoEdit))) //this may be a bit overzealous?
			{	//strip off MHz, make double, multiply by LOFREQ_MULT:
				newvalue = (int)(editedvalue.replace("MHz", "").toDouble() * LOFREQ_MULT); //strip the units, get the double, multiple by the novelsat scaler, and cast it to an int. Truncation here is OK.
				infoWrite(m_intDevice, bncs_string(newvalue), m_intLoSlot, true);
			}
			controlHide(PANEL_DEMODULATOR, "lbl_ns_lo_edited");
			controlHide(PANEL_DEMODULATOR, "btn_ns_lo_live_edited");
			controlShow(PANEL_DEMODULATOR, "btn_ns_lo_live_unedited");

			//in theory, doing these in this order changes the order they arrive at the driver, and so when you change both, the LO frequency will change first, the NS will then change RX freq.
			//if the opposite is written, then The RX freq is set, the Demod applies it, and then the LO arrives and the change in that causes your RX freq to change, so you end up with "not what you asked for".

				// rx frequency
			textGet("text", PANEL_DEMODULATOR, "lbl_ns_freq_edited", editedvalue);
			if (isedited_freq && (bncs_string(m_dblNsFreqLive != m_dblNsFreqEdit))) //this may be a bit overzealous?
			{	//strip off MHz, make double, multiply by RXFREQ_MULT:
				newvalue = (int)(editedvalue.replace("MHz", "").toDouble()*RXFREQ_MULT); //strip the units, get the double, multiple by the novelsat scaler, and cast it to an int. In this case, truncation instead of rounding is OK, as the multiplier is going to be so big that the odd 50Hz will make "no difference"
				infoWrite(m_intDevice, bncs_string(newvalue), m_intFreqSlot, true);
			}
			controlHide(PANEL_DEMODULATOR, "lbl_ns_freq_edited");
			controlHide(PANEL_DEMODULATOR, "btn_ns_freq_live_edited");
			controlShow(PANEL_DEMODULATOR, "btn_ns_freq_live_unedited");
			


				// symbol rate
			textGet("text", PANEL_DEMODULATOR, "lbl_ns_sym_edited", editedvalue);
			if (isedited_sym && (bncs_string(m_dblNsSymLive != m_dblNsSymEdit))) //this may be a bit overzealous?
			{	//strip off MS/s, make double, multiply by SYMRATE_MULT:
				newvalue = (int)(editedvalue.replace("MS/s", "").toDouble() * SYMRATE_MULT); //strip the units, get the double, multiple by the novelsat scaler, and cast it to an int. Truncation here is OK.
				infoWrite(m_intDevice, bncs_string(newvalue), m_intSymSlot, true);
			}
			controlHide(PANEL_DEMODULATOR, "lbl_ns_sym_edited");
			controlHide(PANEL_DEMODULATOR, "btn_ns_sym_live_edited");
			controlShow(PANEL_DEMODULATOR, "btn_ns_sym_live_unedited");

			//reset the edited value flags
			isedited_freq = false;
			isedited_lo = false;
			isedited_sym = false;

			//emulate the delay / progress bar the NS2000 webgui has 
			delayOnApply();
		}
		else {
			//no idea what button we got, but it isn't one of those.
		}
	}
	else if (b->panel() == PANEL_ENGINEERING)
	{
		//there are no useful buttons on the engineering panel. it's just a status window and some standard double click enums in their own scripts
	}
	else if (b->panel() == PANEL_BROWSER)
	{
		if (b->id() == "deviceip")
		{
			//we don't seem to be getting the callback...
			textPut("homepage", b->value(), PANEL_BROWSER, "browser");
			textPut("text", bncs_string("%1 - %2").arg(m_strInstance).arg(b->value()), PANEL_BROWSER, "banner");
		}
	}
	else {
		//no idea what panel you meant. Maybe there's a new tabbed panel you need to write code for?
	}
}

// all revertives come here
int ns2000::revertiveCallback( revertiveNotify * r )
{
	char format[16];
	char formattedtext[100];
	sprintf_s(format, "%%1.%df", 2);
	//if revertve is ns rx freq
	if ((r->device() == m_intDevice) && (r->index() == m_intFreqSlot)) {
		//convert to MHz (/100000)
		m_dblNsFreqLive = r->sInfo().toDouble()/ RXFREQ_MULT;
		sprintf_s(format, "%%1.%df", 3);
		sprintf_s(formattedtext, format, m_dblNsFreqLive);
		//put MHz value on btn_ns_freq_live_edited
		textPut("text", (string)formattedtext + "MHz", PANEL_DEMODULATOR, "btn_ns_freq_live_edited");
		//put MHz value on btn_ns_freq_live_unedited
		textPut("text", (string)formattedtext + "MHz", PANEL_DEMODULATOR, "btn_ns_freq_live_unedited");
	}

	//if revertive is ns LO freq
	if ((r->device() == m_intDevice) && (r->index() == m_intLoSlot)) {
		//convert to MHz (/100000)
		m_dblNsLoLive = r->sInfo().toDouble() / LOFREQ_MULT;
		sprintf_s(formattedtext, format, m_dblNsLoLive);
		//put MHz value on btn_ns_lo_live_edited
		textPut("text", (string)formattedtext + "MHz", PANEL_DEMODULATOR, "btn_ns_lo_live_edited");
		//put MHz value on btn_ns_lo_live_unedited
		textPut("text", (string)formattedtext + "MHz", PANEL_DEMODULATOR, "btn_ns_lo_live_unedited");
	}

	//if revertive is ns symbol rate
	if ((r->device() == m_intDevice) && (r->index() == m_intSymSlot)) {
		//convert to MS/s (/1,000,000)
		m_dblNsSymLive = r->sInfo().toDouble() / SYMRATE_MULT;
		sprintf_s(format, "%%1.%df", 6);
		sprintf_s(formattedtext, format, m_dblNsSymLive);
		//put MS/s value on btn_ns_sym_live_edited
		textPut("text", (string)formattedtext + "MS/s", PANEL_DEMODULATOR, "btn_ns_sym_live_edited");
		//put MS/s value on btn_ns_sym_live_unedited
		textPut("text", (string)formattedtext + "MS/s", PANEL_DEMODULATOR, "btn_ns_sym_live_unedited");
	}

	if ((r->device() == m_intDevice) && (r->index() == m_intCommsSlot))
	{
		if (r->sInfo() == "0")
		{
			//disable take, write something helpful
			controlDisable(PANEL_DEMODULATOR, "take");
			textPut("text"," Last known settings", PANEL_DEMODULATOR, "grp_demod");
			textPut("text"," Last known status", PANEL_DEMODULATOR, "grp_status");

		}
		else if (r->sInfo() == "1")
		{
			//disable take, write something helpful
			controlEnable(PANEL_DEMODULATOR, "take");
			textPut("text", " Demodulator RF", PANEL_DEMODULATOR, "grp_demod");
			textPut("text", " Status", PANEL_DEMODULATOR, "grp_status");
		}
		else
		{
			//somehow your device is neither connected, nor disconnected.
			//you are probably still in the initialisation stage
			//however, if you are suffering, you may wish to check caplog and the bncs_comms slot of the devicetype file for any unexpected possibilities
		}
	}

	//(The enums and sensibly valued parameters that don't have correction scaling do their own local updates in their scripts)
	return 0;
}

// all database name changes come back here
void ns2000::databaseCallback( revertiveNotify * r )
{
	//we don't have any db callbacks to work with. The only thing in the NS2000 that does is the input indicator, and the instancetoport handles that itself.
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string ns2000::parentCallback( parentNotify *p )
{
	//debug(bncs_string("NS2000::parentCallback command=%1  value=%2").arg(p->command()).arg(p->value()));

	if ( (p->command() == "instance") && ( p->value() != m_strInstance) )
	{	// Our instance is being set/changed
		m_strInstance = p->value();
		//GetLBandRoutername(m_str_Instance)
		textPut("router_instance", GetLBandRouterName(m_strInstance), PANEL_DEMODULATOR, "lband_tally");
		//textPut("instance", m_strInstance, PANEL_DEMODULATOR, "lband_tally");
		
		//feed the input tally the corect router instance (we don't know if we're looking at LDC/NHN/ROW)

		

		//(register for the slots for freq,lo,sym)
		char cThrowaway = 'x';
		getDevSlot(m_strInstance, "bncs_comms", &m_intDevice, &m_intCommsSlot, &cThrowaway);
		getDevSlot(m_strInstance, "nsDemodConfigLineRFFreq", &m_intDevice, &m_intFreqSlot, &cThrowaway);
		getDevSlot(m_strInstance, "nsDemodConfigLineLOFreq", &m_intDevice, &m_intLoSlot, &cThrowaway);
		getDevSlot(m_strInstance, "nsDemodConfigLineSymbolRate", &m_intDevice, &m_intSymSlot, &cThrowaway);

		infoRegister(m_intDevice, m_intFreqSlot, m_intFreqSlot, false);//using this false to terminate all old registering
		infoRegister(m_intDevice, m_intLoSlot, m_intLoSlot, true); //and for this round we just add to the reg
		infoRegister(m_intDevice, m_intSymSlot, m_intSymSlot, true); //...
		infoRegister(m_intDevice, m_intCommsSlot, m_intCommsSlot, true);

		infoPoll(m_intDevice, m_intCommsSlot, m_intCommsSlot);
		infoPoll(m_intDevice, m_intFreqSlot, m_intFreqSlot);
		infoPoll(m_intDevice, m_intLoSlot, m_intLoSlot);
		infoPoll(m_intDevice, m_intSymSlot, m_intSymSlot);

	}
	return "";
}

// timer events come here
void ns2000::timerCallback( int id )
{
	switch( id )
	{
	case TIMER_WAITBAR:
		timerStop(id);
		enableAfterDelay();
	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void ns2000::showFirstTab(void)
{
	if (m_intCurrentTab != 1)
	{
		controlHide("tabs", bncs_string("bg_tab_%1").arg(m_intCurrentTab));
		panelRemove(m_intCurrentTab);

		m_intCurrentTab = 1;
		panelShow(1, "NS2000_demodulator.bncs_ui");
		controlShow("tabs", "bg_tab_1");
		controlHide("tabs", "bg_tab_2");
		controlHide("tabs", "bg_tab_3");
	}
}

void ns2000::delayOnApply()
{
	//##deactivate the take button
	controlDisable(PANEL_DEMODULATOR, "take");

	//##disable all the editables
	controlDisable(PANEL_DEMODULATOR, "mem_RO");
	controlDisable(PANEL_DEMODULATOR, "mem_demodmode");
	controlDisable(PANEL_DEMODULATOR, "mem_ns3nl");
	controlDisable(PANEL_DEMODULATOR, "mem_ns4nlc");
	controlDisable(PANEL_DEMODULATOR, "btn_ns_freq_live_unedited");
	controlDisable(PANEL_DEMODULATOR, "btn_ns_lo_live_unedited");
	controlDisable(PANEL_DEMODULATOR, "btn_ns_sym_live_unedited");


	timerStart(TIMER_WAITBAR, SUITABLE_DELAY);
}

/// <summary>
/// re-enable all the editable fields after a take.
/// </summary>
void ns2000::enableAfterDelay()
{
	//##re-enable all the editables
	controlEnable(PANEL_DEMODULATOR, "mem_RO");
	controlEnable(PANEL_DEMODULATOR, "mem_demodmode");
	controlEnable(PANEL_DEMODULATOR, "mem_ns3nl");
	controlEnable(PANEL_DEMODULATOR, "mem_ns4nlc");
	controlEnable(PANEL_DEMODULATOR, "btn_ns_freq_live_unedited");
	controlEnable(PANEL_DEMODULATOR, "btn_ns_lo_live_unedited");
	controlEnable(PANEL_DEMODULATOR, "btn_ns_sym_live_unedited");
	//##RE-activate the take button
	controlEnable(PANEL_DEMODULATOR, "take");
}

/// <summary>
/// demod Lband range = 950-2150MHz, but demod only talks in Ku freq and LO freq. We need a quick test.
/// </summary>
/// <param name="downlinkFrequency"></param>
/// <param name="LocalOscillatorFrequency"></param>
/// <returns></returns>
int ns2000::LBandFreqCheck(double downlinkFrequency, double LocalOscillatorFrequency) {
	int state = FREQ_OK;
	if ((downlinkFrequency - LocalOscillatorFrequency) < LBAND_MIN) { state = FREQ_LOW; }
	else if ((downlinkFrequency - LocalOscillatorFrequency) > LBAND_MAX) { state = FREQ_HIGH; }
	else { state = FREQ_OK; }
	return state;
}

/// <summary>
/// Headsup: this will need filling out when we know the peculiarities of the demodulator. until then, it just returns all good for everything.
/// For now, this has been set like this because the NS2000 only seems to like setting the LO, THEN the RXfreq., and even its own webgui has seemingly inconsistent behaviour with ordering...
/// </summary>
/// <param name="userinput"></param>
/// <param name="validationreason"></param>
/// <returns></returns>
bool ns2000::validatetext(bncs_string userinput, bncs_string validationreason) {
	if (validationreason == "freq") 
	{
		
		return true;
	}
	else if (validationreason == "lo") { return true; }
	else if (validationreason == "sym") { return true; }
	else {
		//by default just be OK.
		return true;
	}
}

/// <summary>
/// Discovery specific for site determination
/// </summary>
/// <param name="irdinstance">instance name of ird</param>
/// <returns>the lband outer instance name</returns>
bncs_string ns2000::GetLBandRouterName(bncs_string irdinstance) {
//depreciated dirty option
	/*
	bncs_string rtrname = "";
	if (irdinstance.startsWith("12001")) {
		rtrname = "12001/RTR/001";
	}
	else if (irdinstance.startsWith("12305")) {
		rtrname = "12305/RTR/001";
	}
	else if (irdinstance.startsWith("12401")) {
		rtrname = "12401/RTR/001";
	}
	else {
		//you are not looking at an IRD from NHN,LDC, or ROW. add more cases as needed.
		rtrname = "rtr_lband"; //in most installations
	}
	debug(bncs_string("router name is %1").arg(rtrname));
	return rtrname;
	*/

	//slick option
	bncs_config c(bncs_string("instances.%1").arg(irdinstance));
	bncs_string lbandrtr = "";
	while (c.isChildValid())
	{
		bncs_string tag = c.childTag(); //should be "setting"
		if (tag == "setting")
		{
			if (c.childAttr("lband_rtr").length() > 0) {
				lbandrtr = c.childAttr("lband_rtr");
				debug(bncs_string("ns2000::GetLBandRouterName : Childtag=%1  lbandrtr=%2").arg(tag).arg(lbandrtr));
			}
		}
		c.nextChild();
		debug(bncs_string("ns2000::GetLBandRouterName : around we go"));
	}
	//return lbandrtr;
	return lbandrtr;
}