#ifndef RC2500_INCLUDED
	#define RC2500_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class RC2500 : public bncs_script_helper
{
public:
	RC2500( bncs_client_callback * parent, const char* path );
	virtual ~RC2500();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
private:
	void initDevice();
	bncs_string m_strInstance;
	int m_intDevice;
	int m_intOffset;
	bncs_string m_currentPopupEditor;
	bncs_stringlist m_controls;
};


#endif // RC2500_INCLUDED