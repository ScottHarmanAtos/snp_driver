#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "RC2500.h"

#define PANEL_MAIN	1

#define	SLOT_MEMORY_BASE	1000
#define SLOT_JOG			81
#define SLOT_POLJOG			91
#define	SLOT_RECALLGO		51
#define	SLOT_RECALL			52
#define MAX_PRESETS			50
#define SLOT_AZ				61
#define SLOT_EL				62
#define SLOT_POL			71

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( RC2500 )

// constructor - equivalent to ApplCore STARTUP
RC2500::RC2500( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{

	m_strInstance = "";
	m_intDevice = 0;
	m_intOffset = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PANEL_MAIN, "RC2500.bncs_ui" );
}

// destructor - equivalent to ApplCore CLOSEDOWN
RC2500::~RC2500()
{
}

// all button pushes and notifications come here
void RC2500::buttonCallback( buttonNotify *b )
{
	if( b->panel() == PANEL_MAIN )
	{
		//This is called when a memory is recalled
		if(b->id() == "memory")
		{
			bncs_stringlist lValues(b->value(), '|');
			
			//A recalled memory is a delimited list of Parameters*Values pairs (e.g AzimuthPosition*20|ElevationPosition*30|PolarityPosition*45 )
			for( bncs_stringlist::iterator it = lValues.begin() ; it != lValues.end(); ++it )
			{	
				bncs_string parameter, value;
				//This splits the pair on the * (e.g AzimuthPosition*20 : parameter=AzimuthPosition value=20)
				it->split('*', parameter, value);
				debug("RC2500::buttonCallback parameter:%1",parameter);

				//This places the value on the button, to recall the value the button is Named Automove2 plus the Parameter name (e.g Automove2AzimuthPosition)
				textPut("text", value, 1, bncs_string("%1%2").arg("Automove2").arg(parameter));
				//This changes the colour of the button so that it is obvious the value has changed
				textPut( "colour.background=yellow", PANEL_MAIN, bncs_string("%1%2").arg("Automove2").arg(parameter) );
			}
		
		}
		if( b->id() == "btnJogUp")
		{
			if( b->value() == "pressed" )
				infoWrite( m_intDevice, "U", m_intOffset + SLOT_JOG );
//			else if( b->value() == "released" )
//				infoWrite( m_intDevice, "X", m_intOffset + SLOT_JOG );
		}
		else if( b->id() == "btnJogDown")
		{
			if( b->value() == "pressed" )
				infoWrite( m_intDevice, "D", m_intOffset + SLOT_JOG );
//			else if( b->value() == "released" )
//				infoWrite( m_intDevice, "X", m_intOffset + SLOT_JOG );
		}
		else if( b->id() == "btnJogEast")
		{
			if( b->value() == "pressed" )
				infoWrite( m_intDevice, "C", m_intOffset + SLOT_JOG );
//			else if( b->value() == "released" )
//				infoWrite( m_intDevice, "X", m_intOffset + SLOT_JOG );
		}
		else if( b->id() == "btnJogWest")
		{
			if( b->value() == "pressed" )
				infoWrite( m_intDevice, "W", m_intOffset + SLOT_JOG );
//			else if( b->value() == "released" )
//				infoWrite( m_intDevice, "X", m_intOffset + SLOT_JOG );
		}
		else if( b->id() == "btnCCW")
		{
			if( b->value() == "pressed" )
				infoWrite( m_intDevice, "O", m_intOffset + SLOT_JOG );//SLOT_POLJOG );
///			debug("m_intdevice=%1/n", m_strInstance);
		}
		else if( b->id() == "btnCW")
		{
			if( b->value() == "pressed" )
				infoWrite( m_intDevice, "L", m_intOffset + SLOT_JOG );//SLOT_POLJOG );
		}
		else if( b->id() == "btnStop")
		{
				infoWrite( m_intDevice, "X", m_intOffset + SLOT_JOG );
		}
		else if( b->id() == "btnRecallPreset" )
		{
			bncs_string ret;
			textGet( "selected", 1, "lbxPresets", ret );

			// remove the memory number from the front of the string
			ret.remove( 0, 3 );
			infoWrite( m_intDevice, ret, m_intOffset + SLOT_RECALL );

			// I have no idea why you have to write space, H or V to this slot.....but H seems to make it work. Bizarre.
			infoWrite( m_intDevice, "H", m_intOffset + SLOT_RECALLGO );
		}
		else if( b->id() == "Automove2AzPosition" )
		{
			m_currentPopupEditor = b->id();
			panelPopup( 2, "editRange.bncs_ui" );
			textPut( "name=Enter Azimuth", 2, 1 );
			textPut( "min=0", 2, 1 );
			textPut( "max=360", 2, 1 );
			textPut( "dp=1", 2, 1 );
			textPut( "help= Type Azimuth value in here!",2, 1  );
		}
		else if( b->id() == "Automove2ElPosition" )
		{
			m_currentPopupEditor = b->id();
			panelPopup( 2, "editRange.bncs_ui" );
			textPut( "name=Enter Elevation", 2, 1 );
			textPut( "min=0", 2, 1 );
			textPut( "max=180", 2, 1 );
			textPut( "dp=1", 2, 1 );
		}
		else if( b->id() == "Automove2PolPosition" )
		{
			m_currentPopupEditor = b->id();
			panelPopup( 2, "editRange.bncs_ui" );
			textPut( "name=Enter Polarity", 2, 1 );
			textPut( "min=-180", 2, 1 );
			textPut( "max=180", 2, 1 );
			textPut( "dp=1", 2, 1 );
		}
		else if( b->id() == "btnGoAzEl" )
		{
			bncs_string az;
			bncs_string el;
			textGet( "text", 1, "Automove2AzPosition", az );
			textGet( "text", 1, "Automove2ElPosition", el );
			infoWrite( m_intDevice, el,  SLOT_EL + m_intOffset );
			infoWrite( m_intDevice, az,  SLOT_AZ + m_intOffset );
			textPut( "colour.background=", 1, "Automove2AzPosition" );
			textPut( "colour.background=", 1, "Automove2ElPosition" );
		}
		else if( b->id() == "btnGoPol" )
		{
			bncs_string pol;
			textGet( "text", 1, "Automove2PolPosition", pol );
			infoWrite( m_intDevice, pol,  SLOT_POL + m_intOffset );
			textPut( "colour.background=", 1, "Automove2PolPosition" );
		}
	}
	else if( b->panel() == 2 )
	{
		if( b->value().length() )
		{
			textPut( "text", b->value(), 1, m_currentPopupEditor );
			textPut( "colour.background=yellow", 1, m_currentPopupEditor );
		}
		panelDestroy( 2 );
	}
}

// all revertives come here
int RC2500::revertiveCallback( revertiveNotify * r )
{
	if( r->device() == m_intDevice)
	{
		if( r->index() > SLOT_MEMORY_BASE && r->index() <= SLOT_MEMORY_BASE + MAX_PRESETS)
		{
			bncs_string item = bncs_string( "%1" ).arg( r->index() - SLOT_MEMORY_BASE, ' ', 2 );

			textPut( "remove.text", item, 1, "lbxPresets" );

			bncs_string strListItem = bncs_string("%1 %2").arg(r->index() - SLOT_MEMORY_BASE, ' ', 2 ).arg(r->sInfo());
			textPut("add", strListItem, PANEL_MAIN, "lbxPresets");
		}
		else if( r->index() == SLOT_AZ )
		{
			textPut( "text", r->sInfo(), 1, "Automove2AzPosition" );
			textPut( "colour.background=", 1, "Automove2AzPosition" );
		}
		else if( r->index() == SLOT_EL )
		{
			textPut( "text", r->sInfo(), 1, "Automove2ElPosition" );
			textPut( "colour.background=", 1, "Automove2ElPosition" );
		}
		else if( r->index() == SLOT_POL )
		{
			textPut( "text", r->sInfo(), 1, "Automove2PolPosition" );
			textPut( "colour.background=", 1, "Automove2PolPosition" );
		}
	}
	return 0;
}

// all database name changes come back here
void RC2500::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string RC2500::parentCallback( parentNotify *p )
{
	if(p->command() == "instance")
	{
		m_strInstance = p->value();

		if (m_strInstance.length())
		{
			getDev( m_strInstance, &m_intDevice, &m_intOffset); // iDev and iOffset will now contain correct information.
			initDevice();
//			debug("m_strInstance=%1", m_strInstance);
		}
	}

	return "";
}

// timer events come here
void RC2500::timerCallback( int id )
{
}

void RC2500::initDevice()
{
	textPut("clear", "clear", PANEL_MAIN, "lbxPresets");

	//Register and poll for preset range - slots 1001-1030
	infoRegister(m_intDevice, SLOT_MEMORY_BASE + 1, SLOT_MEMORY_BASE + MAX_PRESETS);
	infoPoll(m_intDevice, SLOT_MEMORY_BASE + 1, SLOT_MEMORY_BASE + MAX_PRESETS);

	infoRegister( m_intDevice, SLOT_AZ + m_intOffset, SLOT_AZ + m_intOffset, true );
	infoRegister( m_intDevice, SLOT_EL + m_intOffset, SLOT_EL + m_intOffset, true );
	infoRegister( m_intDevice, SLOT_POL + m_intOffset, SLOT_POL + m_intOffset, true );
	infoPoll( m_intDevice, SLOT_AZ + m_intOffset, SLOT_AZ  + m_intOffset );
	infoPoll( m_intDevice, SLOT_EL + m_intOffset, SLOT_EL + m_intOffset );
	infoPoll( m_intDevice, SLOT_POL + m_intOffset, SLOT_POL + m_intOffset );

}
