#ifndef rc2500_summary_INCLUDED
	#define rc2500_summary_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class rc2500_summary : public bncs_script_helper
{
private:
	bncs_string m_strInstance;
public:
	rc2500_summary( bncs_client_callback * parent, const char* path );
	virtual ~rc2500_summary();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
	
};


#endif // rc2500_summary_INCLUDED