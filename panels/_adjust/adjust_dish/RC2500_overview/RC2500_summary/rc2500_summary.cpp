#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "instanceLookup.h"
#include "rc2500_summary.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( rc2500_summary )

// constructor - equivalent to ApplCore STARTUP
rc2500_summary::rc2500_summary( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( 1, "rc2500_summary.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
rc2500_summary::~rc2500_summary()
{
}

// all button pushes and notifications come here
void rc2500_summary::buttonCallback( buttonNotify *b )
{
	if( b->panel() == 1 && b->id() == "adjust")
	{
		if (navigateCanAdjust(m_strInstance))
			navigateAdjust(m_strInstance);
	}
}

// all revertives come here
int rc2500_summary::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void rc2500_summary::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string rc2500_summary::parentCallback( parentNotify *p )
{
	if (p->command() == "instance")
	{
		m_strInstance = p->value();
	}
	return "";
}

// timer events come here
void rc2500_summary::timerCallback( int id )
{
}
