#ifndef multi_test_INCLUDED
	#define multi_test_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class multi_test : public bncs_script_helper
{
public:
	multi_test( bncs_client_callback * parent, const char* path );
	virtual ~multi_test();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:

	struct device
	{
		bncs_string id;
		bncs_string instance;
		int dev;
		int sources;
		int dests;
		int slots;

		int count;
		int current;
		int last;
		bool inOrder;
		bool isAll;
		bool isClearing;

		void reset(bool fIsAll)
		{
			count = 0;
			current = -1;
			last = -2;
			inOrder = true;
			isAll = fIsAll;
			isClearing = false;
		}

		void setCurrent(int c)
		{
			last = current;
			current = c;
			count++;
			if (current < last)
			{
				inOrder = false;
			}
		}
	};

	bncs_string m_instance;
	bncs_stringlist m_instances;
	device m_info;
	device m_router;

	void goOneSlot(void);
	void goAllSlots(void);
	void goClearSlots(void);
	void goOneDest(void);
	void goAllDests(void);
	void goClearDests(void);
	void d(bncs_string s);
	void display(int base, bool stat1, bool stat2, bool stat3);

};


#endif // multi_test_INCLUDED