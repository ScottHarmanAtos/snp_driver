#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <bncs_datetime.h>
#include "source_package_editor.h"
#include "packager_common.h"
#include "instanceLookup.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(source_package_editor)

#define PANEL_MAIN						1
#define POPUP_EDIT_LABEL				2
#define POPOVER_SELECT_VIDEO_SOURCE		3
#define POPUP_VIDEO_HUB_TAG				4
#define POPOVER_SELECT_AUDIO_PACKAGE	5
#define POPOVER_EDIT_AUDIO_PACKAGE		6
#define	POPUP_CONFIRM_DESTROY			7
#define POPOVER_EDIT_SPEV				8
#define POPUP_LANGUAGE_TAG				9
#define POPOVER_SELECT_ANC_SOURCE		10
#define POPUP_ABANDON_CHANGES			19

#define BTN_APPLY				"apply"
#define BTN_CANCEL				"cancel"
#define BTN_CLOSE				"close"
#define BTN_NONE				"none"
#define BTN_DESTROY				"destroy"
#define BTN_RESET				"reset"
#define BTN_SELECT				"select"
#define BTN_SET					"set"
#define BTN_SET_1				"set_1"
#define BTN_SET_2				"set_2"
#define BTN_SET_3				"set_3"
#define BTN_SET_4				"set_4"

#define BTN_EDIT				"edit"
#define BTN_DELETE				"delete"
#define BTN_NEW					"new"
#define BTN_CLEAR				"clear"
#define BTN_SET_LIST			"set_list"
#define LINE_EDIT				"line_edit"
#define SPEV_LIST				"spev_list"

#define KEYBOARD				"keyboard"

#define FIELD_MCR_NOTES			"mcr_notes"
#define FIELD_PACKAGE_NAME		"package_name"
#define FIELD_PACKAGE_TITLE		"package_title"
#define FIELD_SPEV_ID			"spev_id"
#define FIELD_RECORD_ID			"record_id"

#define FIELD_MCR_UMD_NAME		"mcr_umd"
#define FIELD_PROD_UMD_1		"prod_umd_1"
#define FIELD_PROD_UMD_2		"prod_umd_2"
#define FIELD_PROD_UMD_3		"prod_umd_3"
#define FIELD_PROD_UMD_4		"prod_umd_4"

#define LVW_AP					"lvw_ap"
#define CURRENT_USERS			"current_users"
#define SOURCE_GRID				"source_grid"
#define EDIT_AP					"edit_ap"

#define	LBL_TITLE				"title"
#define	BTN_ALL					"all"
#define LANGUAGE_GRID			"language_grid"

/*
#define DATABASE_SOURCE_NAME	0
#define DATABASE_DEST_NAME		1
#define DATABASE_SOURCE_TITLE	2
#define DATABASE_SOURCE_AP		6
#define DATABASE_MCR_UMD		9
#define DATABASE_PROD_UMD		10
#define DATABASE_SOURCE_VIDEO	11
#define DATABASE_SPEV_ID		31
#define DATABASE_RECORD_ID		32
#define DATABASE_MCR_NOTES		37
#define DATABASE_VIDEO_HUB_TAG	41
#define DATABASE_LANG_TAG		43
#define DATABASE_AP_NAME		60
#define DATABASE_AP_TAGS		63
#define DATABASE_AP_VIDEO_LINK	66
*/

#define PARK_SOURCE_CMD_SLOT	4081

#define AP_LEVEL_COUNT			32
#define MAX_SPEV_COUNT			30
#define MAX_SPEV_LENGTH			30

#define TIMER_INIT_AP_LIST		1
#define TIMEOUT_INIT_AP_LIST	10

//#define	INSTANCE_PACKAGE_ROUTER	"packager_router_main"

// constructor - equivalent to ApplCore STARTUP
source_package_editor::source_package_editor( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//Init vars
	m_intSelectedAudioPackage = -1;
	m_intSelectedANCSource = 0; 
	m_intSelectedVideoSource = 0;
	m_intSelectedVideoSourceAudio = 0;
	m_selectedVideoTag = 0;
	m_intFirstDynamicPackage = 0;
	m_intLastDynamicPackage = 0;

	m_intSelectedVideoLevel = 0;
	m_intSelectedANCLevel = 0;
	m_intSelectedAPLevel = 0;
	m_selectedLanguageFilter = -1;
	m_languageFilter = 0;

	m_strEditLabelField = "";
	m_strEditTimeField = "";
	m_strEditPortField = "";
	m_dynamicPackage = false;
	m_audioPackagePage = 0;
	m_loadAPComplete = false;
	m_fillAPListComplete = false;
	m_showAPListPanel = false;
	m_loadedAPIndex = 0;
	m_loadedAPCount = 0;

	m_devicePackageRouter = 0;
	m_deviceVideoHD = 0;

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow(PANEL_MAIN, "main.bncs_ui");

	initEditor();

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
source_package_editor::~source_package_editor()
{
}

// all button pushes and notifications come here
void source_package_editor::buttonCallback( buttonNotify *b )
{
	debug("source_package_editor::buttonCallback() pnl=%1 id=%2 command=%3 value=%4",b->panel(),b->id(),b->command(),b->value());
	if( b->panel() == PANEL_MAIN )
	{
		if( b->id() == BTN_CLOSE)
		{
			if(m_blnApplyEnabled)
			{
				panelPopup(POPUP_ABANDON_CHANGES, "popup_abandon_changes.bncs_ui");
			}
			else
			{
				navigateAdjust("/close");
			}
		}
		else if (b->id() == BTN_DESTROY)
		{
			panelPopup(POPUP_CONFIRM_DESTROY, "popup_confirm_destroy.bncs_ui");
		}
		else if (b->id() == BTN_RESET)
		{
			//TODO reset package
		}
		else if( b->id() == BTN_APPLY)
		{
			applyChanges();
		}
		else if (b->id() == "debug")
		{
			inspect();
		}

		else if (b->id() == FIELD_MCR_NOTES)		editLabel(b->id());
		else if (b->id() == FIELD_PACKAGE_NAME)		editLabel(b->id());
		else if (b->id() == FIELD_PACKAGE_TITLE)	editLabel(b->id());
		else if (b->id() == FIELD_RECORD_ID)		editLabel(b->id());
		else if (b->id() == FIELD_MCR_UMD_NAME)		editLabel(b->id());
		else if (b->id() == FIELD_PROD_UMD_1)		editLabel(b->id());
		else if (b->id() == FIELD_PROD_UMD_2)		editLabel(b->id());
		else if (b->id() == FIELD_PROD_UMD_3)		editLabel(b->id());
		else if (b->id() == FIELD_PROD_UMD_4)		editLabel(b->id());
		else if (b->id() == FIELD_SPEV_ID) 			showEditSPEV();
		else if (b->id().startsWith("video_"))
		{
			if (b->command() == "display_level")
			{
				m_intSelectedVideoLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_VIDEO_SOURCE, "popover_select_video_source.bncs_ui");
				textPut("text", bncs_string("  Select Video Source - Level %1").arg(b->value()), POPOVER_SELECT_VIDEO_SOURCE, "title");
				m_intSelectedVideoSource = 0;
				m_intSelectedVideoSourceAudio = 0;
				textPut("text", "", POPOVER_SELECT_VIDEO_SOURCE, "selected_source");

				debug("source_package_editor::buttonCallback() m_instanceVideoHD=%1", m_instanceVideoHD);

				//Check if we have been passed a composite instance.
				bncs_string instanceVideoHD_section_01 = "";
				if (instanceLookupComposite(m_instanceVideoHD, "section_01", instanceVideoHD_section_01))
				{
					debug("source_package_editor::buttonCallback() instanceVideoHD_section_01=%1", instanceVideoHD_section_01);
					textPut("instance", instanceVideoHD_section_01, POPOVER_SELECT_VIDEO_SOURCE, SOURCE_GRID);
				}
				else
				{
					debug("source_package_editor::buttonCallback() instanceVideoHD.section_01 NOT FOUND");
				}

				//Show selected videolevel
				showSelectedVideoLevel();

				//deselect ANC Level and hide popover
				panelRemove(POPOVER_SELECT_ANC_SOURCE);
				m_intSelectedANCLevel = 0;

				//deselect AP level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
				m_intSelectedAPLevel = 0;
				showSelectedAPLevel();
			}
			else if (b->command() == "hub_tag_level")
			{
				panelPopup(POPUP_VIDEO_HUB_TAG, "popup_video_hub_tag.bncs_ui");
				bncs_string level;
				bncs_string tag;
				b->value().split(',', level, tag);

				m_intSelectedVideoLevel = level.toInt();
				initVideoHubTagPopup(m_intSelectedVideoLevel, tag.toInt());
			}
			else if (b->command() == "edit_ap")
			{
				//deselect VideoLevel and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_SOURCE);
				m_intSelectedVideoLevel = 0;
				showSelectedVideoLevel();

				//deselect ANC Level and hide popover
				panelRemove(POPOVER_SELECT_ANC_SOURCE);
				m_intSelectedANCLevel = 0;

				//deselect AP level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
				m_intSelectedAPLevel = 0;
				showSelectedAPLevel();

				bncs_string level;
				bncs_string ap;
				b->value().split(',', level, ap);
				int apIndex = ap.toInt();
				panelShow(POPOVER_EDIT_AUDIO_PACKAGE, "popover_edit_ap.bncs_ui");
				textPut("ap", apIndex, POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
				textPut("sp", m_intSourcePackage, POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
				textPut("sp_level", bncs_string("v_%1").arg(b->value().toInt()), POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
			}
			else if (b->command() == "set_anc")
			{
				m_intSelectedANCSource = 0;
				m_intSelectedANCLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_ANC_SOURCE, "popover_select_anc_source.bncs_ui");
				textPut("text", bncs_string("  Select ANC Source - Video Level %1").arg(b->value()), POPOVER_SELECT_ANC_SOURCE, "title");

				//Check if we have been passed a composite instance.
				bncs_string instanceANC_section_01 = "";
				if (instanceLookupComposite(m_instanceANC, "section_01", instanceANC_section_01))
				{
					debug("source_package_editor::buttonCallback() instanceANC_section_01=%1", instanceANC_section_01);
					textPut("instance", instanceANC_section_01, POPOVER_SELECT_ANC_SOURCE, SOURCE_GRID);
				}
				else
				{
					debug("source_package_editor::buttonCallback() instanceANC.section_01 NOT FOUND");
				}

				showEditANC();

				//deselect VideoLevel and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_SOURCE);
				m_intSelectedVideoLevel = 0;
				showSelectedVideoLevel();

				//deselect AP level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
				m_intSelectedAPLevel = 0;
				showSelectedAPLevel();
			}
		}
		else if (b->id().startsWith("ap_"))
		{
			if (b->command() == "display_level")
			{
				m_intSelectedAPLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_AUDIO_PACKAGE, "popover_select_audio_package.bncs_ui");
				m_showAPListPanel = true;

				textPut("text", bncs_string("  Select Audio Package - Level %1").arg(b->value()), POPOVER_SELECT_AUDIO_PACKAGE, "title");
				m_intSelectedAudioPackage = -1;
				textPut("text", "", POPOVER_SELECT_AUDIO_PACKAGE, "selected_ap");

				textPut("text", "Edit AP| | ", POPOVER_SELECT_AUDIO_PACKAGE, EDIT_AP);
				controlDisable(POPOVER_SELECT_AUDIO_PACKAGE, EDIT_AP);

				//Show selected ap level
				showSelectedAPLevel();
				if (m_loadAPComplete == true && m_fillAPListComplete == false)
				{
					showAPList();
				}

				//clear any existing selection
				textPut("clearSelection", POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
				controlDisable(POPOVER_SELECT_AUDIO_PACKAGE, BTN_SET);
				controlEnable(POPOVER_SELECT_AUDIO_PACKAGE, BTN_NONE);

				textPut("text", "", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
				textPut("statesheet", "enum_deselected", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");

				//deselect VideoLevel and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_SOURCE);
				m_intSelectedVideoLevel = 0;
				showSelectedVideoLevel();
			}
			else if (b->command() == "edit_ap")
			{
				//NOTE assumption that it is possible to edit an AP without having selected the AP level
				// i.e. at any time including when a video level had been selected

				//deselect VideoLevel and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_SOURCE);
				m_intSelectedVideoLevel = 0;
				showSelectedVideoLevel();

				//deselect AP level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
				m_intSelectedAPLevel = 0;
				showSelectedAPLevel();

				int apIndex = b->value().toInt();
				panelShow(POPOVER_EDIT_AUDIO_PACKAGE, "popover_edit_ap.bncs_ui");
				textPut("ap", apIndex, POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
				textPut("sp", m_intSourcePackage, POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");

				bncs_stringlist controlIndex(b->id(), '_');
				int index = controlIndex[1].toInt();
				textPut("sp_level", bncs_string("a_%1").arg(index), POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
			}
		}
		else if (b->id().startsWith("ap-page_"))
		{
			bncs_stringlist controlIndex(b->id(), '_');
			int index = controlIndex[1].toInt();
			showAudioPackagePage(index);
		}
	}
	else if (b->panel() == POPOVER_SELECT_VIDEO_SOURCE)
	{
		if (b->id() == SOURCE_GRID)
		{
			m_intSelectedVideoSource = b->value().toInt();
			showSourceAudio();
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("index", 0, POPOVER_SELECT_VIDEO_SOURCE, SOURCE_GRID);
			m_intSelectedVideoSource = 0;
			showSourceAudio();

			//Must be able to set a video level to none
			controlEnable(POPOVER_SELECT_VIDEO_SOURCE, BTN_SET);
		}
		else if (b->id() == BTN_SET)
		{
			//TODO - why are these only setting video 1?
			//m_spEdit.video_1.video = m_intSelectedVideoSource;
			//m_spEdit.video_1.ap = m_intSelectedVideoSourceAudio;
			panelRemove(POPOVER_SELECT_VIDEO_SOURCE);
			setEditVideo(m_intSelectedVideoLevel, m_intSelectedVideoSource, m_intSelectedVideoSourceAudio);
			m_intSelectedVideoLevel = 0;
			showSelectedVideoLevel();
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelRemove(POPOVER_SELECT_VIDEO_SOURCE);
			m_intSelectedVideoLevel = 0;
			showSelectedVideoLevel();
		}
	}
	else if (b->panel() == POPOVER_SELECT_ANC_SOURCE)
	{
		if (b->id() == SOURCE_GRID)
		{
			m_intSelectedANCSource = b->value().toInt();
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("index", 0, POPOVER_SELECT_ANC_SOURCE, SOURCE_GRID);
			m_intSelectedANCSource = 0;

			//Must be able to set a video level to none
			controlEnable(POPOVER_SELECT_ANC_SOURCE, BTN_SET);
		}
		else if (b->id() == BTN_SET_1)
		{
			panelRemove(POPOVER_SELECT_ANC_SOURCE);
			setEditANC(m_intSelectedANCLevel, 1, m_intSelectedANCSource);
			m_intSelectedANCLevel = 0;
		}
		else if (b->id() == BTN_SET_2)
		{
			panelRemove(POPOVER_SELECT_ANC_SOURCE);
			setEditANC(m_intSelectedANCLevel, 2, m_intSelectedANCSource);
			m_intSelectedANCLevel = 0;
		}
		else if (b->id() == BTN_SET_3)
		{
			panelRemove(POPOVER_SELECT_ANC_SOURCE);
			setEditANC(m_intSelectedANCLevel, 3, m_intSelectedANCSource);
			m_intSelectedANCLevel = 0;
		}
		else if (b->id() == BTN_SET_4)
		{
			panelRemove(POPOVER_SELECT_ANC_SOURCE);
			setEditANC(m_intSelectedANCLevel, 4, m_intSelectedANCSource);
			m_intSelectedANCLevel = 0;
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelRemove(POPOVER_SELECT_ANC_SOURCE);
			m_intSelectedANCLevel = 0;
		}
	}
	else if (b->panel() == POPOVER_SELECT_AUDIO_PACKAGE)
	{
		if (b->id() == LVW_AP)
		{
			m_intSelectedAudioPackage = b->value().toInt();
			showSelectedAudioPackage();
			controlEnable(POPOVER_SELECT_AUDIO_PACKAGE, BTN_NONE);
		}
		else if (b->id() == BTN_SET)
		{
			panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
			setEditAudioPackage(m_intSelectedAPLevel, m_intSelectedAudioPackage);
			m_intSelectedAPLevel = 0;
			showSelectedAPLevel();
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("clearSelection", POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
			m_intSelectedAudioPackage = 0;
			textPut("text", "None", POPOVER_SELECT_AUDIO_PACKAGE, "selected_ap");
			textPut("text", "", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
			textPut("statesheet", "enum_deselected", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
			textPut("text", "Edit AP| | ", POPOVER_SELECT_AUDIO_PACKAGE, EDIT_AP);
			controlDisable(POPOVER_SELECT_AUDIO_PACKAGE, EDIT_AP);

			//Must be able to set an AP level to none
			controlEnable(POPOVER_SELECT_AUDIO_PACKAGE, BTN_SET);
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
			m_intSelectedAPLevel = 0;
			showSelectedAPLevel();
		}
		else if (b->id() == "language_grid")
		{
			if (b->command() == "select")
			{
				int lang = b->value().toInt();
				fillAPListview(lang);

				textPut("selected", lang, POPOVER_SELECT_AUDIO_PACKAGE, "language_grid");
			}
		}
		else if (b->id() == ("lang_all"))
		{
			int lang = 0;
			textPut("selected", lang, POPOVER_SELECT_AUDIO_PACKAGE, "language_grid");

			panelPopup(POPUP_LANGUAGE_TAG, "popup_language_tag.bncs_ui");
			textPut("text", " Set Language Tag Filter", POPUP_LANGUAGE_TAG, LBL_TITLE);
			textPut("instance", m_instancePackagerRouterMain, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
			textPut("selected", m_selectedLanguageFilter, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
		}
		else if (b->id() == ("lang_filter"))
		{
			panelPopup(POPUP_LANGUAGE_TAG, "popup_language_tag.bncs_ui");
			textPut("text", " Set Language Tag Filter", POPUP_LANGUAGE_TAG, LBL_TITLE);
			textPut("instance", m_instancePackagerRouterMain, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
			textPut("selected", m_selectedLanguageFilter, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
		}
		else if (b->id() == EDIT_AP)
		{
			if (m_intSelectedAudioPackage > 0)
			{
				//NOTE assumption that it is possible to edit an AP without having selected the AP level
				// i.e. at any time including when a video level had been selected

				//deselect AP level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_PACKAGE);
				m_intSelectedAPLevel = 0;
				showSelectedAPLevel();

				panelShow(POPOVER_EDIT_AUDIO_PACKAGE, "popover_edit_ap.bncs_ui");
				textPut("ap", m_intSelectedAudioPackage, POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
				textPut("sp", m_intSourcePackage, POPOVER_EDIT_AUDIO_PACKAGE, "audio_package_edit_view");
			}
		}
	}
	else if (b->panel() == POPOVER_EDIT_AUDIO_PACKAGE)
	{
		if (b->id() == "audio_package_edit_view")
		{
			if (b->command() == "action" && b->value() == "close")
			{
				panelRemove(POPOVER_EDIT_AUDIO_PACKAGE);
			}
		}
	}
	else if (b->panel() == POPUP_EDIT_LABEL)
	{
		if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_EDIT_LABEL);
		}
		else if( b->id() == "keyboard")
		{
			bncs_string strKeyboard;
			textGet("text", POPUP_EDIT_LABEL, "keyboard", strKeyboard);
			panelDestroy(POPUP_EDIT_LABEL);
			debug("source_package_editor::buttonCallback() field=%1 edit value=%2", m_strEditLabelField, strKeyboard);
			updateLabel(strKeyboard);
		}
	}
	else if (b->panel() == POPUP_CONFIRM_DESTROY)
	{
		if (b->id() == "confirm")
		{
			panelDestroy(POPUP_CONFIRM_DESTROY);

			destroyPackage();;
		}
		else if (b->id() == "cancel")
		{
			panelDestroy(POPUP_CONFIRM_DESTROY);
		}
	}
	else if (b->panel() == POPUP_ABANDON_CHANGES)
	{
		if( b->id() == "confirm")
		{
			panelDestroy(POPUP_ABANDON_CHANGES);

			navigateAdjust("/close");
		}
		else if( b->id() == "cancel")
		{
			panelDestroy(POPUP_ABANDON_CHANGES);
		}
	}
	else if (b->panel() == POPUP_VIDEO_HUB_TAG)
	{
		b->dump("source_package_editor::buttonCallback - POPUP_VIDEO_HUB_TAG");
		if (b->id() == "lvw_tags")
		{
			if (b->command() == "cell_selection")
			{
				bncs_string selectedName = b->value();
				textPut("text", selectedName, POPUP_VIDEO_HUB_TAG, "selected_name");
				//[lvw_tags: cell_selection.0.0=LDC Main
			}
			else if (b->command() == "tag")
			{
				m_selectedVideoTag = b->value().toInt();
				textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedVideoTag), POPUP_VIDEO_HUB_TAG, "selected_tag");
			}
		}
		else if (b->id() == "select")
		{
			//m_spEdit.video_1.videoTag = m_selectedVideoTag;
			panelDestroy(POPUP_VIDEO_HUB_TAG);
			setEditVideoTag(m_intSelectedVideoLevel, m_selectedVideoTag);
		}
		else if (b->id() == "cancel")
		{
			panelDestroy(POPUP_VIDEO_HUB_TAG);
		}
	}
	else if (b->panel() == POPOVER_EDIT_SPEV)
	{
		popoverEditSpev(b);
	}
	else if (b->panel() == POPUP_LANGUAGE_TAG)
	{
		popupLanguageTag(b);
	}
}

// all revertives come here
int source_package_editor::revertiveCallback( revertiveNotify * r )
{
	debug("source_package_editor::revertiveCallback() dev=%1 index=%2 value=%3", r->device(), r->index(), r->sInfo());

	return 0;
}

// all database name changes come back here
void source_package_editor::databaseCallback( revertiveNotify * r )
{
	debug("source_package_editor::databaseCallback() dev=%1 database=%2 index=%3 value=%4", r->device(), r->database(), r->index(), r->sInfo());

	if (r->device() == m_devicePackageRouter)
	{
		if (r->index() == m_intSourcePackage)
		{
			if (r->database() == DATABASE_SOURCE_NAME)
			{
				refreshName(r->sInfo());
			}
			else if (r->database() == DATABASE_SOURCE_TITLE)
			{
				refreshTitle(r->sInfo());
			}
			else if (r->database() == DATABASE_SPEV_ID || 
				r->database() == DATABASE_SPEV_09_16 ||
				r->database() == DATABASE_SPEV_17_24 ||
				r->database() == DATABASE_SPEV_25_30)
			{
				refreshSPEVID(r->sInfo());
			}
			else if (r->database() == DATABASE_SPEV_ID)
			{
				//refreshSPEVID(r->sInfo());
			}
			else if (r->database() == DATABASE_RECORD_ID)
			{
				refreshRecordID(r->sInfo());
			}
			else if (r->database() == DATABASE_MCR_NOTES)
			{
				refreshNotes(r->sInfo());
			}
			else if (r->database() == DATABASE_MCR_UMD)
			{
				refreshUMD(r->sInfo());
			}
			else if (r->database() == DATABASE_PROD_UMD)
			{
				refreshProdUMD(r->sInfo());
			}
			else if (r->database() == DATABASE_SOURCE_VIDEO)
			{
				refreshVideoLevels(r->sInfo());
			}
			else if (r->database() == DATABASE_SOURCE_AP)
			{
				refreshAPLevels(r->sInfo());
			}
		}
		
		if (r->database() == DATABASE_AP_TAGS)
		{
			refreshAPTags(r->index(), r->sInfo());
		}
		else if (r->database() == DATABASE_AP_NAME)
		{
			refreshAPName(r->index(), r->sInfo());
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string source_package_editor::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		bncs_stringlist sl;
				
		return sl.toString( '\n' );
	}
	else if(p->command() == "raw_parameters")
	{
		int intIndex = p->value().toInt();
		if(intIndex > 0)
		{
			m_intSourcePackage = intIndex;
			textPut("source_package", m_intSourcePackage, PANEL_MAIN, "mm_manage");
			editPackage();
		}
	}
	return "";
}

// timer events come here
void source_package_editor::timerCallback( int id )
{
	if (id == TIMER_INIT_AP_LIST)
	{
		timerStop(TIMER_INIT_AP_LIST);
		loadAudioPackages();
	}
}

void source_package_editor::httpCallback(httpNotify *h)
{
	//debug("source_package_editor::httpCallback() got data %1 %2 %3", h->url(), (int)h->data().length(), h->ok());

	if (h->ok())
	{
		//Ensure its the one we care about
		if (h->url() == m_lastSpevRequest)
		{
			debug("source_package_editor::httpCallback() got data %1", h->data());
			checkSpevClash(h->data());
		}
		else if (h->url() == m_lastStatusRequest)
		{
			debug("source_package_editor::httpCallback() got data %1", h->data());
			debug("source_package_editor::httpCallback() status url - server=%1", m_apiServer);
		}
	}
	else
	{
		debug("source_package_editor::httpCallback() !h->ok() url=%1", h->url());
		if (h->url() == m_lastStatusRequest)
		{
			debug("source_package_editor::httpCallback() api server=%1 not responding", m_apiServer);
			for (bncs_stringlist::iterator it = m_apiServerList.begin(); it != m_apiServerList.end(); it++)
			{
				bncs_string server = *it;
				if (server != m_apiServer)
				{
					m_apiServer = server;
					sendStatusRequest();
					break;
				}
			}
		}
	}
}

void source_package_editor::editPackage()
{
	m_spDiff.reset();
	m_spEdit.reset();
	m_blnSourceLineup = false;
	m_sltStack.clear();
	m_blnApplyEnabled = false;


	controlDisable(PANEL_MAIN, BTN_APPLY);

	//Show index and current name of package
	textPut("text", m_intSourcePackage, PANEL_MAIN, "package_index");
	bncs_string strPackageName;
	routerName(m_devicePackageRouter, DATABASE_SOURCE_NAME, m_intSourcePackage, strPackageName);
	m_spDiff.name = strPackageName;
	m_spEdit.name = strPackageName;
	textPut("text", "  " + strPackageName.replace("|", "|  "), PANEL_MAIN, FIELD_PACKAGE_NAME);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PACKAGE_NAME);

	//Get Video Levels
	bncs_string strVideoLevels;
	routerName(m_devicePackageRouter, DATABASE_SOURCE_VIDEO, m_intSourcePackage, strVideoLevels);
	refreshVideoLevels(strVideoLevels);

	//Get Audio Package Levels
	bncs_string strAPLevels;
	routerName(m_devicePackageRouter, DATABASE_SOURCE_AP, m_intSourcePackage, strAPLevels);
	refreshAPLevels(strAPLevels);

	showAudioPackagePage(1);

	//Get IDs
	bncs_string spevs = readSpevDatabases();
	textPut("text", formatSpev(spevs), PANEL_MAIN, FIELD_SPEV_ID);

	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_SPEV_ID);
	m_spDiff.spev_id = spevs;
	m_spEdit.spev_id = spevs;

	bncs_string strRecordId;
	routerName(m_devicePackageRouter, DATABASE_RECORD_ID, m_intSourcePackage, strRecordId);
	if (strRecordId == "!!!")
		strRecordId = "";
	textPut("text", "  " + strRecordId, PANEL_MAIN, FIELD_RECORD_ID);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_RECORD_ID);
	m_spDiff.record_id = strRecordId;
	m_spEdit.record_id = strRecordId;

	//Get Labels
	bncs_string strSourceLables;
	//routerName(m_intDevicePackageRouter, DATABASE_SOURCE_LABELS, m_intSourcePackage, strSourceLables);
	bncs_stringlist sltLabels(strSourceLables);

	//Update Package Title
	bncs_string strPackageTitle;
	routerName(m_devicePackageRouter, DATABASE_SOURCE_TITLE, m_intSourcePackage, strPackageTitle);
	textPut("text", "  " + strPackageTitle, PANEL_MAIN, FIELD_PACKAGE_TITLE);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PACKAGE_TITLE);
	m_spDiff.package_title = strPackageTitle;
	m_spEdit.package_title = strPackageTitle;

	//Update MCR Notes
	bncs_string strMCRNotes;
	routerName(m_devicePackageRouter, DATABASE_MCR_NOTES, m_intSourcePackage, strMCRNotes);
	if (strMCRNotes == "!!!")
		strMCRNotes = "";

	bncs_string richNotes = strMCRNotes;
	while (richNotes.contains('|'))
	{
		richNotes.replace("|", "<br>");
	}
	textPut("text", "<qt>" + richNotes + "</qt>", PANEL_MAIN, FIELD_MCR_NOTES);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_MCR_NOTES);
	m_spDiff.mcr_notes = strMCRNotes;
	m_spEdit.mcr_notes = strMCRNotes;

	//Update MCR UMD Name
	bncs_string strUMDName;
	routerName(m_devicePackageRouter, DATABASE_MCR_UMD, m_intSourcePackage, strUMDName);
	textPut("text", "  " + strUMDName, PANEL_MAIN, FIELD_MCR_UMD_NAME);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_MCR_UMD_NAME);
	m_spDiff.mcr_umd = strUMDName;
	m_spEdit.mcr_umd = strUMDName;

	//Update Prod UMD Names
	/*
	[Database_10]
	0001=umd1=,umd2=,umd3=,umd4=
	*/
	bncs_string strProdUMDNames;
	routerName(m_devicePackageRouter, DATABASE_PROD_UMD, m_intSourcePackage, strProdUMDNames);
	bncs_stringlist fields(strProdUMDNames);
	m_spDiff.prod_umd_1 = fields.getNamedParam("umd1");
	m_spEdit.prod_umd_1 = fields.getNamedParam("umd1");
	textPut("text", "  " + m_spEdit.prod_umd_1, PANEL_MAIN, FIELD_PROD_UMD_1);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PROD_UMD_1);

	m_spDiff.prod_umd_2 = fields.getNamedParam("umd2");
	m_spEdit.prod_umd_2 = fields.getNamedParam("umd2");
	textPut("text", "  " + m_spEdit.prod_umd_2, PANEL_MAIN, FIELD_PROD_UMD_2);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PROD_UMD_2);

	m_spDiff.prod_umd_3 = fields.getNamedParam("umd3");
	m_spEdit.prod_umd_3 = fields.getNamedParam("umd3");
	textPut("text", "  " + m_spEdit.prod_umd_3, PANEL_MAIN, FIELD_PROD_UMD_3);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PROD_UMD_3);

	m_spDiff.prod_umd_4 = fields.getNamedParam("umd4");
	m_spEdit.prod_umd_4 = fields.getNamedParam("umd4");
	textPut("text", "  " + m_spEdit.prod_umd_4, PANEL_MAIN, FIELD_PROD_UMD_4);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PROD_UMD_4);

	debug("source_package_editor::editPackage():: package=%1 first_dynamic=%2 last_dynamic=%3", 
		m_intSourcePackage, m_intFirstDynamicPackage, m_intLastDynamicPackage);

	if (m_intSourcePackage >= m_intFirstDynamicPackage && m_intSourcePackage <= m_intLastDynamicPackage)
	{
		m_dynamicPackage = true;
	}
	else
	{
		m_dynamicPackage = false;
	}	
	
	textPut("source_index", m_intSourcePackage, PANEL_MAIN, CURRENT_USERS);

	//Poll for current users
	//infoRegister(m_intDevicePackagerPTI, m_intSourcePackage, m_intSourcePackage);
	//infoPoll(m_intDevicePackagerPTI, m_intSourcePackage, m_intSourcePackage);
}

void source_package_editor::popupLanguageTag(buttonNotify *b)
{
	if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPUP_LANGUAGE_TAG);
	}
	else if (b->id() == BTN_SET)
	{
		fillAPListview(m_selectedLanguageFilter);
		panelDestroy(POPUP_LANGUAGE_TAG);
	}
	else if (b->id() == BTN_ALL)
	{
		m_selectedLanguageFilter = 0;
		textPut("selected", m_selectedLanguageFilter, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
	}
	else if (b->id() == LANGUAGE_GRID)
	{
		if (b->command() == "select")
		{
			m_selectedLanguageFilter = b->value().toInt();
			textPut("selected", m_selectedLanguageFilter, POPUP_LANGUAGE_TAG, LANGUAGE_GRID);
		}
	}
}

void source_package_editor::popoverEditSpev(buttonNotify *b)
{
	if (b->id() == BTN_CANCEL)
	{
		panelDestroy(POPOVER_EDIT_SPEV);
	}
	else if (b->id() == BTN_SET)
	{
		panelDestroy(POPOVER_EDIT_SPEV);

		m_spEdit.spev_id = m_spevEditList.toString();
		textPut("text", formatSpev(m_spEdit.spev_id), PANEL_MAIN, FIELD_SPEV_ID);

		if (m_spDiff.spev_id == m_spEdit.spev_id)
		{
			textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_SPEV_ID);
		}
		else
		{
			textPut("statesheet", "field_modified", PANEL_MAIN, FIELD_SPEV_ID);
		}
		checkApply();
	}
	else if (b->id() == LINE_EDIT)
	{
		//textPut("text", bncs_string("command=%1 value=%2").arg(b->command()).arg(b->value()), POPOVER_EDIT_SPEV, "title");
		if (b->command() == "value")
		{
			//return pressed - set SPEV list
			bncs_string spevList;
			textGet("text", POPOVER_EDIT_SPEV, LINE_EDIT, spevList);
			setSPEVList(spevList);
		}
		else if (b->command() == "text")
		{
			//line edit content has changed
			controlEnable(POPOVER_EDIT_SPEV, BTN_SET_LIST);
		}
	}
	else if (b->id() == BTN_CLEAR)
	{
		textPut("text", "", POPOVER_EDIT_SPEV, LINE_EDIT);
	}
	else if (b->id() == BTN_SET_LIST)
	{
		bncs_string spevList;
		textGet("text", POPOVER_EDIT_SPEV, LINE_EDIT, spevList);
		setSPEVList(spevList);
	}
	else if (b->id() == SPEV_LIST)
	{
		//textPut("text", bncs_string("SPEV_LIST: command=%1 value=%2").arg(b->command()).arg(b->value()), POPOVER_EDIT_SPEV, "title");

		//check if selection is made
		if (b->command() == "selection")
		{
			//enable edit and delete
			controlEnable(POPOVER_EDIT_SPEV, BTN_EDIT);
			controlEnable(POPOVER_EDIT_SPEV, BTN_DELETE);
		}
	}
	else if (b->id() == BTN_EDIT)
	{
		bncs_string spev;
		textGet("selected", POPOVER_EDIT_SPEV, SPEV_LIST, spev);
		bncs_string selectedIndex;
		textGet("selectedindex", POPOVER_EDIT_SPEV, SPEV_LIST, selectedIndex);

		m_selectedSpevItem = selectedIndex.toInt();

		//textPut("text", bncs_string("spev=%1 index=%2").arg(spev).arg(m_selectedSpevItem), POPOVER_EDIT_SPEV, "title");

		textPut("text", spev, POPOVER_EDIT_SPEV, KEYBOARD);
		controlEnable(POPOVER_EDIT_SPEV, KEYBOARD);

		//Disable all other controls that could modify the listview state
		controlDisable(POPOVER_EDIT_SPEV, LINE_EDIT);
		controlDisable(POPOVER_EDIT_SPEV, BTN_CLEAR);
		controlDisable(POPOVER_EDIT_SPEV, BTN_SET_LIST);

		controlDisable(POPOVER_EDIT_SPEV, SPEV_LIST);
		controlDisable(POPOVER_EDIT_SPEV, BTN_EDIT);
		controlDisable(POPOVER_EDIT_SPEV, BTN_DELETE);
		controlDisable(POPOVER_EDIT_SPEV, BTN_NEW);
	}
	else if (b->id() == BTN_DELETE)
	{
		bncs_string selectedIndex;
		textGet("selectedindex", POPOVER_EDIT_SPEV, SPEV_LIST, selectedIndex);

		bncs_stringlist newList;

		for (int item = 0; item < m_spevEditList.count(); item++)
		{
			if (item != selectedIndex)
			{
				newList.append(m_spevEditList[item]);
			}
		}
		setSPEVList(newList.toString());
	}
	else if (b->id() == BTN_NEW)
	{
		m_selectedSpevItem = -1;

		textPut("text", "", POPOVER_EDIT_SPEV, KEYBOARD);
		controlEnable(POPOVER_EDIT_SPEV, KEYBOARD);

		//Disable all other controls that could modify the listview state
		controlDisable(POPOVER_EDIT_SPEV, LINE_EDIT);
		controlDisable(POPOVER_EDIT_SPEV, BTN_CLEAR);
		controlDisable(POPOVER_EDIT_SPEV, BTN_SET_LIST);

		controlDisable(POPOVER_EDIT_SPEV, SPEV_LIST);
		controlDisable(POPOVER_EDIT_SPEV, BTN_EDIT);
		controlDisable(POPOVER_EDIT_SPEV, BTN_DELETE);
		controlDisable(POPOVER_EDIT_SPEV, BTN_NEW);

	}
	else if (b->id() == KEYBOARD)
	{
		//return pressed
		bncs_string keyboard;
		textGet("text", POPOVER_EDIT_SPEV, KEYBOARD, keyboard);

		controlDisable(POPOVER_EDIT_SPEV, KEYBOARD);
		textPut("text", "", POPOVER_EDIT_SPEV, KEYBOARD);

		bncs_string spev = keyboard.stripWhiteSpace().left(MAX_SPEV_LENGTH);

		//two cases
		if (m_selectedSpevItem > -1)
		{
			//1 Edit
			//replace item at selected index in listview
			m_spevEditList[m_selectedSpevItem] = spev;
		}
		else
		{
			//2 New
			m_spevEditList.append(spev);
		}

		setSPEVList(m_spevEditList.toString());

		controlEnable(POPOVER_EDIT_SPEV, LINE_EDIT);
		controlEnable(POPOVER_EDIT_SPEV, BTN_CLEAR);
		controlEnable(POPOVER_EDIT_SPEV, BTN_SET_LIST);

		controlEnable(POPOVER_EDIT_SPEV, SPEV_LIST);
	}
}

void source_package_editor::showEditSPEV()
{
	panelShow(POPOVER_EDIT_SPEV, "popover_edit_spev.bncs_ui");
	
	//initialise list
	int count = setSPEVList(m_spEdit.spev_id);
	textPut("text", "", POPOVER_EDIT_SPEV, KEYBOARD);

	//initialist controls - no item currently selected
	controlDisable(POPOVER_EDIT_SPEV, BTN_SET_LIST);
	controlDisable(POPOVER_EDIT_SPEV, KEYBOARD);
	controlDisable(POPOVER_EDIT_SPEV, BTN_EDIT);
	controlDisable(POPOVER_EDIT_SPEV, BTN_DELETE);
	if (count >= MAX_SPEV_COUNT)
	{
		controlDisable(POPOVER_EDIT_SPEV, BTN_NEW);
	}
	m_selectedSpevItem = -1;
}

bncs_string source_package_editor::formatSpev(bncs_string spevID)
{
	bncs_stringlist spevList(spevID);
	if (spevList.count() > 1)
	{
		if (spevID.length() > 20)
		{
			bncs_string multiSpev = spevID;
			return bncs_string(" %1 +").arg(multiSpev.left(20));
		}
		else
		{
			return bncs_string(" %1").arg(spevID);
		}
	}
	else
	{
		return bncs_string(" %1").arg(spevID);
	}
}

int source_package_editor::setSPEVList(bncs_string spevList)
{
	int count = 0;
	bncs_stringlist items(spevList);
	textPut("clear", POPOVER_EDIT_SPEV, SPEV_LIST);
	m_spevEditList.clear();

	m_mapSpevSourcePackages.clear();
	m_mapSpevSourcePackages["a123"] = "1001";
	m_mapSpevSourcePackages["b123"] = "1002";

	for (int item = 0; item < items.count() && count < MAX_SPEV_COUNT; item++)
	{
		bncs_string spev = items[item];
		m_spevEditList.append(spev);

		bncs_string pixmap = "led16x32_transparent";
		bncs_string fg = "#f0f0f0";

		MAP_STRING_STRING_ITERATOR it = m_mapSpevSourcePackages.find(spev);
		if (it != m_mapSpevSourcePackages.end())
		{
			bncs_string sp = it->second;
			if (sp.toInt() != m_intSourcePackage)
			{
				//pixmap = "led16x32_red";
				//fg = "#ffff00";
			}
		}

		textPut("add", bncs_string("@pixmap=%1~text=%2~fg=%3@").arg(pixmap).arg(spev).arg(fg), POPOVER_EDIT_SPEV, SPEV_LIST);
			count++;
	}

	if (count < MAX_SPEV_COUNT)
	{
		controlEnable(POPOVER_EDIT_SPEV, BTN_NEW);
	}
	else
	{
		controlDisable(POPOVER_EDIT_SPEV, BTN_NEW);
	}
	
	//Reset edit and delete controls
	controlDisable(POPOVER_EDIT_SPEV, BTN_EDIT);
	controlDisable(POPOVER_EDIT_SPEV, BTN_DELETE);

	textPut("text", bncs_string("  Edit SPEV List - %1 items").arg(count), POPOVER_EDIT_SPEV, "grpSpevList");

	controlEnable(POPOVER_EDIT_SPEV, BTN_SET);
	sendSpevRequest(spevList);

	return count;
}

void source_package_editor::checkSpevClash(bncs_string spevData)
{
	//[6744] source_package_editor::httpCallback() got data a123=1001,b123=1002,c123=1001

	bncs_stringlist spevItems(spevData, ',');

	bncs_string rowCount;
	textGet("count", POPOVER_EDIT_SPEV, SPEV_LIST, rowCount);
	int rows = rowCount.toInt();
	debug("source_package_editor::checkSpevClash rows=%1", rows);

	bool setDisabled = false;

	for (int row = 0; row < rows; row++)
	{
		bncs_string spev;
		textGet(bncs_string("row.%1").arg(row), POPOVER_EDIT_SPEV, SPEV_LIST, spev);
		bncs_string sourcePackageList = spevItems.getNamedParam(spev);
		debug("source_package_editor::checkSpevClash row=%1 spev=%2 spList=%3", row, spev, sourcePackageList);

		bncs_string pixmap = "led16x32_transparent";
		bncs_string fg = "#f0f0f0";
		bncs_stringlist sourcePackages(sourcePackageList, '|');

		bool clash = false;

		for (int item = 0; item < sourcePackages.count(); item++)
		{
			int sp = sourcePackages[item].toInt();
			if (sp > 0 && sp != m_intSourcePackage)
			{
				clash = true;
			}
		}

		if (clash)
		{
			pixmap = "led16x32_red";
			fg = "#ffff00";
			setDisabled = true;
		}

		textPut(bncs_string("cell.%1.0").arg(row), bncs_string("@pixmap=%1~text=%2~fg=%3@").arg(pixmap).arg(spev).arg(fg), POPOVER_EDIT_SPEV, SPEV_LIST);
	}

	if (setDisabled)
	{
		controlDisable(POPOVER_EDIT_SPEV, BTN_SET);
	}
	else
	{
		controlEnable(POPOVER_EDIT_SPEV, BTN_SET);
	}
}

void source_package_editor::sendSpevRequest(const bncs_string& spevList)
{
	debug("source_package_editor::sendSpevRequest() spevList=%1", spevList);

	if (spevList.length() > 0)
	{
		bncs_string serverPage = bncs_string("%1/packager/v1/spevUsageQuery/%2").arg(m_apiServer).arg(spevList);

		bncs_string url = bncs_string("%1").arg(serverPage);

		debug("source_package_editor::sendSpevRequest() url=%1", url);

		m_lastSpevRequest = url;
		httpGetString(url);
	}
}

void source_package_editor::sendStatusRequest()
{
	debug("source_package_editor::sendStatusRequest()");

	bncs_string serverPage = bncs_string("%1/heartbeat/v1/status").arg(m_apiServer);

	bncs_string url = bncs_string("%1").arg(serverPage);

	debug("source_package_editor::sendStatusRequest() url=%1", url);

	m_lastStatusRequest = url;
	httpGetString(url);
}
void source_package_editor::initEditor()
{

	//Get Packager Auto first/last dynamic package indexes
	//<packager_config>
	//  <data id="Packager_Auto">
	//    <setting id="first_dynamic_package" value="1" />
	//    <setting id="last_dynamic_package" value="96" />
	//  </data>
	//</packager_config>

	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);

	//Get main router device
	m_devicePackageRouter = m_packagerRouter.destDev(1);
	debug("source_package_editor::initEditor() m_devicePackageRouter=%1", m_devicePackageRouter);

	//Get main router instance
	m_instancePackagerRouterMain = m_packagerRouter.instance(1);

	//m_firstVirtualPackage = m_packagerRouter.firstVirtual();
	//debug("dest_package::initDestPackage() firstVirtual is %1", m_firstVirtualPackage);

	bncs_config cfgPackagerFirstDynamic(bncs_string("packager_config.%1.first_dynamic_package").arg(m_compositePackager));
	if (cfgPackagerFirstDynamic.isValid())
	{
		m_intFirstDynamicPackage = cfgPackagerFirstDynamic.attr("value").toInt();
	}

	bncs_config cfgPackagerLastDynamic(bncs_string("packager_config.%1.last_dynamic_package").arg(m_compositePackager));
	if (cfgPackagerLastDynamic.isValid())
	{
		m_intLastDynamicPackage = cfgPackagerLastDynamic.attr("value").toInt();
	}


	//getDev(INSTANCE_PACKAGE_ROUTER, &m_devicePackageRouter);


	//Register for database changes
	infoRegister(m_devicePackageRouter, 1, 1);

	/* packager_config.xml
	<packager_config>
		<data id="packager_auto_ldc">
			<setting id="video_router_hd" value="instance=vip_router_video_hd_01" />
	*/
	m_instanceVideoHD = m_packagerRouter.getConfigItem("video_router_hd");
	bncs_string instanceVideoHD_section_01 = "";
	if (instanceLookupComposite(m_instanceVideoHD, "section_01", instanceVideoHD_section_01))
	{
		getDev(instanceVideoHD_section_01, &m_deviceVideoHD);
	}
	else
	{
		debug("source_package_editor::initEditor() instanceVideoHD.section_01 NOT FOUND");
	}

	m_instanceANC = m_packagerRouter.getConfigItem("anc_router");
	bncs_string instanceANC_section_01 = "";
	if (instanceLookupComposite(m_instanceANC, "section_01", instanceANC_section_01))
	{
		getDev(instanceANC_section_01, &m_deviceANC);
	}
	else
	{
		debug("source_package_editor::initEditor() instanceANC.section_01 NOT FOUND");
	}


	/*
	bncs_config cfgPackagerVideoRouterHD(bncs_string("packager_config.%1.video_router_hd").arg(m_compositePackager));
	if (cfgPackagerVideoRouterHD.isValid())
	{
		bncs_stringlist sltPackagerRouterVideoHD(cfgPackagerVideoRouterHD.attr("value"));
		m_instanceVideoHD = sltPackagerRouterVideoHD.getNamedParam("instance");
		getDev(m_instanceVideoHD, &m_deviceVideoHD);
	}
	else
	{
		m_deviceVideoHD = -1;
	}
	*/

	//init GV API for SPEV clash checking
	m_site = m_compositePackager.right(3);

	/*
	<sites>
	  <site id="ldc">
		<packager id="c_packager_ldc" />
		<ringmaster id="c_ringmaster_ldc" />
		<riedel_rings>
		  <ring id="c_riedel_london_1" ring="11" />
		  <ring id="c_riedel_london_2" ring="12" />
		</riedel_rings>
		<workspace_assign id="c_wsa_ldc" />
		<oska id="c_oska_ldc" />
		<gv_api>
		  <server id="1" url="http://10.24.150.149:8080/" />
		  <server id="2" url="http://10.24.151.149:8080/" />
		</gv_api>
	  </site>
	</sites>
	*/

	bncs_string siteSection = bncs_string("sites.%1").arg(m_site);
	bncs_config cfgSite = bncs_config(siteSection);
	if (cfgSite.isValid())
	{
		debug("source_package_editor::initEditor() cfgSite valid for section %1", siteSection);
		while (cfgSite.isChildValid())            // are we sitting on a valid entry?
		{
			debug("source_package_editor::initEditor() childTag=%1", cfgSite.childTag());
			if (cfgSite.childTag() == "gv_api")
			{
				debug("source_package_editor::initEditor() found childTag=%1", cfgSite.childTag());
				cfgSite.drillDown();

				while (cfgSite.isChildValid())
				{
					bncs_string id = cfgSite.childAttr("id");
					bncs_string url = cfgSite.childAttr("url");
					debug("source_package_editor::initEditor() id=%1 url=%2", id, url);
					m_apiServerList.append(url);
					cfgSite.nextChild();
				}
				break;
			}
			cfgSite.nextChild();
		}
	}
	debug("source_package_editor::initEditor() api server list: %1", m_apiServerList.toString());

	if (m_apiServerList.count() > 0)
	{
		//send initial test
		//heartbeat/v1/status
		m_apiServer = m_apiServerList[0];
		sendStatusRequest();
	}

	//Start loading audio packages in the background
	timerStart(TIMER_INIT_AP_LIST, TIMEOUT_INIT_AP_LIST);

	controlDisable(PANEL_MAIN, BTN_RESET);

}

void source_package_editor::initVideoHubTagPopup(int level, int videoTag)
{
	textPut("text", bncs_string("Set Video Hub Tag - Level: %1").arg(level), POPUP_VIDEO_HUB_TAG, "title");

	textPut("add", bncs_string("%1;#TAG=%2").arg("[NONE]").arg(0), POPUP_VIDEO_HUB_TAG, "lvw_tags");
	for (int tag = 1; tag <= 30; tag++)
	{
		bncs_string tagValueStatus;
		routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, tag, tagValueStatus);

		bncs_string tagValue;
		bncs_string tagStatus;
		tagValueStatus.split('|', tagValue, tagStatus);

		int status = tagStatus.toInt();
		if (status == 1)
		{
			textPut("add", bncs_string("%1;#TAG=%2").arg(tagValue).arg(tag), POPUP_VIDEO_HUB_TAG, "lvw_tags");
		}
	}

	textPut("selectTag", videoTag, POPUP_VIDEO_HUB_TAG, "lvw_tags");

}

void source_package_editor::editLabel(bncs_string strField)
{
	bncs_string strCaption = "Edit Label";
	bncs_string strCurrentLabel = "";

	int intLabelLength = 0;

	m_strEditLabelField = strField;
	panelPopup(POPUP_EDIT_LABEL, "popup_edit_label.bncs_ui");
	textPut("setFocus", "setFocus", POPUP_EDIT_LABEL, "keyboard");

	if (m_strEditLabelField == FIELD_MCR_NOTES)
	{
		strCurrentLabel = m_spEdit.mcr_notes;
		strCaption = "Edit MCR Notes (use vertical pipe character for a new line - max 255 characters including pipes)";
	}
	else if (m_strEditLabelField == FIELD_PACKAGE_NAME)
	{
		strCurrentLabel = m_spEdit.name;
		strCaption = "Edit Package Name";
	}
	else if (m_strEditLabelField == FIELD_PACKAGE_TITLE)
	{
		strCurrentLabel = m_spEdit.package_title;
		strCaption = "Edit Package Title";
	}
	else if (m_strEditLabelField == FIELD_SPEV_ID)
	{
		strCurrentLabel = m_spEdit.spev_id;
		strCaption = "Edit SPEV";
	}
	else if (m_strEditLabelField == FIELD_RECORD_ID)
	{
		strCurrentLabel = m_spEdit.record_id;
		strCaption = "Edit Record ID";
	}
	else if (m_strEditLabelField == FIELD_MCR_UMD_NAME)
	{
		strCurrentLabel = m_spEdit.mcr_umd;
		strCaption = "Edit MCR UMD Name";
		intLabelLength = 12;
	}
	else if (m_strEditLabelField == FIELD_PROD_UMD_1)
	{
		strCurrentLabel = m_spEdit.prod_umd_1;
		strCaption = "Edit Prod UMD 1";
		intLabelLength = 16;
	}
	else if (m_strEditLabelField == FIELD_PROD_UMD_2)
	{
		strCurrentLabel = m_spEdit.prod_umd_2;
		strCaption = "Edit Prod UMD 2";
		intLabelLength = 8;
	}
	else if (m_strEditLabelField == FIELD_PROD_UMD_3)
	{
		strCurrentLabel = m_spEdit.prod_umd_3;
		strCaption = "Edit Prod UMD 3";
		intLabelLength = 4;
	}
	else if (m_strEditLabelField == FIELD_PROD_UMD_4)
	{
		strCurrentLabel = m_spEdit.prod_umd_4;
		strCaption = "Edit Prod UMD 4";
		intLabelLength = 16;
	}

	//Set keyboard label length limits
	if(intLabelLength > 0)
	{
		textPut("numpadValidation", "Length", POPUP_EDIT_LABEL, "keyboard");
		textPut("minLength", "0", POPUP_EDIT_LABEL, "keyboard");
		textPut("maxLength", intLabelLength, POPUP_EDIT_LABEL, "keyboard");

	}
	else
	{
		textPut("numpadValidation", "", POPUP_EDIT_LABEL, "keyboard");
	}

	textPut("text", strCurrentLabel, POPUP_EDIT_LABEL, "keyboard");
	textPut("text", strCaption, POPUP_EDIT_LABEL, "grpEditLabel");

	debug("source_package_editor::editLabel() Field=%1 Caption='%2' Label='%3'", strField, strCaption, strCurrentLabel);
}

void source_package_editor::updateLabel(bncs_string strLabel)
{
	bool modified = false;

	if (m_strEditLabelField == FIELD_MCR_NOTES)
	{
		m_spEdit.mcr_notes = strLabel;
		modified = (m_spEdit.mcr_notes == m_spDiff.mcr_notes);

		while (strLabel.contains('|'))
		{
			strLabel.replace("|", "<br>");
		}
		textPut("text", "<qt>" + strLabel + "</qt>", PANEL_MAIN, m_strEditLabelField);
	}
	else
	{
		if (m_strEditLabelField == FIELD_PACKAGE_NAME)
		{
			m_spEdit.name = strLabel;
			modified = (m_spEdit.name == m_spDiff.name);
		}
		else if (m_strEditLabelField == FIELD_PACKAGE_TITLE)
		{
			m_spEdit.package_title = strLabel;
			modified = (m_spEdit.package_title == m_spDiff.package_title);
		}
		else if (m_strEditLabelField == FIELD_RECORD_ID)
		{
			m_spEdit.record_id = strLabel;
			modified = (m_spEdit.record_id == m_spDiff.record_id);
		}
		else if (m_strEditLabelField == FIELD_MCR_UMD_NAME)
		{
			m_spEdit.mcr_umd = strLabel;
			modified = (m_spEdit.mcr_umd == m_spDiff.mcr_umd);
		}
		else if (m_strEditLabelField == FIELD_PROD_UMD_1)
		{
			m_spEdit.prod_umd_1 = strLabel;
			modified = (m_spEdit.prod_umd_1 == m_spDiff.prod_umd_1);
		}
		else if (m_strEditLabelField == FIELD_PROD_UMD_2)
		{
			m_spEdit.prod_umd_2 = strLabel;
			modified = (m_spEdit.prod_umd_2 == m_spDiff.prod_umd_2);
		}
		else if (m_strEditLabelField == FIELD_PROD_UMD_3)
		{
			m_spEdit.prod_umd_3 = strLabel;
			modified = (m_spEdit.prod_umd_3 == m_spDiff.prod_umd_3);
		}
		else if (m_strEditLabelField == FIELD_PROD_UMD_4)
		{
			m_spEdit.prod_umd_4 = strLabel;
			modified = (m_spEdit.prod_umd_4 == m_spDiff.prod_umd_4);
		}
		textPut("text", "  " + strLabel, PANEL_MAIN, m_strEditLabelField);
	}

	textPut("statesheet", modified ? "field_unmodified" : "field_modified", PANEL_MAIN, m_strEditLabelField);
	m_strEditLabelField = "";

	checkApply();
}

void source_package_editor::checkApply()
{
	//check ap levels
	bool apDirty = false;
	for (int level = 0; level < AP_LEVEL_COUNT; level++)
	{
		if (m_spEdit.ap[level] != m_spDiff.ap[level])
		{
			apDirty = true;
		}
	}

	if (m_spEdit.name != m_spDiff.name ||
		m_spEdit.package_title != m_spDiff.package_title ||
		m_spEdit.mcr_notes != m_spDiff.mcr_notes ||
		m_spEdit.mcr_umd != m_spDiff.mcr_umd ||
		m_spEdit.record_id != m_spDiff.record_id ||
		m_spEdit.spev_id != m_spDiff.spev_id ||
		m_spEdit.prod_umd_1 != m_spDiff.prod_umd_1 ||
		m_spEdit.prod_umd_2 != m_spDiff.prod_umd_2 ||
		m_spEdit.prod_umd_3 != m_spDiff.prod_umd_3 ||
		m_spEdit.prod_umd_4 != m_spDiff.prod_umd_4 ||
		Video::Compare(m_spEdit.video_1, m_spDiff.video_1) ||
		Video::Compare(m_spEdit.video_2, m_spDiff.video_2) ||
		Video::Compare(m_spEdit.video_3, m_spDiff.video_3) ||
		Video::Compare(m_spEdit.video_4, m_spDiff.video_4) ||
		ANC::Compare(m_spEdit.video_1.anc_1, m_spDiff.video_1.anc_1) ||
		ANC::Compare(m_spEdit.video_2.anc_1, m_spDiff.video_2.anc_1) ||
		ANC::Compare(m_spEdit.video_3.anc_1, m_spDiff.video_3.anc_1) ||
		ANC::Compare(m_spEdit.video_4.anc_1, m_spDiff.video_4.anc_1) ||
		ANC::Compare(m_spEdit.video_1.anc_2, m_spDiff.video_1.anc_2) ||
		ANC::Compare(m_spEdit.video_2.anc_2, m_spDiff.video_2.anc_2) ||
		ANC::Compare(m_spEdit.video_3.anc_2, m_spDiff.video_3.anc_2) ||
		ANC::Compare(m_spEdit.video_4.anc_2, m_spDiff.video_4.anc_2) ||
		ANC::Compare(m_spEdit.video_1.anc_3, m_spDiff.video_1.anc_3) ||
		ANC::Compare(m_spEdit.video_2.anc_3, m_spDiff.video_2.anc_3) ||
		ANC::Compare(m_spEdit.video_3.anc_3, m_spDiff.video_3.anc_3) ||
		ANC::Compare(m_spEdit.video_4.anc_3, m_spDiff.video_4.anc_3) ||
		ANC::Compare(m_spEdit.video_1.anc_4, m_spDiff.video_1.anc_4) ||
		ANC::Compare(m_spEdit.video_2.anc_4, m_spDiff.video_2.anc_4) ||
		ANC::Compare(m_spEdit.video_3.anc_4, m_spDiff.video_3.anc_4) ||
		ANC::Compare(m_spEdit.video_4.anc_4, m_spDiff.video_4.anc_4) ||
		apDirty == true)
	{
		controlEnable(PANEL_MAIN, BTN_APPLY);
		m_blnApplyEnabled = true;
	}
	else
	{
		controlDisable(PANEL_MAIN, BTN_APPLY);
		m_blnApplyEnabled = false;
	}
}

void source_package_editor::applyChanges()
{
	debug("source_package_editor::applyChanges()");
	m_blnApplyEnabled = false;

	//Update Name
	if(m_spEdit.name != m_spDiff.name)
	{
		routerModify(m_devicePackageRouter, DATABASE_SOURCE_NAME, m_intSourcePackage, m_spEdit.name, false);
	}

	//Update Title
	if (m_spEdit.package_title != m_spDiff.package_title)
	{
		routerModify(m_devicePackageRouter, DATABASE_SOURCE_TITLE, m_intSourcePackage, m_spEdit.package_title, false);
	}

	//Update SPEV ID
	if (m_spEdit.spev_id != m_spDiff.spev_id)
	{
		bncs_stringlist spevList0108;
		bncs_stringlist spevList0916;
		bncs_stringlist spevList1724;
		bncs_stringlist spevList2530;

		bncs_string spevData0108;
		bncs_string spevData0916;
		bncs_string spevData1724;
		bncs_string spevData2530;

		bncs_stringlist spevList(m_spEdit.spev_id, ',');
		debug("source_package_editor::applyChanges() spevList.count()=%1", spevList.count());

		for (int spev = 0; spev < 8 && spev < spevList.count(); spev++)
		{
			spevList0108.append(spevList[spev]);
		}
		spevData0108 = spevList0108.toString() + (spevList.count() > 8 ? "+": "");

		for (int spev = 8; spev < 16 && spev < spevList.count(); spev++)
		{
			spevList0916.append(spevList[spev]);
		}
		spevData0916 = spevList0916.toString() + (spevList.count() > 16 ? "+" : "");

		for (int spev = 16; spev < 24 && spev < spevList.count(); spev++)
		{
			spevList1724.append(spevList[spev]);
		}
		spevData1724 = spevList1724.toString() + (spevList.count() > 24 ? "+" : "");

		for (int spev = 24; spev < 30 && spev < spevList.count(); spev++)
		{
			spevList2530.append(spevList[spev]);
		}
		spevData2530 = spevList2530.toString() + (spevList.count() > 8 ? "+" : "");

		routerModify(m_devicePackageRouter, DATABASE_SPEV_ID, m_intSourcePackage, spevData0108, false);
		routerModify(m_devicePackageRouter, DATABASE_SPEV_09_16, m_intSourcePackage, spevData0916, false);
		routerModify(m_devicePackageRouter, DATABASE_SPEV_17_24, m_intSourcePackage, spevData1724, false);
		routerModify(m_devicePackageRouter, DATABASE_SPEV_25_30, m_intSourcePackage, spevData2530, false);
	}

	//Update Record ID
	if (m_spEdit.record_id != m_spDiff.record_id)
	{
		routerModify(m_devicePackageRouter, DATABASE_RECORD_ID, m_intSourcePackage, m_spEdit.record_id, false);
	}

	//Update MCR Notes
	if (m_spEdit.mcr_notes != m_spDiff.mcr_notes)
	{
		routerModify(m_devicePackageRouter, DATABASE_MCR_NOTES, m_intSourcePackage, m_spEdit.mcr_notes, false);
	}

	//Update MCR UMD
	if(m_spEdit.mcr_umd != m_spDiff.mcr_umd)
	{
		routerModify(m_devicePackageRouter, DATABASE_MCR_UMD, m_intSourcePackage, m_spEdit.mcr_umd, false);
	}

	//Update Prod UMDs
	if (m_spEdit.prod_umd_1 != m_spDiff.prod_umd_1 ||
		m_spEdit.prod_umd_2 != m_spDiff.prod_umd_2 ||
		m_spEdit.prod_umd_3 != m_spDiff.prod_umd_3 ||
		m_spEdit.prod_umd_4 != m_spDiff.prod_umd_4)
	{
		bncs_stringlist prodUMD;
		prodUMD.append("umd1=" + m_spEdit.prod_umd_1);
		prodUMD.append("umd2=" + m_spEdit.prod_umd_2);
		prodUMD.append("umd3=" + m_spEdit.prod_umd_3);
		prodUMD.append("umd4=" + m_spEdit.prod_umd_4);
		routerModify(m_devicePackageRouter, DATABASE_PROD_UMD, m_intSourcePackage, prodUMD.toString(), false);

		debug("source_package_editor::applyChanges() write prod UMDs - 1:%1 2:%2 3:%3 4:%4", m_spEdit.prod_umd_1, m_spEdit.prod_umd_2, m_spEdit.prod_umd_3, m_spEdit.prod_umd_4);
	}

	//Update Video Levels
	if (Video::Compare(m_spEdit.video_1, m_spDiff.video_1) ||
		Video::Compare(m_spEdit.video_2, m_spDiff.video_2) || 
		Video::Compare(m_spEdit.video_3, m_spDiff.video_3) || 
		Video::Compare(m_spEdit.video_4, m_spDiff.video_4) ||
		ANC::Compare(m_spEdit.video_1.anc_1, m_spDiff.video_1.anc_1) ||
		ANC::Compare(m_spEdit.video_2.anc_1, m_spDiff.video_2.anc_1) ||
		ANC::Compare(m_spEdit.video_3.anc_1, m_spDiff.video_3.anc_1) ||
		ANC::Compare(m_spEdit.video_4.anc_1, m_spDiff.video_4.anc_1) ||
		ANC::Compare(m_spEdit.video_1.anc_2, m_spDiff.video_1.anc_2) ||
		ANC::Compare(m_spEdit.video_2.anc_2, m_spDiff.video_2.anc_2) ||
		ANC::Compare(m_spEdit.video_3.anc_2, m_spDiff.video_3.anc_2) ||
		ANC::Compare(m_spEdit.video_4.anc_2, m_spDiff.video_4.anc_2) ||
		ANC::Compare(m_spEdit.video_1.anc_3, m_spDiff.video_1.anc_3) ||
		ANC::Compare(m_spEdit.video_2.anc_3, m_spDiff.video_2.anc_3) ||
		ANC::Compare(m_spEdit.video_3.anc_3, m_spDiff.video_3.anc_3) ||
		ANC::Compare(m_spEdit.video_4.anc_3, m_spDiff.video_4.anc_3) ||
		ANC::Compare(m_spEdit.video_1.anc_4, m_spDiff.video_1.anc_4) ||
		ANC::Compare(m_spEdit.video_2.anc_4, m_spDiff.video_2.anc_4) ||
		ANC::Compare(m_spEdit.video_3.anc_4, m_spDiff.video_3.anc_4) ||
		ANC::Compare(m_spEdit.video_4.anc_4, m_spDiff.video_4.anc_4) )
	{
		debug("source_package_editor::applyChanges() write video levels");
		writeVideoLevels();
	}
	else
	{
		debug("source_package_editor::applyChanges() no changes to video levels");
	}

	bool apDirty = false;
	bncs_stringlist sltAPLevels;
	for (int level = 0; level < AP_LEVEL_COUNT; level++)
	{
		sltAPLevels.append(m_spEdit.ap[level]);

		if (m_spEdit.ap[level] != m_spDiff.ap[level])
		{
			apDirty = true;
		}
	}

	if (apDirty == true)
	{
		routerModify(m_devicePackageRouter, DATABASE_SOURCE_AP, m_intSourcePackage, sltAPLevels.toString('|'), false);
	}
	
	//bncs_string strVideoLevels;
	//routerName(m_devicePackageRouter, DATABASE_SOURCE_VIDEO, m_intSourcePackage, strVideoLevels);
	//refreshVideoLevels(strVideoLevels);

	//Do this check here as sometimes we don't get any callbacks but everything is correct.
	checkApply();
}

void source_package_editor::refreshName(bncs_string strNewName)
{
	m_spDiff.name = strNewName;
	m_spEdit.name = strNewName;
	textPut("text", "  " + strNewName.replace("|", "|  "), PANEL_MAIN, FIELD_PACKAGE_NAME);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PACKAGE_NAME);
	checkApply();
}

void source_package_editor::refreshTitle(bncs_string strNewTitle)
{
	textPut("text", "  " + strNewTitle, PANEL_MAIN, FIELD_PACKAGE_TITLE);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PACKAGE_TITLE);
	m_spDiff.package_title = strNewTitle;
	m_spEdit.package_title = strNewTitle;
	checkApply();
}

void source_package_editor::refreshSPEVID(bncs_string strNewID)
{
	bncs_string join = readSpevDatabases();
	
	textPut("text", formatSpev(join), PANEL_MAIN, FIELD_SPEV_ID);

	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_SPEV_ID);
	m_spDiff.spev_id = join;
	m_spEdit.spev_id = join;
	checkApply();
}

bncs_string source_package_editor::readSpevDatabases()
{
	bncs_string spevData0108;
	bncs_string spevData0916;
	bncs_string spevData1724;
	bncs_string spevData2530;
	routerName(m_devicePackageRouter, DATABASE_SPEV_ID, m_intSourcePackage, spevData0108);
	routerName(m_devicePackageRouter, DATABASE_SPEV_09_16, m_intSourcePackage, spevData0916);
	routerName(m_devicePackageRouter, DATABASE_SPEV_17_24, m_intSourcePackage, spevData1724);
	routerName(m_devicePackageRouter, DATABASE_SPEV_25_30, m_intSourcePackage, spevData2530);
	if (spevData0108 == "!!!")	spevData0108 = "";
	if (spevData0916 == "!!!")	spevData0916 = "";
	if (spevData1724 == "!!!")	spevData1724 = "";
	if (spevData2530 == "!!!")	spevData2530 = "";

	if (spevData0108.endsWith("+"))	spevData0108.replace("+", "");
	if (spevData0916.endsWith("+"))	spevData0916.replace("+", "");
	if (spevData1724.endsWith("+"))	spevData1724.replace("+", "");
	if (spevData2530.endsWith("+"))	spevData2530.replace("+", "");

	bncs_stringlist spevList;
	
	bncs_stringlist spevList0108(spevData0108);
	bncs_stringlist spevList0916(spevData0916);
	bncs_stringlist spevList1724(spevData1724);
	bncs_stringlist spevList2530(spevData2530);

	for (int spev0108 = 0; spev0108 < spevList0108.count(); spev0108++)
	{
		bncs_string spev = spevList0108[spev0108];
		if (spev != "")
		{
			spevList.append(spev);
		}
	}
	for (int spev0916 = 0; spev0916 < spevList0916.count(); spev0916++)
	{
		bncs_string spev = spevList0916[spev0916];
		if (spev != "")
		{
			spevList.append(spev);
		}
	}
	for (int spev1724 = 0; spev1724 < spevList1724.count(); spev1724++)
	{
		bncs_string spev = spevList1724[spev1724];
		if (spev != "")
		{
			spevList.append(spev);
		}
	}
	for (int spev2530 = 0; spev2530 < spevList2530.count(); spev2530++)
	{
		bncs_string spev = spevList2530[spev2530];
		if (spev != "")
		{
			spevList.append(spev);
		}
	}

	for (int spev = 0; spev < spevList.count(); spev++)
	{
		debug("source_package_editor::readSpevDatabases() [%1] = %2", spev, spevList[spev]);
	}

	return spevList.toString();
}


void source_package_editor::refreshRecordID(bncs_string strNewID)
{
	textPut("text", "  " + strNewID, PANEL_MAIN, FIELD_RECORD_ID);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_RECORD_ID);
	m_spDiff.record_id = strNewID;
	m_spEdit.record_id = strNewID;
	checkApply();
}

void source_package_editor::refreshNotes(bncs_string strNewNotes)
{
	bncs_string richNotes = strNewNotes;
	while (richNotes.contains('|'))
	{
		richNotes.replace("|", "<br>");
	}
	textPut("text", "<qt>" + richNotes + "</qt>", PANEL_MAIN, FIELD_MCR_NOTES);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_MCR_NOTES);
	m_spDiff.mcr_notes = strNewNotes;
	m_spEdit.mcr_notes = strNewNotes;
	checkApply();
}
void source_package_editor::refreshUMD(bncs_string strNewUMD)
{
	textPut("text", "  " + strNewUMD, PANEL_MAIN, FIELD_MCR_UMD_NAME);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_MCR_UMD_NAME);
	m_spDiff.mcr_umd = strNewUMD;
	m_spEdit.mcr_umd = strNewUMD;
	checkApply();
}

void source_package_editor::refreshProdUMD(bncs_string strNewUMD)
{
	/*
	[Database_10]
	0001=umd1=,umd2=,umd3=,umd4=
	*/
	bncs_stringlist fields(strNewUMD);
	m_spDiff.prod_umd_1 = fields.getNamedParam("umd1");
	m_spEdit.prod_umd_1 = fields.getNamedParam("umd1");
	textPut("text", "  " + m_spEdit.prod_umd_1, PANEL_MAIN, FIELD_PROD_UMD_1);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PROD_UMD_1);

	m_spDiff.prod_umd_2 = fields.getNamedParam("umd2");
	m_spEdit.prod_umd_2 = fields.getNamedParam("umd2");
	textPut("text", "  " + m_spEdit.prod_umd_2, PANEL_MAIN, FIELD_PROD_UMD_2);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PROD_UMD_2);

	m_spDiff.prod_umd_3 = fields.getNamedParam("umd3");
	m_spEdit.prod_umd_3 = fields.getNamedParam("umd3");
	textPut("text", "  " + m_spEdit.prod_umd_3, PANEL_MAIN, FIELD_PROD_UMD_3);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PROD_UMD_3);

	m_spDiff.prod_umd_4 = fields.getNamedParam("umd4");
	m_spEdit.prod_umd_4 = fields.getNamedParam("umd4");
	textPut("text", "  " + m_spEdit.prod_umd_4, PANEL_MAIN, FIELD_PROD_UMD_4);
	textPut("statesheet", "field_unmodified", PANEL_MAIN, FIELD_PROD_UMD_4);

	checkApply();
}

void source_package_editor::refreshAPLevels(bncs_string strNewLevels)
{
	//init AP
	bncs_stringlist sltApLevels(strNewLevels, '|');

	debug("source_package_editor::refreshAPLevels levels=%1", strNewLevels);

	//update levels
	for (int level = 1; level <= AP_LEVEL_COUNT; level++)
	{
		int ap = sltApLevels[level - 1].toInt();
		textPut("diff_data", ap, PANEL_MAIN, bncs_string("ap_%1").arg(level));
		textPut("edit_data", ap, PANEL_MAIN, bncs_string("ap_%1").arg(level));
		m_spDiff.ap[level - 1] = ap;
		m_spEdit.ap[level - 1] = ap;
	}

	checkApply();
}

void source_package_editor::refreshAPTags(int apIndex, bncs_string strNewTags)
{
	bncs_stringlist sltApTag(strNewTags);
	debug("source_package_editor::refreshAPTags ap=%1 - %2", apIndex, strNewTags);

	//example tags: 'video=201#,rev_vision=#,language_tag=2,ifb1=,ifb2=,4w1=#0,4w2=#0'

	//lookup language tag for ap
	bncs_stringlist sltAPTags(strNewTags);
	int languageTag = sltAPTags.getNamedParam("language_tag").toInt();

	bncs_string languageNameStatus;
	routerName(m_devicePackageRouter, DATABASE_LANG_TAG, languageTag, languageNameStatus);

	debug("source_package_editor::refreshAPTags language_tag=%1 - languageName=%2", languageTag, languageNameStatus);

	bncs_stringlist sltNameStatus(languageNameStatus, '|');
	bncs_string languageName = sltNameStatus[0];

	if (languageTag > 0)
	{
		textPut(bncs_string("cellTextTag.%1.2").arg(apIndex), languageName, POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
	}
	else
	{
		textPut(bncs_string("cellTextTag.%1.2").arg(apIndex), "---", POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
	}

	//update the ap map
	MAP_INT_AP_ITERATOR it = m_mapAPList.find(apIndex);
	if (it != m_mapAPList.end())
	{
		CAudioPackage* ap = it->second;
		ap->language_tag = languageTag;
		ap->language_name = languageName;
	}
	else
	{
		if (languageTag > 0)
		{
			bncs_string apName;
			routerName(m_devicePackageRouter, DATABASE_AP_NAME, apIndex, apName);

			//an AP that that did not have a language tag has been modified
			CAudioPackage *ap = new CAudioPackage();
			ap->index = apIndex;
			ap->name = apName;
			ap->language_name = languageName;
			ap->language_tag = languageTag;
			m_mapAPList.insert(make_pair(apIndex, ap));
		}
	}
}

void source_package_editor::refreshAPName(int apIndex, bncs_string name)
{
	debug("source_package_editor::refreshAPName ap=%1 - %2", apIndex, name);

	//update the ap map
	MAP_INT_AP_ITERATOR it = m_mapAPList.find(apIndex);
	if (it != m_mapAPList.end())
	{
		CAudioPackage* ap = it->second;
		ap->name = name;

		//Update listview if shown
		textPut(bncs_string("cellTextTag.%1.1").arg(apIndex), name.replace('|', ' '), POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
	}
	//Note the AP will only be updated if it has already been added to the map
	// for this to have happened it would have had to have a non-zero language tag
	// if it wasn't added then the AP name will be picked up when the AP fields are modified

}

void source_package_editor::refreshVideoLevels(bncs_string strNewLevels)
{
	debug("source_package_editor::refreshVideoLevels strNewLevels=%1", strNewLevels);

	//17501=L1=501#1|0#1|0#1|0#1|0#1|17501#1, L2=#0|#0|#0|#0|#0|#0, L3=#0|#0|#0|#0|#0|#0, L4=#0|#0|#0|#0|#0|#0

	bncs_stringlist sltVideoLevels(strNewLevels);

	for (int level = 1; level <= 4; level++)
	{
		Video* vDiff;
		Video* vEdit;
		if (level == 1)
		{
			vDiff = &m_spDiff.video_1;
			vEdit = &m_spEdit.video_1;
		}
		else if (level == 2)
		{
			vDiff = &m_spDiff.video_2;
			vEdit = &m_spEdit.video_2;
		}
		else if (level == 3)
		{
			vDiff = &m_spDiff.video_3;
			vEdit = &m_spEdit.video_3;
		}
		else if (level == 4)
		{
			vDiff = &m_spDiff.video_4;
			vEdit = &m_spEdit.video_4;
		}

		bncs_string index;
		bncs_string tag;
		bncs_stringlist sltVideo(sltVideoLevels.getNamedParam(bncs_string("L%1").arg(level)), '|');

		// 0: Video#Tag
		// 1: Anc1#Tag
		// 2: Anc2#Tag
		// 3: Anc3#Tag
		// 4: Anc4#Tag
		// 5: Ap#Tag

		debug("source_package_editor::refreshVideoLevels level=%1 sltVideo=%2", level, sltVideo.toString('|'));
		debug("source_package_editor::refreshVideoLevels level=%1 sltVideo[5]=%2", level, sltVideo[5]);


		bncs_string videoTag = sltVideo[0];
		videoTag.split('#', index, tag);
		vDiff->video = index.toInt();
		vEdit->video = index.toInt();
		vDiff->videoTag = tag.toInt();
		vEdit->videoTag = tag.toInt();

		bncs_string anc1 = sltVideo[1];
		anc1.split('#', index, tag);
		vDiff->anc_1.source = index.toInt();
		vEdit->anc_1.source = index.toInt();
		vDiff->anc_1.tag = tag.toInt();
		vEdit->anc_1.tag = tag.toInt();

		bncs_string anc2 = sltVideo[2];
		anc2.split('#', index, tag);
		vDiff->anc_2.source = index.toInt();
		vEdit->anc_2.source = index.toInt();
		vDiff->anc_2.tag = tag.toInt();
		vEdit->anc_2.tag = tag.toInt();

		bncs_string anc3 = sltVideo[3];
		anc3.split('#', index, tag);
		vDiff->anc_3.source = index.toInt();
		vEdit->anc_3.source = index.toInt();
		vDiff->anc_3.tag = tag.toInt();
		vEdit->anc_3.tag = tag.toInt();

		bncs_string anc4 = sltVideo[4];
		anc4.split('#', index, tag);
		vDiff->anc_4.source = index.toInt();
		vEdit->anc_4.source = index.toInt();
		vDiff->anc_4.tag = tag.toInt();
		vEdit->anc_4.tag = tag.toInt();

		bncs_string apTag = sltVideo[5];
		apTag.split('#', index, tag);
		vDiff->ap = index.toInt();
		vEdit->ap = index.toInt();
		vDiff->apTag = tag.toInt();
		vEdit->apTag = tag.toInt();

		//textPut("source", bncs_string("source=%1,state=false").arg(intVideoSource), PANEL_MAIN, COMPONENT_TRACE);

		//Query - what is apTag? is this valid or is the same as video hubTag?
		bncs_string diffData = bncs_string("video=%1,hubTag=%2,ap=%3,apTag=%4,anc_1=%5,anc_2=%6,anc_3=%7,anc_4=%8")
			.arg(vDiff->video).arg(vDiff->videoTag).arg(vDiff->ap).arg(vDiff->apTag)
			.arg(vDiff->anc_1.source).arg(vDiff->anc_2.source).arg(vDiff->anc_3.source).arg(vDiff->anc_4.source);
		bncs_string editData = bncs_string("video=%1,hubTag=%2,ap=%3,apTag=%4,anc_1=%5,anc_2=%6,anc_3=%7,anc_4=%8")
			.arg(vEdit->video).arg(vEdit->videoTag).arg(vEdit->ap).arg(vEdit->apTag)
			.arg(vEdit->anc_1.source).arg(vEdit->anc_2.source).arg(vEdit->anc_3.source).arg(vEdit->anc_4.source);

		debug("source_package_editor::refreshVideoLevels diffData=%1", diffData);

		textPut("diff_data", diffData, PANEL_MAIN, bncs_string("video_%1").arg(level));
		textPut("edit_data", editData, PANEL_MAIN, bncs_string("video_%1").arg(level));
	}

	//updateCoord(sltLevels, m_spDiff.coord_1, m_spEdit.coord_1);
	//updateCoord(sltLevels, m_spDiff.coord_2, m_spEdit.coord_2);
	checkApply();

}

void source_package_editor::writeVideoLevels()
{
	//17501=L1=501#1|0#1|0#1|0#1|0#1|17501#1, L2=#0|#0|#0|#0|#0|#0, L3=#0|#0|#0|#0|#0|#0, L4=#0|#0|#0|#0|#0|#0

	bncs_stringlist levels;
	for (int level = 1; level <= 4; level++)
	{
		Video* vEdit;
		if (level == 1)
		{
			vEdit = &m_spEdit.video_1;
		}
		else if (level == 2)
		{
			vEdit = &m_spEdit.video_2;
		}
		else if (level == 3)
		{
			vEdit = &m_spEdit.video_3;
		}
		else if (level == 4)
		{
			vEdit = &m_spEdit.video_4;
		}

		bncs_stringlist levelData;

		levelData.append(bncs_string("%1#%2").arg(vEdit->video).arg(vEdit->videoTag));			//Video#Tag
		levelData.append(bncs_string("%1#%2").arg(vEdit->anc_1.source).arg(vEdit->videoTag));	//Anc1#Tag
		levelData.append(bncs_string("%1#%2").arg(vEdit->anc_2.source).arg(vEdit->videoTag));	//Anc2#Tag
		levelData.append(bncs_string("%1#%2").arg(vEdit->anc_3.source).arg(vEdit->videoTag));	//Anc3#Tag
		levelData.append(bncs_string("%1#%2").arg(vEdit->anc_4.source).arg(vEdit->videoTag));	//Anc4#Tag
		levelData.append(bncs_string("%1#%2").arg(vEdit->ap).arg(vEdit->apTag));				//Ap#Tag

		//textPut("source", bncs_string("source=%1,state=false").arg(intVideoSource), PANEL_MAIN, COMPONENT_TRACE);
		debug("source_package_editor::writeVideoLevels level=%1 levelData=%2", level, levelData.toString('|'));
		
		levels.append(bncs_string("L%1=%2").arg(level).arg(levelData.toString('|')));
	}
	levels.append(bncs_string("DateTime=%1").arg(bncs_dateTime::nowUTC().toString("yyyy/MM/dd@hh:mm:ss")));
	levels.append(bncs_string("ws=%1").arg(workstation()));

	debug("source_package_editor::writeVideoLevels levels=%1", levels.toString());
	textPut("text", "RM|" + levels.toString(), PANEL_MAIN, "debug");

	routerModify(m_devicePackageRouter, DATABASE_SOURCE_VIDEO, m_intSourcePackage, levels.toString(), true);

}


void source_package_editor::updatePTI(bncs_string strList)
{
	textPut("clear", "clear", PANEL_MAIN, "current_users");
	bncs_stringlist sltPTI(strList);
	
	for(int intItem = 0; intItem < sltPTI.count(); intItem++)
	{
		bncs_string strName;
		routerName(m_devicePackageRouter, DATABASE_DEST_NAME, sltPTI[intItem].toInt(), strName);
		textPut("add", strName.replace('|', ' '), PANEL_MAIN, "current_users");
	}
}

void source_package_editor::setEditANC(int level, int anc, int source)
{
	bncs_string editData = bncs_string("anc_%1=%2").arg(anc).arg(source);

	if (level == 1)
	{
		if (anc == 1) m_spEdit.video_1.anc_1.source = source;
		else if (anc == 2) m_spEdit.video_1.anc_2.source = source;
		else if (anc == 3) m_spEdit.video_1.anc_3.source = source;
		else if (anc == 4) m_spEdit.video_1.anc_4.source = source;

	}
	else if (level == 2)
	{
		if (anc == 1) m_spEdit.video_2.anc_1.source = source;
		else if (anc == 2) m_spEdit.video_2.anc_2.source = source;
		else if (anc == 3) m_spEdit.video_2.anc_3.source = source;
		else if (anc == 4) m_spEdit.video_2.anc_4.source = source;
	}
	else if (level == 3)
	{
		if (anc == 1) m_spEdit.video_3.anc_1.source = source;
		else if (anc == 2) m_spEdit.video_3.anc_2.source = source;
		else if (anc == 3) m_spEdit.video_3.anc_3.source = source;
		else if (anc == 4) m_spEdit.video_3.anc_4.source = source;
	}
	else if (level == 4)
	{
		if (anc == 1) m_spEdit.video_4.anc_1.source = source;
		else if (anc == 2) m_spEdit.video_4.anc_2.source = source;
		else if (anc == 3) m_spEdit.video_4.anc_3.source = source;
		else if (anc == 4) m_spEdit.video_4.anc_4.source = source;
	}
	else
	{
		return;
	}


	debug("source_package_editor::refreshVideoLevels editData=%1", editData);

	textPut("edit_data", editData, PANEL_MAIN, bncs_string("video_%1").arg(level));

	checkApply();
	inspect();
}

void source_package_editor::setEditVideo(int level, int source, int sourceAudio)
{
	//Find corresponding ANC source for selected Video
	int ancSource = 0;

	if (source > 0)
	{
		//Get logical ID for Video Source @@@_v01
		bncs_string videoID;
		routerName(m_deviceVideoHD, DATABASE_SOURCE_ID, source, videoID);

		debug("source_package_editor::setEditVideo() routerName(m_deviceVideoHD=%1, DATABASE_SOURCE_ID=%2, source=%3, videoID)",
			m_deviceVideoHD, DATABASE_SOURCE_ID, source);

		debug("source_package_editor::setEditVideo() source=%1 videoID=%2", source, videoID);

		if (videoID.endsWith("_v01"))
		{
			bncs_string ancID = videoID;
			ancID.replace("_v01", "_m01");

			//Check if there is corresponding @@@_m01 logical ID on the ANC router device
			ancSource = routerIndex(m_deviceANC, DATABASE_SOURCE_ID, ancID);

			debug("source_package_editor::setEditVideo() ancSource=%1 ancID=%2", source, videoID);
		}
		else
		{
			debug("source_package_editor::setEditVideo() videoID with _v01 suffix not found");
		}
	}

	//Video vEdit;
	if (level == 1)
	{
		//vEdit = m_spEdit.video_1;
		m_spEdit.video_1.video = source;
		m_spEdit.video_1.anc_1.source = ancSource;
		m_spEdit.video_1.ap = sourceAudio;
	}
	else if (level == 2)
	{
		//vEdit = m_spEdit.video_2;
		m_spEdit.video_2.video = source;
		m_spEdit.video_2.anc_1.source = ancSource;
		m_spEdit.video_2.ap = sourceAudio;
	}
	else if (level == 3)
	{
		//vEdit = m_spEdit.video_3;
		m_spEdit.video_3.video = source;
		m_spEdit.video_3.anc_1.source = ancSource;
		m_spEdit.video_3.ap = sourceAudio;
	}
	else if (level == 4)
	{
		//vEdit = m_spEdit.video_4;
		m_spEdit.video_4.video = source;
		m_spEdit.video_4.anc_1.source = ancSource;
		m_spEdit.video_4.ap = sourceAudio;
	}
	else
	{
		return;
	}

	//vEdit.video = source;
	//vEdit.ap = sourceAudio;

	bncs_string editData = bncs_string("video=%1,ap=%2,anc_1=%3")
		.arg(source).arg(sourceAudio).arg(ancSource);

	debug("source_package_editor::refreshVideoLevels editData=%1", editData);

	textPut("edit_data", editData, PANEL_MAIN, bncs_string("video_%1").arg(level));

	checkApply();
	inspect();

}

void source_package_editor::setEditVideoTag(int level, int videoTag)
{
	debug("source_package_editor::setEditVideoTag level=%1 videoTag=%2", level, videoTag);

	//Video vEdit;
	if (level == 1)
	{
		//vEdit = m_spEdit.video_1;
		m_spEdit.video_1.videoTag = videoTag;
	}
	else if (level == 2)
	{
		//vEdit = m_spEdit.video_2;
		m_spEdit.video_2.videoTag = videoTag;
	}
	else if (level == 3)
	{
		//vEdit = m_spEdit.video_3;
		m_spEdit.video_3.videoTag = videoTag;
	}
	else if (level == 4)
	{
		//vEdit = m_spEdit.video_4;
		m_spEdit.video_4.videoTag = videoTag;
	}
	else
	{
		return;
	}
	//vEdit.videoTag = videoTag;

	bncs_string editData = bncs_string("hubTag=%1").arg(videoTag);
	debug("source_package_editor::refreshVideoLevels editData=%1", editData);
	textPut("edit_data", editData, PANEL_MAIN, bncs_string("video_%1").arg(level));

	checkApply();
	inspect();

}

void source_package_editor::setEditAudioPackage(int level, int ap)
{
	debug("source_package_editor::setEditAudioPackage() level=%1 ap=%2", level, ap);
	//vEdit.video = source;
	//vEdit.ap = sourceAudio;

	bncs_string editData = bncs_string("%1").arg(ap);

	debug("source_package_editor::setEditAudioPackage editData=%1", editData);

	textPut("edit_data", editData, PANEL_MAIN, bncs_string("ap_%1").arg(level));

	m_spEdit.ap[level - 1] = ap;

	checkApply();
}

void source_package_editor::showAudioPackagePage(int page)
{
	if (m_audioPackagePage != page)
	{
		//highlight buttons
		textPut("statesheet", "enum_deselected", PANEL_MAIN, bncs_string("ap-page_%1").arg(m_audioPackagePage));
	}
	m_audioPackagePage = page;
	textPut("statesheet", "enum_selected", PANEL_MAIN, bncs_string("ap-page_%1").arg(m_audioPackagePage));

	//first hide all
	for (int l = 1; l <= AP_LEVEL_COUNT; l++)
	{

		controlHide(PANEL_MAIN, bncs_string("ap_%1").arg(l));
	}

	//show range of 8 for current page
	int levelOffset = (m_audioPackagePage - 1) * 8;
	for (int level = levelOffset + 1; level <= levelOffset + 8; level++)
	{
		controlShow(PANEL_MAIN, bncs_string("ap_%1").arg(level));
	}
	debug("source_package_editor::showAudioPackagePage() page=%1 levelOffset=%2", m_audioPackagePage, levelOffset);
}

void source_package_editor::showSelectedAudioPackage()
{
	bncs_string selectedAudioPackageName;
	routerName(m_devicePackageRouter, DATABASE_AP_NAME, m_intSelectedAudioPackage, selectedAudioPackageName);

	if (m_intSelectedAudioPackage > 0)
	{
		textPut("text", bncs_string("Edit AP| |%1").arg(selectedAudioPackageName), POPOVER_SELECT_AUDIO_PACKAGE, EDIT_AP);
		controlEnable(POPOVER_SELECT_AUDIO_PACKAGE, EDIT_AP);
	}
	else
	{
		textPut("text", "Edit AP| | ", POPOVER_SELECT_AUDIO_PACKAGE, EDIT_AP);
		controlDisable(POPOVER_SELECT_AUDIO_PACKAGE, EDIT_AP);
	}

	int selectedLanguageTag = 0;
	bncs_string selectedLanguageName = "-";

	MAP_INT_AP_ITERATOR it = m_mapAPList.find(m_intSelectedAudioPackage);
	if (it != m_mapAPList.end())
	{
		CAudioPackage* ap = it->second;
		selectedLanguageTag = ap->language_tag;
		selectedLanguageName = ap->language_name;
	}
	else
	{
		selectedLanguageName = "[not found]";
	}

	//Show selected AP
	bncs_string selectionInfo = bncs_string("AP:|%1| |%2| |Language:|%3")
		.arg(m_intSelectedAudioPackage)
		.arg(selectedAudioPackageName)
		.arg(selectedLanguageName);
	textPut("text", selectionInfo, POPOVER_SELECT_AUDIO_PACKAGE, "selected_ap");

	bncs_stringlist warnings;

	//check if this audio package is valid for selection
	if (selectedLanguageTag > 0)
	{
		//Level Vid 1
		int apVid1 = m_spEdit.video_1.ap;
		if (apVid1 > 0)
		{
			if (getAPLanguageTag(apVid1) == selectedLanguageTag)
			{
				warnings.append("Language Tag|already used as|Source Audio|of level|Vid 1");
			}
		}
		//Level Vid 2
		int apVid2 = m_spEdit.video_2.ap;
		if (apVid2 > 0)
		{
			if (getAPLanguageTag(apVid2) == selectedLanguageTag)
			{
				warnings.append("Language Tag|already used as|Source Audio|of level|Vid 2");
			}
		}
		//Level Vid 3
		int apVid3 = m_spEdit.video_3.ap;
		if (apVid3 > 0)
		{
			if (getAPLanguageTag(apVid3) == selectedLanguageTag)
			{
				warnings.append("Language Tag|already used as|Source Audio|of level|Vid 3");
			}
		}
		//Level Vid 4
		int apVid4 = m_spEdit.video_4.ap;
		if (apVid4 > 0)
		{
			if (getAPLanguageTag(apVid4) == selectedLanguageTag)
			{
				warnings.append("Language Tag|already used as|Source Audio|of level|Vid 4");
			}
		}

		for (int level = 1; level <= AP_LEVEL_COUNT; level++)
		{
			if (level != m_intSelectedAPLevel)
			{
				int ap = m_spEdit.ap[level - 1];
				if (ap > 0)
				{
					if (getAPLanguageTag(ap) == selectedLanguageTag)
					{
						warnings.append(bncs_string("Language Tag|already used|on level|AP %1").arg(level));
					}
				}
			}
		}
	}

	if (warnings.count() > 0)
	{
		bncs_string warning = warnings[0];
		if (warnings.count() > 1)
		{
			warning.append(bncs_string("|+ %1 other level%2")
				.arg(warnings.count() - 1).arg( (warnings.count() == 1) ? "":"s"));
		}
		textPut("text", warning, POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");

		textPut("statesheet", "enum_warning", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
		controlDisable(POPOVER_SELECT_AUDIO_PACKAGE, BTN_SET);
	}
	else
	{
		textPut("text", "Selection|OK", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
		textPut("statesheet", "enum_deselected", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
		textPut("text", "Selection|OK", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
		controlEnable(POPOVER_SELECT_AUDIO_PACKAGE, BTN_SET);
	}
}

int source_package_editor::getAPLanguageTag(int ap)
{
	int languageTag = -1;
	MAP_INT_AP_ITERATOR it = m_mapAPList.find(m_intSelectedAudioPackage);
	it = m_mapAPList.find(ap);
	if (it != m_mapAPList.end())
	{
		CAudioPackage* ap = it->second;
		languageTag = ap->language_tag;
	}
	return languageTag;
}

void source_package_editor::showSourceAudio()
{
	if (m_intSelectedVideoSource > 0)
	{
		bncs_string selectedVideoSourceName;
		routerName(m_deviceVideoHD, DATABASE_SOURCE_NAME, m_intSelectedVideoSource, selectedVideoSourceName);

		textPut("text", bncs_string("Video: %1| |%2").arg(m_intSelectedVideoSource).arg(selectedVideoSourceName), POPOVER_SELECT_VIDEO_SOURCE, "selected_source");

		//find audio source package that has this video linked
		m_intSelectedVideoSourceAudio = routerIndex(m_devicePackageRouter, DATABASE_AP_VIDEO_LINK, m_intSelectedVideoSource);

		controlEnable(POPOVER_SELECT_VIDEO_SOURCE, BTN_SET);
	}
	else
	{
		textPut("text", "Video: None|Selected", POPOVER_SELECT_VIDEO_SOURCE, "selected_source");
		m_intSelectedVideoSourceAudio = 0;
		controlDisable(POPOVER_SELECT_VIDEO_SOURCE, BTN_SET);
	}


	if (m_intSelectedVideoSourceAudio > 0)
	{
		//find the same name in the audio packages database 60
		bncs_string apName;
		routerName(m_devicePackageRouter, DATABASE_AP_NAME, m_intSelectedVideoSourceAudio, apName);


		//Don't need to look up the ap name - the index was found because it was identical to the video name
		textPut("text", bncs_string("Source Audio:|%1| |%2").arg(m_intSelectedVideoSourceAudio).arg(apName), POPOVER_SELECT_VIDEO_SOURCE, "source_audio");
	}
	else
	{
		//No source audio found - index is 0
		textPut("text", "Source Audio|NOT FOUND", POPOVER_SELECT_VIDEO_SOURCE, "source_audio");
	}
}

void source_package_editor::showSelectedVideoLevel()
{
	bncs_stringlist sltVideo = getIdList(PANEL_MAIN, "video_");
	for (int level = 0; level < sltVideo.count(); level++)
	{
		textPut("select_level", m_intSelectedVideoLevel, PANEL_MAIN, sltVideo[level]);
	}
}

void source_package_editor::showEditANC()
{


	Video vDiff;
	Video vEdit;

	if (m_intSelectedANCLevel == 1)
	{
		vDiff = m_spDiff.video_1;
		vEdit = m_spEdit.video_1;
	}
	else if (m_intSelectedANCLevel == 2)
	{
		vDiff = m_spDiff.video_2;
		vEdit = m_spEdit.video_2;
	}
	else if (m_intSelectedANCLevel == 3)
	{
		vDiff = m_spDiff.video_3;
		vEdit = m_spEdit.video_3;
	}
	else if (m_intSelectedANCLevel == 4)
	{
		vDiff = m_spDiff.video_4;
		vEdit = m_spEdit.video_4;
	}

	bncs_string name1 = "---";
	bncs_string name2 = "---";
	bncs_string name3 = "---";
	bncs_string name4 = "---";

	routerName(m_deviceANC, DATABASE_SOURCE_NAME, vEdit.anc_1.source, name1);
	routerName(m_deviceANC, DATABASE_SOURCE_NAME, vEdit.anc_2.source, name2);
	routerName(m_deviceANC, DATABASE_SOURCE_NAME, vEdit.anc_3.source, name3);
	routerName(m_deviceANC, DATABASE_SOURCE_NAME, vEdit.anc_4.source, name4);

	textPut("text", name1.replace('|', ' '), POPOVER_SELECT_ANC_SOURCE, "anc_1");
	textPut("text", name2.replace('|', ' '), POPOVER_SELECT_ANC_SOURCE, "anc_2");
	textPut("text", name3.replace('|', ' '), POPOVER_SELECT_ANC_SOURCE, "anc_3");
	textPut("text", name4.replace('|', ' '), POPOVER_SELECT_ANC_SOURCE, "anc_4");

	textPut("statesheet", (vEdit.anc_1.source == vDiff.anc_1.source)?"level_clean":"level_dirty", POPOVER_SELECT_ANC_SOURCE, "anc_1");
	textPut("statesheet", (vEdit.anc_2.source == vDiff.anc_2.source)?"level_clean":"level_dirty", POPOVER_SELECT_ANC_SOURCE, "anc_2");
	textPut("statesheet", (vEdit.anc_3.source == vDiff.anc_3.source)?"level_clean":"level_dirty", POPOVER_SELECT_ANC_SOURCE, "anc_3");
	textPut("statesheet", (vEdit.anc_4.source == vDiff.anc_4.source)?"level_clean":"level_dirty", POPOVER_SELECT_ANC_SOURCE, "anc_4");
}

void source_package_editor::showSelectedAPLevel()
{
	bncs_stringlist sltAP = getIdList(PANEL_MAIN, "ap_");
	for (int level = 0; level < sltAP.count(); level++)
	{
		textPut("select_level", m_intSelectedAPLevel, PANEL_MAIN, sltAP[level]);
	}
}

void source_package_editor::inspect()
{
	bncs_string inspect = "Inspect:|Diff - Edit| ";

	bncs_stringlist levels;
	for (int level = 1; level <= 4; level++)
	{
		Video* vDiff;
		Video* vEdit;
		if (level == 1)
		{
			vDiff = &m_spDiff.video_1;
			vEdit = &m_spEdit.video_1;
		}
		else if (level == 2)
		{
			vDiff = &m_spDiff.video_2;
			vEdit = &m_spEdit.video_2;
		}
		else if (level == 3)
		{
			vDiff = &m_spDiff.video_3;
			vEdit = &m_spEdit.video_3;
		}
		else if (level == 4)
		{
			vDiff = &m_spDiff.video_4;
			vEdit = &m_spEdit.video_4;
		}

		bncs_stringlist levelData;

		levelData.append(bncs_string("L%1").arg(level));

		levelData.append(bncs_string("%1#%2 - %3#%4").arg(vDiff->video).arg(vDiff->videoTag).arg(vEdit->video).arg(vEdit->videoTag));	//Video#Tag
		levelData.append(bncs_string("%1#%2 - %3#%4").arg(0).arg(0).arg(0).arg(0));													//Anc1#Tag
		levelData.append(bncs_string("%1#%2 - %3#%4").arg(0).arg(0).arg(0).arg(0));													//Anc2#Tag
		levelData.append(bncs_string("%1#%2 - %3#%4").arg(0).arg(0).arg(0).arg(0));													//Anc3#Tag
		levelData.append(bncs_string("%1#%2 - %3#%4").arg(0).arg(0).arg(0).arg(0));													//Anc4#Tag
		levelData.append(bncs_string("%1#%2 - %3#%4").arg(vDiff->ap).arg(vDiff->apTag).arg(vEdit->ap).arg(vEdit->apTag));				//Ap#Tag

		//textPut("source", bncs_string("source=%1,state=false").arg(intVideoSource), PANEL_MAIN, COMPONENT_TRACE);
		debug("source_package_editor::writeVideoLevels level=%1 levelData=%2", level, levelData.toString('|'));
		levels.append(levelData.toString('|'));
	}

	debug("source_package_editor::writeVideoLevels levels=%1", levels.toString());

	inspect.append(levels.toString('|'));

	textPut("text", inspect, PANEL_MAIN, "debug");



}

void source_package_editor::loadAudioPackages()
{

	//Range NHN:  2001-12000
	//Range LDC: 17001-32000

	int start = 2001;		//1
	int end = 32000;		//30001
	int block = 200;

	if (m_loadedAPIndex == 0)
	{
		m_loadedAPIndex = start;
		//controlShow(POPOVER_SELECT_AUDIO_PACKAGE, "loading");
		//textPut("clear", POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
	}

	if (m_loadedAPIndex < end)
	{
		textPut("text", bncs_string(" Loading Audio Packages: %1-%2").arg(m_loadedAPIndex).arg(m_loadedAPIndex + block - 1), POPOVER_SELECT_AUDIO_PACKAGE, "list_container");
		for (int apIndex = m_loadedAPIndex; apIndex < (m_loadedAPIndex + block); apIndex++)
		{
			bncs_string apName;
			routerName(m_devicePackageRouter, DATABASE_AP_NAME, apIndex, apName);

			//lookup language tag for ap
			bncs_string apTags;
			routerName(m_devicePackageRouter, DATABASE_AP_TAGS, apIndex, apTags);

			bncs_stringlist sltAPTags(apTags);
			int languageTag = sltAPTags.getNamedParam("language_tag").toInt();


			bncs_string languageNameStatus;
			routerName(m_devicePackageRouter, DATABASE_LANG_TAG, languageTag, languageNameStatus);

			bncs_stringlist sltNameStatus(languageNameStatus, '|');
			bncs_string languageName = sltNameStatus[0];

			//debug("source_package_editor::loadAudioPackages() index=%1 apName=%2 languageTag=%3", apIndex, apName, languageTag);

			if (languageTag > 0)
			{
				//add APs to a map and later load the listview from memory
				CAudioPackage *ap = new CAudioPackage();
				ap->index = apIndex;
				ap->name = apName;
				ap->language_name = languageName;
				ap->language_tag = languageTag;
				m_mapAPList.insert(make_pair(apIndex, ap));

				//TODO only show APs that are not tagged as Source Audio

				//textPut("add", bncs_string("%1;%2;%3;#TAG=%4").arg(apIndex).arg(apName).arg(languageName).arg(apIndex), POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
				m_loadedAPCount++;
			}
		}
	}
	debug("source_package_editor::loadAudioPackages() index: %1 count: %2", m_loadedAPIndex, m_loadedAPCount);

	if (m_loadedAPIndex >= end)
	{
		m_loadAPComplete = true;
		debug("source_package_editor::loadAudioPackages() complete - count: %2", m_loadedAPCount);
		showAPList();
	}
	else
	{
		m_loadedAPIndex = m_loadedAPIndex + block;
		timerStart(TIMER_INIT_AP_LIST, TIMEOUT_INIT_AP_LIST);
	}
}

void source_package_editor::showAPList()
{
	debug("source_package_editor::showAPList() complete: %1 count: %2", m_loadAPComplete?"true":"false", m_loadedAPCount);

	//check if the panel has been created and the data is available but the list hasn't yet been filled
	if (m_loadAPComplete == true && m_showAPListPanel == true && m_fillAPListComplete == false)
	{
		fillAPListview(0);

		m_fillAPListComplete = true;
		controlHide(POPOVER_SELECT_AUDIO_PACKAGE, "loading");
	}
	else
	{
		controlShow(POPOVER_SELECT_AUDIO_PACKAGE, "loading");
	}
}

void source_package_editor::fillAPListview(int languageFilter)
{
	//load listbox from full map
	textPut("clear", POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);

	int showCount = 0;
	for (MAP_INT_AP_ITERATOR it = m_mapAPList.begin(); it != m_mapAPList.end(); it++)
	{
		CAudioPackage* ap = it->second;
		bool show = true;
		if (languageFilter > 0 && ap->language_tag != languageFilter || ap->language_tag == 0)
		{
			show = false;
		}

		if (show)
		{
			bncs_string lvwFields = bncs_string("%1;%2;%3;#TAG=%4")
				.arg(ap->index, '0', 5)
				.arg(ap->name.replace('|', ' '))
				.arg(ap->language_name)
				.arg(ap->index);
			textPut("add", lvwFields, POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
			showCount++;
		}
	}
	if (showCount != m_loadedAPCount)
	{
		textPut("text", bncs_string(" Showing %1 of %2 Audio Packages").arg(showCount).arg(m_loadedAPCount), POPOVER_SELECT_AUDIO_PACKAGE, "list_container");
	}
	else
	{
		textPut("text", bncs_string(" Audio Package Count: %1").arg(m_loadedAPCount), POPOVER_SELECT_AUDIO_PACKAGE, "list_container");
	}

	bncs_string displayLanguage = "All";

	if (languageFilter == 0)
	{
		textPut("statesheet", "enum_selected", POPOVER_SELECT_AUDIO_PACKAGE, "lang_all");
	}
	else
	{
		textPut("statesheet", "enum_deselected", POPOVER_SELECT_AUDIO_PACKAGE, "lang_all");

		bncs_string languageName;
		routerName(m_devicePackageRouter, DATABASE_LANG_TAG, languageFilter, languageName);

		bncs_string tagValue;
		bncs_string tagStatus;
		languageName.split('|', tagValue, tagStatus);
		displayLanguage = tagValue;
	}

	textPut("text", displayLanguage, POPOVER_SELECT_AUDIO_PACKAGE, "lang_filter");

	m_languageFilter = languageFilter;

	//clear any existing selection
	textPut("clearSelection", POPOVER_SELECT_AUDIO_PACKAGE, LVW_AP);
	m_intSelectedAudioPackage = -1;
	textPut("text", "", POPOVER_SELECT_AUDIO_PACKAGE, "selected_ap");
	textPut("text", "", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
	textPut("statesheet", "enum_deselected", POPOVER_SELECT_AUDIO_PACKAGE, "selection_warning");
	controlDisable(POPOVER_SELECT_AUDIO_PACKAGE, BTN_SET);
	controlEnable(POPOVER_SELECT_AUDIO_PACKAGE, BTN_NONE);
}

void source_package_editor::destroyPackage()
{
	/*
	1.	Park / disconnect all destinations with a connection to the SP and the associated APs.
	2.	Remove all APs from the SP slots. Set the Language Tags in the SP to park.
	3.	Remove all Video devices from the Video levels. Set Video Hub Tags to park.
	4.	Delete all Package info excluding Button Name and Package Number.
	5.	Clear 'MCR Line-up' flags (obviously a future thing as I don't think you know about these yet!).
	*/


	//TODO - popup confirm

	//1. Disconnect all destinations
	infoWrite(m_devicePackageRouter, bncs_string("%1").arg(m_intSourcePackage), PARK_SOURCE_CMD_SLOT);

	//Prepare edit buffer to apply changes to clear all edited levels

	//2.1 Remove all APs from SP slots
	bncs_stringlist sltAPLevels;
	for (int level = 0; level < AP_LEVEL_COUNT; level++)
	{
		m_spEdit.ap[level] = 0;
	}
	//2.2 Set the Language Tags in the SP to park - nothing to do, the Language tag is a property of the Audio Package

	//3.	Remove all Video devices from the Video levels. Set Video Hub Tags to park.
	m_spEdit.video_1.Reset();
	m_spEdit.video_2.Reset();
	m_spEdit.video_3.Reset();
	m_spEdit.video_4.Reset();

	//4.	Delete all Package info excluding Button Name and Package Number.
	m_spEdit.mcr_umd = "";
	m_spEdit.prod_umd_1	= "";
	m_spEdit.prod_umd_2	= "";
	m_spEdit.prod_umd_3	= "";
	m_spEdit.prod_umd_4	= "";
	m_spEdit.package_title = "";
	m_spEdit.record_id = "";
	m_spEdit.spev_id = "";

	//5.	Clear 'MCR Line-up' flags (obviously a future thing as I don't think you know about these yet!).
	//  [future CR]

	applyChanges();

}

