#pragma once

#include <bncs_string.h>

class CAudioPackage
{
public:
	CAudioPackage();
	~CAudioPackage();

	//tags: 'video=201#,rev_vision=#,language_tag=2,ifb1=,ifb2=,4w1=#0,4w2=#0'
	int index = 0;
	int video = 0;
	int rev_vision = 0;
	int language_tag = 0;
	int ifb_1 = 0;
	int ifb_2 = 0;
	int four_wire_1 = 0;
	int four_wire_2 = 0;

	bncs_string name;
	bncs_string language_name;
};

