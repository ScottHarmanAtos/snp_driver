#ifndef source_package_editor_INCLUDED
	#define source_package_editor_INCLUDED

#include <bncs_script_helper.h>
#include "SourcePackage.h"
#include "AudioPackage.h"
#include "bncs_packager.h"

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif

#include <map>
typedef map<int, CAudioPackage*> MAP_INT_AP;
typedef map<int, CAudioPackage*>::iterator MAP_INT_AP_ITERATOR;
typedef map<bncs_string, bncs_string> MAP_STRING_STRING;
typedef map<bncs_string, bncs_string>::iterator MAP_STRING_STRING_ITERATOR;

class source_package_editor : public bncs_script_helper
{
public:
	source_package_editor( bncs_client_callback * parent, const char* path );
	virtual ~source_package_editor();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	void httpCallback(httpNotify *p);

private:

	int m_devicePackageRouter;		//dev_1001 or dev_2001
	int m_deviceVideoHD;			//dev_1201 or dev_2201
	int m_deviceANC;				//dev_1241 or dev_2241

	int m_intSourcePackage;
	int m_audioPackagePage;

	int m_intSelectedANCSource;
	int m_intSelectedVideoSource;
	int m_intSelectedVideoSourceAudio;
	int m_intSelectedAudioPackage;
	int m_intSelectedVideoLevel;
	int m_intSelectedANCLevel;
	int m_intSelectedAPLevel;
	int m_selectedVideoTag;
	int m_selectedLanguageFilter;
	int m_selectedSpevItem;

	int m_intFirstDynamicPackage;
	int m_intLastDynamicPackage;
	int m_loadedAPIndex;
	int m_loadedAPCount;
	int m_languageFilter;

	bool m_blnApplyEnabled;
	bool m_dynamicPackage;
	bool m_blnSourceLineup;
	bool m_blnListView;
	bool m_loadAPComplete;
	bool m_fillAPListComplete;
	bool m_showAPListPanel;

	bncs_packager m_packagerRouter;

	bncs_string m_site;								//nhn or ldc

	bncs_string m_compositePackager;				//c_packager_nhn or c_packager_ldc

	bncs_string m_instancePackagerRouterMain;		//packager_router_nhn_main or packager_router_main

	bncs_string m_instanceVideoHD;					//c_vip_router_video_hd_nhn or c_vip_router_video_hd (ldc)
	bncs_string m_instanceANC;

	bncs_string m_strEditTimeField;
	bncs_string m_strEditLabelField;
	bncs_string m_strEditPortField;
	bncs_string m_strEditPortType;
	bncs_string m_strEditPortMapping;
	bncs_string m_strSelectedPortType;
	bncs_string m_lastSpevRequest;
	bncs_string m_lastStatusRequest;

	bncs_stringlist m_sltStack;
	bncs_stringlist m_spevEditList;
	bncs_stringlist m_apiServerList;

	bncs_string m_apiServer;

	CSourcePackage m_spDiff;			//contains a snapshot of the source package definition prior to editing
	CSourcePackage m_spEdit;			//contains the levels of the source package that is being edited

	MAP_INT_AP m_mapAPList;

	MAP_STRING_STRING m_mapSpevSourcePackages;

	void applyChanges();
	void checkApply();

	void checkSpevClash(bncs_string spevData);

	int getAPLanguageTag(int ap);

	void destroyPackage();

	void editLabel(bncs_string strField);
	void editPackage();
	
	bncs_string formatSpev(bncs_string spevID);
	bncs_string readSpevDatabases();

	void fillAPListview(int languageFilter);
	void initEditor();
	void initVideoHubTagPopup(int level, int videoTag);
	void loadAudioPackages();

	void popoverEditSpev(buttonNotify *b);
	void popupLanguageTag(buttonNotify *b);

	void refreshAPLevels(bncs_string strNewLevels);
	void refreshVideoLevels(bncs_string strNewLevels);
	void refreshName(bncs_string strNewName);
	void refreshTitle(bncs_string strNewTitle);
	void refreshRecordID(bncs_string strNewID);
	void refreshSPEVID(bncs_string strNewID);
	void refreshNotes(bncs_string strNewNotes);
	void refreshUMD(bncs_string strNewUMD);
	void refreshProdUMD(bncs_string strNewUMD);
	void refreshAPTags(int ap, bncs_string strNewTags);
	void refreshAPName(int ap, bncs_string strNewName);
	
	void sendSpevRequest(const bncs_string& page);
	void sendStatusRequest();

	void setEditANC(int level, int anc, int source);
	void setEditVideo(int level, int source, int sourceAudio);
	void setEditVideoTag(int level, int videoTag);
	void setEditAudioPackage(int level, int ap);
	void showAudioPackagePage(int page);
	void showSourceAudio();
	void showSelectedAudioPackage();
	void showSelectedVideoLevel();
	void showSelectedAPLevel();
	void showEditANC();
	void showAPList();
	void showEditSPEV();
	int setSPEVList(bncs_string spevList);

	void updateLabel(bncs_string strLabel);
	
	void updatePTI(bncs_string strList);

	void writeVideoLevels();

	void inspect();

};


#endif // source_package_editor_INCLUDED