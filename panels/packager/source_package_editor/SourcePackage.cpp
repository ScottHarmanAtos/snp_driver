// SourcePackage.cpp: implementation of the CSourcePackage class.
//
//////////////////////////////////////////////////////////////////////

#include "SourcePackage.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSourcePackage::CSourcePackage() : coord_1(1), coord_2(2), video_1(1), video_2(2), video_3(3), video_4(4)
{
}

CSourcePackage::~CSourcePackage()
{

}

void CSourcePackage::reset()
{
	index =0;
	//video = 0;
	cf = 0;
	coord_1.Reset();
	coord_2.Reset();
	video_1.Reset();
	video_2.Reset();
	video_3.Reset();
	video_4.Reset();

	//Reset labels
	start = "";
	end = "";
	ticket = "";
}
