// SourcePackage.h: interface for the CSourcePackage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SOURCEPACKAGE_H__468D1BC2_8D5C_4385_88A0_5E2E233269EF__INCLUDED_)
#define AFX_SOURCEPACKAGE_H__468D1BC2_8D5C_4385_88A0_5E2E233269EF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <bncs_string.h>

struct ANC
{
	int source = 0;
	int tag = 0;

	static bool Compare(const ANC& one, const ANC& two)
	{
		return
			one.source != two.source ||
			one.tag != two.tag;
	}

	void Reset()
	{
		source = 0;
		tag = 0;
	}
};


struct Video
{
	int video = 0;
	int videoTag = 0;
	int ap = 0;
	int apTag = 0;
	ANC anc_1;
	ANC anc_2;
	ANC anc_3;
	ANC anc_4;
	int number = 0; //The video number

	explicit Video(int number = 0)
	{
		this->number = number;
	}

	static bool Compare(const Video& one, const Video& two)
	{
		return
			one.video != two.video ||
			one.videoTag != two.videoTag ||
			one.ap != two.ap ||
			one.apTag != two.apTag ||
			one.anc_1.source != two.anc_1.source ||
			one.anc_2.source != two.anc_2.source ||
			one.anc_3.source != two.anc_3.source ||
			one.anc_4.source != two.anc_4.source;
	}

	void Reset()
	{
		video = 0;
		videoTag = 0;
		ap = 0;
		apTag = 0;
		anc_1.source = 0;
		anc_2.source = 0;
		anc_3.source = 0;
		anc_4.source = 0;
	}
};

struct Coord
{
	int in = 0;
	int out= 0;
	int in_type = 0;
	int out_type = 0;
	int number = 0; //The coord number
	bncs_string suffix;

	explicit Coord(int number = 0)
	{
		this->number = number;
	}

	static bool Compare(const Coord& one, const Coord& two)
	{
		return
			one.in != two.in ||
			one.out != two.out ||
			one.suffix != two.suffix;
	}

	static bool CompareTL(const Coord& one, const Coord& two)
	{
		return
			one.in   != two.in ||
			one.out != two.out;
	}

	void Reset()
	{
		in = 0;
		out = 0;
		in_type = 0;
		out_type = 0;
	}
};

class CSourcePackage  
{
public:
	void reset();
	CSourcePackage();
	virtual ~CSourcePackage();

	int index = 0;
	//int video = 0;
	int cf = 0;

	int ap[32];

	Video video_1;
	Video video_2;
	Video video_3;
	Video video_4;

	Coord coord_1;
	Coord coord_2;

	bncs_string name;
	bncs_string comms_prefix;
	bncs_string mcr_notes;
	bncs_string mcr_umd;
	bncs_string prod_umd_1;
	bncs_string prod_umd_2;
	bncs_string prod_umd_3;
	bncs_string prod_umd_4;
	bncs_string package_title;
	bncs_string notes;
	bncs_string start;
	bncs_string end;
	bncs_string ticket;
	bncs_string record_id;
	bncs_string spev_id;

	bool traceChanged = false;
};

#endif // !defined(AFX_SOURCEPACKAGE_H__468D1BC2_8D5C_4385_88A0_5E2E233269EF__INCLUDED_)
