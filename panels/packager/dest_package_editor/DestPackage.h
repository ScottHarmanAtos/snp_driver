// DestPackage.h: interface for the CDestPackage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DESTPACKAGE_H__A637DD6F_3AC3_4CF4_A352_24C4DFF85112__INCLUDED_)
#define AFX_DESTPACKAGE_H__A637DD6F_3AC3_4CF4_A352_24C4DFF85112__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <bncs_string.h>
#include <bncs_stringlist.h>

const int DEST_PACKAGE_AUDIO_COUNT(32);
const int DEST_PACKAGE_REV_AUDIO_COUNT(2);
const int DEST_PACKAGE_IFB_COUNT(2);
const int DEST_PACKAGE_ANC_COUNT(4);

class CDestPackage  
{
public:
	void reset();
	CDestPackage();
	virtual ~CDestPackage();

	int index;
	int video;
	int videoTag;
	int anc[DEST_PACKAGE_ANC_COUNT];

	int audio[DEST_PACKAGE_AUDIO_COUNT];
	int languageTag[DEST_PACKAGE_AUDIO_COUNT];
	int languageTag_1[DEST_PACKAGE_AUDIO_COUNT];
	int languageTag_2[DEST_PACKAGE_AUDIO_COUNT];
	int typeTag[DEST_PACKAGE_AUDIO_COUNT];
	int typeTag_1[DEST_PACKAGE_AUDIO_COUNT];
	int typeTag_2[DEST_PACKAGE_AUDIO_COUNT];

	int ifbRing[DEST_PACKAGE_IFB_COUNT];
	int ifbPort[DEST_PACKAGE_IFB_COUNT];

	int revAudio[DEST_PACKAGE_REV_AUDIO_COUNT];
	int revVideo;

	bncs_string name;
	bncs_string title;
};

#endif // !defined(AFX_DESTPACKAGE_H__A637DD6F_3AC3_4CF4_A352_24C4DFF85112__INCLUDED_)
