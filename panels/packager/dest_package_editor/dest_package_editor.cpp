#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dest_package_editor.h"
#include "packager_common.h"
#include "instanceLookup.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( dest_package_editor )

#define PNL_MAIN						1
#define POPUP_EDIT_LABEL				2
#define POPOVER_SELECT_VIDEO_DEST		3
#define POPUP_VIDEO_HUB_TAG				4
#define POPUP_AUDIO_TYPE_TAG			5
#define POPUP_LANGUAGE_TAG				6
#define POPUP_ABANDON_CHANGES			7
#define POPOVER_SELECT_AUDIO_DEST		8
#define POPOVER_SELECT_AUDIO_SOURCE		9
#define POPOVER_SELECT_SOURCE_PACKAGE	10
#define POPOVER_SELECT_COMMS_INPUT		11
#define POPOVER_SELECT_ANC_DEST			12
//#define POPUP_AUDIO_MAPPING			9

#define FIELD_PACKAGE_NAME		"package_name"
#define FIELD_PACKAGE_TITLE		"package_title"
#define REPEAT_PACKAGE_NAME		"package_name_repeat"

#define FIELD_VIDEO_DEST_INDEX	"index_v"
#define FIELD_VIDEO_DEST_NAME	"name_v"
#define BUTTON_SET_VIDEO		"setlevel_v"
#define	FIELD_REV_VIDEO			"rev_video_package"

#define BTN_APPLY				"apply"
#define BTN_CANCEL				"cancel"
#define BTN_CLOSE				"close"
#define BTN_NONE				"none"
#define BTN_RESET				"reset"
#define BTN_SELECT				"select"
#define BTN_SET					"set"
#define BTN_SET_1				"set_1"
#define BTN_SET_2				"set_2"
#define BTN_SET_3				"set_3"
#define BTN_SET_4				"set_4"

#define DEST_LOCKED				"dest_locked"

#define AUDIO_PRESET_RECALL		"audio_preset_recall"

#define SOURCE_GRID				"source_grid"
#define DEST_GRID				"dest_grid"

#define TYPE_SDI_AND_COMMS		"sdi_and_comms"	
#define TYPE_SDI_ONLY			"sdi_only"		
#define TYPE_COMMS_ONLY			"comms_only"	
#define TYPE_EXTERNAL			"external"		
#define TYPE_NNR_OUTGOING		"nnr_outgoing"	
#define TYPE_MM_ONLY			"mm_only"		
#define TYPE_PROCESS_LOOP		"PROCESS_LOOP"	

#define NEVION_DEST_USE_HD		"hd"
#define NEVION_DEST_USE_AUDIO	"audio"
#define NEVION_DEST_USE_ANC		"anc"
#define NEVION_DEST_USE_REVVIS	"revvis"
#define NEVION_DEST_USE_AUDRET	"audret"

#define AUDIO_PACKAGE_PAIR_COUNT	12

// constructor - equivalent to ApplCore STARTUP
dest_package_editor::dest_package_editor( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	//init vars
	m_strDestPackageType = "";
	m_strEditLabelField = "";
	m_selectedPort = 0;
	m_selectedVideoDest = 0;
	m_intSelectedANCLevel = 0;
	m_selectedVideoLevel = 0;
	m_substituteLanguage = 0;
	m_substituteType = 0;
	m_audioDestPage = 0;
	m_intSelectedANCDest = 0;
	m_selectedAudioDest = 0;
	m_selectedAudioLevel = 0;
	m_selectedTypeTag = 0;
	m_selectedLanguageTag = 0;
	m_selectedVideoTag = 0;
	m_selectedRevAudioLevel = 0;
	m_selectedAudioSource = 0;
	m_selectedRevVideoLevel = 0;
	m_selectedSourcePackage = 0;
	m_selectedIFBLevel = 0;
	m_selectedCommsRing = 0;
	m_selectedCommsPort = 0;

	m_devicePackageRouter = 0;
	m_deviceRouterAudio = 0;
	m_deviceRouterANC = 0;
	m_deviceDestStatus = 0;
	m_slotDestStatus = 0;
	m_lock = false; 

	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( PNL_MAIN, "main.bncs_ui" );
	
	controlHide(PNL_MAIN, DEST_LOCKED);
	controlDisable(PNL_MAIN, BTN_RESET);

	//if ( !isDeveloper() ) 	controlHide( PNL_MAIN, "raw_parameters" );


	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel

	initEditor();

	//Register for database changes
	infoRegister(m_devicePackageRouter, 1, 1);

}

// destructor - equivalent to ApplCore CLOSEDOWN
dest_package_editor::~dest_package_editor()
{
}

// all button pushes and notifications come here
void dest_package_editor::buttonCallback( buttonNotify *b )
{
	b->dump("dest_package_editor::buttonCallback()");
	if( b->panel() == PNL_MAIN )
	{
		bncs_stringlist controlIndex(b->id(), '_');
		int index = controlIndex[1].toInt();
		if (b->id() == AUDIO_PRESET_RECALL)
		{
			if (b->command() == "apply_preset")
			{
				audioPresetRecall(b->value());
			}
		}
		else if (b->id().startsWith("audio-page_"))
		{
			showAudioDestPage(index);
		}
		else if (b->id().startsWith("video_"))
		{
			if (b->command() == "display_level")
			{
				m_selectedVideoLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_VIDEO_DEST, "popover_select_video_dest.bncs_ui");
				textPut("text", "  Select Video Destination", POPOVER_SELECT_VIDEO_DEST, "title");
				textPut("instance", m_instanceRouterVideoHD_section_01, POPOVER_SELECT_VIDEO_DEST, DEST_GRID);
				textPut("text", "", POPOVER_SELECT_VIDEO_DEST, "selected_dest");
				m_selectedVideoDest = 0;

				//Show selected videolevel
				showSelectedVideoLevel();

				//deselect audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_DEST);
				m_selectedAudioLevel = 0;
				showSelectedAudioLevel();

				//deselect rev audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
				m_selectedRevAudioLevel = 0;
				showSelectedRevAudioLevel();

				//deselect reverse video level and hide popover
				panelRemove(POPOVER_SELECT_SOURCE_PACKAGE);
				m_selectedRevVideoLevel = 0;
				showSelectedRevVideoLevel();

				//deselect IFB level and hide popover
				panelRemove(POPOVER_SELECT_COMMS_INPUT);
				m_selectedIFBLevel = 0;
				showSelectedIFBLevel();
			}
			else if (b->command() == "hub_tag_level")
			{
				panelPopup(POPUP_VIDEO_HUB_TAG, "popup_video_hub_tag.bncs_ui");
				bncs_string level;
				bncs_string tag;
				b->value().split(',', level, tag);

				m_selectedVideoLevel = level.toInt();
				initVideoHubTagPopup(m_selectedVideoLevel, tag.toInt());
			}
			else if (b->command() == "set_anc")
			{
				m_intSelectedANCDest = 0;
				m_intSelectedANCLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_ANC_DEST, "popover_select_anc_dest.bncs_ui");
				textPut("text", "  Select ANC Destinations", POPOVER_SELECT_ANC_DEST, "title");
				textPut("instance", m_instanceRouterANC_section_01, POPOVER_SELECT_ANC_DEST, DEST_GRID);

				showEditANC();

				//deselect VideoLevel and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_DEST);
				m_selectedVideoLevel = 0;
				showSelectedVideoLevel();

				//deselect audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_DEST);
				m_selectedAudioLevel = 0;
				showSelectedAudioLevel();

				//deselect rev audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
				m_selectedRevAudioLevel = 0;
				showSelectedRevAudioLevel();

				//deselect reverse video level and hide popover
				panelRemove(POPOVER_SELECT_SOURCE_PACKAGE);
				m_selectedRevVideoLevel = 0;
				showSelectedRevVideoLevel();

				//deselect IFB level and hide popover
				panelRemove(POPOVER_SELECT_COMMS_INPUT);
				m_selectedIFBLevel = 0;
				showSelectedIFBLevel();
			}
		}
		else if (b->id().startsWith("audio_"))
		{
			if (b->command() == "display_level")
			{
				m_selectedAudioLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_AUDIO_DEST, "popover_select_audio_dest.bncs_ui");
				textPut("text", bncs_string("  Select Audio Destination - Level %1").arg(b->value()), POPOVER_SELECT_AUDIO_DEST, "title");
				textPut("instance", m_instanceRouterAudio_section_01, POPOVER_SELECT_AUDIO_DEST, DEST_GRID);
				textPut("text", "", POPOVER_SELECT_AUDIO_DEST, "selected_dest");
				m_selectedAudioDest = 0;

				//Show selected audiolevel
				showSelectedAudioLevel();

				//deselect video level and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_DEST);
				m_selectedVideoLevel = 0;
				showSelectedVideoLevel();

				//deselect rev audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
				m_selectedRevAudioLevel = 0;
				showSelectedRevAudioLevel();

				//deselect reverse video level and hide popover
				panelRemove(POPOVER_SELECT_SOURCE_PACKAGE);
				m_selectedRevVideoLevel = 0;
				showSelectedRevVideoLevel();

				//deselect IFB level and hide popover
				panelRemove(POPOVER_SELECT_COMMS_INPUT);
				m_selectedIFBLevel = 0;
				showSelectedIFBLevel();
			}
			/*
			else if (b->command() == "hub_tag_level")
			{
				panelPopup(POPUP_VIDEO_HUB_TAG, "popup_video_hub_tag.bncs_ui");
				bncs_string level;
				bncs_string tag;
				b->value().split(',', level, tag);

				m_intSelectedVideoLevel = level.toInt();
				initVideoHubTagPopup(m_intSelectedVideoLevel, tag.toInt());
			}
			*/
			else if (b->command().startsWith("language_tag"))
			{
				panelPopup(POPUP_LANGUAGE_TAG, "popup_language_tag.bncs_ui");
				textPut("instance", m_instancePackagerRouterMain, POPUP_LANGUAGE_TAG, "language_grid");


				bncs_string level;
				bncs_string tag;
				b->value().split(',', level, tag);
				m_selectedAudioLevel = level.toInt();

				if (b->command() == "language_tag_1"){
					m_substituteLanguage = 1;
					textPut("text", " Set Language Tag Substitute 1", POPUP_LANGUAGE_TAG, "title");
					textPut("selected", m_dpEdit.languageTag_1[m_selectedAudioLevel - 1], POPUP_LANGUAGE_TAG, "language_grid");
				}
				else if (b->command() == "language_tag_2"){
					m_substituteLanguage = 2;
					textPut("text", " Set Language Tag Substitute 2", POPUP_LANGUAGE_TAG, "title");
					textPut("selected", m_dpEdit.languageTag_2[m_selectedAudioLevel - 1], POPUP_LANGUAGE_TAG, "language_grid");
				}
				else
				{
					m_substituteLanguage = 0;
					textPut("text", " Set Language Tag", POPUP_LANGUAGE_TAG, "title");
					textPut("selected", m_dpEdit.languageTag[m_selectedAudioLevel - 1], POPUP_LANGUAGE_TAG, "language_grid");
				}
			}
			else if (b->command().startsWith("type_tag"))
			{
				panelPopup(POPUP_AUDIO_TYPE_TAG, "popup_audio_type_tag.bncs_ui");

				bncs_string level;
				bncs_string tag;
				b->value().split(',', level, tag);
				m_selectedAudioLevel = level.toInt();

				m_selectedAudioLevel = level.toInt();
				initAudioTypeTagPopup(level, tag.toInt());

				if (b->command() == "type_tag_1"){
					m_substituteType = 1;
				}
				else if (b->command() == "type_tag_2"){
					m_substituteType = 2;
				}
				else
				{
					m_substituteType = 0;
					//textPut("text", " Set Language Tag", POPUP_AUDIO_TYPE_TAG, "title");
					//textPut("selected", m_dpEdit.typeTag[m_selectedAudioLevel], POPUP_AUDIO_TYPE_TAG, );
				}
			}
		}
		else if (b->id().startsWith("revaudio_"))
		{
			if (b->command() == "display_level")
			{
				m_selectedRevAudioLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_AUDIO_SOURCE, "popover_select_audio_source.bncs_ui");
				textPut("text", bncs_string("  Select Audio Source - Reverse Audio Level %1").arg(b->value()), POPOVER_SELECT_AUDIO_SOURCE, "title");
				textPut("instance", m_instanceRouterAudio_section_01, POPOVER_SELECT_AUDIO_SOURCE, SOURCE_GRID);
				textPut("text", "", POPOVER_SELECT_AUDIO_SOURCE, "selected_source");
				m_selectedAudioDest = 0;

				//Show selected audiolevel
				showSelectedRevAudioLevel();

				//deselect video level and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_DEST);
				m_selectedVideoLevel = 0;
				showSelectedVideoLevel();

				//deselect audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_DEST);
				m_selectedAudioLevel = 0;
				showSelectedAudioLevel();

				//deselect reverse video level and hide popover
				panelRemove(POPOVER_SELECT_SOURCE_PACKAGE);
				m_selectedRevVideoLevel = 0;
				showSelectedRevVideoLevel();

				//deselect IFB level and hide popover
				panelRemove(POPOVER_SELECT_COMMS_INPUT);
				m_selectedIFBLevel = 0;
				showSelectedIFBLevel();
			}
		}
		else if (b->id() == FIELD_REV_VIDEO)
		{
			if (b->command() == "display_level")
			{
				m_selectedRevVideoLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_SOURCE_PACKAGE, "popover_select_source_package.bncs_ui");
				textPut("text", "  Select Sorce Package - Reverse Video Level", POPOVER_SELECT_SOURCE_PACKAGE, "title");
				textPut("instance", m_instancePackagerRouterMain, POPOVER_SELECT_SOURCE_PACKAGE, SOURCE_GRID);

				m_selectedRevVideoLevel = 1;
				showSelectedRevVideoLevel();

				//deselect video level and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_DEST);
				m_selectedVideoLevel = 0;
				showSelectedVideoLevel();

				//deselect audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_DEST);
				m_selectedAudioLevel = 0;
				showSelectedAudioLevel();

				//deselect rev audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
				m_selectedRevAudioLevel = 0;
				showSelectedRevAudioLevel();

				//deselect IFB level and hide popover
				panelRemove(POPOVER_SELECT_COMMS_INPUT);
				m_selectedIFBLevel = 0;
				showSelectedIFBLevel();
			}
		}
		else if (b->id().startsWith("ifb_"))
		{
			if (b->command() == "display_level")
			{
				m_selectedIFBLevel = b->value().toInt();
				panelShow(POPOVER_SELECT_COMMS_INPUT, "popover_select_comms_input.bncs_ui");
				textPut("text", bncs_string("  Select Riedel Input - IFB Level %1").arg(b->value()), POPOVER_SELECT_COMMS_INPUT, "title");

				m_selectedCommsRing = 0;
				m_selectedCommsPort = 0;
				textPut("text", "", POPOVER_SELECT_COMMS_INPUT, "selected_port");

				showSelectedIFBLevel();

				//deselect video level and hide popover
				panelRemove(POPOVER_SELECT_VIDEO_DEST);
				m_selectedVideoLevel = 0;
				showSelectedVideoLevel();

				//deselect audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_DEST);
				m_selectedAudioLevel = 0;
				showSelectedAudioLevel();

				//deselect rev audio level and hide popover
				panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
				m_selectedRevAudioLevel = 0;
				showSelectedRevAudioLevel();

				//deselect reverse video level and hide popover
				panelRemove(POPOVER_SELECT_SOURCE_PACKAGE);
				m_selectedRevVideoLevel = 0;
				showSelectedRevVideoLevel();
			}
		}
		else if (b->id() == "close")
		{
			if(m_applyEnabled)
			{
				panelPopup(POPUP_ABANDON_CHANGES, "popup_abandon_changes.bncs_ui");
			}
			else
			{
				navigateAdjust("/close");
			}
		}
		else if (b->id() == BTN_RESET)
		{
			//TODO reset package
		}
		else if (b->id() == BTN_APPLY)
		{
			applyChanges();
		}

		else if (b->id() == FIELD_VIDEO_DEST_NAME)	editField(b->id(), DATABASE_DEST_NAME, "Select Video Destination");
		else if( b->id() == FIELD_PACKAGE_NAME)		editLabel(b->id());
		else if(b->id() == FIELD_PACKAGE_TITLE)		editLabel(b->id());
	}
	else if( b->panel() == POPUP_EDIT_LABEL )
	{
		if( b->id() == "cancel")
		{
			panelDestroy(POPUP_EDIT_LABEL);
		}
		else if( b->id() == "keyboard")
		{
			bncs_string strKeyboard;
			textGet("text", POPUP_EDIT_LABEL, "keyboard", strKeyboard);
			panelDestroy(POPUP_EDIT_LABEL);
			debug("dest_package_editor::buttonCallback() field=%1 edit value=%2", m_strEditLabelField, strKeyboard);
			updateLabel(strKeyboard);
		}
	}
	else if (b->panel() == POPUP_AUDIO_TYPE_TAG)
	{
		b->dump("audio_package_edit_view::buttonCallback - POPUP_AUDIO_TYPE_TAG");
		if (b->id() == "lvw_tags")
		{
			if (b->command() == "cell_selection")
			{
				bncs_string selectedName = b->value();
				textPut("text", selectedName, POPUP_AUDIO_TYPE_TAG, "selected_name");
				//[lvw_tags: cell_selection.0.0=LDC Main
			}
			else if (b->command() == "tag")
			{
				m_selectedTypeTag = b->value().toInt();
				textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedTypeTag), POPUP_AUDIO_TYPE_TAG, "selected_tag");
			}
		}
		else if (b->id() == BTN_SET)
		{
			panelDestroy(POPUP_AUDIO_TYPE_TAG);
			setEditTypeTag();
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_AUDIO_TYPE_TAG);
		}
	}
	else if (b->panel() == POPUP_LANGUAGE_TAG)
	{
		if (b->id() == BTN_SET)
		{
			panelDestroy(POPUP_LANGUAGE_TAG);
			setEditLanguageTag();
		}
		else if (b->id() == "language_grid")
		{
			if (b->command() == "select")
			{
				m_selectedLanguageTag = b->value().toInt();
				textPut("selected", m_selectedLanguageTag, POPUP_LANGUAGE_TAG, "language_grid");
			}
		}
		else if (b->id() == BTN_NONE)
		{
			m_selectedLanguageTag = 0;
			textPut("selected", m_selectedLanguageTag, POPUP_LANGUAGE_TAG, "language_grid");
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelDestroy(POPUP_LANGUAGE_TAG);
		}
	}
	else if (b->panel() == POPUP_VIDEO_HUB_TAG)
	{
		b->dump("source_package_editor::buttonCallback - POPUP_VIDEO_HUB_TAG");
		if (b->id() == "lvw_tags")
		{
			if (b->command() == "cell_selection")
			{
				bncs_string selectedName = b->value();
				textPut("text", selectedName, POPUP_VIDEO_HUB_TAG, "selected_name");
				//[lvw_tags: cell_selection.0.0=LDC Main
			}
			else if (b->command() == "tag")
			{
				m_selectedVideoTag = b->value().toInt();
				textPut("text", bncs_string("Selected Tag:|%1").arg(m_selectedVideoTag), POPUP_VIDEO_HUB_TAG, "selected_tag");
			}
		}
		else if (b->id() == "select")
		{
			//m_spEdit.video_1.videoTag = m_selectedVideoTag;
			panelDestroy(POPUP_VIDEO_HUB_TAG);
			setEditVideoTag(m_selectedVideoLevel, m_selectedVideoTag);
		}
		else if (b->id() == "cancel")
		{
			panelDestroy(POPUP_VIDEO_HUB_TAG);
		}
	}
	/*
	else if (b->panel() == POPUP_SELECT_VIDEO_DEST)
	{
		if( b->id() == "select")
		{
			panelDestroy(POPUP_SELECT_VIDEO_DEST);
			setVideoDest();
		}
		else if( b->id() == "cancel")
		{
			panelDestroy(POPUP_SELECT_VIDEO_DEST);
		}
		else if( b->id() == "none")
		{
			m_selectedVideoDest = 0;
			panelDestroy(POPUP_SELECT_VIDEO_DEST);
			setVideoDest();
		}
		else if( b->id() == "grid")
		{
			if(b->value().toInt() > 0)
			{
				m_selectedVideoDest = b->value().toInt();
			}
		}
	}
	*/
	else if( b->panel() == POPUP_ABANDON_CHANGES )
	{
		if( b->id() == "confirm")
		{
			panelDestroy(POPUP_ABANDON_CHANGES);
			navigateAdjust("/close");
		}
		else if( b->id() == "cancel")
		{
			panelDestroy(POPUP_ABANDON_CHANGES);
		}
	}
	else if (b->panel() == POPOVER_SELECT_VIDEO_DEST)
	{
		if (b->id() == DEST_GRID)
		{
			m_selectedVideoDest = b->value().toInt();
			showSelectedVideoDest();
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("index", 0, POPOVER_SELECT_VIDEO_DEST, DEST_GRID);
			textPut("text", "-", POPOVER_SELECT_VIDEO_DEST, "selected_dest");
			textPut("text", "", POPOVER_SELECT_VIDEO_DEST, "dest_use_warning");
			textPut("statesheet", "enum_deselected", POPOVER_SELECT_VIDEO_DEST, "dest_use_warning");
			m_selectedVideoDest = 0;

			//Must be able to set the video level to none
			controlEnable(POPOVER_SELECT_VIDEO_DEST, BTN_SET);
		}
		else if (b->id() == BTN_SET)
		{
			panelRemove(POPOVER_SELECT_VIDEO_DEST);
			setEditVideo(m_selectedVideoLevel, m_selectedVideoDest);
			m_selectedVideoLevel = 0;
			showSelectedVideoLevel();
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelRemove(POPOVER_SELECT_VIDEO_DEST);
			m_selectedVideoLevel = 0;
			showSelectedVideoLevel();
		}
	}
	else if (b->panel() == POPOVER_SELECT_ANC_DEST)
	{
		if (b->id() == DEST_GRID)
		{
			m_intSelectedANCDest = b->value().toInt();
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("index", 0, POPOVER_SELECT_ANC_DEST, DEST_GRID);
			m_intSelectedANCDest = 0;

			//Must be able to set a video level to none
			controlEnable(POPOVER_SELECT_ANC_DEST, BTN_SET);
		}
		else if (b->id() == BTN_SET_1)
		{
			panelRemove(POPOVER_SELECT_ANC_DEST);
			setEditANC(m_intSelectedANCLevel, 1, m_intSelectedANCDest);
			m_intSelectedANCLevel = 0;
		}
		else if (b->id() == BTN_SET_2)
		{
			panelRemove(POPOVER_SELECT_ANC_DEST);
			setEditANC(m_intSelectedANCLevel, 2, m_intSelectedANCDest);
			m_intSelectedANCLevel = 0;
		}
		else if (b->id() == BTN_SET_3)
		{
			panelRemove(POPOVER_SELECT_ANC_DEST);
			setEditANC(m_intSelectedANCLevel, 3, m_intSelectedANCDest);
			m_intSelectedANCLevel = 0;
		}
		else if (b->id() == BTN_SET_4)
		{
			panelRemove(POPOVER_SELECT_ANC_DEST);
			setEditANC(m_intSelectedANCLevel, 4, m_intSelectedANCDest);
			m_intSelectedANCLevel = 0;
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelRemove(POPOVER_SELECT_ANC_DEST);
			m_intSelectedANCLevel = 0;
		}
	}
	else if (b->panel() == POPOVER_SELECT_AUDIO_DEST)
	{
		if (b->id() == DEST_GRID)
		{
			m_selectedAudioDest = b->value().toInt();
			showSelectedAudioDest();

			//textPut("text", m_selectedAudioDest, POPOVER_SELECT_AUDIO_DEST, "selected_dest");
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("index", 0, POPOVER_SELECT_AUDIO_DEST, DEST_GRID);
			textPut("text", "-", POPOVER_SELECT_AUDIO_DEST, "selected_dest");
			textPut("text", "", POPOVER_SELECT_AUDIO_DEST, "dest_use_warning");
			textPut("statesheet", "enum_deselected", POPOVER_SELECT_AUDIO_DEST, "dest_use_warning");
			m_selectedAudioDest = 0;

			//Must be able to set an audio level to none
			controlEnable(POPOVER_SELECT_AUDIO_DEST, BTN_SET);
		}
		else if (b->id() == BTN_SET)
		{
			panelRemove(POPOVER_SELECT_AUDIO_DEST);
			setEditAudio(m_selectedAudioLevel, m_selectedAudioDest);
			m_selectedAudioLevel = 0;
			showSelectedAudioLevel();
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelRemove(POPOVER_SELECT_AUDIO_DEST);
			m_selectedAudioLevel = 0;
			showSelectedAudioLevel();
		}
	}

	else if (b->panel() == POPOVER_SELECT_AUDIO_SOURCE)
	{
		if (b->id() == SOURCE_GRID)
		{
			m_selectedAudioSource = b->value().toInt();
			showSelectedAudioSource();
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("index", 0, POPOVER_SELECT_AUDIO_SOURCE, SOURCE_GRID);
			m_selectedAudioSource = 0;
			showSelectedAudioSource();

			//Must be able to set an audio level to none
			controlEnable(POPOVER_SELECT_AUDIO_SOURCE, BTN_SET);
		}
		else if (b->id() == BTN_SET)
		{
			panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
			setEditRevAudio(m_selectedRevAudioLevel, m_selectedAudioSource);
			m_selectedRevAudioLevel = 0;
			showSelectedRevAudioLevel();
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelRemove(POPOVER_SELECT_AUDIO_SOURCE);
			m_selectedRevAudioLevel = 0;
			showSelectedRevAudioLevel();
		}
	}

	else if (b->panel() == POPOVER_SELECT_SOURCE_PACKAGE)
	{
		if (b->id() == SOURCE_GRID)
		{
			m_selectedSourcePackage = b->value().toInt();
			showSelectedSourcePackage();
		}
		else if (b->id() == BTN_NONE)
		{
			textPut("index", 0, POPOVER_SELECT_SOURCE_PACKAGE, SOURCE_GRID);
			m_selectedSourcePackage = 0;
			showSelectedSourcePackage();

			//Must be able to set an reverse video level to none
			controlEnable(POPOVER_SELECT_SOURCE_PACKAGE, BTN_SET);
		}
		else if (b->id() == BTN_SET)
		{
			panelRemove(POPOVER_SELECT_SOURCE_PACKAGE);
			setEditRevVideo(m_selectedRevVideoLevel, m_selectedSourcePackage);
			m_selectedRevVideoLevel = 0;
			showSelectedRevVideoLevel();
		}
		else if (b->id() == BTN_CANCEL)
		{
			panelRemove(POPOVER_SELECT_SOURCE_PACKAGE);
			//m_selectedRevAudioLevel = 0;
			//showSelectedRevAudioLevel();
		}
	}

	else if (b->panel() == POPOVER_SELECT_COMMS_INPUT)
	{
		popoverSelectCommsInput(b);
	}	
}

// all revertives come here
int dest_package_editor::revertiveCallback( revertiveNotify * r )
{
	r->dump("dest_package_editor::revertiveCallback()");
	if (r->device() == m_deviceDestStatus && r->index() == m_slotDestStatus)
	{
		updateStatus(r->sInfo());
	}
	else
	{
		for (int device = 0; device < NEVION_DEST_USE_DEVICES; device++)
		{
			//if (r->device() == m_deviceNevionDestUse[device])
			//{
			//int index = (device * 4096) + r->index();
			//m_mapNevionDestUse[index] = r->sInfo();
			//}

			if (r->device() == m_selectedNevionDestUseDevice && r->index() == m_selectedNevionDestUseSlot)
			{
				//debug("dest_package_editor::revertiveCallback() nevion usage=%1", r->sInfo());
				showNevionDestUse(r->sInfo());
			}
		}
	}

	return 0;
}

// all database name changes come back here
void dest_package_editor::databaseCallback( revertiveNotify * r )
{
	debug("dest_package_editor::databaseCallback() dev=%1 database=%2 index=%3 value=%4", r->device(), r->database(), r->index(), r->sInfo());
	
	if(r->device() == m_devicePackageRouter && r->index() == m_destPackage)
	{
		if(r->database() == DATABASE_DEST_NAME)
		{
			refreshName(r->sInfo());
		}
		else if(r->database() == DATABASE_DEST_TITLE)
		{
			refreshTitle(r->sInfo());
		}
		else if(r->database() == DATABASE_DEST_LEVELS)
		{
			refreshLevels(r->sInfo());
		}
		else if (r->database() == DATABASE_DEST_AUDIO)
		{
			refreshAudioLevels(r->sInfo());
		}
		else if (r->database() == DATABASE_DEST_LANG_TYPE)
		{
			refreshAudioTags(r->sInfo());
		}
		else if (r->database() == DATABASE_DEST_AUDIO_1)
		{
			refreshAudioTags_1(r->sInfo());
		}
		else if (r->database() == DATABASE_DEST_AUDIO_2)
		{
			refreshAudioTags_2(r->sInfo());
		}
		else if (r->database() == DATABASE_DEST_REV_AUDIO)
		{
			refreshReverseAudioLevels(r->sInfo());
		}
	}
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dest_package_editor::parentCallback( parentNotify *p )
{
	p->dump("dest_package_editor::parentCallback()");
	if (p->command() == "return")
	{
		bncs_stringlist sl;
		
		sl << "myParam=" + m_myParam;
		
		return sl.toString( '\n' );
	}
	else if( p->command() == "myParam" )
	{
		m_myParam = p->value();
	}
	else if(p->command() == "raw_parameters")
	{
		textPut("text", bncs_string("cmd=%1 value=%2").arg(p->command()).arg(p->value()), PNL_MAIN, "raw_parameters");
		int index = p->value().toInt();
		
		if(index > 0)
		{
			m_destPackage = index;
			editPackage();
		}
	}
	return "";
}

// timer events come here
void dest_package_editor::timerCallback( int id )
{
}

void dest_package_editor::initEditor()
{

	/*
	bncs_config cfgPackagerVideoRouterHD(bncs_string("packager_config.packager_auto.video_router_hd"));
	if (cfgPackagerVideoRouterHD.isValid())
	{
		bncs_stringlist sltPackagerRouterVideoHD(cfgPackagerVideoRouterHD.attr("value"));
		m_instanceVideoHD = sltPackagerRouterVideoHD.getNamedParam("instance");
		getDev(m_instanceVideoHD, &m_deviceVideoHD);
	}
	else
	{
		m_deviceVideoHD = -1;
	}

	bncs_config cfgPackagerAudioRouter(bncs_string("packager_config.packager_auto.audio_router"));
	if (cfgPackagerAudioRouter.isValid())
	{
		bncs_stringlist sltPackagerRouterAudio(cfgPackagerAudioRouter.attr("value"));
		m_instanceAudioRouter = sltPackagerRouterAudio.getNamedParam("instance");
		getDev(m_instanceAudioRouter, &m_deviceAudioRouter);
	}
	else
	{
		m_deviceAudioRouter = -1;
	}
	*/

	//register for nevion dest usage
	/*
	<instance composite="yes" id="c_packager_nevion_dest" >
	<group id="packager_nevion_dest_1" instance="packager_nevion_dest_use_main" />
	<group id="packager_nevion_dest_2" instance="packager_nevion_dest_use_2" />
	<group id="packager_nevion_dest_3" instance="packager_nevion_dest_use_3" />
	<group id="packager_nevion_dest_4" instance="packager_nevion_dest_use_4" />
	<group id="packager_nevion_dest_5" instance="packager_nevion_dest_use_5" />
	<group id="packager_nevion_dest_6" instance="packager_nevion_dest_use_6" />
	<group id="packager_nevion_dest_7" instance="packager_nevion_dest_use_7" />
	<group id="packager_nevion_dest_8" instance="packager_nevion_dest_use_8" />
	<group id="packager_nevion_dest_9" instance="packager_nevion_dest_use_9" />
	<group id="packager_nevion_dest_10" instance="packager_nevion_dest_use_10" />
	<group id="packager_nevion_dest_11" instance="packager_nevion_dest_use_11" />
	<group id="packager_nevion_dest_12" instance="packager_nevion_dest_use_12" />
	<group id="packager_nevion_dest_13" instance="packager_nevion_dest_use_13" />
	<group id="packager_nevion_dest_14" instance="packager_nevion_dest_use_14" />
	<group id="packager_nevion_dest_15" instance="packager_nevion_dest_use_15" />
	<group id="packager_nevion_dest_16" instance="packager_nevion_dest_use_16" />
	</instance>
	*/

	//packager_config.xml
	//===================
	//<packager_config>
	//  <data id="packager_auto">
	//    <setting id="packager_nevion_dest_use" value="packager_nevion_dest_use_main" />
	/*
	bncs_config cfgPackagerNevionDestUse(bncs_string("packager_config.packager_auto.packager_nevion_dest_use"));
	if (cfgPackagerNevionDestUse.isValid())
	{
		bncs_string instancePackagerNevionDestUseMain = cfgPackagerNevionDestUse.attr("value");
	}
	else
	{
		m_deviceVideoHD = -1;
	}
	*/

	//Get top level packager instance of the tech hub for this workstation
	m_compositePackager = getWorkstationSetting("packager_instance");
	m_packagerRouter = bncs_packager(m_compositePackager);
	m_packagerRouter = bncs_packager(m_compositePackager);
	m_devicePackageRouter = m_packagerRouter.destDev(1);
	m_instancePackagerRouterMain = m_packagerRouter.instance(1);

	/* packager_config.xml
	<packager_config>
	<data id="packager_auto">
	<setting id="video_router_hd" value="instance=vip_router_video_hd_01" />
	*/

	//Get video router device
	m_instanceRouterVideoHD = m_packagerRouter.getConfigItem("video_router_hd");

	//Check if we have been passed a composite instance.
	if (instanceLookupComposite(m_instanceRouterVideoHD, "section_01", m_instanceRouterVideoHD_section_01))
	{
		getDev(m_instanceRouterVideoHD_section_01, &m_deviceRouterVideoHD);
		debug("dest_package_editor::initEditor() m_instanceRouterVideoHD_section_01=%1 device=%2", m_instanceRouterVideoHD_section_01, m_deviceRouterVideoHD);
	}
	else
	{
		debug("dest_package_editor::initEditor() m_instanceRouterVideoHD_section_01 NOT FOUND");
	}

	//Get ANC router device
	m_instanceRouterANC = m_packagerRouter.getConfigItem("anc_router");

	m_instanceRouterANC_section_01 = "";
	if (instanceLookupComposite(m_instanceRouterANC, "section_01", m_instanceRouterANC_section_01))
	{
		getDev(m_instanceRouterANC_section_01, &m_deviceRouterANC);
	}
	else
	{
		debug("dest_package_editor::initEditor() instanceRouterANC.section_01 NOT FOUND");
	}

	//Get audio router device
	m_instanceRouterAudio = m_packagerRouter.getConfigItem("audio_router");

	debug("dest_package_editor::initEditor() m_instanceRouterAudio=%1", m_instanceRouterAudio);

	//Check if we have been passed a composite instance.
	if (instanceLookupComposite(m_instanceRouterAudio, "section_01", m_instanceRouterAudio_section_01))
	{
		getDev(m_instanceRouterAudio_section_01, &m_deviceRouterAudio);
		debug("dest_package_editor::initEditor() m_instanceRouterAudio_section_01=%1 device=%2", m_instanceRouterAudio_section_01, m_deviceRouterAudio);
	}
	else
	{
		debug("dest_package_editor::initEditor() m_instanceRouterAudio_section_01 NOT FOUND");
	}

	m_packagerNevionDestUse = bncs_packager(m_compositePackager, "nevion_dest_use");
	for (int device = 0; device < NEVION_DEST_USE_DEVICES; device++)
	{
		int index = (device * 4096) + 1;
		m_deviceNevionDestUse[device] = m_packagerNevionDestUse.destDev(index);
		infoRegister(m_deviceNevionDestUse[device], 1, 4096);
		//infoPoll(m_deviceNevionDestUse[device], 1, 4096);
	}

	m_packagerDestStatus = bncs_packager(m_compositePackager, "dest_status");

}

void dest_package_editor::editPackage()
{
	debug("dest_package_editor::editPackage()");
	
	m_dpDiff.reset();
	m_dpEdit.reset();

	m_selectedPort = 0;
	m_selectedVideoDest = 0;
	m_selectedAudioDest = 0;
	m_selectedAudioLevel = 0;
	m_selectedTypeTag = 0;
	m_selectedLanguageTag = 0;
	m_selectedRevAudioLevel = 0;
	m_selectedAudioSource = 0;

	m_applyEnabled = false;

	textPut("style=button", PNL_MAIN, FIELD_PACKAGE_NAME);
	textPut("notify.released=true", PNL_MAIN, FIELD_PACKAGE_NAME);
	textPut("style=button", PNL_MAIN, FIELD_PACKAGE_TITLE);
	textPut("notify.released=true", PNL_MAIN, FIELD_PACKAGE_TITLE);

	controlDisable(PNL_MAIN, BTN_APPLY);

	//Show index and current name of package
	textPut("text", m_destPackage, PNL_MAIN, "package_index");
	bncs_string name;
	routerName(m_devicePackageRouter, DATABASE_DEST_NAME, m_destPackage, name);
	m_dpDiff.name = name;
	m_dpEdit.name = name;

	textPut("text", name, PNL_MAIN, FIELD_PACKAGE_NAME);
	textPut("text", name.replace('|',' '), PNL_MAIN, REPEAT_PACKAGE_NAME);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_PACKAGE_NAME);

	textPut("dest_name", name, PNL_MAIN, AUDIO_PRESET_RECALL);
	textPut("dest_index", m_destPackage, PNL_MAIN, AUDIO_PRESET_RECALL);

	bncs_string strPackageTitle;
	routerName(m_devicePackageRouter, DATABASE_DEST_TITLE, m_destPackage, strPackageTitle);
	textPut("text", strPackageTitle, PNL_MAIN, FIELD_PACKAGE_TITLE);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_PACKAGE_TITLE);

	m_dpDiff.title = strPackageTitle;
	m_dpEdit.title = strPackageTitle;

	//Get dest levels
	
	//Video/ANC/Rev levels
	bncs_string destLevels;
	//17501=video=501#1,anc1=#1,anc2=#1,anc3=#1,anc4=#1,rev_vision=#1,ifb1=,ifb2=
	
	//19897=video=2897#1,anc1=#1,anc2=#1,anc3=#1,anc4=#1,rev_vision=#1,ifb1=,ifb2=
	routerName(m_devicePackageRouter, DATABASE_DEST_LEVELS, m_destPackage, destLevels);
	refreshLevels(destLevels);

	/*
	bncs_string videoDestName;
	routerName(m_deviceVideoHD, DATABASE_DEST_NAME, videoDest, videoDestName);
	debug("dest name=%1", videoDestName);
	textPut("text", videoDest, PNL_MAIN, FIELD_VIDEO_DEST_INDEX);
	textPut("text", videoDestName, PNL_MAIN, FIELD_VIDEO_DEST_NAME);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_VIDEO_DEST_INDEX);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_VIDEO_DEST_NAME);
	*/

	//Get audio levels
	bncs_string destAudioLevels;
	routerName(m_devicePackageRouter, DATABASE_DEST_AUDIO, m_destPackage, destAudioLevels);
	refreshAudioLevels(destAudioLevels);
	//debug("dest_package_editor::editPackage() audio levels=%1", destAudioLevels);

	//Get audio tags
	bncs_string destAudioTags;
	routerName(m_devicePackageRouter, DATABASE_DEST_LANG_TYPE, m_destPackage, destAudioTags);
	refreshAudioTags(destAudioTags);
	routerName(m_devicePackageRouter, DATABASE_DEST_AUDIO_1, m_destPackage, destAudioTags);
	refreshAudioTags_1(destAudioTags);
	routerName(m_devicePackageRouter, DATABASE_DEST_AUDIO_2, m_destPackage, destAudioTags);
	refreshAudioTags_2(destAudioTags);

	//Get reverse audio levels
	bncs_string destReverseAudioLevels;
	routerName(m_devicePackageRouter, DATABASE_DEST_REV_AUDIO, m_destPackage, destReverseAudioLevels);
	refreshReverseAudioLevels(destReverseAudioLevels);

	showAudioDestPage(1);

	if (m_deviceDestStatus > 0)
	{
		infoUnregister(m_deviceDestStatus);
	}

	//register for lock status
	m_deviceDestStatus = m_packagerDestStatus.destDev(m_destPackage);
	m_slotDestStatus = m_packagerDestStatus.destSlot(m_destPackage);

	infoRegister(m_deviceDestStatus, m_slotDestStatus, m_slotDestStatus);
	infoPoll(m_deviceDestStatus, m_slotDestStatus, m_slotDestStatus);

	controlHide(PNL_MAIN, DEST_LOCKED);

}

void dest_package_editor::updateStatus(bncs_string status)
{
	//video=1,audio=1,anc=1,trace=2001,lock=1

	bncs_stringlist sltStatus(status);
	int lock = sltStatus.getNamedParam("lock").toInt();

	if (lock == 1)
	{
		m_lock = true;
		controlShow(PNL_MAIN, DEST_LOCKED);
	}
	else
	{
		m_lock = false;
		controlHide(PNL_MAIN, DEST_LOCKED);
	}
}

void dest_package_editor::editField(bncs_string strField, int intNameDatabase, bncs_string strCaption)
{
}

void dest_package_editor::editLabel(bncs_string strField)
{
	bncs_string strCaption = "Edit Label";
	bncs_string strCurrentLabel = "";
	
	int intLabelLength = 0;
	
	m_strEditLabelField = strField;
	panelPopup(POPUP_EDIT_LABEL, "popup_edit_label.bncs_ui");
	textPut("setFocus", "setFocus", POPUP_EDIT_LABEL, "keyboard");
	
	if(m_strEditLabelField == FIELD_PACKAGE_NAME)
	{
		strCurrentLabel = m_dpEdit.name;
		strCaption = "Edit Button Name";
	}
	else if(m_strEditLabelField == FIELD_PACKAGE_TITLE)
	{
		strCurrentLabel = m_dpEdit.title;
		strCaption = "Edit Package Title";
	}

	textPut("text", strCurrentLabel, POPUP_EDIT_LABEL, "keyboard");
	textPut("text", strCaption, POPUP_EDIT_LABEL, "grpEditLabel");
	
	debug("dest_package_editor::editLabel() Field=%1 Caption='%2' Label='%3'", strField, strCaption, strCurrentLabel);
}

void dest_package_editor::updateLabel(bncs_string strLabel)
{
	if(m_strEditLabelField == FIELD_PACKAGE_NAME)
	{
		m_dpEdit.name = strLabel;
		textPut("dest_name", strLabel, PNL_MAIN, AUDIO_PRESET_RECALL);
	}
	else if(m_strEditLabelField == FIELD_PACKAGE_TITLE)
	{
		m_dpEdit.title = strLabel;
	}
	textPut("text", strLabel, PNL_MAIN, m_strEditLabelField);
	textPut("statesheet", "field_modified", PNL_MAIN, m_strEditLabelField);
	m_strEditLabelField = "";
	
	checkApply();
}

void dest_package_editor::checkApply()
{
	bncs_string strChanges = bncs_string("hd=%1,prehear=%2,mixminus=%3,coord=%4,mlisten=%5")
			.arg(m_dpEdit.video);
	debug(bncs_string("dest_package_editor::applyChanges() changes=%1").arg(strChanges));

	bool audioDirty = false;
	for (int level = 0; level < DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		if (m_dpEdit.audio[level] != m_dpDiff.audio[level] ||
			m_dpEdit.languageTag[level] != m_dpDiff.languageTag[level] ||
			m_dpEdit.languageTag_1[level] != m_dpDiff.languageTag_1[level] ||
			m_dpEdit.languageTag_2[level] != m_dpDiff.languageTag_2[level] ||
			m_dpEdit.typeTag[level] != m_dpDiff.typeTag[level] ||
			m_dpEdit.typeTag_1[level] != m_dpDiff.typeTag_1[level] ||
			m_dpEdit.typeTag_2[level] != m_dpDiff.typeTag_2[level])
		{
			audioDirty = true;
		}
	}

	bool revAudioDirty = false;
	for (int level = 0; level < DEST_PACKAGE_REV_AUDIO_COUNT; level++)
	{
		if (m_dpEdit.revAudio[level] != m_dpDiff.revAudio[level] )
		{
			revAudioDirty = true;
		}
	}

	bool ifbDirty = false;
	for (int level = 0; level < DEST_PACKAGE_IFB_COUNT; level++)
	{
		if (m_dpDiff.ifbRing[level] != m_dpEdit.ifbRing[level])
		{
			ifbDirty = true;
		}
		if (m_dpDiff.ifbPort[level] != m_dpEdit.ifbPort[level])
		{
			ifbDirty = true;
		}
	}

	if (m_dpEdit.name != m_dpDiff.name ||
		m_dpEdit.title != m_dpDiff.title ||
		m_dpEdit.video != m_dpDiff.video ||
		m_dpEdit.videoTag != m_dpDiff.videoTag ||
		m_dpEdit.anc[0] != m_dpDiff.anc[0] ||
		m_dpEdit.anc[1] != m_dpDiff.anc[1] ||
		m_dpEdit.anc[2] != m_dpDiff.anc[2] ||
		m_dpEdit.anc[3] != m_dpDiff.anc[3] ||
		m_dpEdit.revVideo != m_dpDiff.revVideo ||
		audioDirty == true || revAudioDirty == true || ifbDirty == true)
	{
		controlEnable(PNL_MAIN, "apply");
		m_applyEnabled = true;
	}
	else
	{
		controlDisable(PNL_MAIN, "apply");
		m_applyEnabled = false;
	}
}

void dest_package_editor::applyChanges()
{
	debug("dest_package_editor::applyChanges()");
	m_applyEnabled = false;
	
	bool ifbDirty = false;
	bncs_stringlist sltIFB;
	for (int level = 0; level < DEST_PACKAGE_IFB_COUNT; level++)
	{
		if (m_dpDiff.ifbRing[level] != m_dpEdit.ifbRing[level])
		{
			ifbDirty = true;
		}
		if (m_dpDiff.ifbPort[level] != m_dpEdit.ifbPort[level])
		{
			ifbDirty = true;
		}
		bncs_string field = bncs_string("ifb%1=%2.%3").arg(level + 1).arg(m_dpEdit.ifbRing[level]).arg(m_dpEdit.ifbPort[level]);
		sltIFB.append(field);
		debug("dest_package_editor::applyChanges() ifb level=%1 field=%2", level + 1, field);
	}

	//Update video Level
	if(m_dpEdit.video != m_dpDiff.video || 
		m_dpEdit.videoTag != m_dpDiff.videoTag || 
		m_dpEdit.anc[0] != m_dpDiff.anc[0] ||
		m_dpEdit.anc[1] != m_dpDiff.anc[1] ||
		m_dpEdit.anc[2] != m_dpDiff.anc[2] ||
		m_dpEdit.anc[3] != m_dpDiff.anc[3] ||
		m_dpEdit.revVideo != m_dpDiff.revVideo || 
		ifbDirty)
	{
		//17501=video=501#1,anc1=#1,anc2=#1,anc3=#1,anc4=#1,rev_vision=#1,ifb1=,ifb2=
		//19897=video=2897#1,anc1=#1,anc2=#1,anc3=#1,anc4=#1,rev_vision=#1,ifb1=,ifb2=
		bncs_stringlist levelData;
		levelData.append(bncs_string("video=%1#%2").arg(m_dpEdit.video).arg(m_dpEdit.videoTag));			//Video#Tag
		levelData.append(bncs_string("anc1=%1#%2").arg(m_dpEdit.anc[0]).arg(m_dpEdit.videoTag));			//Anc1#Tag
		levelData.append(bncs_string("anc2=%1#%2").arg(m_dpEdit.anc[1]).arg(m_dpEdit.videoTag));			//Anc2#Tag
		levelData.append(bncs_string("anc3=%1#%2").arg(m_dpEdit.anc[2]).arg(m_dpEdit.videoTag));			//Anc3#Tag
		levelData.append(bncs_string("anc4=%1#%2").arg(m_dpEdit.anc[3]).arg(m_dpEdit.videoTag));			//Anc4#Tag
		levelData.append(bncs_string("rev_vision=%1#%2").arg(m_dpEdit.revVideo).arg(m_dpEdit.videoTag));	//Rev#Tag
		levelData.append(sltIFB[0]);																		//ifb1=ring.port
		levelData.append(sltIFB[1]);																		//ifb2=ring.port
		routerModify(m_devicePackageRouter, DATABASE_DEST_LEVELS, m_destPackage, levelData.toString(), false);
	}
	else
	{
		debug("dest_package_editor::applyChanges() video level unmodified");
	}

	bool audioDestDirty = false;
	bncs_stringlist sltAudioDestLevels;
	for (int level = 0; level < DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		sltAudioDestLevels.append(bncs_string("%1").arg(m_dpEdit.audio[level]));

		if (m_dpEdit.audio[level] != m_dpDiff.audio[level])
		{
			audioDestDirty = true;
		}
	}

	if (audioDestDirty == true)
	{
		//19897=8169,8170,8171,8172,8173,8174,8175,8176,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		routerModify(m_devicePackageRouter, DATABASE_DEST_AUDIO, m_destPackage, sltAudioDestLevels.toString(), false);
	}

	bool audioLangTypeDirty = false;
	bncs_stringlist sltLangTypeLevels;
	for (int level = 0; level < DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		sltLangTypeLevels.append(bncs_string("%1:%2").arg(m_dpEdit.languageTag[level]).arg(m_dpEdit.typeTag[level]));

		if (m_dpEdit.languageTag[level] != m_dpDiff.languageTag[level] || 
			m_dpEdit.typeTag[level] != m_dpDiff.typeTag[level])
		{
			audioLangTypeDirty = true;
		}
	}

	if (audioLangTypeDirty == true)
	{
		//19897=2:151,2:152,2:153,2:154,2:155,2:156,2:157,2:158,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0
		routerModify(m_devicePackageRouter, DATABASE_DEST_LANG_TYPE, m_destPackage, sltLangTypeLevels.toString(), false);
	}

	bool audioLangTypeDirty_1 = false;
	bncs_stringlist sltLangTypeLevels_1;
	for (int level = 0; level < DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		sltLangTypeLevels_1.append(bncs_string("%1:%2").arg(m_dpEdit.languageTag_1[level]).arg(m_dpEdit.typeTag_1[level]));

		if (m_dpEdit.languageTag_1[level] != m_dpDiff.languageTag_1[level] ||
			m_dpEdit.typeTag_1[level] != m_dpDiff.typeTag_1[level])
		{
			audioLangTypeDirty_1 = true;
		}
	}

	if (audioLangTypeDirty_1 == true)
	{
		routerModify(m_devicePackageRouter, DATABASE_DEST_AUDIO_1, m_destPackage, sltLangTypeLevels_1.toString(), false);
	}

	bool audioLangTypeDirty_2 = false;
	bncs_stringlist sltLangTypeLevels_2;
	for (int level = 0; level < DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		sltLangTypeLevels_2.append(bncs_string("%1:%2").arg(m_dpEdit.languageTag_2[level]).arg(m_dpEdit.typeTag_2[level]));

		if (m_dpEdit.languageTag_2[level] != m_dpDiff.languageTag_2[level] ||
			m_dpEdit.typeTag_2[level] != m_dpDiff.typeTag_2[level])
		{
			audioLangTypeDirty_2 = true;
		}
	}

	if (audioLangTypeDirty_2 == true)
	{
		routerModify(m_devicePackageRouter, DATABASE_DEST_AUDIO_2, m_destPackage, sltLangTypeLevels_2.toString(), false);
	}
	bool revAudioDirty = false;
	bncs_stringlist sltRevAudioLevels;
	for (int level = 0; level < DEST_PACKAGE_REV_AUDIO_COUNT; level++)
	{
		//a%=audio#lang-tag:type-tag
		sltRevAudioLevels.append(bncs_string("a%1=%2#%3:%4").arg(level + 1).arg(m_dpEdit.revAudio[level]).arg(0).arg(0));
		if (m_dpEdit.revAudio[level] != m_dpDiff.revAudio[level])
		{
			revAudioDirty = true;
		}
	}
	
	if (revAudioDirty == true)
	{
		routerModify(m_devicePackageRouter, DATABASE_DEST_REV_AUDIO, m_destPackage, sltRevAudioLevels.toString(), false);
	}


	//Update Name
	if(m_dpEdit.name != m_dpDiff.name)
	{
		routerModify(m_devicePackageRouter, DATABASE_DEST_NAME, m_destPackage, m_dpEdit.name, false);
	}

	//Update Title
	if(m_dpEdit.title != m_dpDiff.title)
	{
		routerModify(m_devicePackageRouter, DATABASE_DEST_TITLE, m_destPackage, m_dpEdit.title, false);
	}
}

void dest_package_editor::setVideoDest()
{
	debug("dest_package_editor::setVideoDest() %1", m_selectedVideoDest);
	
	bncs_string videoDestName;
	routerName(m_deviceRouterVideoHD, DATABASE_DEST_NAME, m_selectedVideoDest, videoDestName);
	textPut("text", m_selectedVideoDest, PNL_MAIN, FIELD_VIDEO_DEST_INDEX);
	textPut("text", videoDestName, PNL_MAIN, FIELD_VIDEO_DEST_NAME);
	
	//Debug label
	textPut("text", bncs_string("%1 - %2").arg(m_selectedVideoDest, '0', 3)
		.arg(videoDestName.replace('|', ' ')), PNL_MAIN, "sdi_source");
	
	m_dpEdit.video = m_selectedVideoDest;
	textPut("statesheet", (m_dpEdit.video == m_dpDiff.video) ? "field_unmodified" : "field_modified", PNL_MAIN, FIELD_VIDEO_DEST_INDEX);
	textPut("statesheet", (m_dpEdit.video == m_dpDiff.video) ? "field_unmodified" : "field_modified", PNL_MAIN, FIELD_VIDEO_DEST_NAME);

	checkApply();
}

void dest_package_editor::setEditAudio(int level, int dest)
{
	//store edit data
	m_dpEdit.audio[level - 1] = dest;

	bncs_string editData = bncs_string("audio=%1").arg(dest);

	textPut("edit_data", editData, PNL_MAIN, bncs_string("audio_%1").arg(level, '0', 2));

	checkApply();
}

void dest_package_editor::setEditRevAudio(int level, int source)
{
	//store edit data
	m_dpEdit.revAudio[level - 1] = source;

	bncs_string editData = bncs_string("audio=%1").arg(source);

	textPut("edit_data", editData, PNL_MAIN, bncs_string("revaudio_%1").arg(level));

	checkApply();
}

void dest_package_editor::setEditRevVideo(int level, int source)
{
	//store edit data
	m_dpEdit.revVideo = source;

	bncs_string editData = bncs_string("sp=%1").arg(source);

	textPut("edit_data", editData, PNL_MAIN, FIELD_REV_VIDEO);

	checkApply();
}

void dest_package_editor::setEditVideo(int level, int dest)
{
	//Find corresponding ANC destination for selected Video
	int ancDest = 0;

	if (dest > 0)
	{
		//Get logical ID for Video Dest @@@_v01
		bncs_string videoID;
		routerName(m_deviceRouterVideoHD, DATABASE_DEST_ID, dest, videoID);

		debug("dest_package_editor::setEditVideo() routerName(m_deviceRouterVideoHD=%1, DATABASE_DEST_ID=%2, dest=%3, videoID)",
			m_deviceRouterVideoHD, DATABASE_DEST_ID, dest);

		debug("dest_package_editor::setEditVideo() dest=%1 videoID=%2", dest, videoID);

		if (videoID.endsWith("_v01"))
		{
			bncs_string ancID = videoID;
			ancID.replace("_v01", "_m01");

			//Check if there is corresponding @@@_m01 logical ID on the ANC router device
			ancDest = routerIndex(m_deviceRouterANC, DATABASE_DEST_ID, ancID);

			debug("dest_package_editor::setEditVideo() ancDest=%1 ancID=%2", dest, videoID);
		}
		else
		{
			debug("dest_package_editor::setEditVideo() videoID with _v01 suffix not found");
		}
	}


	//store edit data
	m_dpEdit.video = dest;
	m_dpEdit.anc[0] = ancDest;

	bncs_string editData = bncs_string("video=%1,anc_1=%2").arg(dest).arg(ancDest);

	textPut("edit_data", editData, PNL_MAIN, bncs_string("video_%1").arg(level));

	checkApply();
}

void dest_package_editor::setEditANC(int level, int anc, int dest)
{
	bncs_string editData;
	editData = bncs_string("anc_%1=%2").arg(anc).arg(dest);
	m_dpEdit.anc[anc - 1] = dest;

	debug("dest_package_editor::setEditANC editData=%1", editData);

	textPut("edit_data", editData, PNL_MAIN, bncs_string("video_%1").arg(level));

	checkApply();
}

void dest_package_editor::setEditLanguageTag()
{
	bncs_string controlID = bncs_string("audio_%1").arg(m_selectedAudioLevel, '0', 2);

	if (m_substituteLanguage == 0)
	{
		m_dpEdit.languageTag[m_selectedAudioLevel - 1] = m_selectedLanguageTag;
		textPut("edit_data", bncs_string("language_tag_0=%1").arg(m_selectedLanguageTag), PNL_MAIN, controlID);
	}
	else if (m_substituteLanguage == 1)
	{
		m_dpEdit.languageTag_1[m_selectedAudioLevel - 1] = m_selectedLanguageTag;
		textPut("edit_data", bncs_string("language_tag_1=%1").arg(m_selectedLanguageTag), PNL_MAIN, controlID);
	}
	else if (m_substituteLanguage == 2)
	{
		m_dpEdit.languageTag_2[m_selectedAudioLevel - 1] = m_selectedLanguageTag;
		textPut("edit_data", bncs_string("language_tag_2=%1").arg(m_selectedLanguageTag), PNL_MAIN, controlID);
	}

	checkApply();
}

void dest_package_editor::setEditTypeTag()
{
	bncs_string controlID = bncs_string("audio_%1").arg(m_selectedAudioLevel, '0', 2);
	if (m_substituteType == 0)
	{
		m_dpEdit.typeTag[m_selectedAudioLevel - 1] = m_selectedTypeTag;
		textPut("edit_data", bncs_string("type_tag_0=%1").arg(m_selectedTypeTag), PNL_MAIN, controlID);
	}
	else if (m_substituteType == 1)
	{
		m_dpEdit.typeTag_1[m_selectedAudioLevel - 1] = m_selectedTypeTag;
		textPut("edit_data", bncs_string("type_tag_1=%1").arg(m_selectedTypeTag), PNL_MAIN, controlID);
	}
	else if (m_substituteType == 2)
	{
		m_dpEdit.typeTag_2[m_selectedAudioLevel - 1] = m_selectedTypeTag;
		textPut("edit_data", bncs_string("type_tag_2=%1").arg(m_selectedTypeTag), PNL_MAIN, controlID);
	}

	checkApply();
}

void dest_package_editor::setEditVideoTag(int level, int videoTag)
{
	debug("source_package_editor::setEditVideoTag level=%1 videoTag=%2", level, videoTag);

	m_dpEdit.videoTag = videoTag;

	bncs_string editData = bncs_string("hub_tag=%1").arg(videoTag);
	textPut("edit_data", editData, PNL_MAIN, bncs_string("video_%1").arg(level));

	checkApply();
}

void dest_package_editor::setEditIFBInput(int level, int ring, int port, bncs_string inOut)
{
	debug("dest_package_editor::setEditIFBInput() level=%1 ring=%2 port=%3 inOut=%4", level, ring, port, inOut);

	bncs_string editData;

	if (inOut == "in")
	{
		m_dpEdit.ifbRing[level - 1] = ring;
		m_dpEdit.ifbPort[level - 1] = port;

		editData = bncs_string("in_ring=%1,in_port=%2")
			.arg(ring)
			.arg(port);

		textPut("edit_data", editData, PNL_MAIN, bncs_string("ifb_%1").arg(level));
	}

	debug("dest_package_editor::setEditIFBInput() edit_data=%1", editData);

	checkApply();
}

void dest_package_editor::refreshName(bncs_string strNewName)
{
	m_dpDiff.name = strNewName;
	m_dpEdit.name = strNewName;
	textPut("text", strNewName, PNL_MAIN, FIELD_PACKAGE_NAME);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_PACKAGE_NAME);
	textPut("text", strNewName.replace('|', ' '), PNL_MAIN, REPEAT_PACKAGE_NAME);
	checkApply();
}

void dest_package_editor::refreshTitle(bncs_string strNewName)
{
	textPut("text", strNewName, PNL_MAIN, FIELD_PACKAGE_TITLE);
	textPut("statesheet", "field_unmodified", PNL_MAIN, FIELD_PACKAGE_TITLE);
	m_dpDiff.title = strNewName;
	m_dpEdit.title = strNewName;
	checkApply();
}

void dest_package_editor::refreshLevels(bncs_string newLevels)
{
	//video=2987#1,anc1=#1,anc2=#1,anc3=#1,anc4=#1,rev_vision=#1,ifb1=r.p,ifb2=r.p

	bncs_stringlist sltLevels = bncs_stringlist().fromString(newLevels);

	bncs_stringlist sltDestLevels = bncs_stringlist().fromString(newLevels);
	debug("dest_package_editor::refreshLevels() dest levels=%1", sltDestLevels.toString());

	//Update Video Dest
	bncs_stringlist sltVideoHub(sltDestLevels.getNamedParam("video"), '#');
	int videoDest = sltVideoHub[0].toInt();
	m_dpDiff.video = videoDest;
	m_dpEdit.video = videoDest;
	int videoTag = sltVideoHub[1].toInt();
	m_dpDiff.videoTag = videoTag;
	m_dpEdit.videoTag = videoTag;

	//Update ANC Dest 1
	bncs_stringlist sltAncHub1(sltDestLevels.getNamedParam("anc1"), '#');
	int anc1 = sltAncHub1[0].toInt();
	m_dpDiff.anc[0] = anc1;
	m_dpEdit.anc[0] = anc1;

	debug("dest_package_editor::refreshLevels() anc1=%1 diff=%2 edit=%3", anc1, m_dpDiff.anc[0], m_dpEdit.anc[0]);

	debug("dest_package_editor::refreshLevels() AAA diff anc1=%1 anc2=%2 anc3=%3 anc4=%4",
		m_dpDiff.anc[0], m_dpDiff.anc[1], m_dpDiff.anc[2], m_dpDiff.anc[3]);
	debug("dest_package_editor::refreshLevels() AAA edit anc1=%1 anc2=%2 anc3=%3 anc4=%4",
		m_dpEdit.anc[0], m_dpEdit.anc[1], m_dpEdit.anc[2], m_dpEdit.anc[3]);

	//Update ANC Dest 2
	bncs_stringlist sltAncHub2(sltDestLevels.getNamedParam("anc2"), '#');
	int anc2 = sltAncHub2[0].toInt();
	m_dpDiff.anc[1] = anc2;
	m_dpEdit.anc[1] = anc2;

	//Update ANC Dest 3
	bncs_stringlist sltAncHub3(sltDestLevels.getNamedParam("anc3"), '#');
	int anc3 = sltAncHub3[0].toInt();
	m_dpDiff.anc[2] = anc3;
	m_dpEdit.anc[2] = anc3;

	//Update ANC Dest 4
	bncs_stringlist sltAncHub4(sltDestLevels.getNamedParam("anc4"), '#');
	int anc4 = sltAncHub4[0].toInt();
	m_dpDiff.anc[3] = anc4;
	m_dpEdit.anc[3] = anc4;

	bncs_string videoData = bncs_string("video=%1,hub_tag=%2,anc_1=%3,anc_2=%4,anc_3=%5,anc_4=%6")
		.arg(videoDest).arg(videoTag).arg(anc1).arg(anc2).arg(anc3).arg(anc4);
	textPut("diff_data", videoData, PNL_MAIN, bncs_string("video_%1").arg(1));
	textPut("edit_data", videoData, PNL_MAIN, bncs_string("video_%1").arg(1));

	debug("dest_package_editor::refreshLevels() BBB diff anc1=%1 anc2=%2 anc3=%3 anc4=%4", 
		m_dpDiff.anc[0], m_dpDiff.anc[1], m_dpDiff.anc[2], m_dpDiff.anc[3]);

	debug("dest_package_editor::refreshLevels() BBB edit anc1=%1 anc2=%2 anc3=%3 anc4=%4",
		m_dpEdit.anc[0], m_dpEdit.anc[1], m_dpEdit.anc[2], m_dpEdit.anc[3]);

	//Update Reverse Video
	bncs_stringlist sltRevVideoHub(sltDestLevels.getNamedParam("rev_vision"), '#');
	int revVideo = sltRevVideoHub[0].toInt();
	m_dpDiff.revVideo = revVideo;
	m_dpEdit.revVideo = revVideo;

	bncs_string revVideoData = bncs_string("sp=%1").arg(revVideo);
	textPut("diff_data", revVideoData, PNL_MAIN, FIELD_REV_VIDEO);
	textPut("edit_data", revVideoData, PNL_MAIN, FIELD_REV_VIDEO);

	//Update IFB 1&2
	for (int level = 0; level < DEST_PACKAGE_IFB_COUNT; level++)
	{
		bncs_stringlist sltIFB(sltDestLevels.getNamedParam(bncs_string("ifb%1").arg(level + 1)), '.');

		int inRing = sltIFB[0].toInt();
		m_dpDiff.ifbRing[level] = inRing;
		m_dpEdit.ifbRing[level] = inRing;
		
		int inPort = sltIFB[1].toInt();
		m_dpDiff.ifbPort[level] = inPort;
		m_dpEdit.ifbPort[level] = inPort;

		bncs_string IFBData = bncs_string("in_ring=%1,in_port=%2")
			.arg(inRing)
			.arg(inPort);

		textPut("diff_data", IFBData, PNL_MAIN, bncs_string("ifb_%1").arg(level + 1));
		textPut("edit_data", IFBData, PNL_MAIN, bncs_string("ifb_%1").arg(level + 1));
	}
	checkApply();
}

void dest_package_editor::refreshAudioLevels(bncs_string newLevels)
{
	bncs_stringlist sltLevels(newLevels);

	debug("dest_package_editor::refreshAudioLevels levels=%1", newLevels);

	//19897=8169,8170,8171,8172,8173,8174,8175,8176,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

	//update levels
	for (int level = 1; level <= DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		int audioDest = sltLevels[level - 1].toInt();
		bncs_string controlID = bncs_string("audio_%1").arg(level, '0', 2);
		debug("dest_package_editor::refreshAudioLevels level=%1 audioDest=%2 controlID=%3 ", level, audioDest, controlID);

		textPut("diff_data", bncs_string("audio=%1").arg(audioDest), PNL_MAIN, controlID);
		textPut("edit_data", bncs_string("audio=%1").arg(audioDest), PNL_MAIN, controlID);

		m_dpDiff.audio[level - 1] = audioDest;
		m_dpEdit.audio[level - 1] = audioDest;
	}

	checkApply();
}

void dest_package_editor::refreshAudioTags(bncs_string newTags)
{
	bncs_stringlist sltTags(newTags);
	//19897=2:151,2:152,2:153,2:154,2:155,2:156,2:157,2:158,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0
	debug("dest_package_editor::refreshAudioTags tags=%1", newTags);

	//update tags
	for (int level = 1; level <= DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		bncs_stringlist sltLanguageType(sltTags[level - 1], ':');
		int languageTag = sltLanguageType[0].toInt();
		int typeTag = sltLanguageType[1].toInt();
		bncs_string controlID = bncs_string("audio_%1").arg(level, '0', 2);
		debug("dest_package_editor::refreshAudioTags tag=%1 language=%2 type=%3 controlID=%3 ", level, languageTag, typeTag, controlID);

		textPut("diff_data", bncs_string("language_tag_0=%1,type_tag_0=%2").arg(languageTag).arg(typeTag), PNL_MAIN, controlID);
		textPut("edit_data", bncs_string("language_tag_0=%1,type_tag_0=%2").arg(languageTag).arg(typeTag), PNL_MAIN, controlID);

		m_dpDiff.languageTag[level - 1] = languageTag;
		m_dpEdit.languageTag[level - 1] = languageTag;

		m_dpDiff.typeTag[level - 1] = typeTag;
		m_dpEdit.typeTag[level - 1] = typeTag;
	}

	checkApply();
}

void dest_package_editor::refreshAudioTags_1(bncs_string newTags)
{
	bncs_stringlist sltTags(newTags);
	//19897=2:151,2:152,2:153,2:154,2:155,2:156,2:157,2:158,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0
	debug("dest_package_editor::refreshAudioTags subsitute 1 tags=%1", newTags);

	//update tags
	for (int level = 1; level <= DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		bncs_stringlist sltLanguageType(sltTags[level - 1], ':');
		int languageTag = sltLanguageType[0].toInt();
		int typeTag = sltLanguageType[1].toInt();
		bncs_string controlID = bncs_string("audio_%1").arg(level, '0', 2);
		debug("dest_package_editor::refreshAudioTags subsitute 1 tag=%1 language=%2 type=%3 controlID=%3 ", level, languageTag, typeTag, controlID);

		textPut("diff_data", bncs_string("language_tag_1=%1,type_tag_1=%2").arg(languageTag).arg(typeTag), PNL_MAIN, controlID);
		textPut("edit_data", bncs_string("language_tag_1=%1,type_tag_1=%2").arg(languageTag).arg(typeTag), PNL_MAIN, controlID);

		m_dpDiff.languageTag_1[level - 1] = languageTag;
		m_dpEdit.languageTag_1[level - 1] = languageTag;

		m_dpDiff.typeTag_1[level - 1] = typeTag;
		m_dpEdit.typeTag_1[level - 1] = typeTag;
	}

	checkApply();
}

void dest_package_editor::refreshAudioTags_2(bncs_string newTags)
{
	bncs_stringlist sltTags(newTags);
	//19897=2:151,2:152,2:153,2:154,2:155,2:156,2:157,2:158,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0,0:0
	debug("dest_package_editor::refreshAudioTags subsitute 2 tags=%1", newTags);

	//update tags
	for (int level = 1; level <= DEST_PACKAGE_AUDIO_COUNT; level++)
	{
		bncs_stringlist sltLanguageType(sltTags[level - 1], ':');
		int languageTag = sltLanguageType[0].toInt();
		int typeTag = sltLanguageType[1].toInt();
		bncs_string controlID = bncs_string("audio_%1").arg(level, '0', 2);
		debug("dest_package_editor::refreshAudioTags subsitute 2 tag=%1 language=%2 type=%3 controlID=%3 ", level, languageTag, typeTag, controlID);

		textPut("diff_data", bncs_string("language_tag_2=%1,type_tag_2=%2").arg(languageTag).arg(typeTag), PNL_MAIN, controlID);
		textPut("edit_data", bncs_string("language_tag_2=%1,type_tag_2=%2").arg(languageTag).arg(typeTag), PNL_MAIN, controlID);

		m_dpDiff.languageTag_2[level - 1] = languageTag;
		m_dpEdit.languageTag_2[level - 1] = languageTag;

		m_dpDiff.typeTag_2[level - 1] = typeTag;
		m_dpEdit.typeTag_2[level - 1] = typeTag;
	}

	checkApply();
}

void dest_package_editor::refreshReverseAudioLevels(bncs_string newLevels)
{
	bncs_stringlist sltLevels(newLevels);

	debug("dest_package_editor::refreshReverseAudioLevels levels=%1", newLevels);

	//19897=a1=audio#lang-tag:type-tag,a2=audio#lang-tag:type-tag

	//update levels
	for (int level = 1; level <= DEST_PACKAGE_REV_AUDIO_COUNT; level++)
	{
		bncs_string data = sltLevels.getNamedParam(bncs_string("a%1").arg(level));

		bncs_string audio;
		bncs_string langType;
		data.split('#', audio, langType);

		int audioSource = audio.toInt();

		bncs_string lang;
		bncs_string type;
		langType.split(':', lang, type);


		debug("dest_package_editor::refreshReverseAudioLevels level=%1 audio=%2 lang=%3 type=%4", level, audio, lang, type);

		//TODO store lang/type tags - depends on Nigel

		bncs_string controlID = bncs_string("revaudio_%1").arg(level);
		debug("dest_package_editor::refreshReverseAudioLevels level=%1 audioSource=%2 controlID=%3 ", level, audioSource, controlID);

		textPut("diff_data", bncs_string("audio=%1").arg(audioSource), PNL_MAIN, controlID);
		textPut("edit_data", bncs_string("audio=%1").arg(audioSource), PNL_MAIN, controlID);

		m_dpDiff.revAudio[level - 1] = audioSource;
		m_dpEdit.revAudio[level - 1] = audioSource;
	}

	checkApply();
}

void dest_package_editor::showAudioDestPage(int page)
{
	int pageItems = 8;

	if (m_audioDestPage != page)
	{
		//highlight buttons
		textPut("statesheet", "enum_deselected", PNL_MAIN, bncs_string("audio-page_%1").arg(m_audioDestPage));
	}
	m_audioDestPage = page;
	textPut("statesheet", "enum_selected", PNL_MAIN, bncs_string("audio-page_%1").arg(m_audioDestPage));

	//first hide all
	for (int l = 1; l <= DEST_PACKAGE_AUDIO_COUNT; l++)
	{
		controlHide(PNL_MAIN, bncs_string("audio_%1").arg(l, '0', 2));
	}

	//show range of 8 for current page
	int offset = (m_audioDestPage - 1) * pageItems;
	for (int level = offset + 1; level <= offset + pageItems; level++)
	{
		controlShow(PNL_MAIN, bncs_string("audio_%1").arg(level, '0', 2));
	}
	debug("dest_package_editor::showAudioDestPage() page=%1 offset=%2", m_audioDestPage, offset);
}

void dest_package_editor::showSelectedAudioLevel()
{
	bncs_stringlist sltAudio = getIdList(PNL_MAIN, "audio_");
	for (int level = 0; level < sltAudio.count(); level++)
	{
		textPut("select_level", m_selectedAudioLevel, PNL_MAIN, sltAudio[level]);
	}
}

void dest_package_editor::showSelectedAudioDest()
{
	if (m_selectedAudioDest > 0)
	{
		bncs_string selectedAudioDestName;
		routerName(m_deviceRouterAudio, DATABASE_DEST_NAME, m_selectedAudioDest, selectedAudioDestName);
		textPut("text", bncs_string("%1|[%2]").arg(selectedAudioDestName).arg(m_selectedAudioDest), POPOVER_SELECT_AUDIO_DEST, "selected_dest");
	}
	else
	{
		textPut("text", "-", POPOVER_SELECT_AUDIO_DEST, "selected_dest");
	}

	//check if this destination is used in a dest package with a different index
	textPut("text", "", POPOVER_SELECT_AUDIO_DEST, "dest_use_warning");
	textPut("statesheet", "enum_deselected", POPOVER_SELECT_AUDIO_DEST, "dest_use_warning");

	//calculate device
	int device = (m_selectedAudioDest - 1) / 4000;
	m_selectedNevionDestUseDevice = m_deviceNevionDestUse[device];
	m_selectedNevionDestUseSlot = ((m_selectedAudioDest - 1) % 4000) + 1;
	m_selectedNevionDestUseLevel = NEVION_DEST_USE_AUDIO;

	//poll for usage
	if (m_selectedNevionDestUseSlot > 0)
	{
		infoPoll(m_selectedNevionDestUseDevice, m_selectedNevionDestUseSlot, m_selectedNevionDestUseSlot);
		debug("dest_package_editor::showSelectedAudioDest() polling device=%1 slot=%2",
			m_selectedNevionDestUseDevice, m_selectedNevionDestUseSlot);
	}

	controlEnable(POPOVER_SELECT_AUDIO_DEST, BTN_SET);
}

void dest_package_editor::showSelectedAudioSource()
{
	bncs_string selectedAudioSourceName = "---";
	if (m_selectedAudioSource > 0)
	{
		routerName(m_deviceRouterAudio, DATABASE_SOURCE_NAME, m_selectedAudioSource, selectedAudioSourceName);
		textPut("text", bncs_string("%1|[%2]").arg(selectedAudioSourceName).arg(m_selectedAudioSource), POPOVER_SELECT_AUDIO_SOURCE, "selected_source");
	}
	else
	{
		textPut("text", "-", POPOVER_SELECT_AUDIO_SOURCE, "selected_source");
	}


	controlEnable(POPOVER_SELECT_AUDIO_SOURCE, BTN_SET);
}

void dest_package_editor::showSelectedSourcePackage()
{
	bncs_string selectedSourcePackageName = "---";
	if (m_selectedSourcePackage > 0)
	{
		routerName(m_devicePackageRouter, DATABASE_SOURCE_NAME, m_selectedSourcePackage, selectedSourcePackageName);
	}
	textPut("text", selectedSourcePackageName, POPOVER_SELECT_SOURCE_PACKAGE, "selected_source");

	controlEnable(POPOVER_SELECT_SOURCE_PACKAGE, BTN_SET);
}

void dest_package_editor::showSelectedVideoLevel()
{
	bncs_stringlist sltVideo = getIdList(PNL_MAIN, "video_");
	for (int level = 0; level < sltVideo.count(); level++)
	{
		textPut("select_level", m_selectedVideoLevel, PNL_MAIN, sltVideo[level]);
	}
}

void dest_package_editor::showSelectedVideoDest()
{
	if (m_selectedVideoDest > 0)
	{
		bncs_string selectedVideoDestName;
		routerName(m_deviceRouterVideoHD, DATABASE_DEST_NAME, m_selectedVideoDest, selectedVideoDestName);
		textPut("text", bncs_string("%1|[%2]").arg(selectedVideoDestName).arg(m_selectedVideoDest), POPOVER_SELECT_VIDEO_DEST, "selected_dest");
	}
	else
	{
		textPut("text", "-", POPOVER_SELECT_VIDEO_DEST, "selected_dest");
	}

	//check if this destination is used in a dest package with a different index
	textPut("text", "", POPOVER_SELECT_VIDEO_DEST, "dest_use_warning");
	textPut("statesheet", "enum_deselected", POPOVER_SELECT_VIDEO_DEST, "dest_use_warning");

	//calculate device
	//int device = (m_selectedVideoDest - 1) / 4000;
	//m_selectedNevionDestUseDevice = m_deviceNevionDestUse[device];
	//m_selectedNevionDestUseSlot = ((m_selectedVideoDest - 1) % 4000) + 1;
	m_selectedNevionDestUseLevel = NEVION_DEST_USE_HD;
	m_selectedNevionDestUseDevice = m_packagerNevionDestUse.destDev(m_selectedVideoDest);
	m_selectedNevionDestUseSlot = m_packagerNevionDestUse.destSlot(m_selectedVideoDest);

	debug("dest_package_editor::showSelectedVideoDest() m_selectedVideoDest=%1", m_selectedVideoDest);
	debug("dest_package_editor::showSelectedVideoDest() m_selectedNevionDestUseDevice=%1", m_selectedNevionDestUseDevice);
	debug("dest_package_editor::showSelectedVideoDest() m_selectedNevionDestUseSlot=%1", m_selectedNevionDestUseSlot);


	//poll for usage
	if (m_selectedNevionDestUseSlot > 0)
	{
		infoPoll(m_selectedNevionDestUseDevice, m_selectedNevionDestUseSlot, m_selectedNevionDestUseSlot);
		debug("dest_package_editor::showSelectedVideoDest() polling device=%1 slot=%2",
			m_selectedNevionDestUseDevice, m_selectedNevionDestUseSlot);
	}

	controlEnable(POPOVER_SELECT_VIDEO_DEST, BTN_SET);
}

void dest_package_editor::showSelectedRevAudioLevel()
{
	bncs_stringlist sltAudio = getIdList(PNL_MAIN, "revaudio_");
	for (int level = 0; level < sltAudio.count(); level++)
	{
		textPut("select_level", m_selectedRevAudioLevel, PNL_MAIN, sltAudio[level]);
	}
}

void dest_package_editor::showSelectedRevVideoLevel()
{
	textPut("select_level", m_selectedRevVideoLevel, PNL_MAIN, FIELD_REV_VIDEO);
}

void dest_package_editor::showSelectedIFBLevel()
{
	bncs_stringlist sltIFB = getIdList(PNL_MAIN, "ifb_");
	for (int level = 0; level < sltIFB.count(); level++)
	{
		textPut("select_level", m_selectedIFBLevel, PNL_MAIN, sltIFB[level]);
	}
}

void dest_package_editor::showEditANC()
{
	bncs_string name1 = "---";
	bncs_string name2 = "---";
	bncs_string name3 = "---";
	bncs_string name4 = "---";

	routerName(m_deviceRouterANC, DATABASE_DEST_NAME, m_dpEdit.anc[0], name1);
	routerName(m_deviceRouterANC, DATABASE_DEST_NAME, m_dpEdit.anc[1], name2);
	routerName(m_deviceRouterANC, DATABASE_DEST_NAME, m_dpEdit.anc[2], name3);
	routerName(m_deviceRouterANC, DATABASE_DEST_NAME, m_dpEdit.anc[3], name4);

	textPut("text", name1.replace('|', ' '), POPOVER_SELECT_ANC_DEST, "anc_1");
	textPut("text", name2.replace('|', ' '), POPOVER_SELECT_ANC_DEST, "anc_2");
	textPut("text", name3.replace('|', ' '), POPOVER_SELECT_ANC_DEST, "anc_3");
	textPut("text", name4.replace('|', ' '), POPOVER_SELECT_ANC_DEST, "anc_4");

	textPut("statesheet", (m_dpEdit.anc[0] == m_dpDiff.anc[0]) ? "level_clean" : "level_dirty", POPOVER_SELECT_ANC_DEST, "anc_1");
	textPut("statesheet", (m_dpEdit.anc[1] == m_dpDiff.anc[1]) ? "level_clean" : "level_dirty", POPOVER_SELECT_ANC_DEST, "anc_2");
	textPut("statesheet", (m_dpEdit.anc[2] == m_dpDiff.anc[2]) ? "level_clean" : "level_dirty", POPOVER_SELECT_ANC_DEST, "anc_3");
	textPut("statesheet", (m_dpEdit.anc[3] == m_dpDiff.anc[3]) ? "level_clean" : "level_dirty", POPOVER_SELECT_ANC_DEST, "anc_4");
}

void dest_package_editor::initAudioTypeTagPopup(int level, int typeTag)
{
	textPut("text", bncs_string("Set Audio Type Tag - Level: %1").arg(level), POPUP_AUDIO_TYPE_TAG, "title");

	textPut("add", bncs_string("%1;#TAG=%2").arg("[NONE]").arg(0), POPUP_AUDIO_TYPE_TAG, "lvw_tags");
	for (int tag = 1; tag <= 500; tag++)
	{
		bncs_string tagValueStatus;
		routerName(m_devicePackageRouter, DATABASE_TYPE_TAG, tag, tagValueStatus);

		bncs_string tagValue;
		bncs_string tagStatus;
		tagValueStatus.split('|', tagValue, tagStatus);

		int status = tagStatus.toInt();
		if (status == 1)
		{
			textPut("add", bncs_string("%1;#TAG=%2").arg(tagValue).arg(tag), POPUP_AUDIO_TYPE_TAG, "lvw_tags");
		}
	}

	textPut("selectTag", typeTag, POPUP_AUDIO_TYPE_TAG, "lvw_tags");
}

void dest_package_editor::initVideoHubTagPopup(int level, int videoTag)
{
	textPut("text", bncs_string("Set Video Hub Tag - Level: %1").arg(level), POPUP_VIDEO_HUB_TAG, "title");

	textPut("add", bncs_string("%1;#TAG=%2").arg("[NONE]").arg(0), POPUP_VIDEO_HUB_TAG, "lvw_tags");
	for (int tag = 1; tag <= 30; tag++)
	{
		bncs_string tagValueStatus;
		routerName(m_devicePackageRouter, DATABASE_VIDEO_HUB_TAG, tag, tagValueStatus);

		bncs_string tagValue;
		bncs_string tagStatus;
		tagValueStatus.split('|', tagValue, tagStatus);

		int status = tagStatus.toInt();
		if (status == 1)
		{
			textPut("add", bncs_string("%1[%2];#TAG=%3").arg(tagValue).arg(m_devicePackageRouter).arg(tag), POPUP_VIDEO_HUB_TAG, "lvw_tags");
		}
	}

	textPut("selectTag", videoTag, POPUP_VIDEO_HUB_TAG, "lvw_tags");
}

void dest_package_editor::popoverSelectCommsInput(buttonNotify *b)
{
	bool clearLevel = false;

	debug("dest_package_editor::popupSelectCommsInput");
	if (b->id() == "grid" && b->command() == "index")
	{
		if (b->value().contains('.'))
		{
			bncs_string ring;
			bncs_string port;
			b->value().split('.', ring, port);

			m_selectedCommsRing = ring.toInt();
			m_selectedCommsPort = port.toInt();

			textPut("text", bncs_string("Ring: %1|Port: %2").arg(m_selectedCommsRing).arg(m_selectedCommsPort), POPOVER_SELECT_COMMS_INPUT, "selected_port");
		}
		else
		{
			m_selectedCommsRing = 0;
			m_selectedCommsPort = 0;
			textPut("text", "---", POPOVER_SELECT_COMMS_INPUT, "selected_port");
		}
	}
	else if (b->id() == BTN_NONE)
	{
		textPut("deselect", POPOVER_SELECT_COMMS_INPUT, "grid");
		m_selectedCommsRing = 0;
		m_selectedCommsPort = 0;
		textPut("text", "---", POPOVER_SELECT_COMMS_INPUT, "selected_port");
	}
	else if (b->id() == BTN_SET)
	{
		panelRemove(POPOVER_SELECT_COMMS_INPUT);


		if (m_selectedIFBLevel > 0)
		{
			setEditIFBInput(m_selectedIFBLevel, m_selectedCommsRing, m_selectedCommsPort, "in");	//in & out
		}

		clearLevel = true;
	}
	else if (b->id() == BTN_CANCEL)
	{
		panelRemove(POPOVER_SELECT_COMMS_INPUT);
		clearLevel = true;
	}

	if (clearLevel)
	{
		if (m_selectedIFBLevel > 0)
		{
			//Clear IFB level selection
			m_selectedIFBLevel = 0;
			showSelectedIFBLevel();
		}
	}
}

void dest_package_editor::audioPresetRecall(bncs_string presetData)
{
	debug("dest_package_editor::audioPresetRecall() presetData=%1", presetData);

	//split into primary, sub1, sub2
	bncs_stringlist sltPrimarySub1Sub2(presetData, '|');

	for (int sub = 0; sub <= 2; sub++)
	{
		//Write to edit buffer
		bncs_stringlist sltLevels(sltPrimarySub1Sub2[sub], ',');

		//debug("dest_package_editor::audioPresetRecall() sub=%1 data=%2", sub, sltPrimarySub1Sub2[sub]);

		for (int index = 0; index < sltLevels.count(); index++)
		{
			bncs_string lang;
			bncs_string type;
			sltLevels[index].split(':', lang, type);

			int langNum = lang.toInt();
			int typeNum = type.toInt();

			if (sub == 0)
			{
				m_dpEdit.languageTag[index] = langNum;
				m_dpEdit.typeTag[index] = typeNum;
			}
			else if (sub == 1)
			{
				m_dpEdit.languageTag_1[index] = langNum;
				m_dpEdit.typeTag_1[index] = typeNum;
			}
			else if (sub == 2)
			{
				m_dpEdit.languageTag_2[index] = langNum;
				m_dpEdit.typeTag_2[index] = typeNum;
			}

			bncs_string controlID = bncs_string("audio_%1").arg(index + 1, '0', 2);
			bncs_string editData = bncs_string("language_tag_%1=%2,type_tag_%3=%4")
				.arg(sub).arg(langNum).arg(sub).arg(typeNum);

			textPut("edit_data", editData, PNL_MAIN, controlID);
		}
	}

	checkApply();
}

void dest_package_editor::showNevionDestUse(bncs_string fields)
{
	//hd=17001,audio=17001,anc=,revvis=,audret=
	bncs_stringlist sltNevionDestUse(fields);

	int index = sltNevionDestUse.getNamedParam(m_selectedNevionDestUseLevel);
	bncs_string name;
	routerName(m_devicePackageRouter, DATABASE_DEST_NAME, index, name);

	if (m_selectedNevionDestUseLevel == NEVION_DEST_USE_HD)
	{
		if (index == m_destPackage || index == 0)
		{
			textPut("text", "", POPOVER_SELECT_VIDEO_DEST, "dest_use_warning");
			textPut("statesheet", "enum_deselected", POPOVER_SELECT_VIDEO_DEST, "dest_use_warning");
			controlEnable(POPOVER_SELECT_VIDEO_DEST, BTN_SET);
		}
		else
		{
			textPut("text", bncs_string("Dest used|in [%1]|%2").arg(index).arg(name), POPOVER_SELECT_VIDEO_DEST, "dest_use_warning");
			textPut("statesheet", "enum_warning", POPOVER_SELECT_VIDEO_DEST, "dest_use_warning");
			controlDisable(POPOVER_SELECT_VIDEO_DEST, BTN_SET);
		}
	}
	else if (m_selectedNevionDestUseLevel == NEVION_DEST_USE_AUDIO)
	{
		if (index == m_destPackage || index == 0)
		{
			textPut("text", "", POPOVER_SELECT_AUDIO_DEST, "dest_use_warning");
			textPut("statesheet", "enum_deselected", POPOVER_SELECT_AUDIO_DEST, "dest_use_warning");
			controlEnable(POPOVER_SELECT_AUDIO_DEST, BTN_SET);
		}
		else
		{
			textPut("text", bncs_string("Dest used|in [%1]|%2").arg(index).arg(name), POPOVER_SELECT_AUDIO_DEST, "dest_use_warning");
			textPut("statesheet", "enum_warning", POPOVER_SELECT_AUDIO_DEST, "dest_use_warning");
			controlDisable(POPOVER_SELECT_AUDIO_DEST, BTN_SET);
		}
	}

}