#ifndef dest_package_editor_INCLUDED
	#define dest_package_editor_INCLUDED

#include <bncs_script_helper.h>
#include "DestPackage.h"
#include "bncs_packager.h"

//#include <map>
//typedef map<int, bncs_string> MAP_INT_BNCS_STRING;
//typedef map<int, bncs_string>::iterator MAP_INT_BNCS_STRING_ITERATOR;

#define HYDRA_AUDIO_LEVELS			4
#define NEVION_DEST_USE_DEVICES		16

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class dest_package_editor : public bncs_script_helper
{
public:
	dest_package_editor( bncs_client_callback * parent, const char* path );
	virtual ~dest_package_editor();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_strEditPortField;
	bncs_string m_strEditLabelField;
	bncs_string m_strDestPackageType;
	bncs_string m_myParam;

	bncs_string m_instanceRouterVideoHD;
	bncs_string m_instanceRouterVideoHD_section_01;
	bncs_string m_instanceRouterAudio;
	bncs_string m_instanceRouterAudio_section_01;
	bncs_string m_instanceRouterANC;
	bncs_string m_instanceRouterANC_section_01;

	bncs_string m_instancePackagerRouterMain;

	bncs_string m_compositePackager;

	bool m_lock;
	bool m_applyEnabled;
	int m_destPackage;
	int m_audioDestPage;

	int m_devicePackageRouter;		//dev_1001
	int m_deviceDestStatus;			//dev_1061+
	int m_deviceRouterVideoHD;		//dev_1201
	int m_deviceRouterAudio;		//dev_1261
	int m_deviceRouterANC;			//dev_1241 or dev_2241

	int m_slotDestStatus;

	int m_selectedPort;
	int m_intSelectedANCDest;
	int m_selectedVideoDest;
	int m_intSelectedANCLevel;
	int m_selectedVideoLevel;
	int m_selectedAudioDest;
	int m_selectedAudioLevel;
	int m_selectedTypeTag;
	int m_selectedLanguageTag;
	int m_selectedVideoTag;
	int m_selectedRevAudioLevel;
	int m_selectedAudioSource;
	int m_selectedRevVideoLevel;
	int m_selectedSourcePackage;
	int m_selectedIFBLevel;
	int m_selectedCommsRing;
	int m_selectedCommsPort;

	int m_substituteLanguage;
	int m_substituteType;

	int m_selectedNevionDestUseDevice;
	int m_selectedNevionDestUseSlot;
	bncs_string m_selectedNevionDestUseLevel;

	CDestPackage m_dpDiff;			//contains a snapshot of the dest package definition prior to editing
	CDestPackage m_dpEdit;			//contains the levels of the dest package that is being edited

	//MAP_INT_BNCS_STRING m_mapNevionDestUse;	//key=index 1-65536 - value="hd=17001,audio=17001,anc=,revvis=,audret="

	int m_deviceNevionDestUse[NEVION_DEST_USE_DEVICES];

	bncs_packager m_packagerRouter; 
	bncs_packager m_packagerNevionDestUse;
	bncs_packager m_packagerDestStatus;

	void applyChanges();
	void checkApply();
	void editField(bncs_string strField, int intNameDatabase, bncs_string strCaption);
	void editLabel(bncs_string strField);
	void editPackage();
	void initEditor();
	void initAudioTypeTagPopup(int level, int typeTag);
	void initVideoHubTagPopup(int level, int videoTag);
	void popoverSelectCommsInput(buttonNotify *b);
	void refreshAudioLevels(bncs_string newLevels);
	void refreshAudioTags(bncs_string newTags);
	void refreshAudioTags_1(bncs_string newTags);
	void refreshAudioTags_2(bncs_string newTags);
	void refreshReverseAudioLevels(bncs_string newLevels);
	void refreshLevels(bncs_string newLevels);
	void refreshName(bncs_string strNewName);
	void refreshTitle(bncs_string strNewName);

	void setEditANC(int level, int anc, int dest);
	void setEditAudio(int level, int dest);
	void setEditRevAudio(int level, int source);
	void setEditRevVideo(int level, int source);
	void setEditVideo(int level, int dest);
	void setEditLanguageTag();
	void setEditTypeTag();
	void setEditVideoTag(int level, int videoTag);
	void setEditIFBInput(int level, int ring, int port, bncs_string inOut);
	void setVideoDest();
	void showEditANC();
	void showAudioDestPage(int page);
	void showSelectedAudioLevel();
	void showSelectedAudioDest();
	void showSelectedVideoLevel();
	void showSelectedVideoDest();
	void showSelectedRevAudioLevel();
	void showSelectedAudioSource();
	void showSelectedRevVideoLevel();
	void showSelectedSourcePackage();
	void showSelectedIFBLevel();
	void showNevionDestUse(bncs_string fields);
	void updateLabel(bncs_string strLabel);
	void updateStatus(bncs_string status);
	void audioPresetRecall(bncs_string presetData);
};


#endif // dest_package_editor_INCLUDED