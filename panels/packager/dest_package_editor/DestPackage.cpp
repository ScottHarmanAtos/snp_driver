// DestPackage.cpp: implementation of the CDestPackage class.
//
//////////////////////////////////////////////////////////////////////

#include "DestPackage.h"
#include <bncs_string.h>




//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDestPackage::CDestPackage()
{

}

CDestPackage::~CDestPackage()
{

}

void CDestPackage::reset()
{
	index = 0;
	video = 0;
	videoTag = 0;
	anc[0] = 0;
	anc[1] = 0;
	anc[2] = 0;
	anc[3] = 0;
	revVideo = 0;
	title = "";	//TBC reset
	name = "";	//TBC reset
}
