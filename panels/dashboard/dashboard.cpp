#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include "dashboard.h"

// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT( dashboard )

// constructor - equivalent to ApplCore STARTUP
dashboard::dashboard( bncs_client_callback * parent, const char * path ) : bncs_script_helper( parent, path )
{
	m_tx=false;
	
	// show a panel from file p1.bncs_ui and we'll know it as our panel 1
	panelShow( 1, "p1.bncs_ui" );

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
//	setSize( 1024,668 );		// set the size explicitly
//	setSize( 1 );				// set the size to the same as the specified panel

	// connect to our infodriver host
	m_info = new ccInfodriver( this );

	// don't really hard code yer device number....
	m_info->connect( 123 );
}

// destructor - equivalent to ApplCore CLOSEDOWN
dashboard::~dashboard()
{
	delete m_info;
}

// all button pushes and notifications come here
void dashboard::buttonCallback( buttonNotify *b )
{
	if( b->panel() == 1 )
	{
		switch( b->id() )
		{
			case 1:			
			{
				// if the infodriver told us we're tx
				if (m_tx)
					// go write a client command to the network
					infoWrite(123, "hello", 1);

				// just for demo, lets also write a slot - note the use of (const char*) to convert bncs_string to STL string
				bncs_string s("hello" );
				m_info->setSlot(1, (const char*) s);
			}
		}
	}
}

// all revertives come here
int dashboard::revertiveCallback( revertiveNotify * r )
{
/*	switch( r->device() )
	{
		case 123:
 			textPut( "text", r->sInfo(), 1, 3 );
			break;
	}
*/	return 0;
}

// all database name changes come back here
void dashboard::databaseCallback( revertiveNotify * r )
{
}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string dashboard::parentCallback( parentNotify *p )
{
	if( p->command() == "return" )
	{
		bncs_stringlist sl;
		
		sl << "myParam=" + m_myParam;
		
		return sl.toString( '\n' );
	}
	else if( p->command() == "myParam" )
	{
		m_myParam = p->value();
	}
	return "";
}

// timer events come here
void dashboard::timerCallback( int id )
{
}



/** Request from client to change a slot
\param[in] device The device number
\param[in] index The slot number
\param[in] slot The requested contents to use
\note This command does not automatically update the contents of the slot
*/
void dashboard::cciSlotRequest( int device, int index, const string & slot )
{
	// convert incoming STL string to local bncs_string so all our favourite string-wrangling 
	// calls will work (STL string doesn't have many)
	bncs_string s( slot );

	// just use the string s as usual
	textPut( "text", "Request to set slot to:|" + s, 1, 4 );
}

/** Direct message from a client requesting workstation specific information on this device
\param[in] workstation The workstation requesting the information
\param[in] device The device number
\param[in] message The message string from the client
\param[in] reference An arbitary message reference that should be returned with the reply to this command
\sa ccInfodriver::clientMessage();
\note Implementation of this messaging mechanism is patchy so beware....
*/
void dashboard::cciClientMessage( int workstation, int device, const string & message, const string & clientHandle, const string & reference )
{
}

/** Notification of redundancy state
\param[in] state The current state
*/
void dashboard::cciRedundancyState( enum cc::redundancyState state )
{
	switch( state )
	{
		case cc::tx:
			// set our internal flag to know whether this automatic is active and we should
			// be sending client commands to the network
			m_tx=true;
			break;
		case cc::rx:
			m_tx=false;
			break;
	}
}

/** Connected event */
void dashboard::cciConnected( int )
{
	textPut( "text", "Connected to Infodriver", 1, 4 );
}

/** Disconnected event */
void dashboard::cciDisconnected( int )
{
	textPut( "text", "Connected to Infodriver", 1, 4 );
}