#ifndef dashboard_INCLUDED
	#define dashboard_INCLUDED

#include <bncs_script_helper.h>
#include <ccInfodriver.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT __declspec(dllimport) 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 

class dashboard : public bncs_script_helper, public IccInfodriverCallback
{
public:
	dashboard( bncs_client_callback * parent, const char* path );
	virtual ~dashboard();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
protected:
	virtual void cciSlotRequest( int device, int index, const string & slot );
	virtual void cciClientMessage( int workstation, int device, const string & message, const string & clientHandle, const string & reference );
	virtual void cciRedundancyState( enum cc::redundancyState state );
	virtual void cciConnected( int );
	virtual void cciDisconnected( int );

private:
	ccInfodriver *m_info;
	bool m_tx;
	bncs_string m_myParam;
	
};


#endif // dashboard_INCLUDED