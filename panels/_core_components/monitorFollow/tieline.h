// tieline.h: interface for the tieline class.
//
//////////////////////////////////////////////////////////////////////
#include "bncs_script_helper.h"

#if !defined(AFX_TIELINE_H__927AE807_C1B0_4ADC_B385_2AC240140FB5__INCLUDED_)
#define AFX_TIELINE_H__927AE807_C1B0_4ADC_B385_2AC240140FB5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class tieline  
{
public:
	tieline();
	virtual ~tieline();

	int device;
	int index_dest;
	int id_source_package;
	int index_source;
	bncs_string source_name;

};

#endif // !defined(AFX_TIELINE_H__927AE807_C1B0_4ADC_B385_2AC240140FB5__INCLUDED_)
