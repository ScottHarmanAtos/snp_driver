// direct.h: interface for the direct class.
//
//////////////////////////////////////////////////////////////////////
#include "bncs_script_helper.h"

#if !defined(AFX_DIRECT_H__08F586B6_4BF7_45E5_8417_00BB9C35B809__INCLUDED_)
#define AFX_DIRECT_H__08F586B6_4BF7_45E5_8417_00BB9C35B809__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class direct  
{
public:
	bncs_string routeType;	//This is the route type, i.e HD or SD and so on
	int device;
	int index_dest;
	int index_slaveDest;
	bncs_string source_name;
	int index_source;
	direct();
	virtual ~direct();

};

#endif // !defined(AFX_DIRECT_H__08F586B6_4BF7_45E5_8417_00BB9C35B809__INCLUDED_)
