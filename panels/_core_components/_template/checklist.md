[UI Components](../index.html)
# Component Publication Checklist

This is something you should run though when publishing a component:

- a terse one-line description
- a screenshot
- a fundamental type (UI building, custom popup editor, hidden)
- commands: whether design or runtime
- events
- what BNCS databases are used
- what object/workstation settings are required
- what instances/types
- what options are available to configure/modify/customise the UI (button naming conventions)
- \#pragma message about not using modified versions of core_components (but feel free to copy and mod)
- add "Core: " header to the description (the FileDescription in the version resources).
- correct company name, no colledia references, correct copyright
- a version history (and perhaps note where they originate from)
- an entry in an index or sampler page
- breadcrumbs linked at top of page to system root (the top level doc), section root (e.g. panels\core_components\index.html)