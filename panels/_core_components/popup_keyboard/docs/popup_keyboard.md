[UI Components](../../index.html)
# popup_keyboard

## Description
This is a popup script that is invoked from a simple navigate. It can be passed a "preset" on invocation, which is presented on a button on the form

## Commands

Name   | Type     | Use
-------|----------|-----------------
preset | optional | button is hidden if preset text is empty

## Notifications

Name     | Use 
---------|------
value=&lt;something&gt; | value from preset or keyboard when panel is dismissed
dismiss  | command to dismiss the popup

## Stylesheets
None
 