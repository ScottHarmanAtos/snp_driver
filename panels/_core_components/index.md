# Index of Core UI Components
These are reusable components that are usually taken from other systems but wrap up useful features.

## How to use them
These are released with the core system but also may be released independently. 

The major difference is that when released with the core system you'll get binaries, otherwise you have to build them yourself (so that the resulting DLLs match your current version of the core system).

## Router Components

### Generic
Component | Description
----------|------------
[Take](take/docs/take.html) | Router Take
[Lock](lock/docs/lock.html) | Destination Lock
[PTI](router_pti/docs/router_pti.html) | Router "Press To Indicate" (dest list for source)
[Dest](router_dest/docs/router_dest.html) | Router single destination button

### Audio
These are to support 5.1 audio routing. See an overview [here](audio/index.html)

Component | Description
----------|------------
[Audio Take](audio/audio_take/docs/audio_take.html) | Audio Take Button
[Audio Dest](audio/audio_dest/docs/audio_dest.html) | Audio Destination Button
[Audio List](audio/audio_list/docs/audio_list.html) | Audio List Control
[Audio Lock](audio/audio_lock/docs/audio_lock.html) | Audio Lock Control

## Navigation

Component | Description
----------|------------
[Adjust](adjust/docs/adjust.html) | Adjust Button

## Other Components

Component | Description
----------|------------
[AxonIdentity](axonIdentity/docs/axonIdentity.html) | Axon Identity
[Global XY](globalxy/docs/globalxy.html) | Generic XY panel

## See also...

|Component|Description|
|--|--|
|[Template](_template/_template.html)|Documentation Template|
|[Checklist](_template/checklist.html)|Pre-publication Checklist|
