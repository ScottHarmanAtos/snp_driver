# info_test Component

Component to run a sequence of tests on an infodriver

The script will initialise and obtain device information on the instance provided</br>
The status of this will show in the listbox

Pressing the "GO" button will start the tests

The current tests are:

Test Name|Description of what the test actually does
---------|----------------------------------------------
WriteAll|Iterates through every infodriver slot and writes test text containing a random number
ClearAll|Iterates through every infodriver slot and clears the contents
WriteAll Full|Iterates through every infodriver slot and writes test text to the full length allowed
Database<br/>Write|Iterates throguh a preset set of databases and indices and writes a key word and offset along with random text. An echo component (db_echo) then reverses the database write by adding the offset and changing the keyword with an echo word to stop howlround.

All tests are fully qualified in that it waits for a response and validates the text before moving onto the next test. As it is possible for the revertive not to be received (in the event of an error) there is also a timer system to trap for those errors. Out of bounds messages are also tried (such as index 0 and 4097) and trapped for with a no response indicating success.

Since the qualification does a full network path it is possible to time the message loop and the application keeps a running track of average time taken for the ongoing iterations on each test. This average is displayed in the listbox on test completion

Database tests require the echo component as a database change happens within CSI and the qualification does not rely on a network message. The echo component runs on the infodriver machine and when its CSI updates that entity of the database the message is turned around and looked for on the test machine

The database write trigger key section of the message text is "RUN_DATABASE_WRITE::" and the echo component changes this to "RUN_DATABASE_ECHO::". The echo component also takes the firstInt() value of the message and uses that as the offset to the database number so it is possible to return the message via a different database if required. Currentl;y this offset is set at 0 so the message comes back on the same database as it was sent. The return message always uses the same index number as sent.

## Commands

### instance
Instance of the device to test


