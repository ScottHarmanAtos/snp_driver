#ifndef info_test_INCLUDED
	#define info_test_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class info_test : public bncs_script_helper
{
public:
	info_test( bncs_client_callback * parent, const char* path );
	virtual ~info_test();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	int m_device;
	int m_slots;
	int m_testIndex;
	int m_database;
	bncs_stringlist m_database_set;
	bncs_string m_setText;
	bncs_string m_testText;
	int m_revertiveIndex;
	bncs_string m_revertiveText;
	int m_databaseIndex;
	bncs_string m_databaseText;
	int m_undefinedIndex;
	bncs_string m_undefinedText;
	int m_revertiveTryCount;
	bool m_ddeleteNext;
	bncs_dateTime m_startTime;
	bncs_dateTime m_endTime;
	__int64 m_testDuration;
	__int64 m_testAveDuration;
	int m_testCount;
	bool m_isGo;
	int m_loop;
	bool m_haveDatabaseKey;
	bool m_haveDatabaseEcho;

	enum tests {
		START,
		INSTANCE,
		REGISTER,
		HOLD,
		WRITE_ALL,
		CLEAR_ALL,
		WRITE_ALL_FULL,
		DATABASE_WRITE,
		FINISH,
		FAULT = 1000,
		TERMINATE = 2000
	};

	enum status {
		BEGIN,
		FOR,
		RUN,
		REVERTIVE,
		DATABASE,
		NEXT,
		COMPLETE,
		// FAULT CODES BELOW
		FAIL = 1000,
		FAIL_UNEXPECTED_REVERTIVE,
		FAIL_WRONG_REVERTIVE,
		FAIL_NO_REVERTIVE,
		FAIL_UNEXPECTED_DATABASE = 1101,
		FAIL_WRONG_DATABASE,
		FAIL_NO_DATABASE,
		FAIL_UNDEFINED = 1999,
		// END CODE
		END = 2000
	};

/*	enum status {
		START,
		INSTANCE,
		REGISTER,
		START_CLEAR_ALL,
		RUN_CLEAR_ALL,
		REVERTIVE_CLEAR_ALL,
		COMPLETE_CLEAR_ALL,
		START_WRITE_ALL,
		RUN_WRITE_ALL,
		REVERTIVE_WRITE_ALL,
		COMPLETE_WRITE_ALL,
		START_WRITE_ALL_FULL,
		RUN_WRITE_ALL_FULL,
		REVERTIVE_WRITE_ALL_FULL,
		COMPLETE_WRITE_ALL_FULL,
		START_DATABASE_WRITE,
		RUN_DATABASE_WRITE,
		DATABASE_DATABASE_WRITE,
		COMPLETE_DATABASE_WRITE,
		END,
		// FAULT CODES BELOW
		FAIL = 1000,
		FAIL_UNEXPECTED_REVERTIVE,
		FAIL_WRONG_REVERTIVE,
		FAIL_NO_REVERTIVE,
		// TERMINATE CODE
		TERMINATE = 2000
	};
	*/

	status m_status;
	tests m_tests;

	void runExec(int timer);
	void d(bncs_string msg, bool bisKeep = true);
	void runStatus(int index, bncs_string text);
	void clearStatus(void);
	void nextIndex(int inc = 1, int max = 4096);
	void stopwatchStart(void);
	void stopwatchStop(void);
	bool revertiveTryCount(int retries);


	void timerRun(tests t);
	int timerRunDuration(void);
	bncs_string padString(int len, bool isRandom = false);
	bncs_string responseTime(int div = 10, bncs_string units = "us");

	status clearAll(status s);
	status writeAll(status s);
	status writeAllFull(status s);
	status databaseWrite(status s);

};


#endif // info_test_INCLUDED