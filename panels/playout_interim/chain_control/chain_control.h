#ifndef chain_control_INCLUDED
	#define chain_control_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class chain_control : public bncs_script_helper
{
public:
	chain_control( bncs_client_callback * parent, const char* path );
	virtual ~chain_control();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_myParam;
	bncs_string m_instance;

	bncs_string m_opsArea;
	bncs_string m_opsPosition;
	bncs_string m_monDestPvw;

	bncs_string m_playoutMFR;
	bncs_string m_currentChannel;
	bncs_string m_selectChannel;
	bncs_string m_currentChainPanel;

	int m_devMFR;
	int m_slotAssign;
	int m_slotChannel;
	int m_slotRole;
	int m_slotFunction;

	bncs_string m_role;

	bool m_regionsVisible;
	bool m_isMFR;

	void init();
	void assignChannel(int role);
	void releaseChannel();
	void selectChannel(bncs_string channel);
	void updateChannel(bncs_string channel);
	void updateRole(bncs_string role);
	void showChainDetails();

	
};


#endif // chain_control_INCLUDED