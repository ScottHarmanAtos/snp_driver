// tests.h
#include <bncs_script_helper.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <bncs.h>
#include <Winbase.h>
#include <map>

class tests
{
public:
	tests();
	~tests();

	bncs_string name;
	void setTests(bncs_stringlist slTests);
	void setIndicies(bncs_stringlist slIndicies);
	bncs_string getTests(bool sorted = false);
	int count(void);
	int progress(void);
	void begin(void);
	bool end(void);
	bool next(void);
	int index(void);
	int item;
	bool setPass(bool b);
	bool isPass(void);
	bool invert;


private:
	bncs_stringlist testList;
	map<int, int> testMap;
	bncs_stringlist indexList;
	map<int, int> indexMap;
	int startIndex;
	int endIndex;
	map<int,int>::iterator test;
	bool pass;
};
