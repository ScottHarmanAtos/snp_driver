#include "stopwatch.h"

stopwatch::stopwatch()
{
	reset();
}


stopwatch::~stopwatch()
{
}

bool stopwatch::start(void)
{
	if (m_startTime.isNull())
	{
		m_startTime = bncs_dateTime::nowUTC();
		return true;
	}
	else
	{
		m_startTime = bncs_dateTime::nowUTC();
		return false;
	}
}

bool stopwatch::stop(void)
{
	if (m_startTime.isNull())
	{
		return false;
	}
	else
	{
		m_endTime = bncs_dateTime::nowUTC();
		m_testDuration = m_endTime.difference(m_startTime);
		m_testCount++;
		m_testAveDuration = ((m_testAveDuration * (m_testCount - 1)) + m_testDuration) / (m_testCount);

		return true;
	}
}

bncs_string stopwatch::duration(void)
{
	if (m_startTime.isNull() || m_endTime.isNull())
	{
		return "";
	}
	else
	{
		return bncs_string((long)(m_testDuration / 10));
	}
}

__int64 stopwatch::average(void)
{
	return m_testAveDuration;
}

int stopwatch::count(void)
{
	return m_testCount;
}

void stopwatch::reset(void)
{
	m_testAveDuration = 0;
	m_testCount = 0;
	m_startTime.clear();
	m_endTime.clear();
}

bncs_string stopwatch::millis(void)
{
	return bncs_string((long)(m_testAveDuration / 10000));
}

bncs_string stopwatch::micros(void)
{
	return bncs_string((long)(m_testAveDuration / 10));
}

bncs_string stopwatch::nanos(void)
{
	return bncs_string((long)(m_testAveDuration * 100));
}