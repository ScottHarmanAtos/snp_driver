#include <bncs_string.h>
#include <bncs_datetime.h>

class stopwatch
{
public:
	stopwatch();
	~stopwatch();

	bool start(void);
	bool stop(void);
	bncs_string duration(void);
	__int64 average(void);
	int count(void);
	void reset(void);
	bncs_string millis(void);
	bncs_string micros(void);
	bncs_string nanos(void);

private:
	bncs_dateTime m_startTime;
	bncs_dateTime m_endTime;
	__int64 m_testDuration;
	__int64 m_testAveDuration;
	int m_testCount;

};

