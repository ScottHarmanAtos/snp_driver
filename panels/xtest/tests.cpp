#include "tests.h"

tests::tests()
{
	invert = false;
}


tests::~tests()
{
}

void tests::setTests(bncs_stringlist slTests)
{
	testList = "";
	testMap.clear();

	if (slTests.count())
	{
		for (bncs_stringlistIterator slit = slTests.begin(); slit != slTests.end(); slit++)
		{
			bncs_string s = *slit;

			if (s.contains('-'))
			{
				bncs_string s1, s2;
				s.split('-', s1, s2);
				if (s1.firstInt() > 0 && s1.firstInt() < s2.firstInt() && s2.firstInt() <= MAX_SLOTS)
				{
					for (int i = s1.firstInt(); i <= s2.firstInt(); i++)
					{
						testMap[i] = i;
					}
					testList += bncs_string("%1-%2").arg(s1).arg(s2);
				}
			}
			else //if (s.firstInt() > 0 && s.firstInt() <= MAX_SLOTS)
			{
				testList += bncs_string(s.firstInt());
				testMap[s.firstInt()] = s.firstInt();
			}
		}
	}

	bncs_stringlist slOut = "";
	for (map<int, int>::iterator it = testMap.begin(); it != testMap.end(); it++)
	{
		slOut += it->second;
	}

	OutputDebugString(bncs_string("tests::setTests()::in   <%1>").arg(slTests));
	OutputDebugString(bncs_string("tests::setTests()::out  <%1>").arg(testList.toString()));
	OutputDebugString(bncs_string("tests::setTests()::sort <%1>").arg(slOut.toString()));
}

void tests::setIndicies(bncs_stringlist slIndicies)
{
	indexList = "";
	indexMap.clear();

	if (slIndicies.count())
	{
		for (bncs_stringlistIterator slit = slIndicies.begin(); slit != slIndicies.end(); slit++)
		{
			bncs_string s = *slit;

			if (s.contains('-'))
			{
				bncs_string s1, s2;
				s.split('-', s1, s2);
				if (s1.firstInt() > 0 && s1.firstInt() < s2.firstInt() && s2.firstInt() <= MAX_SLOTS)
				{
					for (int i = s1.firstInt(); i <= s2.firstInt(); i++)
					{
						indexMap[i] = i;
					}
					indexList += bncs_string("%1-%2").arg(s1).arg(s2);
				}
			}
			else //if (s.firstInt() > 0 && s.firstInt() <= MAX_SLOTS)
			{
				indexList += bncs_string(s.firstInt());
				indexMap[s.firstInt()] = s.firstInt();
			}
		}
	}

	/*
	bncs_stringlist slOut = "";
	for (map<int, int>::iterator it = indexMap.begin(); it != indexMap.end(); it++)
	{
		slOut += it->second;
	}

	OutputDebugString(bncs_string("tests::setTests()::in   <%1>").arg(slIndicies));
	OutputDebugString(bncs_string("tests::setTests()::out  <%1>").arg(indexList.toString()));
	OutputDebugString(bncs_string("tests::setTests()::sort <%1>").arg(slOut.toString()));
	*/
}

bncs_string tests::getTests(bool sorted)
{
	if (sorted)
	{
		bncs_stringlist slOut = "";
		for (map<int, int>::iterator it = testMap.begin(); it != testMap.end(); it++)
		{
			slOut += it->second;
		}
		return slOut.toString();
	}
	else
	{
		return testList.toString();
	}
}

int tests::count(void)
{
	return testMap.size();
}

int tests::progress(void)
{
	return (int)( (float)item / float(testMap.size()) * 100 );
}

void tests::begin(void)
{
	test = testMap.begin();
	item = 1;
	pass = true;
}

bool tests::end(void)	// return true if at end of tests
{
	if (test != testMap.end())
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool tests::next(void)	// return true if at end of tests
{
	if (end())
	{
		return true;
	}
	else
	{
		test++;
		item++;
		if (end())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

int tests::index(void)
{
	return test->second;
}

bool tests::setPass(bool b)
{
	pass = b;
	return pass;
}

bool tests::isPass(void)
{
	return (invert ? !pass : pass);
}