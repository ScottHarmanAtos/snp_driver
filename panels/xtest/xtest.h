#ifndef xtest_INCLUDED
	#define xtest_INCLUDED

#include <bncs_script_helper.h>

#ifdef WIN32
	#ifdef DOEXPORT_SCRIPT
		#define EXPORT_SCRIPT __declspec(dllexport) 
	#else
		#define EXPORT_SCRIPT 
	#endif
#else 
	#define EXPORT_SCRIPT
#endif
 
class xtest : public bncs_script_helper
{
public:
	xtest( bncs_client_callback * parent, const char* path );
	virtual ~xtest();

	void buttonCallback( buttonNotify *b );
	int revertiveCallback( revertiveNotify * r );
	void databaseCallback( revertiveNotify * r );
	bncs_string parentCallback( parentNotify *p );
	void timerCallback( int );
	
private:
	bncs_string m_instance;
	bncs_string m_layout = "default";
	bool isReady = false;
	bool canParse = false;
	bool haltOnFail = false;
	bool isRunning = false;

	test m_test;	
	tests m_tests;
	bncs_string m_testIn;
	bncs_string m_testOut;
	//CB_REVERTIVE m_result;
	stopwatch m_stopwatch;

	enum params
	{
		INVALID = 0,
		TIME_OF_DAY,
		TIME_MILLIS,
		INDEX,
		SOURCE,
		DEST,
		RAND,
		CHAR,
		ASCII
	};

	struct element
	{
		bncs_string text;
		params param;
		unsigned int value;
	};

	map<int, element> m_testTextMap;
	//map<int, element> m_testElementMap;

	void init(void);
	void timerSetup(void);
	void runTests(void);
	void runTest(int id);
	bool runCheck(bool result);
	void runClock(void);
	void runTimeout(void);
	int showProgress(bool clear = false);
	bool parseTestText(bncs_string text);
	bncs_string visualiseTestText(bncs_string &test);
	bool parseTestElement(bncs_string &parse, element &element);
	bncs_string stringifyTestText(bncs_string &text);
	bncs_string stringifyTestElement(element &element, int index, int source, int dest);
	void reset(void);
	bool checkChar(char c);
};


#endif // xtest_INCLUDED