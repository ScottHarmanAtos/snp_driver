// test.h
#include <bncs_script_helper.h>
#include <bncs_string.h>
#include <bncs_stringlist.h>
#include <bncs.h>

/*
CB_REVERTIVE

enum revertiveType
{
ROUTER=1,
INFODRIVER,
GPI,
DATABASE,
};
// the following may be optionally filled by different revertive types
// e.g. database is not used by the infodriver revertive type
int iDevice;			// device id
int iDatabase;			// infodriver - undefined / router - undefined / gpi - undefined / database - database number
int iIndex;				// infodriver - slot number / router - index / gpi - index / database - index
int iInfo;				// infodriver - always 0 / router - source number / gpi - state / database - unknown
char szInfo[ 256 ];		// infodriver - slot contents / router - source name / gpi - always null / database - database entry
enum revertiveType type;
const char *szDevice;	// NYI
const char *szIndex;	// NYI
*/

class test
{
public:
	test();
	~test();

	int device;
	int database;
	int index;
	int info;
	bncs_string sInfo;

	/*struct bncs_device
	{
		enum revertiveType
		{
			ROUTER = 1,
			INFODRIVER,
			GPI,
			DATABASE,
		};

		// the following may be optionally filled by different revertive types
		// e.g. database is not used by the infodriver revertive type
		int device;
		int iDatabase;
		int iIndex;				// infodriver slot number / router index
		int iInfo;				// router source number / gpi state
		char szInfo[256];		// infodriver slot contents / router source name
	};*/

	void setResult(revertiveNotify *r);
	void setType(bncs_string t);
	void setTest(int i, bncs_string t);
	bncs_string getTestText(void);


	CB_REVERTIVE::revertiveType type;

	//CB_REVERTIVE device;
	//CB_REVERTIVE result;

	//int device;	// devicenum
	//int index;	// info=slot, router=destnum, GPI=index
	//int info;	// info=, router=sourcenum, GPI=state
	int getType(void);
	bncs_string getTypeString(void);

	void setTimeout(int t);
	int getTimeout(void);
	void setClock(int c);
	int getClock(void);
	//void fail(void);
	//void pass(void);
	bool isPass(void);
	bool setPass(bool b);
	bool isKey(void);

private:
	//bncs_string sInfo;	//info=slotcont, router=DB0name, GPI=NULL
	//CB_REVERTIVE::revertiveType type = CB_REVERTIVE::ROUTER;	// 1=router, 2=info, 3=GPI, 4=database
	//CB_REVERTIVE result;
	bool isValid;
	int timeout;
	int clock;
	bool pass;
	bool key;

};


