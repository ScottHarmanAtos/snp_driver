#include <windows.h>
#include <stdio.h>
#include <bncs_string.h>
#include <bncs_config.h>
#include <bncs_datetime.h>
#include <ctime> // for seeding the random number generator
#include "test.h"
#include "tests.h"
#include "stopwatch.h"
#include "xtest.h"

#define PNL_MAIN	1

#define TIMER_SETUP		1
#define TIMER_SETUP_DUR	1

#define TIMER_CLOCK		5
#define TIMER_TIMEOUT	6



// this nasty little macro to make our class visible to the outside world
EXPORT_BNCS_SCRIPT(xtest)

// constructor - equivalent to ApplCore STARTUP
xtest::xtest(bncs_client_callback * parent, const char * path) : bncs_script_helper(parent, path)
{
	// show a panel from file p1.bncs_ui and we'll know it as our panel PNL_MAIN
	//panelShow(PNL_MAIN, "default.bncs_ui");

	// you may need this call to set the size of this component 
	//  if it's used in a popup window 
	//	setSize( 1024,668 );		// set the size explicitly
	//setSize(PNL_MAIN);		// set the size to the same as the specified panel
}

// destructor - equivalent to ApplCore CLOSEDOWN
xtest::~xtest()
{
}

// all button pushes and notifications come here
void xtest::buttonCallback(buttonNotify *b)
{
	b->dump("xtest::BCB::dump:");
	if (b->panel() == PNL_MAIN)
	{
		runTests();
	}
}

// all revertives come here
int xtest::revertiveCallback(revertiveNotify * r)
{
	r->dump("xtest::Revertive::dump:");
	debug("xtest::Revertive::type %1", r->type());

	m_test.setResult(r);

	runTest(m_test.getType());

	return 0;
}

// all database name changes come back here
void xtest::databaseCallback(revertiveNotify * r)
{
	r->dump("xtest::Database::dump:");
	debug("xtest::Database::type %1", r->type());

	m_test.setResult(r);

	runTest(m_test.getType());

}

// all parent notifications come here i.e. when this script is just one 
//  component of another dialog then our host might want to tell us things
bncs_string xtest::parentCallback(parentNotify *p)
{
	p->dump("xtest::PCB::dump:");
	if (p->command() == "return")
	{
		if (p->value() == "all")
		{	// Persisting values for bncs_vis_ed
			bncs_stringlist sl;

			sl << bncs_string("name=%1").arg(m_tests.name);
			sl << bncs_string("deviceType_RtrInfoGpiData=%1").arg(m_test.getTypeString());

			switch (m_test.getType())
			{
			case CB_REVERTIVE::ROUTER:
			case CB_REVERTIVE::INFODRIVER:
			case CB_REVERTIVE::GPI:
				sl << bncs_string("indices=%1").arg(m_tests.getTests());
				sl << bncs_string("test=%1").arg(m_testIn);
				break;
			case CB_REVERTIVE::DATABASE:
				sl << bncs_string("indices=%1").arg(m_tests.getTests());
				sl << bncs_string("test=%1").arg(m_testIn);
				sl << bncs_string("database=%1").arg(0);
				break;
			default:
				break;
			}

			sl << bncs_string("timeout=%1").arg(m_test.getTimeout());
			sl << bncs_string("clock=%1").arg(m_test.getClock());
			sl << bncs_string("halt_on_fail=%1").arg(haltOnFail ? "true" : "false");
			sl << bncs_string("invert_result=%1").arg(m_tests.invert ? "true" : "false");
			sl << bncs_string("layout=%1").arg(m_layout);

			return sl.toString('\n');
		}

		//		else if( p->value() == "myParam" )
		//		{	// Specific value being asked for by a textGet
		//			return( bncs_string( "%1=%2" ).arg( p->value() ).arg( m_myParam ) );
		//		}

	}
	else if (p->command() == "instance" && p->value() != m_instance)
	{	// Our instance is being set/changed
		m_instance = p->value();
		//Do something instance-change related here
		timerSetup();
	}

	else if (p->command() == "layout")
	{	// Persisted value or 'Command' being set here
		m_layout = p->value();
		if (m_layout.lower().endsWith(".bncs_ui"))
		{
			m_layout.remove(m_layout.length() - 8, 8);
		}
		timerSetup();
	}

	else if (p->command() == "name")
	{	// Persisted value or 'Command' being set here
		m_tests.name = p->value();
		timerSetup();
	}

	else if (p->command() == "database")
	{	// Persisted value or 'Command' being set here
		m_test.database = p->value().firstInt();
		timerSetup();
	}

	else if (p->command() == "indices")
	{	// Persisted value or 'Command' being set here
		m_tests.setTests(p->value());
		timerSetup();
	}

	else if (p->command() == "deviceType_RtrInfoGpiData")
	{	// Persisted value or 'Command' being set here
		m_test.setType(p->value());
		timerSetup();
	}

	else if (p->command() == "timeout")
	{	// Persisted value or 'Command' being set here
		m_test.setTimeout(p->value());
		timerSetup();
	}

	else if (p->command() == "clock")
	{	// Persisted value or 'Command' being set here
		m_test.setClock(p->value());
		timerSetup();
	}

	else if (p->command() == "halt_on_fail")
	{	// Persisted value or 'Command' being set here
		bncs_string s = p->value().lower().left(1);

		if (s == "y" || s == "1" || s == "t")	// yes, 1 or true
		{
			haltOnFail = true;
		}
		else
		{
			haltOnFail = false;
		}

		timerSetup();
	}

	else if (p->command() == "invert_result")
	{	// Persisted value or 'Command' being set here
		bncs_string s = p->value().lower().left(1);

		if (s == "y" || s == "1" || s == "t")	// yes, 1 or true
		{
			m_tests.invert = true;
		}
		else
		{
			m_tests.invert = false;
		}

		timerSetup();
	}

	else if (p->command() == "go")
	{	// Persisted value or 'Command' being set here
		runTests();
	}

	else if (p->command() == "test")
	{	// Persisted value or 'Command' being set here
		parseTestText(p->value());
	}

	else if (p->command() == "reset")
	{	// Persisted value or 'Command' being set here
		if (!isRunning && isReady)
		{
			reset();
		}
	}

	// ***** CONNECTIONS EVENTS HELPER LIST *****
	else if (p->command() == "_events")
	{	// Helper-list of everything in this component generated by hostNotify's
		bncs_stringlist sl;

		sl << "result=*";
		sl << "error";

		return sl.toString('\n');
	}

	// ***** CONNECTIONS COMMANDS HELPER LIST *****
	else if (p->command() == "_commands")
	{	// Helper-list of any commands/parameters you might want to set at run-time
		bncs_stringlist sl;

		sl << "go";

		return sl.toString('\n');
	}

	return "";
}

// timer events come here
void xtest::timerCallback(int id)
{
	switch (id)
	{
	case TIMER_SETUP:
		timerStop(id);
		init();
		break;

	case TIMER_CLOCK: case TIMER_TIMEOUT:
		debug("xtest::runTest::timeout and Clock timer stopped after timer callback");
		timerStop(TIMER_CLOCK);
		timerStop(TIMER_TIMEOUT);
		runTest(id);
		break;

	default:	// Unhandled timer event
		timerStop(id);
		break;
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Callbacks above - Methods below ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////


void xtest::timerSetup(void)
{
	// this is the timer startup trigger routine

	timerStart(TIMER_SETUP, TIMER_SETUP_DUR);
}

void xtest::init(void)
{
	debug("xtest::init");

	// this is the initialisation routine

	// randomize seed
	srand((unsigned)time(0));

	if (panelShow(PNL_MAIN, m_layout + ".bncs_ui"))
	{
		m_layout = "default";
		panelShow(PNL_MAIN, m_layout + ".bncs_ui");
	}
	setSize(PNL_MAIN);

	getDev(m_instance, &m_test.device);

	textPut("text", m_tests.name, PNL_MAIN, "name");
	textPut("text", "", PNL_MAIN, "instance");
	textPut("text", "", PNL_MAIN, "tests");

	if (m_test.device)
	{
		textPut("text", bncs_string("%1 (%2)").arg(m_instance).arg(m_test.device), PNL_MAIN, "instance");

		if (m_tests.count())
		{
			isReady = true;
			textPut("text", m_tests.getTests(true), PNL_MAIN, "tests");
		}
		else
		{
			isReady = false;
			textPut("text", "Check Config", PNL_MAIN, "result");
		}
	}

	/*
	switch (m_test.getType)
	{
	case CB_REVERTIVE::ROUTER:
	routerRegister(m_test.device,);
	break;

	case CB_REVERTIVE::INFODRIVER:
	break;

	case CB_REVERTIVE::GPI:
	break;

	case CB_REVERTIVE::DATABASE:
	break;
	}
	*/
}

void xtest::runTests(void)
{
	if (isReady && !isRunning)
	{
		runTest(0);
	}
	else
	{
		hostNotify("error");
	}
}

void xtest::runTest(int id)
{
	debug("xtest::runTest::id(%1)", id);

	switch (id)
	{
	case 0:	// initial
		reset();
		isRunning = true;
		runClock();
		break;

	case 1: case 2: case 3: case 4:	// router / infodriver / gpi / database
		m_stopwatch.stop();
		debug("xtest::runTest::Clock timer stopped after case1-4");
		timerStop(TIMER_CLOCK);
		
		textPut("text", bncs_string("%1us").arg(m_stopwatch.duration()), PNL_MAIN, "test_time");

		// check if the test was a pass
		if (runCheck(m_test.isPass()))
		{
			debug("xtest::runTest::timeout timer stopped after runCheck returned true");
			timerStop(TIMER_TIMEOUT);
		}
		break;

	case 5:	// Clock
		m_test.setTest(m_tests.index(), stringifyTestText(m_testOut));	// set the testing index and text
		textPut("text", bncs_string("%1 of %2").arg(m_tests.item).arg(m_tests.count()), PNL_MAIN, "index");
		textPut("text", m_test.index, PNL_MAIN, "index");
		textPut("text", stringifyTestText(m_testOut), PNL_MAIN, "test_text");
		debug("xtest::runTest::clock - test item %1, index %2", m_tests.item, m_test.index);

		infoUnregister(m_test.device);
		infoRegister(m_test.device, m_test.index, m_test.index);	// this should also register for a router or GPI, it's just the same
		runTimeout();		// initiate the timeout timer
		m_stopwatch.start();	// start the stopwatch

		// write routine
		switch (m_test.getType())
		{
		case CB_REVERTIVE::ROUTER:
			debug("xtest::runTest::Sending RC %1 %2 %3", m_test.device, m_test.info, m_test.index);
			routerCrosspoint(m_test.device, m_test.info, m_test.index, true);
			break;
		case CB_REVERTIVE::INFODRIVER:
			debug("xtest::runTest::Sending IW %1 '%2' %3", m_test.device, m_test.sInfo, m_test.index);
			infoWrite(m_test.device, m_test.sInfo, m_test.index, true);
			break;
		case CB_REVERTIVE::GPI:
			debug("xtest::runTest::Sending RC %1 %2 %3", m_test.device, m_test.info, m_test.index);
			gpiSwitch(m_test.device, m_test.info, m_test.index, true);
			break;
		case CB_REVERTIVE::DATABASE:
			debug("xtest::runTest::Sending RM %1 %2 %3 '%4'", m_test.device, m_test.database, m_test.index, m_test.sInfo);
			routerModify(m_test.device, m_test.database, m_test.index, m_test.sInfo, false);
			break;
		default:
			debug("xtest::runTest::CAUTION, type not defined");
			break;
		}

		break;

	case 6:	default: // timeout timer
		m_stopwatch.stop();
		m_test.setPass(false);
		m_tests.setPass(false);
		debug("xtest::runTest::test FAIL - timeout");

		runCheck(false);
		break;

	}


}

bool xtest::runCheck(bool bResult)	// returns true if the runclock is instigated
{
	if (bResult)
	{
		// this is a pass
		debug("xtest::runCheck::test PASS");
		textPut("text", bncs_string("This PASS, Overall %1").arg(m_tests.isPass() ? "PASS" : "FAIL"), PNL_MAIN, "result");
	}
	else
	{
		// this is a fail
		m_tests.setPass(false);
		debug("xtest::runCheck::test FAIL - check");
		textPut("text", bncs_string("This FAIL, Overall %1").arg(m_tests.isPass() ? "PASS" : "FAIL"), PNL_MAIN, "result");	// this should always be fail/fail !!!
	}

	debug("xtest::runCheck::progress %1\%", showProgress());

	// we need to see if this is the local or remote echo of the database
	if (m_test.getType() == CB_REVERTIVE::DATABASE && m_test.isKey() && m_test.isPass())
	{
		// if the first database rev is good then don't increment or run the clock, just wait for remote response
		debug("xtest::runCheck::We are a database with a pass key response");
		return false;
	}
	else
	{
		// stop the timeout timer
		timerStop(TIMER_TIMEOUT);

		// go to the next test
		m_tests.next();

		// do some checks
		if (m_tests.end() || (haltOnFail & !m_test.isPass()))
		{
			hostNotify(bncs_string("result=%1").arg(m_tests.isPass() ? "pass" : "fail"));
			textPut("text", "", PNL_MAIN, "index");
			textPut("text", "", PNL_MAIN, "test_text");
			textPut("text", "", PNL_MAIN, "test_time");
			textPut("text", bncs_string("Overall %1 - Ave %2us").arg(m_tests.isPass() ? "Pass" : "Fail").arg(m_stopwatch.micros()), PNL_MAIN, "result");
			isRunning = false;
			infoUnregister(m_test.device);
			debug("xtest::runCheck::we are at the end");
			return false;
		}
		else
		{
			runClock();
			debug("xtest::runCheck::instigating next test");
			return true;
		}
	}

}

void xtest::runClock(void)
{
	debug("xtest::runClock::Clock timer started");
	timerStart(TIMER_CLOCK, m_test.getClock());
}

void xtest::runTimeout(void)
{
	debug("xtest::runTimeout::Timeout timer started");
	timerStart(TIMER_TIMEOUT, m_test.getTimeout());
}

int xtest::showProgress(bool clear)
{
	if (clear)
	{
		textPut("progress", "0", PNL_MAIN, "progress");
		return 0;
	}
	else
	{
		textPut("progress", m_tests.progress(), PNL_MAIN, "progress");
		textPut("colour.bar", m_tests.isPass() ? "green" : "red", PNL_MAIN, "progress");
		return m_tests.progress();
	}
}

bool xtest::parseTestText(bncs_string test)
{
	debug("xtest::parseTest::m_testIn <%1>", test);
	canParse = true;
	m_testIn = test;
	m_testOut = "";
	m_testTextMap.clear();
	int i = 0;

	while (test.contains('#'))
	{
		bncs_string s, sl, sm, sr;
		test.split('#', sl, s);
		debug("xtest::parseTest::split left <%1>, right <%2>", sl, s);

		if (s.contains('#'))
		{
			s.split('#', sm, sr);
			// sm now contains the test element, try parsing it
			element e;
			if (parseTestElement(sm, e))
			{
				m_testOut += sl;
				m_testOut += bncs_string("%");
				m_testOut += bncs_string(i + 1);
				m_testTextMap.insert(pair<int, element>(i, e));
				test = sr;
				i++;
				debug("xtest::parseTest::pair added <%1>", sm);
			}
			else
			{
				canParse = false;
				break;
			}
		}
		else
		{
			debug("xtest::parseTest::# containder mismatch, cannot parse");
			canParse = false;
			break;
		}
	}

	if (canParse)
	{
		m_testOut += test;
		debug("xtest::parseTest::number of replacements %1", (int)m_testTextMap.size());
		bncs_string s = m_testOut;
		debug("xtest::parseTest::m_testOut %1", visualiseTestText(m_testOut));
	}
	else
	{
		m_testOut = "";
		m_testTextMap.clear();
	}

	return canParse;
}

bncs_string xtest::visualiseTestText(bncs_string &text)
{
	bncs_string ret = text;
	for (int i = 0; i < (int)m_testTextMap.size(); i++)
	{
		//debug("xtest::visualiseTestText::replacement %1 is <%2>", i + 1, m_testTextMap[i].text);
		ret = ret.arg(bncs_string("#%1#").arg(m_testTextMap[i].text));
	}
	return ret;
}

bool xtest::parseTestElement(bncs_string &parse, element &element)
{
	/*
	these are our elements:
	enum params
	{
	INVALID, (0)
	TIME_OF_DAY, (t)
	TIME_MILLIS, (m)
	INDEX, (i)
	SOURCE, (s)
	DEST, (d)
	RAND, (r)
	CHAR, (c)
	ASCII, (a)
	};
	*/

	if (parse.length())
	{
		switch (parse[0])
		{
		case 't':
			element.param = TIME_OF_DAY;
			element.value = 0;
			break;

		case 'm':
			element.param = TIME_MILLIS;
			element.value = 0;
			break;

		case 'i':
			element.param = INDEX;
			element.value = 0;
			break;

		case 's':
			element.param = SOURCE;
			element.value = 0;
			break;

		case 'd':
			element.param = DEST;
			element.value = 0;
			break;

		case 'r':
			element.param = RAND;
			if (parse.length() == 1)
			{
				element.value = 0;
			}
			else
			{
				element.value = parse.right(parse.length() - 1).firstInt();
			}
			break;

		case 'c':
			element.param = CHAR;
			if (parse.length() == 1)
			{
				element.value = 255;
			}
			else
			{
				element.value = parse.right(parse.length() - 1).firstInt();
			}
			break;

		case 'a':
			element.param = ASCII;
			if (parse.length() == 1)
			{
				element.value = 255;
			}
			else
			{
				element.value = parse.right(parse.length() - 1).firstInt();
			}
			break;

		default:
			debug("xtest::parseTestElement::unknown param <%1>", parse);
			element.text = "";
			element.param = INVALID;
			element.value = 0;
			return false;
			break;
		}
	}
	else
	{
		debug("xtest::parseTestElement::nothing to parse");
		return false;
	}
	element.text = parse;
	debug("xtest::parseTestElement::parse <%1>, param <%2>, value <%3>", parse, element.param, (int)element.value);
	return true;
}

bncs_string xtest::stringifyTestText(bncs_string &text)
{
	//debug("xtest::stringifyTestText::text in %1", visualiseTestText(text));
	bncs_string ret = text;
	for (int i = 0; i < (int)m_testTextMap.size(); i++)
	{
		//debug("xtest::stringifyTestText::element %1", i);
		ret = ret.arg(stringifyTestElement(m_testTextMap[i], m_tests.index(), 0, m_tests.index()));
	}
	return ret;
}

bncs_string xtest::stringifyTestElement(element &element, int index, int source, int dest)
{
	/*
	these are our elements:
	enum params
	{
	TIME_OF_DAY, (t)
	TIME_MILLIS, (m)
	INDEX, (i)
	SOURCE, (s)
	DEST, (d)
	RAND, (r)
	CHAR, (c)
	ASCII, (a)
	};
	*/
	bncs_string s = "";

	switch (element.param)
	{
	case TIME_OF_DAY:
		s = bncs_dateTime::now().toString("dddd ddddd MMMM yyyy hh:mm:ss:zzz");
		break;

	case TIME_MILLIS:
		s = bncs_dateTime::now().toString("ss:zzz");
		break;

	case INDEX:
		s = index;
		break;

	case SOURCE:
		s = source;
		break;

	case DEST:
		s = dest;
		break;

	case RAND:
	{
		char c;
		while (s.length() < element.value && element.value > 0)
		{
			do
			{
				c = (rand() % 95) + 32;	// rand() % 95 = 0 -> 94; add 32; range = 32 -> (94+32) = 32 -> 126
			} while (checkChar(c));

			s.append((char)c);
			//debug("xtest::strigifyTestElement::RAND added char <%1>", (int)c);
		}
		break;
	}

	case CHAR:
	{
		char c = 32;
		while (s.length() < element.value && element.value > 0)
		{
			do
			{
				c++;	// range = 32 -> 126
			} while (checkChar(c));

			if (c > 126)
			{
				c = 32;
			}

			s.append((char)c);
			//debug("xtest::strigifyTestElement::CHAR added char <%1>", (int)c);
		}
		break;
	}

	case ASCII:
		s = (char)element.value;
		break;

	default:
		debug("xtest::strigifyTestElement::unknown param <%1>", element.param);
		return "";
		break;
	}
	//debug("xtest::parseTestElement::parse <%1>, param <%2>, value <%3>", parse, element.param, element.value);
	return s;
}
void xtest::reset(void)
{
	m_tests.begin();
	m_stopwatch.reset();
	textPut("text", "", PNL_MAIN, "test");
	textPut("text", "", PNL_MAIN, "index");
	textPut("text", "", PNL_MAIN, "test_text");
	textPut("text", "", PNL_MAIN, "test_time");
	textPut("text", "", PNL_MAIN, "result");
	showProgress(true);
}

bool xtest::checkChar(char c)
{
	// returns true if char is invalid

	/*	Notes on BNCS reserved characters
	=================================
	Char	Desc			Dec	Hex
	:		colon			58	3A
	'		single quote	39	27
	"		double quote	34	22
	.		period			46	2E
	=		equal sign		61	3D
	|		pipe			124	7C
	%		percent			37	25	*/

	/*  So as not to confuse the parser we should also exclude
	Char	Desc			Dec	Hex
	,		comma			44	2C	*/

	/*  ...and bncs_csi is currently not coding some other characters properly we should also exclude
	Char	Desc			Dec	Hex
	<		less than		60	3C
	>		greater than	62	3E
	�		pound			??	?? */

	return (c == 58 || c == 39 || c == 34 || c == 46 || c == 61 || c == 124 || c == 37 || c == 44 || c == 60 || c == 62 || c == '�');
}