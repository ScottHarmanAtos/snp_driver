#include "test.h"

#define MAX_TIMEOUT	15000
#define MIN_TIMEOUT 40
#define MAX_CLOCK	100
#define MIN_CLOCK	1

#define TRIGGER_KEY "RUN_DATABASE_WRITE::"
#define TRIGGER_RESPONSE "RUN_DATABASE_ECHO::"


test::test()
{
	timeout = MIN_TIMEOUT;
	clock = MIN_CLOCK;
	key = true;
}


test::~test()
{
}

void test::setType(bncs_string t)
{

	/*
	This is defined in CB_REVETIVE as :

	\code
	enum revertiveType
	{
		ROUTER = 1,		// 1
		INFODRIVER,		// 2
		GPI,			// 3
		DATABASE,		// 4
	};
	\endcode
	*/
		
	if (t.lower().startsWith('r'))
	{
		type = CB_REVERTIVE::ROUTER;
	}
	else if (t.lower().startsWith('i'))
	{
		type = CB_REVERTIVE::INFODRIVER;
	}
	else if (t.lower().startsWith('g'))
	{
		type = CB_REVERTIVE::GPI;
	}
	else if (t.lower().startsWith('d'))
	{
		type = CB_REVERTIVE::DATABASE;
	}
	else
	{
		type = CB_REVERTIVE::ROUTER;
	}
	OutputDebugString(bncs_string("test::setType::ttype set to %1").arg(type));
}

void test::setTest(int i, bncs_string t)
{
	// for infodriver
	// i - index
	// t - text

	index = i;
	sInfo = t.left(MAX_SLOTSIZE);
	info = t.firstInt();
	pass = false;
	key = true;
}

/*bncs_string test::getTestText(void)
{
	return sInfo;
}*/

int test::getType(void)
{
	switch (type)
	{
	case CB_REVERTIVE::ROUTER:
		return 1;
	case CB_REVERTIVE::INFODRIVER:
		return 2;
	case CB_REVERTIVE::GPI:
		return 3;
	case CB_REVERTIVE::DATABASE:
		return 4;
	default:
		return 1;
	}
}

bncs_string test::getTypeString(void)
{
	switch (type)
	{
	case CB_REVERTIVE::ROUTER:
		return "Rtr";
	case CB_REVERTIVE::INFODRIVER:
		return "Info";
	case CB_REVERTIVE::GPI:
		return "Gpi";
	case CB_REVERTIVE::DATABASE:
		return "Data";
	default:
		return "Rtr";
	}
}

void test::setResult(revertiveNotify *r)
{
	OutputDebugString(bncs_string("test::setResult::test      device<%1>, index<%2>, database <%3>, sInfo<%4>, info<%5>").arg(device).arg(index).arg(database).arg(sInfo).arg(info));
	OutputDebugString(bncs_string("test::setResult::revertive device<%1>, index<%2>, database <%3>, sInfo<%4>, info<%5>").arg(r->device()).arg(r->index()).arg(r->database()).arg(r->sInfo()).arg(r->info()));

	switch (type)
	{
	case CB_REVERTIVE::ROUTER:
		pass = r->device() == device &&	r->index() == index && r->info() == info;
		break;
	case CB_REVERTIVE::INFODRIVER:
		pass = r->device() == device &&	r->index() == index && r->sInfo() == sInfo;
		break;
	case CB_REVERTIVE::GPI:
		pass = r->device() == device &&	r->index() == index && r->info() == info;
		break;
	case CB_REVERTIVE::DATABASE:
		if (r->sInfo().find(TRIGGER_KEY) > -1)	// revertive contained the key - local response
		{
			OutputDebugString(bncs_string("test::setResult::Database revertive contains key"));
			pass = r->device() == device &&	r->index() == index && r->database() == database && r->sInfo() == sInfo;
			key = true;
		}
		else if (r->sInfo().find(TRIGGER_RESPONSE) > -1)	// revertive contained the response - remote response
		{
			OutputDebugString(bncs_string("test::setResult::Database revertive contains response"));
			bncs_string s = sInfo;
			s.replace(TRIGGER_KEY, TRIGGER_RESPONSE);	// switch key for response
			OutputDebugString(bncs_string("test::setResult::Looking for revertive of %1").arg(s));

			pass = r->device() == device &&	r->index() == index && r->database() == database && r->sInfo() == s;
			key = false;
		}
		else
		{
			pass = false;
		}
		break;
	default:
		pass = false;	// this should never happen
		break;
	}

	OutputDebugString(bncs_string("test::setResult::%1").arg(pass ? "PASSED" : "FAILED"));
}

void test::setTimeout(int t)
{
	if (t > MAX_TIMEOUT)
	{
		timeout = MAX_TIMEOUT;
	}
	else if (t < MIN_TIMEOUT)
	{
		timeout = MIN_TIMEOUT;
	}
	else
	{
		timeout = t;
	}
}

int test::getTimeout(void)
{
	return timeout;
}

void test::setClock(int c)
{
	if (c > MAX_CLOCK)
	{
		clock = MAX_CLOCK;
	}
	else if (c < MIN_CLOCK)
	{
		clock = MIN_CLOCK;
	}
	else
	{
		clock = c;
	}
}

int test::getClock(void)
{
	return clock;
}

bool test::setPass(bool b)
{
	pass = b;
	return pass;
}

bool test::isPass(void)
{
	return pass;
}

bool test::isKey(void)
{
	return key;
}
