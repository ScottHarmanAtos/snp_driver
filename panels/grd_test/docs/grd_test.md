# grd_test panel

This utility should be run from Vis_Ed.
It polls all destinations then routes source 2 to each destination followed by the highest source number to each destination before finally returning each destination to its original source.
Should a revertive be missed or lost it will try 3 times before declaring a failure.


![Example](example.png)

Example of what it should look like

## Commands

## instance
Instance of the GRD is entered into the panel edit dialog, which is then looked up in instances.xml

## index
The size of both the source and destination databases is found automatically

## Stylesheets
None
